using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.NewsletterSubscriptionModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "NewsletterSubscription")]
    public abstract class NewsletterSubscriptionBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  INewsletterSubscriptionBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static NewsletterSubscriptionBaseFactory Factory
        {
            get
            {
                return NewsletterSubscriptionBaseFactory.Instance; 
            }
        }    
        /*
        public static NewsletterSubscriptionBase CreateNewItem()
        {
            return (NewsletterSubscriptionBase)Factory.CreateNewItem();
        }

		public static NewsletterSubscriptionBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<NewsletterSubscriptionBase, NewsletterSubscriptionBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static NewsletterSubscriptionBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static NewsletterSubscriptionBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<NewsletterSubscriptionBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<NewsletterSubscriptionBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<NewsletterSubscriptionBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _email;
        
        public virtual string Email 
        {
        	get
        	{
        		return _email;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_email,value);
        		_email = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _name;
        
        public virtual string Name 
        {
        	get
        	{
        		return _name;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_name,value);
        		_name = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _telephone;
        
        public virtual string Telephone 
        {
        	get
        	{
        		return _telephone;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_telephone,value);
        		_telephone = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _dateadded;
        
        public virtual DateTime DateAdded 
        {
        	get
        	{
        		return _dateadded;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_dateadded,value);
        		_dateadded = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _enabled;
        
        public virtual bool Enabled 
        {
        	get
        	{
        		return _enabled;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_enabled,value);
        		_enabled = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _registeredipaddress;
        
        public virtual string RegisteredIPAddress 
        {
        	get
        	{
        		return _registeredipaddress;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_registeredipaddress,value);
        		_registeredipaddress = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
