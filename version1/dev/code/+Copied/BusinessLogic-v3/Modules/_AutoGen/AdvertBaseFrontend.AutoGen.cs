using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.AdvertModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.AdvertModule.AdvertBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.AdvertModule.IAdvertBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.AdvertModule.AdvertBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.AdvertModule.IAdvertBase item)
        {
        	return BusinessLogic_v3.Frontend.AdvertModule.AdvertBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class AdvertBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<AdvertBaseFrontend, BusinessLogic_v3.Modules.AdvertModule.IAdvertBase>

    {
		
        
        protected AdvertBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
