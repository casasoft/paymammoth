using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.MemberReferralCommissionModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IMemberReferralCommissionBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        DateTime DateIssued { get; set; }
		//Iproperty_normal
        double Amount { get; set; }
		//Iproperty_normal
        BusinessLogic_v3.Enums.MEMBER_REFERRAL_COMMISSION_STATUS Status { get; set; }
		//Iproperty_normal
        DateTime PaidOn { get; set; }
		//Iproperty_normal
        BusinessLogic_v3.Enums.MEMBER_REFERRAL_COMMISSION_PAID_BY PaidBy { get; set; }
		//Iproperty_normal
        string PaymentRemarks { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.MemberReferralModule.IMemberReferralBase MemberReferral {get; set; }

   

// [interface_base_collections]

 
		

    	
    	
      
      

    }
}
