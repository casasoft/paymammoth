using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.ProductVariationColour_CultureInfoModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "ProductVariationColour_CultureInfo")]
    public abstract class ProductVariationColour_CultureInfoBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IProductVariationColour_CultureInfoBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
      , IMultilingualContentInfo
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static ProductVariationColour_CultureInfoBaseFactory Factory
        {
            get
            {
                return ProductVariationColour_CultureInfoBaseFactory.Instance; 
            }
        }    
        /*
        public static ProductVariationColour_CultureInfoBase CreateNewItem()
        {
            return (ProductVariationColour_CultureInfoBase)Factory.CreateNewItem();
        }

		public static ProductVariationColour_CultureInfoBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<ProductVariationColour_CultureInfoBase, ProductVariationColour_CultureInfoBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static ProductVariationColour_CultureInfoBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static ProductVariationColour_CultureInfoBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<ProductVariationColour_CultureInfoBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<ProductVariationColour_CultureInfoBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<ProductVariationColour_CultureInfoBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? CultureInfoID
		{
		 	get 
		 	{
		 		return (this.CultureInfo != null ? (long?)this.CultureInfo.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase _cultureinfo;
        public virtual BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase CultureInfo 
        {
        	get
        	{
        		return _cultureinfo;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_cultureinfo,value);
        		_cultureinfo = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase BusinessLogic_v3.Modules._AutoGen.IProductVariationColour_CultureInfoBaseAutoGen.CultureInfo 
        {
            get
            {
            	return this.CultureInfo;
            }
            set
            {
            	this.CultureInfo = (BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase) value;
            }
        }
            
/*
		public virtual long? ProductVariationColourID
		{
		 	get 
		 	{
		 		return (this.ProductVariationColour != null ? (long?)this.ProductVariationColour.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ProductVariationColourModule.ProductVariationColourBase _productvariationcolour;
        public virtual BusinessLogic_v3.Modules.ProductVariationColourModule.ProductVariationColourBase ProductVariationColour 
        {
        	get
        	{
        		return _productvariationcolour;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_productvariationcolour,value);
        		_productvariationcolour = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ProductVariationColourModule.IProductVariationColourBase BusinessLogic_v3.Modules._AutoGen.IProductVariationColour_CultureInfoBaseAutoGen.ProductVariationColour 
        {
            get
            {
            	return this.ProductVariationColour;
            }
            set
            {
            	this.ProductVariationColour = (BusinessLogic_v3.Modules.ProductVariationColourModule.ProductVariationColourBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _title;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)




#region IMultilingualContentInfo Members

        long IMultilingualContentInfo.CultureInfoID
        {
            get
            {                          
            	if (this.CultureInfo != null)
                	return this.CultureInfo.ID;
               	else
               		return 0;
            }
        }
  		BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject IMultilingualContentInfo.GetLinkedItem()
        {
            return this.ProductVariationColour;
        }
#endregion



#endregion       

		

    }
}
