using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.CultureInfoModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.CultureInfoModule.CultureInfoBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.CultureInfoModule.ICultureInfoBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.CultureInfoModule.CultureInfoBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.CultureInfoModule.ICultureInfoBase item)
        {
        	return BusinessLogic_v3.Frontend.CultureInfoModule.CultureInfoBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class CultureInfoBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<CultureInfoBaseFrontend, BusinessLogic_v3.Modules.CultureInfoModule.ICultureInfoBase>

    {
		
        
        protected CultureInfoBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
