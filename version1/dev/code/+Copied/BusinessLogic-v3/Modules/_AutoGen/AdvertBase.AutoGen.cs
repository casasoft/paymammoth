using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.AdvertModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "Advert")]
    public abstract class AdvertBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IAdvertBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static AdvertBaseFactory Factory
        {
            get
            {
                return AdvertBaseFactory.Instance; 
            }
        }    
        /*
        public static AdvertBase CreateNewItem()
        {
            return (AdvertBase)Factory.CreateNewItem();
        }

		public static AdvertBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<AdvertBase, AdvertBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static AdvertBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static AdvertBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<AdvertBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<AdvertBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<AdvertBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? AdvertSlotID
		{
		 	get 
		 	{
		 		return (this.AdvertSlot != null ? (long?)this.AdvertSlot.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.AdvertSlotModule.AdvertSlotBase _advertslot;
        public virtual BusinessLogic_v3.Modules.AdvertSlotModule.AdvertSlotBase AdvertSlot 
        {
        	get
        	{
        		return _advertslot;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_advertslot,value);
        		_advertslot = value;
        	}
        }
            
        BusinessLogic_v3.Modules.AdvertSlotModule.IAdvertSlotBase BusinessLogic_v3.Modules._AutoGen.IAdvertBaseAutoGen.AdvertSlot 
        {
            get
            {
            	return this.AdvertSlot;
            }
            set
            {
            	this.AdvertSlot = (BusinessLogic_v3.Modules.AdvertSlotModule.AdvertSlotBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _description;
        
        public virtual string Description 
        {
        	get
        	{
        		return _description;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_description,value);
        		_description = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _filename;
        
        public virtual string Filename 
        {
        	get
        	{
        		return _filename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_filename,value);
        		_filename = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _advertshownperround;
        
        public virtual int AdvertShownPerRound 
        {
        	get
        	{
        		return _advertshownperround;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_advertshownperround,value);
        		_advertshownperround = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _roundcounter;
        
        public virtual int RoundCounter 
        {
        	get
        	{
        		return _roundcounter;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_roundcounter,value);
        		_roundcounter = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _videoduration;
        
        public virtual int VideoDuration 
        {
        	get
        	{
        		return _videoduration;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_videoduration,value);
        		_videoduration = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _link;
        
        public virtual string Link 
        {
        	get
        	{
        		return _link;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_link,value);
        		_link = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _showdatefrom;
        
        public virtual DateTime? ShowDateFrom 
        {
        	get
        	{
        		return _showdatefrom;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_showdatefrom,value);
        		_showdatefrom = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _showdateto;
        
        public virtual DateTime? ShowDateTo 
        {
        	get
        	{
        		return _showdateto;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_showdateto,value);
        		_showdateto = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.HREF_TARGET _hreftarget;
        
        public virtual CS.General_v3.Enums.HREF_TARGET HrefTarget 
        {
        	get
        	{
        		return _hreftarget;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_hreftarget,value);
        		_hreftarget = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _backgroundcolor;
        
        public virtual string BackgroundColor 
        {
        	get
        	{
        		return _backgroundcolor;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_backgroundcolor,value);
        		_backgroundcolor = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __AdvertHitsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.AdvertModule.AdvertBase, 
        	BusinessLogic_v3.Modules.AdvertHitModule.AdvertHitBase,
        	BusinessLogic_v3.Modules.AdvertHitModule.IAdvertHitBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.AdvertModule.AdvertBase, BusinessLogic_v3.Modules.AdvertHitModule.AdvertHitBase>
        {
            private BusinessLogic_v3.Modules.AdvertModule.AdvertBase _item = null;
            public __AdvertHitsCollectionInfo(BusinessLogic_v3.Modules.AdvertModule.AdvertBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.AdvertHitModule.AdvertHitBase> Collection
            {
                get { return _item.__collection__AdvertHits; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.AdvertModule.AdvertBase, BusinessLogic_v3.Modules.AdvertHitModule.AdvertHitBase>.SetLinkOnItem(BusinessLogic_v3.Modules.AdvertHitModule.AdvertHitBase item, BusinessLogic_v3.Modules.AdvertModule.AdvertBase value)
            {
                item.Advert = value;
            }
        }
        
        private __AdvertHitsCollectionInfo _AdvertHits = null;
        public __AdvertHitsCollectionInfo AdvertHits
        {
            get
            {
                if (_AdvertHits == null)
                    _AdvertHits = new __AdvertHitsCollectionInfo((BusinessLogic_v3.Modules.AdvertModule.AdvertBase)this);
                return _AdvertHits;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.AdvertHitModule.IAdvertHitBase> IAdvertBaseAutoGen.AdvertHits
        {
            get {  return this.AdvertHits; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.AdvertHitModule.AdvertHitBase> __collection__AdvertHits
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'AdvertHitBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
