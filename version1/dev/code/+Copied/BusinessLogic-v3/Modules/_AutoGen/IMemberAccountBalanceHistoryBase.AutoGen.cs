using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IMemberAccountBalanceHistoryBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.MemberModule.IMemberBase Member {get; set; }

		//Iproperty_normal
        double BalanceUpdate { get; set; }
		//Iproperty_normal
        DateTime Date { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.OrderModule.IOrderBase OrderLink {get; set; }

		//Iproperty_normal
        BusinessLogic_v3.Enums.MEMBER_ACCOUNT_BALANCE_HISTORY_UPDATE_TYPE UpdateType { get; set; }
		//Iproperty_normal
        double BalanceTotal { get; set; }
		//Iproperty_normal
        string Comments { get; set; }
		//Iproperty_normal
        double RenewableBalanceTotal { get; set; }
   

// [interface_base_collections]

 
		

    	
    	
      
      

    }
}
