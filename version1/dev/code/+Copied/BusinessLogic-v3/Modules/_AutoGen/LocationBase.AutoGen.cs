using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.LocationModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "Location")]
    public abstract class LocationBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  ILocationBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static LocationBaseFactory Factory
        {
            get
            {
                return LocationBaseFactory.Instance; 
            }
        }    
        /*
        public static LocationBase CreateNewItem()
        {
            return (LocationBase)Factory.CreateNewItem();
        }

		public static LocationBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<LocationBase, LocationBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static LocationBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static LocationBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<LocationBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<LocationBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<LocationBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _referenceid;
        
        public virtual string ReferenceID 
        {
        	get
        	{
        		return _referenceid;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_referenceid,value);
        		_referenceid = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private BusinessLogic_v3.Enums.LOCATION_TYPE _locationtype;
        
        public virtual BusinessLogic_v3.Enums.LOCATION_TYPE LocationType 
        {
        	get
        	{
        		return _locationtype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_locationtype,value);
        		_locationtype = value;
        		
        	}
        }
        
/*
		public virtual long? ParentLocationID
		{
		 	get 
		 	{
		 		return (this.ParentLocation != null ? (long?)this.ParentLocation.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.LocationModule.LocationBase _parentlocation;
        public virtual BusinessLogic_v3.Modules.LocationModule.LocationBase ParentLocation 
        {
        	get
        	{
        		return _parentlocation;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_parentlocation,value);
        		_parentlocation = value;
        	}
        }
            
        BusinessLogic_v3.Modules.LocationModule.ILocationBase BusinessLogic_v3.Modules._AutoGen.ILocationBaseAutoGen.ParentLocation 
        {
            get
            {
            	return this.ParentLocation;
            }
            set
            {
            	this.ParentLocation = (BusinessLogic_v3.Modules.LocationModule.LocationBase) value;
            }
        }
            
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ChildLocationsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.LocationModule.LocationBase, 
        	BusinessLogic_v3.Modules.LocationModule.LocationBase,
        	BusinessLogic_v3.Modules.LocationModule.ILocationBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.LocationModule.LocationBase, BusinessLogic_v3.Modules.LocationModule.LocationBase>
        {
            private BusinessLogic_v3.Modules.LocationModule.LocationBase _item = null;
            public __ChildLocationsCollectionInfo(BusinessLogic_v3.Modules.LocationModule.LocationBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.LocationModule.LocationBase> Collection
            {
                get { return _item.__collection__ChildLocations; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.LocationModule.LocationBase, BusinessLogic_v3.Modules.LocationModule.LocationBase>.SetLinkOnItem(BusinessLogic_v3.Modules.LocationModule.LocationBase item, BusinessLogic_v3.Modules.LocationModule.LocationBase value)
            {
                item.ParentLocation = value;
            }
        }
        
        private __ChildLocationsCollectionInfo _ChildLocations = null;
        public __ChildLocationsCollectionInfo ChildLocations
        {
            get
            {
                if (_ChildLocations == null)
                    _ChildLocations = new __ChildLocationsCollectionInfo((BusinessLogic_v3.Modules.LocationModule.LocationBase)this);
                return _ChildLocations;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.LocationModule.ILocationBase> ILocationBaseAutoGen.ChildLocations
        {
            get {  return this.ChildLocations; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.LocationModule.LocationBase> __collection__ChildLocations
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'LocationBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
