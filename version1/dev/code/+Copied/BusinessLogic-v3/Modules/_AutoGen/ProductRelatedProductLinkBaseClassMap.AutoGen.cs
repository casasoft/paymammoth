using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Modules.ProductRelatedProductLinkModule;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;

namespace BusinessLogic_v3.Modules._AutoGen
{
    
    public abstract class ProductRelatedProductLinkBaseMap_AutoGen<TData> : BusinessLogic_v3.Classes.NHibernateClasses.Mappings.BaseClassMap<TData>
    	where TData : ProductRelatedProductLinkBase
    {
        public ProductRelatedProductLinkBaseMap_AutoGen()
        {
            
        }
        
        


// [baseclassmap_properties]

//BaseClassMap_PropertyLinkedObject

        protected virtual void ProductMappingInfo(ICollectionMapInfo mapInfo)
        {
        
            mapInfo.CollectionType = BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_TYPE.Set;
            
            mapInfo.CascadeType.Clear();
            
            mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
            mapInfo.Inverse = false;
            mapInfo.ColumnName = "ProductId";   
            
            mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
           
        }
        
        
//BaseClassMap_PropertyLinkedObject

        protected virtual void RelatedProductMappingInfo(ICollectionMapInfo mapInfo)
        {
        
            mapInfo.CollectionType = BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_TYPE.Set;
            
            mapInfo.CascadeType.Clear();
            
            mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
            mapInfo.Inverse = false;
            mapInfo.ColumnName = "RelatedProductId";   
            
            mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
           
        }
        
        



// [baseclassmap_collections]

            
        
        protected override void initMappings()
        {
           
            
//[BaseClassMap_InitProperties]

//[BaseClassMap_InitCollections]                
            
            base.initMappings();
        }
        
        
        
        
        
    }
   
}
