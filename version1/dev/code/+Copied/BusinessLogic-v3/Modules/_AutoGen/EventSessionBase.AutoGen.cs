using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.EventSessionModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "EventSession")]
    public abstract class EventSessionBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IEventSessionBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static EventSessionBaseFactory Factory
        {
            get
            {
                return EventSessionBaseFactory.Instance; 
            }
        }    
        /*
        public static EventSessionBase CreateNewItem()
        {
            return (EventSessionBase)Factory.CreateNewItem();
        }

		public static EventSessionBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<EventSessionBase, EventSessionBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static EventSessionBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static EventSessionBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<EventSessionBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<EventSessionBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<EventSessionBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? EventID
		{
		 	get 
		 	{
		 		return (this.Event != null ? (long?)this.Event.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.EventModule.EventBase _event;
        public virtual BusinessLogic_v3.Modules.EventModule.EventBase Event 
        {
        	get
        	{
        		return _event;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_event,value);
        		_event = value;
        	}
        }
            
        BusinessLogic_v3.Modules.EventModule.IEventBase BusinessLogic_v3.Modules._AutoGen.IEventSessionBaseAutoGen.Event 
        {
            get
            {
            	return this.Event;
            }
            set
            {
            	this.Event = (BusinessLogic_v3.Modules.EventModule.EventBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _description;
        
        public virtual string Description 
        {
        	get
        	{
        		return _description;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_description,value);
        		_description = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _location;
        
        public virtual string Location 
        {
        	get
        	{
        		return _location;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_location,value);
        		_location = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _startdate;
        
        public virtual DateTime StartDate 
        {
        	get
        	{
        		return _startdate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_startdate,value);
        		_startdate = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _enddate;
        
        public virtual DateTime EndDate 
        {
        	get
        	{
        		return _enddate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_enddate,value);
        		_enddate = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _cost;
        
        public virtual double Cost 
        {
        	get
        	{
        		return _cost;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_cost,value);
        		_cost = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _slotsavailable;
        
        public virtual int SlotsAvailable 
        {
        	get
        	{
        		return _slotsavailable;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_slotsavailable,value);
        		_slotsavailable = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _slotstaken;
        
        public virtual int SlotsTaken 
        {
        	get
        	{
        		return _slotstaken;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_slotstaken,value);
        		_slotstaken = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __EventSessionPeriodsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase, 
        	BusinessLogic_v3.Modules.EventSessionPeriodModule.EventSessionPeriodBase,
        	BusinessLogic_v3.Modules.EventSessionPeriodModule.IEventSessionPeriodBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase, BusinessLogic_v3.Modules.EventSessionPeriodModule.EventSessionPeriodBase>
        {
            private BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase _item = null;
            public __EventSessionPeriodsCollectionInfo(BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.EventSessionPeriodModule.EventSessionPeriodBase> Collection
            {
                get { return _item.__collection__EventSessionPeriods; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase, BusinessLogic_v3.Modules.EventSessionPeriodModule.EventSessionPeriodBase>.SetLinkOnItem(BusinessLogic_v3.Modules.EventSessionPeriodModule.EventSessionPeriodBase item, BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase value)
            {
                item.EventSession = value;
            }
        }
        
        private __EventSessionPeriodsCollectionInfo _EventSessionPeriods = null;
        public __EventSessionPeriodsCollectionInfo EventSessionPeriods
        {
            get
            {
                if (_EventSessionPeriods == null)
                    _EventSessionPeriods = new __EventSessionPeriodsCollectionInfo((BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase)this);
                return _EventSessionPeriods;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.EventSessionPeriodModule.IEventSessionPeriodBase> IEventSessionBaseAutoGen.EventSessionPeriods
        {
            get {  return this.EventSessionPeriods; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.EventSessionPeriodModule.EventSessionPeriodBase> __collection__EventSessionPeriods
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'EventSessionPeriodBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
