using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.CultureInfoModule;                

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public abstract class CultureInfoBaseAutoGen 
    : BusinessLogic_v3.Classes.DB.BaseDbObject, ICultureInfoBaseAutoGen
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static CultureInfoBaseFactory Factory
        {
            get
            {
                return CultureInfoBaseFactory.Instance; 
            }
        }    
        /*
        public static CultureInfoBase CreateNewItem()
        {
            return (CultureInfoBase)Factory.CreateNewItem();
        }

		public static CultureInfoBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<CultureInfoBase, CultureInfoBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static CultureInfoBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static CultureInfoBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<CultureInfoBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<CultureInfoBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<CultureInfoBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
