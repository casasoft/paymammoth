using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.ArticleModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "Article")]
    public abstract class ArticleBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IArticleBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
        

			var cultureInfo = __getCurrentCultureInfo(); //since this is a new item, it is imperative that the culture is preloaded
            
            
            base.initPropertiesForNewItem();
        }
    	
  
    	public BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase GetCultureInfoByLanguage(CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 lang)
        {
            return (BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase) __getCultureInfoByLanguage(lang);
        }

     	public static ArticleBaseFactory Factory
        {
            get
            {
                return ArticleBaseFactory.Instance; 
            }
        }    
        /*
        public static ArticleBase CreateNewItem()
        {
            return (ArticleBase)Factory.CreateNewItem();
        }

		public static ArticleBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<ArticleBase, ArticleBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static ArticleBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static ArticleBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<ArticleBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<ArticleBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<ArticleBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _identifier;
        
        public virtual string Identifier 
        {
        	get
        	{
        		return _identifier;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_identifier,value);
        		_identifier = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.CMS_ACCESS_TYPE _allowupdate_accessrequired;
        
        public virtual CS.General_v3.Enums.CMS_ACCESS_TYPE AllowUpdate_AccessRequired 
        {
        	get
        	{
        		return _allowupdate_accessrequired;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_allowupdate_accessrequired,value);
        		_allowupdate_accessrequired = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.CMS_ACCESS_TYPE _allowdelete_accessrequired;
        
        public virtual CS.General_v3.Enums.CMS_ACCESS_TYPE AllowDelete_AccessRequired 
        {
        	get
        	{
        		return _allowdelete_accessrequired;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_allowdelete_accessrequired,value);
        		_allowdelete_accessrequired = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.CMS_ACCESS_TYPE _allowaddchildren_accessrequired;
        
        public virtual CS.General_v3.Enums.CMS_ACCESS_TYPE AllowAddChildren_AccessRequired 
        {
        	get
        	{
        		return _allowaddchildren_accessrequired;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_allowaddchildren_accessrequired,value);
        		_allowaddchildren_accessrequired = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.CMS_ACCESS_TYPE _allowaddsubchildren_accessrequired;
        
        public virtual CS.General_v3.Enums.CMS_ACCESS_TYPE AllowAddSubChildren_AccessRequired 
        {
        	get
        	{
        		return _allowaddsubchildren_accessrequired;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_allowaddsubchildren_accessrequired,value);
        		_allowaddsubchildren_accessrequired = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _customlink;
        
        public virtual string CustomLink 
        {
        	get
        	{
        		return _customlink;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_customlink,value);
        		_customlink = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string HtmlText
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.HtmlText;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__HtmlText_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.HtmlText,value);
                cultureInfo.HtmlText = value;
            }
        }
        
        private string _htmltext;
        public virtual string __HtmlText_cultureBase
        {
        	get
        	{
        		return _htmltext;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_htmltext,value);
        		
        		_htmltext = value;
        	}
        }
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string HtmlText_Search
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.HtmlText_Search;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__HtmlText_Search_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.HtmlText_Search,value);
                cultureInfo.HtmlText_Search = value;
            }
        }
        
        private string _htmltext_search;
        public virtual string __HtmlText_Search_cultureBase
        {
        	get
        	{
        		return _htmltext_search;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_htmltext_search,value);
        		
        		_htmltext_search = value;
        	}
        }
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string MetaKeywords
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.MetaKeywords;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__MetaKeywords_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.MetaKeywords,value);
                cultureInfo.MetaKeywords = value;
            }
        }
        
        private string _metakeywords;
        public virtual string __MetaKeywords_cultureBase
        {
        	get
        	{
        		return _metakeywords;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_metakeywords,value);
        		
        		_metakeywords = value;
        	}
        }
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string MetaKeywords_Search
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.MetaKeywords_Search;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__MetaKeywords_Search_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.MetaKeywords_Search,value);
                cultureInfo.MetaKeywords_Search = value;
            }
        }
        
        private string _metakeywords_search;
        public virtual string __MetaKeywords_Search_cultureBase
        {
        	get
        	{
        		return _metakeywords_search;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_metakeywords_search,value);
        		
        		_metakeywords_search = value;
        	}
        }
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string MetaDescription
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.MetaDescription;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__MetaDescription_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.MetaDescription,value);
                cultureInfo.MetaDescription = value;
            }
        }
        
        private string _metadescription;
        public virtual string __MetaDescription_cultureBase
        {
        	get
        	{
        		return _metadescription;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_metadescription,value);
        		
        		_metadescription = value;
        	}
        }
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string MetaDescription_Search
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.MetaDescription_Search;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__MetaDescription_Search_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.MetaDescription_Search,value);
                cultureInfo.MetaDescription_Search = value;
            }
        }
        
        private string _metadescription_search;
        public virtual string __MetaDescription_Search_cultureBase
        {
        	get
        	{
        		return _metadescription_search;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_metadescription_search,value);
        		
        		_metadescription_search = value;
        	}
        }
		//BaseClass/Fields/Property_Normal
        private string _color;
        
        public virtual string Color 
        {
        	get
        	{
        		return _color;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_color,value);
        		_color = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _name;
        
        public virtual string Name 
        {
        	get
        	{
        		return _name;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_name,value);
        		_name = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string PageTitle
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.PageTitle;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__PageTitle_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.PageTitle,value);
                cultureInfo.PageTitle = value;
            }
        }
        
        private string _pagetitle;
        public virtual string __PageTitle_cultureBase
        {
        	get
        	{
        		return _pagetitle;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_pagetitle,value);
        		
        		_pagetitle = value;
        	}
        }
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string PageTitle_Search
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.PageTitle_Search;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__PageTitle_Search_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.PageTitle_Search,value);
                cultureInfo.PageTitle_Search = value;
            }
        }
        
        private string _pagetitle_search;
        public virtual string __PageTitle_Search_cultureBase
        {
        	get
        	{
        		return _pagetitle_search;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_pagetitle_search,value);
        		
        		_pagetitle_search = value;
        	}
        }
		//BaseClass/Fields/Property_Normal
        private bool _excludefromrewriteurl;
        
        public virtual bool ExcludeFromRewriteUrl 
        {
        	get
        	{
        		return _excludefromrewriteurl;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_excludefromrewriteurl,value);
        		_excludefromrewriteurl = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _imagefilename;
        
        public virtual string ImageFilename 
        {
        	get
        	{
        		return _imagefilename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_imagefilename,value);
        		_imagefilename = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _showadverts;
        
        public virtual bool ShowAdverts 
        {
        	get
        	{
        		return _showadverts;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_showadverts,value);
        		_showadverts = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.CMS_ACCESS_TYPE _visibleincms_accessrequired;
        
        public virtual CS.General_v3.Enums.CMS_ACCESS_TYPE VisibleInCMS_AccessRequired 
        {
        	get
        	{
        		return _visibleincms_accessrequired;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_visibleincms_accessrequired,value);
        		_visibleincms_accessrequired = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool? _donotshowlasteditedon;
        
        public virtual bool? DoNotShowLastEditedOn 
        {
        	get
        	{
        		return _donotshowlasteditedon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_donotshowlasteditedon,value);
        		_donotshowlasteditedon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _viewcount;
        
        public virtual int ViewCount 
        {
        	get
        	{
        		return _viewcount;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_viewcount,value);
        		_viewcount = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _isfeatured;
        
        public virtual bool IsFeatured 
        {
        	get
        	{
        		return _isfeatured;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_isfeatured,value);
        		_isfeatured = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _metatitle;
        
        public virtual string MetaTitle 
        {
        	get
        	{
        		return _metatitle;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_metatitle,value);
        		_metatitle = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _nofollow;
        
        public virtual bool NoFollow 
        {
        	get
        	{
        		return _nofollow;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_nofollow,value);
        		_nofollow = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _noindex;
        
        public virtual bool NoIndex 
        {
        	get
        	{
        		return _noindex;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_noindex,value);
        		_noindex = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _usedinproject;
        
        public virtual bool UsedInProject 
        {
        	get
        	{
        		return _usedinproject;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_usedinproject,value);
        		_usedinproject = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private BusinessLogic_v3.Enums.ARTICLE_TYPE _articletype;
        
        public virtual BusinessLogic_v3.Enums.ARTICLE_TYPE ArticleType 
        {
        	get
        	{
        		return _articletype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_articletype,value);
        		_articletype = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _notvisibleinfrontend;
        
        public virtual bool NotVisibleInFrontend 
        {
        	get
        	{
        		return _notvisibleinfrontend;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_notvisibleinfrontend,value);
        		_notvisibleinfrontend = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _visibleinfrontendonlywhenloggedin;
        
        public virtual bool VisibleInFrontendOnlyWhenLoggedIn 
        {
        	get
        	{
        		return _visibleinfrontendonlywhenloggedin;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_visibleinfrontendonlywhenloggedin,value);
        		_visibleinfrontendonlywhenloggedin = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _notvisibleinfrontendwhenloggedin;
        
        public virtual bool NotVisibleInFrontendWhenLoggedIn 
        {
        	get
        	{
        		return _notvisibleinfrontendwhenloggedin;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_notvisibleinfrontendwhenloggedin,value);
        		_notvisibleinfrontendwhenloggedin = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _customcontenttags;
        
        public virtual string CustomContentTags 
        {
        	get
        	{
        		return _customcontenttags;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_customcontenttags,value);
        		_customcontenttags = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool? _donotredirectonlogin;
        
        public virtual bool? DoNotRedirectOnLogin 
        {
        	get
        	{
        		return _donotredirectonlogin;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_donotredirectonlogin,value);
        		_donotredirectonlogin = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool? _donotshowaddthis;
        
        public virtual bool? DoNotShowAddThis 
        {
        	get
        	{
        		return _donotshowaddthis;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_donotshowaddthis,value);
        		_donotshowaddthis = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool? _donotshowfooter;
        
        public virtual bool? DoNotShowFooter 
        {
        	get
        	{
        		return _donotshowfooter;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_donotshowfooter,value);
        		_donotshowfooter = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual int? DialogWidth
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.DialogWidth;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__DialogWidth_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.DialogWidth,value);
                cultureInfo.DialogWidth = value;
            }
        }
        
        private int? _dialogwidth;
        public virtual int? __DialogWidth_cultureBase
        {
        	get
        	{
        		return _dialogwidth;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_dialogwidth,value);
        		
        		_dialogwidth = value;
        	}
        }
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual int? DialogHeight
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.DialogHeight;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__DialogHeight_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.DialogHeight,value);
                cultureInfo.DialogHeight = value;
            }
        }
        
        private int? _dialogheight;
        public virtual int? __DialogHeight_cultureBase
        {
        	get
        	{
        		return _dialogheight;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_dialogheight,value);
        		
        		_dialogheight = value;
        	}
        }
		//BaseClass/Fields/Property_Normal
        private bool _dialogpage;
        
        public virtual bool DialogPage 
        {
        	get
        	{
        		return _dialogpage;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_dialogpage,value);
        		_dialogpage = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _ratingcount;
        
        public virtual double RatingCount 
        {
        	get
        	{
        		return _ratingcount;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_ratingcount,value);
        		_ratingcount = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _avgrating;
        
        public virtual double AvgRating 
        {
        	get
        	{
        		return _avgrating;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_avgrating,value);
        		_avgrating = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool? _iscommentable;
        
        public virtual bool? IsCommentable 
        {
        	get
        	{
        		return _iscommentable;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_iscommentable,value);
        		_iscommentable = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _summary;
        
        public virtual string Summary 
        {
        	get
        	{
        		return _summary;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_summary,value);
        		_summary = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _omitchildreninnavigation;
        
        public virtual bool OmitChildrenInNavigation 
        {
        	get
        	{
        		return _omitchildreninnavigation;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_omitchildreninnavigation,value);
        		_omitchildreninnavigation = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _considerasrootnode;
        
        public virtual bool ConsiderAsRootNode 
        {
        	get
        	{
        		return _considerasrootnode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_considerasrootnode,value);
        		_considerasrootnode = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool? _donotshownavigationbreadcrumbs;
        
        public virtual bool? DoNotShowNavigationBreadcrumbs 
        {
        	get
        	{
        		return _donotshownavigationbreadcrumbs;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_donotshownavigationbreadcrumbs,value);
        		_donotshownavigationbreadcrumbs = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _createdondate;
        
        public virtual DateTime CreatedOnDate 
        {
        	get
        	{
        		return _createdondate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_createdondate,value);
        		_createdondate = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _physicalfilepath;
        
        public virtual string PhysicalFilePath 
        {
        	get
        	{
        		return _physicalfilepath;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_physicalfilepath,value);
        		_physicalfilepath = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _processcontentusingnvelocity;
        
        public virtual bool ProcessContentUsingNVelocity 
        {
        	get
        	{
        		return _processcontentusingnvelocity;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_processcontentusingnvelocity,value);
        		_processcontentusingnvelocity = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _dialogcssclass;
        
        public virtual string DialogCssClass 
        {
        	get
        	{
        		return _dialogcssclass;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_dialogcssclass,value);
        		_dialogcssclass = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool? _dialogmodal;
        
        public virtual bool? DialogModal 
        {
        	get
        	{
        		return _dialogmodal;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_dialogmodal,value);
        		_dialogmodal = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool? _dialogcloseable;
        
        public virtual bool? DialogCloseable 
        {
        	get
        	{
        		return _dialogcloseable;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_dialogcloseable,value);
        		_dialogcloseable = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool? _donotredirectonlogout;
        
        public virtual bool? DoNotRedirectOnLogout 
        {
        	get
        	{
        		return _donotredirectonlogout;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_donotredirectonlogout,value);
        		_donotredirectonlogout = value;
        		
        	}
        }
        
/*
		public virtual long? LinkedRouteID
		{
		 	get 
		 	{
		 		return (this.LinkedRoute != null ? (long?)this.LinkedRoute.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.RoutingInfoModule.RoutingInfoBase _linkedroute;
        public virtual BusinessLogic_v3.Modules.RoutingInfoModule.RoutingInfoBase LinkedRoute 
        {
        	get
        	{
        		return _linkedroute;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_linkedroute,value);
        		_linkedroute = value;
        	}
        }
            
        BusinessLogic_v3.Modules.RoutingInfoModule.IRoutingInfoBase BusinessLogic_v3.Modules._AutoGen.IArticleBaseAutoGen.LinkedRoute 
        {
            get
            {
            	return this.LinkedRoute;
            }
            set
            {
            	this.LinkedRoute = (BusinessLogic_v3.Modules.RoutingInfoModule.RoutingInfoBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string SubTitle
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.SubTitle;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__SubTitle_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.SubTitle,value);
                cultureInfo.SubTitle = value;
            }
        }
        
        private string _subtitle;
        public virtual string __SubTitle_cultureBase
        {
        	get
        	{
        		return _subtitle;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_subtitle,value);
        		
        		_subtitle = value;
        	}
        }
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.HREF_TARGET? _hreftarget;
        
        public virtual CS.General_v3.Enums.HREF_TARGET? HrefTarget 
        {
        	get
        	{
        		return _hreftarget;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_hreftarget,value);
        		_hreftarget = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool? _isinheritedcommentable_computed;
        
        public virtual bool? IsInheritedCommentable_Computed 
        {
        	get
        	{
        		return _isinheritedcommentable_computed;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_isinheritedcommentable_computed,value);
        		_isinheritedcommentable_computed = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _authorname;
        
        public virtual string AuthorName 
        {
        	get
        	{
        		return _authorname;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_authorname,value);
        		_authorname = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _donotshowinsearchresults;
        
        public virtual bool DoNotShowInSearchResults 
        {
        	get
        	{
        		return _donotshowinsearchresults;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_donotshowinsearchresults,value);
        		_donotshowinsearchresults = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _donotshowinsearchresults_computed;
        
        public virtual bool DoNotShowInSearchResults_Computed 
        {
        	get
        	{
        		return _donotshowinsearchresults_computed;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_donotshowinsearchresults_computed,value);
        		_donotshowinsearchresults_computed = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int __computed_commentandrepliescount;
        
        public virtual int _Computed_CommentAndRepliesCount 
        {
        	get
        	{
        		return __computed_commentandrepliescount;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(__computed_commentandrepliescount,value);
        		__computed_commentandrepliescount = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string _ComputedURL
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo._ComputedURL;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	___ComputedURL_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo._ComputedURL,value);
                cultureInfo._ComputedURL = value;
            }
        }
        
        private string __computedurl;
        public virtual string ___ComputedURL_cultureBase
        {
        	get
        	{
        		return __computedurl;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(__computedurl,value);
        		
        		__computedurl = value;
        	}
        }
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string _Computed_ParentArticleNames
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo._Computed_ParentArticleNames;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	___Computed_ParentArticleNames_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo._Computed_ParentArticleNames,value);
                cultureInfo._Computed_ParentArticleNames = value;
            }
        }
        
        private string __computed_parentarticlenames;
        public virtual string ___Computed_ParentArticleNames_cultureBase
        {
        	get
        	{
        		return __computed_parentarticlenames;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(__computed_parentarticlenames,value);
        		
        		__computed_parentarticlenames = value;
        	}
        }
		//BaseClass/Fields/Property_Normal
        private bool __computed_isleafnode;
        
        public virtual bool _Computed_IsLeafNode 
        {
        	get
        	{
        		return __computed_isleafnode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(__computed_isleafnode,value);
        		__computed_isleafnode = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __AffiliatesCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ArticleModule.ArticleBase, 
        	BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase,
        	BusinessLogic_v3.Modules.AffiliateModule.IAffiliateBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ArticleModule.ArticleBase, BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase>
        {
            private BusinessLogic_v3.Modules.ArticleModule.ArticleBase _item = null;
            public __AffiliatesCollectionInfo(BusinessLogic_v3.Modules.ArticleModule.ArticleBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase> Collection
            {
                get { return _item.__collection__Affiliates; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ArticleModule.ArticleBase, BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase>.SetLinkOnItem(BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase item, BusinessLogic_v3.Modules.ArticleModule.ArticleBase value)
            {
                item.LinkedContentPageNode = value;
            }
        }
        
        private __AffiliatesCollectionInfo _Affiliates = null;
        public __AffiliatesCollectionInfo Affiliates
        {
            get
            {
                if (_Affiliates == null)
                    _Affiliates = new __AffiliatesCollectionInfo((BusinessLogic_v3.Modules.ArticleModule.ArticleBase)this);
                return _Affiliates;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.AffiliateModule.IAffiliateBase> IArticleBaseAutoGen.Affiliates
        {
            get {  return this.Affiliates; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase> __collection__Affiliates
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'AffiliateBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __Article_CultureInfosCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ArticleModule.ArticleBase, 
        	BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase,
        	BusinessLogic_v3.Modules.Article_CultureInfoModule.IArticle_CultureInfoBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ArticleModule.ArticleBase, BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase>
        {
            private BusinessLogic_v3.Modules.ArticleModule.ArticleBase _item = null;
            public __Article_CultureInfosCollectionInfo(BusinessLogic_v3.Modules.ArticleModule.ArticleBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase> Collection
            {
                get { return _item.__collection__Article_CultureInfos; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ArticleModule.ArticleBase, BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase>.SetLinkOnItem(BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase item, BusinessLogic_v3.Modules.ArticleModule.ArticleBase value)
            {
                item.Article = value;
            }
        }
        
        private __Article_CultureInfosCollectionInfo _Article_CultureInfos = null;
        public __Article_CultureInfosCollectionInfo Article_CultureInfos
        {
            get
            {
                if (_Article_CultureInfos == null)
                    _Article_CultureInfos = new __Article_CultureInfosCollectionInfo((BusinessLogic_v3.Modules.ArticleModule.ArticleBase)this);
                return _Article_CultureInfos;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.Article_CultureInfoModule.IArticle_CultureInfoBase> IArticleBaseAutoGen.Article_CultureInfos
        {
            get {  return this.Article_CultureInfos; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase> __collection__Article_CultureInfos
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'Article_CultureInfoBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ChildArticleLinksCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ArticleModule.ArticleBase, 
        	BusinessLogic_v3.Modules.Article_ParentChildLinkModule.Article_ParentChildLinkBase,
        	BusinessLogic_v3.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLinkBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ArticleModule.ArticleBase, BusinessLogic_v3.Modules.Article_ParentChildLinkModule.Article_ParentChildLinkBase>
        {
            private BusinessLogic_v3.Modules.ArticleModule.ArticleBase _item = null;
            public __ChildArticleLinksCollectionInfo(BusinessLogic_v3.Modules.ArticleModule.ArticleBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.Article_ParentChildLinkModule.Article_ParentChildLinkBase> Collection
            {
                get { return _item.__collection__ChildArticleLinks; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ArticleModule.ArticleBase, BusinessLogic_v3.Modules.Article_ParentChildLinkModule.Article_ParentChildLinkBase>.SetLinkOnItem(BusinessLogic_v3.Modules.Article_ParentChildLinkModule.Article_ParentChildLinkBase item, BusinessLogic_v3.Modules.ArticleModule.ArticleBase value)
            {
                item.Parent = value;
            }
        }
        
        private __ChildArticleLinksCollectionInfo _ChildArticleLinks = null;
        public __ChildArticleLinksCollectionInfo ChildArticleLinks
        {
            get
            {
                if (_ChildArticleLinks == null)
                    _ChildArticleLinks = new __ChildArticleLinksCollectionInfo((BusinessLogic_v3.Modules.ArticleModule.ArticleBase)this);
                return _ChildArticleLinks;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLinkBase> IArticleBaseAutoGen.ChildArticleLinks
        {
            get {  return this.ChildArticleLinks; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.Article_ParentChildLinkModule.Article_ParentChildLinkBase> __collection__ChildArticleLinks
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'Article_ParentChildLinkBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ParentArticleLinksCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ArticleModule.ArticleBase, 
        	BusinessLogic_v3.Modules.Article_ParentChildLinkModule.Article_ParentChildLinkBase,
        	BusinessLogic_v3.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLinkBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ArticleModule.ArticleBase, BusinessLogic_v3.Modules.Article_ParentChildLinkModule.Article_ParentChildLinkBase>
        {
            private BusinessLogic_v3.Modules.ArticleModule.ArticleBase _item = null;
            public __ParentArticleLinksCollectionInfo(BusinessLogic_v3.Modules.ArticleModule.ArticleBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.Article_ParentChildLinkModule.Article_ParentChildLinkBase> Collection
            {
                get { return _item.__collection__ParentArticleLinks; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ArticleModule.ArticleBase, BusinessLogic_v3.Modules.Article_ParentChildLinkModule.Article_ParentChildLinkBase>.SetLinkOnItem(BusinessLogic_v3.Modules.Article_ParentChildLinkModule.Article_ParentChildLinkBase item, BusinessLogic_v3.Modules.ArticleModule.ArticleBase value)
            {
                item.Child = value;
            }
        }
        
        private __ParentArticleLinksCollectionInfo _ParentArticleLinks = null;
        public __ParentArticleLinksCollectionInfo ParentArticleLinks
        {
            get
            {
                if (_ParentArticleLinks == null)
                    _ParentArticleLinks = new __ParentArticleLinksCollectionInfo((BusinessLogic_v3.Modules.ArticleModule.ArticleBase)this);
                return _ParentArticleLinks;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLinkBase> IArticleBaseAutoGen.ParentArticleLinks
        {
            get {  return this.ParentArticleLinks; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.Article_ParentChildLinkModule.Article_ParentChildLinkBase> __collection__ParentArticleLinks
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'Article_ParentChildLinkBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __RelatedPagesCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ArticleModule.ArticleBase, 
        	BusinessLogic_v3.Modules.Article_RelatedLinkModule.Article_RelatedLinkBase,
        	BusinessLogic_v3.Modules.Article_RelatedLinkModule.IArticle_RelatedLinkBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ArticleModule.ArticleBase, BusinessLogic_v3.Modules.Article_RelatedLinkModule.Article_RelatedLinkBase>
        {
            private BusinessLogic_v3.Modules.ArticleModule.ArticleBase _item = null;
            public __RelatedPagesCollectionInfo(BusinessLogic_v3.Modules.ArticleModule.ArticleBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.Article_RelatedLinkModule.Article_RelatedLinkBase> Collection
            {
                get { return _item.__collection__RelatedPages; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ArticleModule.ArticleBase, BusinessLogic_v3.Modules.Article_RelatedLinkModule.Article_RelatedLinkBase>.SetLinkOnItem(BusinessLogic_v3.Modules.Article_RelatedLinkModule.Article_RelatedLinkBase item, BusinessLogic_v3.Modules.ArticleModule.ArticleBase value)
            {
                item.ParentPage = value;
            }
        }
        
        private __RelatedPagesCollectionInfo _RelatedPages = null;
        public __RelatedPagesCollectionInfo RelatedPages
        {
            get
            {
                if (_RelatedPages == null)
                    _RelatedPages = new __RelatedPagesCollectionInfo((BusinessLogic_v3.Modules.ArticleModule.ArticleBase)this);
                return _RelatedPages;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.Article_RelatedLinkModule.IArticle_RelatedLinkBase> IArticleBaseAutoGen.RelatedPages
        {
            get {  return this.RelatedPages; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.Article_RelatedLinkModule.Article_RelatedLinkBase> __collection__RelatedPages
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'Article_RelatedLinkBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ParentsOfWhichThisIsRelatedCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ArticleModule.ArticleBase, 
        	BusinessLogic_v3.Modules.Article_RelatedLinkModule.Article_RelatedLinkBase,
        	BusinessLogic_v3.Modules.Article_RelatedLinkModule.IArticle_RelatedLinkBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ArticleModule.ArticleBase, BusinessLogic_v3.Modules.Article_RelatedLinkModule.Article_RelatedLinkBase>
        {
            private BusinessLogic_v3.Modules.ArticleModule.ArticleBase _item = null;
            public __ParentsOfWhichThisIsRelatedCollectionInfo(BusinessLogic_v3.Modules.ArticleModule.ArticleBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.Article_RelatedLinkModule.Article_RelatedLinkBase> Collection
            {
                get { return _item.__collection__ParentsOfWhichThisIsRelated; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ArticleModule.ArticleBase, BusinessLogic_v3.Modules.Article_RelatedLinkModule.Article_RelatedLinkBase>.SetLinkOnItem(BusinessLogic_v3.Modules.Article_RelatedLinkModule.Article_RelatedLinkBase item, BusinessLogic_v3.Modules.ArticleModule.ArticleBase value)
            {
                item.RelatedPage = value;
            }
        }
        
        private __ParentsOfWhichThisIsRelatedCollectionInfo _ParentsOfWhichThisIsRelated = null;
        public __ParentsOfWhichThisIsRelatedCollectionInfo ParentsOfWhichThisIsRelated
        {
            get
            {
                if (_ParentsOfWhichThisIsRelated == null)
                    _ParentsOfWhichThisIsRelated = new __ParentsOfWhichThisIsRelatedCollectionInfo((BusinessLogic_v3.Modules.ArticleModule.ArticleBase)this);
                return _ParentsOfWhichThisIsRelated;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.Article_RelatedLinkModule.IArticle_RelatedLinkBase> IArticleBaseAutoGen.ParentsOfWhichThisIsRelated
        {
            get {  return this.ParentsOfWhichThisIsRelated; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.Article_RelatedLinkModule.Article_RelatedLinkBase> __collection__ParentsOfWhichThisIsRelated
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'Article_RelatedLinkBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ArticleCommentsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ArticleModule.ArticleBase, 
        	BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase,
        	BusinessLogic_v3.Modules.ArticleCommentModule.IArticleCommentBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ArticleModule.ArticleBase, BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase>
        {
            private BusinessLogic_v3.Modules.ArticleModule.ArticleBase _item = null;
            public __ArticleCommentsCollectionInfo(BusinessLogic_v3.Modules.ArticleModule.ArticleBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase> Collection
            {
                get { return _item.__collection__ArticleComments; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ArticleModule.ArticleBase, BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase item, BusinessLogic_v3.Modules.ArticleModule.ArticleBase value)
            {
                item.Article = value;
            }
        }
        
        private __ArticleCommentsCollectionInfo _ArticleComments = null;
        public __ArticleCommentsCollectionInfo ArticleComments
        {
            get
            {
                if (_ArticleComments == null)
                    _ArticleComments = new __ArticleCommentsCollectionInfo((BusinessLogic_v3.Modules.ArticleModule.ArticleBase)this);
                return _ArticleComments;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ArticleCommentModule.IArticleCommentBase> IArticleBaseAutoGen.ArticleComments
        {
            get {  return this.ArticleComments; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase> __collection__ArticleComments
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ArticleCommentBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ArticleMediaItemsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ArticleModule.ArticleBase, 
        	BusinessLogic_v3.Modules.ArticleMediaItemModule.ArticleMediaItemBase,
        	BusinessLogic_v3.Modules.ArticleMediaItemModule.IArticleMediaItemBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ArticleModule.ArticleBase, BusinessLogic_v3.Modules.ArticleMediaItemModule.ArticleMediaItemBase>
        {
            private BusinessLogic_v3.Modules.ArticleModule.ArticleBase _item = null;
            public __ArticleMediaItemsCollectionInfo(BusinessLogic_v3.Modules.ArticleModule.ArticleBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ArticleMediaItemModule.ArticleMediaItemBase> Collection
            {
                get { return _item.__collection__ArticleMediaItems; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ArticleModule.ArticleBase, BusinessLogic_v3.Modules.ArticleMediaItemModule.ArticleMediaItemBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ArticleMediaItemModule.ArticleMediaItemBase item, BusinessLogic_v3.Modules.ArticleModule.ArticleBase value)
            {
                item.ContentPage = value;
            }
        }
        
        private __ArticleMediaItemsCollectionInfo _ArticleMediaItems = null;
        public __ArticleMediaItemsCollectionInfo ArticleMediaItems
        {
            get
            {
                if (_ArticleMediaItems == null)
                    _ArticleMediaItems = new __ArticleMediaItemsCollectionInfo((BusinessLogic_v3.Modules.ArticleModule.ArticleBase)this);
                return _ArticleMediaItems;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ArticleMediaItemModule.IArticleMediaItemBase> IArticleBaseAutoGen.ArticleMediaItems
        {
            get {  return this.ArticleMediaItems; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ArticleMediaItemModule.ArticleMediaItemBase> __collection__ArticleMediaItems
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ArticleMediaItemBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ArticleRatingsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ArticleModule.ArticleBase, 
        	BusinessLogic_v3.Modules.ArticleRatingModule.ArticleRatingBase,
        	BusinessLogic_v3.Modules.ArticleRatingModule.IArticleRatingBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ArticleModule.ArticleBase, BusinessLogic_v3.Modules.ArticleRatingModule.ArticleRatingBase>
        {
            private BusinessLogic_v3.Modules.ArticleModule.ArticleBase _item = null;
            public __ArticleRatingsCollectionInfo(BusinessLogic_v3.Modules.ArticleModule.ArticleBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ArticleRatingModule.ArticleRatingBase> Collection
            {
                get { return _item.__collection__ArticleRatings; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ArticleModule.ArticleBase, BusinessLogic_v3.Modules.ArticleRatingModule.ArticleRatingBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ArticleRatingModule.ArticleRatingBase item, BusinessLogic_v3.Modules.ArticleModule.ArticleBase value)
            {
                item.Article = value;
            }
        }
        
        private __ArticleRatingsCollectionInfo _ArticleRatings = null;
        public __ArticleRatingsCollectionInfo ArticleRatings
        {
            get
            {
                if (_ArticleRatings == null)
                    _ArticleRatings = new __ArticleRatingsCollectionInfo((BusinessLogic_v3.Modules.ArticleModule.ArticleBase)this);
                return _ArticleRatings;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ArticleRatingModule.IArticleRatingBase> IArticleBaseAutoGen.ArticleRatings
        {
            get {  return this.ArticleRatings; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ArticleRatingModule.ArticleRatingBase> __collection__ArticleRatings
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ArticleRatingBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionManyToManyRightSide  
        
		public class __ReadByCollectionInfo : DbCollectionManyToManyManager<
			BusinessLogic_v3.Modules.ArticleModule.ArticleBase,
			BusinessLogic_v3.Modules.MemberModule.MemberBase,
			BusinessLogic_v3.Modules.MemberModule.IMemberBase>,
            IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.ArticleModule.ArticleBase, BusinessLogic_v3.Modules.MemberModule.MemberBase>
        {
            private BusinessLogic_v3.Modules.ArticleModule.ArticleBase _item = null;
            public __ReadByCollectionInfo(BusinessLogic_v3.Modules.ArticleModule.ArticleBase item)
                : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }


            public IEnumerable<BusinessLogic_v3.Modules.MemberModule.MemberBase> Collection
            {
                get { return _item.__collection__ReadBy; }
            }

            #region IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.ArticleModule.ArticleBase,BusinessLogic_v3.Modules.MemberModule.MemberBase> Members

            public ICollectionManager<BusinessLogic_v3.Modules.ArticleModule.ArticleBase> GetLinkedCollectionForItem(BusinessLogic_v3.Modules.MemberModule.MemberBase item)
            {
                return item.ArticlesRead;
                
            }


            #endregion
        }

        private __ReadByCollectionInfo _ReadBy = null;
        public __ReadByCollectionInfo ReadBy
        {
            get
            {
                if (_ReadBy == null)
                    _ReadBy = new __ReadByCollectionInfo((BusinessLogic_v3.Modules.ArticleModule.ArticleBase)this);
                return _ReadBy;
            }
        }
        
           
        ICollectionManager<BusinessLogic_v3.Modules.MemberModule.IMemberBase> IArticleBaseAutoGen.ReadBy
        {
            get {  return this.ReadBy; }
        }
           

		protected virtual IEnumerable<BusinessLogic_v3.Modules.MemberModule.MemberBase> __collection__ReadBy
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'MemberBase' is not marked as UsedInProject"); }
        }





        

    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
