using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.SpecialOfferModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface ISpecialOfferBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//IProperty_Multilingual
	   string Title { get; set ;}
		//Iproperty_normal
        string Name { get; set; }
		//Iproperty_normal
        DateTime? DateFrom { get; set; }
		//Iproperty_normal
        DateTime? DateTo { get; set; }
		//Iproperty_normal
        int RequiredTotalItemsToBuy { get; set; }
		//Iproperty_normal
        double RequiredTotalToSpend { get; set; }
		//Iproperty_normal
        double DiscountPercentage { get; set; }
		//Iproperty_normal
        bool RequiresPromoCode { get; set; }
		//IProperty_Multilingual
	   string Description { get; set ;}
		//Iproperty_normal
        int QuantityLeft { get; set; }
		//Iproperty_normal
        bool Activated { get; set; }
		//Iproperty_normal
        bool RequirementsCanMatchAnyItem { get; set; }
		//Iproperty_normal
        bool IsExclusiveOffer { get; set; }
		//Iproperty_normal
        bool IsNotLimitedByQuantity { get; set; }
   

// [interface_base_collections]

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule.ISpecialOffer_CultureInfoBase> SpecialOffer_CultureInfos { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.ISpecialOfferVoucherCodeBase> SpecialOfferVoucherCodes { get; }

        //IBaseClass_CollectionManyToManyLeftSide  
        ICollectionManager<BusinessLogic_v3.Modules.CategoryModule.ICategoryBase> ApplicableCategories { get; }

 
		

    	
    	
      
      

    }
}
