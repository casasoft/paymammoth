using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.CultureDetailsModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface ICultureDetailsBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        string Title { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.CurrencyModule.ICurrencyBase DefaultCurrency {get; set; }

		//Iproperty_normal
        string ScriptSuffix { get; set; }
		//Iproperty_normal
        bool IsDefaultCulture { get; set; }
		//Iproperty_normal
        string BaseUrlRegex { get; set; }
		//Iproperty_normal
        string BaseRedirectionUrl { get; set; }
		//Iproperty_normal
        string GoogleAnalyticsRootDomain { get; set; }
		//Iproperty_normal
        string GoogleAnalyticsID { get; set; }
		//Iproperty_normal
        string BaseRedirectionUrlLocalhost { get; set; }
		//Iproperty_normal
        string BaseUrlRegexLocalhost { get; set; }
   

// [interface_base_collections]

        //IBaseClass_CollectionManyToManyRightSide  
        
        ICollectionManager<BusinessLogic_v3.Modules.BannerModule.IBannerBase> Banners { get; }
        
        
 
		

    	
    	
      
      

    }
}
