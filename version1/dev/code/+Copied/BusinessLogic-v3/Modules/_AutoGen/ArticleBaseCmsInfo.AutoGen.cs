using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ArticleModule;
using BusinessLogic_v3.Cms.ArticleModule;
using BusinessLogic_v3.Frontend.ArticleModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ArticleBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ArticleModule.ArticleBase>
    {
		public ArticleBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ArticleModule.ArticleBase dbItem)
            : base(BusinessLogic_v3.Cms.ArticleModule.ArticleBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ArticleBaseFrontend FrontendItem
        {
            get { return (ArticleBaseFrontend)base.FrontendItem; }

        }
        public new ArticleBase DbItem
        {
            get
            {
                return (ArticleBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Identifier { get; protected set; }

        public CmsPropertyInfo AllowUpdate_AccessRequired { get; protected set; }

        public CmsPropertyInfo AllowDelete_AccessRequired { get; protected set; }

        public CmsPropertyInfo AllowAddChildren_AccessRequired { get; protected set; }

        public CmsPropertyInfo AllowAddSubChildren_AccessRequired { get; protected set; }

        public CmsPropertyInfo CustomLink { get; protected set; }

        public CmsPropertyInfo HtmlText { get; protected set; }

        public CmsPropertyInfo HtmlText_Search { get; protected set; }

        public CmsPropertyInfo MetaKeywords { get; protected set; }

        public CmsPropertyInfo MetaKeywords_Search { get; protected set; }

        public CmsPropertyInfo MetaDescription { get; protected set; }

        public CmsPropertyInfo MetaDescription_Search { get; protected set; }

        public CmsPropertyInfo Color { get; protected set; }

        public CmsPropertyInfo Name { get; protected set; }

        public CmsPropertyInfo PageTitle { get; protected set; }

        public CmsPropertyInfo PageTitle_Search { get; protected set; }

        public CmsPropertyInfo ExcludeFromRewriteUrl { get; protected set; }

        public CmsPropertyInfo ImageFilename { get; protected set; }

        public CmsPropertyInfo ShowAdverts { get; protected set; }

        public CmsPropertyInfo VisibleInCMS_AccessRequired { get; protected set; }

        public CmsPropertyInfo DoNotShowLastEditedOn { get; protected set; }

        public CmsPropertyInfo ViewCount { get; protected set; }

        public CmsPropertyInfo IsFeatured { get; protected set; }

        public CmsPropertyInfo CustomRewriteUrl { get; protected set; }

        public CmsPropertyInfo MetaTitle { get; protected set; }

        public CmsPropertyInfo NoFollow { get; protected set; }

        public CmsPropertyInfo NoIndex { get; protected set; }

        public CmsPropertyInfo UsedInProject { get; protected set; }

        public CmsPropertyInfo ArticleType { get; protected set; }

        public CmsPropertyInfo NotVisibleInFrontend { get; protected set; }

        public CmsPropertyInfo VisibleInFrontendOnlyWhenLoggedIn { get; protected set; }

        public CmsPropertyInfo NotVisibleInFrontendWhenLoggedIn { get; protected set; }

        public CmsPropertyInfo CustomContentTags { get; protected set; }

        public CmsPropertyInfo DoNotRedirectOnLogin { get; protected set; }

        public CmsPropertyInfo DoNotShowAddThis { get; protected set; }

        public CmsPropertyInfo DoNotShowFooter { get; protected set; }

        public CmsPropertyInfo DialogWidth { get; protected set; }

        public CmsPropertyInfo DialogHeight { get; protected set; }

        public CmsPropertyInfo DialogPage { get; protected set; }

        public CmsPropertyInfo RatingCount { get; protected set; }

        public CmsPropertyInfo AvgRating { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo Affiliates { get; protected set; }
        

        public CmsCollectionInfo Article_CultureInfos { get; protected set; }
        

        public CmsCollectionInfo ChildArticleLinks { get; protected set; }
        

        public CmsCollectionInfo ParentArticleLinks { get; protected set; }
        

        public CmsCollectionInfo RelatedPages { get; protected set; }
        

        public CmsCollectionInfo ParentsOfWhichThisIsRelated { get; protected set; }
        

        public CmsCollectionInfo ArticleComments { get; protected set; }
        

        public CmsCollectionInfo ArticleMediaItems { get; protected set; }
        

        public CmsCollectionInfo ArticleRatings { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.Affiliates = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ArticleModule.ArticleBase>.GetPropertyBySelector(item => item.Affiliates),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase>.GetPropertyBySelector(item => item.LinkedContentPageNode)));
		
		//InitCollectionBaseOneToMany
		this.Article_CultureInfos = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ArticleModule.ArticleBase>.GetPropertyBySelector(item => item.Article_CultureInfos),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase>.GetPropertyBySelector(item => item.Article)));
		
		//InitCollectionBaseOneToMany
		this.ChildArticleLinks = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ArticleModule.ArticleBase>.GetPropertyBySelector(item => item.ChildArticleLinks),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Article_ParentChildLinkModule.Article_ParentChildLinkBase>.GetPropertyBySelector(item => item.Parent)));
		
		//InitCollectionBaseOneToMany
		this.ParentArticleLinks = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ArticleModule.ArticleBase>.GetPropertyBySelector(item => item.ParentArticleLinks),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Article_ParentChildLinkModule.Article_ParentChildLinkBase>.GetPropertyBySelector(item => item.Child)));
		
		//InitCollectionBaseOneToMany
		this.RelatedPages = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ArticleModule.ArticleBase>.GetPropertyBySelector(item => item.RelatedPages),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Article_RelatedLinkModule.Article_RelatedLinkBase>.GetPropertyBySelector(item => item.ParentPage)));
		
		//InitCollectionBaseOneToMany
		this.ParentsOfWhichThisIsRelated = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ArticleModule.ArticleBase>.GetPropertyBySelector(item => item.ParentsOfWhichThisIsRelated),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Article_RelatedLinkModule.Article_RelatedLinkBase>.GetPropertyBySelector(item => item.RelatedPage)));
		
		//InitCollectionBaseOneToMany
		this.ArticleComments = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ArticleModule.ArticleBase>.GetPropertyBySelector(item => item.ArticleComments),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase>.GetPropertyBySelector(item => item.Article)));
		
		//InitCollectionBaseOneToMany
		this.ArticleMediaItems = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ArticleModule.ArticleBase>.GetPropertyBySelector(item => item.ArticleMediaItems),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ArticleMediaItemModule.ArticleMediaItemBase>.GetPropertyBySelector(item => item.ContentPage)));
		
		//InitCollectionBaseOneToMany
		this.ArticleRatings = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ArticleModule.ArticleBase>.GetPropertyBySelector(item => item.ArticleRatings),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ArticleRatingModule.ArticleRatingBase>.GetPropertyBySelector(item => item.Article)));


			base.initBasicFields();
          
        }

    }
}
