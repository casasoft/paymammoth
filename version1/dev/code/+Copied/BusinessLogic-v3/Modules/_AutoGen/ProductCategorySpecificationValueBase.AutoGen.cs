using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "ProductCategorySpecificationValue")]
    public abstract class ProductCategorySpecificationValueBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IProductCategorySpecificationValueBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static ProductCategorySpecificationValueBaseFactory Factory
        {
            get
            {
                return ProductCategorySpecificationValueBaseFactory.Instance; 
            }
        }    
        /*
        public static ProductCategorySpecificationValueBase CreateNewItem()
        {
            return (ProductCategorySpecificationValueBase)Factory.CreateNewItem();
        }

		public static ProductCategorySpecificationValueBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<ProductCategorySpecificationValueBase, ProductCategorySpecificationValueBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static ProductCategorySpecificationValueBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static ProductCategorySpecificationValueBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<ProductCategorySpecificationValueBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<ProductCategorySpecificationValueBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<ProductCategorySpecificationValueBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _value;
        
        public virtual string Value 
        {
        	get
        	{
        		return _value;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_value,value);
        		_value = value;
        		
        	}
        }
        
/*
		public virtual long? ProductID
		{
		 	get 
		 	{
		 		return (this.Product != null ? (long?)this.Product.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ProductModule.ProductBase _product;
        public virtual BusinessLogic_v3.Modules.ProductModule.ProductBase Product 
        {
        	get
        	{
        		return _product;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_product,value);
        		_product = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ProductModule.IProductBase BusinessLogic_v3.Modules._AutoGen.IProductCategorySpecificationValueBaseAutoGen.Product 
        {
            get
            {
            	return this.Product;
            }
            set
            {
            	this.Product = (BusinessLogic_v3.Modules.ProductModule.ProductBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
