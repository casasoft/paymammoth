using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.BackgroundImageModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "BackgroundImage")]
    public abstract class BackgroundImageBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IBackgroundImageBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static BackgroundImageBaseFactory Factory
        {
            get
            {
                return BackgroundImageBaseFactory.Instance; 
            }
        }    
        /*
        public static BackgroundImageBase CreateNewItem()
        {
            return (BackgroundImageBase)Factory.CreateNewItem();
        }

		public static BackgroundImageBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<BackgroundImageBase, BackgroundImageBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static BackgroundImageBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static BackgroundImageBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<BackgroundImageBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<BackgroundImageBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<BackgroundImageBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _imagefilename;
        
        public virtual string ImageFilename 
        {
        	get
        	{
        		return _imagefilename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_imagefilename,value);
        		_imagefilename = value;
        		
        	}
        }
        
/*
		public virtual long? CategoryLinkID
		{
		 	get 
		 	{
		 		return (this.CategoryLink != null ? (long?)this.CategoryLink.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CategoryModule.CategoryBase _categorylink;
        public virtual BusinessLogic_v3.Modules.CategoryModule.CategoryBase CategoryLink 
        {
        	get
        	{
        		return _categorylink;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_categorylink,value);
        		_categorylink = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CategoryModule.ICategoryBase BusinessLogic_v3.Modules._AutoGen.IBackgroundImageBaseAutoGen.CategoryLink 
        {
            get
            {
            	return this.CategoryLink;
            }
            set
            {
            	this.CategoryLink = (BusinessLogic_v3.Modules.CategoryModule.CategoryBase) value;
            }
        }
            
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
