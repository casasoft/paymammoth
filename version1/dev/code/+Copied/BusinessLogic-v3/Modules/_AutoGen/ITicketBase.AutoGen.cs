using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.TicketModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface ITicketBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        string IPAddress { get; set; }
		//Iproperty_normal
        DateTime CreatedOn { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBase AssignedTo {get; set; }

		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.MemberModule.IMemberBase Member {get; set; }

		//Iproperty_normal
        DateTime? ClosedOn { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBase ClosedBy {get; set; }

		//Iproperty_normal
        string ClosedByIP { get; set; }
		//Iproperty_normal
        DateTime? ReOpenedOn { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBase ReOpenedByCmsUser {get; set; }

		//Iproperty_normal
        string ReOpenedByIP { get; set; }
		//Iproperty_normal
        string Title { get; set; }
		//Iproperty_normal
        BusinessLogic_v3.Enums.TICKET_PRIORITY TicketPriority { get; set; }
		//Iproperty_normal
        BusinessLogic_v3.Enums.TICKET_STATE TicketState { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.MemberModule.IMemberBase ReOpenedByMember {get; set; }

   

// [interface_base_collections]

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.TicketCommentModule.ITicketCommentBase> TicketComments { get; }

 
		

    	
    	
      
      

    }
}
