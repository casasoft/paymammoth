using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ContentText_CultureInfoModule;

//DbObjectBuilder v8

namespace BusinessLogic_v3.Modules._AutoGen
{
   
    public abstract class ContentText_CultureInfoBaseFactoryAutoGen  : BusinessLogic_v3.Classes.DbObjects.Factories.BaseDbFactory<ContentText_CultureInfoBase, IContentText_CultureInfoBase>, IContentText_CultureInfoBaseFactoryAutoGen
    
    {
    
        public ContentText_CultureInfoBaseFactoryAutoGen()
        {
        	
            
        }
        
		static ContentText_CultureInfoBaseFactoryAutoGen()
        {
            
        }
        
        
        public new static ContentText_CultureInfoBaseFactory Instance
        {
            get
            {
                return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<ContentText_CultureInfoBaseFactory>(null);
            }
        }
        
		public IQueryOver<ContentText_CultureInfoBase, ContentText_CultureInfoBase> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
        	ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<ContentText_CultureInfoBase>();
            qParams.FillQueryOver(q, this);
        	postGetQueryOver(q, qParams);
            return q;
            
        } 
        
        
    }
}
