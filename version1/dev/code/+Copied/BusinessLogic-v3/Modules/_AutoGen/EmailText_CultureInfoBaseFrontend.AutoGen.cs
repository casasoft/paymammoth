using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.EmailText_CultureInfoModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.EmailText_CultureInfoModule.EmailText_CultureInfoBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfoBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.EmailText_CultureInfoModule.EmailText_CultureInfoBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfoBase item)
        {
        	return BusinessLogic_v3.Frontend.EmailText_CultureInfoModule.EmailText_CultureInfoBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class EmailText_CultureInfoBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<EmailText_CultureInfoBaseFrontend, BusinessLogic_v3.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfoBase>

    {
		
        
        protected EmailText_CultureInfoBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
