using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.EventBasicModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "EventBasic")]
    public abstract class EventBasicBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IEventBasicBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static EventBasicBaseFactory Factory
        {
            get
            {
                return EventBasicBaseFactory.Instance; 
            }
        }    
        /*
        public static EventBasicBase CreateNewItem()
        {
            return (EventBasicBase)Factory.CreateNewItem();
        }

		public static EventBasicBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<EventBasicBase, EventBasicBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static EventBasicBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static EventBasicBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<EventBasicBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<EventBasicBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<EventBasicBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _descriptionhtml;
        
        public virtual string DescriptionHtml 
        {
        	get
        	{
        		return _descriptionhtml;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_descriptionhtml,value);
        		_descriptionhtml = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _summary;
        
        public virtual string Summary 
        {
        	get
        	{
        		return _summary;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_summary,value);
        		_summary = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _startdate;
        
        public virtual DateTime StartDate 
        {
        	get
        	{
        		return _startdate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_startdate,value);
        		_startdate = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _enddate;
        
        public virtual DateTime EndDate 
        {
        	get
        	{
        		return _enddate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_enddate,value);
        		_enddate = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _time;
        
        public virtual string Time 
        {
        	get
        	{
        		return _time;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_time,value);
        		_time = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _location;
        
        public virtual string Location 
        {
        	get
        	{
        		return _location;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_location,value);
        		_location = value;
        		
        	}
        }
        
/*
		public virtual long? CategoryID
		{
		 	get 
		 	{
		 		return (this.Category != null ? (long?)this.Category.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CategoryModule.CategoryBase _category;
        public virtual BusinessLogic_v3.Modules.CategoryModule.CategoryBase Category 
        {
        	get
        	{
        		return _category;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_category,value);
        		_category = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CategoryModule.ICategoryBase BusinessLogic_v3.Modules._AutoGen.IEventBasicBaseAutoGen.Category 
        {
            get
            {
            	return this.Category;
            }
            set
            {
            	this.Category = (BusinessLogic_v3.Modules.CategoryModule.CategoryBase) value;
            }
        }
            
/*
		public virtual long? EventSubmissionID
		{
		 	get 
		 	{
		 		return (this.EventSubmission != null ? (long?)this.EventSubmission.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.EventSubmissionModule.EventSubmissionBase _eventsubmission;
        public virtual BusinessLogic_v3.Modules.EventSubmissionModule.EventSubmissionBase EventSubmission 
        {
        	get
        	{
        		return _eventsubmission;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_eventsubmission,value);
        		_eventsubmission = value;
        	}
        }
            
        BusinessLogic_v3.Modules.EventSubmissionModule.IEventSubmissionBase BusinessLogic_v3.Modules._AutoGen.IEventBasicBaseAutoGen.EventSubmission 
        {
            get
            {
            	return this.EventSubmission;
            }
            set
            {
            	this.EventSubmission = (BusinessLogic_v3.Modules.EventSubmissionModule.EventSubmissionBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _imagefilename;
        
        public virtual string ImageFilename 
        {
        	get
        	{
        		return _imagefilename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_imagefilename,value);
        		_imagefilename = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __EventMediaItemsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.EventBasicModule.EventBasicBase, 
        	BusinessLogic_v3.Modules.EventMediaItemModule.EventMediaItemBase,
        	BusinessLogic_v3.Modules.EventMediaItemModule.IEventMediaItemBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.EventBasicModule.EventBasicBase, BusinessLogic_v3.Modules.EventMediaItemModule.EventMediaItemBase>
        {
            private BusinessLogic_v3.Modules.EventBasicModule.EventBasicBase _item = null;
            public __EventMediaItemsCollectionInfo(BusinessLogic_v3.Modules.EventBasicModule.EventBasicBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.EventMediaItemModule.EventMediaItemBase> Collection
            {
                get { return _item.__collection__EventMediaItems; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.EventBasicModule.EventBasicBase, BusinessLogic_v3.Modules.EventMediaItemModule.EventMediaItemBase>.SetLinkOnItem(BusinessLogic_v3.Modules.EventMediaItemModule.EventMediaItemBase item, BusinessLogic_v3.Modules.EventBasicModule.EventBasicBase value)
            {
                item.EventBasic = value;
            }
        }
        
        private __EventMediaItemsCollectionInfo _EventMediaItems = null;
        public __EventMediaItemsCollectionInfo EventMediaItems
        {
            get
            {
                if (_EventMediaItems == null)
                    _EventMediaItems = new __EventMediaItemsCollectionInfo((BusinessLogic_v3.Modules.EventBasicModule.EventBasicBase)this);
                return _EventMediaItems;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.EventMediaItemModule.IEventMediaItemBase> IEventBasicBaseAutoGen.EventMediaItems
        {
            get {  return this.EventMediaItems; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.EventMediaItemModule.EventMediaItemBase> __collection__EventMediaItems
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'EventMediaItemBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
