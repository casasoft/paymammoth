using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.ArticleCommentModule;
using BusinessLogic_v3.Cms.ArticleCommentModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class ArticleCommentBaseCmsFactory_AutoGen : CmsFactoryBase<ArticleCommentBaseCmsInfo, ArticleCommentBase>
    {
       
       public new static ArticleCommentBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ArticleCommentBaseCmsFactory)CmsFactoryBase<ArticleCommentBaseCmsInfo, ArticleCommentBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = ArticleCommentBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ArticleComment.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ArticleComment";

            this.QueryStringParamID = "ArticleCommentId";

            cmsInfo.TitlePlural = "Article Comments";

            cmsInfo.TitleSingular =  "Article Comment";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ArticleCommentBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "ArticleComment/";
			UsedInProject = ArticleCommentBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
