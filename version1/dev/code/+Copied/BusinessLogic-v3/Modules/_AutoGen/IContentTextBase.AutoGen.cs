using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ContentTextModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IContentTextBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        string Name { get; set; }
		//Iproperty_normal
        string Name_Search { get; set; }
		//Iproperty_normal
        string Identifier { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.ContentTextModule.IContentTextBase Parent {get; set; }

		//Iproperty_normal
        CS.General_v3.Enums.CMS_ACCESS_TYPE AllowAddSubItems_AccessRequired { get; set; }
		//Iproperty_normal
        CS.General_v3.Enums.CMS_ACCESS_TYPE VisibleInCMS_AccessRequired { get; set; }
		//Iproperty_normal
        BusinessLogic_v3.Enums.CONTENT_TEXT_TYPE ContentType { get; set; }
		//Iproperty_normal
        bool UsedInProject { get; set; }
		//IProperty_Multilingual
	   string ImageAlternateText { get; set ;}
		//Iproperty_normal
        string CustomContentTags { get; set; }
		//Iproperty_normal
        bool ProcessContentUsingNVelocity { get; set; }
		//Iproperty_normal
        bool _Computed_IsHtml { get; set; }
   

// [interface_base_collections]

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.ContentTextModule.IContentTextBase> ChildContentTexts { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.ContentText_CultureInfoModule.IContentText_CultureInfoBase> ContentText_CultureInfos { get; }

 
		

    	
    	
      
      

    }
}
