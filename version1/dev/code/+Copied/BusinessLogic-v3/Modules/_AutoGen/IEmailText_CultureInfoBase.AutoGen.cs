using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.EmailText_CultureInfoModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IEmailText_CultureInfoBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase CultureInfo {get; set; }

		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.EmailTextModule.IEmailTextBase EmailText {get; set; }

		//Iproperty_normal
        string Subject { get; set; }
		//Iproperty_normal
        string Subject_Search { get; set; }
		//Iproperty_normal
        string Body { get; set; }
		//Iproperty_normal
        string Body_Search { get; set; }
   

// [interface_base_collections]

 
		

    	
    	
      
      

    }
}
