using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.MemberResponsibleLimitsModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.MemberResponsibleLimitsModule.MemberResponsibleLimitsBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimitsBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.MemberResponsibleLimitsModule.MemberResponsibleLimitsBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimitsBase item)
        {
        	return BusinessLogic_v3.Frontend.MemberResponsibleLimitsModule.MemberResponsibleLimitsBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class MemberResponsibleLimitsBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<MemberResponsibleLimitsBaseFrontend, BusinessLogic_v3.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimitsBase>

    {
		
        
        protected MemberResponsibleLimitsBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
