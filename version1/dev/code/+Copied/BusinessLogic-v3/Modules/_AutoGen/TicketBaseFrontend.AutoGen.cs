using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.TicketModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.TicketModule.TicketBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.TicketModule.ITicketBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.TicketModule.TicketBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.TicketModule.ITicketBase item)
        {
        	return BusinessLogic_v3.Frontend.TicketModule.TicketBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class TicketBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<TicketBaseFrontend, BusinessLogic_v3.Modules.TicketModule.ITicketBase>

    {
		
        
        protected TicketBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
