using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.BrandModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "Brand")]
    public abstract class BrandBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IBrandBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
        

			var cultureInfo = __getCurrentCultureInfo(); //since this is a new item, it is imperative that the culture is preloaded
            
            
            base.initPropertiesForNewItem();
        }
    	
  
    	public BusinessLogic_v3.Modules.Brand_CultureInfoModule.Brand_CultureInfoBase GetCultureInfoByLanguage(CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 lang)
        {
            return (BusinessLogic_v3.Modules.Brand_CultureInfoModule.Brand_CultureInfoBase) __getCultureInfoByLanguage(lang);
        }

     	public static BrandBaseFactory Factory
        {
            get
            {
                return BrandBaseFactory.Instance; 
            }
        }    
        /*
        public static BrandBase CreateNewItem()
        {
            return (BrandBase)Factory.CreateNewItem();
        }

		public static BrandBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<BrandBase, BrandBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static BrandBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static BrandBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<BrandBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<BrandBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<BrandBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string Description
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.Brand_CultureInfoModule.Brand_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.Description;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.Brand_CultureInfoModule.Brand_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__Description_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.Description,value);
                cultureInfo.Description = value;
            }
        }
        
        private string _description;
        public virtual string __Description_cultureBase
        {
        	get
        	{
        		return _description;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_description,value);
        		
        		_description = value;
        	}
        }
		//BaseClass/Fields/Property_Normal
        private string _brandlogofilename;
        
        public virtual string BrandLogoFilename 
        {
        	get
        	{
        		return _brandlogofilename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_brandlogofilename,value);
        		_brandlogofilename = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _isfeatured;
        
        public virtual bool IsFeatured 
        {
        	get
        	{
        		return _isfeatured;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_isfeatured,value);
        		_isfeatured = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _url;
        
        public virtual string Url 
        {
        	get
        	{
        		return _url;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_url,value);
        		_url = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _importreference;
        
        public virtual string ImportReference 
        {
        	get
        	{
        		return _importreference;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_importreference,value);
        		_importreference = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.HREF_TARGET _hreftarget;
        
        public virtual CS.General_v3.Enums.HREF_TARGET HrefTarget 
        {
        	get
        	{
        		return _hreftarget;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_hreftarget,value);
        		_hreftarget = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __Brand_CultureInfosCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.BrandModule.BrandBase, 
        	BusinessLogic_v3.Modules.Brand_CultureInfoModule.Brand_CultureInfoBase,
        	BusinessLogic_v3.Modules.Brand_CultureInfoModule.IBrand_CultureInfoBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.BrandModule.BrandBase, BusinessLogic_v3.Modules.Brand_CultureInfoModule.Brand_CultureInfoBase>
        {
            private BusinessLogic_v3.Modules.BrandModule.BrandBase _item = null;
            public __Brand_CultureInfosCollectionInfo(BusinessLogic_v3.Modules.BrandModule.BrandBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.Brand_CultureInfoModule.Brand_CultureInfoBase> Collection
            {
                get { return _item.__collection__Brand_CultureInfos; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.BrandModule.BrandBase, BusinessLogic_v3.Modules.Brand_CultureInfoModule.Brand_CultureInfoBase>.SetLinkOnItem(BusinessLogic_v3.Modules.Brand_CultureInfoModule.Brand_CultureInfoBase item, BusinessLogic_v3.Modules.BrandModule.BrandBase value)
            {
                item.Brand = value;
            }
        }
        
        private __Brand_CultureInfosCollectionInfo _Brand_CultureInfos = null;
        public __Brand_CultureInfosCollectionInfo Brand_CultureInfos
        {
            get
            {
                if (_Brand_CultureInfos == null)
                    _Brand_CultureInfos = new __Brand_CultureInfosCollectionInfo((BusinessLogic_v3.Modules.BrandModule.BrandBase)this);
                return _Brand_CultureInfos;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.Brand_CultureInfoModule.IBrand_CultureInfoBase> IBrandBaseAutoGen.Brand_CultureInfos
        {
            get {  return this.Brand_CultureInfos; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.Brand_CultureInfoModule.Brand_CultureInfoBase> __collection__Brand_CultureInfos
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'Brand_CultureInfoBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ProductsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.BrandModule.BrandBase, 
        	BusinessLogic_v3.Modules.ProductModule.ProductBase,
        	BusinessLogic_v3.Modules.ProductModule.IProductBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.BrandModule.BrandBase, BusinessLogic_v3.Modules.ProductModule.ProductBase>
        {
            private BusinessLogic_v3.Modules.BrandModule.BrandBase _item = null;
            public __ProductsCollectionInfo(BusinessLogic_v3.Modules.BrandModule.BrandBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ProductModule.ProductBase> Collection
            {
                get { return _item.__collection__Products; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.BrandModule.BrandBase, BusinessLogic_v3.Modules.ProductModule.ProductBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ProductModule.ProductBase item, BusinessLogic_v3.Modules.BrandModule.BrandBase value)
            {
                item.Brand = value;
            }
        }
        
        private __ProductsCollectionInfo _Products = null;
        public __ProductsCollectionInfo Products
        {
            get
            {
                if (_Products == null)
                    _Products = new __ProductsCollectionInfo((BusinessLogic_v3.Modules.BrandModule.BrandBase)this);
                return _Products;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ProductModule.IProductBase> IBrandBaseAutoGen.Products
        {
            get {  return this.Products; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ProductModule.ProductBase> __collection__Products
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ProductBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
