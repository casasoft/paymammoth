using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.CmsAuditLogModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.CmsAuditLogModule.CmsAuditLogBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.CmsAuditLogModule.ICmsAuditLogBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.CmsAuditLogModule.CmsAuditLogBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.CmsAuditLogModule.ICmsAuditLogBase item)
        {
        	return BusinessLogic_v3.Frontend.CmsAuditLogModule.CmsAuditLogBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class CmsAuditLogBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<CmsAuditLogBaseFrontend, BusinessLogic_v3.Modules.CmsAuditLogModule.ICmsAuditLogBase>

    {
		
        
        protected CmsAuditLogBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
