using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ClassifiedModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IClassifiedBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        string Title { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.CategoryModule.ICategoryBase Category {get; set; }

		//Iproperty_normal
        string PhotoFilename { get; set; }
		//Iproperty_normal
        double Price { get; set; }
		//Iproperty_normal
        string Description { get; set; }
		//Iproperty_normal
        DateTime PostedOn { get; set; }
		//Iproperty_normal
        DateTime? ApprovedOn { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBase ApprovedBy {get; set; }

		//Iproperty_normal
        bool Approved { get; set; }
		//Iproperty_normal
        bool IsPriceNegotiable { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.MemberModule.IMemberBase CreatedBy {get; set; }

		//Iproperty_normal
        bool Submitted { get; set; }
		//Iproperty_normal
        string CustomContentTags { get; set; }
   

// [interface_base_collections]

 
		

    	
    	
      
      

    }
}
