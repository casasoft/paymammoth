using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.VacancyModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.VacancyModule.VacancyBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.VacancyModule.IVacancyBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.VacancyModule.VacancyBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.VacancyModule.IVacancyBase item)
        {
        	return BusinessLogic_v3.Frontend.VacancyModule.VacancyBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class VacancyBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<VacancyBaseFrontend, BusinessLogic_v3.Modules.VacancyModule.IVacancyBase>

    {
		
        
        protected VacancyBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
