using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.EmailTextModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IEmailTextBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        string Title { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.EmailTextModule.IEmailTextBase Parent {get; set; }

		//IProperty_Multilingual
	   string Subject { get; set ;}
		//IProperty_Multilingual
	   string Subject_Search { get; set ;}
		//Iproperty_normal
        string Remarks { get; set; }
		//Iproperty_normal
        string Identifier { get; set; }
		//Iproperty_normal
        CS.General_v3.Enums.CMS_ACCESS_TYPE VisibleInCMS_AccessRequired { get; set; }
		//Iproperty_normal
        bool UsedInProject { get; set; }
		//Iproperty_normal
        string CustomContentTags { get; set; }
		//Iproperty_normal
        string WhenIsThisEmailSent { get; set; }
		//Iproperty_normal
        bool NotifyAdminAboutEmail { get; set; }
		//Iproperty_normal
        string NotifyAdminCustomEmail { get; set; }
		//Iproperty_normal
        bool ProcessContentUsingNVelocity { get; set; }
   

// [interface_base_collections]

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.EmailTextModule.IEmailTextBase> ChildEmails { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfoBase> EmailText_CultureInfos { get; }

 
		

    	
    	
      
      

    }
}
