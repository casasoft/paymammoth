using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.EmailLogModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "EmailLog")]
    public abstract class EmailLogBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IEmailLogBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static EmailLogBaseFactory Factory
        {
            get
            {
                return EmailLogBaseFactory.Instance; 
            }
        }    
        /*
        public static EmailLogBase CreateNewItem()
        {
            return (EmailLogBase)Factory.CreateNewItem();
        }

		public static EmailLogBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<EmailLogBase, EmailLogBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static EmailLogBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static EmailLogBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<EmailLogBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<EmailLogBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<EmailLogBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private DateTime _datetime;
        
        public virtual DateTime DateTime 
        {
        	get
        	{
        		return _datetime;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_datetime,value);
        		_datetime = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _fromemail;
        
        public virtual string FromEmail 
        {
        	get
        	{
        		return _fromemail;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_fromemail,value);
        		_fromemail = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _fromname;
        
        public virtual string FromName 
        {
        	get
        	{
        		return _fromname;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_fromname,value);
        		_fromname = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _toemail;
        
        public virtual string ToEmail 
        {
        	get
        	{
        		return _toemail;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_toemail,value);
        		_toemail = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _toname;
        
        public virtual string ToName 
        {
        	get
        	{
        		return _toname;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_toname,value);
        		_toname = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _replytoemail;
        
        public virtual string ReplyToEmail 
        {
        	get
        	{
        		return _replytoemail;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_replytoemail,value);
        		_replytoemail = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _replytoname;
        
        public virtual string ReplyToName 
        {
        	get
        	{
        		return _replytoname;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_replytoname,value);
        		_replytoname = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _success;
        
        public virtual bool Success 
        {
        	get
        	{
        		return _success;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_success,value);
        		_success = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _subject;
        
        public virtual string Subject 
        {
        	get
        	{
        		return _subject;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_subject,value);
        		_subject = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _plaintext;
        
        public virtual string PlainText 
        {
        	get
        	{
        		return _plaintext;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_plaintext,value);
        		_plaintext = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _htmltext;
        
        public virtual string HtmlText 
        {
        	get
        	{
        		return _htmltext;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_htmltext,value);
        		_htmltext = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _attachements;
        
        public virtual string Attachements 
        {
        	get
        	{
        		return _attachements;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_attachements,value);
        		_attachements = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _resources;
        
        public virtual string Resources 
        {
        	get
        	{
        		return _resources;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_resources,value);
        		_resources = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _host;
        
        public virtual string Host 
        {
        	get
        	{
        		return _host;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_host,value);
        		_host = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _port;
        
        public virtual string Port 
        {
        	get
        	{
        		return _port;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_port,value);
        		_port = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _user;
        
        public virtual string User 
        {
        	get
        	{
        		return _user;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_user,value);
        		_user = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _pass;
        
        public virtual string Pass 
        {
        	get
        	{
        		return _pass;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_pass,value);
        		_pass = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _usessecureconnection;
        
        public virtual bool UsesSecureConnection 
        {
        	get
        	{
        		return _usessecureconnection;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_usessecureconnection,value);
        		_usessecureconnection = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _resultdetails;
        
        public virtual string ResultDetails 
        {
        	get
        	{
        		return _resultdetails;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_resultdetails,value);
        		_resultdetails = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
