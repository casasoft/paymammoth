using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ProductVariationSizeModule;
using BusinessLogic_v3.Cms.ProductVariationSizeModule;
using BusinessLogic_v3.Frontend.ProductVariationSizeModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ProductVariationSizeBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ProductVariationSizeModule.ProductVariationSizeBase>
    {
		public ProductVariationSizeBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ProductVariationSizeModule.ProductVariationSizeBase dbItem)
            : base(BusinessLogic_v3.Cms.ProductVariationSizeModule.ProductVariationSizeBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ProductVariationSizeBaseFrontend FrontendItem
        {
            get { return (ProductVariationSizeBaseFrontend)base.FrontendItem; }

        }
        public new ProductVariationSizeBase DbItem
        {
            get
            {
                return (ProductVariationSizeBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo ProductVariations { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.ProductVariations = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductVariationSizeModule.ProductVariationSizeBase>.GetPropertyBySelector(item => item.ProductVariations),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase>.GetPropertyBySelector(item => item.Size)));


			base.initBasicFields();
          
        }

    }
}
