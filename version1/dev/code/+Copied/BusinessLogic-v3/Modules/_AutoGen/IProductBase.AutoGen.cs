using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ProductModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IProductBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//IProperty_Multilingual
	   string Title { get; set ;}
		//Iproperty_normal
        string ReferenceCode { get; set; }
		//Iproperty_normal
        string SupplierReferenceCode { get; set; }
		//IProperty_Multilingual
	   string Description { get; set ;}
		//Iproperty_normal
        double PriceRetailIncTaxPerUnit { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.BrandModule.IBrandBase Brand {get; set; }

		//Iproperty_normal
        string ExtraInfo { get; set; }
		//Iproperty_normal
        double PriceDiscounted { get; set; }
		//Iproperty_normal
        string PDFFilename { get; set; }
		//Iproperty_normal
        bool IsSpecialOffer { get; set; }
		//Iproperty_normal
        bool ShowPrice { get; set; }
		//Iproperty_normal
        double WeightInKg { get; set; }
		//Iproperty_normal
        int WarrantyInMonths { get; set; }
		//Iproperty_normal
        DateTime DateAdded { get; set; }
		//Iproperty_normal
        bool IsFeatured { get; set; }
		//Iproperty_normal
        string ImportReference { get; set; }
		//Iproperty_normal
        string WarrantyText { get; set; }
		//Iproperty_normal
        string MetaKeywords { get; set; }
		//Iproperty_normal
        string CategoryFeatureValues_ForSearch { get; set; }
		//Iproperty_normal
        double? TaxRatePercentage { get; set; }
		//Iproperty_normal
        string ProductSpecificationsHtml { get; set; }
		//Iproperty_normal
        double DimensionsLengthInCm { get; set; }
		//Iproperty_normal
        double DimensionsWidthInCm { get; set; }
		//Iproperty_normal
        double DimensionsHeightInCm { get; set; }
		//Iproperty_normal
        double DimensionsVolumeInCc { get; set; }
		//Iproperty_normal
        string ShortDescription { get; set; }
		//IProperty_Multilingual
	   string MetaTitle { get; set ;}
		//IProperty_Multilingual
	   string MetaDescription { get; set ;}
		//IProperty_Multilingual
	   string Material { get; set ;}
		//Iproperty_normal
        double _SpecialOfferDiscount { get; set; }
		//Iproperty_normal
        string CustomContentTags { get; set; }
   

// [interface_base_collections]

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.CategoryFeatureModule.ICategoryFeatureBase> CategoryFeatures { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.ContactFormModule.IContactFormBase> ContactForms { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.Product_CultureInfoModule.IProduct_CultureInfoBase> Product_CultureInfos { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.ProductCategoryModule.IProductCategoryBase> CategoryLinks { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValueBase> ProductCategoryFeatureValues { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValueBase> ProductCategorySpecificationValues { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.ProductRelatedProductLinkModule.IProductRelatedProductLinkBase> RelatedProductLinks { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.ProductVariationModule.IProductVariationBase> ProductVariations { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.ProductVersionModule.IProductVersionBase> ProductVersions { get; }

 
		

    	
    	
      
      

    }
}
