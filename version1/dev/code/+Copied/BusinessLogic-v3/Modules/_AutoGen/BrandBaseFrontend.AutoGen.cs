using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.BrandModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.BrandModule.BrandBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.BrandModule.IBrandBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.BrandModule.BrandBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.BrandModule.IBrandBase item)
        {
        	return BusinessLogic_v3.Frontend.BrandModule.BrandBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class BrandBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<BrandBaseFrontend, BusinessLogic_v3.Modules.BrandModule.IBrandBase>

    {
		
        
        protected BrandBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
