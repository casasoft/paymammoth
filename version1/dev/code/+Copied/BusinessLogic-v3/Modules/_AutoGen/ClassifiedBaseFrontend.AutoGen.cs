using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ClassifiedModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.ClassifiedModule.ClassifiedBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.ClassifiedModule.IClassifiedBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.ClassifiedModule.ClassifiedBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.ClassifiedModule.IClassifiedBase item)
        {
        	return BusinessLogic_v3.Frontend.ClassifiedModule.ClassifiedBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ClassifiedBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<ClassifiedBaseFrontend, BusinessLogic_v3.Modules.ClassifiedModule.IClassifiedBase>

    {
		
        
        protected ClassifiedBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
