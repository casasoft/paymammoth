using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ContentTextModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.ContentTextModule.ContentTextBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.ContentTextModule.IContentTextBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.ContentTextModule.ContentTextBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.ContentTextModule.IContentTextBase item)
        {
        	return BusinessLogic_v3.Frontend.ContentTextModule.ContentTextBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ContentTextBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<ContentTextBaseFrontend, BusinessLogic_v3.Modules.ContentTextModule.IContentTextBase>

    {
		
        
        protected ContentTextBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
