using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.ShippingMethodModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "ShippingMethod")]
    public abstract class ShippingMethodBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IShippingMethodBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static ShippingMethodBaseFactory Factory
        {
            get
            {
                return ShippingMethodBaseFactory.Instance; 
            }
        }    
        /*
        public static ShippingMethodBase CreateNewItem()
        {
            return (ShippingMethodBase)Factory.CreateNewItem();
        }

		public static ShippingMethodBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<ShippingMethodBase, ShippingMethodBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static ShippingMethodBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static ShippingMethodBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<ShippingMethodBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<ShippingMethodBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<ShippingMethodBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _description;
        
        public virtual string Description 
        {
        	get
        	{
        		return _description;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_description,value);
        		_description = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _minimumorderamount;
        
        public virtual double MinimumOrderAmount 
        {
        	get
        	{
        		return _minimumorderamount;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_minimumorderamount,value);
        		_minimumorderamount = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _extrachargeamountifminimumnotreached;
        
        public virtual double ExtraChargeAmountIfMinimumNotReached 
        {
        	get
        	{
        		return _extrachargeamountifminimumnotreached;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_extrachargeamountifminimumnotreached,value);
        		_extrachargeamountifminimumnotreached = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _isdefaultmethod;
        
        public virtual bool IsDefaultMethod 
        {
        	get
        	{
        		return _isdefaultmethod;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_isdefaultmethod,value);
        		_isdefaultmethod = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __OrdersCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase, 
        	BusinessLogic_v3.Modules.OrderModule.OrderBase,
        	BusinessLogic_v3.Modules.OrderModule.IOrderBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase, BusinessLogic_v3.Modules.OrderModule.OrderBase>
        {
            private BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase _item = null;
            public __OrdersCollectionInfo(BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.OrderModule.OrderBase> Collection
            {
                get { return _item.__collection__Orders; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase, BusinessLogic_v3.Modules.OrderModule.OrderBase>.SetLinkOnItem(BusinessLogic_v3.Modules.OrderModule.OrderBase item, BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase value)
            {
                item.ShippingMethod = value;
            }
        }
        
        private __OrdersCollectionInfo _Orders = null;
        public __OrdersCollectionInfo Orders
        {
            get
            {
                if (_Orders == null)
                    _Orders = new __OrdersCollectionInfo((BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase)this);
                return _Orders;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.OrderModule.IOrderBase> IShippingMethodBaseAutoGen.Orders
        {
            get {  return this.Orders; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.OrderModule.OrderBase> __collection__Orders
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'OrderBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ShippingRatesGroupsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase, 
        	BusinessLogic_v3.Modules.ShippingRatesGroupModule.ShippingRatesGroupBase,
        	BusinessLogic_v3.Modules.ShippingRatesGroupModule.IShippingRatesGroupBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase, BusinessLogic_v3.Modules.ShippingRatesGroupModule.ShippingRatesGroupBase>
        {
            private BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase _item = null;
            public __ShippingRatesGroupsCollectionInfo(BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ShippingRatesGroupModule.ShippingRatesGroupBase> Collection
            {
                get { return _item.__collection__ShippingRatesGroups; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase, BusinessLogic_v3.Modules.ShippingRatesGroupModule.ShippingRatesGroupBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ShippingRatesGroupModule.ShippingRatesGroupBase item, BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase value)
            {
                item.ShippingMethod = value;
            }
        }
        
        private __ShippingRatesGroupsCollectionInfo _ShippingRatesGroups = null;
        public __ShippingRatesGroupsCollectionInfo ShippingRatesGroups
        {
            get
            {
                if (_ShippingRatesGroups == null)
                    _ShippingRatesGroups = new __ShippingRatesGroupsCollectionInfo((BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase)this);
                return _ShippingRatesGroups;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ShippingRatesGroupModule.IShippingRatesGroupBase> IShippingMethodBaseAutoGen.ShippingRatesGroups
        {
            get {  return this.ShippingRatesGroups; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ShippingRatesGroupModule.ShippingRatesGroupBase> __collection__ShippingRatesGroups
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ShippingRatesGroupBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
