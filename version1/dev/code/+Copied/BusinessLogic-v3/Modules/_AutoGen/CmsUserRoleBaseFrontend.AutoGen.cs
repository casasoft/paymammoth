using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.CmsUserRoleModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.CmsUserRoleModule.CmsUserRoleBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.CmsUserRoleModule.ICmsUserRoleBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.CmsUserRoleModule.CmsUserRoleBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.CmsUserRoleModule.ICmsUserRoleBase item)
        {
        	return BusinessLogic_v3.Frontend.CmsUserRoleModule.CmsUserRoleBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class CmsUserRoleBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<CmsUserRoleBaseFrontend, BusinessLogic_v3.Modules.CmsUserRoleModule.ICmsUserRoleBase>

    {
		
        
        protected CmsUserRoleBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
