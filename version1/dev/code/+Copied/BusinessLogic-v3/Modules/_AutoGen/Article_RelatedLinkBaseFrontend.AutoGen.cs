using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.Article_RelatedLinkModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.Article_RelatedLinkModule.Article_RelatedLinkBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.Article_RelatedLinkModule.IArticle_RelatedLinkBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.Article_RelatedLinkModule.Article_RelatedLinkBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.Article_RelatedLinkModule.IArticle_RelatedLinkBase item)
        {
        	return BusinessLogic_v3.Frontend.Article_RelatedLinkModule.Article_RelatedLinkBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class Article_RelatedLinkBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<Article_RelatedLinkBaseFrontend, BusinessLogic_v3.Modules.Article_RelatedLinkModule.IArticle_RelatedLinkBase>

    {
		
        
        protected Article_RelatedLinkBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
