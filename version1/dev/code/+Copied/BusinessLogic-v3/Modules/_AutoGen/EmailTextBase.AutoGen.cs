using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.EmailTextModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "EmailText")]
    public abstract class EmailTextBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IEmailTextBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
        

			var cultureInfo = __getCurrentCultureInfo(); //since this is a new item, it is imperative that the culture is preloaded
            
            
            base.initPropertiesForNewItem();
        }
    	
  
    	public BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase GetCultureInfoByLanguage(CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 lang)
        {
            return (BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase) __getCultureInfoByLanguage(lang);
        }

     	public static EmailTextBaseFactory Factory
        {
            get
            {
                return EmailTextBaseFactory.Instance; 
            }
        }    
        /*
        public static EmailTextBase CreateNewItem()
        {
            return (EmailTextBase)Factory.CreateNewItem();
        }

		public static EmailTextBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<EmailTextBase, EmailTextBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static EmailTextBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static EmailTextBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<EmailTextBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<EmailTextBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<EmailTextBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
/*
		public virtual long? ParentID
		{
		 	get 
		 	{
		 		return (this.Parent != null ? (long?)this.Parent.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase _parent;
        public virtual BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase Parent 
        {
        	get
        	{
        		return _parent;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_parent,value);
        		_parent = value;
        	}
        }
            
        BusinessLogic_v3.Modules.EmailTextModule.IEmailTextBase BusinessLogic_v3.Modules._AutoGen.IEmailTextBaseAutoGen.Parent 
        {
            get
            {
            	return this.Parent;
            }
            set
            {
            	this.Parent = (BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string Subject
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.Subject;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__Subject_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.Subject,value);
                cultureInfo.Subject = value;
            }
        }
        
        private string _subject;
        public virtual string __Subject_cultureBase
        {
        	get
        	{
        		return _subject;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_subject,value);
        		
        		_subject = value;
        	}
        }
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string Subject_Search
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.Subject_Search;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__Subject_Search_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.Subject_Search,value);
                cultureInfo.Subject_Search = value;
            }
        }
        
        private string _subject_search;
        public virtual string __Subject_Search_cultureBase
        {
        	get
        	{
        		return _subject_search;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_subject_search,value);
        		
        		_subject_search = value;
        	}
        }
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string Body
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.Body;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__Body_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.Body,value);
                cultureInfo.Body = value;
            }
        }
        
        private string _body;
        public virtual string __Body_cultureBase
        {
        	get
        	{
        		return _body;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_body,value);
        		
        		_body = value;
        	}
        }
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string Body_Search
        {
            get
            {
                var cultureInfo = (BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.Body_Search;
                
            }
            set
            {
            
                
                var cultureInfo = (BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__Body_Search_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.Body_Search,value);
                cultureInfo.Body_Search = value;
            }
        }
        
        private string _body_search;
        public virtual string __Body_Search_cultureBase
        {
        	get
        	{
        		return _body_search;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_body_search,value);
        		
        		_body_search = value;
        	}
        }
		//BaseClass/Fields/Property_Normal
        private string _remarks;
        
        public virtual string Remarks 
        {
        	get
        	{
        		return _remarks;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_remarks,value);
        		_remarks = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _identifier;
        
        public virtual string Identifier 
        {
        	get
        	{
        		return _identifier;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_identifier,value);
        		_identifier = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.CMS_ACCESS_TYPE _visibleincms_accessrequired;
        
        public virtual CS.General_v3.Enums.CMS_ACCESS_TYPE VisibleInCMS_AccessRequired 
        {
        	get
        	{
        		return _visibleincms_accessrequired;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_visibleincms_accessrequired,value);
        		_visibleincms_accessrequired = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _usedinproject;
        
        public virtual bool UsedInProject 
        {
        	get
        	{
        		return _usedinproject;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_usedinproject,value);
        		_usedinproject = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _customcontenttags;
        
        public virtual string CustomContentTags 
        {
        	get
        	{
        		return _customcontenttags;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_customcontenttags,value);
        		_customcontenttags = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _whenisthisemailsent;
        
        public virtual string WhenIsThisEmailSent 
        {
        	get
        	{
        		return _whenisthisemailsent;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_whenisthisemailsent,value);
        		_whenisthisemailsent = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _notifyadminaboutemail;
        
        public virtual bool NotifyAdminAboutEmail 
        {
        	get
        	{
        		return _notifyadminaboutemail;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_notifyadminaboutemail,value);
        		_notifyadminaboutemail = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _notifyadmincustomemail;
        
        public virtual string NotifyAdminCustomEmail 
        {
        	get
        	{
        		return _notifyadmincustomemail;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_notifyadmincustomemail,value);
        		_notifyadmincustomemail = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _processcontentusingnvelocity;
        
        public virtual bool ProcessContentUsingNVelocity 
        {
        	get
        	{
        		return _processcontentusingnvelocity;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_processcontentusingnvelocity,value);
        		_processcontentusingnvelocity = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ChildEmailsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase, 
        	BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase,
        	BusinessLogic_v3.Modules.EmailTextModule.IEmailTextBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase, BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase>
        {
            private BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase _item = null;
            public __ChildEmailsCollectionInfo(BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase> Collection
            {
                get { return _item.__collection__ChildEmails; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase, BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase>.SetLinkOnItem(BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase item, BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase value)
            {
                item.Parent = value;
            }
        }
        
        private __ChildEmailsCollectionInfo _ChildEmails = null;
        public __ChildEmailsCollectionInfo ChildEmails
        {
            get
            {
                if (_ChildEmails == null)
                    _ChildEmails = new __ChildEmailsCollectionInfo((BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase)this);
                return _ChildEmails;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.EmailTextModule.IEmailTextBase> IEmailTextBaseAutoGen.ChildEmails
        {
            get {  return this.ChildEmails; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase> __collection__ChildEmails
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'EmailTextBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __EmailText_CultureInfosCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase, 
        	BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase,
        	BusinessLogic_v3.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfoBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase, BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase>
        {
            private BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase _item = null;
            public __EmailText_CultureInfosCollectionInfo(BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase> Collection
            {
                get { return _item.__collection__EmailText_CultureInfos; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase, BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase>.SetLinkOnItem(BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase item, BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase value)
            {
                item.EmailText = value;
            }
        }
        
        private __EmailText_CultureInfosCollectionInfo _EmailText_CultureInfos = null;
        public __EmailText_CultureInfosCollectionInfo EmailText_CultureInfos
        {
            get
            {
                if (_EmailText_CultureInfos == null)
                    _EmailText_CultureInfos = new __EmailText_CultureInfosCollectionInfo((BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase)this);
                return _EmailText_CultureInfos;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfoBase> IEmailTextBaseAutoGen.EmailText_CultureInfos
        {
            get {  return this.EmailText_CultureInfos; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase> __collection__EmailText_CultureInfos
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'EmailText_CultureInfoBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
