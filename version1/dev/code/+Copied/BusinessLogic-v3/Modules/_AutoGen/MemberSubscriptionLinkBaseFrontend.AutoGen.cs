using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.MemberSubscriptionLinkModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.MemberSubscriptionLinkModule.MemberSubscriptionLinkBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.IMemberSubscriptionLinkBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.MemberSubscriptionLinkModule.MemberSubscriptionLinkBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.IMemberSubscriptionLinkBase item)
        {
        	return BusinessLogic_v3.Frontend.MemberSubscriptionLinkModule.MemberSubscriptionLinkBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class MemberSubscriptionLinkBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<MemberSubscriptionLinkBaseFrontend, BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.IMemberSubscriptionLinkBase>

    {
		
        
        protected MemberSubscriptionLinkBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
