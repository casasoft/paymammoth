using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.AffiliateModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IAffiliateBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        string Title { get; set; }
		//Iproperty_normal
        string CssFilename { get; set; }
		//Iproperty_normal
        string LogoFilename { get; set; }
		//Iproperty_normal
        string Username { get; set; }
		//Iproperty_normal
        string Password { get; set; }
		//Iproperty_normal
        DateTime ActiveFrom { get; set; }
		//Iproperty_normal
        string AffiliateCode { get; set; }
		//Iproperty_normal
        string ReferralCode { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.ArticleModule.IArticleBase LinkedContentPageNode {get; set; }

		//Iproperty_normal
        double CommissionRate { get; set; }
		//Iproperty_normal
        string AffiliateUrl { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBase LinkedCmsUser {get; set; }

		//Iproperty_normal
        bool HasWhiteLabellingFunctionality { get; set; }
		//Iproperty_normal
        string AffiliateWhiteLabelBaseUrl { get; set; }
		//Iproperty_normal
        string MainPanelHtml { get; set; }
		//Iproperty_normal
        bool Activated { get; set; }
   

// [interface_base_collections]

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.IAffiliatePaymentInfoBase> AffiliatePaymentInfos { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.MemberModule.IMemberBase> Members { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.OrderModule.IOrderBase> Orders { get; }

 
		

    	
    	
      
      

    }
}
