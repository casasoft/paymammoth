using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule;
using BusinessLogic_v3.Cms.SpecialOffer_CultureInfoModule;
using BusinessLogic_v3.Frontend.SpecialOffer_CultureInfoModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class SpecialOffer_CultureInfoBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule.SpecialOffer_CultureInfoBase>
    {
		public SpecialOffer_CultureInfoBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule.SpecialOffer_CultureInfoBase dbItem)
            : base(BusinessLogic_v3.Cms.SpecialOffer_CultureInfoModule.SpecialOffer_CultureInfoBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new SpecialOffer_CultureInfoBaseFrontend FrontendItem
        {
            get { return (SpecialOffer_CultureInfoBaseFrontend)base.FrontendItem; }

        }
        public new SpecialOffer_CultureInfoBase DbItem
        {
            get
            {
                return (SpecialOffer_CultureInfoBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo CultureInfo { get; protected set; }

        public CmsPropertyInfo SpecialOffer { get; protected set; }

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
