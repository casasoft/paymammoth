using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.MemberLoginInfoModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "MemberLoginInfo")]
    public abstract class MemberLoginInfoBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IMemberLoginInfoBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static MemberLoginInfoBaseFactory Factory
        {
            get
            {
                return MemberLoginInfoBaseFactory.Instance; 
            }
        }    
        /*
        public static MemberLoginInfoBase CreateNewItem()
        {
            return (MemberLoginInfoBase)Factory.CreateNewItem();
        }

		public static MemberLoginInfoBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<MemberLoginInfoBase, MemberLoginInfoBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static MemberLoginInfoBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static MemberLoginInfoBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<MemberLoginInfoBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<MemberLoginInfoBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<MemberLoginInfoBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? MemberID
		{
		 	get 
		 	{
		 		return (this.Member != null ? (long?)this.Member.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.MemberModule.MemberBase _member;
        public virtual BusinessLogic_v3.Modules.MemberModule.MemberBase Member 
        {
        	get
        	{
        		return _member;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_member,value);
        		_member = value;
        	}
        }
            
        BusinessLogic_v3.Modules.MemberModule.IMemberBase BusinessLogic_v3.Modules._AutoGen.IMemberLoginInfoBaseAutoGen.Member 
        {
            get
            {
            	return this.Member;
            }
            set
            {
            	this.Member = (BusinessLogic_v3.Modules.MemberModule.MemberBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private DateTime _datetime;
        
        public virtual DateTime DateTime 
        {
        	get
        	{
        		return _datetime;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_datetime,value);
        		_datetime = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _ip;
        
        public virtual string IP 
        {
        	get
        	{
        		return _ip;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_ip,value);
        		_ip = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private BusinessLogic_v3.Enums.MEMBER_LOGIN_INFO _logininfotype;
        
        public virtual BusinessLogic_v3.Enums.MEMBER_LOGIN_INFO LoginInfoType 
        {
        	get
        	{
        		return _logininfotype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_logininfotype,value);
        		_logininfotype = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _additionalmsg;
        
        public virtual string AdditionalMsg 
        {
        	get
        	{
        		return _additionalmsg;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_additionalmsg,value);
        		_additionalmsg = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
