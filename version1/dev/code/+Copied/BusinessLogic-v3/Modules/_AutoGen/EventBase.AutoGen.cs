using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.EventModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "Event")]
    public abstract class EventBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IEventBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static EventBaseFactory Factory
        {
            get
            {
                return EventBaseFactory.Instance; 
            }
        }    
        /*
        public static EventBase CreateNewItem()
        {
            return (EventBase)Factory.CreateNewItem();
        }

		public static EventBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<EventBase, EventBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static EventBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static EventBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<EventBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<EventBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<EventBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _description;
        
        public virtual string Description 
        {
        	get
        	{
        		return _description;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_description,value);
        		_description = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _startdate;
        
        public virtual DateTime StartDate 
        {
        	get
        	{
        		return _startdate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_startdate,value);
        		_startdate = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _enddate;
        
        public virtual DateTime EndDate 
        {
        	get
        	{
        		return _enddate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_enddate,value);
        		_enddate = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _imagefilename;
        
        public virtual string ImageFilename 
        {
        	get
        	{
        		return _imagefilename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_imagefilename,value);
        		_imagefilename = value;
        		
        	}
        }
        
/*
		public virtual long? CategoryID
		{
		 	get 
		 	{
		 		return (this.Category != null ? (long?)this.Category.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CategoryModule.CategoryBase _category;
        public virtual BusinessLogic_v3.Modules.CategoryModule.CategoryBase Category 
        {
        	get
        	{
        		return _category;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_category,value);
        		_category = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CategoryModule.ICategoryBase BusinessLogic_v3.Modules._AutoGen.IEventBaseAutoGen.Category 
        {
            get
            {
            	return this.Category;
            }
            set
            {
            	this.Category = (BusinessLogic_v3.Modules.CategoryModule.CategoryBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _subtitle;
        
        public virtual string SubTitle 
        {
        	get
        	{
        		return _subtitle;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_subtitle,value);
        		_subtitle = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _price;
        
        public virtual string Price 
        {
        	get
        	{
        		return _price;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_price,value);
        		_price = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _identifier;
        
        public virtual string Identifier 
        {
        	get
        	{
        		return _identifier;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_identifier,value);
        		_identifier = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _sessions;
        
        public virtual string Sessions 
        {
        	get
        	{
        		return _sessions;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_sessions,value);
        		_sessions = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _classsize;
        
        public virtual int ClassSize 
        {
        	get
        	{
        		return _classsize;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_classsize,value);
        		_classsize = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _dates;
        
        public virtual string Dates 
        {
        	get
        	{
        		return _dates;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_dates,value);
        		_dates = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _duration;
        
        public virtual string Duration 
        {
        	get
        	{
        		return _duration;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_duration,value);
        		_duration = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _shortdescription;
        
        public virtual string ShortDescription 
        {
        	get
        	{
        		return _shortdescription;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_shortdescription,value);
        		_shortdescription = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _location;
        
        public virtual string Location 
        {
        	get
        	{
        		return _location;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_location,value);
        		_location = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _summary;
        
        public virtual string Summary 
        {
        	get
        	{
        		return _summary;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_summary,value);
        		_summary = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _time;
        
        public virtual string Time 
        {
        	get
        	{
        		return _time;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_time,value);
        		_time = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __ContactFormsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.EventModule.EventBase, 
        	BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase,
        	BusinessLogic_v3.Modules.ContactFormModule.IContactFormBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.EventModule.EventBase, BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>
        {
            private BusinessLogic_v3.Modules.EventModule.EventBase _item = null;
            public __ContactFormsCollectionInfo(BusinessLogic_v3.Modules.EventModule.EventBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase> Collection
            {
                get { return _item.__collection__ContactForms; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.EventModule.EventBase, BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.SetLinkOnItem(BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase item, BusinessLogic_v3.Modules.EventModule.EventBase value)
            {
                item.Event = value;
            }
        }
        
        private __ContactFormsCollectionInfo _ContactForms = null;
        public __ContactFormsCollectionInfo ContactForms
        {
            get
            {
                if (_ContactForms == null)
                    _ContactForms = new __ContactFormsCollectionInfo((BusinessLogic_v3.Modules.EventModule.EventBase)this);
                return _ContactForms;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.ContactFormModule.IContactFormBase> IEventBaseAutoGen.ContactForms
        {
            get {  return this.ContactForms; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase> __collection__ContactForms
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'ContactFormBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __EventMediaItemsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.EventModule.EventBase, 
        	BusinessLogic_v3.Modules.EventMediaItemModule.EventMediaItemBase,
        	BusinessLogic_v3.Modules.EventMediaItemModule.IEventMediaItemBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.EventModule.EventBase, BusinessLogic_v3.Modules.EventMediaItemModule.EventMediaItemBase>
        {
            private BusinessLogic_v3.Modules.EventModule.EventBase _item = null;
            public __EventMediaItemsCollectionInfo(BusinessLogic_v3.Modules.EventModule.EventBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.EventMediaItemModule.EventMediaItemBase> Collection
            {
                get { return _item.__collection__EventMediaItems; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.EventModule.EventBase, BusinessLogic_v3.Modules.EventMediaItemModule.EventMediaItemBase>.SetLinkOnItem(BusinessLogic_v3.Modules.EventMediaItemModule.EventMediaItemBase item, BusinessLogic_v3.Modules.EventModule.EventBase value)
            {
                item.Event = value;
            }
        }
        
        private __EventMediaItemsCollectionInfo _EventMediaItems = null;
        public __EventMediaItemsCollectionInfo EventMediaItems
        {
            get
            {
                if (_EventMediaItems == null)
                    _EventMediaItems = new __EventMediaItemsCollectionInfo((BusinessLogic_v3.Modules.EventModule.EventBase)this);
                return _EventMediaItems;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.EventMediaItemModule.IEventMediaItemBase> IEventBaseAutoGen.EventMediaItems
        {
            get {  return this.EventMediaItems; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.EventMediaItemModule.EventMediaItemBase> __collection__EventMediaItems
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'EventMediaItemBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __EventSessionsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.EventModule.EventBase, 
        	BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase,
        	BusinessLogic_v3.Modules.EventSessionModule.IEventSessionBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.EventModule.EventBase, BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase>
        {
            private BusinessLogic_v3.Modules.EventModule.EventBase _item = null;
            public __EventSessionsCollectionInfo(BusinessLogic_v3.Modules.EventModule.EventBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase> Collection
            {
                get { return _item.__collection__EventSessions; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.EventModule.EventBase, BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase>.SetLinkOnItem(BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase item, BusinessLogic_v3.Modules.EventModule.EventBase value)
            {
                item.Event = value;
            }
        }
        
        private __EventSessionsCollectionInfo _EventSessions = null;
        public __EventSessionsCollectionInfo EventSessions
        {
            get
            {
                if (_EventSessions == null)
                    _EventSessions = new __EventSessionsCollectionInfo((BusinessLogic_v3.Modules.EventModule.EventBase)this);
                return _EventSessions;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.EventSessionModule.IEventSessionBase> IEventBaseAutoGen.EventSessions
        {
            get {  return this.EventSessions; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase> __collection__EventSessions
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'EventSessionBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __FileItemsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.EventModule.EventBase, 
        	BusinessLogic_v3.Modules.FileItemModule.FileItemBase,
        	BusinessLogic_v3.Modules.FileItemModule.IFileItemBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.EventModule.EventBase, BusinessLogic_v3.Modules.FileItemModule.FileItemBase>
        {
            private BusinessLogic_v3.Modules.EventModule.EventBase _item = null;
            public __FileItemsCollectionInfo(BusinessLogic_v3.Modules.EventModule.EventBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.FileItemModule.FileItemBase> Collection
            {
                get { return _item.__collection__FileItems; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.EventModule.EventBase, BusinessLogic_v3.Modules.FileItemModule.FileItemBase>.SetLinkOnItem(BusinessLogic_v3.Modules.FileItemModule.FileItemBase item, BusinessLogic_v3.Modules.EventModule.EventBase value)
            {
                item.Event = value;
            }
        }
        
        private __FileItemsCollectionInfo _FileItems = null;
        public __FileItemsCollectionInfo FileItems
        {
            get
            {
                if (_FileItems == null)
                    _FileItems = new __FileItemsCollectionInfo((BusinessLogic_v3.Modules.EventModule.EventBase)this);
                return _FileItems;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.FileItemModule.IFileItemBase> IEventBaseAutoGen.FileItems
        {
            get {  return this.FileItems; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.FileItemModule.FileItemBase> __collection__FileItems
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'FileItemBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __TestimonialsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.EventModule.EventBase, 
        	BusinessLogic_v3.Modules.TestimonialModule.TestimonialBase,
        	BusinessLogic_v3.Modules.TestimonialModule.ITestimonialBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.EventModule.EventBase, BusinessLogic_v3.Modules.TestimonialModule.TestimonialBase>
        {
            private BusinessLogic_v3.Modules.EventModule.EventBase _item = null;
            public __TestimonialsCollectionInfo(BusinessLogic_v3.Modules.EventModule.EventBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.TestimonialModule.TestimonialBase> Collection
            {
                get { return _item.__collection__Testimonials; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.EventModule.EventBase, BusinessLogic_v3.Modules.TestimonialModule.TestimonialBase>.SetLinkOnItem(BusinessLogic_v3.Modules.TestimonialModule.TestimonialBase item, BusinessLogic_v3.Modules.EventModule.EventBase value)
            {
                item.Event = value;
            }
        }
        
        private __TestimonialsCollectionInfo _Testimonials = null;
        public __TestimonialsCollectionInfo Testimonials
        {
            get
            {
                if (_Testimonials == null)
                    _Testimonials = new __TestimonialsCollectionInfo((BusinessLogic_v3.Modules.EventModule.EventBase)this);
                return _Testimonials;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.TestimonialModule.ITestimonialBase> IEventBaseAutoGen.Testimonials
        {
            get {  return this.Testimonials; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.TestimonialModule.TestimonialBase> __collection__Testimonials
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'TestimonialBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionManyToManyRightSide  
        
		public class __MembersCollectionInfo : DbCollectionManyToManyManager<
			BusinessLogic_v3.Modules.EventModule.EventBase,
			BusinessLogic_v3.Modules.MemberModule.MemberBase,
			BusinessLogic_v3.Modules.MemberModule.IMemberBase>,
            IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.EventModule.EventBase, BusinessLogic_v3.Modules.MemberModule.MemberBase>
        {
            private BusinessLogic_v3.Modules.EventModule.EventBase _item = null;
            public __MembersCollectionInfo(BusinessLogic_v3.Modules.EventModule.EventBase item)
                : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }


            public IEnumerable<BusinessLogic_v3.Modules.MemberModule.MemberBase> Collection
            {
                get { return _item.__collection__Members; }
            }

            #region IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.EventModule.EventBase,BusinessLogic_v3.Modules.MemberModule.MemberBase> Members

            public ICollectionManager<BusinessLogic_v3.Modules.EventModule.EventBase> GetLinkedCollectionForItem(BusinessLogic_v3.Modules.MemberModule.MemberBase item)
            {
                return item.Events;
                
            }


            #endregion
        }

        private __MembersCollectionInfo _Members = null;
        public __MembersCollectionInfo Members
        {
            get
            {
                if (_Members == null)
                    _Members = new __MembersCollectionInfo((BusinessLogic_v3.Modules.EventModule.EventBase)this);
                return _Members;
            }
        }
        
           
        ICollectionManager<BusinessLogic_v3.Modules.MemberModule.IMemberBase> IEventBaseAutoGen.Members
        {
            get {  return this.Members; }
        }
           

		protected virtual IEnumerable<BusinessLogic_v3.Modules.MemberModule.MemberBase> __collection__Members
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'MemberBase' is not marked as UsedInProject"); }
        }





        

    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
