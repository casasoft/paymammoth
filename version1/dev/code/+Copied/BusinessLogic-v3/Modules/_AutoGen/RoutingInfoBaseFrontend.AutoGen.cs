using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.RoutingInfoModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.RoutingInfoModule.RoutingInfoBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.RoutingInfoModule.IRoutingInfoBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.RoutingInfoModule.RoutingInfoBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.RoutingInfoModule.IRoutingInfoBase item)
        {
        	return BusinessLogic_v3.Frontend.RoutingInfoModule.RoutingInfoBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class RoutingInfoBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<RoutingInfoBaseFrontend, BusinessLogic_v3.Modules.RoutingInfoModule.IRoutingInfoBase>

    {
		
        
        protected RoutingInfoBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
