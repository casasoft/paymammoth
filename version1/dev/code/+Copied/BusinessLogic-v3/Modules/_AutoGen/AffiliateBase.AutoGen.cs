using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.AffiliateModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "Affiliate")]
    public abstract class AffiliateBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IAffiliateBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static AffiliateBaseFactory Factory
        {
            get
            {
                return AffiliateBaseFactory.Instance; 
            }
        }    
        /*
        public static AffiliateBase CreateNewItem()
        {
            return (AffiliateBase)Factory.CreateNewItem();
        }

		public static AffiliateBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<AffiliateBase, AffiliateBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static AffiliateBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static AffiliateBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<AffiliateBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<AffiliateBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<AffiliateBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _cssfilename;
        
        public virtual string CssFilename 
        {
        	get
        	{
        		return _cssfilename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_cssfilename,value);
        		_cssfilename = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _logofilename;
        
        public virtual string LogoFilename 
        {
        	get
        	{
        		return _logofilename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_logofilename,value);
        		_logofilename = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _username;
        
        public virtual string Username 
        {
        	get
        	{
        		return _username;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_username,value);
        		_username = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _password;
        
        public virtual string Password 
        {
        	get
        	{
        		return _password;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_password,value);
        		_password = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _activefrom;
        
        public virtual DateTime ActiveFrom 
        {
        	get
        	{
        		return _activefrom;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_activefrom,value);
        		_activefrom = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _affiliatecode;
        
        public virtual string AffiliateCode 
        {
        	get
        	{
        		return _affiliatecode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_affiliatecode,value);
        		_affiliatecode = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _referralcode;
        
        public virtual string ReferralCode 
        {
        	get
        	{
        		return _referralcode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_referralcode,value);
        		_referralcode = value;
        		
        	}
        }
        
/*
		public virtual long? LinkedContentPageNodeID
		{
		 	get 
		 	{
		 		return (this.LinkedContentPageNode != null ? (long?)this.LinkedContentPageNode.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ArticleModule.ArticleBase _linkedcontentpagenode;
        public virtual BusinessLogic_v3.Modules.ArticleModule.ArticleBase LinkedContentPageNode 
        {
        	get
        	{
        		return _linkedcontentpagenode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_linkedcontentpagenode,value);
        		_linkedcontentpagenode = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ArticleModule.IArticleBase BusinessLogic_v3.Modules._AutoGen.IAffiliateBaseAutoGen.LinkedContentPageNode 
        {
            get
            {
            	return this.LinkedContentPageNode;
            }
            set
            {
            	this.LinkedContentPageNode = (BusinessLogic_v3.Modules.ArticleModule.ArticleBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private double _commissionrate;
        
        public virtual double CommissionRate 
        {
        	get
        	{
        		return _commissionrate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_commissionrate,value);
        		_commissionrate = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _affiliateurl;
        
        public virtual string AffiliateUrl 
        {
        	get
        	{
        		return _affiliateurl;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_affiliateurl,value);
        		_affiliateurl = value;
        		
        	}
        }
        
/*
		public virtual long? LinkedCmsUserID
		{
		 	get 
		 	{
		 		return (this.LinkedCmsUser != null ? (long?)this.LinkedCmsUser.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase _linkedcmsuser;
        public virtual BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase LinkedCmsUser 
        {
        	get
        	{
        		return _linkedcmsuser;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_linkedcmsuser,value);
        		_linkedcmsuser = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBase BusinessLogic_v3.Modules._AutoGen.IAffiliateBaseAutoGen.LinkedCmsUser 
        {
            get
            {
            	return this.LinkedCmsUser;
            }
            set
            {
            	this.LinkedCmsUser = (BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private bool _haswhitelabellingfunctionality;
        
        public virtual bool HasWhiteLabellingFunctionality 
        {
        	get
        	{
        		return _haswhitelabellingfunctionality;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_haswhitelabellingfunctionality,value);
        		_haswhitelabellingfunctionality = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _affiliatewhitelabelbaseurl;
        
        public virtual string AffiliateWhiteLabelBaseUrl 
        {
        	get
        	{
        		return _affiliatewhitelabelbaseurl;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_affiliatewhitelabelbaseurl,value);
        		_affiliatewhitelabelbaseurl = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _mainpanelhtml;
        
        public virtual string MainPanelHtml 
        {
        	get
        	{
        		return _mainpanelhtml;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_mainpanelhtml,value);
        		_mainpanelhtml = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _activated;
        
        public virtual bool Activated 
        {
        	get
        	{
        		return _activated;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_activated,value);
        		_activated = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __AffiliatePaymentInfosCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase, 
        	BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.AffiliatePaymentInfoBase,
        	BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.IAffiliatePaymentInfoBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase, BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.AffiliatePaymentInfoBase>
        {
            private BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase _item = null;
            public __AffiliatePaymentInfosCollectionInfo(BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.AffiliatePaymentInfoBase> Collection
            {
                get { return _item.__collection__AffiliatePaymentInfos; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase, BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.AffiliatePaymentInfoBase>.SetLinkOnItem(BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.AffiliatePaymentInfoBase item, BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase value)
            {
                item.Affiliate = value;
            }
        }
        
        private __AffiliatePaymentInfosCollectionInfo _AffiliatePaymentInfos = null;
        public __AffiliatePaymentInfosCollectionInfo AffiliatePaymentInfos
        {
            get
            {
                if (_AffiliatePaymentInfos == null)
                    _AffiliatePaymentInfos = new __AffiliatePaymentInfosCollectionInfo((BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase)this);
                return _AffiliatePaymentInfos;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.IAffiliatePaymentInfoBase> IAffiliateBaseAutoGen.AffiliatePaymentInfos
        {
            get {  return this.AffiliatePaymentInfos; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.AffiliatePaymentInfoBase> __collection__AffiliatePaymentInfos
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'AffiliatePaymentInfoBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __MembersCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase, 
        	BusinessLogic_v3.Modules.MemberModule.MemberBase,
        	BusinessLogic_v3.Modules.MemberModule.IMemberBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase, BusinessLogic_v3.Modules.MemberModule.MemberBase>
        {
            private BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase _item = null;
            public __MembersCollectionInfo(BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.MemberModule.MemberBase> Collection
            {
                get { return _item.__collection__Members; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase, BusinessLogic_v3.Modules.MemberModule.MemberBase>.SetLinkOnItem(BusinessLogic_v3.Modules.MemberModule.MemberBase item, BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase value)
            {
                item.LinkedAffiliate = value;
            }
        }
        
        private __MembersCollectionInfo _Members = null;
        public __MembersCollectionInfo Members
        {
            get
            {
                if (_Members == null)
                    _Members = new __MembersCollectionInfo((BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase)this);
                return _Members;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.MemberModule.IMemberBase> IAffiliateBaseAutoGen.Members
        {
            get {  return this.Members; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.MemberModule.MemberBase> __collection__Members
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'MemberBase' is not marked as UsedInProject"); }
        }

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __OrdersCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase, 
        	BusinessLogic_v3.Modules.OrderModule.OrderBase,
        	BusinessLogic_v3.Modules.OrderModule.IOrderBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase, BusinessLogic_v3.Modules.OrderModule.OrderBase>
        {
            private BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase _item = null;
            public __OrdersCollectionInfo(BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.OrderModule.OrderBase> Collection
            {
                get { return _item.__collection__Orders; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase, BusinessLogic_v3.Modules.OrderModule.OrderBase>.SetLinkOnItem(BusinessLogic_v3.Modules.OrderModule.OrderBase item, BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase value)
            {
                item.LinkedAffiliate = value;
            }
        }
        
        private __OrdersCollectionInfo _Orders = null;
        public __OrdersCollectionInfo Orders
        {
            get
            {
                if (_Orders == null)
                    _Orders = new __OrdersCollectionInfo((BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase)this);
                return _Orders;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.OrderModule.IOrderBase> IAffiliateBaseAutoGen.Orders
        {
            get {  return this.Orders; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.OrderModule.OrderBase> __collection__Orders
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'OrderBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
