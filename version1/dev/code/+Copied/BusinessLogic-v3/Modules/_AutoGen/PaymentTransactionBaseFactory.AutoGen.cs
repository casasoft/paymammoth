using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.PaymentTransactionModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
   
    public abstract class PaymentTransactionBaseFactoryAutoGen  : BusinessLogic_v3.Classes.DB.BaseDbFactory<PaymentTransactionBase, IPaymentTransactionBase>, IPaymentTransactionBaseFactoryAutoGen
    
    {
    
        public PaymentTransactionBaseFactoryAutoGen()
        {
        	
            
        }
        
		static PaymentTransactionBaseFactoryAutoGen()
        {
            UsedInProject = false;
        }
        
        
        public new static PaymentTransactionBaseFactory Instance
        {
            get
            {
                return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<PaymentTransactionBaseFactory>(null);
            }
        }
        
		public IQueryOver<PaymentTransactionBase, PaymentTransactionBase> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
        	
        	var q = session.QueryOver<PaymentTransactionBase>();
            qParams.FillQueryOver(q);
        	postGetQueryOver(q, qParams);
            return q;
            
        } 
        
        
    }
}
