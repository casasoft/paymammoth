using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ImageSpecificSizeInfoModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfoBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfoBase item)
        {
        	return BusinessLogic_v3.Frontend.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ImageSpecificSizeInfoBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<ImageSpecificSizeInfoBaseFrontend, BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfoBase>

    {
		
        
        protected ImageSpecificSizeInfoBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
