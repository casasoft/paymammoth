using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using CS.General_v3.Classes.DbObjects.Collections;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using CS.General_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.ContentTagModule;                

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public abstract class ContentTagBaseAutoGen 
    : BusinessLogic_v3.Classes.DB.BaseDbObject, IContentTagBaseAutoGen
    
      
      
      
    
    
    		
    {
    
   
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	
        public static BusinessLogic_v3.Cms.ContentTagModule.ContentTagBaseCmsFactory CmsFactory
        {
            get { return BusinessLogic_v3.Cms.ContentTagModule.ContentTagBaseCmsFactory.Instance; }
        }
        
     	public static ContentTagBaseFactory Factory
        {
            get
            {
                return ContentTagBaseFactory.Instance; 
            }
        }    
        /*
        public static ContentTagBase CreateNewItem()
        {
            return (ContentTagBase)Factory.CreateNewItem();
        }

		public static ContentTagBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<ContentTagBase, ContentTagBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static ContentTagBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static ContentTagBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<ContentTagBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<ContentTagBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<ContentTagBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _tagname;
        public virtual string TagName 
        {
        	get
        	{
        		return _tagname;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_tagname,value);
        		_tagname = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _description;
        public virtual string Description 
        {
        	get
        	{
        		return _description;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_description,value);
        		_description = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private BusinessLogic_v3.Enums.ContentTagType _tagtype;
        public virtual BusinessLogic_v3.Enums.ContentTagType TagType 
        {
        	get
        	{
        		return _tagtype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_tagtype,value);
        		_tagtype = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _usedinproject;
        public virtual bool UsedInProject 
        {
        	get
        	{
        		return _usedinproject;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_usedinproject,value);
        		_usedinproject = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionManyToManyRightSide  
        
		public class __ArticlesCollectionInfo : DbCollectionManyToManyManager<
			BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase,
			BusinessLogic_v3.Modules.ArticleModule.ArticleBase,
			BusinessLogic_v3.Modules.ArticleModule.IArticleBase>,
            IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase, BusinessLogic_v3.Modules.ArticleModule.ArticleBase>
        {
            private BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase _item = null;
            public __ArticlesCollectionInfo(BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase item)
                : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }


            public IEnumerable<BusinessLogic_v3.Modules.ArticleModule.ArticleBase> Collection
            {
                get { return _item.__collection__Articles; }
            }

            #region IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase,BusinessLogic_v3.Modules.ArticleModule.ArticleBase> Members

            public ICollectionManager<BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase> GetLinkedCollectionForItem(BusinessLogic_v3.Modules.ArticleModule.ArticleBase item)
            {
                return item.ContentTags;
                
            }


            #endregion
        }

        private __ArticlesCollectionInfo _Articles = null;
        public __ArticlesCollectionInfo Articles
        {
            get
            {
                if (_Articles == null)
                    _Articles = new __ArticlesCollectionInfo((BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase)this);
                return _Articles;
            }
        }
        
           
        ICollectionManager<BusinessLogic_v3.Modules.ArticleModule.IArticleBase> IContentTagBaseAutoGen.Articles
        {
            get {  return this.Articles; }
        }
           

		protected virtual IEnumerable<BusinessLogic_v3.Modules.ArticleModule.ArticleBase> __collection__Articles
        {
            get { throw new InvalidOperationException("This must be defined in the final implementation"); }
        }





        
        //BaseClass_CollectionManyToManyRightSide  
        
		public class __ContentTextsCollectionInfo : DbCollectionManyToManyManager<
			BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase,
			BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase,
			BusinessLogic_v3.Modules.ContentTextModule.IContentTextBase>,
            IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase, BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase>
        {
            private BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase _item = null;
            public __ContentTextsCollectionInfo(BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase item)
                : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }


            public IEnumerable<BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase> Collection
            {
                get { return _item.__collection__ContentTexts; }
            }

            #region IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase,BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase> Members

            public ICollectionManager<BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase> GetLinkedCollectionForItem(BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase item)
            {
                return item.ContentTags;
                
            }


            #endregion
        }

        private __ContentTextsCollectionInfo _ContentTexts = null;
        public __ContentTextsCollectionInfo ContentTexts
        {
            get
            {
                if (_ContentTexts == null)
                    _ContentTexts = new __ContentTextsCollectionInfo((BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase)this);
                return _ContentTexts;
            }
        }
        
           
        ICollectionManager<BusinessLogic_v3.Modules.ContentTextModule.IContentTextBase> IContentTagBaseAutoGen.ContentTexts
        {
            get {  return this.ContentTexts; }
        }
           

		protected virtual IEnumerable<BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase> __collection__ContentTexts
        {
            get { throw new InvalidOperationException("This must be defined in the final implementation"); }
        }





        
        //BaseClass_CollectionManyToManyRightSide  
        
		public class __EmailTextsCollectionInfo : DbCollectionManyToManyManager<
			BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase,
			BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase,
			BusinessLogic_v3.Modules.EmailTextModule.IEmailTextBase>,
            IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase, BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase>
        {
            private BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase _item = null;
            public __EmailTextsCollectionInfo(BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase item)
                : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }


            public IEnumerable<BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase> Collection
            {
                get { return _item.__collection__EmailTexts; }
            }

            #region IDbCollectionManyToManyInfo<BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase,BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase> Members

            public ICollectionManager<BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase> GetLinkedCollectionForItem(BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase item)
            {
                return item.ContentTags;
                
            }


            #endregion
        }

        private __EmailTextsCollectionInfo _EmailTexts = null;
        public __EmailTextsCollectionInfo EmailTexts
        {
            get
            {
                if (_EmailTexts == null)
                    _EmailTexts = new __EmailTextsCollectionInfo((BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase)this);
                return _EmailTexts;
            }
        }
        
           
        ICollectionManager<BusinessLogic_v3.Modules.EmailTextModule.IEmailTextBase> IContentTagBaseAutoGen.EmailTexts
        {
            get {  return this.EmailTexts; }
        }
           

		protected virtual IEnumerable<BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase> __collection__EmailTexts
        {
            get { throw new InvalidOperationException("This must be defined in the final implementation"); }
        }





        

    
#region Multilingual
          


#endregion    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
