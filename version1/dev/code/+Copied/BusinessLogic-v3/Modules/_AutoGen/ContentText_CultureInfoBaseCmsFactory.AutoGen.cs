using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.ContentText_CultureInfoModule;
using BusinessLogic_v3.Cms.ContentText_CultureInfoModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class ContentText_CultureInfoBaseCmsFactory_AutoGen : CmsFactoryBase<ContentText_CultureInfoBaseCmsInfo, ContentText_CultureInfoBase>
    {
       
       public new static ContentText_CultureInfoBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ContentText_CultureInfoBaseCmsFactory)CmsFactoryBase<ContentText_CultureInfoBaseCmsInfo, ContentText_CultureInfoBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = ContentText_CultureInfoBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ContentText_CultureInfo.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ContentText_CultureInfo";

            this.QueryStringParamID = "ContentText_CultureInfoId";

            cmsInfo.TitlePlural = "Content Text _ Culture Infos";

            cmsInfo.TitleSingular =  "Content Text _ Culture Info";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ContentText_CultureInfoBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "ContentText_CultureInfo/";
			UsedInProject = ContentText_CultureInfoBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
