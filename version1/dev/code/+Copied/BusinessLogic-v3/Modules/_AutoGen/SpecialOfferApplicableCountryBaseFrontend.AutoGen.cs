using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.SpecialOfferApplicableCountryModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.SpecialOfferApplicableCountryModule.SpecialOfferApplicableCountryBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.SpecialOfferApplicableCountryModule.ISpecialOfferApplicableCountryBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.SpecialOfferApplicableCountryModule.SpecialOfferApplicableCountryBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.SpecialOfferApplicableCountryModule.ISpecialOfferApplicableCountryBase item)
        {
        	return BusinessLogic_v3.Frontend.SpecialOfferApplicableCountryModule.SpecialOfferApplicableCountryBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class SpecialOfferApplicableCountryBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<SpecialOfferApplicableCountryBaseFrontend, BusinessLogic_v3.Modules.SpecialOfferApplicableCountryModule.ISpecialOfferApplicableCountryBase>

    {
		
        
        protected SpecialOfferApplicableCountryBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
