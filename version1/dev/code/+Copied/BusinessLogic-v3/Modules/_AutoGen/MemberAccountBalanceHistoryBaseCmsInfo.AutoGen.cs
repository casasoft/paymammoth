using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule;
using BusinessLogic_v3.Cms.MemberAccountBalanceHistoryModule;
using BusinessLogic_v3.Frontend.MemberAccountBalanceHistoryModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class MemberAccountBalanceHistoryBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBase>
    {
		public MemberAccountBalanceHistoryBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBase dbItem)
            : base(BusinessLogic_v3.Cms.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new MemberAccountBalanceHistoryBaseFrontend FrontendItem
        {
            get { return (MemberAccountBalanceHistoryBaseFrontend)base.FrontendItem; }

        }
        public new MemberAccountBalanceHistoryBase DbItem
        {
            get
            {
                return (MemberAccountBalanceHistoryBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Member { get; protected set; }

        public CmsPropertyInfo BalanceUpdate { get; protected set; }

        public CmsPropertyInfo Date { get; protected set; }

        public CmsPropertyInfo OrderLink { get; protected set; }

        public CmsPropertyInfo UpdateType { get; protected set; }

        public CmsPropertyInfo BalanceTotal { get; protected set; }

        public CmsPropertyInfo Comments { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
