using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.AdvertHitModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "AdvertHit")]
    public abstract class AdvertHitBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IAdvertHitBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static AdvertHitBaseFactory Factory
        {
            get
            {
                return AdvertHitBaseFactory.Instance; 
            }
        }    
        /*
        public static AdvertHitBase CreateNewItem()
        {
            return (AdvertHitBase)Factory.CreateNewItem();
        }

		public static AdvertHitBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<AdvertHitBase, AdvertHitBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static AdvertHitBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static AdvertHitBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<AdvertHitBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<AdvertHitBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<AdvertHitBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private BusinessLogic_v3.Enums.ADVERT_HIT_TYPE _hittype;
        
        public virtual BusinessLogic_v3.Enums.ADVERT_HIT_TYPE HitType 
        {
        	get
        	{
        		return _hittype;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_hittype,value);
        		_hittype = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _userip;
        
        public virtual string UserIP 
        {
        	get
        	{
        		return _userip;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_userip,value);
        		_userip = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _datetime;
        
        public virtual DateTime DateTime 
        {
        	get
        	{
        		return _datetime;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_datetime,value);
        		_datetime = value;
        		
        	}
        }
        
/*
		public virtual long? AdvertID
		{
		 	get 
		 	{
		 		return (this.Advert != null ? (long?)this.Advert.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.AdvertModule.AdvertBase _advert;
        public virtual BusinessLogic_v3.Modules.AdvertModule.AdvertBase Advert 
        {
        	get
        	{
        		return _advert;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_advert,value);
        		_advert = value;
        	}
        }
            
        BusinessLogic_v3.Modules.AdvertModule.IAdvertBase BusinessLogic_v3.Modules._AutoGen.IAdvertHitBaseAutoGen.Advert 
        {
            get
            {
            	return this.Advert;
            }
            set
            {
            	this.Advert = (BusinessLogic_v3.Modules.AdvertModule.AdvertBase) value;
            }
        }
            
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
