using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.VacancyModule;
using BusinessLogic_v3.Cms.VacancyModule;
using BusinessLogic_v3.Frontend.VacancyModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class VacancyBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.VacancyModule.VacancyBase>
    {
		public VacancyBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.VacancyModule.VacancyBase dbItem)
            : base(BusinessLogic_v3.Cms.VacancyModule.VacancyBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new VacancyBaseFrontend FrontendItem
        {
            get { return (VacancyBaseFrontend)base.FrontendItem; }

        }
        public new VacancyBase DbItem
        {
            get
            {
                return (VacancyBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }

        public CmsPropertyInfo RefCode { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo ContactForms { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.ContactForms = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.VacancyModule.VacancyBase>.GetPropertyBySelector(item => item.ContactForms),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.GetPropertyBySelector(item => item.Vacancy)));


			base.initBasicFields();
          
        }

    }
}
