using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ContactFormModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.ContactFormModule.ContactFormBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.ContactFormModule.IContactFormBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.ContactFormModule.ContactFormBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.ContactFormModule.IContactFormBase item)
        {
        	return BusinessLogic_v3.Frontend.ContactFormModule.ContactFormBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ContactFormBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<ContactFormBaseFrontend, BusinessLogic_v3.Modules.ContactFormModule.IContactFormBase>

    {
		
        
        protected ContactFormBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
