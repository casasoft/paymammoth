using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ShippingRatePriceModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IShippingRatePriceBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.ShippingRatesGroupModule.IShippingRatesGroupBase ShippingRatesGroup {get; set; }

		//Iproperty_normal
        double FromWeight { get; set; }
		//Iproperty_normal
        double ToWeight { get; set; }
		//Iproperty_normal
        double Price { get; set; }
		//Iproperty_normal
        BusinessLogic_v3.Enums.ShippingPricingType PricingType { get; set; }
   

// [interface_base_collections]

 
		

    	
    	
      
      

    }
}
