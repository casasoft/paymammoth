using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.SpecialOfferModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.SpecialOfferModule.SpecialOfferBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.SpecialOfferModule.ISpecialOfferBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.SpecialOfferModule.SpecialOfferBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.SpecialOfferModule.ISpecialOfferBase item)
        {
        	return BusinessLogic_v3.Frontend.SpecialOfferModule.SpecialOfferBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class SpecialOfferBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<SpecialOfferBaseFrontend, BusinessLogic_v3.Modules.SpecialOfferModule.ISpecialOfferBase>

    {
		
        
        protected SpecialOfferBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
