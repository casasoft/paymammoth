using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.TestimonialModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.TestimonialModule.TestimonialBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.TestimonialModule.ITestimonialBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.TestimonialModule.TestimonialBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.TestimonialModule.ITestimonialBase item)
        {
        	return BusinessLogic_v3.Frontend.TestimonialModule.TestimonialBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class TestimonialBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<TestimonialBaseFrontend, BusinessLogic_v3.Modules.TestimonialModule.ITestimonialBase>

    {
		
        
        protected TestimonialBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
