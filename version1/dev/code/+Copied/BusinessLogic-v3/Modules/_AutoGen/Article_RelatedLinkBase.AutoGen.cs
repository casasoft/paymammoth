using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.Article_RelatedLinkModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "Article_RelatedLink")]
    public abstract class Article_RelatedLinkBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IArticle_RelatedLinkBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static Article_RelatedLinkBaseFactory Factory
        {
            get
            {
                return Article_RelatedLinkBaseFactory.Instance; 
            }
        }    
        /*
        public static Article_RelatedLinkBase CreateNewItem()
        {
            return (Article_RelatedLinkBase)Factory.CreateNewItem();
        }

		public static Article_RelatedLinkBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<Article_RelatedLinkBase, Article_RelatedLinkBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static Article_RelatedLinkBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static Article_RelatedLinkBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<Article_RelatedLinkBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<Article_RelatedLinkBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<Article_RelatedLinkBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? ParentPageID
		{
		 	get 
		 	{
		 		return (this.ParentPage != null ? (long?)this.ParentPage.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ArticleModule.ArticleBase _parentpage;
        public virtual BusinessLogic_v3.Modules.ArticleModule.ArticleBase ParentPage 
        {
        	get
        	{
        		return _parentpage;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_parentpage,value);
        		_parentpage = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ArticleModule.IArticleBase BusinessLogic_v3.Modules._AutoGen.IArticle_RelatedLinkBaseAutoGen.ParentPage 
        {
            get
            {
            	return this.ParentPage;
            }
            set
            {
            	this.ParentPage = (BusinessLogic_v3.Modules.ArticleModule.ArticleBase) value;
            }
        }
            
/*
		public virtual long? RelatedPageID
		{
		 	get 
		 	{
		 		return (this.RelatedPage != null ? (long?)this.RelatedPage.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ArticleModule.ArticleBase _relatedpage;
        public virtual BusinessLogic_v3.Modules.ArticleModule.ArticleBase RelatedPage 
        {
        	get
        	{
        		return _relatedpage;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_relatedpage,value);
        		_relatedpage = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ArticleModule.IArticleBase BusinessLogic_v3.Modules._AutoGen.IArticle_RelatedLinkBaseAutoGen.RelatedPage 
        {
            get
            {
            	return this.RelatedPage;
            }
            set
            {
            	this.RelatedPage = (BusinessLogic_v3.Modules.ArticleModule.ArticleBase) value;
            }
        }
            
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
