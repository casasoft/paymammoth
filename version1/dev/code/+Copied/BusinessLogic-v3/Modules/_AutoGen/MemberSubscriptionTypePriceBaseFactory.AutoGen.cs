using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule;

//DbObjectBuilder v8

namespace BusinessLogic_v3.Modules._AutoGen
{
   
    public abstract class MemberSubscriptionTypePriceBaseFactoryAutoGen  : BusinessLogic_v3.Classes.DbObjects.Factories.BaseDbFactory<MemberSubscriptionTypePriceBase, IMemberSubscriptionTypePriceBase>, IMemberSubscriptionTypePriceBaseFactoryAutoGen
    
    {
    
        public MemberSubscriptionTypePriceBaseFactoryAutoGen()
        {
        	
            
        }
        
		static MemberSubscriptionTypePriceBaseFactoryAutoGen()
        {
            
        }
        
        
        public new static MemberSubscriptionTypePriceBaseFactory Instance
        {
            get
            {
                return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<MemberSubscriptionTypePriceBaseFactory>(null);
            }
        }
        
		public IQueryOver<MemberSubscriptionTypePriceBase, MemberSubscriptionTypePriceBase> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
        	ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<MemberSubscriptionTypePriceBase>();
            qParams.FillQueryOver(q, this);
        	postGetQueryOver(q, qParams);
            return q;
            
        } 
        
        
    }
}
