using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.EventSessionPeriodDayModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.EventSessionPeriodDayModule.EventSessionPeriodDayBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.EventSessionPeriodDayModule.IEventSessionPeriodDayBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.EventSessionPeriodDayModule.EventSessionPeriodDayBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.EventSessionPeriodDayModule.IEventSessionPeriodDayBase item)
        {
        	return BusinessLogic_v3.Frontend.EventSessionPeriodDayModule.EventSessionPeriodDayBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class EventSessionPeriodDayBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<EventSessionPeriodDayBaseFrontend, BusinessLogic_v3.Modules.EventSessionPeriodDayModule.IEventSessionPeriodDayBase>

    {
		
        
        protected EventSessionPeriodDayBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
