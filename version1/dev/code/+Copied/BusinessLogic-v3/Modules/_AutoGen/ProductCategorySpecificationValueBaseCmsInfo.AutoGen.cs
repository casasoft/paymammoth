using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule;
using BusinessLogic_v3.Cms.ProductCategorySpecificationValueModule;
using BusinessLogic_v3.Frontend.ProductCategorySpecificationValueModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ProductCategorySpecificationValueBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueBase>
    {
		public ProductCategorySpecificationValueBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueBase dbItem)
            : base(BusinessLogic_v3.Cms.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ProductCategorySpecificationValueBaseFrontend FrontendItem
        {
            get { return (ProductCategorySpecificationValueBaseFrontend)base.FrontendItem; }

        }
        public new ProductCategorySpecificationValueBase DbItem
        {
            get
            {
                return (ProductCategorySpecificationValueBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Value { get; protected set; }

        public CmsPropertyInfo Product { get; protected set; }

        public CmsPropertyInfo Title { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
