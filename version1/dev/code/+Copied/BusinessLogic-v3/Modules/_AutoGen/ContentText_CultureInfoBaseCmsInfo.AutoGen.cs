using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ContentText_CultureInfoModule;
using BusinessLogic_v3.Cms.ContentText_CultureInfoModule;
using BusinessLogic_v3.Frontend.ContentText_CultureInfoModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ContentText_CultureInfoBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoBase>
    {
		public ContentText_CultureInfoBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoBase dbItem)
            : base(BusinessLogic_v3.Cms.ContentText_CultureInfoModule.ContentText_CultureInfoBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ContentText_CultureInfoBaseFrontend FrontendItem
        {
            get { return (ContentText_CultureInfoBaseFrontend)base.FrontendItem; }

        }
        public new ContentText_CultureInfoBase DbItem
        {
            get
            {
                return (ContentText_CultureInfoBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo CultureInfo { get; protected set; }

        public CmsPropertyInfo ContentText { get; protected set; }

        public CmsPropertyInfo Content { get; protected set; }

        public CmsPropertyInfo ImageAlternateText { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
