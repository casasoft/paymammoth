using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.ShoppingCartItemModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "ShoppingCartItem")]
    public abstract class ShoppingCartItemBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IShoppingCartItemBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static ShoppingCartItemBaseFactory Factory
        {
            get
            {
                return ShoppingCartItemBaseFactory.Instance; 
            }
        }    
        /*
        public static ShoppingCartItemBase CreateNewItem()
        {
            return (ShoppingCartItemBase)Factory.CreateNewItem();
        }

		public static ShoppingCartItemBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<ShoppingCartItemBase, ShoppingCartItemBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static ShoppingCartItemBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static ShoppingCartItemBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<ShoppingCartItemBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<ShoppingCartItemBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<ShoppingCartItemBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private int _quantity;
        
        public virtual int Quantity 
        {
        	get
        	{
        		return _quantity;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_quantity,value);
        		_quantity = value;
        		
        	}
        }
        
/*
		public virtual long? ShoppingCartID
		{
		 	get 
		 	{
		 		return (this.ShoppingCart != null ? (long?)this.ShoppingCart.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase _shoppingcart;
        public virtual BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase ShoppingCart 
        {
        	get
        	{
        		return _shoppingcart;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_shoppingcart,value);
        		_shoppingcart = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ShoppingCartModule.IShoppingCartBase BusinessLogic_v3.Modules._AutoGen.IShoppingCartItemBaseAutoGen.ShoppingCart 
        {
            get
            {
            	return this.ShoppingCart;
            }
            set
            {
            	this.ShoppingCart = (BusinessLogic_v3.Modules.ShoppingCartModule.ShoppingCartBase) value;
            }
        }
            
/*
		public virtual long? LinkedProductVariationID
		{
		 	get 
		 	{
		 		return (this.LinkedProductVariation != null ? (long?)this.LinkedProductVariation.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase _linkedproductvariation;
        public virtual BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase LinkedProductVariation 
        {
        	get
        	{
        		return _linkedproductvariation;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_linkedproductvariation,value);
        		_linkedproductvariation = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ProductVariationModule.IProductVariationBase BusinessLogic_v3.Modules._AutoGen.IShoppingCartItemBaseAutoGen.LinkedProductVariation 
        {
            get
            {
            	return this.LinkedProductVariation;
            }
            set
            {
            	this.LinkedProductVariation = (BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase) value;
            }
        }
            
/*
		public virtual long? LinkedSubscriptionTypePriceID
		{
		 	get 
		 	{
		 		return (this.LinkedSubscriptionTypePrice != null ? (long?)this.LinkedSubscriptionTypePrice.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase _linkedsubscriptiontypeprice;
        public virtual BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase LinkedSubscriptionTypePrice 
        {
        	get
        	{
        		return _linkedsubscriptiontypeprice;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_linkedsubscriptiontypeprice,value);
        		_linkedsubscriptiontypeprice = value;
        	}
        }
            
        BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.IMemberSubscriptionTypePriceBase BusinessLogic_v3.Modules._AutoGen.IShoppingCartItemBaseAutoGen.LinkedSubscriptionTypePrice 
        {
            get
            {
            	return this.LinkedSubscriptionTypePrice;
            }
            set
            {
            	this.LinkedSubscriptionTypePrice = (BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private bool _autorenewsubscriptiontypeprice;
        
        public virtual bool AutoRenewSubscriptionTypePrice 
        {
        	get
        	{
        		return _autorenewsubscriptiontypeprice;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_autorenewsubscriptiontypeprice,value);
        		_autorenewsubscriptiontypeprice = value;
        		
        	}
        }
        
/*
		public virtual long? OrderLinkWhenConvertedID
		{
		 	get 
		 	{
		 		return (this.OrderLinkWhenConverted != null ? (long?)this.OrderLinkWhenConverted.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.OrderModule.OrderBase _orderlinkwhenconverted;
        public virtual BusinessLogic_v3.Modules.OrderModule.OrderBase OrderLinkWhenConverted 
        {
        	get
        	{
        		return _orderlinkwhenconverted;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_orderlinkwhenconverted,value);
        		_orderlinkwhenconverted = value;
        	}
        }
            
        BusinessLogic_v3.Modules.OrderModule.IOrderBase BusinessLogic_v3.Modules._AutoGen.IShoppingCartItemBaseAutoGen.OrderLinkWhenConverted 
        {
            get
            {
            	return this.OrderLinkWhenConverted;
            }
            set
            {
            	this.OrderLinkWhenConverted = (BusinessLogic_v3.Modules.OrderModule.OrderBase) value;
            }
        }
            
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
