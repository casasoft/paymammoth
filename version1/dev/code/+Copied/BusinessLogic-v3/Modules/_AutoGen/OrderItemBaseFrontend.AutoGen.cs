using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.OrderItemModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.OrderItemModule.OrderItemBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.OrderItemModule.IOrderItemBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.OrderItemModule.OrderItemBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.OrderItemModule.IOrderItemBase item)
        {
        	return BusinessLogic_v3.Frontend.OrderItemModule.OrderItemBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class OrderItemBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<OrderItemBaseFrontend, BusinessLogic_v3.Modules.OrderItemModule.IOrderItemBase>

    {
		
        
        protected OrderItemBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
