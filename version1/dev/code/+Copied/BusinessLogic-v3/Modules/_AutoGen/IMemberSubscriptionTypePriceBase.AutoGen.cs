using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface IMemberSubscriptionTypePriceBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        int DurationInDays { get; set; }
		//Iproperty_normal
        string Title { get; set; }
		//Iproperty_normal
        string Description { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.MemberSubscriptionTypeModule.IMemberSubscriptionTypeBase MemberSubscriptionType {get; set; }

		//Iproperty_normal
        int? Notification1BeforeInDays { get; set; }
		//Iproperty_normal
        int? Notification2BeforeInDays { get; set; }
		//Iproperty_normal
        double Price { get; set; }
   

// [interface_base_collections]

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.MemberSubscriptionLinkModule.IMemberSubscriptionLinkBase> MemberSubscriptionLinks { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.OrderItemModule.IOrderItemBase> OrderItems { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.ShoppingCartItemModule.IShoppingCartItemBase> ShoppingCartItems { get; }

 
		

    	
    	
      
      

    }
}
