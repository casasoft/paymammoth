using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.MeasurementUnitModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.MeasurementUnitModule.MeasurementUnitBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.MeasurementUnitModule.IMeasurementUnitBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.MeasurementUnitModule.MeasurementUnitBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.MeasurementUnitModule.IMeasurementUnitBase item)
        {
        	return BusinessLogic_v3.Frontend.MeasurementUnitModule.MeasurementUnitBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class MeasurementUnitBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<MeasurementUnitBaseFrontend, BusinessLogic_v3.Modules.MeasurementUnitModule.IMeasurementUnitBase>

    {
		
        
        protected MeasurementUnitBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
