using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.BannerModule;
using BusinessLogic_v3.Cms.BannerModule;
using BusinessLogic_v3.Frontend.BannerModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class BannerBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.BannerModule.BannerBase>
    {
		public BannerBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.BannerModule.BannerBase dbItem)
            : base(BusinessLogic_v3.Cms.BannerModule.BannerBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new BannerBaseFrontend FrontendItem
        {
            get { return (BannerBaseFrontend)base.FrontendItem; }

        }
        public new BannerBase DbItem
        {
            get
            {
                return (BannerBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Identifier { get; protected set; }

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo MediaItemFilename { get; protected set; }

        public CmsPropertyInfo Link { get; protected set; }

        public CmsPropertyInfo DurationMS { get; protected set; }

        public CmsPropertyInfo HrefTarget { get; protected set; }

        public CmsPropertyInfo SlideshowDelaySec { get; protected set; }

        public CmsPropertyInfo HTMLDescription { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
