using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.ArticleMediaItemModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.ArticleMediaItemModule.ArticleMediaItemBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.ArticleMediaItemModule.IArticleMediaItemBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.ArticleMediaItemModule.ArticleMediaItemBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.ArticleMediaItemModule.IArticleMediaItemBase item)
        {
        	return BusinessLogic_v3.Frontend.ArticleMediaItemModule.ArticleMediaItemBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ArticleMediaItemBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<ArticleMediaItemBaseFrontend, BusinessLogic_v3.Modules.ArticleMediaItemModule.IArticleMediaItemBase>

    {
		
        
        protected ArticleMediaItemBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
