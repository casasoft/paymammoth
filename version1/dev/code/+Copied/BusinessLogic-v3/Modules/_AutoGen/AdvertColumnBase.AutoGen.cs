using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.AdvertColumnModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "AdvertColumn")]
    public abstract class AdvertColumnBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IAdvertColumnBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static AdvertColumnBaseFactory Factory
        {
            get
            {
                return AdvertColumnBaseFactory.Instance; 
            }
        }    
        /*
        public static AdvertColumnBase CreateNewItem()
        {
            return (AdvertColumnBase)Factory.CreateNewItem();
        }

		public static AdvertColumnBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<AdvertColumnBase, AdvertColumnBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static AdvertColumnBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static AdvertColumnBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<AdvertColumnBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<AdvertColumnBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<AdvertColumnBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _identifier;
        
        public virtual string Identifier 
        {
        	get
        	{
        		return _identifier;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_identifier,value);
        		_identifier = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __AdvertSlotsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.AdvertColumnModule.AdvertColumnBase, 
        	BusinessLogic_v3.Modules.AdvertSlotModule.AdvertSlotBase,
        	BusinessLogic_v3.Modules.AdvertSlotModule.IAdvertSlotBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.AdvertColumnModule.AdvertColumnBase, BusinessLogic_v3.Modules.AdvertSlotModule.AdvertSlotBase>
        {
            private BusinessLogic_v3.Modules.AdvertColumnModule.AdvertColumnBase _item = null;
            public __AdvertSlotsCollectionInfo(BusinessLogic_v3.Modules.AdvertColumnModule.AdvertColumnBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.AdvertSlotModule.AdvertSlotBase> Collection
            {
                get { return _item.__collection__AdvertSlots; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.AdvertColumnModule.AdvertColumnBase, BusinessLogic_v3.Modules.AdvertSlotModule.AdvertSlotBase>.SetLinkOnItem(BusinessLogic_v3.Modules.AdvertSlotModule.AdvertSlotBase item, BusinessLogic_v3.Modules.AdvertColumnModule.AdvertColumnBase value)
            {
                item.AdvertColumn = value;
            }
        }
        
        private __AdvertSlotsCollectionInfo _AdvertSlots = null;
        public __AdvertSlotsCollectionInfo AdvertSlots
        {
            get
            {
                if (_AdvertSlots == null)
                    _AdvertSlots = new __AdvertSlotsCollectionInfo((BusinessLogic_v3.Modules.AdvertColumnModule.AdvertColumnBase)this);
                return _AdvertSlots;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.AdvertSlotModule.IAdvertSlotBase> IAdvertColumnBaseAutoGen.AdvertSlots
        {
            get {  return this.AdvertSlots; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.AdvertSlotModule.AdvertSlotBase> __collection__AdvertSlots
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'AdvertSlotBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
