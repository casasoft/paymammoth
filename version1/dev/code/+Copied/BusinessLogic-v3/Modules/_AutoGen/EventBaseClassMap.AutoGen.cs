using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Modules.EventModule;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;

namespace BusinessLogic_v3.Modules._AutoGen
{
    
    public abstract class EventBaseMap_AutoGen<TData> : BusinessLogic_v3.Classes.NHibernateClasses.Mappings.BaseClassMap<TData>
    	where TData : EventBase
    {
        public EventBaseMap_AutoGen()
        {
            
        }
        
        


// [baseclassmap_properties]

//BaseClassMap_Property
             
        protected virtual void TitleMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void DescriptionMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void StartDateMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void EndDateMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void ImageFilenameMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_PropertyLinkedObject

        protected virtual void CategoryMappingInfo(ICollectionMapInfo mapInfo)
        {
        
            mapInfo.CollectionType = BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_TYPE.Set;
            
            mapInfo.CascadeType.Clear();
            
            mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
            mapInfo.Inverse = false;
            mapInfo.ColumnName = "CategoryId";   
            
            mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
           
        }
        
        
//BaseClassMap_Property
             
        protected virtual void SubTitleMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void PriceMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void IdentifierMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void SessionsMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void ClassSizeMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void DatesMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void DurationMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void ShortDescriptionMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void LocationMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void SummaryMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void TimeMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        



// [baseclassmap_collections]

//BaseClassMap_Collection_OneToMany

        protected virtual void ContactFormsMappingInfo(ICollectionMapInfo mapInfo)
        {
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.SaveUpdate;
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.None;
                
           mapInfo.CascadeType.SetAll();     
           if (mapInfo.LazyMode == null)
           {
               if (true)
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
               else
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.NoLazy;
           }                
           if (!mapInfo.Inverse.HasValue) mapInfo.Inverse = true;
           if (string.IsNullOrEmpty(mapInfo.ColumnName)) mapInfo.ColumnName = "EventID";
           mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
            
        }
        
        
//BaseClassMap_Collection_OneToMany

        protected virtual void EventMediaItemsMappingInfo(ICollectionMapInfo mapInfo)
        {
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.SaveUpdate;
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.None;
                
           mapInfo.CascadeType.SetAll();     
           if (mapInfo.LazyMode == null)
           {
               if (true)
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
               else
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.NoLazy;
           }                
           if (!mapInfo.Inverse.HasValue) mapInfo.Inverse = true;
           if (string.IsNullOrEmpty(mapInfo.ColumnName)) mapInfo.ColumnName = "EventID";
           mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
            
        }
        
        
//BaseClassMap_Collection_OneToMany

        protected virtual void EventSessionsMappingInfo(ICollectionMapInfo mapInfo)
        {
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.SaveUpdate;
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.None;
                
           mapInfo.CascadeType.SetAll();     
           if (mapInfo.LazyMode == null)
           {
               if (true)
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
               else
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.NoLazy;
           }                
           if (!mapInfo.Inverse.HasValue) mapInfo.Inverse = true;
           if (string.IsNullOrEmpty(mapInfo.ColumnName)) mapInfo.ColumnName = "EventID";
           mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
            
        }
        
        
//BaseClassMap_Collection_OneToMany

        protected virtual void FileItemsMappingInfo(ICollectionMapInfo mapInfo)
        {
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.SaveUpdate;
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.None;
                
           mapInfo.CascadeType.SetAll();     
           if (mapInfo.LazyMode == null)
           {
               if (true)
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
               else
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.NoLazy;
           }                
           if (!mapInfo.Inverse.HasValue) mapInfo.Inverse = true;
           if (string.IsNullOrEmpty(mapInfo.ColumnName)) mapInfo.ColumnName = "EventID";
           mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
            
        }
        
        
//BaseClassMap_Collection_OneToMany

        protected virtual void TestimonialsMappingInfo(ICollectionMapInfo mapInfo)
        {
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.SaveUpdate;
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.None;
                
           mapInfo.CascadeType.SetAll();     
           if (mapInfo.LazyMode == null)
           {
               if (true)
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
               else
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.NoLazy;
           }                
           if (!mapInfo.Inverse.HasValue) mapInfo.Inverse = true;
           if (string.IsNullOrEmpty(mapInfo.ColumnName)) mapInfo.ColumnName = "EventID";
           mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
            
        }
        
        
//BaseClassMap_Collection_ManyToManyRight

        protected virtual void MembersMappingInfo(ICollectionMapInfo mapInfo)
        {
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.SaveUpdate;
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.None;
           // mapInfo.RelationshipType = Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.ManyToMany;               
           if (string.IsNullOrEmpty(mapInfo.ManyToManyTableName )) mapInfo.ManyToManyTableName = "MembersEventsTable";
           if (mapInfo.LazyMode == null)
           {
               if (true)
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
               else
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.NoLazy;
           }  
           
           if (!mapInfo.Inverse.HasValue) mapInfo.Inverse = true;
           if (string.IsNullOrEmpty(mapInfo.ParentKey)) mapInfo.ParentKey = "EventID";
           if (string.IsNullOrEmpty(mapInfo.ChildKey )) mapInfo.ChildKey = "MemberID";
            
        }
     
     
            
        
        protected override void initMappings()
        {
           
            
//[BaseClassMap_InitProperties]

//[BaseClassMap_InitCollections]                
            
            base.initMappings();
        }
        
        
        
        
        
    }
   
}
