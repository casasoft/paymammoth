using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.CategoryModule;

//BaseClass.AutoGen

namespace BusinessLogic_v3.Modules._AutoGen
{
	
    public interface ICategoryBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//IProperty_Multilingual
	   string TitleSingular { get; set ;}
		//IProperty_Multilingual
	   string TitleSingular_Search { get; set ;}
		//IProperty_Multilingual
	   string TitlePlural { get; set ;}
		//IProperty_Multilingual
	   string TitlePlural_Search { get; set ;}
		//IProperty_Multilingual
	   string TitleSEO { get; set ;}
		//IProperty_Multilingual
	   string Description { get; set ;}
		//IProperty_Multilingual
	   string MetaKeywords { get; set ;}
		//IProperty_Multilingual
	   string MetaDescription { get; set ;}
		//Iproperty_normal
        bool CanDelete { get; set; }
		//Iproperty_normal
        string Identifier { get; set; }
		//Iproperty_normal
        bool ComingSoon { get; set; }
		//Iproperty_normal
        int PriorityAll { get; set; }
		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.BrandModule.IBrandBase Brand {get; set; }

		//Iproperty_normal
        string ImportReference { get; set; }
		//Iproperty_normal
        bool DontShowInNavigation { get; set; }
		//Iproperty_normal
        string IconFilename { get; set; }
		//Iproperty_normal
        bool OmitChildrenInNavigation { get; set; }
		//Iproperty_normal
        string MetaTitle { get; set; }
		//Iproperty_normal
        bool ConsiderAsRootNode { get; set; }
		//Iproperty_normal
        string SubTitle { get; set; }
   

// [interface_base_collections]

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.BackgroundImageModule.IBackgroundImageBase> BackgroundImages { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.Category_CultureInfoModule.ICategory_CultureInfoBase> Category_CultureInfos { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLinkBase> ChildCategoryLinks { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLinkBase> ParentCategoryLinks { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.CategoryFeatureModule.ICategoryFeatureBase> CategoryFeatures { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.CategoryImageModule.ICategoryImageBase> CategoryImages { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.CategorySpecificationModule.ICategorySpecificationBase> CategorySpecifications { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.ClassifiedModule.IClassifiedBase> Classifieds { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.EventModule.IEventBase> Events { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.EventBasicModule.IEventBasicBase> EventBasics { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.EventSubmissionModule.IEventSubmissionBase> EventSubmissions { get; }

        //IBaseClass_CollectionOneToManyLeftSide
        ICollectionManager<BusinessLogic_v3.Modules.ProductCategoryModule.IProductCategoryBase> ProductLinks { get; }

        //IBaseClass_CollectionManyToManyRightSide  
        
        ICollectionManager<BusinessLogic_v3.Modules.MemberModule.IMemberBase> SubscribedMembers { get; }
        
        
        //IBaseClass_CollectionManyToManyRightSide  
        
        ICollectionManager<BusinessLogic_v3.Modules.SpecialOfferModule.ISpecialOfferBase> ApplicableSpecialOffers { get; }
        
        
 
		

    	
    	
      
      

    }
}
