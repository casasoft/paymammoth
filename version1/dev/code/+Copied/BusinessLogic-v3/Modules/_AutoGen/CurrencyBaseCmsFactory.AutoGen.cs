using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.CurrencyModule;
using BusinessLogic_v3.Cms.CurrencyModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class CurrencyBaseCmsFactory_AutoGen : CmsFactoryBase<CurrencyBaseCmsInfo, CurrencyBase>
    {
       
       public new static CurrencyBaseCmsFactory Instance
	    {
	         get
	         {
                 return (CurrencyBaseCmsFactory)CmsFactoryBase<CurrencyBaseCmsInfo, CurrencyBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = CurrencyBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Currency.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Currency";

            this.QueryStringParamID = "CurrencyId";

            cmsInfo.TitlePlural = "Currencies";

            cmsInfo.TitleSingular =  "Currency";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public CurrencyBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "Currency/";
			UsedInProject = CurrencyBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
