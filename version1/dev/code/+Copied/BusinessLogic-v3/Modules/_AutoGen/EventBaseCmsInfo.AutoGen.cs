using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.EventModule;
using BusinessLogic_v3.Cms.EventModule;
using BusinessLogic_v3.Frontend.EventModule;
namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class EventBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.EventModule.EventBase>
    {
		public EventBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.EventModule.EventBase dbItem)
            : base(BusinessLogic_v3.Cms.EventModule.EventBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new EventBaseFrontend FrontendItem
        {
            get { return (EventBaseFrontend)base.FrontendItem; }

        }
        public new EventBase DbItem
        {
            get
            {
                return (EventBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }

        public CmsPropertyInfo StartDate { get; protected set; }

        public CmsPropertyInfo EndDate { get; protected set; }

        public CmsPropertyInfo ImageFilename { get; protected set; }

        public CmsPropertyInfo EventCategory { get; protected set; }

        public CmsPropertyInfo Category { get; protected set; }



// [basecmsinfo_collectiondeclarations]

        public CmsCollectionInfo EventMediaItems { get; protected set; }
        

        public CmsCollectionInfo EventSessions { get; protected set; }
        




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]

		
		//InitCollectionBaseOneToMany
		this.EventMediaItems = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventModule.EventBase>.GetPropertyBySelector(item => item.EventMediaItems),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventMediaItemModule.EventMediaItemBase>.GetPropertyBySelector(item => item.Event)));
		
		//InitCollectionBaseOneToMany
		this.EventSessions = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventModule.EventBase>.GetPropertyBySelector(item => item.EventSessions),
						CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase>.GetPropertyBySelector(item => item.Event)));


			base.initBasicFields();
          
        }

    }
}
