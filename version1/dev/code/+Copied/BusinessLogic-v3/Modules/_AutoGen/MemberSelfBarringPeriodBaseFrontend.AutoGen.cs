using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.MemberSelfBarringPeriodModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.MemberSelfBarringPeriodModule.MemberSelfBarringPeriodBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.MemberSelfBarringPeriodModule.IMemberSelfBarringPeriodBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.MemberSelfBarringPeriodModule.MemberSelfBarringPeriodBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.MemberSelfBarringPeriodModule.IMemberSelfBarringPeriodBase item)
        {
        	return BusinessLogic_v3.Frontend.MemberSelfBarringPeriodModule.MemberSelfBarringPeriodBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class MemberSelfBarringPeriodBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<MemberSelfBarringPeriodBaseFrontend, BusinessLogic_v3.Modules.MemberSelfBarringPeriodModule.IMemberSelfBarringPeriodBase>

    {
		
        
        protected MemberSelfBarringPeriodBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
