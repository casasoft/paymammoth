using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Frontend.PopularSearchModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<BusinessLogic_v3.Frontend.PopularSearchModule.PopularSearchBaseFrontend> ToFrontendBaseList(this IEnumerable<BusinessLogic_v3.Modules.PopularSearchModule.IPopularSearchBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static BusinessLogic_v3.Frontend.PopularSearchModule.PopularSearchBaseFrontend ToFrontendBase(this BusinessLogic_v3.Modules.PopularSearchModule.IPopularSearchBase item)
        {
        	return BusinessLogic_v3.Frontend.PopularSearchModule.PopularSearchBaseFrontend.Get(item);
        }
    }
}


namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class PopularSearchBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<PopularSearchBaseFrontend, BusinessLogic_v3.Modules.PopularSearchModule.IPopularSearchBase>

    {
		
        
        protected PopularSearchBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
