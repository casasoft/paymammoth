using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.MemberResponsibleLimitsModule;
using BusinessLogic_v3.Cms.MemberResponsibleLimitsModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class MemberResponsibleLimitsBaseCmsFactory_AutoGen : CmsFactoryBase<MemberResponsibleLimitsBaseCmsInfo, MemberResponsibleLimitsBase>
    {
       
       public new static MemberResponsibleLimitsBaseCmsFactory Instance
	    {
	         get
	         {
                 return (MemberResponsibleLimitsBaseCmsFactory)CmsFactoryBase<MemberResponsibleLimitsBaseCmsInfo, MemberResponsibleLimitsBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = MemberResponsibleLimitsBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "MemberResponsibleLimits.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "MemberResponsibleLimits";

            this.QueryStringParamID = "MemberResponsibleLimitsId";

            cmsInfo.TitlePlural = "Member Responsible Limits";

            cmsInfo.TitleSingular =  "Member Responsible Limits";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public MemberResponsibleLimitsBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "MemberResponsibleLimits/";
			UsedInProject = MemberResponsibleLimitsBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
