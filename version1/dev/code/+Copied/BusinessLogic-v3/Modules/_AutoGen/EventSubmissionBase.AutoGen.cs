using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.EventSubmissionModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "EventSubmission")]
    public abstract class EventSubmissionBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IEventSubmissionBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static EventSubmissionBaseFactory Factory
        {
            get
            {
                return EventSubmissionBaseFactory.Instance; 
            }
        }    
        /*
        public static EventSubmissionBase CreateNewItem()
        {
            return (EventSubmissionBase)Factory.CreateNewItem();
        }

		public static EventSubmissionBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<EventSubmissionBase, EventSubmissionBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static EventSubmissionBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static EventSubmissionBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<EventSubmissionBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<EventSubmissionBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<EventSubmissionBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _eventname;
        
        public virtual string EventName 
        {
        	get
        	{
        		return _eventname;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_eventname,value);
        		_eventname = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _userfullname;
        
        public virtual string UserFullName 
        {
        	get
        	{
        		return _userfullname;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_userfullname,value);
        		_userfullname = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _useremail;
        
        public virtual string UserEmail 
        {
        	get
        	{
        		return _useremail;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_useremail,value);
        		_useremail = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _eventstartdate;
        
        public virtual DateTime EventStartDate 
        {
        	get
        	{
        		return _eventstartdate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_eventstartdate,value);
        		_eventstartdate = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _eventenddate;
        
        public virtual DateTime EventEndDate 
        {
        	get
        	{
        		return _eventenddate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_eventenddate,value);
        		_eventenddate = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _useripaddress;
        
        public virtual string UserIpAddress 
        {
        	get
        	{
        		return _useripaddress;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_useripaddress,value);
        		_useripaddress = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _timestamp;
        
        public virtual DateTime Timestamp 
        {
        	get
        	{
        		return _timestamp;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_timestamp,value);
        		_timestamp = value;
        		
        	}
        }
        
/*
		public virtual long? CategoryID
		{
		 	get 
		 	{
		 		return (this.Category != null ? (long?)this.Category.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CategoryModule.CategoryBase _category;
        public virtual BusinessLogic_v3.Modules.CategoryModule.CategoryBase Category 
        {
        	get
        	{
        		return _category;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_category,value);
        		_category = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CategoryModule.ICategoryBase BusinessLogic_v3.Modules._AutoGen.IEventSubmissionBaseAutoGen.Category 
        {
            get
            {
            	return this.Category;
            }
            set
            {
            	this.Category = (BusinessLogic_v3.Modules.CategoryModule.CategoryBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private bool _isconvertedtorealevent;
        
        public virtual bool IsConvertedToRealEvent 
        {
        	get
        	{
        		return _isconvertedtorealevent;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_isconvertedtorealevent,value);
        		_isconvertedtorealevent = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _approved;
        
        public virtual bool Approved 
        {
        	get
        	{
        		return _approved;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_approved,value);
        		_approved = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _approvedon;
        
        public virtual DateTime? ApprovedOn 
        {
        	get
        	{
        		return _approvedon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_approvedon,value);
        		_approvedon = value;
        		
        	}
        }
        
/*
		public virtual long? ApprovedByID
		{
		 	get 
		 	{
		 		return (this.ApprovedBy != null ? (long?)this.ApprovedBy.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase _approvedby;
        public virtual BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase ApprovedBy 
        {
        	get
        	{
        		return _approvedby;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_approvedby,value);
        		_approvedby = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CmsUserModule.ICmsUserBase BusinessLogic_v3.Modules._AutoGen.IEventSubmissionBaseAutoGen.ApprovedBy 
        {
            get
            {
            	return this.ApprovedBy;
            }
            set
            {
            	this.ApprovedBy = (BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _eventdescription;
        
        public virtual string EventDescription 
        {
        	get
        	{
        		return _eventdescription;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_eventdescription,value);
        		_eventdescription = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _eventsummary;
        
        public virtual string EventSummary 
        {
        	get
        	{
        		return _eventsummary;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_eventsummary,value);
        		_eventsummary = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]

        //BaseClass_CollectionOneToManyLeftSide
        
        public class __EventBasicsCollectionInfo : DbCollectionOneToManyManager<
        	BusinessLogic_v3.Modules.EventSubmissionModule.EventSubmissionBase, 
        	BusinessLogic_v3.Modules.EventBasicModule.EventBasicBase,
        	BusinessLogic_v3.Modules.EventBasicModule.IEventBasicBase>,
            IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.EventSubmissionModule.EventSubmissionBase, BusinessLogic_v3.Modules.EventBasicModule.EventBasicBase>
        {
            private BusinessLogic_v3.Modules.EventSubmissionModule.EventSubmissionBase _item = null;
            public __EventBasicsCollectionInfo(BusinessLogic_v3.Modules.EventSubmissionModule.EventSubmissionBase item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<BusinessLogic_v3.Modules.EventBasicModule.EventBasicBase> Collection
            {
                get { return _item.__collection__EventBasics; }
            }

            void IDbCollectionOneToManyInfo<BusinessLogic_v3.Modules.EventSubmissionModule.EventSubmissionBase, BusinessLogic_v3.Modules.EventBasicModule.EventBasicBase>.SetLinkOnItem(BusinessLogic_v3.Modules.EventBasicModule.EventBasicBase item, BusinessLogic_v3.Modules.EventSubmissionModule.EventSubmissionBase value)
            {
                item.EventSubmission = value;
            }
        }
        
        private __EventBasicsCollectionInfo _EventBasics = null;
        public __EventBasicsCollectionInfo EventBasics
        {
            get
            {
                if (_EventBasics == null)
                    _EventBasics = new __EventBasicsCollectionInfo((BusinessLogic_v3.Modules.EventSubmissionModule.EventSubmissionBase)this);
                return _EventBasics;
            }
        }
           
		ICollectionManager<BusinessLogic_v3.Modules.EventBasicModule.IEventBasicBase> IEventSubmissionBaseAutoGen.EventBasics
        {
            get {  return this.EventBasics; }
        }               
           

        protected virtual IEnumerable<BusinessLogic_v3.Modules.EventBasicModule.EventBasicBase> __collection__EventBasics
        {
            get { throw new InvalidOperationException("This must be overriden in the final implementation.  If you are getting this error message, this means that somewhere the code is accessing this collection, but the class 'EventBasicBase' is not marked as UsedInProject"); }
        }


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
