using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using BusinessLogic_v3.Modules.Product_CultureInfoModule;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace BusinessLogic_v3.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "Product_CultureInfo")]
    public abstract class Product_CultureInfoBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IProduct_CultureInfoBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
      , IMultilingualContentInfo
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static Product_CultureInfoBaseFactory Factory
        {
            get
            {
                return Product_CultureInfoBaseFactory.Instance; 
            }
        }    
        /*
        public static Product_CultureInfoBase CreateNewItem()
        {
            return (Product_CultureInfoBase)Factory.CreateNewItem();
        }

		public static Product_CultureInfoBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<Product_CultureInfoBase, Product_CultureInfoBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static Product_CultureInfoBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static Product_CultureInfoBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<Product_CultureInfoBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<Product_CultureInfoBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<Product_CultureInfoBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? CultureInfoID
		{
		 	get 
		 	{
		 		return (this.CultureInfo != null ? (long?)this.CultureInfo.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase _cultureinfo;
        public virtual BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase CultureInfo 
        {
        	get
        	{
        		return _cultureinfo;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_cultureinfo,value);
        		_cultureinfo = value;
        	}
        }
            
        BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase BusinessLogic_v3.Modules._AutoGen.IProduct_CultureInfoBaseAutoGen.CultureInfo 
        {
            get
            {
            	return this.CultureInfo;
            }
            set
            {
            	this.CultureInfo = (BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase) value;
            }
        }
            
/*
		public virtual long? ProductID
		{
		 	get 
		 	{
		 		return (this.Product != null ? (long?)this.Product.ID : null);
		 	}
		}*/
		
        protected BusinessLogic_v3.Modules.ProductModule.ProductBase _product;
        public virtual BusinessLogic_v3.Modules.ProductModule.ProductBase Product 
        {
        	get
        	{
        		return _product;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_product,value);
        		_product = value;
        	}
        }
            
        BusinessLogic_v3.Modules.ProductModule.IProductBase BusinessLogic_v3.Modules._AutoGen.IProduct_CultureInfoBaseAutoGen.Product 
        {
            get
            {
            	return this.Product;
            }
            set
            {
            	this.Product = (BusinessLogic_v3.Modules.ProductModule.ProductBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private string _title;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _description;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string Description 
        {
        	get
        	{
        		return _description;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_description,value);
        		_description = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _metatitle;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string MetaTitle 
        {
        	get
        	{
        		return _metatitle;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_metatitle,value);
        		_metatitle = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _metadescription;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string MetaDescription 
        {
        	get
        	{
        		return _metadescription;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_metadescription,value);
        		_metadescription = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _material;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string Material 
        {
        	get
        	{
        		return _material;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_material,value);
        		_material = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string __actualtitle;
        
        [BusinessLogic_v3.Classes.Attributes.Multilingual]
        
        public virtual string _ActualTitle 
        {
        	get
        	{
        		return __actualtitle;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(__actualtitle,value);
        		__actualtitle = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)




#region IMultilingualContentInfo Members

        long IMultilingualContentInfo.CultureInfoID
        {
            get
            {                          
            	if (this.CultureInfo != null)
                	return this.CultureInfo.ID;
               	else
               		return 0;
            }
        }
  		BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject IMultilingualContentInfo.GetLinkedItem()
        {
            return this.Product;
        }
#endregion



#endregion       

		

    }
}
