﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules.ShoppingCartItemModule
{
    public class ShoppingCartItemResultHelperBase
    {
        public Enums.SHOPPING_CART_ITEM_UPDATE_QTY_RESULT UpdateQuantityResult { get; set; }
        public int QuantityRemaining { get; set; }
        public int QuantityRequested { get; set; }
    }
}
