using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Modules.ProductModule;
using BusinessLogic_v3.Modules.SpecialOfferModule;
using BusinessLogic_v3.Modules.OrderItemModule;
using BusinessLogic_v3.Modules.OrderModule;
using BusinessLogic_v3.Classes.Pricing;

namespace BusinessLogic_v3.Modules.ShoppingCartItemModule
{
    using ProductVariationModule;

//IBaseClass-File
	
    public interface IShoppingCartItemBase : BusinessLogic_v3.Modules._AutoGen.IShoppingCartItemBaseAutoGen, BusinessLogic_v3.Modules.SpecialOfferModule.Helpers.ISpecialOfferCalculationItem
    {
        double GetShipmentWeight();
        IProductBase GetLinkedProduct();
        IProductVariationBase GetLinkedProductVariation();

        ///// <summary>
        ///// Returns the applicable special offer, first preference going to the voucher code.
        ///// </summary>
        ///// <returns></returns>
        //ISpecialOfferBase GetApplicableSpecialOffer();

        //double GetTotalPrice(bool includeTax = true, bool reduceDiscount = true);
        //double GetTotalDiscount();
        //double GetDiscountPerUnit();
        //double GetPriceExcTaxPerUnit();
        ShoppingCartItemResultHelperBase UpdateQuantity(int newQuantity, bool autoSave = true);
        int GetQuantity();
        //IOrderItemBase ConvertToOrderItem(IOrderBase linkedOrder, bool autoSave = true);
        //double GetPricePerUnit(bool includeTax = true, bool reduceDiscount = true);
        //bool HasDiscount();
        string GetSubtitle();
        //double GetPriceBeforeDiscountIncTax();
        IPricingInfo PriceInfo { get; }
    }
}
