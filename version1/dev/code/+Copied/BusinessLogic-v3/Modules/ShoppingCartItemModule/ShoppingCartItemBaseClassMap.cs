using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.ShoppingCartItemModule
{

//BaseClassMap-File
    
    public abstract class ShoppingCartItemBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.ShoppingCartItemBaseMap_AutoGen<TData>
    	where TData : ShoppingCartItemBase
    {

        protected override void LinkedProductVariationMappingInfo(ICollectionMapInfo mapInfo)
        {
            base.LinkedProductVariationMappingInfo(mapInfo);
            mapInfo.Inverse = true;
        }
 
    }
   
}
