﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.ProductModule;
using BusinessLogic_v3.Classes.Pricing;
using CS.General_v3.Util;
using BusinessLogic_v3.Modules.SpecialOfferModule;

namespace BusinessLogic_v3.Modules.ShoppingCartItemModule
{
    public class ShoppingCartItemBasePricingInfo : PricingInfo
    {

        public static ShoppingCartItemBasePricingInfo CreateInstance(ShoppingCartItemBase shoppingCartItem)
        {
            ShoppingCartItemBasePricingInfo priceInfo = CS.General_v3.Util.ReflectionUtil.CreateObjectWithPrivateConstructorForLastType<ShoppingCartItemBasePricingInfo>();
            priceInfo._item = shoppingCartItem;
            return priceInfo;

        }
        private ShoppingCartItemBase _m_item;
        protected ShoppingCartItemBase _item
        {
            get { return _m_item; }
            set 
            {
                _m_item = value; 
                //base._product = (ProductBase) _m_item.GetLinkedProduct(); 
            }
        }

        protected ProductBase product
        {
            get { return (ProductBase) _item.GetLinkedProduct(); }
        }

        protected ShoppingCartItemBasePricingInfo() 
        {
            
        }
        public override int Quantity
        {
            get
            {
                return _item.GetQuantity();
            }
            set
            {
                base.Quantity = value;
            }
        }
       

        private double getTotalDiscount()
        {

            double cartDiscount = _item.ShoppingCart.SpecialOfferCalculator.CalculateDiscountForCartItemForOneUnit(_item);
            cartDiscount += _item.GetLinkedProduct().GetDiscountManual();
            return Math.Round(cartDiscount, Constants.General.NUMBER_PRECISION_FINANCIAL);
            //return Math.Round(cartDiscount + product.PriceInfo.Discount, Constants.General.NUMBER_PRECISION_FINANCIAL);
        }

        public override double Discount
        {
            get
            {

                return getTotalDiscount();
            }
            set
            {
                
            }
        }



        public override double RetailPriceBeforeDiscount
        {
            get
            {
                return product.PriceInfo.RetailPriceBeforeDiscount;
                
            }
            set
            {
        //        base.RetailPriceBeforeDiscount = value;
            }
        }
        //public override double PriceExcTax
        //{
        //    get
        //    {

        //        return product.PriceInfo.PriceExcTax;
        //    }
        //    set
        //    {

        //    }
        //}

        //private double getProductDiscount()
        //{
        //    double discount = _product.PriceRetailIncTaxPerUnit - _product._ActualPrice;
        //    return discount;
        //}

        public override double TaxRate
        {
            get
            {
                return product.PriceInfo.TaxRate;
                
            }
            set
            {
                //base.TaxRate = value;
            }
        }
        //public override double TaxAmount
        //{
        //    get
        //    {
        //        return product.PriceInfo.TaxAmount;
                

        //    }
        //    set
        //    {

        //    }
        //}


    }
}
