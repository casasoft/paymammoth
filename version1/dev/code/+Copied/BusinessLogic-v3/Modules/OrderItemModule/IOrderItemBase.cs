using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Modules.SpecialOfferModule;

namespace BusinessLogic_v3.Modules.OrderItemModule
{

//IBaseClass-File
	
    public interface IOrderItemBase : BusinessLogic_v3.Modules._AutoGen.IOrderItemBaseAutoGen
    {

        double TaxRate { get; }
        double GetDiscountPerUnit();

        double GetPriceExcTaxPerUnit(bool includeDiscount = true);
        double GetTotalPriceExcludingTaxes(bool reduceDiscount);

        double GetTaxAmountPerUnit(bool includeDiscount = true);
        double GetTotalTaxAmount(bool includeDiscount = true);
        double GetRetailUnitPrice(bool includeDiscount = true);
        bool HasDiscount();
        //ISpecialOfferBase GetApplicableSpecialOffer();
        double GetTotalPrice(bool includeTax = true, bool reduceDiscount = true);
        double GetShipmentWeight();

        //void SetUnitPriceIncVat(double priceIncVAT, double VATRatePerc);
        string GetTitle();
    }
}
