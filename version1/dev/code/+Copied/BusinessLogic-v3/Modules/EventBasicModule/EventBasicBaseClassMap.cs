using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.EventBasicModule
{

//BaseClassMap-File
    
    public abstract class EventBasicBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.EventBasicBaseMap_AutoGen<TData>
    	where TData : EventBasicBase
    {
        protected override void TitleMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.LuceneInfo.MappedInLucene = true;
            base.TitleMappingInfo(mapInfo);
        }

        protected override void DescriptionHtmlMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.LuceneInfo.MappedInLucene = true;
            base.DescriptionHtmlMappingInfo(mapInfo);
        }

        protected override void SummaryMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.LuceneInfo.MappedInLucene = true;
            base.SummaryMappingInfo(mapInfo);
        }
    }
}
