using BusinessLogic_v3.Classes.MediaItems;
using BusinessLogic_v3.Modules.EventMediaItemModule;
using BusinessLogic_v3.Modules.EventSubmissionModule;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Util;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.EventBasicModule
{

//IBaseClass-File
	
    public interface IEventBasicBase : BusinessLogic_v3.Modules._AutoGen.IEventBasicBaseAutoGen
    {
        /// <summary>
        /// Returns true if this event was created by a user and not site admin/owner.
        /// </summary>
        /// <returns></returns>
        bool IsSubmittedByUser();

        /// <summary>
        /// If this event was created by a user (i.e. not a CMSUser), the original submission is returned else null.
        /// </summary>
        /// <returns></returns>
        IEventSubmissionBase GetEventSubmission();

        IEnumerable<IEventMediaItemBase> GetMediaItems();

        IMediaItemImage<EventBasicBase.ImageSizingEnum> Image { get; }
    }
}
