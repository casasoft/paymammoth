using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.EventModule;

namespace BusinessLogic_v3.Modules.EventSessionPeriodDayModule
{   

//BaseFactory-File

	public abstract class EventSessionPeriodDayBaseFactory : BusinessLogic_v3.Modules._AutoGen.EventSessionPeriodDayBaseFactoryAutoGen, IEventSessionPeriodDayBaseFactory
    {
		public IEnumerable<IEventSessionPeriodDayBase> GetEventSessionPeriodDaysOfEvent(IEventBase eventLink)
		{
		    var q = GetQuery();
		    var qoEventSessionPeriod = q.JoinQueryOver(item => item.EventSessionPeriod);
		    var qoEventSession = qoEventSessionPeriod.JoinQueryOver(item => item.EventSession);
		    var qoEvent = qoEventSession.JoinQueryOver(item => item.Event).Where(item => item.ID == eventLink.ID);
		    return FindAll(qoEvent);
		}
		

    }

}
