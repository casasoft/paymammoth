using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;


using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Modules.EventModule;

namespace BusinessLogic_v3.Modules.EventSessionPeriodDayModule
{   

//IBaseFactory-File

	public interface IEventSessionPeriodDayBaseFactory : BusinessLogic_v3.Modules._AutoGen.IEventSessionPeriodDayBaseFactoryAutoGen
	{

	    IEnumerable<IEventSessionPeriodDayBase> GetEventSessionPeriodDaysOfEvent(IEventBase eventLink);

	}

}
