using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.CultureDetailsModule
{

//BaseClassMap-File
    
    public abstract class CultureDetailsBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.CultureDetailsBaseMap_AutoGen<TData>
    	where TData : CultureDetailsBase
    {

        public override Enums.NHIBERNATE_CACHE_TYPE CacheType
        {
            get
            {
                return Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            }
            set
            {
                base.CacheType = value;
            }
        }

        protected override void SpecificCountryCodeMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.CustomTypeMapping = typeof(BusinessLogic_v3.Classes.NHibernateClasses.Mappings.Types.EnumIsoCountryType);
            base.SpecificCountryCodeMappingInfo(mapInfo);
        }

        protected override void LanguageISOCodeMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.CustomTypeMapping = typeof(BusinessLogic_v3.Classes.NHibernateClasses.Mappings.Types.EnumIsoLanguageType);
            mapInfo.IsIndexed = true;
            mapInfo.Length = 100;
            base.LanguageISOCodeMappingInfo(mapInfo);
        }
 
    }
   
}
