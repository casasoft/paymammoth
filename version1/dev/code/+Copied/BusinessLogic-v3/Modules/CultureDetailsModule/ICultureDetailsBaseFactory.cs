using CS.General_v3.Extensions;
using System;
using System.Linq;
using System.Text;


using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using System.Collections.Generic;

namespace BusinessLogic_v3.Modules.CultureDetailsModule
{   

//IBaseFactory-File

	public interface ICultureDetailsBaseFactory : BusinessLogic_v3.Modules._AutoGen.ICultureDetailsBaseFactoryAutoGen
    {

        ICultureDetailsBase GetCurrentCultureInSession();
        ICultureDetailsBase GetCultureByBaseUrl(string baseUrl);
        ICultureDetailsBase GetCultureByCode(CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 languageCode);
        ICultureDetailsBase GetCultureByCode(string code, bool throwErrorIfCultureDoesNotExist = true);
        IEnumerable<ICultureDetailsBase> GetAvailableLanguages(bool loadNotActivated = false);
       
        ICultureDetailsBase GetDefaultCulture();
    }

}
