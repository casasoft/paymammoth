﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Util;

namespace BusinessLogic_v3.Modules.CultureDetailsModule
{
    public class CultureDetailsCache
    {
        public static CultureDetailsCache Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<CultureDetailsCache>(); } }

        private CultureDetailsCache()
        {
            init();
            loadData();

        }

        private void init()
        {
            CultureDetailsBaseFactory.Instance.OnItemUpdate += new BusinessLogic_v3.Classes.DbObjects.General.OnItemUpdateHandler(Instance_OnItemUpdate);
        }

        void Instance_OnItemUpdate(BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject item, Enums.UPDATE_TYPE updateType)
        {
            loadData();
        }

        private List<CultureDetailsBase> cultureDetails = null;

        private void loadData()
        {
            var session = nHibernateUtil.CreateNewSessionForCurrentContext();

            cultureDetails = CultureDetailsBaseFactory.Instance.FindAll().ToList();
            int cnt = cultureDetails.Count;//so that it is preloaded while the session still exists
            nHibernateUtil.DisposeCurrentSessionInContext();

            

        }
        public CultureDetailsBase GetCultureMatchingCurrentUrl()
        {
            string url = CS.General_v3.Util.PageUtil.GetCurrentUrlLocation().GetURL(fullyQualified: true);
            return GetCultureMatchingUrl(url);
        }

        public CultureDetailsBase GetCultureMatchingUrl(string url)
        {
            for (int i = 0; i < cultureDetails.Count; i++)
            {
                var c = cultureDetails[i];

                

                if (c.CheckIfBaseUrlMatchesUrl(url))
                {
                    return c;
                }
            }
            return null;
        }

    }
}
