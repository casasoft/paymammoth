using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Util;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.CultureDetailsModule
{

//IBaseClass-File
	
    public interface ICultureDetailsBase : BusinessLogic_v3.Modules._AutoGen.ICultureDetailsBaseAutoGen
    {

        System.Globalization.CultureInfo GetCultureGlobalizationInfo();

        CS.General_v3.Classes.PorterStemmer.StemmerInterface GetStemmerForCulture();
        CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? GetCountryISOEnumValue();
        CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 GetLanguageISOEnumValue();
        string GetLanguage2LetterCode();
        string GetLanguage3LetterCode();
    }
}
