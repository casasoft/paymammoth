﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules.CultureDetailsModule.HelperClasses
{
    /// <summary>
    /// Performs serialization/deserialization of culture codes, e.g en-GB or fr-FR
    /// </summary>
    public class CultureCode
    {
        public CultureCode()
        {
            
                
        }
        public CultureCode(string s)
        {
            this.ParseFromString(s);
        }


        public CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639? Language { get; set; }
        /// <summary>
        /// The suffix is sometimes used to denote the alphabet used, e.g 'Cyrl' would mean a language written in Cyrillic.  Normal values should only be 'Cyrl' or 'Latn'
        /// </summary>
        public string Suffix { get; set; }

        public CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? Country { get; set; }

        /// <summary>
        /// Returns the formatted code, e.g 'en-GB'
        /// </summary>
        /// <returns></returns>
        public string GetCultureCode()
        {
            StringBuilder sb = new StringBuilder();
            if (this.Language.HasValue)
            {
                sb.Append(CS.General_v3.Enums.ISO_ENUMS.Language_ISO639_2letter_ToCode(Language.Value).ToLower() + "-");

                if (Country.HasValue)
                {
                    sb.Append(CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_To2LetterCode(Country).ToUpper());
                }
                else
                {
                    sb.Append(CS.General_v3.Enums.ISO_ENUMS.Language_ISO639_2letter_ToCode(Language.Value).ToUpper());
                }
                //cultureCode = cultureCode.ToLower();
                if (!string.IsNullOrEmpty(Suffix))
                    sb.Append("-" + Suffix);
            }
            return sb.ToString();
        }
        public override string ToString()
        {
            return this.GetCultureCode();
        }
        public void ParseFromString(string s)
        {
            if (string.IsNullOrWhiteSpace(s)) throw new InvalidOperationException("String must contain information in the format of [LANG]-[COUNTRY]-[SUFFIX]");
            var tokens = CS.General_v3.Util.Text.Split(s, "-");
            this.Language = CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639FromCode(tokens[0], true).GetValueOrDefault();
            if (tokens.Count >= 2)
            {
                this.Country = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_FromCode(tokens[1]);
            }
            if (this.Country.HasValue && tokens.Count >= 3)
            {
                this.Suffix = tokens[2];
            }
        }
        


    }
}
