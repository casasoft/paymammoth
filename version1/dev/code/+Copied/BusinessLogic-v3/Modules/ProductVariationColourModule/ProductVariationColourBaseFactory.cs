using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Modules.ProductVariationColourModule
{   

//BaseFactory-File

	public abstract class ProductVariationColourBaseFactory : BusinessLogic_v3.Modules._AutoGen.ProductVariationColourBaseFactoryAutoGen, IProductVariationColourBaseFactory
    {




        public ProductVariationColourBase GetByIdentifier(string colour, bool createIfNotExists = true)
        {
            colour = colour ?? "";
            colour = colour.Trim();
            var q = GetQuery();
            q = q.Where(x => x.Identifier == colour);
            var item = FindItem(q);
            
            
            
            if (item == null && createIfNotExists)
            {
                if (string.Compare(colour, "Pink", true) == 0)
                {
                    int k = 5;
                }
                //BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();
                using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionIfNotOneAlreadyActive())
                {
                    item = CreateNewItem();
                    item.Title = colour;
                    item.Identifier = colour;
                    item.Save();
                    if (t != null) t.Commit();
                }
                //BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();
                //var session = BusinessLogic_v3.Util.nHibernateUtil.GetCurrentSessionFromContext();
                //session.Merge(item);
            }
                
            
            return item;

        }
    }

}
