using BusinessLogic_v3.Modules.CmsUserModule;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Modules.CmsAuditLogModule
{   

//BaseFactory-File

	public abstract class CmsAuditLogBaseFactory : BusinessLogic_v3.Modules._AutoGen.CmsAuditLogBaseFactoryAutoGen, ICmsAuditLogBaseFactory
    {


        /// <summary>
        /// Adds an audit log to the CMS
        /// </summary>
        /// <param name="msgType">Message type</param>
        /// <param name="objectName">Name of the object in question, e.g Product.  Can be left null if not applicable</param>
        /// <param name="objectID">The ID of the related object.  Leave as 0 if not applicable</param>
        /// <param name="shortMsg">A short message detailing the log</param>
        /// <param name="furtherInfo">Further information</param>
        public void AddAuditLog(Enums.CMS_AUDIT_MESSAGE_TYPE msgType, string objectName, long objectID, string shortMsg, string furtherInfo)
        {

            CmsAuditLogBase log = CmsAuditLogBase.Factory.CreateNewItem();
            var session = NHClasses.NhManager.CreateNewSessionForContextAndReturn();
            using (var transaction = session.BeginTransaction())
            {
                var user = CmsUserSessionLogin.Instance.GetLoggedInUser();
                if (user != null)
                {
                    log.CmsUser = user;
                }
                log.IPAddress = CS.General_v3.Util.PageUtil.GetUserIP();
                log.MsgType = msgType;
                log.ItemID = objectID;
                log.Message = shortMsg;
                log.FurtherInformation = furtherInfo;
                log.ObjectName = objectName;
                log.DateTime = CS.General_v3.Util.Date.Now;
                log.Create();
                if (transaction.IsActive)
                    transaction.Commit();
            }
            NHClasses.NhManager.DisposeCurrentSessionInContext();
        }


    }

}
