﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.ShippingRatePriceModule;


namespace BusinessLogic_v3.Modules.ShippingRatesGroupModule
{
    public class ShippingRateResult
    {


        public IShippingRatesGroupBase ApplicableGroup { get; private set; }
        public IShippingRatePriceBase ApplicableRate { get; private set; }
        public bool HasExtraCharge()
        {
            return (ExtraChargeDueToMinimumAmountNotReached > 0);
        }
        public double ExtraChargeDueToMinimumAmountNotReached { get; set; }

        public double? ShippingPrice { get; private set; }
        public double? GetTotalPrice()
        {
            double? d = null;
            if (ShippingPrice.HasValue)
            {
                d = 0;
                d += ShippingPrice.Value;
                d += ExtraChargeDueToMinimumAmountNotReached;

            }
            return d;
        }

        public bool CouldNotBeCalculated()
        {
            return !ShippingPrice.HasValue;
        }
        public ShippingRateResult(IShippingRatesGroupBase group, IShippingRatePriceBase rate, double? shippingPrice, double extraChargeDueToMinimumAmountNotReached = 0)
        {
            this.ApplicableGroup = group;
            this.ApplicableRate = rate;
            this.ShippingPrice= shippingPrice;
            this.ExtraChargeDueToMinimumAmountNotReached = extraChargeDueToMinimumAmountNotReached;
        }

    }
}
