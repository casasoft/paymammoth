﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.ShippingMethodModule;
using log4net;

namespace BusinessLogic_v3.Modules.ShippingRatesGroupModule
{
    public class ShippingRatesCalculator
    {
        private ILog _log = log4net.LogManager.GetLogger(typeof(ShippingRatesCalculator));
        private IEnumerable<ShippingRatesGroupBase> shippingRates = null;
        public ShippingRatesCalculator()
        {
           


        }
        protected ShippingRatesGroupBase getRestOfTheWorld()
        {
            if (shippingRates != null)
            {
                foreach (var item in shippingRates)
                {

                    if (item != null && item.IsRestOfTheWorld())
                    {
                        return item;
                    }
                }
            }
            return null;
        }
        protected ShippingRatesGroupBase getApplicableRate(CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166 country)
        {
            ShippingRatesGroupBase rate = null;
            if (shippingRates != null)
            {
                foreach (var item in shippingRates)
                {
                    if (item.MatchesCountry(country))
                    {
                        rate = item;
                        break;
                    }

                }
            }
            if (rate == null)
            {
                rate =getRestOfTheWorld();
            }

            return rate;
            
        }

        private void loadRatesForMethod(IShippingMethodBase shippingMethodToUse)
        {
            if (_log.IsInfoEnabled) _log.Info("Loading rates for method " + shippingMethodToUse);

            shippingRates = shippingMethodToUse.ShippingRatesGroups.Cast<ShippingRatesGroupBase>();
        }

        public ShippingRateResult GetTotalPriceFor(IShippingMethodBase shippingMethodToUse, double totalWeight, double totalOrderValueExcShipping,
            CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166 country)
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(shippingMethodToUse, "shippingMethodToUse");
            if (_log.IsInfoEnabled) _log.Info("GetTotalPriceFor - Method: " + shippingMethodToUse.ToString() + " Total Weight: " + totalWeight.ToString("0.0000") + ", Country: " + country); 
            loadRatesForMethod(shippingMethodToUse);
            
            ShippingRateResult result = null;
            if (_log.IsInfoEnabled) _log.Info("Retrieving applicable rate" + shippingMethodToUse);
            ShippingRatesGroupBase rate = getApplicableRate(country);
            if (rate != null)
            {
                var pricing = rate.GetRateForWeight(totalWeight);
                if (pricing != null)
                {
                    double? totalShippingPrice = pricing.GetPriceForWeight(totalWeight);
                    if (totalShippingPrice.HasValue)
                    {
                        double extraCharge = 0;
                        if (totalOrderValueExcShipping < shippingMethodToUse.MinimumOrderAmount)
                        {
                            extraCharge = shippingMethodToUse.ExtraChargeAmountIfMinimumNotReached;
                            
                        }

                        result = new ShippingRateResult(rate, pricing, totalShippingPrice.Value, extraCharge);
                    }
                }
            }
            if (_log.IsInfoEnabled) _log.Info("GetTotalPriceFor - Result: " + (result != null ? result.GetTotalPrice().GetValueOrDefault().ToString("0.0000") : "N/A"));
            return result;


        }

    }
}
