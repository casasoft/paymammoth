using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ArticleModule;

namespace BusinessLogic_v3.Modules.ArticleRatingModule
{   

//BaseFactory-File

	public abstract class ArticleRatingBaseFactory : BusinessLogic_v3.Modules._AutoGen.ArticleRatingBaseFactoryAutoGen, IArticleRatingBaseFactory
    {
        
    }

}
