﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.ArticleModule;
using BusinessLogic_v3.Util;
using CS.General_v3.Util;

namespace BusinessLogic_v3.Modules.ArticleRatingModule
{
    public class ArticleRatingManager
    {
        private IArticleBase article { get; set; }

        public ArticleRatingManager(IArticleBase article)
        {
            ContractsUtil.RequiresNotNullable(article, "Article must not be null!");
            this.article = article;
        }

        public void AddRating(double ratingValue)
        {
            using (var t = nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
                IArticleRatingBase rating = Factories.ArticleRatingFactory.CreateNewItem();
                rating.Article = article;
                rating.Rating = ratingValue;
                rating.IPAddress = CS.General_v3.Util.PageUtil.GetUserIP();
                rating.Date = Date.Now;
                article.ArticleRatings.Add(rating);
                rating.Save();
                t.CommitIfActiveElseFlush();
            }
            RefreshRatings();
        }

        public IEnumerable<IArticleRatingBase> GetRatings()
        {
            IEnumerable<IArticleRatingBase> result = null;
            if (article != null)
            {
                result = article.ArticleRatings;
            }
            return result;
        }

        public void RefreshRatings()
        {
            double ratingVal = 0;
            IEnumerable<IArticleRatingBase> ratings = article.ArticleRatings;
            foreach (IArticleRatingBase rating in ratings)
            {
                ratingVal += rating.Rating;
            }
            int ratingsCount = ratings.Count();
            article.AvgRating = ratingVal / ratingsCount;
            article.RatingCount = ratingsCount;
            article.Save();
        }
    }
}
