﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Text;

namespace BusinessLogic_v3.Modules.MemberModule.KYC
{
    public class MemberKYCManager
    {
        public MemberKYCManager(MemberBase member)
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(member, "Member is required");
            this.Member = member;
            this.Member.OnSaveBefore += new Classes.DbObjects.Parameters.SaveBeforeAfterHandler(Member_OnSaveBefore);
        }

        void Member_OnSaveBefore(CS.General_v3.Classes.HelperClasses.OperationResult result, Classes.DbObjects.Parameters.SaveBeforeAfterParams saveParams)
        {
            if (this.Member.KYCVerified && !this.Member.KYCVerifiedOn.HasValue)
            {
                this.Member.KYCVerifiedOn = CS.General_v3.Util.Date.Now;//if it does not have a value, mark as now
            }
        }

        public MemberBase Member { get; private set; }
        private void _sendEmail(Enum identifier )
        {
            var emailText = BusinessLogic_v3.Modules.EmailTextModule.EmailTextBaseFactory.Instance.GetByIdentifier(identifier);
            TokenReplacerWithContentTagItem tokenRep = new TokenReplacerWithContentTagItem(emailText);
            this.Member.FillTokenReplacerWithInfo(tokenRep);

            var email = emailText.GetEmailMessage();
            if (this.Member.AddToRecipientOfEmail(email))
            {
                email.SendAsynchronously();
            }


        }
        private void sendEmailAboutStartOfKYCProcedure()
        {
            _sendEmail(Enums.EmailsBusinessLogic.User_Members_KnowYourCustomer_ProcessStarted);
            
        }

        public void StartKYCProcedure()
        {
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
                Member.KYCProcessStarted = true;
                Member.KYCProcessStartedOn = CS.General_v3.Util.Date.Now;
                Member.KYCVerified = false;
                Member.Save();
                t.Commit();
            }
            sendEmailAboutStartOfKYCProcedure();
        }

    }
}
