﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.Login;

using CS.General_v3.Classes.Login;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using log4net;
using BusinessLogic_v3.Util;
using Brickred.SocialAuth.NET.Core.BusinessObjects;


namespace BusinessLogic_v3.Modules.MemberModule.SessionManager
{
    public partial class MemberSessionLoginManager : WebUserSessionLoginManager<MemberBase>, IMemberSessionLoginManager
    {
        private static readonly MemberSessionLoginManager _Instance = new MemberSessionLoginManager(null);
        private static readonly ILog _log = LogManager.GetLogger(typeof(MemberSessionLoginManager));
       
        //public static MemberSessionLoginManager _Instance = null;
        public static MemberSessionLoginManager Instance
        {
            get
            {
                return _Instance;
            }
        }
        public MemberSessionLoginManager(string welcomeURL)
            : base(welcomeURL, loginID: "MemberUserLogin")
        {
            
        }

        public override bool DoNotAllowConcurrentLogins
        {
            get
            {
                return CS.General_v3.Settings.GetSettingFromDatabaseByGenericEnum<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Member_Login_DoNotAllowConcurrentLogins);

                
            }
            set
            {
                base.DoNotAllowConcurrentLogins = value;
            }
        }
       
        public new virtual MemberBase GetLoggedInUser()
        {
                //2012-06-12: Added by Mark due to an inconsistency
            return GetLoggedInUserAlsoFromRememberMe();
            //return base.GetLoggedInUser();
        }
        
        //protected override MemberBase _getTemporaryUser()
        //{
        //    MemberBase m = base._getTemporaryUser();
        //    if (m != null && !m.IsTemporary()) //if not temporary member, then clear logged in user
        //    {
        //        clearTemporaryUser();
        //        m = base._getTemporaryUser();
        //    }
        //    return m;
        //}


        protected override bool checkPassword(MemberBase user, string password)
        {
            return user.CheckPassword(password);
        }
        public override IEnumerable<MemberBase> GetUsersList(string usernameOrEmail)
        {
            var members = MemberBaseFactory.Instance.GetMembersByUsernameOrEmail(usernameOrEmail);
            return members;
        }


        public override MemberBase GetUserByID(long id)
        {
            //If it is not used then no user is logged in
            if (MemberBaseFactory.UsedInProject)
            {
                return MemberBaseFactory.Instance.GetByPrimaryKey(id);
            }
            else
            {
                if (CS.General_v3.Util.Other.IsLocalTestingMachine)
                    throw new InvalidOperationException("Cannot call 'GetUserById' of MemberSessionLoginManager if MemberFactory is not UsedInProject");
                else
                    return null;
            }
        }

      

        protected override MemberBase getUserFromSocialLogin(bool autoLogin)
        {
            MemberBase result = null;
            if (SocialAuthUser.IsLoggedIn())
            {
                SocialAuthUser socialUser = SocialAuthUser.GetCurrentUser();
                UserProfile socialUserProfile = socialUser.GetProfile();
               
                MemberBase existingUser = MemberBase.Factory.GetMemberByEmailOrUsername(socialUserProfile.Email);
                if (existingUser != null)
                {
                    if(autoLogin)
                    {
                        Login(existingUser, false);
                    }
                    return existingUser;
                }
                else
                {
                    var newUser = createNewMemberFromSocial(socialUserProfile);
                    if(autoLogin)
                    {
                        Login(newUser, false);
                    }
                    return newUser;
                }
            }
            return result;
        }

        private MemberBase createNewMemberFromSocial(UserProfile socialUserProfile)
        {
            MemberBase newUser = null;
            using (var t = nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
                newUser = MemberBase.Factory.CreateNewItem();
                newUser.FirstName = socialUserProfile.FirstName;
                newUser.LastName = socialUserProfile.LastName;
                
                if(socialUserProfile.GenderType != GENDER.NOT_SPECIFIED)
                {
                    newUser.Gender = socialUserProfile.GenderType == GENDER.MALE
                                         ? CS.General_v3.Enums.GENDER.Male
                                         : CS.General_v3.Enums.GENDER.Female;
                }
                if (socialUserProfile.DateOfBirth != null)
                {
                    newUser.DateOfBirth = DateTime.Parse(socialUserProfile.DateOfBirth);
                }
                if (socialUserProfile.Provider == PROVIDER_TYPE.TWITTER)
                {
                    newUser.TwitterScreenName = socialUserProfile.DisplayName;
                    newUser.TwitterUserID = long.Parse(socialUserProfile.ID);
                }
                else
                {
                    newUser.Email = newUser.Username = socialUserProfile.Email;
                }
                var language = Factories.CultureDetailsFactory.GetCultureByCode(socialUserProfile.Language, false);
                if(language == null)
                {
                    language = Factories.CultureDetailsFactory.GetDefaultCulture();
                }
                newUser.SetCurrentCultureInfoDBId(language);
                newUser.RegisteredThroughSocialMedium = true;
                newUser.Save();
                t.Commit();
            }
            return newUser;
        }

        private long? cookie_TempMemberID
        {
            get
            {
                return CS.General_v3.Util.PageUtil.GetCookieLongNullable(getCookieName(), "TempMemberID");
            }
            set
            {
                CS.General_v3.Util.PageUtil.SetCookie(getCookieName(), "TempMemberID", (value != null ? value.ToString() : ""), CS.General_v3.Util.Date.Now.AddHours(HoursToRememberTemporaryUser));

            }

        }

        private string cookie_TempMemberGUID
        {
            get
            {
                return CS.General_v3.Util.PageUtil.GetCookie(getCookieName(), "TempMemberGUID");
            }
            set
            {
                CS.General_v3.Util.PageUtil.SetCookie(getCookieName(), "TempMemberGUID", value);

            }

        }
        protected MemberBase createNewTemporaryUser()
        {
            MemberBase member = null;
            if (CS.General_v3.Util.PageUtil.CheckIfCookiesAreEnabled())
            {
                using (var t = nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
                {
                    member = MemberBaseFactory.Instance.CreateNewItem();
                    member.MarkAsTemporary();

                    member.Save(new SaveParams(savePermanently: false));
                    t.Commit();
                }
            }
            return member;
        }
        protected void clearTemporaryUser()
        {
            this.cookie_TempMemberGUID = null;
            this.cookie_TempMemberID = null;
        }
        public virtual MemberBase GetLoggedInOrTemporaryUser(bool doNotLoadIfSearchbot = true)
        {
            var user = GetLoggedInUser();
            if (user == null)
            {
                user = GetTemporaryUser(doNotLoadIfSearchbot);
            }
            return user;
        }

        public virtual MemberBase GetTemporaryUser(bool doNotLoadIfSearchbot = true)
        {
            MemberBase user = null;

            bool isSearchBot = false;
            if(doNotLoadIfSearchbot)
            { 
                isSearchBot = CS.General_v3.Util.PageUtil.CheckUserAgentForKnownSearchEngineBots();
            }
            if (CS.General_v3.Util.PageUtil.CheckIfCookiesAreEnabled() && !isSearchBot)
            {
                long? memberID = cookie_TempMemberID;
                if (memberID.HasValue)
                {
                    user = GetUserByID(memberID.Value);
                }
                if (user != null)
                {
                    if (!user.CheckSessionGUID(cookie_TempMemberGUID))
                        user = null;
                }
                if (user == null)
                {
                    user = createNewTemporaryUser();
                    user.GenerateSessionGUIDAndSave();
                }
                cookie_TempMemberID = user.ID;
                cookie_TempMemberGUID = user.SessionGUID;

            }
            else
            {
                if (_log.IsInfoEnabled)
                {
                    if (false)
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine("Tried to create temporary user, without COOKIES = DISABLED");
                        var r = CS.General_v3.Util.PageUtil.GetCurrentRequest();
                        if (r != null)
                        {
                            sb.AppendLine("UserAgent: " + r.UserAgent);
                            sb.AppendLine("Url: " + r.Url);
                        }
                        sb.AppendLine("StackTrace: " + Environment.StackTrace);
                        _log.Debug(sb.ToString());
                    }
                }
            }
            return user;
        }
    }
}
