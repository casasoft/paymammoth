using BusinessLogic_v3.Classes.Interfaces;
using BusinessLogic_v3.Modules.EventModule;
using BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule;
using BusinessLogic_v3.Modules.MemberResponsibleLimitsModule;
using BusinessLogic_v3.Modules.OrderModule;
using BusinessLogic_v3.Modules.ShoppingCartModule;
using BusinessLogic_v3.Modules.SpecialOfferModule;
using BusinessLogic_v3.Modules.TicketModule;
using CS.General_v3.Classes.Login;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Modules.CategoryModule;
using BusinessLogic_v3.Modules.MemberSubscriptionLinkModule;
using BusinessLogic_v3.Classes.MediaItems;
using BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule;
using BusinessLogic_v3.Modules.MemberSubscriptionTypeModule;

namespace BusinessLogic_v3.Modules.MemberModule
{
    using MemberSelfBarringPeriodModule;
    using BusinessLogic_v3.Modules.CultureDetailsModule;
using BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule;

//IBaseClass-File
    public enum AddDiscountCodeResult
    {
        Invalid,
        OK

    }
    public interface IMemberBase : BusinessLogic_v3.Modules._AutoGen.IMemberBaseAutoGen, IUser, IMemberAddressDetails, ITicketingSystemUser
    {
        Enums.MEMBER_REGISTRATION_RESULT RegisterMember();
        //OperationResult RegisterMember();
        OperationResult UpdateMember();
        SearchPagedResults<IOrderBase> GetOrderHistoryWithPaging(int pageNo, int pageSize, Enums.ORDER_HISTORY_SORT_BY sortBy, bool includeNonPaid = true);
        IMemberResponsibleLimitsBase SetNewResponsibleLimits(Enums.MEMBER_RESPONSIBLE_LIMITS_FREQUENCY frequencyType, double limitValue, DateTime? dateEffected = null);
        bool CheckIfMemberHasEnoughFundsInBalance(double funds, bool throwErrorIfNot = false);
        //IMemberAccountBalanceHistoryBase UpdateAccountBalance(double updateAmount, string msg, DateTime? date = null);

        void SubscribeToCategory(ICategoryBase cat);

        bool VerifyForgotPasswordUserCode(string code);
        bool VerifyForgotPasswordUserCodeAndChange(string code, string newPassword, bool autoSave = true);

        void Logout();
        void SaveTemporarily();
        IMemberSubscriptionLinkBase GetCurrentSubscriptionLink();
        IOrderBase GenerateOrderForCreditPurchase(double creditDifference);
        IOrderBase GenerateOrderForSubscriptionPurchase(IMemberSubscriptionTypePriceBase subscriptionType, bool autoRenew);
        SearchPagedResults<IMemberAccountBalanceHistoryBase> GetAccountBalanceHistoryItems(int pageNo, int pageSize, BusinessLogic_v3.Enums.MEMBER_ACCOUNT_BALANCE_HISTORY_SORT_BY sortBy, DateTime? fromDate = null, DateTime? toDate = null);
        void SetSelfExclusion(DateTime dateUntil, bool autoSaveInDb = true);
        MemberResponsibleLimitsBase GetResponsibleLimitsApplicableToday();
        bool CheckIfCanDepositAmount(double credits, DateTime? dateEffected = null);
        void DeletePreviousUnpaidAndUnprocessedOrders(IOrderBase excludeOrder = null);
        IMediaItemImage<MemberBase.KYCProofOfIdentitySizingEnum> KYCProofOfIdentity { get; }
        IEnumerable<ITicketBase> GetClosedTickets(Enums.TICKET_SORTBY sortBy, int pgNo, int showAmt);
        IEnumerable<ITicketBase> GetOpenTickets(Enums.TICKET_SORTBY sortBy, int pgNo, int showAmt);
        ITicketBase CreateNewTicket();
        IMediaItemImage<MemberBase.ImageSizingEnum> Image { get; }
        string CompanyProfile { get; set; }
        DateTime? GetSubscriptionExpiryDate();
        IEnumerable<IEventBase> GetEvents();

        void SetAutoRenewalSubscriptionTypePrice(IMemberSubscriptionTypePriceBase subscriptionTypePrice,
                                                 bool useTransaction = true);

        /// <summary>
        /// If member has no subscription, a new one is applied.
        /// If member has a subscription but new one is different, current one is ended and new one applies from today.
        /// If member has a subscription and new one is like it, it's put in a queue and once current one ends, new one applies.
        /// </summary>
        /// <param name="newMemberSubscriptionTypePrice"></param>
        /// <returns></returns>
        bool PurchaseSubscription(IMemberSubscriptionTypePriceBase subscriptionTypePrice, bool isAutoRenewal,
                                  IOrderBase order = null, bool autoSave = true);

        IMediaItemImage<MemberBase.KYCProofOfAddressSizingEnum> KYCProofOfAddress { get; }

        /// <summary>
        /// Returns only the renewable credits / balance the user has. (Renewable Account Balance).
        /// </summary>
        /// <returns></returns>
        double GetRenewableCredits();

        /// <summary>
        /// Returns the fixed Account balance of the user. (AccountBalance).
        /// </summary>
        /// <returns></returns>
        double GetAccountBalance();

        /// <summary>
        /// Returns the combined credit / account balance of this user (AccountBalance + RenewableAccountBalance).
        /// </summary>
        /// <returns></returns>
        double GetTotalCredits();

        /// <summary>
        /// Returns the date in which the next top up is due to take place. Returns null if no renewal will take place.
        /// </summary>
        /// <returns></returns>
        DateTime? GetNextTopUpDate();

        /// <summary>
        /// Returns true if there are sufficient funds available for an amount to be detucted. No transactions actually take place.
        /// </summary>
        /// <param name="amountToDetuct"></param>
        /// <returns></returns>
        bool IsThereSufficientCreditsForDeduction(double amountToDetuct);

        void AddSelfBarringPeriod(int lengthInDays);
        IMemberSelfBarringPeriodBase GetCurrentSelfBarringPeriod();

        /// <summary>
        /// True if the member currently has a Self Barring Period.
        /// </summary>
        /// <returns></returns>
        bool IsSelfBanned();

        IShoppingCartBase GetShoppingCart(bool createIfNotExists, bool createEvenIfCookiesDisabled = false,
                                          bool autoSave = true);
        ICultureDetailsBase GetPreferredCultureOrDefault();

        
        bool HasDiscountCode();
        ISpecialOfferVoucherCodeBase GetCurrentDiscountCode();
        
        AddDiscountCodeResult SetDiscountCode(string discountCode);
    }
}
