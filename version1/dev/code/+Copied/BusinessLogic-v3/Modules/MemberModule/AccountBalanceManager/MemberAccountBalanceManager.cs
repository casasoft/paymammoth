﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.MemberSubscriptionTypeModule;
using BusinessLogic_v3.Util;
using CS.General_v3.Util;
using BusinessLogic_v3.Modules.MemberSubscriptionLinkModule;

namespace BusinessLogic_v3.Modules.MemberModule.AccountBalanceManager
{
    public class MemberAccountBalanceManager
    {
        #region Singleton
        private MemberAccountBalanceManager()
        {

        }
        public static MemberAccountBalanceManager Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<MemberAccountBalanceManager>(); } }
        #endregion

        /// <summary>
        /// This checks all the members who are due to have their periodical account balance topped up. If top-up is allowed, their periodical account balance is updated.
        /// </summary>
        public void CheckRenewableAccountBalances()
        {
            int queryChunkSize =
                Factories.SettingFactory.GetSettingValue<int>(
                    Enums.BusinessLogicSettingsEnum.RenewableAccountBalanceCheckerQueryChunkSize);

            int pageNumber = 1;
            int resultCount = -1;

            do
            {
                var query = MemberBase.Factory.GetQuery();
                query = query.Where(
                item =>
                item.NextRenewableAccountBalanceTopupOn != null && item.NextRenewableAccountBalanceTopupOn <= Date.Now.GetEndOfDay());
                query.LimitQueryByPrimaryKeys(pageNumber, queryChunkSize);
                IEnumerable<MemberBase> results = MemberBase.Factory.FindAll(query);
                pageNumber++;
                resultCount = results.Count();
                foreach (MemberBase member in results)
                {
                    RenewAccountBalanceBasedOnSubscription(member);
                }
            } while (resultCount != 0);
        }

        /// <summary>
        /// This checks if the member can have his/her periodical account balance topped up (refreshed) or not, and if yes, affects the refresh.
        /// </summary>
        /// <param name="member"></param>
        public void RenewAccountBalanceBasedOnSubscription(MemberBase member, bool autoSave = true)
        {
            //If member is null, throw error as this can never happen!
            ContractsUtil.RequiresNotNullable(member, "Member can never be null!");

            //get the current subscription of the member
            MemberSubscriptionLinkBase subscriptionLink = (MemberSubscriptionLinkBase)member.GetCurrentSubscriptionLink();

            //if the user has a subscription, proceed, else do not top up and disable the next top up on feature
            if(subscriptionLink != null)
            {
                //if(member.NextRenewableAccountBalanceTopupOn >= Date.Now.GetEndOfDay())
                //{
                //    throw new InvalidOperationException(
                //        "This method should never be called if the member's next top-on date has not elapsed yet!");
                //}

                double accountBalanceDifference = subscriptionLink.GetSubscriptionTypePrice().MemberSubscriptionType.RenewableBalanceValue - member.RenewableAccountBalance;

                member.UpdateAccountBalance(accountBalanceDifference,
                                            Enums.MEMBER_ACCOUNT_BALANCE_HISTORY_UPDATE_TYPE.RenewalBasedOnSubscription,
                                            null,
                                            Enums.MEMBER_ACCOUNT_BALANCE_UPDATE_TYPE_BALANCE_CHANGE_TYPE.
                                            RenewableAccountBalance, autoSave:autoSave, order: subscriptionLink.Order);
                using (var t = autoSave ? nHibernateUtil.BeginTransactionFromSessionInCurrentContext() : null)
                {
                    member.NextRenewableAccountBalanceTopupOn =
                        Date.Now.AddDays(
                            ((MemberSubscriptionTypeBase) subscriptionLink.GetSubscriptionTypePrice().MemberSubscriptionType).
                                RenewableBalanceRefreshPeriodInDays);
                    member.Save();
                    if (autoSave)
                    {
                        t.Commit();
                    }
                }
            }
            else
            {
                using (var t = nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
                {
                    member.NextRenewableAccountBalanceTopupOn = null;
                    member.Save();
                    t.Commit();
                }
            }
        }
    }
}