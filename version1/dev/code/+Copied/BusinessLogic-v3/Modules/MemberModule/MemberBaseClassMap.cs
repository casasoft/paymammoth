using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.MemberModule
{

//BaseClassMap-File
    
    public abstract class MemberBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.MemberBaseMap_AutoGen<TData>
    	where TData : MemberBase
    {


        public override Enums.NHIBERNATE_CACHE_TYPE CacheType
        {
            get
            {
                return Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            }
            set
            {
                base.CacheType = value;
            }
        }

        protected override void FirstNameMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.LuceneInfo.MappedInLucene = true;
            base.FirstNameMappingInfo(mapInfo);
        }

        protected override void LastNameMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.LuceneInfo.MappedInLucene = true;
            base.LastNameMappingInfo(mapInfo);
        }

        protected override void MiddleNameMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.LuceneInfo.MappedInLucene = true;
            base.MiddleNameMappingInfo(mapInfo);
        }

        protected override void CompanyNameMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.LuceneInfo.MappedInLucene = true;
            base.CompanyNameMappingInfo(mapInfo);
        }

        protected override void CompanyProfileMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.LuceneInfo.MappedInLucene = true;
            base.CompanyProfileMappingInfo(mapInfo);
        }
    }
   
}
