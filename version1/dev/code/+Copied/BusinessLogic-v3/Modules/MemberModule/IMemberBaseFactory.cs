using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using Brickred.SocialAuth.NET.Core.BusinessObjects;

namespace BusinessLogic_v3.Modules.MemberModule
{   

//IBaseFactory-File

	public interface IMemberBaseFactory : BusinessLogic_v3.Modules._AutoGen.IMemberBaseFactoryAutoGen
    {

        IEnumerable<IMemberBase> GetMembersByUsernameOrEmail(string usernameOrEmail);
        bool CheckIfEmailExists(string Email, long? excludeMemberID = null);
        bool CheckIfUserExists(string Username);
	    IEnumerable<IMemberBase> GetFeaturedMembers(int amount);
        bool SubmitForgotPasswordEmail(string resetEmail);
        IMemberBase GetLoggedInUserForCurrentSession(bool getTemporaryUserIfNull = false);
	    //IMemberBase GetLoggedUserForCurrentSessionOrFromCookie();

        IMemberBase ActivateMemberByCode(string code, bool autoLogin = true);
    }
}
