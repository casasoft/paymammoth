﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule;
using BusinessLogic_v3.Util;
using BusinessLogic_v3.Modules.MemberSubscriptionLinkModule;
using CS.General_v3.Util;
using BusinessLogic_v3.Modules.OrderModule;
using BusinessLogic_v3.Classes.Text;
using BusinessLogic_v3.Modules.EmailTextModule;
using BusinessLogic_v3.Classes.Exceptions;
using BusinessLogic_v3.Modules.MemberModule.AccountBalanceManager;

namespace BusinessLogic_v3.Modules.MemberModule.SubscriptionManager
{
    public class MemberSubscriptionManager
    {
        #region Singleton
        private MemberSubscriptionManager()
        {

        }
        public static MemberSubscriptionManager Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<MemberSubscriptionManager>(); } }
        #endregion

        #region Purchase Subscription
        /// <summary>
        /// If member has no subscription, a new one is applied.
        /// If member has a subscription but new one is different, current one is ended and new one applies from today.
        /// If member has a subscription and new one is like it, it's put in a queue and once current one ends, new one applies.
        /// </summary>
        /// <param name="newMemberSubscriptionTypePrice"></param>
        /// <returns></returns>
        public bool PurchaseSubscription(MemberSubscriptionTypePriceBase subscriptionTypePrice, MemberBase member, bool isAutoRenewal, OrderBase order = null, bool autoSave = true)
        {
            if (subscriptionTypePrice != null && subscriptionTypePrice.DurationInDays > 0)
            {
                var currentMemberSubscription = member.GetCurrentSubscriptionLink();
                if (currentMemberSubscription != null)
                {
                    var currentSubscriptionTypePrice = currentMemberSubscription.GetSubscriptionTypePrice();
                    if (currentSubscriptionTypePrice == null)
                    {
                        throw new InvalidOperationException("This can never be null!");
                    }

                    if (currentSubscriptionTypePrice.ID != subscriptionTypePrice.ID)
                    {
                        //If a member is in a subscription, but not of the SubscriptionType
                        //Make the current one end as of yesterday
                        //Create a new one as of today, until the duration of the subscription

                        return cancelOldSubscriptionAndStartNewSubscription(member: member,
                                                                            subscriptionTypePrice: subscriptionTypePrice,
                                                                            order: order, isAutoRenewal: isAutoRenewal, autoSave: autoSave);
                    }
                    else
                    {
                        //If a member is in a subscription, the same as of this SubscriptionType
                        //Create a new one right after the end of this one, e.g if the purchase is for a
                        //3 month period, and the current one ends in 31/05/2012, make it from 01/06/2012 till 3 months later
                        //(call _purchaseSubscription, with date = end of current subscription)
                        return _purchaseSubscription(newMemberSubscriptionTypePrice: subscriptionTypePrice, member: member,
                        startDate: currentMemberSubscription.CurrentSubscriptionEndDate.AddDays(
                        1).GetStartOfDay(), order: order, isAutoRenewal: isAutoRenewal, subscriptionPurchaseCondition: Enums.MEMBER_SUBSCRIPTION_PURCHASE_CONDITION.Renewal, useTransaction: autoSave);
                    }
                }
                else
                {
                    //If a member is not currently in any subscription, just create a new subscription link from today,
                    //till duration of subscription
                    return _purchaseSubscription(newMemberSubscriptionTypePrice: subscriptionTypePrice, member: member,
                    startDate: Date.Now, isAutoRenewal: isAutoRenewal, order: order, subscriptionPurchaseCondition: Enums.MEMBER_SUBSCRIPTION_PURCHASE_CONDITION.NewSubscription, useTransaction: autoSave);
                }
                return false;
            }
            return false;
        }

        /// <summary>
        /// Returns the dates of a new subscription were it to take place today. No subscription is really created.
        /// </summary>
        /// <param name="member"></param>
        /// <param name="subscriptionTypePrice"></param>
        public void GetDatesOfNewSubscriptionIfItHadToTakePlace(MemberBase member, MemberSubscriptionTypePriceBase subscriptionTypePrice, out DateTime startDate, out DateTime endDate)
        {
            var currLink = member.GetCurrentSubscriptionLink();
            if(currLink == null || currLink.SubscriptionTypePrice.ID != subscriptionTypePrice.ID)
            {
                //new applies from today
                startDate = Date.Now;
                endDate = startDate.AddDays(subscriptionTypePrice.DurationInDays);
            }
            else
            {
                //extend
                startDate = currLink.CurrentSubscriptionEndDate.AddSeconds(1);
                endDate = startDate.AddDays(subscriptionTypePrice.DurationInDays);
            }
        }

        private bool cancelOldSubscriptionAndStartNewSubscription(MemberBase member, MemberSubscriptionTypePriceBase subscriptionTypePrice, OrderBase order, bool isAutoRenewal, bool autoSave = true)
        {
            var oldSubscription = member.GetCurrentSubscriptionLink();
            bool result = false;
            using (var t = autoSave ? nHibernateUtil.BeginTransactionFromSessionInCurrentContext() : null)
            {
                //cancel the old Subscription
                oldSubscription.CurrentSubscriptionEndDate = Date.Now.GetEndOfDay().AddDays(-1);
                oldSubscription.Save();
                result = _purchaseSubscription(newMemberSubscriptionTypePrice: subscriptionTypePrice, member: member,
                                               startDate: Date.Now, isAutoRenewal: isAutoRenewal, order: order,
                                               useTransaction: false, subscriptionPurchaseCondition: Enums.MEMBER_SUBSCRIPTION_PURCHASE_CONDITION.ChangeSubscriptionType);
                if (autoSave)
                {
                    t.Commit();
                }
            }
            return result;
        }

        private bool _purchaseSubscription(MemberSubscriptionTypePriceBase newMemberSubscriptionTypePrice, MemberBase member, DateTime startDate, bool isAutoRenewal, Enums.MEMBER_SUBSCRIPTION_PURCHASE_CONDITION subscriptionPurchaseCondition, OrderBase order = null, bool useTransaction = true)
        {
            if (newMemberSubscriptionTypePrice != null)
            {
                MemberSubscriptionLinkBase subscriptionLink = null;
                using (var t = useTransaction ? nHibernateUtil.BeginTransactionFromSessionInCurrentContext() : null)
                {
                    subscriptionLink = (MemberSubscriptionLinkBase)Factories.MemberSubscriptionLinkFactory.CreateNewItem();
                    subscriptionLink.CurrentSubscriptionStartDate = startDate.GetStartOfDay();
                    subscriptionLink.CurrentSubscriptionEndDate =
                        startDate.AddDays(newMemberSubscriptionTypePrice.DurationInDays).GetEndOfDay();
                    subscriptionLink.Member = member;
                    subscriptionLink.SubscriptionTypePrice = newMemberSubscriptionTypePrice;
                    subscriptionLink.Order = order;
                    if (order != null)
                    {
                        order.MemberSubscriptionLinks.Add(subscriptionLink);
                    }

                    member.MemberSubscriptionLinks.Add(subscriptionLink);
                    member.SetAutoRenewalSubscriptionTypePrice((isAutoRenewal ? newMemberSubscriptionTypePrice : null),
                                                               false);
                    subscriptionLink.Save();
                    member.ResetAllNotificationFlags(false);
                    member.Save();
                    if (useTransaction)
                    {
                        t.Commit();
                    }
                }
                MemberAccountBalanceManager.Instance.RenewAccountBalanceBasedOnSubscription(member, autoSave: useTransaction);
                sendEmailNotifications(subscriptionPurchaseCondition, isAutoRenewal, subscriptionLink, useTransaction);
                return true;
            }
            return false;
        }

        private Enums.EmailsBusinessLogic getEmailTextAccordingToSubscriptionPurchaseCondition(Enums.MEMBER_SUBSCRIPTION_PURCHASE_CONDITION subscriptionPurchaseCondition, bool isAutoRenewal)
        {
            Enums.EmailsBusinessLogic emailText;
            if (isAutoRenewal)
            {
                switch (subscriptionPurchaseCondition)
                {
                    case Enums.MEMBER_SUBSCRIPTION_PURCHASE_CONDITION.ChangeSubscriptionType:
                        emailText = BusinessLogic_v3.Enums.EmailsBusinessLogic.Subscriptions_ChangedSubscriptionType_InclAutoRenew;
                        break;
                    case Enums.MEMBER_SUBSCRIPTION_PURCHASE_CONDITION.NewSubscription:
                        emailText = BusinessLogic_v3.Enums.EmailsBusinessLogic.Subscriptions_NewSubscription_InclAutoRenew;
                        break;
                    case Enums.MEMBER_SUBSCRIPTION_PURCHASE_CONDITION.Renewal:
                        emailText = BusinessLogic_v3.Enums.EmailsBusinessLogic.Subscriptions_Renewal_InclAutoRenew;
                        break;
                    default:
                        throw new InvalidOperationException("Subscription Purchase Condition <" +
                                                            subscriptionPurchaseCondition + "> not implemented yet!");
                }
            }
            else
            {
                switch (subscriptionPurchaseCondition)
                {
                    case Enums.MEMBER_SUBSCRIPTION_PURCHASE_CONDITION.ChangeSubscriptionType:
                        emailText = BusinessLogic_v3.Enums.EmailsBusinessLogic.Subscriptions_ChangedSubscriptionType;
                        break;
                    case Enums.MEMBER_SUBSCRIPTION_PURCHASE_CONDITION.NewSubscription:
                        emailText = BusinessLogic_v3.Enums.EmailsBusinessLogic.Subscriptions_NewSubscription;
                        break;
                    case Enums.MEMBER_SUBSCRIPTION_PURCHASE_CONDITION.Renewal:
                        emailText = BusinessLogic_v3.Enums.EmailsBusinessLogic.Subscriptions_Renewal;
                        break;
                    default:
                        throw new InvalidOperationException("Subscription Purchase Condition <" +
                                                            subscriptionPurchaseCondition + "> not implemented yet!");
                }
            }
            return emailText;
        }

        private void sendEmailNotifications(Enums.MEMBER_SUBSCRIPTION_PURCHASE_CONDITION subscriptionPurchaseCondition, bool isAutoRenewal, MemberSubscriptionLinkBase subscriptionLink, bool useTransaction = true)
        {
            if (subscriptionLink != null && subscriptionLink.SubscriptionTypePrice != null && subscriptionLink.SubscriptionTypePrice.Price != 0)
            {
                Enum emailText = getEmailTextAccordingToSubscriptionPurchaseCondition(subscriptionPurchaseCondition,
                                                                                      isAutoRenewal);


                var emailDb = EmailTextBaseFactory.Instance.GetByIdentifier(emailText, useTransaction);
                TokenReplacerWithContentTagItem tokenReplacer = new TokenReplacerWithContentTagItem(emailDb);
                fillTokenReplacerWithTags(tokenReplacer, subscriptionLink);

                var emailMsg = emailDb.GetEmailMessage(tokenReplacerToReplaceTags: tokenReplacer);
                emailMsg.AddToEmails(subscriptionLink.Member.Email, subscriptionLink.Member.GetFullName());

                string fromEmail =
                    Factories.SettingFactory.GetSettingValue<string>(
                        CS.General_v3.Enums.SETTINGS_ENUM.Email_EmailAddresses_General_FromEmail);
                string fromName =
                    Factories.SettingFactory.GetSettingValue<string>(
                        CS.General_v3.Enums.SETTINGS_ENUM.Email_EmailAddresses_General_FromName);

                emailMsg.SetReplyToEmail(fromEmail, fromName);

                emailMsg.SendAsynchronously();
            }
        }

        private void fillTokenReplacerWithTags(TokenReplacerWithContentTagItem tokenReplacer, MemberSubscriptionLinkBase subscriptionLink)
        {
            const string tagPrefix = "[SubscriptionPurchase:";

            tokenReplacer[tagPrefix + "ClientCompanyName]"] = subscriptionLink.Member.CompanyName;
            tokenReplacer[tagPrefix + "ClientPersonalFullName]"] = subscriptionLink.Member.GetFullName();
            tokenReplacer[tagPrefix + "SubscriptionTypeName]"] =
                subscriptionLink.GetSubscriptionTypePrice().MemberSubscriptionType.Title;
            tokenReplacer[tagPrefix + "SubscriptionTypePriceName]"] =
                subscriptionLink.GetSubscriptionTypePrice().Title;
            tokenReplacer[tagPrefix + "SubscriptionTypePriceValue]"] =
                BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.FormatNumberWithCurrentCulture(
                    subscriptionLink.GetSubscriptionTypePrice().Price, NumberUtil.NUMBER_FORMAT_TYPE.Currency);
            tokenReplacer[tagPrefix + "SubscriptionStartDate]"] =
                BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.FormatDateTimeWithCurrentCulture(
                    subscriptionLink.CurrentSubscriptionStartDate, Date.DATETIME_FORMAT.LongDate);
            tokenReplacer[tagPrefix + "SubscriptionEndDate]"] =
                BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.FormatDateTimeWithCurrentCulture(
                    subscriptionLink.CurrentSubscriptionEndDate, Date.DATETIME_FORMAT.LongDate);
        }
        #endregion

        #region Subscription Checker

        /// <summary>
        /// Checks which subscriptions have expired, marks them as expired, sends emails, and if enabled also auto-renews. Intended to be executed on a background scheduled thread.
        /// </summary>
        public void CheckSubscriptions()
        {
            int queryChunkSize =
                Factories.SettingFactory.GetSettingValue<int>(
                    Enums.BusinessLogicSettingsEnum.SubscriptionCheckerQueryChunkSize);

            int pageNumber = 1;
            int resultCount = -1;

            do
            {
                var query = MemberSubscriptionLinkBase.Factory.GetQuery();
                query = query.Where(item => item.CurrentSubscriptionEndDate < Date.Now && !item.IsSubscriptionExpired);
                query.LimitQueryByPrimaryKeys(pageNumber, queryChunkSize);
                IEnumerable<MemberSubscriptionLinkBase> results = MemberSubscriptionLinkBase.Factory.FindAll(query);
                pageNumber++;
                resultCount = results.Count();
                _checkSubscriptions(results);
            } while (resultCount != 0);
        }

        private void _checkSubscriptions(IEnumerable<MemberSubscriptionLinkBase> subscriptionLinks)
        {
            foreach (MemberSubscriptionLinkBase subscriptionLink in subscriptionLinks)
            {
                MarkAsExpired(subscriptionLink);
            }
        }

        public void MarkAsExpired(IMemberSubscriptionLinkBase subscriptionLink)
        {
            if (!subscriptionLink.IsSubscriptionExpired && subscriptionLink.CurrentSubscriptionEndDate < Date.Now)
            {
                bool isLastSubscriptionLinkOfMember = !subscriptionLink.AreThereMoreLinksAfterThis();
                if (isLastSubscriptionLinkOfMember)
                {
                    bool markedAsExpired = false;

                    //check if can auto-renew
                    if (subscriptionLink.Member.AutoRenewSubscriptionTypePrice != null)
                    {
                        AutoRenewSubscription(subscriptionLink.GetMember());
                    }
                    else
                    {
                        _markAsExpired((MemberSubscriptionLinkBase)subscriptionLink);
                        markedAsExpired = true;
                        bool freeSubscriptionGiven = giveUserFreeSubscriptionIfAvailable(subscriptionLink.Member);
                        if (subscriptionLink.SubscriptionTypePrice.Price != 0 && freeSubscriptionGiven)
                        {
                            sendSubscriptionDowngradedEmail((MemberSubscriptionLinkBase) subscriptionLink);
                        }
                        else
                        {
                            sendSubscriptionExpiredEmail((MemberSubscriptionLinkBase) subscriptionLink);
                        }
                    }
                    if (!markedAsExpired)
                    {
                        _markAsExpired((MemberSubscriptionLinkBase)subscriptionLink);
                    }
                }
            }
            else
            {
                throw new InvalidOperationException(
                    "This method should never be called if the Subscription Link was already marked as expired or is not yet expired!");
            }
        }

        private bool giveUserFreeSubscriptionIfAvailable(IMemberBase member)
        {
            MemberSubscriptionTypePriceBase subscriptionTypePrice = (MemberSubscriptionTypePriceBase)Factories.MemberSubscriptionTypePriceFactory.GetFreeSubscriptionTypePrices().FirstOrDefault();
            if (subscriptionTypePrice != null)
            {
                member.PurchaseSubscription(subscriptionTypePrice, true);
                return true;
            }
            return false;
        }

        private void sendSubscriptionExpiredEmail(MemberSubscriptionLinkBase subscriptionLink)
        {
            Enum emailText = Enums.EmailsBusinessLogic.Subscriptions_Expired;


            var emailDb = EmailTextBaseFactory.Instance.GetByIdentifier(emailText);
            TokenReplacerWithContentTagItem tokenReplacer = new TokenReplacerWithContentTagItem(emailDb);
            fillTokenReplacerWithTags(tokenReplacer, subscriptionLink);

            var emailMsg = emailDb.GetEmailMessage(tokenReplacerToReplaceTags: tokenReplacer);
            emailMsg.AddToEmails(subscriptionLink.Member.Email, subscriptionLink.Member.GetFullName());

            string fromEmail = Factories.SettingFactory.GetSettingValue<string>(CS.General_v3.Enums.SETTINGS_ENUM.Email_EmailAddresses_General_FromEmail);
            string fromName = Factories.SettingFactory.GetSettingValue<string>(CS.General_v3.Enums.SETTINGS_ENUM.Email_EmailAddresses_General_FromName);

            emailMsg.SetReplyToEmail(fromEmail, fromName);

            emailMsg.SendAsynchronously();
        }

        private void sendSubscriptionDowngradedEmail(MemberSubscriptionLinkBase subscriptionLink)
        {
            Enum emailText = Enums.EmailsBusinessLogic.Subscriptions_Downgraded;

            var emailDb = EmailTextBaseFactory.Instance.GetByIdentifier(emailText);
            TokenReplacerWithContentTagItem tokenReplacer = new TokenReplacerWithContentTagItem(emailDb);
            fillTokenReplacerWithTags(tokenReplacer, subscriptionLink);

            var emailMsg = emailDb.GetEmailMessage(tokenReplacerToReplaceTags: tokenReplacer);
            emailMsg.AddToEmails(subscriptionLink.Member.Email, subscriptionLink.Member.GetFullName());

            string fromEmail = Factories.SettingFactory.GetSettingValue<string>(CS.General_v3.Enums.SETTINGS_ENUM.Email_EmailAddresses_General_FromEmail);
            string fromName = Factories.SettingFactory.GetSettingValue<string>(CS.General_v3.Enums.SETTINGS_ENUM.Email_EmailAddresses_General_FromName);

            emailMsg.SetReplyToEmail(fromEmail, fromName);

            emailMsg.SendAsynchronously();
        }

        private void _markAsExpired(MemberSubscriptionLinkBase subscriptionLink)
        {
            using (var t = nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
                subscriptionLink.IsSubscriptionExpired = true;
                subscriptionLink.Save();
                t.Commit();
            }
        }
        #endregion

        #region Auto-Renewal
        public bool AutoRenewSubscription(IMemberBase m)
        {
            if (m.AutoRenewSubscriptionTypePrice == null)
            {
                throw new AutoRenewNotAllowedException("Member does not have auto-renewal set!");
            }
            bool result = false;
            using (var t = nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
                //todo: 20120322 - Franco / Karl: This still needs to be done!
                m.AutoRenewLastMessage = "Not yet implemented!";
                m.Save();
                t.Commit();
                result = true;
            }
            return result;
        }
        #endregion
    }
}
