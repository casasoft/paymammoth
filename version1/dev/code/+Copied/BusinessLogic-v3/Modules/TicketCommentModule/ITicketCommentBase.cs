using BusinessLogic_v3.Modules.AttachmentModule;
using BusinessLogic_v3.Modules.TicketModule;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Util;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.TicketCommentModule
{

//IBaseClass-File
	
    public interface ITicketCommentBase : BusinessLogic_v3.Modules._AutoGen.ITicketCommentBaseAutoGen
    {
        void AddAttachment(IAttachmentBase attachment);
        IEnumerable<IAttachmentBase> GetAttachments();
        Enums.TICKET_COMMENT_TYPE GetCommentType();
        ITicketingSystemUser GetPostedByUser();
        void SetUser(ITicketingSystemUser user);
        ITicketBase GetTicket();
    }
}
