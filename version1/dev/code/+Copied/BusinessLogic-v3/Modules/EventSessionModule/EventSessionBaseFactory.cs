using BusinessLogic_v3.Classes.Routing;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Modules.EventSessionModule
{   

//BaseFactory-File

	public abstract class EventSessionBaseFactory : BusinessLogic_v3.Modules._AutoGen.EventSessionBaseFactoryAutoGen, IEventSessionBaseFactory
    {
        public IEnumerable<IEventSessionBase> GetUpcomingEventSessions(int eventSessionsAmount)
        {
            var q = GetQuery();
            q.Where(item => item.StartDate >= Date.Now);
            q.OrderBy(item => item.StartDate);
            q.Take(eventSessionsAmount);
            return FindAll(q);
        }
    }

}
