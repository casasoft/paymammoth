using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using CS.General_v3.Classes.HelperClasses;

namespace BusinessLogic_v3.Modules.EventSessionModule
{   

//IBaseFactory-File

	public interface IEventSessionBaseFactory : BusinessLogic_v3.Modules._AutoGen.IEventSessionBaseFactoryAutoGen
	{
        /// <summary>
        /// This method shall return the upcoming (soon to start) event sessions
        /// </summary>
        /// <param name="eventSessionsAmount"></param>
        /// <returns></returns>
        IEnumerable<IEventSessionBase> GetUpcomingEventSessions(int eventSessionsAmount);
	}

}
