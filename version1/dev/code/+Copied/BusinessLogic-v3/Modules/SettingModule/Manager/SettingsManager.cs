﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Concurrent;

namespace BusinessLogic_v3.Modules.SettingModule.Manager
{
    public class SettingsManager
    {

        public static SettingsManager Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<SettingsManager>(); } }

        private readonly object _lock_reloadFromDb = new object();
        private readonly object _lock_general = new object();


        private ConcurrentDictionary<string, SettingBase> _m_settings_inCache
        {
            get { return (ConcurrentDictionary<string, SettingBase>)System.Web.HttpRuntime.Cache["__SettingsManager_Values"]; }
            set { System.Web.HttpRuntime.Cache["__SettingsManager_Values"] = value; }
        }

        private ConcurrentDictionary<string, SettingBase> _settings 
        {
            get 
            {
                if (_m_settings_inCache == null)
                {
                    lock (_lock_general)
                    {
                        if (_m_settings_inCache == null)
                        {//if the settings was not in cache, create one and reload
                            _m_settings_inCache = new ConcurrentDictionary<string, SettingBase>();
                            ReloadFromDatabase();
                        }

                    }

                }
                return _m_settings_inCache;
                
            }
        }
        

        private string getKey(string identifier)
        {
            string key = identifier ?? "";
            key = key.ToLower();
            return key;
        }

        private SettingsManager()
        {
            
            StartMonitoringSettingsUpdate();
        }

        public void StopMonitoringSettingsUpdate()
        {

            if (currentlyMonitoring)
            {
                lock (_lock_general)
                {
                    if (currentlyMonitoring)
                    {
                        currentlyMonitoring = false;
                        SettingBaseFactory.Instance.OnItemUpdate -= new Classes.DbObjects.General.OnItemUpdateHandler(Instance_OnItemUpdate);
                    }
                }
            }
        }
        private bool currentlyMonitoring = false;
        public void StartMonitoringSettingsUpdate()
        {
            if (!currentlyMonitoring)
            {
                lock (_lock_general)
                {
                    if (!currentlyMonitoring)
                    {
                        currentlyMonitoring = true;

                        SettingBaseFactory.Instance.OnItemUpdate += new Classes.DbObjects.General.OnItemUpdateHandler(Instance_OnItemUpdate);
                    }
                }
            }
        }

        void Instance_OnItemUpdate(Classes.DbObjects.Objects.IBaseDbObject item, Enums.UPDATE_TYPE updateType)
        {
            
            SettingBase setting = (SettingBase) item;
            switch (updateType)
            {
                case Enums.UPDATE_TYPE.CreateNew:
                case Enums.UPDATE_TYPE.Update:
                    {
                        loadSetting(setting);
                        break;
                    }
                case Enums.UPDATE_TYPE.Delete:
                    {
                        
                        string key = getKey(setting.Identifier);
                        SettingBase removedItem = null;
                        this._settings.TryRemove(key, out removedItem);
                        break;
                    }
                default:
                    throw new InvalidOperationException("Update Type <" + updateType + "> not mapped");

            }
            
        }

        public SettingBase GetSetting(string identifier)
        {
            SettingBase result = null;
            
            //lock (_lock_reloadFromDb)
            //{
            //    if (!initialLoadedFromDb)
            //    {//if never loaded from database, load.
            //        ReloadFromDatabase();
            //    }
           
                
            //}
            string key = getKey(identifier);
            {
                SettingBase value = null;
                
                bool containsKey = _settings.TryGetValue(key, out value);

                if (containsKey)
                {
                    result = value;
                }
                else
                {
                    //for some reason, the cache does not contain this key

                    ReloadFromDatabase(); //reload all from database

                    containsKey = _settings.TryGetValue(key, out value); //retry to get
                    
                    

                }
                if (!containsKey) value = null;

            }
            

            return result;
        }

        private bool initialLoadedFromDb = false;
        private void loadSetting(SettingBase s)
        {
            string key = getKey(s.Identifier);
            _settings[key] = s;
        }

        private bool _reloadingFromDb = false;
        public void ReloadFromDatabase()
        {
            if (_reloadingFromDb)
            {
                while (_reloadingFromDb)
                {
                    System.Threading.Thread.Sleep(50);
                }
            }
            else
            {
                lock (_lock_reloadFromDb)
                {
                    _reloadingFromDb = true;
                    _settings.Clear();
                    var session = BusinessLogic_v3.Util.nHibernateUtil.CreateNewSession();
                    //using (var t = session.BeginTransaction())
                    {
                        var settingList = SettingBaseFactory.Instance.FindAll(session);
                        foreach (var s in settingList)
                        {
                            loadSetting(s);
                        }
                    }
                    session.Dispose();
                    //BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();
                    initialLoadedFromDb = true;
                    _reloadingFromDb = false;
                }
            }

        }


    }
}
