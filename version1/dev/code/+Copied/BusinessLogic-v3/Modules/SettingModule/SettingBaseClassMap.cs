using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.SettingModule
{

//BaseClassMap-File
    
    public abstract class SettingBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.SettingBaseMap_AutoGen<TData>
    	where TData : SettingBase
    {

        public override Enums.NHIBERNATE_CACHE_TYPE CacheType
        {
            get
            {
                return Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            }
            set
            {
                base.CacheType = value;
            }
        }

        protected override void IdentifierMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.IsIndexed = true;
            mapInfo.Length = 250;
            base.IdentifierMappingInfo(mapInfo);
        }
    }
   
}
