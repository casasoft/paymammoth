using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;

namespace BusinessLogic_v3.Modules.SettingModule
{   

//IBaseFactory-File

	public interface ISettingBaseFactory : BusinessLogic_v3.Modules._AutoGen.ISettingBaseFactoryAutoGen
    {

        ISettingBase GetSetting(Enum identifier, bool createIfNotExists = true);
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="identifier"></param>
        /// <returns></returns>
        TReturnType GetSettingValue<TReturnType>(Enum identifier, bool createIfNotExists = true);
        TReturnType GetSettingValueByStringIdentifier<TReturnType>(string identifier, CS.General_v3.Classes.Attributes.SettingsInfoAttribute defaultValues, bool createIfNotExists = true);


        string GetAddressAsOneLine(string delimiter = ", ", bool includeCompanyName = true);


        void SetSettingByStringIdentifier(string settingName, object value, CS.General_v3.Classes.Attributes.SettingsInfoAttribute defaultValuesdefaultValues);
        void SetSetting(Enum identifier, object value);

        void MarkAllSettingsAsNotUsedInProject();


        void DeleteDuplicateSettings();
    }

}
