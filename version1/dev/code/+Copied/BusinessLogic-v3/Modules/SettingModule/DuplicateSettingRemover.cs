﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.SettingModule.Manager;
using Iesi.Collections.Generic;

namespace BusinessLogic_v3.Modules.SettingModule
{
    public class DuplicateSettingRemover
    {


        private Dictionary<string, HashedSet<SettingBase>> allSettings = new Dictionary<string, HashedSet<SettingBase>>();
        private void addSetting(SettingBase s)
        {
            string identifier = (s.Identifier ?? "").ToLower();
            HashedSet<SettingBase> list = null;
            if (!allSettings.TryGetValue(identifier, out list))
            {
                list = new HashedSet<SettingBase>();
                allSettings[identifier] =list;
            }
            list.Add(s);
            
        }

        private void loadSettings()
        {
            var settings = SettingBaseFactory.Instance.FindAll().ToList();
            foreach (var s in settings)
            {
                addSetting(s);
            }
        }

        private SettingBase getFirstItemWithNonDefaultValue(IEnumerable<SettingBase> list)
        {
            foreach (var item in list)
            {
                string value = item.Value ?? "";
                value = value.ToLower();
                if (value != "0" && !string.IsNullOrWhiteSpace(value))
                {
                    return item;
                }
            }
            return list.FirstOrDefault();
        }

        private void parseSettings()
        {
            foreach (var settingList in allSettings.Values)
            {
                SettingBase nonDefaultValue = getFirstItemWithNonDefaultValue(settingList);
                for (int i = 0; i < settingList.Count; i++)
                {
                    var s = settingList.ElementAt(i);
                    if (!s.Equals(nonDefaultValue))
                    {
                        s.Delete();
                        settingList.Remove(s);
                        i--;
                    }
                }
            }
        }

        public void DeleteDuplicateSettings()
        {
            SettingsManager.Instance.StopMonitoringSettingsUpdate();
            BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();


            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
                loadSettings();
                parseSettings();
                

                t.Commit();

            }

            BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();
            SettingsManager.Instance.StartMonitoringSettingsUpdate();
            SettingsManager.Instance.ReloadFromDatabase();
        }
    }
}
