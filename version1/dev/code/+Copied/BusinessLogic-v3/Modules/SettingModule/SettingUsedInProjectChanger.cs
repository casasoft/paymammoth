﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.SettingModule.Manager;

namespace BusinessLogic_v3.Modules.SettingModule
{
    public class SettingUsedInProjectChanger
    {

        public SettingUsedInProjectChanger(long id, bool usedInProjectValue)
        {
            this.Id = id;
            this.UsedInProjectValue = usedInProjectValue;
        }


        public long Id { get; set; }
        public bool UsedInProjectValue { get; set; }

        private static readonly object _lock = new object();

        private void _start()
        {
            lock (_lock)
            { //only one item at a time
                SettingsManager.Instance.StopMonitoringSettingsUpdate();

                bool ok = false;
                do
                {
                    var session = BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();


                    try
                    {

                        var setting = SettingBaseFactory.Instance.GetByPrimaryKey(this.Id, session);
                        if (UsedInProjectValue)
                            setting.MarkAsUsedInProject();
                        else
                            setting.MarkAsNotUsedInProject();

                        session.Flush();
                        ok = true;
                    }
                    catch (NHibernate.StaleObjectStateException ex)
                    {
                        ok = false;
                        System.Threading.Thread.Sleep(CS.General_v3.Util.Random.GetInt(500, 1000));
                    }


                    BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();
                } while (!ok);

                SettingsManager.Instance.StartMonitoringSettingsUpdate();
            }

            //session.Dispose();
        }

        public void Start()
        {
            CS.General_v3.Util.ThreadUtil.CallMethodOnSeperateThread(_start, "SettingUsedInProjectChanger-" + this.Id, System.Threading.ThreadPriority.Lowest);
        }


    }
}
