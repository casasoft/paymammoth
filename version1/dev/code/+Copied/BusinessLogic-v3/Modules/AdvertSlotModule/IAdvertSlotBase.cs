using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Modules.AdvertModule;

namespace BusinessLogic_v3.Modules.AdvertSlotModule
{

//IBaseClass-File
	
    public interface IAdvertSlotBase : BusinessLogic_v3.Modules._AutoGen.IAdvertSlotBaseAutoGen
    {

        IAdvertBase GetNextAdvertToShow();

        bool HasAdvertsToShow();
    }
}
