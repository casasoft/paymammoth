using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.AdvertSlotModule
{

//BaseClassMap-File
    
    public abstract class AdvertSlotBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.AdvertSlotBaseMap_AutoGen<TData>
    	where TData : AdvertSlotBase
    {

        public override Enums.NHIBERNATE_CACHE_TYPE CacheType
        {
            get
            {
                return Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            }
            set
            {
                base.CacheType = value;
            }
        }
        protected override void AdvertsMappingInfo(ICollectionMapInfo mapInfo)
        {
            mapInfo.CacheType = Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            base.AdvertsMappingInfo(mapInfo);
        }
        protected override void IdentifierMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.IsIndexed = true;
            base.IdentifierMappingInfo(mapInfo);
        }
 
    }
   
}
