using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Modules.MemberModule;

namespace BusinessLogic_v3.Modules.FileItemModule
{   

//IBaseFactory-File

	public interface IFileItemBaseFactory : BusinessLogic_v3.Modules._AutoGen.IFileItemBaseFactoryAutoGen
    {

        IEnumerable<IFileItemBase> GetFiles(int pageNo, int pageSize, BusinessLogic_v3.Enums.FILE_ITEM_SORT_BY sortBy, out int totalResults);

        IEnumerable<IFileItemBase> GetFiles(IMemberBase member, int pageNo, int pageSize, BusinessLogic_v3.Enums.FILE_ITEM_SORT_BY sortBy, out int totalResults);

	    IEnumerable<IFileItemBase> GetFiles(BusinessLogic_v3.Enums.FILE_ITEM_SORT_BY sortBy, out int totalResults);

    }

}
