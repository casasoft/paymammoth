using BusinessLogic_v3.Classes.Searches;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.Searches.FileItemSearcher;

namespace BusinessLogic_v3.Modules.FileItemModule
{   

//BaseFactory-File

    public abstract class FileItemBaseFactory : BusinessLogic_v3.Modules._AutoGen.FileItemBaseFactoryAutoGen, IFileItemBaseFactory
    {

        public IEnumerable<IFileItemBase> GetFiles(int pageNo, int pageSize, BusinessLogic_v3.Enums.FILE_ITEM_SORT_BY sortBy, out int totalResults)
        {
            FileItemSearchParams fileItemSearchParams = FileItemSearchParams.CreateLastTypeInstance();
            fileItemSearchParams.PageNo = pageNo;
            fileItemSearchParams.ShowAmt = pageSize;
            fileItemSearchParams.SortBy = sortBy;
            FileItemSearcher fileItemSearcher = FileItemSearcher.CreateLastTypeInstance();
            fileItemSearcher.FileItemSearchParameters = fileItemSearchParams;
            var items = fileItemSearcher.GetSearchResults();

            totalResults = fileItemSearcher.TotalResults;

            return items;
        }

        public IEnumerable<IFileItemBase> GetFiles(BusinessLogic_v3.Enums.FILE_ITEM_SORT_BY sortBy, out int totalResults)
        {
            FileItemSearchParams fileItemSearchParams = FileItemSearchParams.CreateLastTypeInstance();
            fileItemSearchParams.SortBy = sortBy;
            FileItemSearcher fileItemSearcher = FileItemSearcher.CreateLastTypeInstance();
            fileItemSearcher.FileItemSearchParameters = fileItemSearchParams;
            var items = fileItemSearcher.GetSearchResults();
            totalResults = fileItemSearcher.TotalResults;
            return items;
        }

        public IEnumerable<IFileItemBase> GetFiles(MemberModule.IMemberBase member, int pageNo, int pageSize, Enums.FILE_ITEM_SORT_BY sortBy, out int totalResults)
        {
            FileItemSearchParams fileItemSearchParams = FileItemSearchParams.CreateLastTypeInstance();
            fileItemSearchParams.PageNo = pageNo;
            fileItemSearchParams.ShowAmt = pageSize;
            fileItemSearchParams.SortBy = sortBy;
            FileItemSearcher fileItemSearcher = FileItemSearcher.CreateLastTypeInstance();
            fileItemSearcher.FileItemSearchParameters = fileItemSearchParams;
            var items = fileItemSearcher.GetSearchResults();

            totalResults = fileItemSearcher.TotalResults;

            return items;
        }
    }
}
