using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Modules.ProductVariationModule
{   

//BaseFactory-File

	public abstract class ProductVariationBaseFactory : BusinessLogic_v3.Modules._AutoGen.ProductVariationBaseFactoryAutoGen, IProductVariationBaseFactory
    {



        public ProductVariationBase GetByBarcode(string barCode)
        {
            var q = GetQuery();
            q = q.Where(x => x.Barcode == barCode);
            return FindItem(q);
            
        }
    }

}
