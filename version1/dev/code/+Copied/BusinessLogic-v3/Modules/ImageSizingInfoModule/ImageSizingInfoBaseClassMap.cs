using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.ImageSizingInfoModule
{

//BaseClassMap-File
    
    public abstract class ImageSizingInfoBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.ImageSizingInfoBaseMap_AutoGen<TData>
    	where TData : ImageSizingInfoBase
    {

        public override Enums.NHIBERNATE_CACHE_TYPE CacheType
        {
            get
            {
                return Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            }
            set
            {
                base.CacheType = value;
            }
        }   
 
    }
   
}
