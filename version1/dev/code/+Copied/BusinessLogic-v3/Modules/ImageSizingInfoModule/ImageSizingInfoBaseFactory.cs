using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Modules.ImageSizingInfoModule
{   

//BaseFactory-File

	public abstract class ImageSizingInfoBaseFactory : BusinessLogic_v3.Modules._AutoGen.ImageSizingInfoBaseFactoryAutoGen, IImageSizingInfoBaseFactory
    {



        public ImageSizingInfoBase GetByIdentifier(string identifier)
        {
            var q = GetQuery();
            q.Cacheable();
            q = q.Where(item => item.Identifier == identifier);
            return FindItem(q);
        }
    }

}
