using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;

namespace BusinessLogic_v3.Modules.MemberReferralModule
{   

//IBaseFactory-File

	public interface IMemberReferralBaseFactory : BusinessLogic_v3.Modules._AutoGen.IMemberReferralBaseFactoryAutoGen
    {

        IMemberReferralBase GetReferralBySentMemberIDAndEmail(long sentMemberID, string email);
        void InvalidateOtherSimilarReferrals(long? referralID, string email);

    }

}
