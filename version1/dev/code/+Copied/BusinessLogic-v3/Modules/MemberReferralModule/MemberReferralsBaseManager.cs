﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.MemberModule;


namespace BusinessLogic_v3.Modules.MemberReferralModule
{
    public class MemberReferralsBaseManager
    {

        public static MemberReferralsBaseManager Instance
        {
            get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<MemberReferralsBaseManager>(); }
        }

        public static long? CurrentQueryStringReferralId
        {
            get
            {
                
                return
                    CS.General_v3.Util.QueryStringUtil.GetLongFromQS(
                        BusinessLogic_v3.Modules.ModuleSettings.MemberReferrals.ReferralQueryStringParamName);
            }
            
        }
        public static long? CurrentSessionReferralId
        {
            get { return CS.General_v3.Util.PageUtil.GetSessionObject<long?>(ModuleSettings.MemberReferrals.ReferralCookieName_Id); }
            set { CS.General_v3.Util.PageUtil.SetSessionObject(ModuleSettings.MemberReferrals.ReferralCookieName_Id, value); }
        }
        public static string CurrentSessionReferral_ReferrerUrl
        {
            get { return CS.General_v3.Util.PageUtil.GetSessionObject<string>(ModuleSettings.MemberReferrals.ReferralCookieName_Url); }
            set { CS.General_v3.Util.PageUtil.SetSessionObject(ModuleSettings.MemberReferrals.ReferralCookieName_Url, value); }
        }
        public static long? CurrentCookieReferralId
        {
            get { return CS.General_v3.Util.PageUtil.GetCookieLongNullable(ModuleSettings.MemberReferrals.ReferralCookieName_Id); }
            set
            {
                DateTime expDate = CS.General_v3.Util.Date.Now;
                expDate = expDate.AddDays(ModuleSettings.MemberReferrals.DaysToStoreReferralCookie);
                CS.General_v3.Util.PageUtil.SetCookie(ModuleSettings.MemberReferrals.ReferralCookieName_Id, value.ToString(), expDate);
            }
        }
        public static string CurrentCookieReferral_ReferrerUrl
        {
            get { return CS.General_v3.Util.PageUtil.GetCookie(ModuleSettings.MemberReferrals.ReferralCookieName_Url); }
            set
            {
                DateTime expDate = CS.General_v3.Util.Date.Now;
                expDate = expDate.AddDays(ModuleSettings.MemberReferrals.DaysToStoreReferralCookie);
                CS.General_v3.Util.PageUtil.SetCookie(ModuleSettings.MemberReferrals.ReferralCookieName_Url, value.ToString(), expDate);
            }
        }
        //public bool CheckCurrentUrl()
        //{
        //    long? refID = CS.General_v3.Util.QueryStringUtil.GetLongFromQS(BusinessLogic_v3.Modules.ModuleSettings.MemberReferrals.ReferralQueryStringParamName);
        //    if (refID.HasValue)
        //    {
        //        CS.General_v3.Util.SessionUtil.SetObject(BusinessLogic_v3.Modules.ModuleSettings.MemberReferrals.ReferralCookieName, refID.Value);
        //        return true;
        //    }
        //    return false;
        //}

        public void CheckForReferringMember()
        {

            if (ModuleSettings.MemberReferrals.Enabled)
            {
                if (CurrentQueryStringReferralId.HasValue)
                {
                    CurrentSessionReferralId = CurrentQueryStringReferralId;
                    CurrentSessionReferral_ReferrerUrl = CS.General_v3.Util.PageUtil.GetCurrentRequest().UrlReferrer.AbsoluteUri;
                    
                    if (ModuleSettings.MemberReferrals.UseCookiesToStoreReferral)
                    {
                        CurrentCookieReferralId = CurrentQueryStringReferralId;
                        CurrentCookieReferral_ReferrerUrl = CurrentSessionReferral_ReferrerUrl;
                    }
                    CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass();
                    url.Remove(BusinessLogic_v3.Modules.ModuleSettings.MemberReferrals.ReferralQueryStringParamName);
                    url.RedirectTo();
                }
            }
        }
        protected long? getCurrentReferringMemberId()
        {
            long? result = null;

            if (ModuleSettings.MemberReferrals.Enabled)
            {
                if (CurrentQueryStringReferralId.HasValue)
                    result = CurrentQueryStringReferralId.Value;
                else if (CurrentSessionReferralId.HasValue)
                    result = CurrentSessionReferralId.Value;
                else
                {
                    if (ModuleSettings.MemberReferrals.UseCookiesToStoreReferral && CurrentCookieReferralId.HasValue)
                    {
                        result = CurrentCookieReferralId;
                    }
                }
            }
            return result;
        }
        public string GetCurrentReferring_ReferralUrl()
        {

            string s = null;
            if (ModuleSettings.MemberReferrals.Enabled)
            {
                if (!string.IsNullOrEmpty(CurrentSessionReferral_ReferrerUrl))
                {
                    s = CurrentSessionReferral_ReferrerUrl;
                }
                else
                {
                    if (ModuleSettings.MemberReferrals.UseCookiesToStoreReferral && CurrentCookieReferralId.HasValue)
                    {
                        s = CurrentCookieReferral_ReferrerUrl;
                    }
                }
            }
            return s;
        }

        public IMemberBase GetCurrentReferringMember()
        {
            long? refID = getCurrentReferringMemberId();
            if (refID.HasValue)
            {
                IMemberBase referringMember = MemberBase.Factory.GetByPrimaryKey(refID.Value);
                if (referringMember != null)
                {
                    return referringMember;
                }
                
            }
            return null;
        }
    }
}
