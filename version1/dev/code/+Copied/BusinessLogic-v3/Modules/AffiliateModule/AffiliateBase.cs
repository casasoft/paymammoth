using BusinessLogic_v3.Classes.MediaItems;
using BusinessLogic_v3.Classes.MediaItems.Impl;
using BusinessLogic_v3.Modules.ArticleModule;
using BusinessLogic_v3.Modules.CmsUserModule;

using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Modules.AffiliateModule
{

//BaseClass-File
	
    public abstract class AffiliateBase : BusinessLogic_v3.Modules._AutoGen.AffiliateBaseAutoGen, IAffiliateBase
    {


        public AffiliateBase()
        {
            //Any fields auto-filled here can serve as default values,
            //as if it is loaded from the database they will be auto-overridden later on 
            //when they are filled in!

        }

        public override string ToString()
        {
            return this.Title;
        }

        public override string Username
        {
            get
            {
                return base.Username;
            }
            set
            {
                base.Username = value;
                //updateCmsUser_Username(value);

            }
        }

        private void updateCmsUser_Username(string newValue)
        {

            if (!this.IsTransient())
            {
                CmsUserBase affiliateCmsUser = this.LinkedCmsUser;
                if (affiliateCmsUser == null || string.Compare(affiliateCmsUser.Username, newValue, true) != 0)
                {
                    var existingCmsUser = CmsUserBase.Factory.GetCmsUserByUsername(newValue, this.LinkedCmsUser);
                    if (existingCmsUser != null)
                    {


                        throw new PropertyUpdateException("Cannot set username to <" + newValue + "> as another user already has the same username");
                    }
                    else
                    {
                        affiliateCmsUser = CmsUserBase.Factory.GetCmsUserForAffiliate(this, true);
                    }
                    affiliateCmsUser.Username = newValue;

                    affiliateCmsUser.UpdateAndCommit();
                }
                // all ok 


            }

            base.Username = newValue;



        }

        private static readonly object _padlock = new object();
        private void checkCodes()
        {
            if (string.IsNullOrEmpty(this.ReferralCode))
            {
                lock (_padlock)
                {
                    bool ok = false;
                    string code = null;

                    while (!ok)
                    {
                        if (string.IsNullOrEmpty(this.ReferralCode))
                        {
                            code = CS.General_v3.Util.Random.GetAlpha(8, 8);
                            var existingAffiliate = AffiliateBaseFactory.Instance.GetAffiliateByReferralCode(code);
                            if (existingAffiliate == null)
                            {
                                ok = true; // affiliate does not exist, ok!

                            }
                        }
                        else
                        {
                            ok = true;
                        }
                    }

                    this.ReferralCode = code;
                    // this.Save();
                }
            }

        }
        protected override void Save_Before(OperationResult result, SaveBeforeAfterParams sParams)
        {
            if (sParams == null) sParams = new SaveBeforeAfterParams();
            checkCodes();
            if (sParams.SavePermanently)
            {
                updateCmsUserDetails();
                checkAffiliateContentPageNode(autoSaveInDb: false);
            }
            base.Save_Before(result, sParams);
        }



        public ArticleBase GetAffiliateContentPageNode(bool createIfNotExists, bool autoSaveInDb = true)
        {
            if (this.LinkedContentPageNode == null)
            {
                this.LinkedContentPageNode = createAffiliateContentPageNode();
                if (autoSaveInDb)
                    this.Save();
            }
            return this.LinkedContentPageNode;
        }
        private ArticleBase createAffiliateContentPageNode()
        {

            var rootAffiliateNode = ArticleBaseFactory.Instance.GetContentPageByIdentifier(Enums.CONTENT_PAGE_IDENTIFIER.AffiliateContentPages);

            //ContentPageBase item =  (ContentPageBase)rootAffiliateNode.AddNewChildContentPage();
            ArticleBase item = ArticleBase.Factory.CreateNewItem();

            //item.SetIdentifier(Enums.ContentPagesBusinessLogic.AffiliateContentPages);

            item.PageTitle = this.Title;
            item.SetAllAccessRequiredTo(CS.General_v3.Enums.CMS_ACCESS_TYPE.RestrictedUser);
            item.AllowAddSubChildren_AccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Normal;
            item.AllowDelete_AccessRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Normal;
            item.ExcludeFromRewriteUrl = true;
            item.Published = false;
            item.CreateAndCommit();
            rootAffiliateNode.AddChildArticle(item);
            this.LinkedContentPageNode = item;
            return item;
        }

        private void checkAffiliateContentPageNode(bool autoSaveInDb = true)
        {
            var item = GetAffiliateContentPageNode(true, autoSaveInDb: autoSaveInDb);


        }


        private void updateCmsUserDetails()
        {
            updateCmsUser_Username(this.Username);


            var cmsUser = this.LinkedCmsUser;

            cmsUser.AccessType = CS.General_v3.Enums.CMS_ACCESS_TYPE.RestrictedUser;

            cmsUser.Password = this.Password;
            cmsUser.Name = this.Username;
            cmsUser.Update();


        }
        private OperationResult deleteLinkedContentPageNode()
        {
            OperationResult result = new OperationResult();
            if (this.LinkedContentPageNode != null)
            {
                var cp = this.LinkedContentPageNode;
                if (cp != null)
                {

                    result = cp.Delete();

                }
                this.LinkedContentPageNode = null;
                this.Save();
            }
            return result;
        }
        protected override void Delete_Before(OperationResult result, DeleteBeforeAfterParams delParams)
        {

            if (delParams == null) delParams = new DeleteBeforeAfterParams();
            base.Delete_Before(result, delParams);
            var cmsUser = CmsUserBase.Factory.GetCmsUserForAffiliate(this, false);
            if (delParams.DeletePermanently && cmsUser != null)
            {
                this.LinkedCmsUser = null;


                result.AddFromOperationResult(cmsUser.DeleteAndCommit());
            }
        }


        













        

			

        public override string AffiliateWhiteLabelBaseUrl
        {
            get
            {

                return base.AffiliateWhiteLabelBaseUrl;
            }
            set
            {
                string url = CS.General_v3.Util.UrlUtil.ConvertTextToHttpLink(value);
                base.AffiliateWhiteLabelBaseUrl = url;
            }
        }

        public string GetAffiliateFrontpageUrl()
        {

            var url  = CS.General_v3.Util.PageUtil.GetApplicationBaseUrl();
            
            url[BusinessLogic_v3.Settings.GetSettingFromDatabase(Enums.BusinessLogicSettingsEnum.ModuleSettings_Affiliates_Querystring_ParamName)] = this.ReferralCode;
            return url.GetURL(fullyQualified: true);

        }


        public bool CheckIfActivated()
        {



            return !this.Deleted &&
                this.Published &&
                (this.ActiveFrom.Date.IsBeforeNow()) &&

                !this._Temporary_Flag;

        }



        #region Logo Media Item Snippet (v3)

        // Logo : This should be the name of the image, e.g MainPhoto, etc. 
        // AffiliateBase : This should be the name of the class that is implementing this snippet, e.g EventBase or Member

        public enum LogoSizingEnum
        {
            [ImageSpecificSizeDefaultValues(Width = 210, Height = 150, CropType = CS.General_v3.Enums.CROP_IDENTIFIER.None)]
            Thumbnail,

            [ImageSpecificSizeDefaultValues(Width = 1600, Height = 1200, CropType = CS.General_v3.Enums.CROP_IDENTIFIER.None)]
            Normal
        }

        public class LogoMediaItem : BaseMediaItemImage<LogoSizingEnum>
        {
            /* If you need to update SetAsMainItem, Priority, etc, these are fields/methods that can be overriden in the base class */


            public AffiliateBase Item { get; private set; }
            public LogoMediaItem(AffiliateBase item)
            {

                this.Item = item;

                MediaItemImageInfo imageInfo = new MediaItemImageInfo();
                imageInfo.InitByDb(this.Item, typeof(LogoSizingEnum),
                                        p => p.LogoFilename,  //this should be the filename field
                                        () => item.Title,  //this should be the title field.  Note that this is optional.  This is only used to generate the filename from, to include the title for Google Images purposes 
                                        "Affiliate - Logo"); //this should be the identifier for this image, and what will as the title in the Cms/Sizing Info, so make it nice and understandable :)

                imageInfo.DeleteDbItemOnRemove = false; //if you want that on delete of this image, the item is deleted, set this as true.

                this.MediaItemInfo = imageInfo;

            }



        }

        protected LogoMediaItem _m_Logo = null;
        public LogoMediaItem Logo { get { if (_m_Logo == null) _m_Logo = new LogoMediaItem(this); return _m_Logo; } }



        #endregion

        #region Css File

        // Css : This should be the name of the file type, e.g Css
        // AffiliateBase     : This should be the name of the class that is implementing this snippet

        public class CssMediaItem : BaseMediaItemFile
        {
            public AffiliateBase Item { get; private set; }
            public CssMediaItem(AffiliateBase item)
            {
                this.Item = item;

                MediaItemInfoImpl mediaItemInfo = new MediaItemInfoImpl();

                mediaItemInfo.InitByDb(this.Item,
                    p => p.CssFilename,
                    () => item.Title, //title that should be used for saving the file
                    "Affiliate - Css");

                this.MediaItemInfo = mediaItemInfo;
                this.MediaItemInfo.DeleteDbItemOnRemove = false; //if you want that on delete of this image, the item is deleted, set this as true.

            }

        }


        private CssMediaItem _Css = null;
        public CssMediaItem Css
        {
            get
            {
                if (_Css == null)
                    _Css = new CssMediaItem(this);
                return _Css;
            }
        }

        #endregion
		
    
    }
}
