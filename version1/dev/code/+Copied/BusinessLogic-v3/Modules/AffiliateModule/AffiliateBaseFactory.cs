using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Modules.AffiliateModule
{   

//BaseFactory-File

	public abstract class AffiliateBaseFactory : BusinessLogic_v3.Modules._AutoGen.AffiliateBaseFactoryAutoGen, IAffiliateBaseFactory
    {

        public AffiliateBase GetAffiliateByReferralCode(string refCode)
        {
            var q = AffiliateBase.Factory.GetQuery();
            q.Cacheable();
            q.Where(item => item.ReferralCode == refCode);
            return FindItem(q);

        }

        public override void OnPostInitialisation()
        {
            base.OnPostInitialisation();
            this.RegisterFactoryWithController();
        }

     
        #region IAffiliateBaseDataFactory Members


        public AffiliateBase GetAffiliateByHostHeader(string domain)
        {
            var q = GetQuery();
            q.Cacheable();
            q = q.Where(item => item.Published);
            q = q.WhereRestrictionOn(item => item.AffiliateWhiteLabelBaseUrl).IsLike("%/" + domain + "/%", MatchMode.Anywhere);
            //q.Left.JoinQueryOver<WhiteLabelAffiliateHostHeaderBase>(item => item.HostHeaders).Where(hh => hh.HostHeader == domain);

            return FindItem(q);
        }

        #endregion

		

    }

}
