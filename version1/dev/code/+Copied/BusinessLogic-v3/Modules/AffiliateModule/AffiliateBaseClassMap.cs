using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.AffiliateModule
{

//BaseClassMap-File
    
    public abstract class AffiliateBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.AffiliateBaseMap_AutoGen<TData>
    	where TData : AffiliateBase
    {
        public override Enums.NHIBERNATE_CACHE_TYPE CacheType
        {
            get
            {
                return Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            }
            set
            {
                base.CacheType = value;
            }
        }
        protected override void ReferralCodeMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.IsIndexed = true;
            mapInfo.Length = 100;
            base.ReferralCodeMappingInfo(mapInfo);
        }
        
 
    }
   
}
