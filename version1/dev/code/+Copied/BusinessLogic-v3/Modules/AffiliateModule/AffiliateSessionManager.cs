﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules.AffiliateModule
{
    public class AffiliateSessionManager
    {
        public static AffiliateSessionManager Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<AffiliateSessionManager>(); } }
        private AffiliateSessionManager()
        {

        }
        public AffiliateBase GetAffiliateForSession()
        {
           // var id = GetAffiliateIDForSession();
            AffiliateBase affiliate = null;
            


            // an affiliate from the host header wins any other affiliates in cookies/ session
            affiliate = getAffiliateFromHostHeader();
            
            if (affiliate == null)
            {
                long? id = CS.General_v3.Util.PageUtil.GetSessionObject<long?>("AffiliateID_CurrentSession");
                if (id.HasValue)
                    affiliate = AffiliateBase.Factory.GetByPrimaryKey(id.Value);
                if (affiliate == null && BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Affiliates_Cookies_UseCookiesToStoreAffiliate))
                {
                    string referralCodeFromCookie = BusinessLogic_v3.Settings.GetSettingFromDatabase<string>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Affiliates_Cookies_CookieName);
                    affiliate = AffiliateBaseFactory.Instance.GetAffiliateByReferralCode(referralCodeFromCookie);
                    if (affiliate != null)
                    {
                        SetAffiliateInSession(affiliate, true);
                    }

                }
            }

            return affiliate;

        }
        public long? GetAffiliateIDForSession()
        {
            var a = GetAffiliateForSession();
            if (a == null)
                return null;
            else
                return a.ID;
            
        }
      

        /// <summary>
        /// Sets the current affiliate to the current session. 
        /// </summary>
        /// <param name="referralCode"></param>
        /// <param name="storeInCookies">Whether to store in cookies or not.  Please note that this still also checks the UseCookiesToStoreAffiliate module param</param>
        /// <returns></returns>
        public bool SetAffiliateInSession(AffiliateBase affiliate, bool storeInCookies)
        {
            var a = affiliate;
            bool ok = false;
            if (a != null && a.CheckIfActivated())
            {
                ok = true;
                CS.General_v3.Util.PageUtil.SetSessionObject("AffiliateID_CurrentSession", a.ID);

                if (storeInCookies && BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Affiliates_Cookies_UseCookiesToStoreAffiliate))
                {
                    DateTime dExpiry = CS.General_v3.Util.Date.Now.AddDays(BusinessLogic_v3.Settings.GetSettingFromDatabase<int>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Affiliates_Cookies_DaysToStoreAffiliateCookie));
                    CS.General_v3.Util.PageUtil.SetCookie( BusinessLogic_v3.Settings.GetSettingFromDatabase<string>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Affiliates_Cookies_CookieName), affiliate.ReferralCode, dExpiry);
                }
            }
            return ok;
        }

        private bool checkForAffiliateFromQueryStringParam()
        {

            string qsParamName = BusinessLogic_v3.Settings.GetSettingFromDatabase(Enums.BusinessLogicSettingsEnum.ModuleSettings_Affiliates_Querystring_ParamName);


            bool ok = false;
            {
                string qsValue = CS.General_v3.Util.PageUtil.GetVariableFromQuerystring(qsParamName);
                var affiliate = AffiliateBaseFactory.Instance.GetAffiliateByReferralCode(qsValue);
                if (affiliate != null)
                    ok = SetAffiliateInSession(affiliate, true);
            }
            return ok;
        }
        private AffiliateBase getAffiliateFromHostHeader()
        {
            AffiliateBase result = null;
            if (BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>( Enums.BusinessLogicSettingsEnum.ModuleSettings_Affiliates_Whitelabelling_Enabled))
            {
                string domain = CS.General_v3.Util.PageUtil.GetDomainFromUrl(null);
                AffiliateBase affiliate = AffiliateBaseFactory.Instance.GetAffiliateByHostHeader(domain);
                if (affiliate != null && affiliate.CheckIfActivated())
                {
                    result = affiliate;
                }


            }
            return result;
        }

        public void CheckForCurrentAffiliate()
        {
            if (BusinessLogic_v3.Settings.GetSettingFromDatabase<bool>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Affiliates_Enabled))
            {
                bool ok = checkForAffiliateFromQueryStringParam();
                

            }
        }

    }
}
