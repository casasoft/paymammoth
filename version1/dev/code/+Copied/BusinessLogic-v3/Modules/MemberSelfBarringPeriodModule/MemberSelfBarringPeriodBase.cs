using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.EmailTextModule;
using BusinessLogic_v3.Classes.Text;
using BusinessLogic_v3.Classes.Culture;

namespace BusinessLogic_v3.Modules.MemberSelfBarringPeriodModule
{

    //BaseClass-File

    public abstract class MemberSelfBarringPeriodBase : BusinessLogic_v3.Modules._AutoGen.MemberSelfBarringPeriodBaseAutoGen, IMemberSelfBarringPeriodBase
    {

        #region BaseClass-AutoGenerated
        #endregion

        public MemberSelfBarringPeriodBase()
        {

        }
        protected override void initPropertiesForNewItem()
        {
            //Fill any default values for NEW items.

            base.initPropertiesForNewItem();
        }
        public override string ToString()
        {
            return base.ToString();
        }

        public void SendNotificationExpiry1Email()
        {
            _sendEmail(Enums.EmailsBusinessLogic.User_Members_SelfBarring_ExpiryNotification1);
            using (var t = beginTransaction())
            {
                this.NotificationExpiry1Sent = true;
                this.Save();
                t.Commit();
            }
        }

        public void SendNotificationExpiry2Email()
        {
            _sendEmail(Enums.EmailsBusinessLogic.User_Members_SelfBarring_ExpiryNotification2);
            using (var t = beginTransaction())
            {
                this.NotificationExpiry2Sent = true;
                this.Save();
                t.Commit();
            }
        }

        public void SendExpiredEmail()
        {
            _sendEmail(Enums.EmailsBusinessLogic.User_Members_SelfBarring_ExpiredNotification);
            using (var t = beginTransaction())
            {
                this.NotificationExpiredSent = true;
                this.Save();
                t.Commit();
            }
        }

        private void _sendEmail(Enums.EmailsBusinessLogic emailText, bool autoSave = true)
        {
            var emailDb = EmailTextBaseFactory.Instance.GetByIdentifier(emailText, autoSave);
            TokenReplacerWithContentTagItem tokenReplacer = new TokenReplacerWithContentTagItem(emailDb);
            fillTokenReplacerWithTags(tokenReplacer);

            var emailMsg = emailDb.GetEmailMessage(tokenReplacerToReplaceTags: tokenReplacer);
            emailMsg.AddToEmails(this.Member.Email, this.Member.GetFullName());

            string fromEmail = Factories.SettingFactory.GetSettingValue<string>(CS.General_v3.Enums.SETTINGS_ENUM.Email_EmailAddresses_General_FromEmail);
            string fromName = Factories.SettingFactory.GetSettingValue<string>(CS.General_v3.Enums.SETTINGS_ENUM.Email_EmailAddresses_General_FromName);

            emailMsg.SetReplyToEmail(fromEmail, fromName);

            emailMsg.SendAsynchronously();
        }

        private void fillTokenReplacerWithTags(TokenReplacerWithContentTagItem tokenReplacer)
        {
            const string tagPrefix = "[SelfBarring:";

            tokenReplacer[tagPrefix + "FirstName]"] = this.Member.FirstName;
            tokenReplacer[tagPrefix + "LastName]"] = this.Member.LastName;
            tokenReplacer[tagPrefix + "FullName]"] = this.Member.GetFullName();
            tokenReplacer[tagPrefix + "ExpiryDate]"] = DefaultCultureManager.Instance.FormatDateTimeWithCulture(this.EndDate, Date.DATETIME_FORMAT.FullDateShortTime, this.Member.PreferredCultureInfo);
        }

        public bool IsExpired()
        {
            return this.EndDate < Date.Now ? true : false;
        }
    }
}
