using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.ArticleModule
{

//BaseClassMap-File
    
    public abstract class ArticleBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.ArticleBaseMap_AutoGen<TData>
    	where TData : ArticleBase
    {

        protected override void IdentifierMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.IsIndexed = true;
            mapInfo.Length = 250;
            base.IdentifierMappingInfo(mapInfo);
        }

        protected override void Article_CultureInfosMappingInfo(ICollectionMapInfo mapInfo)
        {
            base.Article_CultureInfosMappingInfo(mapInfo);
            mapInfo.CascadeType.SetAll();
            mapInfo.CacheType = Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            mapInfo.FetchType = Enums.NHIBERNATE_COLLECTION_FETCHMODE.Select;
            
        }

        protected override void ChildArticleLinksMappingInfo(ICollectionMapInfo mapInfo)
        {
            base.ChildArticleLinksMappingInfo(mapInfo);
            mapInfo.CascadeType.SetAll();

            mapInfo.FetchType = Enums.NHIBERNATE_COLLECTION_FETCHMODE.Select;
            //[20/jun/2012,Karl] - THis was removed as it was not being automatically invalidated when a change is done in CMS, but one had to clear the frontend cache
         //   mapInfo.CacheType = Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            mapInfo.Inverse = false;
        }
        protected override void ParentArticleLinksMappingInfo(ICollectionMapInfo mapInfo)
        {
            base.ParentArticleLinksMappingInfo(mapInfo);
            mapInfo.CascadeType.SetAll();
            mapInfo.FetchType = Enums.NHIBERNATE_COLLECTION_FETCHMODE.Select;
            //mapInfo.CacheType = Enums.NHIBERNATE_CACHE_TYPE.Default;
         //   mapInfo.CacheType = Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            mapInfo.Inverse = false;
            
        }


        public override Enums.NHIBERNATE_CACHE_TYPE CacheType
        {
            get
            {
                return Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            }
            set
            {
                base.CacheType = value;
            }
        }
 
    }
   
}
