﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Modules.CultureDetailsModule;

namespace BusinessLogic_v3.Modules.ArticleModule
{
    public class ArticleUrlComputer
    {
        public ArticleBase Article { get; set; }

        public ArticleUrlComputer()
        {
            
        }
        public string GetUrlForArticleForCurrentCulture()
        {
            var culture = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.GetCulture();
            return GetUrlForArticle(culture);
        }

        public string GetUrlForArticle(ICultureDetailsBase culture)
        {
            string url = null;
            var cultureInfo = this.Article.Article_CultureInfos.Where(x => x.CultureInfo.ID == culture.ID).FirstOrDefault();
            if (cultureInfo != null)
            {
                if (string.IsNullOrWhiteSpace(cultureInfo._ComputedURL))
                {
                    var session = BusinessLogic_v3.Util.nHibernateUtil.GetCurrentSessionFromContext();
                    bool createdNewSession = false;
                    if (session == null)
                    {
                        createdNewSession = true;
                        session = BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();
                    }
                    using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
                    {
                        UpdateComputedUrls();
                        t.Commit();
                    }
                    if (createdNewSession) BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();
                }
                url = cultureInfo._ComputedURL;
                if (string.IsNullOrWhiteSpace(url))
                {
                    int k = 5;
                }
            }
            return url;
        }

        public string ComputeUrlForArticleForCurrentCulture()
        {
            var culture = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.GetCulture();
            return ComputeUrlForArticle(culture);
        }

        public virtual string ComputeUrlForArticle(ICultureDetailsBase culture)
        {
            var prevCulture = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.GetCulture();
            BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.SetCulture(culture);

            string url = null;
            if (!string.IsNullOrWhiteSpace(this.Article.CustomLink))
            {
                url = this.Article.CustomLink;
            }
            else if (this.Article.LinkedRoute != null)
            {
                url = this.Article.LinkedRoute.GetMappedUrlForRoute(null);
            }
            else
            {
                string generatedRouteTitle = GenerateRouteTitle();
                url = BusinessLogic_v3.Classes.Routing.ArticlesRoute.GetURL(this.Article.ID, generatedRouteTitle,
                                                                            this.Article.DialogPage);
            }

            BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.SetCulture(prevCulture);

            return url;
        }
        



        public string GenerateRouteTitle()
        {
            char separator = '-';
            string title = string.Empty;
            ArticleBase cp = this.Article;

            while (cp != null)
            {
                if (cp.ExcludeFromRewriteUrl) break;

                string cpTitle = cp.GetTitleForUrlRewrite();

                if (!string.IsNullOrEmpty(cpTitle))
                {
                    title = cpTitle + separator + title;
                }
                cp = cp.GetParentMain();
            }
            return title.TrimEnd(separator);
        }


        public void UpdateComputedUrls()
        {


            
            
            foreach (var cultureInfo in this.Article.Article_CultureInfos)
            {
                string url = this.ComputeUrlForArticle(cultureInfo.CultureInfo);
                cultureInfo._ComputedURL = url;
                cultureInfo.Save();
            }
             

        }

    }
}
