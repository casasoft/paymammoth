﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.General.ParentChildLinks;
using BusinessLogic_v3.Modules.Article_ParentChildLinkModule;

namespace BusinessLogic_v3.Modules.ArticleModule
{
    public class ArticleParentChildLinksCacher : ParentChildLinksCacher<ArticleBase>
    {
        public static ArticleParentChildLinksCacher Instance
        {
            get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<ArticleParentChildLinksCacher>(); }
        }
        private ArticleParentChildLinksCacher()
        {


        }


        protected override IEnumerable<IParentChildLink> getAllLinks()
        {

            var q = Article_ParentChildLinkBase.Factory.GetQuery();
            var tmp1 = q.Fetch(item => item.Child).Eager;
            var tmp2 = q.Fetch(item => item.Parent).Eager;

            IEnumerable<Article_ParentChildLinkBase> allLinks = Article_ParentChildLinkBase.Factory.FindAll(q);
            return allLinks;
        }





        
        protected override IBaseDbFactory dbFactory
        {
            get { return ArticleBase.Factory; }
        }
    }
}
