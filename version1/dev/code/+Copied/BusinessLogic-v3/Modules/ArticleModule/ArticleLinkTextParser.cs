﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using BusinessLogic_v3.Extensions;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Frontend.ArticleModule;

namespace BusinessLogic_v3.Modules.ArticleModule
{
    /// <summary>
    /// Converts any found article links like '{Article:1}' to the actual link
    /// </summary>
    public class ArticleLinkTextParser
    {

        

        public const string ARTICLE_IDENTIFIER = "Article";

        private HashedSet<string> foundKeys = null;

        private string _data = null;

        public string ParseText(string txt)
        {
            _data = txt;
            if (!string.IsNullOrWhiteSpace(txt))
            {
                foundKeys = new HashedSet<string>();
                string pattern = @"\{Article\:(.*?)\}";
                MatchCollection matches = Regex.Matches(_data, pattern, RegexOptions.IgnoreCase | RegexOptions.Singleline);
                if (matches != null)
                {
                    foreach (Match m in matches)
                    {
                        if (m.Success)
                        {
                            string key = m.Groups[1].Value.ToLower();
                            foundKeys.Add(key);
                        }
                    }
                }
                parseFoundKeys();
            }
            return _data;
        }

        private void parseKey(string s)
        {

            bool isPrimaryKey = false;
            long pKey = 0;
            {
                string tmp = s ?? "";
                tmp = tmp.ToLower();
                long id = 0;
                if (long.TryParse(tmp, out id))
                {
                    isPrimaryKey = true;
                    pKey = id;
                }
                
            }
            ArticleBase article = null;

            if (isPrimaryKey)
            {
                article = ArticleBase.Factory.GetByPrimaryKey(pKey);
            }
            else
            {
                string identifier = s;
                article = (ArticleBase)ArticleBase.Factory.GetArticleByStringIdentifier(identifier);
            }
            if (article != null)
            {
                ArticleBaseFrontend articleFrontend = article.ToFrontendBase();
                string url = articleFrontend.GetUrl();
                string patternToMatch = @"\{Article:" + s +@"\}";
                string replaceWith = url;
                _data = Regex.Replace(_data, patternToMatch, replaceWith, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            }

        }

        private void parseFoundKeys()
        {
            foreach (var key in foundKeys)
            {
                parseKey(key);
            }

        }
    }
}
