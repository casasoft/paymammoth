﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using log4net;

namespace BusinessLogic_v3.Modules.ArticleModule
{
    public class ArticleCustomRouteMapper
    {
        private static void _updateRouteMappingsForAllArticles()
        {
            BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();
            var q = ArticleBaseFactory.Instance.GetQuery();
            q.Where(x => x.PhysicalFilePath != null && x.PhysicalFilePath != "");
            q.WhereRestrictionOn(x => x.CustomLink).IsLike("/", NHibernate.Criterion.MatchMode.Start);
            var articles = ArticleBaseFactory.Instance.FindAll(q);
            foreach (var a in articles)
            {
                a.UpdateRouteMapping();
            }
            BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();

        }

        public static void UpdateRouteMappingsForAllArticles()
        {
            CS.General_v3.Util.ThreadUtil.CallMethodOnSeperateThread(_updateRouteMappingsForAllArticles, "ArticleCustomRouteMapper.UpdateRouteMappingsForAllArticles", System.Threading.ThreadPriority.Normal);
        }


        private readonly ILog _log = LogManager.GetLogger(typeof(ArticleCustomRouteMapper));
        private ArticleBase _article;
        public ArticleCustomRouteMapper(ArticleBase a)
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(a, "Article must be filled in");
            _article = a;
        }

        private string getRouteName()
        {
            string routeName = "CustomRoute_Article_" + _article.ID;

            return routeName;
        }

        private bool checkIfPhysicalFileExists()
        {
            bool exists = false;
            if (CS.General_v3.Util.Other.IsLocalTestingMachine)
            {
                string file = _article.PhysicalFilePath;
                string localPath = CS.General_v3.Util.PageUtil.MapPath(file);
                exists = (File.Exists(localPath));
                if (!exists)
                {
                    _log.Warn("Custom route for article [" + _article.ToString() + ", ID: " + _article.ID + "] could not be added as physical file '" + _article.PhysicalFilePath + "' does not exist");
                }

            }
            return exists;
        }

        private void removeRoute()
        {
            string routeName = getRouteName();
            bool removed = CS.General_v3.Util.RoutingUtil.RemoveRoute(routeName);


        }

        public void UpdateRouteMapping()
        {
            removeRoute();
            if (!string.IsNullOrWhiteSpace(_article.PhysicalFilePath) && !string.IsNullOrWhiteSpace(_article.CustomLink) && _article.CustomLink.StartsWith("/"))
            {
                checkIfPhysicalFileExists();

                string routeName = getRouteName();
            
                CS.General_v3.Util.RoutingUtil.MapPageRoute(routeName, _article.CustomLink,
                    _article.PhysicalFilePath, null, null, throwErrorIfPhysicalFileDoesNotExist: false);


            }
            
        }

        public bool CheckIfCustomRouteExists()
        {
            string routeName = getRouteName();
            var route= CS.General_v3.Util.RoutingUtil.GetRouteByName(routeName);
            return route != null;
        }

    }
}
