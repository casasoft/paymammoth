using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.General;
using BusinessLogic_v3.Modules.Article_ParentChildLinkModule;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using Iesi.Collections.Generic;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.Routing;
using BusinessLogic_v3.Classes.Searches.ArticleSearcher;

namespace BusinessLogic_v3.Modules.ArticleModule
{
    using Helpers;

    //BaseFactory-File

    public abstract class ArticleBaseFactory : BusinessLogic_v3.Modules._AutoGen.ArticleBaseFactoryAutoGen, IArticleBaseFactory, IItemWithIdentifierFactory<ArticleBase>
    {


        public ArticleBaseFactory()
        {

            //this.identifierCache = new ItemsByIdentifierCache<ArticleBase>(this);

        }
        private const string IDENTIFIER_TEMP_DEFUALT_NODE = "temp-default-node";

        //protected override void fillInitialItemsInEmtpyDb()
        //{
        //Karl 17/may/2012 - this was removed.  We will now be creating standard items from the template DB, and not here.
        //    //Upon calling them they will be created in DB
        //    var cpErrorPageNotFound = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Error_PageNotFound);
        //    var cpErrorPageGeneric = BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER.Error_Generic);



        //    base.fillInitialItemsInEmtpyDb();
        //}


        public void CreateAllArticlesByEnum(Type enumType, Enums.ARTICLE_TYPE articleType)
        {
            var enumValues = CS.General_v3.Util.EnumUtils.GetListOfEnumValues(enumType);
            foreach (var enumVal in enumValues)
            {
                var cp = getArticleByIdentifier(enumVal, articleType);
                //this should create it
            }


        }
        public IEnumerable<IArticleBase> GetMostReadArticles(int amount, DateTime endDate, DateTime startDate, IArticleBase category = null, bool considerOnlyLeafNodes = true)
        {
            IEnumerable<ArticleBase> results = null;
            if (category != null)
            {
                results = category.GetChildArticles().Cast<ArticleBase>();
                if (considerOnlyLeafNodes)
                {
                    results = results.Where(item => item._Computed_IsLeafNode);
                }
                results =
                        results.Where(
                            item =>
                            item.PublishedOn != null && item.PublishedOn <= endDate && item.PublishedOn >= startDate);
                results = results.OrderByDescending(item => item.ViewCount).Take(amount);
            }
            else
            {
                var q = GetQuery();
                if(considerOnlyLeafNodes)
                {
                    q = q.Where(item => item._Computed_IsLeafNode);
                }
                q =
                        q.Where(
                            item => item.PublishedOn != null && item.PublishedOn <= endDate && item.PublishedOn >= startDate);
                q = q.OrderBy(item => item.ViewCount).Desc;
                q = q.ThenBy(item => item.PublishedOn).Desc;
                q.Take(amount);
                results =  FindAll(q);
            }
            return results;
        }

        public IEnumerable<IArticleBase> GetLatestNewArticlesBetweenDates(int maxAmount, DateTime fromDate, DateTime toDate, bool considerOnlyLeafNodes = true)
        {
            DateTime smallestDateVal = fromDate <= toDate ? fromDate : toDate;
            DateTime largestDateVal = toDate >= fromDate ? toDate : fromDate;
            var q = GetQuery();
            q = q.Where(item=>item.PublishedOn != null);
            q = q.Where(item => item.PublishedOn >= smallestDateVal);
            q = q.Where(item => item.PublishedOn <= largestDateVal);
            if(considerOnlyLeafNodes)
            {
                q = q.Where(item => item._Computed_IsLeafNode);
            }
            q.Take(maxAmount);
            q = q.OrderBy(item => item.PublishedOn).Desc;
            return FindAll(q);
        }

        public IEnumerable<IArticleBase> GetAllChildArticlesFromAllGenerationsForArticle(IArticleBase article, int takeAmount, bool includeThis)
        {
            ArticleBase articleImp = (ArticleBase) article;
            var q = GetQuery();
            q.WhereRestrictionOn(item => item._Computed_ParentArticleNames).IsLike(
                articleImp._Computed_ParentArticleNames, MatchMode.Start);
            if(!includeThis)
            {
                q.Where(item => item.ID != articleImp.ID);
            }
            q.Take(takeAmount);
            return FindAll(q);
        }

        public IEnumerable<IArticleBase> GetLatestChildArticlesRespectiveToDateForArticle(IArticleBase article, bool includeThis, int takeAmount, DateTime dateToConsider, bool loadOnlyLeafNodes = true)
        {
            ArticleBase articleImp = (ArticleBase)article;
            var q = GetQuery();
            q.WhereRestrictionOn(item => item._Computed_ParentArticleNames).IsLike(
                articleImp._Computed_ParentArticleNames, MatchMode.Start);
            if (!includeThis)
            {
                q = q.Where(item => item.ID != articleImp.ID);
            }
            if(loadOnlyLeafNodes)
            {
                q = q.Where(item => item._Computed_IsLeafNode);
            }
            q = q.Where(item => item.PublishedOn != null && item.PublishedOn <= dateToConsider);
            q = q.OrderBy(item => item.PublishedOn).Desc;
            q.Take(takeAmount);
            return FindAll(q);
        }

        public IEnumerable<IArticleBase> GetMostCommentedArticles(int amount, DateTime endDate, DateTime startDate, IArticleBase category = null, bool considerOnlyLeafNodes = true)
        {
            IEnumerable<ArticleBase> results = null;
            if (category != null)
            {
                results = category.GetChildArticles().Cast<ArticleBase>();
                if (considerOnlyLeafNodes)
                {
                    results = results.Where(item => item._Computed_IsLeafNode);
                }
                results =
                        results.Where(
                            item =>
                            item.PublishedOn != null && item.PublishedOn <= endDate && item.PublishedOn >= startDate);
                results = results.OrderByDescending(item => item._Computed_CommentAndRepliesCount).Take(amount);
            }
            else
            {
                var q = GetQuery();
                if (considerOnlyLeafNodes)
                {
                    q = q.Where(item => item._Computed_IsLeafNode);
                }
                q =
                        q.Where(
                            item => item.PublishedOn != null && item.PublishedOn <= endDate && item.PublishedOn >= startDate);
                q = q.OrderBy(item => item._Computed_CommentAndRepliesCount).Desc;
                q = q.ThenBy(item => item.PublishedOn).Desc;
                q.Take(amount);
                results = FindAll(q);
            }
            return results;
        }

        /// <summary>
        /// Returns featured articles, in descending order of last edited date and limited to the specified amount - if not enough are found, it will add articles from the same category but includes nonfeatured too, same applies when no category is specified.
        /// </summary>
        /// <param name="amount">The amount of articles to be loaded.</param>
        /// <param name="category">The parent node for which the articles must be filtered in.</param>
        /// <param name="loadArticlesOnlyBeforeThisDate">If a date is specified, then load only articles before this date</param>
        /// /// <param name="takeNowAsDateIfNotSpecified">If true and no date is specified, it will take now as the relative date.</param>
        /// <returns></returns>
        public IEnumerable<IArticleBase> GetFeaturedArticlesFromCategory(int amount, IArticleBase category, DateTime? loadArticlesOnlyBeforeThisDate = null, bool takeNowAsDateIfNotSpecified = true)
        {
            /* 1. If category not null, get articles from children which are featured, order by edit date descending, limit amount to desired.
             * 2. Else, same logic as 1, except get from any category.
             * 3. If loaded amount by category is less than required, load from same category but include non-featured to compensate.
             * 4. If loaded amount by no category is less than required, load from any category but include non-featured to compensate.
             */

            List<ArticleBase> results = new List<ArticleBase>();

            if (!loadArticlesOnlyBeforeThisDate.HasValue && takeNowAsDateIfNotSpecified)
            {
                loadArticlesOnlyBeforeThisDate = Date.Now;
            }

            if (category != null)
            {
                IEnumerable<ArticleBase> childArticlesOriginal = category.GetChildArticles().Where(item => item._Computed_IsLeafNode).Cast<ArticleBase>();
                if (loadArticlesOnlyBeforeThisDate.HasValue)
                {
                    childArticlesOriginal =
                        childArticlesOriginal.Where(item => item.PublishedOn != null && item.PublishedOn <= loadArticlesOnlyBeforeThisDate.Value);
                }
                IEnumerable<ArticleBase> childArticles = childArticlesOriginal.Where(item => item.IsFeatured);

                results.AddRange(childArticles.OrderByDescending(item => item.PublishedOn).Take(amount));
                if (results.Count < amount)
                {
                    List<long> loadedId = new List<long>();
                    foreach (IArticleBase artcl in results)
                    {
                        loadedId.Add(((IBaseDbObject)artcl).ID);
                    }
                    IEnumerable<ArticleBase> moreArticles =
                        childArticlesOriginal.Where(item => !loadedId.Contains(item.ID)).OrderByDescending(
                            item => item.PublishedOn).Take(amount - results.Count);
                    results.AddRange(moreArticles);
                }
            }
            else
            {
                IEnumerable<ArticleBase> articls = getArticlesForHeadlines(true, null, amount, loadArticlesOnlyBeforeThisDate);
                results.AddRange(articls);
                if (results.Count < amount)
                {
                    IEnumerable<ArticleBase> moreArticles = getArticlesForHeadlines(false, results,
                                                                                    amount - results.Count, loadArticlesOnlyBeforeThisDate);
                    results.AddRange(moreArticles);
                }
            }
            return results.OrderByDescending(item => item.PublishedOn);
        }

        private IEnumerable<ArticleBase> getArticlesForHeadlines(bool getOnlyFeatured, IEnumerable<ArticleBase> exclusions, int takeAmount, DateTime? loadArticlesOnlyBeforeThisDate = null)
        {
            List<long> alreadyLoadedIds = new List<long>();
            if (exclusions != null)
            {
                foreach (IArticleBase artcl in exclusions)
                {
                    alreadyLoadedIds.Add(((IBaseDbObject)artcl).ID);
                }
            }
            var q = GetQuery();
            q = q.Where(item => item.PublishedOn != null && item._Computed_IsLeafNode);
            if (loadArticlesOnlyBeforeThisDate.HasValue)
            {
                q = q.Where(item => item.PublishedOn <= loadArticlesOnlyBeforeThisDate.Value);
            }
            if (getOnlyFeatured)
            {
                q = q.Where(item => item.IsFeatured);
            }
            if (alreadyLoadedIds.Count > 0)
            {
                q.RootCriteria.Add(Restrictions.Not(Restrictions.InG("ID", alreadyLoadedIds)));
            }
            var qo =
                q.JoinQueryOver<Article_ParentChildLinkBase>(item => item.ChildArticleLinks, NHibernate.SqlCommand.JoinType.LeftOuterJoin).Where(
                    item => item.ID == null);
            q = q.OrderBy(item => item.PublishedOn).Desc;
            q.Take(takeAmount);
            return FindAll(q);
        }

        //private ItemsByIdentifierCache<ArticleBase> identifierCache = null;


        public override void OnPostInitialisation()
        {
            base.OnPostInitialisation();
        }
        #region BaseFactory-AutoGenerated

        #endregion


        public ArticleBase GetArticleByRouteParameter()
        {
            /*if (ArticlesRoute.ROUTE_INCLUDE_CULTURE)
            {
                //Update language code
                object oLang = CS.General_v3.Util.PageUtil.GetRouteDataVariable(ArticlesRoute.ROUTE_PARAM_CULTURE);
                if (oLang != null)
                {
                    string sLangCode = oLang.ToString();
                    
                    BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.SetCultureByCode(sLangCode);
                }

            }*/
            var id = ArticlesRoute.GetIDFromRouteData();

            ArticleBase cp = null;
            if (id != null)
            {
                cp = this.GetByPrimaryKey(id.Value);

            }
            return cp;


        }

        /// <summary>
        /// Gets the root node
        /// </summary>
        /// <param name="autoFillChildren">Whether to auto fill children</param>
        /// <param name="VisibleValue">Whether to show only visible, not visible, or doesn't matter</param>
        /// <returns></returns>
        public IEnumerable<ArticleBase> GetRootNodes(bool getOnlyUsedInProject = false)
        {
            var q = GetQuery().WhereRestrictionOn(c => c.ParentArticleLinks).IsEmpty;
            if (getOnlyUsedInProject)
                q = q.Where(x => x.UsedInProject);

            var list = FindAll(q);

            return list;
        }

        /// <summary>
        /// Gets the root node
        /// </summary>
        /// <param name="autoFillChildren">Whether to auto fill children</param>
        /// <param name="VisibleValue">Whether to show only visible, not visible, or doesn't matter</param>
        /// <returns></returns>
        public ArticleBase GetRootNodeForArticleType(Enums.ARTICLE_TYPE articleType, bool createIfNotExists = true)
        {

            string identifier = ROOT_IDENTIFIER + "_" + CS.General_v3.Util.EnumUtils.StringValueOf(articleType);
            ArticleBase node = _getArticleByIdentifierFromDb(identifier);
            if (node == null)
            {
                using (var t = beginTransaction())
                {
                    DefaultValues.DefaultArticleCreator creator = new DefaultValues.DefaultArticleCreator(identifier, articleType);
                    creator.Name = CS.General_v3.Util.Text.AddSpacesToCamelCasedText(CS.General_v3.Util.EnumUtils.StringValueOf(articleType));
                    creator.AccessTypeRequiredToDelete = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft;
                    creator.ArticleType = articleType;
                    node = creator.CreateDefaultArticle(GetRootNode(autoSave: false), autoSave: false);
                    node.ExcludeFromRewriteUrl = true;
                    node.Update();
                    t.Commit();
                }
            }

            return node;
        }


        public const string ROOT_IDENTIFIER = "Root";

        /// <summary>
        /// Gets the root node
        /// </summary>
        /// <param name="autoFillChildren">Whether to auto fill children</param>
        /// <param name="VisibleValue">Whether to show only visible, not visible, or doesn't matter</param>
        /// <returns></returns>
        public ArticleBase GetRootNode(bool createIfNotExists = true, bool autoSave = true)
        {
            var list = GetRootNodes();
            var root = list.FirstOrDefault();
            if (root != null)
            {


            }
            else
            {
                if (createIfNotExists)
                {
                    using (var t = autoSave ? beginTransaction() : null)
                    {
                        DefaultValues.DefaultArticleCreator creator = new DefaultValues.DefaultArticleCreator(ROOT_IDENTIFIER, Enums.ARTICLE_TYPE.None);
                        creator.AccessTypeRequiredToDelete = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft;
                        root = creator.CreateDefaultArticle(null, autoSave: false);
                        root.ExcludeFromRewriteUrl = true;
                        root.Update();
                        if (autoSave)
                        {
                            t.Commit();
                        }
                    }


                }
            }
            return root;
        }

        public ArticleBase GetArticleForUnlinkedArticles()
        {
            var cp = getArticleByIdentifier(Enums.CONTENT_PAGE_IDENTIFIER.UnlinkedArticles, Enums.ARTICLE_TYPE.ContentPages, throwErrorIfNull: false);
            if (cp == null)
            {
                var root = GetRootNode(false);

                cp = CreateNewItem();
                cp.Name = "Unlinked Pages";
                cp.PageTitle = cp.Name;
                cp.SetIdentifier(Enums.CONTENT_PAGE_IDENTIFIER.UnlinkedArticles);
                cp.Published = false;
                cp.Create();
                root.AddChildArticle(cp);

            }
            return cp;
        }

        protected internal ArticleBase getArticleByIdentifier(Enum identifier, Enums.ARTICLE_TYPE articleType, bool throwErrorIfNull = true, bool createIfNotExists = true)
        {

            string s = CS.General_v3.Util.EnumUtils.StringValueOf(identifier);
            var defaultValues = CS.General_v3.Util.EnumUtils.GetAttributeFromEnumValue<DefaultValues.ArticleDefaultValuesAttribute>(identifier);

            return getArticleByIdentifier(s, articleType, defaultValues, throwErrorIfNull, createIfNotExists);
        }


        internal ArticleBase _getArticleByIdentifierFromDb(string identifier)
        {
            var q = GetQuery().Where(cp => (cp.Identifier == identifier && !cp._Temporary_Flag && !cp.Deleted));
            q.Cacheable();
            var item = FindItem(q);
            return item;
        }

        protected internal ArticleBase getArticleByIdentifier(string identifier, Enums.ARTICLE_TYPE articleType, DefaultValues.IArticleDefaultValues defaultValuesIfNotExists,
            bool throwErrorIfNull = true, bool createIfNotExists = true)
        {

            ArticleBase item = null;
            string sID = identifier;// +"-" + CS.General_v3.Util.EnumUtils.StringValueOf(articleType); //Removed by Mark 2011-11-18 as it was getting confusing

            if (item == null)
            {
                item = _getArticleByIdentifierFromDb(sID);

            }

            if (item == null && createIfNotExists)
            {
                DefaultValues.DefaultArticleCreator creator = new DefaultValues.DefaultArticleCreator(sID, articleType);

                creator.FillFromDefaultValues(defaultValuesIfNotExists);
                creator.ArticleType = articleType;
                var parent = GetRootNodeForArticleType(articleType);
                item = creator.CreateDefaultArticle(parent);

            }
            if (item == null && throwErrorIfNull)
            {
                throw new InvalidOperationException("Article_Base.GetArticleByIdentifier():: Could not load article with identifier <" + sID + ", " + articleType + ">");
            }

            if (item != null)
                item.MarkAsUsedInProject();
            return item;
        }




        #region IItemWithIdentifierFactory<ArticleBase> Members

        ArticleBase IItemWithIdentifierFactory<ArticleBase>.GetItemByIdentifierFromDB(string enumValue)
        {

            return this.getArticleByIdentifier(enumValue, Enums.ARTICLE_TYPE.ContentPages, null, throwErrorIfNull: false);

        }

        IEnumerable<ArticleBase> IItemWithIdentifierFactory<ArticleBase>.GetAllItemsFromDB()
        {
            var q = GetQuery();
            q.JoinQueryOver<Article_ParentChildLinkBase>(item => item.ChildArticleLinks).JoinQueryOver(item => item.Child);
            q.JoinQueryOver<Article_ParentChildLinkBase>(item => item.ParentArticleLinks).JoinQueryOver(item => item.Parent);
            //            q.Fetch(item => item.ChildArticlesLinks);
            //          q.Fetch(item => item.ParentArticlesLinks);



            return FindAll();

        }

        #endregion

        #region IItemWithIdentifierFactory<ArticleBase> Members


        ArticleBase IItemWithIdentifierFactory<ArticleBase>.GetItemByPKeyFromDB(long id)
        {
            return GetByPrimaryKey(id);
        }

        #endregion



        #region IArticleBaseFactory Members

        IArticleBase IArticleBaseFactory.GetArticleByIdentifier(Enum identifier)
        {
            return getArticleByIdentifier(identifier, Enums.ARTICLE_TYPE.ContentPages, true, true);
        }

        #endregion

        #region IArticleBaseFactory Members


        IArticleBase IArticleBaseFactory.GetRoot()
        {

            return this.GetRootNode(true);
        }

        #endregion

        #region IArticleBaseFactory Members
        public IEnumerable<IArticleBase> GetArticlesForParents(IEnumerable<IArticleBase> parents, int pageNo, int pageSize, Enums.CONTENT_PAGE_SORT_BY sortBy, out int totalResults, bool showOnlyLeafNodes = false)
        {
            ArticleSearchParams searchParams = new ArticleSearchParams();
            searchParams.PageNo = pageNo;
            searchParams.ShowAmount = pageSize;
            HashedSet<IArticleBase> articleList = new HashedSet<IArticleBase>();
            articleList.AddAll(parents);
            searchParams.Parents = articleList;
            searchParams.ShowOnlyLeafNodes = showOnlyLeafNodes;
            if (sortBy != null) searchParams.SortBy = sortBy;
            ArticleSearcher cpSearcher = new ArticleSearcher();
            cpSearcher.SearchParams = searchParams;
            var results = cpSearcher.GetSearchResults();
            totalResults = results.TotalResults;
            MarkArticlesAsUsed(results.Cast<ArticleBase>());
            return results;
        }
        private IArticleBase findArticleByURLFromList(string url, IEnumerable<IArticleBase> articles)
        {
            url = url.ToLower();
            foreach (var article in articles)
            {
                if (article.GetUrl().ToLower().Contains(url))
                {
                    return article;
                }
                else
                {
                    var childArticles = article.GetChildArticles();
                    IArticleBase foundArticle = findArticleByURLFromList(url, childArticles);
                    if (foundArticle != null)
                    {
                        return foundArticle;
                    }
                }
            }
            return null;
        }

        //Spoke with Mark, and this will not be used anymore (also it was incomplete) and thus I've commented the code. 2012-06-27 (Franco)
        //public IArticleBase LoadArticleByURL(string url)
        //{

        //    ///Load content page by CustomLink / URL and check whether user can redirect to url;

        //    var allArticles = FindAll();
        //    return findArticleByURLFromList(url, allArticles);
        //}
        public bool CanRedirectToURLOnLogin(string url)
        {
            //todo:2012-06-27 - As discussed with Mark, this will be implemented later on. Franco.
            return true;
            //var article = LoadArticleByURL(url);
            //if (article != null)
            //{
            //    return !article.DoNotRedirectOnLogin.GetValueOrDefault();
            //}
            //else
            //{
            //    return true;
            //}
        }
        public IEnumerable<IArticleBase> GetArticlesForParent(IArticleBase parentPage, int pageNo, int pageSize, Enums.CONTENT_PAGE_SORT_BY sortBy, out int totalResults, bool showOnlyLeafNodes = false)
        {
            return GetArticlesForParents(new List<IArticleBase>() { parentPage }, pageNo, pageSize, sortBy, out totalResults, showOnlyLeafNodes);
        }

        #endregion

        #region IArticleBaseFactory Members

        public IEnumerable<IArticleBase> GetFeaturedArticles(IArticleBase Article, int pageNo, int pageSize, out int totalResults)
        {
            ArticleSearchParams searchParams = new ArticleSearchParams();
            searchParams.PageNo = pageNo;
            searchParams.ShowAmount = pageSize;
            if (Article != null)
            {
                HashedSet<IArticleBase> ArticleList = new HashedSet<IArticleBase> { Article };
                searchParams.Parents = ArticleList;
            }
            searchParams.IsFeatured = true;
            ArticleSearcher cpSearcher = new ArticleSearcher();
            cpSearcher.SearchParams = searchParams;

            var results = cpSearcher.GetSearchResults();
            totalResults = results.TotalResults;
            MarkArticlesAsUsed(results.Cast<ArticleBase>());
            return results;
        }

        #endregion




        #region IArticleBaseFactory Members

        IEnumerable<IArticleBase> IArticleBaseFactory.GetRootNodes()
        {
            return this.GetRootNodes();

        }

        #endregion



        public ArticleBase GetArticleFromRouteData()
        {
            long? ArticleID = BusinessLogic_v3.Classes.Routing.ArticlesRoute.GetIDFromRouteData();
            if (ArticleID.HasValue)
            {
                var item = this.GetByPrimaryKey(ArticleID.Value);
                if (item != null)
                {
                    item.MarkAsUsedInProject();
                    return item;
                }
                return null;
            }
            return null;
        }
        public ArticleBase GetContentPageByIdentifier(BusinessLogic_v3.Enums.CONTENT_PAGE_IDENTIFIER identifier)
        {

            return getArticleByIdentifier(identifier, Enums.ARTICLE_TYPE.ContentPages);
        }



        internal void MarkArticlesAsUsed(IEnumerable<ArticleBase> pages)
        {
            if (pages != null)
            {
                foreach (var pg in pages)
                {
                    pg.MarkAsUsedInProject();
                }
            }
        }




        public IEnumerable<IArticleBase> GetNews(int pageNo, int pageSize, Enums.CONTENT_PAGE_SORT_BY sortBy, out int totalResults)
        {
            var newsPage =
                ArticleBaseFactory.Instance.GetRootNodeForArticleType(Enums.ARTICLE_TYPE.NewsItems);
            if (newsPage != null)
            {
                return newsPage.GetChildrenByFilter(pageNo, pageSize, sortBy, out totalResults);
            }
            else
            {
                totalResults = 0;
                return new List<ArticleBase>();
            }
        }

        #region IArticleBaseFactory Members

        protected string getUrlForArticle(string identifier, Enums.ARTICLE_TYPE articleType, bool throwErrorIfDoesNotExist = true)
        {
            var article = getArticleByIdentifier(identifier, articleType, null, throwErrorIfNull: throwErrorIfDoesNotExist);
            return article.GetUrlForCurrentCulture();
        }

        public string GetUrlForGeneralContentPage(Enums.CONTENT_PAGE_IDENTIFIER identifier)
        {
            string sID = CS.General_v3.Util.EnumUtils.StringValueOf(identifier);
            return getUrlForArticle(sID, Enums.ARTICLE_TYPE.ContentPages);

        }

        #endregion

        #region IArticleBaseFactory Members


        IArticleBase IArticleBaseFactory.GetArticleFromRouteData()
        {
            return this.GetArticleFromRouteData();

        }

        #endregion

        public IArticleBase GetArticleByStringIdentifier(string identifier)
        {
            //var defaultValues = CS.General_v3.Util.EnumUtils.GetAttributeFromEnumValue<DefaultValues.ArticleDefaultValuesAttribute>(identifier);
            return getArticleByIdentifier(identifier, Enums.ARTICLE_TYPE.ContentPages, null, true, true);
        }

        /// <summary>
        /// This method shall return the latest news by amount
        /// </summary>
        /// <param name="amount">If amount is 0, then return all news</param>
        /// <returns></returns>
        public IEnumerable<IArticleBase> GetLatestNews(int amount)
        {
            var q = GetQuery();
            q = q.Where(item => item.ArticleType == Enums.ARTICLE_TYPE.NewsItems);
            q = q.OrderBy(item => item.LastEditedOn).Desc;
            var qq = q.Take(amount);
            return this.FindAll(qq);
        }

        /// <summary>
        /// This method shall return all blogs
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IArticleBase> GetBlogs()
        {
            var q = GetQuery();
            q = q.Where(item => item.ArticleType == Enums.ARTICLE_TYPE.BlogPosts);
            return this.FindAll(q);
        }

        public void MarkArticleAsReadForCurrentUser(IArticleBase article)
        {
            article.MarkReadForCurrentUser();
        }
        public IEnumerable<ArticleReadAndCommentsHelper> LoadArticlesReadAndCommentedAmounts(IEnumerable<long> articleIDs)
        {
            List<ArticleReadAndCommentsHelper> items = new List<ArticleReadAndCommentsHelper>();
            int count = articleIDs.Count();
            foreach (long id in articleIDs)
            {
                IArticleBase atcl = GetByPrimaryKey(id);
                ArticleReadAndCommentsHelper helper = new ArticleReadAndCommentsHelper();
                helper.Comments = atcl.GetCommentsAndRepliesCount();
                helper.ID = id;
                helper.Read = atcl.IsCurrentUserReadArticle();
                items.Add(helper);
            }
            return items;
        }

    }

}
