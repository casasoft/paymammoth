﻿namespace BusinessLogic_v3.Modules.ArticleModule.Helpers
{
    public class ArticleReadAndCommentsHelper
    {
        public long ID { get; set; }
        public bool Read { get; set; }
        public int Comments { get; set; }
    }
}
