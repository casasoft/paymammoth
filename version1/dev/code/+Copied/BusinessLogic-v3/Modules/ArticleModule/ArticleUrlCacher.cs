﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules.ArticleModule
{
    public class ArticleUrlCacher
    {
        private ArticleUrlCacher()
        {

        }
        public static ArticleUrlCacher Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<ArticleUrlCacher>(); } }

        private const string CACHE_KEY_PREFIX = "ArticleUrlsCache_ItemID_";

        private ConcurrentDictionary<long, string> articleUrls = new ConcurrentDictionary<long, string>();

        public string GetUrlForArticle(ArticleBase article)
        {
            string url = getFromCache(article);
            if (string.IsNullOrWhiteSpace(url))
            {
                url = compute(article);
            }
            return url;
        }

        private string compute(ArticleBase article, bool addToCache = true)
        {
            string url = null;
            if (!string.IsNullOrWhiteSpace(article.CustomLink))
            {
                url = article.CustomLink;
            }
            else if (article.LinkedRoute != null)
            {
                url = article.LinkedRoute.GetMappedUrlForRoute(null);
            }
            else
            {
                string generatedRouteTitle = generateRouteTitle(article);
                url = BusinessLogic_v3.Classes.Routing.ArticlesRoute.GetURL(article.ID, generatedRouteTitle,
                                                                            article.DialogPage);
            }
            if (addToCache)
            {
                addUrlToCache(article, url);
            }
            return url;
        }
        private string getCacheKey(ArticleBase article)
        {
            return CACHE_KEY_PREFIX +"_"+ article.ID;
        }

        private void addUrlToCache(ArticleBase article, string url)
        {
           
            CS.General_v3.Util.CachingUtil.AddItemToCache(getCacheKey(article), url, System.Web.Caching.Cache.NoSlidingExpiration, BusinessLogic_v3.Util.CachingUtil.GetOutputCacheDependencyForDBObjects(article));
        }

        private string generateRouteTitle(ArticleBase article)
        {
            char separator = '-';
            string title = string.Empty;
            ArticleBase cp = article;

            while (cp != null)
            {
                if (cp.ExcludeFromRewriteUrl) break;

                string cpTitle = cp.GetTitleForUrlRewrite();

                if (!string.IsNullOrEmpty(cpTitle))
                {
                    title = cpTitle + separator + title;
                }
                cp = cp.GetParentMain();
            }
            return title.TrimEnd(separator);
        }

        private string getFromCache(ArticleBase article)
        {
            string url = CS.General_v3.Util.CachingUtil.GetItemFromCache<string>(getCacheKey(article));
            return string.IsNullOrWhiteSpace(url) ? null : url;
        }
    }
}
