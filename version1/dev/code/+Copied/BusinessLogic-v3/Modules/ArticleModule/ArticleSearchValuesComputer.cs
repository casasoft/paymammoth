﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.Culture;
using CS.General_v3.Util;

namespace BusinessLogic_v3.Modules.ArticleModule
{
    public class ArticleSearchValuesComputer
    {
        public const string HIERARCHY_ROOT_IDENTIFIER = "$";
        public const string HIERARCHY_NODE_DELIMITER = "/";

        private ArticleBase _article { get; set; }

        public ArticleSearchValuesComputer(ArticleBase article)
        {
            if(article == null)
            {
                throw new InvalidOperationException(
                    "You cannot instantiate ArticleSearchvaluesComputer with a null reference of ArticleBase!");
            }
            _article = article;
        }

        public void ComputeParentArticleNamesForAllCultures()
        {
            string hierarchyFlattened = computeHierarchyPath(_article);
            if(hierarchyFlattened != _article._Computed_ParentArticleNames)
            {
                _article._Computed_ParentArticleNames = hierarchyFlattened;
                repeatForChildren(_article);
            }
        }

        private void repeatForChildren(ArticleBase article)
        {
            var childLinks = article.ChildArticleLinks;
            foreach (var childLink in childLinks)
            {
                if (childLink != null)
                {
                    ArticleBase child = childLink.Child;
                    if (child != null)
                    {
                        child.Save();
                    }
                }
            }
        }

        private IEnumerable<ArticleBase> getAllParentsForArticle(ArticleBase article)
        {
            List<ArticleBase> articles = new List<ArticleBase>();
            var mainParentLink = article.ParentArticleLinks.FirstOrDefault();
            if(mainParentLink!=null)
            {
                ArticleBase parent = mainParentLink.Parent;
                if (parent != null)
                {
                    articles.Add(parent);
                    articles.AddRange(getAllParentsForArticle(parent));
                }
            }
            return articles;
        }

        private string computeHierarchyPath(ArticleBase article)
        {
            string result = string.Empty;
            CultureDetailsBase currentCulture = (CultureDetailsBase)DefaultCultureManager.Instance.GetCulture();
            IEnumerable<ArticleBase> parentArticles = getAllParentsForArticle(article);
            foreach (var c in article.Article_CultureInfos)
            {
                List<string> parentNames = new List<string>();
                CultureDetailsBase culture = c.CultureInfo;
                article.SetCurrentCultureInfoDBId(culture);
                StringBuilder sb = new StringBuilder();
                parentNames.Add(article.PageTitle);
                foreach (ArticleBase parentArticle in parentArticles)
                {
                    parentArticle.SetCurrentCultureInfoDBId(culture);
                    parentNames.Add(parentArticle.PageTitle);
                }
                parentNames.Reverse();
                result = HIERARCHY_ROOT_IDENTIFIER + HIERARCHY_NODE_DELIMITER + Text.AppendStrings(HIERARCHY_NODE_DELIMITER, parentNames);
            }
            _article.SetCurrentCultureInfoDBId(currentCulture);
            return result;
        }

        //public void ComputeParentArticleNamesForAllCultures()
        //{
        //    CultureDetailsBase currentCulture = (CultureDetailsBase) DefaultCultureManager.Instance.GetCulture();
        //    IEnumerable<ArticleBase> parentArticles = _article.GetAllParentArticlesWithRecursion();
        //    foreach(var c in _article.Article_CultureInfos)
        //    {
        //        List<string> parentNames = new List<string>();
        //        CultureDetailsBase culture = c.CultureInfo;
        //        _article.SetCurrentCultureInfoDBId(culture);
        //        StringBuilder sb = new StringBuilder();
        //        foreach(ArticleBase parentArticle in parentArticles)
        //        {
        //            parentArticle.SetCurrentCultureInfoDBId(culture);
        //            parentNames.Add(parentArticle.PageTitle);
        //        }
        //        _article._Computed_ParentArticleNames = "$" + Text.AppendStrings("/", parentNames);
        //    }
        //    _article.SetCurrentCultureInfoDBId(currentCulture);
        //}
    }
}
