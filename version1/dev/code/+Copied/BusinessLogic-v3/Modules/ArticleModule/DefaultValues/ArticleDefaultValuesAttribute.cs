﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DefaultValues;

namespace BusinessLogic_v3.Modules.ArticleModule.DefaultValues
{
    public class ArticleDefaultValuesAttribute : BaseContentDefaultValueAttribute, IArticleDefaultValues
    {
    

        public string Name { get; set; }
        public string PageTitle { get; set; }
        public string SubTitle { get; set; }
        public string MetaTitle { get; set; }
        public string Identifier { get; set; }
        // ublic string Identifier { get; set; }

        public string HtmlText { get; set; }
        public string CustomLink { get; set; }
        public bool DialogPage { get; set; }
        public int? DialogWidth { get; set; }
        public int? DialogHeight { get; set; }


        private bool? _doNotShowNavigationBreadcrumbs;
        private bool? _doNotShowAddThis;
        private bool? _doNotShowFooter;
        private bool? _doNotShowLastEditedOn;
        private bool? _doNotRedirectOnLogin;
        

        public bool DoNotRedirectOnLogin { get { return _doNotRedirectOnLogin.GetValueOrDefault(); } set { _doNotRedirectOnLogin = value; } }
        public bool DoNotShowNavigationBreadcrumbs { get { return _doNotShowNavigationBreadcrumbs.GetValueOrDefault(); } set { _doNotShowNavigationBreadcrumbs = value; } }
        public bool DoNotShowAddThis { get { return _doNotShowAddThis.GetValueOrDefault(); } set { _doNotShowAddThis = value; } }
        public bool DoNotShowFooter { get { return _doNotShowFooter.GetValueOrDefault(); } set { _doNotShowFooter = value; } }
        public bool DoNotShowLastEditedOn { get { return _doNotShowLastEditedOn.GetValueOrDefault(); } set { _doNotShowLastEditedOn = value; } }
        public bool NotVisibleInFrontend { get; set; }




        bool? IArticleDefaultValues.DoNotShowNavigationBreadcrumbs { get { return _doNotShowNavigationBreadcrumbs; } set { _doNotShowNavigationBreadcrumbs = value; } }
        bool? IArticleDefaultValues.DoNotRedirectOnLogin { get { return _doNotRedirectOnLogin; } set { _doNotRedirectOnLogin = value; } }
        bool? IArticleDefaultValues.DoNotShowAddThis { get { return _doNotShowAddThis; } set { _doNotShowAddThis = value; } }
        bool? IArticleDefaultValues.DoNotShowFooter { get { return _doNotShowFooter; } set { _doNotShowFooter = value; } }
        bool? IArticleDefaultValues.DoNotShowLastEditedOn { get { return _doNotShowLastEditedOn; } set { _doNotShowLastEditedOn = value; } }
        
        


        public Enums.ARTICLE_TYPE ArticleType { get; set; }

        public CS.General_v3.Enums.CMS_ACCESS_TYPE AccessTypeRequiredToDelete { get; set; }


        /// <summary>
        /// IMPORTANT: This must be an 'Enum' value.
        /// </summary>
        public object LinkedRouteEnum { get; set; }


        #region IArticleDefaultValues Members

        Enum IArticleDefaultValues.LinkedRoute
        {
            get
            {
                return (Enum)LinkedRouteEnum;
                
            }
            set
            {
                this.LinkedRouteEnum = value;
            }
        }

        #endregion
    }
}
