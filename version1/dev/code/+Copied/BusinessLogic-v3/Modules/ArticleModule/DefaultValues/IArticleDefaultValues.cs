﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Modules.ArticleModule.DefaultValues
{
    public interface IArticleDefaultValues
    {
        string Name { get; set; }
        string PageTitle { get; set; }
        string MetaTitle { get; set; }
        string CustomLink { get; set; }
        bool DialogPage { get; set; }
        int? DialogWidth { get; set; }
        int? DialogHeight { get; set; }
        bool? DoNotShowNavigationBreadcrumbs { get; set; }
        bool? DoNotShowAddThis { get; set; }
        bool? DoNotShowFooter { get; set; }
        bool? DoNotShowLastEditedOn { get; set; }
        bool? DoNotRedirectOnLogin { get; set; }
        string Identifier { get; set; }
        bool NotVisibleInFrontend { get; set; }
        Enum LinkedRoute { get; set; }
        string HtmlText { get; set; }
       // Enums.ARTICLE_TYPE ArticleType { get; set; }
        CS.General_v3.Enums.CMS_ACCESS_TYPE AccessTypeRequiredToDelete { get; set; }


    }
}
