﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.Article_ParentChildLinkModule;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Util;

namespace BusinessLogic_v3.Modules.ArticleModule.DefaultValues
{
    public class DefaultArticleCreator : IArticleDefaultValues
    {
        public string Name { get; set; }
        public string FullIdentifier { get; private set; }
        public string HtmlText { get; set; }
        public string PageTitle { get; set; }
        public string MetaTitle { get; set; }
        public string CustomLink { get; set; }
        public bool DialogPage { get; set; }
        public bool NotVisibleInFrontend { get; set; }
        public int? DialogWidth { get; set; }
        public int? DialogHeight { get; set; }
        public string Identifier { get; set; }
        public Enum LinkedRoute { get; set; }

        public bool? DoNotShowNavigationBreadcrumbs { get; set; }
        public bool? DoNotRedirectOnLogin { get; set; }
        public bool? DoNotShowAddThis { get; set; }
        public bool? DoNotShowFooter { get; set; }
        public bool? DoNotShowLastEditedOn { get; set; }



        public Enums.ARTICLE_TYPE ArticleType { get; set; }
        public DefaultArticleCreator(string identifier, Enums.ARTICLE_TYPE articleType)
        {
            this.FullIdentifier = identifier;
            this.ArticleType = articleType;

            int lastUnderscore = identifier.LastIndexOf('_');
            int lastDashPos = identifier.LastIndexOf('-');
            if (lastDashPos == -1) lastDashPos = identifier.Length;
            
            this.Name = CS.General_v3.Util.Text.AddSpacesToCamelCasedText(identifier.Substring(lastUnderscore + 1, lastDashPos - lastUnderscore-1));
            

        }
        public void FillFromDefaultValues(IArticleDefaultValues values)
        {
            if (values != null)
            {
                this.AccessTypeRequiredToDelete = values.AccessTypeRequiredToDelete;
                this.CustomLink = values.CustomLink;
                this.HtmlText = values.HtmlText;
                this.Name = values.Name;
                this.PageTitle = values.PageTitle;
                this.MetaTitle = values.MetaTitle;
                this.DialogHeight = values.DialogHeight;
                this.DialogPage = values.DialogPage;
                this.DialogWidth = values.DialogWidth;
                this.DoNotShowNavigationBreadcrumbs = values.DoNotShowNavigationBreadcrumbs;
                this.DoNotRedirectOnLogin = values.DoNotRedirectOnLogin;
                this.DoNotShowAddThis = values.DoNotShowAddThis;
                this.DoNotShowFooter = values.DoNotShowFooter;
                this.DoNotShowLastEditedOn = values.DoNotShowLastEditedOn;
                this.LinkedRoute = values.LinkedRoute;
                this.NotVisibleInFrontend = values.NotVisibleInFrontend;
                this.Identifier = values.Identifier;
                //CS.General_v3.Util.ReflectionUtil.CopyProperties<IArticleDefaultValues>(values, this);
            }

        }
        public ArticleBase CreateDefaultArticle(ArticleBase parentToAddTo, bool autoSave = true)
        {
            

            ArticleBase result  = null;
            //if (ArticleType == Enums.ARTICLE_TYPE.None) throw new InvalidOperationException("Cannot create a default article without specifying the article type");
            var identifiers = CS.General_v3.Util.Text.Split(FullIdentifier, "_");
            if (string.IsNullOrWhiteSpace(this.Name)) this.Name = CS.General_v3.Util.Text.AddSpacesToCamelCasedText(identifiers.LastOrDefault());
            if (string.IsNullOrWhiteSpace(this.HtmlText)) this.HtmlText = this.Name;

            string currID = null;
            

            ArticleBase parent = parentToAddTo;
            
            for (int i = 0; i < identifiers.Count; i++)
            {
                if (i > 0)
                    currID += "_";
                currID += identifiers[i];
                var newItem = ArticleBaseFactory.Instance._getArticleByIdentifierFromDb(currID);
                if (newItem == null)
                {
                    //if (i < identifiers.Count - 1) // not last item
                    //{
                    //    DefaultArticleCreator creator = new DefaultArticleCreator(currID, this.ArticleType);
                    //    creator.FillFromDefaultValues(this);
                    //    newItem = creator.CreateDefaultArticle(parent);
                    //}
                    //else
                    { //last item, thus create
                        newItem = null;
                            
                        var session = nHibernateUtil.GetCurrentSessionFromContext();
                        
                        using (var t = autoSave? session.BeginTransaction() : null)
                        {
                            newItem = ArticleBase.Factory.CreateNewItem();
                            if (parent != null)
                            {

                                //Article_ParentChildLinkBase link = Article_ParentChildLinkBase.Factory.CreateNewItem();
                                //link.Child = newItem;
                                //link.Parent = parent;
                                //newItem.ParentArticleLinks.Add(link);
                                //parent.ChildArticleLinks.Add(link);
                                newItem.AddParentArticle(parent, autoSave: false);


                            }
                            newItem.ArticleType = this.ArticleType;
                            newItem.CustomLink = CustomLink;
                            newItem.DoNotRedirectOnLogin = DoNotRedirectOnLogin.GetValueOrDefault(false);
                            newItem.PageTitle = PageTitle;
                            newItem.MetaTitle = MetaTitle;
                            newItem.DialogPage = DialogPage;
                            newItem.DoNotShowNavigationBreadcrumbs = DoNotShowNavigationBreadcrumbs;
                            newItem.DoNotShowAddThis = DoNotShowAddThis;
                            newItem.DoNotShowFooter = DoNotShowFooter;
                            newItem.DoNotShowLastEditedOn = DoNotShowLastEditedOn;
                            newItem.NotVisibleInFrontend = this.NotVisibleInFrontend;

                              newItem.Identifier = this.Identifier;
                            newItem.DialogHeight = DialogHeight;
                            newItem.DialogWidth = DialogWidth;
                            newItem.Name = CS.General_v3.Util.Text.AddSpacesToCamelCasedText(identifiers[i]);
                            newItem.Save();
                            if (i == identifiers.Count - 1)
                            {
                                //copy all properties

                                CS.General_v3.Util.ReflectionUtil.CopyProperties<IArticleDefaultValues>(this, newItem);
                            }
                            if (string.IsNullOrWhiteSpace(newItem.Identifier))
                            {
                            
                                newItem.Identifier = currID;
                            }

                        if (string.IsNullOrEmpty(newItem.PageTitle))
                            {
                                newItem.PageTitle = newItem.Name;
                            }
                            //if (parent != null)
                            //{
                            //    parent.Update();
                            //}
                            //session.Save(newItem);
                            if (this.LinkedRoute != null)
                            {
                                newItem.LinkedRoute = Modules.RoutingInfoModule.RoutingInfoBaseFactory.Instance.GetRouteByIdentifier(this.LinkedRoute);
                            }
                            newItem.Save();
                          //  NHibernate.ITransaction t2 = session.BeginTransaction();

                            
                            if (autoSave && t.IsActive)
                                t.Commit();
                          //  t2.Commit();
                        }
                        

                    }


                }
                parent = newItem;

            }
            result = parent;
            return result;


        }




        public CS.General_v3.Enums.CMS_ACCESS_TYPE AccessTypeRequiredToDelete { get; set; }



    }
}
