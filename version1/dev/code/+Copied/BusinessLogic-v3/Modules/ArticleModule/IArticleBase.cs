using BusinessLogic_v3.Modules.ArticleMediaItemModule;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using CS.General_v3.Classes.Interfaces.Hierarchy;

namespace BusinessLogic_v3.Modules.ArticleModule
{
    using Classes.Interfaces;
using BusinessLogic_v3.Modules.ArticleRatingModule;
    using BusinessLogic_v3.Modules.ArticleCommentModule;
    using Classes.MediaItems;
    using CS.General_v3.Classes.Interfaces.BlogPosts;

//IBaseClass-File

    public interface IArticleBase : BusinessLogic_v3.Modules._AutoGen.IArticleBaseAutoGen,
        IHierarchyNavigation, 
        IItemWithContentTags
    {

        void AddParent(IArticleBase parent);
        void AddChild(IArticleBase child);
        IEnumerable<IArticleBase> GetArticlesByKeywords(string keywords, int pageNo, int showAmt, out int totalResults, bool showOnlyLeafNodes = true);
        IEnumerable<IArticleBase> GetChildrenByFilter(int pageNo, int pageSize, Enums.CONTENT_PAGE_SORT_BY sortBy, out int totalResults);
        void RemoveParent(IArticleBase parent);

        void RemoveChild(IArticleBase child);

        bool ContainsContent();

        string GetUrl();

       
        IEnumerable<IArticleBase> GetChildArticles();
        
        bool HasChildArticles();
        bool NotVisibleInFrontend { get; }
        bool NotVisibleInFrontendWhenLoggedIn { get; }
        bool VisibleInFrontendOnlyWhenLoggedIn { get; }
        ArticleRatingManager RatingManager { get; }

        /// <summary>
        /// Return the total amount of comment + replies for this article
        /// </summary>
        /// <returns></returns>
        int GetCommentsAndRepliesCount();
        IEnumerable<IArticleCommentBase> GetComments(bool OrderByDescendingDate = true);
        ArticleBase GetParentMain();
        IArticleBase GetPreviousArticle();
        IArticleBase GetNextArticle();
        CS.General_v3.Enums.COMMENT_POST_RESULT PostComment(ICommentData data, bool autoSave = true);
        IEnumerable<IArticleMediaItemBase> GetMediaItems(bool excludeMainImage = false);

        string GetContent(bool parseLinks = true);
        string _HtmlText_ForEditing { get; set; }

        IMediaItemImage<ArticleBase.ImageSizingEnum> MainImage { get; }

        /// <summary>
        /// Checks the current session, and if the page hasn't had its counter incremented in the current session, increment it.
        /// </summary>
        /// <param name="autoSave">If true, DB update is done inside a transaction - In general this should be true.</param>
        void IncrementViewCount(bool autoSave = true);

        /// <summary>
        /// Returns the ViewCount of this article.
        /// </summary>
        /// <returns></returns>
        int GetViewCount();

        /// <summary>
        /// Returns true if the current logged user has read this article.
        /// </summary>
        /// <param name="throwErrorIfUserNotLoggedIn">If no user is currently logged in and this is true, an error will be thrown - if false and user is null return value will be false.</param>
        /// <returns></returns>
        bool IsCurrentUserReadArticle(bool throwErrorIfUserNotLoggedIn = true);
        
        /// <summary>
        /// Marks this article as read by the user currently logged in.
        /// </summary>
        /// <param name="throwErrorIfUserNotLoggedIn">If no user is currently logged in and this is true, an error will be thrown.</param>
        /// <param name="autoSave">Wrap change in transaction.</param>
        void MarkReadForCurrentUser(bool throwErrorIfUserNotLoggedIn = true, bool autoSave = true);


        IEnumerable<IArticleBase> GetLatestChildArticlesRespectiveToDate(int amount, DateTime dateToConsider, bool loadOnlyLeafNodes = true);
        IEnumerable<IArticleBase> GetRelatedArticles(int amount, bool loadOnlyLeafNodes = true, DateTime? dateToConsider = null);

        IEnumerable<ArticleBase> GetAllParentArticlesWithRecursion(bool includeThis = true);
        IEnumerable<IArticleBase> GetAllChildArticlesFromAllGenerations(int takeAmount, bool includeThis);
    }
}
