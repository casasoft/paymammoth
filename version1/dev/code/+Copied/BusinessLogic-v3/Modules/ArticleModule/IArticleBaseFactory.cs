using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;

namespace BusinessLogic_v3.Modules.ArticleModule
{
    using Helpers;

//IBaseFactory-File

	public interface IArticleBaseFactory : BusinessLogic_v3.Modules._AutoGen.IArticleBaseFactoryAutoGen
    {

        IEnumerable<ArticleReadAndCommentsHelper> LoadArticlesReadAndCommentedAmounts(IEnumerable<long> articleIDs);

        void MarkArticleAsReadForCurrentUser(IArticleBase article);
        IArticleBase GetArticleByStringIdentifier(string identifier);
        IArticleBase GetArticleByIdentifier(Enum identifier);
        IArticleBase GetRoot();

        bool CanRedirectToURLOnLogin(string url);
        
        IEnumerable<IArticleBase> GetLatestNews(int amount);
        IEnumerable<IArticleBase> GetBlogs();

        IEnumerable<IArticleBase> GetArticlesForParents(IEnumerable<IArticleBase> parentPage, int pageNo, int pageSize, Enums.CONTENT_PAGE_SORT_BY sortBy, out int totalResults, bool showOnlyLeafNodes = false);
        IEnumerable<IArticleBase> GetArticlesForParent(IArticleBase parentPage, int pageNo, int pageSize, Enums.CONTENT_PAGE_SORT_BY sortBy, out int totalResults, bool showOnlyLeafNodes = false);
        IEnumerable<IArticleBase> GetFeaturedArticles(IArticleBase contentPage, int pageNo, int pageSize, out int totalResults);
        IArticleBase GetArticleFromRouteData();
        IEnumerable<IArticleBase> GetRootNodes();

        string GetUrlForGeneralContentPage(Enums.CONTENT_PAGE_IDENTIFIER identifier);

	    /// <summary>
	    /// Returns featured articles, in descending order of last edited date and limited to the specified amount - if not enough are found, it will add articles from the same category but includes nonfeatured too, same applies when no category is specified.
	    /// </summary>
	    /// <param name="amount">The amount of articles to be loaded.</param>
	    /// <param name="category">The parent node for which the articles must be filtered in.</param>
	    /// <param name="loadArticlesOnlyBeforeThisDate">If a date is specified, then load only articles before this date</param>
	    /// /// <param name="takeNowAsDateIfNotSpecified">If true and no date is specified, it will take now as the relative date.</param>
	    /// <returns></returns>
	    IEnumerable<IArticleBase> GetFeaturedArticlesFromCategory(int amount, IArticleBase category,
	                                                              DateTime? loadArticlesOnlyBeforeThisDate = null,
	                                                              bool takeNowAsDateIfNotSpecified = true);

	    IEnumerable<IArticleBase> GetMostReadArticles(int amount, DateTime endDate, DateTime startDate,
	                                                         IArticleBase category = null,
	                                                         bool considerOnlyLeafNodes = true);

	    IEnumerable<IArticleBase> GetMostCommentedArticles(int amount, DateTime endDate, DateTime startDate,
	                                                       IArticleBase category = null, bool considerOnlyLeafNodes = true);

	    IEnumerable<IArticleBase> GetLatestNewArticlesBetweenDates(int maxAmount, DateTime fromDate, DateTime toDate,
	                                                            bool considerOnlyLeafNodes = true);

	    IEnumerable<IArticleBase> GetAllChildArticlesFromAllGenerationsForArticle(IArticleBase article, int takeAmount,
	                                                                              bool includeThis);

	    IEnumerable<IArticleBase> GetLatestChildArticlesRespectiveToDateForArticle(IArticleBase article, bool includeThis,
	                                                                               int takeAmount, DateTime dateToConsider,
	                                                                               bool loadOnlyLeafNodes = true);
    }

}
