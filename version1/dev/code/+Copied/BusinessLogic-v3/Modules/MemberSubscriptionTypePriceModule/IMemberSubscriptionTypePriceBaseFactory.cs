using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;

namespace BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule
{   

//IBaseFactory-File

	public interface IMemberSubscriptionTypePriceBaseFactory : BusinessLogic_v3.Modules._AutoGen.IMemberSubscriptionTypePriceBaseFactoryAutoGen
    {
        IEnumerable<IMemberSubscriptionTypePriceBase> GetFreeSubscriptionTypePrices();
    }
}
