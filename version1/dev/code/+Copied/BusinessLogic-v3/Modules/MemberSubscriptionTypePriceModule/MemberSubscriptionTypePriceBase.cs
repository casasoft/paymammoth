using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule
{
    using Extensions;

//BaseClass-File
	
    public abstract class MemberSubscriptionTypePriceBase : BusinessLogic_v3.Modules._AutoGen.MemberSubscriptionTypePriceBaseAutoGen, IMemberSubscriptionTypePriceBase
    {
    
		#region BaseClass-AutoGenerated
		#endregion
    
        public MemberSubscriptionTypePriceBase()
        {
            
        }    
    	protected override void initPropertiesForNewItem()
        {
        	//Fill any default values for NEW items.
        
            base.initPropertiesForNewItem();
        }
    	public override string ToString()
        {
            return base.ToString();
        }


        public double GetPriceExcTax()
        {
            double vatRate = CS.General_v3.Enums.SETTINGS_ENUM.Others_VatRate.GetSettingValue<double>();
            return CS.General_v3.Util.NumberUtil.RemoveTaxFromValue(Price, vatRate);
        }
    }
}
