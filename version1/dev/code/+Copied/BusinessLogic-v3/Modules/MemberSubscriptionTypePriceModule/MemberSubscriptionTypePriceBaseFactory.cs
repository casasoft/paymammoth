using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule
{   

//BaseFactory-File

	public abstract class MemberSubscriptionTypePriceBaseFactory : BusinessLogic_v3.Modules._AutoGen.MemberSubscriptionTypePriceBaseFactoryAutoGen, IMemberSubscriptionTypePriceBaseFactory
    {
        public IEnumerable<IMemberSubscriptionTypePriceBase> GetFreeSubscriptionTypePrices()
        {
            return FindAll((GetQuery()).Where(item => item.Price <= 0));
        }
    }
}
