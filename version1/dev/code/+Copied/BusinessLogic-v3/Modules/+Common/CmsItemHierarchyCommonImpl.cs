﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using CS.General_v3.Controls.WebControls.Classes;
using CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructure;

namespace BusinessLogic_v3.Modules._Common
{
    public class CmsItemHierarchyCommonImpl  : HierarchyCommonImpl
    {
        public ICmsHierarchyInfo CmsHierarchy
        {
            get
            {
                return (ICmsHierarchyInfo)base.Item;
            }
            
        }
        public Type ItemType { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmsHierarchy"></param>
        /// <param name="itemType">Type of item that this is linked to, e.g Category or ContentPage </param>
        //public CmsItemHierarchyCommonImpl(ICmsHierarchy cmsHierarchy, Type itemType) : base(cmsHierarchy)
        public CmsItemHierarchyCommonImpl(ICmsHierarchyInfo cmsHierarchy, Type itemType)
            : base(cmsHierarchy)
        {
            this.ExtraButtons = new List<ExtraButton>();
            if (itemType == null)
                itemType = cmsHierarchy.GetType();
            this.ItemType = itemType;
            this.ImageUrl = "/_common/images/components/v1/tree/icon_category.gif";
            
        }

        public string Message_DeleteOK
        {
            get { return "Item deleted successfully"; }
        }

        public string Message_ConfirmDelete
        {
            get { return "Are you sure you want to delete?"; }
        }

        protected ICmsItemFactory getFactoryForType()
        {
            return CmsSystemBase.Instance.GetFactoryForType(ItemType);
        }
        /// <summary>
        /// Returns the factory for the 'children' type
        /// </summary>
        /// <returns></returns>
        protected ICmsItemFactory getFactoryForSubItemType()
        {
            return this.CmsHierarchy.SubitemLinkedCollection.GetFactoryForLinkedObject();
        }
        public string EditURL //for CMS
        {
            get { return getFactoryForType().GetEditUrlForItemWithID(this.Item.ID).ToString(); }
        }

        public string AddNewItemURL
        {
            get 
            {
                if (CmsHierarchy.SubitemLinkedCollection == null)
                    throw new InvalidOperationException("If you intend to call 'AddNewItemUrl' of the common implementation, make sure that you set the 'SubitemLinkedProperty' info first");

                var factory = getFactoryForSubItemType();
                var url = factory.GetAddUrl();
                string qsValue = CmsHierarchy.SubitemLinkedCollection.GetLinkedQuerystringValueForItem(this.CmsHierarchy);// BusinessLogic_v3.Classes.Cms.Util.CmsUtil.GetLinkQuerystringValue(
                url[CmsHierarchy.SubitemLinkedCollection.GetLinkedQuerystringParamName()] = qsValue;// ((ITreeItem)this.CmsHierarchy).ID;
                return url.ToString();
            }
        }
        public bool AddedExtraButtons { get; set; }
        public List<ExtraButton> ExtraButtons { get; private set; }
        public string ImageUrl {get;set;}
        private ExtraButton getExtraButton()
            {
                ExtraButton btn = new ExtraButton();
                btn.CssClass = "tree_button";
                return btn;
            }
        //private void initDefaultButtons()
        //{
        //    ExtraButton extraBtn = null;
        //        if (((ITreeItem)Item).AllowUpdate)
        //        {

        //            extraBtn = getExtraButton();
        //            extraBtn.Href = this.EditURL;
        //            extraBtn.ImageUrl_Over =  this.ImageURL_Edit_Over;
        //            extraBtn.ImageUrl_Up = this.ImageURL_Edit_Up;
        //            extraBtn.Title = "Edit " + this.ItemTitle;
        //            item.ExtraButtons.Add(extraBtn);
        //        }
        //        else
        //        {
        //            item.ExtraButtons.Add(null);
        //        }
        //        if (item.AllowDelete)
        //        {
        //            extraBtn = GetExtraButton();
        //            extraBtn.Click += new EventHandler(btnRootItem_Delete);
        //            extraBtn.ImageUrl_Over = this.ImageURL_Delete_Over;
        //            extraBtn.ImageUrl_Up = this.ImageURL_Delete_Up;
        //            extraBtn.Title = "Delete " + this.ItemTitle;

        //            extraBtn.ConfirmMessageOnClick = item.Message_ConfirmDelete;
        //            item.ExtraButtons.Add(extraBtn);
        //        }
        //        else
        //        {
        //            item.ExtraButtons.Add(null);
        //        }
        //        if (item.AllowAddSubItems)
        //        {
        //            extraBtn = GetExtraButton();
        //            extraBtn.Href = item.AddNewItemURL;
        //            extraBtn.ImageUrl_Over = this.ImageURL_AddSubItems_Over;
        //            extraBtn.ImageUrl_Up = this.ImageURL_AddSubItems_Up;
        //            extraBtn.Title = "Add new " + this.ItemTitle;
        //            item.ExtraButtons.Add(extraBtn);
        //        }
        //        else
        //        {
        //            item.ExtraButtons.Add(null);
        //        }
        //        item.AddedExtraButtons = true;
        //    }
        //}
        public string LinkUrl
        {
            get
            {
                return this.EditURL;
            }
        }

        public void AddExtraButtons()
        {
            
        }



       
    }
}
