using BusinessLogic_v3.Modules.ShippingRatesGroupModule;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using nHibernateUtil = BusinessLogic_v3.Util.nHibernateUtil;

namespace BusinessLogic_v3.Modules.ShippingMethodModule
{   

//BaseFactory-File

	public abstract class ShippingMethodBaseFactory : BusinessLogic_v3.Modules._AutoGen.ShippingMethodBaseFactoryAutoGen, IShippingMethodBaseFactory
    {

        #region IShippingMethodBaseFactory Members
        public IEnumerable<ShippingMethodBase> GetApplicableDeliveryMethodsForCountry(CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166 countryCode)
        {
            var q = GetQuery();
            var tmp = q.JoinQueryOver(method => method.ShippingRatesGroups);

            string code2Letter = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_To2LetterCode(countryCode);
            string code3Letter = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_To3LetterCode(countryCode);

            var orExpression = Restrictions.Disjunction();
            nHibernateUtil.GetAssociationLinkCriteriaByPath<ShippingMethodBase>(q.RootCriteria, item => item.ShippingRatesGroups);

            orExpression.Add(Restrictions.Where<ShippingRatesGroupBase>(rateGroup => rateGroup.AppliesToRestOfTheWorld));//if rate group applies to rest of the world, then it applies irrespective to the country code

            //The reason there are these 4 matches, are so as to match those with a comma before, or last, so that all are found, even if they are in the beginning or last


            orExpression.Add(Restrictions.On<ShippingRatesGroupBase>(rateGroup => rateGroup.CountriesApplicable).IsLike(ShippingRatesGroupBase.COUNTRY_DELIMITER + code2Letter, MatchMode.Anywhere)); //else try to match country codes
            orExpression.Add(Restrictions.On<ShippingRatesGroupBase>(rateGroup => rateGroup.CountriesApplicable).IsLike(code2Letter + ShippingRatesGroupBase.COUNTRY_DELIMITER, MatchMode.Anywhere));

            orExpression.Add(Restrictions.On<ShippingRatesGroupBase>(rateGroup => rateGroup.CountriesApplicable).IsLike(code3Letter + ShippingRatesGroupBase.COUNTRY_DELIMITER, MatchMode.Anywhere));
            orExpression.Add(Restrictions.On<ShippingRatesGroupBase>(rateGroup => rateGroup.CountriesApplicable).IsLike(ShippingRatesGroupBase.COUNTRY_DELIMITER + code3Letter, MatchMode.Anywhere));
            tmp.Where(orExpression);

            return FindAll(q);
        }

        #endregion

        #region IShippingMethodBaseFactory Members

        IEnumerable<IShippingMethodBase> IShippingMethodBaseFactory.GetApplicableDeliveryMethodsForCountry(CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166 countryCode)
        {
            return GetApplicableDeliveryMethodsForCountry(countryCode);
        }

        #endregion
        public IEnumerable<ShippingMethodBase> GetAllShippingMethods()
        {
            var q = GetQuery();
            q.Cacheable();
            return FindAll(q);

        }

        public IShippingMethodBase GetDefaultShippingMethod()
        {
            var q = GetQuery();
            q.Where(item => item.IsDefaultMethod);
            return FindAll(q).FirstOrDefault();
        }
    }
}
