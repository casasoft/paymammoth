using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.ShippingMethodModule
{

//BaseClassMap-File
    
    public abstract class ShippingMethodBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.ShippingMethodBaseMap_AutoGen<TData>
    	where TData : ShippingMethodBase
    {

        public override Enums.NHIBERNATE_CACHE_TYPE CacheType
        {
            get
            {
                return Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            }
            set
            {
                base.CacheType = value;
            }
        }
 
    }
   
}
