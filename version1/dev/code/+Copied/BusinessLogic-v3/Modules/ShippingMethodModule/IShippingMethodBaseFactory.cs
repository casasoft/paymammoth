using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;

namespace BusinessLogic_v3.Modules.ShippingMethodModule
{   

//IBaseFactory-File

	public interface IShippingMethodBaseFactory : BusinessLogic_v3.Modules._AutoGen.IShippingMethodBaseFactoryAutoGen
    {

        IEnumerable<IShippingMethodBase> GetApplicableDeliveryMethodsForCountry(CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166 countryCode);
	    IShippingMethodBase GetDefaultShippingMethod();
    }

}
