using BusinessLogic_v3.Modules.CultureDetailsModule;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Modules.KeywordModule
{   

//BaseFactory-File

	public abstract class KeywordBaseFactory : BusinessLogic_v3.Modules._AutoGen.KeywordBaseFactoryAutoGen, IKeywordBaseFactory
    {


        /// <summary>
        /// 
        /// </summary>
        /// <param name="text">Typed text</param>
        /// <param name="typingPosition">Typing position.  This is used to get the current keyword from</param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public List<string> GetKeywordsFor(string text, int typingPosition, ICultureDetailsBase culture, int limitKeywords = 10)
        {
            List<string> list = new List<string>();
            if (!string.IsNullOrEmpty(text))
            {
                int spaceBefore = CS.General_v3.Util.Text.LastIndexOfBeforePosition(text, " ", typingPosition);
                int spaceAfter = text.IndexOf(" ", typingPosition);
                if (spaceAfter == -1) spaceAfter = text.Length;
                string currentKeyword = CS.General_v3.Util.Text.GetTextBetweenIndexes(text, spaceBefore + 1, spaceAfter - 1);
                string textBefore = "";
                if (spaceBefore > 0)
                    textBefore = text.Substring(0, spaceBefore + 1);
                string textAfter = "";
                if (text.Length > spaceAfter)
                    textAfter = text.Substring(spaceAfter);

                var tokens = CS.General_v3.Util.Text.Split(text, " ");
                string keyword = tokens.LastOrDefault();


                var keywordsDb = _getKeywordsFor(keyword, culture, count: limitKeywords);
                foreach (var k in keywordsDb)
                {
                    if (k != null)
                    {
                        list.Add(textBefore + k.Keyword + textAfter);
                    }
                }
            }
            return list;
        }

        private IEnumerable<IKeywordBase> _getKeywordsFor(string text,ICultureDetailsBase culture, int count = 5)
        {
            var q = GetQuery();
            q = q.Where(k => k.CultureInfo.ID == culture.ID);
            q = q.WhereRestrictionOn(k => k.Keyword).IsLike(text, MatchMode.Start);
            q = q.OrderBy(k => k.FrequencyCount).Desc;
            q = q.OrderBy(k => k.Keyword).Asc;

            q.Take(count);
            return FindAll(q);
        }




        public List<string> GetKeywordsForCurrentCulture(string text)
        {
            int typingPos = 0;
            if (text != null)
                typingPos = text.Length;
            return GetKeywordsForCurrentCulture(text, typingPos);
        }

        public List<string> GetKeywordsForCurrentCulture(string text, int typingPosition)
        {

            var culture = CultureDetailsBaseFactory.Instance.GetCurrentCultureInSession();

            return GetKeywordsFor(text, typingPosition, culture);


        }

        

        #region IKeywordBaseFactory Members

        

        #endregion

      

        #region IKeywordBaseFactory Members

        IEnumerable<string> IKeywordBaseFactory.GetKeywordsFor(string text, int typingPosition, ICultureDetailsBase culture, int limitKeywords = 10)
        {
            return this.GetKeywordsFor(text, typingPosition, culture, limitKeywords);
            
        }

        #endregion
    }

}
