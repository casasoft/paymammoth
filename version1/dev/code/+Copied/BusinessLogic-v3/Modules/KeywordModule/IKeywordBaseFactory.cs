using BusinessLogic_v3.Modules.CultureDetailsModule;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;

namespace BusinessLogic_v3.Modules.KeywordModule
{   

//IBaseFactory-File

	public interface IKeywordBaseFactory : BusinessLogic_v3.Modules._AutoGen.IKeywordBaseFactoryAutoGen
    {

        IEnumerable<string> GetKeywordsFor(string text, int typingPosition, ICultureDetailsBase culture, int limitKeywords = 10);

    }

}
