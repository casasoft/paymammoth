using BusinessLogic_v3.Classes.Cms.CmsObjects.Access;
using BusinessLogic_v3.Classes.MediaItems;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Modules.TicketModule;

namespace BusinessLogic_v3.Modules.CmsUserModule
{

    //IBaseClass-File

    public interface ICmsUserBase : BusinessLogic_v3.Modules._AutoGen.ICmsUserBaseAutoGen, ITicketingSystemUser
    {
        bool CheckAccess(CmsAccessType cmsAccessType);
        bool CheckAccessIsLessThan(CS.General_v3.Enums.CMS_ACCESS_TYPE? reqAccessType);
        bool CheckAccessAtMostIs(CS.General_v3.Enums.CMS_ACCESS_TYPE? reqAccessType);
        bool CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE? reqAccessType);


        string GetFullName();
        IMediaItemImage<CmsUserBase.CmsCustomLogoSizingEnum> CmsCustomLogo { get; }
        void SetAsTicketingSystemSupportUser();
        void UnsetAsTicketingSystemSupportUser();
        void UnassignAllOpenTicketsFromThisUser();
        SearchPagedResults<TicketBase> GetAllAssignedTickets(int pageNo, int showAmt, Enums.TICKET_SORTBY sortBy);
        SearchPagedResults<TicketBase> GetAllOpenAssignedTickets(int pageNo, int showAmt, Enums.TICKET_SORTBY sortBy);
        SearchPagedResults<TicketBase> GetAllClosedAssignedTickets(int pageNo, int showAmt, Enums.TICKET_SORTBY sortBy);
        SearchPagedResults<TicketBase> GetAllReopenedAssignedTickets(int pageNo, int showAmt, Enums.TICKET_SORTBY sortBy);
    }
}
