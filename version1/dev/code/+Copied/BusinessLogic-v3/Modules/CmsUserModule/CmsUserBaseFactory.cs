using BusinessLogic_v3.Modules.AffiliateModule;
using BusinessLogic_v3.Modules.CmsUserRoleModule;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Modules.CmsUserModule
{   

//BaseFactory-File

	public abstract class CmsUserBaseFactory : BusinessLogic_v3.Modules._AutoGen.CmsUserBaseFactoryAutoGen, ICmsUserBaseFactory
    {

        protected override void fillInitialItemsInEmtpyDb()
        {
          //   Karl 17/may/2012 - this was removed.  We will now be creating standard items from the template DB, and not here.
            var cmsUsers = CmsUserBase.Factory.FindAll();
            if (cmsUsers.IsNullOrEmpty())
            {
                using (var t = beginTransaction())
                {
                    {
                        CmsUserBase user = CmsUserBase.Factory.CreateNewItem();
                        user.AccessType = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft;
                        user.Name = "CasaSoft";
                        user.Password = "cmskc1252";
                        user.Surname = "Ltd.";
                        user.Username = "casasoft";
                        user.Create();
                    }
                    {
                        CmsUserBase user = CmsUserBase.Factory.CreateNewItem();
                        user.AccessType = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft;
                        user.Name = "CasaSoft";
                        user.Password = "CSDevTeam2011";
                        user.Surname = "Development Team";
                        user.Username = "csdev";
                        user.Create();
                    }
                    {
                        CmsUserBase user = CmsUserBase.Factory.CreateNewItem();
                        user.AccessType = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner;
                        user.HiddenFromNonCasaSoft = true;
                        user.Name = "CasaSoft";
                        user.Surname = "(Owner Mode)";
                        user.Username = "casasoft-owner";
                        user.Password = "cmskc1252";
                        user.Create();
                    }
                    {
                        CmsUserBase user = CmsUserBase.Factory.CreateNewItem();
                        user.AccessType = CS.General_v3.Enums.CMS_ACCESS_TYPE.Normal;
                        user.HiddenFromNonCasaSoft = true;
                        user.Name = "CasaSoft";
                        user.Surname = "(Normal Mode)";
                        user.Username = "casasoft-normal";
                        user.Password = "cmskc1252";
                        user.Create();
                    }
                    {
                        CmsUserBase user = CmsUserBase.Factory.CreateNewItem();
                        user.AccessType = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner;
                        user.Name = "Admin";
                        user.Password = "admin";
                        user.Surname = "";
                        user.Username = "admin";
                        user.Create();
                    }
                    t.CommitIfActiveElseFlush();
                }
            }
            base.fillInitialItemsInEmtpyDb();
        }


        public CmsUserBase GetCmsUserByUsername(string username, CmsUserBase excludeUser = null)
        {
            var q = GetQuery();
            q = q.Where(user => user.Username == username);
            if (excludeUser != null)
            {
                q = q.Where(user => user.ID != excludeUser.ID);
            }
            return FindItem(q);
        }
        public CmsUserBase GetCmsUserForAffiliate(AffiliateBase affiliate, bool createIfDoesNotExist)
        {
            var cmsUser = affiliate.LinkedCmsUser;


            if (cmsUser == null && createIfDoesNotExist)
            {
                using (var transaction = beginTransaction())
                {
                    cmsUser = CmsUserBase.Factory.CreateNewItem();
                    affiliate.LinkedCmsUser = cmsUser;
                    
                    cmsUser.CmsUserRoles.Add((CmsUserRoleBase)CmsUserRoleBase.Factory.GetByIdentifier(Enums.CmsUserRoleEnum.Affiliate));
                    cmsUser.CreateAndCommit();
                    transaction.Commit();
                }
                // affiliate.Save();
            }
            return cmsUser;
        }

        public IEnumerable<ICmsUserBase> GetAllTicketingSystemSupportUsers()
        {
            return
                ((IEnumerable<ICmsUserBase>) this.__getAllItemsInRepository()).Where(
                    item => item.TicketingSystemSupportUser);
        }

        public override void OnPostInitialisation()
        {
            base.OnPostInitialisation();
          
        }

    }

}
