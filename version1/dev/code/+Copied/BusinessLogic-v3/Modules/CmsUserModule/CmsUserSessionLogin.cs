﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Util;
using CS.General_v3.Classes.Login;

namespace BusinessLogic_v3.Modules.CmsUserModule
{
    public class CmsUserSessionLogin : CS.General_v3.Classes.Login.WebUserSessionLoginManager<CmsUserBase>
    {

        private readonly object _padlock = new object();

        public static CmsUserSessionLogin Instance
        {
            get
            {
                
                return CS.General_v3.Classes.Singletons.Singleton.GetInstance<CmsUserSessionLogin>();
            }
        }

        private CmsUserSessionLogin(): base(null, "CMSUserLogin")
        {
            this.OnLoginSuccessful += new EventHandler(CmsUserSessionLogin_OnLoginSuccessful);
           
        }



        void CmsUserSessionLogin_OnLoginSuccessful(object sender, EventArgs e)
        {
            BusinessLogic_v3.Modules.AuditLogModule.Manager.AuditLogManager.Instance.AddAuditLog(Enums.AUDITLOG_MSG_TYPE.Login,this.GetLoggedInUser(), "User logged in");
            
        }

        
        protected override bool checkPassword(CmsUserBase user, string password)
        {
            if (!user.Disabled)
            {

                return base.checkPassword(user, password);
            }
            else
            {
                return false;
            }
        }
        private static bool _AutoLoginForDevMachines_Once = false;
        private void checkAutoLoginForDevMachines()
        {
            if (!_AutoLoginForDevMachines_Once)
            {
                lock (_padlock)
                {
                    if (!_AutoLoginForDevMachines_Once && !IsLoggedIn && CS.General_v3.Util.PageUtil.CheckSessionExistsAtContext())
                    {
                        _AutoLoginForDevMachines_Once = true;
                        if (CS.General_v3.Util.Other.IsLocalTestingMachine )
                        {
                            bool createdSesssion = false;
                            var session = nHibernateUtil.GetCurrentSessionFromContext();
                            if (session == null)
                            {
                                createdSesssion = true;
                                session = nHibernateUtil.CreateNewSessionForCurrentContext();
                            }

                            var q =
                                CmsUserBase.Factory.GetQuery().OrderBy(item => item.AccessType).Desc;



                            var cmsUser = CmsUserBase.Factory.FindItem(q);
                            loginUser(cmsUser, false);
                            if (createdSesssion)
                            {
                                nHibernateUtil.DisposeCurrentSessionInContext();
                            }
                        }

                    }
                }
            }
        }
        



        protected override void loginUser(CmsUserBase user, bool fromAdmin)
        {
            base.loginUser(user, fromAdmin);
        }

        public new CmsUserBase GetLoggedInUser()
        {
            return this.GetLoggedInUser(checkForAutoLogin:true);
        }

        public new CmsUserBase GetLoggedInUser(bool checkForAutoLogin= true)
        {
            if (checkForAutoLogin)
            {
            
                checkAutoLoginForDevMachines();
            }

            return (CmsUserBase)base.GetLoggedInUser();
            
        }
        public  bool CheckAuthentication(CS.General_v3.Enums.CMS_ACCESS_TYPE AccessTypeRequired)
        {
            bool b = base.CheckAuthentication();
            if (b)
            {
                b = CS.General_v3.Enums.CheckAdminUserAccess(this.GetLoggedInUser().GetUserAccessType(), AccessTypeRequired);
            }
            return b;
        }
        
        

        public override bool CheckAuthentication()
        {
            return CheckAuthentication(CS.General_v3.Enums.CMS_ACCESS_TYPE.Normal);
            
        }



        public override CmsUserBase GetUserByID(long id)
        {
            return CmsUserBase.Factory.GetByPrimaryKey((long)id);
        }



        public bool IsLoggedInMemberASuperAdministrator()
        {
            bool ok = false;
            var user = this.GetLoggedInUser();
            if (user != null && user.GetUserAccessType() >= CS.General_v3.Enums.CMS_ACCESS_TYPE.SuperAdministrator)
            {
                ok = true;
            }
            
            return ok;

        }
        public bool CheckAccessTypeOfLoggedUser(CS.General_v3.Enums.CMS_ACCESS_TYPE accessType)
        {
            bool ok = false;
            var user = this.GetLoggedInUser();
            if (user != null)
            {
                ok = user.CheckAccessTypeAtLeast(accessType);
                
            }
            return ok;
            
        }

        static CmsUserSessionLogin()
        {
            
        }


        public bool DoesLoggedInUserHasAtLeastAccessOfType(CS.General_v3.Enums.CMS_ACCESS_TYPE access)
        {
            var user = GetLoggedInUser();
            if (user != null)
            {
                return user.CheckAccessTypeAtLeast(access);
            }
            else
            {
                return false;
            }
        }

        public override IEnumerable<CmsUserBase> GetUsersList(string username)
        {
            return CmsUserBase.Factory.FindAll(CmsUserBase.Factory.GetQuery().Where(c => c.Username == username));
            
        }

        //protected override CmsUserBase createNewTemporaryUser()
        //{
        //    throw new InvalidOperationException("invalid operation on CmsUser");

        //    //var cmsItem = CmsUserBase.Factory.CreateNewItem();
        //    //cmsItem.MarkAsTemporary();
        //    //return cmsItem;
            
        //}

        protected override CmsUserBase getUserFromSocialLogin(bool autoLogin)
        {
            return null;
        }
    }
}
