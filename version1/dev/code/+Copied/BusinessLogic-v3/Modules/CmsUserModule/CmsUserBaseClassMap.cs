using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using CS.General_v3.Extensions;

namespace BusinessLogic_v3.Modules.CmsUserModule
{

//BaseClassMap-File
    
    public abstract class CmsUserBaseMap<TData> : BusinessLogic_v3.Modules._AutoGen.CmsUserBaseMap_AutoGen<TData>
    	where TData : CmsUserBase
    {

        public override Enums.NHIBERNATE_CACHE_TYPE CacheType
        {
            get
            {
                return Enums.NHIBERNATE_CACHE_TYPE.ReadWrite;
            }
            set
            {
                base.CacheType = value;
            }
        }
        protected override void CmsUserRolesMappingInfo(ICollectionMapInfo mapInfo)
        {
            mapInfo.Inverse = false;
            base.CmsUserRolesMappingInfo(mapInfo);
        }


        protected override void AccessTypeMappingInfo(IPropertyMapInfo mapInfo)
        {
            mapInfo.MapEnumAsString = false;
            base.AccessTypeMappingInfo(mapInfo);
        }
 
    }
   
}
