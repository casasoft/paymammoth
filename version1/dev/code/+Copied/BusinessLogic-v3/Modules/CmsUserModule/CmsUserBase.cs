using BusinessLogic_v3.Classes.Cms.CmsObjects.Access;
using BusinessLogic_v3.Classes.MediaItems;
using BusinessLogic_v3.Classes.MediaItems.Impl;

using BusinessLogic_v3.Modules.AffiliateModule;
using CS.General_v3.Classes.Login;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.TicketModule;
using BusinessLogic_v3.Classes.Searches.TicketSearcher;

namespace BusinessLogic_v3.Modules.CmsUserModule
{

//BaseClass-File

    public abstract class CmsUserBase : BusinessLogic_v3.Modules._AutoGen.CmsUserBaseAutoGen, ICmsUserBase, IUser, ITicketingSystemUser
    {
    
		#region BaseClass-AutoGenerated
		#endregion
    
        
  
    	protected override void initPropertiesForNewItem()
        {
        	//Fill any default values for NEW items.
            this.AccessType = CS.General_v3.Enums.CMS_ACCESS_TYPE.Normal;
            
            base.initPropertiesForNewItem();
        }



        #region CmsCustomLogo Media Item Snippet (v3)

        // CmsCustomLogo : This should be the name of the image, e.g MainPhoto, etc. 
        // CmsUserBase : This should be the name of the class that is implementing this snippet, e.g EventBase or Member

        public enum CmsCustomLogoSizingEnum
        {



            [ImageSpecificSizeDefaultValues(Width = 230, Height = 200, CropType = CS.General_v3.Enums.CROP_IDENTIFIER.None)]
            Normal
        }

        public class CmsCustomLogoMediaItem : BaseMediaItemImage<CmsCustomLogoSizingEnum>
        {
            /* If you need to update SetAsMainItem, Priority, etc, these are fields/methods that can be overriden in the base class */


            public CmsUserBase Item { get; private set; }
            public CmsCustomLogoMediaItem(CmsUserBase item)
            {

                this.Item = item;

                MediaItemImageInfo imageInfo = new MediaItemImageInfo();
                imageInfo.InitByDb(this.Item,
                    typeof(CmsCustomLogoSizingEnum),
                                        p => p.CmsCustomLogoFilename,  //this should be the filename field
                                        () => item.GetFullName(),  //this should be the title field.  Note that this is optional.  This is only used to generate the filename from, to include the title for Google Images purposes 
                                        "CmsUser - CmsCustomLogo"); //this should be the identifier for this image, and what will as the title in the Cms/Sizing Info, so make it nice and understandable :)

                imageInfo.DeleteDbItemOnRemove = false; //if you want that on delete of this image, the item is deleted, set this as true.

                this.MediaItemInfo = imageInfo;

            }



        }

        protected CmsCustomLogoMediaItem _m_CmsCustomLogo = null;
        public CmsCustomLogoMediaItem CmsCustomLogo { get { if (_m_CmsCustomLogo == null) _m_CmsCustomLogo = new CmsCustomLogoMediaItem(this); return _m_CmsCustomLogo; } }



        #endregion

      



        public CS.General_v3.Enums.CMS_ACCESS_TYPE? SessionCustomAccessType
        {
            get { return CS.General_v3.Util.PageUtil.GetSessionObjectEnum<CS.General_v3.Enums.CMS_ACCESS_TYPE>("CmsUserBase_SessionCustomAccessType_" + this.ID); }
            set { CS.General_v3.Util.PageUtil.SetSessionObject("CmsUserBase_SessionCustomAccessType_" + this.ID, value); }


        }



        public override CS.General_v3.Enums.CMS_ACCESS_TYPE AccessType
        {
            get
            {
                return base.AccessType;
            }
            set
            {
                base.AccessType = value;
            }
        }

        public CS.General_v3.Enums.CMS_ACCESS_TYPE GetUserAccessType()
        {
            CS.General_v3.Enums.CMS_ACCESS_TYPE access = this.AccessType;
            if (SessionCustomAccessType.HasValue && (((int)SessionCustomAccessType.Value) < ((int)access)))
                access = SessionCustomAccessType.Value;

            return access;
        }
        


        //public bool CanAccessAffiliates()
        //{
            
        //    return AffiliateBaseCmsFactory.Instance.UserSpecificGeneralInfoInContext.AccessTypeRequired_ToView.CheckIfUserHasEnoughAccess(this);
            
            
        //}

        public string GetFullName()
        {
            
            return CS.General_v3.Util.Text.AppendStrings(" ", this.Name, this.Surname);
        }

        string IUser.FullName
        {
            get {return  GetFullName(); }
        }


        private void checkDetailsBeforeSaving()
        {

            if (CS.General_v3.Util.PageUtil.CheckSessionExistsAtContext())
            {
                var loggedInCmsUser = CmsUserSessionLogin.Instance.GetLoggedInUser();
                
                //this should be updated to ONLY check if this is being updated from CMS.  
                if ((loggedInCmsUser != null && !loggedInCmsUser.CheckAccessTypeAtLeast(this.GetUserAccessType())))
                {
                    throw new PropertyUpdateException("Cannot set access type higher than access type of currently logged in user [" + loggedInCmsUser.AccessType + "]", CS.General_v3.Enums.STATUS_MSG_TYPE.Warning);
                }
            }



        }
        protected override void  Save_Before(OperationResult result, SaveBeforeAfterParams sParams)
        {
            checkDetailsBeforeSaving();
            
 	         base.Save_Before(result, sParams);
        }
        
        public virtual void OnLoggedIn()
        {

        }

        public virtual void OnLoggedInFromAdmin()
        {

        }

        public virtual void OnLoggedOut()
        {

        }



        public void SetPassword(string password)
        {
            IUserExtensions.SetPassword(this, password, 20000, CS.General_v3.Enums.PASSWORD_ENCRYPTION_TYPE.PBKDF2);
        }

        public bool CheckPassword(string password)
        {
            return IUserExtensions.CheckPassword(this, password, 20000, CS.General_v3.Enums.PASSWORD_ENCRYPTION_TYPE.PBKDF2);
        }
        public override string Password
        {
            get
            {
                return base.Password;
            }
            set
            {
                SetPassword(value);
                
            }
        }
        public AffiliateBase GetAffiliateForCmsUser()
        {
            var q = AffiliateBase.Factory.GetQuery();

            q.Cacheable();
            q.Where(item => item.LinkedCmsUser.ID == this.ID);
            return AffiliateBase.Factory.FindItem(q);

        }

        public bool CheckAccess(CmsAccessType cmsAccessType)
        {
            bool ok =false ;
            if (CheckAccessTypeAtLeast(cmsAccessType.GetAccessLevelRequiredToOverrideRoles()) && CheckAccessTypeAtLeast(cmsAccessType.GetMinimumAccessTypeRequired()))
            { //meets override level
                ok = true;
            }
            else
            {

                if (CheckAccessTypeAtLeast(cmsAccessType.GetMinimumAccessTypeRequired()) && this.CheckUserHasRoles(cmsAccessType.AccessRolesRequired))
                {
                    ok = true;
                }
            }
            if (ok)
            {
                var result = cmsAccessType.CheckCustomAccess();
                ok = (result.Status == CS.General_v3.Enums.STATUS_MSG_TYPE.Success);
            }
            return ok;
        }
        /// <summary>
        /// Checks whether the accessTypeToCompare is less than the access type for this item.
        /// </summary>
        /// <param name="accessTypeToCompare">If param left null, this returns false</param>
        /// <returns></returns>
        public bool CheckAccessIsLessThan(CS.General_v3.Enums.CMS_ACCESS_TYPE? accessTypeToCompare)
        {
            bool ok = false;
            if (accessTypeToCompare.HasValue)
            {
                int reqVal = (int)accessTypeToCompare;
                int thisVal = (int)this.GetUserAccessType();
                ok = (thisVal < reqVal);
            }
            return ok;

        }
        /// <summary>
        /// Checks whether the accessTypeToCompare is at most equal to the access type for this item.
        /// </summary>
        /// <param name="accessTypeToCompare">If param left null, this returns false</param>
        /// <returns></returns>
        public bool CheckAccessAtMostIs(CS.General_v3.Enums.CMS_ACCESS_TYPE? accessTypeToCompare)
        {
            bool ok = false;
            if (accessTypeToCompare.HasValue)
            {
                int reqVal = (int)accessTypeToCompare;
                int thisVal = (int)this.GetUserAccessType();
                ok = (thisVal <= reqVal);
            }
            return ok;

        }
        /// <summary>
        /// Checks whether the accessTypeToCompare is at least the same as the access type for this item.
        /// </summary>
        /// <param name="accessTypeToCompare"></param>
        /// <returns></returns>
        public bool CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE? accessTypeToCompare)
        {
            bool ok = true;
            if (accessTypeToCompare.HasValue)
            {
                int reqVal = (int)accessTypeToCompare;
                int thisVal = (int)this.GetUserAccessType();
                ok = (thisVal >= reqVal);
            }


            return ok;

        }
        public override string ToString()
        {
            return this.Username;
        }


        bool IUser.Accepted
        {
            get { return true; }
        }

        bool IUser.AccountTerminated
        {
            get { return false; }
        }




        
        public bool CheckSessionGUID(string guid)
        {
           return string.Compare(this.SessionGUID, guid, true) == 0;
            
        }

        public bool CheckUserHasRoles(IEnumerable<ICmsAccessRole> roleList)
        {
            bool ok = true;
            foreach (var role in roleList)
            {
                ok = false;
                foreach (var userRole in this.CmsUserRoles)
                {
                    if (userRole.CompareRole(role))
                    {
                        ok = true;
                        break;
                    }
                }
                if (!ok)
                    break;
            }
            return ok;
        }
        public bool CheckUserHasRole(ICmsAccessRole role)
        {
            List<ICmsAccessRole> list = new List<ICmsAccessRole>();
            list.Add(role);
            return CheckUserHasRoles(list);
            
        }

        public bool CheckUserHasRole(Enum role)
        {
            CmsAccessRole cmsRole = new CmsAccessRole(role);
            return CheckUserHasRole(cmsRole);
        }
        public bool CheckUserHasRole(string role)
        {
            CmsAccessRole cmsRole = new CmsAccessRole(role);
            return CheckUserHasRole(cmsRole);
        }


        internal CmsAccessType checkAccessTypeForCms(CmsAccessType accessType)
        {
            var loggedUser = CmsUserSessionLogin.Instance.GetLoggedInUser();
            if (loggedUser == null || ((int)loggedUser.GetUserAccessType() < (int)this.AccessType))
            {
                accessType.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne;
            }
            return accessType;
        }



        #region IUser Members


        public void GenerateSessionGUIDAndSave()
        {
            this.SessionGUID = CS.General_v3.Util.Random.GetGUID();
            this.Update(new SaveParams(savePermanently: false));
        }

        #endregion

        #region IUser Members

        void IUser.UpdatePasswordWithHashedValue(string hashedPassword, string salt, int totalIterations, CS.General_v3.Enums.PASSWORD_ENCRYPTION_TYPE encryptionType)
        {
            base.Password = hashedPassword;
            base.PasswordSalt = salt;
            base.PasswordIterations = totalIterations;
            base.PasswordEncryptionType = encryptionType;
        }

        void IUser.Save()
        {
            this.Save();
        }

        #endregion

        #region IUser Members


        

        #endregion

        #region IUser Members


        public CS.General_v3.Enums.LOGIN_STATUS OnPreLogin()
        {
            return CS.General_v3.Enums.LOGIN_STATUS.Ok;
        }

        #endregion
        
        
		



        #region IUser Members

        bool IUser.Activated
        {
            get
            {
                return !this.Disabled;
                
            }
            set
            {
                this.Disabled = !value;
                
            }
        }

        #endregion

        #region ICmsUserBase Members


        IMediaItemImage<CmsUserBase.CmsCustomLogoSizingEnum> ICmsUserBase.CmsCustomLogo
        {
            get { return this.CmsCustomLogo; }
        }

        #endregion




        public virtual bool IsBlocked()
        {
            return false;
        }

        public virtual  bool IsSelfExcluded()
        {
            return false;
        }

        public Enums.TICKET_USER_TYPE UserType
        {
            get { return Enums.TICKET_USER_TYPE.TicketingSystemStaff; }
        }

        public void SetAsTicketingSystemSupportUser()
        {
            if(!this.TicketingSystemSupportUser)
            {
                this.TicketingSystemSupportUser = true;
            }
        }

        public void UnsetAsTicketingSystemSupportUser()
        {
            if (this.TicketingSystemSupportUser)
            {
                UnassignAllOpenTicketsFromThisUser();
                this.TicketingSystemSupportUser = false;
            }
        }

        public void UnassignAllOpenTicketsFromThisUser()
        {
            IEnumerable<ITicketBase> tickets =
                ((IEnumerable<ITicketBase>) Factories.TicketFactory.GetAllItemsInRepository()).Where(
                    item =>
                    item.TicketState == Enums.TICKET_STATE.Open || item.TicketState == Enums.TICKET_STATE.ReOpened);
            foreach(ITicketBase ticket in tickets)
            {
                ticket.AssignTicketingSystemSupportUser(null);
                ticket.Save();
            }
        }

        public SearchPagedResults<TicketBase> GetAllAssignedTickets(int pageNo, int showAmt, Enums.TICKET_SORTBY sortBy)
        {
            return getTickets(pageNo, showAmt, sortBy);
        }

        public SearchPagedResults<TicketBase> GetAllOpenAssignedTickets(int pageNo, int showAmt, Enums.TICKET_SORTBY sortBy)
        {
            return getTickets(pageNo, showAmt, sortBy, Enums.TICKET_STATE.Open);
        }

        public SearchPagedResults<TicketBase> GetAllClosedAssignedTickets(int pageNo, int showAmt, Enums.TICKET_SORTBY sortBy)
        {
            return getTickets(pageNo, showAmt, sortBy, Enums.TICKET_STATE.Closed);            
        }

        public SearchPagedResults<TicketBase> GetAllReopenedAssignedTickets(int pageNo, int showAmt, Enums.TICKET_SORTBY sortBy)
        {
            return getTickets(pageNo, showAmt, sortBy, Enums.TICKET_STATE.ReOpened);
        }

        private SearchPagedResults<TicketBase> getTickets(int pageNo, int showAmt, Enums.TICKET_SORTBY sortBy, Enums.TICKET_STATE? state = null)
        {
            TicketSearcherParams sparams = new TicketSearcherParams();

            sparams.PageNo = pageNo;
            sparams.ShowAmount = showAmt;
            sparams.SortBy = sortBy;
            sparams.TicketingSystemSupportUser = this;

            if(state.HasValue)
            {
                sparams.State = state.Value;
            }

            TicketSearcher searcher = new TicketSearcher();
            searcher.SearchParams = sparams;
            return searcher.GetSearchResults();
        }

        

        #region IUser Members

        void IUser.UpdateUsernameWithHashedValue(string hashedUsername, string salt, int totalIterations, CS.General_v3.Enums.PASSWORD_ENCRYPTION_TYPE encryptionType)
        {
            base.Username = hashedUsername;
            
        }

        #endregion

        #region IUser Members

        string IUser.UsernameHashed
        {
            get { return this.Username; }
        }

        string IUser.PasswordHashed
        {
            get { return this.Password; }
        }

        public void SetUsername(string usernameUnhashed)
        {
            this.Username = usernameUnhashed;
            
        }

        #endregion

        #region IUser Members


        public void OnInvalidLoginAttempt(CS.General_v3.Enums.LOGIN_STATUS status, string username, string password)
        {
            //do nothing
        }

        #endregion

        #region IUser Members



        public CS.General_v3.Enums.MEMBER_BLOCKED_REASON BlockedReason
        {
            get { return CS.General_v3.Enums.MEMBER_BLOCKED_REASON.None; }
        }

        public DateTime? BlockedUntil
        {
            get { return null; }
        }

        #endregion


        public DateTime? SelfExcludedUntil
        {
            get;
            set;
        }
    }
}
