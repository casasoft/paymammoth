﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI;

using CS.General_v3.Controls.WebControls.Specialized.Hierarchy;
namespace BusinessLogic_v3.Controls.WebControls.Specialised.Navigation
{
    
    public class NavigationBreadcrumbs : MyDiv
    {
        public class FUNCTIONALITY
        {
            public enum CLICKABLE_TYPE
            {
                AllClickable,
                LastOneOnly,
                NoClickable
            }

            public FUNCTIONALITY()
            {
                MenuItems = new ListItemCollection();
                Seperator = " &raquo; ";
                ClickableType = CLICKABLE_TYPE.AllClickable;
            }
            public ListItemCollection MenuItems { get; set; }
            public string Seperator { get; set; }
            public CLICKABLE_TYPE ClickableType { get; set; }
            public void AddFromHierarchy(IHierarchy pg, bool includeRoot)
            {
                


                while (pg != null && (includeRoot || pg.ParentHierarchy != null))
                {

                    MenuItems.Insert(0, new ListItem(pg.Title, pg.Href));
                    pg = pg.ParentHierarchy;
                }

            }
            
        }

        public FUNCTIONALITY Functionality { get; set; }

        public NavigationBreadcrumbs()
        {
            this.Functionality = new FUNCTIONALITY();
            this.CssClass = "nav-breadcrumbs";
        }
        private void initControls()
        {
            for (int i = 0; i < Functionality.MenuItems.Count; i++)
            {
                if (i > 0 && !String.IsNullOrEmpty(Functionality.Seperator))
                {
                    Controls.Add(new Literal() { Text = Functionality.Seperator });
                }

                if (Functionality.ClickableType == FUNCTIONALITY.CLICKABLE_TYPE.AllClickable ||
                    (Functionality.ClickableType == FUNCTIONALITY.CLICKABLE_TYPE.LastOneOnly && i == Functionality.MenuItems.Count-1))
                {
                    HtmlAnchor aItem = new HtmlAnchor();
                    aItem.InnerHtml = Functionality.MenuItems[i].Text;
                    aItem.HRef = Functionality.MenuItems[i].Value;
                    aItem.Title = aItem.InnerHtml;
                    Controls.Add(aItem);
                }
                else
                {
                    Controls.Add(new Literal() { Text = "<span class='link'>" + Functionality.MenuItems[i].Text + "</span>" });
                }
                
                
            }
        }
        protected override void OnLoad(EventArgs e)
        {
            initControls();
            base.OnLoad(e);
        }
    }
}
