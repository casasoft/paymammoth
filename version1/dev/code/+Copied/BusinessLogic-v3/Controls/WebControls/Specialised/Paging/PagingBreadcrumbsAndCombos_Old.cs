﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Classes.SearchParams;
using CS.General_v3.Controls.WebControls.Specialized.Paging;
namespace BusinessLogic_v3.Controls.WebControls.Specialised.Paging
{
    public class PagingBreadcrumbsAndCombos_Old: MyDiv
    {
        public class FUNCTIONALITY
        {
            private PagingBreadcrumbsAndCombosOld _ui;
            public BreadcrumbsAndCombos BreadcrumbsAndCombos { get; private set; }
            public PagingBar_TypeTwo PagingBar { get; private set; }

            public FUNCTIONALITY(PagingBreadcrumbsAndCombosOld ui)
            {
                _ui = ui;
                BreadcrumbsAndCombos = new BreadcrumbsAndCombos();
                PagingBar = new PagingBar_TypeTwo();
            }
            private void initControls()
            {
                
                if ((BreadcrumbsAndCombos.Functionality.BreadCrumbs.Functionality.MenuItems != null &&
                    BreadcrumbsAndCombos.Functionality.BreadCrumbs.Functionality.MenuItems.Count > 0) ||
                    BreadcrumbsAndCombos.Functionality.CombosHasValues)
                {
                    _ui.Controls.Add(BreadcrumbsAndCombos);

                }
                _ui.Controls.Add(PagingBar);
            }
            public void Init()
            {
                initControls();
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }

        public PagingBreadcrumbsAndCombosOld()
        {
            Functionality = new FUNCTIONALITY(this);
            this.CssManager.AddClass("paging-breadcrumbs-combos");
        }
        public PagingBreadcrumbsAndCombosOld(SearchParams searchParams)
            : this()
        {
            Functionality.PagingBar.SearchParams = searchParams;
            Functionality.BreadcrumbsAndCombos.Functionality.SearchParams = searchParams;
        }
        protected override void OnLoad(EventArgs e)
        {
            Functionality.Init();
            
            base.OnLoad(e);
        }
    }
}
