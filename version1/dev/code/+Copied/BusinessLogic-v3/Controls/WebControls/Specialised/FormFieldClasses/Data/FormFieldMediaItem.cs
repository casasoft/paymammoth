﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.MediaItems;
using BusinessLogic_v3.Controls.WebControls.Specialised.MediaItems;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Controls.WebControls.Common.General;
using System.IO;
using CS.General_v3.Classes.MediaItems;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data;


namespace BusinessLogic_v3.Controls.WebControls.Specialised.FormFieldClasses.Data
{
    public class FormFieldMediaItem : FormFieldBaseTypeDataBase<string, Stream>
    {
        public List<string> FileExtensionsAllowed { get; private set; }


        public void SetFileExtensionsAllowedFromString(string str)
        {
            List<string> list = new List<string>();
            string[] tokens = str.Split(new string[] { ";", ",", "|" }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < tokens.Length; i++)
            {
                var t = tokens[i].Trim().ToLower();
                if (t.StartsWith("."))
                    t = t.Substring(1);
                list.Add(t);
            }
            this.FileExtensionsAllowed.AddRange(list.Distinct());
            

        }

        public IMediaItem MediaItem { get; set; }

        public FormFieldMediaItem(string id, IMediaItem mediaItem)
        {
            
            this.ID = id;

            if (mediaItem == null) throw new InvalidOperationException("mediaItem must be filled in");
            
            this.MediaItem = mediaItem;
            this.RowCssClass = "form-row-mediaitem";
            this.FileExtensionsAllowed = new List<string>();
            _validationParams.fileExtensionsAllowed = FileExtensionsAllowed;
            
        }

        public new MyFileUpload GetField()
        {
            return (MyFileUpload)base.GetField();
        }
        
        protected override IMyFormWebControl createFieldControl()
        {
            MediaItemControl ctrl = new MediaItemControl(this.ID, this.MediaItem);
            ctrl.Functionality.IsRequired = this._validationParams.isRequired;
            return ctrl;
            
        }
        public override Stream GetFormValue()
        {
            var txt = GetField();
            return txt.FileContent;

        }
        public Stream GetFileUploadStream()
        {
            return GetField().FileContent;
        }

        
    }
}
