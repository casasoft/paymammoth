﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web.UI;
using BusinessLogic_v3.Controls.WebControls.Specialised.FormFieldClasses.Data;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;

namespace BusinessLogic_v3.Controls.WebControls.Specialised.FormFieldClasses
{
    
    [ToolboxData("<{0}:FormFields runat=server></{0}:FormFields>")]
    public class FormFields : CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.FormFields
    {

        

        #region FormFields Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY : CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.FormFields.FUNCTIONALITY
        {
            protected new FormFields _control { get { return (FormFields)base._control; } }

            internal FUNCTIONALITY(FormFields control)
                : base(control)
            {

            }

            public FormFieldsRowItemField AddMediaItem(FormFieldMediaItem fieldData)
            {
                return AddGenericFormFieldItem(fieldData);
            }
        }

        protected override CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.FormFields.FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        #endregion
		
			

    }
}
