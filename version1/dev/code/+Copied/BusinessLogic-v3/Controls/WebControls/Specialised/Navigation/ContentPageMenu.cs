﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.ArticleModule;

using CS.General_v3.Controls.WebControls.Common;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI;



namespace BusinessLogic_v3.Controls.WebControls.Specialised.Navigation
{
    using CS.General_v3.Classes.Interfaces.Hierarchy;

    /// <summary>
    /// The ContentPageMenu is a generic menu structure which is usually
    /// used at the bottom of the website.  It consists of a title (h3)tag 
    /// and the child pages are displayed in UL's and LI's underneath.
    /// 
    /// Use CSS to style accordingly
    /// </summary>
    public class ContentPageMenu : MyDiv
    {
        public class FUNCTIONALITY
        {
            
            public FUNCTIONALITY()
            {
                HeadingType = 3;
                AddLinkAlsoToEmptyPages = true;
            }

            public IArticleBase ContentPage { get; set; }
            public int HeadingType { get; set; }
            public int MaxDepth { get; set; }
            /// <summary>
            /// Add a link also to pages which has ContainsContent() = false
            /// </summary>
            public bool AddLinkAlsoToEmptyPages { get; set; }
            
            
        }

        public FUNCTIONALITY Functionality { get; set; }

        public ContentPageMenu()
        {
            this.Functionality = new FUNCTIONALITY();
            this.CssClass = "content-page-menu";
        }

        private HtmlGenericControl getChildUl(IEnumerable<IArticleBase> ChildPages, int depth)
        {
            HtmlGenericControl ul = new HtmlGenericControl("ul");

            foreach (var cp in ChildPages)
            {
                HtmlGenericControl li = new HtmlGenericControl("li");
                if (Functionality.AddLinkAlsoToEmptyPages || cp.ContainsContent())
                {
                    HtmlAnchor aHref = new HtmlAnchor();
                    aHref.InnerHtml = aHref.Title = ((IHierarchy)cp).Title;
                    aHref.HRef = cp.GetUrl();
                    li.Controls.Add(aHref);
                }
                else
                {
                    li.Controls.Add(new Literal() { Text = ((IHierarchy)cp).Title });
                }
                if (cp.HasChildArticles() && (Functionality.MaxDepth == 0 || depth < Functionality.MaxDepth))
                {
                    li.Controls.Add(getChildUl(cp.GetChildArticles(), depth + 1));
                }
                ul.Controls.Add(li);
            }

            return ul;
        }

        private void initControls()
        {
            MyHeading heading = new MyHeading(Functionality.HeadingType, null);
            if (Functionality.AddLinkAlsoToEmptyPages || Functionality.ContentPage.ContainsContent())
            {
                HtmlAnchor aHeading = new HtmlAnchor();
                aHeading.InnerHtml = aHeading.Title = ((IHierarchy)Functionality.ContentPage).Title;
                aHeading.HRef = Functionality.ContentPage.GetUrl();
                heading.Controls.Add(aHeading);
            }
            else
            {
                //Not clickable
                heading.InnerHtml = ((IHierarchy)Functionality.ContentPage).Title;
            }
            Controls.Add(heading);
            if (Functionality.ContentPage.HasChildArticles())
            {
                Controls.Add(getChildUl(Functionality.ContentPage.GetChildArticles(), 1));
            }
        }
        protected override void OnLoad(EventArgs e)
        {
            initControls();
            base.OnLoad(e);
        }
    }
}
