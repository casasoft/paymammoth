﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;

namespace BusinessLogic_v3.Controls.UserControls
{
    using Modules.CmsUserModule;


    public abstract class BaseUserControl : CS.General_v3.Controls.UserControls.BaseUserControl
    {
        public new CacheParameters CacheParams { get { return (CacheParameters)base.CacheParams; } }

        public class CacheParameters : CS.General_v3.Controls.UserControls.BaseUserControl.CacheParameters
        {
            public bool IsMultilingual { get; set; }
            public CacheParameters(BaseUserControl userControl)
                : base(userControl)
            {

            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="factories">e.g. BusinessLogic_v3.Modules.Factories.SettingFactory;</param>
            public void SetCacheDependencyFromFactories(bool multilingual, params IBaseDbFactory[] factories)
            {
                if (!this.CacheControl.HasValue)
                {
                    this.CacheControl = true;
                }
                this.IsMultilingual = multilingual;
                this.CacheDependency = BusinessLogic_v3.Util.CachingUtil.GetOutputCacheDependencyForFactories(factories);

                string cacheKey = "";
                foreach (IBaseDbFactory factory in factories)
                {
                    if (!string.IsNullOrWhiteSpace(cacheKey)) cacheKey += "_";
                    cacheKey = factory.GetType().Name;
                }
                this.CacheKey = cacheKey;

            }
            public void SetCacheDependencyFromDBObjects(BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject item, bool multilingual)
            {
                SetCacheDependencyFromDBObjects(new List<BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject>() { item}, multilingual);

            }
            public void SetCacheDependencyFromDBObjects(IEnumerable<BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject> items, bool multilingual)
            {
                if (!this.CacheControl.HasValue)
                {
                    this.CacheControl = true;
                }
                this.IsMultilingual = multilingual;
                this.CacheDependency = BusinessLogic_v3.Util.CachingUtil.GetOutputCacheDependencyForDBObjects(items);

                string cacheKey = "";
                foreach (IBaseDbObject item in items)
                {
                    if (!string.IsNullOrWhiteSpace(cacheKey)) cacheKey += "_";
                    cacheKey = item.GetType().Name + "-" + item.ID;
                }
                this.CacheKey = _userControl.GetType().Name + "_" + cacheKey;
            }
        }
        public BaseUserControl()
        {
            BusinessLogic_v3.Util.CachingUtil.GetOutputCacheDependencyForFactories(BusinessLogic_v3.Modules.Factories.BannerFactory);
        }
        protected override CS.General_v3.Controls.UserControls.BaseUserControl.CacheParameters createCacheParameters()
        {
            return new CacheParameters(this);
        }
        protected override bool checkIfCacheable()
        {
            bool isCacheableFromDB = BusinessLogic_v3.Modules.SettingModule.SettingBaseFactory.Instance.GetSetting<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.Other_CacheControls);
            return isCacheableFromDB && base.checkIfCacheable();
        }
        protected override string getFinalCacheKey()
        {
            string key = base.getFinalCacheKey();
            if (!string.IsNullOrWhiteSpace(key))
            {
                if (this.CacheParams.IsMultilingual)
                {
                    var culture = BusinessLogic_v3.Classes.Culture.DefaultCultureManager.Instance.GetCulture();
                    string cultureCode = culture.GetLanguage2LetterCode();
                    key += "_"+cultureCode;
                }
                if (CmsUserSessionLogin.Instance.IsLoggedIn)
                {
                    key += "-cms";
                }
            }
            return key;
        }
    }


}
