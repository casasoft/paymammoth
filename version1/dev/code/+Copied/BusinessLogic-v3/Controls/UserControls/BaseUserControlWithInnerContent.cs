﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace BusinessLogic_v3.Controls.UserControls
{
    public abstract class BaseUserControlWithInnerContent : BaseUserControl
    {
        /// <summary>
        /// The Template property for the Content area.
        /// </summary>
        [PersistenceMode(PersistenceMode.InnerProperty), TemplateContainer(typeof(INamingContainer)), TemplateInstance(TemplateInstance.Single)]
        public ITemplate Content { get; set; }


        protected override void CreateChildControls()
        {
            if (Content != null)
                Content.InstantiateIn(this._container); //Replace this with container where you want to place such controls

            base.CreateChildControls();
        }

        protected abstract Control _container { get; }


    }
}
