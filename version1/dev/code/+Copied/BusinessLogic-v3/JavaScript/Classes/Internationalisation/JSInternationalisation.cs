﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;
using BusinessLogic_v3.Modules.ContentTextModule;
using BusinessLogic_v3.Modules;

namespace BusinessLogic_v3.JavaScript.Classes.Internationalisation
{
    using CS.General_v3.JavaScript.Data;

    public class JSInternationalisation
    {
        public class FieldValidatorTexts : JavaScriptObject
        {
            public const string TAG_FIELD = "[FIELD]";
            public const string TAG_FIELDS = "[FIELDS]";
            public const string TAG_LENGTH = "[LENGTH]";
            public const string TAG_VALUE = "[VALUES]";

            public string fieldRequired = TAG_FIELD + " is required";
            public string notValidEmail = TAG_FIELD + " must be a valid email address";
            public string notValidAlphanumerical = TAG_FIELD + " must contain only alpha numerical characters i.e. letters, digits and underscore (_)";
            public string notValidIP = TAG_FIELD + " must be a valid IP address, e.g. 192.168.210.123";
            public string notValidCreditCard = TAG_FIELD + " must be a valid credit card number";
            public string notValidWebsite = TAG_FIELD + " must be a valid website URL";
            public string cannotContainLessThanCharRange = TAG_FIELD + " cannot contain less than " + TAG_LENGTH + " character(s)";
            public string cannotContainMoreThanCharRange = TAG_FIELD + " cannot contain more than " + TAG_LENGTH + " character(s)";
            public string cannotContainMoreThanWordsRange = TAG_FIELD + " cannot contain more than " + TAG_LENGTH + " word(s)";
            public string notValidFileExtension = TAG_FIELD + " must be a file with one of the following extension(s): " + TAG_VALUE;
            public string notValidValueIn = TAG_FIELD + " must be one of: " + TAG_VALUE;
            public string caseSensitive = "(case-sensitive)";
            public string invalidValue = TAG_FIELD + " - invalid value";
            public string numberMustBeSmallerOrEqualToRange = TAG_FIELD + " must be smaller than or equal to " + TAG_VALUE;
            public string numberMustBeGreaterOrEqualToRange = TAG_FIELD + " must be greater than or equal to " + TAG_VALUE;
            public string numberMustBeWithinRange = TAG_FIELD + " must be within the range " + TAG_VALUE;
            public string numberMustBePositive = TAG_FIELD + " must be positive";
            public string numberMustBeNegative = TAG_FIELD + " must be negative";
            public string numberMustBeInteger = TAG_FIELD + " must be a whole number";
            public string numberNotValid = TAG_FIELD + " - Invalid number";

            public string dateMustBeBetweenRange = TAG_FIELD + " must be between or equal to " + TAG_VALUE;
            public string dateMustBeBeforeRange = TAG_FIELD + " must be before or equal to " + TAG_VALUE;
            public string dateMustBeAfterRange = TAG_FIELD + " must be after or equal to " + TAG_VALUE;
            public string dateNotValid = TAG_FIELD + " - Invalid date.  Date must be in the format: " + TAG_VALUE;

            public string atLeastOneIsRequired = TAG_FIELDS + " - At least one of the fields must be filled";
            public string sameValuesRequired = TAG_FIELDS + " - Values does not match";
            public string notSameValuesRequired = TAG_FIELDS + " - Values cannot be the same";
            public string waitingForAjaxToLoad = "Please wait to finish validating " + TAG_FIELD + " with server";

            public string passwordMustIncludeAlphabeticalCharacters = "Password must include alphabetical characters";
            public string passwordMustIncludeBothLowerAndUpperCase = "Password must include both lower and upper case letters";
            public string passwordMustIncludeAtLeastOneNumber = "Password must include at least one number";
            public string passwordMustIncludeASpecialCharacter = "Password must include at least one special characters (_ * ? ...)";
            private FieldValidatorTexts()
            {

            }

            public static void UpdateFromDefaultContentTexts()
            {
                FieldValidatorTexts texts = new FieldValidatorTexts();
                texts.atLeastOneIsRequired =
                    Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.JSInternationalisation_FieldValidatorTexts_atLeastOneIsRequired).GetContent();
                texts.cannotContainLessThanCharRange = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.JSInternationalisation_FieldValidatorTexts_cannotContainLessThanCharRange).GetContent();
                texts.cannotContainMoreThanCharRange = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.JSInternationalisation_FieldValidatorTexts_cannotContainMoreThanCharRange).GetContent();
                texts.cannotContainMoreThanWordsRange = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.JSInternationalisation_FieldValidatorTexts_cannotContainMoreThanWordsRange).GetContent();
                texts.caseSensitive = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.JSInternationalisation_FieldValidatorTexts_caseSensitive).GetContent();
                texts.dateMustBeAfterRange = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.JSInternationalisation_FieldValidatorTexts_dateMustBeAfterRange).GetContent();
                texts.dateMustBeBeforeRange = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.JSInternationalisation_FieldValidatorTexts_dateMustBeBeforeRange).GetContent();
                texts.dateMustBeBetweenRange = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.JSInternationalisation_FieldValidatorTexts_dateMustBeBetweenRange).GetContent();
                texts.dateNotValid = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.JSInternationalisation_FieldValidatorTexts_dateNotValid).GetContent();
                texts.fieldRequired = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.JSInternationalisation_FieldValidatorTexts_fieldRequired).GetContent();
                texts.invalidValue = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.JSInternationalisation_FieldValidatorTexts_invalidValue).GetContent();
                texts.notValidAlphanumerical = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.JSInternationalisation_FieldValidatorTexts_notValidAlphanumerical).GetContent();
                texts.notValidCreditCard = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.JSInternationalisation_FieldValidatorTexts_notValidCreditCard).GetContent();
                texts.notValidEmail = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.JSInternationalisation_FieldValidatorTexts_notValidEmail).GetContent();
                texts.notValidFileExtension = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.JSInternationalisation_FieldValidatorTexts_notValidFileExtension).GetContent();
                texts.notValidIP = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.JSInternationalisation_FieldValidatorTexts_notValidIP).GetContent();
                texts.notValidValueIn = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.JSInternationalisation_FieldValidatorTexts_notValidValueIn).GetContent();
                texts.notValidWebsite = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.JSInternationalisation_FieldValidatorTexts_notValidWebsite).GetContent();
                texts.numberMustBeGreaterOrEqualToRange = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.JSInternationalisation_FieldValidatorTexts_numberMustBeGreaterOrEqualToRange).GetContent();
                texts.numberMustBeInteger = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.JSInternationalisation_FieldValidatorTexts_numberMustBeInteger).GetContent();
                texts.numberMustBeNegative = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.JSInternationalisation_FieldValidatorTexts_numberMustBeNegative).GetContent();
                texts.numberMustBePositive = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.JSInternationalisation_FieldValidatorTexts_numberMustBePositive).GetContent();
                texts.numberMustBeSmallerOrEqualToRange = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.JSInternationalisation_FieldValidatorTexts_numberMustBeSmallerOrEqualToRange).GetContent();
                texts.numberMustBeWithinRange = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.JSInternationalisation_FieldValidatorTexts_numberMustBeWithinRange).GetContent();
                texts.numberNotValid = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.JSInternationalisation_FieldValidatorTexts_numberNotValid).GetContent();
                texts.sameValuesRequired = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.JSInternationalisation_FieldValidatorTexts_sameValuesRequired).GetContent();
                texts.notSameValuesRequired = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.JSInternationalisation_FieldValidatorTexts_notSameValuesRequired).GetContent();
                texts.waitingForAjaxToLoad = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.JSInternationalisation_FieldValidatorTexts_waitingForAjaxToLoad).GetContent();
                
                UpdateTexts(texts);
            }

            public static void UpdateTexts(FieldValidatorTexts texts)
            {
                string js = "js.com.cs." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION + ".Classes.Validation.FieldValidatorTexts.updateTexts(" + texts.GetJsObject().GetJS() + ");\r\n";
                var pg = CS.General_v3.Util.PageUtil.GetCurrentPage();
                CS.General_v3.Util.JSUtilOld.AddJSScriptToPage(js, true, key: "jsInternationalisation");
            }

        }
    }
}
