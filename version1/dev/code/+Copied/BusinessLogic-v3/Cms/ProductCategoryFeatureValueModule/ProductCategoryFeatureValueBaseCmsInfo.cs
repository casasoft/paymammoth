using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule;

namespace BusinessLogic_v3.Cms.ProductCategoryFeatureValueModule
{
    public abstract class ProductCategoryFeatureValueBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.ProductCategoryFeatureValueBaseCmsInfo_AutoGen
    {
    	public ProductCategoryFeatureValueBaseCmsInfo(ProductCategoryFeatureValueBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
	}
}
