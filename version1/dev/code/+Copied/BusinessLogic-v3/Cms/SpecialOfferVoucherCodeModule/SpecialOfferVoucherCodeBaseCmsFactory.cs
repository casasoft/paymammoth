using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Cms.SpecialOfferModule;
using BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule;

namespace BusinessLogic_v3.Cms.SpecialOfferVoucherCodeModule
{
    public abstract class SpecialOfferVoucherCodeBaseCmsFactory : BusinessLogic_v3.Modules._AutoGen.SpecialOfferVoucherCodeBaseCmsFactory_AutoGen
    {


        protected override void initCmsParameters()
        {
            
            base.initCmsParameters();
            //custom CMS defaults - important, add AFTER the base.initCmsParameters()
        }
        public string GetGenerateVoucherCodesUrl(SpecialOfferBaseCmsInfo specialOffer)
        {
            return this.GetCmsFolderUrl() + "generateCodes.aspx?" + BusinessLogic_v3.Classes.Cms.Util.QueryStringUtil.ParamSpecialOfferID + "=" + specialOffer.ID;
        }
        public string GetImportVoucherCodesUrl(SpecialOfferBaseCmsInfo specialOffer)
        {
            return this.GetCmsFolderUrl() + "import.aspx?" + BusinessLogic_v3.Classes.Cms.Util.QueryStringUtil.ParamSpecialOfferID + "=" + specialOffer.ID;
        }
    }
}
