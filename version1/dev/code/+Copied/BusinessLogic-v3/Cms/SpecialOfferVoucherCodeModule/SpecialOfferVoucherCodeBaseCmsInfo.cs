using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule;

namespace BusinessLogic_v3.Cms.SpecialOfferVoucherCodeModule
{
    public abstract class SpecialOfferVoucherCodeBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.SpecialOfferVoucherCodeBaseCmsInfo_AutoGen
    {
        public SpecialOfferVoucherCodeBaseCmsInfo(SpecialOfferVoucherCodeBase item)
            : base(item)
        {

        }

		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
            
            this.VoucherCode.IsRequired = true;
            this.VoucherCode.ShowInListing = true;
            this.QuantityLeft.SetShowInListing(true);
            this.CreatedOn.ShowInListing = true;
            this.ListingOrderPriorities.AddColumnsToStart(CreatedOn, VoucherCode, QuantityLeft);
            this.EditOrderPriorities.AddColumnsToStart(Priority, VoucherCode, QuantityLeft);
        }
	}
}
