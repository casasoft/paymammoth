﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.SpecialOfferModule;
using BusinessLogic_v3.Cms.SpecialOfferModule;


namespace BusinessLogic_v3.Cms.SpecialOfferVoucherCodeModule
{
    public abstract class SpecialOfferVoucherCodeGeneratePage : BusinessLogic_v3.Classes.Cms.Pages.BasePage
    {
        private long getSpecialOfferID()
        {
            return BusinessLogic_v3.Classes.Cms.Util.QueryStringUtil.GetSpecialOfferID();
        }
        private SpecialOfferBase specialOffer = null;
        private void loadSpecialOffer()
        {
            specialOffer = SpecialOfferBase.Factory.GetByPrimaryKey(getSpecialOfferID());

            if (specialOffer == null)
            {
                string url = SpecialOfferBaseCmsFactory.Instance.GetListingUrl().ToString();
                CS.General_v3.Util.PageUtil.RedirectPage(url);
            }


        }
        protected override void OnLoad(EventArgs e)
        {
            loadSpecialOffer();
            base.OnLoad(e);
            List<Classes.Cms.CmsObjects.PageTitle> pageTitles = new List<Classes.Cms.CmsObjects.PageTitle>();
            pageTitles.AddRange(SpecialOfferBaseCmsFactory.Instance.GetEditPageTitlesIncludingLinked(specialOffer.ID));
            pageTitles.Add(new Classes.Cms.CmsObjects.PageTitle("Generate Voucher Codes"));
            this.Master.Functionality.SetPageTitles(pageTitles);
        }
    }
}
