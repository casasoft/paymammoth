using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.ShippingMethodModule;

namespace BusinessLogic_v3.Cms.ShippingMethodModule
{
    public abstract class ShippingMethodBaseCmsFactory : BusinessLogic_v3.Modules._AutoGen.ShippingMethodBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            
            base.initCmsParameters();
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = Classes.Cms.EnumsCms.IMAGE_ICON.Shipping;
            //custom CMS defaults - important, add AFTER the base.initCmsParameters()
        }
    }
}
