using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.ShippingMethodModule;

namespace BusinessLogic_v3.Cms.ShippingMethodModule
{
    public abstract class ShippingMethodBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.ShippingMethodBaseCmsInfo_AutoGen
    {
    	public ShippingMethodBaseCmsInfo(ShippingMethodBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here
            this.Title.ShowInListing = true;
            this.Description.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.Html;
            this.ListingOrderPriorities.AddColumnsToStart(this.Title);
            this.EditOrderPriorities.AddColumnsToStart(this.Title,  this.Priority, this.Description,this.MinimumOrderAmount,this.ExtraChargeAmountIfMinimumNotReached);
            this.MinimumOrderAmount.HelpMessage = "This is the minimum that an order amount must be. If not, an extra charge will be incurred for shipping";
            this.ExtraChargeAmountIfMinimumNotReached.HelpMessage = "The extra charge to add to shipping to the order, if the total value of the order is below the minimum amount";
            this.ShippingRatesGroups.ShowLinkInCms = true;
            base.customInitFields();
        }
	}
}
