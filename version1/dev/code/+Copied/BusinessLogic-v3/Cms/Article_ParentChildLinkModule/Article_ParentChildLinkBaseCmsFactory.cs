using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.Article_ParentChildLinkModule;

using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;

namespace BusinessLogic_v3.Cms.Article_ParentChildLinkModule
{
    public abstract class Article_ParentChildLinkBaseCmsFactory : BusinessLogic_v3.Modules._AutoGen.Article_ParentChildLinkBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            
            base.initCmsParameters();
            //custom CMS defaults - important, add AFTER the base.initCmsParameters()
        }
    }
}
