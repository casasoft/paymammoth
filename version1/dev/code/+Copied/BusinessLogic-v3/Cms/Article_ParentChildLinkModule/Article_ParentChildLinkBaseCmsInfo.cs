using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.Article_ParentChildLinkModule;

namespace BusinessLogic_v3.Cms.Article_ParentChildLinkModule
{
    public abstract class Article_ParentChildLinkBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.Article_ParentChildLinkBaseCmsInfo_AutoGen
    {
    	public Article_ParentChildLinkBaseCmsInfo(Article_ParentChildLinkBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here
            
            base.customInitFields();
        }
	}
}
