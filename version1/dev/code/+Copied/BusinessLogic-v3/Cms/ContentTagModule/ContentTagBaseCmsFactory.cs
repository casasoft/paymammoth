using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.ContentTagModule;

namespace BusinessLogic_v3.Cms.ContentTagModule
{
    public abstract class ContentTagBaseCmsFactory : BusinessLogic_v3.Modules._AutoGen.ContentTagBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            
            base.initCmsParameters();
            //custom CMS defaults - important, add AFTER the base.initCmsParameters()
            this.UserSpecificGeneralInfoInContext.SetAllMinimumAccessRequiredAs(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            this.UserSpecificGeneralInfoInContext.ShowInCmsMainMenu = true;
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = Classes.Cms.EnumsCms.IMAGE_ICON.Tag;
        }
    }
}
