using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.ContentTagModule;

namespace BusinessLogic_v3.Cms.ContentTagModule
{
    public abstract class ContentTagBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.ContentTagBaseCmsInfo_AutoGen
    {
        public ContentTagBaseCmsInfo(ContentTagBase item)
            : base(item)
        {

        }

		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
            this.TagName.ShowInListing = true;
            this.TagType.ShowInListing = true;

            


        }
	}
}
