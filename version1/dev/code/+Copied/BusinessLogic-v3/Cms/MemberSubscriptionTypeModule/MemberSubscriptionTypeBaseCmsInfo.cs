using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;

using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;

using BusinessLogic_v3.Modules.MemberSubscriptionTypeModule;

namespace BusinessLogic_v3.Cms.MemberSubscriptionTypeModule
{
    public abstract class MemberSubscriptionTypeBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.MemberSubscriptionTypeBaseCmsInfo_AutoGen
    {
    	public MemberSubscriptionTypeBaseCmsInfo(MemberSubscriptionTypeBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
	}
}
