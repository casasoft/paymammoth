using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.ContactFormModule;

namespace BusinessLogic_v3.Cms.ContactFormModule
{
    public abstract class ContactFormBaseCmsFactory : BusinessLogic_v3.Modules._AutoGen.ContactFormBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = Classes.Cms.EnumsCms.IMAGE_ICON.Email;
            this.UserSpecificGeneralInfoInContext.ShowInCmsMainMenu = true;
            base.initCmsParameters();
        }

    }
}
