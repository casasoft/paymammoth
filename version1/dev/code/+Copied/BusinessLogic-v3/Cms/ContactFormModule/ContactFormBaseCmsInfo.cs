using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.ContactFormModule;

namespace BusinessLogic_v3.Cms.ContactFormModule
{
    public abstract class ContactFormBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.ContactFormBaseCmsInfo_AutoGen
    {
        public ContactFormBaseCmsInfo(ContactFormBase item)
            : base(item)
        {

        }

        protected override void customInitFields()
        {
            base.customInitFields();
            
            initListingItems();
            initListingItemsPriority();

            
        }

        private void initListingItems()
        {
            this.DateSent.ShowInListing = true;
            this.Email.ShowInListing = true;
            this.Name.ShowInListing = true;
            this.Surname.ShowInListing = true;
            this.Enquiry.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.MultiLine;
            this.Remark.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.MultiLine;

            this.SetDefaultSortField(this.DateSent, CS.General_v3.Enums.SORT_TYPE.Descending);
            this.EditOrderPriorities.AddColumnsToStart(this.ID, this.DateSent, this.Name, this.Surname, this.Email, this.Subject, this.Enquiry, this.Company, this.Mobile, this.Telephone, this.Fax, this.Address1,
                this.Address2, this.Address3, this.City, this.ZIPCode, this.State, this.CountryStr, this.Website,  this.Remark);
        }

        private void initListingItemsPriority()
        {
            this.ListingOrderPriorities.AddColumnsToStart(this.DateSent, this.Email, this.Name, this.Surname);
            //this.DateSent.ListingColumnPriority = 1000;
            //this.Email.ListingColumnPriority = 1000;
            //this.Name.ListingColumnPriority = 1000;
            //this.Surname.ListingColumnPriority = 1000;

        }

	}
}
