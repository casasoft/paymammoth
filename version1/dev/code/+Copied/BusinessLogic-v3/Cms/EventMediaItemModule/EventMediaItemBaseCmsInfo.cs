using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Frontend.EventMediaItemModule;
using CS.General_v3.Extensions;

using BusinessLogic_v3.Modules.EventMediaItemModule;

namespace BusinessLogic_v3.Cms.EventMediaItemModule
{
    public abstract class EventMediaItemBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.EventMediaItemBaseCmsInfo_AutoGen
    {
    	public EventMediaItemBaseCmsInfo(EventMediaItemBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
            
            ImageFilename.ShowInEdit = false;
            ImageFilename.ShowInListing = false;
            initMediaItem();
        }

        private void initMediaItem()
        {
            Image = this.AddPropertyForMediaItem<EventMediaItemBase>(item => item.Image, true, true);
        }
        public CmsPropertyMediaItem<EventMediaItemBase> Image { get; set; }

	}
}
