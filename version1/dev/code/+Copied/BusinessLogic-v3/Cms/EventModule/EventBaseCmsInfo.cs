using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Frontend.EventModule;
using CS.General_v3.Extensions;

using BusinessLogic_v3.Modules.EventModule;

namespace BusinessLogic_v3.Cms.EventModule
{
    public abstract class EventBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.EventBaseCmsInfo_AutoGen
    {
    	public EventBaseCmsInfo(EventBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
            initEditPriorities();
            initListingItems();
            initRequired();
            initMediaItem();
            this.EventMediaItems.ShowLinkInCms = true;
            this.EventSessions.ShowLinkInCms = true;
            this.ImageFilename.ShowInEdit = this.ImageFilename.ShowInListing = false;
            
        }

        private void initMediaItem()
        {
            Image = this.AddPropertyForMediaItem<EventBase>(item => item.Image, true, true);
        }
        public CmsPropertyMediaItem<EventBase> Image { get; set; }

        private void initEditPriorities()
        {
            this.EventCategory.ShowInEdit = true;
            this.EditOrderPriorities.AddColumnsToStart(Title, EventCategory, Description);
        }

        private void initListingItems()
        {
            this.Title.ShowInListing = true;
            this.EventCategory.ShowInListing = true;
            this.ListingOrderPriorities.AddColumnsToStart(Title, EventCategory, Image);
        }

        private void initRequired()
        {
            this.Title.IsRequired = true;
            this.Description.IsRequired = true;
            this.Description.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.Html;
            this.EventCategory.ShowInEdit = true;
            this.EventCategory.IsSearchable = true;
        }

	}
}
