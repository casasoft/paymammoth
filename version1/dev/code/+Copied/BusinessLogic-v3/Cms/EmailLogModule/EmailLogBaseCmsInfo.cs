using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;

using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;

using BusinessLogic_v3.Modules.EmailLogModule;

namespace BusinessLogic_v3.Cms.EmailLogModule
{
    public abstract class EmailLogBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.EmailLogBaseCmsInfo_AutoGen
    {
    	public EmailLogBaseCmsInfo(EmailLogBase item)
            : base(item)
        {
            this.AccessTypeRequired_ToView.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.SuperAdministrator);
            
        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            this.DateTime.ShowInListing = true;
            this.Subject.ShowInListing = true;
            this.FromEmail.ShowInListing = true;
            this.FromName.ShowInListing = true;
            this.ToEmail.ShowInListing = true;
            this.ToName.ShowInListing = true;
            this.Subject.ShowInListing = true;
            this.SetDefaultSortField(this.DateTime, CS.General_v3.Enums.SORT_TYPE.Descending);
            this.EditOrderPriorities.AddColumnsToStart(this.DateTime, this.FromEmail, this.FromName, this.ToEmail, this.ToName, this.ReplyToEmail,this.ReplyToName, this.Subject,
                this.PlainText,this.HtmlText,
                this.Success,
                this.ResultDetails);
            this.HtmlText.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.Html;
            this.ListingOrderPriorities.AddColumnsToStart(this.DateTime, this.FromEmail, this.FromName, this.ToEmail, this.ToName, this.Subject, this.Success);
            this.SetNonEditableFields(this.DateTime, this.Attachements, this.FromEmail, this.FromName, this.ToEmail,this.ToName,this.Subject,this.Success,this.User,
                this.UsesSecureConnection, this.Host, this.HtmlText, this.Pass, this.PlainText,
                this.Port, this.Priority, this.ReplyToEmail, this.ReplyToName, this.Resources, this.ResultDetails);

            base.customInitFields();
        }
	}
}
