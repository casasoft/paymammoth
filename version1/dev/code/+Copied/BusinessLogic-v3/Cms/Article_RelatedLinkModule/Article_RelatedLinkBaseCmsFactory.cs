using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.Article_RelatedLinkModule;

using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;

namespace BusinessLogic_v3.Cms.Article_RelatedLinkModule
{
    public abstract class Article_RelatedLinkBaseCmsFactory : BusinessLogic_v3.Modules._AutoGen.Article_RelatedLinkBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            
            base.initCmsParameters();
            //custom CMS defaults - important, add AFTER the base.initCmsParameters()
        }
        
        protected override void initCustomCmsOperations()
        {    //here you should initialise any custom cms operations.  The main reason being that if this is
        //called in the initCmsParameters, you may not yet know the base url
            
            base.initCustomCmsOperations();
        }
    }
}
