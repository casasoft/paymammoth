using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;

using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;

using BusinessLogic_v3.Modules.Article_RelatedLinkModule;

namespace BusinessLogic_v3.Cms.Article_RelatedLinkModule
{
    public abstract class Article_RelatedLinkBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.Article_RelatedLinkBaseCmsInfo_AutoGen
    {
    	public Article_RelatedLinkBaseCmsInfo(Article_RelatedLinkBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
	}
}
