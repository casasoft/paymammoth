using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using CS.General_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Cms.ImageSpecificSizeInfoModule
{

//BaseCmsInfo-Class

    public abstract class ImageSpecificSizeInfoBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.ImageSpecificSizeInfoBaseCmsInfo_AutoGen
    {
    	public ImageSpecificSizeInfoBaseCmsInfo(ImageSpecificSizeInfoBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            this.Height.EditableInListing = true;
		    this.ImageFormat.EditableInListing = true;
            this.Title.ShowInListing = true;
            this.Width.EditableInListing = true;
		    this.ListingOrderPriorities.AddColumnsToStart(this.Title, this.Height, this.Width, this.CropIdentifier, this.ImageFormat);
            this.CropIdentifier.EditableInListing = true;

            base.customInitFields();
        }
	}
}
