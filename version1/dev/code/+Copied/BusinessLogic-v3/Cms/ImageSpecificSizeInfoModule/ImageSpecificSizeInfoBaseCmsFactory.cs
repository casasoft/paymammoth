using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule;
using CS.General_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Cms.ImageSpecificSizeInfoModule
{

//BaseCmsFactory-Class

    public abstract class ImageSpecificSizeInfoBaseCmsFactory : BusinessLogic_v3.Modules._AutoGen.ImageSpecificSizeInfoBaseCmsFactory_AutoGen
    {
       

        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
        	//any user-specific information, add them to 'cmsInfo'.

            base.getUserSpecificGeneralInfo(cmsInfo);
        }

    }
}
