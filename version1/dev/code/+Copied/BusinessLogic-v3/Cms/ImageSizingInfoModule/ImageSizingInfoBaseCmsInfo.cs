using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ImageSizingInfoModule;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using CS.General_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Cms.ImageSizingInfoModule
{

//BaseCmsInfo-Class

    public abstract class ImageSizingInfoBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.ImageSizingInfoBaseCmsInfo_AutoGen
    {
    	public ImageSizingInfoBaseCmsInfo(ImageSizingInfoBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here
            this.Title.ShowInListing = true;
            this.MaximumWidth.EditableInListing = true;
            this.MaximumHeight.EditableInListing = true;
		    this.ListingOrderPriorities.AddColumnsToStart(this.Title, this.MaximumWidth, this.MaximumHeight);
            this.SizingInfos.ShowLinkInCms = true;


            base.customInitFields();
        }
	}
}
