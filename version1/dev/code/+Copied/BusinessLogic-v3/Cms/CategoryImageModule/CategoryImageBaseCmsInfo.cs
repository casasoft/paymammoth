using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Frontend.CategoryImageModule;
using BusinessLogic_v3.Modules.CategoryImageModule;

namespace BusinessLogic_v3.Cms.CategoryImageModule
{
    public abstract class CategoryImageBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.CategoryImageBaseCmsInfo_AutoGen
    {
    	public CategoryImageBaseCmsInfo(CategoryImageBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
            this.Category.ShowInEdit = true;
            this.Title.IsRequired = true;
            this.ImageFilename.ShowInEdit = this.ImageFilename.ShowInListing = false;
            this.EditOrderPriorities.AddColumnsToStart(this.Title, this.Category, this.Priority, this.Image);
            this.Title.ShowInListing = true;
            this.Category.ShowInListing = true;
            this.Priority.ShowInListing = true;
            this.ListingOrderPriorities.AddColumnsToStart(this.Priority, this.Category, this.Title, this.Image);
            initMediaItem();
        }

        private void initMediaItem()
        {
            this.Image = this.AddPropertyForMediaItem<CategoryImageBase>(item => item.Image, true, true);
        }
        public CmsPropertyMediaItem<CategoryImageBase> Image { get; set; }
	}
}
