using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;


using BusinessLogic_v3.Modules.CategoryModule;

using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;

namespace BusinessLogic_v3.Cms.CategoryModule
{
    public abstract class CategoryBaseCmsFactory : BusinessLogic_v3.Modules._AutoGen.CategoryBaseCmsFactory_AutoGen
    {
        private string _QUERYSTRING_PARENTID = "ParentID";
        public string QUERYSTRING_PARENTID { get { return _QUERYSTRING_PARENTID; } }
        public long? GetParentIDFromCurrentQueryString()
        {
            return CS.General_v3.Util.PageUtil.GetVariableFromQuerystring<long?>(QUERYSTRING_PARENTID);
        }
        public override CategoryBaseCmsInfo CreateNewItem(bool isTemporary)
        {
            var newItem = base.CreateNewItem(isTemporary);

            long? parentID = GetParentIDFromCurrentQueryString();
            if (parentID.GetValueOrDefault() > 0)
            {

                newItem.DbItem.AddParentCategory(CategoryBase.Factory.GetByPrimaryKey(parentID.Value));
            }
            return newItem;
        }
        
        protected override void initCmsParameters()
        {
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = Classes.Cms.EnumsCms.IMAGE_ICON.Category;
            this.UserSpecificGeneralInfoInContext.RenderType = Classes.Cms.EnumsCms.SECTION_RENDER_TYPE.Tree;
            if (CategoryBaseFactory.UsedInProject)
                this.UserSpecificGeneralInfoInContext.ShowInCmsMainMenu = true;
            base.initCmsParameters();
        }
        public override IEnumerable<ICmsHierarchyItem> GetRootItems(IEnumerable<CmsPropertySearchInfo> searchCriteria)
        {
            var root = CategoryBase.GetRootCategory();
            List<ICmsHierarchyItem> list = new List<ICmsHierarchyItem>();
            list.Add(GetCmsItemFromDBObject( root));
            return list;
        }
    }
}
