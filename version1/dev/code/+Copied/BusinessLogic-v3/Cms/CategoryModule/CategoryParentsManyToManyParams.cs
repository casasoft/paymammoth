﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties.CustomPropertyTypes;

using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using System.Web.UI.WebControls;
using BusinessLogic_v3.Modules.Category_ParentChildLinkModule;
using BusinessLogic_v3.Modules.CategoryModule;
using CS.General_v3.Classes.DbObjects.Collections;

namespace BusinessLogic_v3.Cms.CategoryModule
{
    public class CategoryParentsManyToManyParams : CmsPropertyManyToManyWithCustomClassParamsBase<CategoryBase, Category_ParentChildLinkBase, CategoryBase>
    {
        

        //protected override IEnumerable<System.Web.UI.WebControls.ListItem> getListItemsFromItem(ContentPageBase item)
        //{
        //    var list = ContentPageBase.FindAll().Where(cp => cp.ID != item.ID).ToList().ConvertAll<ListItem>(
        //            cp => new ListItem(cp.GetFullTitle(), cp.ID.ToString()));
        //    list.Sort((c1, c2) => (c1.Text.CompareTo(c2.Text)));
        //    return list;
        //}

        //protected override string getUniqueIDFromItem(ContentPage_ParentChildLinkBase item)
        //{
        //    return item.ParentID.GetValueOrDefault().ToString();
        //}

        //protected override void setValuesToItem(ContentPageBase item, IEnumerable<string> values)
        //{
        //    var parents = values.ToList().ConvertAll<ContentPageBase>(s => ContentPageBase.GetInstance(long.Parse(s)));
        //    item.SetParentsAs(parents);
            
        //}
        
    
        //protected override System.Linq.Expressions.Expression<Func<ContentPageBase,IEnumerable<ContentPage_ParentChildLinkBase>>>  getPropertySelector()
        //{
        //    return (cp => cp.ParentContentPagesLinks);
        //}
        protected override IEnumerable<ListItem> getListItemsFromItem(CategoryBase item)
        {
            List<ListItem> listItemColl = new List<ListItem>();
            
            var cpList = CategoryBase.Factory.FindAll();
            foreach (var cp in cpList)
            {
                if (cp != null && 
                    (item == null || 
                       (item != null && cp.ID != item.ID && !CategoryParentChildLinksCacher.Instance.CheckIfItemIsParentOf(item, cp))))
                { //do not allow parents to be selected, that are children of 'item'
                    ListItem li = new ListItem(cp.GetFullCategoryTitleFromRoot(" > ", true), cp.ID.ToString());
                    listItemColl.Add(li);
                }
            }
            listItemColl.Sort((c1, c2) => (c1.Text.CompareTo(c2.Text)));
            return listItemColl;
            
        }

        

        protected override System.Linq.Expressions.Expression<Func<CategoryBase, ICollectionManager<Category_ParentChildLinkBase>>> getPropertySelector()
        {
            return item => item.ParentCategoryLinks;
            
        }
        protected override System.Linq.Expressions.Expression<Func<Category_ParentChildLinkBase, CategoryBase>> getChildPropertyFromLinkedItemSelector()
        {
            return link => link.ParentCategory;
        }

        protected override System.Linq.Expressions.Expression<Func<Category_ParentChildLinkBase, CategoryBase>> getParentPropertyFromLinkedItemSelector()
        {
            return link => link.ChildCategory;
            
        }
    }
}
