using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Access;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties.CustomPropertyTypes;
using BusinessLogic_v3.Cms.ProductModule;
using BusinessLogic_v3.Modules.Category_ParentChildLinkModule;
using BusinessLogic_v3.Modules.CmsUserModule;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructure;
using CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructureBasic;
using BusinessLogic_v3.Modules._Common;
using CS.General_v3.Controls.WebControls.Specialized.Hierarchy;
using CS.General_v3.Classes.URL;

using BusinessLogic_v3.Modules.CategoryModule;
using CS.General_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Cms.CategoryModule
{
    public abstract class CategoryBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.CategoryBaseCmsInfo_AutoGen , ICmsHierarchyItem
    {
        public CategoryBaseCmsInfo(CategoryBase item)
            : base(item)
        {

        }



        private void addParentLinksProperty()
        {

            var p = new CmsPropertyManyToManyWithCustomClass<CategoryBase, Category_ParentChildLinkBase, CategoryBase>(this,
                new CategoryParentsManyToManyParams(),
                 Classes.Cms.EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);

            this.Parents = this.AddProperty(p);
        }

        public CmsPropertyManyToManyWithCustomClass<CategoryBase, Category_ParentChildLinkBase, CategoryBase> Parents { get; private set; }


        protected override void customInitFields()
        {
            this.CategoryFeatures.ShowLinkInCms = true;
            this.CategoryImages.ShowLinkInCms = true;
            this.CategorySpecifications.ShowLinkInCms = true;
            this.ProductLinks.ShowLinkInCms = true;
            this.ComingSoon.ShowInEdit = false;
            this.Identifier.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            this.ImportReference.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            this.Description.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.Html;
            this.DontShowInNavigation.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            this.MetaDescription.HelpMessage = "A meta description is used by search engines to describe in brief what this category should contain";
            this.MetaKeywords.HelpMessage = "A list of keywords which are used by some search engines.";
            this.PriorityAll.ShowInEdit = false;
            this.TitleSEO.HelpMessage = "This title can contain an alternate title, which will be used for Search Engine Optimisation (SEO) purposes";
            this.TitleSEO.Label = "Title for SEO";
            //this.Brand.LinkedWithInCms = true;
            addParentLinksProperty();
            this.Parents.Label = "Parent Categories";
            this.CanDelete.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            this.TitlePlural.IsRequired = true;
            this.TitleSingular.IsRequired = false;
            this.TitleSingular.ShowInListing = true;
            this.TitlePlural.ShowInListing = true;
            this.Description.ShowInListing = true;
            this.ListingOrderPriorities.AddColumnsToStart(this.TitleSingular, this.TitlePlural, this.Description);
            //this.ChildrenCategories.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
       //     this.Visible.ShowInListing = true;
            this.DontShowInNavigation.ShowInEdit = true;
            this.DontShowInNavigation.AccessTypeRequired_Edit.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft, CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft, null);
            base.customInitFields();
            
            this.EditOrderPriorities.AddColumnsToStart(TitlePlural, TitleSingular, MetaKeywords, MetaDescription, ComingSoon, this.Parents);
            addCustomLinkToViewFilteredProductsByCategory();
            addCustomLinkToMoveAllProducts();
        }
        
        public new CategoryBaseCmsFactory factory
        {
            get
            {
                return (CategoryBaseCmsFactory)base.factory;
            }
        }

        public override string TitleForCms
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                CategoryBase curr = this.DbItem;
                while (curr != null)
                {
                    if (sb.Length > 0)
                        sb.Insert(0, " > ");
                    sb.Insert(0, curr.TitlePlural);
                    curr = curr.GetParent();
                }
                return sb.ToString();
            }
        }
        private List<CategoryBaseCmsInfo> _children = null;
        protected List<CategoryBaseCmsInfo> getChildren()
        {
            if (_children == null)
            {
                List<CategoryBaseCmsInfo> list = this.DbItem.GetChildCategories().ToList()
                    .ConvertAll(item => factory.GetCmsItemFromDBObject(item));
               
                _children = list;
            }
            return _children;
        }
      
        public class CMS_HIERARCHY : ICmsHierarchyInfo
        {
            private CmsItemHierarchyCommonImpl _commonImpl_CmsItemHierarchy = null;
            protected CategoryBaseCmsInfo _cat = null;
            public CMS_HIERARCHY(CategoryBaseCmsInfo cat)
            {
                _cat = cat;
                _commonImpl_CmsItemHierarchy = new CmsItemHierarchyCommonImpl(this, typeof(CategoryBase));
            }
            public string AddNewItemURL
            {
                get
                {
                    var url = _cat.factory.GetAddUrl();
                    url[_cat.factory.QUERYSTRING_PARENTID] = this.ID;
                    return url.ToString();
                }
            }

            public ICmsCollectionInfo SubitemLinkedCollection
            {
                get { return null; }
            }

            public string Href
            {
                get { return _cat.factory.GetEditUrlForItemWithID(_cat.DbItem.ID).ToString(); }
            }

            public CS.General_v3.Enums.HREF_TARGET HrefTarget
            {
                get { return CS.General_v3.Enums.HREF_TARGET.Self; }
            }

            public IEnumerable<IHierarchy> GetChildren()
            {
                return _cat.getChildren().ConvertAll<IHierarchy>(item => item.GetAsCmsHierarchy());

            }

            public IHierarchy ParentHierarchy
            {
                get { return _cat.factory.GetCmsItemFromDBObject(_cat.DbItem.GetParent()).GetAsCmsHierarchy(); }
            }

            public bool Selected
            {
                get { return false; }
            }

            public string GetFullHierarchyName(string delimiter, bool reverseOrder, bool includeRoot)
            {
                return _commonImpl_CmsItemHierarchy.GetFullHierarchyName(delimiter, reverseOrder, includeRoot);

            }

            public bool Visible
            {
                get { return _cat.DbItem.Published; }
            }

            public OperationResult DeleteFromCms()
            {
                return _cat.DeleteFromCms();
            }

            public IEnumerable<CmsFieldBase> GetCmsProperties()
            {
                return _cat.GetListOfCMSProperties();
            }

            public OperationResult SaveFromCms(SaveParams sParams = null)
            {
                return _cat.SaveFromCms(sParams);

            }

            public bool CheckIfStillTemporary()
            {
                return _cat.CheckIfStillTemporary();

            }

            public bool HasTemporaryFields()
            {
                return _cat.HasTemporaryFields();

            }

            public ICmsHierarchyInfo GetAsCmsHierarchy()
            {
                return this;

            }

            public long ID
            {
                get { return _cat.DbItem.ID; }
            }

            public string Title
            {
                get { return _cat.DbItem.TitlePlural; }
            }

            public string EditURL
            {
                get { return _commonImpl_CmsItemHierarchy.EditURL; }
            }

            public bool AllowUpdate
            {
                get { return true; }
            }

            public bool AllowDelete
            {
                get 
                {
                    bool ok = true;
                    var loggedUser = CmsUserSessionLogin.Instance.GetLoggedInUser();
                    if (loggedUser != null && loggedUser.CheckAccessIsLessThan(  CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft) && _cat.DbItem.ParentCategoryLinks.Count== 0)
                        ok = false;
                    return ok; 
                }
            }

            public bool CanRemove(out string errorMsg)
            {
                errorMsg = "";
                return true;

            }




            public string Message_DeleteOK
            {
                get { return _commonImpl_CmsItemHierarchy.Message_DeleteOK; }
            }

            public string Message_ConfirmDelete
            {
                get { return _commonImpl_CmsItemHierarchy.Message_ConfirmDelete; }
            }

            public bool AddedExtraButtons
            {
                get
                {
                    return _commonImpl_CmsItemHierarchy.AddedExtraButtons;

                }
                set
                {
                    _commonImpl_CmsItemHierarchy.AddedExtraButtons = value;

                }
            }


            public int Priority
            {
                get
                {
                    return _cat.DbItem.Priority;
                }
                set
                {
                    _cat.DbItem.Priority = value;
                }
            }

            public string ImageURL
            {
                get { return _commonImpl_CmsItemHierarchy.ImageUrl; }
            }

            public string LinkURL
            {
                get { return _commonImpl_CmsItemHierarchy.LinkUrl; }
            }

            public IEnumerable<ITreeItemBasic> GetChildTreeItems()
            {
                return _cat.getChildren().ConvertAll<ITreeItemBasic>(item => item.GetAsCmsHierarchy());

            }

            public List<CS.General_v3.Controls.WebControls.Classes.ExtraButton> ExtraButtons
            {
                get { return _commonImpl_CmsItemHierarchy.ExtraButtons; }
            }

            public void AddExtraButtons()
            {
                _commonImpl_CmsItemHierarchy.AddExtraButtons();
            }


            public bool AllowAddSubItems
            {
                get { return true; }
            }


            OperationResult ITreeItem.Remove()
            {
                return this.DeleteFromCms();
            }

            OperationResult ITreeItem.Save()
            {
                return this.SaveFromCms();

            }


            public ICmsItemFactory GetCmsFactoryForItem()
            {
                throw new NotImplementedException();
            }

            public CmsAccessType AccessTypeRequired_ToDelete
            {
                get { return _cat.AccessTypeRequired_ToDelete; }
            }

            public CmsAccessType AccessTypeRequired_ToView
            {
                get { return _cat.AccessTypeRequired_ToView; }
            }

            public CmsAccessType AccessTypeRequired_ToEdit
            {
                get { return _cat.AccessTypeRequired_ToEdit; }
            }

            #region ICmsItemInstance Members


            public IEnumerable<CmsItemSpecificOperation> GetCustomCmsOperations()
            {
                return null;
            }

            #endregion

            #region ICmsItemInstance Members


            string ICmsItemInstance.TitleForCms
            {
                get { return Title; }
            }

            #endregion

            #region ICmsHierarchyInfo Members


            string ICmsHierarchyInfo.GetTitleInHierarchy()
            {
                return this.Title;

            }

            #endregion

            #region ICmsItemInstance Members


            public void ErrorOccurredWhileSavingInCms()
            {
                _cat.ErrorOccurredWhileSavingInCms();
            }

            #endregion

            #region ICmsItemInstance Members


            public Classes.DB.IBaseDbObject DbItem
            {
                get { return _cat.DbItem; }
            }

            #endregion

            #region ICmsItemInstance Members


            public ColumnPrioritizer GetListingOrderPrioritizer()
            {
                return _cat.ListingOrderPriorities;
                
            }

            public ColumnPrioritizer GetEditOrderPrioritizer()
            {
                return _cat.EditOrderPriorities;
                
            }

            #endregion


            #region IHierarchy Members


            public IEnumerable<IHierarchy> GetParents()
            {
                return CS.General_v3.Util.ListUtil.GetListFromSingleItem(this.ParentHierarchy);
                
            }

            #endregion

            #region ICmsItemInstance Members


            public bool CheckIfUserCanEdit(ICmsUserBase user)
            {
                return this._cat.CheckIfUserCanEdit(user);
                
            }

            public bool CheckIfUserCanDelete(ICmsUserBase user)
            {
                return this._cat.CheckIfUserCanDelete(user);
                
            }

            public bool CheckIfUserCanView(ICmsUserBase user)
            {
                return this._cat.CheckIfUserCanView(user);
                
            }

            #endregion

            #region ICmsItemInstance Members


            public CmsAccessType GetAccessRequiredToDelete()
            {
                return this._cat.GetAccessRequiredToDelete();
            }

            public CmsAccessType GetAccessRequiredToEdit()
            {

                return this._cat.GetAccessRequiredToEdit();
            }

            public CmsAccessType GetAccessRequiredToView()
            {
                return this._cat.GetAccessRequiredToView();
                
            }

            #endregion

            #region ICmsItemInstance Members


            public void CheckCustomAccess(OperationResult result)
            {
                this._cat.CheckCustomAccess(result);
            }

            #endregion
        }
        private CMS_HIERARCHY _cmsHierarchy = null;
        public CMS_HIERARCHY GetAsCmsHierarchy()
        {
            if (_cmsHierarchy == null)
                _cmsHierarchy = new CMS_HIERARCHY(this);
            return _cmsHierarchy;
        }




        #region ICmsHierarchyItem Members

        ICmsHierarchyInfo ICmsHierarchyItem.GetCmsHierarchyInfo()
        {
            return this.GetAsCmsHierarchy();
            
        }

        #endregion


        private void addCustomLinkToViewFilteredProductsByCategory()
        {
            if (DbItem != null)
            {
                var url = ProductBaseCmsFactory.Instance.GetListingUrl(removeAnyQuerystringParams: true);
                url[ProductBaseCmsFactory.QUERYSTRING_FILTERCATEGORYID] = this.DbItem.ID;

                this.CustomCmsOperations.Add(new CmsItemSpecificOperation(
                                                 this, "View " + ProductBaseCmsFactory.Instance.TitlePlural,
                                                 Classes.Cms.EnumsCms.IMAGE_ICON.Product, url.ToString()));
            }


        }
        private void addCustomLinkToMoveAllProducts()
        {
            if (DbItem != null)
            {
                var loggedUser = getLoggedCmsUser();
                if (loggedUser != null && loggedUser.CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator))
                {

                    string sUrl = CmsRoutesMapper.Instance.GetCategory_MoveProductsPage(this.DbItem.ID);

                    this.CustomCmsOperations.Add(new CmsItemSpecificOperation(
                                                     this, "Move " + ProductBaseCmsFactory.Instance.TitlePlural + " to another category",
                                                     Classes.Cms.EnumsCms.IMAGE_ICON.ReOrder, sUrl));
                }
            }


        }
    }
}
