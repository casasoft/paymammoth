using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.CmsUserRoleModule;

namespace BusinessLogic_v3.Cms.CmsUserRoleModule
{
    public abstract class CmsUserRoleBaseCmsFactory : BusinessLogic_v3.Modules._AutoGen.CmsUserRoleBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            base.initCmsParameters();
            this.UserSpecificGeneralInfoInContext.SetAllMinimumAccessRequiredAs(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            this.UserSpecificGeneralInfoInContext.ShowInCmsMainMenu = false;
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = Classes.Cms.EnumsCms.IMAGE_ICON.Padlock;
        }
    }
}
