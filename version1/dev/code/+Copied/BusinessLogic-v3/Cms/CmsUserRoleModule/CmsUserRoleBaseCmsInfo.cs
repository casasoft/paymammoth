using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.CmsUserRoleModule;

namespace BusinessLogic_v3.Cms.CmsUserRoleModule
{
    public abstract class CmsUserRoleBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.CmsUserRoleBaseCmsInfo_AutoGen
    {
        public CmsUserRoleBaseCmsInfo(CmsUserRoleBase item)
            : base(item)
        {

        }

        protected override void customInitFields()
        {
            base.customInitFields();
            this.Title.ShowInListing = true;
            this.Title.IsRequired = true;
            this.Identifier.IsRequired = true;
        }
	}
}
