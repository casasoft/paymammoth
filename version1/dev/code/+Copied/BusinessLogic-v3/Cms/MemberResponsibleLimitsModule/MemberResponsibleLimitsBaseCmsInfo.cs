using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.MemberResponsibleLimitsModule;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using CS.General_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Cms.MemberResponsibleLimitsModule
{

//BaseCmsInfo-Class

    public abstract class MemberResponsibleLimitsBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.MemberResponsibleLimitsBaseCmsInfo_AutoGen
    {
    	public MemberResponsibleLimitsBaseCmsInfo(MemberResponsibleLimitsBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
	}
}
