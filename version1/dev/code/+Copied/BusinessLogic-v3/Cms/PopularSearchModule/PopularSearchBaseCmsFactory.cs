using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.PopularSearchModule;

namespace BusinessLogic_v3.Cms.PopularSearchModule
{
    public abstract class PopularSearchBaseCmsFactory : BusinessLogic_v3.Modules._AutoGen.PopularSearchBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            
            base.initCmsParameters();
            //custom CMS defaults - important, add AFTER the base.initCmsParameters()
            this.UserSpecificGeneralInfoInContext.TitlePlural = "Popular Searches";
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = BusinessLogic_v3.Classes.Cms.EnumsCms.IMAGE_ICON.Photos;
        }
    }
}
