using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.AffiliateModule;

using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Modules.CmsUserModule;
using CS.General_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Cms.AffiliateModule
{
    public abstract class AffiliateBaseCmsFactory : BusinessLogic_v3.Modules._AutoGen.AffiliateBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
           
            base.initCmsParameters();

            this.UserSpecificGeneralInfoInContext.AccessTypeRequired_ToAdd.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator, CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator);
            this.UserSpecificGeneralInfoInContext.SetAllMinimumAccessRequiredAs(CS.General_v3.Enums.CMS_ACCESS_TYPE.RestrictedUser);
            this.UserSpecificGeneralInfoInContext.SetAllAccessLevelRequiredToOverrideRolesAs(CS.General_v3.Enums.CMS_ACCESS_TYPE.Normal);
            this.UserSpecificGeneralInfoInContext.AddAccessRoleRequiredToAll(Enums.CmsUserRoleEnum.Affiliate);
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = Classes.Cms.EnumsCms.IMAGE_ICON.Link;
            //custom CMS defaults - important, add AFTER the base.initCmsParameters()
        }

        protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
            var q = AffiliateBase.Factory.GetQuery(new GetQueryParams(loadDelItems: showDeleted));
            var loggedInUser = (CmsUserBase)getCurrentLoggedUser();

            if (loggedInUser.GetUserAccessType() == CS.General_v3.Enums.CMS_ACCESS_TYPE.RestrictedUser)
            {
                q.Where(item => item.LinkedCmsUser == loggedInUser);
            }
            return q;
        }
        protected override void initCustomCmsOperations()
        {    //here you should initialise any custom cms operations.  The main reason being that if this is
        //called in the initCmsParameters, you may not yet know the base url
            
            base.initCustomCmsOperations();
        }
    }
}
