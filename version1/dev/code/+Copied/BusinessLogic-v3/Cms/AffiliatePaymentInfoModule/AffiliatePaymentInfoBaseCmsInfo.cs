using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;

using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;

using BusinessLogic_v3.Modules.AffiliatePaymentInfoModule;

namespace BusinessLogic_v3.Cms.AffiliatePaymentInfoModule
{
    public abstract class AffiliatePaymentInfoBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.AffiliatePaymentInfoBaseCmsInfo_AutoGen
    {
    	public AffiliatePaymentInfoBaseCmsInfo(AffiliatePaymentInfoBase item)
            : base(item)
        {

        }



		protected override void customInitFields()
        {
            //custom init field logic here

            this.DatePaymentIssued.ShowInListing = true;
            this.Description.ShowInListing = true;
            this.PaymentMethod.ShowInListing = true;
            this.Total.ShowInListing = true;
            this.Currency.ShowInListing = true;
            this.Currency.ShowInEdit = true;
            

            this.AccessTypeRequired_ToEdit.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator, CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator, null);
            this.EditOrderPriorities.AddColumnsToStart(this.DatePaymentIssued, this.Description, this.PaymentMethod,this.Currency, this.Total, this.AdditionalComments);
            this.AdditionalComments.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.MultiLine;

            this.ListingOrderPriorities.AddColumnsToStart(this.DatePaymentIssued, this.Description, this.PaymentMethod, this.Currency, this.Total);

            base.customInitFields();

            base.customInitFields();
        }
	}
}
