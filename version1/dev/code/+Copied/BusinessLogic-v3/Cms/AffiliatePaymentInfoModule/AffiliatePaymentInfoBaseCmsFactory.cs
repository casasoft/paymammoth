using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.AffiliatePaymentInfoModule;

using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Modules.CmsUserModule;
using CS.General_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Cms.AffiliatePaymentInfoModule
{
    public abstract class AffiliatePaymentInfoBaseCmsFactory : BusinessLogic_v3.Modules._AutoGen.AffiliatePaymentInfoBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            
            base.initCmsParameters();
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = Classes.Cms.EnumsCms.IMAGE_ICON.BagOfMoney;
            var user = getLoggedCmsUser();
            this.UserSpecificGeneralInfoInContext.AccessTypeRequired_ToAdd.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator, CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator, null);
            this.UserSpecificGeneralInfoInContext.AccessTypeRequired_ToView.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.RestrictedUser, CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator, null);
            if (user != null && user.CheckAccessIsLessThan(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator))
            {
                this.UserSpecificGeneralInfoInContext.ShowInCmsMainMenu = true;
            }

            //custom CMS defaults - important, add AFTER the base.initCmsParameters()
        }


        protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
            var q = AffiliatePaymentInfoBase.Factory.GetQuery(new GetQueryParams(loadDelItems: showDeleted));
            
            var loggedInUser = (CmsUserBase)getCurrentLoggedUser();

            if (loggedInUser.GetUserAccessType() == CS.General_v3.Enums.CMS_ACCESS_TYPE.RestrictedUser)
            {
                q.Left.JoinQueryOver(item => item.Affiliate).Where(item => item.LinkedCmsUser.ID == loggedInUser.ID);
                //CS.General_v3.Util.nHibernateUtil.GetAssociationLinkCriteriaByPath<AffiliatePaymentInfoBase>(q.RootCriteria, item => item.Affiliate);
                //q.Where(item => item.Affiliate.LinkedCmsUser ==  loggedInUser);
            }
            return q;
        }

        protected override void initCustomCmsOperations()
        {    //here you should initialise any custom cms operations.  The main reason being that if this is
        //called in the initCmsParameters, you may not yet know the base url
            
            base.initCustomCmsOperations();
        }


        
    }
}
