﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties.CustomPropertyTypes;

using System.Web.UI.WebControls;
using BusinessLogic_v3.Modules.CategoryModule;
using BusinessLogic_v3.Modules.ProductCategoryModule;
using BusinessLogic_v3.Modules.ProductModule;
using CS.General_v3.Util;
using Iesi.Collections;
using Iesi.Collections.Generic;
using CS.General_v3.Classes.DbObjects.Collections;

namespace BusinessLogic_v3.Cms.ProductModule
{
    public class ProductCategoriesManyToManyParams : CmsPropertyManyToManyWithCustomClassParamsBase<ProductBase, ProductCategoryBase, CategoryBase>
    {
        /*

         this.CategoryFeatures = new CmsPropertyMultiChoiceGeneric<ProductBase, ProductCategoryFeatureValueBase>(
                this.factory,
                (item => item.ProductCategoryFeatureValues),
                (itemGroup => itemGroup.GetAllCategoryFeaturesAvailableForProductAsListItems(true)),
                (featureValue => featureValue.CategoryFeature.ID.ToString()),
                ((item, values) => item.SetCategoryFeaturesForProductFromStrings(values)), 
                EnumsCms.MULTICHOICE_DISPLAY_TYPE.CheckboxList);
        */


        protected override IEnumerable<System.Web.UI.WebControls.ListItem> getListItemsFromItem(ProductBase item)
        {
            var list = CategoryBase.Factory.FindAll().Where(cp => (item == null || (item != null && cp.ID != item.ID))).ToList().ConvertAll<ListItem>(
                   cp => new ListItem(cp.GetFullCategoryTitleFromRoot(" > ", true), cp.ID.ToString()));
            list.Sort((c1, c2) => (c1.Text.CompareTo(c2.Text)));
            return list;
            
        }




        

        protected override System.Linq.Expressions.Expression<Func<ProductBase, ICollectionManager<ProductCategoryBase>>> getPropertySelector()
        {
            return item => item.CategoryLinks;
            
        }

        protected override System.Linq.Expressions.Expression<Func<ProductCategoryBase, CategoryBase>> getChildPropertyFromLinkedItemSelector()
        {
            return item => item.Category;
            
        }

        public override NHibernate.Criterion.ICriterion GetCustomCriterionForSearch(NHibernate.ICriteria criteria, List<long> ids)
        {
            NHibernate.Criterion.ICriterion crit = null;
            // criteria.
            
                criteria.CreateAlias(this.Property.Name, this.Property.Name);

            HashedSet<long> allIds = new HashedSet<long>();
            foreach (var id in ids)
            {
                CategoryBase c = CategoryBase.Factory.GetByPrimaryKey(id);
                allIds.AddAll(c.GetAllCategoryIDsUnderThis(includeThis: true));
            }
            string propertyName = this.Property.Name + "." + this.GetPropertyToRetrieveLinkedItemFromLink().Name;
            crit = NHibernate.Criterion.Restrictions.InG(propertyName, allIds);
            return crit;
        }

        protected override System.Linq.Expressions.Expression<Func<ProductCategoryBase, ProductBase>> getParentPropertyFromLinkedItemSelector()
        {
            return item => item.Product;
            
        }
    }
}
