using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;

using BusinessLogic_v3.Classes.Cms.Util;
using BusinessLogic_v3.Modules.CategoryModule;
using BusinessLogic_v3.Modules.ProductCategoryModule;
using NHibernate;
using NHibernate.Criterion;
using CS.General_v3.Util;

using BusinessLogic_v3.Modules.ProductModule;

namespace BusinessLogic_v3.Cms.ProductModule
{
    public abstract class ProductBaseCmsFactory : BusinessLogic_v3.Modules._AutoGen.ProductBaseCmsFactory_AutoGen
    {
        public ProductBaseCmsFactory()
        {
            
        }

        protected override void initCmsParameters()
        {
            this.UserSpecificGeneralInfoInContext.TitlePlural = "Products";
            this.UserSpecificGeneralInfoInContext.TitleSingular = "Product";
            base.initCmsParameters();
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = Classes.Cms.EnumsCms.IMAGE_ICON.Bookshelf;
            addUpdateProductsButton();
            addImportButton();
            //custom CMS defaults - important, add AFTER the base.initCmsParameters()
        }

        private void addImportButton()
        {
            var loggedUser = getLoggedCmsUser();
            if (BusinessLogic_v3.Modules.ModuleSettings.Shop.CanBulkImportProducts && loggedUser != null && loggedUser.CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator))
            {
                this.UserSpecificGeneralInfoInContext.CustomCmsOperations.Add(
                    new CmsGeneralOperation("Import", Classes.Cms.EnumsCms.IMAGE_ICON.Import, CmsRoutesMapper.Instance.GetProducts_Import()));
            }
        }

        private void addUpdateProductsButton()
        {
            var loggedUser = getLoggedCmsUser();
            if ( loggedUser != null && loggedUser.CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft))
            {
                this.UserSpecificGeneralInfoInContext.CustomCmsOperations.Add(
                    new CmsGeneralOperation("Update Item Groups Data", Classes.Cms.EnumsCms.IMAGE_ICON.Update, updateProductsData));

            }

        }
        private void updateProductsData()
        {
            BusinessLogic_v3.Classes.DataUpdates.ProductUpdater.Instance.UpdateAll();
            CmsUtil.ShowStatusMessageInCMS("Item groups data updated successfully", CS.General_v3.Enums.STATUS_MSG_TYPE.Success);
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            cmsInfo.TitlePlural = "Products";
            cmsInfo.TitleSingular = "Product";
            if (UsedInProject)
                cmsInfo.ShowInCmsMainMenu = true;
            base.getUserSpecificGeneralInfo(cmsInfo);
        }

        public const string QUERYSTRING_FILTERCATEGORYID = "CategoryFilterId";

        public long? GetFilterCategoryIdFromQuerystring()
        {
            return CS.General_v3.Util.PageUtil.GetVariableFromQuerystring<long?>(QUERYSTRING_FILTERCATEGORYID);
        }



        private void checkFilterCategoryId(IQueryOver query)
        {
            var id = GetFilterCategoryIdFromQuerystring();
            if (id.GetValueOrDefault() > 0)
            {
                CategoryBase c = CategoryBase.Factory.GetByPrimaryKey(id.Value);
                if (c != null)
                {
                    var allIds = c.GetAllCategoryIDsUnderThis(includeThis: true);

                    var crit = query.RootCriteria.GetAssociationLinkCriteriaByPath<ProductBase>(item => item.CategoryLinks);
                    crit.Add(Restrictions.On<ProductCategoryBase>(link => link.Category).IsInG(allIds));

                }
            }
        }

        protected override void parseQueryForListing(NHibernate.IQueryOver query)
        {
            checkFilterCategoryId(query);

            base.parseQueryForListing(query);
        }

        public override CmsSearchResults GetSearchResultsForListing(IEnumerable<Classes.Cms.CmsObjects.Properties.CmsPropertySearchInfo> searchCriteria, bool showDeleted, bool filterByLinkedItems, int pageNo, int pageSize, Classes.Cms.CmsObjects.Properties.CmsFieldBase propertyToSortWith, CS.General_v3.Enums.SORT_TYPE sortType)
        {
            

            return base.GetSearchResultsForListing(searchCriteria, showDeleted, filterByLinkedItems, pageNo, pageSize, propertyToSortWith, sortType);
        }

    }
}
