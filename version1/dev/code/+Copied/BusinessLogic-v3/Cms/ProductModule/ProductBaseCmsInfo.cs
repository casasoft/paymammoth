using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties.CustomPropertyTypes;

using System.Web.UI.WebControls;
using BusinessLogic_v3.Modules.CategoryFeatureModule;
using BusinessLogic_v3.Modules.CategoryModule;
using BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule;
using BusinessLogic_v3.Modules.ProductCategoryModule;
using BusinessLogic_v3.Modules.ProductModule;

namespace BusinessLogic_v3.Cms.ProductModule
{
    public abstract class ProductBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.ProductBaseCmsInfo_AutoGen
    {
        public ProductBaseCmsInfo(ProductBase item)
            : base(item)
        {

        }

		protected override void customInitFields()
        {
            
            //custom init field logic here
            //this.Categories.LinkedWithInCms = true;
            //this.Category.LinkedWithInCms = true;
            base.customInitFields();
            addCategories();
            this.ProductVariations.ShowLinkInCms = true;
            initListingItems();
            initRequired();
            initListingPriority();
            initEditPriorities();
            this.IsFeatured.EditableInListing = true;
            this.IsSpecialOffer.EditableInListing = true;
            this.ProductSpecificationsHtml.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.Html;
            this.PriceRetailBefore.ShowInEdit = false;
            this.IsFeatured.ShowInListing = this.IsFeatured.ShowInEdit = false;
            
            this.CategoryFeatureValues_ForSearch.ShowInEdit = false;
            addCategoryFeaturesProperty();
            this.CategoryFeatures.Label = "Applicable Features";
            initSearchable();
            this.WarrantyText.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.Html;
            //this.WarrantyText.ShowInListing = true;
            this.WarrantyText.ShowInEdit = true;
            this.WeightInKg.HelpMessage = "This is the weight in Kilograms, as used for calculating shipping";

        }

        private void initSearchable()
        {
           // Category.IsSearchable = false;
            Published.IsSearchable = false;
            PriceWholesale.IsSearchable = false;
            ExtraInfo.IsSearchable = false;
            PriceRetailBefore.IsSearchable = false;
            WeightInKg.IsSearchable = false;
            WarrantyInMonths.IsSearchable = false;
            DateAdded.IsSearchable = false;
            IsFeatured.IsSearchable = false;
            this.ProductCategorySpecificationValues.ShowLinkInCms = true;
            this.ProductVariations.ShowLinkInCms = true;
            this.CategoryFeatureValues_ForSearch.IsSearchable = false;

            this.Priority.ShowInListing = false;
            Priority.IsSearchable = false;
            ExtraInfo.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
        }

        private void initEditPriorities()
        {
           
            this.Categories.ShowInEdit = true;
            this.EditOrderPriorities.AddColumnsToStart(Title, this.Brand, this.Categories, Description, ExtraInfo, this.CategoryFeatures, IsSpecialOffer, Published ,
                PriceRetailBefore, PriceRetailIncTaxPerUnit, PriceWholesale, TaxRatePercentage, ReferenceCode, SupplierReferenceCode, this.ProductSpecificationsHtml, this.WeightInKg, this.DimensionsLengthInCm,
                this.DimensionsWidthInCm, this.DimensionsHeightInCm, this.DimensionsVolumeInCc);
        
        }

        private void initListingItems()
        {
            this.Title.ShowInListing = true;
            this.Categories.ShowInListing = true;
           // this.PriceRetailBefore.ShowInListing = true;
            this.PriceRetailIncTaxPerUnit.ShowInListing = true;
            this.PriceRetailIncTaxPerUnit.Label = "Price";
            this.IsSpecialOffer.ShowInListing = true;
            this.PriceRetailIncTaxPerUnit.EditableInListing = true;
            //this.Title.EditableInListing = true;
        }

        private void initRequired()
        {
            this.Title.IsRequired = true;
            this.PriceRetailBefore.IsRequired = true;
            this.PriceRetailIncTaxPerUnit.IsRequired = true;
            this.IsSpecialOffer.IsRequired = false;
            this.Description.IsRequired = true;
            this.Description.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.Html;
            this.Categories.ShowInEdit = true;
            this.Brand.ShowInEdit = true;
            this.Brand.IsSearchable = true;
            this.Brand.ShowInListing = true;

            
        }

        public CmsPropertyManyToManyWithCustomClass<ProductBase, ProductCategoryBase, CategoryBase> Categories { get; set; }
        private void addCategories()
        {
            CmsPropertyManyToManyWithCustomClass<ProductBase, ProductCategoryBase, CategoryBase> p = new CmsPropertyManyToManyWithCustomClass<ProductBase, ProductCategoryBase, CategoryBase>(
                this,
                new ProductCategoriesManyToManyParams(),
                 EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);

            this.Categories = AddProperty(p);
            this.Categories.IsSearchable = true;
                
        }

        private void addCategoryFeaturesProperty()
        {
            this.CategoryFeatures = new CmsPropertyManyToManyWithCustomClass<ProductBase, ProductCategoryFeatureValueBase, CategoryFeatureBase>(
                this,
                new ProductCategoryFeaturesManyToManyParams(),
                EnumsCms.MULTICHOICE_DISPLAY_TYPE.CheckboxList);
            this.AddProperty(CategoryFeatures);

        }
        public CmsPropertyManyToManyWithCustomClass<ProductBase, ProductCategoryFeatureValueBase, CategoryFeatureBase> CategoryFeatures { get; private set; }
        



        private void initListingPriority()
        {
            this.ListingOrderPriorities.AddColumnsToStart(this.Brand, this.Categories, this.Title, this.PriceRetailBefore, this.PriceRetailIncTaxPerUnit, this.IsSpecialOffer);
        }
        
	}
}
