using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.MemberModule;

namespace BusinessLogic_v3.Cms.MemberModule
{
    public abstract class MemberBaseCmsFactory : BusinessLogic_v3.Modules._AutoGen.MemberBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            base.initCmsParameters();
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = Classes.Cms.EnumsCms.IMAGE_ICON.Member2;
        }
        
    }
}
