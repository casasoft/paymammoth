using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;

using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;

using BusinessLogic_v3.Modules.MemberModule;
using BusinessLogic_v3.Modules.MemberModule.SessionManager;

namespace BusinessLogic_v3.Cms.MemberModule
{
    public abstract class MemberBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.MemberBaseCmsInfo_AutoGen
    {
        public MemberBaseCmsInfo(MemberBase item)
            : base(item)
        {

        }

        protected override void customInitFields()
        {
            base.customInitFields();
            this.DateRegistered.ShowInListing = true;
            this.FirstName.ShowInListing = true;
            this.LastName.ShowInListing = true;
            this.Username.ShowInListing = true;
            this.Email.ShowInListing = true;
            this.Telephone.ShowInListing = true;
            this.Mobile.ShowInListing = true;
            this.Orders.ShowLinkInCms = true;
            this._LastLoggedIn_New.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            this.SessionGUID.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            this.ForgottenPassCode.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);

            this.Password.HelpMessage = "Please note that the password is 'hashed', so you cannot know what the password is.  However, if you enter a new password, it will be saved and the hashed value will be visible in this field";
            this.ListingOrderPriorities.AddColumnsToStart(this.DateRegistered, this.FirstName, this.LastName, this.Username, this.Email, this.Telephone, this.Mobile);

            this.EditOrderPriorities.AddColumnsToStart(this.ID, this.DateRegistered, this.Username,
                this.Password, this.FirstName, this.LastName, this.Gender, this.DateOfBirth, this.Email, this.Address1, this.Address2, this.Address3,
                this.Locality, this.State, this.PostCode, this.Country, this.Telephone, this.LastLoggedIn);

            this.CustomCmsOperations.Add(new CmsItemSpecificOperation(this, "Login as member",
                 Classes.Cms.EnumsCms.IMAGE_ICON.Member, loginAsMember));

            this.PasswordSalt.ShowInEdit = false;
            this.PasswordEncryptionType.ShowInEdit = false;

        }

        private void loginAsMember()
        {
            if (MemberSessionLoginManager.Instance != null && MemberBaseFactory.Instance != null)
            {
                MemberSessionLoginManager.Instance.Login(this.DbItem, true);
            }
            BusinessLogic_v3.Classes.Cms.Util.CmsUtil.ShowStatusMessageInCMS("Logged in successfully in frontend. Kindly go to the frontend, and you will be automatically logged in", CS.General_v3.Enums.STATUS_MSG_TYPE.Success);
        }
	}
}
