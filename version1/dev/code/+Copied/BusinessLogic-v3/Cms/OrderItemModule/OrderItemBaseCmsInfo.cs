using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.OrderItemModule;

namespace BusinessLogic_v3.Cms.OrderItemModule
{
    public abstract class OrderItemBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.OrderItemBaseCmsInfo_AutoGen
    {
        public OrderItemBaseCmsInfo(OrderItemBase item)
            : base(item)
        {

        }

        protected override void customInitFields()
        {
            
            base.customInitFields();
            
            this.Title.EditableInListing = true;
            this.ItemReference.ShowInListing = true;
            this.Quantity.EditableInListing = true;
            this.SpecialOffer.ShowInListing = true;
            this.PriceExcTaxPerUnit.EditableInListing = true;
          //  this.ShippingCost.EditableInListing = true;
           // this.HandlingCost.EditableInListing = true;
            this.ShipmentWeight.ShowInListing= true;
            this.DiscountPerUnit.EditableInListing = true;
            this.TaxAmountPerUnit.EditableInListing = true;
            this.Order.IsSearchable = false;
            this.SpecialOfferVoucherCode.IsSearchable = false;
            this.ListingOrderPriorities.AddColumnsToStart(this.Title,this.ItemReference,this.Quantity,this.SpecialOffer, this.PriceExcTaxPerUnit,  this.ShipmentWeight, this.DiscountPerUnit, this.TaxAmountPerUnit);
            
            
        }

        

	}
}
