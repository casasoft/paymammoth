using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Modules.FileItemModule;

namespace BusinessLogic_v3.Cms.FileItemModule
{
    public abstract class FileItemBaseCmsFactory : BusinessLogic_v3.Modules._AutoGen.FileItemBaseCmsFactory_AutoGen
    {
       

        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
        	//any user-specific information, add them to 'cmsInfo'.

            base.getUserSpecificGeneralInfo(cmsInfo);
            cmsInfo.CmsImageIcon = BusinessLogic_v3.Classes.Cms.EnumsCms.IMAGE_ICON.Listing;

        }

    }
}
