using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;

using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties.CustomPropertyTypes;
using BusinessLogic_v3.Frontend.FileItemModule;
using CS.General_v3.Extensions;

using BusinessLogic_v3.Modules.FileItemModule;

namespace BusinessLogic_v3.Cms.FileItemModule
{
    public abstract class FileItemBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.FileItemBaseCmsInfo_AutoGen
    {
    	public FileItemBaseCmsInfo(FileItemBase item)
            : base(item)
        {

        }

        protected override void customInitFields()
        {
            base.customInitFields();
            initRequired();
            initListingItems();
            initListingItemsPriority();
            initUpload();
        }

        private void initUpload()
        {
            this.File = this.AddPropertyForMediaItem<FileItemBase>(item => item.File, true, true);
        }
        public CmsPropertyMediaItem<FileItemBase> File { get; set; }

        private void initListingItemsPriority()
        {
            this.ListingOrderPriorities.AddColumnsToStart(this.Title,this.UploadedBy, this.UploadedOn);
        }

        private void initRequired()
        {
            this.Title.IsRequired = true;
            //this.UploadedBy.IsRequired = true;
            this.UploadedOn.IsRequired = true;
        }

        private void initListingItems()
        {
            this.Title.ShowInListing = true;
          //  this.UploadedBy.ShowInListing = true;
            this.UploadedOn.ShowInListing = true;
        }
    }
}
