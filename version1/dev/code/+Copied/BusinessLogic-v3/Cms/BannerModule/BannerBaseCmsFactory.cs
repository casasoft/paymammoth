using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.BannerModule;

using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;

namespace BusinessLogic_v3.Cms.BannerModule
{
    public abstract class BannerBaseCmsFactory : BusinessLogic_v3.Modules._AutoGen.BannerBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            
            base.initCmsParameters();
            //custom CMS defaults - important, add AFTER the base.initCmsParameters()
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = BusinessLogic_v3.Classes.Cms.EnumsCms.IMAGE_ICON.Photos;
        }
    }
}
