using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Frontend.BannerModule;
using BusinessLogic_v3.Modules.BannerModule;

namespace BusinessLogic_v3.Cms.BannerModule
{
    public abstract class BannerBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.BannerBaseCmsInfo_AutoGen
    {
    	public BannerBaseCmsInfo(BannerBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();

            initRequired();
            initListingItems();
            initListingItemsPriority();
            initUpload();
            MediaItemFilename.ShowInEdit = MediaItemFilename.ShowInListing = false;
            Identifier.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            
        }

        private void initUpload()
        {
            this.Image = this.AddPropertyForMediaItem<BannerBase>(item => item.MediaItem, true, true);
        }
        public CmsPropertyMediaItem<BannerBase> Image { get; set; }

        private void initListingItemsPriority()
        {
            this.ListingOrderPriorities.AddColumnsToStart(this.Identifier, this.Title, this.Link, this.DurationMS);

            //this.Identifier.ListingColumnPriority = 1000;
            //this.Title.ListingColumnPriority = 2000;
            //this.MediaItemURL.ListingColumnPriority = 3000;
            //this.Link.ListingColumnPriority = 4000;
            //this.DurationMS.ListingColumnPriority = 5000;
        }

        private void initRequired()
        {
            //Identifier.IsRequired = true;
            Title.IsRequired = true; 
            
            DurationMS.IsRequired = true;
        }

        private void initListingItems()
        {
            DurationMS.ShowInListing = true; 
            Link.ShowInListing = true; 
            

            Title.ShowInListing = true; 
            Identifier.ShowInListing = true;
        }
	}
}
