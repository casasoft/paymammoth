using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.CurrencyModule;

namespace BusinessLogic_v3.Cms.CurrencyModule
{
    public abstract class CurrencyBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.CurrencyBaseCmsInfo_AutoGen
    {
        public CurrencyBaseCmsInfo(CurrencyBase item)
            : base(item)
        {
            this.AccessTypeRequired_ToDelete.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner;
            this.AccessTypeRequired_ToEdit.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner;
            this.AccessTypeRequired_ToView.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner;
            
        }

        protected override void customInitFields()
        {
            base.customInitFields();
            this.Published.EditableInListing = true; 
            this.CurrencyISOCode.ShowInListing = true; this.CurrencyISOCode.IsRequired = true;
            this.ExchangeRateMultiplier.EditableInListing = true; this.ExchangeRateMultiplier.IsRequired = true;
            this.ExchangeRateMultiplier.HelpMessage =
                @"This is the exchange rate multiplier from the BASE/MAIN CURRENCY to this.  I.e, if this currency is USD, and the base currency is GBP,
    the multiplier must be the amount you must multiply 1 GBP, in order to get 1USD equivalent. E.g as of 23/Aug/2011, This is roughly 1.65600. Please note that numbers can even be less than 1.";
            
            this.Priority.EditableInListing = true; this.Priority.IsRequired = true;
            this.Title.ShowInListing = true; this.Title.IsRequired = true;
            this.Symbol.ShowInListing = true; this.Symbol.IsRequired = true;




        }
	}
}
