using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;


using BusinessLogic_v3.Modules.ContentTextModule;

namespace BusinessLogic_v3.Cms.ContentTextModule
{
    public abstract class ContentTextBaseCmsFactory : BusinessLogic_v3.Modules._AutoGen.ContentTextBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            base.initCmsParameters();
            
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            cmsInfo.RenderType = Classes.Cms.EnumsCms.SECTION_RENDER_TYPE.Tree;
            cmsInfo.CmsImageIcon = Classes.Cms.EnumsCms.IMAGE_ICON.ShortNotes;
            cmsInfo.ShowInCmsMainMenu = true;
            base.getUserSpecificGeneralInfo(cmsInfo);
        }

        public override IEnumerable<ICmsHierarchyItem> GetRootItems(IEnumerable<CmsPropertySearchInfo> searchCriteria)
        {

            List<ICmsHierarchyItem> list = new List<ICmsHierarchyItem>();
            var root = ContentTextBase.Factory.GetRootNode(true);
            list.Add(GetCmsItemFromDBObject( root));
            return list;

        }
        
    }
}
