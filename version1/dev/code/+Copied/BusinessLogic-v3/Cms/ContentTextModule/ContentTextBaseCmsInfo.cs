using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Frontend.ContentTextModule;
using BusinessLogic_v3.Modules._Common;
using BusinessLogic_v3.Modules.CmsUserModule;
using CS.General_v3.Controls.WebControls.Specialized.Hierarchy;
using CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructureBasic;
using CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructure;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Extensions;

using BusinessLogic_v3.Modules.ContentTextModule;

namespace BusinessLogic_v3.Cms.ContentTextModule
{
    public abstract class ContentTextBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.ContentTextBaseCmsInfo_AutoGen ,ICmsHierarchyInfo,ICmsHierarchyItem
    {


        private CmsItemHierarchyCommonImpl _commonImpl_CmsItemHierarchy = null;
        public ContentTextBaseCmsInfo(ContentTextBase item)
            : base(item)
        {
            this._commonImpl_CmsItemHierarchy = new CmsItemHierarchyCommonImpl(this, typeof(ContentTextBase));
            
        }


        private void initImageFields()
        {



            this.ContentImage = this.AddPropertyForMediaItem<ContentTextBase>(item => item.ContentImage);
        }

        public CmsPropertyMediaItem<ContentTextBase> ContentImage { get; private set; }


        protected override void customInitFields()
        {
            initImageFields();
            var user = CmsUserSessionLogin.Instance.GetLoggedInUser();
            base.customInitFields();
            this.Parent.CustomListItemCollectionRetriever += new CmsPropertyInfo.CustomListItemCollectionDelegate(Parent_CustomListItemCollectionRetriever);
            this.ChildContentTexts.ShowLinkInCms = true;
            
            this.Parent.ShowInEdit = true;
            this.Name.ShowInListing = true;
            this.Parent.ShowInListing = true;
            this.Name.IsRequired = true;
            this.Content.IsRequired = true;
            this.Identifier.AccessTypeRequired_Edit.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft;
            this.Name.AccessTypeRequired_Edit.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft;
            this.Parent.IsRequired = true;
            this.ImageAlternateText.HelpMessage = "Optional.  Used only when the content type is Image and is used to describe the image in words";
           // this.ContentTags.AccessTypeRequired_Edit.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft;
            this.UsedInProject.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            this.UsedInProject.SetDefaultSearchValue(true);
            if (DbItem != null)

            {
                if (DbItem.ContentType == Enums.CONTENT_TEXT_TYPE.Html)
                {
                    this.Content.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.Html;
                }
                else if (DbItem.ContentType == Enums.CONTENT_TEXT_TYPE.PlainText)
                {
                    this.Content.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.MultiLine;

                }
               /*if (user != null && user.CheckAccessIsLessThan(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft))
                {*/
                    this.Content.AccessTypeRequired_View.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne;
                    this.ContentImage.AccessTypeRequired_View.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne;
                    if (DbItem.ContentType == Enums.CONTENT_TEXT_TYPE.Html)
                    {
                        this.Content.AccessTypeRequired_View.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Normal;
                    }
                   /* 2012-01-12 Removed by Mark as this will now be made as HTML
                    * else if (DbItem.ContentType == Enums.CONTENT_TEXT_TYPE.Image)
                    {
                        this.ContentImage.IsRequired = true;
                        this.ContentImage.AccessTypeRequired_View.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Normal;

                    }*/
                    else
                    {
                        this.Content.AccessTypeRequired_View.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Normal;

                    }

                //}
            }
            

            this.EditOrderPriorities.AddColumnsToStart(this.ID, this.Name, this.Content, this.ContentImage, this.Parent);


        }

        System.Web.UI.WebControls.ListItemCollection Parent_CustomListItemCollectionRetriever(CmsPropertyInfo propertyInfo, ICmsItemInstance item, BusinessLogic_v3.Classes.Cms.EnumsCms.SECTION_TYPE sectionType)
        {
            var list = this.Parent.GetListItemCollectionForDataType(sectionType);
            if (item != null && item.DbItem.ID > 0)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    var listItem = list[i];
                    if (listItem.Value == item.DbItem.ID.ToString())
                    {
                        list.RemoveAt(i);
                        i--;
                        
                    }
                }
            }
            return list;            
        }

        #region ICmsHierarchyItem Members

        ICmsHierarchyInfo ICmsHierarchyItem.GetCmsHierarchyInfo()
        {
            return this;
        }
        #endregion

        #region ICmsHierarchyInfo Members

        ICmsCollectionInfo ICmsHierarchyInfo.SubitemLinkedCollection
        {
            get { return this.ChildContentTexts; }
        }


        string ICmsHierarchyInfo.GetTitleInHierarchy()
        {
            return this.ToString();
        }

        #endregion

        #region IHierarchy Members


        string IHierarchy.Title
        {
            get { return this.DbItem.Name; }
        }

        #endregion

        #region IHierarchy Members


        string IHierarchy.Href
        {
            get { return "#"; }
        }

        CS.General_v3.Enums.HREF_TARGET IHierarchy.HrefTarget
        {
            get { return CS.General_v3.Enums.HREF_TARGET.Self; }
        }

        IEnumerable<CS.General_v3.Controls.WebControls.Specialized.Hierarchy.IHierarchy> IHierarchy.GetChildren()
        {
            return getChildren();
            
        }


        bool IHierarchy.Selected
        {
            get { return false; }
        }

        string IHierarchy.GetFullHierarchyName(string delimiter, bool reverseOrder, bool includeRoot)
        {
            return _commonImpl_CmsItemHierarchy.GetFullHierarchyName(delimiter, reverseOrder, includeRoot);
        }

        bool IHierarchy.Visible
        {
            get { return true; }
        }

        #endregion


        #region ITreeItem Members

        string ITreeItem.EditURL
        {
            get { return _commonImpl_CmsItemHierarchy.EditURL; }
        }

        string ITreeItem.AddNewItemURL
        {
            get { return _commonImpl_CmsItemHierarchy.AddNewItemURL; }
        }

        bool ITreeItem.AllowUpdate
        {
            get { return CmsUserSessionLogin.Instance.GetLoggedInUser().CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator); }
        }

        bool ITreeItem.AllowDelete
        {
            get { return CmsUserSessionLogin.Instance.GetLoggedInUser().CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft); }
        }

        bool ITreeItem.AllowAddSubItems
        {
            get { return CmsUserSessionLogin.Instance.GetLoggedInUser().CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft); }
        }

        bool ITreeItem.CanRemove(out string errorMsg)
        {
            errorMsg = "";
            return ((ITreeItem)this).AllowDelete;

        }

        OperationResult ITreeItem.Remove()
        {
            return this.DeleteFromCms();

        }

        string ITreeItem.Message_DeleteOK
        {
            get { return _commonImpl_CmsItemHierarchy.Message_DeleteOK; }
        }

        string ITreeItem.Message_ConfirmDelete
        {
            get { return _commonImpl_CmsItemHierarchy.Message_ConfirmDelete; }
        }

        bool ITreeItem.AddedExtraButtons
        {
            get
            {
                return _commonImpl_CmsItemHierarchy.AddedExtraButtons;

            }
            set
            {
                _commonImpl_CmsItemHierarchy.AddedExtraButtons = value;

            }
        }

        #endregion

        #region ITreeItemBasic Members


        int ITreeItemBasic.Priority
        {
            get
            {
                return 0;

            }
            set
            {

            }
        }

        string ITreeItemBasic.ImageURL
        {
            get { return _commonImpl_CmsItemHierarchy.ImageUrl; }
        }

        string ITreeItemBasic.LinkURL
        {
            get { return _commonImpl_CmsItemHierarchy.LinkUrl; }
        }

        private List<ContentTextBaseCmsInfo> _children = null;
        protected List<ContentTextBaseCmsInfo> getChildren()
        {
            if (_children == null)
            {
                List<ContentTextBaseCmsInfo> list = new List<ContentTextBaseCmsInfo>();
                foreach (var item in DbItem.ChildContentTexts)
                {
                    list.Add((ContentTextBaseCmsInfo)factory.GetCmsItemFromDBObject(item));
                }
                _children = list;
                _children = list.Where<ContentTextBaseCmsInfo>(item => item.DbItem.UsedInProject).ToList();
            }
            return _children; ;
        }

        IEnumerable<CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructureBasic.ITreeItemBasic> ITreeItemBasic.GetChildTreeItems()
        {
            return getChildren();
            

        }

        List<CS.General_v3.Controls.WebControls.Classes.ExtraButton> ITreeItemBasic.ExtraButtons
        {
            get { return _commonImpl_CmsItemHierarchy.ExtraButtons; }
        }

        void ITreeItemBasic.AddExtraButtons()
        {
            _commonImpl_CmsItemHierarchy.AddExtraButtons();
        }

        #endregion



        #region ITreeItemBasic Members


        string ITreeItemBasic.Title
        {
            get { return ((IHierarchy)this).Title; }
        }

        #endregion

        #region ITreeItem Members


        OperationResult ITreeItem.Save()
        {
            return this.SaveFromCms();
            
        }

        #endregion

        public class TREE_ITEM : CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructure.TreeItem
        {
            public TREE_ITEM()
            {
                //this.ExtraButtons = new List<CS.General_v3.Controls.WebControls.Specialized.TreeStructure.TreeStructureBasic.ExtraButton>();
            }
            public override bool CanRemove(out string errorMessage)
            {
                errorMessage = null;
                return CmsUserSessionLogin.Instance.CheckAccessTypeOfLoggedUser(CS.General_v3.Enums.CMS_ACCESS_TYPE.SuperAdministrator);


            }

            private ContentTextBaseCmsInfo  _item = null;
            public void FillFromCategory(ContentTextBaseCmsInfo cat)
            {
                _item = cat;
            }
            #region ITreeItem Members

            public override long ID
            {
                get
                {
                    return _item.DbItem.ID;
                }

            }

            public override string Title
            {
                get
                {
                    return _item.DbItem.Name;
                }
                set
                {
                    _item.DbItem.Name = value;
                }
            }

            public override int Priority
            {
                get
                {
                    return 0;
                }
                set
                {

                }
            }

            public override string ImageURL
            {
                get
                {
                    return "/_common/images/components/v1/tree/icon_category.gif";
                }

            }

            public override string EditURL
            {
                get
                {
                    return "/cms/ContentText/ContentText.aspx?id=" + _item.ID;
                }

            }

            public override string AddNewItemURL
            {
                get
                {
                    return "/cms/ContentText/ContentText.aspx?parentID=" + _item.ID + "&id=0";
                }

            }

            public override bool AllowUpdate
            {
                get { return true; }
            }

            public override bool AllowDelete
            {
                get
                {
                    return CmsUserSessionLogin.Instance.CheckAccessTypeOfLoggedUser(CS.General_v3.Enums.CMS_ACCESS_TYPE.SuperAdministrator);


                }
            }



            public override OperationResult Remove()
            {
                var cmsUser = CmsUserSessionLogin.Instance.GetLoggedInUser();
                if (cmsUser != null && cmsUser.CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE.Administrator))
                {
                    return this._item.DeleteFromCms();

                }
                else
                {
                    throw new InvalidOperationException("Cannot remove!");
                }
            }

            public override OperationResult Save()
            {
                return _item.SaveFromCms();
            }
            /*private bool _parsedChildItems = false;
            public override IEnumerable<CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructureBasic.ITreeItemBasic> GetChildTreeItems()
            {
                if (!_parsedChildItems)
                {
                    _parsedChildItems = true;
                    List<CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructureBasic.ITreeItemBasic> list = new List<CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructureBasic.ITreeItemBasic>();
                    foreach (var c in _item.ChildContentTexts)
                    {
                        list.Add(c.GetAsITreeItem());
                    }
                    this.ChildItems = list;
                }
                return this.ChildItems;
            }
            */


            public override string Message_DeleteOK
            {
                get
                {
                    return "Content text deleted successfully";
                }

            }


            public override string Message_ConfirmDelete
            {
                get
                {
                    string msg = @"Are you sure you want to delete this text, and all its sub-texts?\r\n\r\n";

                    return msg;
                }

            }


            #endregion

            #region ITreeItemBasic Members



            #endregion

            #region ITreeItem Members


            public override bool AllowAddSubItems
            {
                get { return CmsUserSessionLogin.Instance.CheckAccessTypeOfLoggedUser(CS.General_v3.Enums.CMS_ACCESS_TYPE.SuperAdministrator); }
            }


            #endregion
        }
        private TREE_ITEM _item = null;
        public TREE_ITEM GetAsITreeItem()
        {
            if (_item == null)
            {
                _item = new TREE_ITEM();
                _item.FillFromCategory(this);
            }
            return _item;
        }

        #region IHierarchy Members

        long IHierarchy.ID
        {
            get { return this.DbItem.ID; }
        }

        #endregion


        long ITreeItemBasic.ID
        {
            get { return this.DbItem.ID; }
        }

       
        IEnumerable<IHierarchy> IHierarchy.GetParents()
        {
            return CS.General_v3.Util.ListUtil.GetListFromSingleItem((IHierarchy)factory.GetCmsItemFromDBObject( this.DbItem.Parent) ); 
        }
       
    }

    
}
