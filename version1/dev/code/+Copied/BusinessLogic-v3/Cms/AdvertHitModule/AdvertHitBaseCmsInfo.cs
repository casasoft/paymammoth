using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.AdvertHitModule;

namespace BusinessLogic_v3.Cms.AdvertHitModule
{
    public abstract class AdvertHitBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.AdvertHitBaseCmsInfo_AutoGen
    {
    	public AdvertHitBaseCmsInfo(AdvertHitBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
	}
}
