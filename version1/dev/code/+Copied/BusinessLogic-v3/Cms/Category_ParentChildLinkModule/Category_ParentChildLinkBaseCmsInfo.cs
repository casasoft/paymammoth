using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.Category_ParentChildLinkModule;

namespace BusinessLogic_v3.Cms.Category_ParentChildLinkModule
{
    public abstract class Category_ParentChildLinkBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.Category_ParentChildLinkBaseCmsInfo_AutoGen
    {
    	public Category_ParentChildLinkBaseCmsInfo(Category_ParentChildLinkBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
	}
}
