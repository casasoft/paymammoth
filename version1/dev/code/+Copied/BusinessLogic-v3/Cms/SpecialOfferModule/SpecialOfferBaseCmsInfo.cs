using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.SpecialOfferModule;

namespace BusinessLogic_v3.Cms.SpecialOfferModule
{
    public abstract class SpecialOfferBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.SpecialOfferBaseCmsInfo_AutoGen
    {
        public SpecialOfferBaseCmsInfo(SpecialOfferBase item)
            : base(item)
        {

        }

        protected override void customInitFields()
        {
            base.customInitFields();
            this.SpecialOfferVoucherCodes.ShowLinkInCms = true;
            this.Name.ShowInListing = true;
            this.DateFrom.ShowInListing = true;
            this.DateTo.ShowInListing = true;
            this.DiscountFixed.ShowInListing = true;
            this.DiscountPercentage.ShowInListing = true;
            this.Activated.EditableInListing = true;
            this.Category.ShowInListing = true;
            this.QuantityLeft.ShowInListing = true;

            this.Title.IsRequired = true;
            this.QuantityLeft.IsRequired = true;
            this.DateFrom.IsRequired = true;
            this.DateTo.IsRequired = true;
            this.DiscountFixed.IsRequired = true;
            this.DiscountPercentage.IsRequired = true;


            this.ListingOrderPriorities.AddColumnsToStart(this.Name, this.Category, this.DateFrom, this.DateTo, this.DiscountFixed, this.DiscountPercentage, this.QuantityLeft, this.Activated);
            this.EditOrderPriorities.AddColumnsToStart(this.Name, this.Category, this.DateFrom,this.DateTo, this.DiscountPercentage, this.DiscountPercentage, this.RequiredTotalItemsToBuy,
                this.RequiredTotalToSpend, this.RequiresPromoCode,
                this.QuantityLeft, this.Activated);
                

        }
	}
}
