using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.SpecialOfferModule;

namespace BusinessLogic_v3.Cms.SpecialOfferModule
{
    public abstract class SpecialOfferBaseCmsFactory : BusinessLogic_v3.Modules._AutoGen.SpecialOfferBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            base.initCmsParameters();
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = Classes.Cms.EnumsCms.IMAGE_ICON.Present_Gift;
        }
    }
}
