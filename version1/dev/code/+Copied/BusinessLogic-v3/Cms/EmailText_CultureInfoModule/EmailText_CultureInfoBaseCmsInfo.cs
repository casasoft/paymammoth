using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.EmailText_CultureInfoModule;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using CS.General_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Cms.EmailText_CultureInfoModule
{

//BaseCmsInfo-Class

    public abstract class EmailText_CultureInfoBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.EmailText_CultureInfoBaseCmsInfo_AutoGen
    {
    	public EmailText_CultureInfoBaseCmsInfo(EmailText_CultureInfoBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
	}
}
