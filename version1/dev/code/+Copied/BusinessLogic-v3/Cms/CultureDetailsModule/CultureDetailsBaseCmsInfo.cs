using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.CultureDetailsModule;

namespace BusinessLogic_v3.Cms.CultureDetailsModule
{
    public abstract class CultureDetailsBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.CultureDetailsBaseCmsInfo_AutoGen
    {
        public CultureDetailsBaseCmsInfo(CultureDetailsBase item)
            : base(item)
        {
            this.AccessTypeRequired_ToDelete.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner;
            this.AccessTypeRequired_ToEdit.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner;
            this.AccessTypeRequired_ToView.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.Owner;
        }

        protected override void customInitFields()
        {
            base.customInitFields();
            this.DefaultCurrency.ShowInEdit = true;
            this.DefaultCurrency.ShowInListing = true;
            this.Published.EditableInListing = true;
            this.DefaultCurrency.IsRequired = true;
            this.IsDefaultCulture.ShowInListing = true;
            this.LanguageISOCode.ShowInListing = true; this.LanguageISOCode.IsRequired = true;
            this.Priority.EditableInListing = true; this.Priority.IsRequired = true;
            this.Title.ShowInListing = true; this.Title.IsRequired = true;
            this.BaseRedirectionUrl.HelpMessage = "This allows you to set the url where the user will be redirected to, when he chooses that language. E.g http://www.oneoutof100.com";
            this.BaseUrlRegex.HelpMessage = @"This allows you to setup the url match which is used to compare which language the user is currently in, e.g if the regex is 
(fr|ca)\.oneoutof100\.com, this means taht both fr.oneoutof100.com and ca.oneoutof100.com will use the French language for example";

            

        }
        
	}
}
