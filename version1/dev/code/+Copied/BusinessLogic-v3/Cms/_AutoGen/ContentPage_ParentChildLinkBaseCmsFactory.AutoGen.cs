using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.ContentPage_ParentChildLinkModule;
using BusinessLogic_v3.Cms.ContentPage_ParentChildLinkModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class ContentPage_ParentChildLinkBaseCmsFactory_AutoGen : CmsFactoryBase<ContentPage_ParentChildLinkBaseCmsInfo, ContentPage_ParentChildLinkBase>
    {
       
       public new static ContentPage_ParentChildLinkBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ContentPage_ParentChildLinkBaseCmsFactory)CmsFactoryBase<ContentPage_ParentChildLinkBaseCmsInfo, ContentPage_ParentChildLinkBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = ContentPage_ParentChildLinkBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ContentPage_ParentChildLink.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ContentPage_ParentChildLink";

            this.QueryStringParamID = "ContentPage_ParentChildLinkId";

            cmsInfo.TitlePlural = "Content Page _ Parent Child Links";

            cmsInfo.TitleSingular =  "Content Page _ Parent Child Link";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ContentPage_ParentChildLinkBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "ContentPage_ParentChildLink/";


        }
       
    }

}
