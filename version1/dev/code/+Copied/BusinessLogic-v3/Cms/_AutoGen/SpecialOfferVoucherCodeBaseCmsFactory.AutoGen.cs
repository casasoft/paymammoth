using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule;
using BusinessLogic_v3.Cms.SpecialOfferVoucherCodeModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class SpecialOfferVoucherCodeBaseCmsFactory_AutoGen : CmsFactoryBase<SpecialOfferVoucherCodeBaseCmsInfo, SpecialOfferVoucherCodeBase>
    {
       
       public new static SpecialOfferVoucherCodeBaseCmsFactory Instance
	    {
	         get
	         {
                 return (SpecialOfferVoucherCodeBaseCmsFactory)CmsFactoryBase<SpecialOfferVoucherCodeBaseCmsInfo, SpecialOfferVoucherCodeBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = SpecialOfferVoucherCodeBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "SpecialOfferVoucherCode.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "SpecialOfferVoucherCode";

            this.QueryStringParamID = "SpecialOfferVoucherCodeId";

            cmsInfo.TitlePlural = "Special Offer Voucher Codes";

            cmsInfo.TitleSingular =  "Special Offer Voucher Code";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public SpecialOfferVoucherCodeBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "SpecialOfferVoucherCode/";


        }
       
    }

}
