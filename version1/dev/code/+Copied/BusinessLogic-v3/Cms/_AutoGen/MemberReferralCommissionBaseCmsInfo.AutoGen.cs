using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.MemberReferralCommissionModule;
using BusinessLogic_v3.Cms.MemberReferralCommissionModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class MemberReferralCommissionBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.MemberReferralCommissionModule.MemberReferralCommissionBase>
    {
		public MemberReferralCommissionBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.MemberReferralCommissionModule.MemberReferralCommissionBase dbItem)
            : base(BusinessLogic_v3.Cms.MemberReferralCommissionModule.MemberReferralCommissionBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new MemberReferralCommissionBase DbItem
        {
            get
            {
                return (MemberReferralCommissionBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo DateIssued { get; private set; }

        public CmsPropertyInfo Amount { get; private set; }

        public CmsPropertyInfo Status { get; private set; }

        public CmsPropertyInfo PaidOn { get; private set; }

        public CmsPropertyInfo PaidBy { get; private set; }

        public CmsPropertyInfo PaymentRemarks { get; private set; }

        public CmsPropertyInfo MemberReferral { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.DateIssued = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberReferralCommissionModule.MemberReferralCommissionBase>.GetPropertyBySelector( item => item.DateIssued),
        			isEditable: true,
        			isVisible: true
        			);

        this.Amount = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberReferralCommissionModule.MemberReferralCommissionBase>.GetPropertyBySelector( item => item.Amount),
        			isEditable: true,
        			isVisible: true
        			);

        this.Status = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberReferralCommissionModule.MemberReferralCommissionBase>.GetPropertyBySelector( item => item.Status),
        			isEditable: true,
        			isVisible: true
        			);

        this.PaidOn = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberReferralCommissionModule.MemberReferralCommissionBase>.GetPropertyBySelector( item => item.PaidOn),
        			isEditable: true,
        			isVisible: true
        			);

        this.PaidBy = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberReferralCommissionModule.MemberReferralCommissionBase>.GetPropertyBySelector( item => item.PaidBy),
        			isEditable: true,
        			isVisible: true
        			);

        this.PaymentRemarks = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberReferralCommissionModule.MemberReferralCommissionBase>.GetPropertyBySelector( item => item.PaymentRemarks),
        			isEditable: true,
        			isVisible: true
        			);

        this.MemberReferral = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberReferralCommissionModule.MemberReferralCommissionBase>.GetPropertyBySelector( item => item.MemberReferral),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
