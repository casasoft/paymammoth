using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.CategoryFeatureModule;
using BusinessLogic_v3.Cms.CategoryFeatureModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class CategoryFeatureBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase>
    {
		public CategoryFeatureBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase dbItem)
            : base(BusinessLogic_v3.Cms.CategoryFeatureModule.CategoryFeatureBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new CategoryFeatureBase DbItem
        {
            get
            {
                return (CategoryFeatureBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo IconFilename { get; private set; }

        public CmsPropertyInfo Description { get; private set; }

        public CmsPropertyInfo Link { get; private set; }

        public CmsPropertyInfo Category { get; private set; }

        public CmsPropertyInfo ItemGroup { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.IconFilename = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase>.GetPropertyBySelector( item => item.IconFilename),
        			isEditable: false,
        			isVisible: false
        			);

        this.Description = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase>.GetPropertyBySelector( item => item.Description),
        			isEditable: true,
        			isVisible: true
        			);

        this.Link = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase>.GetPropertyBySelector( item => item.Link),
        			isEditable: true,
        			isVisible: true
        			);

        this.Category = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase>.GetPropertyBySelector( item => item.Category),
        			isEditable: true,
        			isVisible: true
        			);

        this.ItemGroup = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryFeatureModule.CategoryFeatureBase>.GetPropertyBySelector( item => item.ItemGroup),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
