using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.CmsUserRoleModule;
using BusinessLogic_v3.Cms.CmsUserRoleModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class CmsUserRoleBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.CmsUserRoleModule.CmsUserRoleBase>
    {
		public CmsUserRoleBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.CmsUserRoleModule.CmsUserRoleBase dbItem)
            : base(BusinessLogic_v3.Cms.CmsUserRoleModule.CmsUserRoleBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new CmsUserRoleBase DbItem
        {
            get
            {
                return (CmsUserRoleBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo Identifier { get; private set; }



// [basecmsinfo_manytomanydeclarations]

        public CmsPropertyManyToManyCollection<BusinessLogic_v3.Cms.CMSUserModule.CMSUserBaseCmsInfo> CmsUsers { get; private set; }



        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CmsUserRoleModule.CmsUserRoleBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.Identifier = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CmsUserRoleModule.CmsUserRoleBase>.GetPropertyBySelector( item => item.Identifier),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]

//InitFieldManyToManyBase_RightSide
		this.CmsUsers = this.AddManyToManyCollectionWithDirectProperty<BusinessLogic_v3.Cms.CMSUserModule.CMSUserBaseCmsInfo>(
		BusinessLogic_v3.Cms.CMSUserModule.CMSUserBaseCmsFactory.Instance, 
		CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CmsUserRoleModule.CmsUserRoleBase>.GetPropertyBySelector(item => item.GetCmsUsers()), 
		
		((item) => ((BusinessLogic_v3.Modules.CmsUserRoleModule.CmsUserRoleBase) item).ClearCmsUsers()),
            ((item, itemToAdd) => ((BusinessLogic_v3.Modules.CmsUserRoleModule.CmsUserRoleBase) item).AddCMSUserToCmsUsers((BusinessLogic_v3.Modules.CMSUserModule.CMSUserBase)itemToAdd)),
		
		
		Classes.Cms.EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);


			base.initBasicFields();
          
        }

    }
}
