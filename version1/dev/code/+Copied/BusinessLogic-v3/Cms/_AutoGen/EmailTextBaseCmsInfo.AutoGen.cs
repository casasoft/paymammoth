using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.EmailTextModule;
using BusinessLogic_v3.Cms.EmailTextModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class EmailTextBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase>
    {
		public EmailTextBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase dbItem)
            : base(BusinessLogic_v3.Cms.EmailTextModule.EmailTextBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new EmailTextBase DbItem
        {
            get
            {
                return (EmailTextBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo Parent { get; private set; }

        public CmsPropertyInfo Subject { get; private set; }

        public CmsPropertyInfo Subject_Search { get; private set; }

        public CmsPropertyInfo Body { get; private set; }

        public CmsPropertyInfo Body_Search { get; private set; }

        public CmsPropertyInfo Remarks { get; private set; }

        public CmsPropertyInfo Identifier { get; private set; }

        public CmsPropertyInfo VisibleInCMS_AccessRequired { get; private set; }

        public CmsPropertyInfo UsedInProject { get; private set; }



// [basecmsinfo_manytomanydeclarations]

        public CmsPropertyManyToManyCollection<BusinessLogic_v3.Cms.ContentTagModule.ContentTagBaseCmsInfo> ContentTags { get; private set; }



        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.Parent = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase>.GetPropertyBySelector( item => item.Parent),
        			isEditable: true,
        			isVisible: true
        			);

        this.Subject = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase>.GetPropertyBySelector( item => item.Subject),
        			isEditable: true,
        			isVisible: true
        			);

        this.Subject_Search = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase>.GetPropertyBySelector( item => item.Subject_Search),
        			isEditable: false,
        			isVisible: false
        			);

        this.Body = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase>.GetPropertyBySelector( item => item.Body),
        			isEditable: true,
        			isVisible: true
        			);

        this.Body_Search = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase>.GetPropertyBySelector( item => item.Body_Search),
        			isEditable: false,
        			isVisible: false
        			);

        this.Remarks = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase>.GetPropertyBySelector( item => item.Remarks),
        			isEditable: true,
        			isVisible: true
        			);

        this.Identifier = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase>.GetPropertyBySelector( item => item.Identifier),
        			isEditable: true,
        			isVisible: true
        			);

        this.VisibleInCMS_AccessRequired = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase>.GetPropertyBySelector( item => item.VisibleInCMS_AccessRequired),
        			isEditable: true,
        			isVisible: true
        			);

        this.UsedInProject = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase>.GetPropertyBySelector( item => item.UsedInProject),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]

//InitFieldManyToManyBase_LeftSide
		this.ContentTags = this.AddManyToManyCollectionWithDirectProperty<BusinessLogic_v3.Cms.ContentTagModule.ContentTagBaseCmsInfo>(
		BusinessLogic_v3.Cms.ContentTagModule.ContentTagBaseCmsFactory.Instance, 
		CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase>.GetPropertyBySelector(item => item.GetContentTags()),
		((item) => ((BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase) item).ClearContentTags()),
            ((item, itemToAdd) => ((BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase) item).AddContentTagToContentTags((BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase)itemToAdd)),
		 Classes.Cms.EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);



			base.initBasicFields();
          
        }

    }
}
