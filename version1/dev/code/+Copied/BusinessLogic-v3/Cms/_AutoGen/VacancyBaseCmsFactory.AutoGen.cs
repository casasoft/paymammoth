using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.VacancyModule;
using BusinessLogic_v3.Cms.VacancyModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class VacancyBaseCmsFactory_AutoGen : CmsFactoryBase<VacancyBaseCmsInfo, VacancyBase>
    {
       
       public new static VacancyBaseCmsFactory Instance
	    {
	         get
	         {
                 return (VacancyBaseCmsFactory)CmsFactoryBase<VacancyBaseCmsInfo, VacancyBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = VacancyBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Vacancy.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Vacancy";

            this.QueryStringParamID = "VacancyId";

            cmsInfo.TitlePlural = "Vacancies";

            cmsInfo.TitleSingular =  "Vacancy";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public VacancyBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "Vacancy/";


        }
       
    }

}
