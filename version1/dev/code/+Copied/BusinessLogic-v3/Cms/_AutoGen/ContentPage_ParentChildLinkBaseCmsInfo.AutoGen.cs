using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ContentPage_ParentChildLinkModule;
using BusinessLogic_v3.Cms.ContentPage_ParentChildLinkModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ContentPage_ParentChildLinkBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ContentPage_ParentChildLinkModule.ContentPage_ParentChildLinkBase>
    {
		public ContentPage_ParentChildLinkBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ContentPage_ParentChildLinkModule.ContentPage_ParentChildLinkBase dbItem)
            : base(BusinessLogic_v3.Cms.ContentPage_ParentChildLinkModule.ContentPage_ParentChildLinkBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ContentPage_ParentChildLinkBase DbItem
        {
            get
            {
                return (ContentPage_ParentChildLinkBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Parent { get; private set; }

        public CmsPropertyInfo Child { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Parent = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPage_ParentChildLinkModule.ContentPage_ParentChildLinkBase>.GetPropertyBySelector( item => item.Parent),
        			isEditable: true,
        			isVisible: true
        			);

        this.Child = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPage_ParentChildLinkModule.ContentPage_ParentChildLinkBase>.GetPropertyBySelector( item => item.Child),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
