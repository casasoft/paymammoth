using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ShippingRatesGroupModule;
using BusinessLogic_v3.Cms.ShippingRatesGroupModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ShippingRatesGroupBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ShippingRatesGroupModule.ShippingRatesGroupBase>
    {
		public ShippingRatesGroupBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ShippingRatesGroupModule.ShippingRatesGroupBase dbItem)
            : base(BusinessLogic_v3.Cms.ShippingRatesGroupModule.ShippingRatesGroupBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ShippingRatesGroupBase DbItem
        {
            get
            {
                return (ShippingRatesGroupBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo CountriesApplicable { get; private set; }

        public CmsPropertyInfo ShippingMethod { get; private set; }

        public CmsPropertyInfo AppliesToRestOfTheWorld { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ShippingRatesGroupModule.ShippingRatesGroupBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.CountriesApplicable = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ShippingRatesGroupModule.ShippingRatesGroupBase>.GetPropertyBySelector( item => item.CountriesApplicable),
        			isEditable: true,
        			isVisible: true
        			);

        this.ShippingMethod = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ShippingRatesGroupModule.ShippingRatesGroupBase>.GetPropertyBySelector( item => item.ShippingMethod),
        			isEditable: true,
        			isVisible: true
        			);

        this.AppliesToRestOfTheWorld = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ShippingRatesGroupModule.ShippingRatesGroupBase>.GetPropertyBySelector( item => item.AppliesToRestOfTheWorld),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
