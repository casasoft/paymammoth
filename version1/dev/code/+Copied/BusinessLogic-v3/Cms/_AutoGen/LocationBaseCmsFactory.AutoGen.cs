using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.LocationModule;
using BusinessLogic_v3.Cms.LocationModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class LocationBaseCmsFactory_AutoGen : CmsFactoryBase<LocationBaseCmsInfo, LocationBase>
    {
       
       public new static LocationBaseCmsFactory Instance
	    {
	         get
	         {
                 return (LocationBaseCmsFactory)CmsFactoryBase<LocationBaseCmsInfo, LocationBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = LocationBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Location.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Location";

            this.QueryStringParamID = "LocationId";

            cmsInfo.TitlePlural = "Locations";

            cmsInfo.TitleSingular =  "Location";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public LocationBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "Location/";


        }
       
    }

}
