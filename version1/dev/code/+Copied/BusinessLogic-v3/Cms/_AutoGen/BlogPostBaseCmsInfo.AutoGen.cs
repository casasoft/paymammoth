using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.BlogPostModule;
using BusinessLogic_v3.Cms.BlogPostModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class BlogPostBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.BlogPostModule.BlogPostBase>
    {
		public BlogPostBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.BlogPostModule.BlogPostBase dbItem)
            : base(BusinessLogic_v3.Cms.BlogPostModule.BlogPostBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new BlogPostBase DbItem
        {
            get
            {
                return (BlogPostBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo CreationDate { get; private set; }

        public CmsPropertyInfo HtmlText { get; private set; }

        public CmsPropertyInfo ShortDescription { get; private set; }

        public CmsPropertyInfo ParentBlog { get; private set; }

        public CmsPropertyInfo _LastEditedBy { get; private set; }

        public CmsPropertyInfo _LastEditedOn { get; private set; }

        public CmsPropertyInfo Tags { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BlogPostModule.BlogPostBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.CreationDate = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BlogPostModule.BlogPostBase>.GetPropertyBySelector( item => item.CreationDate),
        			isEditable: true,
        			isVisible: true
        			);

        this.HtmlText = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BlogPostModule.BlogPostBase>.GetPropertyBySelector( item => item.HtmlText),
        			isEditable: true,
        			isVisible: true
        			);

        this.ShortDescription = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BlogPostModule.BlogPostBase>.GetPropertyBySelector( item => item.ShortDescription),
        			isEditable: true,
        			isVisible: true
        			);

        this.ParentBlog = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BlogPostModule.BlogPostBase>.GetPropertyBySelector( item => item.ParentBlog),
        			isEditable: true,
        			isVisible: true
        			);

        this._LastEditedBy = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BlogPostModule.BlogPostBase>.GetPropertyBySelector( item => item._LastEditedBy),
        			isEditable: true,
        			isVisible: true
        			);

        this._LastEditedOn = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BlogPostModule.BlogPostBase>.GetPropertyBySelector( item => item._LastEditedOn),
        			isEditable: true,
        			isVisible: true
        			);

        this.Tags = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BlogPostModule.BlogPostBase>.GetPropertyBySelector( item => item.Tags),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
