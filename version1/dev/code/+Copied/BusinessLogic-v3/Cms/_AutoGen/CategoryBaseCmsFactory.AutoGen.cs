using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.CategoryModule;
using BusinessLogic_v3.Cms.CategoryModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class CategoryBaseCmsFactory_AutoGen : CmsFactoryBase<CategoryBaseCmsInfo, CategoryBase>
    {
       
       public new static CategoryBaseCmsFactory Instance
	    {
	         get
	         {
                 return (CategoryBaseCmsFactory)CmsFactoryBase<CategoryBaseCmsInfo, CategoryBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = CategoryBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Category.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Category";

            this.QueryStringParamID = "CategoryId";

            cmsInfo.TitlePlural = "Categories";

            cmsInfo.TitleSingular =  "Category";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public CategoryBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "Category/";


        }
       
    }

}
