using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.MemberModule;
using BusinessLogic_v3.Cms.MemberModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class MemberBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.MemberModule.MemberBase>
    {
		public MemberBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.MemberModule.MemberBase dbItem)
            : base(BusinessLogic_v3.Cms.MemberModule.MemberBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new MemberBase DbItem
        {
            get
            {
                return (MemberBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo DateRegistered { get; private set; }

        public CmsPropertyInfo Username { get; private set; }

        public CmsPropertyInfo Password { get; private set; }

        public CmsPropertyInfo Name { get; private set; }

        public CmsPropertyInfo Surname { get; private set; }

        public CmsPropertyInfo Gender { get; private set; }

        public CmsPropertyInfo DateOfBirth { get; private set; }

        public CmsPropertyInfo Address1 { get; private set; }

        public CmsPropertyInfo Address2 { get; private set; }

        public CmsPropertyInfo Address3 { get; private set; }

        public CmsPropertyInfo City { get; private set; }

        public CmsPropertyInfo State { get; private set; }

        public CmsPropertyInfo ZIPCode { get; private set; }

        public CmsPropertyInfo CountryStr { get; private set; }

        public CmsPropertyInfo IDCard { get; private set; }

        public CmsPropertyInfo LastLoggedIn { get; private set; }

        public CmsPropertyInfo _LastLoggedIn_New { get; private set; }

        public CmsPropertyInfo Accepted { get; private set; }

        public CmsPropertyInfo Email { get; private set; }

        public CmsPropertyInfo Telephone { get; private set; }

        public CmsPropertyInfo Mobile { get; private set; }

        public CmsPropertyInfo Fax { get; private set; }

        public CmsPropertyInfo Remarks { get; private set; }

        public CmsPropertyInfo Website { get; private set; }

        public CmsPropertyInfo Activated { get; private set; }

        public CmsPropertyInfo ActivatedOn { get; private set; }

        public CmsPropertyInfo ActivatedIP { get; private set; }

        public CmsPropertyInfo ActivationCode { get; private set; }

        public CmsPropertyInfo SessionGUID { get; private set; }

        public CmsPropertyInfo PreferredCultureInfo { get; private set; }

        public CmsPropertyInfo ReferredByMember { get; private set; }

        public CmsPropertyInfo AccountTerminated { get; private set; }

        public CmsPropertyInfo AccountTerminatedOn { get; private set; }

        public CmsPropertyInfo AccountTerminatedIP { get; private set; }

        public CmsPropertyInfo ForgottenPassCode { get; private set; }

        public CmsPropertyInfo SubscribedToNewsletter { get; private set; }

        public CmsPropertyInfo CurrentShoppingCart { get; private set; }

        public CmsPropertyInfo HowDidYouFindUs { get; private set; }

        public CmsPropertyInfo LinkedAffiliate { get; private set; }

        public CmsPropertyInfo CompanyName { get; private set; }

        public CmsPropertyInfo PasswordEncryptionType { get; private set; }

        public CmsPropertyInfo PasswordSalt { get; private set; }

        public CmsPropertyInfo PasswordIterations { get; private set; }

        public CmsPropertyInfo CountryCode { get; private set; }

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo CurrentSubscription { get; private set; }

        public CmsPropertyInfo CurrentSubscriptionStartDate { get; private set; }

        public CmsPropertyInfo CurrentSubscriptionEndDate { get; private set; }

        public CmsPropertyInfo SubscriptionNotification1Sent { get; private set; }

        public CmsPropertyInfo SubscriptionNotification2Sent { get; private set; }

        public CmsPropertyInfo Paid { get; private set; }



// [basecmsinfo_manytomanydeclarations]

        public CmsPropertyManyToManyCollection<BusinessLogic_v3.Cms.CategoryModule.CategoryBaseCmsInfo> SubscribedCategories { get; private set; }



        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.DateRegistered = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.DateRegistered),
        			isEditable: true,
        			isVisible: true
        			);

        this.Username = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.Username),
        			isEditable: true,
        			isVisible: true
        			);

        this.Password = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.Password),
        			isEditable: true,
        			isVisible: true
        			);

        this.Name = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.Name),
        			isEditable: true,
        			isVisible: true
        			);

        this.Surname = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.Surname),
        			isEditable: true,
        			isVisible: true
        			);

        this.Gender = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.Gender),
        			isEditable: true,
        			isVisible: true
        			);

        this.DateOfBirth = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.DateOfBirth),
        			isEditable: true,
        			isVisible: true
        			);

        this.Address1 = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.Address1),
        			isEditable: true,
        			isVisible: true
        			);

        this.Address2 = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.Address2),
        			isEditable: true,
        			isVisible: true
        			);

        this.Address3 = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.Address3),
        			isEditable: true,
        			isVisible: true
        			);

        this.City = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.City),
        			isEditable: true,
        			isVisible: true
        			);

        this.State = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.State),
        			isEditable: true,
        			isVisible: true
        			);

        this.ZIPCode = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.ZIPCode),
        			isEditable: true,
        			isVisible: true
        			);

        this.CountryStr = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.CountryStr),
        			isEditable: true,
        			isVisible: true
        			);

        this.IDCard = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.IDCard),
        			isEditable: true,
        			isVisible: true
        			);

        this.LastLoggedIn = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.LastLoggedIn),
        			isEditable: true,
        			isVisible: true
        			);

        this._LastLoggedIn_New = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item._LastLoggedIn_New),
        			isEditable: true,
        			isVisible: true
        			);

        this.Accepted = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.Accepted),
        			isEditable: true,
        			isVisible: true
        			);

        this.Email = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.Email),
        			isEditable: true,
        			isVisible: true
        			);

        this.Telephone = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.Telephone),
        			isEditable: true,
        			isVisible: true
        			);

        this.Mobile = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.Mobile),
        			isEditable: true,
        			isVisible: true
        			);

        this.Fax = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.Fax),
        			isEditable: true,
        			isVisible: true
        			);

        this.Remarks = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.Remarks),
        			isEditable: true,
        			isVisible: true
        			);

        this.Website = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.Website),
        			isEditable: true,
        			isVisible: true
        			);

        this.Activated = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.Activated),
        			isEditable: true,
        			isVisible: true
        			);

        this.ActivatedOn = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.ActivatedOn),
        			isEditable: true,
        			isVisible: true
        			);

        this.ActivatedIP = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.ActivatedIP),
        			isEditable: true,
        			isVisible: true
        			);

        this.ActivationCode = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.ActivationCode),
        			isEditable: true,
        			isVisible: true
        			);

        this.SessionGUID = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.SessionGUID),
        			isEditable: true,
        			isVisible: true
        			);

        this.PreferredCultureInfo = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.PreferredCultureInfo),
        			isEditable: true,
        			isVisible: true
        			);

        this.ReferredByMember = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.ReferredByMember),
        			isEditable: true,
        			isVisible: true
        			);

        this.AccountTerminated = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.AccountTerminated),
        			isEditable: true,
        			isVisible: true
        			);

        this.AccountTerminatedOn = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.AccountTerminatedOn),
        			isEditable: true,
        			isVisible: true
        			);

        this.AccountTerminatedIP = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.AccountTerminatedIP),
        			isEditable: true,
        			isVisible: true
        			);

        this.ForgottenPassCode = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.ForgottenPassCode),
        			isEditable: true,
        			isVisible: true
        			);

        this.SubscribedToNewsletter = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.SubscribedToNewsletter),
        			isEditable: true,
        			isVisible: true
        			);

        this.CurrentShoppingCart = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.CurrentShoppingCart),
        			isEditable: true,
        			isVisible: true
        			);

        this.HowDidYouFindUs = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.HowDidYouFindUs),
        			isEditable: true,
        			isVisible: true
        			);

        this.LinkedAffiliate = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.LinkedAffiliate),
        			isEditable: true,
        			isVisible: true
        			);

        this.CompanyName = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.CompanyName),
        			isEditable: true,
        			isVisible: true
        			);

        this.PasswordEncryptionType = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.PasswordEncryptionType),
        			isEditable: true,
        			isVisible: true
        			);

        this.PasswordSalt = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.PasswordSalt),
        			isEditable: true,
        			isVisible: true
        			);

        this.PasswordIterations = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.PasswordIterations),
        			isEditable: true,
        			isVisible: true
        			);

        this.CountryCode = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.CountryCode),
        			isEditable: true,
        			isVisible: true
        			);

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.CurrentSubscription = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.CurrentSubscription),
        			isEditable: true,
        			isVisible: true
        			);

        this.CurrentSubscriptionStartDate = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.CurrentSubscriptionStartDate),
        			isEditable: true,
        			isVisible: true
        			);

        this.CurrentSubscriptionEndDate = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.CurrentSubscriptionEndDate),
        			isEditable: true,
        			isVisible: true
        			);

        this.SubscriptionNotification1Sent = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.SubscriptionNotification1Sent),
        			isEditable: true,
        			isVisible: true
        			);

        this.SubscriptionNotification2Sent = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.SubscriptionNotification2Sent),
        			isEditable: true,
        			isVisible: true
        			);

        this.Paid = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector( item => item.Paid),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]

//InitFieldManyToManyBase_LeftSide
		this.SubscribedCategories = this.AddManyToManyCollectionWithDirectProperty<BusinessLogic_v3.Cms.CategoryModule.CategoryBaseCmsInfo>(
		BusinessLogic_v3.Cms.CategoryModule.CategoryBaseCmsFactory.Instance, 
		CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberModule.MemberBase>.GetPropertyBySelector(item => item.GetSubscribedCategories()),
		((item) => ((BusinessLogic_v3.Modules.MemberModule.MemberBase) item).ClearSubscribedCategories()),
            ((item, itemToAdd) => ((BusinessLogic_v3.Modules.MemberModule.MemberBase) item).AddCategoryToSubscribedCategories((BusinessLogic_v3.Modules.CategoryModule.CategoryBase)itemToAdd)),
		 Classes.Cms.EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);



			base.initBasicFields();
          
        }

    }
}
