using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.FrontpageBannerModule;
using BusinessLogic_v3.Cms.FrontpageBannerModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class FrontpageBannerBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.FrontpageBannerModule.FrontpageBannerBase>
    {
		public FrontpageBannerBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.FrontpageBannerModule.FrontpageBannerBase dbItem)
            : base(BusinessLogic_v3.Cms.FrontpageBannerModule.FrontpageBannerBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new FrontpageBannerBase DbItem
        {
            get
            {
                return (FrontpageBannerBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo Activated { get; private set; }

        public CmsPropertyInfo ImageFilename { get; private set; }

        public CmsPropertyInfo Description { get; private set; }

        public CmsPropertyInfo Name { get; private set; }

        public CmsPropertyInfo ExternalURL { get; private set; }

        public CmsPropertyInfo FrontpageBannerURL { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.FrontpageBannerModule.FrontpageBannerBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.Activated = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.FrontpageBannerModule.FrontpageBannerBase>.GetPropertyBySelector( item => item.Activated),
        			isEditable: true,
        			isVisible: true
        			);

        this.ImageFilename = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.FrontpageBannerModule.FrontpageBannerBase>.GetPropertyBySelector( item => item.ImageFilename),
        			isEditable: false,
        			isVisible: false
        			);

        this.Description = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.FrontpageBannerModule.FrontpageBannerBase>.GetPropertyBySelector( item => item.Description),
        			isEditable: true,
        			isVisible: true
        			);

        this.Name = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.FrontpageBannerModule.FrontpageBannerBase>.GetPropertyBySelector( item => item.Name),
        			isEditable: true,
        			isVisible: true
        			);

        this.ExternalURL = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.FrontpageBannerModule.FrontpageBannerBase>.GetPropertyBySelector( item => item.ExternalURL),
        			isEditable: true,
        			isVisible: true
        			);

        this.FrontpageBannerURL = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.FrontpageBannerModule.FrontpageBannerBase>.GetPropertyBySelector( item => item.FrontpageBannerURL),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
