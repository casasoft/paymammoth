using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ContentText_CultureInfoModule;
using BusinessLogic_v3.Cms.ContentText_CultureInfoModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ContentText_CultureInfoBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoBase>
    {
		public ContentText_CultureInfoBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoBase dbItem)
            : base(BusinessLogic_v3.Cms.ContentText_CultureInfoModule.ContentText_CultureInfoBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ContentText_CultureInfoBase DbItem
        {
            get
            {
                return (ContentText_CultureInfoBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo CultureInfo { get; private set; }

        public CmsPropertyInfo ContentText { get; private set; }

        public CmsPropertyInfo ContentPlain { get; private set; }

        public CmsPropertyInfo ContentHTML { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.CultureInfo = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoBase>.GetPropertyBySelector( item => item.CultureInfo),
        			isEditable: true,
        			isVisible: true
        			);

        this.ContentText = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoBase>.GetPropertyBySelector( item => item.ContentText),
        			isEditable: true,
        			isVisible: true
        			);

        this.ContentPlain = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoBase>.GetPropertyBySelector( item => item.ContentPlain),
        			isEditable: true,
        			isVisible: true
        			);

        this.ContentHTML = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoBase>.GetPropertyBySelector( item => item.ContentHTML),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
