using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.EventMediaItemModule;
using BusinessLogic_v3.Cms.EventMediaItemModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class EventMediaItemBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.EventMediaItemModule.EventMediaItemBase>
    {
		public EventMediaItemBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.EventMediaItemModule.EventMediaItemBase dbItem)
            : base(BusinessLogic_v3.Cms.EventMediaItemModule.EventMediaItemBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new EventMediaItemBase DbItem
        {
            get
            {
                return (EventMediaItemBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo ImageFilename { get; private set; }

        public CmsPropertyInfo Event { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventMediaItemModule.EventMediaItemBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.ImageFilename = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventMediaItemModule.EventMediaItemBase>.GetPropertyBySelector( item => item.ImageFilename),
        			isEditable: false,
        			isVisible: false
        			);

        this.Event = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventMediaItemModule.EventMediaItemBase>.GetPropertyBySelector( item => item.Event),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
