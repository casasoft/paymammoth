using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ContentPage_RelatedLinkModule;
using BusinessLogic_v3.Cms.ContentPage_RelatedLinkModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ContentPage_RelatedLinkBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ContentPage_RelatedLinkModule.ContentPage_RelatedLinkBase>
    {
		public ContentPage_RelatedLinkBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ContentPage_RelatedLinkModule.ContentPage_RelatedLinkBase dbItem)
            : base(BusinessLogic_v3.Cms.ContentPage_RelatedLinkModule.ContentPage_RelatedLinkBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ContentPage_RelatedLinkBase DbItem
        {
            get
            {
                return (ContentPage_RelatedLinkBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo ParentPage { get; private set; }

        public CmsPropertyInfo RelatedPage { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.ParentPage = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPage_RelatedLinkModule.ContentPage_RelatedLinkBase>.GetPropertyBySelector( item => item.ParentPage),
        			isEditable: true,
        			isVisible: true
        			);

        this.RelatedPage = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPage_RelatedLinkModule.ContentPage_RelatedLinkBase>.GetPropertyBySelector( item => item.RelatedPage),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
