using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.EventModule;
using BusinessLogic_v3.Cms.EventModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class EventBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.EventModule.EventBase>
    {
		public EventBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.EventModule.EventBase dbItem)
            : base(BusinessLogic_v3.Cms.EventModule.EventBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new EventBase DbItem
        {
            get
            {
                return (EventBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo Description { get; private set; }

        public CmsPropertyInfo StartDate { get; private set; }

        public CmsPropertyInfo EndDate { get; private set; }

        public CmsPropertyInfo ImageFilename { get; private set; }

        public CmsPropertyInfo EventCategory { get; private set; }

        public CmsPropertyInfo Category { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventModule.EventBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.Description = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventModule.EventBase>.GetPropertyBySelector( item => item.Description),
        			isEditable: true,
        			isVisible: true
        			);

        this.StartDate = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventModule.EventBase>.GetPropertyBySelector( item => item.StartDate),
        			isEditable: true,
        			isVisible: true
        			);

        this.EndDate = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventModule.EventBase>.GetPropertyBySelector( item => item.EndDate),
        			isEditable: true,
        			isVisible: true
        			);

        this.ImageFilename = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventModule.EventBase>.GetPropertyBySelector( item => item.ImageFilename),
        			isEditable: false,
        			isVisible: false
        			);

        this.EventCategory = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventModule.EventBase>.GetPropertyBySelector( item => item.EventCategory),
        			isEditable: true,
        			isVisible: true
        			);

        this.Category = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventModule.EventBase>.GetPropertyBySelector( item => item.Category),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
