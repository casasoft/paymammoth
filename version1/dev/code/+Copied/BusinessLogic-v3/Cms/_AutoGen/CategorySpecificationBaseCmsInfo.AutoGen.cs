using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.CategorySpecificationModule;
using BusinessLogic_v3.Cms.CategorySpecificationModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class CategorySpecificationBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.CategorySpecificationModule.CategorySpecificationBase>
    {
		public CategorySpecificationBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.CategorySpecificationModule.CategorySpecificationBase dbItem)
            : base(BusinessLogic_v3.Cms.CategorySpecificationModule.CategorySpecificationBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new CategorySpecificationBase DbItem
        {
            get
            {
                return (CategorySpecificationBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo DataType { get; private set; }

        public CmsPropertyInfo MeasurementUnit { get; private set; }

        public CmsPropertyInfo Category { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategorySpecificationModule.CategorySpecificationBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.DataType = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategorySpecificationModule.CategorySpecificationBase>.GetPropertyBySelector( item => item.DataType),
        			isEditable: true,
        			isVisible: true
        			);

        this.MeasurementUnit = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategorySpecificationModule.CategorySpecificationBase>.GetPropertyBySelector( item => item.MeasurementUnit),
        			isEditable: true,
        			isVisible: true
        			);

        this.Category = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategorySpecificationModule.CategorySpecificationBase>.GetPropertyBySelector( item => item.Category),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
