using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.AffiliatePaymentInfoModule;
using BusinessLogic_v3.Cms.AffiliatePaymentInfoModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class AffiliatePaymentInfoBaseCmsFactory_AutoGen : CmsFactoryBase<AffiliatePaymentInfoBaseCmsInfo, AffiliatePaymentInfoBase>
    {
       
       public new static AffiliatePaymentInfoBaseCmsFactory Instance
	    {
	         get
	         {
                 return (AffiliatePaymentInfoBaseCmsFactory)CmsFactoryBase<AffiliatePaymentInfoBaseCmsInfo, AffiliatePaymentInfoBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = AffiliatePaymentInfoBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "AffiliatePaymentInfo.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "AffiliatePaymentInfo";

            this.QueryStringParamID = "AffiliatePaymentInfoId";

            cmsInfo.TitlePlural = "Affiliate Payment Infos";

            cmsInfo.TitleSingular =  "Affiliate Payment Info";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public AffiliatePaymentInfoBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "AffiliatePaymentInfo/";


        }
       
    }

}
