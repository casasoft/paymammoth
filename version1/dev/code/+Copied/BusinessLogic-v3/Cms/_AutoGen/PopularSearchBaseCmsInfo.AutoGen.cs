using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.PopularSearchModule;
using BusinessLogic_v3.Cms.PopularSearchModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class PopularSearchBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.PopularSearchModule.PopularSearchBase>
    {
		public PopularSearchBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.PopularSearchModule.PopularSearchBase dbItem)
            : base(BusinessLogic_v3.Cms.PopularSearchModule.PopularSearchBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new PopularSearchBase DbItem
        {
            get
            {
                return (PopularSearchBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo SearchURL { get; private set; }

        public CmsPropertyInfo Activated { get; private set; }

        public CmsPropertyInfo HtmlText { get; private set; }

        public CmsPropertyInfo HideFromFrontend { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.PopularSearchModule.PopularSearchBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.SearchURL = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.PopularSearchModule.PopularSearchBase>.GetPropertyBySelector( item => item.SearchURL),
        			isEditable: true,
        			isVisible: true
        			);

        this.Activated = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.PopularSearchModule.PopularSearchBase>.GetPropertyBySelector( item => item.Activated),
        			isEditable: true,
        			isVisible: true
        			);

        this.HtmlText = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.PopularSearchModule.PopularSearchBase>.GetPropertyBySelector( item => item.HtmlText),
        			isEditable: true,
        			isVisible: true
        			);

        this.HideFromFrontend = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.PopularSearchModule.PopularSearchBase>.GetPropertyBySelector( item => item.HideFromFrontend),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
