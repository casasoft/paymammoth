using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.EventSessionModule;
using BusinessLogic_v3.Cms.EventSessionModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class EventSessionBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase>
    {
		public EventSessionBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase dbItem)
            : base(BusinessLogic_v3.Cms.EventSessionModule.EventSessionBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new EventSessionBase DbItem
        {
            get
            {
                return (EventSessionBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Event { get; private set; }

        public CmsPropertyInfo Description { get; private set; }

        public CmsPropertyInfo Location { get; private set; }

        public CmsPropertyInfo StartDate { get; private set; }

        public CmsPropertyInfo EndDate { get; private set; }

        public CmsPropertyInfo Cost { get; private set; }

        public CmsPropertyInfo SlotsAvailable { get; private set; }

        public CmsPropertyInfo SlotsTaken { get; private set; }

        public CmsPropertyInfo Title { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Event = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase>.GetPropertyBySelector( item => item.Event),
        			isEditable: true,
        			isVisible: true
        			);

        this.Description = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase>.GetPropertyBySelector( item => item.Description),
        			isEditable: true,
        			isVisible: true
        			);

        this.Location = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase>.GetPropertyBySelector( item => item.Location),
        			isEditable: true,
        			isVisible: true
        			);

        this.StartDate = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase>.GetPropertyBySelector( item => item.StartDate),
        			isEditable: true,
        			isVisible: true
        			);

        this.EndDate = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase>.GetPropertyBySelector( item => item.EndDate),
        			isEditable: true,
        			isVisible: true
        			);

        this.Cost = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase>.GetPropertyBySelector( item => item.Cost),
        			isEditable: true,
        			isVisible: true
        			);

        this.SlotsAvailable = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase>.GetPropertyBySelector( item => item.SlotsAvailable),
        			isEditable: true,
        			isVisible: true
        			);

        this.SlotsTaken = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase>.GetPropertyBySelector( item => item.SlotsTaken),
        			isEditable: true,
        			isVisible: true
        			);

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventSessionModule.EventSessionBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
