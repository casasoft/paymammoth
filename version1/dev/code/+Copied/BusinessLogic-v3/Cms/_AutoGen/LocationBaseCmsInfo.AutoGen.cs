using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.LocationModule;
using BusinessLogic_v3.Cms.LocationModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class LocationBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.LocationModule.LocationBase>
    {
		public LocationBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.LocationModule.LocationBase dbItem)
            : base(BusinessLogic_v3.Cms.LocationModule.LocationBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new LocationBase DbItem
        {
            get
            {
                return (LocationBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo ReferenceID { get; private set; }

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo LocationType { get; private set; }

        public CmsPropertyInfo ParentLocation { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.ReferenceID = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.LocationModule.LocationBase>.GetPropertyBySelector( item => item.ReferenceID),
        			isEditable: true,
        			isVisible: true
        			);

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.LocationModule.LocationBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.LocationType = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.LocationModule.LocationBase>.GetPropertyBySelector( item => item.LocationType),
        			isEditable: true,
        			isVisible: true
        			);

        this.ParentLocation = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.LocationModule.LocationBase>.GetPropertyBySelector( item => item.ParentLocation),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
