using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ItemModule;
using BusinessLogic_v3.Cms.ItemModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ItemBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ItemModule.ItemBase>
    {
		public ItemBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ItemModule.ItemBase dbItem)
            : base(BusinessLogic_v3.Cms.ItemModule.ItemBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ItemBase DbItem
        {
            get
            {
                return (ItemBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo ReferenceCode { get; private set; }

        public CmsPropertyInfo SupplierRefCode { get; private set; }

        public CmsPropertyInfo ItemGroup { get; private set; }

        public CmsPropertyInfo Barcode { get; private set; }

        public CmsPropertyInfo Colour { get; private set; }

        public CmsPropertyInfo Size { get; private set; }

        public CmsPropertyInfo Quantity { get; private set; }

        public CmsPropertyInfo ReorderAmount { get; private set; }

        public CmsPropertyInfo Activated { get; private set; }

        public CmsPropertyInfo ImportReference { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.ReferenceCode = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemModule.ItemBase>.GetPropertyBySelector( item => item.ReferenceCode),
        			isEditable: true,
        			isVisible: true
        			);

        this.SupplierRefCode = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemModule.ItemBase>.GetPropertyBySelector( item => item.SupplierRefCode),
        			isEditable: true,
        			isVisible: true
        			);

        this.ItemGroup = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemModule.ItemBase>.GetPropertyBySelector( item => item.ItemGroup),
        			isEditable: true,
        			isVisible: true
        			);

        this.Barcode = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemModule.ItemBase>.GetPropertyBySelector( item => item.Barcode),
        			isEditable: true,
        			isVisible: true
        			);

        this.Colour = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemModule.ItemBase>.GetPropertyBySelector( item => item.Colour),
        			isEditable: true,
        			isVisible: true
        			);

        this.Size = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemModule.ItemBase>.GetPropertyBySelector( item => item.Size),
        			isEditable: true,
        			isVisible: true
        			);

        this.Quantity = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemModule.ItemBase>.GetPropertyBySelector( item => item.Quantity),
        			isEditable: true,
        			isVisible: true
        			);

        this.ReorderAmount = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemModule.ItemBase>.GetPropertyBySelector( item => item.ReorderAmount),
        			isEditable: true,
        			isVisible: true
        			);

        this.Activated = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemModule.ItemBase>.GetPropertyBySelector( item => item.Activated),
        			isEditable: true,
        			isVisible: true
        			);

        this.ImportReference = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemModule.ItemBase>.GetPropertyBySelector( item => item.ImportReference),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
