using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.EventSessionPeriodModule;
using BusinessLogic_v3.Cms.EventSessionPeriodModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class EventSessionPeriodBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.EventSessionPeriodModule.EventSessionPeriodBase>
    {
		public EventSessionPeriodBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.EventSessionPeriodModule.EventSessionPeriodBase dbItem)
            : base(BusinessLogic_v3.Cms.EventSessionPeriodModule.EventSessionPeriodBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new EventSessionPeriodBase DbItem
        {
            get
            {
                return (EventSessionPeriodBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo EventSession { get; private set; }

        public CmsPropertyInfo StartDate { get; private set; }

        public CmsPropertyInfo EndDate { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.EventSession = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventSessionPeriodModule.EventSessionPeriodBase>.GetPropertyBySelector( item => item.EventSession),
        			isEditable: true,
        			isVisible: true
        			);

        this.StartDate = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventSessionPeriodModule.EventSessionPeriodBase>.GetPropertyBySelector( item => item.StartDate),
        			isEditable: true,
        			isVisible: true
        			);

        this.EndDate = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventSessionPeriodModule.EventSessionPeriodBase>.GetPropertyBySelector( item => item.EndDate),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
