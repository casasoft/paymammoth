using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.ItemGroupCategorySpecificationValueModule;
using BusinessLogic_v3.Cms.ItemGroupCategorySpecificationValueModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class ItemGroupCategorySpecificationValueBaseCmsFactory_AutoGen : CmsFactoryBase<ItemGroupCategorySpecificationValueBaseCmsInfo, ItemGroupCategorySpecificationValueBase>
    {
       
       public new static ItemGroupCategorySpecificationValueBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ItemGroupCategorySpecificationValueBaseCmsFactory)CmsFactoryBase<ItemGroupCategorySpecificationValueBaseCmsInfo, ItemGroupCategorySpecificationValueBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = ItemGroupCategorySpecificationValueBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ItemGroupCategorySpecificationValue.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ItemGroupCategorySpecificationValue";

            this.QueryStringParamID = "ItemGroupCategorySpecificationValueId";

            cmsInfo.TitlePlural = "Item Group Category Specification Values";

            cmsInfo.TitleSingular =  "Item Group Category Specification Value";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ItemGroupCategorySpecificationValueBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "ItemGroupCategorySpecificationValue/";


        }
       
    }

}
