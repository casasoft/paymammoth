using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.CategorySpecificationModule;
using BusinessLogic_v3.Cms.CategorySpecificationModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class CategorySpecificationBaseCmsFactory_AutoGen : CmsFactoryBase<CategorySpecificationBaseCmsInfo, CategorySpecificationBase>
    {
       
       public new static CategorySpecificationBaseCmsFactory Instance
	    {
	         get
	         {
                 return (CategorySpecificationBaseCmsFactory)CmsFactoryBase<CategorySpecificationBaseCmsInfo, CategorySpecificationBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = CategorySpecificationBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "CategorySpecification.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "CategorySpecification";

            this.QueryStringParamID = "CategorySpecificationId";

            cmsInfo.TitlePlural = "Category Specifications";

            cmsInfo.TitleSingular =  "Category Specification";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public CategorySpecificationBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "CategorySpecification/";


        }
       
    }

}
