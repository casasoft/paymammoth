using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.OrderModule;
using BusinessLogic_v3.Cms.OrderModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class OrderBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.OrderModule.OrderBase>
    {
		public OrderBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.OrderModule.OrderBase dbItem)
            : base(BusinessLogic_v3.Cms.OrderModule.OrderBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new OrderBase DbItem
        {
            get
            {
                return (OrderBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo DateCreated { get; private set; }

        public CmsPropertyInfo Reference { get; private set; }

        public CmsPropertyInfo CustomerName { get; private set; }

        public CmsPropertyInfo CustomerEmail { get; private set; }

        public CmsPropertyInfo Member { get; private set; }

        public CmsPropertyInfo CustomerAddress1 { get; private set; }

        public CmsPropertyInfo CustomerAddress2 { get; private set; }

        public CmsPropertyInfo CustomerAddress3 { get; private set; }

        public CmsPropertyInfo AuthCode { get; private set; }

        public CmsPropertyInfo CustomerCountryStr { get; private set; }

        public CmsPropertyInfo CustomerZIPCode { get; private set; }

        public CmsPropertyInfo CustomerCity { get; private set; }

        public CmsPropertyInfo CustomerState { get; private set; }

        public CmsPropertyInfo Paid { get; private set; }

        public CmsPropertyInfo PaidOn { get; private set; }

        public CmsPropertyInfo Cancelled { get; private set; }

        public CmsPropertyInfo CancelledOn { get; private set; }

        public CmsPropertyInfo Remarks { get; private set; }

        public CmsPropertyInfo CreatedByAdmin { get; private set; }

        public CmsPropertyInfo GUID { get; private set; }

        public CmsPropertyInfo ItemID { get; private set; }

        public CmsPropertyInfo ItemType { get; private set; }

        public CmsPropertyInfo IsShoppingCart { get; private set; }

        public CmsPropertyInfo CustomerSurname { get; private set; }

        public CmsPropertyInfo ShippedOn { get; private set; }

        public CmsPropertyInfo ShipmentTrackingCode { get; private set; }

        public CmsPropertyInfo ShipmentType { get; private set; }

        public CmsPropertyInfo ShipmentRemarks { get; private set; }

        public CmsPropertyInfo IsConfirmed { get; private set; }

        public CmsPropertyInfo ShipmentTrackingUrl { get; private set; }

        public CmsPropertyInfo FixedDiscount { get; private set; }

        public CmsPropertyInfo OrderStatus { get; private set; }

        public CmsPropertyInfo TotalShippingCost { get; private set; }

        public CmsPropertyInfo LastUpdatedOn { get; private set; }

        public CmsPropertyInfo OrderCurrency { get; private set; }

        public CmsPropertyInfo ShippingMethod { get; private set; }

        public CmsPropertyInfo LinkedAffiliate { get; private set; }

        public CmsPropertyInfo AffiliateCommissionRate { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.DateCreated = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.DateCreated),
        			isEditable: true,
        			isVisible: true
        			);

        this.Reference = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.Reference),
        			isEditable: true,
        			isVisible: true
        			);

        this.CustomerName = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.CustomerName),
        			isEditable: true,
        			isVisible: true
        			);

        this.CustomerEmail = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.CustomerEmail),
        			isEditable: true,
        			isVisible: true
        			);

        this.Member = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.Member),
        			isEditable: true,
        			isVisible: true
        			);

        this.CustomerAddress1 = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.CustomerAddress1),
        			isEditable: true,
        			isVisible: true
        			);

        this.CustomerAddress2 = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.CustomerAddress2),
        			isEditable: true,
        			isVisible: true
        			);

        this.CustomerAddress3 = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.CustomerAddress3),
        			isEditable: true,
        			isVisible: true
        			);

        this.AuthCode = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.AuthCode),
        			isEditable: true,
        			isVisible: true
        			);

        this.CustomerCountryStr = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.CustomerCountryStr),
        			isEditable: true,
        			isVisible: true
        			);

        this.CustomerZIPCode = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.CustomerZIPCode),
        			isEditable: true,
        			isVisible: true
        			);

        this.CustomerCity = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.CustomerCity),
        			isEditable: true,
        			isVisible: true
        			);

        this.CustomerState = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.CustomerState),
        			isEditable: true,
        			isVisible: true
        			);

        this.Paid = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.Paid),
        			isEditable: true,
        			isVisible: true
        			);

        this.PaidOn = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.PaidOn),
        			isEditable: true,
        			isVisible: true
        			);

        this.Cancelled = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.Cancelled),
        			isEditable: true,
        			isVisible: true
        			);

        this.CancelledOn = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.CancelledOn),
        			isEditable: true,
        			isVisible: true
        			);

        this.Remarks = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.Remarks),
        			isEditable: true,
        			isVisible: true
        			);

        this.CreatedByAdmin = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.CreatedByAdmin),
        			isEditable: true,
        			isVisible: true
        			);

        this.GUID = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.GUID),
        			isEditable: true,
        			isVisible: true
        			);

        this.ItemID = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.ItemID),
        			isEditable: true,
        			isVisible: true
        			);

        this.ItemType = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.ItemType),
        			isEditable: true,
        			isVisible: true
        			);

        this.IsShoppingCart = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.IsShoppingCart),
        			isEditable: true,
        			isVisible: true
        			);

        this.CustomerSurname = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.CustomerSurname),
        			isEditable: true,
        			isVisible: true
        			);

        this.ShippedOn = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.ShippedOn),
        			isEditable: true,
        			isVisible: true
        			);

        this.ShipmentTrackingCode = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.ShipmentTrackingCode),
        			isEditable: true,
        			isVisible: true
        			);

        this.ShipmentType = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.ShipmentType),
        			isEditable: true,
        			isVisible: true
        			);

        this.ShipmentRemarks = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.ShipmentRemarks),
        			isEditable: true,
        			isVisible: true
        			);

        this.IsConfirmed = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.IsConfirmed),
        			isEditable: true,
        			isVisible: true
        			);

        this.ShipmentTrackingUrl = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.ShipmentTrackingUrl),
        			isEditable: true,
        			isVisible: true
        			);

        this.FixedDiscount = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.FixedDiscount),
        			isEditable: true,
        			isVisible: true
        			);

        this.OrderStatus = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.OrderStatus),
        			isEditable: true,
        			isVisible: true
        			);

        this.TotalShippingCost = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.TotalShippingCost),
        			isEditable: true,
        			isVisible: true
        			);

        this.LastUpdatedOn = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.LastUpdatedOn),
        			isEditable: true,
        			isVisible: true
        			);

        this.OrderCurrency = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.OrderCurrency),
        			isEditable: true,
        			isVisible: true
        			);

        this.ShippingMethod = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.ShippingMethod),
        			isEditable: true,
        			isVisible: true
        			);

        this.LinkedAffiliate = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.LinkedAffiliate),
        			isEditable: true,
        			isVisible: true
        			);

        this.AffiliateCommissionRate = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderModule.OrderBase>.GetPropertyBySelector( item => item.AffiliateCommissionRate),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
