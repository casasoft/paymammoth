using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ShippingMethodModule;
using BusinessLogic_v3.Cms.ShippingMethodModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ShippingMethodBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase>
    {
		public ShippingMethodBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase dbItem)
            : base(BusinessLogic_v3.Cms.ShippingMethodModule.ShippingMethodBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ShippingMethodBase DbItem
        {
            get
            {
                return (ShippingMethodBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo Description { get; private set; }

        public CmsPropertyInfo MinimumOrderAmount { get; private set; }

        public CmsPropertyInfo ExtraChargeAmountIfMinimumNotReached { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.Description = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase>.GetPropertyBySelector( item => item.Description),
        			isEditable: true,
        			isVisible: true
        			);

        this.MinimumOrderAmount = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase>.GetPropertyBySelector( item => item.MinimumOrderAmount),
        			isEditable: true,
        			isVisible: true
        			);

        this.ExtraChargeAmountIfMinimumNotReached = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ShippingMethodModule.ShippingMethodBase>.GetPropertyBySelector( item => item.ExtraChargeAmountIfMinimumNotReached),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
