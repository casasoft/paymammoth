using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.BannerModule;
using BusinessLogic_v3.Cms.BannerModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class BannerBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.BannerModule.BannerBase>
    {
		public BannerBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.BannerModule.BannerBase dbItem)
            : base(BusinessLogic_v3.Cms.BannerModule.BannerBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new BannerBase DbItem
        {
            get
            {
                return (BannerBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Identifier { get; private set; }

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo MediaItemURL { get; private set; }

        public CmsPropertyInfo Link { get; private set; }

        public CmsPropertyInfo DurationMS { get; private set; }

        public CmsPropertyInfo HrefTarget { get; private set; }

        public CmsPropertyInfo SlideshowDelaySec { get; private set; }

        public CmsPropertyInfo HTMLDescription { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Identifier = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BannerModule.BannerBase>.GetPropertyBySelector( item => item.Identifier),
        			isEditable: true,
        			isVisible: true
        			);

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BannerModule.BannerBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.MediaItemURL = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BannerModule.BannerBase>.GetPropertyBySelector( item => item.MediaItemURL),
        			isEditable: true,
        			isVisible: true
        			);

        this.Link = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BannerModule.BannerBase>.GetPropertyBySelector( item => item.Link),
        			isEditable: true,
        			isVisible: true
        			);

        this.DurationMS = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BannerModule.BannerBase>.GetPropertyBySelector( item => item.DurationMS),
        			isEditable: true,
        			isVisible: true
        			);

        this.HrefTarget = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BannerModule.BannerBase>.GetPropertyBySelector( item => item.HrefTarget),
        			isEditable: true,
        			isVisible: true
        			);

        this.SlideshowDelaySec = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BannerModule.BannerBase>.GetPropertyBySelector( item => item.SlideshowDelaySec),
        			isEditable: true,
        			isVisible: true
        			);

        this.HTMLDescription = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BannerModule.BannerBase>.GetPropertyBySelector( item => item.HTMLDescription),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
