using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.EventCategoryModule;
using BusinessLogic_v3.Cms.EventCategoryModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class EventCategoryBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.EventCategoryModule.EventCategoryBase>
    {
		public EventCategoryBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.EventCategoryModule.EventCategoryBase dbItem)
            : base(BusinessLogic_v3.Cms.EventCategoryModule.EventCategoryBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new EventCategoryBase DbItem
        {
            get
            {
                return (EventCategoryBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EventCategoryModule.EventCategoryBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
