using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.TransactionModule;
using BusinessLogic_v3.Cms.TransactionModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class TransactionBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.TransactionModule.TransactionBase>
    {
		public TransactionBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.TransactionModule.TransactionBase dbItem)
            : base(BusinessLogic_v3.Cms.TransactionModule.TransactionBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new TransactionBase DbItem
        {
            get
            {
                return (TransactionBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Date { get; private set; }

        public CmsPropertyInfo Order { get; private set; }

        public CmsPropertyInfo Details { get; private set; }

        public CmsPropertyInfo Amount { get; private set; }

        public CmsPropertyInfo Success { get; private set; }

        public CmsPropertyInfo AuthCode { get; private set; }

        public CmsPropertyInfo Remarks { get; private set; }

        public CmsPropertyInfo ItemID { get; private set; }

        public CmsPropertyInfo ItemType { get; private set; }

        public CmsPropertyInfo LinkedItem { get; private set; }

        public CmsPropertyInfo Member { get; private set; }

        public CmsPropertyInfo ExternalReference1 { get; private set; }

        public CmsPropertyInfo ExternalReference2 { get; private set; }

        public CmsPropertyInfo LastUpdated { get; private set; }

        public CmsPropertyInfo ErrorMsg { get; private set; }

        public CmsPropertyInfo InternalReference { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Date = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.TransactionModule.TransactionBase>.GetPropertyBySelector( item => item.Date),
        			isEditable: true,
        			isVisible: true
        			);

        this.Order = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.TransactionModule.TransactionBase>.GetPropertyBySelector( item => item.Order),
        			isEditable: true,
        			isVisible: true
        			);

        this.Details = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.TransactionModule.TransactionBase>.GetPropertyBySelector( item => item.Details),
        			isEditable: true,
        			isVisible: true
        			);

        this.Amount = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.TransactionModule.TransactionBase>.GetPropertyBySelector( item => item.Amount),
        			isEditable: true,
        			isVisible: true
        			);

        this.Success = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.TransactionModule.TransactionBase>.GetPropertyBySelector( item => item.Success),
        			isEditable: true,
        			isVisible: true
        			);

        this.AuthCode = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.TransactionModule.TransactionBase>.GetPropertyBySelector( item => item.AuthCode),
        			isEditable: true,
        			isVisible: true
        			);

        this.Remarks = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.TransactionModule.TransactionBase>.GetPropertyBySelector( item => item.Remarks),
        			isEditable: true,
        			isVisible: true
        			);

        this.ItemID = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.TransactionModule.TransactionBase>.GetPropertyBySelector( item => item.ItemID),
        			isEditable: true,
        			isVisible: true
        			);

        this.ItemType = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.TransactionModule.TransactionBase>.GetPropertyBySelector( item => item.ItemType),
        			isEditable: true,
        			isVisible: true
        			);

        this.LinkedItem = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.TransactionModule.TransactionBase>.GetPropertyBySelector( item => item.LinkedItem),
        			isEditable: true,
        			isVisible: true
        			);

        this.Member = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.TransactionModule.TransactionBase>.GetPropertyBySelector( item => item.Member),
        			isEditable: true,
        			isVisible: true
        			);

        this.ExternalReference1 = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.TransactionModule.TransactionBase>.GetPropertyBySelector( item => item.ExternalReference1),
        			isEditable: true,
        			isVisible: true
        			);

        this.ExternalReference2 = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.TransactionModule.TransactionBase>.GetPropertyBySelector( item => item.ExternalReference2),
        			isEditable: true,
        			isVisible: true
        			);

        this.LastUpdated = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.TransactionModule.TransactionBase>.GetPropertyBySelector( item => item.LastUpdated),
        			isEditable: true,
        			isVisible: true
        			);

        this.ErrorMsg = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.TransactionModule.TransactionBase>.GetPropertyBySelector( item => item.ErrorMsg),
        			isEditable: true,
        			isVisible: true
        			);

        this.InternalReference = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.TransactionModule.TransactionBase>.GetPropertyBySelector( item => item.InternalReference),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
