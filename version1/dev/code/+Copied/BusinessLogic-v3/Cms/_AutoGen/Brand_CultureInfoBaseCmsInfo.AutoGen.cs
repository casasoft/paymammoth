using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.Brand_CultureInfoModule;
using BusinessLogic_v3.Cms.Brand_CultureInfoModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class Brand_CultureInfoBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.Brand_CultureInfoModule.Brand_CultureInfoBase>
    {
		public Brand_CultureInfoBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.Brand_CultureInfoModule.Brand_CultureInfoBase dbItem)
            : base(BusinessLogic_v3.Cms.Brand_CultureInfoModule.Brand_CultureInfoBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new Brand_CultureInfoBase DbItem
        {
            get
            {
                return (Brand_CultureInfoBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo CultureInfo { get; private set; }

        public CmsPropertyInfo Brand { get; private set; }

        public CmsPropertyInfo Description { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.CultureInfo = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Brand_CultureInfoModule.Brand_CultureInfoBase>.GetPropertyBySelector( item => item.CultureInfo),
        			isEditable: true,
        			isVisible: true
        			);

        this.Brand = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Brand_CultureInfoModule.Brand_CultureInfoBase>.GetPropertyBySelector( item => item.Brand),
        			isEditable: true,
        			isVisible: true
        			);

        this.Description = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Brand_CultureInfoModule.Brand_CultureInfoBase>.GetPropertyBySelector( item => item.Description),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
