using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.VacancyModule;
using BusinessLogic_v3.Cms.VacancyModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class VacancyBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.VacancyModule.VacancyBase>
    {
		public VacancyBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.VacancyModule.VacancyBase dbItem)
            : base(BusinessLogic_v3.Cms.VacancyModule.VacancyBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new VacancyBase DbItem
        {
            get
            {
                return (VacancyBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo Description { get; private set; }

        public CmsPropertyInfo RefCode { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.VacancyModule.VacancyBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.Description = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.VacancyModule.VacancyBase>.GetPropertyBySelector( item => item.Description),
        			isEditable: true,
        			isVisible: true
        			);

        this.RefCode = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.VacancyModule.VacancyBase>.GetPropertyBySelector( item => item.RefCode),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
