using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.MeasurementUnitModule;
using BusinessLogic_v3.Cms.MeasurementUnitModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class MeasurementUnitBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.MeasurementUnitModule.MeasurementUnitBase>
    {
		public MeasurementUnitBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.MeasurementUnitModule.MeasurementUnitBase dbItem)
            : base(BusinessLogic_v3.Cms.MeasurementUnitModule.MeasurementUnitBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new MeasurementUnitBase DbItem
        {
            get
            {
                return (MeasurementUnitBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo Description { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MeasurementUnitModule.MeasurementUnitBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.Description = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MeasurementUnitModule.MeasurementUnitBase>.GetPropertyBySelector( item => item.Description),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
