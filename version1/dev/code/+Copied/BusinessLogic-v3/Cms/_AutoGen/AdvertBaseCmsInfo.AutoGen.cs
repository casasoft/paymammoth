using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.AdvertModule;
using BusinessLogic_v3.Cms.AdvertModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class AdvertBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.AdvertModule.AdvertBase>
    {
		public AdvertBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.AdvertModule.AdvertBase dbItem)
            : base(BusinessLogic_v3.Cms.AdvertModule.AdvertBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new AdvertBase DbItem
        {
            get
            {
                return (AdvertBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo AdvertSlot { get; private set; }

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo Description { get; private set; }

        public CmsPropertyInfo Filename { get; private set; }

        public CmsPropertyInfo AdvertShownPerRound { get; private set; }

        public CmsPropertyInfo RoundCounter { get; private set; }

        public CmsPropertyInfo VideoDuration { get; private set; }

        public CmsPropertyInfo Link { get; private set; }

        public CmsPropertyInfo ShowDateFrom { get; private set; }

        public CmsPropertyInfo ShowDateTo { get; private set; }

        public CmsPropertyInfo Activated { get; private set; }

        public CmsPropertyInfo HrefTarget { get; private set; }

        public CmsPropertyInfo BackgroundColor { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.AdvertSlot = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertModule.AdvertBase>.GetPropertyBySelector( item => item.AdvertSlot),
        			isEditable: true,
        			isVisible: true
        			);

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertModule.AdvertBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.Description = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertModule.AdvertBase>.GetPropertyBySelector( item => item.Description),
        			isEditable: true,
        			isVisible: true
        			);

        this.Filename = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertModule.AdvertBase>.GetPropertyBySelector( item => item.Filename),
        			isEditable: false,
        			isVisible: false
        			);

        this.AdvertShownPerRound = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertModule.AdvertBase>.GetPropertyBySelector( item => item.AdvertShownPerRound),
        			isEditable: true,
        			isVisible: true
        			);

        this.RoundCounter = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertModule.AdvertBase>.GetPropertyBySelector( item => item.RoundCounter),
        			isEditable: true,
        			isVisible: true
        			);

        this.VideoDuration = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertModule.AdvertBase>.GetPropertyBySelector( item => item.VideoDuration),
        			isEditable: true,
        			isVisible: true
        			);

        this.Link = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertModule.AdvertBase>.GetPropertyBySelector( item => item.Link),
        			isEditable: true,
        			isVisible: true
        			);

        this.ShowDateFrom = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertModule.AdvertBase>.GetPropertyBySelector( item => item.ShowDateFrom),
        			isEditable: true,
        			isVisible: true
        			);

        this.ShowDateTo = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertModule.AdvertBase>.GetPropertyBySelector( item => item.ShowDateTo),
        			isEditable: true,
        			isVisible: true
        			);

        this.Activated = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertModule.AdvertBase>.GetPropertyBySelector( item => item.Activated),
        			isEditable: true,
        			isVisible: true
        			);

        this.HrefTarget = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertModule.AdvertBase>.GetPropertyBySelector( item => item.HrefTarget),
        			isEditable: true,
        			isVisible: true
        			);

        this.BackgroundColor = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertModule.AdvertBase>.GetPropertyBySelector( item => item.BackgroundColor),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
