using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.ContentPageModule;
using BusinessLogic_v3.Cms.ContentPageModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class ContentPageBaseCmsFactory_AutoGen : CmsFactoryBase<ContentPageBaseCmsInfo, ContentPageBase>
    {
       
       public new static ContentPageBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ContentPageBaseCmsFactory)CmsFactoryBase<ContentPageBaseCmsInfo, ContentPageBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = ContentPageBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ContentPage.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ContentPage";

            this.QueryStringParamID = "ContentPageId";

            cmsInfo.TitlePlural = "Content Pages";

            cmsInfo.TitleSingular =  "Content Page";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ContentPageBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "ContentPage/";


        }
       
    }

}
