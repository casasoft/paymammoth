using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.BlogModule;
using BusinessLogic_v3.Cms.BlogModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class BlogBaseCmsFactory_AutoGen : CmsFactoryBase<BlogBaseCmsInfo, BlogBase>
    {
       
       public new static BlogBaseCmsFactory Instance
	    {
	         get
	         {
                 return (BlogBaseCmsFactory)CmsFactoryBase<BlogBaseCmsInfo, BlogBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = BlogBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Blog.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Blog";

            this.QueryStringParamID = "BlogId";

            cmsInfo.TitlePlural = "Blogs";

            cmsInfo.TitleSingular =  "Blog";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public BlogBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "Blog/";


        }
       
    }

}
