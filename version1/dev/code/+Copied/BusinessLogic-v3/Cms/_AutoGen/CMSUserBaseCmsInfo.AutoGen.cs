using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.CMSUserModule;
using BusinessLogic_v3.Cms.CMSUserModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class CMSUserBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.CMSUserModule.CMSUserBase>
    {
		public CMSUserBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.CMSUserModule.CMSUserBase dbItem)
            : base(BusinessLogic_v3.Cms.CMSUserModule.CMSUserBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new CMSUserBase DbItem
        {
            get
            {
                return (CMSUserBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Name { get; private set; }

        public CmsPropertyInfo Surname { get; private set; }

        public CmsPropertyInfo Username { get; private set; }

        public CmsPropertyInfo Password { get; private set; }

        public CmsPropertyInfo LastLoggedIn { get; private set; }

        public CmsPropertyInfo _LastLoggedIn_New { get; private set; }

        public CmsPropertyInfo AccessType { get; private set; }

        public CmsPropertyInfo Activated { get; private set; }

        public CmsPropertyInfo SessionGUID { get; private set; }

        public CmsPropertyInfo PasswordSalt { get; private set; }

        public CmsPropertyInfo PasswordEncryptionType { get; private set; }

        public CmsPropertyInfo PasswordIterations { get; private set; }

        public CmsPropertyInfo HiddenFromNonCasaSoft { get; private set; }

        public CmsPropertyInfo CmsCustomLogoFilename { get; private set; }

        public CmsPropertyInfo CmsCustomLogoLinkUrl { get; private set; }



// [basecmsinfo_manytomanydeclarations]

        public CmsPropertyManyToManyCollection<BusinessLogic_v3.Cms.CmsUserRoleModule.CmsUserRoleBaseCmsInfo> CmsUserRoles { get; private set; }



        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Name = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CMSUserModule.CMSUserBase>.GetPropertyBySelector( item => item.Name),
        			isEditable: true,
        			isVisible: true
        			);

        this.Surname = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CMSUserModule.CMSUserBase>.GetPropertyBySelector( item => item.Surname),
        			isEditable: true,
        			isVisible: true
        			);

        this.Username = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CMSUserModule.CMSUserBase>.GetPropertyBySelector( item => item.Username),
        			isEditable: true,
        			isVisible: true
        			);

        this.Password = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CMSUserModule.CMSUserBase>.GetPropertyBySelector( item => item.Password),
        			isEditable: true,
        			isVisible: true
        			);

        this.LastLoggedIn = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CMSUserModule.CMSUserBase>.GetPropertyBySelector( item => item.LastLoggedIn),
        			isEditable: true,
        			isVisible: true
        			);

        this._LastLoggedIn_New = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CMSUserModule.CMSUserBase>.GetPropertyBySelector( item => item._LastLoggedIn_New),
        			isEditable: true,
        			isVisible: true
        			);

        this.AccessType = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CMSUserModule.CMSUserBase>.GetPropertyBySelector( item => item.AccessType),
        			isEditable: true,
        			isVisible: true
        			);

        this.Activated = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CMSUserModule.CMSUserBase>.GetPropertyBySelector( item => item.Activated),
        			isEditable: true,
        			isVisible: true
        			);

        this.SessionGUID = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CMSUserModule.CMSUserBase>.GetPropertyBySelector( item => item.SessionGUID),
        			isEditable: true,
        			isVisible: true
        			);

        this.PasswordSalt = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CMSUserModule.CMSUserBase>.GetPropertyBySelector( item => item.PasswordSalt),
        			isEditable: true,
        			isVisible: true
        			);

        this.PasswordEncryptionType = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CMSUserModule.CMSUserBase>.GetPropertyBySelector( item => item.PasswordEncryptionType),
        			isEditable: true,
        			isVisible: true
        			);

        this.PasswordIterations = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CMSUserModule.CMSUserBase>.GetPropertyBySelector( item => item.PasswordIterations),
        			isEditable: true,
        			isVisible: true
        			);

        this.HiddenFromNonCasaSoft = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CMSUserModule.CMSUserBase>.GetPropertyBySelector( item => item.HiddenFromNonCasaSoft),
        			isEditable: true,
        			isVisible: true
        			);

        this.CmsCustomLogoFilename = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CMSUserModule.CMSUserBase>.GetPropertyBySelector( item => item.CmsCustomLogoFilename),
        			isEditable: false,
        			isVisible: false
        			);

        this.CmsCustomLogoLinkUrl = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CMSUserModule.CMSUserBase>.GetPropertyBySelector( item => item.CmsCustomLogoLinkUrl),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]

//InitFieldManyToManyBase_LeftSide
		this.CmsUserRoles = this.AddManyToManyCollectionWithDirectProperty<BusinessLogic_v3.Cms.CmsUserRoleModule.CmsUserRoleBaseCmsInfo>(
		BusinessLogic_v3.Cms.CmsUserRoleModule.CmsUserRoleBaseCmsFactory.Instance, 
		CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CMSUserModule.CMSUserBase>.GetPropertyBySelector(item => item.GetCmsUserRoles()),
		((item) => ((BusinessLogic_v3.Modules.CMSUserModule.CMSUserBase) item).ClearCmsUserRoles()),
            ((item, itemToAdd) => ((BusinessLogic_v3.Modules.CMSUserModule.CMSUserBase) item).AddCmsUserRoleToCmsUserRoles((BusinessLogic_v3.Modules.CmsUserRoleModule.CmsUserRoleBase)itemToAdd)),
		 Classes.Cms.EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);



			base.initBasicFields();
          
        }

    }
}
