using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.NewsletterSubscriptionModule;
using BusinessLogic_v3.Cms.NewsletterSubscriptionModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class NewsletterSubscriptionBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.NewsletterSubscriptionModule.NewsletterSubscriptionBase>
    {
		public NewsletterSubscriptionBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.NewsletterSubscriptionModule.NewsletterSubscriptionBase dbItem)
            : base(BusinessLogic_v3.Cms.NewsletterSubscriptionModule.NewsletterSubscriptionBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new NewsletterSubscriptionBase DbItem
        {
            get
            {
                return (NewsletterSubscriptionBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Email { get; private set; }

        public CmsPropertyInfo Name { get; private set; }

        public CmsPropertyInfo Telephone { get; private set; }

        public CmsPropertyInfo DateAdded { get; private set; }

        public CmsPropertyInfo Enabled { get; private set; }

        public CmsPropertyInfo RegisteredIPAddress { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Email = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.NewsletterSubscriptionModule.NewsletterSubscriptionBase>.GetPropertyBySelector( item => item.Email),
        			isEditable: true,
        			isVisible: true
        			);

        this.Name = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.NewsletterSubscriptionModule.NewsletterSubscriptionBase>.GetPropertyBySelector( item => item.Name),
        			isEditable: true,
        			isVisible: true
        			);

        this.Telephone = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.NewsletterSubscriptionModule.NewsletterSubscriptionBase>.GetPropertyBySelector( item => item.Telephone),
        			isEditable: true,
        			isVisible: true
        			);

        this.DateAdded = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.NewsletterSubscriptionModule.NewsletterSubscriptionBase>.GetPropertyBySelector( item => item.DateAdded),
        			isEditable: true,
        			isVisible: true
        			);

        this.Enabled = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.NewsletterSubscriptionModule.NewsletterSubscriptionBase>.GetPropertyBySelector( item => item.Enabled),
        			isEditable: true,
        			isVisible: true
        			);

        this.RegisteredIPAddress = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.NewsletterSubscriptionModule.NewsletterSubscriptionBase>.GetPropertyBySelector( item => item.RegisteredIPAddress),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
