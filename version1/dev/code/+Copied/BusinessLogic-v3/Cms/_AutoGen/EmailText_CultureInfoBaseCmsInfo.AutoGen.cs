using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.EmailText_CultureInfoModule;
using BusinessLogic_v3.Cms.EmailText_CultureInfoModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class EmailText_CultureInfoBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase>
    {
		public EmailText_CultureInfoBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase dbItem)
            : base(BusinessLogic_v3.Cms.EmailText_CultureInfoModule.EmailText_CultureInfoBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new EmailText_CultureInfoBase DbItem
        {
            get
            {
                return (EmailText_CultureInfoBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo CultureInfo { get; private set; }

        public CmsPropertyInfo EmailText { get; private set; }

        public CmsPropertyInfo Subject { get; private set; }

        public CmsPropertyInfo Subject_Search { get; private set; }

        public CmsPropertyInfo Body { get; private set; }

        public CmsPropertyInfo Body_Search { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.CultureInfo = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase>.GetPropertyBySelector( item => item.CultureInfo),
        			isEditable: true,
        			isVisible: true
        			);

        this.EmailText = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase>.GetPropertyBySelector( item => item.EmailText),
        			isEditable: true,
        			isVisible: true
        			);

        this.Subject = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase>.GetPropertyBySelector( item => item.Subject),
        			isEditable: true,
        			isVisible: true
        			);

        this.Subject_Search = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase>.GetPropertyBySelector( item => item.Subject_Search),
        			isEditable: false,
        			isVisible: false
        			);

        this.Body = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase>.GetPropertyBySelector( item => item.Body),
        			isEditable: true,
        			isVisible: true
        			);

        this.Body_Search = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase>.GetPropertyBySelector( item => item.Body_Search),
        			isEditable: false,
        			isVisible: false
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
