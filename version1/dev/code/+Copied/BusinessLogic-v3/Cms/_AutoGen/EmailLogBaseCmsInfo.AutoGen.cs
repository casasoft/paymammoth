using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.EmailLogModule;
using BusinessLogic_v3.Cms.EmailLogModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class EmailLogBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.EmailLogModule.EmailLogBase>
    {
		public EmailLogBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.EmailLogModule.EmailLogBase dbItem)
            : base(BusinessLogic_v3.Cms.EmailLogModule.EmailLogBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new EmailLogBase DbItem
        {
            get
            {
                return (EmailLogBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo DateTime { get; private set; }

        public CmsPropertyInfo FromEmail { get; private set; }

        public CmsPropertyInfo FromName { get; private set; }

        public CmsPropertyInfo ToEmail { get; private set; }

        public CmsPropertyInfo ToName { get; private set; }

        public CmsPropertyInfo ReplyToEmail { get; private set; }

        public CmsPropertyInfo ReplyToName { get; private set; }

        public CmsPropertyInfo Success { get; private set; }

        public CmsPropertyInfo Subject { get; private set; }

        public CmsPropertyInfo PlainText { get; private set; }

        public CmsPropertyInfo HtmlText { get; private set; }

        public CmsPropertyInfo Attachements { get; private set; }

        public CmsPropertyInfo Resources { get; private set; }

        public CmsPropertyInfo Host { get; private set; }

        public CmsPropertyInfo Port { get; private set; }

        public CmsPropertyInfo User { get; private set; }

        public CmsPropertyInfo Pass { get; private set; }

        public CmsPropertyInfo UsesSecureConnection { get; private set; }

        public CmsPropertyInfo ResultDetails { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.DateTime = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailLogModule.EmailLogBase>.GetPropertyBySelector( item => item.DateTime),
        			isEditable: true,
        			isVisible: true
        			);

        this.FromEmail = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailLogModule.EmailLogBase>.GetPropertyBySelector( item => item.FromEmail),
        			isEditable: true,
        			isVisible: true
        			);

        this.FromName = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailLogModule.EmailLogBase>.GetPropertyBySelector( item => item.FromName),
        			isEditable: true,
        			isVisible: true
        			);

        this.ToEmail = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailLogModule.EmailLogBase>.GetPropertyBySelector( item => item.ToEmail),
        			isEditable: true,
        			isVisible: true
        			);

        this.ToName = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailLogModule.EmailLogBase>.GetPropertyBySelector( item => item.ToName),
        			isEditable: true,
        			isVisible: true
        			);

        this.ReplyToEmail = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailLogModule.EmailLogBase>.GetPropertyBySelector( item => item.ReplyToEmail),
        			isEditable: true,
        			isVisible: true
        			);

        this.ReplyToName = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailLogModule.EmailLogBase>.GetPropertyBySelector( item => item.ReplyToName),
        			isEditable: true,
        			isVisible: true
        			);

        this.Success = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailLogModule.EmailLogBase>.GetPropertyBySelector( item => item.Success),
        			isEditable: true,
        			isVisible: true
        			);

        this.Subject = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailLogModule.EmailLogBase>.GetPropertyBySelector( item => item.Subject),
        			isEditable: true,
        			isVisible: true
        			);

        this.PlainText = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailLogModule.EmailLogBase>.GetPropertyBySelector( item => item.PlainText),
        			isEditable: true,
        			isVisible: true
        			);

        this.HtmlText = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailLogModule.EmailLogBase>.GetPropertyBySelector( item => item.HtmlText),
        			isEditable: true,
        			isVisible: true
        			);

        this.Attachements = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailLogModule.EmailLogBase>.GetPropertyBySelector( item => item.Attachements),
        			isEditable: true,
        			isVisible: true
        			);

        this.Resources = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailLogModule.EmailLogBase>.GetPropertyBySelector( item => item.Resources),
        			isEditable: true,
        			isVisible: true
        			);

        this.Host = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailLogModule.EmailLogBase>.GetPropertyBySelector( item => item.Host),
        			isEditable: true,
        			isVisible: true
        			);

        this.Port = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailLogModule.EmailLogBase>.GetPropertyBySelector( item => item.Port),
        			isEditable: true,
        			isVisible: true
        			);

        this.User = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailLogModule.EmailLogBase>.GetPropertyBySelector( item => item.User),
        			isEditable: true,
        			isVisible: true
        			);

        this.Pass = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailLogModule.EmailLogBase>.GetPropertyBySelector( item => item.Pass),
        			isEditable: true,
        			isVisible: true
        			);

        this.UsesSecureConnection = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailLogModule.EmailLogBase>.GetPropertyBySelector( item => item.UsesSecureConnection),
        			isEditable: true,
        			isVisible: true
        			);

        this.ResultDetails = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.EmailLogModule.EmailLogBase>.GetPropertyBySelector( item => item.ResultDetails),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
