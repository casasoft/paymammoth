using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.AffiliateModule;
using BusinessLogic_v3.Cms.AffiliateModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class AffiliateBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase>
    {
		public AffiliateBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase dbItem)
            : base(BusinessLogic_v3.Cms.AffiliateModule.AffiliateBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new AffiliateBase DbItem
        {
            get
            {
                return (AffiliateBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo CssFilename { get; private set; }

        public CmsPropertyInfo LogoFilename { get; private set; }

        public CmsPropertyInfo Username { get; private set; }

        public CmsPropertyInfo Password { get; private set; }

        public CmsPropertyInfo Activated { get; private set; }

        public CmsPropertyInfo ActiveFrom { get; private set; }

        public CmsPropertyInfo AffiliateCode { get; private set; }

        public CmsPropertyInfo ReferralCode { get; private set; }

        public CmsPropertyInfo LinkedContentPageNode { get; private set; }

        public CmsPropertyInfo CommissionRate { get; private set; }

        public CmsPropertyInfo AffiliateUrl { get; private set; }

        public CmsPropertyInfo LinkedCmsUser { get; private set; }

        public CmsPropertyInfo HasWhiteLabellingFunctionality { get; private set; }

        public CmsPropertyInfo AffiliateWhiteLabelBaseUrl { get; private set; }

        public CmsPropertyInfo MainPanelHtml { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.CssFilename = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase>.GetPropertyBySelector( item => item.CssFilename),
        			isEditable: false,
        			isVisible: false
        			);

        this.LogoFilename = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase>.GetPropertyBySelector( item => item.LogoFilename),
        			isEditable: false,
        			isVisible: false
        			);

        this.Username = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase>.GetPropertyBySelector( item => item.Username),
        			isEditable: true,
        			isVisible: true
        			);

        this.Password = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase>.GetPropertyBySelector( item => item.Password),
        			isEditable: true,
        			isVisible: true
        			);

        this.Activated = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase>.GetPropertyBySelector( item => item.Activated),
        			isEditable: true,
        			isVisible: true
        			);

        this.ActiveFrom = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase>.GetPropertyBySelector( item => item.ActiveFrom),
        			isEditable: true,
        			isVisible: true
        			);

        this.AffiliateCode = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase>.GetPropertyBySelector( item => item.AffiliateCode),
        			isEditable: true,
        			isVisible: true
        			);

        this.ReferralCode = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase>.GetPropertyBySelector( item => item.ReferralCode),
        			isEditable: true,
        			isVisible: true
        			);

        this.LinkedContentPageNode = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase>.GetPropertyBySelector( item => item.LinkedContentPageNode),
        			isEditable: true,
        			isVisible: true
        			);

        this.CommissionRate = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase>.GetPropertyBySelector( item => item.CommissionRate),
        			isEditable: true,
        			isVisible: true
        			);

        this.AffiliateUrl = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase>.GetPropertyBySelector( item => item.AffiliateUrl),
        			isEditable: true,
        			isVisible: true
        			);

        this.LinkedCmsUser = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase>.GetPropertyBySelector( item => item.LinkedCmsUser),
        			isEditable: true,
        			isVisible: true
        			);

        this.HasWhiteLabellingFunctionality = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase>.GetPropertyBySelector( item => item.HasWhiteLabellingFunctionality),
        			isEditable: true,
        			isVisible: true
        			);

        this.AffiliateWhiteLabelBaseUrl = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase>.GetPropertyBySelector( item => item.AffiliateWhiteLabelBaseUrl),
        			isEditable: true,
        			isVisible: true
        			);

        this.MainPanelHtml = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliateModule.AffiliateBase>.GetPropertyBySelector( item => item.MainPanelHtml),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
