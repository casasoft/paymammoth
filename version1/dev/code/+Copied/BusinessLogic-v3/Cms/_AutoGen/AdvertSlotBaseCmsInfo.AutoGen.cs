using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.AdvertSlotModule;
using BusinessLogic_v3.Cms.AdvertSlotModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class AdvertSlotBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.AdvertSlotModule.AdvertSlotBase>
    {
		public AdvertSlotBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.AdvertSlotModule.AdvertSlotBase dbItem)
            : base(BusinessLogic_v3.Cms.AdvertSlotModule.AdvertSlotBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new AdvertSlotBase DbItem
        {
            get
            {
                return (AdvertSlotBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo AdvertColumn { get; private set; }

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo RoundNo { get; private set; }

        public CmsPropertyInfo Width { get; private set; }

        public CmsPropertyInfo Height { get; private set; }

        public CmsPropertyInfo Identifier { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.AdvertColumn = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertSlotModule.AdvertSlotBase>.GetPropertyBySelector( item => item.AdvertColumn),
        			isEditable: true,
        			isVisible: true
        			);

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertSlotModule.AdvertSlotBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.RoundNo = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertSlotModule.AdvertSlotBase>.GetPropertyBySelector( item => item.RoundNo),
        			isEditable: true,
        			isVisible: true
        			);

        this.Width = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertSlotModule.AdvertSlotBase>.GetPropertyBySelector( item => item.Width),
        			isEditable: true,
        			isVisible: true
        			);

        this.Height = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertSlotModule.AdvertSlotBase>.GetPropertyBySelector( item => item.Height),
        			isEditable: true,
        			isVisible: true
        			);

        this.Identifier = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertSlotModule.AdvertSlotBase>.GetPropertyBySelector( item => item.Identifier),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
