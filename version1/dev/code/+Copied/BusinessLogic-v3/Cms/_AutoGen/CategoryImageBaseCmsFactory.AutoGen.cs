using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.CategoryImageModule;
using BusinessLogic_v3.Cms.CategoryImageModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class CategoryImageBaseCmsFactory_AutoGen : CmsFactoryBase<CategoryImageBaseCmsInfo, CategoryImageBase>
    {
       
       public new static CategoryImageBaseCmsFactory Instance
	    {
	         get
	         {
                 return (CategoryImageBaseCmsFactory)CmsFactoryBase<CategoryImageBaseCmsInfo, CategoryImageBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = CategoryImageBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "CategoryImage.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "CategoryImage";

            this.QueryStringParamID = "CategoryImageId";

            cmsInfo.TitlePlural = "Category Images";

            cmsInfo.TitleSingular =  "Category Image";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public CategoryImageBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "CategoryImage/";


        }
       
    }

}
