using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ItemGroupCategorySpecificationValueModule;
using BusinessLogic_v3.Cms.ItemGroupCategorySpecificationValueModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ItemGroupCategorySpecificationValueBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ItemGroupCategorySpecificationValueModule.ItemGroupCategorySpecificationValueBase>
    {
		public ItemGroupCategorySpecificationValueBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ItemGroupCategorySpecificationValueModule.ItemGroupCategorySpecificationValueBase dbItem)
            : base(BusinessLogic_v3.Cms.ItemGroupCategorySpecificationValueModule.ItemGroupCategorySpecificationValueBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ItemGroupCategorySpecificationValueBase DbItem
        {
            get
            {
                return (ItemGroupCategorySpecificationValueBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Value { get; private set; }

        public CmsPropertyInfo ItemGroup { get; private set; }

        public CmsPropertyInfo Title { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Value = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroupCategorySpecificationValueModule.ItemGroupCategorySpecificationValueBase>.GetPropertyBySelector( item => item.Value),
        			isEditable: true,
        			isVisible: true
        			);

        this.ItemGroup = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroupCategorySpecificationValueModule.ItemGroupCategorySpecificationValueBase>.GetPropertyBySelector( item => item.ItemGroup),
        			isEditable: true,
        			isVisible: true
        			);

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroupCategorySpecificationValueModule.ItemGroupCategorySpecificationValueBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
