using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.MemberReferralModule;
using BusinessLogic_v3.Cms.MemberReferralModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class MemberReferralBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase>
    {
		public MemberReferralBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase dbItem)
            : base(BusinessLogic_v3.Cms.MemberReferralModule.MemberReferralBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new MemberReferralBase DbItem
        {
            get
            {
                return (MemberReferralBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo DateTime { get; private set; }

        public CmsPropertyInfo SentByMember { get; private set; }

        public CmsPropertyInfo RegisteredMember { get; private set; }

        public CmsPropertyInfo Email { get; private set; }

        public CmsPropertyInfo Name { get; private set; }

        public CmsPropertyInfo Surname { get; private set; }

        public CmsPropertyInfo Message { get; private set; }

        public CmsPropertyInfo Telephone { get; private set; }

        public CmsPropertyInfo Status { get; private set; }

        public CmsPropertyInfo ReferredByIP { get; private set; }

        public CmsPropertyInfo ReferralCode { get; private set; }

        public CmsPropertyInfo Remarks { get; private set; }

        public CmsPropertyInfo SentByName { get; private set; }

        public CmsPropertyInfo SentByEmail { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.DateTime = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase>.GetPropertyBySelector( item => item.DateTime),
        			isEditable: true,
        			isVisible: true
        			);

        this.SentByMember = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase>.GetPropertyBySelector( item => item.SentByMember),
        			isEditable: true,
        			isVisible: true
        			);

        this.RegisteredMember = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase>.GetPropertyBySelector( item => item.RegisteredMember),
        			isEditable: true,
        			isVisible: true
        			);

        this.Email = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase>.GetPropertyBySelector( item => item.Email),
        			isEditable: true,
        			isVisible: true
        			);

        this.Name = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase>.GetPropertyBySelector( item => item.Name),
        			isEditable: true,
        			isVisible: true
        			);

        this.Surname = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase>.GetPropertyBySelector( item => item.Surname),
        			isEditable: true,
        			isVisible: true
        			);

        this.Message = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase>.GetPropertyBySelector( item => item.Message),
        			isEditable: true,
        			isVisible: true
        			);

        this.Telephone = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase>.GetPropertyBySelector( item => item.Telephone),
        			isEditable: true,
        			isVisible: true
        			);

        this.Status = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase>.GetPropertyBySelector( item => item.Status),
        			isEditable: true,
        			isVisible: true
        			);

        this.ReferredByIP = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase>.GetPropertyBySelector( item => item.ReferredByIP),
        			isEditable: true,
        			isVisible: true
        			);

        this.ReferralCode = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase>.GetPropertyBySelector( item => item.ReferralCode),
        			isEditable: true,
        			isVisible: true
        			);

        this.Remarks = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase>.GetPropertyBySelector( item => item.Remarks),
        			isEditable: true,
        			isVisible: true
        			);

        this.SentByName = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase>.GetPropertyBySelector( item => item.SentByName),
        			isEditable: true,
        			isVisible: true
        			);

        this.SentByEmail = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberReferralModule.MemberReferralBase>.GetPropertyBySelector( item => item.SentByEmail),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
