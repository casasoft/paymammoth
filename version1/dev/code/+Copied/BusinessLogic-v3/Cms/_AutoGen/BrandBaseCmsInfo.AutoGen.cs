using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.BrandModule;
using BusinessLogic_v3.Cms.BrandModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class BrandBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.BrandModule.BrandBase>
    {
		public BrandBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.BrandModule.BrandBase dbItem)
            : base(BusinessLogic_v3.Cms.BrandModule.BrandBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new BrandBase DbItem
        {
            get
            {
                return (BrandBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo Description { get; private set; }

        public CmsPropertyInfo BrandLogoFilename { get; private set; }

        public CmsPropertyInfo IsFeatured { get; private set; }

        public CmsPropertyInfo Url { get; private set; }

        public CmsPropertyInfo ImportReference { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BrandModule.BrandBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.Description = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BrandModule.BrandBase>.GetPropertyBySelector( item => item.Description),
        			isEditable: true,
        			isVisible: true
        			);

        this.BrandLogoFilename = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BrandModule.BrandBase>.GetPropertyBySelector( item => item.BrandLogoFilename),
        			isEditable: false,
        			isVisible: false
        			);

        this.IsFeatured = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BrandModule.BrandBase>.GetPropertyBySelector( item => item.IsFeatured),
        			isEditable: true,
        			isVisible: true
        			);

        this.Url = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BrandModule.BrandBase>.GetPropertyBySelector( item => item.Url),
        			isEditable: true,
        			isVisible: true
        			);

        this.ImportReference = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BrandModule.BrandBase>.GetPropertyBySelector( item => item.ImportReference),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
