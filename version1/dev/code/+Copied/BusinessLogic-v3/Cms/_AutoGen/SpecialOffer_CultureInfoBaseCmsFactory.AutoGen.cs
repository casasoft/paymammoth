using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.SpecialOffer_CultureInfoModule;
using BusinessLogic_v3.Cms.SpecialOffer_CultureInfoModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class SpecialOffer_CultureInfoBaseCmsFactory_AutoGen : CmsFactoryBase<SpecialOffer_CultureInfoBaseCmsInfo, SpecialOffer_CultureInfoBase>
    {
       
       public new static SpecialOffer_CultureInfoBaseCmsFactory Instance
	    {
	         get
	         {
                 return (SpecialOffer_CultureInfoBaseCmsFactory)CmsFactoryBase<SpecialOffer_CultureInfoBaseCmsInfo, SpecialOffer_CultureInfoBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = SpecialOffer_CultureInfoBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "SpecialOffer_CultureInfo.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "SpecialOffer_CultureInfo";

            this.QueryStringParamID = "SpecialOffer_CultureInfoId";

            cmsInfo.TitlePlural = "Special Offer _ Culture Infos";

            cmsInfo.TitleSingular =  "Special Offer _ Culture Info";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public SpecialOffer_CultureInfoBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "SpecialOffer_CultureInfo/";


        }
       
    }

}
