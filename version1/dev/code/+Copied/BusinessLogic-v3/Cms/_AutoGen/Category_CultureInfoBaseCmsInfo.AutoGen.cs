using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.Category_CultureInfoModule;
using BusinessLogic_v3.Cms.Category_CultureInfoModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class Category_CultureInfoBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase>
    {
		public Category_CultureInfoBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase dbItem)
            : base(BusinessLogic_v3.Cms.Category_CultureInfoModule.Category_CultureInfoBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new Category_CultureInfoBase DbItem
        {
            get
            {
                return (Category_CultureInfoBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo CultureInfo { get; private set; }

        public CmsPropertyInfo Category { get; private set; }

        public CmsPropertyInfo TitleSingular { get; private set; }

        public CmsPropertyInfo TitleSingular_Search { get; private set; }

        public CmsPropertyInfo TitlePlural { get; private set; }

        public CmsPropertyInfo TitlePlural_Search { get; private set; }

        public CmsPropertyInfo TitleSEO { get; private set; }

        public CmsPropertyInfo Description { get; private set; }

        public CmsPropertyInfo MetaKeywords { get; private set; }

        public CmsPropertyInfo MetaDescription { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.CultureInfo = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase>.GetPropertyBySelector( item => item.CultureInfo),
        			isEditable: true,
        			isVisible: true
        			);

        this.Category = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase>.GetPropertyBySelector( item => item.Category),
        			isEditable: true,
        			isVisible: true
        			);

        this.TitleSingular = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase>.GetPropertyBySelector( item => item.TitleSingular),
        			isEditable: true,
        			isVisible: true
        			);

        this.TitleSingular_Search = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase>.GetPropertyBySelector( item => item.TitleSingular_Search),
        			isEditable: false,
        			isVisible: false
        			);

        this.TitlePlural = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase>.GetPropertyBySelector( item => item.TitlePlural),
        			isEditable: true,
        			isVisible: true
        			);

        this.TitlePlural_Search = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase>.GetPropertyBySelector( item => item.TitlePlural_Search),
        			isEditable: false,
        			isVisible: false
        			);

        this.TitleSEO = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase>.GetPropertyBySelector( item => item.TitleSEO),
        			isEditable: true,
        			isVisible: true
        			);

        this.Description = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase>.GetPropertyBySelector( item => item.Description),
        			isEditable: true,
        			isVisible: true
        			);

        this.MetaKeywords = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase>.GetPropertyBySelector( item => item.MetaKeywords),
        			isEditable: true,
        			isVisible: true
        			);

        this.MetaDescription = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase>.GetPropertyBySelector( item => item.MetaDescription),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
