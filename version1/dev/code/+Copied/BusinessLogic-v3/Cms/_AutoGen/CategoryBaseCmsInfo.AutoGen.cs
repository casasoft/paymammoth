using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.CategoryModule;
using BusinessLogic_v3.Cms.CategoryModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class CategoryBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>
    {
		public CategoryBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.CategoryModule.CategoryBase dbItem)
            : base(BusinessLogic_v3.Cms.CategoryModule.CategoryBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new CategoryBase DbItem
        {
            get
            {
                return (CategoryBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo TitleSingular { get; private set; }

        public CmsPropertyInfo TitleSingular_Search { get; private set; }

        public CmsPropertyInfo TitlePlural { get; private set; }

        public CmsPropertyInfo TitlePlural_Search { get; private set; }

        public CmsPropertyInfo TitleSEO { get; private set; }

        public CmsPropertyInfo Description { get; private set; }

        public CmsPropertyInfo MetaKeywords { get; private set; }

        public CmsPropertyInfo MetaDescription { get; private set; }

        public CmsPropertyInfo CanDelete { get; private set; }

        public CmsPropertyInfo Identifier { get; private set; }

        public CmsPropertyInfo ComingSoon { get; private set; }

        public CmsPropertyInfo PriorityAll { get; private set; }

        public CmsPropertyInfo Visible { get; private set; }

        public CmsPropertyInfo Brand { get; private set; }

        public CmsPropertyInfo ImportReference { get; private set; }

        public CmsPropertyInfo DontShowInNavigation { get; private set; }



// [basecmsinfo_manytomanydeclarations]

        public CmsPropertyManyToManyCollection<BusinessLogic_v3.Cms.MemberModule.MemberBaseCmsInfo> SubscribedMembers { get; private set; }



        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.TitleSingular = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector( item => item.TitleSingular),
        			isEditable: true,
        			isVisible: true
        			);

        this.TitleSingular_Search = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector( item => item.TitleSingular_Search),
        			isEditable: false,
        			isVisible: false
        			);

        this.TitlePlural = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector( item => item.TitlePlural),
        			isEditable: true,
        			isVisible: true
        			);

        this.TitlePlural_Search = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector( item => item.TitlePlural_Search),
        			isEditable: false,
        			isVisible: false
        			);

        this.TitleSEO = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector( item => item.TitleSEO),
        			isEditable: true,
        			isVisible: true
        			);

        this.Description = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector( item => item.Description),
        			isEditable: true,
        			isVisible: true
        			);

        this.MetaKeywords = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector( item => item.MetaKeywords),
        			isEditable: true,
        			isVisible: true
        			);

        this.MetaDescription = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector( item => item.MetaDescription),
        			isEditable: true,
        			isVisible: true
        			);

        this.CanDelete = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector( item => item.CanDelete),
        			isEditable: true,
        			isVisible: true
        			);

        this.Identifier = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector( item => item.Identifier),
        			isEditable: true,
        			isVisible: true
        			);

        this.ComingSoon = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector( item => item.ComingSoon),
        			isEditable: true,
        			isVisible: true
        			);

        this.PriorityAll = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector( item => item.PriorityAll),
        			isEditable: true,
        			isVisible: true
        			);

        this.Visible = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector( item => item.Visible),
        			isEditable: true,
        			isVisible: true
        			);

        this.Brand = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector( item => item.Brand),
        			isEditable: true,
        			isVisible: true
        			);

        this.ImportReference = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector( item => item.ImportReference),
        			isEditable: true,
        			isVisible: true
        			);

        this.DontShowInNavigation = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector( item => item.DontShowInNavigation),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]

//InitFieldManyToManyBase_RightSide
		this.SubscribedMembers = this.AddManyToManyCollectionWithDirectProperty<BusinessLogic_v3.Cms.MemberModule.MemberBaseCmsInfo>(
		BusinessLogic_v3.Cms.MemberModule.MemberBaseCmsFactory.Instance, 
		CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryModule.CategoryBase>.GetPropertyBySelector(item => item.GetSubscribedMembers()), 
		
		((item) => ((BusinessLogic_v3.Modules.CategoryModule.CategoryBase) item).ClearSubscribedMembers()),
            ((item, itemToAdd) => ((BusinessLogic_v3.Modules.CategoryModule.CategoryBase) item).AddMemberToSubscribedMembers((BusinessLogic_v3.Modules.MemberModule.MemberBase)itemToAdd)),
		
		
		Classes.Cms.EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);


			base.initBasicFields();
          
        }

    }
}
