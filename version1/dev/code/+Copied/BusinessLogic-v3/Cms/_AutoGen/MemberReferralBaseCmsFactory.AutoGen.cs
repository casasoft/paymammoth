using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.MemberReferralModule;
using BusinessLogic_v3.Cms.MemberReferralModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class MemberReferralBaseCmsFactory_AutoGen : CmsFactoryBase<MemberReferralBaseCmsInfo, MemberReferralBase>
    {
       
       public new static MemberReferralBaseCmsFactory Instance
	    {
	         get
	         {
                 return (MemberReferralBaseCmsFactory)CmsFactoryBase<MemberReferralBaseCmsInfo, MemberReferralBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = MemberReferralBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "MemberReferral.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "MemberReferral";

            this.QueryStringParamID = "MemberReferralId";

            cmsInfo.TitlePlural = "Member Referrals";

            cmsInfo.TitleSingular =  "Member Referral";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public MemberReferralBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "MemberReferral/";


        }
       
    }

}
