using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.ContentPage_CultureInfoModule;
using BusinessLogic_v3.Cms.ContentPage_CultureInfoModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class ContentPage_CultureInfoBaseCmsFactory_AutoGen : CmsFactoryBase<ContentPage_CultureInfoBaseCmsInfo, ContentPage_CultureInfoBase>
    {
       
       public new static ContentPage_CultureInfoBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ContentPage_CultureInfoBaseCmsFactory)CmsFactoryBase<ContentPage_CultureInfoBaseCmsInfo, ContentPage_CultureInfoBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = ContentPage_CultureInfoBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ContentPage_CultureInfo.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ContentPage_CultureInfo";

            this.QueryStringParamID = "ContentPage_CultureInfoId";

            cmsInfo.TitlePlural = "Content Page _ Culture Infos";

            cmsInfo.TitleSingular =  "Content Page _ Culture Info";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ContentPage_CultureInfoBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "ContentPage_CultureInfo/";


        }
       
    }

}
