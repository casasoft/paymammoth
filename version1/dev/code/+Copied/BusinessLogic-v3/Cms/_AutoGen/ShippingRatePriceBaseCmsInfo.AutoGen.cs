using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ShippingRatePriceModule;
using BusinessLogic_v3.Cms.ShippingRatePriceModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ShippingRatePriceBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ShippingRatePriceModule.ShippingRatePriceBase>
    {
		public ShippingRatePriceBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ShippingRatePriceModule.ShippingRatePriceBase dbItem)
            : base(BusinessLogic_v3.Cms.ShippingRatePriceModule.ShippingRatePriceBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ShippingRatePriceBase DbItem
        {
            get
            {
                return (ShippingRatePriceBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo ShippingRatesGroup { get; private set; }

        public CmsPropertyInfo FromWeight { get; private set; }

        public CmsPropertyInfo ToWeight { get; private set; }

        public CmsPropertyInfo Price { get; private set; }

        public CmsPropertyInfo PricingType { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.ShippingRatesGroup = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ShippingRatePriceModule.ShippingRatePriceBase>.GetPropertyBySelector( item => item.ShippingRatesGroup),
        			isEditable: true,
        			isVisible: true
        			);

        this.FromWeight = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ShippingRatePriceModule.ShippingRatePriceBase>.GetPropertyBySelector( item => item.FromWeight),
        			isEditable: true,
        			isVisible: true
        			);

        this.ToWeight = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ShippingRatePriceModule.ShippingRatePriceBase>.GetPropertyBySelector( item => item.ToWeight),
        			isEditable: true,
        			isVisible: true
        			);

        this.Price = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ShippingRatePriceModule.ShippingRatePriceBase>.GetPropertyBySelector( item => item.Price),
        			isEditable: true,
        			isVisible: true
        			);

        this.PricingType = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ShippingRatePriceModule.ShippingRatePriceBase>.GetPropertyBySelector( item => item.PricingType),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
