using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using BusinessLogic_v3.Cms.CultureDetailsModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class CultureDetailsBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase>
    {
		public CultureDetailsBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase dbItem)
            : base(BusinessLogic_v3.Cms.CultureDetailsModule.CultureDetailsBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new CultureDetailsBase DbItem
        {
            get
            {
                return (CultureDetailsBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo LanguageISOCode { get; private set; }

        public CmsPropertyInfo DefaultCurrency { get; private set; }

        public CmsPropertyInfo Activated { get; private set; }

        public CmsPropertyInfo SpecificCountryCode { get; private set; }

        public CmsPropertyInfo ScriptSuffix { get; private set; }

        public CmsPropertyInfo IsDefaultCulture { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.LanguageISOCode = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase>.GetPropertyBySelector( item => item.LanguageISOCode),
        			isEditable: true,
        			isVisible: true
        			);

        this.DefaultCurrency = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase>.GetPropertyBySelector( item => item.DefaultCurrency),
        			isEditable: true,
        			isVisible: true
        			);

        this.Activated = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase>.GetPropertyBySelector( item => item.Activated),
        			isEditable: true,
        			isVisible: true
        			);

        this.SpecificCountryCode = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase>.GetPropertyBySelector( item => item.SpecificCountryCode),
        			isEditable: true,
        			isVisible: true
        			);

        this.ScriptSuffix = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase>.GetPropertyBySelector( item => item.ScriptSuffix),
        			isEditable: true,
        			isVisible: true
        			);

        this.IsDefaultCulture = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase>.GetPropertyBySelector( item => item.IsDefaultCulture),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
