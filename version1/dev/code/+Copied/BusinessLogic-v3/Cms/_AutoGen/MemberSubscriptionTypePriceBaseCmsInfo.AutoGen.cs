using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule;
using BusinessLogic_v3.Cms.MemberSubscriptionTypePriceModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class MemberSubscriptionTypePriceBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase>
    {
		public MemberSubscriptionTypePriceBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase dbItem)
            : base(BusinessLogic_v3.Cms.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new MemberSubscriptionTypePriceBase DbItem
        {
            get
            {
                return (MemberSubscriptionTypePriceBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo DurationInDays { get; private set; }

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo Description { get; private set; }

        public CmsPropertyInfo MemberSubscriptionType { get; private set; }

        public CmsPropertyInfo Notification1BeforeInDays { get; private set; }

        public CmsPropertyInfo Notification2BeforeInDays { get; private set; }

        public CmsPropertyInfo Price { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.DurationInDays = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase>.GetPropertyBySelector( item => item.DurationInDays),
        			isEditable: true,
        			isVisible: true
        			);

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.Description = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase>.GetPropertyBySelector( item => item.Description),
        			isEditable: true,
        			isVisible: true
        			);

        this.MemberSubscriptionType = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase>.GetPropertyBySelector( item => item.MemberSubscriptionType),
        			isEditable: true,
        			isVisible: true
        			);

        this.Notification1BeforeInDays = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase>.GetPropertyBySelector( item => item.Notification1BeforeInDays),
        			isEditable: true,
        			isVisible: true
        			);

        this.Notification2BeforeInDays = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase>.GetPropertyBySelector( item => item.Notification2BeforeInDays),
        			isEditable: true,
        			isVisible: true
        			);

        this.Price = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule.MemberSubscriptionTypePriceBase>.GetPropertyBySelector( item => item.Price),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
