using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.AffiliatePaymentInfoModule;
using BusinessLogic_v3.Cms.AffiliatePaymentInfoModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class AffiliatePaymentInfoBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.AffiliatePaymentInfoBase>
    {
		public AffiliatePaymentInfoBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.AffiliatePaymentInfoBase dbItem)
            : base(BusinessLogic_v3.Cms.AffiliatePaymentInfoModule.AffiliatePaymentInfoBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new AffiliatePaymentInfoBase DbItem
        {
            get
            {
                return (AffiliatePaymentInfoBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Affiliate { get; private set; }

        public CmsPropertyInfo Description { get; private set; }

        public CmsPropertyInfo PaymentMethod { get; private set; }

        public CmsPropertyInfo DatePaymentIssued { get; private set; }

        public CmsPropertyInfo AdditionalComments { get; private set; }

        public CmsPropertyInfo Total { get; private set; }

        public CmsPropertyInfo Currency { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Affiliate = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.AffiliatePaymentInfoBase>.GetPropertyBySelector( item => item.Affiliate),
        			isEditable: true,
        			isVisible: true
        			);

        this.Description = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.AffiliatePaymentInfoBase>.GetPropertyBySelector( item => item.Description),
        			isEditable: true,
        			isVisible: true
        			);

        this.PaymentMethod = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.AffiliatePaymentInfoBase>.GetPropertyBySelector( item => item.PaymentMethod),
        			isEditable: true,
        			isVisible: true
        			);

        this.DatePaymentIssued = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.AffiliatePaymentInfoBase>.GetPropertyBySelector( item => item.DatePaymentIssued),
        			isEditable: true,
        			isVisible: true
        			);

        this.AdditionalComments = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.AffiliatePaymentInfoBase>.GetPropertyBySelector( item => item.AdditionalComments),
        			isEditable: true,
        			isVisible: true
        			);

        this.Total = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.AffiliatePaymentInfoBase>.GetPropertyBySelector( item => item.Total),
        			isEditable: true,
        			isVisible: true
        			);

        this.Currency = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AffiliatePaymentInfoModule.AffiliatePaymentInfoBase>.GetPropertyBySelector( item => item.Currency),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
