using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.SpecialOfferModule;
using BusinessLogic_v3.Cms.SpecialOfferModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class SpecialOfferBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase>
    {
		public SpecialOfferBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase dbItem)
            : base(BusinessLogic_v3.Cms.SpecialOfferModule.SpecialOfferBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new SpecialOfferBase DbItem
        {
            get
            {
                return (SpecialOfferBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Category { get; private set; }

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo Name { get; private set; }

        public CmsPropertyInfo Activated { get; private set; }

        public CmsPropertyInfo DateFrom { get; private set; }

        public CmsPropertyInfo DateTo { get; private set; }

        public CmsPropertyInfo RequiredTotalItemsToBuy { get; private set; }

        public CmsPropertyInfo RequiredTotalToSpend { get; private set; }

        public CmsPropertyInfo DiscountPercentage { get; private set; }

        public CmsPropertyInfo DiscountFixed { get; private set; }

        public CmsPropertyInfo RequiresPromoCode { get; private set; }

        public CmsPropertyInfo Description { get; private set; }

        public CmsPropertyInfo QuantityLeft { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Category = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase>.GetPropertyBySelector( item => item.Category),
        			isEditable: true,
        			isVisible: true
        			);

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.Name = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase>.GetPropertyBySelector( item => item.Name),
        			isEditable: true,
        			isVisible: true
        			);

        this.Activated = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase>.GetPropertyBySelector( item => item.Activated),
        			isEditable: true,
        			isVisible: true
        			);

        this.DateFrom = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase>.GetPropertyBySelector( item => item.DateFrom),
        			isEditable: true,
        			isVisible: true
        			);

        this.DateTo = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase>.GetPropertyBySelector( item => item.DateTo),
        			isEditable: true,
        			isVisible: true
        			);

        this.RequiredTotalItemsToBuy = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase>.GetPropertyBySelector( item => item.RequiredTotalItemsToBuy),
        			isEditable: true,
        			isVisible: true
        			);

        this.RequiredTotalToSpend = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase>.GetPropertyBySelector( item => item.RequiredTotalToSpend),
        			isEditable: true,
        			isVisible: true
        			);

        this.DiscountPercentage = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase>.GetPropertyBySelector( item => item.DiscountPercentage),
        			isEditable: true,
        			isVisible: true
        			);

        this.DiscountFixed = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase>.GetPropertyBySelector( item => item.DiscountFixed),
        			isEditable: true,
        			isVisible: true
        			);

        this.RequiresPromoCode = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase>.GetPropertyBySelector( item => item.RequiresPromoCode),
        			isEditable: true,
        			isVisible: true
        			);

        this.Description = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase>.GetPropertyBySelector( item => item.Description),
        			isEditable: true,
        			isVisible: true
        			);

        this.QuantityLeft = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SpecialOfferModule.SpecialOfferBase>.GetPropertyBySelector( item => item.QuantityLeft),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
