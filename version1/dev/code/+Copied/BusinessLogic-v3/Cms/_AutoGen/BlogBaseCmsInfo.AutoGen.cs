using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.BlogModule;
using BusinessLogic_v3.Cms.BlogModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class BlogBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.BlogModule.BlogBase>
    {
		public BlogBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.BlogModule.BlogBase dbItem)
            : base(BusinessLogic_v3.Cms.BlogModule.BlogBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new BlogBase DbItem
        {
            get
            {
                return (BlogBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo AuthorName { get; private set; }

        public CmsPropertyInfo AuthorDescription { get; private set; }

        public CmsPropertyInfo ImageFilename { get; private set; }

        public CmsPropertyInfo CmsUser { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BlogModule.BlogBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.AuthorName = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BlogModule.BlogBase>.GetPropertyBySelector( item => item.AuthorName),
        			isEditable: true,
        			isVisible: true
        			);

        this.AuthorDescription = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BlogModule.BlogBase>.GetPropertyBySelector( item => item.AuthorDescription),
        			isEditable: true,
        			isVisible: true
        			);

        this.ImageFilename = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BlogModule.BlogBase>.GetPropertyBySelector( item => item.ImageFilename),
        			isEditable: false,
        			isVisible: false
        			);

        this.CmsUser = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.BlogModule.BlogBase>.GetPropertyBySelector( item => item.CmsUser),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
