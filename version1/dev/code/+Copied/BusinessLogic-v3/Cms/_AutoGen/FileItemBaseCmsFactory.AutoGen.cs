using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.FileItemModule;
using BusinessLogic_v3.Cms.FileItemModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class FileItemBaseCmsFactory_AutoGen : CmsFactoryBase<FileItemBaseCmsInfo, FileItemBase>
    {
       
       public new static FileItemBaseCmsFactory Instance
	    {
	         get
	         {
                 return (FileItemBaseCmsFactory)CmsFactoryBase<FileItemBaseCmsInfo, FileItemBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = FileItemBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "FileItem.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "FileItem";

            this.QueryStringParamID = "FileItemId";

            cmsInfo.TitlePlural = "File Items";

            cmsInfo.TitleSingular =  "File Item";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public FileItemBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "FileItem/";


        }
       
    }

}
