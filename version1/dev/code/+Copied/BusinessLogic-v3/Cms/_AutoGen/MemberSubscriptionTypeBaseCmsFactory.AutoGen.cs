using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.MemberSubscriptionTypeModule;
using BusinessLogic_v3.Cms.MemberSubscriptionTypeModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class MemberSubscriptionTypeBaseCmsFactory_AutoGen : CmsFactoryBase<MemberSubscriptionTypeBaseCmsInfo, MemberSubscriptionTypeBase>
    {
       
       public new static MemberSubscriptionTypeBaseCmsFactory Instance
	    {
	         get
	         {
                 return (MemberSubscriptionTypeBaseCmsFactory)CmsFactoryBase<MemberSubscriptionTypeBaseCmsInfo, MemberSubscriptionTypeBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = MemberSubscriptionTypeBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "MemberSubscriptionType.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "MemberSubscriptionType";

            this.QueryStringParamID = "MemberSubscriptionTypeId";

            cmsInfo.TitlePlural = "Member Subscription Types";

            cmsInfo.TitleSingular =  "Member Subscription Type";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public MemberSubscriptionTypeBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "MemberSubscriptionType/";


        }
       
    }

}
