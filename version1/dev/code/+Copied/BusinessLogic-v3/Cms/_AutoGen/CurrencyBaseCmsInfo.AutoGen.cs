using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.CurrencyModule;
using BusinessLogic_v3.Cms.CurrencyModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class CurrencyBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase>
    {
		public CurrencyBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase dbItem)
            : base(BusinessLogic_v3.Cms.CurrencyModule.CurrencyBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new CurrencyBase DbItem
        {
            get
            {
                return (CurrencyBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo CurrencyISOCode { get; private set; }

        public CmsPropertyInfo Activated { get; private set; }

        public CmsPropertyInfo HtmlCode { get; private set; }

        public CmsPropertyInfo UnicodeText { get; private set; }

        public CmsPropertyInfo ExchangeRateMultiplier { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.CurrencyISOCode = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase>.GetPropertyBySelector( item => item.CurrencyISOCode),
        			isEditable: true,
        			isVisible: true
        			);

        this.Activated = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase>.GetPropertyBySelector( item => item.Activated),
        			isEditable: true,
        			isVisible: true
        			);

        this.HtmlCode = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase>.GetPropertyBySelector( item => item.HtmlCode),
        			isEditable: true,
        			isVisible: true
        			);

        this.UnicodeText = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase>.GetPropertyBySelector( item => item.UnicodeText),
        			isEditable: true,
        			isVisible: true
        			);

        this.ExchangeRateMultiplier = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase>.GetPropertyBySelector( item => item.ExchangeRateMultiplier),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
