using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ItemGroupCategoryFeatureValueModule;
using BusinessLogic_v3.Cms.ItemGroupCategoryFeatureValueModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ItemGroupCategoryFeatureValueBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ItemGroupCategoryFeatureValueModule.ItemGroupCategoryFeatureValueBase>
    {
		public ItemGroupCategoryFeatureValueBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ItemGroupCategoryFeatureValueModule.ItemGroupCategoryFeatureValueBase dbItem)
            : base(BusinessLogic_v3.Cms.ItemGroupCategoryFeatureValueModule.ItemGroupCategoryFeatureValueBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ItemGroupCategoryFeatureValueBase DbItem
        {
            get
            {
                return (ItemGroupCategoryFeatureValueBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo ItemGroup { get; private set; }

        public CmsPropertyInfo CategoryFeature { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.ItemGroup = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroupCategoryFeatureValueModule.ItemGroupCategoryFeatureValueBase>.GetPropertyBySelector( item => item.ItemGroup),
        			isEditable: true,
        			isVisible: true
        			);

        this.CategoryFeature = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ItemGroupCategoryFeatureValueModule.ItemGroupCategoryFeatureValueBase>.GetPropertyBySelector( item => item.CategoryFeature),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
