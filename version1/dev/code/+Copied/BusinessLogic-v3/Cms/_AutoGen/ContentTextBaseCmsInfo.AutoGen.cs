using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ContentTextModule;
using BusinessLogic_v3.Cms.ContentTextModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ContentTextBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase>
    {
		public ContentTextBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase dbItem)
            : base(BusinessLogic_v3.Cms.ContentTextModule.ContentTextBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ContentTextBase DbItem
        {
            get
            {
                return (ContentTextBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Name { get; private set; }

        public CmsPropertyInfo Name_Search { get; private set; }

        public CmsPropertyInfo Identifier { get; private set; }

        public CmsPropertyInfo Parent { get; private set; }

        public CmsPropertyInfo AllowAddSubItems_AccessRequired { get; private set; }

        public CmsPropertyInfo VisibleInCMS_AccessRequired { get; private set; }

        public CmsPropertyInfo ContentPlain { get; private set; }

        public CmsPropertyInfo ContentHTML { get; private set; }

        public CmsPropertyInfo ContentImageFilename { get; private set; }

        public CmsPropertyInfo ContentType { get; private set; }

        public CmsPropertyInfo UsedInProject { get; private set; }



// [basecmsinfo_manytomanydeclarations]

        public CmsPropertyManyToManyCollection<BusinessLogic_v3.Cms.ContentTagModule.ContentTagBaseCmsInfo> ContentTags { get; private set; }



        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Name = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase>.GetPropertyBySelector( item => item.Name),
        			isEditable: true,
        			isVisible: true
        			);

        this.Name_Search = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase>.GetPropertyBySelector( item => item.Name_Search),
        			isEditable: false,
        			isVisible: false
        			);

        this.Identifier = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase>.GetPropertyBySelector( item => item.Identifier),
        			isEditable: true,
        			isVisible: true
        			);

        this.Parent = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase>.GetPropertyBySelector( item => item.Parent),
        			isEditable: true,
        			isVisible: true
        			);

        this.AllowAddSubItems_AccessRequired = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase>.GetPropertyBySelector( item => item.AllowAddSubItems_AccessRequired),
        			isEditable: true,
        			isVisible: true
        			);

        this.VisibleInCMS_AccessRequired = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase>.GetPropertyBySelector( item => item.VisibleInCMS_AccessRequired),
        			isEditable: true,
        			isVisible: true
        			);

        this.ContentPlain = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase>.GetPropertyBySelector( item => item.ContentPlain),
        			isEditable: true,
        			isVisible: true
        			);

        this.ContentHTML = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase>.GetPropertyBySelector( item => item.ContentHTML),
        			isEditable: true,
        			isVisible: true
        			);

        this.ContentImageFilename = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase>.GetPropertyBySelector( item => item.ContentImageFilename),
        			isEditable: false,
        			isVisible: false
        			);

        this.ContentType = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase>.GetPropertyBySelector( item => item.ContentType),
        			isEditable: true,
        			isVisible: true
        			);

        this.UsedInProject = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase>.GetPropertyBySelector( item => item.UsedInProject),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]

//InitFieldManyToManyBase_LeftSide
		this.ContentTags = this.AddManyToManyCollectionWithDirectProperty<BusinessLogic_v3.Cms.ContentTagModule.ContentTagBaseCmsInfo>(
		BusinessLogic_v3.Cms.ContentTagModule.ContentTagBaseCmsFactory.Instance, 
		CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase>.GetPropertyBySelector(item => item.GetContentTags()),
		((item) => ((BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase) item).ClearContentTags()),
            ((item, itemToAdd) => ((BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase) item).AddContentTagToContentTags((BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase)itemToAdd)),
		 Classes.Cms.EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);



			base.initBasicFields();
          
        }

    }
}
