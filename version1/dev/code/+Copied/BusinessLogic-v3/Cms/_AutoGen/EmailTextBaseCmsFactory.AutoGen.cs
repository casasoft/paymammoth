using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.EmailTextModule;
using BusinessLogic_v3.Cms.EmailTextModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class EmailTextBaseCmsFactory_AutoGen : CmsFactoryBase<EmailTextBaseCmsInfo, EmailTextBase>
    {
       
       public new static EmailTextBaseCmsFactory Instance
	    {
	         get
	         {
                 return (EmailTextBaseCmsFactory)CmsFactoryBase<EmailTextBaseCmsInfo, EmailTextBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = EmailTextBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "EmailText.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "EmailText";

            this.QueryStringParamID = "EmailTextId";

            cmsInfo.TitlePlural = "Email Texts";

            cmsInfo.TitleSingular =  "Email Text";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public EmailTextBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "EmailText/";


        }
       
    }

}
