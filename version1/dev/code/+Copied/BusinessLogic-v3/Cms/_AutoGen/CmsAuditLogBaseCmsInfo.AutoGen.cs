using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.CmsAuditLogModule;
using BusinessLogic_v3.Cms.CmsAuditLogModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class CmsAuditLogBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.CmsAuditLogModule.CmsAuditLogBase>
    {
		public CmsAuditLogBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.CmsAuditLogModule.CmsAuditLogBase dbItem)
            : base(BusinessLogic_v3.Cms.CmsAuditLogModule.CmsAuditLogBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new CmsAuditLogBase DbItem
        {
            get
            {
                return (CmsAuditLogBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo CmsUser { get; private set; }

        public CmsPropertyInfo DateTime { get; private set; }

        public CmsPropertyInfo Message { get; private set; }

        public CmsPropertyInfo FurtherInformation { get; private set; }

        public CmsPropertyInfo MsgType { get; private set; }

        public CmsPropertyInfo IPAddress { get; private set; }

        public CmsPropertyInfo ObjectName { get; private set; }

        public CmsPropertyInfo Remarks { get; private set; }

        public CmsPropertyInfo ItemID { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.CmsUser = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CmsAuditLogModule.CmsAuditLogBase>.GetPropertyBySelector( item => item.CmsUser),
        			isEditable: true,
        			isVisible: true
        			);

        this.DateTime = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CmsAuditLogModule.CmsAuditLogBase>.GetPropertyBySelector( item => item.DateTime),
        			isEditable: true,
        			isVisible: true
        			);

        this.Message = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CmsAuditLogModule.CmsAuditLogBase>.GetPropertyBySelector( item => item.Message),
        			isEditable: true,
        			isVisible: true
        			);

        this.FurtherInformation = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CmsAuditLogModule.CmsAuditLogBase>.GetPropertyBySelector( item => item.FurtherInformation),
        			isEditable: true,
        			isVisible: true
        			);

        this.MsgType = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CmsAuditLogModule.CmsAuditLogBase>.GetPropertyBySelector( item => item.MsgType),
        			isEditable: true,
        			isVisible: true
        			);

        this.IPAddress = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CmsAuditLogModule.CmsAuditLogBase>.GetPropertyBySelector( item => item.IPAddress),
        			isEditable: true,
        			isVisible: true
        			);

        this.ObjectName = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CmsAuditLogModule.CmsAuditLogBase>.GetPropertyBySelector( item => item.ObjectName),
        			isEditable: true,
        			isVisible: true
        			);

        this.Remarks = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CmsAuditLogModule.CmsAuditLogBase>.GetPropertyBySelector( item => item.Remarks),
        			isEditable: true,
        			isVisible: true
        			);

        this.ItemID = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CmsAuditLogModule.CmsAuditLogBase>.GetPropertyBySelector( item => item.ItemID),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
