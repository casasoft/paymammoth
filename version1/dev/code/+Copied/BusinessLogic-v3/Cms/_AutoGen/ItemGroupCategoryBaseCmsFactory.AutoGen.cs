using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.ItemGroupCategoryModule;
using BusinessLogic_v3.Cms.ItemGroupCategoryModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class ItemGroupCategoryBaseCmsFactory_AutoGen : CmsFactoryBase<ItemGroupCategoryBaseCmsInfo, ItemGroupCategoryBase>
    {
       
       public new static ItemGroupCategoryBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ItemGroupCategoryBaseCmsFactory)CmsFactoryBase<ItemGroupCategoryBaseCmsInfo, ItemGroupCategoryBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = ItemGroupCategoryBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ItemGroupCategory.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ItemGroupCategory";

            this.QueryStringParamID = "ItemGroupCategoryId";

            cmsInfo.TitlePlural = "Item Group Categories";

            cmsInfo.TitleSingular =  "Item Group Category";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ItemGroupCategoryBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "ItemGroupCategory/";


        }
       
    }

}
