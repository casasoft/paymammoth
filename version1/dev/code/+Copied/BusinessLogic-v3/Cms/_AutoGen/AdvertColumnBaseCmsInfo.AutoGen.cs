using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.AdvertColumnModule;
using BusinessLogic_v3.Cms.AdvertColumnModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class AdvertColumnBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.AdvertColumnModule.AdvertColumnBase>
    {
		public AdvertColumnBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.AdvertColumnModule.AdvertColumnBase dbItem)
            : base(BusinessLogic_v3.Cms.AdvertColumnModule.AdvertColumnBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new AdvertColumnBase DbItem
        {
            get
            {
                return (AdvertColumnBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo Identifier { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertColumnModule.AdvertColumnBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.Identifier = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertColumnModule.AdvertColumnBase>.GetPropertyBySelector( item => item.Identifier),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
