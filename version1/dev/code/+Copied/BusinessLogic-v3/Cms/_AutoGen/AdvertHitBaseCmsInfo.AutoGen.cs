using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.AdvertHitModule;
using BusinessLogic_v3.Cms.AdvertHitModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class AdvertHitBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.AdvertHitModule.AdvertHitBase>
    {
		public AdvertHitBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.AdvertHitModule.AdvertHitBase dbItem)
            : base(BusinessLogic_v3.Cms.AdvertHitModule.AdvertHitBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new AdvertHitBase DbItem
        {
            get
            {
                return (AdvertHitBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo HitType { get; private set; }

        public CmsPropertyInfo UserIP { get; private set; }

        public CmsPropertyInfo DateTime { get; private set; }

        public CmsPropertyInfo Advert { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.HitType = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertHitModule.AdvertHitBase>.GetPropertyBySelector( item => item.HitType),
        			isEditable: true,
        			isVisible: true
        			);

        this.UserIP = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertHitModule.AdvertHitBase>.GetPropertyBySelector( item => item.UserIP),
        			isEditable: true,
        			isVisible: true
        			);

        this.DateTime = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertHitModule.AdvertHitBase>.GetPropertyBySelector( item => item.DateTime),
        			isEditable: true,
        			isVisible: true
        			);

        this.Advert = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.AdvertHitModule.AdvertHitBase>.GetPropertyBySelector( item => item.Advert),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
