using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ContentPageModule;
using BusinessLogic_v3.Cms.ContentPageModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ContentPageBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>
    {
		public ContentPageBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase dbItem)
            : base(BusinessLogic_v3.Cms.ContentPageModule.ContentPageBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ContentPageBase DbItem
        {
            get
            {
                return (ContentPageBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Identifier { get; private set; }

        public CmsPropertyInfo AllowUpdate_AccessRequired { get; private set; }

        public CmsPropertyInfo AllowDelete_AccessRequired { get; private set; }

        public CmsPropertyInfo AllowAddChildren_AccessRequired { get; private set; }

        public CmsPropertyInfo AllowAddSubChildren_AccessRequired { get; private set; }

        public CmsPropertyInfo VisibleInFrontend { get; private set; }

        public CmsPropertyInfo CustomLink { get; private set; }

        public CmsPropertyInfo HtmlText { get; private set; }

        public CmsPropertyInfo HtmlText_Search { get; private set; }

        public CmsPropertyInfo MetaKeywords { get; private set; }

        public CmsPropertyInfo MetaKeywords_Search { get; private set; }

        public CmsPropertyInfo MetaDescription { get; private set; }

        public CmsPropertyInfo MetaDescription_Search { get; private set; }

        public CmsPropertyInfo Color { get; private set; }

        public CmsPropertyInfo Name { get; private set; }

        public CmsPropertyInfo PageTitle { get; private set; }

        public CmsPropertyInfo PageTitle_Search { get; private set; }

        public CmsPropertyInfo ExcludeFromRewriteUrl { get; private set; }

        public CmsPropertyInfo ImageFilename { get; private set; }

        public CmsPropertyInfo ShowAdverts { get; private set; }

        public CmsPropertyInfo VisibleInCMS_AccessRequired { get; private set; }

        public CmsPropertyInfo _LastEditedBy { get; private set; }

        public CmsPropertyInfo _LastEditedOn { get; private set; }

        public CmsPropertyInfo DoNotShowLastEditedOn { get; private set; }

        public CmsPropertyInfo ViewCount { get; private set; }

        public CmsPropertyInfo Rating { get; private set; }

        public CmsPropertyInfo IsFeatured { get; private set; }

        public CmsPropertyInfo CustomRewriteURL { get; private set; }

        public CmsPropertyInfo MetaTitle { get; private set; }

        public CmsPropertyInfo NoFollow { get; private set; }

        public CmsPropertyInfo NoIndex { get; private set; }

        public CmsPropertyInfo UsedInProject { get; private set; }



// [basecmsinfo_manytomanydeclarations]

        public CmsPropertyManyToManyCollection<BusinessLogic_v3.Cms.ContentTagModule.ContentTagBaseCmsInfo> ContentTags { get; private set; }



        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Identifier = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.Identifier),
        			isEditable: true,
        			isVisible: true
        			);

        this.AllowUpdate_AccessRequired = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.AllowUpdate_AccessRequired),
        			isEditable: true,
        			isVisible: true
        			);

        this.AllowDelete_AccessRequired = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.AllowDelete_AccessRequired),
        			isEditable: true,
        			isVisible: true
        			);

        this.AllowAddChildren_AccessRequired = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.AllowAddChildren_AccessRequired),
        			isEditable: true,
        			isVisible: true
        			);

        this.AllowAddSubChildren_AccessRequired = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.AllowAddSubChildren_AccessRequired),
        			isEditable: true,
        			isVisible: true
        			);

        this.VisibleInFrontend = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.VisibleInFrontend),
        			isEditable: true,
        			isVisible: true
        			);

        this.CustomLink = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.CustomLink),
        			isEditable: true,
        			isVisible: true
        			);

        this.HtmlText = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.HtmlText),
        			isEditable: true,
        			isVisible: true
        			);

        this.HtmlText_Search = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.HtmlText_Search),
        			isEditable: false,
        			isVisible: false
        			);

        this.MetaKeywords = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.MetaKeywords),
        			isEditable: true,
        			isVisible: true
        			);

        this.MetaKeywords_Search = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.MetaKeywords_Search),
        			isEditable: false,
        			isVisible: false
        			);

        this.MetaDescription = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.MetaDescription),
        			isEditable: true,
        			isVisible: true
        			);

        this.MetaDescription_Search = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.MetaDescription_Search),
        			isEditable: false,
        			isVisible: false
        			);

        this.Color = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.Color),
        			isEditable: true,
        			isVisible: true
        			);

        this.Name = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.Name),
        			isEditable: true,
        			isVisible: true
        			);

        this.PageTitle = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.PageTitle),
        			isEditable: true,
        			isVisible: true
        			);

        this.PageTitle_Search = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.PageTitle_Search),
        			isEditable: false,
        			isVisible: false
        			);

        this.ExcludeFromRewriteUrl = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.ExcludeFromRewriteUrl),
        			isEditable: true,
        			isVisible: true
        			);

        this.ImageFilename = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.ImageFilename),
        			isEditable: false,
        			isVisible: false
        			);

        this.ShowAdverts = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.ShowAdverts),
        			isEditable: true,
        			isVisible: true
        			);

        this.VisibleInCMS_AccessRequired = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.VisibleInCMS_AccessRequired),
        			isEditable: true,
        			isVisible: true
        			);

        this._LastEditedBy = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item._LastEditedBy),
        			isEditable: true,
        			isVisible: true
        			);

        this._LastEditedOn = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item._LastEditedOn),
        			isEditable: true,
        			isVisible: true
        			);

        this.DoNotShowLastEditedOn = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.DoNotShowLastEditedOn),
        			isEditable: true,
        			isVisible: true
        			);

        this.ViewCount = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.ViewCount),
        			isEditable: true,
        			isVisible: true
        			);

        this.Rating = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.Rating),
        			isEditable: true,
        			isVisible: true
        			);

        this.IsFeatured = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.IsFeatured),
        			isEditable: true,
        			isVisible: true
        			);

        this.CustomRewriteURL = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.CustomRewriteURL),
        			isEditable: true,
        			isVisible: true
        			);

        this.MetaTitle = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.MetaTitle),
        			isEditable: true,
        			isVisible: true
        			);

        this.NoFollow = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.NoFollow),
        			isEditable: true,
        			isVisible: true
        			);

        this.NoIndex = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.NoIndex),
        			isEditable: true,
        			isVisible: true
        			);

        this.UsedInProject = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector( item => item.UsedInProject),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]

//InitFieldManyToManyBase_LeftSide
		this.ContentTags = this.AddManyToManyCollectionWithDirectProperty<BusinessLogic_v3.Cms.ContentTagModule.ContentTagBaseCmsInfo>(
		BusinessLogic_v3.Cms.ContentTagModule.ContentTagBaseCmsFactory.Instance, 
		CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase>.GetPropertyBySelector(item => item.GetContentTags()),
		((item) => ((BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase) item).ClearContentTags()),
            ((item, itemToAdd) => ((BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase) item).AddContentTagToContentTags((BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase)itemToAdd)),
		 Classes.Cms.EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);



			base.initBasicFields();
          
        }

    }
}
