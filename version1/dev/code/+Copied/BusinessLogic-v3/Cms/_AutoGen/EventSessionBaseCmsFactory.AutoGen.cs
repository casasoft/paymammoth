using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.EventSessionModule;
using BusinessLogic_v3.Cms.EventSessionModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class EventSessionBaseCmsFactory_AutoGen : CmsFactoryBase<EventSessionBaseCmsInfo, EventSessionBase>
    {
       
       public new static EventSessionBaseCmsFactory Instance
	    {
	         get
	         {
                 return (EventSessionBaseCmsFactory)CmsFactoryBase<EventSessionBaseCmsInfo, EventSessionBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = EventSessionBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "EventSession.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "EventSession";

            this.QueryStringParamID = "EventSessionId";

            cmsInfo.TitlePlural = "Event Sessions";

            cmsInfo.TitleSingular =  "Event Session";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public EventSessionBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "EventSession/";


        }
       
    }

}
