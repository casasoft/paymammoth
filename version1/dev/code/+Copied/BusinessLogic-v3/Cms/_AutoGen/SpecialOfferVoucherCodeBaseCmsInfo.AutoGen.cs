using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule;
using BusinessLogic_v3.Cms.SpecialOfferVoucherCodeModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class SpecialOfferVoucherCodeBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase>
    {
		public SpecialOfferVoucherCodeBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase dbItem)
            : base(BusinessLogic_v3.Cms.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new SpecialOfferVoucherCodeBase DbItem
        {
            get
            {
                return (SpecialOfferVoucherCodeBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo SpecialOffer { get; private set; }

        public CmsPropertyInfo QuantityLeft { get; private set; }

        public CmsPropertyInfo VoucherCode { get; private set; }

        public CmsPropertyInfo CreatedOn { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.SpecialOffer = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase>.GetPropertyBySelector( item => item.SpecialOffer),
        			isEditable: true,
        			isVisible: true
        			);

        this.QuantityLeft = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase>.GetPropertyBySelector( item => item.QuantityLeft),
        			isEditable: true,
        			isVisible: true
        			);

        this.VoucherCode = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase>.GetPropertyBySelector( item => item.VoucherCode),
        			isEditable: true,
        			isVisible: true
        			);

        this.CreatedOn = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule.SpecialOfferVoucherCodeBase>.GetPropertyBySelector( item => item.CreatedOn),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
