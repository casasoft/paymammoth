using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.MemberSubscriptionTypeModule;
using BusinessLogic_v3.Cms.MemberSubscriptionTypeModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class MemberSubscriptionTypeBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.MemberSubscriptionTypeModule.MemberSubscriptionTypeBase>
    {
		public MemberSubscriptionTypeBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.MemberSubscriptionTypeModule.MemberSubscriptionTypeBase dbItem)
            : base(BusinessLogic_v3.Cms.MemberSubscriptionTypeModule.MemberSubscriptionTypeBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new MemberSubscriptionTypeBase DbItem
        {
            get
            {
                return (MemberSubscriptionTypeBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo Description { get; private set; }

        public CmsPropertyInfo MemberSubscriptionType { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberSubscriptionTypeModule.MemberSubscriptionTypeBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.Description = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberSubscriptionTypeModule.MemberSubscriptionTypeBase>.GetPropertyBySelector( item => item.Description),
        			isEditable: true,
        			isVisible: true
        			);

        this.MemberSubscriptionType = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.MemberSubscriptionTypeModule.MemberSubscriptionTypeBase>.GetPropertyBySelector( item => item.MemberSubscriptionType),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
