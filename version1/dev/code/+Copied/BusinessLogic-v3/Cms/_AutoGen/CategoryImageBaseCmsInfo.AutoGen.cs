using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.CategoryImageModule;
using BusinessLogic_v3.Cms.CategoryImageModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class CategoryImageBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.CategoryImageModule.CategoryImageBase>
    {
		public CategoryImageBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.CategoryImageModule.CategoryImageBase dbItem)
            : base(BusinessLogic_v3.Cms.CategoryImageModule.CategoryImageBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new CategoryImageBase DbItem
        {
            get
            {
                return (CategoryImageBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Category { get; private set; }

        public CmsPropertyInfo ImageFilename { get; private set; }

        public CmsPropertyInfo Title { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Category = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryImageModule.CategoryImageBase>.GetPropertyBySelector( item => item.Category),
        			isEditable: true,
        			isVisible: true
        			);

        this.ImageFilename = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryImageModule.CategoryImageBase>.GetPropertyBySelector( item => item.ImageFilename),
        			isEditable: false,
        			isVisible: false
        			);

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.CategoryImageModule.CategoryImageBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
