using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.ImageSizingInfoModule;
using BusinessLogic_v3.Cms.ImageSizingInfoModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class ImageSizingInfoBaseCmsFactory_AutoGen : CmsFactoryBase<ImageSizingInfoBaseCmsInfo, ImageSizingInfoBase>
    {
       
       public new static ImageSizingInfoBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ImageSizingInfoBaseCmsFactory)CmsFactoryBase<ImageSizingInfoBaseCmsInfo, ImageSizingInfoBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = ImageSizingInfoBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ImageSizingInfo.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ImageSizingInfo";

            this.QueryStringParamID = "ImageSizingInfoId";

            cmsInfo.TitlePlural = "Image Sizing Infos";

            cmsInfo.TitleSingular =  "Image Sizing Info";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ImageSizingInfoBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "ImageSizingInfo/";


        }
       
    }

}
