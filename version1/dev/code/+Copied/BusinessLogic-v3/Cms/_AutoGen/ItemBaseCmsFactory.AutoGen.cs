using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.ItemModule;
using BusinessLogic_v3.Cms.ItemModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class ItemBaseCmsFactory_AutoGen : CmsFactoryBase<ItemBaseCmsInfo, ItemBase>
    {
       
       public new static ItemBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ItemBaseCmsFactory)CmsFactoryBase<ItemBaseCmsInfo, ItemBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = ItemBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Item.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Item";

            this.QueryStringParamID = "ItemId";

            cmsInfo.TitlePlural = "Items";

            cmsInfo.TitleSingular =  "Item";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ItemBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "Item/";


        }
       
    }

}
