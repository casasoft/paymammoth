using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.Category_CultureInfoModule;
using BusinessLogic_v3.Cms.Category_CultureInfoModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class Category_CultureInfoBaseCmsFactory_AutoGen : CmsFactoryBase<Category_CultureInfoBaseCmsInfo, Category_CultureInfoBase>
    {
       
       public new static Category_CultureInfoBaseCmsFactory Instance
	    {
	         get
	         {
                 return (Category_CultureInfoBaseCmsFactory)CmsFactoryBase<Category_CultureInfoBaseCmsInfo, Category_CultureInfoBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = Category_CultureInfoBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Category_CultureInfo.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Category_CultureInfo";

            this.QueryStringParamID = "Category_CultureInfoId";

            cmsInfo.TitlePlural = "Category _ Culture Infos";

            cmsInfo.TitleSingular =  "Category _ Culture Info";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public Category_CultureInfoBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "Category_CultureInfo/";


        }
       
    }

}
