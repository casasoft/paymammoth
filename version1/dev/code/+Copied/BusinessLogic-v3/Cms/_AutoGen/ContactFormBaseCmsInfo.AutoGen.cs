using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ContactFormModule;
using BusinessLogic_v3.Cms.ContactFormModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ContactFormBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>
    {
		public ContactFormBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase dbItem)
            : base(BusinessLogic_v3.Cms.ContactFormModule.ContactFormBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ContactFormBase DbItem
        {
            get
            {
                return (ContactFormBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo DateSent { get; private set; }

        public CmsPropertyInfo Name { get; private set; }

        public CmsPropertyInfo Company { get; private set; }

        public CmsPropertyInfo Subject { get; private set; }

        public CmsPropertyInfo Email { get; private set; }

        public CmsPropertyInfo Telephone { get; private set; }

        public CmsPropertyInfo Mobile { get; private set; }

        public CmsPropertyInfo Address1 { get; private set; }

        public CmsPropertyInfo Address2 { get; private set; }

        public CmsPropertyInfo Address3 { get; private set; }

        public CmsPropertyInfo ZIPCode { get; private set; }

        public CmsPropertyInfo State { get; private set; }

        public CmsPropertyInfo CountryStr { get; private set; }

        public CmsPropertyInfo City { get; private set; }

        public CmsPropertyInfo Fax { get; private set; }

        public CmsPropertyInfo Website { get; private set; }

        public CmsPropertyInfo IP { get; private set; }

        public CmsPropertyInfo Enquiry { get; private set; }

        public CmsPropertyInfo Remark { get; private set; }

        public CmsPropertyInfo SentByMember { get; private set; }

        public CmsPropertyInfo Surname { get; private set; }

        public CmsPropertyInfo Vacancy { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.DateSent = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.GetPropertyBySelector( item => item.DateSent),
        			isEditable: true,
        			isVisible: true
        			);

        this.Name = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.GetPropertyBySelector( item => item.Name),
        			isEditable: true,
        			isVisible: true
        			);

        this.Company = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.GetPropertyBySelector( item => item.Company),
        			isEditable: true,
        			isVisible: true
        			);

        this.Subject = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.GetPropertyBySelector( item => item.Subject),
        			isEditable: true,
        			isVisible: true
        			);

        this.Email = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.GetPropertyBySelector( item => item.Email),
        			isEditable: true,
        			isVisible: true
        			);

        this.Telephone = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.GetPropertyBySelector( item => item.Telephone),
        			isEditable: true,
        			isVisible: true
        			);

        this.Mobile = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.GetPropertyBySelector( item => item.Mobile),
        			isEditable: true,
        			isVisible: true
        			);

        this.Address1 = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.GetPropertyBySelector( item => item.Address1),
        			isEditable: true,
        			isVisible: true
        			);

        this.Address2 = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.GetPropertyBySelector( item => item.Address2),
        			isEditable: true,
        			isVisible: true
        			);

        this.Address3 = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.GetPropertyBySelector( item => item.Address3),
        			isEditable: true,
        			isVisible: true
        			);

        this.ZIPCode = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.GetPropertyBySelector( item => item.ZIPCode),
        			isEditable: true,
        			isVisible: true
        			);

        this.State = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.GetPropertyBySelector( item => item.State),
        			isEditable: true,
        			isVisible: true
        			);

        this.CountryStr = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.GetPropertyBySelector( item => item.CountryStr),
        			isEditable: true,
        			isVisible: true
        			);

        this.City = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.GetPropertyBySelector( item => item.City),
        			isEditable: true,
        			isVisible: true
        			);

        this.Fax = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.GetPropertyBySelector( item => item.Fax),
        			isEditable: true,
        			isVisible: true
        			);

        this.Website = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.GetPropertyBySelector( item => item.Website),
        			isEditable: true,
        			isVisible: true
        			);

        this.IP = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.GetPropertyBySelector( item => item.IP),
        			isEditable: true,
        			isVisible: true
        			);

        this.Enquiry = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.GetPropertyBySelector( item => item.Enquiry),
        			isEditable: true,
        			isVisible: true
        			);

        this.Remark = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.GetPropertyBySelector( item => item.Remark),
        			isEditable: true,
        			isVisible: true
        			);

        this.SentByMember = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.GetPropertyBySelector( item => item.SentByMember),
        			isEditable: true,
        			isVisible: true
        			);

        this.Surname = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.GetPropertyBySelector( item => item.Surname),
        			isEditable: true,
        			isVisible: true
        			);

        this.Vacancy = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase>.GetPropertyBySelector( item => item.Vacancy),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
