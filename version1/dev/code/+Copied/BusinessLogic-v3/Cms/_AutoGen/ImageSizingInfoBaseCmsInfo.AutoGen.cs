using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ImageSizingInfoModule;
using BusinessLogic_v3.Cms.ImageSizingInfoModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ImageSizingInfoBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ImageSizingInfoModule.ImageSizingInfoBase>
    {
		public ImageSizingInfoBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ImageSizingInfoModule.ImageSizingInfoBase dbItem)
            : base(BusinessLogic_v3.Cms.ImageSizingInfoModule.ImageSizingInfoBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ImageSizingInfoBase DbItem
        {
            get
            {
                return (ImageSizingInfoBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Identifier { get; private set; }

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo Description { get; private set; }

        public CmsPropertyInfo MaximumWidth { get; private set; }

        public CmsPropertyInfo MaximumHeight { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Identifier = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ImageSizingInfoModule.ImageSizingInfoBase>.GetPropertyBySelector( item => item.Identifier),
        			isEditable: true,
        			isVisible: true
        			);

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ImageSizingInfoModule.ImageSizingInfoBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.Description = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ImageSizingInfoModule.ImageSizingInfoBase>.GetPropertyBySelector( item => item.Description),
        			isEditable: true,
        			isVisible: true
        			);

        this.MaximumWidth = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ImageSizingInfoModule.ImageSizingInfoBase>.GetPropertyBySelector( item => item.MaximumWidth),
        			isEditable: true,
        			isVisible: true
        			);

        this.MaximumHeight = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ImageSizingInfoModule.ImageSizingInfoBase>.GetPropertyBySelector( item => item.MaximumHeight),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
