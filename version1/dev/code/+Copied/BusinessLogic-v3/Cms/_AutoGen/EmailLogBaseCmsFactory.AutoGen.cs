using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.EmailLogModule;
using BusinessLogic_v3.Cms.EmailLogModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class EmailLogBaseCmsFactory_AutoGen : CmsFactoryBase<EmailLogBaseCmsInfo, EmailLogBase>
    {
       
       public new static EmailLogBaseCmsFactory Instance
	    {
	         get
	         {
                 return (EmailLogBaseCmsFactory)CmsFactoryBase<EmailLogBaseCmsInfo, EmailLogBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = EmailLogBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "EmailLog.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "EmailLog";

            this.QueryStringParamID = "EmailLogId";

            cmsInfo.TitlePlural = "Email Logs";

            cmsInfo.TitleSingular =  "Email Log";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public EmailLogBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "EmailLog/";


        }
       
    }

}
