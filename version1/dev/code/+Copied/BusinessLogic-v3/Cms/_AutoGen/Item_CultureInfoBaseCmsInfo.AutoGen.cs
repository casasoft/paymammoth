using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.Item_CultureInfoModule;
using BusinessLogic_v3.Cms.Item_CultureInfoModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class Item_CultureInfoBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.Item_CultureInfoModule.Item_CultureInfoBase>
    {
		public Item_CultureInfoBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.Item_CultureInfoModule.Item_CultureInfoBase dbItem)
            : base(BusinessLogic_v3.Cms.Item_CultureInfoModule.Item_CultureInfoBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new Item_CultureInfoBase DbItem
        {
            get
            {
                return (Item_CultureInfoBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo CultureInfo { get; private set; }

        public CmsPropertyInfo Item { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.CultureInfo = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Item_CultureInfoModule.Item_CultureInfoBase>.GetPropertyBySelector( item => item.CultureInfo),
        			isEditable: true,
        			isVisible: true
        			);

        this.Item = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Item_CultureInfoModule.Item_CultureInfoBase>.GetPropertyBySelector( item => item.Item),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
