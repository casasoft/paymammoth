using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ContentPageMediaItemModule;
using BusinessLogic_v3.Cms.ContentPageMediaItemModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ContentPageMediaItemBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ContentPageMediaItemModule.ContentPageMediaItemBase>
    {
		public ContentPageMediaItemBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ContentPageMediaItemModule.ContentPageMediaItemBase dbItem)
            : base(BusinessLogic_v3.Cms.ContentPageMediaItemModule.ContentPageMediaItemBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ContentPageMediaItemBase DbItem
        {
            get
            {
                return (ContentPageMediaItemBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Caption { get; private set; }

        public CmsPropertyInfo ContentPage { get; private set; }

        public CmsPropertyInfo VideoDuration { get; private set; }

        public CmsPropertyInfo Filename { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Caption = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageMediaItemModule.ContentPageMediaItemBase>.GetPropertyBySelector( item => item.Caption),
        			isEditable: true,
        			isVisible: true
        			);

        this.ContentPage = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageMediaItemModule.ContentPageMediaItemBase>.GetPropertyBySelector( item => item.ContentPage),
        			isEditable: true,
        			isVisible: true
        			);

        this.VideoDuration = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageMediaItemModule.ContentPageMediaItemBase>.GetPropertyBySelector( item => item.VideoDuration),
        			isEditable: true,
        			isVisible: true
        			);

        this.Filename = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentPageMediaItemModule.ContentPageMediaItemBase>.GetPropertyBySelector( item => item.Filename),
        			isEditable: false,
        			isVisible: false
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
