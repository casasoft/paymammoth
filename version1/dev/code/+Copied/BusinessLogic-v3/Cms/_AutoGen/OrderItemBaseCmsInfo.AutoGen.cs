using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.OrderItemModule;
using BusinessLogic_v3.Cms.OrderItemModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class OrderItemBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>
    {
		public OrderItemBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase dbItem)
            : base(BusinessLogic_v3.Cms.OrderItemModule.OrderItemBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new OrderItemBase DbItem
        {
            get
            {
                return (OrderItemBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo Quantity { get; private set; }

        public CmsPropertyInfo PriceExcTaxPerUnit { get; private set; }

        public CmsPropertyInfo TaxAmountPerUnit { get; private set; }

        public CmsPropertyInfo ItemReference { get; private set; }

        public CmsPropertyInfo Remarks { get; private set; }

        public CmsPropertyInfo DiscountPerUnit { get; private set; }

        public CmsPropertyInfo ItemID { get; private set; }

        public CmsPropertyInfo ItemType { get; private set; }

        public CmsPropertyInfo ShipmentWeight { get; private set; }

        public CmsPropertyInfo ShippingCost { get; private set; }

        public CmsPropertyInfo HandlingCost { get; private set; }

        public CmsPropertyInfo Order { get; private set; }

        public CmsPropertyInfo SpecialOffer { get; private set; }

        public CmsPropertyInfo SpecialOfferVoucherCode { get; private set; }

        public CmsPropertyInfo LinkedItem { get; private set; }

        public CmsPropertyInfo ItemGroup { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.Quantity = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>.GetPropertyBySelector( item => item.Quantity),
        			isEditable: true,
        			isVisible: true
        			);

        this.PriceExcTaxPerUnit = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>.GetPropertyBySelector( item => item.PriceExcTaxPerUnit),
        			isEditable: true,
        			isVisible: true
        			);

        this.TaxAmountPerUnit = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>.GetPropertyBySelector( item => item.TaxAmountPerUnit),
        			isEditable: true,
        			isVisible: true
        			);

        this.ItemReference = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>.GetPropertyBySelector( item => item.ItemReference),
        			isEditable: true,
        			isVisible: true
        			);

        this.Remarks = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>.GetPropertyBySelector( item => item.Remarks),
        			isEditable: true,
        			isVisible: true
        			);

        this.DiscountPerUnit = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>.GetPropertyBySelector( item => item.DiscountPerUnit),
        			isEditable: true,
        			isVisible: true
        			);

        this.ItemID = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>.GetPropertyBySelector( item => item.ItemID),
        			isEditable: true,
        			isVisible: true
        			);

        this.ItemType = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>.GetPropertyBySelector( item => item.ItemType),
        			isEditable: true,
        			isVisible: true
        			);

        this.ShipmentWeight = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>.GetPropertyBySelector( item => item.ShipmentWeight),
        			isEditable: true,
        			isVisible: true
        			);

        this.ShippingCost = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>.GetPropertyBySelector( item => item.ShippingCost),
        			isEditable: true,
        			isVisible: true
        			);

        this.HandlingCost = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>.GetPropertyBySelector( item => item.HandlingCost),
        			isEditable: true,
        			isVisible: true
        			);

        this.Order = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>.GetPropertyBySelector( item => item.Order),
        			isEditable: true,
        			isVisible: true
        			);

        this.SpecialOffer = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>.GetPropertyBySelector( item => item.SpecialOffer),
        			isEditable: true,
        			isVisible: true
        			);

        this.SpecialOfferVoucherCode = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>.GetPropertyBySelector( item => item.SpecialOfferVoucherCode),
        			isEditable: true,
        			isVisible: true
        			);

        this.LinkedItem = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>.GetPropertyBySelector( item => item.LinkedItem),
        			isEditable: true,
        			isVisible: true
        			);

        this.ItemGroup = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase>.GetPropertyBySelector( item => item.ItemGroup),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
