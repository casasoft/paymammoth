using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.FileItemModule;
using BusinessLogic_v3.Cms.FileItemModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class FileItemBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.FileItemModule.FileItemBase>
    {
		public FileItemBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.FileItemModule.FileItemBase dbItem)
            : base(BusinessLogic_v3.Cms.FileItemModule.FileItemBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new FileItemBase DbItem
        {
            get
            {
                return (FileItemBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo UploadedOn { get; private set; }

        public CmsPropertyInfo UploadedBy { get; private set; }

        public CmsPropertyInfo UploadFilename { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.FileItemModule.FileItemBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.UploadedOn = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.FileItemModule.FileItemBase>.GetPropertyBySelector( item => item.UploadedOn),
        			isEditable: true,
        			isVisible: true
        			);

        this.UploadedBy = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.FileItemModule.FileItemBase>.GetPropertyBySelector( item => item.UploadedBy),
        			isEditable: true,
        			isVisible: true
        			);

        this.UploadFilename = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.FileItemModule.FileItemBase>.GetPropertyBySelector( item => item.UploadFilename),
        			isEditable: false,
        			isVisible: false
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
