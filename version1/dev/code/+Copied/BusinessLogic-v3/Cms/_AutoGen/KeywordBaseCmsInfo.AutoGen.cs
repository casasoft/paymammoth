using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.KeywordModule;
using BusinessLogic_v3.Cms.KeywordModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class KeywordBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.KeywordModule.KeywordBase>
    {
		public KeywordBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.KeywordModule.KeywordBase dbItem)
            : base(BusinessLogic_v3.Cms.KeywordModule.KeywordBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new KeywordBase DbItem
        {
            get
            {
                return (KeywordBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Keyword { get; private set; }

        public CmsPropertyInfo FrequencyCount { get; private set; }

        public CmsPropertyInfo CultureInfo { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Keyword = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.KeywordModule.KeywordBase>.GetPropertyBySelector( item => item.Keyword),
        			isEditable: true,
        			isVisible: true
        			);

        this.FrequencyCount = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.KeywordModule.KeywordBase>.GetPropertyBySelector( item => item.FrequencyCount),
        			isEditable: true,
        			isVisible: true
        			);

        this.CultureInfo = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.KeywordModule.KeywordBase>.GetPropertyBySelector( item => item.CultureInfo),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
