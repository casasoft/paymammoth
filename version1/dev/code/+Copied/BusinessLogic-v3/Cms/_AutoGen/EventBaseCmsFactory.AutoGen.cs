using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.EventModule;
using BusinessLogic_v3.Cms.EventModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class EventBaseCmsFactory_AutoGen : CmsFactoryBase<EventBaseCmsInfo, EventBase>
    {
       
       public new static EventBaseCmsFactory Instance
	    {
	         get
	         {
                 return (EventBaseCmsFactory)CmsFactoryBase<EventBaseCmsInfo, EventBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = EventBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "Event.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "Event";

            this.QueryStringParamID = "EventId";

            cmsInfo.TitlePlural = "Events";

            cmsInfo.TitleSingular =  "Event";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public EventBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "Event/";


        }
       
    }

}
