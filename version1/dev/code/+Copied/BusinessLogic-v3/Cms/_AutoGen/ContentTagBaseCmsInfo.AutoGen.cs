using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ContentTagModule;
using BusinessLogic_v3.Cms.ContentTagModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class ContentTagBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase>
    {
		public ContentTagBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase dbItem)
            : base(BusinessLogic_v3.Cms.ContentTagModule.ContentTagBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new ContentTagBase DbItem
        {
            get
            {
                return (ContentTagBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo TagName { get; private set; }

        public CmsPropertyInfo Description { get; private set; }

        public CmsPropertyInfo TagType { get; private set; }



// [basecmsinfo_manytomanydeclarations]

        public CmsPropertyManyToManyCollection<BusinessLogic_v3.Cms.ContentPageModule.ContentPageBaseCmsInfo> ContentPages { get; private set; }
        public CmsPropertyManyToManyCollection<BusinessLogic_v3.Cms.ContentTextModule.ContentTextBaseCmsInfo> ContentTexts { get; private set; }
        public CmsPropertyManyToManyCollection<BusinessLogic_v3.Cms.EmailTextModule.EmailTextBaseCmsInfo> EmailTexts { get; private set; }



        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.TagName = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase>.GetPropertyBySelector( item => item.TagName),
        			isEditable: true,
        			isVisible: true
        			);

        this.Description = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase>.GetPropertyBySelector( item => item.Description),
        			isEditable: true,
        			isVisible: true
        			);

        this.TagType = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase>.GetPropertyBySelector( item => item.TagType),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]

//InitFieldManyToManyBase_RightSide
		this.ContentPages = this.AddManyToManyCollectionWithDirectProperty<BusinessLogic_v3.Cms.ContentPageModule.ContentPageBaseCmsInfo>(
		BusinessLogic_v3.Cms.ContentPageModule.ContentPageBaseCmsFactory.Instance, 
		CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase>.GetPropertyBySelector(item => item.GetContentPages()), 
		
		((item) => ((BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase) item).ClearContentPages()),
            ((item, itemToAdd) => ((BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase) item).AddContentPageToContentPages((BusinessLogic_v3.Modules.ContentPageModule.ContentPageBase)itemToAdd)),
		
		
		Classes.Cms.EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);
//InitFieldManyToManyBase_RightSide
		this.ContentTexts = this.AddManyToManyCollectionWithDirectProperty<BusinessLogic_v3.Cms.ContentTextModule.ContentTextBaseCmsInfo>(
		BusinessLogic_v3.Cms.ContentTextModule.ContentTextBaseCmsFactory.Instance, 
		CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase>.GetPropertyBySelector(item => item.GetContentTexts()), 
		
		((item) => ((BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase) item).ClearContentTexts()),
            ((item, itemToAdd) => ((BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase) item).AddContentTextToContentTexts((BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase)itemToAdd)),
		
		
		Classes.Cms.EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);
//InitFieldManyToManyBase_RightSide
		this.EmailTexts = this.AddManyToManyCollectionWithDirectProperty<BusinessLogic_v3.Cms.EmailTextModule.EmailTextBaseCmsInfo>(
		BusinessLogic_v3.Cms.EmailTextModule.EmailTextBaseCmsFactory.Instance, 
		CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase>.GetPropertyBySelector(item => item.GetEmailTexts()), 
		
		((item) => ((BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase) item).ClearEmailTexts()),
            ((item, itemToAdd) => ((BusinessLogic_v3.Modules.ContentTagModule.ContentTagBase) item).AddEmailTextToEmailTexts((BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase)itemToAdd)),
		
		
		Classes.Cms.EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);


			base.initBasicFields();
          
        }

    }
}
