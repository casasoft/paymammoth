using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.OrderItemModule;
using BusinessLogic_v3.Cms.OrderItemModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class OrderItemBaseCmsFactory_AutoGen : CmsFactoryBase<OrderItemBaseCmsInfo, OrderItemBase>
    {
       
       public new static OrderItemBaseCmsFactory Instance
	    {
	         get
	         {
                 return (OrderItemBaseCmsFactory)CmsFactoryBase<OrderItemBaseCmsInfo, OrderItemBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = OrderItemBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "OrderItem.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "OrderItem";

            this.QueryStringParamID = "OrderItemId";

            cmsInfo.TitlePlural = "Order Items";

            cmsInfo.TitleSingular =  "Order Item";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public OrderItemBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "OrderItem/";


        }
       
    }

}
