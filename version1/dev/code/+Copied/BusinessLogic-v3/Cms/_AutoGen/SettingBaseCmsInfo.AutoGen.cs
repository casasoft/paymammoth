using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.SettingModule;
using BusinessLogic_v3.Cms.SettingModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class SettingBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.SettingModule.SettingBase>
    {
		public SettingBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.SettingModule.SettingBase dbItem)
            : base(BusinessLogic_v3.Cms.SettingModule.SettingBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new SettingBase DbItem
        {
            get
            {
                return (SettingBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Name { get; private set; }

        public CmsPropertyInfo Value { get; private set; }

        public CmsPropertyInfo VisibleInCMS_AccessRequired { get; private set; }

        public CmsPropertyInfo DataType { get; private set; }

        public CmsPropertyInfo Parent { get; private set; }

        public CmsPropertyInfo Identifier { get; private set; }

        public CmsPropertyInfo Description { get; private set; }

        public CmsPropertyInfo Disabled { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Name = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SettingModule.SettingBase>.GetPropertyBySelector( item => item.Name),
        			isEditable: true,
        			isVisible: true
        			);

        this.Value = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SettingModule.SettingBase>.GetPropertyBySelector( item => item.Value),
        			isEditable: true,
        			isVisible: true
        			);

        this.VisibleInCMS_AccessRequired = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SettingModule.SettingBase>.GetPropertyBySelector( item => item.VisibleInCMS_AccessRequired),
        			isEditable: true,
        			isVisible: true
        			);

        this.DataType = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SettingModule.SettingBase>.GetPropertyBySelector( item => item.DataType),
        			isEditable: true,
        			isVisible: true
        			);

        this.Parent = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SettingModule.SettingBase>.GetPropertyBySelector( item => item.Parent),
        			isEditable: true,
        			isVisible: true
        			);

        this.Identifier = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SettingModule.SettingBase>.GetPropertyBySelector( item => item.Identifier),
        			isEditable: true,
        			isVisible: true
        			);

        this.Description = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SettingModule.SettingBase>.GetPropertyBySelector( item => item.Description),
        			isEditable: true,
        			isVisible: true
        			);

        this.Disabled = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.SettingModule.SettingBase>.GetPropertyBySelector( item => item.Disabled),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
