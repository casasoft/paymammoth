using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.ShippingRatePriceModule;
using BusinessLogic_v3.Cms.ShippingRatePriceModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class ShippingRatePriceBaseCmsFactory_AutoGen : CmsFactoryBase<ShippingRatePriceBaseCmsInfo, ShippingRatePriceBase>
    {
       
       public new static ShippingRatePriceBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ShippingRatePriceBaseCmsFactory)CmsFactoryBase<ShippingRatePriceBaseCmsInfo, ShippingRatePriceBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = ShippingRatePriceBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ShippingRatePrice.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ShippingRatePrice";

            this.QueryStringParamID = "ShippingRatePriceId";

            cmsInfo.TitlePlural = "Shipping Rate Prices";

            cmsInfo.TitleSingular =  "Shipping Rate Price";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ShippingRatePriceBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "ShippingRatePrice/";


        }
       
    }

}
