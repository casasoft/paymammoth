using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.EventMediaItemModule;
using BusinessLogic_v3.Cms.EventMediaItemModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class EventMediaItemBaseCmsFactory_AutoGen : CmsFactoryBase<EventMediaItemBaseCmsInfo, EventMediaItemBase>
    {
       
       public new static EventMediaItemBaseCmsFactory Instance
	    {
	         get
	         {
                 return (EventMediaItemBaseCmsFactory)CmsFactoryBase<EventMediaItemBaseCmsInfo, EventMediaItemBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = EventMediaItemBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "EventMediaItem.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "EventMediaItem";

            this.QueryStringParamID = "EventMediaItemId";

            cmsInfo.TitlePlural = "Event Media Items";

            cmsInfo.TitleSingular =  "Event Media Item";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public EventMediaItemBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "EventMediaItem/";


        }
       
    }

}
