using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.MemberReferralCommissionModule;
using BusinessLogic_v3.Cms.MemberReferralCommissionModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class MemberReferralCommissionBaseCmsFactory_AutoGen : CmsFactoryBase<MemberReferralCommissionBaseCmsInfo, MemberReferralCommissionBase>
    {
       
       public new static MemberReferralCommissionBaseCmsFactory Instance
	    {
	         get
	         {
                 return (MemberReferralCommissionBaseCmsFactory)CmsFactoryBase<MemberReferralCommissionBaseCmsInfo, MemberReferralCommissionBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = MemberReferralCommissionBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "MemberReferralCommission.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "MemberReferralCommission";

            this.QueryStringParamID = "MemberReferralCommissionId";

            cmsInfo.TitlePlural = "Member Referral Commissions";

            cmsInfo.TitleSingular =  "Member Referral Commission";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public MemberReferralCommissionBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "MemberReferralCommission/";


        }
       
    }

}
