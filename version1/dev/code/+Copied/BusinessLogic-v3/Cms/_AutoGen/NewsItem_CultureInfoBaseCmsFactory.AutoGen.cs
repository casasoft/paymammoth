using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.NewsItem_CultureInfoModule;
using BusinessLogic_v3.Cms.NewsItem_CultureInfoModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class NewsItem_CultureInfoBaseCmsFactory_AutoGen : CmsFactoryBase<NewsItem_CultureInfoBaseCmsInfo, NewsItem_CultureInfoBase>
    {
       
       public new static NewsItem_CultureInfoBaseCmsFactory Instance
	    {
	         get
	         {
                 return (NewsItem_CultureInfoBaseCmsFactory)CmsFactoryBase<NewsItem_CultureInfoBaseCmsInfo, NewsItem_CultureInfoBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = NewsItem_CultureInfoBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "NewsItem_CultureInfo.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "NewsItem_CultureInfo";

            this.QueryStringParamID = "NewsItem_CultureInfoId";

            cmsInfo.TitlePlural = "News Item _ Culture Infos";

            cmsInfo.TitleSingular =  "News Item _ Culture Info";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public NewsItem_CultureInfoBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "NewsItem_CultureInfo/";


        }
       
    }

}
