using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.ContentTextModule;
using BusinessLogic_v3.Cms.ContentTextModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class ContentTextBaseCmsFactory_AutoGen : CmsFactoryBase<ContentTextBaseCmsInfo, ContentTextBase>
    {
       
       public new static ContentTextBaseCmsFactory Instance
	    {
	         get
	         {
                 return (ContentTextBaseCmsFactory)CmsFactoryBase<ContentTextBaseCmsInfo, ContentTextBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = ContentTextBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "ContentText.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "ContentText";

            this.QueryStringParamID = "ContentTextId";

            cmsInfo.TitlePlural = "Content Texts";

            cmsInfo.TitleSingular =  "Content Text";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public ContentTextBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "ContentText/";


        }
       
    }

}
