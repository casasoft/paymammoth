using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.SpecialOfferModule;
using BusinessLogic_v3.Cms.SpecialOfferModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class SpecialOfferBaseCmsFactory_AutoGen : CmsFactoryBase<SpecialOfferBaseCmsInfo, SpecialOfferBase>
    {
       
       public new static SpecialOfferBaseCmsFactory Instance
	    {
	         get
	         {
                 return (SpecialOfferBaseCmsFactory)CmsFactoryBase<SpecialOfferBaseCmsInfo, SpecialOfferBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = SpecialOfferBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "SpecialOffer.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "SpecialOffer";

            this.QueryStringParamID = "SpecialOfferId";

            cmsInfo.TitlePlural = "Special Offers";

            cmsInfo.TitleSingular =  "Special Offer";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public SpecialOfferBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "SpecialOffer/";


        }
       
    }

}
