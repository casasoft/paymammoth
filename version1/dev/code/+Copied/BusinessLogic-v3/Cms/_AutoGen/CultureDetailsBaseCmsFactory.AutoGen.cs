using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using BusinessLogic_v3.Cms.CultureDetailsModule;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace BusinessLogic_v3.Modules._AutoGen
{
	public abstract class CultureDetailsBaseCmsFactory_AutoGen : CmsFactoryBase<CultureDetailsBaseCmsInfo, CultureDetailsBase>
    {
       
       public new static CultureDetailsBaseCmsFactory Instance
	    {
	         get
	         {
                 return (CultureDetailsBaseCmsFactory)CmsFactoryBase<CultureDetailsBaseCmsInfo, CultureDetailsBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = CultureDetailsBase.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "CultureDetails.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "CultureDetails";

            this.QueryStringParamID = "CultureDetailsId";

            cmsInfo.TitlePlural = "Culture Details";

            cmsInfo.TitleSingular =  "Culture Details";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public CultureDetailsBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "CultureDetails/";


        }
       
    }

}
