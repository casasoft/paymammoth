using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.Category_ParentChildLinkModule;
using BusinessLogic_v3.Cms.Category_ParentChildLinkModule;

namespace BusinessLogic_v3.Modules._AutoGen
{
    public abstract class Category_ParentChildLinkBaseCmsInfo_AutoGen : CmsItemInfoDbBase<BusinessLogic_v3.Modules.Category_ParentChildLinkModule.Category_ParentChildLinkBase>
    {
		public Category_ParentChildLinkBaseCmsInfo_AutoGen(BusinessLogic_v3.Modules.Category_ParentChildLinkModule.Category_ParentChildLinkBase dbItem)
            : base(BusinessLogic_v3.Cms.Category_ParentChildLinkModule.Category_ParentChildLinkBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new Category_ParentChildLinkBase DbItem
        {
            get
            {
                return (Category_ParentChildLinkBase)base.DbItem;
            }
        }   

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo ParentCategory { get; private set; }

        public CmsPropertyInfo ChildCategory { get; private set; }



// [basecmsinfo_manytomanydeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.ParentCategory = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Category_ParentChildLinkModule.Category_ParentChildLinkBase>.GetPropertyBySelector( item => item.ParentCategory),
        			isEditable: true,
        			isVisible: true
        			);

        this.ChildCategory = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<BusinessLogic_v3.Modules.Category_ParentChildLinkModule.Category_ParentChildLinkBase>.GetPropertyBySelector( item => item.ChildCategory),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initmanytomanyfields]



			base.initBasicFields();
          
        }

    }
}
