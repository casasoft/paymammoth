using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Frontend.ProductVariationMediaItemModule;
using BusinessLogic_v3.Modules.ProductVariationMediaItemModule;


namespace BusinessLogic_v3.Cms.ProductVariationMediaItemModule
{
    public abstract class ProductVariationMediaItemBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.ProductVariationMediaItemBaseCmsInfo_AutoGen
    {
        public ProductVariationMediaItemBaseCmsInfo(ProductVariationMediaItemBase item)
            : base(item)
        {
            initListingItems();
            initListingPriority();
            initLinks();
            initEditPriorities();
            this.ImageFilename.ShowInEdit = false;
            initMediaItem();
        }

        private void initLinks()
        {
            
        }

        private void initListingItems()
        {
            Caption.ShowInListing = true;
            Priority.ShowInListing = true;
            Priority.EditableInListing = true;
            ImageFilename.ShowInEdit = false;
        }

        private void initListingPriority()
        {
            this.ListingOrderPriorities.AddColumnsToStart(this.Caption, this.Priority);
        }

        private void initEditPriorities()
        {
            this.EditOrderPriorities.AddColumnsToStart(Title, Caption);
        }

            private void initMediaItem()
        {
            this.Image = this.AddPropertyForMediaItem<ProductVariationMediaItemBase>(item => item.Image, true, true);
        }
            public CmsPropertyMediaItem<ProductVariationMediaItemBase> Image { get; set; }

    }
}