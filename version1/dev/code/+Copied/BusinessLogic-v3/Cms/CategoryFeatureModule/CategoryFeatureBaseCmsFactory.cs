using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.CategoryFeatureModule;

using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;

namespace BusinessLogic_v3.Cms.CategoryFeatureModule
{
    public abstract class CategoryFeatureBaseCmsFactory : BusinessLogic_v3.Modules._AutoGen.CategoryFeatureBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            
            base.initCmsParameters();
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = Classes.Cms.EnumsCms.IMAGE_ICON.Label1;
            //custom CMS defaults - important, add AFTER the base.initCmsParameters()
        }
    }
}
