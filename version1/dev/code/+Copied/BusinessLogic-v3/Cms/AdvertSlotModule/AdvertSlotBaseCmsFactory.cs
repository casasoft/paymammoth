using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.AdvertSlotModule;

using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;

namespace BusinessLogic_v3.Cms.AdvertSlotModule
{
    public abstract class AdvertSlotBaseCmsFactory : BusinessLogic_v3.Modules._AutoGen.AdvertSlotBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            
            base.initCmsParameters();
            this.UserSpecificGeneralInfoInContext.AccessTypeRequired_ToAdd.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft;
            
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = Classes.Cms.EnumsCms.IMAGE_ICON.Advert;
        }
    }
}
