using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.CategorySpecificationModule;

namespace BusinessLogic_v3.Cms.CategorySpecificationModule
{
    public abstract class CategorySpecificationBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.CategorySpecificationBaseCmsInfo_AutoGen
    {
    	public CategorySpecificationBaseCmsInfo(CategorySpecificationBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
		    initLinksInCMS();
		    initListingItems();
		    initRequired();
            initListingPriority();
        }

        private void initLinksInCMS()
        {
            

        }

        private void initListingItems()
        {
            this.Title.ShowInListing = true;
            this.DataType.ShowInListing = true;
            this.Category.ShowInListing = true;
            this.MeasurementUnit.ShowInListing = true;
            this.Category.ShowInEdit = true;
            this.MeasurementUnit.ShowInEdit = true;
        }
        
        private void initRequired()
        {
            this.Title.IsRequired = true;
            this.DataType.IsRequired = true;
            this.MeasurementUnit.IsRequired = true;
            this.Category.IsRequired = true;
        }

        private void initListingPriority()
        {
            this.ListingOrderPriorities.AddColumnsToStart(this.Title,this.Category, this.DataType, this.MeasurementUnit);
        }
	}
}
