using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.CategorySpecificationModule;

namespace BusinessLogic_v3.Cms.CategorySpecificationModule
{
    public abstract class CategorySpecificationBaseCmsFactory : BusinessLogic_v3.Modules._AutoGen.CategorySpecificationBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            
            base.initCmsParameters();
            //custom CMS defaults - important, add AFTER the base.initCmsParameters()
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = Classes.Cms.EnumsCms.IMAGE_ICON.Label2;
        }
    }
}
