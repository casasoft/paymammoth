using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using CS.General_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Cms.ProductVariation_CultureInfoModule
{

//BaseCmsInfo-Class

    public abstract class ProductVariation_CultureInfoBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.ProductVariation_CultureInfoBaseCmsInfo_AutoGen
    {
    	public ProductVariation_CultureInfoBaseCmsInfo(ProductVariation_CultureInfoBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
	}
}
