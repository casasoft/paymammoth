using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.CmsAuditLogModule;

namespace BusinessLogic_v3.Cms.CmsAuditLogModule
{
    public abstract class CmsAuditLogBaseCmsFactory : BusinessLogic_v3.Modules._AutoGen.CmsAuditLogBaseCmsFactory_AutoGen
    {
       

        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
        	//any user-specific information, add them to 'cmsInfo'.
            var loggedUser = this.getLoggedCmsUser();
            if (loggedUser != null && loggedUser.CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE.SuperAdministrator))
            {
                this.UserSpecificGeneralInfoInContext.ShowInCmsMainMenu = true;
                
            }
            this.UserSpecificGeneralInfoInContext.AccessTypeRequired_ToDelete.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne, CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            this.UserSpecificGeneralInfoInContext.AccessTypeRequired_ToAdd.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne, CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = Classes.Cms.EnumsCms.IMAGE_ICON.Audit;
            base.getUserSpecificGeneralInfo(cmsInfo);
        }

    }
}
