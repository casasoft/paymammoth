using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;

using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;

using BusinessLogic_v3.Modules.CmsAuditLogModule;

namespace BusinessLogic_v3.Cms.CmsAuditLogModule
{
    public abstract class CmsAuditLogBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.CmsAuditLogBaseCmsInfo_AutoGen
    {
    	public CmsAuditLogBaseCmsInfo(CmsAuditLogBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here
            this.SetDefaultSortField(this.DateTime, CS.General_v3.Enums.SORT_TYPE.Descending);
            this.DateTime.ShowInListing = true;
            this.MsgType.ShowInListing = true;
            this.Message.ShowInListing = true;

            this.ObjectName.ShowInListing = true;
            this.CmsUser.ShowInListing = true;
            this.CmsUser.ShowInEdit = true;
            
            this.CmsUser.IsSearchable = true;
            this.Remarks.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.MultiLine;
            this.FurtherInformation.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.MultiLine;
            this.AccessTypeRequired_ToDelete.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne, CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);

            this.ListingOrderPriorities.AddColumnsToStart(this.DateTime, this.CmsUser, this.MsgType, this.ObjectName, this.Message);
            this.EditOrderPriorities.AddColumnsToStart(this.DateTime, this.CmsUser, this.MsgType, this.ObjectName, this.Message, this.FurtherInformation, this.IPAddress, this.Remarks);

            this.SetNonEditableFields(this.DateTime,this.MsgType,this.Message,this.ObjectName,this.CmsUser,this.FurtherInformation, this.IPAddress);
            this.AccessTypeRequired_ToView.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.SuperAdministrator, CS.General_v3.Enums.CMS_ACCESS_TYPE.SuperAdministrator);

            base.customInitFields();
        }

	}
}
