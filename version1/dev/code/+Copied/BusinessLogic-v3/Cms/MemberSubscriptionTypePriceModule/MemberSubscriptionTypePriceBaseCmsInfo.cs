using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;

using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;

using BusinessLogic_v3.Modules.MemberSubscriptionTypePriceModule;

namespace BusinessLogic_v3.Cms.MemberSubscriptionTypePriceModule
{
    public abstract class MemberSubscriptionTypePriceBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.MemberSubscriptionTypePriceBaseCmsInfo_AutoGen
    {
    	public MemberSubscriptionTypePriceBaseCmsInfo(MemberSubscriptionTypePriceBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
	}
}
