using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.PaymentTransactionModule;

namespace BusinessLogic_v3.Cms.PaymentTransactionModule
{
    public abstract class PaymentTransactionBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.PaymentTransactionBaseCmsInfo_AutoGen
    {
        public PaymentTransactionBaseCmsInfo(PaymentTransactionBase item)
            : base(item)
        {

        }

        protected override void customInitFields()
        {
            base.customInitFields();

            

            this.Amount.ShowInListing = true;
            this.AuthCode.ShowInListing = true;
            this.Date.ShowInListing = true;
            this.ErrorMsg.ShowInListing = true;
            this.InternalReference.ShowInListing = true;
            this.ExternalReference1.ShowInListing = true;
            this.Success.ShowInListing = true;
        }
	}
}
