using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.PaymentTransactionModule;

namespace BusinessLogic_v3.Cms.PaymentTransactionModule
{
    public abstract class PaymentTransactionBaseCmsFactory : BusinessLogic_v3.Modules._AutoGen.PaymentTransactionBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            this.UserSpecificGeneralInfoInContext.SetAllMinimumAccessRequiredAs(CS.General_v3.Enums.CMS_ACCESS_TYPE.SuperAdministrator);
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = Classes.Cms.EnumsCms.IMAGE_ICON.BagOfMoney;
            base.initCmsParameters();
            
        }
    }
}
