using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.ProductVariationModule;

namespace BusinessLogic_v3.Cms.ProductVariationModule
{
    public abstract class ProductVariationBaseCmsFactory : BusinessLogic_v3.Modules._AutoGen.ProductVariationBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            this.TitlePlural = "Product Variations";
            this.TitleSingular = "Product Variation";
            base.initCmsParameters();
        }
        
    }
}
