using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.ProductVariationModule;

namespace BusinessLogic_v3.Cms.ProductVariationModule
{
    public abstract class ProductVariationBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.ProductVariationBaseCmsInfo_AutoGen
    {
        public ProductVariationBaseCmsInfo(ProductVariationBase item)
            : base(item)
        {

        }

        protected override void customInitFields()
        {
            base.customInitFields();
            initListingItems();
            initRequired();
            initListingPriority();
            initLinksInCMS();
            initEditPriorities();
            this.MediaItems.ShowLinkInCms = true;
        }

        private void initLinksInCMS()
        {
            
        }

        private void initListingItems()
        {
            this.Colour.ShowInListing = true;
            this.Size.ShowInListing = true;
            this.Quantity.ShowInListing = true;
            this.Barcode.ShowInListing = true;
            
        }

        private void initRequired()
        {
            //this.Colour.IsRequired = true;
        }

        private void initListingPriority()
        {
            this.ListingOrderPriorities.AddColumnsToStart(this.Barcode, this.Colour, this.Size, this.Quantity);
        }

        private void initEditPriorities()
        {
            this.EditOrderPriorities.AddColumnsToStart(Barcode, ReferenceCode, SupplierRefCode, Size, Colour, this.Quantity);
        }
	}
}
