using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties.CustomPropertyTypes;


using BusinessLogic_v3.Modules.ShippingRatesGroupModule;

namespace BusinessLogic_v3.Cms.ShippingRatesGroupModule
{
    public abstract class ShippingRatesGroupBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.ShippingRatesGroupBaseCmsInfo_AutoGen
    {
    	public ShippingRatesGroupBaseCmsInfo(ShippingRatesGroupBase item)
            : base(item)
        {
            
        }
        private void addCountriesApplicable()
        {
            {
                this.CountriesApplicable = this.AddProperty(new CmsCountryProperty(this, CS.General_v3.Util.ReflectionUtil<ShippingRatesGroupBase>.GetPropertyBySelector(item => item.CountriesApplicable)));
                this.CountriesApplicable.ShowInListing = true;
            }
        }

        protected override void customInitFields()
        {
            //custom init field logic here
            this.ShippingRatePrices.ShowLinkInCms = true;
            base.customInitFields();

            this.Title.IsRequired = true;
            this.Title.ShowInListing = true;
            this.ShippingMethod.ShowInEdit = true;
            this.AppliesToRestOfTheWorld.HelpMessage = 
@"If this is ticked, and no country match is found for the selected method, this rate group will be used. If more than one are ticked as rest of the world
 the last one is saved, and all others are resetted";

            

            this.RemoveProperty(base.CountriesApplicable);

            addCountriesApplicable();
            
            this.ListingOrderPriorities.AddColumnsToStart(this.Title, this.CountriesApplicable);
            this.EditOrderPriorities.AddColumnsToStart(this.Title, this.CountriesApplicable);

        }
        public new CmsCountryProperty CountriesApplicable { get; private set; }
	}
}
