using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.MemberSubscriptionLinkModule;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using CS.General_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Cms.MemberSubscriptionLinkModule
{

//BaseCmsInfo-Class

    public abstract class MemberSubscriptionLinkBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.MemberSubscriptionLinkBaseCmsInfo_AutoGen
    {
    	public MemberSubscriptionLinkBaseCmsInfo(MemberSubscriptionLinkBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
	}
}
