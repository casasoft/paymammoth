using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties.CustomPropertyTypes;
using BusinessLogic_v3.Modules.Article_ParentChildLinkModule;
using BusinessLogic_v3.Modules.CmsUserModule;
using CS.General_v3.Controls.WebControls.Specialized.Hierarchy;
using CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructure;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructureBasic;
using BusinessLogic_v3.Modules._Common;
using CS.General_v3.Controls.WebControls.Classes;
using CS.General_v3.Extensions;
using System.Web.UI.WebControls;

using CS.General_v3.Controls.WebControls.Common;

using BusinessLogic_v3.Modules.ArticleModule;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Modules.ArticleRatingModule;

namespace BusinessLogic_v3.Cms.ArticleModule
{
    public abstract class ArticleBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.ArticleBaseCmsInfo_AutoGen,ICmsHierarchyItem,ICmsHierarchyInfo
    {
        private CmsItemHierarchyCommonImpl _commonImpl_CmsItemHierarchy = null;
        
        public ArticleBaseCmsInfo(ArticleBase item)
            : base(item)
        {

            _commonImpl_CmsItemHierarchy = new CmsItemHierarchyCommonImpl(this, typeof(ArticleBase));
            if (this.DbItem != null)
            {
                this.AccessTypeRequired_ToDelete.MinimumAccessTypeRequired = this.DbItem.AllowDelete_AccessRequired;
                this.AccessTypeRequired_ToEdit.MinimumAccessTypeRequired = this.DbItem.AllowUpdate_AccessRequired;
            }
        }

        private void checkAffiliateAccess()
        {

        }
        //private void initArticleRatings()
        //{
        //    this.ArticleRatings = this.AddProperty(new CmsCollectionInfo<ArticleRatingBase>(this.factory,
        //        CS.General_v3.Util.ReflectionUtil<ArticleBase>.GetPropertyBySelector(item => item.ArticleRatings)));
            
        //}

        //public CmsCollectionInfo<ArticleRatingBase> ArticleRatings { get; private set; }

        protected override void customInitFields()
        {
            
            addParentLinksProperty();
            initCustomProperties();
            if (Modules.ModuleSettings.Articles.EnableCommentingSystem)
                this.ArticleComments.ShowLinkInCms = true;
            //initArticleRatings();
            this.ArticleRatings.ShowLinkInCms = true;
          //  this.ArticleRatings.LinkedWithInCms = true;
            this.Priority.IsRequired = true;
            this.Name.IsRequired = true;
            this.PageTitle.IsRequired = true;
            this.CustomRewriteUrl.Label = "Custom title for Url";
            this.Color.AccessTypeRequired_View.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft;
            this.ViewCount.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            this.Parents.Label = "Parent Pages";
            this.ShowAdverts.HelpMessage = "If applicable, this allows you to chose whether you want to show adverts for this page or not";
            this.IsFeatured.HelpMessage = "If applicable, this allows you to mark this page as 'featured'";
            this.ID.AccessTypeRequired_Edit.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne;
            this.MetaDescription.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.MultiLine;
            this.Color.ShowInEdit = false;
            this.AvgRating.AccessTypeRequired_Edit.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            //this.Rating.HelpMessage = "If applicable, this lets you modify the 'rating' given to this page by users";
            //this.Priority.HelpMessage = "This is a numerical val2ue used for sorting.  If priorities are the same, e.g both are 10, then they are sorted by Title";
            this.HtmlText.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.Html;
            this.Identifier.AccessTypeRequired_View.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft;
            this.CustomLink.AccessTypeRequired_View.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft;
            this.Name.AccessTypeRequired_Edit.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft;
            this.AllowAddChildren_AccessRequired.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            this.AllowAddSubChildren_AccessRequired.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            this.AllowDelete_AccessRequired.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            this.AllowUpdate_AccessRequired.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            this.VisibleInCMS_AccessRequired.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            this.DoNotShowLastEditedOn.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            this.ExcludeFromRewriteUrl.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            this.NoFollow.HelpMessage = "This instructs search engines not to follow a link to this content page. Use this for irrelevant pages like Privacy Policy, Disclaimer, or un-important external links";
            this.NoIndex.HelpMessage = "Instructs search engines not to index this page, or a link to this page";
            this.Published.ShowInListing = true;
            this.Name.HelpMessage = "This name will not be used within the front end but used internally for yourself (mainly used for multi-lingual purposes)";
            this.PageTitle.HelpMessage = "This is the actual page title used for the page on top of the page content";
            this.CustomRewriteUrl.HelpMessage = "Should you want to alter the URL shown in the address bar, enter your custom (friendly) URL here.";
            this.MetaTitle.HelpMessage = "This will be the title shown in the browser tab within the address bar.  If left empty, this will be the same as PageTitle.";
            this.UsedInProject.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft);
            this.UsedInProject.SetDefaultSearchValue(true);
            this.Name.ShowInListing = true;
            this.PageTitle.ShowInListing = true;

         //   this.ContentTags = this.AddManyToManyCollectionProperty<ContentTagBase>(ContentTagBase.Factory.CMSFactory, CS.General_v3.Util.ReflectionUtil<ArticleBase>.GetPropertyBySelector(item => item.ContentTags));
            //this.ContentTags.AccessTypeRequired_Edit.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.CasaSoft;
            //this.ChildArticles.AccessTypeRequired_View.MinimumAccessTypeRequired = CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne;
            base.customInitFields();

           /* this.EditOrderPriorities.AddColumnsToStart(this.ID, this.Name, this.PageTitle, this.CustomLink, this.Identifier, this.HtmlText, this.MetaKeywords, this.MetaDescription, this.Priority, this.Parents, this.Color,
                this.ShowAdverts,
                this._Deleted, this._DeletedBy, this._DeletedOn, this.AllowAddChildren_AccessRequired, this.AllowAddSubChildren_AccessRequired, this.AllowDelete_AccessRequired, this.AllowUpdate_AccessRequired,
                this.VisibleInCMS_AccessRequired);
            */

            this.EditOrderPriorities.AddColumnsToStart(
    this.Name,
    this.PageTitle,
    this.CustomRewriteUrl,
    this.HtmlText,
    this.MetaTitle,
    this.MetaKeywords,
    this.MetaDescription,
    this.Priority,
    this.IsFeatured,
    this.AvgRating,
    this.ViewCount,
    this.ExcludeFromRewriteUrl
);

        }

     //   public CmsPropertyManyToManyCollection<ContentTagBase> ContentTags { get; private set; }



        private void addParentLinksProperty()
        {

            var p = new CmsPropertyManyToManyWithCustomClass<ArticleBase, Article_ParentChildLinkBase, ArticleBase>(this,
                new ArticleParentLinksParams(),
                 Classes.Cms.EnumsCms.MULTICHOICE_DISPLAY_TYPE.Listbox);
            
            this.Parents = this.AddProperty(p);
            
        }

        private void initCustomProperties()
        {
            this.FrontendUrl = this.AddProperty(new CmsPropertyLiteral<ArticleBase>(this, "Frontend Url", 
            item => {
                var cpFrontend = item;
                string url = cpFrontend.GetUrl();
                MyAnchor a = new MyAnchor();
                a.Text = url;
                a.Href = url;
                a.HRefTarget = CS.General_v3.Enums.HREF_TARGET.Blank;
                a.Title = cpFrontend.PageTitle;
                return a;
            
            }));
        }

        public CmsPropertyManyToManyWithCustomClass<ArticleBase, Article_ParentChildLinkBase, ArticleBase> Parents { get; private set; }

        public CmsPropertyLiteral<ArticleBase> FrontendUrl { get; private set; }

        #region ICmsHierarchyItem Members

        ICmsHierarchyInfo ICmsHierarchyItem.GetCmsHierarchyInfo()
        {
            return this;
            
        }

        #endregion
#region ICmsHierarchyInfo Members

        ICmsCollectionInfo ICmsHierarchyInfo.SubitemLinkedCollection
        {
            get { return null; }
        }
        string ICmsHierarchyInfo.GetTitleInHierarchy()
        {
            return this.ToString();
        }

#endregion
        
        #region IHierarchy items
        string IHierarchy.Title
        {
            get
            {
                return this.DbItem.PageTitle;
            }
        }

        string IHierarchy.Href
        {
            get
            {
                return "#";
            }
        }

        CS.General_v3.Enums.HREF_TARGET IHierarchy.HrefTarget
        {
            get
            {
                return CS.General_v3.Enums.HREF_TARGET.Self;
            }
        }

        IEnumerable<IHierarchy> IHierarchy.GetChildren()
        {
            return this.getChildren();
            
        }



        public bool Selected
        {
            get
            {
                var cp = ArticleBase.Factory.GetArticleByRouteParameter();
                if (cp != null && cp.ID == this.DbItem.ID)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        string IHierarchy.GetFullHierarchyName(string delimiter, bool reverseOrder, bool includeRoot)
        {
            return _commonImpl_CmsItemHierarchy.GetFullHierarchyName(delimiter, reverseOrder, includeRoot);
            
        }

        bool IHierarchy.Visible
        {
            get
            {
                return this.DbItem.Published;
            }
        }
        #endregion

#region ITreeItem Members
        string ITreeItem.EditURL //for CMS
        {
            get { return _commonImpl_CmsItemHierarchy.EditURL;}
        }

        string ITreeItem.AddNewItemURL
        {
            get 
            {

                var f = (ArticleBaseCmsFactory)factory;
                var url = f.GetAddUrl();
                url[f.QUERYSTRING_PARENTID] = this.DbItem.ID;
                return url.ToString();
            
            }
        }

        bool ITreeItem.AllowUpdate
        {
            get { return CmsUserSessionLogin.Instance.GetLoggedInUser().CheckAccessTypeAtLeast(this.DbItem.AllowUpdate_AccessRequired); }
        }

        bool ITreeItem.AllowDelete
        {
            get { return CmsUserSessionLogin.Instance.GetLoggedInUser().CheckAccessTypeAtLeast(this.DbItem.AllowDelete_AccessRequired); }
        }

        bool ITreeItem.AllowAddSubItems
        {
            get { return CmsUserSessionLogin.Instance.GetLoggedInUser().CheckAccessTypeAtLeast(this.DbItem.AllowAddChildren_AccessRequired); }
        }

        bool ITreeItem.CanRemove(out string errorMsg)
        {
            
            errorMsg = null;
            return ((ITreeItem)this).AllowDelete;
        }

        OperationResult  ITreeItem.Remove()
        {
            return this.DeleteFromCms();
            
        }

        string ITreeItem.Message_DeleteOK
        {
            get {return _commonImpl_CmsItemHierarchy.Message_DeleteOK;}
        }

        string ITreeItem.Message_ConfirmDelete
        {
            get { return _commonImpl_CmsItemHierarchy.Message_ConfirmDelete; }
        }

        bool ITreeItem.AddedExtraButtons
        {
            get
            {
                return _commonImpl_CmsItemHierarchy.AddedExtraButtons;
            }
            set
            {
                _commonImpl_CmsItemHierarchy.AddedExtraButtons = value;
            }
        }


        string CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructureBasic.ITreeItemBasic.Title
        {
            get
            {
                return this.DbItem.Name ;
            }
            
        }
        
        string ITreeItemBasic.ImageURL
        {
            get { return _commonImpl_CmsItemHierarchy.ImageUrl;}
        }

        string ITreeItemBasic.LinkURL
        {
            get { return _commonImpl_CmsItemHierarchy.LinkUrl; }
        }
        private List<ArticleBaseCmsInfo> _children = null;
        protected List<ArticleBaseCmsInfo> getChildren()
        {
            if (_children == null)
            {
                List<ArticleBaseCmsInfo> list = new List<ArticleBaseCmsInfo>();
                foreach (var item in this.DbItem.GetChildArticles())
                {
                    list.Add((ArticleBaseCmsInfo)factory.GetCmsItemFromDBObject(item));
                }
                _children = list.Where<ArticleBaseCmsInfo>(item=>item.DbItem.UsedInProject).ToList();
            }
            return _children;
        }
        IEnumerable<CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructureBasic.ITreeItemBasic> ITreeItemBasic.GetChildTreeItems()
        {
            return getChildren();
            
            
        }

        List<ExtraButton> ITreeItemBasic.ExtraButtons
        {
            get { return _commonImpl_CmsItemHierarchy.ExtraButtons; }
        }

        void ITreeItemBasic.AddExtraButtons()
        {
            _commonImpl_CmsItemHierarchy.AddExtraButtons();
        }


#endregion


        #region ITreeItem Members


        OperationResult ITreeItem.Save()
        {
            return this.SaveFromCms();
            
        }

        #endregion

        #region ITreeItemBasic Members


        int ITreeItemBasic.Priority
        {
            get
            {
                return this.DbItem.Priority;
            }
            set
            {
                this.DbItem.Priority = value;
                
            }
        }

        #endregion

        #region IHierarchy Members

        long IHierarchy.ID
        {
            get { return this.DbItem.ID; }
        }

        #endregion


        #region ITreeItemBasic Members

        long ITreeItemBasic.ID
        {
            get { return this.DbItem.ID; }
        }

        #endregion

        #region IHierarchy Members

        IEnumerable<IHierarchy> IHierarchy.GetParents()
        {
            return this.DbItem.GetParentArticles();
            
        }

        #endregion
    }
}
