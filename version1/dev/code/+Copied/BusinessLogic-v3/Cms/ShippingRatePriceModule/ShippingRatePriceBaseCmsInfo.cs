using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.ShippingRatePriceModule;

namespace BusinessLogic_v3.Cms.ShippingRatePriceModule
{
    public abstract class ShippingRatePriceBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.ShippingRatePriceBaseCmsInfo_AutoGen
    {
    	public ShippingRatePriceBaseCmsInfo(ShippingRatePriceBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
            
            this.FromWeight.IsRequired = true;
            this.FromWeight.ShowInListing = true;
            this.FromWeight.EditableInListing = true;
            this.FromWeight.Label = "From Weight - in Kg";
            this.ToWeight.Label = "To Weight - in Kg";

            this.ToWeight.IsRequired = true;
            this.ToWeight.ShowInListing = true; 
            this.ToWeight.EditableInListing = true;
            
            this.Price.IsRequired = true;
            this.Price.ShowInListing = true;
            this.Price.EditableInListing = true;

//            this.PricingType.HelpMessage = @"'Fixed Amount' means that if the total weight falls within the from/to range, a fixed price will be charged, e.g 10EUR.  Per kilogram means that the 
//total price will be multiplied based on the total weight.  E.g, if the range is 4 - 6kg, and the price is 10EUR/kg, if the total weight is 5kg, then price is 50EUR.";
                
            
            this.PricingType.IsRequired = true;
            this.PricingType.ShowInListing = true;
            this.PricingType.EditableInListing = true;

            this.ListingOrderPriorities.AddColumnsToStart(this.FromWeight, this.ToWeight, this.PricingType, this.Price);
            this.EditOrderPriorities.AddColumnsToStart(this.FromWeight, this.ToWeight, this.Price, this.PricingType);
            
            

        }
	}
}
