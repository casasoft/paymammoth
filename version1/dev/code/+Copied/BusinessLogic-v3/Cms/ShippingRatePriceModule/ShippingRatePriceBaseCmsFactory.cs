using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.ShippingRatePriceModule;

namespace BusinessLogic_v3.Cms.ShippingRatePriceModule
{
    public abstract class ShippingRatePriceBaseCmsFactory : BusinessLogic_v3.Modules._AutoGen.ShippingRatePriceBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            
            base.initCmsParameters();
            //custom CMS defaults - important, add AFTER the base.initCmsParameters()
        }
    }
}
