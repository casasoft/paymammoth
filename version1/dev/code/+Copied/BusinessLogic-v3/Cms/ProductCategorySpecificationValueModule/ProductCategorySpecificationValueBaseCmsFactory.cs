using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule;

namespace BusinessLogic_v3.Cms.ProductCategorySpecificationValueModule
{
    public abstract class ProductCategorySpecificationValueBaseCmsFactory : BusinessLogic_v3.Modules._AutoGen.ProductCategorySpecificationValueBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            
            base.initCmsParameters();
            //custom CMS defaults - important, add AFTER the base.initCmsParameters()
        }
    }
}
