using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule;

namespace BusinessLogic_v3.Cms.ProductCategorySpecificationValueModule
{
    public abstract class ProductCategorySpecificationValueBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.ProductCategorySpecificationValueBaseCmsInfo_AutoGen
    {
    	public ProductCategorySpecificationValueBaseCmsInfo(ProductCategorySpecificationValueBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
            initListingItems();
            initListingPriority();
        }

        private void initListingItems()
        {
            this.Title.ShowInListing = true;
            this.Title.EditableInListing = true;
            Value.ShowInListing = true;
            this.Value.EditableInListing = true;
            
        }

        private void initListingPriority()
        {
            this.ListingOrderPriorities.AddColumnsToStart(this.Title, this.Value);
            
            
        }

	}
}
