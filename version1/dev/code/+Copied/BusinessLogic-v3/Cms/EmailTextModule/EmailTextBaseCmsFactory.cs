using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;

using CS.General_v3.Extensions;

using BusinessLogic_v3.Modules.EmailTextModule;
using CS.General_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Cms.EmailTextModule
{
    public abstract class EmailTextBaseCmsFactory : BusinessLogic_v3.Modules._AutoGen.EmailTextBaseCmsFactory_AutoGen
    {
        
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            cmsInfo.ShowInCmsMainMenu = true;
            cmsInfo.CmsImageIcon = Classes.Cms.EnumsCms.IMAGE_ICON.EmailEdit;
            cmsInfo.RenderType = Classes.Cms.EnumsCms.SECTION_RENDER_TYPE.Tree;
            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        public override IEnumerable<ICmsHierarchyItem> GetRootItems(IEnumerable<CmsPropertySearchInfo> searchCriteria)
        {
            var q = EmailTextBase.Factory.GetQuery();
            q.Where( item => item.Parent == null);
            var list = EmailTextBase.Factory.FindAll(q);
            return list.ConvertAll(item => GetCmsItemFromDBObject(item));
            
        }


        protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
            var q = EmailTextBase.Factory.GetQuery(new GetQueryParams(loadDelItems: showDeleted));
            var tmp1 = q.Fetch(item => item.Parent).Eager;
            var tmp2 = q.Fetch(item => item.ChildEmails).Eager;
            return q;
        }


    }
}
