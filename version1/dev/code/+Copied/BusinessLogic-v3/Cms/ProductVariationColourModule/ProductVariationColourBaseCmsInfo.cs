using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Modules.ProductVariationColourModule;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using CS.General_v3.Classes.DbObjects.Parameters;

namespace BusinessLogic_v3.Cms.ProductVariationColourModule
{

//BaseCmsInfo-Class

    public abstract class ProductVariationColourBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.ProductVariationColourBaseCmsInfo_AutoGen
    {
    	public ProductVariationColourBaseCmsInfo(ProductVariationColourBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
	}
}
