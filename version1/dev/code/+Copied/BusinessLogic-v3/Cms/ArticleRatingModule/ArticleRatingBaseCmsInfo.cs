using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;

using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;

using BusinessLogic_v3.Modules.ArticleRatingModule;

namespace BusinessLogic_v3.Cms.ArticleRatingModule
{
    public abstract class ArticleRatingBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.ArticleRatingBaseCmsInfo_AutoGen
    {
    	public ArticleRatingBaseCmsInfo(ArticleRatingBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
	}
}
