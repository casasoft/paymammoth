using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.MeasurementUnitModule;

namespace BusinessLogic_v3.Cms.MeasurementUnitModule
{
    public abstract class MeasurementUnitBaseCmsFactory : BusinessLogic_v3.Modules._AutoGen.MeasurementUnitBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            
            base.initCmsParameters();
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = Classes.Cms.EnumsCms.IMAGE_ICON.Ruler;
            //custom CMS defaults - important, add AFTER the base.initCmsParameters()
        }
    }
}
