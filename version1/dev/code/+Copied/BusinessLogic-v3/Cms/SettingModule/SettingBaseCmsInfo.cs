using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.SettingModule;

namespace BusinessLogic_v3.Cms.SettingModule
{
    public abstract class SettingBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.SettingBaseCmsInfo_AutoGen
    {
        public SettingBaseCmsInfo(SettingBase item)
            : base(item)
        {

        }

        

        protected override void customInitFields()
        {
            base.customInitFields();
           // this.ID.SetAccessTypeRequiredForAll(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            
            this.Name.ShowInListing = true;
            this.Name.EditableInListing = false;
            this.Value.ShowInListing = true;
            this.Value.EditableInListing = true;
            this.Value.WidthInListing = 500;

            this.Identifier.ShowInListing = true;
            this.UsedInProject.SetDefaultSearchValue(true);
            this.ListingOrderPriorities.AddColumnsToStart(this.Identifier, this.Name, this.Value);
            this.SetDefaultSortField(this.Identifier, CS.General_v3.Enums.SORT_TYPE.Ascending);


        }
	}
}
