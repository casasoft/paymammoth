using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;

using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Util;
using NHibernate.Criterion;

using BusinessLogic_v3.Modules.SettingModule;

namespace BusinessLogic_v3.Cms.SettingModule
{
    public abstract class SettingBaseCmsFactory : BusinessLogic_v3.Modules._AutoGen.SettingBaseCmsFactory_AutoGen
    {
        protected override void initCmsParameters()
        {
            this.UserSpecificGeneralInfoInContext.ShowInCmsMainMenu = true;
            this.UserSpecificGeneralInfoInContext.SetAllMinimumAccessRequiredAs(CS.General_v3.Enums.CMS_ACCESS_TYPE.Normal);
            base.initCmsParameters();
            this.UserSpecificGeneralInfoInContext.CmsImageIcon = Classes.Cms.EnumsCms.IMAGE_ICON.Tools;
            addCustomOperations();
        }
        private void addCustomOperations()
        {
            var loggedUser = getLoggedCmsUser();
            this.UserSpecificGeneralInfoInContext.CustomCmsOperations.Add(new CmsGeneralOperation("Clear Frontend Cache", Classes.Cms.EnumsCms.IMAGE_ICON.Delete, _clearAppCache));
            this.UserSpecificGeneralInfoInContext.CustomCmsOperations.Add(new CmsGeneralOperation("Send Test Email", Classes.Cms.EnumsCms.IMAGE_ICON.Email, BusinessLogic_v3.Classes.Cms.CmsObjects.CmsRoutesMapper.Instance.GetSettings_SendTestEmailPage()));

            {
                if (loggedUser != null &&  loggedUser.CheckAccessTypeAtLeast(CS.General_v3.Enums.CMS_ACCESS_TYPE.SuperAdministrator))
                {
                    var op = new CmsGeneralOperation("Restart Application", Classes.Cms.EnumsCms.IMAGE_ICON.ReOrder, restartApplication);
                    op.ConfirmMessage = "Are you sure you want to restart the application?  This might log out all users!";
                    this.UserSpecificGeneralInfoInContext.CustomCmsOperations.Add(op);
                }

            }
        }

        private void restartApplication()
        {
            System.Web.HttpRuntime.UnloadAppDomain();
        }

        //
        private void _clearAppCache()
        {
            CS.General_v3.Classes.Caching.CustomCacheDependencyController.Instance.InvalidateAll();
            CS.General_v3.Util.PageUtil.ClearAllCacheInContext();
            CachingUtil.InvalidateAllPagesOuputCachingDependency();
            //HttpContext.Current.Cache.Remove("AllPages");
            //HttpContext.Current.Cache.Insert("AllPages", DateTime.Now, null, System.DateTime.MaxValue, System.TimeSpan.Zero,
              //  System.Web.Caching.CacheItemPriority.NotRemovable, null);
            BusinessLogic_v3.Classes.Cms.Util.CmsUtil.ShowStatusMessageInCMS("Frontend cache cleared successfully", CS.General_v3.Enums.STATUS_MSG_TYPE.Success);
            
            
        }

        protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
            var q =base._getQueryForSearchResults(showDeleted);
            q.RootCriteria.Add(NHibernate.Criterion.Restrictions.IsEmpty(Projections.Property<SettingBase>(item => item.ChildSettings).PropertyName));
            return q;

        }

        

    }
}
