using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;


using BusinessLogic_v3.Modules.LocationModule;

namespace BusinessLogic_v3.Cms.LocationModule
{
    public abstract class LocationBaseCmsInfo : BusinessLogic_v3.Modules._AutoGen.LocationBaseCmsInfo_AutoGen
    {
    	public LocationBaseCmsInfo(LocationBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
	}
}
