﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.SessionState;

namespace BusinessLogic_v3
{
    public static class SessionData
    {
        private const string MEMBER_DISCOUNT_CODE = "memberDiscountCode";

        public static string MemberDiscountCode
        {
            get { return CS.General_v3.Util.PageUtil.GetSessionObject<string>(MEMBER_DISCOUNT_CODE); }
            set { CS.General_v3.Util.PageUtil.SetSessionObject<string>(MEMBER_DISCOUNT_CODE, value); }
        }
    }
}
