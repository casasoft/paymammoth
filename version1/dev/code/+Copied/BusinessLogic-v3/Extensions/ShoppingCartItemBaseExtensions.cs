﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.ProductModule;
using BusinessLogic_v3.Modules.ShoppingCartItemModule;
using BusinessLogic_v3.Modules.ProductVariationModule;

namespace BusinessLogic_v3.Extensions
{
    public static class ShoppingCartItemBaseExtensions
    {
        public static bool ContainsProductVariation(this IEnumerable<ShoppingCartItemBase> list, ProductVariationBase productVariation)
        {
            bool ok = false;
            if (list != null)
            {
                foreach (var item in list)
                {
                    if (item.LinkedProductVariation == productVariation)
                    {
                        ok = true;
                        break;
                    }
                }
            }
            return ok;


        }
        public static bool ContainsProduct(this IEnumerable<ShoppingCartItemBase> list, ProductBase product)
        {
            bool ok = false;
            foreach (var v in product.ProductVariations)
            {
                ok = ContainsProductVariation(list, v);
                if (ok) break;
            }
            return ok;


        }
    }
}
