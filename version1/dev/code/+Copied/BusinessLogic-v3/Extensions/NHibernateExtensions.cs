﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Transactions;

namespace BusinessLogic_v3.Extensions
{
    public static class NHibernateExtensions
    {
        public static void CommitIfNotNull(this MyTransaction transaction)
        {
            if (transaction != null)
            {
                transaction.Commit();
            }
        }
    }
}
