﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.SpecialOfferApplicableCountryModule;
using Iesi.Collections.Generic;

namespace BusinessLogic_v3.Extensions
{
    public static class SpecialOfferApplicableCountryExtensions
    {
        public static HashedSet<CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166> GetAsListOfCountries(this IEnumerable<ISpecialOfferApplicableCountryBase> items)
        {
            HashedSet<CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166> result = new HashedSet<CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166>();
            if (items != null)
            {
                foreach (var item in items)
                {
                    result.Add(item.Country);
                }
            }
            return result;
        }
    }
}
