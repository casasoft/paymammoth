﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using CS.General_v3.Util;

namespace BusinessLogic_v3.Extensions
{
    public static class ListExtensions
    {

        public static void RemoveTemporaryItemsFromList<T>(this ICollection<T> list) where T : IBaseDbObject
        {
            list.RemoveAllFromEnumerable<T>(item => item._Temporary_Flag);


        }
        public static IEnumerable<T> RemoveTemporaryItemsFromListFromEnumerable<T>(this IEnumerable<T> list) where T : IBaseDbObject
        {
            return list.RemoveAllFromEnumerable<T>(item => item._Temporary_Flag);


        }

    }
}
