﻿



using System;
using BusinessLogic_v3.Classes.Interfaces;
using CS.General_v3.Classes.Text;





namespace BusinessLogic_v3.Extensions
{
    public static class IMemberAddressDetailsExtensions
    {
        public static ITokenReplacer FillTokenReplacer(this IMemberAddressDetails memberDetails, ITokenReplacer tokenRep = null, string preTag = null)
        {
            if (tokenRep == null)
                tokenRep =  TokenReplacer.CreateDefault();
            tokenRep["[Member:"+preTag+"FullName]"] = memberDetails.GetFullName();
            tokenRep["[Member:" + preTag + "FirstName]"] = memberDetails.FirstName;
            tokenRep["[Member:" + preTag + "MiddleName]"] = memberDetails.MiddleName;
            tokenRep["[Member:" + preTag + "LastName]"] = memberDetails.LastName;
            tokenRep["[Member:" + preTag + "AddressOneLine]"] = memberDetails.GetAddressAsOneLine();
            return tokenRep;
        }

      

        public static void CopyMemberAddressDetailsExtension(this IMemberAddressDetails copyTo, IMemberAddressDetails copyFrom, bool copyIfAlreadyFilled =false)
        {
            if (string.IsNullOrWhiteSpace(copyTo.AddressLocality) || copyIfAlreadyFilled) copyTo.AddressLocality = copyFrom.AddressLocality;
            if (!copyTo.AddressCountry.HasValue || copyIfAlreadyFilled) copyTo.AddressCountry = copyFrom.AddressCountry;
            if (string.IsNullOrWhiteSpace(copyTo.AddressLine1) || copyIfAlreadyFilled) copyTo.AddressLine1 = copyFrom.AddressLine1;
            if (string.IsNullOrWhiteSpace(copyTo.AddressLine2) || copyIfAlreadyFilled) copyTo.AddressLine2 = copyFrom.AddressLine2;
            if (string.IsNullOrWhiteSpace(copyTo.AddressLine3) || copyIfAlreadyFilled) copyTo.AddressLine3 = copyFrom.AddressLine3;
            if (string.IsNullOrWhiteSpace(copyTo.AddressState) || copyIfAlreadyFilled) copyTo.AddressState = copyFrom.AddressState;
            if (string.IsNullOrWhiteSpace(copyTo.AddressPostCode) || copyIfAlreadyFilled) copyTo.AddressPostCode = copyFrom.AddressPostCode;
            if (string.IsNullOrWhiteSpace(copyTo.FirstName) || copyIfAlreadyFilled) copyTo.FirstName = copyFrom.FirstName;
            if (string.IsNullOrWhiteSpace(copyTo.MiddleName) || copyIfAlreadyFilled) copyTo.MiddleName = copyFrom.MiddleName;
            if (string.IsNullOrWhiteSpace(copyTo.LastName) || copyIfAlreadyFilled) copyTo.LastName = copyFrom.LastName;

        }
        public static string GetAddressAsOneLineExtension(this IMemberAddressDetails memberDetails, string delimeter = ", ", bool includeCountry = false)
        {
            string city = memberDetails.AddressLocality;
            if (!string.IsNullOrWhiteSpace(memberDetails.AddressPostCode))
                city += ", " + memberDetails.AddressPostCode;

            string txt = CS.General_v3.Util.Text.AppendStrings(delimeter, memberDetails.AddressLine1,memberDetails.AddressLine2,memberDetails.AddressLine3, city, memberDetails.AddressState);
            if (includeCountry && memberDetails.AddressCountry.HasValue)
                txt += ". " + CS.General_v3.Util.EnumUtils.StringValueOf(memberDetails.AddressCountry.Value);
            return txt;


        }
        public static string GetFullNameExtension(this IMemberAddressDetails memberDetails)
        {
            return CS.General_v3.Util.Text.AppendStrings(" ", memberDetails.FirstName, memberDetails.MiddleName, memberDetails.LastName);

        }
    }
}
