﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.OrderItemModule;
using BusinessLogic_v3.Modules.ProductModule;
using BusinessLogic_v3.Modules.CategoryModule;
using BusinessLogic_v3.Modules.ShoppingCartItemModule;
using BusinessLogic_v3.Modules.SpecialOfferModule.Helpers;

namespace BusinessLogic_v3.Extensions
{
    public static class ProductExtensions
    {


        /// <summary>
        /// Returns a list of shopping cart items, that match the given categories.  Also matches products whose category is a nested category of the given categories, i.e recursively
        /// </summary>
        /// <param name="categories"></param>
        /// <returns></returns>
        public static List<ISpecialOfferCalculationItem> GetItemsMatchingCategories(this IEnumerable<ISpecialOfferCalculationItem> items, IEnumerable<ICategoryBase> categories)
        {
            List<ISpecialOfferCalculationItem> result = new List<ISpecialOfferCalculationItem>();
            var productList = items.ToList().ConvertAll(x => x.Product);
            var matchingList = GetProductsMatchingCategories(productList, categories);
            foreach (var item in items)
            {
                if (matchingList.Contains(item.Product))
                {
                    result.Add(item);
                }
            }
            
            return result;
        }

        /// <summary>
        /// Returns a list of shopping cart items, that match the given categories.  Also matches products whose category is a nested category of the given categories, i.e recursively
        /// </summary>
        /// <param name="categories"></param>
        /// <returns></returns>
        public static List<IProductBase> GetProductsMatchingCategories(this IEnumerable<IProductBase> products, IEnumerable<ICategoryBase> categories)
        {
            var allSubCategories = CategoryUtil.GetAllSubCategoriesAsFlatList(categories.Cast<CategoryBase>());
            List<IProductBase> result = new List<IProductBase>();
            
            foreach (var item in products)
            {
                var p = item;
                if (p != null)
                {
                    var productCategories = p.GetCategories();//get the product categories
                    foreach (var cat in productCategories)

                        if (allSubCategories.Contains(cat))
                        {//if at least one product category is in the all sub categories list, then this is a match
                            {
                                result.Add(item);
                                break;
                            }
                        }

                }

            }
            return result;
        }
    }
}
