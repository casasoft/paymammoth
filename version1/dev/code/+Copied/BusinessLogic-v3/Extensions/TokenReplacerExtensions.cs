﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using CS.General_v3.Classes.Text;

namespace BusinessLogic_v3.Extensions
{
    public static class TokenReplacerExtensions
    {
        /// <summary>
        /// Sets a token from an item property
        /// </summary>
        /// <typeparam name="TItem">Item class</typeparam>
        /// <typeparam name="TDataType">data type</typeparam>
        /// <param name="item">Item</param>
        /// <param name="preTag">tag to prepend key with</param>
        /// <param name="selector">Property</param>
        /// <param name="dataTypeToString">conversion from data type to string</param>
        public static void SetToken<TItem, TDataType>(this ITokenReplacer tokenRep, TItem item, string preTag, System.Linq.Expressions.Expression<Func<TItem, TDataType>> selector,
            Func<TDataType, string> dataTypeToString)
        {
            PropertyInfo pInfo = CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(selector);
            string key = "[" + preTag + pInfo.Name + "]";
            TDataType value = (TDataType)pInfo.GetValue(item, null);
            string s = null;
            if (dataTypeToString != null)
                s = dataTypeToString(value);
            else
                s = CS.General_v3.Util.Other.ConvertBasicDataTypeToString(value, ifEnumConvertToIntValue: true);
            tokenRep[key]=s;
        }


        public static void SetToken(this ITokenReplacer tokenRep, string key, string value)
        {
            tokenRep[key] = value;
        }

        /// <summary>
        /// Sets tokens from an item, by specifying property selectors
        /// </summary>
        /// <typeparam name="T">Type of object</typeparam>
        /// <param name="item"></param>
        /// <param name="preTag">Any tag to prepend before the property name</param>
        /// <param name="selectors">A list of selectors</param>
        public static void SetTokens<T>(this ITokenReplacer tokenRep, T item, string preTag,
            params System.Linq.Expressions.Expression<Func<T, object>>[] selectors)
        {

            for (int i = 0; i < selectors.Length; i++)
            {
                var selector = selectors[i];
                PropertyInfo pInfo = CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(selector);
                string key = "[" + preTag + pInfo.Name + "]";
                object value = pInfo.GetValue(item, null);
                string s = CS.General_v3.Util.Other.ConvertBasicDataTypeToString(value, ifEnumConvertToIntValue: false);
                tokenRep[key] = s;
                
            }
        }
        /// <summary>
        /// Adds a token from a type
        /// </summary>
        /// <typeparam name="TItem">The item</typeparam>
        /// <typeparam name="TData">The data type returned</typeparam>
        /// <param name="item">The instance of the TItem to load data from</param>
        /// <param name="selector">The property selector</param>
        /// <param name="dataToStringFunc">Conversion from the data type returned by selector, to a string.  If left null, it is converted by a default object-to-string conversion function.</param>
        public static void AddTokenFromType<TItem, TData>(this ITokenReplacer tokenRep, TItem item, System.Linq.Expressions.Expression<Func<TItem, TData>> selector, Func<TData, string> dataToStringFunc = null)
        {
            Type type = typeof(TItem);
            string typeName = type.Name;
            if (typeName.ToLower().EndsWith("base"))
                typeName = typeName.Substring(0, typeName.Length - "base".Length); //remove base

            var property = CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(selector);
            string propertyName = property.Name;

            string key = "[" + typeName + ":" + propertyName + "]";

            object value = property.GetValue(item, null);

            string sValue = null;
            if (dataToStringFunc != null)
                sValue = dataToStringFunc((TData)value);
            else
                sValue = CS.General_v3.Util.Other.ConvertBasicDataTypeToString(value);
            tokenRep[key] = sValue;
            


        }
        /// <summary>
        /// Adds all the public fields of a type as tokens to this token replacer.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item"></param>
        /// <param name="preTag"></param>
        public static void AddAllPublicFieldsOfTypeAsTokens<T>(this ITokenReplacer tokenRep, T item, string preTag = null)
        {
            var properties = CS.General_v3.Util.ReflectionUtil.GetAllPropertiesForType(typeof(T), loadNonPublicTypes: false, loadStaticProperties: false, loadAlsoFromInterfaces: true);

            for (int i = 0; i < properties.Length; i++)
            {
                var pInfo = properties[i];
                string key = "[" + preTag + pInfo.Name + "]";
                object value = pInfo.GetValue(item, null);
                string s = CS.General_v3.Util.Other.ConvertBasicDataTypeToString(value, ifEnumConvertToIntValue: false);
                tokenRep[key] = s;
                
            }
        }
        public static void SetTokens(this ITokenReplacer tokenRep, params string[] nameValuePairs)
        {
            if (nameValuePairs.Length % 2 > 0)
                throw new InvalidOperationException("name value pairs must contain an even number of items");
            for (int i = 0; i < nameValuePairs.Length; i += 2)
            {
                string name = nameValuePairs[i];
                string value = nameValuePairs[i + 1];
                tokenRep.SetToken(name, value);
                

            }

        }

        /// <summary>
        /// Adds tokens from a type
        /// </summary>
        /// <typeparam name="TItem">The item</typeparam>
        /// <param name="item">The instance of the TItem to load data from</param>
        /// <param name="selectors">a list of selectors</param>
        public static void AddTokensFromType<TItem>(this ITokenReplacer tokenRep, TItem item, params System.Linq.Expressions.Expression<Func<TItem, object>>[] selectors)
        {

            for (int i = 0; i < selectors.Length; i++)
            {
                var selector = selectors[i];
                tokenRep.AddTokenFromType<TItem, object>(item, selector, null);

            }

        }

        

    }
}
