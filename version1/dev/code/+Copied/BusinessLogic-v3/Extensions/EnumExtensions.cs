﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Extensions
{
    using Frontend.ArticleModule;
    using Frontend.ContentTextModule;

    public static class EnumExtensions
    {
        public static ContentTextBaseFrontend GetContentTextFrontend(this Enums.CONTENT_TEXT contentText)
        {
            return BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(contentText).ToFrontendBase();
        }
        public static ContentTextBaseFrontend GetContentTextFrontend(this Enum contentText)
        {
            return BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(contentText).ToFrontendBase();
        }
        public static ArticleBaseFrontend GetArticleFrontend(this Enums.CONTENT_PAGE_IDENTIFIER enumValue)
        {
            return BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(enumValue).ToFrontendBase();
        }
        public static ArticleBaseFrontend GetArticleFrontend(this Enum enumValue)
        {
            return BusinessLogic_v3.Modules.Factories.ArticleFactory.GetArticleByIdentifier(enumValue).ToFrontendBase();
        }
        public static T GetSettingValue<T>(this Enum enumValue)
        {
            return BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<T>(enumValue);
        }
        public static T GetSettingValue<T>(this Enums.BusinessLogicSettingsEnum enumValue)
        {
            return BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<T>(enumValue);
        }
        public static T GetSettingValue<T>(this CS.General_v3.Enums.SETTINGS_ENUM enumValue)
        {
            return BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<T>(enumValue);
        }
    }
}
