﻿using System.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Objects;

namespace BusinessLogic_v3.Extensions
{
    public static class IBaseDbObject_Extensions
    {
        public static T FindByPKey<T>(this IEnumerable<T> list, long pkey) where T : class,IBaseDbObject
        {
            foreach (var item in list)
            {
                if (item.ID == pkey)
                {
                    return item;
                }
            }
            return null;
        }
        public static T FindByPKey<T>(this IEnumerable<T> list, IBaseDbObject item) where T : class,IBaseDbObject
        {
            if (item != null)
                return FindByPKey(list, item.ID);
            else
                return null;
        }
    }
}
