﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Interfaces;
using BusinessLogic_v3.Util;


namespace BusinessLogic_v3.Extensions
{
    public static class IItemWithContentTagsExtensions
    {
        
        /// <summary>
        /// Returns all the content tags of an item, based on its types and custom tags
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static List<string> GetAllContentTags(this IItemWithContentTags item)
        {
            List<string> list = new List<string>();
           

            if (item.CustomContentTags != null)
                list.AddRange(item.CustomContentTags);
            
            return list;

        }


        /// <summary>
        /// Returns all the content tags of an item, based on its types and custom tags
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static void CheckIfItemHasTagElseCreate(this IItemWithContentTags item, string tag)
        {
            bool found = false;
            foreach (var t in item.CustomContentTags)
            {
                string tagToCmp = t.Trim();
                if (string.Compare(tagToCmp, tag, true) == 0)
                {
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                var session = nHibernateUtil.GetCurrentSessionFromContext();
                bool createTransaction = session.Transaction == null ||
                                         (session.Transaction != null && !session.Transaction.IsActive);
                try
                {
                    using (var t = createTransaction ? nHibernateUtil.BeginTransactionFromSessionInCurrentContext() : null)
                    {
                        List<string> contentTags = new List<string>(item.CustomContentTags);
                        contentTags.Add(tag);
                        item.CustomContentTags = contentTags.ToArray();
                        item.Update();
                        if (createTransaction)
                        {
                            t.Commit();
                        }
                    }

                }
                catch (Exception ex)
                {
                    int k = 5;
                }

            }
            
        }
        ///// <summary>
        ///// Returns all the content tags of an item, based on its types and custom tags
        ///// </summary>
        ///// <param name="item"></param>
        ///// <returns></returns>
        //public static void CheckIfItemHasTypeElseCreate(this IItemWithContentTags item, Type typeToCheck)
        //{
        //    bool found = false;
        //    foreach (var t in item.TypesForContentTags)
        //    {
        //        if (t == typeToCheck)
        //        {
        //            found = true;
        //            break;
        //        }
        //    }
        //    if (!found)
        //    {
        //        List<Type> typeList = new List<Type>(item.TypesForContentTags);
        //        typeList.Add(typeToCheck);
        //        item.TypesForContentTags = typeList.ToArray();
        //        item.Update();
        //    }
            

        //}

        public static string[] GetCustomContentTagsFromString(string s)
        {
            List<string> list = new List<string>();
            if (!string.IsNullOrWhiteSpace(s))
            {
                list.AddRange(CS.General_v3.Util.Text.Split(s, ",", "|"));
            }
            return list.ToArray();
        }

        //public static Type[] GetTypesForContentTagsFromString(string s)
        //{
        //    return CS.General_v3.Util.ReflectionUtil.GetTypesFromExecutingAssemblies(CS.General_v3.Util.Text.Split(s, ",", "|")).ToArray();
        //}
    }
}
