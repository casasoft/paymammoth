﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.OrderItemModule;

namespace BusinessLogic_v3.Extensions
{
    public static class OrderItemExtensions
    {

        //public static double GetTotalPriceForAllIncVAT(this IEnumerable<IOrderItemBase> list, bool reduceDiscount)
        //{
        //    double d = 0;
        //    foreach (var item in list)
        //    {
        //        d += GetTotalPrice(item,  reduceDiscount);
        //    }
        //    return d;
        //}
        public static int GetTotalQuantityForAll(this IEnumerable<IOrderItemBase> list)
        {
            int qty = 0;
            foreach (var item in list)
            {
                qty += item.Quantity;
            }
            return qty;
        }
        public static double GetTotalPriceExcludingTaxes(this IOrderItemBase orderItem, bool reduceDiscount)
        {
            double d = 0;
            int qty = orderItem.Quantity;

            d += orderItem.GetPriceExcTaxPerUnit(includeDiscount: reduceDiscount) * orderItem.Quantity;
            return Math.Round(d, Constants.General.NUMBER_PRECISION_FINANCIAL);
        }

        public static double GetTotalDiscount(this IOrderItemBase orderItem)
        {

            return orderItem.GetDiscountPerUnit() * orderItem.Quantity;
        }
        //public static double GetTotalPrice(this IOrderItemBase orderItem, bool reduceDiscount)
        //{
        //    double d = 0;

        //    int qty = orderItem.Quantity;

        //    d += orderItem.GetTotalPrice(.GetRetailUnitPrice(includeDiscount: reduceDiscount) * qty;
        // //   if (includeTax) d += orderItem.GetTaxAmountPerUnit() * qty;

        //    //if (reduceDiscount) d -= orderItem.GetTotalDiscount();
        //    return d;
        //}
    }
}
