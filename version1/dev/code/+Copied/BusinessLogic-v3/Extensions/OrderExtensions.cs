﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.OrderModule;
using BusinessLogic_v3.Modules.SpecialOfferModule;
using BusinessLogic_v3.Modules.SpecialOfferVoucherCodeModule;

namespace BusinessLogic_v3.Extensions
{
    public static class OrderExtensions
    {
        

        //public static double GetFixedDiscountForOrder(this IOrderBase order)
        //{
        //    double d = 0;
            
        //    var appliedSpecialOffers = order.GetAppliedSpecialOffers();
        //    foreach (var sp in appliedSpecialOffers)
        //    {
                
        //        d += sp.DiscountFixed;
        //    }
            
        //    return d;

        //}

        //public static double GetTotalPrice(this IOrderBase order, bool includeShipping, bool includeDiscount)
        //{
        //    double dTotal = 0;
        //    if (order != null)
        //    {
        //        var items = order.OrderItems.ToList();
        //        if (items != null)
        //        {

        //            foreach (var item in items)
        //            {
        //                if (item != null)
        //                {
        //                    dTotal += item.GetTotalPrice( includeDiscount);
        //                }

        //            }
        //        }
        //        if (includeDiscount)
        //            dTotal -= order.GetFixedDiscountForOrder();
        //        if (includeShipping)
        //        {
                    
        //            dTotal += order.GetTotalShippingCost().GetValueOrDefault();
        //        }
        //    }
        //    return dTotal;
        //}

        //public static IEnumerable<ISpecialOfferBase> GetAppliedSpecialOffers(this IOrderBase order)
        //{
        //    List<ISpecialOfferBase> list = new List<ISpecialOfferBase>();
        //    if (order.SpecialOfferVoucherCode != null)
        //    {
        //        list.Add((SpecialOfferVoucherCodeBase) order.SpecialOfferVoucherCode);
        //    }
        //    foreach (var item in order.OrderItems)
        //    {
                
        //        if (item.GetApplicableSpecialOffer() != null)
        //            list.Add(item.GetApplicableSpecialOffer());
        //    }
        //    CS.General_v3.Util.ListUtil.RemoveDuplicates(list, ((item1, item2) => (item1.ID.CompareTo(item2.ID))));
        //    return list;
        //}

        public static bool HasItemsToBeShipped(this IOrderBase order)
        {
            foreach (var item in order.OrderItems)
            {
                
                if (item.GetShipmentWeight() > 0)
                    return true;
            }
            return false;
        }

        public static double GetTotalDiscount(this IOrderBase order)
        {
            double d = 0;
            foreach (var item in order.OrderItems)
            {
                d += item.GetTotalDiscount();
            }
            d += order.FixedDiscount;
            return d;
        }

    }
}
