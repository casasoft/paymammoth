﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Objects;

namespace BusinessLogic_v3.Extensions
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<long> GetAllPrimaryKeys(this IEnumerable<IBaseDbObject> dbObjects)
        {
            List<long> list = new List<long>();
            foreach (var item in dbObjects)
            {
                if (item != null)
                {
                    list.Add(item.ID);
                }
            }
            return list;
        }
    }
}
