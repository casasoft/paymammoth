﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.ProductVariationModule;

namespace BusinessLogic_v3.Extensions
{
    public static class ProductVariationExtensions
    {
        public static List<IProductVariationBase> SortProductVariationsByColorTitle(this IEnumerable<IProductVariationBase> list)
        {
            var tmpList = list.ToList();
            tmpList.Sort((x1, x2) =>
                                   {
                                       string title1 = x1.Colour != null ? x1.Colour.Title : "";
                                       string title2 = x2.Colour != null ? x2.Colour.Title : "";
                                       return title1.CompareTo(title2);

                                   });
            return tmpList;
        }
    }
}
