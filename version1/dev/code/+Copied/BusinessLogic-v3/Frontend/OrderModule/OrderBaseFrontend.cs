using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using CS.General_v3.Classes.URL;
using BusinessLogic_v3.Classes.Routing;
using System.Collections.Specialized;
using BusinessLogic_v3.Frontend.ContentTextModule;
using BusinessLogic_v3.Extensions;

namespace BusinessLogic_v3.Frontend.OrderModule
{
    public abstract class OrderBaseFrontend : BusinessLogic_v3.Modules._AutoGen.OrderBaseFrontend_AutoGen
    {

        protected OrderBaseFrontend()
            : base()
        {

        }

        public string GetURL()
        {
            URLClass url = new URLClass(OrderRoute.GetURL(this.Data.ID), false);
            NameValueCollection nvc = new NameValueCollection();
            nvc.Add(Constants.ParameterNames.GUID, this.Data.GUID);
            url.AddQuerystringParamsFromNameValueColl(nvc);
            return url.GetURL(fullyQualified: true);
        }
        public void GetDescription(out string description, out ContentTextBaseFrontend contentText)
        {
            contentText = null;
            description = null;
            var items = Data.OrderItems.ToFrontendBaseList();
            if (items.Count() > 1)
            {
                //More than 1 item
                contentText = BusinessLogic_v3.Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.Orders_MultipleItemDescription).ToFrontendBase();
                contentText.TokenReplacer[BusinessLogic_v3.Constants.Tokens.TAG_AMOUNT] = items.Count().ToString();
            }
            else
            {
                description = items.FirstOrDefault().Data.GetTitle();
            }
        }

        public string GetPayOnlineUrl()
        {
            var urlLink = BusinessLogic_v3.Modules.Factories.RoutingInfoFactory.GetUrlForGeneralRoute(Enums.RouteUrlsEnum.Member_Order_PayOnline,null);
            URLClass url = new URLClass(urlLink);
            url[BusinessLogic_v3.Constants.ParameterNames.ID] = this.Data.ID;
            url[BusinessLogic_v3.Constants.ParameterNames.GUID] = this.Data.GUID;
            return url.GetURL(fullyQualified: true);
        }
    }
}
