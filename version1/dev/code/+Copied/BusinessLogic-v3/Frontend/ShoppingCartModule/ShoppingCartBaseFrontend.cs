using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.Routing;


namespace BusinessLogic_v3.Frontend.ShoppingCartModule
{
    public abstract class ShoppingCartBaseFrontend : BusinessLogic_v3.Modules._AutoGen.ShoppingCartBaseFrontend_AutoGen
    {

        protected ShoppingCartBaseFrontend()
            : base()
        {

        }

        public virtual string GetUrl()
        {
            return BusinessLogic_v3.Modules.RoutingInfoModule.RoutingInfoBaseFactory.Instance.GetUrlForGeneralRoute(Enums.RouteUrlsEnum.ShoppingCart, null);
        }

    }
}
