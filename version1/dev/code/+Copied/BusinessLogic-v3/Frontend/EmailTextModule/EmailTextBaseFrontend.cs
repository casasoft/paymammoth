using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using CS.General_v3.Extensions;
using CS.General_v3.Util;


namespace BusinessLogic_v3.Frontend.EmailTextModule
{
    public abstract class EmailTextBaseFrontend : BusinessLogic_v3.Modules._AutoGen.EmailTextBaseFrontend_AutoGen
    {

        protected EmailTextBaseFrontend()
            : base()
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="contentTagsAndValues">Replace content tags e.g. [TITLE], Test, [AMT], 10</param>
        /// <returns></returns>
        public string GetContent(params string[] contentTagsAndValues)
        {
            return BusinessLogic_v3.Util.TextUtil.ReplaceTagsForContentTagItem(this.Data, contentTagsAndValues);
        }


    }
}
