using BusinessLogic_v3.Frontend.ArticleModule;
using CS.General_v3.Classes.Interfaces.Gallery;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.Routing;


namespace BusinessLogic_v3.Frontend.ClassifiedModule
{
    public abstract class ClassifiedBaseFrontend : BusinessLogic_v3.Modules._AutoGen.ClassifiedBaseFrontend_AutoGen, IContentPageProperties
    {

        protected ClassifiedBaseFrontend()
            : base()
        {

        }

        public string GetURL()
        {
            return ClassifiedRoute.GetURL(Data.Title, Data.ID);
        }

        public string GetSummary()
        {
            return CS.General_v3.Util.Text.LimitText(CS.General_v3.Util.Text.ConvertHTMLToPlainText(Data.Description), BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<int>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Article_SummaryLimit));
        }

        string IContentPageProperties.PageTitle
        {
            get { return Data.Title; }
        }

        bool? IContentPageProperties.DoNotShowFooter
        {
            get { return false; }
        }

        bool? IContentPageProperties.DoNotShowAddThis
        {
            get { return null; }
        }

        bool? IContentPageProperties.DoNotShowLastEditedOn
        {
            get { return false; }
        }

        bool? IContentPageProperties.DoNotShowNavigationBreadcrumbs
        {
            get { return true; }
        }

        string IContentPageProperties._HtmlText_ForEditing
        {
            get { return CS.General_v3.Util.Text.ConvertPlainTextToHTML(Data.Description); }
        }

        string IContentPageProperties.SubTitle
        {
            get { return null; }
        }

        string IContentPageProperties.GetMainImageUrl(string nullImageURL = null)
        {
            return Data.Image.GetSpecificSizeUrl(Modules.ClassifiedModule.ClassifiedBase.ImageSizingEnum.Large);
        }

        string IContentPageProperties.GetContent(params string[] contentTagsAndValues)
        {
            return Data.Description;
        }

        BusinessLogic_v3.Enums.ARTICLE_TYPE IContentPageProperties.ArticleType
        {
            get { return BusinessLogic_v3.Enums.ARTICLE_TYPE.None; }
        }

        int? IContentPageProperties.DialogHeight
        {
            get { return null; }
        }

        int? IContentPageProperties.DialogWidth
        {
            get { return null; }
        }

        bool IContentPageProperties.DialogPage
        {
            get { return false; }
        }

        string IContentPageProperties.GetTitleForSEO()
        {
            return Data.Title;
        }

        string IContentPageProperties.GetMetaDescriptionForSEO(bool ifEmptyMetaDescUseHtmlText)
        {
            return CS.General_v3.Util.Text.LimitText(Data.Description, 150);
        }

        string IContentPageProperties.GetMetaKeywordsForSEO()
        {
            return null;
        }

        string IContentPageProperties.MetaDescription
        {
            get { return CS.General_v3.Util.Text.LimitText(Data.Description, 150); }
        }

        IEnumerable<IMediaItemData> IContentPageProperties.GetMediaItems(bool excludeMainImage)
        {
            return null;
        }

        BusinessLogic_v3.Enums.CONTENT_PAGE_TYPE IContentPageProperties.ContentPageType
        {
            get { return BusinessLogic_v3.Enums.CONTENT_PAGE_TYPE.Classified; }
        }


        public string Title
        {
            get { return Data.Title; }
        }

        public string GetUrl()
        {
            return GetURL();
        }
    }
}
