using BusinessLogic_v3.Extensions;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using System.Web.UI;
using BusinessLogic_v3.Modules.OrderModule;
using BusinessLogic_v3.Frontend.OrderModule;
using CS.General_v3.Controls.WebControls.Common;
using BusinessLogic_v3.Modules.ContentTextModule;


namespace BusinessLogic_v3.Frontend.MemberAccountBalanceHistoryModule
{
    public abstract class MemberAccountBalanceHistoryBaseFrontend : BusinessLogic_v3.Modules._AutoGen.MemberAccountBalanceHistoryBaseFrontend_AutoGen
    {

        protected MemberAccountBalanceHistoryBaseFrontend()
            : base()
        {

        }

        public virtual Control GetDescription()
        {
            MySpan spanDescription = new MySpan();
            if (!string.IsNullOrEmpty(this.Data.Comments))
            {
                spanDescription.InnerHtml = this.Data.Comments;
            }
            else if(CS.General_v3.Util.Other.IsLocalTestingMachine)
            {
                spanDescription.InnerHtml = "Please ovverride this method";
            }
            return spanDescription;
        }

        public double GetMonthlyCredits()
        {
            return this.Data.RenewableBalanceTotal;
        }

        public double GetAdditionalCredits()
        {
            return this.Data.BalanceTotal;
        }

        public double GetCreditDifference()
        {
            return this.Data.BalanceUpdate;
        }

        public double GetTotal()
        {
            return GetMonthlyCredits() + GetAdditionalCredits();
        }
    }
}
