using BusinessLogic_v3.Constants;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.Routing;


namespace BusinessLogic_v3.Frontend.EventSessionModule
{
    public abstract class EventSessionBaseFrontend : BusinessLogic_v3.Modules._AutoGen.EventSessionBaseFrontend_AutoGen
    {

        protected EventSessionBaseFrontend()
            : base()
        {

        }



        public string GetUrl()
        {
            var parentEvent = Data.Event;
            if (parentEvent != null)
            {
                string parentEventTitle = parentEvent.Title;
                var paramValues = new CS.General_v3.Classes.Routing.MyRouteValueDictionary(Tokens.ROUTE_PARAM_TITLE, parentEvent.Title, Tokens.ROUTE_PARAM_ID, parentEvent.ID.ToString());
                return Modules.Factories.RoutingInfoFactory.GetUrlForGeneralRoute(Enums.RouteUrlsEnum.Event,paramValues);
            }
            return null;
        } 
    }
}
