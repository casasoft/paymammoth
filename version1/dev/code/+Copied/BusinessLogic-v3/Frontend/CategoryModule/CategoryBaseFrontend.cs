using BusinessLogic_v3.Classes.Pages;
using BusinessLogic_v3.Classes.Routing;
using BusinessLogic_v3.Extensions;
using CS.General_v3.Classes.Routing;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using CS.General_v3.Classes.Interfaces.Hierarchy;
using BusinessLogic_v3.Classes.URL.ListingURLParser;


namespace BusinessLogic_v3.Frontend.CategoryModule
{
    public abstract class CategoryBaseFrontend : BusinessLogic_v3.Modules._AutoGen.CategoryBaseFrontend_AutoGen, IHierarchyNavigation
    {

        protected CategoryBaseFrontend()
            : base()
        {

        }

        public string GetUrl()
        {
            if (!this.Data.DontShowInNavigation)
            {
                MyRouteValueDictionary routingVariables = new MyRouteValueDictionary();
                routingVariables[ShopRoute.ROUTE_PARAM_CATEGORY_TITLE] = this.Data.Title;
                routingVariables[ShopRoute.ROUTE_PARAM_CATEGORY_ID] = this.Data.ID.ToString();
                return CS.General_v3.Util.RoutingUtil.GetRouteUrl(ShopRoute.ROUTE_NAME_CATEGORY, routingVariables);
            }
            else
            {
                return "";
            }
        }

        public virtual string Href
        {
            get { return GetUrl(); }
        }

        public CS.General_v3.Enums.HREF_TARGET HrefTarget
        {
            get { return CS.General_v3.Enums.HREF_TARGET.Self; }
        }

        public bool IsSelected()
        {
            long? id = ShopRoute.GetCategoryIDFromRouteData();
            if (id.HasValue)
            {
                if (this.Data.ID == id)
                {
                    return true;
                }
            }
            return false;
        }

        public bool Selected
        {
            get { return this.IsSelected(); }
        }

        public long ID
        {
            get { return this.Data.ID; }
        }

        public string Title
        {
            get { return this.Data.Title; }
        }

        public IEnumerable<IHierarchy> GetChildren()
        {
            return this.Data.GetChildCategories().ToFrontendBaseList();
        }

        public IEnumerable<IHierarchy> GetParents()
        {
            return this.Data.GetParentCategories(true).ToFrontendBaseList();
        }

        public bool Visible
        {
            get { return !this.Data.DontShowInNavigation; }
        }

        public bool OmitChildrenInNavigation
        {
            get { return this.Data.OmitChildrenInNavigation; }
        }

        public bool ConsiderAsRootNode
        {
            get { return false; }
        }

        IEnumerable<IHierarchyNavigation> IHierarchyNavigation.GetChildren()
        {
            return this.Data.GetChildCategories().ToFrontendBaseList();
        }

        IEnumerable<IHierarchyNavigation> IHierarchyNavigation.GetParents()
        {
            return this.Data.GetParentCategories(false).ToFrontendBaseList();
        }


        IHierarchyNavigation IHierarchyNavigation.GetMainParent()
        {
            return this.Data.GetParentCategories(false).ToFrontendBaseList().FirstOrDefault();
        }


        IHierarchy IHierarchy.GetMainParent()
        {
            return this.Data.GetParentCategories(false).ToFrontendBaseList().FirstOrDefault();
        }

        public string GetCategoryTitleForSEO()
        {
            CategoryBaseFrontend category = this;
            string categoryTitle = null;
            while (category != null && !category.Data.ConsiderAsRootNode)
            {
                if (!string.IsNullOrEmpty(categoryTitle))
                {
                    categoryTitle += ", ";
                }
                if (!string.IsNullOrEmpty(category.Data.TitlePlural))
                {
                    categoryTitle += category.Data.TitlePlural;
                }
                else
                {
                    categoryTitle += category.Data.TitleSingular;
                }
                category = (CategoryBaseFrontend) category.GetParents().FirstOrDefault();
            }
            return categoryTitle;
        }

        public List<string> GetKeywordsForSEO()
        {
            List<string> keywords = new List<string>();
            string[] keywordsBase = GetCategoryTitleForSEO().Split(',');
            string concatKeyword = "";
            foreach (var keyword in keywordsBase)
            {
                string trimmedKeyword = keyword.Trim().ToLower();
                keywords.Add(trimmedKeyword);
                if (!string.IsNullOrEmpty(concatKeyword))
                {
                    concatKeyword += " ";
                }
                concatKeyword += trimmedKeyword;
                if (string.Compare(trimmedKeyword, concatKeyword, true) != 0)
                {
                    keywords.Add(concatKeyword);
                }
            }
            keywords.Sort((a, b) => a.Length.CompareTo(b.Length));
            return keywords;

        }

        public string GetKeywordsAsStringForSEO()
        {
            return GetKeywordsForSEO().Join(", ");
        }

        public void UpdateSEOForPage(BasePageBL page)
        {
            string keywords = this.Data.MetaKeywords;

            string keywordsSuffix = Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.SEO_KeywordsSuffix);

            if (string.IsNullOrEmpty(keywords))
            {
                keywords = GetKeywordsAsStringForSEO();
                if (!string.IsNullOrEmpty(keywordsSuffix))
                {
                    string maltaKeywords = CS.General_v3.Util.Text.ReplaceTexts(keywords, " " + keywordsSuffix + ",", ",") + " " + keywordsSuffix;
                    keywords += ", " + maltaKeywords;

                }

            }
            string desc = this.Data.MetaDescription;
            if (string.IsNullOrEmpty(desc))
            {
                desc = CS.General_v3.Util.Text.LimitText(CS.General_v3.Util.Text.ConvertHTMLToPlainText(Data.Description, false), 255, true);
            }
            page.UpdateTitleAndMetaTags(GetCategoryTitleForSEO(), keywords, desc);
        }
    }
}
