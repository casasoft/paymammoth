using BusinessLogic_v3.Classes.MediaItems;
using BusinessLogic_v3.Classes.MediaItems.Impl;
using BusinessLogic_v3.Modules.ProductModule;
using BusinessLogic_v3.Modules.ProductVariationMediaItemModule;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using CS.General_v3.Classes.Interfaces.Gallery;
using CS.General_v3.Classes.Interfaces.ImageItem;


namespace BusinessLogic_v3.Frontend.ProductVariationMediaItemModule
{
    public abstract class ProductVariationMediaItemBaseFrontend : BusinessLogic_v3.Modules._AutoGen.ProductVariationMediaItemBaseFrontend_AutoGen, IMediaItemData
    {

        protected ProductVariationMediaItemBaseFrontend()
            : base()
        {
        }

        public string ThumbnailImageUrl
        {
            get {   return this.Data.Image.GetSpecificSizeUrl(ProductVariationMediaItemBase.ImageSizingEnum.Thumbnail); }
        }

        public string LargeImageUrl
        {
            get {   return this.Data.Image.GetSpecificSizeUrl(ProductVariationMediaItemBase.ImageSizingEnum.Large); }
        }

        public string Caption
        {
            get
            {
                if (!string.IsNullOrEmpty(this.Data.Caption))
                {
                    return (this.Data).Caption;
                }
                else
                {
                    var productVariation = this.Data.ProductVariation;
                    if (productVariation != null)
                    {
                        IProductBase product = this.Data.ProductVariation.Product;
                        if (product != null)
                        {
                            return product.GetTitle();
                        }
                    }
                }
                return null;
            }
        }


        public string VideoLink
        {
            get { return this.Data.VideoLink; }
        }


        public string NormalImageUrl
        {
            get { return this.Data.Image.GetSpecificSizeUrl(ProductVariationMediaItemBase.ImageSizingEnum.Normal); }
        }
    }
}
