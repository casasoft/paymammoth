using System.Web.UI;
using BusinessLogic_v3.Classes.Pages;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Frontend.ArticleModule;
using BusinessLogic_v3.Frontend.BrandModule;
using BusinessLogic_v3.Frontend.CategoryModule;
using BusinessLogic_v3.Modules.BrandModule;
using BusinessLogic_v3.Modules.CategoryModule;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using CS.General_v3.Extensions;
using CS.General_v3.Util;


namespace BusinessLogic_v3.Frontend.ProductModule
{
    using Classes.Interfaces;
    using Modules.ProductVariationMediaItemModule;
    using BusinessLogic_v3.Modules.ProductModule;
    using CS.General_v3.Classes.Interfaces.Gallery;

    public abstract class ProductBaseFrontend : BusinessLogic_v3.Modules._AutoGen.ProductBaseFrontend_AutoGen, IListingItemData, IContentPageProperties
    {

        protected ProductBaseFrontend()
            : base()
        {

        }

        public string GetTitle()
        {
            return Data.GetTitle();
        }
        

        
        

        /*private string getDescription(bool isShortDescription, bool requiredAsPlainText)
        {
            string desc = null;
            if (isShortDescription)
            {
                
            }
            else
            {
                //desc = CS.General_v3.Util.Text.ConvertHTMLToPlainText(this.Data.Description, false);
                desc = this.Data.Description;
                if (string.IsNullOrWhiteSpace(desc))
                {
                    desc = (this.Data.ShortDescription);
                }
            }

            

            if (!string.IsNullOrWhiteSpace(desc))
            {
                if (requiredAsPlainText)
                {
                    desc = CS.General_v3.Util.Text.ConvertHTMLToPlainText(desc, false);
                }
            }

            if (string.IsNullOrWhiteSpace(desc))
            {
                var noDescriptionCntTxt = Modules.Factories.ContentTextFactory.GetContentTextByGenericIdentifier(Enums.CONTENT_TEXT.ProductPage_Description_NoDescription);
                desc = noDescriptionCntTxt.GetContent();
            }
            return desc;
        }
        */
        public string ImageUrl
        {
            get { return getImageUrl(); }
        }

        protected virtual string getImageUrl()
        {
            var img = this.Data.GetMainImage();
            if (img != null)
            {
                return img.Image.GetSpecificSizeUrl(ProductVariationMediaItemBase.ImageSizingEnum.ListingThumbnail);
            }
            else
            {
                var defaultImageUrl = Modules.Factories.SettingFactory.GetSettingValue<string>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Products_DefaultImageUrl);
                return defaultImageUrl;
            }
        }

        public string Url
        {
            get { return this.Data.GetFrontendUrl(); }
        }

        public long ID
        {
            get { return this.Data.ID; }
        }

        public string GetMetaDescription()
        {
            string metaDesc = null;
            if (!string.IsNullOrEmpty(this.Data.MetaDescription))
            {
                metaDesc = this.Data.MetaDescription;
            }
            if (!string.IsNullOrEmpty(this.Data.ShortDescription))
            {
                metaDesc = this.Data.ShortDescription;
            }
            else if (!string.IsNullOrEmpty(this.Data.Description))
            {
                metaDesc = this.Data.Description;
            }
            metaDesc = CS.General_v3.Util.SEOUtil.GetDescriptionFromHTMLParagraphTexts(metaDesc);
            return metaDesc;
        }
        public string GetMetaTitle(bool includeParentCategory = true)
        {
            string title = this.GetTitle();
            if (includeParentCategory)
            {
                var parentCat = this.Data.GetCategories().FirstOrDefault();
                if (parentCat != null)
                {
                    string catTitle = parentCat.GetTitleForSEO();
                    if (string.IsNullOrEmpty(title) || !title.ToLower().Contains(catTitle.ToLower())) //not to duplicate
                    { 
                        if (!string.IsNullOrWhiteSpace(title)) title += ", ";
                        title += catTitle;
                    }
                }
            }
            title = CS.General_v3.Util.Text.ToCamelCase(title);
            return title;
        }
        public string GetMetaKeywords(bool includeTitle = true, bool includeCategories = true)
        {

            List<string> keywords = new List<string>();
            if (!string.IsNullOrEmpty(this.Data.MetaKeywords)) keywords.Add(this.Data.MetaKeywords);
            if (includeTitle)
            {
                keywords.Add(this.Data.GetTitle());
            }
            if (includeCategories)
            {
                var categories = this.Data.GetCategories();
                foreach (var cat in categories)
                {
                    keywords.Add(cat.GetTitleForSEO());
                }
            }
            return CS.General_v3.Util.ListUtil.JoinList(keywords, ", ");
        }


        public bool HasDiscount
        {
            get {
                return this.Data.HasDiscountPrice(); }
        }

        

        public double PriceActual
        {
            get
            {
                return this.Data.PriceInfo.GetTotalPrice();
            }
        }
    
        public double PriceBeforeDiscount
        {
            get { return this.Data.PriceInfo.GetUnitPrice(reduceDiscount:false); }
        }

        private string getCategoriesAsStringForSEO(IEnumerable<CategoryBaseFrontend> cats)
        {
            string str = "";
            foreach (var cat in cats)
            {
                if (str.ToLower().IndexOf(cat.Data.TitleSingular.ToLower()) == -1)
                {
                    if (!string.IsNullOrEmpty(str)) str += ", ";
                    CategoryBaseFrontend currCat = cat;
                    while (currCat != null && !currCat.Data.ConsiderAsRootNode)
                    {
                        if (!string.IsNullOrEmpty(str)) str += " ";
                        if (!string.IsNullOrEmpty(cat.Data.TitleSingular))
                        {
                            str += currCat.Data.TitleSingular;
                        }
                        else
                        {
                            str += currCat.Data.TitlePlural;
                        }
                        str = str.Trim();
                        currCat = ((ICategoryBase)currCat.GetParents().FirstOrDefault()).ToFrontendBase();
                    }

                }
            }
            return str.Trim();
        }


        public void UpdateSEOForPage(BasePageBL page)
        {
            string pageTitle = Data.MetaTitle;
            if (string.IsNullOrEmpty(pageTitle)) pageTitle = Data.GetTitle();
            IEnumerable<CategoryBaseFrontend> productCategories = this.Data.GetCategories().ToFrontendBaseList();
            if (productCategories.Count() > 0)
            {
                string categories = getCategoriesAsStringForSEO(productCategories);
                if (!string.IsNullOrEmpty(categories))
                {
                    pageTitle += " - " + categories;
                }
                IBrandBase brand = this.Data.Brand;
                if (brand != null)
                {
                    pageTitle += " - " + brand.Title;
                }
            }

            CategoryBaseFrontend productCategory = this.Data.GetCategories().FirstOrDefault().ToFrontendBase();
            StringBuilder strKeywords = new StringBuilder();

            if (productCategory != null)
            {
                string strProductCategories = productCategory.GetKeywordsAsStringForSEO();
                strKeywords.Append(productCategories);
            }
            if (strKeywords.Length > 0)
            {
                strKeywords.Append(", ");
            }
            strKeywords.Append(Data.GetTitle().ToLower());
            var productBrand = this.Data.Brand.ToFrontendBase();
            if (productBrand != null)
            {
                if (strKeywords.Length > 0)
                {
                    strKeywords.Append(", ");
                }
                strKeywords.Append(productBrand.Data.Title.ToLower());
            }
            string keywords = strKeywords.ToString();
            string keywordsSuffix =
                Modules.Factories.SettingFactory.GetSettingValue<string>(
                    BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.SEO_KeywordsSuffix);
            if (!string.IsNullOrEmpty(keywordsSuffix))
            {
                string maltaKeywords = Text.ReplaceTexts(keywords, " " + keywordsSuffix + ",", ",") + " " +
                                       keywordsSuffix;
                keywords += ", " + maltaKeywords;
            }
            string desc = this.Data.MetaDescription;
            if (string.IsNullOrEmpty(desc))
            {
                desc = Text.LimitText(Text.ConvertHTMLToPlainText(Data.Description, false), 255, true);
            }
            var mainImage = this.Data.GetMainImage().ToFrontendBase();
            page.UpdateTitleAndMetaTags(pageTitle, keywords, desc);
            var imageURL = mainImage.Data.Image.GetSpecificSizeUrl(ProductVariationMediaItemBase.ImageSizingEnum.Normal);
            var singleItemImgURL = imageURL;
            page.UpdateOpenGraphPageType(CS.General_v3.Enums.OPEN_GRAPH_TYPE.Product);
            page.UpdateOpenGraphTag(CS.General_v3.Enums.OPEN_GRAPH.Image,
                                    PageUtil.ConvertRelativeUrlToAbsoluteUrl(singleItemImgURL));
        }

        string IListingItemData.ReferenceCode
        {
            get
            {
                return this.Data.ReferenceCode;
            }
        }

        Classes.DbObjects.Objects.IBaseDbObject IListingItemData.DbObject
        {
            get { return this.Data;  }
        }


        public DateTime CreatedOnDate
        {
            get
            {
                return this.Data.LastEditedOn.HasValue ? this.Data.LastEditedOn.Value : CS.General_v3.Util.Date.Now;
            }
            set
            {
                this.Data.LastEditedOn = value;
            }
        }

        public Control GetFreeText()
        {
            return null;
        }

        string IContentPageProperties.PageTitle
        {
            get { return ((ProductBase)Data).PageTitle; }
        }

        public virtual bool? DoNotShowFooter
        {
            get { return null; }
        }

        public virtual bool? DoNotShowAddThis
        {
            get { return null; }
        }

        public virtual bool? DoNotShowLastEditedOn
        {
            get { return null; }
        }

        public virtual bool? DoNotShowNavigationBreadcrumbs
        {
            get { return true; }
        }

        string IContentPageProperties._HtmlText_ForEditing
        {
            get { return ((ProductBase) Data)._HtmlText_ForEditing; }
        }

        string IContentPageProperties.Title
        {
            get { return Data.GetTitle(); }
        }

        string IContentPageProperties.SubTitle
        {
            get { return null; }
        }

        string IContentPageProperties.GetUrl()
        {
            return Data.GetFrontendUrl();
        }

        string IContentPageProperties.GetMainImageUrl(string nullImageURL = null)
        {
            return this.Data.GetMainImage().Image.GetSpecificSizeUrl(ProductVariationMediaItemBase.ImageSizingEnum.Large);
        }

        string IContentPageProperties.GetContent(params string[] contentTagsAndValues)
        {
            return Data.GetDescription(false);
        }

        Enums.ARTICLE_TYPE IContentPageProperties.ArticleType
        {
            get { return Enums.ARTICLE_TYPE.None; }
        }

        int? IContentPageProperties.DialogHeight
        {
            get { return null; }
        }

        int? IContentPageProperties.DialogWidth
        {
            get { return null; }
        }

        bool IContentPageProperties.DialogPage
        {
            get { return false; }
        }

        string IContentPageProperties.GetTitleForSEO()
        {
            return this.Data.GetTitle();
        }

        string IContentPageProperties.GetMetaDescriptionForSEO(bool ifEmptyMetaDescUseHtmlText)
        {
            return Data.MetaKeywords;
        }

        string IContentPageProperties.GetMetaKeywordsForSEO()
        {
            return Data.MetaKeywords;
        }

        string IContentPageProperties.MetaDescription
        {
            get { return Data.MetaDescription; }
        }

        public virtual IEnumerable<IMediaItemData> GetMediaItems(bool excludeMainImage)
        {
            return Data.GetAllImages().ToFrontendBaseList();
        }


        public Enums.CONTENT_PAGE_TYPE ContentPageType
        {
            get { return Enums.CONTENT_PAGE_TYPE.Product; }
        }


        public string GetShortDescription(bool convertFullDescriptionToPlainTextIfShortDescriptionIsEmpty, bool requiredAsHtml)
        {
            return Data.GetShortDescription(convertFullDescriptionToPlainTextIfShortDescriptionIsEmpty, requiredAsHtml);
        }
    }
        
}
