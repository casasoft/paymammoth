using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using CS.General_v3.Classes.Interfaces.BlogPosts;


namespace BusinessLogic_v3.Frontend.ArticleCommentModule
{
    public abstract class ArticleCommentBaseFrontend : BusinessLogic_v3.Modules._AutoGen.ArticleCommentBaseFrontend_AutoGen
    {

        protected ArticleCommentBaseFrontend()
            : base()
        {

        }
    }
}
