using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using CS.General_v3.Extensions;
using CS.General_v3.Util;


namespace BusinessLogic_v3.Frontend.CultureDetailsModule
{
    public abstract class CultureDetailsBaseFrontend : BusinessLogic_v3.Modules._AutoGen.CultureDetailsBaseFrontend_AutoGen
    {

        protected CultureDetailsBaseFrontend()
            : base()
        {

        }
        public string GetBaseRedirectionURL()
        {
            if (CS.General_v3.Util.Other.IsLocalTestingMachine && !string.IsNullOrWhiteSpace(this.Data.BaseRedirectionUrlLocalhost))
            {
                
                return this.Data.BaseRedirectionUrlLocalhost;

            }
            else
            {
                return this.Data.BaseRedirectionUrl;
            }
        }


    }
}
