using BusinessLogic_v3.Classes.MediaItems;
using BusinessLogic_v3.Classes.MediaItems.Impl;
using BusinessLogic_v3.Modules.EventMediaItemModule;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using CS.General_v3.Classes.Interfaces.Gallery;


namespace BusinessLogic_v3.Frontend.EventMediaItemModule
{
    public abstract class EventMediaItemBaseFrontend : BusinessLogic_v3.Modules._AutoGen.EventMediaItemBaseFrontend_AutoGen, IMediaItemData
    {

        protected EventMediaItemBaseFrontend()
            : base()
        {

        }

        public string ThumbnailImageUrl
        {
            get { return this.Data.Image.GetSpecificSizeUrl(EventMediaItemBase.ImageSizingEnum.Thumbnail); }
        }

        public string LargeImageUrl
        {
            get { return this.Data.Image.GetSpecificSizeUrl(EventMediaItemBase.ImageSizingEnum.Normal); }
        }

        public string Caption
        {
            get
            {
                string title = this.Data.Title;
                if(!string.IsNullOrWhiteSpace(title))
                {
                    return title;
                }
                if(this.Data.Event!=null)
                {
                    return Data.Event.Title;
                }
                if(this.Data.EventBasic != null)
                {
                    return Data.EventBasic.Title;
                }
                return null;
            }
        }

        public string VideoLink
        {
            get { return this.Data.VideoLink; }
        }


        public string NormalImageUrl
        {
            get { return this.Data.Image.GetSpecificSizeUrl(EventMediaItemBase.ImageSizingEnum.Normal); }
        }
    }
}
