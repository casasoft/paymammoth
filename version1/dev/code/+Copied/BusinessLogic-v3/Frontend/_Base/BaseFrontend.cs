﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Objects;

namespace BusinessLogic_v3.Frontend._Base
{

    public abstract class BaseFrontend<TClass, TData> : BaseFrontend, IBaseFrontend<TData>
        where TData : IBaseDbObject
        where TClass : BaseFrontend<TClass, TData>
    {
        public new TData Data
        {
            get { return (TData) base.Data; }
            set { base.Data = value; }

        }
        public void ReloadDataFromDatabase()
        {
            long pKey = this.Data.ID;
            var factory = BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryForType(typeof(TData));
            this.Data = (TData) factory.GetByPrimaryKey(pKey);

        }

        public static TClass Get(TData data)
        {
            if (data != null)
            {
                var newItem = CS.General_v3.Util.ReflectionUtil.CreateObjectWithPrivateConstructorForLastType<TClass>();
                newItem.Data = data;
                return newItem;
            }
            else
            {
                return null;
            }
        }

        public static TClass CreateNewItem()
        {
            var newItem = CS.General_v3.Util.ReflectionUtil.CreateObjectWithPrivateConstructorForLastType<TClass>();
            var data = BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.CreateItemWhichImplementsType<TData>();


            newItem.Data = data;
            return newItem;
        }

        public static List<TClass> GetList(IEnumerable<TData> dataList)
        {
            List<TClass> list = new List<TClass>();
            if (dataList != null)
            {
                foreach (var item in dataList)
                {
                    var frontendItem = Get(item);
                    if (frontendItem != null)
                        list.Add(frontendItem);
                }
            }
            return list;
        }

        protected BaseFrontend()
        {
            
        }




        #region IBaseFrontend Members

        IBaseDbObject IBaseFrontend.Data
        {
            get { return this.Data; }
        }

        #endregion
    }

    public abstract class BaseFrontend : IBaseFrontend
    {

        #region IBaseFrontend Members

        public IBaseDbObject Data { get; protected set; }
        #endregion
    }
}
