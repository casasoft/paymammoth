using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Objects;

namespace BusinessLogic_v3.Frontend._Base
{
    public interface IBaseFrontend<T> :IBaseFrontend
        where T : IBaseDbObject
    {
        new T Data { get; }
    }
    public interface IBaseFrontend
    {
        IBaseDbObject Data { get; }
    }
}