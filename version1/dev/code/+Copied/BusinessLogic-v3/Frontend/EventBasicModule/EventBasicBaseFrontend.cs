using BusinessLogic_v3.Constants;
using BusinessLogic_v3.Extensions;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.Interfaces;
using CS.General_v3.Controls.WebControls.Common;
using BusinessLogic_v3.Classes.Text;


namespace BusinessLogic_v3.Frontend.EventBasicModule
{
    public abstract class EventBasicBaseFrontend : BusinessLogic_v3.Modules._AutoGen.EventBasicBaseFrontend_AutoGen, IListingItemData
    {

        protected EventBasicBaseFrontend()
            : base()
        {

        }

        public string GetUrl()
        {
            var paramValues = new CS.General_v3.Classes.Routing.MyRouteValueDictionary(Tokens.ROUTE_PARAM_TITLE, this.Data.Title, Tokens.ROUTE_PARAM_ID, this.Data.ID.ToString());
            return Modules.Factories.RoutingInfoFactory.GetUrlForGeneralRoute(Enums.RouteUrlsEnum.EventBasic, paramValues);
        }


        public Classes.DbObjects.Objects.IBaseDbObject DbObject
        {
            get { return Data; }
        }

        public long ID
        {
            get { return Data.ID; }
        }

        public string GetTitle()
        {
            return Data.Title;
        }

        public string GetShortDescription(bool convertFullDescriptionToPlainTextIfShortDescriptionIsEmpty, bool requiredAsHtml)
        {
            return Data.Summary;
        }

        public string ImageUrl
        {
            get { return getMainImageUrl(); }
        }

        private string getMainImageUrl()
        {
            if(!Data.Image.IsEmpty())
            {
                return Data.Image.GetSpecificSizeUrl(Modules.EventBasicModule.EventBasicBase.ImageSizingEnum.Normal);
            }
            var mediaItem = Data.GetMediaItems().FirstOrDefault();
            if(mediaItem!= null)
            {
                return
                    mediaItem.Image.GetSpecificSizeUrl(
                        Modules.EventMediaItemModule.EventMediaItemBase.ImageSizingEnum.Thumbnail);
            }
            return null;
        }

        public string Url
        {
            get { return GetUrl(); }
        }

        public bool HasDiscount
        {
            get { throw new NotImplementedException(); }
        }

        public double PriceActual
        {
            get { throw new NotImplementedException(); }
        }

        public double PriceBeforeDiscount
        {
            get { throw new NotImplementedException(); }
        }

        public string ReferenceCode
        {
            get { throw new NotImplementedException(); }
        }

        public DateTime CreatedOnDate
        {
            get { return Data.StartDate; }
        }

        public System.Web.UI.Control GetFreeText()
        {
            MySpan span = new MySpan("events-listing-locationtext");
            var ct = Enums.CONTENT_TEXT.EventsListing_Location_Text.GetContentTextFrontend();
            TokenReplacerNew tr = new TokenReplacerNew(Tokens.LOCATION, Data.Location);
            span.InnerHtml = ct.GetContent(tr);
            return span;
        }
    }
}
