using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using System.Web.UI.WebControls;
using CS.General_v3.Classes.HelperClasses;


namespace BusinessLogic_v3.Frontend.AttachmentModule
{
    public abstract class AttachmentBaseFrontend : BusinessLogic_v3.Modules._AutoGen.AttachmentBaseFrontend_AutoGen
    {

        protected AttachmentBaseFrontend()
            : base()
        {

        }
        
        public OperationResult AddFileUpload(FileUpload uploadData)
        {
            return Data.UploadFile(uploadData.FileContent, uploadData.FileName);
        }

    }
}
