using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using CS.General_v3.Classes.Interfaces.Gallery;
using BusinessLogic_v3.Modules.ProductVersionMediaItemModule;


namespace BusinessLogic_v3.Frontend.ProductVersionMediaItemModule
{
    public abstract class ProductVersionMediaItemBaseFrontend : BusinessLogic_v3.Modules._AutoGen.ProductVersionMediaItemBaseFrontend_AutoGen, IMediaItemData
    {

        protected ProductVersionMediaItemBaseFrontend()
            : base()
        {

        }

        public string ThumbnailImageUrl
        {
            get { return this.Data.Image.GetSpecificSizeUrl(ProductVersionMediaItemBase.ImageSizingEnum.Thumbnail); }
        }

        public string NormalImageUrl
        {
            get { return this.Data.Image.GetSpecificSizeUrl(ProductVersionMediaItemBase.ImageSizingEnum.Normal); }
        }

        public string LargeImageUrl
        {
            get { return this.Data.Image.GetSpecificSizeUrl(ProductVersionMediaItemBase.ImageSizingEnum.Large); }
        }

        public string Caption
        {
            get
            {
                if (!string.IsNullOrEmpty(this.Data.Caption))
                {
                    return this.Data.Caption;
                }
                else
                {
                    var version = this.Data.ProductVersion;
                    if (version != null)
                    {
                        return version.Title;
                    }
                }
                return null;
            }
        }

        public string VideoLink
        {
            get { return null; }
        }
    }
}
