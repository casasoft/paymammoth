using BusinessLogic_v3.Extensions;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Util;


namespace BusinessLogic_v3.Frontend.EventSubmissionModule
{
    public abstract class EventSubmissionBaseFrontend : BusinessLogic_v3.Modules._AutoGen.EventSubmissionBaseFrontend_AutoGen
    {

        protected EventSubmissionBaseFrontend()
            : base()
        {

        }

        /// <summary>
        /// Used to call the Save() method and wrap it inside a transaction if autoSave is true.
        /// </summary>
        /// <param name="autoSave"></param>
        public void Save(bool autoSave=true)
        {
            using (var t = nHibernateUtil.BeginTransactionBasedOnAutoSaveBool(autoSave))
            {
                this.Data.Save();
                t.CommitIfNotNull();
            }
        }

    }
}
