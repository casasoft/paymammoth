using BusinessLogic_v3.Classes.MediaItems;
using BusinessLogic_v3.Classes.MediaItems.Impl;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.Interfaces;
using BusinessLogic_v3.Classes.Text;


namespace BusinessLogic_v3.Frontend.ContentTextModule
{
    public abstract class ContentTextBaseFrontend : BusinessLogic_v3.Modules._AutoGen.ContentTextBaseFrontend_AutoGen
    {
        private TokenReplacerNew _tokenReplacer;
        public TokenReplacerNew TokenReplacer
        {
            get
            {
                if (_tokenReplacer == null) _tokenReplacer = CreateTokenReplacer();
                return _tokenReplacer;
            }
        }
            protected ContentTextBaseFrontend()
            : base()
        {
            
        }
           

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contentTagsAndValues">Replace content tags e.g. [TITLE], Test, [AMT], 10</param>
        /// <returns></returns>
        public string GetContent(params string[] contentTagsAndValues)
        {
            return BusinessLogic_v3.Util.TextUtil.ReplaceTagsForContentTagItem(this.Data, contentTagsAndValues);
        }
        /*public string GetContent(Dictionary<string, string> replacementTags)
        {
            return BusinessLogic_v3.Util.TextUtil.ReplaceTagsForContentTagItem(this.Data, replacementTags);
        }*/
        public string GetContent(TokenReplacerNew replacementTags = null)
        {
            if (replacementTags == null && _tokenReplacer != null)
            {
                replacementTags = _tokenReplacer;
            }
            this._tokenReplacer = replacementTags;
           /* if (true) //TODO: 2012-07-03 Added by Mark as can't stand the waiting any more
            {
                return this.Data.GetContent();
            }
            else
            {*/
            
                return BusinessLogic_v3.Util.TextUtil.ReplaceTagsForContentTagItem(this.Data, replacementTags);
           // }
        }


        public Classes.Text.TokenReplacerNew CreateTokenReplacer()
        {
            return new Classes.Text.TokenReplacerNew(this.Data);
        }
        public void UpdateTokenReplacer(TokenReplacerNew tokenReplacer)
        {
            this._tokenReplacer = tokenReplacer;
        }
    }
}
