using System.Web.UI;
using BusinessLogic_v3.Classes.MediaItems;
using BusinessLogic_v3.Classes.MediaItems.Impl;
using BusinessLogic_v3.Modules.ArticleMediaItemModule;
using BusinessLogic_v3.Modules.ArticleModule;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using CS.General_v3.Classes.Interfaces.Hierarchy;


namespace BusinessLogic_v3.Frontend.ArticleModule
{
    using CS.General_v3.Classes.Factories;
    using Extensions;
    using BusinessLogic_v3.Modules._AutoGen;
    using BusinessLogic_v3.Classes.Text;
    using IListingItemData = Classes.Interfaces.IListingItemData;
    using BusinessLogic_v3.Classes.Interfaces;
    using BusinessLogic_v3.Classes.DbObjects.Objects;
    using CS.General_v3.Classes.Interfaces.Gallery;

    public abstract class ArticleBaseFrontend : BusinessLogic_v3.Modules._AutoGen.ArticleBaseFrontend_AutoGen, IHierarchyNavigation, IListingItemData, IContentPageProperties
    {

        protected ArticleBaseFrontend()
            : base()
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="contentTagsAndValues">Replace content tags e.g. [TITLE], Test, [AMT], 10</param>
        /// <returns></returns>
        public string GetContent(params string[] contentTagsAndValues)
        {
            return BusinessLogic_v3.Util.TextUtil.ReplaceTagsForContentTagItem(this.Data, contentTagsAndValues);
        }

        public string GetSummary()
        {
            if(!String.IsNullOrEmpty(Data.Summary))
            {
                return Data.Summary;
            }
            return CS.General_v3.Util.Text.LimitText(
                CS.General_v3.Util.Text.ConvertHTMLToPlainText(Data._HtmlText_ForEditing),
                BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<int>(
                    BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Article_SummaryLimit));
        }


        public string GetTitleForSEO()
        {
            string title = Data.MetaTitle;
            if (string.IsNullOrWhiteSpace(title))
            {
                title = Data.PageTitle;
            }
            return title;
        }
        public string GetMetaKeywordsForSEO()
        {
            string metaKeywords = Data.MetaKeywords;
            return metaKeywords;
        }
        public string GetMetaDescriptionForSEO(bool ifEmptyMetaDescUseHtmlText = true)
        {
            string metaDesc = Data.MetaDescription;
            if (ifEmptyMetaDescUseHtmlText && string.IsNullOrWhiteSpace(metaDesc) && Data.GetContent() != null)
            {
                metaDesc = CS.General_v3.Util.Text.ConvertHTMLToPlainText(Data.GetContent(), false);
                int maxDescLength = BusinessLogic_v3.Modules.SettingModule.SettingBaseFactory.Instance.GetSetting<int>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.SEO_MaxDescriptionLength);
                if (metaDesc.Length > maxDescLength)
                {
                    metaDesc = metaDesc.Substring(0, maxDescLength);
                }
            }
            return metaDesc;
        }
      
        public virtual string GetUrl()
        {
            return this.Data.GetUrl();
        }
        public IMediaItemImage<ArticleMediaItemBase.ImageSizingEnum> GetMainImageFromMediaItems()
        {
           
            IArticleMediaItemBase mediaItem = this.Data.ArticleMediaItems.Where(item => item.Image != null).OrderBy(item => item.Priority).FirstOrDefault();
            if (mediaItem != null)
            {
                return mediaItem.Image;
            }
            else
            {
                return null;
            }

        }

        public IMediaItemImage<ArticleBase.ImageSizingEnum> GetMainImage()
        {
            return this.Data.MainImage;
        }
        public string GetMainImageUrl(string nullImageURL = null)
        {
            return GetMainImageUrlForSize(nullImageURL, ArticleBase.ImageSizingEnum.Normal, ArticleMediaItemBase.ImageSizingEnum.Normal);
        }

        public string GetMainImageUrlForSize(string nullImageURL = null, ArticleBase.ImageSizingEnum mainImageSizeIfAvailable = ArticleBase.ImageSizingEnum.Normal, ArticleMediaItemBase.ImageSizingEnum mediaItemSizeIfAvailable = ArticleMediaItemBase.ImageSizingEnum.Normal)
        {

            if (this.Data.MainImage != null && !this.Data.MainImage.IsEmpty())
            {
                return this.Data.MainImage.GetSpecificSizeUrl(mainImageSizeIfAvailable);
            }
            else
            {
                var image = GetMainImageFromMediaItems();
                if (image != null)
                {
                    return image.GetSpecificSizeUrl(mediaItemSizeIfAvailable);
                }
                else
                {
                    return nullImageURL;
                }
            }
        }

        #region IHierarchyNavigation Members

        string IHierarchyNavigation.Href
        {
            get { return this.GetUrl(); }
        }

        CS.General_v3.Enums.HREF_TARGET IHierarchyNavigation.HrefTarget
        {
            get { return ((IHierarchyNavigation)this.Data).HrefTarget; }
        }

        bool IHierarchyNavigation.Selected
        {
            get { return ((IHierarchyNavigation)this.Data).Selected; }
        }
        #endregion

        #region IHierarchy Members

        long IHierarchy.ID
        {
            get { return ((IHierarchy)this.Data).ID; }
        }

        string IHierarchy.Title
        {
            get { return ((IHierarchy)this.Data).Title; }
        }
        IEnumerable<IHierarchy> IHierarchy.GetChildren()
        {
            return this.Data.GetChildArticles();
        }

        IEnumerable<IHierarchy> IHierarchy.GetParents()
        {
            return ((IHierarchy)this.Data).GetParents();
        }

        bool IHierarchy.Visible
        {
            get { return ((IHierarchy)this.Data).Visible; }
        }


        #endregion


        IEnumerable<IHierarchyNavigation> IHierarchyNavigation.GetChildren()
        {
            return this.Data.GetChildArticles();
        }

        IEnumerable<IHierarchyNavigation> IHierarchyNavigation.GetParents()
        {
            return ((IHierarchyNavigation)this.Data).GetParents();
        }


        IHierarchyNavigation IHierarchyNavigation.GetMainParent()
        {
            return this.Data.GetParentMain();
        }


        IHierarchy IHierarchy.GetMainParent()
        {
            return this.Data.GetParentMain();
        }

        string IListingItemData.GetTitle()
        {
            return this.Data.Title;
        }

        string IListingItemData.GetShortDescription(bool convertFullDescriptionToPlainTextIfShortDescriptionIsEmpty, bool requiredAsHtml)
        {
            string desc = null;
            if (!string.IsNullOrEmpty(this.Data.Summary))
            {
                desc =  this.Data.Summary;
            }
            else if (!string.IsNullOrEmpty(this.Data.MetaDescription))
            {
                desc =  this.Data.MetaDescription;
            }
            else
            {
                int limitAmt = Factories.SettingFactory.GetSettingValue<int>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Article_DescriptionLimitValue);
                desc = this.Data.GetContent();
                if (convertFullDescriptionToPlainTextIfShortDescriptionIsEmpty)
                {
                    desc = CS.General_v3.Util.Text.ConvertHTMLToPlainText(this.Data.GetContent());
                }
                desc = CS.General_v3.Util.Text.LimitText(desc, limitAmt);
            }
            return desc;
        }

        string IListingItemData.ImageUrl
        {
            get
            {
                string imageUrl = this.GetMainImageUrl();
                return !string.IsNullOrEmpty(imageUrl) ? imageUrl : Modules.Factories.SettingFactory.GetSettingValue<string>(Enums.BusinessLogicSettingsEnum.Listing_DefaultImageUrl);
            }
        }

        string IListingItemData.Url
        {
            get { return this.GetUrl(); }
        }
        /// <summary>
        /// Check whether this article is of a particular type
        /// </summary>
        /// <param name="articleType"></param>
        /// <param name="checkParents">Whether to check recursively upwards</param>
        /// <returns></returns>
        public bool IsArticleType(Enums.ARTICLE_TYPE articleType, bool checkParents = true)
        {
            ArticleBaseFrontend article = this;
            bool ok = false;
            while (!ok && article != null)
            {
                ok = article.Data.ArticleType == articleType;
                if (checkParents)
                {
                    article = article.Data.GetParentMain().ToFrontendBase();
                }
                else
                {
                    article = null;
                }
            }
            return ok;
        }

        public bool IsCommentable()
        {
            bool commentable = false;

            if (this.Data.IsCommentable.HasValue)
            {
                commentable = this.Data.IsCommentable.Value;
            }
            else
            {
                bool? commentableFromParents = Data.IsInheritedCommentable_Computed;
                if (commentableFromParents.HasValue)
                {
                    return commentableFromParents.Value;
                }
                else
                {
                    bool globalCommentable = BusinessLogic_v3.Modules.Factories.SettingFactory.GetSettingValue<bool>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Article_CommentingEnabled);
                    return globalCommentable;
                }
                /*
                if (this.IsArticleType(Enums.ARTICLE_TYPE.BlogPosts))
                {
                    commentable = ModuleSettings.Articles.EnableBlogCommentingSystem;
                }
                else if (this.IsArticleType(Enums.ARTICLE_TYPE.ContentPages))
                {
                    commentable = ModuleSettings.Articles.EnableContentPagesCommentingSystem;
                }
                else if (this.IsArticleType(Enums.ARTICLE_TYPE.NewsItems))
                {
                    commentable = ModuleSettings.Articles.EnableNewsCommentingSystem;
                }*/

            }
            return commentable;
            /*if (commentable)
            {
                return true;
            }
            
            if (this.Data.ArticleType == Enums.ARTICLE_TYPE.BlogPosts)
            {
                bool notCommentable = this.Data.IsCommentable.HasValue && !this.Data.IsCommentable.Value;
                return !notCommentable;
            }
            return false;*/
        }

        bool IHierarchyNavigation.OmitChildrenInNavigation
        {
            get { return ((IHierarchyNavigation)this.Data).OmitChildrenInNavigation; }
        }

        bool IHierarchyNavigation.ConsiderAsRootNode
        {
            get { return ((IHierarchyNavigation)this.Data).ConsiderAsRootNode; }
        }

        long IListingItemData.ID
        {
            get { return ((IBaseObject)this.Data).ID; }
        }


        public bool HasDiscount
        {
            get { throw new NotImplementedException(); }
        }

        public double PriceActual
        {
            get { throw new NotImplementedException(); }
        }

        public double PriceBeforeDiscount
        {
            get { throw new NotImplementedException(); }
        }
        string IListingItemData.ReferenceCode
        {
            get
            {
                return null;
            }
        }

        public TokenReplacerNew CreateTokenReplacer()
        {
            return new TokenReplacerNew(this.Data);
            
        }
        Classes.DbObjects.Objects.IBaseDbObject IListingItemData.DbObject
        {
            get { return this.Data; }
        }

        public DateTime CreatedOnDate
        {
            get { return this.Data.CreatedOnDate; }
            set { this.Data.CreatedOnDate = value; }
        }

        public Control GetFreeText()
        {
            return null;
        }
        public string GetAuthor()
        {
            return string.IsNullOrWhiteSpace(Data.AuthorName) ? null : Data.AuthorName;
        }

        string IContentPageProperties.PageTitle
        {
            get { return Data.PageTitle; }
        }

        public string HtmlText
        {
            get { return Data._HtmlText_ForEditing; }
        }

        bool? IContentPageProperties.DoNotShowFooter
        {
            get { return Data.DoNotShowFooter; }
        }


        bool? IContentPageProperties.DoNotShowAddThis
        {
            get { return Data.DoNotShowAddThis; }
        }

        bool? IContentPageProperties.DoNotShowLastEditedOn
        {
            get { return Data.DoNotShowLastEditedOn; }
        }

        bool? IContentPageProperties.DoNotShowNavigationBreadcrumbs
        {
            get { return Data.DoNotShowNavigationBreadcrumbs; }
        }

        string IContentPageProperties._HtmlText_ForEditing
        {
            get { return Data._HtmlText_ForEditing; }
        }

        string IContentPageProperties.Title
        {
            get { return Data.Title; }
        }

        string IContentPageProperties.SubTitle
        {
            get { return Data.SubTitle; }
        }

        public string GetItemContent()
        {
            return Data.GetItemContent();
        }


        Enums.ARTICLE_TYPE IContentPageProperties.ArticleType
        {
            get { return Data.ArticleType; }
        }


        int? IContentPageProperties.DialogHeight
        {
            get { return Data.DialogHeight; }
        }

        int? IContentPageProperties.DialogWidth
        {
            get { return Data.DialogWidth; }
        }

        bool IContentPageProperties.DialogPage
        {
            get { return Data.DialogPage; }
        }


        string IContentPageProperties.MetaDescription
        {
            get { return Data.MetaDescription; }
        }


        IEnumerable<IMediaItemData> IContentPageProperties.GetMediaItems(bool excludeMainImage = false)
        {
            return Data.GetMediaItems(excludeMainImage).ToFrontendBaseList();
        }

        public Enums.CONTENT_PAGE_TYPE ContentPageType
        {
            get { return Enums.CONTENT_PAGE_TYPE.Article; }
        }
    }
}
