﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic_v3.Frontend.ArticleModule
{
    public interface IContentPageDataProperties
    {
        string PageTitle { get; }
        string _HtmlText_ForEditing { get; }
    }
}
