﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Frontend._Base;
using BusinessLogic_v3.Classes.Interfaces;

namespace BusinessLogic_v3.Frontend.ArticleModule
{
    using Modules.ArticleMediaItemModule;
    using Modules.ArticleModule;
    using CS.General_v3.Classes.Interfaces.Gallery;

    public interface IContentPageProperties : IBaseFrontend
    {
        string PageTitle { get; }
        bool? DoNotShowFooter { get; }
        bool? DoNotShowAddThis { get; }
        bool? DoNotShowLastEditedOn { get; }
        bool? DoNotShowNavigationBreadcrumbs { get; }
        string _HtmlText_ForEditing { get; }
        string Title { get; }
        string SubTitle { get; }

        string GetUrl();
        string GetMainImageUrl(string nullImageURL = null);
        string GetContent(params string[] contentTagsAndValues);

        Enums.ARTICLE_TYPE ArticleType { get; }

        int? DialogHeight { get; }

        int? DialogWidth { get; }

        bool DialogPage { get; }

        string GetTitleForSEO();

        string GetMetaDescriptionForSEO(bool ifEmptyMetaDescUseHtmlText);

        string GetMetaKeywordsForSEO();

        string MetaDescription { get; }

        IEnumerable<IMediaItemData> GetMediaItems(bool excludeMainImage);

        Enums.CONTENT_PAGE_TYPE ContentPageType { get; }
    }
}
