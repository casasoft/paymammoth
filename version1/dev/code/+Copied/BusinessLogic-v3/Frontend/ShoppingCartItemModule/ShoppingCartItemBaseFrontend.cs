using BusinessLogic_v3.Extensions;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Util;
using BusinessLogic_v3.Modules.ProductModule;


namespace BusinessLogic_v3.Frontend.ShoppingCartItemModule
{
    public abstract class ShoppingCartItemBaseFrontend : BusinessLogic_v3.Modules._AutoGen.ShoppingCartItemBaseFrontend_AutoGen
    {

        protected ShoppingCartItemBaseFrontend()
            : base()
        {

        }

        public void DeleteItem(bool autoSave = true)
        {
            using(var t = autoSave ? nHibernateUtil.BeginTransactionFromSessionInCurrentContext() : null)
            {
                this.Data.Delete();
                t.CommitIfNotNull();
            }
        }
        public virtual double GetPrice()
        {
            return Data.PriceInfo.GetTotalPrice();
        }

    }
}
