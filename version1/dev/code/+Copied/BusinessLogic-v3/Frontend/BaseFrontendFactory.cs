﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using System.ComponentModel;

namespace BusinessLogic_v3.Frontend
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TDataItemInterface">The data interface that this factory generates</typeparam>
    public abstract class BaseFrontendFactory<TFrontendItem, TDataItemInterface>
        where TFrontendItem: BaseFrontendItem<TDataItemInterface>, new()
        where TDataItemInterface : IBaseItemData
    {

        


        protected virtual TFrontendItem CreateNewItem(TDataItemInterface dataItem = default(TDataItemInterface))
        {
            var item = new TFrontendItem();
            
            item.SetDataItem(dataItem == null ? factory.CreateItemInstance() : dataItem);
            return item;
        }
        
        protected IBaseItemDataFactory<TDataItemInterface> factory { get; private set; }
        public BaseFrontendFactory(IBaseItemDataFactory<TDataItemInterface> factory)
        {
            this.factory = factory;
            
        }

        public virtual IEnumerable<TFrontendItem> GetAllItemsInRepository()
        {
            return GetListFromDataItems(this.factory.GetAllItemsInRepository());
        }

        public virtual TFrontendItem GetByPrimaryKey(long id)
        {
            var dataItem = factory.GetByPrimaryKey(id);
            if (dataItem == null)
            {
                return null;
            }
            else
            {
                return CreateNewItem(dataItem);
            }
        }
        public virtual IEnumerable<TFrontendItem> GetByPrimaryKeys(IEnumerable<long> ids)
        {
            return GetListFromDataItems(factory.GetByPrimaryKeys(ids));
            
        }


        public List<TFrontendItem> GetListFromDataItems(IEnumerable<TDataItemInterface> dataItems)
        {
            List<TFrontendItem> list = new List<TFrontendItem>();
            foreach (var data in dataItems)
            {
                list.Add(CreateNewItem(data));
            }

            return list;
        }

        
    }
}
