using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Modules.ContentTextModule;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Modules.OrderModule;
using BusinessLogic_v3.Modules.MemberModule;
using BusinessLogic_v3.Classes.Text;
using BusinessLogic_v3.Util;
using BusinessLogic_v3.Classes.Interfaces;
using System.Web.UI;


namespace BusinessLogic_v3.Frontend.MemberModule
{
    public abstract class MemberBaseFrontend : BusinessLogic_v3.Modules._AutoGen.MemberBaseFrontend_AutoGen, IListingItemData
    {

        protected MemberBaseFrontend()
            : base()
        {

        }

        public void Save(bool autoSave = true)
        {
            using(var t = nHibernateUtil.BeginTransactionBasedOnAutoSaveBool(autoSave))
            {
                this.Data.Save();
                t.Commit();
            }
        }

        public virtual string GetUrl()
        {
            //override in specific
            throw new InvalidOperationException("This must be overridden in the specific project!");
        }

        public BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject DbObject
        {
            get { return this.Data; }
        }

        public long ID
        {
            get { return this.Data.ID; }
        }

        public string GetTitle()
        {
            return this.Data.GetFullName();
        }

        public string GetShortDescription(bool convertFullDescriptionToPlainTextIfShortDescriptionIsEmpty, bool requiredAsHtml)
        {
            string desc = null;
            if (!string.IsNullOrEmpty(this.Data.CompanyProfile))
            {
                desc = this.Data.CompanyProfile;
            }
            if(!string.IsNullOrEmpty(desc))
            {
                int limitAmt = Factories.SettingFactory.GetSettingValue<int>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.ModuleSettings_Article_DescriptionLimitValue);
                if (convertFullDescriptionToPlainTextIfShortDescriptionIsEmpty)
                {
                    desc = CS.General_v3.Util.Text.ConvertHTMLToPlainText(desc);
                }
                desc = CS.General_v3.Util.Text.LimitText(desc, limitAmt);
            }
            return desc;
        }

        public virtual string ImageUrl
        {
            get
            {
                string imageUrl = this.Data.Image.GetSpecificSizeUrl(MemberBase.ImageSizingEnum.Thumbnail);
                return !string.IsNullOrEmpty(imageUrl) ? imageUrl : Modules.Factories.SettingFactory.GetSettingValue<string>(BusinessLogic_v3.Enums.BusinessLogicSettingsEnum.Listing_DefaultImageUrl);
            }
        }

        public string Url
        {
            get { return this.GetUrl(); }
        }

        public bool HasDiscount
        {
            get { throw new NotImplementedException(); }
        }

        public double PriceActual
        {
            get { throw new NotImplementedException(); }
        }

        public double PriceBeforeDiscount
        {
            get { throw new NotImplementedException(); }
        }

        public string ReferenceCode
        {
            get { throw new NotImplementedException(); }
        }

        public DateTime CreatedOnDate
        {
            get { return this.Data.DateRegistered; }
        }

        public virtual Control GetFreeText()
        {
            //override this in specific
            throw new InvalidOperationException("This must be overriden in the specific project!");
        }
    }
}
