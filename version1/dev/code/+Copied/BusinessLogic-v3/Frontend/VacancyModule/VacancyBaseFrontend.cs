using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using CS.General_v3.Extensions;
using CS.General_v3.Util;


namespace BusinessLogic_v3.Frontend.VacancyModule
{
    public abstract class VacancyBaseFrontend : BusinessLogic_v3.Modules._AutoGen.VacancyBaseFrontend_AutoGen
    {

        protected VacancyBaseFrontend()
            : base()
        {

        }

        public string GetUrl()
        {
            return BusinessLogic_v3.Classes.Routing.VacancyRoute.GetURL(this.Data.ID, this.Data.Title);
        }

    }
}
