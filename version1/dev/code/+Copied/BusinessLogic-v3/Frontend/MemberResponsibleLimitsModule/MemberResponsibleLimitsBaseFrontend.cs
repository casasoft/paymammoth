using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Extensions;
using System.Web.UI;


namespace BusinessLogic_v3.Frontend.MemberResponsibleLimitsModule
{
    public abstract class MemberResponsibleLimitsBaseFrontend : BusinessLogic_v3.Modules._AutoGen.MemberResponsibleLimitsBaseFrontend_AutoGen
    {

        protected MemberResponsibleLimitsBaseFrontend()
            : base()
        {

        }

        public string GetFrequencyAsString()
        {
            string result = null;
            switch (Data.FrequencyType)
            {
                case BusinessLogic_v3.Enums.MEMBER_RESPONSIBLE_LIMITS_FREQUENCY.Day:
                    result = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ResponsibleGaming_YourLimits_Freq_Day).GetContent(parseLinks:true);
                    break;
                case BusinessLogic_v3.Enums.MEMBER_RESPONSIBLE_LIMITS_FREQUENCY.Month:
                    result = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ResponsibleGaming_YourLimits_Freq_Month).GetContent(parseLinks: true);
                    break;
                case BusinessLogic_v3.Enums.MEMBER_RESPONSIBLE_LIMITS_FREQUENCY.Week:
                    result = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ResponsibleGaming_YourLimits_Freq_Week).GetContent(parseLinks: true);
                    break;
                case BusinessLogic_v3.Enums.MEMBER_RESPONSIBLE_LIMITS_FREQUENCY.Year:
                    result = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(BusinessLogic_v3.Enums.CONTENT_TEXT.MembersArea_ResponsibleGaming_YourLimits_Freq_Year).GetContent(parseLinks: true);
                    break;
                default:
                    throw new InvalidOperationException("Value <" + Data.FrequencyType + "> not mapped");
            }
            return result;
        }

        public string GetCurrentLimitEndDateAndNewLimitDetails()
        {
            string noExpiryText = Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                                 Enums.CONTENT_TEXT.MembersArea_ResponsibleGaming_YourLimits_NoExpiry).GetContent();

            string newLimitSoonText =
                Factories.ContentTextFactory.GetContentTextByGenericIdentifier(
                    Enums.CONTENT_TEXT.MembersArea_ResponsibleGaming_YourLimits_NewLimitSoon).GetContent();

            string result = null;
            DateTime? endDate = Data.GetValidityEndDate();

            string validUntilTag = "VALID_UNTIL_DATE";
            string newLimitValueTag = "NEW_LIMIT_VALUE";
            string newLimitFrequencyTag = "NEW_LIMIT_FREQUENCY";
            string newLimitDateTag = "NEW_LIMIT_DATE";

            MemberResponsibleLimitsBaseFrontend newLimit = Data.GetNextResponsibleLimit().ToFrontendBase();

            if (endDate.HasValue && newLimit!=null)
            {
                newLimitSoonText = Text.ReplaceTag(newLimitSoonText, validUntilTag,
                                Classes.Culture.DefaultCultureManager.Instance.FormatDateTimeWithCurrentCulture(
                                    endDate.Value, Date.DATETIME_FORMAT.ShortDate));
                newLimitSoonText = Text.ReplaceTag(newLimitSoonText, newLimitValueTag,
                                                   Classes.Culture.DefaultCultureManager.Instance.
                                                       FormatNumberWithCurrentCulture(newLimit.Data.Value,
                                                                                      NumberUtil.NUMBER_FORMAT_TYPE.
                                                                                          Currency));

                newLimitSoonText = Text.ReplaceTag(newLimitSoonText, newLimitFrequencyTag, newLimit.GetFrequencyAsString());
                newLimitSoonText = Text.ReplaceTag(newLimitSoonText, newLimitDateTag,
                                                   Classes.Culture.DefaultCultureManager.Instance.
                                                       FormatDateTimeWithCurrentCulture(
                                                           newLimit.Data.StartDate, Date.DATETIME_FORMAT.ShortDate));
                result = newLimitSoonText;
            }
            else
            {
                result = noExpiryText;
            }
            return result;
        }

    }
}
