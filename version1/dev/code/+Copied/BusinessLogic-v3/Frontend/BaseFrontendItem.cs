﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;

namespace BusinessLogic_v3.Frontend
{
    public abstract class BaseFrontendItem<TItemData>
        where TItemData : IBaseItemData
    {
        public TItemData Data { get; private set; }
        public BaseFrontendItem()
        {

        }
        public BaseFrontendItem(TItemData data)
        {
            this.Data = data;
        }
        public void SetDataItem(TItemData data)
        {
            this.Data = data;
        }
        public long ID
        {
            get
            {
                return this.Data.PrimaryKey;
            }
        }
    }
}
