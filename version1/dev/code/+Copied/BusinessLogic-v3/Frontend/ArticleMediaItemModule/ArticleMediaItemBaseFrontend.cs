using BusinessLogic_v3.Modules.ArticleMediaItemModule;
using BusinessLogic_v3.Modules.ArticleModule;
using CS.General_v3.Classes.Interfaces.Gallery;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using CS.General_v3.Extensions;
using CS.General_v3.Util;


namespace BusinessLogic_v3.Frontend.ArticleMediaItemModule
{
    using CS.General_v3.Classes.Interfaces.ImageItem;

    public abstract class ArticleMediaItemBaseFrontend : BusinessLogic_v3.Modules._AutoGen.ArticleMediaItemBaseFrontend_AutoGen, IMediaItemData
    {

        protected ArticleMediaItemBaseFrontend()
            : base()
        {
            
        }


        public string ThumbnailImageUrl
        {
            get { return this.Data.Image.GetSpecificSizeUrl(ArticleMediaItemBase.ImageSizingEnum.Thumbnail); }
        }

        public string LargeImageUrl
        {
            get { return this.Data.Image.GetSpecificSizeUrl(ArticleMediaItemBase.ImageSizingEnum.Large); }
        }

        public string Caption
        {
            get
            {
                if (!string.IsNullOrEmpty(this.Data.Caption))
                {
                    return (this.Data).Caption;
                }
                else
                {
                    var article = this.Data.ContentPage;
                    if(article != null)
                    {
                        return article.Title;
                    }
                }
                return null;
            }
        }

        public string VideoLink
        {
            get { return this.Data.VideoLink; }
        }


        public string NormalImageUrl
        {
            get { return this.Data.Image.GetSpecificSizeUrl(ArticleMediaItemBase.ImageSizingEnum.Normal); }
        }
    }
}
