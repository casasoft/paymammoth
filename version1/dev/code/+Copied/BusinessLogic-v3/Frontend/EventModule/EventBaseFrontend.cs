using BusinessLogic_v3.Classes.Interfaces;
using BusinessLogic_v3.Classes.MediaItems;
using BusinessLogic_v3.Classes.MediaItems.Impl;
using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.Routing;
using BusinessLogic_v3.Constants;


namespace BusinessLogic_v3.Frontend.EventModule
{
    public abstract class EventBaseFrontend : BusinessLogic_v3.Modules._AutoGen.EventBaseFrontend_AutoGen
    {

        protected EventBaseFrontend()
            : base()
        {

        }

        public string GetUrl()
        {
            var paramValues = new CS.General_v3.Classes.Routing.MyRouteValueDictionary(Tokens.ROUTE_PARAM_TITLE, this.Data.Title, Tokens.ROUTE_PARAM_ID, this.Data.ID.ToString());
            return Modules.Factories.RoutingInfoFactory.GetUrlForGeneralRoute(Enums.RouteUrlsEnum.Event, paramValues);
        }


    }
}
