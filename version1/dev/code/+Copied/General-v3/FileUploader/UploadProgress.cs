#region License
/*

File Upload HTTP module for ASP.Net
Copyright (C) 2007 Darren Johnstone (http://darrenjohnstone.com)

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#endregion

using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.General_20090518.FileUploader
{
    /// <summary>
    /// Implements a progress bar to monitor uploads.
    /// <remarks>Only one upload progress bar is supported per page.</remarks>
    /// </summary>
    public class UploadProgress : WebControl
    {
        /// <summary>
        /// Called when the control is loaded.
        /// </summary>
        /// <param name="e">Event args.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "upAction", UploadManager.JAVASCRIPT_UPLOADER_NAME + ".UpdateFormAction(); ");
        }

        /// <summary>
        /// Renders the progress bar.
        /// </summary>
        /// <param name="writer">HTML text writer to write to.</param>
        protected override void Render(HtmlTextWriter writer)
        {
            /*
            writer.Write("<div class='upContainer'>");
            writer.Write("<div class='upOuterBar'>");
            writer.Write("<div id='upProgressBar' class='upInnerBar'>");
            writer.Write("</div>");
            writer.Write("<div id='upLabel' class='upLabel'>");
            writer.Write("Waiting for uploads");
            writer.Write("</div>");
            writer.Write("</div>");
            writer.Write("</div>");*/
            writer.Write("<input type='hidden' name='" + UploadManager.UPLOADID_FORMELEMENT_NAME + "' id='" + UploadManager.UPLOADID_FORMELEMENT_NAME + "' value='" + Guid.NewGuid().ToString() + "'/>");
        }
    }
}
