#region License
/*

File Upload HTTP module for ASP.Net (v 2.0)
Copyright (C) 2007-2008 Darren Johnstone (http://darrenjohnstone.net)

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#endregion

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization;

namespace CS.General_v3.FileUploader
{
    /// <summary>
    /// Implements the IFileProcessor interface to stream uploaded files to
    /// a directory in the file system.
    /// </summary>
    
    public partial class FileSystemProcessor 
    {
        private static bool _INDUCE_LAG = false;

        public static bool INDUCE_LAG
        {
            get { return FileSystemProcessor._INDUCE_LAG; }
            set { FileSystemProcessor._INDUCE_LAG = value; }
        }
        
        public static int INDUCE_LAG_TIME = 50;//milleseconds

        private string _newFileName;
        private string _fieldName;
        public string GetNewFileName()
        {
            return _newFileName;
        }

        public string GetFieldname()
        {
            return _fieldName;
        }
        


    }
}
