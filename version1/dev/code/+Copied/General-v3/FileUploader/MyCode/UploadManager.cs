#region License
/*

File Upload HTTP module for ASP.Net (v 2.0)
Copyright (C) 2007-2008 Darren Johnstone (http://darrenjohnstone.net)

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#endregion

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Web;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;

namespace CS.General_v3.FileUploader
{
    


    /// <summary>
    /// Manages uploads and acts as a factory class for file processors.
    /// </summary>
    public  sealed partial class UploadManager
    {
        
        #region Constants
        public const int TEMPORARY_FILE_DELETE_INTERVAL = 12 * 60; //in minutes;
        /// <summary>
        /// Can be either a local path, or a relative path that is then resolved
        /// </summary>
        public static string UPLOADS_FOLDER = "uploads/tmp/";
        public const string GLOBAL_INDEX_APP_KEY = "uploadprogress_globalindex";
        public const string UPLOADID_QUERYSTRING_PARAMETER = "uploadID";
        public const string UPLOADID_FORMELEMENT_NAME = "uploadprogressbar_uploadid";
        public const string JAVASCRIPT_UPLOADER_NAME = "com.cs.forms.FileUploader.instance";
        private bool _Initialised = false;
        public bool IsInitialised()
        {
            return Initialised && UploadModule.LoadedModule;
        }
        public bool Initialised
        {
            get { return _Initialised; }
            set { _Initialised = value; }
        }
        
        public const string STATUS_KEY = UPLOADID_QUERYSTRING_PARAMETER;
        public static string GetStatusKeyFromQueryString()
        {
            return CS.General_v3.Util.PageUtil.GetQueryStringVariable(UPLOADID_QUERYSTRING_PARAMETER);
        }
        public static int GlobalIndex
        {
            get
            {
                if (HttpContext.Current.Application[GLOBAL_INDEX_APP_KEY] == null)
                    GlobalIndex = 1;


                return (int)HttpContext.Current.Application[GLOBAL_INDEX_APP_KEY];
            }
            set
            {
                HttpContext.Current.Application[GLOBAL_INDEX_APP_KEY] = value;
            }
        }
        /// <summary>
        /// Gets the current upload status.
        /// </summary>
        public UploadStatus Status
        {
            get
            {
                string key;

                key = CS.General_v3.Util.PageUtil.GetQueryStringVariable(STATUS_KEY);
                if (key == null)
                {
                    key = (string)HttpContext.Current.Items[STATUS_KEY];
                    if (key == null)
                        return null;
                }
                else
                {
                    key = key.Trim();
                }

                return HttpContext.Current.Application[STATUS_KEY + key] as UploadStatus;
            }
            internal set
            {
                string key;

                key = CS.General_v3.Util.PageUtil.GetQueryStringVariable(STATUS_KEY);
                if (key == null)
                    throw new Exception("Query status key not found");
                else
                    SetStatus(value, key);
            }
        }
        #endregion
        public static string GetUploadsFolder()
        {
            string localPath = "";
            if (UploadManager.UPLOADS_FOLDER.Contains("/"))
            {
                string path = "~/" + UploadManager.UPLOADS_FOLDER;
                if (path[path.Length - 1] != '\\')
                    path += "\\";
                localPath = System.Web.HttpContext.Current.Server.MapPath(path);
            }
            else
            {
                localPath = UploadManager.UPLOADS_FOLDER;
            }

            return localPath;
        }
        public static void CheckTemporaryFolder()
        {
            string path = GetUploadsFolder();
            if (System.IO.Directory.Exists(path))
            {
                System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(path);
                System.IO.FileInfo[] files = dir.GetFiles();
                for (int i = 0; i < files.Length; i++)
                {
                    System.IO.FileInfo f = files[i];
                    string s = f.Name;
                    try
                    {
                        if (s.Length >= 12)
                        {
                            int iYear = Convert.ToInt32(s.Substring(0, 4));
                            int iMonth = Convert.ToInt32(s.Substring(4, 2));
                            int iDay = Convert.ToInt32(s.Substring(6, 2));
                            int iHour = Convert.ToInt32(s.Substring(9, 2));
                            int iMin = Convert.ToInt32(s.Substring(11, 2));
                            int iSec = Convert.ToInt32(s.Substring(13, 2));
                            DateTime d = new DateTime(iYear, iMonth, iDay, iHour, iMin, iSec);
                            if (d.AddMinutes(TEMPORARY_FILE_DELETE_INTERVAL) < CS.General_v3.Util.Date.Now)
                            {
                                try
                                {
                                    f.Delete();
                                }
                                catch
                                {
                                }
                            }
                        }
                    }
                    catch
                    { }

                }
            }
        }
        public static void InitPage(System.Web.UI.Page pg)
        {
            /*
            System.Web.UI.WebControls.Literal literal = new System.Web.UI.WebControls.Literal();
            literal.Text = "<input type='hidden' name='" + UploadManager.UPLOADID_FORMELEMENT_NAME +
                "' id='" + UploadManager.UPLOADID_FORMELEMENT_NAME + "' value='" + Guid.NewGuid().ToString() + "'/>";
            //pg.ClientScript.RegisterOnSubmitStatement(typeof(UploadManager), "upAction", UploadManager.JAVASCRIPT_UPLOADER_NAME + ".UpdateFormAction(); ");
            CS.General_v3.Controls.WebControls.Common.MyHiddenField field = new CS.General_v3.Controls.WebControls.Common.MyHiddenField();
            field.ID = UploadManager.UPLOADID_FORMELEMENT_NAME;
            field.Value = Guid.NewGuid().ToString();
            //writer.Write("<input type='hidden' name='" + UploadManager.UPLOADID_FORMELEMENT_NAME + "' id='" + UploadManager.UPLOADID_FORMELEMENT_NAME + "' value='" + Guid.NewGuid().ToString() + "'/>");
            */
            pg.ClientScript.RegisterHiddenField(UploadManager.UPLOADID_FORMELEMENT_NAME, Guid.NewGuid().ToString());
//            pg.Form.Controls.Add(literal);
           
        }
        
        /// <summary>
        /// Initialises the upload module instance.
        /// </summary>
        public static void InitInstance()
        {
            InitInstance(null);
        }
        /// <summary>
        /// Initialises the upload module instance.
        /// </summary>
        /// <param name="downloadPath">Download Path</param>
        public static void InitInstance(string downloadPath)
        {
            if (!UploadManager.Instance.Initialised)
            {
                if (!string.IsNullOrEmpty(downloadPath))
                    UploadManager.UPLOADS_FOLDER = downloadPath;
                UploadManager.Instance.Initialised = true;
                UploadManager.Instance.ProcessorType = typeof(FileSystemProcessor);
                UploadManager.Instance.ProcessorInit += new FileProcessorInitEventHandler(Processor_Init);
                
            }
        }
        /// <summary>
        /// Initialises the file processor.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="args">Arguments</param>
        static void Processor_Init(object sender, FileProcessorInitEventArgs args)
        {
            FileSystemProcessor processor;

            processor = args.Processor as FileSystemProcessor;
            if (processor != null)
            {
                //processor.OutputPath = CS.General_v3.Util.PageUtil.MapPath(UPLOADS_FOLDER);
                // Set up the download path here - default to the root of the web application
                //processor.OutputPath = @"c:\uploads";
            }
        }
        



    }
}
