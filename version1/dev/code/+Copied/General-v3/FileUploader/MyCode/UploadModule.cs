#region License
/*

File Upload HTTP module for ASP.Net (v 2.0)
Copyright (C) 2007-2008 Darren Johnstone (http://darrenjohnstone.net)

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#endregion

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Web;
using System.Globalization;
using System.Reflection;
using System.Security.Permissions;
using System.Text.RegularExpressions;

namespace CS.General_v3.FileUploader
{
    /// <summary>
    /// Http handler which can process very large file uploads
    /// by passing the request stream to a FormStream instance
    /// for persistance by an IFileProcessor implementation.
    /// </summary>
    public partial class UploadModule : IHttpModule
    {
        
        private static List<string> _URLsToExclude = null;
        /// <summary>
        /// A list of URLs to exclude.  Can be set to regular expressions
        /// </summary>
        public static List<string> URLsToExclude
        {
            get
            {
                if (CS.General_v3.Util.PageUtil.GetApplicationInfoObject("_UploadModule_URLsToExclude") == null)
                {
                    initURLsToExclude();
                }
                return (List<string>)CS.General_v3.Util.PageUtil.GetApplicationInfoObject("_UploadModule_URLsToExclude");
            }
        }
        private static void initURLsToExclude()
        {
            List<string> list = new List<string>();
            list.Add("(.*)/fckeditor/.*");
            list.Add("(.*)/fck/.*");
            list.Add("(.*)/tinymce/.*");
            list.Add("(.*)/tiny_mce/.*");
            list.Add(@".*/filebrowser_upload\.ashx.*");
            list.Add(@".*/includes/ckeditor_browser/.*");
            list.Add(@".*/_common/aspx/ckeditor_browser/.*");

            CS.General_v3.Util.PageUtil.SetApplicationInfo("_UploadModule_URLsToExclude", list);
        }
        public static bool IsAllowedURL
        {
            get
            {
                string s = CS.General_v3.Util.PageUtil.GetCurrentFullURL(false);
                for (int i = 0; i < URLsToExclude.Count; i++)
                {
                    string exc = URLsToExclude[i];
                    if (Regex.IsMatch(s, exc, RegexOptions.IgnoreCase))
                    {
                        return false;
                    }
                }
                return true;

            }
        }
       
        
    }
}
