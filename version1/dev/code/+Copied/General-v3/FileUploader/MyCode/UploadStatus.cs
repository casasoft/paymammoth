#region License
/*

File Upload HTTP module for ASP.Net (v 2.0)
Copyright (C) 2007-2008 Darren Johnstone (http://darrenjohnstone.net)

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

*/
#endregion

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace CS.General_v3.FileUploader
{
    /// <summary>
    /// Contains the status information for an upload request and
    /// allows serialization as as XML message for passing to the
    /// client.
    /// </summary>
    public partial class UploadStatus
    {
        
        public System.IO.Stream GetUploadedFileForControl(System.Web.UI.WebControls.FileUpload  txt)
        {
            return GetUploadedFileForControl(txt, System.IO.FileMode.Open);
        }
        public System.IO.Stream GetUploadedFileForControl(System.Web.UI.WebControls.FileUpload txt, System.IO.FileMode fileMode)
        {
            for (int i = 0; i < _uploadedFiles.Count; i++)
            {
                string clientName = CS.General_v3.Util.Forms.GetFormVariableID(txt);
                if (_uploadedFiles[i].FieldName == clientName)
                {
                    string path = UploadManager.GetUploadsFolder() + _uploadedFiles[i].NewFilename;
                    System.IO.FileStream fs = new System.IO.FileStream(path, fileMode,   System.IO.FileAccess.Read, System.IO.FileShare.None);
                    
                    byte[] by = new byte[fs.Length];
                    fs.Read(by, 0, by.Length);
                    
                    System.IO.MemoryStream ms = new System.IO.MemoryStream(by,false);
                    fs.Close();
                    fs.Dispose();
                    CS.General_v3.Util.IO.DeleteFile(path);
                    return ms;
                }
            }
            return null;
        }

       
        
    }
}
