﻿To Use:
---------

+ in the 'global.asax' file, add these:

	UploadManager.Instance.ProcessorType = typeof(FileSystemProcessor);
    UploadManager.Instance.ProcessorInit += new FileProcessorInitEventHandler(Processor_Init); 
    
+ make sure to also add the import:

	<%@ Import Namespace="CS.General_20090518.FileUploader" %>
    
+ Add an HTTP Handler in 'web.config':

	<add verb="GET" type="CS.General_20090518.FileUploader.UploadProgressHandler, General" path="uploadProgress.ashx"/>
      
+ Add an HTTP Module in 'web.config':

	<add name="uploadProgress" type="CS.General_20090518.FileUploader.UploadModule, General"/>
      
+ If you want to change the temporary folder for the uploaded files, change the variable in the 'global.asax', <Processor.OutputPath>.

+ Make sure to include 'includes/com/cs/forms/FileUploader.js'


