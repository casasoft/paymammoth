﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_20090518.FileUploader
{
    public class FileUploaderJS
    {
        public class ELEMENTS
        {
            public string Main {get;set;}
            public string ProgressBar {get;set;}
            public string Percentage {get;set;}
            public string Status {get;set;}
            public string Filename {get;set;}
            public string BytesTransferred {get;set;}
            
        }
        private ELEMENTS _Elements = new ELEMENTS();
        public ELEMENTS Elements { get { return _Elements; } }
        public bool CenteredInWindow { get; set; }
        public string VariableName { get; set; }
        public int CheckProgressInterval { get; set; }
        public int ProgressBarWidth { get; set; }
        public string JSFunctionToCallOnStart { get; set; }
        public FileUploaderJS()
        {
            this.VariableName = "com.cs.forms.FileUploader.instance";
            this.CheckProgressInterval = 1000;
            this.ProgressBarWidth = 380;
            this.JSFunctionToCallOnStart = "null";
            this.CenteredInWindow = true;
        }
        public void GenerateJSCode(System.Web.UI.Page pg)
        {
            string JS = "";
            string tmp = "";
            if (string.IsNullOrEmpty(Elements.Main) || string.IsNullOrEmpty(Elements.ProgressBar))
            {
                throw new InvalidOperationException("You need to set Elements.Main and Elements.ProgressBar for the fileuploader to work");
            }
            JS = VariableName + ".SetUIControls('" + this.Elements.Main + "','" + this.Elements.ProgressBar + "','" + this.Elements.Status + "'" + 
                ",'" +this.Elements.Percentage + "','" + this.Elements.BytesTransferred + "','" + this.Elements.Filename + "'," + JSFunctionToCallOnStart + ");";
            JS += VariableName + ".CheckProgressInterval = " + this.CheckProgressInterval + ";";
            JS += VariableName + ".progressBarFullWidth = " + this.ProgressBarWidth + ";";
            tmp = "false";
            if (CenteredInWindow)
                tmp = "true";
            JS += VariableName + ".centeredInWindow = " + tmp + ";";
            


            pg.ClientScript.RegisterStartupScript(this.GetType(), this.GetType().ToString(), CS.General_20090518.Text.makeJavaScript(JS));


        }

    }
}
