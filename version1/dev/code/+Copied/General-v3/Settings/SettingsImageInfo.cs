using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Configuration;
using System.Web;
using System.IO;

namespace CS.General_v3
{
    public static partial class Settings
    {

        public static class ImageInfo
        {
            public static int GetImageWidthFromSettings(string baseSetting)
            {
                int width, height;

                GetImageInfoFromSettings(baseSetting, "1600x1200-None", out width, out height);
                return width;
            }
            public static int GetImageHeightFromSettings(string baseSetting)
            {
                int width, height;

                GetImageInfoFromSettings(baseSetting,"1600x1200-None", out width, out height);
                return height;
            }
            
            public static bool GetImageInfoFromSettings(string baseSetting, string defaultValue,  out int width, out int height )
            {
                CS.General_v3.Enums.CROP_IDENTIFIER cropIdentifier;
                return GetImageInfoFromSettings(baseSetting, defaultValue, out width, out height, out cropIdentifier);
            }

            public static void ParseImageSettingsIntoValues(string settingValue, out int width, out int height, out CS.General_v3.Enums.CROP_IDENTIFIER cropIdentifier)
            {
                string[] tmp = settingValue.Split('-');
                string size = tmp[0].Trim();
                string cropInfo = null;
                if (tmp.Length > 1)
                    cropInfo = tmp[1].Trim();

                size = size.Replace(" ", "");
                size = size.ToLower();
                size = size.Replace("*", "x");
                string[] tokens = size.Split(new string[] { "x" }, StringSplitOptions.RemoveEmptyEntries);
                width = Convert.ToInt32(tokens[0]);
                height = Convert.ToInt32(tokens[1]);
                cropIdentifier = CS.General_v3.Enums.CropIdentifierFromText(cropInfo);

            }

            public static bool GetImageInfoFromSettings(string baseSetting, string defaultValue,
                out int width, out int height, out CS.General_v3.Enums.CROP_IDENTIFIER cropIdentifier)
            {
                if (baseSetting.EndsWith("/"))
                    baseSetting = CS.General_v3.Util.Text.RemoveLastNumberOfCharactersFromString(baseSetting, 1);
                    
                width = 0;
                height = 0;
                
                cropIdentifier = Enums.CROP_IDENTIFIER.None;

                //new way
                bool ok = true;
                CS.General_v3.Classes.Attributes.SettingsInfoAttribute defaultValues = new Classes.Attributes.SettingsInfoAttribute();
                defaultValues.DefaultValue = defaultValue;
                string sizeCropInfo = CS.General_v3.Settings.GetSettingByStringIdentifier<string>(baseSetting, defaultValues);
                if (string.IsNullOrWhiteSpace(sizeCropInfo))
                    sizeCropInfo = CS.General_v3.Settings.GetSettingByStringIdentifier<string>(baseSetting + "/", defaultValues, createIfNotExists: false);
                
                string cropInfo = "";
                if (!string.IsNullOrEmpty(sizeCropInfo))
                {
                    ParseImageSettingsIntoValues(sizeCropInfo, out width, out height, out cropIdentifier);
                }
                else
                {
                    ok = false;
                }
                return ok;

            }
        }

    }
}
