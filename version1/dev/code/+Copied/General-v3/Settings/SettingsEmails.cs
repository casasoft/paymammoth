using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Configuration;
using System.Web;
using System.IO;

namespace CS.General_v3
{
    public static partial class Settings
    {

        public static class Emails
        {

            [Obsolete("Call the settings factory to load such setting")]
            public static string SMTPHost
            {
                get
                {
                    return Enums.SETTINGS_ENUM.Email_SMTP_Host.GetSettingFromDatabase();
                    
                }
            }


            [Obsolete("Call the settings factory to load such setting")]
            public static bool SendEmailsOnError
            {
                get { return !CS.General_v3.Util.Other.IsLocalTestingMachine; }
            }


            [Obsolete("Call the settings factory to load such setting")]
            public static string SMTPUser
            {
                get
                {
                    return Enums.SETTINGS_ENUM.Email_SMTP_User.GetSettingFromDatabase<string>();
                }
            }


            [Obsolete("Call the settings factory to load such setting")]
            public static string SMTPPass
            {
                get
                {
                    return Enums.SETTINGS_ENUM.Email_SMTP_Pass.GetSettingFromDatabase<string>();
                }
            }



            [Obsolete("Call the settings factory to load such setting")]
            public static bool SMTPIsSecure
            {
                get
                {
                    return Enums.SETTINGS_ENUM.Email_SMTP_UseSecureAuthentication.GetSettingFromDatabase<bool>();
                }
            }
            [Obsolete("Call the settings factory to load such setting")]
            public static int SMTPPort
            {
                get
                {
                    return Enums.SETTINGS_ENUM.Email_SMTP_Port.GetSettingFromDatabase<int>();
                    
                }
            }



            ///// <summary>
            ///// signature tag
            ///// </summary>
            //public static string EmailSignatureTag
            //{
            //    get
            //    {
            //        return GetSetting("Email/SignatureTag", "[SIGNATURE]");
            //    }
            //}
            //public static string EmailFolder
            //{
            //    get
            //    {
            //        string s = GetSetting("Email/Folder", "includes/emails");
            //        if (s[s.Length - 1] != '/')
            //            s += "/";
            //        return s;
            //    }
            //}
            [Obsolete("Call the settings factory to load such setting")]
            public static string ContactForm_FromEmail
            {
                get
                {
                    string s = Enums.SETTINGS_ENUM.Email_EmailAddresses_ContactForm_FromEmail.GetSettingFromDatabase();
                    if (string.IsNullOrWhiteSpace(s))
                        s = SentEmailsFromEmail;
                    return s;

                }
            }
            [Obsolete("Call the settings factory to load such setting")]
            public static string ContactForm_FromName
            {
                get
                {
                    string s = Enums.SETTINGS_ENUM.Email_EmailAddresses_ContactForm_FromName.GetSettingFromDatabase();
                    if (string.IsNullOrWhiteSpace(s))
                        s = SentEmailsFromName;
                    return s;

                }
            }

            [Obsolete("Call the settings factory to load such setting")]
            public static string ContactForm_ToEmail
            {
                get
                {
                    string s = Enums.SETTINGS_ENUM.Email_EmailAddresses_ContactForm_ToEmail.GetSettingFromDatabase();
                    if (string.IsNullOrWhiteSpace(s))
                    {
                        //if it is empty, make is same as the from email
                        s = ContactForm_FromEmail;
                    }
                    return s;

                }
            }
            [Obsolete("Call the settings factory to load such setting")]
            public static string ContactForm_ToName
            {
                get
                {
                    string s = Enums.SETTINGS_ENUM.Email_EmailAddresses_ContactForm_ToName.GetSettingFromDatabase();
                    if (string.IsNullOrWhiteSpace(s))
                    {
                        //if it is empty, make is same as the from email
                        s = ContactForm_FromName;
                    }
                    return s;

                }
            }

            [Obsolete("Call the settings factory to load such setting")]
            public static string SentEmailsReplyToEmail
            {
                get
                {
                    return Enums.SETTINGS_ENUM.Email_EmailAddresses_General_ReplyToEmail.GetSettingFromDatabase();
                }
            }
            [Obsolete("Call the settings factory to load such setting")]
            public static string SentEmailsReplyToName
            {
                get
                {
                    return Enums.SETTINGS_ENUM.Email_EmailAddresses_General_ReplyToName.GetSettingFromDatabase();
                }
            }

            [Obsolete("Call the settings factory to load such setting")]
            public static string SentEmailsFromName
            {
                get
                {
                    return Enums.SETTINGS_ENUM.Email_EmailAddresses_General_FromName.GetSettingFromDatabase<string>();
                    
                    

                }
            }

            [Obsolete("Call the settings factory to load such setting")]
            public static string SentEmailsFromEmail
            {
                get
                {
                    return Enums.SETTINGS_ENUM.Email_EmailAddresses_General_FromEmail.GetSettingFromDatabase();
                }
            }


            [Obsolete("Call the settings factory to load such setting")]
            public static string NotificationEmail
            {
                get
                {
                    return Enums.SETTINGS_ENUM.Email_EmailAddresses_Notification_Email.GetSettingFromDatabase();
                  

                }
            }


            


        }
    }
}
