using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Configuration;
using System.Web;
using System.IO;

namespace CS.General_v3
{
    public static partial class Settings
    {
        public static class CultureInfo
        {
            private static string rootNode = "CultureInfo/";
            

            [Obsolete]
            public static string CurrencySymbol
            {
                get
                {
                    return Settings._getSettingFromFileSystem<string>(rootNode  + "CurrencySymbol", "�");
                }
            }
            /*public static string DefaultCultureCode
            {
                get
                {
                    
                    return Settings.GetSetting(rootNode + "DefaultCultureCode", CS.General_v3.Enums.ISO_ENUMS.Language_ISO639_2letter_ToCode(Enums.ISO_ENUMS.LANGUAGE_ISO639.English));
                }
            }*/
            /*public static string DefaultCurrencyCode
            {
                get
                {
                    return Settings.GetSetting(rootNode + "DefaultCurrencyCode", CS.General_v3.Enums.ISO_ENUMS.Currency_ISO4217ToCode(  Enums.ISO_ENUMS.CURRENCY_ISO4217.EuroMemberCountriesEuro));
                }
            }*/
        }
    }
}
