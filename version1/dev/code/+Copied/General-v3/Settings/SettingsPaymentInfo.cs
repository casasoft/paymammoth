using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Configuration;
using System.Web;
using System.IO;

namespace CS.General_v3
{
    public static partial class Settings
    {

        public static class PaymentInfo
        {
        //    private static string rootNode = "PaymentInfo/";



            public static bool PaymentTesting
            {
                get
                {
                    var result = Enums.SETTINGS_ENUM.OnlinePayment_PaymentInTestingMode.GetSettingFromDatabase<bool>();
                    return result;
                }
            }

           public static bool PaymentUseSSL
            {
                get
                {
                    return Enums.SETTINGS_ENUM.OnlinePayment_PaymentPageUsesSSL.GetSettingFromDatabase<bool>();
                }
            }

        }

    }
}
