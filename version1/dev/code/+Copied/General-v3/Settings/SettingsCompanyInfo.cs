using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Configuration;
using System.Web;
using System.IO;

namespace CS.General_v3
{
    public static partial class Settings
    {
        public static class CompanyInfo
        {
            private static string rootNode = "Company/";

            public static string GetAllTelephonesAsStr(string delimeter = ", ")
            {
                return CS.General_v3.Util.Text.AppendStrings(delimeter,
                    Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Telephone.GetSettingFromDatabase(),
                    Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Mobile.GetSettingFromDatabase());
            }
            /*public static string Facebook { get { return CS.General_v3.Settings.GetSetting(Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Facebook); } }
            public static string FacebookGroup { get { return CS.General_v3.Settings.GetSetting(Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Facebook); } }
            public static string Twitter { get { return CS.General_v3.Settings.GetSetting(Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Facebook); } }
            public static string CompanyName { get { return CS.General_v3.Settings.GetSetting(Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Facebook); } }
            public static string Website { get { return CS.General_v3.Settings.GetSetting(Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Facebook); } }
            public static string Tel { get { return CS.General_v3.Settings.GetSetting(Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Facebook); } }
            public static string Fax { get { return CS.General_v3.Settings.GetSetting(Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Facebook); } }
            public static string Skype { get { return CS.General_v3.Settings.GetSetting(Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Facebook); } }
            public static string DirectorName { get { return CS.General_v3.Settings.GetSetting(Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Facebook); } }
            public static string Mobile { get { return CS.General_v3.Settings.GetSetting(Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Facebook); } }
            public static string Address1 { get { return CS.General_v3.Settings.GetSetting(Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Facebook); } }
            public static string Address2 { get { return CS.General_v3.Settings.GetSetting(Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Facebook); } }
            public static string Address3 { get { return CS.General_v3.Settings.GetSetting(Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Facebook); } }
            public static string PostCode { get { return CS.General_v3.Settings.GetSetting(Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Facebook); } }
            public static string Country { get { return CS.General_v3.Settings.GetSetting(Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Facebook); } }
            public static string Locality { get { return CS.General_v3.Settings.GetSetting(Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Facebook); } }
            public static string Email { get { return CS.General_v3.Settings.GetSetting(Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Facebook); } }
            public static string VatNumber { get { return CS.General_v3.Settings.GetSetting(rootNode + "VATNumber"); } }
            public static string PaymentGatewayTransactionID { get { return CS.General_v3.Settings.GetSetting(rootNode + "PaymentGatewayTransactionID"); } }

            public static double? GoogleLat
            {
                get
                {
                    string str = CS.General_v3.Settings.GetSetting(rootNode + "GoogleLat");
                    if (!String.IsNullOrEmpty(str))
                    {
                        return Convert.ToDouble(str);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            public static double? GoogleLng
            {
                get
                {
                    string str = CS.General_v3.Settings.GetSetting(rootNode + "GoogleLng");
                    if (!String.IsNullOrEmpty(str))
                    {
                        return Convert.ToDouble(str);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            public static int? GoogleZoomLevel
            {
                get
                {
                    string str = CS.General_v3.Settings.GetSetting(rootNode + "GoogleZoomLevel");
                    if (!String.IsNullOrEmpty(str))
                    {
                        return Convert.ToInt32(str);
                    }
                    else
                    {
                        return null;
                    }
                }
            }*/
            public static string GetAddressAsOneLine(string delimeter, bool includeCompanyName = false)
            {
                string companyName = null;
                if (includeCompanyName)
                {
                    companyName = Settings.GetSettingFromDatabase(Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_CompanyName);
                }
                string loc = Settings.GetSettingFromDatabase(Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Locality);
                string postCode = Settings.GetSettingFromDatabase(Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_PostCode);
                
                string locAndPostCode = loc;
                if (!String.IsNullOrEmpty(postCode))
                {
                    locAndPostCode += ", " + postCode;
                }
                string s = CS.General_v3.Util.Text.AppendStrings(delimeter, 
                    companyName, 
                    Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Address1.GetSettingFromDatabase(),
                    Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Address2.GetSettingFromDatabase(),
                    Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Address3.GetSettingFromDatabase(),
                    locAndPostCode,
                    Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Country.GetSettingFromDatabase());


                return s;

            }

        }

    }
}
