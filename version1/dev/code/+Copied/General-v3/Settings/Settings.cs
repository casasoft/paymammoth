using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Configuration;
using System.Web;
using System.IO;
using CS.General_v3.Classes.Attributes;

namespace CS.General_v3
{
    public static partial class Settings
    {
        public delegate string LoadSettingDelegate(string identifier, SettingsInfoAttribute settingInfo);
        public static event LoadSettingDelegate LoadSettingFromDatabase;





        private static string getSettingsUserXmlFilePath()
        {
            string path = "";
            if (string.IsNullOrEmpty(CustomSettingsUserFilePath))
            {

                List<string> list = new List<string>();
                if (!CS.General_v3.Util.Other.IsLocalTestingMachine)
                {
                    list.Add("/App_Data/Settings.online.xml");
                }

                list.Add(SettingsUserPath_1);
                

                for (int i = 0; i < list.Count;i++)
                {
                    path = CS.General_v3.Util.PageUtil.MapPath(list[i]);
                    if (File.Exists(path))
                        break;
                }



            }
            else
            {
                path = CustomSettingsUserFilePath;
            }
            return path;
        }


        private static readonly object __xmlDocLoadPadlock = new object();

        public static XmlDocument __xmlDocUser = null;
        public static XmlDocument _xmlDocUser
        {
            get
            {
                if (__xmlDocUser == null)
                {
                    lock (__xmlDocLoadPadlock)
                    {
                        if (__xmlDocUser == null)
                        {
                            string path = getSettingsUserXmlFilePath();
                            reloadSettingsUserXMLFile();
                            CS.General_v3.Util.IO.WatchFileForChanges(path, _onSettings_fileChange);

                        }
                    }
                }
                return __xmlDocUser;
            }
        }


        private static XmlDocument reloadSettingsXmlFileFromPath(string path)
        {
            XmlDocument doc = null;
            lock (__xmlDocLoadPadlock)
            {
                int tryTimes = 0;
                bool ok = false;
                while (!ok)
                {
                    try
                    {

                        string xmlContents = CS.General_v3.Util.IO.LoadFile(path);
                        if (!string.IsNullOrEmpty(xmlContents))
                        {
                            doc = new XmlDocument();
                            doc.LoadXml(xmlContents);
                            ok = true;

                        }

                        break;

                    }
                    catch (IOException exception)
                    {
                        ok = false;
                        string msg = exception.Message;
                        if (msg.Contains("cannot access the file"))
                        {
                            System.Threading.Thread.Sleep(5000);
                            //retry
                            if (tryTimes > 5)
                                throw;
                            tryTimes++;
                        }
                        else
                        {
                            throw;
                        }
                    }
                }
            }
            return doc;
        }


        private static void reloadSettingsUserXMLFile()
        {
            __xmlDocUser = reloadSettingsXmlFileFromPath(getSettingsUserXmlFilePath());
        }


        public static void _onSettings_fileChange(object sender, FileSystemEventArgs e)
        {
            if (__xmlDocUser != null)  reloadSettingsUserXMLFile();
            

            

        }
        public static bool DoNotUseSettings = false;




        public static string SettingsUserPath_1 = "/App_Data/Settings.user.xml";
        public static string CustomSettingsUserFilePath { get; set; }



        /// <summary>
        /// Returns a setting value
        /// </summary>
        /// <typeparam name="TOutputDataType"></typeparam>
        /// <param name="setting"></param>
        /// <param name="defaultValue"></param>
        /// <param name="isRequiredInLocalhost"></param>
        /// <returns></returns>
        private static TOutputDataType _getSettingFromDatabase<TOutputDataType>(string setting, SettingsInfoAttribute settingInfo)
        {
            string value =CS.General_v3.Classes.Application.AppInstance.Instance.GetSettingFromDatabase(setting, settingInfo);
             
            
            return CS.General_v3.Util.Other.ConvertStringToBasicDataType<TOutputDataType>(value);
        }
        public static string GetSettingFromDatabase(Enums.SETTINGS_ENUM identifier)
        {
            return GetSettingFromDatabase<string>(identifier);
        }

        public static TOutputDataType GetSettingFromDatabase<TOutputDataType>(Enums.SETTINGS_ENUM identifier)
        {
            return GetSettingFromDatabaseByGenericEnum<TOutputDataType>(identifier);
        }

        public static string GetSettingFromDatabaseByGenericEnum(Enum identifier)
        {
            return GetSettingFromDatabaseByGenericEnum<string>(identifier);
        }
        public static TOutputDataType GetSettingFromDatabaseByGenericEnum<TOutputDataType>(Enum identifier)
        {
            SettingsInfoAttribute settingsInfo = CS.General_v3.Util.EnumUtils.GetAttributeFromEnumValue<SettingsInfoAttribute>(identifier);
            if (settingsInfo == null)
                throw new InvalidOperationException("All settings must be decorated with the 'SettingsInfoAttribute' attribute - <" + identifier + ">");

            string sIdentifier = CS.General_v3.Util.EnumUtils.StringValueOf(identifier);
            return _getSettingFromDatabase<TOutputDataType>(sIdentifier, settingsInfo);
            
        }





        private static TOutputDataType _getSettingFromUserXmlFile<TOutputDataType>(Enum identifier, bool isRequiredInLocalhost)
        {
            SettingsInfoAttribute settingsInfo = CS.General_v3.Util.EnumUtils.GetAttributeFromEnumValue<SettingsInfoAttribute>(identifier);
            string sIdentifier = CS.General_v3.Util.EnumUtils.StringValueOf(identifier);
            return GetSettingFromUserXmlFile<TOutputDataType>(sIdentifier, settingsInfo, isRequiredInLocalhost);
        }

        public static TOutputDataType GetSettingFromUserXmlFile<TOutputDataType>(Enums.XMLFILE_SETTINGS_ENUM identifier, bool isRequiredInLocalhost)
        {
            return _getSettingFromUserXmlFile<TOutputDataType>(identifier, isRequiredInLocalhost);
        }





        /// <summary>
        /// Returns a setting value
        /// </summary>
        /// <typeparam name="TOutputDataType"></typeparam>
        /// <param name="setting"></param>
        /// <param name="defaultValue"></param>
        /// <param name="isRequiredInLocalhost"></param>
        /// <returns></returns>
        public static TOutputDataType GetSettingFromUserXmlFile<TOutputDataType>(string setting, SettingsInfoAttribute settingInfo, bool isRequiredInLocalhost)
        {
            string value = _getSettingFromXmlFile(setting, _xmlDocUser, settingInfo, isRequiredInLocalhost);
            return CS.General_v3.Util.Other.ConvertStringToBasicDataType<TOutputDataType>(value);
        }
        public static TOutputDataType GetSettingFromUserXmlFile<TOutputDataType>(Enums.XMLFILE_SETTINGS_ENUM normalIdentifier, Enums.XMLFILE_SETTINGS_ENUM localhostIdentifier, bool isRequiredInLocalhost)
        {
            TOutputDataType result = default(TOutputDataType);
            
            if (CS.General_v3.Util.Other.IsLocalTestingMachine)
            {
                result = GetSettingFromUserXmlFile<TOutputDataType>(localhostIdentifier, isRequiredInLocalhost);
            }
            if (result == null || result.Equals(default(TOutputDataType)))
            {
                result = GetSettingFromUserXmlFile<TOutputDataType>(normalIdentifier, isRequiredInLocalhost);
            }
           return result;
        }
        private static string _getSettingFromXmlFile(string settingValue, XmlDocument doc, SettingsInfoAttribute settingInfo, bool isRequiredInLocalHost = false)
        {
            string identifier = "Settings/" + settingValue.Replace("_", "/");
            if (identifier.EndsWith("/"))
                identifier = identifier.Substring(0, identifier.Length - 1);
            string s = settingInfo.GetDefaultValueAsString();
            
            if (doc != null)
            {
                XmlNode nodeSetting = doc.SelectSingleNode(identifier);
                if (CS.General_v3.Util.Other.IsLocalTestingMachine && nodeSetting == null &&isRequiredInLocalHost)
                {
                    throw new InvalidOperationException("Localhost: Make sure the Settings XML file contains a '" + identifier + "' node");
                }
                try
                {

                    if (nodeSetting != null && nodeSetting.ChildNodes.Count <= 1)
                    {
                        s = nodeSetting.InnerText;
                    }
                }
                catch (Exception ex)
                {

                }
            }
            
            
            return s;
        }



        private static readonly object tmpLock = new object();






    }
}
