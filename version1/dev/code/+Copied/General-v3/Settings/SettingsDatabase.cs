using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Configuration;
using System.Web;
using System.IO;

namespace CS.General_v3
{
    public static partial class Settings
    {
        public static class Database
        {
            public enum DATABASE_TYPE
            {
                MySQL,
                SQLite
            }
            public enum SCHEMA_CHECK_TYPE
            {
                Update,
                Create
            }

            private static string rootNode = "Database/";
            //public class OVERRIDE_INFO
            //{
            //    public string Host_Normal = null;
            //    public string Host_Localhost = null;
            //    public int? Port_Normal = null;
            //    public int? Port_Localhost = null;
            //    public string DatabaseName_Normal = null;
            //    public string DatabaseName_Localhost = null;
            //    public string User_Normal = null;
            //    public string User_Localhost = null;
            //    public string Pass_Normal = null;
            //    public string Pass_Localhost = null;
            //    public bool? EncryptedPass = null;
            //    public string PrimaryKeyValuesTable = null;
            //    public string CharacterSet = null;
            //    public bool? UsePrimaryKeyValuesTable = null;
            //    public void Reset()
            //    {
            //        PrimaryKeyValuesTable = null;
            //        DatabaseName_Localhost = DatabaseName_Normal = null;
            //        User_Normal = null;
            //        User_Localhost = null;
            //        Pass_Normal = Pass_Localhost = null;
            //        Port_Localhost = Port_Normal = null;
            //        UsePrimaryKeyValuesTable = null;
            //        Host_Normal = null;
            //        Host_Localhost = null;
            //        EncryptedPass = null;
            //        CharacterSet = null;
            //    }

            //}
            //private static OVERRIDE_INFO _OverrideInfo = null;
            //public static OVERRIDE_INFO OverrideInfo
            //{
            //    get
            //    {
            //        OVERRIDE_INFO info = null;
            //        if (_OverrideInfo == null)
            //        {
            //            _OverrideInfo = new OVERRIDE_INFO();
            //        }
            //        info = _OverrideInfo;

                   
            //        return info;
            //    }
            //    set
            //    {

            //        _OverrideInfo = value;
                    
            //    }
            //}

            public static DATABASE_TYPE? CustomDatabaseType { get; set; }

            public static DATABASE_TYPE DatabaseType
            {
                get
                {
                    DATABASE_TYPE dbType;
                    if (CustomDatabaseType.HasValue)
                    {
                        dbType = CustomDatabaseType.Value;
                    }
                    else
                    {

                        dbType= GetSettingFromUserXmlFile<DATABASE_TYPE>(CS.General_v3.Enums.XMLFILE_SETTINGS_ENUM.Database_DatabaseType, false);
                        
                    }
                    return dbType;

                }

            }
            public static bool UseNHibernateProfiler
            {
                get
                {
                    return GetSettingFromUserXmlFile<bool>(Enums.XMLFILE_SETTINGS_ENUM.UseNHibernateProfiler, false);
                }

            }
            public static string Host
            {
                get
                {

                    return GetSettingFromUserXmlFile<string>(Enums.XMLFILE_SETTINGS_ENUM.Database_Host, Enums.XMLFILE_SETTINGS_ENUM.Database_HostLocalhost, false);
                }

            }


            /*public static int Port
            {
                get
                {
                    if (OverrideInfo.Port != null)
                        return OverrideInfo.Port.Value;
                    else
                        return Convert.ToInt32(getDatabaseSetting(rootNode + "Port", "3306"));
                }
            }
            */

            public static int Port
            {
                get
                {
                    return GetSettingFromUserXmlFile<int?>(Enums.XMLFILE_SETTINGS_ENUM.Database_Port, Enums.XMLFILE_SETTINGS_ENUM.Database_PortLocalhost, false).GetValueOrDefault(3306);
                }

            }
            



            public static string User
            {
                get
                {
                    return GetSettingFromUserXmlFile<string>(Enums.XMLFILE_SETTINGS_ENUM.Database_User, Enums.XMLFILE_SETTINGS_ENUM.Database_UserLocalhost, false);
                }

            }
            






            public static string Pass
            {
                get
                {
                    return GetSettingFromUserXmlFile<string>(Enums.XMLFILE_SETTINGS_ENUM.Database_Pass, Enums.XMLFILE_SETTINGS_ENUM.Database_PassLocalhost, false); 
                    
                }

            }




            public static string DatabasePathOverride { get; set; }
            public static string DatabasePath
            {
                get
                {

                    if (DatabasePathOverride == null)
                        return GetSettingFromUserXmlFile<string>(Enums.XMLFILE_SETTINGS_ENUM.Database_Path, true); 
                        
                    else
                        return DatabasePathOverride;
                    
                }

            }
            public static string DatabaseName
            {
                get
                {
                    return GetSettingFromUserXmlFile<string>(Enums.XMLFILE_SETTINGS_ENUM.Database_Name, Enums.XMLFILE_SETTINGS_ENUM.Database_NameLocalhost, false); 
                    
                }

            }

            public static bool? CheckMappingsAndOthersOverride { get; set; }
            public static bool CheckMappingsAndOthers
            {
                get
                {

                    return true; //27/jan/2012 - this was overriden to always return true, as Lucene.Net does not support serialization

                    bool b = false;
                    if (GetWhetherToCheckSchema())
                        b = true;
                    else if (CheckMappingsAndOthersOverride.HasValue)
                        b = CheckMappingsAndOthersOverride.Value;
                    else if (!CS.General_v3.Util.Other.IsLocalTestingMachine)
                        b = true;
                    else
                        b = GetSettingFromUserXmlFile<bool>(Enums.XMLFILE_SETTINGS_ENUM.Database_CheckMappingsAndOthers, true);
                    return b;
                }
            }

            public static SCHEMA_CHECK_TYPE SchemaCheckType = SCHEMA_CHECK_TYPE.Update;

            public static bool? CheckSchemaOverride { get; set; }
            private static bool CheckSchema
            {
                get
                {
                    if (CheckSchemaOverride.HasValue)
                        return CheckSchemaOverride.Value;
                    else
                        return GetSettingFromUserXmlFile<bool>(Enums.XMLFILE_SETTINGS_ENUM.Database_CheckSchema, true); 
                }
            }
            public static string CheckSchemaFileLocation = "/App_Data/nhSavedMappings/checkschema.tmp";
            public static bool CheckIfSchemaFileExists()
            {
                string localPath = CS.General_v3.Util.PageUtil.MapPath(CheckSchemaFileLocation);
                return (File.Exists(localPath));
            }
            public static void SaveCheckSchemaFile()
            {
                string localPath = CS.General_v3.Util.PageUtil.MapPath(CheckSchemaFileLocation);
                
                CS.General_v3.Util.IO.SaveToFile(localPath,"CheckSchema");
            }

            public static bool GetWhetherToCheckSchema()
            {
                bool ok = false;
                if (CheckSchema)
                    ok = true;
                else if (GetSettingFromUserXmlFile<bool>(Enums.XMLFILE_SETTINGS_ENUM.Database_CheckSchemaBasedOnFileExists, true) && !CheckIfSchemaFileExists())
                {
                    ok = true;
                }
                return ok;
            }
            public static bool GenerateNHibernateXmlMappingFiles
            {
                get
                {
                    bool ok = false;
                    if (CS.General_v3.Util.Other.IsLocalTestingMachine && (GetSettingFromUserXmlFile<bool?>(Enums.XMLFILE_SETTINGS_ENUM.Database_GenerateNHibernateXmlMappingFiles,isRequiredInLocalhost:false).GetValueOrDefault(true)))
                    {
                        ok = true;
                    }

                    return ok;
                }
            }
            public static bool? CheckDefaultsInRealDatabaseOverride { get; set; }
            /// <summary>
            /// This will compare the temporary factories with the real database factories, and add them to the real database factories if they are empty
            /// </summary>
            public static bool CheckDefaultsInRealDatabase
            {
                get
                {
                    if (CheckDefaultsInRealDatabaseOverride.HasValue)
                        return CheckDefaultsInRealDatabaseOverride.Value;
                    else
                        return GetSettingFromUserXmlFile<bool>(Enums.XMLFILE_SETTINGS_ENUM.Database_CheckDefaultsInRealDatabase, true); 
                    
                }
            }
            public static bool UseRealDatabase
            {
                get
                {
                    if (CS.General_v3.Util.Other.IsLocalTestingMachine)
                        return GetSettingFromUserXmlFile<bool>(Enums.XMLFILE_SETTINGS_ENUM.Database_UseRealDatabase, true); 
                    else
                        return true;
                }
            }

            
            public static bool EnableDbTempItemChecker
            {
                get
                {
                    return GetSettingFromUserXmlFile<bool>(Enums.XMLFILE_SETTINGS_ENUM.EnableDbTempItemChecker, false); 
                    
                }
            }
        }

    }
}
