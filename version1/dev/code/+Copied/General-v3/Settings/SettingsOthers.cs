using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Configuration;
using System.Web;
using System.IO;

namespace CS.General_v3
{
    public static partial class Settings
    {
        public static class Others
        {
        //    private static string rootNode = "Others/";
            public static bool IsUsingTestingFramework
            {
                get { return Util.Other.IsInUnitTestingEnvironment; }
            }

        //    public static string BlogLink
        //    {
        //        get
        //        {
        //            string s = GetSetting("BlogLink"); ;

        //            return s;

        //        }
        //    }
        //    public static string WebsiteName
        //    {
        //        get
        //        {
        //            string s = GetSetting("Website/Name"); ;
        //            if (string.IsNullOrEmpty(s)) s = GetSetting("Title");
        //            return s;

        //        }
        //    }
        //    public static string Title
        //    {
        //        get
        //        {
        //            return WebsiteName;
        //        }
        //    }
        //    public static bool IsOnProductionServer
        //    {
        //        get
        //        {
        //            return GetSettingBool("IsOnProductionServer", false);
        //        }
        //    }
        //    public static string PermanentRedirectionUrls
        //    {
        //        get
        //        {
        //            return GetSetting("PermanentRedirectionUrls", "");
        //        }
        //    }
        //    public static Enums.JSCRIPT_FRAMEWORK? JScriptFramework_Override { get; set; }
        //    public static Enums.JSCRIPT_FRAMEWORK JScriptFramework
        //    {
        //        get
        //        {
        //            if (JScriptFramework_Override == null)
        //            {
        //                if (String.Compare(GetSetting("JScriptFramework"), "jquery", true) == 0)
        //                {
        //                    return Enums.JSCRIPT_FRAMEWORK.JQuery;
        //                }
        //                else
        //                {
        //                    //this is the default framework
        //                    return Enums.JSCRIPT_FRAMEWORK.JQuery;
        //                    //return JSCRIPT_FRAMEWORK.NotSpecified;
        //                }
        //            }
        //            else
        //            {
        //                return JScriptFramework_Override.Value;
        //            }
        //        }

        //    }
        //    public static string ApplicationRoot
        //    {
        //        get
        //        {
        //            string root = GetSetting("Application/Root");
        //            if (root != null && root.Length > 0)
        //            {
        //                if (root[root.Length - 1] == '/')
        //                    root = root.Remove(root.Length - 1, 1);
        //            }
        //            if (root != null && root.Length > 0)
        //            {
        //                if (root[0] != '/')
        //                    root = "/" + root;
        //            }
        //            return root;
        //        }
        //    }
        //    public static string ApplicationFolder
        //    {
        //        get
        //        {
        //            string root = GetSetting("Application/Folder");
        //            if (root != null && root.Length > 0)
        //            {
        //                if (root[root.Length - 1] != '\\')
        //                    root += "\\";
        //            }
        //            return root;
        //        }
        //    }

        //    public static string FFMpeg_Path
        //    {
        //        get
        //        {
        //            string ffmpegPath = GetSetting("FFMpeg/Path").Replace("/", "\\");
        //            string path = CS.General_v3.Settings.Others.LocalRootFolder + ffmpegPath + "ffmpeg.exe";
        //            return path;
        //        }
        //    }
        //    public static string MaxMind_GeoIP_DBLocation
        //    {
        //        get
        //        {
        //            return GetSetting("MaxMind/GeoIP_DBLocation", "/_common/geoip/maxmind.dat");
        //        }
        //    }

        //    public static double GeneralLogicVersion
        //    {
        //        get
        //        {
        //            return GetSettingDouble("GeneralLogicVersion", 1);
        //        }
        //    }

            public static int ServerGMT
            {
                get
                {
                    //return 0;
                    return GetSettingFromUserXmlFile<int>(Enums.XMLFILE_SETTINGS_ENUM.ServerTimeGmt, false);
                    //return GetSettingInt("Time/GMT");
                }
            }



            /// <summary>
            /// Gets the local root folder.  Returns either the local root folder, or LocalRootFolderCustom if it is set. Ends with a '/'
            /// </summary>
            public static string LocalRootFolder
            {
                get
                {
                    if (!string.IsNullOrEmpty(LocalRootFolderCustom))
                    {
                        return LocalRootFolderCustom;
                    }
                    
                    else
                    {
                        if (CS.General_v3.Util.Other.IsWebApplication)
                        {
                            //string s = CS.General_v3.Util.PageUtil.MapPath(CS.General_v3.Util.PageUtil.GetRoot());
                            string s = CS.General_v3.Util.PageUtil.MapPath("/");
                            if (s[s.Length - 1] != '\\') s += "\\";
                            return s;
                        }
                        else
                        {
                            return CS.General_v3.Util.ApplicationUtil.ApplicationPath;
                        }
                    }
                }
            }
            
            /// <summary>
            /// When this is set, the return of 'LocalRootFolder' is this value
            /// </summary>
            public static string LocalRootFolderCustom
            {
                get { return _LocalRootFolderCustom; }
                set
                {
                    _LocalRootFolderCustom = value;
                    if (_LocalRootFolderCustom != null && _LocalRootFolderCustom.Length > 0 && _LocalRootFolderCustom[_LocalRootFolderCustom.Length - 1] != '\\')
                        _LocalRootFolderCustom += "\\";
                }
            }
            private static string _LocalRootFolderCustom = "";

            private static string _BaseUrlCustom_ApplicationWide = "";

            /// <summary>
            /// The base URL to use when calling Util.PageUtil.GetBaseURL().  Please do not specify the http:// or https://, and without a trailing /
            /// </summary>
            public static string BaseUrlCustom_ApplicationWide
            {
                get {
                    return _BaseUrlCustom_ApplicationWide; 
                
                }
                set
                {

                    _BaseUrlCustom_ApplicationWide = parseBaseUrlCustom(value);

                }
            }
            public static string UserFakeIPV4Address {
                get {
                    if (CS.General_v3.Classes.Application.AppInstance.Instance != null)
                    {
                        return Settings.GetSettingFromDatabase(Enums.SETTINGS_ENUM.UserInfo_FakeIPV4Address);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            public static string UserFakeIPV6Address
            {
                get
                {

                    return Settings.GetSettingFromDatabase(Enums.SETTINGS_ENUM.UserInfo_FakeIPV6Address);
                }
            }
            private static string parseBaseUrlCustom(string url)
            {
                string s = url;
                if (s != null && s.Length >= "http://".Length && s.Substring(0, "http://".Length).ToLower() == "http://")
                {
                    s = s.Remove(0, "http://".Length);
                }
                else if (s != null && s.Length >= "https://".Length && s.Substring(0, "https://".Length).ToLower() == "https://")
                {
                    s = s.Remove(0, "https://".Length);
                }
                if (s != null && s.Length > "/".Length && s[s.Length - 1] == '/')
                    s = s.Remove(s.Length - 1, 1);
                return s;
            }

            /// <summary>
            /// The base URL to use when calling Util.PageUtil.GetBaseURL().  Please do not specify the http:// or https://, and without a trailing /
            /// </summary>
            public static string BaseUrlCustom_SessionWide
            {
                get
                {
                    if (CS.General_v3.Util.PageUtil.CheckSessionExistsAtContext())
                        return CS.General_v3.Util.PageUtil.GetSessionObject<string>("BaseUrlCustom_SessionWide");
                    else
                        return null;

                }
                set
                {

                    if (CS.General_v3.Util.PageUtil.CheckSessionExistsAtContext())
                    {
                        CS.General_v3.Util.PageUtil.SetSessionObject("BaseUrlCustom_SessionWide", parseBaseUrlCustom(value));
                    }



                }
            }

            public static bool IsInLocalTesting
            {
                get
                {
                    return CS.General_v3.Util.Other.IsLocalTestingMachine;
                   

                }
            }

            public const string JAVASCRIPT_VERSION = "v3";



            /// <summary>
            /// this boolean is used to stop throwing thread-state exceptiosn when an error occurs in a thread.  Only in localhost!
            /// </summary>
            public static bool DontThrowThreadStateExceptionsInLocalhost
            {
                get { return GetSettingFromUserXmlFile<bool>(Enums.XMLFILE_SETTINGS_ENUM.Other_DontThrowThreadStateExceptionsInLocalhost,false); }
            }
        }
    }
}
