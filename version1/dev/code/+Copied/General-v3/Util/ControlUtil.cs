﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.IO;
namespace CS.General_v3.Util
{
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using System.Web;

    public static class ControlUtil
    {
        private class _ControlsCollectionToListClass
        {
            public List<Control> list { get; set; }
            public Control Control { get; set; }
            private void addControlsToList(Control ctrl)
            {
                list.Add(ctrl);
                if (ctrl.Controls != null)
                {
                    foreach (Control item in ctrl.Controls)
                    {
                        addControlsToList(item);
                    }

                }
            }

            public _ControlsCollectionToListClass()
            {
                

            }

            public List<Control> GetList(Control ctrl)
            {
                this.list = new List<Control>();
                addControlsToList(ctrl);
                return this.list;
            }
        }
        public static T GetFirstControlFromHtmlTagName<T>(Control c, string tagName, bool recursive = false) where T :Control
        {
            List<T> controls = new List<T>();
            getControlsFromHtmlTagName(controls, c, tagName, true, recursive);
            return controls.FirstOrDefault();
        }
        private static void getControlsFromHtmlTagName<T>(List<T> result, Control c, string tagName, bool loadOnlyFirst, bool recursive = false) where T : Control
        {
            if (c != null)
            {
                var children = c.Controls;
                foreach (Control child in children)
                {
                    if (child is T)
                    {
                        
                        if (child is HtmlControl)
                        {
                            HtmlControl hControl = ((HtmlControl) child);
                            if (string.Compare(hControl.TagName, tagName, true) == 0)
                            {
                                result.Add((T)child);
                            }
                        }
                        else if (child is WebControl)
                        {
                            WebControl wControl = (WebControl) child;
                            //Not implemented in this case
                            


                        }
                        if (recursive)
                        {
                            T foundControl = GetFirstControlFromHtmlTagName<T>((T)child, tagName, recursive);
                            if (foundControl != null)
                            {
                                result.Add(foundControl);
                            }
                        }
                        if (loadOnlyFirst && result.Count > 0)
                        {
                            break;
                        }
                    }
                }
            }
        }
        public static List<T> GetControlsFromHtmlTagName<T>(Control c, string tagName, bool recursive = false) where T : Control
        {
            List<T> result = new List<T>();
            getControlsFromHtmlTagName(result, c, tagName, recursive);
            return result;
        }
        /// <summary>
        /// Gets all the controls under this control as a list, INCLUDING this control
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public static List<Control> GetAllControlsUnderControl(Control c)
        {
            _ControlsCollectionToListClass listGen = new _ControlsCollectionToListClass();
            return listGen.GetList(c);
        }
        public static string RenderInnerContents(System.Web.UI.Control c)
        {
            StringBuilder html = new StringBuilder();
            foreach(Control control in c.Controls)
            {
                html.Append(RenderControl(control));
            }
            return html.ToString();
        }
        public static string RenderControl(System.Web.UI.Control c)
        {
            return RenderControl(c, true);
        }
        public static string RenderControl(System.Web.UI.Control c, bool removeLineBreaks)
        {
            
            StringBuilder sb = new StringBuilder();

            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter textWriter = new HtmlTextWriter(sw);

            
            c.RenderControl(textWriter);
            string output = sb.ToString();
            if (removeLineBreaks)
            {
                output = output.Replace("\r\n", "");
            }
            return output;
        }
        /// <summary>
        /// Adds a control to the display list as a sibling relative to another particular control
        /// </summary>
        /// <param name="controlToAdd"></param>
        /// <param name="referenceControl"></param>
        /// <param name="addBefore"></param>
        public static void AddControlRelativeToControlAsSibling(Control controlToAdd, Control referenceControl, bool addBefore)
        {
            
            if (referenceControl.Parent != null)
            {
                int refControlIndex = 0;
                for (int i = 0; i < referenceControl.Parent.Controls.Count; i++)
                {
                    if (referenceControl.Parent.Controls[i] == referenceControl)
                    {
                        refControlIndex = i;
                        break;
                    }
                }
                referenceControl.Parent.Controls.AddAt((addBefore ? refControlIndex : refControlIndex + 1), controlToAdd);
            }
            else
            {
                throw new Exception("Control " + referenceControl.ID + " has got no parent, please add it to display list");
            }
        }

        /// <summary>
        /// Removes a control from it's parent container,
        /// </summary>
        /// <param name="ctrl"></param>
        /// <returns></returns>
        public static bool RemoveControlFromParentContainer(Control ctrl)
        {
            bool ok = false;
            if (ctrl != null &&  ctrl.Parent != null)
            {
                ctrl.Parent.Controls.Remove(ctrl);
                ok = true;
            }
            return ok;
            
        }
    }
}
