﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Util
{
    public static class IPAddressUtil
    {
        public static string GetUserIPAddress(bool ipv4 = true)
        {
            return CS.General_v3.Util.PageUtil.GetUserIP(ipv4);
        }
        public static string ConvertIPV4ToIPV6(string IPV4)
        {
            string[] octets = IPV4.Split('.');
            byte[] octetBytes = new byte[4];
            for (int i = 0; i < 4; ++i)
            {
                octetBytes[i] = (byte)int.Parse(octets[i]);
            }

            byte[] ipv4asIpV6addr = new byte[16];
            ipv4asIpV6addr[10] = (byte)0xff;
            ipv4asIpV6addr[11] = (byte)0xff;
            ipv4asIpV6addr[12] = octetBytes[0];
            ipv4asIpV6addr[13] = octetBytes[1];
            ipv4asIpV6addr[14] = octetBytes[2];
            ipv4asIpV6addr[15] = octetBytes[3];

            string ipv6 = "";
            for (int i = 0; i < ipv4asIpV6addr.Length; i +=2)
            {
                if (i > 0)
                {
                    ipv6 += ":";
                }

                ipv6 += CS.General_v3.Util.Text.PadText(ipv4asIpV6addr[i].ToString("X"), Text.ALIGN.Right, 2,"0");
                ipv6 += CS.General_v3.Util.Text.PadText(ipv4asIpV6addr[i+1].ToString("X"), Text.ALIGN.Right, 2, "0");
            }
            return ipv6;

        }
    }
}
