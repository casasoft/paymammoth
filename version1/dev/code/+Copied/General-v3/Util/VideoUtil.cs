﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Util
{
    public static class VideoUtil
    {
        public static List<string> GetVideoFilesExtensions()
        {
            List<string> list = new List<string>();
            list.Add("flv");
            list.Add("mpg");
            list.Add("mpeg");
            list.Add("wmv");
            list.Add("wmv1");
            list.Add("wmv2");
            list.Add("wmv3");
            list.Add("avi");
            list.Add("mp4");
            list.Add("mov");
            list.Add("asf");
            return list;
        }
        public static bool CheckVideoExtension(string filename)
        {
            string ext = CS.General_v3.Util.IO.GetExtension(filename);
            ext = ext.ToLower();
            List<string> allowed = GetVideoFilesExtensions();
            return allowed.Contains(ext);
        }
    }
}
