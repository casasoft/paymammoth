﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CS.General_v3.Util
{
    public static class ValidationUtil
    {
        public static bool IsEmail(string s)
        {
            //return Regex.IsMatch(s, @"[a-zA-Z0-9._%+-]+@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,4}");
            return Regex.IsMatch(s, @"\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}\b"); 
        }
        public static bool IsDouble(string s)
        {
            return Regex.IsMatch(s, @"[-+]?[0-9]*\.?[0-9]+");
            
        }
        public static bool IsInteger(string s)
        {
            return Regex.IsMatch(s, @"[-+]?[0-9]*");
        }
        /// <summary>
        /// Validates a date in dd/mm/yyyy format
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static bool IsValidDate_DD_MM_YYYY(string s)
        {
            string currYearStart= DateTime.Now.Year.ToString().Substring(0, 2);

            return Regex.IsMatch(s, @"^(0[1-9]|[12][0-9]|3[01])([- /.])(0[1-9]|1[012])\2(19|20|21|22|23|24|" + currYearStart + @")\d\d$");
            
        }




    }
}
