using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Web.UI.WebControls;
using System.Collections.Specialized;

namespace CS.General_v3.Util
{
    public static class Stopwatch
    {
        static Stopwatch()
        {
            ResetTimersBetween(0, 1000);
        }

        public static int TICK_COUNTER_MAX = 1001;
        private static long[] tickCounterTmp = null;
        public static long[] tickCounter = null;
       
        private static long LAST_TICK_COUNT;
        public static void ResetTimersBetween(int from, int to)
        {
            for (int i = from; i <= to; i++)
            {
                ResetTimer(i);
            }
        }
        public static void ResetTimers(params int[] IDs)
        {
            for (int i = 0; i < IDs.Length; i++)
            {
                ResetTimer(IDs[i]);
            }
        }
        /// <summary>
        /// Resets the counter
        /// </summary>
        /// <param name="ID">ID of timer (from 0 to 4)</param>
        public static void ResetTimer(int ID)
        {
            if (tickCounter == null)
            {
                tickCounter = new long[TICK_COUNTER_MAX];
                tickCounterTmp = new long[TICK_COUNTER_MAX];
            }
            tickCounter[ID] = 0;
            tickCounterTmp[ID] = 0;
        }
        /// <summary>
        /// Start the timer, to calculate time, and resets it from 0
        /// </summary>
        /// <param name="ID">ID of timer (from 0 to 4)</param>
        public static void ResetAndStartTimer(int ID)
        {
            ResetTimer(ID);
            StartTimer(ID);

        }
        /// <summary>
        /// Start the timer, to calculate time
        /// </summary>
        /// <param name="ID">ID of timer (from 0 to 4)</param>
        public static void StartTimerAndStopPrev(int ID)
        {
            EndTimer(ID - 1);
            StartTimer(ID);

        }
        /// <summary>
        /// Start the timer, to calculate time
        /// </summary>
        /// <param name="ID">ID of timer (from 0 to 4)</param>
        public static void StartTimer(int ID)
        {
            if (tickCounter == null)
                ResetTimer(ID);
            tickCounterTmp[ID] =CS.General_v3.Util.Date.Now.Ticks * 100;;
            
        }
        /// <summary>
        /// Returns the total time passed in nanoseconds
        /// </summary>
        /// <param name="ID">ID of timer (from 0 to 4)</param>
        /// <returns>total of nanoseconds (1,000,000,000 = 1 second)</returns>
        public static long EndTimer(int ID)
        {
            long currTicks = CS.General_v3.Util.Date.Now.Ticks * 100;

            tickCounterTmp[ID] = currTicks - tickCounterTmp[ID];
            tickCounter[ID] += tickCounterTmp[ID];
            return ReadTimer(ID);
        }
        /// <summary>
        /// Returns the total time passed in milleseconds
        /// </summary>
        /// <param name="ID">ID of timer (from 0 to 4)</param>
        /// <returns>total of milleseconds</returns>
        public static double EndTimerMilleSec(int ID)
        {
            EndTimer(ID);
            return ReadTimerMilleSec(ID);
        }
        /// <summary>
        /// Returns the total time passed in microseconds 
        /// </summary>
        /// <param name="ID">ID of timer (from 0 to 4)</param>
        /// <returns>total of microseconds (1,000,000 = 1 second)</returns>
        public static double EndTimerMicroSec(int ID)
        {
            EndTimer(ID);
            return ReadTimerMicroSec(ID);
        }
        /// <summary>
        /// Returns the total time passed in seconds
        /// </summary>
        /// <param name="ID">ID of timer (from 0 to 4)</param>
        /// <returns>total of seconds</returns>
        public static double EndTimerSeconds(int ID)
        {
            EndTimer(ID);
            return ReadTimerSecond(ID);

        }

        public static long ReadTimer(int ID)
        {
            return tickCounter[ID];
        }
        public static double ReadTimerMicroSec(int ID)
        {
            return (double)ReadTimer(ID) / (double)1000;
        }
        public static double ReadTimerMilleSec(int ID)
        {
            return ReadTimerMicroSec(ID) / (double)1000;
        }
        public static List<string> ReadTimerMilleSecBetweenAsStr(int IDFrom, int IDTo)
        {
            List<string> list = new List<string>();
            for (int i = IDFrom; i <= IDTo; i++)
            {
                double ms = ReadTimerMilleSec(i);
                string s = CS.General_v3.Util.Text.PadText(i.ToString(), Text.ALIGN.Right, 5) + " => " + ms.ToString("0.0000");
                list.Add(s);
            }
            return list;
            
        }
        public static double ReadTimerSecond(int ID)
        {
            return ReadTimerMilleSec(ID) / (double)1000;
        }

    }
}
