﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CS.General_v3.Controls.WebControls.Common;
namespace CS.General_v3.Util
{
    public static class JQueryUtil
    {
        /// <summary>
        /// When using pretty sociable, you need to add title / excerpt.  And make rel='prettySociable'
        /// </summary>
        /// <param name="url"></param>
        /// <param name="title"></param>
        /// <param name="excerpt"></param>
        /// <returns></returns>
        public static void AddPrettySociableRelTag(Control c, string title, string excerpt)
        {
            string rel = "prettySociable";
           
            rel += ";title:" + title;
            
            rel += ";excerpt:" + excerpt;
            
            if (c is HtmlControl)
            {
                ((HtmlControl)c).Attributes["rel"] = rel;
            }
            else if (c is MyImage)
            {

                ((MyImage)c).Rel = rel;
            }
            else if (c is WebControl)
            {

                ((WebControl)c).Attributes["rel"] = rel;
            }

            if (c is HtmlAnchor)
            {
                HtmlAnchor a = (HtmlAnchor)c;
                string url = a.HRef;
                url = CS.General_v3.Util.PageUtil.GetAbsoluteUrlFromRelative(url);
                a.HRef = url;
            }
            else if (c is MyImage)
            {
                MyImage img = (MyImage)c;
                string url = img.HRef;
                url = CS.General_v3.Util.PageUtil.GetAbsoluteUrlFromRelative(url);
                img.HRef = url;
            }
            else if (c is MyAnchor)
            {
                MyAnchor a = (MyAnchor)c;
                string url = a.Href;
                url = CS.General_v3.Util.PageUtil.GetAbsoluteUrlFromRelative(url);
                a.Href = url;
            }

        }

    }
}
