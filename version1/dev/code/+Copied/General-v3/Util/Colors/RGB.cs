﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
namespace CS.General_v3.Util.Colors
{
        public class RGB
        {
            
            private uint _r;
            private uint _g;
            private uint _b;

            private uint _color;
            public RGB(uint r, uint g, uint b)
            {
                this._r = r;
                this._g = g;
                this._b = b;
                this.updateColor();
            }
            public RGB(uint color) {
                this._color = color;
                this.updateRGB();
            }


            private void updateRGB()
            {
                RGB rgb = GetRGBFromColor(this._color);
                this._r = rgb.R;
                this._g = rgb.G;
                this._b = rgb.B;
            }
            private void updateColor()
            {
                this._color = GetColorFromRGB(this._r, this._g, this._b);
            }
            /// <summary>
            /// R channel in hex format e.g. 0xCC or 220 [0..255]
            /// </summary>
            public uint R
            {
                get { return this._r; }
                set
                {
                    this._r = value;
                    updateColor();
                }
            }
            /// <summary>
            /// G channel in hex format e.g. 0xCC or 220 [0..255]
            /// </summary>
            public uint G
            {
                get { return this._g; }
                set
                {
                    this._g = value;
                    updateColor();
                }
            }
            /// <summary>
            /// B channel in hex format e.g. 0xCC or 220 [0..255]
            /// </summary>
            public uint B
            {
                get { return this._b; }
                set
                {
                    this._b = value;
                    updateColor();
                }
            }
            /// <summary>
            /// Color in hex format e.g. 0xFF0000
            /// </summary>
            public uint Color { get { return this._color; }
                set
                {
                    this._color = value;
                    this.updateRGB();
                }
            }
            /// <summary>
            /// R channel in percentage [0..1]
            /// </summary>
            public double RPerc
            {
                get
                {
                    return (double)this.R / (double)0xFF;
                }
                set
                {
                    this.R = (uint)Math.Round(RPerc * (double)0xFF);
                }
            }
            /// <summary>
            /// G channel in percentage [0..1]
            /// </summary>
            public double GPerc
            {
                get
                {
                    return (double)this.G / (double)0xFF;
                }
                set
                {
                    this.G = (uint)Math.Round(GPerc * (double)0xFF);
                }
            }
            /// <summary>
            /// B channel in percentage [0..1]
            /// </summary>
            public double BPerc
            {
                get
                {
                    return (double)this.B / (double)0xFF;
                }
                set
                {
                    this.B = (uint)Math.Round(BPerc * (double)0xFF);
                }
            }


            /// <summary>
            /// Returns a color from the Hex value - If it begins with # or 0x, it is removed
            /// </summary>
            /// <param name="Hex"></param>
            /// <returns></returns>
            public static Color ColorFromHex(string Hex)
            {
                if (Hex != null)
                {
                    if (Hex.ToLower().StartsWith("0x"))
                        Hex = Hex.Substring(2);
                    if (Hex != null && Hex.Length >= 1 && Hex[0] != '#')
                    {
                        Hex = "#" + Hex;
                    }
                    Color c = System.Drawing.ColorTranslator.FromHtml(Hex);
                    return c;
                }
                else
                {
                    return System.Drawing.Color.Black;
                }
            }
            public static RGB GetRGBFromColor(uint color)
            {
                uint r = color >> 16;
                uint g = (color >> 8) & 0xFF;
                uint b = (color & 0xFF);
                return new RGB(r, g, b);

            }
            public static uint GetUIntFromRGB(uint r, uint g, uint b)
            {
                uint color = r << 16 | g << 8 | b;

                return color;
            }
            public static uint GetUIntValueFromColor(Color clr)
            {
                return GetColorFromRGB(clr.R, clr.G, clr.B);
                
            }
            public static void SplitUIntIntoRGB(uint color, out uint r, out uint g, out uint b)
            {
                r = color & 0xFF0000;
                g = color & 0x00FF00;
                b = color & 0x0000FF;
                g /= 0xFF;
                if (g > 255) g = 255;
                r /= 0xFFFF;
                if (r > 255) r = 255;


            }
            public static Color GetColorFromUInt(uint color)
            {
                
                uint r, g, b;
                SplitUIntIntoRGB(color, out r, out g, out b);
                return System.Drawing.Color.FromArgb((int)r, (int)g, (int)b);
            }
            public static uint GetColorFromRGB(uint r, uint g, uint b)
            {
                uint color = r << 16 | g << 8 | b;

                return color;
            }
        }

        


}
