﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.ComponentModel;

namespace CS.General_v3.Util
{
    public static class NumberUtil
    {
        public enum NUMBER_FORMAT_TYPE
        {
            [Description("c")]
            Currency,
            [Description("d")]
            Decimal,
            [Description("e")]
            Exponential,
            [Description("f")]
            FixedPoint,
            [Description("g")]
            General,
            [Description("n")]
            Number,
            [Description("p")]
            Percent,
            [Description("r")]
            RoundTrip,
            [Description("x")]
            Hexadecimal
        }

        public static double GetPriceFromOrPriceToFromPercentage(bool isPriceFrom, double pricePercentageDifference, double price)
        {
            double result = 0;
            if (isPriceFrom)
            {
                result = ((100 - pricePercentageDifference) * price) / 100;
            }
            else
            {
                result = ((100 + pricePercentageDifference) * price) / 100;
            }
            return result;
        }

        public static string ConvertNumberFormatTypeToFormatCode(NUMBER_FORMAT_TYPE value)
        {
            string code = "";
            switch (value)
            {
                case NUMBER_FORMAT_TYPE.Currency: code = "C"; break;
                case NUMBER_FORMAT_TYPE.Decimal: code = "D"; break;
                case NUMBER_FORMAT_TYPE.Exponential: code = "E"; break;
                case NUMBER_FORMAT_TYPE.FixedPoint: code = "F"; break;
                case NUMBER_FORMAT_TYPE.General: code = "G"; break;
                case NUMBER_FORMAT_TYPE.Number: code = "N"; break;
                case NUMBER_FORMAT_TYPE.Percent: code = "P"; break;
                case NUMBER_FORMAT_TYPE.RoundTrip: code = "R"; break;
                case NUMBER_FORMAT_TYPE.Hexadecimal: code = "X"; break;
                default:
                    throw new InvalidOperationException("Invalid code < " + value + ">");
            }
            return code;
        }
        
        public static string FormatNumber(double numberToFormat, NUMBER_FORMAT_TYPE formatType, IFormatProvider formatProvider = null, string specificCurrencySymbol = null)
        {
            string formatCode = ConvertNumberFormatTypeToFormatCode(formatType);
            IFormatProvider formatProv = formatProvider;
            if (specificCurrencySymbol != null)
            {
                NumberFormatInfo clonedNumInfo = null;
                if (formatProvider is CultureInfo)
                {
                    clonedNumInfo = (NumberFormatInfo)((CultureInfo)formatProvider).NumberFormat.Clone();
                }
                else if (formatProvider is NumberFormatInfo)
                {
                    clonedNumInfo = (NumberFormatInfo)((NumberFormatInfo)formatProvider).Clone();
                }

                clonedNumInfo.CurrencySymbol = specificCurrencySymbol;
                formatProv = clonedNumInfo;
            }

            return numberToFormat.ToString(formatCode, formatProv);
            
        }

        public static string FormatCurrencyUsingCurrentCultureInfo(double price, bool htmlEncode = true)
        {
            var culture = CultureInfo.CurrentCulture;
            string s = null;
            string currencySign = null;

            return price.ToString(CS.General_v3.Util.EnumUtils.StringValueOf(NUMBER_FORMAT_TYPE.Currency), culture);
        }
        
        public static string FormatCurrency(double price, int? decimalPlaces = 2, string thousandsDelimiter = ",", string currencySymbol = "&euro;")
        {
            NumberFormatInfo nfi = (NumberFormatInfo)CultureInfo.CurrentCulture.NumberFormat.Clone();
            if (thousandsDelimiter != null)
            {
                nfi.CurrencyGroupSeparator = thousandsDelimiter;
            }
            if (decimalPlaces.HasValue)
            {
                nfi.CurrencyDecimalDigits = decimalPlaces.Value;
            }
            if (!string.IsNullOrEmpty(currencySymbol))
            {
                nfi.CurrencySymbol = currencySymbol;
            }
            var s = price.ToString(ConvertNumberFormatTypeToFormatCode(NUMBER_FORMAT_TYPE.Currency), nfi);

            return s; 
            
            

        }
        public static bool AreNumeric(params object[] list)
        {
            for (int i = 0; i < list.Length; i++)
            {
                object val = list[i];
                if (!IsNumeric(val))
                {
                    return false;
                }
            }
            return true;
        }
        public static int ConvertHexToDecimal(string hexValue)
        {
            return Convert.ToInt32(hexValue, 16);
        }
        public static class Financial
        {
            public static double GetVATRateFromTotalAndVATAmt(double totalIncVAT, double vatOnly)
            {
                return ((totalIncVAT) / (totalIncVAT - vatOnly)) - 1;
            }
            /// <summary>
            /// Gets the total VAT amount from a price (inclusive of vat)
            /// </summary>
            /// <param name="totalIncVAT"></param>
            /// <param name="VATRate">VAT rate , from 0 to 1.</param>
            /// <returns></returns>
            public static double GetVATAmountFromPriceIncVAT(double totalIncVAT, double VATRate)
            {
                if (VATRate > 1 || VATRate < 0)
                    throw new InvalidOperationException("This function should take a VAT Rate between 0 and 1");

                return (totalIncVAT * (VATRate / (double)((double)1 + (double)VATRate)));
            }
        }
        public static bool IsInteger(double d)
        {

            double d2 = Math.Truncate(d);
            return (d2 == d);
            
        }
        public static bool HasDecimalPoint(double d)
        {
            double d1 = d;
            double d2 = Util.Conversion.ToDouble(d.ToString("0"));
            return (d1 != d2);
                
        }
        public static void ConvertDecimalToFraction(double d, out int numerator, out int denominator)
        {
            ConvertDecimalToFraction(d, 28, out numerator, out denominator);
        }
        public static string FormatNumber(double d, string thousandsDelimiter = ",", int? decimalPlaces = 2)
        {
            NumberFormatInfo nfi = (NumberFormatInfo)CultureInfo.InvariantCulture.NumberFormat.Clone();
            if (thousandsDelimiter != null)
            {
                nfi.NumberGroupSeparator = thousandsDelimiter;
            }
            if (decimalPlaces.HasValue)
            {
                nfi.CurrencyDecimalDigits = nfi.NumberDecimalDigits = nfi.PercentDecimalDigits = decimalPlaces.Value;
            }

            return d.ToString("n", nfi);

        }
        public static void ConvertDecimalToFraction(double d, int maxPrecision, out int numerator, out int denominator)
        {
            if (maxPrecision > 8 || maxPrecision <= 0)
                maxPrecision = 8;
            int wholePart = (int)d;
            d -= wholePart;
            double dDenominator = 1;
            double dNumerator = d;
            int times = 0;
            while (!IsInteger(dNumerator) && times < maxPrecision)
            {
                times++;
                dNumerator *= 10;
                dDenominator *= 10;
            }
            dNumerator = Math.Round(dNumerator);
            dDenominator = Math.Round(dDenominator);

            long gcd = 0;
            long lNumerator = (long)dNumerator;
            long lDenominator = (long)dDenominator;
            do
            {
                gcd = FindGreatestCommonDivisor((long)lNumerator, (long)lDenominator);
                lNumerator = lNumerator / gcd;
                dDenominator = dDenominator / gcd;
            } while (gcd != 1);
            numerator = (int)lNumerator;
            denominator = (int)lDenominator;
            
        }
        public static long FindGreatestCommonDivisor(long a, long b)
        {
                    
             while (a != 0 && b != 0)
             {
                 if (a > b)
                    a %= b;
                 else
                    b %= a;
             }

             if (a == 0)
                 return b;
             else
                 return a;
        }
        public static int FindGreatestCommonDivisorForAll(params double[] numbers)
        {
            int[] arr = new int[numbers.Length];
            for (int i = 0; i < numbers.Length; i++)
            {
                arr[i] = (int)numbers[i];
            }
            return FindGreatestCommonDivisorForAll(arr);
        }

        public static int FindGreatestCommonDivisorForAll(params int[] numbers)
        {
            if (numbers.Length == 0)
                return 0;
            else if (numbers.Length == 1)
                return numbers[0];
            else
            {
                long currGCD = numbers[0];
                for (int i = 1; i < numbers.Length; i++)
                {
                    currGCD = FindGreatestCommonDivisor(currGCD, numbers[i]);
                }
                return (int)currGCD;
            }



        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="taxRate">0 - 100</param>
        /// <returns></returns>
        public static double RemoveTaxFromValue(double value, double taxRate)
        {
            return value * (100d / (100d + taxRate));
        }

        public static double TruncateNumberInRange(double num, double from, double to)
        {
            if (num < from)
            {
                return from;
            }
            else if (num > to)
            {
                return to;
            }
            else
            {
                return num;
            }
        }

        /// <summary>
        /// Return the indices of the start / to of the items with pagin
        /// </summary>
        /// <param name="pg">Page index (1-based)</param>
        /// <param name="pgSize">The size of the page</param>
        /// <param name="itemIndexFrom">Returns the from index</param>
        /// <param name="itemIndexTo">Returns the to index</param>
        /// <param name="totalCount">The total count of items.  If left -1, this does not apply</param>
        /// <returns></returns>
        public static void GetPageItemRanges(int pg, int pgSize, out int itemIndexFrom, out int itemIndexTo, int totalCount = -1) {
            itemIndexFrom = ((pg - 1) * pgSize) + 1;
            itemIndexTo = pg * pgSize;
            if (totalCount != -1)
            {
                itemIndexTo = Math.Min(totalCount, itemIndexTo);
            }

        }

        /// <summary>
        /// Tries to parse a number into a string.  If it cannot parse, default Num is returend
        /// </summary>
        /// <param name="s"></param>
        /// <param name="defaultNum"></param>
        /// <returns></returns>
        public static int GetNumberFromString(string s, int defaultNum)
        {
            int num;
            if (!Int32.TryParse(s, out num))
                num = defaultNum;
            return num;
        }
        /// <summary>
        /// Returns wheter a generic object is numeric or not, like int, double ...
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>True / False whether it is numeric or not.</returns>
        public static bool IsNumeric(object obj)
        {
            bool isNum; 
            double retNum; 
            isNum = Double.TryParse(Convert.ToString(obj), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum); 
            return isNum;  
        }
        public static int RoundNumber(double number, int roundToTheNearest)
        {

            // n = 31
            // r = 15
            // x = 2.01010101
            // x = 2
            // x = 2 x 15 = 30
            //n = 38
            // x = 38 / 15
            double num = number / (double)roundToTheNearest;
            return (int)Math.Round(num) * roundToTheNearest;
            /*
            double midPoint = (double)roundToTheNearest / (double)2;
            double remainder = number % roundToTheNearest;
            int whole = (int)number / roundToTheNearest;
            int num = whole * roundToTheNearest;
            if (remainder >= midPoint)
            {
                num += roundToTheNearest;
            }
            return num;*/
        }
       /* public static T ParseNumber<T>(object val) where T : IComparable
        {
            double v = 0;
            if (double.TryParse(val.ToString(), out v)) {
                return (T)(IComparable)v;
            }
            return default(T);
        }*/
        public static double ParseDouble(object val)
        {
            double v = 0;
            if (double.TryParse(val.ToString(), out v))
            {
                return v;
            }
            return 0;
        }
        public static int ParseInt(object val)
        {
            return (int)ParseLong(val);
        }
        public static long ParseLong(object val)
        {
            long v = 0;
            if (long.TryParse(val.ToString(), out v))
            {
                return v;
            }
            return 0;
        }
        public static int Max(params int[] numbers)
        {
            var d = int.MinValue;
            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] > d)
                    d = numbers[i];
            }
            return d;

        }
        public static double Max(params double[] numbers)
        {
            double d = double.MinValue;
            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] > d)
                    d = numbers[i];
            }
            return d;
            
        }
    }
}
