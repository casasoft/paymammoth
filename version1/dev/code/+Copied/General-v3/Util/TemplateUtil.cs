﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Commons.Collections;
using NVelocity;
using NVelocity.App;
using System.Collections;

namespace CS.General_v3.Util
{
    public static class TemplateUtil
    {
        static TemplateUtil()
        {
            _initNVelocity();
        }

        private static VelocityEngine _velocity = null;
        
        private static void _initNVelocity()
        {
            _velocity = new VelocityEngine();
            ExtendedProperties props = new ExtendedProperties();

            _velocity.Init(props);
        }
       

        /// <summary>
        /// This will parse a string based on the nVelocity templating-engine.  For reference, check out the reference guide on:
        /// http://www.castleproject.org/others/nvelocity/improvements.html#fancyloops and http://www.castleproject.org/others/nvelocity/usingit.html
        /// </summary>
        /// <param name="templateData">The string data containing the template and the tags to be parsed</param>
        /// <param name="data">An object containing data which will be used as the context data.  Will be called 'data'.  Optional</param>
        /// <param name="additionalValues">A hashtable of any additional values required.  </param>
        /// <returns>The processed string</returns>
        public static  string ParseTemplateUsingNVelocity(string templateData, object data, Hashtable additionalValues = null)
        {
            string result = null;
            if (templateData != null)
            {
               



                VelocityContext context = new VelocityContext(additionalValues);
                if (data != null)
                {
                    context.Put("data", data);
                }

                StringWriter writer = new StringWriter();
                _velocity.Evaluate(context, writer, null, templateData);

                result= writer.GetStringBuilder().ToString();
                
            }
            return result;
        }


    }
}
