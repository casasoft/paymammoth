﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel;
using System.Web.UI.WebControls;
using System.Collections;
using CS.General_v3.Classes.Attributes;
namespace CS.General_v3.Util
{

    //add using System.ComponentModel, use [Description("")] to specify description;

    public static class EnumUtils
    {
        /// <summary>
        /// Returns an enum, as a list of CS.General_v3.Classes.ListItem.  
        /// </summary>
        /// <typeparam name="T">Type of enum</typeparam>
        /// <param name="func">Function that converts the enum ordinal to a string</param>
        /// <returns>The list item collection</returns>
        public static List<CS.General_v3.Classes.ListItem> EnumToListOfMyListItems(Type T)
        {
            List<CS.General_v3.Classes.ListItem> list = new List<CS.General_v3.Classes.ListItem>();
            string[] names = Enum.GetNames(T);
            Array values = Enum.GetValues(T);
            for (int i = 0; i < names.Length; i++)
            {
                CS.General_v3.Classes.ListItem item = new CS.General_v3.Classes.ListItem(names[i], values.GetValue(i));
                list.Add(item);

            }
            list.Sort((item1, item2) => (item1.Text.CompareTo(item2.Text)));
            return list;

        }
        public static string GetEnumName(Type t, object oValue)
        {
            int value = (int)oValue;
            string[] names = Enum.GetNames(t);
            Array values = Enum.GetValues(t);
            for (int i = 0; i < values.Length; i++)
            {
                int v = (int)values.GetValue(i);
                if (v == value)
                {
                    return ConvertEnumNameToString(names[i]);
                }
            }
            return "";

        }

        public static IEnumerable<T> ConvertToIEnumerable<T>(Type enumType)
        {
            List<T> resultList = new List<T>();
            var result = Enum.GetValues(enumType).Cast<T>();
            resultList = result.ToList();
            return resultList;
        }

        private static string ConvertEnumNameToString(string s)
        {
            s = s.Replace("_", " ");
            s = s.Replace("-", " ");
            return s;
        }
        private class ENUM_VALUE
        {
            public string Name { get; set; }
            public int Value { get; set; }
        }

        public static string GetCountryFullName(this Enums.ISO_ENUMS.Country_ISO3166 country)
        {
            return EnumUtils.StringValueOf(country, true);
        }

        public static int GetCountryCallingCode(this Enums.ISO_ENUMS.Country_ISO3166 country)
        {
            return CS.General_v3.Util.Data.GetCountryCallingCode(country);
        }
        public static string GetCountry2LetterCode(this Enums.ISO_ENUMS.Country_ISO3166 country)
        {
            return Enums.ISO_ENUMS.Country_ISO3166_To2LetterCode(country);
        }
        public static string GetCountry3LetterCode(this Enums.ISO_ENUMS.Country_ISO3166 country)
        {
            return Enums.ISO_ENUMS.Country_ISO3166_To3LetterCode(country);
        }
        public static IList<string> GetListOfEnumNames(Type enumType, Enums.ENUM_SORT_BY sortBy = Enums.ENUM_SORT_BY.NameAscending)
        {
            List<string> list = new List<string>();
            var values = GetListOfEnumValues(enumType, sortBy);
            foreach (Enum value in values)
            {
                string s = StringValueOf(value);
                list.Add(s);

            }
            return list;
        }

        public static List<T> GetListOfEnumsByValuesFromString<T>(string s) where T : struct
        {
            return GetListOfEnumsByValuesFromString(typeof(T), s).Cast<T>().ToList();
        }
        public static List<TEnum> GetListOfEnumsFromListOfStringValues<TEnum>(IEnumerable<string> values) where TEnum : struct, IConvertible
        {
            List<TEnum> enums = new List<TEnum>();
            if (values != null)
            {
                foreach (string item in values)
                {
                    TEnum? value = CS.General_v3.Util.EnumUtils.GetEnumByStringValueNullable<TEnum>(item);
                    if (value.HasValue)
                    {
                        enums.Add(value.Value);
                    }
                }
            }
            return enums;
        }
        public static List<Enum> GetListOfEnumsByValuesFromString(Type enumType, string s)
        {
            List<Enum> list = new List<Enum>();
            if (!string.IsNullOrWhiteSpace(s))
            {
                string[] tokens = s.Split(new string[] {",", "|"}, StringSplitOptions.RemoveEmptyEntries);
                foreach (var t in tokens)
                {
                    Enum enumValue = GetEnumByStringValueNullable(enumType, t);
                    if (enumValue != null)
                    {
                        list.Add(enumValue);
                    }
                }
            }
            return list;
        }
        private static int enumPriorityComparer<TEnum>(TEnum itemA, TEnum itemB)
        {
            return PriorityValueOf(itemA).CompareTo(PriorityValueOf(itemB));
        }
        private static int enumStringValueComparerAsc(Enum itemA, Enum itemB)
        {
            return StringValueOf(itemA).CompareTo(StringValueOf(itemB));
        }
        private static int enumStringValueComparerAsc<TEnum>(TEnum itemA, TEnum itemB)
        {
            return StringValueOf(itemA).CompareTo(StringValueOf(itemB));
        }
        public static List<Enum> GetListOfEnumValues(Type enumType, Enums.ENUM_SORT_BY sortBy = Enums.ENUM_SORT_BY.NameAscending)
        {
            List<Enum> list = new List<Enum>();
            bool allowsNullableValue = false;

            if (CS.General_v3.Util.ReflectionUtil.CheckIfTypeIsNullable(enumType))
            {
                allowsNullableValue = true;
                enumType = enumType.GetGenericArguments()[0];
            }


            Array values = Enum.GetValues(enumType);
            for (int i = 0; i < values.Length; i++)
            {
                var val = values.GetValue(i);
                list.Add((Enum)val);
            }
            if (sortBy == Enums.ENUM_SORT_BY.NameAscending || sortBy == Enums.ENUM_SORT_BY.NameDescending)
            {
                list.Sort((itemA, itemB) => enumStringValueComparerAsc(itemA, itemB) * (sortBy == Enums.ENUM_SORT_BY.NameAscending ? 1 : -1));
            }
            else if (sortBy == Enums.ENUM_SORT_BY.PriorityAttributeValue)
            {
                //Sort by Priority, Title Ascending
                list = CS.General_v3.Util.ListUtil.SortByMultipleComparers(list, enumPriorityComparer, enumStringValueComparerAsc).ToList();
            }


            if (allowsNullableValue)
            {
                //2012/feb/08 - this was removed (Karl) as it was thought that it doesnt make sense to add the null automatically
                //list.Insert(0, null);
            }


            return list;
        }
        public static List<TEnum> GetListOfEnumValues<TEnum>(Type enumType, Enums.ENUM_SORT_BY sortBy = Enums.ENUM_SORT_BY.NameAscending)
        {
            List<TEnum> list = new List<TEnum>();
            bool allowsNullableValue = false;
            
            if (CS.General_v3.Util.ReflectionUtil.CheckIfTypeIsNullable(enumType))
            {
                allowsNullableValue = true;
                enumType = enumType.GetGenericArguments()[0];
            }


            Array values = Enum.GetValues(enumType);
            for (int i = 0; i < values.Length; i++)
            {
                var val = values.GetValue(i);
                list.Add((TEnum)val);
            }
            if (sortBy == Enums.ENUM_SORT_BY.NameAscending || sortBy == Enums.ENUM_SORT_BY.NameDescending)
            {
                list.Sort((itemA, itemB) => enumStringValueComparerAsc(itemA, itemB) * (sortBy == Enums.ENUM_SORT_BY.NameAscending ? 1 : -1));
            }
            else if (sortBy == Enums.ENUM_SORT_BY.PriorityAttributeValue)
            {
                //Sort by Priority, Title Ascending
                list = CS.General_v3.Util.ListUtil.SortByMultipleComparers<TEnum>(list, enumPriorityComparer, enumStringValueComparerAsc).ToList();
            }


            if (allowsNullableValue)
            {
                //2012/feb/08 - this was removed (Karl) as it was thought that it doesnt make sense to add the null automatically
                //list.Insert(0, null);
            }


            return list;
        }

        public static List<TEnumType> GetListOfEnumValues<TEnumType>(Enums.ENUM_SORT_BY sortBy = Enums.ENUM_SORT_BY.NameAscending)
            where TEnumType : struct
        {
            var generalLIst = GetListOfEnumValues(typeof(TEnumType), sortBy);

            List<TEnumType> list = new List<TEnumType>();
            foreach (var item in generalLIst)
            {
                
                list.Add((TEnumType)(object)item);
            }
            return list;
        }
        
        public static ListItemCollection GetListItemCollectionFromEnum(Type enumType, string blankItemOnTopText = null, Enums.ENUM_SORT_BY sortBy = Enums.ENUM_SORT_BY.NameAscending, bool addSpacesToCamelCasedName = false)
        {
            ListItem blankItem = null;
            if (blankItemOnTopText != null)
            {
                blankItem = new ListItem(blankItemOnTopText);
            }
            return GetListItemCollectionFromEnum(enumType, blankItem, sortBy, addSpacesToCamelCasedName);
        }

        public static ListItemCollection GetListItemCollectionFromEnum<TEnumType>(ListItem blankItemOnTop = null, Enums.ENUM_SORT_BY sortBy = Enums.ENUM_SORT_BY.NameAscending, bool addSpacesToCamelCasedName = false, TEnumType? selectedValue = null, IEnumerable<TEnumType> excludeItems = null) where TEnumType : struct, IConvertible
        {
            Type enumType = typeof(TEnumType);
            long? selValue = null;
            if (selectedValue.HasValue) selValue = (Convert.ToInt64(selectedValue.Value));

            List<long> excludeItemsList = excludeItems == null ? null : excludeItems.ToList().ConvertAll(x => (Convert.ToInt64(x)));
            return GetListItemCollectionFromEnum(enumType, blankItemOnTop, sortBy, addSpacesToCamelCasedName, selValue, excludeItemsList);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="enumType"></param>
        /// <param name="blankItemOnTopValue"></param>
        /// <param name="sortItemsAlphabetically">If you have PriorityAttribute with each item, that will take precedence</param>
        /// <param name="addSpacesToCamelCasedName"></param>
        /// <returns></returns>
        public static ListItemCollection GetListItemCollectionFromEnum(Type enumType, ListItem blankItemOnTop = null,
            Enums.ENUM_SORT_BY sortBy = Enums.ENUM_SORT_BY.NameAscending, bool addSpacesToCamelCasedName = false, long? selectedValue = null, IEnumerable<long> excludeItems = null)
        {
            ListItemCollection lc = new ListItemCollection();
            IList<string> names = GetListOfEnumNames(enumType, sortBy);
            List<Enum> values = GetListOfEnumValues(enumType, sortBy);
            //List<ENUM_VALUE> list = new List<ENUM_VALUE>();

            //for (int i = 0; i < names.Length; i++)
            //{
            //    string name = StringValueOf((Enum)values.GetValue(i), addSpacesToCamelCasedName: addSpacesToCamelCasedName);
            //    int val = (int)values.GetValue(i);
            //    list.Add(new ENUM_VALUE() { Name = name, Value = val});
            //}
            //if (sortItemsAlphabetically)
            //{
            //    list.Sort((v1, v2) => (v1.Name.CompareTo(v2.Name)));
            //}
            for (int i = 0;i<values.Count;i++)
            {
                

                long value = (Convert.ToInt64(values[i]));
                if (excludeItems == null || !excludeItems.Contains(value))
                {
                    var li = new ListItem(names[i], value.ToString());
                    if (selectedValue.HasValue && value == selectedValue.Value)
                    {
                        li.Selected = true;
                    }
                    lc.Add(li);
                }
            }
            if (blankItemOnTop != null)
            {
                lc.Insert(0, blankItemOnTop);
            }
                        
            return lc;
        }

        public static string GetEnumValueName(Enum value, bool addSpacesToCamelCasedName = false)
        {
            return _getEnumValueName(value, addSpacesToCamelCasedName);
        }

        private static string _getEnumValueName(object value, bool addSpacesToCamelCasedName)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            string s = value.ToString();
            if (addSpacesToCamelCasedName)
                s = CS.General_v3.Util.Text.AddSpacesToCamelCasedText(s);
            return s;
        }

        public static string GetEnumValueName<T>(T value, bool addSpacesToCamelCasedName = false            )
            where T: struct
        {
            return _getEnumValueName(value, addSpacesToCamelCasedName);
        }
        /// <summary>
        /// Return the string value of an Enum if the DescriptionAttribute tag is defined (requires System.ComponentModel)
        /// 
        ///[DescriptionAttribute("Food")]
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string StringValueOf(this Enum value, bool addSpacesToCamelCasedName = false, bool getFromDescriptionAttributeIfAvailable = true)
        {
            return _StringValueOf(value, addSpacesToCamelCasedName, getFromDescriptionAttributeIfAvailable);
            
        }
        public static int PriorityValueOf<TEnum>(this TEnum value, int defaultValue = 0)
        {
            return PriorityValueOfNullable(value) ?? defaultValue;
        }
        public static int? PriorityValueOfNullable<TEnum>(this TEnum value)
        {
            string s = null;
            if (value != null)
            {
                FieldInfo fi = value.GetType().GetField(value.ToString());

                if (fi != null)
                {
                    PriorityAttribute[] attributes = (PriorityAttribute[])fi.GetCustomAttributes(typeof(PriorityAttribute), false);
                    if (attributes.Length > 0)
                    {
                        return attributes[0].Priority;
                    }
                }
                
            }
            return null;
        }
        /// <summary>
        /// Get the conversion ratio of a measurement unit to its base
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double? UnitRatioOfNullable<TEnum>(this TEnum value)
        {
            string s = null;
            if (value != null)
            {
                FieldInfo fi = value.GetType().GetField(value.ToString());

                if (fi != null)
                {
                    UnitAttribute[] attributes = (UnitAttribute[])fi.GetCustomAttributes(typeof(UnitAttribute), false);
                    if (attributes.Length > 0)
                    {
                        return attributes[0].Ratio;
                    }
                }

            }
            return null;
        }/// <summary>
        /// Get the conversion ratio of a measurement unit to its base
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetUnitTextSuffix<TEnum>(this TEnum value)
        {
            string s = null;
            if (value != null)
            {
                FieldInfo fi = value.GetType().GetField(value.ToString());

                if (fi != null)
                {
                    UnitAttribute[] attributes = (UnitAttribute[])fi.GetCustomAttributes(typeof(UnitAttribute), false);
                    if (attributes.Length > 0)
                    {
                        return attributes[0].UnitText;
                    }
                }

            }
            return null;
        }
        private static string _StringValueOf(object value, bool addSpacesToCamelCasedName, bool getFromDescriptionAttributeIfAvailable)
        {
            string s = null;
            if (value != null)
            {
                if (getFromDescriptionAttributeIfAvailable)
                {
                    FieldInfo fi = value.GetType().GetField(value.ToString());

                    if (fi != null)
                    {
                        DescriptionAttribute[] attributes = (DescriptionAttribute[]) fi.GetCustomAttributes(typeof (DescriptionAttribute), false);
                        if (attributes.Length > 0)
                        {
                            return attributes[0].Description;
                        }
                    }
                }
                s = _getEnumValueName(value, addSpacesToCamelCasedName);
            }
            return s;
        }
        /// <summary>
        /// Return the string value of an Enum if the DescriptionAttribute tag is defined (requires System.ComponentModel)
        /// 
        ///[DescriptionAttribute("Food")]
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string StringValueOf<T>(T value, bool addSpacesToCamelCasedName = false, bool getFromDescriptionAttributeIfAvailable = true)
            
        {
            return _StringValueOf(value, addSpacesToCamelCasedName, getFromDescriptionAttributeIfAvailable);
        }


        public static T GetEnumByAttributeValue<T>(Type attribType, string attribValue)
            where T : struct
        {
            var enumVal = GetEnumByAttributeValue(typeof(T), attribType, attribValue);
            return (T)(object)enumVal;
            
        }
          public static Enum GetEnumByAttributeValue(Type enumType, Type attribType, string attribValue)
        {
            Array enumValues = Enum.GetValues(enumType);
            for (int i = 0; i < enumValues.Length; i++)
            {
                Enum enumValue = (Enum)enumValues.GetValue(i);
                var attrib = GetAttributeFromEnumValue(enumValue, attribType);
                if (string.Compare(attrib.ToString(), attribValue, 0) == 0)
                    return enumValue;
            }
            return null;
        }
        /// <summary>
        /// Return the string value of an Enum if the DescriptionAttribute tag is defined (requires System.ComponentModel)
        /// 
        ///[DescriptionAttribute("Food")]
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Attribute GetAttributeFromEnumValue(Enum value, Type attribType)
        {
            return ReflectionUtil.GetAttributeFromEnumValue(value, attribType);
        }

      

        /// <summary>
        /// Return the string value of an Enum if the DescriptionAttribute tag is defined (requires System.ComponentModel)
        /// 
        ///[DescriptionAttribute("Food")]
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T GetAttributeFromEnumValue<T>(Enum value)
            where T : Attribute
        {
            Attribute attrib =  GetAttributeFromEnumValue(value, typeof(T));
            return (T)attrib;
            /*

            FieldInfo fi = value.GetType().GetField(value.ToString());
            if (fi != null)
            {
                T[] attributesList = (T[])fi.GetCustomAttributes(T, false);
                if (attributesList.Length > 0)
                {
                    return attributesList[0];
                }
            }
            return default(T);*/
        }

        /// <summary>
        /// Return the respective Enum which matches the DescriptionAttribute tag
        /// </summary>
        /// <param name="value"></param>
        /// <param name="enumType"></param>
        /// <returns></returns>
        public static Enum EnumValueOf(Type type, string value, bool convertEnumValuesForRouteData = false)
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(type, "Type must be filled in! [EnumsUtil.EnumValueOf]");

            Enum result = null;
            if (value != null)
            {
                string[] names = Enum.GetNames(type);
                Array values = Enum.GetValues(type);
                int i = 0;
                foreach (Enum enumValue in values)
                {
                    //string name = names[i++];
                    //Enum enumValue = (Enum)Enum.Parse(type, name);
                    string enumTitle = StringValueOf(enumValue);
                    if (convertEnumValuesForRouteData)
                    {
                        enumTitle = CS.General_v3.Util.RoutingUtil.ConvertStringForRouteUrl(enumTitle);
                    }

                    if (string.Compare(enumTitle, value, true) == 0 || string.Compare(GetEnumValueName(enumValue), value, true) == 0)
                    {
                        result = enumValue;
                        break;
                    }
                    else if (value == ((int) (object) enumValue).ToString())
                    {
                        result = enumValue;
                        break;
                    }
                }
            }
            return result;
            //throw new ArgumentException("The string is not a description or value of the specified enum.");
        }
        public static T EnumValueOf<T>(string value, bool convertEnumValuesForRouteData = false, T defaultValue = default(T)) where T : struct, IConvertible
        {
            T? v = EnumValueNullableOf<T>(value, convertEnumValuesForRouteData);
            if (v == null)
            {
                return defaultValue;
            }
            else
            {
                return (T)(object)v;
            }
        }
        public static bool CompareEnumValue(object value, Enum enumValue)
        {
            if (value != null)
            {
                string strValue = value.ToString();
                if ((Convert.ToInt32(enumValue)).ToString() == strValue)
                {
                    return true;
                }
                else if (StringValueOf(enumValue) == strValue) {
                    return true;
                }

            }
            return false;
        }

        public static T? EnumValueNullableOf<T>(string value, bool convertEnumValuesForRouteData = false) where T : struct, IConvertible
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("T must be an enumerated type");
            }
            //Done by Mark - nispera mhix oxxata :)
            Enum result = EnumValueOf(typeof(T), value, convertEnumValuesForRouteData);
            if (result != null)
            {
                return (T)(object)result;
            }
            else
            {
                return null;
            }
        }
        public static bool IsValidEnum(Type enumType, object value)
        {
            return Enum.IsDefined(enumType, value);

        }

        /// <summary>
        /// Returns an enum, based on the string value.  E.g, if the enum type is DayOfWeek, and the string value is "Monday", that would count. It will even return
        /// enums where the string matches the integer value of the enumeration, e.g "1".
        /// </summary>
        /// <param name="type"></param>
        /// <param name="stringValue"></param>
        /// <returns></returns>
        public static T GetEnumByStringValue<T>(string stringValue, T defaultValue) where T : struct, IConvertible
        {
            return (T)(object)GetEnumByStringValue(typeof(T), stringValue, (Enum)(object)defaultValue);
            
        }
        /// <summary>
        /// Returns an enum, based on the string value.  E.g, if the enum type is DayOfWeek, and the string value is "Monday", that would count. It will even return
        /// enums where the string matches the integer value of the enumeration, e.g "1".
        /// </summary>
        /// <param name="type"></param>
        /// <param name="stringValue"></param>
        /// <returns></returns>
        public static Enum GetEnumByStringValue(Type type, string stringValue, Enum defaultValue)
        {
            var val = GetEnumByStringValueNullable(type, stringValue);
            if (val == null)
            {
                return defaultValue;
            }
            else
            {
                return val;
            }
        }
        public static T? GetEnumByStringValueNullable<T>(string stringValue) where T : struct, IConvertible
        {
            return (T?)(object)GetEnumByStringValueNullable(typeof(T), stringValue);
        }

        public static Enum GetEnumByStringValueNullable(Type type, string stringValue)
        {
            if (stringValue == null) stringValue = "";
            stringValue = stringValue.Trim();
            var enumValues = GetListOfEnumValues(type);
            for (int i = 0; i < enumValues.Count; i++)
            {
                var enumvalue = enumValues[i];
                int numValue = Convert.ToInt32(enumvalue);
                string s = StringValueOf(enumvalue);
                if (string.Compare(s, stringValue, true) == 0 || stringValue.Trim() == numValue.ToString())
                    return enumvalue;
            }
            return null;
        }


        #region Old Code


        //public static string StringDataTypeToText(STRING_DATA_TYPE dataType)
        //{

        //    return CS.General_v3.Util.EnumUtils.StringValueOf(dataType);

        //}
        //public static STRING_DATA_TYPE StringDataTypeFromText(string s)
        //{
        //    s = s.ToLower();
        //    switch (s)
        //    {
        //        case "singleline": return STRING_DATA_TYPE.SingleLine;
        //        case "email": return STRING_DATA_TYPE.Email;
        //        case "multiline": return STRING_DATA_TYPE.MultiLine;
        //        case "password": return STRING_DATA_TYPE.Password;
        //        case "website": return STRING_DATA_TYPE.Website;
        //    }
        //    throw new InvalidOperationException("Invalid string input");

        //}

        //public static string SortTypeToText(SORT_TYPE value)
        //{
        //    switch (value)
        //    {
        //        case SORT_TYPE.Ascending: return "Ascending";
        //        case SORT_TYPE.Descending: return "Descending";
        //    }
        //    return "";

        //}
        //public static string SortTypeForSQL(SORT_TYPE value)
        //{
        //    switch (value)
        //    {
        //        case SORT_TYPE.None: return "";
        //        case SORT_TYPE.Ascending: return "Asc";
        //        case SORT_TYPE.Descending: return "Desc";
        //    }
        //    return "";

        //}
        //public static SORT_TYPE SortTypeFromText(string s)
        //{
        //    s = s.ToLower();
        //    SORT_TYPE sortType = SORT_TYPE.Ascending;
        //    if (s == "desc" || s == "descending")
        //        sortType = SORT_TYPE.Descending;
        //    return sortType;
        //}
        //public static string SortTypeToText(Enum value)
        //{
        //    return SortTypeToText((SORT_TYPE)value);
        //}
        //public static void FillMyWebComboBox(Type t, bool addBlankToTop, CS.General_v3.Controls.WebControls.Common.MyDropDownList cmb, bool clearCmbItems)
        //{
        //    if (clearCmbItems)
        //        cmb.Items.Clear();
        //    cmb.Items.AddRange(EnumToListItemArray(t, addBlankToTop));
        //}
        //public static void FillMyWebComboBox<T>(EnumToString func, CS.General_v3.Controls.WebControls.Common.MyDropDownList cmb)
        //{
        //    FillMyWebComboBox<T>(func, cmb, false, "", "");
        //}
        //public static void FillMyWebComboBox<T>(EnumToString func, CS.General_v3.Controls.WebControls.Common.MyDropDownList cmb, bool addEmpty, string emptyText, string emptyValue)
        //{
        //    cmb.Items.Clear();
        //    List<ListItem> list = EnumToListItems<T>(func, addEmpty, emptyText, emptyValue);
        //    list.Sort((s1, s2) => (s1.Text.CompareTo(s2.Text)));
        //    for (int i = 0; i < list.Count; i++)
        //    {
        //        cmb.Items.Add(list[i]);
        //    }


        //}

        //public static void FillMyComboBox<T>(EnumToString func, CS.General_v3.Controls.WebControls.Common.MyDropDownList cmb, bool addEmpty, string emptyText, string emptyValue)
        //{
        //    FillMyWebComboBox<T>(func, cmb, addEmpty, emptyText, emptyValue);
        //}
        //public static void FillMyComboBox<T>(EnumToString func, CS.General_v3.Controls.WebControls.Common.MyDropDownList cmb)
        //{
        //    FillMyWebComboBox<T>(func, cmb);
        //}

        ///// <summary>
        ///// A delegate that refers to function that converts an integer ordinal to a string value
        ///// </summary>
        ///// <param name="value"></param>
        ///// <returns></returns>
        //


        ///// <summary>
        ///// Returns an enum, as a list of CS.General_v3.Classes.ListItem.  
        ///// </summary>
        ///// <typeparam name="T">Type of enum</typeparam>
        ///// <param name="func">Function that converts the enum ordinal to a string</param>
        ///// <returns>The list item collection</returns>
        //public static List<CS.General_v3.Classes.ListItem> EnumToListOfMyListItems(Type T)
        //{
        //    List<CS.General_v3.Classes.ListItem> list = new List<CS.General_v3.Classes.ListItem>();
        //    string[] names = Enum.GetNames(T);
        //    Array values = Enum.GetValues(T);
        //    for (int i = 0; i < names.Length; i++)
        //    {
        //        CS.General_v3.Classes.ListItem item = new CS.General_v3.Classes.ListItem(names[i], values.GetValue(i));
        //        list.Add(item);

        //    }
        //    list.Sort((item1, item2) => (item1.Text.CompareTo(item2.Text)));
        //    return list;

        //}
        /// <summary>
        /// Returns an enum, as a list of CS.General_v3.Classes.ListItem.  
        /// </summary>
        /// <typeparam name="T">Type of enum</typeparam>
        /// <param name="func">Function that converts the enum ordinal to a string</param>
        /// <returns>The list item collection</returns>
        public static List<CS.General_v3.Classes.ListItem> EnumToListOfMyListItems<T>(Func<Enum,string> func)
        {
            return EnumToListOfMyListItems<T>(func, false, "");
        }
        /// <summary>
        /// Returns an enum, as a list of CS.General_v3.Classes.ListItem.  
        /// </summary>
        /// <typeparam name="T">Type of enum</typeparam>
        /// <param name="func">Function that converts the enum ordinal to a string</param>
        /// <param name="addEmpty">Whether to add an empty value</param>
        /// <param name="emptyText">The empty value text</param>
        /// <returns>The list item collection</returns>
        public static List<CS.General_v3.Classes.ListItem> EnumToListOfMyListItems<T>(Func<Enum,string> func, bool addEmpty, string emptyText)
        {
            return EnumToListOfMyListItems<T>(func, addEmpty, emptyText, emptyText);
        }
        /// <summary>
        /// Returns an enum, as a list of CS.General_v3.Classes.ListItem.  
        /// </summary>
        /// <typeparam name="T">Type of enum</typeparam>
        /// <param name="func">Function that converts the enum ordinal to a string</param>
        /// <param name="addEmpty">Whether to add an empty value</param>
        /// <param name="emptyText">The empty value text</param>
        /// <returns>The list item collection</returns>
        public static List<CS.General_v3.Classes.ListItem> EnumToListOfMyListItems<T>(Func<Enum, string> func, bool addEmpty, string emptyText, string emptyValue)
        {
            List<ListItem> list = EnumToListItems<T>(func, addEmpty, emptyText, emptyValue);
            List<CS.General_v3.Classes.ListItem> result = new List<CS.General_v3.Classes.ListItem>();
            for (int i = 0; i < list.Count; i++)
            {
                result.Add(new CS.General_v3.Classes.ListItem(list[i].Text, list[i].Value));
            }
            return result;
        }
        ///// <summary>
        ///// Returns an enum, as a list of strings.  
        ///// </summary>
        ///// <typeparam name="T">Type of enum</typeparam>
        ///// <param name="func">Function that converts the enum ordinal to a string</param>
        ///// <returns>The list of strings</returns>
        //public static List<string> EnumToListOfStrings<T>(EnumToString func)
        //{

        //    List<string> list = new List<string>();

        //    foreach (Enum element in Enum.GetValues(typeof(T)))
        //    {

        //        string s = func(element);
        //        if (!string.IsNullOrEmpty(s))
        //        {
        //            list.Add(s);
        //        }
        //    }
        //    return list;
        //}

        /// <summary>
        /// Returns an enum, as a list of ListItems.  
        /// </summary>
        /// <typeparam name="T">Type of enum</typeparam>
        /// <param name="func">Function that converts the enum ordinal to a string</param>
        /// <returns>The list item collection</returns>
        public static List<ListItem> EnumToListItems<T>(Func<Enum,string> func)
        {

            List<ListItem> list = new List<ListItem>();

            foreach (Enum element in Enum.GetValues(typeof(T)))
            {

                string s = func(element);
                if (!string.IsNullOrEmpty(s))
                {
                    list.Add(new ListItem(s, (Convert.ToInt32(element).ToString())));
                }
            }
            return list;
        }
        /// <summary>
        /// Returns an enum, as a list of ListItems.  
        /// </summary>
        /// <typeparam name="T">Type of enum</typeparam>
        /// <param name="func">Function that converts the enum ordinal to a string</param>
        /// <param name="addEmpty">Whether to add an empty value</param>
        /// <param name="emptyText">The empty value text</param>
        /// <returns>The list item collection</returns>
        public static List<ListItem> EnumToListItems<T>(Func<Enum, string> func, bool addEmpty, string emptyText)
        {
            return EnumToListItems<T>(func, addEmpty, emptyText, emptyText);
        }
        /// <summary>
        /// Returns an enum, as a list of ListItems.  
        /// </summary>
        /// <typeparam name="T">Type of enum</typeparam>
        /// <param name="func">Optional. Function that converts the enum ordinal to a string</param>
        /// <param name="addEmpty">Whether to add an empty value</param>
        /// <param name="emptyText">The empty value text</param>
        /// <returns>The list item collection</returns>
        public static List<ListItem> EnumToListItems<T>(Func<Enum, string> func, bool addEmpty, string emptyText, string emptyValue)
        {
            if (emptyText == null)
                emptyText = "";
            List<ListItem> list = new List<ListItem>();
            if (addEmpty)
                list.Add(new ListItem(emptyText, emptyValue));

            foreach (Enum element in Enum.GetValues(typeof(T)))
            {
                string s = null;
                if (func != null)
                    s = func(element);
                else
                    s = EnumUtils.StringValueOf(element);
                if (!string.IsNullOrEmpty(s))
                {
                    list.Add(new ListItem(s, (Convert.ToInt32(element).ToString())));
                }
            }
            return list;
        }
        ///// <summary>
        ///// Returns an enum, as a list item collection.  
        ///// </summary>
        ///// <typeparam name="T">Type of enum</typeparam>
        ///// <param name="emptyText">The empty value text</param>
        ///// <returns>The list item collection</returns>
        //public static ListItemCollection EnumToListItemCollection<T>(EnumToString func)
        //{
        //    return EnumToListItemCollection<T>(func, false, null);
        //}
        ///// <summary>
        ///// Returns an enum, as a list item collection.  
        ///// </summary>
        ///// <typeparam name="T">Type of enum</typeparam>
        ///// <param name="func">Function that converts the enum ordinal to a string</param>
        ///// <param name="addEmpty">Whether to add an empty value</param>
        ///// <param name="emptyText">The empty value text</param>
        ///// <returns>The list item collection</returns>
        //public static ListItemCollection EnumToListItemCollection<T>(EnumToString func, bool addEmpty, string emptyText)
        //{
        //    List<ListItem> list = EnumToListItems<T>(func, addEmpty, emptyText);
        //    ListItemCollection coll = new ListItemCollection();
        //    for (int i = 0; i < list.Count; i++)
        //    {
        //        coll.Add(list[i]);
        //    }
        //    return coll;
        //}
        ///// <summary>
        ///// Returns the enum value with the name specified
        ///// </summary>
        ///// <param name="enumType"></param>
        ///// <param name="name"></param>
        ///// <returns></returns>
        //public static object GetEnumValue(Type enumType, string name)
        //{
        //    if (name == null)
        //        name = "";
        //    Array values = Enum.GetValues(enumType);

        //    string[] names = Enum.GetNames(enumType);
        //    name = name.ToLower().Trim();
        //    name = name.Replace(" ", "");
        //    for (int i = 0; i < names.Length; i++)
        //    {
        //        string s = names[i].ToLower().Trim().Replace(" ", "");
        //        if (name == s)
        //        {
        //            return values.GetValue(i);
        //        }
        //    }
        //    return 0;
        //}

        //private class ENUM_VALUE
        //{
        //    public string Name { get; set; }
        //    public int Value { get; set; }
        //}

        //public static ListItem[] EnumToListItemArray(Type t, bool addBlankOnTop)
        //{
        //    ListItemCollection list = EnumToListItemCollection(t, addBlankOnTop);
        //    ListItem[] array = new ListItem[list.Count];
        //    for (int i = 0; i < list.Count; i++)
        //    {
        //        array[i] = list[i];
        //    }
        //    return array;
        //}
        //public static ListItemCollection EnumToListItemCollection(Type t, bool addBlankOnTop)
        //{
        //    ListItemCollection lc = new ListItemCollection();
        //    string[] names = Enum.GetNames(t);
        //    Array values = Enum.GetValues(t);
        //    List<ENUM_VALUE> list = new List<ENUM_VALUE>();

        //    for (int i = 0; i < names.Length; i++)
        //    {
        //        list.Add(new ENUM_VALUE() { Name = ConvertEnumNameToString(names[i]), Value = (int)values.GetValue(i) });
        //    }
        //    list.Sort((v1, v2) => (v1.Name.CompareTo(v2.Name)));
        //    foreach (var v in list)
        //    {

        //        lc.Add(new ListItem(v.Name, v.Value.ToString()));
        //    }
        //    if (addBlankOnTop)
        //    {
        //        lc.Insert(0, new ListItem("", "0"));
        //    }


        //    return lc;

        //}



        #endregion

       

    }
}
