﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using CS.General_v3.Extensions;
using CS.General_v3.Classes.Interfaces.Hierarchy;

namespace CS.General_v3.Util
{
    public static class SEOUtil
    {
        public const int GOOGLE_DESC_LIMIT =150;
        private static int _stringLengthCompare(string a, string b)
        {
            int amtSpacesInA = Text.CountCharactersInString(a, " ");
            int amtSpacesInB = Text.CountCharactersInString(b, " ");

            if (amtSpacesInA > amtSpacesInB)
            {
                return 1;
            }
            else if (amtSpacesInA < amtSpacesInB)
            {
                return -1;
            }
            else
            {
                return a.CompareTo(b);
                
            }
            
        }
        private static List<string> _getKeywordsFromHierarchy(IHierarchy hierarchy, List<string> keywords, bool includeRootNode = false)
        {
            if (includeRootNode || hierarchy.GetParents().IsNotNullOrEmpty())
            {
                if (hierarchy != null)
                {
                    int currCount = keywords.Count;
                    for (int i = 0; i < currCount; i++)
                    {
                        keywords.Add(hierarchy.Title + " " + keywords[i]);
                    }
                    keywords.Add(hierarchy.Title);
                    if (hierarchy.GetParents().IsNotNullOrEmpty())
                    {
                        keywords = _getKeywordsFromHierarchy(hierarchy.GetParents().FirstOrDefault(), keywords);
                    }
                }
            }
            return keywords;
        }
        public static List<string> GetKeywordsFromHierarchy(IHierarchy hierarchy, bool includeRootNode = false)
        {
            var keywords = _getKeywordsFromHierarchy(hierarchy, new List<string>(), includeRootNode);
            keywords.Sort(_stringLengthCompare);
            return keywords;

        }
        /// <summary>
        /// Strip out the paragraph tags and get the content of them as description
        /// </summary>
        /// <returns></returns>
        public static string GetDescriptionFromHTMLParagraphTexts(string html, int limit = GOOGLE_DESC_LIMIT)
        {
            if (!string.IsNullOrEmpty(html))
            {
                html = CS.General_v3.Util.Text.ConvertHTMLToPlainText(html, " ", " ");
                if (html.Length > limit)
                {
                    html = html.Substring(0, limit);
                }
            }
            return html;
            Regex r = new Regex("<p>(.*?)</p>", RegexOptions.Singleline | RegexOptions.Multiline | RegexOptions.IgnoreCase);

            List<string> matches = RegexUtil.GetMatchesContent(html, r);

            

            if (matches != null && matches.Count > 0)
            {

                html = matches.Join(" ");
            }

            html = CS.General_v3.Util.Text.ConvertHTMLToPlainText(html, " ", " ");

            
            return html;

        }
    }
}
