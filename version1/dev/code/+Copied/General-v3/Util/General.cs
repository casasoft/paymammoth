﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
namespace CS.General_v3.Util
{
    public static class General
    {

        public static object Clone(object obj)
        {

            MemoryStream ms = new MemoryStream();

            BinaryFormatter bf = new BinaryFormatter();

            bf.Serialize(ms, obj);

            ms.Position = 0;

            object obj2 = bf.Deserialize(ms);

            ms.Close();

            return obj2;
        }

    }
}
