﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CS.General_v3.Util
{
    public static class SearchTextUtil
    {

        public static bool SearchKeywordsInSearchFields(string keywords, IEnumerable<string> searchFields, bool replaceDiacritics = true)
        {


            keywords = CS.General_v3.Util.Text.ConvertStringForSearch(keywords, stemmerToUse:null, replaceDiacritics: replaceDiacritics);

            string[] tokens = keywords.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

            List<string> dataToSearchOn = CS.General_v3.Util.ListUtil.GetListFromEnumerator<string>(searchFields);
            bool ok = true;
            for (int i = 0; i < tokens.Length; i++)
            {
                string keyword = tokens[i];
                bool keywordOk = false;
                foreach (var searchField in dataToSearchOn)
                {
                    if (Regex.IsMatch(searchField, keyword, RegexOptions.IgnoreCase))
                    {
                        keywordOk = true;
                        break;
                    }

                }
                if (!keywordOk)
                {
                    ok = false;
                    break;
                }
            }
            return ok;
        }
    }
}
