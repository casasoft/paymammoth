﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CS.General_v3.Util
{
    public static class HtmlUtil
    {
        public static string CleanHtmlTemplateForJavascript(string html)
        {
            html = StripAttributeFromHTML(html, "id");
            return html;
        }

        public static string StripAttributeFromHTML(string html, params string[] attributes)
        {
            string sAttributes = "";
            for (int i = 0; i < attributes.Length; i++)
            {
                if (i > 0)
                {
                    sAttributes += "|";
                }
                sAttributes += attributes[i];
            }


            Regex r = new Regex("(" + sAttributes + ")=(\"|')(.*?)(\"|')", RegexOptions.Multiline | RegexOptions.Singleline | RegexOptions.IgnoreCase);
            html = r.Replace(html, "");
            return html;
        }
        public static string GetTagNameFromControl(IAttributeAccessor control)
        {
            string tagName = null;
            if (control is HtmlGenericControl)
            {
                tagName = ((HtmlGenericControl) control).TagName;
            }
            else if (control is Control)
            {
                string html = CS.General_v3.Util.ControlUtil.RenderControl((Control)control);
                var m = Regex.Match(@"<(.*?)(\s.*?)?>", html, RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.Multiline);
                if (m.Success)
                {
                    tagName = m.Groups[0].Value;
                }
            }
            if (tagName != null) tagName = tagName.ToLower();
            return tagName;
        }
        public static bool IsControlOneOfTheseTags(IAttributeAccessor control, params string[] tags)
        {
            string tagName = GetTagNameFromControl(control);
            foreach (string tag in tags)
            {
                if (string.Compare(tagName, tag, true) == 0)
                {
                    return true;
                }
            }
            return false;
        }
        public static string GetAttributeValueFromHTML(string html, string attribute)
        {
            string result = null;
            if (Text.CheckIfStringArrayIsNotNullOrWhiteSpace(html, attribute))
            {
                Regex r = new Regex(attribute + @"=['""]?(.*?)(['""])?[\s>]",
                                    RegexOptions.Multiline | RegexOptions.Singleline | RegexOptions.IgnoreCase);
                var match = r.Match(html);
                if (match != null)
                {
                    if (match.Groups.Count >= 1)
                    {
                        result = match.Groups[1].Value;
                    }
                    else
                    {
                        result = match.Groups[0].Value;
                    }
                }
            }
            return result;
        }


        public static string StripTargetFromAnchor(string htmlContent)
        {
            return StripAttributeFromHTML(htmlContent, "target");
        }

        private static string AddBlankTargetToAllAnchors(string htmlContent)
        {
            string result = htmlContent;
            Regex r = new Regex(@"(?<=href="")(?<url>[^""]+)(?="")", RegexOptions.Multiline | RegexOptions.Singleline | RegexOptions.IgnoreCase);
            MatchCollection matches = r.Matches(htmlContent);
            int indexMoved = 0;
            string target = " target=\"_blank\" ";
            int targetSize = target.Length;
            foreach (Match m in matches)
            {
                int pos = m.Index + indexMoved;
                result = result.Insert(pos - 7, target);
                indexMoved = indexMoved + targetSize;
            }
            return result;
        }

        public static string ConvertAllAnchorTargetsToBlank(string htmlContent)
        {
            string tmp = StripTargetFromAnchor(htmlContent);
            return AddBlankTargetToAllAnchors(tmp);
        }
        public static bool IsHtml(string html)
        {
            if(html == null)
            {
                return false;
            }
            Regex r = new Regex(@"(<.*?>)|(&[#a-zA-Z0-9]+?;)", RegexOptions.Multiline | RegexOptions.IgnoreCase | RegexOptions.Singleline);
            var match = r.Match(html);
            return match.Success;
        }
    }
}
