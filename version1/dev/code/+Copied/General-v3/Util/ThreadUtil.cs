﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using log4net;

namespace CS.General_v3.Util
{
    public static class ThreadUtil
    {

        private static readonly ILog _log = LogManager.GetLogger(typeof(ThreadUtil));
        public static Thread CallMethodOnSeperateThread(ThreadStart methodToCall, string threadName, ThreadPriority priority = ThreadPriority.Normal)
        {
            //methodToCall();
            //return null;

            MethodToCall m = new MethodToCall(methodToCall);
            m.StackTrace = Environment.StackTrace;
            System.Threading.Thread t = new Thread(m.Start);
            if (threadName != null)
            {
                t.Name = threadName;
            }
            
            t.Priority = priority;
            t.Start();
            return t;
        }

        /// <summary>
        /// Pauses the current thread until an interrupt is called onit
        /// </summary>
        public static void Pause()
        {
            try
            {
                System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);
                
            }
            catch (ThreadInterruptedException ex)
            {
                //continue
                
            }

        }

        public static void Sleep(int milliseconds)
        {
           
            System.Threading.Thread.Sleep(milliseconds);

            

        }

       

        private class MethodToCall
        {
            private ThreadStart _start = null;
            public string StackTrace { get; set; }
            public void Start()
            {
                try
                {
                    _start();
                }
                catch (Exception ex)
                {

                    bool throwError = true;
                    if (CS.General_v3.Util.Other.IsInUnitTestingEnvironment && ex is ThreadAbortException)
                        throwError = false;

                    if (throwError)
                    {


                        StringBuilder sb = new StringBuilder();
                        sb.Append("FATAL ERROR DURING BACKGROUND THREAD EXECUTION - ");
                        if (_start != null && _start.Method != null)
                        {
                            string s = _start.Method.ToString();
                            if (_start.Method.DeclaringType != null)
                            {
                                s = _start.Method.DeclaringType.ToString() + "." + s;
                            }
                            sb.Append(s);
                        }
                        sb.AppendLine();
                        sb.AppendLine("Exception  : " + ex.Message + " (" + ex.GetType().FullName + ") [For full details check inner exception]");
                        sb.AppendLine("---------------");
                        sb.AppendLine("Method Caller Stack Trace: " + this.StackTrace);
                        sb.AppendLine("=================");
                        sb.AppendLine();
                        sb.AppendLine("Please note that this WILL STOP THE APPLICATION when using IIS. --> CHECK INNER EXCEPTION for more details <--");

                        CS.General_v3.Error.Reporter.ReportError(Enums.LOG4NET_MSG_TYPE.Fatal, sb.ToString(), ex, sendAsync: false);

                        if (!CS.General_v3.Util.Other.IsLocalTestingMachine ||
                            (CS.General_v3.Util.Other.IsLocalTestingMachine && !CS.General_v3.Settings.Others.DontThrowThreadStateExceptionsInLocalhost))
                        {
                            //throw this error only if NOT in localhost, or in localhost and the boolean is not set to true.
                            throw new ThreadStateException(sb.ToString(), ex);
                        }
                        else
                        {
                            _log.Fatal(sb.ToString(), ex);
                        }

                    }

                }
            }

            public MethodToCall(ThreadStart t)
            {
                _start = t;
            }
        }

        public delegate void GeneralCallBack();
        private class METHOD_CALLER
        {
            private System.Timers.Timer tmr = null;
            private bool justOnce = false;
            public METHOD_CALLER(bool justOnce)
            {
                this.justOnce = justOnce;
            }
            private GeneralCallBack generalCallBack;
            public void CallMethodAfter(GeneralCallBack callBack, int intervalMs)
            {
                tmr = new System.Timers.Timer(intervalMs);
                tmr.Elapsed +=new System.Timers.ElapsedEventHandler(tmr_Elapsed);
                generalCallBack = callBack;
                tmr.Start();
                
                
                
            }
            void  tmr_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
            {
                tmr.Stop();
 	            this.generalCallBack();
                if (justOnce)
                    tmr.Dispose();
                else
                    tmr.Start();

                
            }

        }
        public static void CallMethodAfter(GeneralCallBack callBack, int intervalMs, bool justOnce)
        {
            METHOD_CALLER caller = new METHOD_CALLER(justOnce);
            caller.CallMethodAfter(callBack, intervalMs);
        }

    }
}
