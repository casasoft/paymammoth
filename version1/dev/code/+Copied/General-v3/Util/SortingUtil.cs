﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace CS.General_v3.Util
{
    public static class SortingUtil
    {
        public static void QuickSort<T>(this IList<T> list, int index = 0, int? count = null)
            where T : IComparable<T>
        {

            if (count == null)
                count = list.Count - 1;
            Comparison<T> comparer = new Comparison<T>((x, y) => (x.CompareTo(y)));
            _quickSort(list, index, count.Value, comparer);
        }
        
        public static int IntegerComparer(int a, int b)
        {
            return a.CompareTo(b);
        }
        public static int StringComparer(string a, string b)
        {
            return string.Compare(a, b);
        }
        public static int DoubleComparer(double a, double b)
        {
            return a.CompareTo(b);
        }
        public static int DateComparer(DateTime a, DateTime b)
        {
            return a.CompareTo(b);
        }
        public static void QuickSort<T>(this IList<T> list,  Comparison<T> comparer, int index = 0, int? count = null)
        {
            if (count == null)
                count = list.Count;
            List<T> tmpList = new List<T>();

            
            int toIndex = index + count.Value;
            int i = index;
            while (i < list.Count && i < toIndex)
            {
                tmpList.Add(list[i]);
                i++;
            }
            tmpList.Sort(comparer);
            list.Clear();
            list.AddRange(tmpList);
            
            /*


            if (count == null)
                count = list.Count - 1;
            _quickSort(list, index, count.Value, comparer);*/
        }
      

        private static void _quickSort<T>(IList<T> list, int i, int j, Comparison<T> comparer)
        {
            if (i < j)
            {
                int q = _quickSort_Partition(list, i, j, comparer);
                _quickSort(list, i, q, comparer);
                _quickSort(list, q + 1, j, comparer);
            }
            
        }

        private static int _quickSort_Partition<T>(IList<T> a, int p, int r, Comparison<T> comparer)
        {
            
            T x = a[p];
            
            int i = p - 1;
            int j = r + 1;
            T tmp = default(T);
            while (true)
            {
                do
                {
                    j--;
                } while (comparer(a[j],x) == 1);
                do
                {
                    i++;
                } while (comparer(a[i],x) == -1);
                if (i < j)
                {
                    tmp = a[i];
                    a[i] = a[j];
                    a[j] = tmp;
                }
                else return j;
            }
        } 

    }
}
