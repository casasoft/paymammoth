using System;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.Web;

namespace CS.General_v3.Util
{
    public static class Forms
    {
        public static string GetFormVariableID(Control c)
        {
            return GetFormVariableID(c, '$');
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <param name="ClientIDSeparator"></param>
        /// <param name="ifStaticRemoveUpTillLastUnderscore">This is to keep compatible with previous versions which wasn't removing up till last underscore</param>
        /// <returns></returns>
        public static string GetFormVariableID(Control c, char ClientIDSeparator, bool ifStaticRemoveUpTillLastUnderscore = false)
        {
            string id = "";
            string controlID = c.ClientID;

            if (c.ClientIDMode == ClientIDMode.Static)
            {
                string parentID = c.Parent.ClientID;
                if (ifStaticRemoveUpTillLastUnderscore)
                {
                    int indexOfUnderscore = parentID.LastIndexOf("_");
                    if (indexOfUnderscore != -1)
                    {
                        parentID = parentID.Substring(0, indexOfUnderscore + 1);
                    }
                }
                controlID = parentID + controlID;
            }
            id = controlID.Replace("_", "$");

            Control curr = c;
            while (c != null)
            {
                if (!string.IsNullOrEmpty(c.ID))
                {
                    string s = c.ID.Replace("_", "$");
                    id = id.Replace(s, c.ID);
                }
                c = c.Parent;
            }
            return id;
            //this is old
            /*
            if (c.ClientID.Length != c.ID.Length)
                id = c.ClientID.Substring(0, c.ClientID.Length - c.ID.Length - 1) + "$";

            Control parent = c.Parent;
            int pos = id.Length;
            
            while (parent != null && !string.IsNullOrEmpty(parent.ID))
            {
                pos = id.LastIndexOf(parent.ID);
                if (pos > -1)
                {
                    id = id.Remove(pos - 1, 1);
                    id = id.Insert(pos - 1, "$");
                }
                parent = parent.Parent;
            }
            if (pos == -1)
                pos = id.Length;
            id = id.Substring(0, pos).Replace(ClientIDSeparator.ToString(), "$") + id.Substring(pos);
            id += c.ID;
            //                id = id.Replace(ClientIDSeparator.ToString(),"$"  );
            //              id += ID;
            return id;*/
        }
        /// <summary>
        /// Adds validation to a form (using the validateForm() Javascript routine).  Must incude the 'validateForm.js' file in the page
        /// </summary>
        /// <param name="frm">The form to add validation to</param>
        public static void ProcessForm(HtmlForm frm, bool oldForms)
        {
            ProcessForm(frm, "", oldForms);
        }

        private static void addSubmitButton(HtmlForm frm, bool oldForms)
        {
            string jsKeyEnabled = "try { js.com.cs." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION + ".UI." + (oldForms ? "FormsOld" : "Forms") + ".FormsCollection" + (oldForms ? "Old" : "") + ".get_instance().keyEnterEnabled = false; }catch(e) {}";
            CS.General_v3.Util.JSUtilOld.AddJSScriptToPage(jsKeyEnabled);
            //jsKeyEnabled = CS.General_v3.Util.JSUtil.MakeJavaScript(jsKeyEnabled, true);
            //frm.Page.ClientScript.RegisterClientScriptBlock(frm.Page.GetType(), "no-key-enabled", jsKeyEnabled);


            HtmlGenericControl btn = new HtmlGenericControl("input");
            btn.Attributes["type"] = "submit";
            btn.Attributes["position"] = "absolute";
            btn.Attributes["left"] = "-2000px";
            btn.Attributes["top"] = "-2000px";



            frm.Controls.Add(btn);

        }

        private static void enableValidation(HtmlForm frm, bool oldForms)
        {
            if (frm != null)
            {
                string jsKeyEnabled = "try { js.com.cs." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION + ".UI." + (oldForms ? "FormsOld" : "Forms") + ".FormsCollection" + (oldForms ? "Old" : "") + ".get_instance().keyEnterEnabled = false; }catch(e) {}";
                CS.General_v3.Util.JSUtilOld.AddJSScriptToPage(jsKeyEnabled);
                //jsKeyEnabled = CS.General_v3.Util.JSUtil.MakeJavaScript(jsKeyEnabled, true);
                //frm.Page.ClientScript.RegisterClientScriptBlock(frm.Page.GetType(), "no-key-enabled", jsKeyEnabled);

                string validationJS = "return js.com.cs." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION + ".UI." + (oldForms ? "FormsOld" : "Forms") + ".FormsCollection" + (oldForms ? "Old" : "") + ".ASPNetWebFormOnSubmit();";

                frm.Page.ClientScript.RegisterOnSubmitStatement(frm.Page.GetType(), "form-validation", validationJS);

                HtmlGenericControl btn = new HtmlGenericControl("input");
                btn.Attributes["type"] = "submit";
                btn.Style.Add("position", "absolute");
                btn.Style.Add("left", "-2000px");
                btn.Style.Add("top", "-2000px");



                frm.Controls.Add(btn);
            }
        }
        private static void generateGUIDField(HtmlForm frm)
        {
            //frm.Page.ClientScript.RegisterHiddenField(CS.General_v3.FileUploader.UploadManager.UPLOADID_FORMELEMENT_NAME, Guid.NewGuid().ToString());

        }

        /// <summary>
        /// Adds validation to a form (using the validateForm() Javascript routine).  Must incude the 'validateForm.js' file in the page
        /// </summary>
        /// <param name="frm">The form to add validation to</param>
        /// <param name="addCode">The custom onSubmit code.  Place any javascript code here
        /// and use [V] to signify where the [validateForm] code will be placed.  The [V]
        /// will be ONLY the validateForm(...) code, so include the RETURN and the COLON afterwards
        /// if you do not want the form to post, even if there are error messages!</param>
        public static void ProcessForm(HtmlForm frm, string addCode, bool oldForms)
        {
            if (frm != null)
            {
                enableValidation(frm, oldForms);
                generateGUIDField(frm);




                /*
                string tmp2 = "";
                tmp2 += "var validateResult = js.com.cs."+CS.General_v3.Settings.Others.JAVASCRIPT_VERSION+".UI.Forms.FormsCollection.get_instance().validateCurrentForm();\r\n";
                tmp2 += "var result = (validateResult.length == 0);\r\n";
                tmp2 += "return result;\r\n";


                frm.Page.ClientScript.RegisterOnSubmitStatement(frm.GetType(), "processform", tmp2);*/




                CS.General_v3.Util.PageUtil.CheckLabels(frm);
            }
        }


        /// <summary>
        /// Clears a form, setting all textboxes to '', checkboxes to unchecked, and selects
        /// the first radio button option of each control
        /// </summary>
        /// <param name="frm">Form</param>
        public static void ClearForm(HtmlForm frm)
        {
            ArrayList ctrls = new ArrayList();
            Queue q = new Queue();
            q.Enqueue(frm);

            while (q.Count > 0)
            {
                Control ctrl = (Control)q.Dequeue();
                for (int i = 0; i < ctrl.Controls.Count; i++)
                {
                    q.Enqueue(ctrl.Controls[i]);
                }
                ctrls.Add(ctrl);
            }
            q.Clear();
            q = null;
            for (int i = 0; i < ctrls.Count; i++)
            {
                if (ctrls[i] is CS.General_v3.Controls.WebControls.Common.MyTxtBox)
                {
                    ((CS.General_v3.Controls.WebControls.Common.MyTxtBox)ctrls[i]).Text = "";
                }
                else if (ctrls[i] is CS.General_v3.Controls.WebControls.Common.MyDropDownList)
                {
                    ((CS.General_v3.Controls.WebControls.Common.MyDropDownList)ctrls[i]).SelectedIndex = 0;
                }
                else if (ctrls[i] is CS.General_v3.Controls.WebControls.Common.MyFileUpload)
                {
                    //??
                }
                else if (ctrls[i] is CS.General_v3.Controls.WebControls.Common.MyRadioButtonList)
                {
                    ((CS.General_v3.Controls.WebControls.Common.MyRadioButtonList)ctrls[i]).SelectedIndex = 0;
                }
                else if (ctrls[i] is CS.General_v3.Controls.WebControls.Common.MyCheckBox)
                {
                    ((CS.General_v3.Controls.WebControls.Common.MyCheckBox)ctrls[i]).Checked = false;
                }
                else if (ctrls[i] is CS.General_v3.Controls.WebControls.Common.MyCheckBoxList)
                {
                    CS.General_v3.Controls.WebControls.Common.MyCheckBoxList ctrl = (CS.General_v3.Controls.WebControls.Common.MyCheckBoxList)ctrls[i];
                    for (int j = 0; j < ctrl.Items.Count; j++)
                    {
                        ctrl.Items[j].Selected = false;
                    }
                }
            }
            ctrls.Clear();
        }
    }

}
