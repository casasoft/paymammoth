﻿using System;
using System.Collections.Generic;
using System.Text;
using AxShockwaveFlashObjects;

using System.Windows.Forms;
namespace CS.General_v3.Util.FlashInterop
{
    public class Flash
    {
        private AxShockwaveFlash _flash;

        public event _IShockwaveFlashEvents_FlashCallEventHandler FlashCall;

        public Flash(AxShockwaveFlash flashObject)
        {
            this._flash = flashObject;
            this._flash.FlashCall += new _IShockwaveFlashEvents_FlashCallEventHandler(_flash_FlashCall);
        }

        void _flash_FlashCall(object sender, _IShockwaveFlashEvents_FlashCallEvent e)
        {
            if (FlashCall != null)
            {
                FlashCall(sender, e);
            }
        }


        public void loadMovie(string path)
        {
            this._flash.LoadMovie(0, path);
        }
        public void AttachWithControl(Control control)
        {
            control.Resize += new EventHandler(control_Resize);

            if (this._flash.Parent != control)
            {
                control.Controls.Add(this._flash);
            }
           
           
            this._flash.Left = 0;
            this._flash.Top = 0;
            updateSize(control);
        }
        private void updateSize(Control c)
        {
            this._flash.Width = c.ClientSize.Width;
            this._flash.Height = c.ClientSize.Height;
            
        }
        void control_Resize(object sender, EventArgs e)
        {
            Control c = (Control)sender;
            updateSize(c);
        }

        public AxShockwaveFlash ShockwaveFlash { get { return this._flash; } }
    }
}
