﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

namespace CS.General_v3.Util.FlashInterop
{
    public class FlashInvoke
    {
        private string _functionName;
        private List<object> _parameters;
        private AxShockwaveFlashObjects.AxShockwaveFlash _flash;
        public FlashInvoke(AxShockwaveFlashObjects.AxShockwaveFlash flash, string functionName, List<object> parameters)
        {
            this._flash = flash;
            this._functionName = functionName;
            this._parameters = parameters;
        }
        public void Invoke()
        {
            this.Invoke(true);
        }
        public void Invoke(bool omitErrors)
        {
            if (omitErrors)
            {
                try
                {
                    this._flash.CallFunction(this.XML);
                }
                catch (Exception ex)
                {
                }

            }
            else
            {
                this._flash.CallFunction(this.XML);
            }
        }
        private string objToXML(object obj)
        {
            double num;
            if (obj == null)
            {

                return "<null />";

            }
            else if (obj is string)
            {
                string s = (string)obj;
                return "<string>" + s + "</string>";
            }
            else if (Double.TryParse(obj.ToString(), out num))
            {
                return "<number>" + num + "</number>";
            }
            else if (obj is bool)
            {
                if ((bool)obj)
                {
                    return "<true />";
                }
                else
                {
                    return "<false />";
                }
            }
            else if (obj is IList)
            {
                IList list = (IList)obj;
                string s = "<array>";
                for (int i = 0; i < list.Count; i++)
                {
                    object o = list[i];
                    s += "<property id=\"" + i + "\">";
                    s += objToXML(o);
                    s += "</property>";
                }
                s += "</array>";
                return s;
            }

            throw new Exception("Invalid Type");
        }



        public string XML
        {
            get
            {
                string xml = "<invoke name=\"" + this._functionName + "\">";
                xml += "<arguments>";
                for (int i = 0; i < this._parameters.Count; i++)
                {
                    object obj = this._parameters[i];
                    
                    xml += objToXML(obj);

                }
                xml += "</arguments></invoke>";
                return xml;
            }
        }

    }
}
