﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CS.General_v3.Util.FlashInterop
{
    public class FlashCall
    {
        public FlashCall(string request)
        {
            this.Arguments = new List<string>();
            parseXMLRequest(request);

        }

        private void parseXMLRequest(string request)
        {
            XmlDocument xmlRequest = new XmlDocument();
            xmlRequest.LoadXml(request);
            XmlNode invokeNode = xmlRequest.SelectSingleNode("/invoke");
            this.Name = invokeNode.Attributes["name"].Value;
            this.ReturnType = invokeNode.Attributes["returntype"].Value;

            XmlNode arguments = xmlRequest.SelectSingleNode("/invoke/arguments");
            for (int i = 0; i < arguments.ChildNodes.Count; i++)
            {
                XmlNode argument = arguments.ChildNodes[i];
                this.Arguments.Add(argument.InnerText);
            }

        }

        public string Name { get; set; }
        public string ReturnType { get; set; }

        public List<string> Arguments { get; set; }
    }
}
