﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
namespace CS.General_v3.Util
{
    public static class Data
    {
        public class CountryCallingCode
        {
            public int Code { get; set; }
            public Enums.ISO_ENUMS.Country_ISO3166 CountryISO { get; set; }
        }

        private static List<CountryCallingCode> _COUNTRY_CALLING_CODES;
        private static List<int> _COUNTRY_CALLING_CODES_INTEGERS;

        public enum CONTENT_TYPE
        {
            TextHTML,
            TextPlain,
            PDFDocument,
            ImageJPEG,
            Executable,
            ExcelFile,
            WordDocument,
            Javascript,
            ZIPFile,
            AudioMP3,
            MPEG,
            Quicktime,
            CSS,
            Bitmap,
            FlashShockwave,
            AudioWAV,
            ImageGIF
        }
        
        /// <summary>
        /// Converts one of the enumerated content type to the equivalent text
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetMIMEContentType(string filename)
        {
            string ext;
            int dotPos = filename.LastIndexOf('.');
            if (dotPos > -1)
                ext = filename.Substring(dotPos + 1).ToLower();
            else
                ext = filename;
            switch (ext)
            {
                case "evy": return "application/envoy";
                case "fif": return "application/fractals";
                case "spl": return "application/futuresplash";
                case "hta": return "application/hta";
                case "acx": return "application/internet-property-stream";
                case "doc":
                case "dot": return "application/msword";
                case "flv": return "video/x-flv";
                case "bin":
                case "class":
                case "dms":
                case "exe":
                case "lha":
                case "lzh": return "application/octet-stream";
                case "oda": return "application/oda";
                case "axs": return "application/olescript";
                case "pdf": return "application/pdf";
                case "prf": return "application/pics-rules";
                case "p10": return "application/pkcs10";
                case "crl": return "application/pkix-crl";
                case "eps":
                case "ps":
                case "ai": return "application/postscript";
                case "rtf": return "application/rtf";
                case "setpay": return "application/set-payment-initiation";
                case "setreg": return "application/set-registration-initiation";
                case "xla":
                case "xlc":
                case "xlm":
                case "xls":
                case "xlt":
                case "xlw": return "application/vnd.ms-excel";
                case "msg": return "application/vnd.ms-outlook";
                case "sst": return "application/vnd.ms-pkicertstore";
                case "cat": return "application/vnd.pkiseccat";
                case "stl": return "application/vnd.ms-pkistl";
                case "pot":
                case "pps":
                case "ppt": return "application/vnd.ms-powerpoint";
                case "mpp": return "application/vnd.ms-project";
                case "wcm":
                case "wdb":
                case "wks":
                case "wps": return "application/vnd.ms-works";
                case "hlp": return "application/winhlp";
                case "bcpio": return "application/x-bcpio";
                case "cdf": return "application/x-cdf";
                case "z": return "application/x-compress";
                case "tgz": return "application/x-compressed";
                case "cpio": return "application/x-cpio";
                case "csh": return "application/x-csh";
                case "dcr":
                case "dir":
                case "dxr": return "application/x-director";
                case "dvi": return "application/x-dvi";
                case "gtar": return "application/x-gtar";
                case "gz": return "application/x-gzip";
                case "hdf": return "application/x-hdf";
                case "isp":
                case "ins": return "application/x-internet-signup";
                case "iii": return "application/x-iphone";
                case "js": return "application/x-javascript";
                case "latex": return "application/x-latex";
                case "mdb": return "application/x-msaccess";
                case "crd": return "application/x-mscardfile";
                case "clp": return "application/x-msclip";
                case "dll": return "application/x-msdownload";
                case "m13":
                case "m14":
                case "mvb": return "application/x-msmediaview";
                case "wmf": return "application/x-msmetafile";
                case "mny": return "application/x-msmoney";
                case "pub": return "application/x-ms-publisher";
                case "scd": return "application/x-msschedule";
                case "trm": return "application/x-msterminal";
                case "wri": return "application/x-mswrite";
                case "nc": return "application/x-netcdf";
                case "pma":
                case "pmc":
                case "pml":
                case "pmr":
                case "pmw": return "application/x-perfmon";
                case "p12":
                case "pfx": return "application/x-pkcs12";
                case "p7b":
                case "spc": return "application/x-pkcs7-certificates";
                case "p7r": return "application/x-pkcs7-certreqresp";
                case "p7c":
                case "p7m": return "application/x-pkcs7-mime";
                case "p7s": return "application/x-pkcs7-signature";
                case "sh": return "application/x-sh";
                case "shar": return "application/x-shar";
                case "swf": return "application/x-shockwave-flash";
                case "sv4cpio": return "application/x-sv4cpio";
                case "sv4crc": return "application/x-sv4crc";
                case "tar": return "application/x-tar";
                case "tcl": return "application/x-tcl";
                case "tex": return "application/x-tex";
                case "texi":
                case "texinfo": return "application/x-texinfo";
                case "roff":
                case "t":
                case "tr": return "application/x-troff";
                case "man": return "application/x-troff-man";
                case "me": return "application/x-troff-me";
                case "ms": return "application/x-troff-ms";
                case "ustar": return "application/x-ustar";
                case "src": return "application/x-wais-source";
                case "cer":
                case "crt":
                case "der": return "application/x-x509-ca-cert";
                case "zip": return "application/zip";
                case "pko": return "application/ynd.ms-pkipko";
                case "au":
                case "snd": return "audio/basic";
                case "m4v": return "video/x-m4v";
                case "mid":
                case "rmi": return "audio/mid";
                case "mp2":
                case "mp3": return "audio/mpeg";
                case "aif":
                case "aifc":
                case "aiff": return "audio/x-aiff";
                case "m3u": return "audio/x-mpegurl";
                case "ra":
                case "ram": return "audio/x-pn-realaudio";
                case "wav": return "audio/x-wav";
                case "bmp": return "image/bmp";
                case "cod": return "image/cis-cod";
                case "gif": return "image/gif";
                case "png": return "image/png";
                case "ief": return "image/ief";
                case "jpe":
                case "jpg":
                case "jpeg": return "image/jpeg";
                case "jfif": return "image/pipeg";
                case "svg": return "image/svg+xml";
                case "tif":
                case "tiff": return "image/tiff";
                case "ras": return "image/x-cmu-raster";
                case "cmx": return "image/x-cmx";
                case "ico": return "image/x-icon";
                case "pnm": return "image/x-portable-anymap";
                case "pbm": return "image/x-portable-bitmap";
                case "pgm": return "image/x-portable-graymap";
                case "ppm": return "image/x-portable-pixmap";
                case "rgb": return "image/x-rgb";
                case "xbm": return "image/x-xbitmap";
                case "xpm": return "image/x-xpixmap";
                case "xwd": return "image/x-xwindowdump";
                case "mht":
                case "mhtml":
                case "nws": return "message/rfc822";
                case "css": return "text/css";
                case "323": return "text/h323";
                case "htm":
                case "html":
                case "stm": return "text/html";
                case "uls": return "text/iuls";
                case "txt":
                case "bas":
                case "c":
                case "h": return "text/plain";

                case "rtx": return "text/richtext";
                case "sct": return "text/scriptlet";
                case "tsv": return "text/tab-separated-values";
                case "htt": return "text/webviewhtml";
                case "htc": return "text/x-component";
                case "etx": return "text/x-setext";
                case "vcf": return "text/x-vcard";
                
                case "mpa":
                case "mpe":
                case "mpeg":
                case "mpg":
                case "mpv2": return "video/mpeg";
                case "mov":
                case "qt": return "video/quicktime";
                case "lsf":
                case "lsx": return "video/x-la-asf";
                case "asf":
                case "asr":
                case "asx": return "video/x-ms-asf";
                case "avi": return "video/x-msvideo";
                case "movie": return "video/x-sgi-movie";
                case "flr":
                case "vrml":
                case "wrl":
                case "wrz":
                case "xaf":
                case "xof": return "x-world/x-vrml";
                case "xml": return "text/xml";
                case "mp4": return "video/mp4";

                default: return "application/octet-stream";

            }
            

        }
        /// <summary>
        /// Converts one of the enumerated content type to the equivalent text
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string getExtensionFromMIMEContentType(string contentType)
        {
            contentType = contentType.ToLower();
            switch (contentType)
            {
                case "application/envoy": return "evy";
                case "application/fractals": return "fif";
                case "application/futuresplash": return "spl";
                case "application/hta": return "hta";
                case "application/internet-property-stream": return "acx";
                case "application/msword": return "dot";
                case "video/x-flv": return "flv";
                
                case "application/octet-stream": return "exe";
                case "application/oda": return "oda";
                case "application/olescript": return "axs";
                case "application/pdf": return "pdf";
                case "application/pics-rules": return "prf";
                case "application/pkcs10": return "p10";
                case "application/pkix-crl": return "crl";
                
                case "application/postscript": return "ai";
                case "application/rtf": return "rtf";
                case "application/set-payment-initiation": return "setpay";
                case "application/set-registration-initiation": return "setreg";
                
                case "application/vnd.ms-excel": return "xls";
                case "application/vnd.ms-outlook": return "msg";
                case "application/vnd.ms-pkicertstore": return "sst";
                case "application/vnd.pkiseccat": return "cat";
                case "application/vnd.ms-pkistl": return "stl";


                case "application/vnd.ms-powerpoint": return "ppt";
                case "application/vnd.ms-project": return "mpp";
               

                case "application/vnd.ms-works": return "wps";
                case "application/winhlp": return "hlp";
                case "application/x-bcpio": return "bcpio";
                case "application/x-cdf": return "cdf";
                case "application/x-compress": return "z";
                case "application/x-compressed": return "tgz";
                case "application/x-cpio": return "cpio";
                case "application/x-csh": return "csh";
                

                case "application/x-director": return "dxr";
                case "application/x-dvi": return "dvi";
                case "application/x-gtar": return "gtar";
                case "application/x-gzip": return "gz";
                case "application/x-hdf": return "hdf";
                
                case "application/x-internet-signup": return "ins";
                case "application/x-iphone": return "iii";
                case "application/x-javascript": return "js";
                case "application/x-latex": return "latex";
                case "application/x-msaccess": return "mdb";
                case "application/x-mscardfile": return "crd";
                case "application/x-msclip": return "clp";
                case "application/x-msdownload": return "dll";
                

                case "application/x-msmediaview": return "mvb";
                case "application/x-msmetafile": return "wmf";
                case "application/x-msmoney": return "mny";
                case "application/x-ms-publisher": return "pub";
                case "application/x-msschedule": return "scd";
                case "application/x-msterminal": return "trm";
                case "application/x-mswrite": return "wri";
                case "application/x-netcdf": return "nc";
                

                case "application/x-perfmon": return "pmw";
                
                case "application/x-pkcs12": return "pfx";
                
                case "application/x-pkcs7-certificates": return "spc";
                case "application/x-pkcs7-certreqresp": return "p7r";
                
                case "application/x-pkcs7-mime": return "p7m";
                case "application/x-pkcs7-signature": return "p7s";
                case "application/x-sh": return "sh";
                case "application/x-shar": return "shar";
                case "application/x-shockwave-flash": return "swf";
                case "application/x-sv4cpio": return "sv4cpio";
                case "application/x-sv4crc": return "sv4crc";
                case "application/x-tar": return "tar";
                case "application/x-tcl": return "tcl";
                case "application/x-tex": return "tex";
                
                case "application/x-texinfo": return "texinfo";
                
                case "application/x-troff": return "tr";
                case "application/x-troff-man": return "man";
                case "application/x-troff-me": return "me";
                case "application/x-troff-ms": return "ms";
                case "application/x-ustar": return "ustar";
                case "application/x-wais-source": return "src";
                
                case "application/x-x509-ca-cert": return "cer";
                case "application/zip": return "zip";
                case "application/ynd.ms-pkipko": return "pko";
                
                case "audio/basic": return "snd";
                
                case "audio/mid": return "mid";

                case "audio/mpeg": return "mp3";
                
                case "audio/x-aiff": return "aif";
                case "audio/x-mpegurl": return "m3u";
                
                case "audio/x-pn-realaudio": return "ra";
                case "audio/x-wav": return "wav";
                case "image/bmp": return "bmp";
                case "image/cis-cod": return "cod";
                case "image/gif": return "gif";
                case "image/png": return "png";
                case "ief": return "image/ief";
                
                case "image/jpeg": return "jpg";
                case "image/pipeg": return "jfif";
                case "image/svg+xml": return "svg";
                
                case "image/tiff": return "tif";
                case "image/x-cmu-raster": return "ras";
                case "image/x-cmx": return "cmx";
                case "image/x-icon": return "ico";
                case "image/x-portable-anymap": return "pnm";
                case "image/x-portable-bitmap": return "pbm";
                case "image/x-portable-graymap": return "pgm";
                case "image/x-portable-pixmap": return "ppm";
                case "image/x-rgb": return "rgb";
                case "image/x-xbitmap": return "xbm";
                case "image/x-xpixmap": return "xpm";
                case "image/x-xwindowdump": return "xwd";
                
                case "message/rfc822": return "mhtml";
                case "text/css": return "css";
                case "text/h323": return "323";
                
                case "text/html": return "html";
                case "text/iuls": return "uls";
                case "text/plain": return "txt";

                case "text/richtext": return "rtx";
                case "text/scriptlet": return "sct";
                case "text/tab-separated-values": return "tsv";
                case "text/webviewhtml": return "htt";
                case "text/x-component": return "htc";
                case "text/x-setext": return "etx";
                case "text/x-vcard": return "vcf";
                
                case "video/mpeg": return "mpg";
                case "video/quicktime": return "mov";
                case "video/x-la-asf": return "lsf";
                
                case "video/x-ms-asf": return "asf";
                case "video/x-msvideo": return "avi";
                case "video/x-sgi-movie": return "movie";
                
                case "x-world/x-vrml": return "flr";
                case "text/xml": return "xml";
                

            }
            return "";

        }
        public static ListItemCollection GetMalteseLocalities()
        {
            ListItemCollection items = new ListItemCollection();

            items.Add(new ListItem("Attard"));

            items.Add(new ListItem("Bahrija"));
            items.Add(new ListItem("Balzan"));
            items.Add(new ListItem("Bidnija"));
            items.Add(new ListItem("Bir Id-Deheb"));
            items.Add(new ListItem("Birguma"));
            items.Add(new ListItem("Birkirkara"));

            items.Add(new ListItem("Birzebbugia"));
            items.Add(new ListItem("Blata l-Bajda"));
            items.Add(new ListItem("Bugibba"));
            items.Add(new ListItem("Burmarrad"));
            items.Add(new ListItem("Buskett"));
            items.Add(new ListItem("Cospicua"));

            items.Add(new ListItem("Dingli"));
            items.Add(new ListItem("Fgura"));
            items.Add(new ListItem("Fleur De Lys"));
            items.Add(new ListItem("Floriana"));
            items.Add(new ListItem("Fontana"));
            items.Add(new ListItem("Ghajn Tuffieha"));

            items.Add(new ListItem("Ghajnsielem"));
            items.Add(new ListItem("Ghammar"));
            items.Add(new ListItem("Gharb"));
            items.Add(new ListItem("Gharghur"));
            items.Add(new ListItem("Ghasri"));
            items.Add(new ListItem("Ghaxaq"));

            items.Add(new ListItem("Gudja"));
            items.Add(new ListItem("Gwardamangia"));
            items.Add(new ListItem("Gzira"));
            items.Add(new ListItem("Hal-Far"));
            items.Add(new ListItem("Hamrun"));
            items.Add(new ListItem("High Ridge"));

            items.Add(new ListItem("Iklin"));


            items.Add(new ListItem("Kalafrana"));
            items.Add(new ListItem("Kalkara"));
            items.Add(new ListItem("Kappara"));
            items.Add(new ListItem("Kercem"));
            items.Add(new ListItem("Kirkop"));
            items.Add(new ListItem("Kordin"));

            items.Add(new ListItem("Kuncizzjoni"));
            items.Add(new ListItem("Landriet"));
            items.Add(new ListItem("Lija"));
            items.Add(new ListItem("Luqa"));
            items.Add(new ListItem("Luqa Industrial"));
            items.Add(new ListItem("Madliena"));

            items.Add(new ListItem("Maghtab"));
            items.Add(new ListItem("Manikata"));
            items.Add(new ListItem("Marfa"));
            items.Add(new ListItem("Marsa"));
            items.Add(new ListItem("Marsa Industrial"));
            items.Add(new ListItem("Marsalforn"));

            items.Add(new ListItem("Marsascala"));
            items.Add(new ListItem("Marsaxlokk"));
            items.Add(new ListItem("Mdina"));
            items.Add(new ListItem("Mellieha"));
            items.Add(new ListItem("Mensija"));
            items.Add(new ListItem("Mgarr"));

            items.Add(new ListItem("Mgarr (Gozo)"));
            items.Add(new ListItem("Mizieb"));
            items.Add(new ListItem("Mosta"));
            items.Add(new ListItem("Mqabba"));
            items.Add(new ListItem("Mriehel"));
            items.Add(new ListItem("Mriehel Industrial"));

            items.Add(new ListItem("Msida"));
            items.Add(new ListItem("Mtahleb"));
            items.Add(new ListItem("Mtarfa"));
            items.Add(new ListItem("Munxar"));
            items.Add(new ListItem("Nadur"));
            items.Add(new ListItem("Naxxar"));
            items.Add(new ListItem("Paceville"));
            items.Add(new ListItem("Paola"));

            items.Add(new ListItem("Pembroke"));
            items.Add(new ListItem("Portomaso"));
            items.Add(new ListItem("Pieta"));
            items.Add(new ListItem("Qala"));

            items.Add(new ListItem("Qawra"));
            items.Add(new ListItem("Qormi"));
            items.Add(new ListItem("Qormi Industrial"));
            items.Add(new ListItem("Qrendi"));

            items.Add(new ListItem("Rabat"));
            items.Add(new ListItem("Safi"));
            items.Add(new ListItem("Salina"));
            items.Add(new ListItem("San Gwann"));
            items.Add(new ListItem("San Lawrenz"));
            items.Add(new ListItem("San Pawl Tat-Targa"));

            items.Add(new ListItem("Sannat"));
            items.Add(new ListItem("Santa Lucia"));
            items.Add(new ListItem("Santa Lucia (Gozo)"));
            items.Add(new ListItem("Santa Venera"));
            items.Add(new ListItem("Senglea"));
            items.Add(new ListItem("Siggiewi"));

            items.Add(new ListItem("Sliema"));
            items.Add(new ListItem("St Pauls Bay"));
            items.Add(new ListItem("St.Andrews"));
            items.Add(new ListItem("St.Julians"));
            items.Add(new ListItem("Swatar"));
            items.Add(new ListItem("Swieqi"));

            items.Add(new ListItem("Ta' Giorni"));
            items.Add(new ListItem("Ta' l-Ibrag"));
            items.Add(new ListItem("Tal-Handaq"));
            items.Add(new ListItem("Ta Xbiex"));
            items.Add(new ListItem("Tarxien"));
            items.Add(new ListItem("The Gardens"));

            items.Add(new ListItem("The Village"));
            items.Add(new ListItem("Valletta"));
            items.Add(new ListItem("Vittoriosa"));
            items.Add(new ListItem("Victoria (Gozo)"));
            items.Add(new ListItem("Wardija"));
            items.Add(new ListItem("Xaghra"));
            items.Add(new ListItem("Xemxija"));
            items.Add(new ListItem("Xewkija"));

            items.Add(new ListItem("Xghajra"));
            items.Add(new ListItem("Xlendi"));
            items.Add(new ListItem("Xwieki"));
            items.Add(new ListItem("Zabbar"));
            items.Add(new ListItem("Zebbiegh"));
            items.Add(new ListItem("Zebbug"));
            items.Add(new ListItem("Zebbug (Gozo)"));

            items.Add(new ListItem("Zejtun"));
            items.Add(new ListItem("Zurrieq"));

            return items;
        }
        public static void FillMalteseLocalities(CS.General_v3.Controls.WebControls.Common.MyDropDownList cmb)
        {
            FillComboBox(cmb, GetMalteseLocalities());
        }

        public static void FillCountriesWithCodeInCmb(CS.General_v3.Controls.WebControls.Common.MyDropDownList cmb)
        {
            FillComboBox(cmb, GetCountriesAsListWith2LetterCodes());
        }
        public static void FillComboBox(ListControl cmb, ListItemCollection items)
        {
            for (int i = 0; i < items.Count; i++)
            {
                cmb.Items.Add(items[i]);
            }
        }
        /*
        public static void FillComboBoxFromEnum(ListControl cmb, Type enumType, string blankItemOnTopValue, Enums.ENUM_SORT_BY sortBy =  Enums.ENUM_SORT_BY.NameAscending, bool addSpacesToCamelCasedName = false)
        {
            var list = CS.General_v3.Util.EnumUtils.GetListItemCollectionFromEnum(enumType, blankItemOnTopValue, sortBy, addSpacesToCamelCasedName);
            FillComboBox(cmb, list);
        }*/
        public static ListItemCollection GetCountriesAsListWith2LetterCodes()
        {
            return GetListItemCollectionFromEnum<Enums.ISO_ENUMS.Country_ISO3166>();
        }
        public static ListItemCollection GetListItemCollectionFromEnum<TEnum>()
        {

            ListItemCollection items = new ListItemCollection();
            List<ListItem> list = new List<ListItem>();

            Array values = Enum.GetValues(typeof(TEnum));
            for (int i = 0; i < values.Length; i++)
            {
                Enum val = (Enum)values.GetValue(i);
                string name = EnumUtils.StringValueOf(val);
                string code = (val != null ? val.ToString() : ""); 
                list.Add(new ListItem(name, code));
            }
            list.Sort((item1, item2) => (item1.Text.CompareTo(item2.Text)));
            items.AddRange(list.ToArray());


            return items;
        }


        public static ListItemCollection GetCountriesAsListWith3LetterCodes(List<CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166> excludeCountries = null)
        {

            ListItemCollection items = new ListItemCollection();
            List<ListItem> list = new List<ListItem>();

            var values = CS.General_v3.Util.EnumUtils.GetListOfEnumValues<Enums.ISO_ENUMS.Country_ISO3166>();
            for (int i = 0; i < values.Count; i++)
            {
                var val = values[i];
                if (excludeCountries == null || !excludeCountries.Contains(val))
                {
                    string code = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_To3LetterCode(val);
                    string name = EnumUtils.StringValueOf(val);

                    list.Add(new ListItem(name, code));
                }
            }
            list.Sort((item1, item2) => (item1.Text.CompareTo(item2.Text)));

            foreach (var item in list)
            {
                items.Add(item);
            }

            return items;
        }

        public static void FillCountriesInCmb(CS.General_v3.Controls.WebControls.Common.MyDropDownList cmb, bool use2LetterCountryCode = true)
        {
            FillComboBox(cmb, use2LetterCountryCode ? GetCountriesAsListWith2LetterCodes() : GetCountriesAsListWith3LetterCodes());
        }
        public static int GetCountryCallingCode(Enums.ISO_ENUMS.Country_ISO3166 country)
        {
            var countryCodes = GetCountryCallingCodes();
            for (int i = 0; i < countryCodes.Count; i++)
            {
                if (countryCodes[i] != null && countryCodes[i].CountryISO == country)
                {
                    return countryCodes[i].Code;
                }
            }
            return -1;
        }
        public static List<CountryCallingCode> GetCountryCallingCodes()
        {
            if (_COUNTRY_CALLING_CODES == null)
            {
                _COUNTRY_CALLING_CODES = new List<CountryCallingCode>();

                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 1268, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("AG").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 1264, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("AI").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 672, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("AQ").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 1684, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("AS").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 358, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("AX").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 1246, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("BB").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 590, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("BL").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 1441, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("BM").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 599, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("BQ").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 1242, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("BS").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = -1, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("BV").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 1, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("CA").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 61, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("CC").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 599, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("CW").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 61, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("CX").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 1767, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("DM").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 1809, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("DO").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 212, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("EH").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 1473, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("GD").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 44, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("GG").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = -1, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("GS").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 1671, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("GU").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = -1, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("HM").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 44, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("IM").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 44, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("JE").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 1876, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("JM").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 1869, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("KN").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 1345, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("KY").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 7, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("KZ").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 1758, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("LC").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 590, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("MF").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 1670, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("MP").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 1664, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("MS").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = -1, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("PN").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 1787, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("PR").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 47, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("SJ").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 1721, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("SX").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 1649, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("TC").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = -1, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("TF").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 1868, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("TT").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = -1, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("UM").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 1784, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("VC").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 1284, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("VG").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 1340, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("VI").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 262, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("YT").Value });


                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 1, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("US").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 20, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("EG").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 212, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("MA").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 213, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("DZ").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 216, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("TN").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 218, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("LY").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 220, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("GM").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 221, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("SN").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 222, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("MR").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 223, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("ML").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 224, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("GN").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 225, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("CI").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 226, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("BF").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 227, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("NE").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 228, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("TG").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 229, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("BJ").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 230, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("MU").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 231, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("LR").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 232, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("SL").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 233, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("GH").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 234, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("NG").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 235, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("TD").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 236, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("CF").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 237, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("CM").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 238, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("CV").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 239, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("ST").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 240, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("GQ").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 241, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("GA").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 242, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("CG").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 243, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("CD").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 244, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("AO").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 245, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("GW").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 246, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("IO").Value });
                //_COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 247, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("AC").Value }); not existing
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 248, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("SC").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 249, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("SD").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 250, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("RW").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 251, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("ET").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 252, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("SO").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 253, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("DJ").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 254, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("KE").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 255, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("TZ").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 256, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("UG").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 257, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("BI").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 258, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("MZ").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 260, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("ZM").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 261, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("MG").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 262, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("RE").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 263, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("ZW").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 264, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("NA").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 265, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("MW").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 266, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("LS").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 267, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("BW").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 268, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("SZ").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 269, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("KM").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 27, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("ZA").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 290, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("SH").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 291, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("ER").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 297, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("AW").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 298, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("FO").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 299, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("GL").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 30, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("GR").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 31, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("NL").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 32, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("BE").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 33, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("FR").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 34, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("ES").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 350, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("GI").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 351, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("PT").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 352, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("LU").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 353, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("IE").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 354, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("IS").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 355, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("AL").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 356, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("MT").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 357, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("CY").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 358, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("FI").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 359, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("BG").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 36, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("HU").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 370, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("LT").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 371, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("LV").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 372, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("EE").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 373, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("MD").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 374, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("AM").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 375, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("BY").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 376, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("AD").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 377, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("MC").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 378, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("SM").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 379, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("VA").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 380, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("UA").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 381, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("RS").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 382, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("ME").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 385, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("HR").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 386, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("SI").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 387, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("BA").Value });
                //_COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 388, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("EU").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 389, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("MK").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 39, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("IT").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 40, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("RO").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 41, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("CH").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 420, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("CZ").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 421, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("SK").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 423, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("LI").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 43, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("AT").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 44, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("GB").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 45, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("DK").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 46, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("SE").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 47, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("NO").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 48, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("PL").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 49, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("DE").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 50, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("FK").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 501, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("BZ").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 502, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("GT").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 503, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("SV").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 504, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("HN").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 505, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("NI").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 506, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("CR").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 507, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("PA").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 508, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("PM").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 509, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("HT").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 51, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("PE").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 52, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("MX").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 53, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("CU").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 54, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("AR").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 55, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("BR").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 56, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("CL").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 57, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("CO").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 58, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("VE").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 590, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("GP").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 591, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("BO").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 592, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("GY").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 593, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("EC").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 594, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("GF").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 595, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("PY").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 596, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("MQ").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 597, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("SR").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 598, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("UY").Value });
               // _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 599, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("AN").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 60, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("MY").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 61, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("AU").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 62, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("ID").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 63, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("PH").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 64, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("NZ").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 65, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("SG").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 66, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("TH").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 670, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("TL").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 672, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("NF").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 673, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("BN").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 674, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("NR").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 675, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("PG").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 676, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("TO").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 677, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("SB").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 678, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("VU").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 679, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("FJ").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 680, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("PW").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 681, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("WF").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 682, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("CK").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 683, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("NU").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 685, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("WS").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 686, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("KI").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 687, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("NC").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 688, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("TV").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 689, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("PF").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 690, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("TK").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 691, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("FM").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 692, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("MH").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 7, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("RU").Value });
                //_COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 80, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("XT").Value });
                //_COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 808, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("XS").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 81, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("JP").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 82, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("KR").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 84, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("VN").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 850, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("KP").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 852, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("HK").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 853, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("MO").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 855, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("KH").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 856, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("LA").Value });
                //_COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 857, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("XN").Value });
                //_COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 858, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("XN").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 86, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("CN").Value });
                //_COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 870, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("XN").Value });
                //_COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 878, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("XP").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 880, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("BD").Value });
                //_COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 881, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("XG").Value });
                //_COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 882, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("XV").Value });
                //_COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 883, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("XL").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 886, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("TW").Value });
                //_COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 888, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("XD").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 90, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("TR").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 91, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("IN").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 92, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("PK").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 93, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("AF").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 94, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("LK").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 95, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("MM").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 960, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("MV").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 961, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("LB").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 962, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("JO").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 963, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("SY").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 964, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("IQ").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 965, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("KW").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 966, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("SA").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 967, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("YE").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 968, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("OM").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 970, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("PS").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 971, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("AE").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 972, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("IL").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 973, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("BH").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 974, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("QA").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 975, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("BT").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 976, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("MN").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 977, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("NP").Value });
                //_COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 979, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("XR").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 98, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("IR").Value });
                //_COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 991, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("XC").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 992, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("TJ").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 993, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("TM").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 994, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("AZ").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 995, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("GE").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 996, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("KG").Value });
                _COUNTRY_CALLING_CODES.Add(new CountryCallingCode() { Code = 998, CountryISO = Enums.ISO_ENUMS.Country_ISO3166_FromCode("UZ").Value });

            }
            return _COUNTRY_CALLING_CODES;
        }
        public static List<int> GetCountryCallingCodesIntegers()
        {
            if (_COUNTRY_CALLING_CODES_INTEGERS == null)
            {
                _COUNTRY_CALLING_CODES_INTEGERS = new List<int>();
                var countryCallingCodes = GetCountryCallingCodes();
                for (int i = 0; i < countryCallingCodes.Count; i++)
                {
                    _COUNTRY_CALLING_CODES_INTEGERS.Add(countryCallingCodes[i].Code);
                }
            }
            return _COUNTRY_CALLING_CODES_INTEGERS;
        }
        public static ListItemCollection GetCountryCallingCodesAsListItemCollection(string prefix = "+")
        {
            ListItemCollection coll = new ListItemCollection();
            List<CountryCallingCode> codes = GetCountryCallingCodes();
            for (int i = 0; i < codes.Count; i++)
            {
                coll.Add(new ListItem() { Value = codes[i].Code.ToString(), Text = prefix + codes[i].Code.ToString() });
            }
            return coll;
        }
        public static IEnumerable<TimeZoneInfo> GetTimeZonesList()
        {
            return TimeZoneInfo.GetSystemTimeZones();
        }

        public static ListItemCollection GetTimeZonesListAsListItemCollection()
        {
            ListItemCollection coll = new ListItemCollection();
            
            var timezones = TimeZoneInfo.GetSystemTimeZones();
            foreach (var timezone in timezones)
            {
                
                ListItem item = new ListItem()
                {
                    Text = timezone.DisplayName,
                    Value = timezone.Id
                };
                coll.Add(item);
            }

            return coll;
        }
        public static TimeZoneInfo GetTimeZoneFromCountry(CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166 countryCode)
        {
            TimeSpan? ts = null;
            switch (countryCode)
            {
                case Enums.ISO_ENUMS.Country_ISO3166.France: ts = new TimeSpan(+01, 0, 0); break;
                case Enums.ISO_ENUMS.Country_ISO3166.UnitedStates: ts = new TimeSpan(-05, 0, 0); break;
                case Enums.ISO_ENUMS.Country_ISO3166.Australia: ts = new TimeSpan(+11, 0, 0); break;
                case Enums.ISO_ENUMS.Country_ISO3166.UnitedKingdom: ts = new TimeSpan(0, 0, 0); break;
                case Enums.ISO_ENUMS.Country_ISO3166.Canada: ts = new TimeSpan(-5, 0, 0); break;
                case Enums.ISO_ENUMS.Country_ISO3166.Denmark: ts = new TimeSpan(+15, 0, 0); break;
                case Enums.ISO_ENUMS.Country_ISO3166.NewZealand: ts = new TimeSpan(+12, 0, 0); break;
                case Enums.ISO_ENUMS.Country_ISO3166.Brazil: ts = new TimeSpan(-3, 0, 0); break;
                case Enums.ISO_ENUMS.Country_ISO3166.Indonesia: ts = new TimeSpan(+7, 0, 0); break;
                case Enums.ISO_ENUMS.Country_ISO3166.Kiribati: ts = new TimeSpan(+14, 0, 0); break;
                case Enums.ISO_ENUMS.Country_ISO3166.Mexico: ts = new TimeSpan(-6, 0, 0); break;
                case Enums.ISO_ENUMS.Country_ISO3166.Chile: ts = new TimeSpan(-4, 0, 0); break;
                case Enums.ISO_ENUMS.Country_ISO3166.Ecuador: ts = new TimeSpan(-5, 0, 0); break;
                case Enums.ISO_ENUMS.Country_ISO3166.Micronesia: ts = new TimeSpan(+11, 0, 0); break;
                case Enums.ISO_ENUMS.Country_ISO3166.Kazakhstan: ts = new TimeSpan(+6, 0, 0); break;
                case Enums.ISO_ENUMS.Country_ISO3166.Netherlands: ts = new TimeSpan(+1, 0, 0); break;
                case Enums.ISO_ENUMS.Country_ISO3166.Mongolia: ts = new TimeSpan(+8, 0, 0); break;
                case Enums.ISO_ENUMS.Country_ISO3166.Portugal: ts = new TimeSpan(+0, 0, 0); break;
                case Enums.ISO_ENUMS.Country_ISO3166.Spain: ts = new TimeSpan(+1, 0, 0); break;
                case Enums.ISO_ENUMS.Country_ISO3166.Afghanistan:
                    ts = new TimeSpan(+04, 30, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Albania:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Algeria:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Andorra:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Angola:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.AntiguaandBarbuda:
                    ts = new TimeSpan(-4,0,0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Argentina:
                    ts = new TimeSpan(-3,0,0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Armenia:
                    ts = new TimeSpan(+04, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Austria:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Azerbaijan:
                    ts = new TimeSpan(+04, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Bahamas:
                    ts = new TimeSpan(-5,0,0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Bahrain:
                    ts = new TimeSpan(+03, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Bangladesh:
                    ts = new TimeSpan(+06, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Barbados:
                    ts = new TimeSpan(-4,0,0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Belarus:
                    ts = new TimeSpan(+03, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Belgium:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Belize:
                    ts = new TimeSpan(-6,0,0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Benin:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Bhutan:
                    ts = new TimeSpan(+06, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Bolivia:
                    ts = new TimeSpan(-4,0,0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.BosniaandHerzegovina:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Botswana:
                    ts = new TimeSpan(+02, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.BruneiDarussalam:
                    ts = new TimeSpan(+08, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Bulgaria:
                    ts = new TimeSpan(+02, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.BurkinaFaso:
                    ts = new TimeSpan(0, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Burundi:
                    ts = new TimeSpan(2, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Cambodia:
                    ts = new TimeSpan(+07, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Cameroon:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.CapeVerde:
                    ts = new TimeSpan(-1,0,0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.CentralAfricanRepublic:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Chad:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.China:
                    ts = new TimeSpan(+08, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Colombia:
                    ts = new TimeSpan(-5,0,0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Comoros:
                    ts = new TimeSpan(+03, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Congo:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.CongoTheDemocraticRepublicOfThe:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.CostaRica:
                    ts = new TimeSpan(-6,0,0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.CoteDIvoireIvoryCoast:
                    ts = new TimeSpan(0, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Croatia:
                    ts = new TimeSpan(1, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Cuba:
                    ts = new TimeSpan(-5,0,0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Cyprus:
                    ts = new TimeSpan(+02, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.CzechRepublic:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Djibouti:
                    ts = new TimeSpan(+03, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Dominica:
                    ts = new TimeSpan(-4,0,0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.DominicanRepublic:
                    ts = new TimeSpan(-4,0,0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.TimorLeste:
                    ts = new TimeSpan(+09, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Egypt:
                    ts = new TimeSpan(+02, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.ElSalvador:
                    ts = new TimeSpan(-6,0,0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.EquatorialGuinea:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Eritrea:
                    ts = new TimeSpan(+03, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Estonia:
                    ts = new TimeSpan(+02, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Ethiopia:
                    ts = new TimeSpan(+03, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Fiji:
                    ts = new TimeSpan(+12, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Finland:
                    ts = new TimeSpan(+02, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Gabon:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Gambia:
                    ts = new TimeSpan(0, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Georgia:
                    ts = new TimeSpan(4, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Germany:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Ghana:
                    ts = new TimeSpan(0, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Greece:
                    ts = new TimeSpan(2, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Grenada:
                    ts = new TimeSpan(-4,0,0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Guatemala:
                    ts = new TimeSpan(-6,0,0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Guinea:
                    ts = new TimeSpan(0, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.GuineaBissau:
                    ts = new TimeSpan(0, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Guyana:
                    ts = new TimeSpan(-4, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Haiti:
                    ts = new TimeSpan(-5,0,0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Honduras:
                    ts = new TimeSpan(-6,0,0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.HongKong:
                    ts = new TimeSpan(+08, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Hungary:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Iceland:
                    ts = new TimeSpan(0, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.India:
                    ts = new TimeSpan(5, 30, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Iran:
                    ts = new TimeSpan(+03, 30, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Iraq:
                    ts = new TimeSpan(+03, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Ireland:
                    ts = new TimeSpan(0, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Israel:
                    ts = new TimeSpan(2, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Italy:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Jamaica:
                    ts = new TimeSpan(-5,0,0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Japan:
                    ts = new TimeSpan(+09, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Jordan:
                    ts = new TimeSpan(+02, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Kenya:
                    ts = new TimeSpan(+03, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.KoreaDemocraticPeoplesRepublicOf_NorthKorea:
                    ts = new TimeSpan(+09, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.KoreaRepublicOf_SouthKorea:
                    ts = new TimeSpan(+09, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Kuwait:
                    ts = new TimeSpan(+03, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Kyrgyzstan:
                    ts = new TimeSpan(+06, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Laos:
                    ts = new TimeSpan(+07, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Latvia:
                    ts = new TimeSpan(+02, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Lebanon:
                    ts = new TimeSpan(+02, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Lesotho:
                    ts = new TimeSpan(+02, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Liberia:
                    ts = new TimeSpan(0, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.LibyanArabJamahiriya:
                    ts = new TimeSpan(2, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Liechtenstein:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Lithuania:
                    ts = new TimeSpan(+02, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Luxembourg:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Macao:
                    ts = new TimeSpan(+08, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Macedonia:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Madagascar:
                    ts = new TimeSpan(+03, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Malawi:
                    ts = new TimeSpan(+02, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Malaysia:
                    ts = new TimeSpan(+08, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Maldives:
                    ts = new TimeSpan(+05, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Mali:
                    ts = new TimeSpan(0, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Malta:
                    ts = new TimeSpan(1, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.MarshallIslands:
                    ts = new TimeSpan(+12, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Mauritania:
                    ts = new TimeSpan(0, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Mauritius:
                    ts = new TimeSpan(+04, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Moldova:
                    ts = new TimeSpan(+02, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Monaco:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Montenegro:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Morocco:
                    ts = new TimeSpan(0, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Mozambique:
                    ts = new TimeSpan(0, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Myanmar:
                    ts = new TimeSpan(+06, 30, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Namibia:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Nauru:
                    ts = new TimeSpan(+12, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Nepal:
                    ts = new TimeSpan(+05, 45, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Nicaragua:
                    ts = new TimeSpan(-6,0,0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Niger:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Nigeria:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Norway:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Oman:
                    ts = new TimeSpan(+04, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Pakistan:
                    ts = new TimeSpan(+05, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Palau:
                    ts = new TimeSpan(+09, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Panama:
                    ts = new TimeSpan(-5,0,0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.PapuaNewGuinea:
                    ts = new TimeSpan(+10, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Paraguay:
                    ts = new TimeSpan(-4,0,0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Peru:
                    ts = new TimeSpan(-5,0,0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Philippines:
                    ts = new TimeSpan(+08, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Poland:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Qatar:
                    ts = new TimeSpan(+03, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Romania:
                    ts = new TimeSpan(+02, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Rwanda:
                    ts = new TimeSpan(+02, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.SaintKittsandNevis:
                    ts = new TimeSpan(-4,0,0);
                   
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.SaintLucia:
                    ts = new TimeSpan(-4,0,0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.SaintVincentandtheGrenadines:
                    ts = new TimeSpan(-4,0,0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Samoa:
                    ts = new TimeSpan(+13, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.SanMarino:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.SaoTomeandPrincipe:
                    ts = new TimeSpan(0, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.SaudiArabia:
                    ts = new TimeSpan(+03, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Senegal:
                    ts = new TimeSpan(0, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Serbia:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Seychelles:
                    ts = new TimeSpan(+04, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.SierraLeone:
                    ts = new TimeSpan(0, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Singapore:
                    ts = new TimeSpan(+08, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Slovakia:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Slovenia:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.SolomonIslands:
                    ts = new TimeSpan(+11, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Somalia:
                    ts = new TimeSpan(+03, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.SouthAfrica:
                    ts = new TimeSpan(+02, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Sudan:
                    ts = new TimeSpan(+03, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.SriLanka:
                    ts = new TimeSpan(+05, 30, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Suriname:
                    ts = new TimeSpan(-3,0,0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Swaziland:
                    ts = new TimeSpan(+02, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Sweden:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Switzerland:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.SyrianArabRepublic:
                    ts = new TimeSpan(+02, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Tajikistan:
                    ts = new TimeSpan(+05, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Tanzania:
                    ts = new TimeSpan(+03, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Thailand:
                    ts = new TimeSpan(+07, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Togo:
                    ts = new TimeSpan(0, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Tonga:
                    ts = new TimeSpan(+13, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.TrinidadandTobago:
                    ts = new TimeSpan(-4,0,0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Tunisia:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Turkey:
                    ts = new TimeSpan(+02, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Turkmenistan:
                    ts = new TimeSpan(+05, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Tuvalu:
                    ts = new TimeSpan(+12, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Uganda:
                    ts = new TimeSpan(+03, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Ukraine:
                    ts = new TimeSpan(+02, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.UnitedArabEmirates:
                    ts = new TimeSpan(+04, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Uruguay:
                    ts = new TimeSpan(-03, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Uzbekistan:
                    ts = new TimeSpan(+05, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Vanuatu:
                    ts = new TimeSpan(+11, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.VaticanCityHolySee:
                    ts = new TimeSpan(+01, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Venezuela:
                    ts = new TimeSpan(-4,30,0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Vietnam:
                    ts = new TimeSpan(+07, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Yemen:
                    ts = new TimeSpan(+03, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Zambia:
                    ts = new TimeSpan(+02, 0, 0);
                    break;
                case Enums.ISO_ENUMS.Country_ISO3166.Zimbabwe:
                    ts = new TimeSpan(+02, 0, 0);
                    break;
            }
            if (ts == null)
            {
                if (CS.General_v3.Util.Other.IsLocalTestingMachine)
                {
                    throw new NotImplementedException("Timezone for country '"+countryCode+"' not specified.  Please add it to list");
                }
                else
                {
                    //Do not throw error if online
                    ts = new TimeSpan(0, 0, 0);
                }
            }
            var timezones = GetTimeZonesList();
            TimeZoneInfo tz = null;
            double lastDiffHours = double.MaxValue;
            foreach (var timezone in timezones)
            {
                if (tz == null)
                {
                    tz = timezone;
                }
                else
                {
                    //Compare prev with new
                    double diffHours = Math.Abs(ts.Value.TotalHours - timezone.BaseUtcOffset.TotalHours);
                    if (diffHours < lastDiffHours)
                    {
                        lastDiffHours = diffHours;
                        tz = timezone;
                    }
                    else if (diffHours == lastDiffHours)
                    {
                        //exact same one, update it if display name includes country name
                        if (timezone.DisplayName.ToLower().IndexOf(CS.General_v3.Util.EnumUtils.StringValueOf(countryCode)) != -1)
                        {
                            tz = timezone;
                        }
                    }
                }
            }
            return tz;
        }
    }
}
