﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using CS.General_v3.Controls.FormControls.Common;
namespace CS.General_v3.Util
{
    public static class WindowsForms
    {
        public static class Validation
        {

            private static bool checkControlHasError(Control ctrl)
            {
                if (ctrl is CS.General_v3.Controls.FormControls.Common.MyTxtBox)
                {
                    CS.General_v3.Controls.FormControls.Common.MyTxtBox c = (CS.General_v3.Controls.FormControls.Common.MyTxtBox)ctrl;
                    return c.HasError;
                }
                else if (ctrl is CS.General_v3.Controls.FormControls.Common.MyMaskedTextBox)
                {
                    CS.General_v3.Controls.FormControls.Common.MyMaskedTextBox c = (CS.General_v3.Controls.FormControls.Common.MyMaskedTextBox)ctrl;
                    return c.HasError;
                }
                else if (ctrl is CS.General_v3.Controls.FormControls.Common.MyComboBox)
                {
                    CS.General_v3.Controls.FormControls.Common.MyComboBox c = (CS.General_v3.Controls.FormControls.Common.MyComboBox)ctrl;
                    return c.HasError;
                }
                return false;


            }
            private static Control findControlLeastTabOrderWithError(Control.ControlCollection coll, long leastTabOrder)
            {

                Control ctrl = null;
                for (int i = 0; i < coll.Count; i++)
                {
                    long currTabIndex = CS.General_v3.Util.WindowsForms.CalculateRelativeTabIndex(coll[i]);
                    if (checkControlHasError(coll[i]) && currTabIndex < leastTabOrder)
                    {
                        leastTabOrder = currTabIndex;
                        ctrl = coll[i];
                    }
                    if (coll[i].Controls != null && coll[i].Controls.Count > 0)
                    {
                        Control ctrlInColl = findControlLeastTabOrderWithError(coll[i].Controls, leastTabOrder);
                        if (ctrlInColl != null)
                            currTabIndex = CS.General_v3.Util.WindowsForms.CalculateRelativeTabIndex(ctrlInColl);
                        if (ctrlInColl != null && currTabIndex <= leastTabOrder)
                        {
                            ctrl = ctrlInColl;
                            leastTabOrder = currTabIndex;
                        }

                        /*if (checkControlHasError(coll[i]) && ctrlInColl.TabIndex < leastTabOrder)
                        {
                            leastTabOrder = ctrlInColl.TabIndex;
                            ctrl = ctrlInColl;
                        }*/
                    }
                }
                return ctrl;
            }
            
            public static bool CheckValidation(Control ctrl, string valGroup, ErrorProvider errorProvider)
            {
                bool ok = false;
                if (ctrl != null)
                {
                    if (errorProvider != null)
                        errorProvider.Clear();
                    Control leastTabIndexCtrl;
                    string msg = CS.General_v3.Classes.Validation.ValidateForm(ctrl, valGroup, errorProvider, out leastTabIndexCtrl);
                    if (msg != "")
                    {
                        MessageBox.Show(ctrl, msg, "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        if (leastTabIndexCtrl != null)
                        {
                            if (!leastTabIndexCtrl.Visible)
                            {
                                CS.General_v3.Util.WindowsForms.CheckIfInTabAndShow(leastTabIndexCtrl);
                            }
                            leastTabIndexCtrl.Select();
                            leastTabIndexCtrl.Focus();
                        }
                    }
                    else
                    {
                        ok = true;
                    }
                }
                return ok;
            }

        }

        public static class Events
        {
            public static void AttachGotFocusListener(Control ctrl, EventHandler method)
            {
                ctrl.GotFocus += method;
                if (ctrl.Controls != null && ctrl.Controls.Count > 0)
                {
                    for (int i = 0; i < ctrl.Controls.Count; i++)
                    {
                        AttachGotFocusListener(ctrl.Controls[i], method);
                    }
                }
            }
            public static void AttachLostFocusListener(Control ctrl, EventHandler method)
            {
                ctrl.LostFocus += method;
                if (ctrl.Controls != null && ctrl.Controls.Count > 0)
                {
                    for (int i = 0; i < ctrl.Controls.Count; i++)
                    {
                        AttachLostFocusListener(ctrl.Controls[i], method);
                    }
                }
            }
            public static void AttachMouseEnterListener(Control ctrl, EventHandler method)
            {
                ctrl.MouseEnter += method;
                if (ctrl.Controls != null && ctrl.Controls.Count > 0)
                {
                    for (int i = 0; i < ctrl.Controls.Count; i++)
                    {
                        AttachMouseEnterListener(ctrl.Controls[i], method);
                    }
                }
            }
            public static void DetachMouseEnterListener(Control ctrl, EventHandler method)
            {

                ctrl.MouseEnter -= method;
                if (ctrl.Controls != null && ctrl.Controls.Count > 0)
                {
                    for (int i = 0; i < ctrl.Controls.Count; i++)
                    {
                        DetachMouseEnterListener(ctrl.Controls[i], method);
                    }
                }
            }
            public static void AttachKeyUpListener(Control ctrl, KeyEventHandler method)
            {
                ctrl.KeyUp += method;
                if (ctrl.Controls != null && ctrl.Controls.Count > 0)
                {
                    for (int i = 0; i < ctrl.Controls.Count; i++)
                    {
                        AttachKeyUpListener(ctrl.Controls[i], method);
                    }
                }
            }
            public static void AttachKeyDownListener(Control ctrl, KeyEventHandler method)
            {
                ctrl.KeyDown += method;
                if (ctrl.Controls != null && ctrl.Controls.Count > 0)
                {
                    for (int i = 0; i < ctrl.Controls.Count; i++)
                    {
                        AttachKeyDownListener(ctrl.Controls[i], method);
                    }
                }
            }
            public static void AttachMouseMoveListener(Control ctrl, MouseEventHandler method)
            {
                ctrl.MouseMove += method;
                if (ctrl.Controls != null && ctrl.Controls.Count > 0)
                {
                    for (int i = 0; i < ctrl.Controls.Count; i++)
                    {
                        AttachMouseMoveListener(ctrl.Controls[i], method);
                    }
                }
            }
            public static void AttachMouseClickListener(Control ctrl, MouseEventHandler method)
            {
                
                
                ctrl.MouseClick += method;
                if (ctrl.Controls != null && ctrl.Controls.Count > 0)
                {
                    for (int i = 0; i < ctrl.Controls.Count; i++)
                    {
                        AttachMouseClickListener(ctrl.Controls[i], method);
                    }
                }
            }
            public static void AttachMouseLeaveListener(Control ctrl, EventHandler method)
            {
                ctrl.MouseLeave += method;
                if (ctrl.Controls != null && ctrl.Controls.Count > 0)
                {
                    for (int i = 0; i < ctrl.Controls.Count; i++)
                    {
                        AttachMouseLeaveListener(ctrl.Controls[i], method);
                    }
                }
            }
            public static void AttachMouseWheelListener(Control ctrl, MouseEventHandler method)
            {
                ctrl.MouseWheel += method;
                if (ctrl.Controls != null && ctrl.Controls.Count > 0)
                {
                    for (int i = 0; i < ctrl.Controls.Count; i++)
                    {
                        AttachMouseWheelListener(ctrl.Controls[i], method);
                    }
                }
            }
            public static void DetachMouseWheelListener(Control ctrl, MouseEventHandler method)
            {

                ctrl.MouseWheel -= method;
                if (ctrl.Controls != null && ctrl.Controls.Count > 0)
                {
                    for (int i = 0; i < ctrl.Controls.Count; i++)
                    {
                        DetachMouseWheelListener(ctrl.Controls[i], method);
                    }
                }
            }
            public static void DetachMouseClickListener(Control ctrl, MouseEventHandler method)
            {

                ctrl.MouseClick -= method;
                if (ctrl.Controls != null && ctrl.Controls.Count > 0)
                {
                    for (int i = 0; i < ctrl.Controls.Count; i++)
                    {
                        DetachMouseClickListener(ctrl.Controls[i], method);
                    }
                }
            }
            public static void DetachMouseDownListener(Control ctrl, MouseEventHandler method)
            {

                ctrl.MouseDown -= method;
                if (ctrl.Controls != null && ctrl.Controls.Count > 0)
                {
                    for (int i = 0; i < ctrl.Controls.Count; i++)
                    {
                        DetachMouseDownListener(ctrl.Controls[i], method);
                    }
                }
            }
            public static void DetachMouseMoveListener(Control ctrl, MouseEventHandler method)
            {
                
                ctrl.MouseMove -= method;
                if (ctrl.Controls != null && ctrl.Controls.Count > 0)
                {
                    for (int i = 0; i < ctrl.Controls.Count; i++)
                    {
                        DetachMouseMoveListener(ctrl.Controls[i], method);
                    }
                }
            }
            public static void DetachMouseLeaveListener(Control ctrl, EventHandler method)
            {

                ctrl.MouseLeave -= method;
                if (ctrl.Controls != null && ctrl.Controls.Count > 0)
                {
                    for (int i = 0; i < ctrl.Controls.Count; i++)
                    {
                        DetachMouseLeaveListener(ctrl.Controls[i], method);
                    }
                }
            }
            public static void AttachMouseUpListener(Control ctrl, MouseEventHandler method)
            {
                ctrl.MouseUp += method;
                if (ctrl.Controls != null && ctrl.Controls.Count > 0)
                {
                    for (int i = 0; i < ctrl.Controls.Count; i++)
                    {
                        AttachMouseUpListener(ctrl.Controls[i], method);
                    }
                }
            }
            public static void AttachMouseDownListener(Control ctrl, MouseEventHandler method)
            {
                ctrl.MouseDown += method;
                if (ctrl.Controls != null && ctrl.Controls.Count > 0)
                {
                    for (int i = 0; i < ctrl.Controls.Count; i++)
                    {
                        AttachMouseDownListener(ctrl.Controls[i], method);
                    }
                }
            }
        
        }
        public static class Mouse
        {
            public static class Drag
            {
                private static Control origParent = null;
                private static Control origControl = null;
                private static Form origForm = null;
                private static int origX = 0;
                private static int origY = 0;
                private static int clickX = 0;
                private static int clickY = 0;
                private static int x = 0;
                private static int y = 0;
                private static MouseEventHandler mouseMove = null;
                private static void MoveControl()
                {
                    if (origControl != null)
                    {
                        origControl.Left = x-clickX;
                        origControl.Top = y-clickY;
                    }
                }
                /// <summary>
                /// Starts dragging the control with the mouse
                /// </summary>
                /// <param name="ctrl"></param>
                public static void StartDrag(Control ctrl)
                {
                    StartDrag(ctrl, null);
                }
                /// <summary>
                /// Stops dragging the control with the mouse
                /// </summary>
                /// <param name="ctrl">The control</param>
                /// <param name="e">The mouse event parameters of say the MouseDown event that will start the drag, so that the control is dragged from where it was clicked</param>
                public static void StartDrag(Control ctrl, MouseEventArgs e)
                {
                    if (e == null)
                    {
                        clickX = -2;
                        clickY = -2;
                    }
                    else
                    {
                        clickX = e.X;
                        clickY = e.Y;
                    }
                    origX = ctrl.Left;
                    origY = ctrl.Top;
                    origForm = ctrl.FindForm();
                    origControl = ctrl;
                    Mouse.GetGlobalXY(ctrl, out x, out y);
                    origParent = ctrl.Parent;
                    if (origParent != origForm)
                    { 
                        origParent.Controls.Remove(ctrl);
                        origForm.Controls.Add(ctrl);
                    }
                    ctrl.BringToFront();
                    MoveControl();
                    if (mouseMove == null)
                        mouseMove = new MouseEventHandler(dragMouseMove);
                    Events.AttachMouseMoveListener(origForm, mouseMove);
                }
                /// <summary>
                /// Stops dragging.
                /// </summary>
                /// <param name="ctrl"></param>
                /// <param name="ok">A boolean, showing whether to put the control back to its original place.  If false, place it back, else don't.  Note that the control, once
                /// started dragging is removed from its original container and placed in the form control, to simplify movement</param>
                public static void StopDrag(bool ok)
                {
                    Events.DetachMouseMoveListener(origForm, mouseMove);
                    if (origParent != origForm)
                        origForm.Controls.Remove(origControl);
                    if (!ok)
                    {
                        if (origParent != origForm)
                            origParent.Controls.Add(origControl);
                        origControl.Left = origX;
                        origControl.Top = origY;
                    }
                    origControl = origParent = origForm = null;
                    

                }

                private static void dragMouseMove(object sender, MouseEventArgs e)
                {
                    int origX, origY;
                    Mouse.GetGlobalXY((Control)sender, out origX, out origY);
                    x = origX + e.X;
                    y = origY + e.Y;
                    MoveControl();
                }
                
                
            }
            /// <summary>
            /// Resolves the global X, Y to a local control.
            /// </summary>
            /// <param name="toCtrl">To which control to map</param>
            /// <param name="fromCtrl">From which control</param>
            /// <param name="X">global X</param>
            /// <param name="Y">global Y</param>
            /// <param name="localX">output x</param>
            /// <param name="localY">output y</param>
            public static void GetLocalXY(Control toCtrl, Control fromCtrl, int X, int Y, out int localX, out int localY)
            {
                int globalX, globalY;
                GetGlobalXY(fromCtrl, out globalX, out globalY);
                GetLocalXY(toCtrl, globalX + X, globalY + Y, out localX, out localY);
            }
            /// <summary>
            /// Resolves the global X, Y to a local control.
            /// </summary>
            /// <param name="ctrl">To which control to map</param>
            /// <param name="globalX">global X</param>
            /// <param name="globalY">global Y</param>
            /// <param name="localX">output x</param>
            /// <param name="localY">output y</param>
            public static void GetLocalXY(Control ctrl, int globalX, int globalY, out int localX, out int localY)
            {
                Control curr = ctrl;
                localX = globalX;
                localY = globalY;
                while (curr != null && !(curr is Form))
                {
                    localX -= curr.Left;
                    localY -= curr.Top;
                    curr = curr.Parent;
                }
            }
            public static void GetGlobalXY(Control ctrl, MouseEventArgs e, out int globalX, out int globalY)
            {
                Control curr = ctrl;
                globalX = 0;
                globalY = 0;
                while (curr != null && !(curr is Form))
                {
                    globalX += curr.Left;
                    globalY += curr.Top;
                    curr = curr.Parent;
                }
                if (e != null)
                {
                    globalX += e.X;
                    globalY += e.Y;
                }
            }
            public static void GetGlobalXY(Control ctrl, out int globalX, out int globalY)
            {
                GetGlobalXY(ctrl, null, out globalX, out globalY);
            }
        }

        public static ErrorProvider GetErrorProviderForControl(System.Windows.Forms.Control ctrl)
        {
            System.Windows.Forms.Control curr = ctrl;
            ErrorProvider errorProvider = null;
            while (ctrl != null && errorProvider == null)
            {
                if ((ctrl is MyForm))
                {
                    MyForm form = (MyForm)ctrl;

                    errorProvider = form.ErrorProvider;
                }
                else if (ctrl is MyUserControl)
                {
                    MyUserControl uc = (MyUserControl)ctrl;
                    errorProvider = uc.ErrorProvider;
                }
                ctrl = ctrl.Parent;
            }
            return errorProvider;
        }

        /// <summary>
        /// Updates all controls to reflect their original values as their current text
        /// </summary>
        /// <param name="c"></param>
        public static void UpdateOriginalTextValues(Control c)
        {
            if (string.Compare(c.Name, "txtCancellationDate", true) == 0)
            {
                int k = 5;
            }
            if (c is CS.General_v3.Controls.FormControls.Common.General.IMyFormControl)
            {
                CS.General_v3.Controls.FormControls.Common.General.IMyFormControl formControl = (CS.General_v3.Controls.FormControls.Common.General.IMyFormControl)c;
                if (c.Name == "txtPickUpTime")
                {
                    int k = 5;
                }
                formControl.OriginalValueObject = formControl.FormValueObject;
            }
/*            if (c is CS.General_v3.Controls.FormControls.Common.MyTxtBox)
            {
                CS.General_v3.Controls.FormControls.Common.MyTxtBox txt = (CS.General_v3.Controls.FormControls.Common.MyTxtBox)c;
                txt.OriginalValue = txt.Text;
            }
            else if (c is CS.General_v3.Controls.FormControls.Common.MyMaskedTextBox)
            {
                CS.General_v3.Controls.FormControls.Common.MyMaskedTextBox txt = (CS.General_v3.Controls.FormControls.Common.MyMaskedTextBox)c;
                txt.OriginalText = txt.Text;
            }
            else if (c is CS.General_v3.Controls.FormControls.Common.MyComboBox)
            {
                CS.General_v3.Controls.FormControls.Common.MyComboBox cmb = (CS.General_v3.Controls.FormControls.Common.MyComboBox)c;
                cmb.OriginalText = cmb.Text;
            }
            else if (c is CS.General_v3.Controls.FormControls.Common.MyCheckBox)
            {
                CS.General_v3.Controls.FormControls.Common.MyCheckBox chk = (CS.General_v3.Controls.FormControls.Common.MyCheckBox)c;
                chk.OriginalValue = chk.Checked;
            }
            else if (c is CS.General_v3.Controls.FormControls.Common.MyRadioButton)
            {
                CS.General_v3.Controls.FormControls.Common.MyRadioButton opt = (CS.General_v3.Controls.FormControls.Common.MyRadioButton)c;
                opt.OriginalValue = opt.Checked;
            }*/
                
            if (c.Controls != null)
            {
                for (int i = 0; i < c.Controls.Count; i++)
                {
                    UpdateOriginalTextValues(c.Controls[i]);
                }
            }
        }

        /// <summary>
        /// Returns whether hitter Control is on checkControl 
        /// </summary>
        /// <param name="checkControl"></param>
        /// <param name="hitterControl"></param>
        /// <returns></returns>
        public static bool HitTest(Control checkControl, Control hitterControl)
        {
            int Ax1, Ax2, Ay1, Ay2, Bx1, Bx2, By1, By2;
            Mouse.GetGlobalXY(checkControl, out Bx1, out  By1);
            Mouse.GetGlobalXY(hitterControl, out  Ax1, out  Ay1);
            Bx2 = Bx1 + checkControl.Width;
            By2 = By1 + checkControl.Height;
            Ax2 = Ax1 + hitterControl.Width;
            Ay2 = Ay1 + hitterControl.Height;
            bool hit =((Ax1 <= Bx2) && (Ax2 >= Bx1) && (Ay2 >= By1) && (Ay1 <= By2));
            return hit;

        }
        /// <summary>
        /// Returns whether hitter Control is on checkControl 
        /// </summary>
        /// <param name="checkControl"></param>
        /// <param name="hitterControl"></param>
        /// <returns></returns>
        public static bool HitTest(Control checkControl, int x, int y)
        {
            int Bx1, Bx2, By1, By2;
            Mouse.GetGlobalXY(checkControl, out  Bx1, out By1);
            Bx2 = Bx1 + checkControl.Width;
            By2 = By1 + checkControl.Height;
            bool hit = ((x >= Bx1) && (x <= Bx2) && (y >= By1) && (y <= By2));
            return hit;

        }
        public static long CalculateRelativeTabIndex(Control ctrl)
        {
            Control curr = ctrl;
            Stack<Control> stack = new Stack<Control>();
            while (curr != null && !(curr is Form))
            {
                stack.Push(curr);
                curr = curr.Parent;
            }
            long result = 0;
            long levelMultiplier = 10000000000000000;
            while (stack.Count > 0)
            {
                curr = stack.Pop();
                int currTabIndex = curr.TabIndex + 1;
                result += (currTabIndex * levelMultiplier);
                if (levelMultiplier >= 100)
                    levelMultiplier /= 100;
            }
            return result;
        }
        /// <summary>
        /// Attaches method as a OnKeyUp event delegate to the control, and all subcontrols. 
        /// </summary>
        /// <param name="ctrl">Control</param>
        /// <param name="method">The method to be called</param>
        public static void AttachKeyUpListener(Control ctrl, KeyEventHandler method)
        {
            Events.AttachKeyUpListener(ctrl, method);
        }
        public static void ChangeControlEnabled(Control ctrl, bool enabled)
        {
            ChangeControlEnabled(ctrl, enabled, null );
        }
        public static void ChangeControlEnabled(Control ctrl, bool enabled,Color? backColor)
        {
            ctrl.Enabled = enabled;
            if (backColor != null && (ctrl is TextBox || ctrl is ComboBox || ctrl is MaskedTextBox))
                ctrl.BackColor = backColor.Value;
            for (int i = 0; i < ctrl.Controls.Count; i++)
            {
                ChangeControlEnabled(ctrl.Controls[i], enabled,backColor);
            }
        }
        public static Control FindControl(Control ctrl, string name)
        {
            if (ctrl.Name.ToLower() == name.ToLower())
                return ctrl;
            else
            {
                if (ctrl.Controls != null)
                    for (int i = 0; i < ctrl.Controls.Count; i++)
                    {
                        Control c = FindControl(ctrl.Controls[i], name);
                        if (c != null)
                            return c;
                    }
            }
            return null;
        }
        public static bool CheckModified(Control ctrl)
        {
            if (ctrl is CS.General_v3.Controls.FormControls.Common.MyTxtBox)
            {
                CS.General_v3.Controls.FormControls.Common.MyTxtBox txt = (CS.General_v3.Controls.FormControls.Common.MyTxtBox)ctrl;
                if (!txt.NotPartOfForm && txt.IsModified)
                    return true;
            }
            else if (ctrl is CS.General_v3.Controls.FormControls.Common.MyMaskedTextBox)
            {
                CS.General_v3.Controls.FormControls.Common.MyMaskedTextBox txt = (CS.General_v3.Controls.FormControls.Common.MyMaskedTextBox)ctrl;
                if (!txt.NotPartOfForm && txt.IsModified)
                    return true;
            }
            else if (ctrl is CS.General_v3.Controls.FormControls.Common.MyComboBox)
            {
                CS.General_v3.Controls.FormControls.Common.MyComboBox cmb = (CS.General_v3.Controls.FormControls.Common.MyComboBox)ctrl;
                if (!cmb.NotPartOfForm && cmb.IsModified)
                    return true;

            }
            else if (ctrl is CS.General_v3.Controls.FormControls.Common.MyCheckBox)
            {
                CS.General_v3.Controls.FormControls.Common.MyCheckBox chk = (CS.General_v3.Controls.FormControls.Common.MyCheckBox)ctrl;
                if (!chk.NotPartOfForm && chk.IsModified)
                    return true;
            }
            else if (ctrl is CS.General_v3.Controls.FormControls.Common.MyRadioButton)
            {
                CS.General_v3.Controls.FormControls.Common.MyRadioButton opt = (CS.General_v3.Controls.FormControls.Common.MyRadioButton)ctrl;
                if (!opt.NotPartOfForm && opt.IsModified)
                    return true;
            }

            if (ctrl.Controls != null && ctrl.Controls.Count > 0)
            {
                for (int i = 0; i < ctrl.Controls.Count; i++)
                {
                    if (CheckModified(ctrl.Controls[i]))
                        return true;
                }
            }
            return false;
            
        }
        /// <summary>
        /// Checks if the control is in a tab page, and if so, show the tab page in question
        /// </summary>
        /// <param name="control"></param>
        public static void CheckIfInTabAndShow(Control control)
        {
            Control curr = control;
            while (curr != null && !(curr is TabPage))
            {
                curr = curr.Parent;
            }
            if (curr != null)
            {
                TabPage tbPage = (TabPage)curr;
                TabControl tbControl = (TabControl)curr.Parent;
                tbControl.SelectedTab = tbPage;
            }
        }
        
        public static void ClearControl(Control ctrl)
        {
            if (ctrl is CS.General_v3.Controls.FormControls.Common.MyTxtBox)
            {
                CS.General_v3.Controls.FormControls.Common.MyTxtBox txt = (CS.General_v3.Controls.FormControls.Common.MyTxtBox)ctrl;
                txt.Clear();
            }
            else if (ctrl is CS.General_v3.Controls.FormControls.Common.MyComboBox)
            {
                CS.General_v3.Controls.FormControls.Common.MyComboBox combo = (CS.General_v3.Controls.FormControls.Common.MyComboBox)ctrl;
                combo.Clear();
            }
            else if (ctrl is CS.General_v3.Controls.FormControls.Common.MyCheckBox)
            {
                CS.General_v3.Controls.FormControls.Common.MyCheckBox chk = (CS.General_v3.Controls.FormControls.Common.MyCheckBox)ctrl;
                chk.Clear();
            }
            else if (ctrl is CS.General_v3.Controls.FormControls.Common.MyMaskedTextBox)
            {
                CS.General_v3.Controls.FormControls.Common.MyMaskedTextBox txt = (CS.General_v3.Controls.FormControls.Common.MyMaskedTextBox)ctrl;
                txt.Clear();
            }
            if (ctrl.Controls != null)
                for (int i = 0; i < ctrl.Controls.Count; i++)
                {
                    ClearControl(ctrl.Controls[i]);
                }
            
        }
        public static string InputBox(string title, string message, string initText)
        {
            CS.General_v3.Controls.FormControls.Common.frmInputBox frm = new CS.General_v3.Controls.FormControls.Common.frmInputBox();
            string s = frm.ShowDialog(title, message, initText);
            frm.Dispose();
            return s;
        }
    }
}
