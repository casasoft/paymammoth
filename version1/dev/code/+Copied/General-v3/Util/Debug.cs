using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Reflection;
using log4net;


namespace CS.General_v3.Util
{
    public static class Debug
    {
        private static ILog _log = log4net.LogManager.GetLogger(typeof(Debug));
       /* private static string _Path = "";
        public static string Path
        {
            get { return _Path; }
            set
            {
                _Path = value ?? "";
                _Path = _Path.Replace("/", "\\");
                _Path = _Path.Replace(@"\\", @"\");
            }
        }*/
        public static void WriteLine(string line)
        {
            WriteLine(line, true);
        }
        public static void WriteLine(string line, bool appendTimeStamp)
        {
           
            //FileStream fs = new FileStream(Path, FileMode.Append);
            //StreamWriter writer = new StreamWriter(fs);
            string[] lines = line.Split(new string[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);

            for (int i = 0; i < lines.Length; i++)
            {
                string s = lines[i];
                if (appendTimeStamp)
                {
                    s = CS.General_v3.Util.Date.Now.ToString("dd/MM/yyyy HH:mm:ss: ") + s;
                }
                _log.Debug(s);
                //writer.WriteLine(s);
            }
            /*writer.Close();

            fs.Close();
            writer.Dispose();
            fs.Dispose();*/
            
        }
        public static string GetStackTrace()
        {
            StackTrace callStack = new StackTrace();  
            StackFrame frame = callStack.GetFrame(1);  
            MethodBase method = frame.GetMethod();  
            string declaringType = method.DeclaringType.Name;  
            string methodName = method.Name;
            return callStack.ToString();
        }
    }
}
