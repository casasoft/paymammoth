﻿using System;
using System.Collections.Generic;


using System.Security.Cryptography;
using System.Text;

namespace CS.General_v3.Util
{
    public static class Cryptography
    {
        public class HashResult
        {
            public string Hash { get; set; }
            public string Salt { get; set; }
        }

        public static bool CheckHashedString(string unhashedString, string storedHashedString, int totalIterations,string salt, CS.General_v3.Enums.PASSWORD_ENCRYPTION_TYPE encType)
        {
            var result = HashString(unhashedString, totalIterations, salt, encType);
            return (result.Hash == storedHashedString);
            
        }

        /// <summary>
        /// Returns a hash result, with the password and salt
        /// </summary>
        /// <param name="strToHash">Password to hash</param>
        /// <param name="totalIterations">Total iterations to derive hash</param>
        /// <param name="salt">salt to use.  If left null, a random salt is used</param>
        /// <param name="encType">Encryption type</param>
        /// <returns>Hash Result with hashed password and salt</returns>
        public static HashResult HashString(string strToHash, int totalIterations = 20480, string salt = null, CS.General_v3.Enums.PASSWORD_ENCRYPTION_TYPE encType= Enums.PASSWORD_ENCRYPTION_TYPE.PBKDF2)
        {
            HashResult result = new HashResult();
                        
            switch (encType)
            {
                case Enums.PASSWORD_ENCRYPTION_TYPE.PBKDF2:
                    {
                        if (salt == null)
                            salt = CS.General_v3.Util.Random.GetHexString(32);;
                        result.Hash = GetPBKDF2HashWithHexSaltAsString(strToHash, salt, iterationsToDeriveKey: totalIterations);
                        result.Salt = salt;
                        break;
                    }
                case Enums.PASSWORD_ENCRYPTION_TYPE.PlainText:
                default:
                    {
                        result.Hash = strToHash;

                        break;
                    }
            }
            return result;

        }
        public static string GetPBKDF2HashWithAsciiSaltAsString(string strToHash, string saltInHex, int bytesToReturn = 32, int iterationsToDeriveKey = 20480)
        {


            byte[] by = Conversion.ConvertStringToByteArray(saltInHex);
            return GetPBKDF2HashAsString(strToHash, by, bytesToReturn, iterationsToDeriveKey);
        }
        public static string GetPBKDF2HashWithHexSaltAsString(string strToHash, string saltInHex, int bytesToReturn = 32, int iterationsToDeriveKey = 20480)
        {


            byte[] by = Conversion.ConvertHexStringToByteArray(saltInHex);
            return GetPBKDF2HashAsString(strToHash, by, bytesToReturn, iterationsToDeriveKey);
        }
        public static string GetPBKDF2HashAsString(string strToHash, byte[] salt, int bytesToReturn = 32, int iterationsToDeriveKey = 20480)
        {
            

            

            var byHashed = GetPBKDF2Hash(strToHash, salt, bytesToReturn, iterationsToDeriveKey);
            string sHash = BitConverter.ToString(byHashed).Replace("-", string.Empty);
            return sHash;
        }

        public static byte[] GetPBKDF2Hash(string strToHash, byte[] salt, int bytesToReturn = 32, int iterationsToDeriveKey = 20480)
        {
            if (iterationsToDeriveKey <= 0)
                iterationsToDeriveKey = 20480;

            Rfc2898DeriveBytes k3 = new Rfc2898DeriveBytes(strToHash, salt, iterationsToDeriveKey);
            byte[] result = k3.GetBytes(bytesToReturn);
            return result;
        }
        

        public static string GetSHA1Hash(string s)
        {
            byte[] by = System.Text.Encoding.UTF8.GetBytes(s);
            byte[] byHashed = GetSHA1Hash(by);
            string sHash = BitConverter.ToString(byHashed).Replace("-", string.Empty);
            return sHash;

        }
        public static string GetSHA512Hash(string s)
        {
            byte[] by = System.Text.Encoding.UTF8.GetBytes(s);
            byte[] byHashed = GetSHA512Hash(by);
            string sHash = BitConverter.ToString(byHashed).Replace("-", string.Empty);
            return sHash;
        }
        public static string GetSHA384Hash(string s)
        {
            byte[] by = System.Text.Encoding.UTF8.GetBytes(s);
            byte[] byHashed = GetSHA384Hash(by);
            string sHash = BitConverter.ToString(byHashed).Replace("-", string.Empty);
            return sHash;
        }

        public static byte[] GetSHA1Hash(byte[] by)
        {
            var sha = new SHA1CryptoServiceProvider();
            

            byte[] hash = sha.ComputeHash(by);
            return hash;
        }

        public static byte[] GetSHA512Hash(byte[] by)
        {
            var sha = new SHA512CryptoServiceProvider();

            
            byte[] hash = sha.ComputeHash(by);
            return hash;
        }
        public static byte[] GetSHA384Hash(byte[] by)
        {
            var sha = new SHA384CryptoServiceProvider();


            byte[] hash = sha.ComputeHash(by);
            return hash;
        }

        public static string GetMd5Sum(string str)
        {
            MD5 md5Hasher = MD5.Create();
            
            byte[] data = md5Hasher.ComputeHash(System.Text.Encoding.Default.GetBytes(str));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sb.Append(data[i].ToString("x2"));
            }
            return sb.ToString();
        }
        
        public static string GetMd5SumUnicode(string str)
        {
            // First we need to convert the string into bytes, which
            // means using a text encoder.
            Encoder enc = System.Text.Encoding.Unicode.GetEncoder();

            // Create a buffer large enough to hold the string
            byte[] unicodeText = new byte[str.Length * 2];
            enc.GetBytes(str.ToCharArray(), 0, str.Length, unicodeText, 0, true);

            // Now that we have a byte array we can ask the CSP to hash it
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] result = md5.ComputeHash(unicodeText);

            // Build the final string by converting each byte
            // into hex and appending it to a StringBuilder
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                sb.Append(result[i].ToString("X2"));
            }

            // And return it
            return sb.ToString();
        }

    }
}
