﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.Email;
using log4net;

namespace CS.General_v3.Util
{
    public static class EmailUtil
    {
        public static string LastErrorMessage = "";
        public delegate void EmailSentHandler(EmailMessage msg);
        public delegate void EmailsSentHandler(List<EmailMessage> emails);


        public const string SIGNATURE_TAG = "[SIGNATURE]";
        public static string GetDomainFromEmail(string email)
        {
            string host, domain;
            SplitEmailAddressIntoParts(email, out host, out domain);
            return domain;
        }

        public static void SplitEmailAddressIntoParts(string email, out string host, out string domain)
        {
            host = "";
            domain = "";
            if (!string.IsNullOrEmpty(email))
            {
                int atPos = email.IndexOf('@');
                if (atPos > -1)
                {
                    host = email.Substring(0, atPos);
                    domain = email.Substring(atPos + 1);
                }
            }


        }
        public static string EmailFolder = "includes/emails/"; //include the last slash

        public static string ParseDefaultEmailTokens(string emailTxt)
        {
            emailTxt = CS.General_v3.Util.Text.ReplaceTexts(emailTxt, CS.General_v3.Util.Date.Now.ToString("dd MMM yyyy"), "[DATE_NORMAL]", "[DATE]");
            emailTxt = CS.General_v3.Util.Text.ReplaceTexts(emailTxt, CS.General_v3.Util.Date.Now.ToString("dd/MM/yyyy"), "[DATE_SHORT]");
            emailTxt = CS.General_v3.Util.Text.ReplaceTexts(emailTxt, CS.General_v3.Util.Date.Now.ToString("HH:mm"), "[TIME]");
            emailTxt = CS.General_v3.Util.Text.ReplaceTexts(emailTxt, CS.General_v3.Util.Date.Now.ToString("dd/MM/yyyy hh:mm tt"), "[DATE_TIME]");
            return emailTxt;
        }




    //    public delegate void SendEmailResult(string FromEmail, string FromName, string ToEmailStr, string ToName,
    //        string replyToEmail, string replyToName,
    //         string Subject, string PlainText, string HTMLText, IEnumerable<string> Attachments, IEnumerable<string> Resources,
    //        string host, int port, string user, string pass, bool isSecure, OperationResult result);
    //    public static event SendEmailResult OnSendEmailSyncReady;
    //    private static void dispatchOnSendEmailSyncReady(string FromEmail, string FromName, string ToEmailStr, string ToName,
    //        string replyToEmail, string replyToName,
    //         string Subject, string PlainText, string HTMLText, IEnumerable<string> Attachments, IEnumerable<string> Resources,
    //        string host, int port, string user, string pass, bool isSecure, OperationResult result)
    //    {
    //        if (OnSendEmailSyncReady != null)
    //        {
    //            OnSendEmailSyncReady(FromEmail, FromName, ToEmailStr, ToName, replyToEmail, replyToName, Subject, PlainText, HTMLText, Attachments, Resources, host, port,
    //                user, pass, isSecure, result);
    //        }
    //    }


    //    /// <summary>
    //    /// Sends the email. Blocking
    //    /// </summary>
    //    /// <param name="Sender"></param>
    //    /// <param name="SenderEmail"></param>
    //    /// <param name="Recipient"></param>
    //    /// <param name="RecipientEmail"></param>
    //    /// <param name="Subject"></param>
    //    /// <param name="PlainText"></param>
    //    /// <param name="HTMLText"></param>
    //    /// <param name="Attachments"></param>
    //    /// <param name="Resources"></param>
    //    /// <param name="host"></param>
    //    /// <param name="port"></param>
    //    /// <param name="user"></param>
    //    /// <param name="pass"></param>
    //    /// <returns></returns>
    //    public static OperationResult SendEmailSync(string FromEmail, string FromName, string ToEmailStr, string ToName,
    //        string replyToEmail, string replyToName,
    //         string Subject, string PlainText, string HTMLText, IList<Attachment> Attachments, IList<LinkedResource> Resources,
    //        string host, int port, string user, string pass, bool isSecure, bool throwErrorOnException = false)
    //    {
    //        OperationResult result = new OperationResult();
    //        List<string> attachmentsStrList = new List<string>();
    //        List<string> resourcesStrList = new List<string>();

    //        ToEmailStr = ToEmailStr ?? "";
    //        //bool allOK = false;
    //        string[] toEmailArray = ToEmailStr.Split(new string[] { ",", "|" }, StringSplitOptions.RemoveEmptyEntries);

    //        try
    //        {
    //            if (toEmailArray.Length > 0)
    //            {
    //                string mainToEmail = toEmailArray[0];
    //                mainToEmail = mainToEmail.Trim();

    //                if (!string.IsNullOrWhiteSpace(mainToEmail))
    //                {
    //                    if (_log.IsInfoEnabled)
    //                    {
    //                        StringBuilder sb = new StringBuilder();
    //                        sb.AppendLine("MAIL_SENDER: Sending Email - '" + FromName + "' <" + FromEmail + ">, Recipient - '" + ToName + "' <" + mainToEmail + ">");
    //                        sb.AppendLine("Subject: " + Subject);
    //                        sb.AppendLine("Host: " + host);
    //                        sb.AppendLine("Port: " + port);
    //                        sb.AppendLine("User: " + user);

    //                        string hiddenPass = "";
    //                        if (pass.Length > 0)
    //                            hiddenPass += pass.Substring(0, 1);
    //                        hiddenPass += CS.General_v3.Util.Text.RepeatText("*", pass.Length - 2);
    //                        if (pass.Length >= 2)
    //                            hiddenPass += pass.Substring(pass.Length - 1, 1);

    //                        sb.AppendLine("Pass: " + hiddenPass);

    //                        _log.Info(sb.ToString());

    //                    }
    //                    MailAddress from, to;
    //                    if (string.IsNullOrEmpty(FromEmail)) FromEmail = CS.General_v3.Settings.Emails.SentEmailsFromEmail;
    //                    if (string.IsNullOrEmpty(FromName)) FromName = CS.General_v3.Settings.Emails.SentEmailsFromName;

    //                    if (!string.IsNullOrEmpty(FromName))
    //                        from = new MailAddress(FromEmail, FromName);
    //                    else
    //                        from = new MailAddress(FromEmail);
    //                    if (!string.IsNullOrEmpty(ToName))
    //                        to = new MailAddress(mainToEmail, ToName);
    //                    else
    //                        to = new MailAddress(mainToEmail);
    //                    MailMessage msg = new MailMessage(from, to);
    //                    for (int i = 1; i < toEmailArray.Length; i++)
    //                    {
    //                        //add any other emails as Bcc (blind-carbon-copy)
    //                        var email = toEmailArray[i];
    //                        if (email != null)
    //                        {
    //                            email = email.Trim();
    //                            msg.Bcc.Add(new MailAddress(email, ToName));
    //                        }
    //                    }
    //                    if (!string.IsNullOrEmpty(PlainText))
    //                    {
    //                        AlternateView viewPlain = AlternateView.CreateAlternateViewFromString(PlainText, null, "text/plain");
    //                        msg.AlternateViews.Add(viewPlain);
    //                    }
    //                    if (!string.IsNullOrEmpty(HTMLText))
    //                    {
    //                        AlternateView viewHTML = AlternateView.CreateAlternateViewFromString(HTMLText, null, "text/html");
    //                        if (Resources != null)
    //                        {
    //                            for (int i = 0; i < Resources.Count; i++)
    //                            {
    //                                var r = Resources[i];
    //                                long len = 0;
    //                                if (r.ContentStream != null)
    //                                    len = r.ContentStream.Length;
    //                                resourcesStrList.Add("Content Id: " + r.ContentId + ", Content Type: " + r.ContentType + ", Size: " + (len / 1024).ToString("#,0.00") + " KB");

    //                                viewHTML.LinkedResources.Add(Resources[i]);
    //                            }
    //                        }
    //                        msg.AlternateViews.Add(viewHTML);

    //                    }
    //                    if (Attachments != null)
    //                    {
    //                        for (int i = 0; i < Attachments.Count; i++)
    //                        {
    //                            var a = Attachments[i];
    //                            long len = 0;
    //                            if (a.ContentStream != null)
    //                                len = a.ContentStream.Length;
    //                            attachmentsStrList.Add(a.Name + " (Content Type: " + a.ContentType + ", Content Id: " + a.ContentId + ", Size: " + (len / 1024).ToString("#,0.00") + " KB)");
    //                            msg.Attachments.Add(a);
    //                        }

    //                    }

    //                    msg.Subject = Subject;
    //                    if (!string.IsNullOrEmpty(replyToEmail))
    //                    {
    //                        msg.ReplyToList.Add(new MailAddress(replyToEmail, replyToName));
    //                    }

    //                    int retryTimes = 3;
    //                    bool ok = false;
    //                    for (int i = 0; i < retryTimes; i++)
    //                    {
    //                        try
    //                        {
    //                            SmtpClient smtp = new SmtpClient(host, port);
    //                            smtp.Timeout = 90 * 1000; //5 minutes
    //                            //smtp.UseDefaultCredentials = true;
    //                            smtp.UseDefaultCredentials = false;
    //                            smtp.EnableSsl = isSecure;
    //                            if (!string.IsNullOrEmpty(user))
    //                            {
    //                                smtp.UseDefaultCredentials = true;
    //                                NetworkCredential userInfo = new NetworkCredential(user, pass);
    //                                smtp.Credentials = userInfo;
    //                            }



    //                            smtp.Send(msg);
    //                            if (_log.IsInfoEnabled) _log.Info("MAIL_SENDER: Email sent successfully");
    //                            ok = true;

    //                        }
    //                        catch (Exception ex)
    //                        {
    //                            result.AddException(ex);
    //                            _log.Error("MAIL_SENDER: Error trying to send email - retry #" + retryTimes, ex);
    //                            if (throwErrorOnException)
    //                            {
    //                                retryTimes = 0;
    //                                throw;
    //                            }
    //                            else
    //                            {
    //                                ok = false;
    //                                retryTimes--;
    //                            }
    //                        }
    //                        if (ok || retryTimes < 0)
    //                        {
    //                            if (!ok)
    //                            {
    //                                result.AddStatusMsg(Enums.STATUS_MSG_TYPE.Error, "Email coult not be sent - stopped trying");
    //                                _log.Error("MAIL_SENDER: Email could not be sent, stopped trying");
    //                            }
    //                            break;
    //                        }
    //                    }
                       

    //                    msg.Attachments.Clear();
    //                    msg.Dispose();
    //                }
    //                else
    //                {
    //                    result.AddStatusMsg(Enums.STATUS_MSG_TYPE.Error, "No email address defined!");
    //                }
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            result.AddException(new InvalidOperationException("Could not send email due to a general error", ex));
    //        }
    //        //clear up
    //        if (Attachments != null)
    //        {
    //            for (int i = 0; i < Attachments.Count; i++)
    //            {
    //                Attachments[i].Dispose();
    //            }
    //        }
    //        if (Resources != null)
    //        {
    //            for (int i = 0; i < Resources.Count; i++)
    //            {
    //                Resources[i].Dispose();
    //            }
    //        }

    //        dispatchOnSendEmailSyncReady(FromEmail, FromName, ToEmailStr, ToName, replyToEmail, replyToName, Subject,
    //                    PlainText, HTMLText, attachmentsStrList, resourcesStrList, host, port, user, pass, isSecure, result);



    //        return result;
    //    }
    //    private static ILog _log = log4net.LogManager.GetLogger(typeof(EmailUtil));
       
    //    /// <summary>
    //    /// Sends the email. Blocking
    //    /// </summary>
    //    /// <param name="Sender"></param>
    //    /// <param name="SenderEmail"></param>
    //    /// <param name="Recipient"></param>
    //    /// <param name="RecipientEmail"></param>
    //    /// <param name="Subject"></param>
    //    /// <param name="PlainText"></param>
    //    /// <param name="HTMLText"></param>
    //    /// <param name="Attachments"></param>
    //    /// <param name="Resources"></param>
    //    /// <returns></returns>
    //    public static OperationResult SendEmailSync(string FromEmail, string FromName, string ToEmail, string ToName,
    //string replyToEmail, string replyToName, string Subject, string PlainText, string HTMLText, string AttachmentPath, bool throwErrorOnException = false)
    //    {
    //        List<Attachment> attachmentList = null;
    //        if (!string.IsNullOrEmpty(AttachmentPath))
    //        {
    //            attachmentList = new List<Attachment>();
    //            attachmentList.Add(new Attachment(AttachmentPath));
    //        }
    //        return SendEmailSync(FromEmail, FromName, ToEmail, ToName, replyToEmail, replyToName, Subject, PlainText, HTMLText, attachmentList, null,
    //        throwErrorOnException: throwErrorOnException);
    //    }
    //    /// <summary>
    //    /// Sends the email. Blocking
    //    /// </summary>
    //    /// <param name="Sender"></param>
    //    /// <param name="SenderEmail"></param>
    //    /// <param name="Recipient"></param>
    //    /// <param name="RecipientEmail"></param>
    //    /// <param name="Subject"></param>
    //    /// <param name="PlainText"></param>
    //    /// <param name="HTMLText"></param>
    //    /// <param name="Attachments"></param>
    //    /// <param name="Resources"></param>
    //    /// <returns></returns>
    //    public static OperationResult SendEmailSync(string FromEmail, string FromName, string ToEmail, string ToName,
    //        string replyToEmail, string replyToName,
    //        string Subject, string PlainText, string HTMLText, IList<Attachment> Attachments, IList<LinkedResource> Resources, bool throwErrorOnException = false)
    //    {
    //        string host, user, pass;
    //        int port = Enums.SETTINGS_ENUM.Email_SMTP_Port.GetSettingFromDatabase<int>();
    //        host = Enums.SETTINGS_ENUM.Email_SMTP_Host.GetSettingFromDatabase();

    //        user = Enums.SETTINGS_ENUM.Email_SMTP_User.GetSettingFromDatabase();
    //        pass = Enums.SETTINGS_ENUM.Email_SMTP_Pass.GetSettingFromDatabase();
    //        bool isSecure = Enums.SETTINGS_ENUM.Email_SMTP_UseSecureSettings.GetSettingFromDatabase<bool>();
    //        return SendEmailSync(FromEmail, FromName, ToEmail, ToName, replyToEmail, replyToName, Subject, PlainText, HTMLText, Attachments, Resources, host, port, user, pass, isSecure,
    //        throwErrorOnException: throwErrorOnException);
    //    }


    //    /// <summary>
    //    /// Sends an email
    //    /// </summary>
    //    /// <param name="Sender">Sender's Name</param>
    //    /// <param name="SenderEmail">Sender email address</param>
    //    /// <param name="Recipient">Recipient's Name</param>
    //    /// <param name="RecipientEmail">Recipient email address</param>
    //    /// <param name="Subject">Subject</param>
    //    /// <param name="PlainText">Plain Text</param>
    //    /// <param name="HTMLText">HTML Email </param>
    //    /// <param name="Attachments">Attachments of email</param>
    //    /// <param name="Resources">Linked resources for HTML View.  Do not forget to set also the content ID and content type!</param>
    //    /// <returns>Whether email was sent successfully or not</returns>
    //    public static bool SendEmailAsync(string SenderEmail, string Sender, string RecipientEmail, string Recipient,
    //        string replyToEmail, string replyToName,
    //        string Subject, string PlainText, string HTMLText, IList<Attachment> Attachments, IList<LinkedResource> Resources, System.Threading.ThreadPriority threadPriority = System.Threading.ThreadPriority.BelowNormal)
    //    {
    //        return SendEmailAsync(SenderEmail, Sender, RecipientEmail, Recipient, replyToEmail, replyToName,

    //            Subject, PlainText, HTMLText, Attachments, Resources,null,null,null,null,
    //            threadPriority);


    //    }

    //    /// <summary>
    //    /// Sends an email
    //    /// </summary>
    //    /// <param name="Sender">Sender's Name.  If left null, default value taken from settings</param>
    //    /// <param name="SenderEmail">Sender email address.If left null, default value taken from settings</param>
    //    /// <param name="Recipient">Recipient's Name</param>
    //    /// <param name="RecipientEmail">Recipient email address</param>
    //    /// <param name="Subject">Subject</param>
    //    /// <param name="PlainText">Plain Text</param>
    //    /// <param name="HTMLText">HTML Email </param>
    //    /// <param name="Attachments">Attachments of email</param>
    //    /// <param name="Resources">Linked resources for HTML View.  Do not forget to set also the content ID and content type!</param>
    //    /// <returns>Whether email was sent successfully or not</returns>
    //    public static bool SendEmailAsync(string SenderEmail, string Sender, string RecipientEmail, string Recipient,
    //        string replyToEmail, string replyToName,
    //        string Subject, string PlainText, string HTMLText, IList<Attachment> Attachments, IList<LinkedResource> Resources,
    //        string host, int? port, string user, string pass, System.Threading.ThreadPriority threadPriority = System.Threading.ThreadPriority.BelowNormal,
    //        EmailSentHandler sentFunc = null)
    //    {
    //        if (host == null) host = Enums.SETTINGS_ENUM.Email_SMTP_Host.GetSettingFromDatabase();
    //        if (port == null) port = Enums.SETTINGS_ENUM.Email_SMTP_Port.GetSettingFromDatabase<int>();
    //        if (user == null) user = Enums.SETTINGS_ENUM.Email_SMTP_User.GetSettingFromDatabase();
    //        if (pass == null) pass = Enums.SETTINGS_ENUM.Email_SMTP_Pass.GetSettingFromDatabase();
                
    //        if (_log.IsInfoEnabled)
    //        {
    //            StringBuilder sb = new StringBuilder();
    //            sb.AppendLine("Sending Email Async. Sender - '" + Sender + "' <" + SenderEmail + ">, Recipient - '" + Recipient + "' <" + RecipientEmail + ">");
    //            sb.AppendLine("Subject: " + Subject);
    //            _log.Info(sb.ToString());

    //        }

    //        if (SenderEmail == null) SenderEmail = Settings.Emails.SentEmailsFromEmail;
    //        if (Sender == null) Sender = Settings.Emails.SentEmailsFromName;
    //        if (replyToEmail == null)
    //        {
    //            replyToEmail = Settings.Emails.SentEmailsReplyToEmail;
    //            if (string.IsNullOrEmpty(replyToEmail)) replyToEmail = null;
    //        }

    //        if (replyToName == null)
    //        {
    //            replyToName = Settings.Emails.SentEmailsReplyToName;
    //            if (string.IsNullOrEmpty(replyToName)) replyToName = null;
    //        }


    //        if (port == null || port <= 0)
    //            port = 25;
    //        if (!RecipientEmail.Contains("@"))
    //        {
    //            throw new InvalidOperationException("CS.General_v3.Email.sendEmail:: Cannot call this method without specifying a valid email in 'recipient email' <" + RecipientEmail + ">.  Note that this method has switched parameters over the normal queueEmail, and the order is Sender Name, Sender Email, Recp. Name, Recp.Email, and not vice versa");

    //        }
    //        EmailMessage msg = new EmailMessage();
    //        msg.Sender = Sender;
    //        msg.SenderEmail = SenderEmail;
    //        msg.Subject = Subject;
    //        msg.RecipientEmail = RecipientEmail;
    //        msg.Recipient = Recipient;
    //        msg.PlainText = PlainText;
    //        msg.HTMLText = HTMLText;
    //        msg.Attachments = Attachments;
    //        msg.Resources = Resources;
    //        msg.Port = port.Value;
    //        msg.Host = host;
    //        msg.Pass = pass;
    //        msg.User = user;
    //        msg.ReplyToEmail = replyToEmail;
    //        msg.ReplyToName = replyToName;
    //        if (sentFunc != null)
    //            msg.EmailSent += sentFunc;
    //        System.Threading.ThreadStart threadStart = new System.Threading.ThreadStart(msg.SendViaNormal_Threading);
    //        System.Threading.Thread thread = new System.Threading.Thre3ad(threadStart);
    //        thread.Priority = threadPriority;
    //        thread.Start();
    //        return true;



    //    }



       



    }
}
