using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.Web;
using System.Collections.Specialized;
using System.Web.Routing;
using System.Web.SessionState;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using System.Runtime.Serialization;
using log4net;
using CS.General_v3.Classes.URL;
using System.Web.Caching;

namespace CS.General_v3.Util
{
    public static class PageUtil
    {
      
        public static string HtmlEncode(string URL)
        {
            return Text.HtmlEncode(URL);
            
        }


      
      
      
        //public static string GetQueryStringVariable(string id)
        //{
            
        //    string s = null;
        //    var qs = GetRequestQueryString();
        //    if (string.IsNullOrEmpty(id))
        //    {
        //        s = qs.ToString();
        //    }
        //    else
        //    {
        //        s = qs[id];
        //        if (s == "")
        //        {
        //            id = id.ToLower();
        //            s = null;
        //            for (int i = 0; i < qs.AllKeys.Length; i++)
        //            {
        //                string currKey = qs.AllKeys[i].ToLower();
        //                if (id == currKey)
        //                {
        //                    s = "";
        //                    break;
        //                }
        //            }
        //        }
        //    }
        //    return s;
            
        //}
        
        //public static string GetBaseUrlFromRequest(HttpRequest req)
        //{
        //    HttpRequest Request = req;
           
        //    string link = null;
        //    if (Request.ServerVariables["HTTP_HOST"] != "")
        //    {
        //        link = Request.ServerVariables["HTTP_HOST"];
        //    }
        //    else
        //    {
        //        link = Request.ServerVariables["HTTP_ADDR"];
        //    }
        //    return link;
        //}
        

        
        /// <summary>
        /// Returns the location url.  E.g http://www.maltacarsonline.com/cars/index.aspx?category=Vehicles
        /// /cars/index.aspx?category=Vehicles

        /// </summary>
        /// <param name="getAbsoluteURL">If absolute url, returns it with the http:// etc, else it is relativer</param>
        
        /// <returns></returns>
        public static URLClass GetCurrentUrlLocation()
        {
            string url = GetUrlFromCurrentRequest();
            URLClass urlClass = new URLClass(url, loadCurrentURLIfNull: false);
            return urlClass;
        }
        public static URLClass GetCurrentUrlLocation2()
        {
            string url = GetUrlFromCurrentRequest();
            URLClass urlClass = new URLClass(url, loadCurrentURLIfNull: false);
            return urlClass;
        }
        //    //if (!string.IsNullOrWhiteSpace(CS.General_v3.Settings.Others.BaseUrlCustom_ApplicationWide))
        //    //{
        //    //    link = CS.General_v3.Settings.Others.BaseUrlCustom_ApplicationWide;
        //    //}
        //    //else if (!string.IsNullOrWhiteSpace(CS.General_v3.Settings.Others.BaseUrlCustom_SessionWide))
        //    //{
        //    //    link = CS.General_v3.Settings.Others.BaseUrlCustom_SessionWide;
        //    //}
        //    //else
        //    //{
                
        //    //    if (loadFromDatabaseIfPossible)
        //    //    { //this will load the website from the database, if it specified
        //    //        string dbWebsiteBaseURL = null;
        //    //        dbWebsiteBaseURL = CS.General_v3.Settings.GetSettingFromDatabase(Enums.SETTINGS_ENUM.Others_WebsiteBaseUrl);
        //    //        link = dbWebsiteBaseURL;
        //    //    }

        //    //    if (string.IsNullOrWhiteSpace(link) && CheckRequestExistsAtContext())
        //    //    {
        //    //        link = GetBaseUrlFromCurrentRequest();

        //    //    }
        //    //    if (string.IsNullOrWhiteSpace(link))
        //    //    {
        //    //        link = _baseUrlOfApplicationFromFirstSessionStart;
        //    //    }

        //    //    if (_baseUrlOfApplicationFromFirstSessionStart == null) _baseUrlOfApplicationFromFirstSessionStart = link;

        //    //}




        //    //bool linkIsSSL = link != null && link.ToLower().StartsWith("https://");


        //    //if (port == null && link != null && link.Contains(":"))
        //    //{
        //    //    int startColonSearch = 0;
        //    //    if (link.ToLower().StartsWith("http://")) startColonSearch = "http://".Length;
        //    //    if (link.ToLower().StartsWith("https://")) startColonSearch = "https://".Length;


        //    //    int colonPos = link.IndexOf(":", startColonSearch);
        //    //    if (colonPos > -1)
        //    //    {
        //    //        int slashPos = link.IndexOf("/", colonPos);
        //    //        if (slashPos == -1)
        //    //            slashPos = link.Length;
        //    //        string sPort = link.Substring(link.IndexOf(":") + 1, slashPos - colonPos - 1);
        //    //        port = Convert.ToInt32(sPort);
        //    //    }

        //    //}

        //    //if ((useSsl.GetValueOrDefault() && !linkIsSSL) || (!useSsl.GetValueOrDefault() && linkIsSSL))
        //    //    port = null;

        //    //if (port == null)
        //    //{
        //    //    port = (!useSsl.GetValueOrDefault() ? 80 : 443);
        //    //}
        //    //return GetApplicationBaseUrl(link, useSSL, port, doNotIncludeHttpOrHttps: doNotIncludeHttpOrHttps);






        //    //string path = GetUrlLocation();
        //    //if (!string.IsNullOrEmpty(CS.General_v3.Util.PageUtil.GetRequestQueryString().ToString()))
        //    //    path += "?" + CS.General_v3.Util.PageUtil.GetRequestQueryString().ToString();

        //    //if (getAbsoluteURL)
        //    //{
        //    //    path = ConvertRelativeUrlToAbsoluteUrl(path);
        //    //}
        //    //return path;


        //}
        
        
        //public static TDataType GetContextObject<TDataType>(string key, bool throwErrorIfContextDoesNotExist = true)
        //{
        //    TDataType result = default(TDataType);
        //    if (!CS.General_v3.Settings.Others.IsUsingTestingFramework && CS.General_v3.Util.Other.IsWebApplication)
        //    {
        //        if (throwErrorIfContextDoesNotExist || HttpContext.Current != null)
        //            result = (TDataType)GetHttpContext().Items[key];
        //    }
        //    else
        //        result = (TDataType)Test_Context[key];
        //        return result;

        //}

        

        //public static T RQEnum<T>(string key, T defaultValue) where T:struct, IConvertible
        //{
        //    return CS.General_v3.Util.EnumUtils.GetEnumByStringValue<T>(RQ(key), defaultValue);

        //}
        //public static List<T> RQEnumList<T>(string key) where T : struct, IConvertible
        //{
        //    return CS.General_v3.Util.EnumUtils.GetListOfEnumsByValuesFromString<T>(RQ(key));

        //}
        
        /**************************
         ****************************/


        private static Page _CurrentPage
        {
            get
            {
                return (Page)CS.General_v3.Util.PageUtil.GetContextObject("___CurrentPage");
            }
            set
            {
                CS.General_v3.Util.PageUtil.SetContextObject("___CurrentPage", value);
            }
        }
        public static void SetCurrentPage(Page pg)
        {
            _CurrentPage = pg;
        }
        public static Page GetCurrentPage()
        {
            if (CheckRequestExistsAtContext())
            {

                return _CurrentPage;
            }
            else
            {
                return null;
            }
        }


        public static string UrlDecode(string s)
        {
            return System.Web.HttpUtility.UrlDecode(s);
        }

        public static string UrlEncode(string URL)
        {
            return System.Web.HttpUtility.UrlEncode(URL);
        }
        public static string HtmlEncode(string s, bool convertUnicodeToHtmlEquivalent = true)
        {



            string result = System.Web.HttpUtility.HtmlEncode(s);
            if (convertUnicodeToHtmlEquivalent && !string.IsNullOrEmpty(result))
            {
                result = result.Replace("�", "&pound;");
                result = result.Replace("�", "&euro;");
                result = result.Replace("�", "&yen;");

            }
            return result;
        }
        public static string HtmlDecode(string s)
        {
            return System.Web.HttpUtility.HtmlDecode(s);
        }


        public static bool CheckUserAgentForKnownSearchEngineBots()
        {
            List<string> list = new List<string>();
            list.Add("Google");
            list.Add("msnbot");
            list.Add("yahoo");
            list.Add("spider");
            list.Add("jeeves");
            list.Add("crawler");
            list.Add("bot");
            list.Add("bing");
            list.Add("facebook");
            list.Add("sbider");

            string regex = "(" + CS.General_v3.Util.Text.AppendStrings("|", list) + ")";
            string userAgent = CS.General_v3.Util.PageUtil.GetCurrentRequest().UserAgent;
            return (userAgent == null || Regex.IsMatch(userAgent, regex, RegexOptions.IgnoreCase));

        }


        public static void CheckFromPageWhetherBrowserSupportsCookies()
        {
            bool qsParam = (CS.General_v3.Util.PageUtil.GetVariableFromQuerystring(QS_PARAM_COOKIE_ENABLED) == QS_PARAM_COOKIE_ENABLED);
            bool cookieValue = (CS.General_v3.Util.PageUtil.GetCookie(QS_PARAM_COOKIE_ENABLED) == QS_PARAM_COOKIE_ENABLED);
            CS.General_v3.Util.PageUtil.SetCookie(QS_PARAM_COOKIE_ENABLED, QS_PARAM_COOKIE_ENABLED);
            if (cookieValue)
            {
                PageUtil._cookiesEnabledContext = true;
                if (qsParam)
                { //remove the ugly querystring param
                    var pg = PageUtil._CurrentPage;

                    CS.General_v3.Classes.URL.URLClass url = new General_v3.Classes.URL.URLClass();
                    url.Remove(QS_PARAM_COOKIE_ENABLED);
                    string urlStr = url.GetURL();
                    //pg.Response.Redirect(urlStr, true);
                    url.RedirectTo();
                }
            }
            else if ((!cookieValue && qsParam) || (PageUtil.CheckUserAgentForKnownSearchEngineBots()))
            //if doesnt contain cookie, but contains the querystring param, it means that the browser does not support cookie. If search engine bot, assume also that they don't support cookies
            {
                PageUtil._cookiesEnabledContext = false;
            }
            else if (!cookieValue && !qsParam) //if no cookie, and no qs param, set cookie & redirect 
            {
                CS.General_v3.Classes.URL.URLClass url = new General_v3.Classes.URL.URLClass();

                url[QS_PARAM_COOKIE_ENABLED] = QS_PARAM_COOKIE_ENABLED;
                var pg = PageUtil._CurrentPage;
                string urlStr = url.GetURL();
                //pg.Response.Redirect(urlStr, true);
                url.RedirectTo();

            }

        }


        private const string COOKIES_CHECK_NAME = "CookiesEnabledCheck";
        public static void WriteCookieToCheckIfCookiesAreEnabled()
        {
            DateTime d = Date.Now.AddHours(1);
            SetCookie(COOKIES_CHECK_NAME, "Yes", d);//expires in 1 hour
        }
        public static bool CheckIfCookiesAreEnabled()
        {
            bool enabled = false;
            if (CS.General_v3.Util.PageUtil.CheckRequestExistsAtContext())
            {
                
                enabled = _cookiesEnabledContext.GetValueOrDefault();
                
            }
            return enabled;

            //if (_cookiesEnabledContext.HasValue)
            //    return _cookiesEnabledContext.GetValueOrDefault();
            //else
            //    throw new InvalidOperationException("At this stage, there is no way one can know if cookies are enabled or not! Please wait until BasePage.OnPreInit has been called, before doing this check");
        }
        /*
        /// <summary>
        /// Iterates through all controls in head control, and if it finds any script tags with the src attribute starting with '~/', it gets resolved to root
        /// </summary>
        /// <param name="head"></param>
        public static void ResolveJavaScriptFilesInHead(Control head)
        {
            foreach (Control c in head.Controls)
            {
                if (c is LiteralControl)
                {
                    LiteralControl lc = (LiteralControl)c;

                    if (lc.Text != null && lc.Text.Contains("<script"))
                    {
                        string text = lc.Text;

                        int startTagPos = 0;
                        int endTagPos = -1;
                        while (text.IndexOf("<script", endTagPos + 1) > -1)
                        {
                            startTagPos = text.IndexOf("<script", endTagPos + 1);
                            endTagPos = text.IndexOf("/>", startTagPos);
                            if (endTagPos == -1)
                                endTagPos = text.IndexOf("</script>", startTagPos);
                            if (endTagPos > -1)
                            {
                                int srcPos = text.IndexOf("src=\"", startTagPos);
                                if (srcPos > startTagPos && srcPos < endTagPos)
                                {
                                    int startQuote = text.IndexOf('"', srcPos);
                                    int endQuote = text.IndexOf('"', startQuote + 1);
                                    if (startQuote > -1 && endQuote > -1 && startQuote > startTagPos && startQuote < endTagPos && endQuote > startTagPos && endQuote < endTagPos)
                                    {
                                        string value = text.Substring(startQuote + 1, endQuote - startQuote - 1);
                                        if (value != null && value.Substring(0, 2) == "~/")
                                        {
                                            value = CS.General_v3.Util.PageUtil.GetRoot() + value.Substring(2);
                                        }
                                        text = text.Remove(startQuote + 1, endQuote - startQuote - 1);
                                        text = text.Insert(startQuote + 1, value);
                                    }
                                }
                            }
                        }
                        lc.Text = text;
                    }


                }
            }*
        }*/

        /// <summary>
        /// Sends a renew session image
        /// </summary>
        public static void SendRenewSessionImage()
        {
            byte[] gif = {0x47,0x49,0x46,0x38,0x39,0x61,0x01,0x00,0x01,0x00,0x91,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x21,0xf9,0x04,0x09,0x00,
0x00,0x00,0x00,0x2c,0x00,0x00,0x00,0x00,0x01,0x00,0x01,0x00,0x00,0x08,0x04,
0x00,0x01,0x04,0x04,0x00,0x3b,0x00};

            System.Web.HttpResponse Response = PageUtil.CurrentHttpResponse;
            Response.ContentType = CS.General_v3.Util.Data.GetMIMEContentType(".gif");
            //Response.AddHeader("ContentType", "image/gif");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(gif);
            Response.End();
        }

        public static string SessionKey
        {
            get
            {
                return CurrentHttpRequest.Path.Replace("/", "_");
            }
        }


        private static readonly Dictionary<string, object> _applicationInfo = new Dictionary<string, object>();

        public static object GetApplicationInfoObject(string key)
        {
            object o = null;
            if (CS.General_v3.Util.Other.IsWebApplication)
            {
                o = GetHttpContext().Application[key];
            }
            else
            {
                try
                {
                    o = _applicationInfo[key];
                }
                catch
                {

                }
            }
            return o;
        }
        public static void SetApplicationInfo(string key, object o)
        {
            if (CS.General_v3.Util.Other.IsWebApplication)
            {
                GetHttpContext().Application[key] = o;
            }
            else
            {
                _applicationInfo[key] = o;
            }
        }

        private static HttpRequest CurrentHttpRequest
        {
            get
            {
                HttpRequest req = null;
                try
                {
                    if (GetHttpContext() != null)
                    {
                        req = GetHttpContext().Request;

                    }

                }
                catch (Exception ex)
                {
                    req = null;
                }
                if (req != null && CS.General_v3.Classes.Application.AppInstance.Instance != null && 
                    !CS.General_v3.Classes.Application.AppInstance.Instance.IsFirstSessionStartCalled())
                {
                    throw new InvalidOperationException("Request should not be obtained before the first SessionStart is called, as although it is available in Development enviroment, it will NOT be available in IIS.  Kindly " +
            "move such calls to a later request, or fill in the 'BaseUrl' setting");
                }
                return req;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cssFilePath">Just the file path location</param>
        /// <param name="index"></param>
        /// <param name="media"></param>
        public static void RegisterCSSInHeadOfPage(string cssFilePath, int? index = null, string media="all")
        {
            string script = "<link href='"+cssFilePath+"' rel='stylesheet' type='text/css' media='"+media+"' />";
            RegisterScriptInHeadOfPage(script, cssFilePath, index);
        }
        public static void RegisterJavaScriptInHeadOfPage(string javaScriptFilePath, int? index = null)
        {
            string script =  "<script src='"+javaScriptFilePath+"'></script>";
            
            RegisterScriptInHeadOfPage(script, javaScriptFilePath, index);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="script"></param>
        /// <param name="key">If you specify key, script will be registered only once per context, irrespective of the amount of calls</param>
        /// <param name="index"></param>
        public static void RegisterScriptInHeadOfPage(string script, string key, int? index = null)
        {
            bool isAlreadyRegistered = false;
            if (!string.IsNullOrWhiteSpace(key))
            {
                isAlreadyRegistered = CS.General_v3.Util.PageUtil.GetContextObject<bool>(key);
            }
            if (!isAlreadyRegistered)
            {
                CS.General_v3.Util.PageUtil.SetContextObject(key, true);
                if (index.HasValue)
                {
                    CS.General_v3.Util.PageUtil.GetCurrentPage().Header.Controls.AddAt(0, new Literal() {Text = script});
                }
                else
                {
                    CS.General_v3.Util.PageUtil.GetCurrentPage().Header.Controls.Add(new Literal() {Text = script});

                }
            }
        }

        public static void RedirectPage(string url = null)
        {
            if (string.IsNullOrEmpty(url)) url = GetCurrentUrlLocation().ToString();
            CurrentHttpResponse.Redirect(url, true);
        }

        private static HttpResponse CurrentHttpResponse
        {
            get
            {
                HttpResponse resp = null;
                if (GetHttpContext() != null)
                    resp = GetHttpContext().Response;
                if (resp != null && CS.General_v3.Classes.Application.AppInstance.Instance != null && !CS.General_v3.Classes.Application.AppInstance.Instance.IsFirstSessionStartCalled())
                {
                    throw new InvalidOperationException("Response should not be obtained before the first SessionStart is called, as although it is available in Development enviroment, it will NOT be available in IIS.  Kindly " +
            "move such calls to a later request, or fill in the 'BaseUrl' setting");
                }
                return resp;
            }
        }
        private static HttpServerUtility Server
        {
            get
            {
                return GetHttpContext().Server;
            }
        }
        /// <summary>
        /// Changes a path into the relative path but from the root.  For example if current page is in
        /// /admin/data/index.aspx and you resolve ../images/smiley.jpg, it will return '/admin/images/smiley.jpg'
        /// </summary>
        /// <param name="path">Path to resolve</param>
        /// <returns>Resolved path</returns>
        public static string RelativeMapPath(string path)
        {
            string currPath = CurrentHttpRequest.Path;

            string tmp = path;
            if (tmp.Length >= 1 && tmp.Substring(0, 1) == "/")
            {
                //from root
                currPath = tmp;

            }
            else
            {
                int lastSlash = currPath.LastIndexOf('/');
                currPath = currPath.Substring(0, lastSlash);

                int backPos = 0;
                while (tmp.IndexOf("../") > -1)
                {
                    backPos = tmp.IndexOf("../");
                    lastSlash = currPath.IndexOf('/');
                    if (lastSlash == -1)
                    {
                        throw new InvalidOperationException("Cannot go out of root directory!");
                    }
                    currPath = currPath.Substring(0, lastSlash);
                    tmp = tmp.Substring(backPos + 3);
                }
                currPath += "/" + tmp;
            }

            return currPath;

        }

        private static string ReplaceAllImagesToSSLUrl(string imgPath)
        {

            string path = imgPath;
            if (path == null)
                path = "";
            if (path != "")
            {
                if (path.Length >= 7 && (path.Substring(0, 7).ToLower() == "http://"))
                {
                    path = path.Replace("http://", "https://");
                }
                else if ((path.Length >= 7 && (path.Substring(0, 7).ToLower() != "http://")) &&
                    (path.Length >= 8 && (path.Substring(0, 8).ToLower() != "https://")))
                {
                    if (path.Length >= 1 && path.Substring(0, 1) == "/")
                    {
                        path = path.Remove(0, 1);
                    }
                    string relPath = RelativeMapPath(path).Substring(1);

                    var url = GetApplicationBaseUrl();
                    url.UseSsl = true;
                    path = url.GetURL(fullyQualified: true, includeScheme: false);
                }
            }
            return path;

        }
        public static void RedirectParentUsingJavaScript(System.Web.UI.Page pg, string location)
        {
            
            string funcName = "__redirectParent";

            StringBuilder js = new StringBuilder();
            js.AppendLine("function " + funcName + "()");
            js.AppendLine("{");
            //js.AppendLine("     window.parent.window.location = '" + location + "';");
            js.AppendLine("     window.top.location = '" + location + "';");
            js.AppendLine("}");
            //js.AppendLine("window.setTimeout('" + funcName + "();', 250);");
            //js.AppendLine("window.setTimeout('" + funcName + "();', 500);");
            //js.AppendLine("window.setTimeout('" + funcName + "();', 1000);");
            //js.AppendLine("window.setTimeout('" + funcName + "();', 2000);");
            //js.AppendLine("window.setTimeout('" + funcName + "();', 3000);");
            //js.AppendLine("window.setTimeout('" + funcName + "();', 4000);");
            //js.AppendLine("window.setTimeout('" + funcName + "();', 6000);");
            js.AppendLine(funcName + "();");
            pg.ClientScript.RegisterStartupScript(pg.GetType(), funcName, js.ToString(), true);

        }
        public static void RedirectPageUsingJavaScript(System.Web.UI.Page pg, string location)
        {
            string funcName = "__redirectPage";

            StringBuilder js = new StringBuilder();
            js.AppendLine("function " + funcName + "()");
            js.AppendLine("{");
            js.AppendLine("     window.location = '" + location + "';");
            js.AppendLine("}");
            //js.AppendLine("window.setTimeout('" + funcName + "();', 250);");
            //js.AppendLine("window.setTimeout('" + funcName + "();', 500);");
            //js.AppendLine("window.setTimeout('" + funcName + "();', 1000);");
            //js.AppendLine("window.setTimeout('" + funcName + "();', 2000);");
            //js.AppendLine("window.setTimeout('" + funcName + "();', 3000);");
            //js.AppendLine("window.setTimeout('" + funcName + "();', 4000);");
            //js.AppendLine("window.setTimeout('" + funcName + "();', 6000);");
            js.AppendLine(funcName + "();");
            pg.ClientScript.RegisterStartupScript(pg.GetType(), funcName, js.ToString(), true);

        }
        public static void ReplaceAllImagesToSSL(Control ctrl)
        {
            Queue q = new Queue();
            q.Enqueue(ctrl);

            while (q.Count > 0)
            {
                Control curr = (Control)q.Dequeue();
                if (curr != null)
                {
                    if (curr is CS.General_v3.Controls.WebControls.Common.MyImage)
                    {
                        CS.General_v3.Controls.WebControls.Common.MyImage img = (CS.General_v3.Controls.WebControls.Common.MyImage)curr;
                        img.ImageUrl = ReplaceAllImagesToSSLUrl(img.ImageUrl);
                        img.RollOverImage = ReplaceAllImagesToSSLUrl(img.RollOverImage);
                    }
                    /*else if (curr is CS.General_v3.Controls.WebControls.Common.MyImageButton)
                    {
                        CS.General_v3.Controls.WebControls.Common.MyImageButton img = (CS.General_v3.Controls.WebControls.Common.MyImageButton)curr;
                        img.ImageUrl = ReplaceAllImagesToSSLUrl(img.ImageUrl);
                        img.RollOverImage = ReplaceAllImagesToSSLUrl(img.RollOverImage);
                    }*/
                    else if (curr is System.Web.UI.WebControls.Image)
                    {
                        System.Web.UI.WebControls.Image img = (System.Web.UI.WebControls.Image)curr;
                        img.ImageUrl = ReplaceAllImagesToSSLUrl(img.ImageUrl);
                    }
                    else if (curr is HtmlImage)
                    {
                        HtmlImage img = (HtmlImage)curr;
                        img.Src = ReplaceAllImagesToSSLUrl(img.Src);

                    }
                    for (int i = 0; i < curr.Controls.Count; i++)
                    {
                        q.Enqueue(curr.Controls[i]);
                    }
                }
            }
        }



        /// <summary>
        /// This method is used for a very strange situation when you have a literal '+' in 
        /// the URL.  The problem is that the browser interprets a '+' as a space in the URL
        /// when there is a URL encoded value.  If there isn't a URL encoded value, then the
        /// browser interprets the '+' as a literal '+'.
        /// 
        /// This method will URL encode the URL, and if there is no special character,
        /// it will replace the '+' with a ' ' again.
        /// 
        /// Greetings bro
        /// </summary>
        /// <param name="URL"></param>
        /// <returns></returns>
        public static string URLEncodeEntireURL(string URL)
        {

            string protocol = "";
            string qs = "";
            if (URL.Contains("://"))
            {
                protocol = URL.Substring(0, URL.IndexOf("://") + 3);
                URL = URL.Substring(protocol.Length);
            }
            if (URL.Contains("?"))
            {
                qs = URL.Substring(URL.IndexOf("?"));

                URL = URL.Substring(0, URL.IndexOf("?"));
            }

            string[] folders = URL.Split("/".ToCharArray(), StringSplitOptions.None);
            URL = "";
            for (int i = 0; i < folders.Length; i++)
            {
                if (i > 0) URL += "/";
                URL += Server.UrlEncode(folders[i]);
            }

            if (!URL.Contains("%"))
            {
                URL = URL.Replace("+", " ");
            }
            string fullURL = protocol + URL + qs;
            return fullURL;
        }
        public static bool IsUserUsingFakeIP()
        {
            return !string.IsNullOrWhiteSpace(GetUserFakeIP(false)) || !string.IsNullOrWhiteSpace(GetUserFakeIP(true));
        }

        public static string GetUserFakeIP(bool ipv4 = true)
        {
            string fakeIP = null;
            bool isLocalhost = CS.General_v3.Util.Other.IsLocalTestingMachine;

            bool isLoggedInToCMS = CS.General_v3.Classes.Application.AppInstance.Instance != null ? CS.General_v3.Classes.Application.AppInstance.Instance.IsCurrentUserLoggedInCms() : false;
            
            //CmsUserSessionLogin.Instance.i
            if (isLocalhost || isLoggedInToCMS)
            {
                //Either user is localost or else logged in to CMS
                fakeIP = ipv4 ? CS.General_v3.Settings.Others.UserFakeIPV4Address : CS.General_v3.Settings.Others.UserFakeIPV6Address;
            }
            return fakeIP;
        }

        public static string GetUserIP(bool ipv4 = true)
        {
            string fakeIP = GetUserFakeIP(ipv4);
            if (!string.IsNullOrWhiteSpace(fakeIP))
            {
                return fakeIP;
            }




            string IP = null;
            if (CurrentHttpRequest != null)
            {
                IP = CurrentHttpRequest.UserHostAddress;
                if (!ipv4)
                {
                    IP = CS.General_v3.Util.IPAddressUtil.ConvertIPV4ToIPV6(IP);
                }


            }
            if (ipv4 && IP == "::1")
            {
                IP = "127.0.0.1";
            }
            if (CS.General_v3.Settings.Others.IsUsingTestingFramework && IP == null)
            {
                IP = "192.168.1.1";
            }
            return IP;
        }

        /// <summary>
        /// Returns the last portion, the page name. i.e http://www.maltacarsonline.com/folder/download.asp
        /// returns 'download.asp'
        /// </summary>
        /// <param name="pg"></param>
        /// <returns></returns>
        public static string GetPageName()
        {
            HttpRequest Request = CurrentHttpRequest;
            string page = Request.Path;
            page = page.Substring(page.LastIndexOf("/") + 1);
            return page;

        }

        public static string GetAbsoluteImageURLFromHTML(string HTMLText)
        {
            if (!string.IsNullOrWhiteSpace(HTMLText))
            {
                var r = new Regex("src=\"(.*?)\"");
                Match m = r.Match(HTMLText);
                string imgPath = m.Groups[1].Value;
                if ((imgPath != null) && (imgPath != ""))
                {
                    if (!imgPath.StartsWith("http://") && !imgPath.StartsWith("https://"))
                    {
                        imgPath = CS.General_v3.Util.PageUtil.ConvertRelativeUrlToAbsoluteUrl(imgPath);
                    }
                    return imgPath;
                }
            }
            return null;
        }


        public static void PermanentRedirect(string newURL)
        {
            if (!newURL.StartsWith("http://", true, null) && !newURL.StartsWith("https://", true, null))
            {
                if (newURL.StartsWith("/")) newURL = newURL.Substring(1);
                newURL = Util.PageUtil.GetApplicationBaseUrl() + newURL;

            }
            CurrentHttpResponse.RedirectPermanent(newURL);
            //            Response.Status = "301 Moved Permanently";
            //          Response.AddHeader("Location", newURL);
            //        Response.End();
        }
        public static bool CheckRequestExistsAtContext()
        {
            bool ok = true;
            try
            {
                ok = (GetHttpContext() != null && CurrentHttpRequest != null);

            }
            catch (InvalidOperationException ex)
            {
                if (!ex.Message.ToLower().Contains("Request should not be obtained before the first SessionStart is called".ToLower()))
                    throw;
                ok = false;
            }
            catch (HttpException ex)
            {
                if (!ex.Message.ToLower().Contains("is not available in this context".ToLower()))
                    throw;
                ok = false;
            }
            return ok;
        }
        public static bool CheckResponseExistsAtContext()
        {
            bool ok = true;
            try
            {
                ok = (GetHttpContext() != null && CurrentHttpResponse != null);
            }
            catch (InvalidOperationException ex)
            {
                if (!ex.Message.ToLower().Contains("Response should not be obtained before the first SessionStart is called".ToLower()))
                    throw;
                ok = false;
            }
            catch (HttpException ex)
            {
                if (!ex.Message.ToLower().Contains("is not available in this context".ToLower()))
                    throw ex;
                ok = false;
            }
            return ok;
        }




        /// <summary>
        /// Returns the value of a cookie, as an integer
        /// </summary>
        /// <param name="pg">The page</param>
        /// <param name="name">The name of the cookie</param>
        /// <returns>The value of the cookie</returns>
        public static int GetCookieInt(string name)
        {

            string ret = GetCookie(name);
            int iret = 0;
            Int32.TryParse(ret, out iret);
            return iret;
        }

        public static long? GetCookieLongNullable(string name, string subkey = null)
        {
            string s = GetCookie(name, subkey);
            long? result = null;
            long l;
            if (long.TryParse(s, out l))
                result = l;
            return result;
        }


        public static TOutputType GetCookie<TOutputType>(string name, string subkey = null)
        {
            string s = GetCookie(name, subkey);
            return CS.General_v3.Util.Other.ConvertStringToBasicDataType<TOutputType>(s);

        }
        /// <summary>
        /// Returns the value of a cookie and it's subkey
        /// </summary>
        /// <param name="pg">The page</param>
        /// <param name="name">The name of the cookie</param>
        /// <param name="subkey">The name of the subkey of the cookie</param>
        /// <returns>The value of the cookie</returns>
        public static string GetCookie(string name, string subkey = null)
        {
            string ret = "";
            if (!CS.General_v3.Settings.Others.IsUsingTestingFramework)
            {
                HttpRequest Request = CurrentHttpRequest;

                HttpCookie cookie = Request.Cookies[name];

                if (cookie != null)
                {
                    if (subkey != null)
                    {
                        ret = cookie[subkey];
                    }
                    else
                    {
                        ret = cookie.Value;
                    }


                    if (ret == null)
                    {
                        ret = "";
                    }
                }
            }
            else
            {
                string cookieTmpName = GetTestCookieName(name, subkey);
                ret = Test_Cookies[cookieTmpName];
            }
            return ret;
        }

        /// <summary>
        /// Returns the value of a cookie and it's subkey, as an integer
        /// </summary>
        /// <param name="pg">The page</param>
        /// <param name="name">The name of the cookie</param>
        /// <param name="subkey">The name of the subkey of the cookie</param>
        /// <returns>The value of the cookie</returns>
        public static int GetCookieInt(string name, string subkey)
        {
            string sret = GetCookie(name, subkey);
            int iret = 0;
            Int32.TryParse(sret, out iret);
            return iret;
        }

        /// <summary>
        /// Returns the domain part of the URL, for example for the rul http://www.casasoft.com.mt/cikku/test.html will return 'www.casasoft.com.mt'
        /// </summary>
        /// <param name="fullUrl">If null, takes current url</param>
        /// <returns></returns>
        public static string GetDomainFromUrl(string fullUrl, bool removePort = false)
        {
            
            var url = GetApplicationBaseUrl();
            return url.GetURL(includeScheme: false, fullyQualified: false, appendQueryString: false);
            //if (fullUrl == null) fullUrl = GetApplicationBaseUrl();
            //string domain = fullUrl;
            //int indexOf = fullUrl.IndexOf("://");
            //if (indexOf > -1)
            //{
            //    domain = fullUrl.Substring(indexOf + "://".Length);
            //}
            //if (!string.IsNullOrEmpty(domain))
            //{
            //    indexOf = domain.IndexOf("/");
            //    if (indexOf > -1)
            //        domain = domain.Substring(0, indexOf);
            //}
            //{
            //    int colonPos = domain.IndexOf(":");

            //    if (removePort && colonPos > -1)
            //    {
            //        domain = domain.SubStr(0, colonPos - 1);
            //    }
            //}
            //return domain;
        }

        public static void SetCookieWithDomain(string name, object value, string domain = null)
        {
            SetCookie(name, null, value, null, domain);
        }
        public static void SetCookieByTimeSpan(string name, object value, TimeSpan duration, string domain = null)
        {
            DateTime date = DateTime.Now.AddSeconds(duration.TotalSeconds);
            SetCookie(name, null, value, date, domain);
        }


        /// <summary>
        /// Sets a cookie value. If it does not exist, it is created.  Expiry
        /// date is set for 1 week
        /// </summary>
        /// <param name="pg">The page</param>
        /// <param name="name">The name of the cookie</param>
        /// <param name="value">The value of the cookie</param>
        public static void SetCookie(string name, string value, DateTime? expiryDate = null)
        {
            
            SetCookie(name, null, value, expiryDate);
            

        }

        public static void SetCookie(string name, string subkey, object val, DateTime? cookieExpiryDate = null, string domain = null)
        {
            //Domains must have at least two dots to be eligible
            if (!string.IsNullOrEmpty(domain) && CS.General_v3.Util.Text.CountCharactersInString(domain, "\\.") < 2)
            {
                domain = null;
            }

            string value = CS.General_v3.Util.Other.ConvertBasicDataTypeToString(val);
            if (!CS.General_v3.Settings.Others.IsUsingTestingFramework)
            {
                HttpRequest Request = CurrentHttpRequest;
                HttpResponse Response = CurrentHttpResponse;
                HttpCookie cookie = Request.Cookies[name];

                if (cookie != null)
                {
                    Request.Cookies.Remove(cookie.Name);
                    Response.Cookies.Remove(cookie.Name);
                }
                DateTime dt = new DateTime();
                if (cookieExpiryDate.HasValue)
                    dt = cookieExpiryDate.Value;
                else
                {
                    dt = CS.General_v3.Util.Date.Now.AddDays(90);
                }
                if (cookie == null)
                {
                    cookie = new HttpCookie(name);
                }
                if (!string.IsNullOrEmpty(subkey))
                {
                    cookie[subkey] = value;
                }
                else
                {
                    cookie.Value = value;
                }
                if (!string.IsNullOrEmpty(domain))
                {
                    cookie.Domain = domain;
                }
                cookie.Expires = dt;
                
                Request.Cookies.Add(cookie);
                Response.Cookies.Add(cookie);
                
            }
            else
            {
                string cookieTmpName = GetTestCookieName(name, subkey);
                Test_Cookies[cookieTmpName] = value;
            }
        } 

        ///// <summary>
        ///// Sets a subkey of a cookie to a value. If it does not exist, it is created.  Expiry
        ///// date is set for 90 days
        ///// </summary>
        ///// <param name="pg">The page</param>
        ///// <param name="name">The name of the cookie</param>
        ///// <param name="subkey">The subkey of the cookie</param>
        ///// <param name="value">The value of the cookie</param>
        ///// <param name="cookieExpiryDate">This is the expirty of the WHOLE cookie and not just for this sub key so handle with care :)</param>
        //public static void SetCookie(string name, string subkey, string value, DateTime? cookieExpiryDate = null)
        //{
        //    if (!CS.General_v3.Settings.Others.IsUsingTestingFramework)
        //    {
        //        HttpRequest Request = CurrentHttpRequest;
        //        HttpResponse Response = CurrentHttpResponse;
        //        HttpCookie cookie = Request.Cookies[name];

        //        DateTime dt = new DateTime();
        //        if (cookieExpiryDate.HasValue)
        //            dt = cookieExpiryDate.Value;
        //        else
        //        {
        //            dt = CS.General_v3.Util.Date.Now.AddDays(90);
        //        }
        //        if (cookie == null)
        //        {
        //            cookie = new HttpCookie(name);
        //            // Request.Cookies.Add(cookie);
        //            // Response.Cookies.Add(cookie);
        //        }
        //        if (!string.IsNullOrEmpty(subkey))
        //        {
        //            cookie[subkey] = value;
        //        }
        //        else
        //        {
        //            cookie.Value = value;
        //        }
        //        cookie.Expires = dt;
        //        Request.Cookies.Set(cookie);
        //        Response.Cookies.Set(cookie);
        //        // Request.Cookies.Set(cookie);
        //        //  Response.Cookies.Set(cookie);
        //        /* else
        //         {
        //             if (!string.IsNullOrEmpty(subkey))
        //             {
        //                 cookie.Expires = dt;
        //                 cookie[subkey] = value;
        //             }
        //             else
        //             {
        //                 cookie.Value = value;
        //             }

        //             cookie.Expires = dt;

        //         }
        //         cookie = Response.Cookies[name];
        //         if (cookie == null)
        //         {
        //             cookie = new HttpCookie(name);

        //             cookie.Expires = dt;
        //             if (!string.IsNullOrEmpty(subkey))
        //             {
        //                 //cookie.Expires = dt;
        //                 cookie[subkey] = value;
        //             }
        //             else
        //             {
        //                 cookie.Value = value;
        //             }
        //             Request.Cookies.Add(cookie);

        //         }
        //         else
        //         {
        //             if (!string.IsNullOrEmpty(subkey))
        //             {
        //                 cookie.Expires = dt;
        //                 cookie[subkey] = value;
        //             }
        //             else
        //             {
        //                 cookie.Value = value;
        //             }
        //             cookie.Expires = dt;

        //         }*/
        //    }
        //    else
        //    {
        //        string cookieTmpName = GetTestCookieName(name, subkey);
        //        Test_Cookies[cookieTmpName] = value;
        //    }
        //}
        public static string GetTestCookieName(string name, string subkey = null)
        {
            string s = name + "_" + subkey;
            return s;
        }


        public static string GetSession(string id)
        {
            System.Web.SessionState.HttpSessionState Session = GetHttpContext().Session;

            string ret = "";
            if (Session[id] != null &&
                Session[id].ToString() != "")
                ret = Session[id].ToString();

            return ret;
        }

        public static string ConvertDateForQueryString(DateTime date, bool includeTime = false)
        {
            if (!includeTime)
                return date.ToString("yyyyMMdd");
            else
                return date.ToString("yyyyMMddHHmmss");
        }
        public static T GetFromRouteData<T>(string id)
        {
            string s = GetFromRouteData(id);
            return CS.General_v3.Util.Other.ConvertStringToBasicDataType<T>(s);
        }
        public static int? GetFromRouteDataInt(string id)
        {
            int? retValue = null;
            int num = 0;
            string sVal = GetFromRouteData(id);
            if (Int32.TryParse(sVal, out num))
                retValue = num;
            return retValue;
        }
        public static string GetFromRouteData(string id)
        {
            return GetRouteDataVariable(id);
        }


        private static NameValueCollection _PageRouteData_Test = null;

        public static NameValueCollection Test_PageRouteData
        {
            get
            {
                if (_PageRouteData_Test == null)
                    _PageRouteData_Test = new NameValueCollection();


                return _PageRouteData_Test;
            }

        }
        public static string GetRouteDataVariable(string key)
        {
            string var = null;
            if (_PageRouteData_Test != null && CS.General_v3.Settings.Others.IsUsingTestingFramework)
            {
                var = _PageRouteData_Test[key];
            }
            else
            {

                RouteData routeData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(HttpContext.Current));
                if (routeData != null)
                {
                    object routeValue;
                    if (routeData.Values.TryGetValue(key, out routeValue))
                    {
                        var = (string)routeValue;
                    }
                }
            }
            return var;
        }

        private static Dictionary<object, object> _Context_Test = null;
        public static IDictionary Test_Context
        {
            get
            {
                if (_Context_Test == null)
                    _Context_Test = new Dictionary<object, object>();
                return _Context_Test;
            }
        }

        private static NameValueCollection _Cookies_Test = null;

        public static NameValueCollection Test_Cookies
        {
            get
            {
                if (_Cookies_Test == null)
                    _Cookies_Test = new NameValueCollection();


                return _Cookies_Test;
            }

        }

        private static NameValueCollection _RequestQueryString_Test = null;

        public static NameValueCollection Test_RequestQueryString
        {
            get
            {
                if (_RequestQueryString_Test == null)
                    _RequestQueryString_Test = new NameValueCollection();


                return _RequestQueryString_Test;
            }

        }

        public static NameValueCollection GetRequestQueryString()
        {
            if (CS.General_v3.Settings.Others.IsUsingTestingFramework && Test_RequestQueryString != null)
            {
                return Test_RequestQueryString;

            }
            else
            {

                return CurrentHttpRequest.QueryString;
            }
        }

        private static Dictionary<string, object> _Session_Test = null;

        public static Dictionary<string, object> Test_Session
        {
            get
            {

                if (_Session_Test == null)
                    _Session_Test = new Dictionary<string, object>();


                return _Session_Test;
            }

        }


        private static NameValueCollection _RequestForm_Test = null;

        public static NameValueCollection Test_RequestForm
        {
            get
            {
                if (_RequestForm_Test == null)
                    _RequestForm_Test = new NameValueCollection();


                return _RequestForm_Test;
            }

        }
        public static NameValueCollection GetRequestForm()
        {
            if (_RequestForm_Test != null && CS.General_v3.Settings.Others.IsUsingTestingFramework)
            {
                return _RequestForm_Test;

            }
            else
            {
                return CurrentHttpRequest.Form;
            }
        }
        public static string GetFormVariable(string id)
        {
            //string val = null;
            var form = GetRequestForm();
            if (!string.IsNullOrEmpty(id))
                return form[id];
            else
                return form.ToString();
        }

        public static string RF(string id)
        {
            return GetFormVariable(id);
        }


        public static HttpContext GetHttpContext()
        {
            HttpContext ctx = System.Web.HttpContext.Current;

            return ctx;
        }


        private static string _baseUrlOfApplicationFromFirstSessionStart { get; set; }

        public static void SetBaseUrlOfApplication()
        {
            if (!CS.General_v3.Settings.Others.IsUsingTestingFramework)
            {
                _baseUrlOfApplicationFromFirstSessionStart = GetCurrentUrlLocation().GetURL(fullyQualified: true, appendQueryString: false, includeScheme: true, includePath: false);
            }

        }

        //public static string GetBaseUrlFromCurrentRequest()
        //{
        //    HttpRequest Request = null;
        //    Request = CurrentHttpRequest;
        //    string link = null;
        //    if (Request.ServerVariables["HTTP_HOST"] != "")
        //    {
        //        link = Request.ServerVariables["HTTP_HOST"];
        //    }
        //    else
        //    {
        //        link = Request.ServerVariables["HTTP_ADDR"];
        //    }
        //    return link;
        //}

        public static string GetUrlFromCurrentRequest()
        {
            string result = null;
            if (CheckRequestExistsAtContext())
            {
                HttpRequest Request = null;
                Request = CurrentHttpRequest;
                string host = Request.ServerVariables["HTTP_HOST"];
                string protocol = CS.General_v3.Util.UrlUtil.GetProtocolFromUrl(Request.Url.ToString());
                string url = protocol + "://" + host;

                url = CS.General_v3.Util.IO.EnsurePathDoesNotEndWithSlash(url);
                url += Request.Url.PathAndQuery;
                result = url;
            
            }
            return result;
            
        }
        /// <summary>
        /// Returns the application base url - for example http://www.maltacarsonline.com
        /// </summary>
        /// <param name="useSSL">Whether to require SSL or not</param>
        /// <param name="port">Whether to change the port</param>
        /// <param name="doNotIncludeHttpOrHttps">Whether to include the protocol or not</param>
        /// <param name="loadFromDatabase">Whether to load the base url from the database if available</param>
        /// <returns>The url</returns>
        public static string GetApplicationBaseUrlAsString(bool? useSSL = false, int? port = null, bool doNotIncludeHttpOrHttps = false, bool loadFromDatabase = true)
        {
            URLClass url = GetApplicationBaseUrl(loadFromDatabase: loadFromDatabase);
            if (port.HasValue)
                url.Port = port;
            if (useSSL.HasValue)
                url.UseSsl = useSSL.Value;
            return url.GetURL(fullyQualified: true, appendQueryString: false, includeScheme: !doNotIncludeHttpOrHttps);


        }

        /// <summary>
        /// Returns the base url.  E.g http://www.maltacarsonline.com/index.aspx
        /// </summary>
        /// <param name="loadFromDatabase">Whether to load from database first, before trying the current request</param>
        /// <returns>The url</returns>
        public static URLClass GetApplicationBaseUrl(bool loadFromDatabase = true)
        {
            

            string link = null;


            if (!string.IsNullOrWhiteSpace(CS.General_v3.Settings.Others.BaseUrlCustom_ApplicationWide))
            {
                link = CS.General_v3.Settings.Others.BaseUrlCustom_ApplicationWide;
            }
            else if (!string.IsNullOrWhiteSpace(CS.General_v3.Settings.Others.BaseUrlCustom_SessionWide))
            {
                link = CS.General_v3.Settings.Others.BaseUrlCustom_SessionWide;
            }
            else
            {
                if (loadFromDatabase)
                {
                    string dbWebsiteBaseURL = CS.General_v3.Settings.GetSettingFromDatabase(CS.General_v3.Enums.SETTINGS_ENUM.Others_WebsiteBaseUrl);
                    link = dbWebsiteBaseURL;
                }

                if (string.IsNullOrWhiteSpace(link) && CheckRequestExistsAtContext())
                {
                    link = GetCurrentUrlLocation().GetURL(fullyQualified: true, appendQueryString: false, includePath: false); ;
                    //link = GetBaseUrlFromCurrentRequest();

                }
                if (string.IsNullOrWhiteSpace(link))
                {
                    link = _baseUrlOfApplicationFromFirstSessionStart;
                }

                if (_baseUrlOfApplicationFromFirstSessionStart == null) _baseUrlOfApplicationFromFirstSessionStart = link;

            }
            link = UrlUtil.GetBaseUrl(link); //strip off the relative and querystring, this is not what this does.
            URLClass url = new URLClass(link, loadCurrentURLIfNull:false);
            return url;

        }



        /// <summary>
        /// Returns the base url.  E.g http://www.maltacarsonline.com/index.aspx, based on several parameters
        /// </summary>
        /// <param name="link">The link to convert</param>
        /// <param name="useSSL">Whether the result Url should be SSL or not. If not, it is based on the current link value.</param>
        /// <param name="port">The port to use, if any</param>
        /// <param name="doNotIncludeHttpOrHttps">Whether to include the protocol or not, e.g http:// in front</param>
        /// <param name="returnQuerystringAsWell">Whether to return the querystring as well</param>
        /// <param name="getAbsoluteUrl">Whether to return the absolute url, i.e with the front part e.g http://www.maltacarsonline.com/testing, or just /testing</param>
        /// <returns>The url</returns>
        private static string _getApplicationUrl(string link, bool? useSSL = false, int? port = null, bool doNotIncludeHttpOrHttps = false, bool returnQuerystringAsWell = false,
            bool getAbsoluteUrl = true)
        {
            link = link ?? "";
            if (!useSSL.HasValue)
                useSSL = link.ToLower().StartsWith("https://");


            if (link.ToLower().StartsWith("http://")) link = link.Substring("http://".Length);
            if (link.ToLower().StartsWith("https://")) link = link.Substring("https://".Length);

            if (port == null && link.Contains(":"))
            {
                int startColonSearch = 0;
                if (link.ToLower().StartsWith("http://")) startColonSearch = "http://".Length;
                if (link.ToLower().StartsWith("https://")) startColonSearch = "https://".Length;


                int colonPos = link.IndexOf(":", startColonSearch);
                if (colonPos > -1)
                {
                    int slashPos = link.IndexOf("/", colonPos);
                    if (slashPos == -1)
                        slashPos = link.Length;
                    string sPort = link.Substring(link.IndexOf(":") + 1, slashPos - colonPos - 1);
                    port = Convert.ToInt32(sPort);
                }

            }
            if (port == null)
            {
                port = (useSSL.GetValueOrDefault() ? 443 : 80);
            }
            int firstSlashPos = link.IndexOf('/');
            if (firstSlashPos == -1)
                firstSlashPos = link.Length;
            link = link.Substring(0, firstSlashPos);
            if (link.IndexOf(':') > -1)
            {
                link = link.Substring(0, link.IndexOf(':'));
            }
            if (port.HasValue && !(((port.Value == 80 && !useSSL.Value) || (port.Value == 443 && useSSL.Value))))
                link += ":" + port.ToString();
            link += "/";
            link = link.Replace("//", "/");
            if (useSSL.Value)
            {
                link = "https://" + link;
            }
            else
            {
                link = "http://" + link;
            }
            if (doNotIncludeHttpOrHttps)
            {
                if (link.ToLower().StartsWith("http://"))
                    link = link.Substring("http://".Length);
                else if (link.ToLower().StartsWith("https://"))
                    link = link.Substring("https://".Length);
            }
            if (!returnQuerystringAsWell)
            {
                int questionMarkPos = link.IndexOf('?');
                if (questionMarkPos > -1)
                {
                    link = link.SubStr(0, questionMarkPos);
                }
            }
            if (!getAbsoluteUrl)
            {
                int startCheckPos = -1;
                if (link.ToLower().StartsWith("http://")) startCheckPos = "http://".Length;
                if (link.ToLower().StartsWith("https://")) startCheckPos = "https://".Length;
                int nextSlashPos = link.IndexOf('/', startCheckPos);
                if (nextSlashPos > -1)
                    link = link.Substring(nextSlashPos);
                else
                    link = "/";
            }
            return link;


        }


        /// <summary>
        /// Returns the base url.  E.g http://www.maltacarsonline.com/index.aspx, based on several parameters
        /// </summary>
        /// <param name="link">The link to convert</param>
        /// <param name="useSSL">Whether the result Url should be SSL or not. If not, it is based on the current link value.</param>
        /// <param name="port">The port to use, if any</param>
        /// <param name="doNotIncludeHttpOrHttps">Whether to include the protocol or not, e.g http:// in front</param>
        /// <param name="returnQuerystringAsWell">Whether to return the querystring as well</param>
        /// <param name="getAbsoluteUrl">Whether to return the absolute url, i.e with the front part e.g http://www.maltacarsonline.com/testing, or just /testing</param>
        /// <returns>The url</returns>
        public static string GetApplicationBaseUrl(string link, bool? useSSL = false, int? port = null, bool doNotIncludeHttpOrHttps = false, bool returnQuerystringAsWell = false,
            bool getAbsoluteUrl = true)
        {
            link = link ?? "";
            if (!useSSL.HasValue)
                useSSL = link.ToLower().StartsWith("https://");


            if (link.ToLower().StartsWith("http://")) link = link.Substring("http://".Length);
            if (link.ToLower().StartsWith("https://")) link = link.Substring("https://".Length);

            if (port == null && link.Contains(":"))
            {
                int startColonSearch = 0;
                if (link.ToLower().StartsWith("http://")) startColonSearch = "http://".Length;
                if (link.ToLower().StartsWith("https://")) startColonSearch = "https://".Length;


                int colonPos = link.IndexOf(":", startColonSearch);
                if (colonPos > -1)
                {
                    int slashPos = link.IndexOf("/", colonPos);
                    if (slashPos == -1)
                        slashPos = link.Length;
                    string sPort = link.Substring(link.IndexOf(":") + 1, slashPos - colonPos - 1);
                    port = Convert.ToInt32(sPort);
                }

            }
            if (port == null)
            {
                port = (useSSL.GetValueOrDefault() ? 443 : 80);
            }
            int firstSlashPos = link.IndexOf('/');
            if (firstSlashPos == -1)
                firstSlashPos = link.Length;
            link = link.Substring(0, firstSlashPos);
            if (link.IndexOf(':') > -1)
            {
                link = link.Substring(0, link.IndexOf(':'));
            }
            if (port.HasValue && !(((port.Value == 80 && !useSSL.Value) || (port.Value == 443 && useSSL.Value))))
                link += ":" + port.ToString();
            link += "/";
            link = link.Replace("//", "/");
            if (useSSL.Value)
            {
                link = "https://" + link;
            }
            else
            {
                link = "http://" + link;
            }
            if (doNotIncludeHttpOrHttps)
            {
                if (link.ToLower().StartsWith("http://"))
                    link = link.Substring("http://".Length);
                else if (link.ToLower().StartsWith("https://"))
                    link = link.Substring("https://".Length);
            }
            if (!returnQuerystringAsWell)
            {
                int questionMarkPos = link.IndexOf('?');
                if (questionMarkPos > -1)
                {
                    link = link.SubStr(0, questionMarkPos);
                }
            }
            if (!getAbsoluteUrl)
            {
                int startCheckPos = -1;
                if (link.ToLower().StartsWith("http://")) startCheckPos = "http://".Length;
                if (link.ToLower().StartsWith("https://")) startCheckPos = "https://".Length;
                int nextSlashPos = link.IndexOf('/', startCheckPos);
                if (nextSlashPos > -1)
                    link = link.Substring(nextSlashPos);
                else
                    link = "/";
            }
            return link;


        }

        ///// <summary>
        ///// Returns the location url.  E.g http://www.maltacarsonline.com/cars/index.aspx?category=Vehicles
        ///// returns /cars/index.aspx?category=Vehicles
        ///// </summary>
        ///// <returns>The URL Location</returns>
        //public static string GetURLLocationAndQuerystring(bool getAbsoluteURL = false)
        //{
        //    return GetApplicationBaseUrl(getAbsoluteUrl: getAbsoluteURL, loadFromDatabase: false);
        //    string path = GetURLLocation();
        //    if (!string.IsNullOrEmpty(CS.General_v3.Util.PageUtil.GetRequestQueryString().ToString()))
        //        path += "?" + CS.General_v3.Util.PageUtil.GetRequestQueryString().ToString();

        //    if (getAbsoluteURL)
        //    {
        //        path = ConvertRelativeUrlToAbsoluteUrl(path);
        //    }
        //    return path;


        //}
        public static bool IsAbsoluteURL(string url)
        {
            
            return url.IndexOf("://") != -1;
        }
        public static string ConvertAbsoluteUrlToRelativeRootUrl(string absoluteURL)
        {
            string url = absoluteURL;
            //Remove the 'http://' or 'https://'
            if (url.IndexOf("://") != -1)
            {
                int startIndex = url.IndexOf("/", absoluteURL.IndexOf("://") + 3);
                if (startIndex != -1)
                {
                    url = url.Substring(startIndex);
                }
            }
            return url;
        }

        /// <summary>
        /// URL Must be from the 'root' like /cikku/peppi.aspx and will be transformed into 
        /// http://localhost:8080/cikku/peppi.aspx
        /// </summary>
        /// <param name="relativeUrl"></param>
        /// <returns></returns>
        public static string ConvertRelativeUrlToAbsoluteUrl(string relativeUrl)
        {

            string url = relativeUrl;
            //Must start from root, else can't convert
            if (url.StartsWith("/"))
            {


                //Get base url from db, e.g. http://localhost:8080/
                string baseURL = CS.General_v3.Util.PageUtil.GetApplicationBaseUrl().GetURL(fullyQualified: true);
                string requiredSchema = null;
                string otherSchema = null;
                if (CurrentHttpRequest != null && CurrentHttpRequest.IsSecureConnection)
                {
                    requiredSchema = "https://";
                    otherSchema = "http://";
                }
                else
                {
                    requiredSchema = "http://";
                    otherSchema = "https://";
                }
                //Make sure it is in the correct protocol / schema
                if (!baseURL.StartsWith(requiredSchema))
                {
                    if (baseURL.StartsWith(otherSchema))
                    {
                        baseURL = baseURL.Replace(otherSchema, requiredSchema);
                    }
                    else
                    {
                        if (baseURL.StartsWith("/"))
                        {
                            baseURL = baseURL.Remove(0, 1);
                        }
                        baseURL = requiredSchema + baseURL;
                    }
                }
                if (baseURL.EndsWith("/"))
                {
                    baseURL = baseURL.Remove(baseURL.Length - 1, 1);
                }
                url = baseURL + url;
                /*url += Request.Url.Host;
                if (!Request.Url.IsDefaultPort)
                {
                    url += ":" + Request.Url.Port;
                }
                url += relativeUrl;*/

            }
            return url;
        }






        /// <summary>
        /// Returns the up location to the base path.  E.g http://www.maltacarsonline.com/cars/index.aspx
        /// returns '../'
        /// </summary>
        /// <returns>The UP Location</returns>
        public static string GetPathToBaseURL()
        {
            HttpRequest Request = CurrentHttpRequest;

            string loc = Request.Path;
            string tmp = "";
            for (int i = 1; i < loc.Length; i++)
            {
                if (loc[i] == '/')
                {
                    tmp += "../";
                }
            }
            return tmp;


        }
        public static void CheckLabels(Control mainCtrl)
        {
            ArrayList list = new ArrayList();
            Queue queue = new Queue();
            queue.Enqueue(mainCtrl);
            while (queue.Count > 0)
            {
                Control ctrl = (Control)queue.Dequeue();
                if (ctrl.HasControls())
                {
                    for (int i = 0; i < ctrl.Controls.Count; i++)
                    {
                        queue.Enqueue(ctrl.Controls[i]);
                    }
                }
                if (ctrl is HtmlGenericControl)
                {
                    list.Add(ctrl);

                }
            }
            for (int i = 0; i < list.Count; i++)
            {
                HtmlGenericControl lbl = (HtmlGenericControl)list[i];
                if (lbl.Attributes["for"] != null &&
                    lbl.Attributes["for"] != "")
                {
                    string forID = lbl.Attributes["for"];
                    string clientID = lbl.ClientID.Substring(0, lbl.ClientID.LastIndexOf(lbl.ID));
                    forID = clientID + forID;
                    lbl.Attributes["for"] = forID;

                }

            }
        }

        /*
        /// <summary>
        /// Loads all subkeys of a cookie into a list of strings
        /// </summary>
        /// <param name="pg">Page</param>
        /// <param name="name">Name of cookie</param>
        /// <param name="key">Subkey of cookie</param>
        /// <returns>The list of strings in the cookie</returns>
        public static List<string> LoadCookies(string name)
        {
            HttpRequest Request = Request;

            HttpCookie cookie = Request.Cookies[name];
            List<string> list = new List<string>();
            if (cookie != null)
            {
                for (int i = 0; i < cookie.Values.Count; i++)
                {
                    if (cookie.Values[i] != null && cookie.Values[i] != "")
                        list.Add(cookie.Values[i]);
                }
            }
            return list;
        }
        */



        /*
        /// <summary>
        /// Saves a cookie
        /// </summary>
        /// <param name="pg">The page</param>
        /// <param name="name">Name of cookie</param>
        /// <param name="subKey">Subkey of cookie.  If left null, takes the same value of name</param>
        /// <param name="value">Value to be saved</param>
        public static void SaveCookie(string name, string subKey, string value)
        {
            if (!CS.General_v3.Settings.Others.IsUsingTestingFramework)
            {

                string Key = subKey;
                if (Key == null || Key == "")
                    Key = name;
                HttpRequest Request = Request;
                HttpResponse Response = Response;

                HttpCookie cookie = Request.Cookies[name];
                if (cookie == null)
                    cookie = Response.Cookies[name];
                cookie[Key] = value;
                Response.Cookies.Add(cookie);
                //pg.Request.Cookies.Add(cookie);
            }
            else
            {
                string cookieTmpName = name + subKey;
                _tmpCookie[cookieTmpName] = value;

            }
        }


        /// <summary>
        /// Saves a list of strings to a cookie
        /// </summary>
        /// <param name="pg">The page</param>
        /// <param name="name">Name of cookie</param>
        /// <param name="values">Values to be saved</param>
        public static void SaveCookies(string name, List<string> values)
        {
            if (values != null && values.Count > 0)
            {

                for (int i = 0; i < values.Count; i++)
                {

                    if (values[i] != null && values[i] != "")
                    {
                        string subKey = i.ToString();
                        SaveCookie(name, subKey, values[i]);
                    }
                        //cookie[i.ToString()] = values[i];
                }

            }
            //pg.Response.Cookies.Add(cookie);
        }*/

        public static string MapPathFromLocalToWeb(string localPath, bool includeBaseUrl = true)
        {
            string result = null;
            string basePath = MapPath("/");
            if (localPath.ToLower().StartsWith(basePath.ToLower()))
            {
                string currPath = localPath.Substring(basePath.Length - 1);
                currPath = currPath.Replace("\\", "/");
                result = currPath;
            }
            else
            {
                throw new InvalidOperationException(
                    "Cannot resolve this path as it is not contained in the website's root folder!");
            }
            if (includeBaseUrl)
            {
                if (result.StartsWith("/"))
                    result = result.Substring(1);
                result = GetApplicationBaseUrl() + result;
            }
            return result;

        }

        

        public static string MapPath(string origPath)
        {
            origPath = ConvertAbsoluteUrlToRelativeRootUrl(origPath);    

            string path = origPath;
            path = path.Replace("//", "/");
            path = path.Replace("\\\\", "\\");
            if (GetHttpContext() != null)
            {
                string s = "";
                if (path.Contains("/"))
                {

                    try
                    {
                        s = GetHttpContext().Server.MapPath(path);
                    }
                    catch (Exception ex)
                    {
                        if (path.StartsWith("/"))
                        {
                            string basePath = GetHttpContext().Server.MapPath("/");
                            s = basePath + path.Substring(1);
                            s = s.Replace("/", "\\");
                        }
                        else
                            throw;
                    }
                }

                else
                {
                    s = Server.MapPath(path);
                    //s = path;
                }
                return s;
            }
            else
            {
                string s = null;
                if (path.StartsWith("~")) path = path.Substring(1);
                if (path.StartsWith("/")) path = path.Substring(1);
                string rootPath = IO.GetApplicationRootLocalFolder();
                if (rootPath.EndsWith("\\"))
                    rootPath = rootPath.Substring(0, rootPath.Length - 1);
                while (path.StartsWith("../"))
                {
                    int lastIndexOfSlash = rootPath.LastIndexOf("\\");
                    if (lastIndexOfSlash >= 0)
                    {
                        rootPath = rootPath.Substring(0, lastIndexOfSlash);

                    }
                    else
                    {
                        throw new InvalidOperationException("Not enough folders to traverse path");
                    }
                    path = path.Substring(3);
                }




                s = rootPath + "\\";

                //if (!s.EndsWith("\\"))
                //s += "\\";
                path = path.Replace("/", "\\");
                if (path.StartsWith("\\"))
                    path = path.Remove(0, 1);
                s += path;


                return s;
            }
        }
        public static void GotoAnchor(System.Web.UI.Page pg, string AnchorName)
        {
            //string JS = "var timer_" + AnchorName + " = setInterval(\"gotoAnchor('" + AnchorName + "')\",100);";
            string JS = "com.cs.require('com.CS.General_v3.general');\r\n";
            JS += "com.CS.General_v3.General.gotoAnchor('" + AnchorName + "');\r\n";
            pg.ClientScript.RegisterStartupScript(pg.GetType(), "anchor_" + AnchorName.ToLower(), CS.General_v3.Util.Text.MakeJavaScript(JS, true));
        }



        public static bool GetSessionBool(string name)
        {
            object o = GetSessionObject(name);
            if (o == null)
                return false;
            else
                return (bool)o;
        }
        public static bool CheckSessionExistsAtContext()
        {
            return (GetHttpContext() != null && GetHttpContext().Session != null);
        }
        private static void setSessionObject(string key, object o)
        {
            if (!CS.General_v3.Settings.Others.IsUsingTestingFramework && CS.General_v3.Util.Other.IsWebApplication)
            {
                GetHttpContext().Session[key] = o;

            }
            else
            {
                Test_Session[key] = o;
            }
        }

        public static void SetSessionObject(string key, string o)
        {
            setSessionObject(key, o);
        }
        public static void SetSessionObject(string key, DateTime? o)
        {
            setSessionObject(key, o);
        }
        public static void SetSessionObject(string key, double? o)
        {
            setSessionObject(key, o);
        }
        public static void SetSessionObject(string key, long? o)
        {
            setSessionObject(key, o);
        }
        public static void SetSessionObject(string key, int? o)
        {
            setSessionObject(key, o);
        }
        public static void SetSessionObject(string key, float? o)
        {
            setSessionObject(key, o);
        }
        public static void SetSessionObject(string key, byte? o)
        {
            setSessionObject(key, o);
        }
        public static void SetSessionObject(string key, bool? o)
        {
            setSessionObject(key, o);
        }
        /// <summary>
        /// You must make sure that the object passed must have the [Serializable] attribute with the class
        /// </summary>
        /// <param name="key"></param>
        /// <param name="o"></param>
        public static void SetSessionObjectFromSerializableItem(string key, object o)
        {

            if (o != null && CS.General_v3.Util.Other.IsLocalTestingMachine && !CS.General_v3.Util.ReflectionUtil.CheckIfTypeContainsAttribute(o.GetType(), typeof(SerializableAttribute)))
            {
                throw new NotSupportedException("Object of type '"+o.GetType().FullName+"' must have attribute [Serializable]");
            }
            setSessionObject(key, o);
        }
        public static void SetSessionObject<T>(string key, T o)
        {
            setSessionObject(key, o);
        }
        public static void SetSessionObject(string key, Enum o)
        {
            int val = Convert.ToInt32(o);
            setSessionObject(key, val);
        }
        public static void SetSessionObject(string key, ISerializable o)
        {
            int val = Convert.ToInt32(o);
            setSessionObject(key, val);
        }

        public static object GetSessionObjectAndRemove(string key, bool throwErrorIfSessionDoesNotExist = true)
        {
            object msg = GetSessionObject(key, throwErrorIfSessionDoesNotExist);
            ClearSessionObject(key);

            return msg;
        }

        public static object GetSessionObject(string key, bool throwErrorIfSessionDoesNotExist = true)
        {
            object o = null;
            if (!CS.General_v3.Settings.Others.IsUsingTestingFramework && CS.General_v3.Util.Other.IsWebApplication)
            {
                if (GetHttpContext() != null && GetHttpContext().Session != null)

                    o = GetHttpContext().Session[key];
                else
                {
                    if (throwErrorIfSessionDoesNotExist)
                    {
                        throw new InvalidOperationException("Session does not exist at this context");
                    }
                }
            }
            else
            {
                if (Test_Session.ContainsKey(key))
                    o = Test_Session[key];
            }
            return o;

        }

        public static T? GetSessionObjectEnum<T>(string key) where T : struct, IConvertible
        {
            object o = GetSessionObject(key);
            if (o != null)
            {
                return CS.General_v3.Util.EnumUtils.EnumValueNullableOf<T>(o.ToString());
            }
            return null;

        }

        public static TDataType GetSessionObject<TDataType>(string key)
        {


            object o = GetSessionObject(key);
            return CS.General_v3.Util.Other.ConvertObjectToBasicDataType<TDataType>(o);
            /*object result = null;
            if (typeof(TDataType) == typeof(int) && !(o is int))
            {
                result = Conversion.ToInt32(o);

            }
            else
            {
                result = o;
            }


            Type t = typeof (TDataType);
            bool isNullable = (t.IsGenericType && t.GetGenericTypeDefinition().Equals(typeof (Nullable<>)));
            if (isNullable)
            {
                Type t2 = Nullable.GetUnderlyingType(t);
                
            }
            return (TDataType?)result;*/
        }

   


        public static bool IsLocalhost()
        {
            var url = GetApplicationBaseUrl();
            string baseUrl = url.GetURL(fullyQualified: true, includeScheme: false, includePath: false);

            string host = baseUrl.ToLower();
            return host.IndexOf("localhost") != -1 || host.IndexOf("127.0.0.1") != -1 || host.IndexOf("loopback") != -1;
        }

        public static string GetReferrerURL(System.Web.UI.Page page)
        {
            return page.Request.ServerVariables["HTTP_REFERER"];
        }
        public static string GetAbsoluteUrlFromRelative(string relativeURL)
        {
            return ConvertRelativeUrlToAbsoluteUrl(relativeURL);
            /*if ((!relativeURL.StartsWith("http://")) && (!relativeURL.StartsWith("www.")) && (!relativeURL.StartsWith("https://")))
            {
                if (relativeURL.StartsWith("/"))
                {
                    relativeURL = relativeURL.Substring(1);
                }
                string host = GetHost();
                string url = host + relativeURL;

                return url;
            }
            else
            {
                return relativeURL;
            }*/
        }

        public static string StripHTTPFromUrl(string url)
        {
            if (url.ToLower().StartsWith("http://"))
                url = url.Substring("http://".Length);
            if (url.ToLower().StartsWith("https://"))
                url = url.Substring("https://".Length);
            return url;
        }

        /// <summary>
        /// Performs permamenent redirection checks,based on the URLs, like for example
        /// birdlifemalta.org => www.birdlifemalta.org, birdlife-malta.org => www.birdlifemalta.org
        /// </summary>
        /// <param name="urls"></param>
        public static void CheckPermanentRedirectionUrls(string urls)
        {
            urls = CS.General_v3.Util.Text.ReplaceTexts(urls, "", "\r", "\n", "\t");
            var currentUrlClass = GetCurrentUrlLocation();

            string currentUrl = currentUrlClass.GetURL(fullyQualified: true, appendQueryString: true);
            
            string httpType = "http://";
            if (currentUrl.ToLower().StartsWith("https://"))
                httpType = "https://";
            string cmpPart = "";
            {
                int slashPos = currentUrl.IndexOf("/", 8);
                if (slashPos == -1)
                    slashPos = currentUrl.Length - 1;
                cmpPart = currentUrl.Substring(0, slashPos + 1);
            }

            string[] urlList = urls.Split(',');
            for (int i = 0; i < urlList.Length; i++)
            {
                string url = urlList[i].Trim();
                int indexOf = url.IndexOf("=>");
                if (indexOf > -1)
                {
                    string from = StripHTTPFromUrl(url.Substring(0, indexOf).Trim());
                    string to = CS.General_v3.Util.Text.RemoveLastPartFromStringIfItIs(StripHTTPFromUrl(url.Substring(indexOf + 2).Trim()), "/");

                    if (Regex.IsMatch(cmpPart, httpType + from, RegexOptions.IgnoreCase))
                    {
                        //match
                        string redirectTo = StripHTTPFromUrl(currentUrl);
                        int index = redirectTo.IndexOf("/");
                        if (index > -1)
                            redirectTo = redirectTo.Substring(index + 1);
                        redirectTo = httpType + to + "/" + redirectTo;
                        PermanentRedirect(redirectTo);
                        break;
                    }

                }

            }
        }
        /// <summary>
        /// Adds JS code to the [head] tag of the page
        /// </summary>
        /// <param name="javascriptCode"></param>
        /// <param name="addScriptTag"></param>
        public static void AddJavaScriptToHead(string javascriptCode, bool addScriptTag = true)
        {
            if (addScriptTag)
            {
                javascriptCode = CS.General_v3.Util.JSUtilOld.MakeJavaScript(javascriptCode, true);
            }
            GetCurrentPage().Header.Controls.Add(new Literal() { Text = javascriptCode });
        }

        public static void ServerTransfer(string url)
        {

            Server.Transfer(url);

        }

        public static HttpResponse _getResponseObject()
        {
            return CurrentHttpResponse;

        }

        public static NameValueCollection _getCurrentForm()
        {
            return CurrentHttpRequest.Form;

        }

        public static NameValueCollection _getCurrentQueryString()
        {
            return CurrentHttpRequest.QueryString;

        }

        public static HttpRequest GetCurrentRequest()
        {
            return CurrentHttpRequest;

        }

        public static void SetContextObject(string key, object value, bool throwErrorIfContextDoesNotExist = true)
        {
            if (!CS.General_v3.Settings.Others.IsUsingTestingFramework && CS.General_v3.Classes.Application.AppInstance.Instance != null && CS.General_v3.Util.Other.IsWebApplication)
            {
                if (throwErrorIfContextDoesNotExist || HttpContext.Current != null)
                {
                    HttpContext.Current.Items[key] = value;
                }
            }
            else
                Test_Context[key] = value;



        }

        public static object GetContextObject(string key)
        {
            return GetContextObject<object>(key);

        }
        public static TDataType GetContextObject<TDataType>(string key, bool throwErrorIfContextDoesNotExist = true, HttpContext context = null)
        {
            TDataType result = default(TDataType);

            object val = null;
            if (!CS.General_v3.Settings.Others.IsUsingTestingFramework && CS.General_v3.Util.Other.IsWebApplication)
            {
                
                if (context != null)
                {
                    val = context.Items[key];
                }
                else if (throwErrorIfContextDoesNotExist || HttpContext.Current != null)
                {
                    val = GetHttpContext().Items[key];

                }
            }
            else
            {
                val = Test_Context[key];
            }
            if (val != null)
            {
                result = (TDataType)val;
            }
            return result;

        }

        /// <summary>
        /// Important - Uses jQuery!
        /// </summary>
        /// <param name="pg"></param>
        /// <param name="actionUrl"></param>
        public static void ChangeFormActionViaJavascript(Page pg, string actionUrl)
        {
            StringBuilder js = new StringBuilder();
            js.AppendLine("jQuery('form').attr('action', '" + actionUrl + "');");
            pg.ClientScript.RegisterStartupScript(pg.GetType(), "changeFormAction_" + actionUrl, js.ToString(), true);

        }

        public static void WriteNameValueAsHiddenFieldsToForm(Page pg, NameValueCollection nv)
        {

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < nv.AllKeys.Length; i++)
            {
                var key = nv.AllKeys[i];
                var value = nv[key];

                sb.AppendLine("<input type=\"hidden\" name=\"" + key + "\" value=\"" + value + "\" />");

            }

            Literal lit = new Literal();
            lit.Text = sb.ToString();
            pg.Form.Controls.Add(lit);

        }

        public static void ClearSessionObject(string key)
        {
            setSessionObject(key, null);

        }
        public static T GetCachedObject<T>(string key)
        {
            return CachingUtil.GetItemFromCache<T>(key);
            //return (T)GetHttpContext().Cache[key];
        }
        public static void SetCachedObject(string key, object value, double expirationMinutes, bool throwErrorIfNoContext = true, CacheDependency dependencies = null)
        {
            TimeSpan span = new TimeSpan(0, (int) Math.Round(expirationMinutes), 0);
            CachingUtil.AddItemToCache(key, value, span, dependencies);
            //CachingUtil.AddItemToCache
            //DateTime d = System.DateTime.UtcNow;
            //d.AddMinutes(expirationMinutes);

            //var ctx = GetHttpContext();
            //if (throwErrorIfNoContext && ctx == null) throw new InvalidOperationException("No HTTP context available at this context");
            //GetHttpContext().Cache.Insert(key, value, null, d, System.Web.Caching.Cache.NoSlidingExpiration);
        }

        public static void ClearAllCacheInContext()
        {

            var cache = HttpRuntime.Cache;

            {
                List<string> keys = new List<string>();

                // retrieve application Cache enumerator
                IDictionaryEnumerator enumerator = cache.GetEnumerator();

                // copy all keys that currently exist in Cache
                while (enumerator.MoveNext())
                {
                    keys.Add(enumerator.Key.ToString());
                }

                // delete every key from cache
                for (int i = 0; i < keys.Count; i++)
                {
                    cache.Remove(keys[i]);
                }
            }

        }

        private const string QS_PARAM_COOKIE_ENABLED = "___c";
        internal static bool? _cookiesEnabledContext
        {
            get { return GetContextObject<bool?>(COOKIES_CHECK_NAME); }
            set { SetContextObject(COOKIES_CHECK_NAME, value); }
        }
        public static void checkFromHandlerWhetherBrowserSupportsCookies()
        {
            bool cookieValue = (CS.General_v3.Util.PageUtil.GetCookie(QS_PARAM_COOKIE_ENABLED) == QS_PARAM_COOKIE_ENABLED);

            PageUtil._cookiesEnabledContext = (cookieValue);

        }






        private static bool? __IsLocalHost = null;
        private static bool? _IsLocalHost
        {
            get
            {
                return __IsLocalHost;
                /*                if (CS.General_v3.Util.PageUtil.ApplicationInfo != null)
                                    return (bool?)CS.General_v3.Util.PageUtil.ApplicationInfo["_IsLocalHost"];
                                else
                                    return null;*/


            }
            set
            {
                __IsLocalHost = value;
                //CS.General_v3.Util.PageUtil.ApplicationInfo["_IsLocalHost"] = value; ;
            }
        }
        public static bool? IsLocalTestingMachine_CustomOverride { get; set; }

        public static bool IsLocalTestingMachine
        {
            get
            {
                if (IsLocalTestingMachine_CustomOverride.HasValue)
                    return IsLocalTestingMachine_CustomOverride.Value;
                else
                {
                    if (!_IsLocalHost.HasValue)
                    {
                        _IsLocalHost = false;
                        string processName = CS.General_v3.Util.ApplicationUtil.GetProcessName();
                        List<string> testingProcessNames = new List<string>();
                        testingProcessNames.Add("WebDev.WebServer40");
                        if (testingProcessNames.Contains(processName))
                        {
                            _IsLocalHost = true;
                        }
                        else
                        {
                            if (IsWebApplication)
                            {
                                string s = HttpRuntime.AppDomainAppPath;
                                if (Regex.Match(s, @".:\\(Work|Work_New)\\", RegexOptions.IgnoreCase).Success)
                                {
                                    _IsLocalHost = true;
                                }

                            }

                            if (__IsLocalHost == true && CheckIfApplicationRootContainsOnlineServerFile())
                                __IsLocalHost = false;
                        }
                    }

                    return _IsLocalHost.Value;


                }

            }
        }
        public static void SetIsLocalHostValue()
        {
            _IsLocalHost = IsLocalTestingMachine;
        }
        public static void SetIsLocalHostValue(bool value)
        {
            _IsLocalHost = value;
        }


        public static bool IsWebApplication
        {
            get
            {
                bool isWebApp = false;
                try
                {

                    bool b = false;
                    if (System.Web.Hosting.HostingEnvironment.IsHosted)
                    {
                        b = true;

                    }

                    else
                    {

                        string s = System.Web.Configuration.WebConfigurationManager.AppSettings["Website_IsWebsite"];
                        if (s != null && s.ToLower() == "true")
                        {
                            b = true;
                        }

                    }
                    isWebApp = b;
                }
                catch
                {
                    isWebApp = false;
                }
                return isWebApp;
            }
        }
        public static bool CheckIfApplicationRootContainsOnlineServerFile()
        {
            List<string> list = new List<string>();
            string basePath = CS.General_v3.Util.ApplicationUtil.ApplicationPath;
            list.Add(basePath + "online_server.txt");
            list.Add(basePath + @"App_Data\online_server.txt");

            for (int i = 0; i < list.Count; i++)
            {
                var p = list[i];
                if (File.Exists(p))
                    return true;
            }
            return false;

        }


        public static bool ConvertStringToBool(string sBool)
        {
            return sBool != null && sBool == "1" || sBool.ToLower() == "true" || sBool.ToLower() == "yes";
        }

        public static string ConvertTextForMetaDescription(string text)
        {
            if (text != null)
            {
                text = CS.General_v3.Util.Text.ConvertHTMLToPlainText(text, false);
                const int maxLength = 155;
                if (text.Length > maxLength)
                {
                    text = Text.LimitText(text, maxLength, true);
                }
            }
            return text;
        }


        public static T GetVariableFromQuerystring<T>(string key, NameValueCollection customQs = null)
        {
            if (customQs == null)
                customQs = GetRequestQueryString();
            return CS.General_v3.Util.Other.ConvertStringToBasicDataType<T>(customQs[key]);
        }
        public static string GetVariableFromQuerystring(string key, NameValueCollection customQs = null)
        {
            
            return GetVariableFromQuerystring<string>(key, customQs);
            
        }
    }
}
