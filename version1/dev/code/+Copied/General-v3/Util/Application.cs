﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using log4net;
using System.Threading;

namespace CS.General_v3.Util
{
    public static class ApplicationUtil
    {
        public delegate void ApplicationExceptionDelegate(Exception ex);
        private static ILog _log = log4net.LogManager.GetLogger(typeof(ApplicationUtil));
        internal class FatalExceptionMethodCaller
        {
            internal FatalExceptionMethodCaller(ApplicationExceptionDelegate methodToCall)
            {
                
                this.methodToCall = methodToCall;
            }
            private ApplicationExceptionDelegate methodToCall;
            internal void Start()
            {
                if (_log.IsDebugEnabled) _log.Debug("FatalExceptionMethodCaller.Start()");
                AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
               // AppDomain.CurrentDomain.FirstChanceException += new EventHandler<System.Runtime.ExceptionServices.FirstChanceExceptionEventArgs>(CurrentDomain_FirstChanceException);
                System.Windows.Forms.Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
                if (_log.IsDebugEnabled) _log.Debug("FatalExceptionMethodCaller.Start() - End");
            }

            

            private void consumeException(Exception ex)
            {
               // if (_log.IsInfoEnabled) _log.Info("FatalExceptionMethodCaller.consumeException()");
                methodToCall(ex);
                
                
            }
            private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
            {
            //    if (_log.IsInfoEnabled) _log.Info("FatalExceptionMethodCaller.CurrentDomain_UnhandledException()");
                Exception ex = (Exception)e.ExceptionObject;
              //  if (_log.IsInfoEnabled) _log.Info("FatalExceptionMethodCaller.CurrentDomain_UnhandledException() : ex.Message = " +ex.Message);
                consumeException(ex);
            }
            private void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
            {
                if (_log.IsInfoEnabled) _log.Info("FatalExceptionMethodCaller.Application_ThreadException()");
                if (_log.IsInfoEnabled) _log.Info("FatalExceptionMethodCaller.Application_ThreadException() : ex.Message = " + e.Exception.Message);
                consumeException(e.Exception);


            }
        }
        public static void MonitorFatalExceptions()
        {
            if (_log.IsInfoEnabled) _log.Info("Started monitoring fatal exceptions");
            CallMethodOnFatalException(_fatalExceptionMonitor);   
        }
        private static void _fatalExceptionMonitor(Exception ex)
        {
       //     if (_log.IsInfoEnabled) _log.Info("Fatal Exception Occured - " + ex.Message);
            if (_log.IsFatalEnabled) _log.Fatal("Fatal Exception Occured - " + ex.Message , ex);
            CS.General_v3.Error.Reporter.ReportError(Enums.LOG4NET_MSG_TYPE.Warn, ex);
          //  if (_log.IsInfoEnabled) _log.Info("Fatal Exception Occured - Waiting until email is sent");
            if (_log.IsFatalEnabled) _log.Fatal("Fatal Exception Occured - Waiting until email is sent");
            //System.Threading.Thread.Sleep(15 * 1000); //wait 30 seconds;
        }
        public static string GetProcessName()
        {
            return System.Diagnostics.Process.GetCurrentProcess().ProcessName;
        }
        public static void CallMethodOnFatalException(ApplicationExceptionDelegate methodToCall)
        {
            FatalExceptionMethodCaller caller = new FatalExceptionMethodCaller(methodToCall);
            caller.Start();

        }

        /// <summary>
        /// Returns the application path, (folder only), with a slash
        /// </summary>
        public static string ApplicationPath
        {
            get
            {
                return CS.General_v3.Util.IO.GetApplicationRootLocalFolder();
                /*

                if (HttpRuntime.AppDomainAppId == null)
                {
                    string s = IO.GetDirName(System.Windows.Forms.Application.ExecutablePath);
                    if (s != null && s.Length > 0 && s[s.Length - 1] != '\\')
                        s += '\\';

                    return s;
                }
                else
                {
                    return HttpRuntime.AppDomainAppPath;
                }*/
            }
        }

        public static bool IsInFormsDesignMode()
        {

            return (System.Diagnostics.Process.GetCurrentProcess().ProcessName == "devenv");
        }

        
    }
}
