﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Util
{
    public static class MediaUtil
    {
        public static int GetMediaLengthInSeconds(string filePath)
        {
            string localPath = filePath;
            if (filePath.Contains("/"))
                localPath = CS.General_v3.Util.PageUtil.MapPath(filePath);

            return FFMpeg.FFMpeg.GetMediaDuration(localPath);


        }
    }
}
