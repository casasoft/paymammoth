﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;

namespace CS.General_v3.Util
{
    public static class ErrorsUtil
    {
        private static ILog _log = log4net.LogManager.GetLogger(typeof(ErrorsUtil));
        
        public static void WriteStringToErrorLog(string s)
        {
            _log.Fatal(s);
            //CS.General_v3.Util.IO.AppendToFile(getErrorLogFilePath(), s);
        }
        public static void WriteExceptionToErrorLog(Exception ex)
        {
            _log.Fatal(ex.Message, ex);
            //string s = ConvertExceptionToString(ex);
            //CS.General_v3.Util.IO.AppendToFile(getErrorLogFilePath(), s);
        }
        public static string ConvertExceptionToString(Exception ex)
        {
            string s = _convertExceptionToString(ex);
            s += "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\r\n";
            s += "+++++++++++++++++++++ EXCEPTION ENDS HERE+++++++++++++++++++++++++++++++++++\r\n";
            s += "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\r\n";
            return s;

        }
        private static string _convertExceptionToString(Exception ex)
        {
        
            
            StringBuilder sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendLine();
            sb.AppendLine("========================================================================================================================");
            sb.AppendLine("Error: " + ex.GetType().ToString() + " - " + CS.General_v3.Util.Date.Now.ToString("dd/MM/yyyy HH:mm:ss") + " :: " + ex.Message);
            sb.AppendLine("========================================================================================================================");
            sb.AppendLine("Source: " + ex.Source);
            sb.AppendLine("StackTrace:\r\n\r\n" + ex.StackTrace);
            if (ex.TargetSite != null)
            {
                sb.AppendLine("++Target Site Info:\r\n------------------\r\n");
                sb.AppendLine("Method Name:" + ex.TargetSite.Name);
                if (ex.TargetSite.Module != null)
                    sb.AppendLine("Module:" + ex.TargetSite.Module.FullyQualifiedName);
            }
            if (ex.Data  != null)
            {
                var keys = CS.General_v3.Util.ListUtil.GetListFromEnumerator<object>(ex.Data.Keys.GetEnumerator());
                foreach (var key in keys)
                {
                    sb.AppendLine("Data: <" + key.ToString() + ">: " +  ex.Data[keys].ToString());
                }
            }
            if (ex.InnerException != null)
            {
                sb.AppendLine("INNER EXCEPTION FOLLOWS");
                sb.AppendLine(_convertExceptionToString(ex.InnerException));
            }
            return sb.ToString();


        }

    }
}
