﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
//\using CS.General_v3.FileUploader;
using System.Web.Routing;
using System.Text.RegularExpressions;
using log4net;


namespace CS.General_v3.Util
{
    public static  class RoutingUtil
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(RoutingUtil));
        public static string ConvertRouteValueToString(string routeVal)
        {
            string s = routeVal;
            if (!string.IsNullOrWhiteSpace(s))
            {
                string regex = "-";
                s = Regex.Replace(s, regex, " ");

            }
            return s;
        }
        public static Route GetRouteByName(string routeName, bool throwErrorIfRouteNotFound = true)
        {
            Route route = (Route)RouteTable.Routes[routeName];
            if (route == null && throwErrorIfRouteNotFound)
            {
                throw new InvalidOperationException("General.RoutingUtil:: Route with name <" + routeName + "> does not exist. Make sure it is mapped on application_start");
            }
            return route;
        }

        public static string GetRouteUrlByName(string routeName, bool throwErrorIfRouteNotFound = true)
        {
            var route = GetRouteByName(routeName, throwErrorIfRouteNotFound);
            string s = route.Url;
            if (!s.StartsWith("/"))
            {
                s = "/" + s;
            }
            return s;
           
            
            
        }
        /// <summary>
        /// Maps a route to a page
        /// </summary>
        /// <param name="routeName">Name of route</param>
        /// <param name="routeUrl">Url of route to parse, for example "Content/{Title}/{Id}"</param>
        /// <param name="physicalFile">Physical file to map to</param>
        /// <param name="defaultValues"></param>
        /// <param name="constraints"></param>
        public static bool MapPageRoute(string routeName, string routeUrl, string physicalFile,
            Classes.Routing.MyRouteValueDictionary defaultValues, Classes.Routing.MyRouteValueDictionary constraints, bool? throwErrorIfPhysicalFileDoesNotExist = null)
        {
            if (string.IsNullOrWhiteSpace(routeUrl) || routeUrl == "/")
            {
                throw new InvalidOperationException("RouteUrl cannot be empty or '/' [Route: " + routeName + ", PhysicalFile: " + physicalFile +"]");
            }


            bool mapped = false;
            Classes.Routing.MyRouteValueDictionary tmpDefaultValues  = null;
            Classes.Routing.MyRouteValueDictionary tmpConstraints = null;
            if (defaultValues != null)
            {
                tmpDefaultValues = (Classes.Routing.MyRouteValueDictionary)defaultValues.Clone();
                tmpDefaultValues.RemoveNonUsedKeysFromRouteURL(routeUrl);
            }
            if (constraints != null)
            {
                tmpConstraints = (Classes.Routing.MyRouteValueDictionary)constraints.Clone();
                tmpConstraints.RemoveNonUsedKeysFromRouteURL(routeUrl);
            }

            //Remove the 'http://' or 'https://' just in case
            physicalFile = PageUtil.ConvertAbsoluteUrlToRelativeRootUrl(physicalFile);


            //physical file must start with a ~/
            if (!physicalFile.StartsWith("~"))
            {
                if (!physicalFile.StartsWith("/"))
                    physicalFile = "/" + physicalFile;
                physicalFile = "~" + physicalFile;
            }

            string physicalLocalFile = CS.General_v3.Util.PageUtil.MapPath(physicalFile);
            
            if (!System.IO.File.Exists(physicalLocalFile))
            {
                string msg ="Error mapping rount <" + routeName + ">: Physical file '" + physicalFile + "' must exist in order to map route!";
                if (throwErrorIfPhysicalFileDoesNotExist.HasValue && throwErrorIfPhysicalFileDoesNotExist.Value)
                    throw new InvalidOperationException(msg);
                else
                {
                    //Report Error
                    CS.General_v3.Error.Reporter.ReportError(Enums.LOG4NET_MSG_TYPE.Error, new InvalidOperationException(msg));
                }

            }
            else
            {

                //route url must not start with a /
                if (routeUrl.StartsWith("/"))
                    routeUrl = routeUrl.Substring(1);

                    var r = RouteTable.Routes.MapPageRoute(routeName, routeUrl, physicalFile, false,
                                                           tmpDefaultValues, tmpConstraints);
                
                mapped = true;
                
            }
            return mapped;

        }


        /// <summary>
        /// Maps a route to a page
        /// </summary>
        /// <param name="routeName">Name of route</param>
        /// <param name="routeUrl">Url of route to parse, for example "Content/{Title}/{Id}"</param>
        /// <param name="physicalFile">Physical file to map to</param>
        /// <param name="defaultValues"></param>
        /// <param name="constraints"></param>
        public static bool RemoveRoute(string routeName, bool throwErrorIfRouteNotFound = false)
        {
            bool ok = false;
            var route = GetRouteByName(routeName, throwErrorIfRouteNotFound);
            if (route != null)
            {
                ok = RouteTable.Routes.Remove(route);
            }
            return ok;
            

        }
        public static Route GetRoute(string routeName)
        {
            Route route = (Route)RouteTable.Routes[routeName];
            return route;

        }

        /// <summary>
        /// Returns the route associated with the route name
        /// </summary>
        /// <param name="routeName">Route name</param>
        /// <param name="paramValues">Parameter Values</param>
        /// <param name="fullyQualifiedURL">Whether to append the server URL, e.g http://www.casasoft.com.mt as well</param>
        /// <returns>URL</returns>
        public static string GetRouteUrl(string routeName, Classes.Routing.MyRouteValueDictionary paramValues, bool fullyQualifiedURL = true, bool throwErrorIfRouteNotMapped = true)
        {
            Classes.Routing.MyRouteValueDictionary tmpParamValues = null;
            //string routeURL = GetRouteByName(routeName);
            if (paramValues == null)
            {
                paramValues = new Classes.Routing.MyRouteValueDictionary();
            }
           /* if (paramValues != null)
            {
                tmpParamValues = (Classes.Routing.MyRouteValueDictionary)paramValues.Clone();
                tmpParamValues.RemoveNonUsedKeysFromRouteURL(routeURL);
            }*/

            string url = null;

            Route route = GetRoute(routeName);
            
            if (route == null)
            {
                if (throwErrorIfRouteNotMapped)
                {
                    throw new InvalidOperationException("Route with name <" + routeName + "> does not exist");
                }
            }
            else
            {
                //var tmp = route.GetVirtualPath(null, paramValues);
                url = "";
                url = paramValues.GetUrlFromRoute(route);
                //url =  Page.Current.GetRouteUrl(routeName, tmpParamValues) ?? "";
                if (fullyQualifiedURL)
                {
                    if (url.StartsWith("/")) url = url.Substring(1);
                    url = CS.General_v3.Util.PageUtil.GetApplicationBaseUrl() + url;
                }
            }
            return url;
        }

        public static string ConvertStringForRouteUrl(string s)
        {

            if (!string.IsNullOrEmpty(s))
            {
                while (s.EndsWith("."))
                {
                    s = CS.General_v3.Util.Text.RemoveLastNumberOfCharactersFromString(s,1);
                }
                
                s = CS.General_v3.Util.Text.ReplaceTexts(s, " and ", "&");
                s = CS.General_v3.Util.Text.ReplaceTexts(s, "-", "|","?", "\"", "'", "%");
                s = CS.General_v3.Util.Text.ReplaceTexts(s, " ", "    ", "   ", "  ");
                s = CS.General_v3.Util.Text.ReplaceTexts(s, "-", ".", "\"", "'", "`","=", "$", "#", "[", "]", "(", ")", "_", "<", ">", "?", "@", "~", "*", "&", "%", "!", "{", "}", "+", " ", "/", "\\");
                
                s = CS.General_v3.Util.Text.ReplaceTexts(s, "-", "-----", "----", "---", "--");
                while (s.EndsWith("-"))
                {
                    s = s.Substring(0, s.Length - 1);
                }
                s = CS.General_v3.Util.PageUtil.UrlEncode(s);
                
            }
            return s == null? null : s.ToLower() ;
        }
        public static T GetRouteValue<T>(string key, T defaultValue = default(T))
        {
            string s = "";
            s = (string)CS.General_v3.Util.PageUtil.GetRouteDataVariable(key);
            T value = CS.General_v3.Util.Other.ConvertStringToBasicDataType<T>(s);
            if (value == null)
            {
                value = defaultValue;

            }
            return value;

        }
        public static string GetRouteValue(string key, string defaultValue = "")
        {
            return GetRouteValue<string>(key, defaultValue);
            /*
            string s = "";
            s = (string)CS.General_v3.Util.PageUtil.GetRouteDataVariable(key);
            if (string.IsNullOrEmpty(s))
                s = defaultValue;
            return s;
            */
        }
        public static int GetRouteValueAsInt( string key, int defaultValue)
        {
            return  (int)GetRouteValue<int?>(key, defaultValue);
            
            /*int? i = GetRouteValueAsIntNullable( key);
            int result = defaultValue;
            if (i.HasValue)
                result = i.Value;
            return result;*/
        }
        public static int? GetRouteValueAsIntNullable(string key)
        {
            return GetRouteValue<int?>(key, null);
           // return (int?)GetRouteValueAsLongNullable(key);
        }
        public static long? GetRouteValueAsLongNullable(string key)
        {
            return GetRouteValue<long?>(key, null);
/*            string s = GetRouteValue(key);
            long? result = null;
            long i = 0;
            if (long.TryParse(s, out i))
                result = i;
            return result;*/
        }
    }
}
