﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Util
{
    public static class SocialNetworkingUtil
    {
        private const string FACEBOOK_BASE_URL = "facebook.com";
        private const string GOOGLE_PLUS_BASE_URL = "plus.google.com";
        private const string TWITTER_BASE_URL = "twitter.com";
        private const string LINKED_IN_BASE_URL = "linkedin.com";
        private const string ORKUT_BASE_URL = "orkut.com";
        private const string HI5_BASE_URL = "hi5.com";
        private const string MY_SPACE_BASE_URL = "myspace.com";

        public static Enums.SOCIAL_NETWORK_TYPES GetSocialNetworkFromUrl(string url)
        {
            string domain = PageUtil.GetDomainFromUrl(url, true);

            if (domain.ToLower().Contains(FACEBOOK_BASE_URL.ToLower()))
            {
                return Enums.SOCIAL_NETWORK_TYPES.Facebook;
            }
            if (domain.ToLower().Contains(GOOGLE_PLUS_BASE_URL.ToLower()))
            {
                return Enums.SOCIAL_NETWORK_TYPES.GooglePlus;
            }
            if (domain.ToLower().Contains(TWITTER_BASE_URL.ToLower()))
            {
                return Enums.SOCIAL_NETWORK_TYPES.Twitter;
            }
            if (domain.ToLower().Contains(LINKED_IN_BASE_URL))
            {
                return Enums.SOCIAL_NETWORK_TYPES.LinkedIn;
            }
            if (domain.ToLower().Contains(ORKUT_BASE_URL))
            {
                return Enums.SOCIAL_NETWORK_TYPES.Orkut;
            }
            if (domain.ToLower().Contains(HI5_BASE_URL))
            {
                return Enums.SOCIAL_NETWORK_TYPES.Hi5;
            }
            if (domain.ToLower().Contains(MY_SPACE_BASE_URL))
            {
                return Enums.SOCIAL_NETWORK_TYPES.MySpace;
            }
            return Enums.SOCIAL_NETWORK_TYPES.Other;
        }
    }
}
