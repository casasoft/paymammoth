using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using System.Reflection;
using CS.General_v3.Util;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using System.IO;

namespace CS.General_v3.Util
{
    public static partial class Other
    {
        public const string DATE_STRING_FORMAT = "yyyyMMddHHmmss";
        public static string TextForRewrite(string s)
        {
            return CS.General_v3.Util.Text.TxtForRewrite(s);
            
        }

        public static bool CheckIfApplicationRootContainsOnlineServerFile()
        {
            List<string> list = new List<string>();
            string basePath = CS.General_v3.Util.ApplicationUtil.ApplicationPath;
            list.Add(basePath + "online_server.txt");
            list.Add(basePath + @"App_Data\online_server.txt");

            for (int i = 0; i < list.Count; i++)
            {
                var p = list[i];
                if (File.Exists(p))
                    return true;
            }
            return false;
            
        }

        public static object GetObjectFromValueByDataType(Enums.DATA_TYPE dataType, object o)
        {
            object result = null;
            switch (dataType)
            {
                case Enums.DATA_TYPE.Bool:
                    if (o is string)
                        result = CS.General_v3.Util.Other.TextToBool((string)o);
                    else
                        result = o;
                    break;
                case Enums.DATA_TYPE.BoolNullable:
                    if (o is string)
                    {
                        result = CS.General_v3.Util.Other.TextToBoolNullable((string)o);
                    }
                    else
                    {
                        result = o;
                    }
                    break;
                case Enums.DATA_TYPE.DateTime:
                case Enums.DATA_TYPE.DateTimeNullable:

                    result = o;
                    break;
                case Enums.DATA_TYPE.Double:
                    if (o is string)
                        result = Util.Conversion.ToDouble(o);
                    else
                        result = o;
                    break;
                case Enums.DATA_TYPE.DoubleNullable:
                    if (o is string)
                    {
                        string s = (string)o;
                        if (!string.IsNullOrEmpty(s))
                            result = Util.Conversion.ToDouble(s);
                    }
                    else
                        result = o;
                    break;
                case Enums.DATA_TYPE.Integer:
                    if (o is string)
                        result = Util.Conversion.ToInt32(o);
                    else
                        result = o;
                    break;
                case Enums.DATA_TYPE.IntegerNullable:
                    if (o is string)
                    {
                        string s = (string)o;
                        if (!string.IsNullOrEmpty(s))
                            result = Util.Conversion.ToInt32(s);
                    }
                    else
                        result = o;

                    break;
                case Enums.DATA_TYPE.String:
                    result = o;
                    break;
                default:
                    result = o;
                    break;
            }
            return result;
        }
        public static T CloneDeep<T>(this T obj)
        {
            System.Runtime.Serialization.IFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            formatter.SurrogateSelector = new System.Runtime.Serialization.SurrogateSelector();
            formatter.SurrogateSelector.ChainSelector(
                new DeepCloning.NonSerialiazableTypeSurrogateSelector());
            var ms = new System.IO.MemoryStream();
            formatter.Serialize(ms, obj);
            ms.Position = 0;
            return (T)formatter.Deserialize(ms);
        }

        /// <summary>
        /// Perform a deep Copy of the object.
        /// </summary>
        /// <typeparam name="T">The type of object being copied.</typeparam>
        /// <param name="source">The object instance to copy.</param>
        /// <returns>The copied object.</returns>
        public static T CloneDeep2<T>(T source)
        {
            if (!typeof(T).IsSerializable)
            {
                throw new ArgumentException("The type must be serializable.", "source");
            }

            // Don't serialize a null object, simply return the default for that object
            if (Object.ReferenceEquals(source, null))
            {
                return default(T);
            }

            IFormatter formatter = new BinaryFormatter();
            System.IO.Stream stream = new System.IO.MemoryStream();
            using (stream)
            {
                formatter.Serialize(stream, source);
                stream.Seek(0, System.IO.SeekOrigin.Begin);
                return (T)formatter.Deserialize(stream);
            }
        }


        /// <summary>
        /// Creates an exact copy of the object, but not the same object
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static T CloneShallow<T>(T o, bool showErrorIfCannotClone)
        {
            T newObj = default(T);
            if (o != null)
            {
                try
                {
                    newObj = (T) Activator.CreateInstance(o.GetType());
                }
                catch (Exception ex)
                {
                    newObj = default(T);
                    if (showErrorIfCannotClone)
                        throw ex;
                }
                CloneShallow(o, newObj);
            }
            return newObj;
        }
        /// <summary>
        /// Creates an exact copy of the object, but not the same object
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static void CloneShallow(object o, object newObj)
        {
            if (o != null)
            {
                Type t = o.GetType();
                FieldInfo[] fields = t.GetFields();
                for (int i = 0; i < fields.Length; i++)
                {
                    try
                    {
                        FieldInfo f = fields[i];
                        object value = f.GetValue(o);
                        /*if (!f.FieldType.IsPrimitive && !f.FieldType.IsEnum && !(f.FieldType == typeof(string)) && f.FieldType.IsValueType)
                        {
                            object newValue = Clone(value, false);
                            value = newValue;
                        }*/
                        f.SetValue(newObj, value);
                    }
                    catch (Exception ex)
                    {
                        
                    }

                }

                PropertyInfo[] properties = t.GetProperties();
                for (int i = 0; i < properties.Length; i++)
                {
                    PropertyInfo p = properties[i];

                    if (p.CanRead && p.CanWrite)
                    {
                        try
                        {
                            object value = p.GetValue(o, null);
                            /*if (!p.PropertyType.IsPrimitive && !p.PropertyType.IsEnum && !(p.PropertyType == typeof(string)) && p.PropertyType.IsValueType)
                            {
                                object newValue = Clone(value, false);
                                value = newValue;
                            }*/
                            p.SetValue(newObj, value, null);
                        }
                        catch (Exception ex) { }

                    }
                }
            }
        }

        /*
        /// <summary>
        /// Splits a list into a list of lists, of size page.  Used for building tables
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">Original List</param>
        /// <param name="listSize">List size per each item</param>
        /// <param name="nullType">The null type to fill, normally it should be 'null'</param>
        /// <returns></returns>
        public static List<List<T>> SplitListIntoMultipleLists<T>(List<T> list, int listSize, T nullType)
        {
            
            return CS.General_v3.ListUtil.ListUtil.SplitListIntoMultipleLists(list, listSize, nullType, true, true);
            
        }
        /// <summary>
        /// Splits a list into a list of lists, of size page.  Used for building tables
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">Original List</param>
        /// <param name="listSize">List size per each item</param>
        /// <param name="nullType">The null type to fill, normally it should be 'null'</param>
        /// <param name="horiztonal">The direction of the splitting.  If horizontal, then order will be A B C D and on next set E F G H etc.. else it will split
        /// them vertically</param>
        /// <returns></returns>
        public static List<List<T>> SplitListIntoMultipleLists<T>(List<T> list, int listSize, T nullType, bool horiztonal)
        {
            return CS.General_v3.ListUtil.ListUtil.SplitListIntoMultipleLists(list, listSize, nullType, horiztonal);
        }
        /// <summary>
        /// Splits a list into a list of lists, of size page.  Used for building tables
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">Original List</param>
        /// <param name="listSize">List size per each item</param>
        /// <param name="nullType">The null type to fill, normally it should be 'null'</param>
        /// <param name="fillLastSet">Wheter to keep on filling the last set with the null types</param>
        /// <param name="horiztonal">The direction of the splitting.  If horizontal, then order will be A B C D and on next set E F G H etc.. else it will split
        
        /// <returns></returns>
        public static List<List<T>> SplitListIntoMultipleLists<T>(List<T> list, int listSize, T nullType, bool fillLastSet, bool horizontal)
        {
            return CS.General_v3.ListUtil.SplitListIntoMultipleLists(list, listSize, nullType, fillLastSet, horizontal);
        }

        */
        public static string GetControlClientName(Control ctrl)
        {
            Stack<Control> ctrls = new Stack<Control>();
            Control curr = ctrl;
            while (curr != null)
            {
                ctrls.Push(curr);
                curr = curr.Parent;
            }
            string s = ctrl.ClientID;
            int currIndex = -1;
            while (ctrls.Count > 0)
            {
                Control c = ctrls.Pop();
                if (c.ID != null)
                {
                    int index = s.IndexOf(c.ID, currIndex + 1);
                    if (index > -1)
                    {
                        currIndex = index;

                        if (index + c.ID.Length < s.Length && s[index + c.ID.Length] == '_')
                        {
                            s = s.Remove(index + c.ID.Length, 1);
                            s = s.Insert(index + c.ID.Length, "$");
                        }
                        if (index > 0 && s[index - 1] == '_')
                        {
                            s = s.Remove(index - 1, 1);
                            s = s.Insert(index - 1, "$");

                        }
                    }
                }


            }
            return s;
        }
       
        /// <summary>
        /// Compares a field, and returns any changes, formatted with it's title and a carraige return 
        /// </summary>
        /// <param name="newField">New data</param>
        /// <param name="oldField">Old Data</param>
        /// <param name="Title">Title of data</param>
        /// <returns>Text string</returns>
        public static string CompareField(object newField, object oldField, string Title)
        {
            return CompareField(newField, oldField, Title, true);
        }
        /// <summary>
        /// Compares a field, and returns any changes, formatted with it's title and a carraige return (optional)
        /// </summary>
        /// <param name="newField">New data</param>
        /// <param name="oldField">Old Data</param>
        /// <param name="Title">Title of data</param>
        /// <param name="AddCarriageReturn">Whether to add a carraige return on end</param>
        /// <returns>Text string</returns>
        public static string CompareField(object newField, object oldField, string Title, bool AddCarriageReturn)
        {
            string msg = "";
            if (newField is string && ((string)newField != (string)oldField))
            {
                string sOld = CS.General_v3.Util.Text.MakeOneLine((string)oldField);
                string sNew = CS.General_v3.Util.Text.MakeOneLine((string)newField);
                msg = Title + " changed from '" + sOld + "' to '" + sNew + "'";
            }
            else if (newField is int && ((int)newField != (int)oldField))
            {
                msg = Title + " changed from " + (int)oldField + " to " + (int)newField + "";
            }
            else if (newField is double)
            {
                double dNew = Convert.ToDouble(((double)newField).ToString("0.00"));
                double dOld = Convert.ToDouble(((double)oldField).ToString("0.00"));
                if (dNew != dOld)
                {
                    msg = Title + " changed from " + (double)oldField + " to " + (double)newField + "";
                }
            }
            
            else if (newField is bool && ((bool)newField != (bool)oldField))
            {
                msg = Title + " changed from " + CS.General_v3.Util.Other.BoolToText((bool)oldField) + " to " + CS.General_v3.Util.Other.BoolToText((bool)newField) + "";
            }
            else if (newField is DateTime)
            {
                DateTime dNew = ((DateTime)newField).Date;
                DateTime dOld = ((DateTime)oldField).Date;
                if (dNew.CompareTo(dOld) != 0)
                {
                    msg = Title + " changed from '" + dOld.ToString("dd MMM yyyy") + "' to '" + dNew.ToString("dd MMM yyyy") + "'";
                }
            }
            if (msg != "" && AddCarriageReturn)
                msg += "\r\n";
            return msg;
        }
        private static bool? __IsLocalHost = null;
        private static bool? _IsLocalHost
        {
            get
            {
                return __IsLocalHost;
/*                if (CS.General_v3.Util.PageUtil.ApplicationInfo != null)
                    return (bool?)CS.General_v3.Util.PageUtil.ApplicationInfo["_IsLocalHost"];
                else
                    return null;*/


            }
            set
            {
                __IsLocalHost = value;
                //CS.General_v3.Util.PageUtil.ApplicationInfo["_IsLocalHost"] = value; ;
            }
        }
        public static bool? IsLocalTestingMachine_CustomOverride { get; set; }
        
        public static bool IsLocalTestingMachine
        {
            get
            {
                if (IsLocalTestingMachine_CustomOverride.HasValue)
                    return IsLocalTestingMachine_CustomOverride.Value;
                else
                {
                    if (!_IsLocalHost.HasValue)
                    {
                        _IsLocalHost = false;
                        string processName = CS.General_v3.Util.ApplicationUtil.GetProcessName();
                        List<string> testingProcessNames = new List<string>();
                        testingProcessNames.Add("WebDev.WebServer40");
                        if (testingProcessNames.Contains(processName))
                        {
                            _IsLocalHost = true;
                        }
                        else
                        {
                            if (IsWebApplication)
                            {
                                string s = HttpRuntime.AppDomainAppPath;
                                if (Regex.Match(s, @".:\\(Work|Work_New)\\", RegexOptions.IgnoreCase).Success)
                                {
                                    _IsLocalHost = true;
                                }
                               
                            }
                            
                            if (__IsLocalHost == true && CheckIfApplicationRootContainsOnlineServerFile())
                                __IsLocalHost = false;
                        }
                    }

                    return _IsLocalHost.Value;
                    
                    
                }

            }
        }
        public static void SetIsLocalHostValue()
        {
            _IsLocalHost = IsLocalTestingMachine;
        }
        public static void SetIsLocalHostValue(bool value)
        {
            _IsLocalHost = value;
        }

      

        public static bool IsWebApplication
        {
            get
            {
                bool isWebApp = false;
                try
                {

                    bool b = false;
                    if (System.Web.Hosting.HostingEnvironment.IsHosted)
                    {
                        b = true;
                        
                    }

                    else
                    {

                        string s = System.Web.Configuration.WebConfigurationManager.AppSettings["Website_IsWebsite"];
                        if (s != null && s.ToLower() == "true")
                        {
                            b = true;
                        }

                    }
                    isWebApp = b;
                }
                catch
                {
                    isWebApp = false;
                }
                return isWebApp;
            }
        }
        public static bool CheckNullValue(object o)
        {
            bool result = false;
            if (o == null)
                result = true;
            else
            {
                if (o is double)
                {
                    double d = (double)o;
                    result = (d == double.MinValue || d == float.MinValue);
                }
                else if (o is int)
                {
                    int i = (int)o;
                    result = (i == int.MinValue);
                }
                else if (o is float)
                {
                    float f = (float)o;
                    result = (f == float.MinValue);
                }
                
                else if (o is DateTime)
                {
                    DateTime d = (DateTime)o;
                    result = (d == DateTime.MinValue);
                }
                else if (o is string)
                {
                    string s = (string)o;
                    result = (s == null);
                }
            }
            return result;

        }
        public static byte[] charArrayToByteArray(char[] c)
        {
            byte[] b = new byte[c.Length];
            for (int i = 0; i < c.Length; i++)
            {
                b[i] = (byte)c[i];
            }
            return b;
        }

        public static char[] byteArrayToCharArray(byte[] b)
        {
            char[] c = new char[b.Length];
            for (int i = 0; i < b.Length; i++)
            {
                c[i] = (char)b[i];
            }
            return c;
        }

        /// <summary>
        /// converts the given text to one without spaces and punctuations, and & is converted to AND
        /// </summary>
        /// <param name="txt">given text</param>
        /// <returns>text withouth spaces & punctionations</returns>
        public static string convertForSearch(string txt)
        {
            string tmp = txt;
            if (tmp != null && tmp != "")
            {
                tmp = tmp.Replace(".", "");
                tmp = tmp.Replace(",", "");
                tmp = tmp.Replace(";", "");
                tmp = tmp.Replace(":", "");
                tmp = tmp.Replace("'", "");
                tmp = tmp.Replace("\"", "");
                tmp = tmp.Replace("`", "");
                tmp = tmp.Replace("~", "");
                tmp = tmp.Replace("|", "");
                tmp = tmp.Replace("\\", "");
                tmp = tmp.Replace("/", "");
                tmp = tmp.Replace(" ", "");
                tmp = tmp.Replace("_", "");
                tmp = tmp.Replace("+", "");
                tmp = tmp.Replace("=", "");
                tmp = tmp.Replace("-", "");
                tmp = tmp.Replace("*", "");
                tmp = tmp.Replace("&", "and");
                tmp = tmp.Replace("<", "");
                tmp = tmp.Replace(">", "");
                tmp = tmp.Replace("?", "");
                tmp = tmp.Replace("#", "");
                tmp = tmp.Replace("@", "");
                tmp = tmp.Replace("%", "");
                tmp = tmp.Replace("^", "");
                tmp = tmp.Replace("(", "");
                tmp = tmp.Replace(")", "");
                tmp = tmp.Replace("!", "");
                tmp = tmp.Replace("\n", "");
                tmp = tmp.Replace("\r", "");
                tmp = tmp.Replace("\t", "");
            }
            return tmp;


        }
        public static bool CheckValidEmail(string email)
        {
            int atPos = email.IndexOf('@');
            int dotPos = -1;
            if (atPos > -1)
                dotPos = email.IndexOf(".", atPos);
            if (dotPos > -1 && (dotPos == atPos + 1 || dotPos == email.Length - 1))
                dotPos = -1;
            return (atPos > -1 && dotPos > -1);
        }

        public static object ObjectToCriteria(object o, SEARCH_CRITERIA_TYPE type)
        {
            switch (type)
            {
                case SEARCH_CRITERIA_TYPE.Text: return (string)o;
                case SEARCH_CRITERIA_TYPE.Integer:
                    {
                        if (o == null)
                            return int.MinValue;
                        else
                            return (int)o;
                    }
                case SEARCH_CRITERIA_TYPE.Double:
                    {
                        if (o == null)
                            return double.MinValue;
                        else
                            return (double)o;
                    }
                case SEARCH_CRITERIA_TYPE.Date :
                    {
                        if (o == null)
                            return DateTime.MinValue;
                        else
                            return (DateTime)o;
                    }
                case SEARCH_CRITERIA_TYPE.Boolean:
                    {
                        if (o == null)
                        {
                            return null;
                        }
                        else
                        {
                            return (bool)o;
                        }
                    }
                    
                case SEARCH_CRITERIA_TYPE.Enumeration:
                    {
                        if (o == null)
                            return 0;
                        else
                            return (int)o;
                    }
                    

            }
            return null;
        }

        public enum SEARCH_CRITERIA_TYPE
        {
            Integer, Double, Text, Date, Boolean,Enumeration
        }

        public interface ISearchCriteria<T>
        {
            bool Check(T item);
            string CriteriaAdminText();
        }

        public interface ISearchComparer
        {
            string SortBy
            {
                get;
                set;
            }
            string SortType
            {
                get;
                set;
            }
        }
        private static string CriteriaTxtObject(object o)
        {
            string txt = "";
            if ((o is string) || (o is int))
            {
                txt += o.ToString();
            }
            else if ((o is DateTime))
            {
                DateTime tmp = (DateTime)o;
                txt += tmp.ToString("dd/MM/yyyy");
            }
            else if (o is double)
            {
                double tmp = (double)o;
                txt += tmp.ToString("0.00");
            }
            
            else if (o is bool)
            {
                if ((bool)o)
                    txt += "yes";
                else
                    txt += "no";
            }
            return txt;
        }
        



        /*private static string SQLCriteria(string Name, object o)
        {
            string ret = "";
            if (!(o is DateTime))
            {

            }
            ret = "(" + Name + " = " + 
        }*/

        /*
        /// <summary>
        /// Converts a string value into the required data ttype
        /// </summary>
        /// <param name="s"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static object AsCriteria(string s, SEARCH_CRITERIA_TYPE type)
        {
            switch (type)
            {
                case SEARCH_CRITERIA_TYPE.Text:
                    {
                        string tmp = null;
                        AsCriteria(s, ref tmp);
                        return tmp;
                    }
                case SEARCH_CRITERIA_TYPE.Date:
                    {
                        DateTime tmp = DateTime.MinValue;
                        AsCriteria(s, ref tmp);
                        return tmp;
                    }
                case SEARCH_CRITERIA_TYPE.Integer:
                    {
                        int tmp = int.MinValue;
                        AsCriteria(s, ref tmp);
                        return tmp;
                    }
                case SEARCH_CRITERIA_TYPE.Double:
                    {
                        double tmp = double.MinValue;
                        AsCriteria(s, ref tmp);
                        return tmp;
                    }
                case SEARCH_CRITERIA_TYPE.Boolean:
                    {
                        bool b = false;
                        AsCriteria(s,ref b);
                        
                        return tmp;
                    }
                case SEARCH_CRITERIA_TYPE.Enumeration:
                    {
                        int tmp = 0;
                        AsCriteria(s, ref tmp);
                        if (tmp == Int32.MinValue)
                            tmp = 0;
                        return tmp;
                    }
                    
            }
            return null;
        }
        
        
        public static void AsCriteria(string s, ref int var)
        {
            if (s != "")
            {
                var = Convert.ToInt32(s);
            }
            else
            {
                var = int.MinValue;
            }
        }
        public static void AsCriteria(string s, ref DateTime var)
        {

            if (s != "")
            {
                var = Convert.ToDateTime(s);
            }
            else
            {
                var = DateTime.MinValue;
            }
        }
        public static void AsCriteria(string s, ref double var)
        {

            if (s != "")
            {
                var = Convert.ToDouble(s);
            }
            else
            {
                var = double.MinValue;
            }
        }
        public static void AsCriteria(string s, ref string var)
        {
            if (s != "")
            {
                var = s;
            }
            else
                var = null;
        }*/
        /*public static object AsCriteria2(SEARCH_CRITERIA_TYPE crit, string s)
        {
            object ret = null;
            switch (crit)
            {
                case SEARCH_CRITERIA_TYPE.Boolean:
                    {
                        CS.Types.MyBool val = new Types.MyBool();
                        s = s.ToLower();
                        if (s != "")
                        {
                            val.Value = ((s == "1") || (s == "true") || (s == "yes") || (s == "on"));
                        }
                        ret = val;
                    }
                    break;
                case SEARCH_CRITERIA_TYPE.Date:
                    {
                        DateTime val;
                        
                        ret = val;
                    }
                    break;
                case SEARCH_CRITERIA_TYPE.Double:
                    {
                        double val;
                        if (s != "")
                        {
                            val = Convert.ToDouble(s);
                        }
                        else
                        {
                            val = double.MinValue;
                        }
                        ret = val;
                    }
                    break;
                case SEARCH_CRITERIA_TYPE.Integer:
                    {
                        int val;
                        if (s != "")
                        {
                            val = Convert.ToInt32(s);
                        }
                        else
                        {
                            val = int.MinValue;
                        }
                        ret = val;
                    }
                    break;
                case SEARCH_CRITERIA_TYPE.Text:
                    {
                        if (s != "")
                        {
                            string val = s;
                            ret = val;
                        }
                        else
                            ret = null;
                    }
                    break;
            }
            return ret;


        }*/
        /*
        /// <summary>
        /// A search item
        /// </summary>
        /// <param name="From">Object to compare with must be from this value.</param>
        /// <param name="To">Object to compare with must be up to this value.</param>
        /// <param name="Compare">Object to be compared</param>
        public static bool CheckCriteria(IComparable From, IComparable To, IComparable Compare)
        {
            bool ok = true;
            IComparable _From = CheckCriteriaOK(From);
            IComparable _To = CheckCriteriaOK(To);
            IComparable _Compare = CheckCriteriaOK(Compare);
            
            if (_Compare != null)
            {


                //if (!(From is DateTime))
               // {
                    if (_From != null)
                        ok = (_Compare.CompareTo(_From) >= 0);
                    else
                        ok = true;
                    if (ok && (_To != null))
                        ok = (_Compare.CompareTo(_To) <= 0);
                //}
                //*else
                {
                    DateTime dFrom = (DateTime)_From, dTo = (DateTime)_To, dCompare = (DateTime)_Compare;
                    if (From != null)
                        ok = (dCompare.Date.CompareTo(dFrom.Date) >= 0);
                    else
                        ok = true;
                    if (ok && (_To != null))
                        ok = (dCompare.Date.CompareTo(dTo.Date) <= 0);
                }/*
            }
            return ok;
        }*/
        /*
        /// <summary>
        /// A search item
        /// </summary>
        /// <param name="Equals">Object to compare with must be equal to this value</param>
        /// <param name="Compare">Object to be compared</param>
        public static bool CheckCriteria(IComparable Equals, IComparable Compare)
        {
            bool ok = true;
            IComparable _Equals = CheckCriteriaOK(Equals);
            IComparable _Compare = CheckCriteriaOK(Compare);

            if (_Compare != null)
            {
                ok = true;
                if (_Equals != null)
                {
                    if ((_Equals is string) || (_Equals is String))
                    {
                        string _equals = (string)Equals;
                        string _compare = (string)Compare;
                        _equals = _equals.ToLower();
                        _compare = _compare.ToLower();
                        ok = (_compare.IndexOf(_equals) > -1);
                    }
                    else
                    {
                        ok = (_Equals.CompareTo(_Compare) == 0);
                    }
                }
            }
            return ok;
        }
        */
        

        //[Serializable]
        //public class SEARCH_FIELD
        //{
        //    [Serializable]
        //    public enum SEARCH_FIELD_TYPE
        //    {
        //        Integer, Double, Text, Date, Boolean
        //    }
        //    /// <summary>
        //    /// Converts a string into its object data type
        //    /// </summary>
        //    /// <param name="type">The type of the object</param>
        //    /// <param name="data">The data, in string.</param>
        //    /// <returns>The object</returns>
        //    public static object convertType(SEARCH_FIELD_TYPE type, string data)
        //    {
                
        //        string s;
        //        DateTime d;
        //        int i;
        //        double dbl;
        //        bool b;
        //        switch (type)
        //        {
        //            case SEARCH_FIELD_TYPE.Text: s = data.ToString();
        //                if (s != "")
        //                    return s;
        //                else
        //                    return null;
        //            case SEARCH_FIELD_TYPE.Integer: i = Convert.ToInt32(data);
        //                return i;
        //            case SEARCH_FIELD_TYPE.Double: dbl = Convert.ToDouble(data);
        //                return dbl;
        //            case SEARCH_FIELD_TYPE.Date: d = Convert.ToDateTime(data);
        //                return d;
        //            case SEARCH_FIELD_TYPE.Boolean:
        //                if (data.ToUpper() == "TRUE" ||
        //                    data.ToUpper() == "YES" ||
        //                    data.ToUpper() == "ON" ||
        //                    data.ToUpper() == "1")
        //                    b = true;
        //                else
        //                    b = false;
        //                return b;

        //        }
        //        return null;

        //    }
        //    public bool FieldOK = true;
        //    public string PropertyText;
        //    public string PropertyName;
        //    public bool isCurrency = false;
        //    public object From = null, To = null, EqualsObject = null;
        //    public ArrayList ListText = null;
        //    public string FromText = null, ToText = null, EqualsText = null;
        //    public ArrayList List = null;

        //    private string ObjToStr(object o)
        //    {
        //        string str = "";
        //        if (o is string)
        //            str += "'";
        //        if (o is DateTime)
        //            str += ((DateTime)o).ToString("dd MMM yy");
        //        else if (o is bool)
        //        {
        //            if ((bool)o)
        //            {
        //                str += "Yes";
        //            }
        //            else
        //                str += "No";
        //        }
        //        else
        //            str += o.ToString();
        //        if (o is string)
        //            str += "'";
        //        return str;
        //    }

        //    private void CreateTexts()
        //    {
        //        if (EqualsObject != null)
        //        {
        //            EqualsText = ObjToStr(EqualsObject);
        //            if (isCurrency)
        //                EqualsText = Settings.CultureInfo.CurrencySymbol + EqualsText;
        //        }
        //        if (From != null)
        //        {
        //            FromText = ObjToStr(From);
        //            if (isCurrency)
        //                FromText = Settings.CultureInfo.CurrencySymbol + FromText;
        //        }
        //        if (To != null)
        //        {
        //            ToText = ObjToStr(To);
        //            if (isCurrency)
        //                ToText = Settings.CultureInfo.CurrencySymbol + ToText;
        //        }
        //        if (List != null)
        //        {
        //            ListText = new ArrayList();
        //            for (int i = 0; i < List.Count; i++)
        //            {
        //                string tmp = "";
        //                if (isCurrency)
        //                    tmp = Settings.CultureInfo.CurrencySymbol;
        //                tmp += ObjToStr(List[i]);
        //                ListText.Add(tmp);
        //            }
        //        }

        //    }

        //    /// <summary>
        //    /// Search field for filtering purposes.
        //    /// </summary>
        //    /// <param name="NameText">The name to be displayed for the property</param>
        //    /// <param name="_PropertyName">The name of the property (Case-Sensitive). Can include nested properties, like Property1.NestedProperty.SubNested</param>
        //    /// <param name="_isCurrency">Whether it is a currency or not. Only used for displaying purposes, to add the Currency sign</param>
        //    /// <param name="list">An array list of values, to be ORed together.  For example if value can be one of Orange, Apple and Banana, enter an array list with 3 strings 'Orange','Apple', 'Banana'</param>
        //    public SEARCH_FIELD(string NameText, string _PropertyName, bool _isCurrency, ArrayList list)
        //    {
        //        PropertyText = NameText;
        //        PropertyName = _PropertyName;
        //        List = list;
        //        FieldOK = (list.Count > 0);
        //        isCurrency = _isCurrency;
        //        CreateTexts();
        //    }
        //    /// <summary>
        //    /// Search field for filtering purposes.
        //    /// </summary>
        //    /// <param name="NameText">The name to be displayed for the property</param>
        //    /// <param name="_PropertyName">The name of the property (Case-Sensitive). Can include nested properties, like Property1.NestedProperty.SubNested</param>
        //    /// <param name="type">The type of the value.  This is used for Util.Conversion purposes, because the value is passed in a string.</param>
        //    /// <param name="_isCurrency">Whether it is a currency or not. Only used for displaying purposes, to add the Currency sign</param>
        //    /// <param name="_Equals">The value must be equal to this value.  For strings, it is a LIKE clause.</param>
        //    public SEARCH_FIELD(string NameText, string _PropertyName, SEARCH_FIELD_TYPE type, bool _isCurrency, string _Equals)
        //    {
        //        PropertyText = NameText;
        //        PropertyName = _PropertyName;
        //        FieldOK = (_Equals != "");
        //        if (FieldOK)
        //            EqualsObject = convertType(type, _Equals);
        //        isCurrency = _isCurrency;
        //        CreateTexts();
        //    }
        //    /// <summary>
        //    /// Search field for filtering purposes.
        //    /// </summary>
        //    /// <param name="NameText">The name to be displayed for the property</param>
        //    /// <param name="_PropertyName">The name of the property (Case-Sensitive). Can include nested properties, like Property1.NestedProperty.SubNested</param>
        //    /// <param name="type">The type of the value.  This is used for Util.Conversion purposes, because the value is passed in a string.</param>
        //    /// <param name="_isCurrency">Whether it is a currency or not. Only used for displaying purposes, to add the Currency sign</param>
        //    /// <param name="_From">The value must be from this value.  If the TO is left empty, it is from this value upwards</param>
        //    /// <param name="_To">The value must be up to this value.  If the FROM is left empty, it is from this value downwards</param>
        //    public SEARCH_FIELD(string NameText, string _PropertyName, SEARCH_FIELD_TYPE type, bool _isCurrency, string _From, string _To)
        //    {
        //        PropertyText = NameText;
        //        PropertyName = _PropertyName;
        //        FieldOK = (_From != "" || _To != "");
        //        if (_From != "")
        //            From = convertType(type, _From);
        //        if (_To != "")
        //            To = convertType(type, _To);
        //        isCurrency = _isCurrency;
        //        CreateTexts();
        //    }
        //    /// <summary>
        //    /// Search field for filtering purposes.
        //    /// </summary>
        //    /// <param name="NameText">The name to be displayed for the property</param>
        //    /// <param name="_PropertyName">The name of the property (Case-Sensitive). Can include nested properties, like Property1.NestedProperty.SubNested</param>
        //    /// <param name="_isCurrency">Whether it is a currency or not. Only used for displaying purposes, to add the Currency sign</param>
        //    /// <param name="_Equals">The value must be equal to this value.</param>
        //    public SEARCH_FIELD(string NameText, string _PropertyName, bool _isCurrency, object _Equals)
        //    {
        //        PropertyText = NameText;
        //        PropertyName = _PropertyName;
        //        isCurrency = _isCurrency;
        //        EqualsObject = _Equals;
        //        CreateTexts();
        //    }
        //    /// <summary>
        //    /// Search field for filtering purposes.
        //    /// </summary>
        //    /// <param name="NameText">The name to be displayed for the property</param>
        //    /// <param name="_PropertyName">The name of the property (Case-Sensitive). Can include nested properties, like Property1.NestedProperty.SubNested</param>
        //    /// <param name="_isCurrency">Whether it is a currency or not. Only used for displaying purposes, to add the Currency sign</param>
        //    /// <param name="_From">The value must be from this value.  If the TO is left null, it is from this value upwards</param>
        //    /// <param name="_To">The value must be up to this value.  If the FROM is left null, it is from this value downwards</param>
        //    public SEARCH_FIELD(string NameText, string _PropertyName, bool _isCurrency, object _From, object _To)
        //    {
        //        PropertyText = NameText;
        //        PropertyName = _PropertyName;
        //        From = _From;
        //        To = _To;
        //        isCurrency = _isCurrency;
        //        CreateTexts();
        //    }
        //}

        /// <summary>
        /// calls a generic method of an object
        /// </summary>
        /// <param name="obj">The object</param>
        /// <param name="MethodName">The method name. Case sensitive.  Can be further deep down, example Object.ClassItem1.Item2.MethodCall</param>
        /// <param name="parameters">Parameter list. Can be null if no parameters</param>
        /// <returns>The return type of the method, if any</returns>
        public static object callMethod(object obj, string MethodName, params object[] parameters)
        {
            Type[] types = null;
            if (parameters != null && parameters.Length > 0)
            {
                types = new Type[parameters.Length];
                for (int i = 0; i < parameters.Length; i++)
                {
                    if (parameters[i] != null)
                    {
                        types[i] = parameters[i].GetType();
                    }
                    else
                    {
                        types[i] = null;
                    }
                }
            }
            return callMethod(obj, MethodName, parameters, types);
        }
        /// <summary>
        /// calls a generic method of an object
        /// </summary>
        /// <param name="obj">The object</param>
        /// <param name="MethodName">The method name. Case sensitive.  Can be further deep down, example Object.ClassItem1.Item2.MethodCall</param>
        /// <param name="parameters">Parameter list. Can be null if no parameters</param>
        /// <returns>The return type of the method, if any</returns>
        public static object callMethod(object obj, string MethodName, object[] parameters, Type[] typesOfParams)
        {
            int dotPos = MethodName.LastIndexOf('.');
            string _propName = "";
            string _methodName = MethodName;
            object currObj = obj;
            if (dotPos > -1)
            {
                _propName = MethodName.Substring(0, dotPos);
                currObj = getProperty(obj, _propName);
                _methodName.Substring(dotPos + 1);
            }
            Type objType = currObj.GetType();
            System.Reflection.MethodInfo method = null;
            if (typesOfParams == null)
                method = objType.GetMethod(_methodName);
            else
                method = objType.GetMethod(_methodName, typesOfParams);
            object[] methodParams = null;
            if (parameters != null && parameters.Length > 0)
                methodParams = parameters;
            return method.Invoke(currObj, methodParams);
        }

        /// <summary>
        /// Gets a property 
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="property"></param>
        /// <returns></returns>
        public static object getProperty(object obj, string property)
        {
            System.Reflection.PropertyInfo propInfo = null;

            System.Type itemType = null;
            object currObj = obj;
            string[] properties = property.Split('.');
            for (int i = 0; i < properties.Length; i++)
            {
                itemType = currObj.GetType();
                propInfo = itemType.GetProperty(properties[i]);
                currObj = propInfo.GetValue(currObj, null);
                if (currObj == null)
                {
                    throw new NullReferenceException("Property does not exist in this object");
                }

            }
            return currObj;
        }

        /// <summary>
        /// Sets a property 
        /// </summary>
        /// <param name="obj">The object</param>
        /// <param name="property">Property name</param>
        /// /// <param name="newValue">The new value</param>
        public static void setProperty(object obj, string property,object newValue)
        {
            System.Reflection.PropertyInfo propInfo = null;

            System.Type itemType = null;
            object currObj = obj;
            string[] properties = property.Split('.');
            for (int i = 0; i < properties.Length; i++)
            {
                itemType = currObj.GetType();
                propInfo = itemType.GetProperty(properties[i]);
                if (i < properties.Length - 1)
                {
                    currObj = propInfo.GetValue(currObj, null);
                    if (currObj == null)
                    {
                        throw new NullReferenceException("Property does not exist in this object");
                    }
                }

            }
            propInfo.SetValue(currObj,newValue,null);
        }

        public static int NOT_AVAILABLE = -1;

        //public static bool StrToBool(string s)
        //{
        //    bool? b = StrToBoolNullable(s);
        //    return b.GetValueOrDefault(false);
        //}
        //public static bool? StrToBoolNullable(string s)
        //{
        //    bool? b = null;
        //    if (!string.IsNullOrWhiteSpace(s))
        //    {
        //        s = s.ToUpper();
        //        b =((s == "1") || (s == "YES") || s == "TRUE" || s == "ON");
        //    }
        //    return b;
        //}

        /// <summary>
        /// Returns '1' if true, else '0'
        /// </summary>
        /// <param name="b"></param>
        /// <returns></returns>
        public static string BoolToStr(bool? b)
        {
            if (b.HasValue)
            {
                if (b.Value)
                    return "1";
                else
                    return "0";
            }
            else
                return "";
        }
        /// <summary>
        /// Returns 'Yes' if true, else 'No'
        /// </summary>
        /// <param name="b"></param>
        /// <returns></returns>
        public static string BoolToText(bool? b)
        {
            string s = "";
            if (b.HasValue)
            {
                if (b.Value)
                    s="Yes";
                else
                    s = "No";
            }
            return s;
        }
        public static bool? TextToBoolNullable(string s)
        {
            bool? b = null;
            if (s == null) s = "";
            s = s.ToLower();
            switch (s)
            {

                case "1":
                case "y":
                case "yes":
                case "true":
                case "t":
                case "on":
                    b = true;
                    break;
                case "off":
                case "false":
                case "no":
                case "n":
                case "0":
                case "f":
                    b = false;
                    break;

            }
            return b;
        }
        public static bool TextToBool(string s)
        {

            bool b = TextToBoolNullable(s).GetValueOrDefault(false);
            return b;
        }
        public static string NumToStr(object num)
        {
            if (num is int)
            {
                if ((int)num == NOT_AVAILABLE)
                    return "";
                else
                    return ((int)num).ToString();
            }
            else if (num is double)
            {
                if ((double)num == (double)NOT_AVAILABLE)
                    return "";
                else
                    return ((double)num).ToString();
            }
            return "";
        }
        public static double StrToNum(string str)
        {
            if (str == "")
            {
                return (double)NOT_AVAILABLE;
            }
            else
            {
                return Convert.ToDouble(str);
            }

        }
        public static void StrToNum(string str, out double num)
        {
            if (str == "")
            {
                num = (double)NOT_AVAILABLE;
            }
            else
            {
                num = Convert.ToDouble(str);
            }
            num = (double)NOT_AVAILABLE;

        }




        
        /// <summary>
        /// Executes a program, and returns its process. Must be a command line!  If path specified is a document, like PDF, etc
        /// it will be opened using the default program
        /// </summary>
        /// <param name="path">Path of program</param>
        /// <param name="parameters">Optional. command line paramters. </param>
        /// <returns></returns>
        public static System.Diagnostics.Process executeProgram(string path,string parameters)
        {
            System.Diagnostics.Process process = System.Diagnostics.Process.Start(path,parameters);
            return process; 
        }

        /// <summary>
        /// A clas which holds information about a data field in databse
        /// </summary>
        public class DATA_FIELD
        {
            public string field;
            public string value;
            public CS.General_v3.Controls.WebControls.Common.FIELD_TYPE_VALUE type;

            /// <summary>
            /// Default constructor
            /// </summary>
            public DATA_FIELD()
            {

            }

            /// <summary>
            /// Sets the Data Field to the specified values
            /// </summary>
            /// <param name="fld">The field name </param>
            /// <param name="val">The value of the field</param>
            /// <param name="type">The data type of the field</param>
            public DATA_FIELD(string fld, string val, CS.General_v3.Controls.WebControls.Common.FIELD_TYPE_VALUE type)
            {
                field = fld;
                value = val;
                this.type = type;

            }
        }

        //[Serializable]
        //public class SEARCH_DATE
        //{
        //    public SEARCH_DATE(DateTime _start, DateTime _end)
        //    {
        //        StartDate = _start;
        //        EndDate = _end;
        //        hasStart = hasEnd = true;
        //        if (EndDate.Hour == 0 && EndDate.Minute == 0 && EndDate.Second == 0)
        //            EndDate = EndDate.Add(new TimeSpan(23, 59, 59));
        //    }
        //    public SEARCH_DATE(string val)
        //    {
        //        if (val != "")
        //            StartDate = Convert.ToDateTime(val);
        //        hasStart = (val!= "");
        //        hasEnd = false;
        //        isValue = true;
        //    }
        //    public SEARCH_DATE(string _start, string _end)
        //    {
        //        isValue = false;
        //        if (_start != "")
        //            StartDate = Convert.ToDateTime(_start);
        //        if (_end != "")
        //            EndDate = Convert.ToDateTime(_end);
        //        if (EndDate.Hour == 0 && EndDate.Minute == 0 && EndDate.Second == 0)
        //            EndDate = EndDate.Add(new TimeSpan(23, 59, 59));
        //        hasStart = (_start != "");
        //        hasEnd = (_end != "");
        //    }
        //    public DateTime StartDate, EndDate;
        //    public bool hasStart, hasEnd,isValue = false;
        //    public string asSQL(string FieldName)
        //    {
        //        string sql = "";
        //        if (hasStart || hasEnd)
        //        {
        //            sql = "(";
        //            if (hasStart)
        //            {
        //                sql += FieldName;
        //                if (isValue)
        //                    sql += " = ";
        //                else
        //                    sql += " >= ";
        //                sql += CS.General_v3.Util.MySQL.forMySql(StartDate);
        //            }
        //            if (hasEnd)
        //            {
        //                if (hasStart)
        //                    sql += " AND ";
        //                sql += FieldName + " <= " + CS.General_v3.Util.MySQL.forMySql(EndDate);
        //            }
        //            sql += ")";
        //        }
        //        return sql;
        //    }
        //    public string asText(string Field)
        //    {
        //        string txt = "";
        //        if (hasStart || hasEnd)
        //        {
        //            txt = Field;
        //            if (hasStart)
        //            {
        //                if (isValue)
        //                {
        //                    txt += " is ";
        //                }
        //                else
        //                {
        //                    txt += " from ";
        //                }
        //                txt += StartDate.ToString("dd MMM yy");
        //            }
        //            if (hasEnd)
        //            {
        //                if (hasStart )
        //                    txt += " to ";
        //                else
        //                    txt += " until ";
        //                txt += EndDate.ToString("dd MMM yy");
        //            }
        //            txt += "<br/>";
        //        }
        //        return txt;
                
        //    }
        //}
        //[Serializable]
        //public class SEARCH_BOOLEAN
        //{
        //    private bool _val = false;
        //    private bool hasVal = false;
        //    public SEARCH_BOOLEAN(string val)
        //    {
        //        hasVal = (val != "");
        //        val = val.ToUpper();
        //        _val = ((val == "YES") || (val == "TRUE") || (val == "ON") || (val == "1"));
        //    }
        //    public string asSQL(string fieldName)
        //    {
        //        string ret = ""; 
        //        if (hasVal)
        //        {
        //            ret = "(" + fieldName + " = ";
        //            if (_val)
        //                ret += "true";
        //            else
        //                ret += "false";
        //            ret += ")";
        //        }
        //        return ret;
        //    }
        //    public string asText(string Field)
        //    {
        //        string txt = "";
        //        if (hasVal)
        //        {
        //            txt = Field + " is ";
        //            if (_val)
        //                txt += " true";
        //            else
        //                txt += " false";
        //            txt += "<br/>";
        //        }
        //        return txt;
        //    }
        //}
        //[Serializable]
        //public class SEARCH_NUMBER
        //{
        //    public bool isCurrency = false;
        //    public bool isPercentage = false;
        //    public double From, To;
        //    public bool hasFrom,hasTo,isValue;
        //    public SEARCH_NUMBER(int _from, int _to)
        //    {
        //        From = (double)_from;
        //        To = (double)_to;
        //        hasFrom = hasTo = true;
        //        isValue = false;
        //    }
        //    public SEARCH_NUMBER(double _from, double _to)
        //    {
        //        From = _from;
        //        To = _to;
        //        hasFrom = hasTo = true;
        //        isValue = false;
        //    }
        //    public SEARCH_NUMBER(string _from, string _to)
        //    {
        //        if (_from != "")
        //            From = Convert.ToDouble(_from);
        //        if (_to != "")
        //            To = Convert.ToDouble(_to);
        //        hasFrom = (_from != "");
        //        isValue = false;
        //        hasTo = (_to != "");


        //    }
        //    public SEARCH_NUMBER(string val)
        //    {
        //        if (val != "")
        //            From = Convert.ToDouble(val);
        //        hasFrom = (val!= "");
        //        hasTo= false;
        //        isValue = true;
        //    }
        //    public string asSQL(string fieldName)
        //    {
        //        string sql = "";
        //        if (hasFrom || hasTo)
        //        {
        //            sql = "(";
        //            if (hasFrom)
        //            {
        //                sql += fieldName;
        //                if (isValue)
        //                    sql += " = ";
        //                else
        //                    sql += " >= ";
        //                sql += From.ToString();
        //            }
        //            if (hasTo)
        //            {
        //                if (hasFrom)
        //                    sql += " AND ";
        //                sql += fieldName + " <= " + To.ToString();
        //            }
        //            sql += ")";
        //        }

        //        return sql;

        //    }
        //    public string asText(string Field)
        //    {
        //        string txt = "";
        //        if (hasFrom || hasTo)
        //        {
        //            txt = Field;
        //            if (hasFrom)
        //            {
        //                if (isValue)
        //                {
        //                    txt += " is ";
        //                }
        //                else
        //                {
        //                    txt += " from ";
        //                }
        //                if (isCurrency)
        //                    txt += CS.General_v3.Settings.CultureInfo.CurrencySymbol;
        //                txt += From.ToString("0,#.##");
        //                if (isPercentage)
        //                    txt += "%";
        //            }
        //            if (hasTo)
        //            {
        //                if (hasFrom)
        //                    txt += " to ";
        //                else
        //                    txt += " max of ";
        //                if (isCurrency)
        //                    txt += CS.General_v3.Settings.CultureInfo.CurrencySymbol;

        //                txt += To.ToString("0,#.##");
        //                if (isPercentage)
        //                    txt += "%";
        //            }
        //            txt += "<br/>";
        //        }
        //        return txt;

        //    }

        //}
        //[Serializable]
        //public class SEARCH_MULTIPLE : ArrayList
        //{
            
        //    public string asSQL(string fieldName)
        //    {
        //        string SQL = "(";
        //        for (int i = 0; i < this.Count; i++)
        //        {
        //            object item = this[i];
        //            if (item is SEARCH_NUMBER)
        //            {
        //                SQL += "[" + ((SEARCH_NUMBER)item).asSQL(fieldName) + "]";
        //            }
        //            else if (item is SEARCH_DATE)
        //            {
        //                SQL += "[" + ((SEARCH_DATE)item).asSQL(fieldName) + "]";
        //            }
        //            else if (item is SEARCH_BOOLEAN)
        //            {
        //                SQL += "[" + ((SEARCH_BOOLEAN)item).asSQL(fieldName) + "]";
        //            }
        //            else if (item is string)
        //            {
        //                SQL += "[" + fieldName + " LIKE '%" + (string)item + "%']";
        //            }
        //            else
        //            {
        //                SQL += "[" + fieldName + " = " + item.ToString() + "]";
        //            }

        //        }
        //        SQL += ")";
        //        if (SQL == "()")
        //            SQL = "";
        //        SQL = SQL.Replace("][", ") or (").Replace("]",")").Replace("[","(");
        //        return SQL;

        //    }
            

        //}
        /// <summary>
        /// Returns whether a string is either null or empty
        /// </summary>
        /// <param name="data">The string to check</param>
        /// <returns>True if null/empty, else false</returns>
        public static bool isNullorEmpty(string data)
        {
            if (data == null)
            {
                return true;
            }
            else
            {
                if (data == "")
                    return true;
                else
                    return false;

            }
        }

        /// <summary>
        /// Adds a list item of text/value to the combo box in order of text
        /// </summary>
        /// <param name="cmb">The combo box</param>
        /// <param name="text">The text of the item.  Value is the same</param>
        public static void addToCmbInOrder(CS.General_v3.Controls.WebControls.Common.MyDropDownList cmb,
                                           string text)
        {
            addToCmbInOrder(cmb, text, text, 0);
        }

        /// <summary>
        /// Adds a list item of text/value to the combo box in order of text
        /// </summary>
        /// <param name="cmb">The combo box</param>
        /// <param name="text">The text of the item.</param>
        /// <param name="value">The value of the item</param>
        public static void addToCmbInOrder(CS.General_v3.Controls.WebControls.Common.MyDropDownList cmb,
                                           string text, string value, int noItemsFromEnd)
        {
            for (int i = 0; i < cmb.Items.Count - noItemsFromEnd; i++)
            {

                if (string.CompareOrdinal(cmb.Items[i].Text, text) > 0)
                {
                    cmb.Items.Insert(i, new ListItem(text, value));
                    return;
                }
            }
            if (noItemsFromEnd == 0)
                cmb.Items.Add(new ListItem(text, value));
            else
            {
                cmb.Items.Insert(cmb.Items.Count - noItemsFromEnd, new ListItem(text, value));
            }
        }

        

        public static Control FindControl(Control ctrl, string id)
        {
            Queue q = new Queue();
            q.Enqueue(ctrl);

            while (q.Count > 0)
            {
                Control curr = (Control)q.Dequeue();
                if (curr != null)
                {
                    if (curr.ID == id || curr.ClientID == id)
                    {
                        return curr;
                    }
                    for (int i = 0; i < curr.Controls.Count; i++)
                    {
                        q.Enqueue(curr.Controls[i]);
                    }
                }
            }
            return null;
        }


        public static XmlNamespaceManager CreateNameSpaceManager(XmlDocument doc)
        {
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
            foreach (XmlAttribute attr in doc.SelectSingleNode("/*").Attributes)
                if (attr.Prefix == "xmlns") nsmgr.AddNamespace(attr.LocalName, attr.Value);
            return nsmgr;
        }

        /// <summary>
        /// Retrieves the first not null variable from a list. Example (null, "karl","mark") returns "karl"
        /// </summary>
        /// <param name="vars">List</param>
        /// <returns>First non-null variable</returns>
        public static object GetFirstNotNull(params object[] vars)
        {
            for (int i = 0; i < vars.Length; i++)
            {
                if (vars[i] != null)
                    return vars[i];
            }
            return null;
        }

        /// <summary>
        /// Copies all the public properties from object to an object
        /// </summary>
        /// <param name="from">From object</param>
        /// <param name="to">To object</param>
        public static void CopyPropertiesAndFields(object from, object to)
        {
            Type fromType, toType;
            fromType = from.GetType();
            toType = to.GetType();
            System.Reflection.PropertyInfo[] fromProps, toProps;
            fromProps = fromType.GetProperties();
            toProps = toType.GetProperties();
            for (int i = 0; i < fromProps.Length; i++)
            {
                System.Reflection.PropertyInfo fromProp = fromProps[i];
                try
                {
                    System.Reflection.PropertyInfo toProp = toType.GetProperty(fromProp.Name);
                    if (fromProp.CanWrite && toProp != null && toProp.PropertyType == fromProp.PropertyType)
                    {
                        object fromVal = fromProp.GetValue(from, null);
                        toProp.SetValue(to, fromVal, null);
                    }
                }
                catch
                {

                }


            }

            System.Reflection.FieldInfo[] fromFields, toFields;
            fromFields = fromType.GetFields();
            toFields = toType.GetFields();
            for (int i = 0; i < fromFields.Length; i++)
            {
                System.Reflection.FieldInfo fromField = fromFields[i];
                try
                {
                    System.Reflection.FieldInfo toField = toType.GetField(fromField.Name);
                    if (toField != null && toField.FieldType == fromField.FieldType && !toField.IsStatic)
                    {
                        object fromVal = fromField.GetValue(from);
                        toField.SetValue(to, fromVal);

                    }
                }
                catch
                {

                }
                
            }
        }
        public enum VIDEO_ASPECT_RATIO
        {
            Normal_4isto3 = 1,
            Widescreen_16isto9 = 2,
            Original = 3
        }
        public static string ConvertBasicDataTypeToString(object o, bool ifEnumConvertToIntValue = true, bool useDescriptionAttributeIfEnum = false)
        {
            string s = null;
            if (o != null)
            {
                var v = o;
                if (v is double || v is float || v is decimal)
                {
                    s = ((double)v).ToString("0.0000");
                }
                else if (v is int || v is long || v is byte || v is short)
                {
                    s = v.ToString();
                }
                else if (v is DateTime)
                {
                    s = ((DateTime)v).ToString(DATE_STRING_FORMAT);
                }
                else if (v is bool)
                {
                    s = CS.General_v3.Util.Other.BoolToStr((bool)v);
                    
                }
                else if (v is IEnumerable && !(v is string))
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (var item in ((IEnumerable)v))
                    {
                        if (sb.Length > 0)
                            sb.Append(",");
                        sb.Append(ConvertBasicDataTypeToString(item));
                    }
                    s = sb.ToString();
                }
                else if (v is Enum)
                {
                    if (ifEnumConvertToIntValue)
                        s = ((int)v).ToString();
                    else
                        s = CS.General_v3.Util.EnumUtils.StringValueOf((Enum)v, getFromDescriptionAttributeIfAvailable: useDescriptionAttributeIfEnum);
                }
                else
                {
                    s = v.ToString();
                }

            }
            return s;
        }

        public static TDataType ConvertObjectToBasicDataType<TDataType>(object o)
            
            
        {
            

            object result = null;
            Type t = typeof(TDataType);

            if (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>) && t.GetGenericArguments()[0].IsEnum)
            {
                t = t.GetGenericArguments()[0];
            }
            if (typeof(TDataType) == typeof(int) && !(o is int))
            {
                result = Conversion.ToInt32(o);

            }
            else if (t.IsEnum)
            {
                
                string s =null;
                if (o != null) s = o.ToString();
                
                result = CS.General_v3.Util.EnumUtils.GetEnumByStringValue(t,s, null);
            }
            else
            {
                result = o;
            }

            

            if (result == null)
            {
                result = default(TDataType);
            }
            return (TDataType)result;

        }

        public static T ConvertStringToBasicDataType<T>(string s)
        {
            return (T) ConvertStringToBasicDataType(s, typeof (T));

        }

        public static object ConvertStringToBasicDataType(string s, Type tType)
        {
            
            object returnValue = null;

            bool isNullable = false;
            Type underlyingType = tType;
            if (tType.IsGenericType && tType.GetGenericTypeDefinition() == typeof(System.Nullable<>))
            {
                underlyingType = tType.GetGenericArguments()[0];
                isNullable = true;
            }

            if (tType == typeof(bool?))
            {
                returnValue = CS.General_v3.Util.Other.TextToBoolNullable(s);
            }
            else if (underlyingType == typeof(bool))
            {
                returnValue = CS.General_v3.Util.Other.TextToBool(s);
            }
            else if (underlyingType == typeof(int))
            {

                int val = 0;
                if (int.TryParse(s, out val))
                {
                    returnValue = val;
                }
                else if (tType == typeof(int))
                {
                    returnValue = ReflectionUtil.GetDefaultValueOfType(tType);
                    //throw new InvalidCastException("'" + s + "' cannot be casted into int");
                }

            }
            else if (underlyingType == typeof(uint))
            {

                uint val = 0;
                if (uint.TryParse(s, out val))
                {
                    returnValue = val;
                }
                else if (tType == typeof(uint))
                {
                    returnValue = ReflectionUtil.GetDefaultValueOfType(tType);
                    //throw new InvalidCastException("'" + s + "' cannot be casted into int");
                }

            }
            

            else if (underlyingType == typeof(double))
            {
                double val = 0;
                if (double.TryParse(s, out val))
                {
                    returnValue = val;
                }
                else if (tType == typeof(double))
                {
                    returnValue = returnValue = ReflectionUtil.GetDefaultValueOfType(tType);
                    //throw new InvalidCastException("'" + s + "' cannot be casted into double");
                }

            }
            else if (underlyingType == typeof(long))
            {
                long val = 0;
                long? nVal = null;
                if (long.TryParse(s, out val))
                {
                    nVal = val;

                    if (isNullable)
                    {
                        returnValue = nVal;
                    }
                    else
                    {
                        returnValue = nVal.GetValueOrDefault();
                    }

                }
                else if (tType == typeof(long))
                {
                    returnValue = ReflectionUtil.GetDefaultValueOfType(tType);
                    //instead of throw error, set default value

                    //throw new InvalidCastException("'" + s + "' cannot be casted into long");
                }
                
                
            }
            else if (underlyingType.IsEnum)
            {
                long val = 0;
                
                object enumVal =null;
                try
                {
                    enumVal = Enum.Parse(underlyingType, s, true);
                }
                catch (Exception)
                {
                    enumVal = null;

                    if (!isNullable)
                    {
                        enumVal = ReflectionUtil.GetDefaultValueOfType(underlyingType);
                        //throw;
                    }
                }
                if (enumVal == null)
                {
                    if (long.TryParse(s, out val))
                    {
                        enumVal = Enum.Parse(underlyingType, val.ToString());

                    }
                    else if (tType == typeof(long))
                    {
                        enumVal = ReflectionUtil.GetDefaultValueOfType(underlyingType);
                        //instead of throw error, set default value

                        //throw new InvalidCastException("'" + s + "' cannot be casted into Enumeration <" + underlyingType.Name + ">");
                    }
                }
                returnValue = enumVal;


            }
            else if (underlyingType == typeof(string))
            {
                returnValue = s;
            }
            else if (underlyingType == typeof(DateTime))
            {
                DateTime? d = CS.General_v3.Util.Date.ParseDate(s, DATE_STRING_FORMAT);
                returnValue = d;
                if (tType == typeof(DateTime))
                {
                    if (returnValue == null)
                    {
                        returnValue = ReflectionUtil.GetDefaultValueOfType(underlyingType);
                        //instead of throw error, set default value

                        //throw new InvalidCastException("'"+s + "' cannot be casted into DateTime");
                    }
                    else
                    {
                        returnValue = d.Value;
                    }
                }
            }
            else
            {
                throw new InvalidOperationException("data type not yet implemented!");
            }
            return returnValue;



        }

        /// <summary>
        /// Converts a video to FLV
        /// </summary>
        /// <param name="FFMpegPath">The path of ffmpeg.exe</param>
        /// <param name="Input">input path</param>
        /// <param name="Output">output path</param>
        /// <param name="Overwrite">Overwrite if another file exists</param>
        /// <param name="Title">Title of video</param>
        /// <param name="Author">Author of video</param>
        /// <param name="CopyrightTxt">Copyright text for video</param>
        /// <param name="BitRate">Video Bitrate. If set to 0, left as original value</param>
        /// <param name="FPS">Frames per Second. If set to 0, left as original value</param>
        /// <param name="Width">Width of Video. If set to 0, left as original value</param>
        /// <param name="Height">Height of Video. If set to 0, left as original value</param>
        /// <param name="aspectRatio">Aspect Ratio. Either 4:3, or 16:9, or left as original value</param>
        public static void convertVideo(string FFMpegPath, string Input, string Output, bool Overwrite, 
            string Title, string Author,
            string CopyrightTxt, string Comment, int VideoFPS, int Width, int Height, 
            VIDEO_ASPECT_RATIO aspectRatio, bool EnableAudio, int AudioBitrate)
        {
            string p = "";
            if (Overwrite)
                p += " -y";
            if (Comment != "")
                p += " -comment \"" + Comment + "\"";
            if (Title != "")
                p += " -title \"" + Title + "\"";
            if (Author != "")
                p += " -author \"" + Author + "\"";
            if (CopyrightTxt != "")
                p += " -copyright \"" + CopyrightTxt + "\"";
            if (VideoFPS > 0)
                p += " -r " + VideoFPS.ToString();
            if (Width > 0 && Height > 0)
            {
                p += " -s " + Width.ToString() + "x" + Height.ToString();
            }
            if (aspectRatio != VIDEO_ASPECT_RATIO.Original)
            {
                p += " -aspect ";
                if (aspectRatio == VIDEO_ASPECT_RATIO.Normal_4isto3)
                    p += "4:3";
                else if (aspectRatio == VIDEO_ASPECT_RATIO.Widescreen_16isto9)
                    p += "16:9";

            }
            if (!EnableAudio)
            {
                p += " -an";
            }
            else
            {
                p += " -ab " + AudioBitrate.ToString();
            }
            p += " -i \"" + Input + "\" \"" + Output + "\"";
            System.Diagnostics.Process process = executeProgram(FFMpegPath, p);
            process.WaitForExit();

        }
        public static void CopyStylesToControl(CssStyleCollection collection, CssStyleCollection collTo)
        {
            string[] keys = new string[collection.Count];
            collection.Keys.CopyTo(keys, 0);
            for (int i = 0; i < keys.Length; i++)
            {
                collTo[keys[i]] += collection[keys[i]];
            }
        }
        public static void CopyStylesToControl(CssStyleCollection collection, WebControl ctrlTo)
        {
            CopyStylesToControl(collection, ctrlTo.Style);
        }
        public static void CopyStylesToControl(WebControl ctrlFrom, WebControl ctrlTo)
        {
            CopyStylesToControl(ctrlFrom.Style, ctrlTo.Style);
        }

        public static void CopyAttribsToControl(AttributeCollection Attributes, WebControl ctrlTo)
        {
            string[] keys = new string[Attributes.Count];

            Attributes.Keys.CopyTo(keys, 0);
            for (int i = 0; i < keys.Length; i++)
            {
                ctrlTo.Attributes[keys[i]] += Attributes[keys[i]];
            }
        }

        internal static void CopyStylesToControl(CssStyleCollection TableStyle, HtmlTable tb)
        {
            throw new NotImplementedException();
        }

        public static ListItemCollection GetListItemCollectionForBoolNullable()
        {
            ListItemCollection list = new ListItemCollection();
            list.Add(new ListItem("", ""));
            list.Add(new ListItem("Yes", "1"));
            list.Add(new ListItem("No", "0"));
            return list;
        }
        public static ListItem[] GetListItemCollectionForBoolNullableAsArray()
        {
            return CS.General_v3.Util.ListUtil.ConvertAll<ListItem, ListItem>(GetListItemCollectionForBoolNullable(), item => item).ToArray();
        }
        private static int _globalCtrlID
        {
            get
            {
                int? value = (int?)(CS.General_v3.Util.PageUtil.GetContextObject("_globalCtrlID"));
                return value.GetValueOrDefault();
            }
            set
            {
                CS.General_v3.Util.PageUtil.SetContextObject("_globalCtrlID", value);
            }
        }
       
        internal static int GetNextControlGlobalIdentifier()
        {
            _globalCtrlID++;
            return _globalCtrlID;
            
        }

        public static void RegenerateUnitTestingEnvironmentSessionGuid()
        {
            UnitTestingEnvironment_SessionGuid = CS.General_v3.Util.Random.GetGUID(removeDashes: true);
        }

        public static void SetIsInUnitTestingEnvironment()
        {
            IsInUnitTestingEnvironment = true;
            Other.IsLocalTestingMachine_CustomOverride = true;
            RegenerateUnitTestingEnvironmentSessionGuid();
        }


        public static string UnitTestingEnvironment_SessionGuid { get; private set; }
        public static bool IsInUnitTestingEnvironment { get; private set; }
    }
}
