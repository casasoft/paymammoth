using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Reflection;
namespace CS.General_v3.Util
{
    public static class XML
    {
        public static void  SanitizeXmlFile(string xmlPathToCheck, string outputPath)
        {
            FileStream fsFrom = new FileStream(xmlPathToCheck, FileMode.Open, FileAccess.Read);
            FileStream fsTo = new FileStream(outputPath, FileMode.Create, FileAccess.Write);
            
            StreamReader srFrom = new StreamReader(fsFrom);
            StreamWriter swTo = new StreamWriter(fsTo);
            
            while (!srFrom.EndOfStream)
            {
                char c = (char)srFrom.Read();
                if (c == 12 || c == 0x0C)
                {
                    int k = 5;
                }
                if (IsLegalXmlChar(c))
                {
                    swTo.Write(c);
                }
                else
                {
                    int k = 5;
                }

            }
            srFrom.Dispose();
            swTo.Dispose();
            fsFrom.Dispose();
            fsTo.Dispose();

        }
        public static string SanitizeXmlFileToString(string path)
        {
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
            StreamReader sr = new StreamReader(fs);
            StringBuilder buffer = new StringBuilder((int)fs.Length);
            while (!sr.EndOfStream)
            {
                char c = (char) sr.Read();
                if (IsLegalXmlChar(c))
                {
                    buffer.Append(c);
                }
                else
                {
                    int k = 5;
                }
            }
            return buffer.ToString();
        }

    
        public static string SanitizeXmlString(string xml)
        {
            if (xml == null)
                throw new ArgumentNullException("xml");
            StringBuilder buffer = new StringBuilder(xml.Length);
            foreach (char c in xml)
            {
                if (IsLegalXmlChar(c))
                {
                    buffer.Append(c);
                }
                else
                {
                    int k = 5;
                }
            }
            return buffer.ToString();
        }
        public static  bool IsLegalXmlChar(int character)
        {
            var ch = character;
            if ((ch < 0x00FD && ch > 0x001F) || ch == '\t' || ch == '\n' || ch == '\r')
            {
                return true;
            }
            else
            {
                return false;
            }

            return (

                 character == 0x9 /* == '\t' == 9   */          ||

         character == 0xA /* == '\n' == 10  */          ||

         character == 0xD /* == '\r' == 13  */          ||

        (character >= 0x20    && character <= 0xD7FF  ) ||

        (character >= 0xE000  && character <= 0xFFFD  ) ||

        (character >= 0x10000 && character <= 0x10FFFF)

                );


        }
        /// <summary>
        /// Adds a node to an Xml document based on an item property
        /// </summary>
        /// <typeparam name="TItem">Type of item</typeparam>
        /// <typeparam name="TData">Type of data to return</typeparam>
        /// <param name="doc">Xml document</param>
        /// <param name="nodeToAddTo">The node to add to</param>
        /// <param name="item">The item to load the data from</param>
        /// <param name="selector">The property</param>
        /// <param name="dataToStringConverter">Optional. Converts the data to a string format</param>
        public static void AddNodeToXml<TItem, TData>(XmlDocument doc, XmlNode nodeToAddTo, TItem item, System.Linq.Expressions.Expression<Func<TItem, TData>> selector,
            Func<TData,string> dataToStringConverter = null)
        {

            PropertyInfo pInfo = CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(selector);
            string title = pInfo.Name;
            TData oValue = (TData) pInfo.GetValue(item, null);
            string sValue = null;
            if (dataToStringConverter != null)
                sValue = dataToStringConverter(oValue);
            else
                sValue = CS.General_v3.Util.Other.ConvertBasicDataTypeToString(oValue);
            AddNodeToXml(doc, nodeToAddTo, title, sValue);
        }
        /// <summary>
        /// Sets an item property value from an xml node
        /// </summary>
        /// <typeparam name="TItem">Type of Item</typeparam>
        /// <typeparam name="TData">Type of data item</typeparam>
        /// <param name="parent">parent to load data from</param>
        /// <param name="item">Item to set the data</param>
        /// <param name="selector">Property selector</param>
        /// <param name="stringToDataConverter">Optional.  Converter from string to data.  Required if data type is not a basic data type</param>
        private static string getValueFromXml<TItem>(this XmlNode parent, System.Linq.Expressions.Expression selector)
        {




            PropertyInfo pInfo = CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(selector);
            string title = pInfo.Name;
            XmlNode node = parent.SelectSingleNode(title);

            string nodeValue = GetNodeValue(node);
            return nodeValue;

        }
        /// <summary>
        /// Sets an item property value from an xml node
        /// </summary>
        /// <typeparam name="TItem">Type of Item</typeparam>
        /// <typeparam name="TData">Type of data item</typeparam>
        /// <param name="parent">parent to load data from</param>
        /// <param name="item">Item to set the data</param>
        /// <param name="selector">Property selector</param>
        /// <param name="stringToDataConverter">Optional.  Converter from string to data.  Required if data type is not a basic data type</param>
        public static TData GetValueFromXml<TItem, TData>(this XmlNode parent, System.Linq.Expressions.Expression<Func<TItem, TData>> selector, Func<string, TData> stringToDataConverter = null)
        {
            string nodeValue = getValueFromXml<TItem>(parent, selector);

            TData value = default(TData);
            if (stringToDataConverter != null)
                value = stringToDataConverter(nodeValue);
            else
                value = CS.General_v3.Util.Other.ConvertStringToBasicDataType<TData>(nodeValue);


            return value;

        }

        /// <summary>
        /// Sets an item property value from an xml node
        /// </summary>
        /// <typeparam name="TItem">Type of Item</typeparam>
        /// <typeparam name="TData">Type of data item</typeparam>
        /// <param name="parent">parent to load data from</param>
        /// <param name="item">Item to set the data</param>
        /// <param name="selector">Property selector</param>
        /// <param name="stringToDataConverter">Optional.  Converter from string to data.  Required if data type is not a basic data type</param>
        public static void SetValueFromXml<TItem, TData>(this XmlNode parent, TItem item, System.Linq.Expressions.Expression<Func<TItem, TData>> selector,
            Func<string, TData> stringToDataConverter = null)
        {



            
            PropertyInfo pInfo = CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(selector);
            string name = pInfo.Name;
            if (parent.SelectSingleNode(name) != null)
            {
                TData value = GetValueFromXml<TItem, TData>(parent, selector, stringToDataConverter);


                pInfo.SetValue(item, value, null);
            }

        }

        public static void AddNodeToXml(XmlDocument doc, XmlNode node, string title, object value)
        {

            string sValue = "";
            if (value != null)
            {
                if (value is bool)
                {
                    sValue = "false";
                    if ((bool)value)
                        sValue = "true";
                }
                else if (value is double)
                {
                    sValue = ((double)value).ToString("0.0000");
                }
                else if (value is DateTime)
                {
                    sValue = ((DateTime)value).ToString("yyyy-MM-dd HH:mm:ss");
                }
                else
                {
                    sValue = value.ToString();
                }
            }
            XmlNode n = doc.CreateElement(title);
            node.AppendChild(n);
            n.InnerText = sValue;
        }
        /// <summary>
        /// Creates an xml node from file.
        /// </summary>
        /// <param name="path">The file</param>
        /// <param name="xml">The Xml Document owner. Can be NULL</param>
        /// <returns>The XmlNode</returns>
        public static XmlNode CreateXmlNodeFromFile(string path, XmlDocument xmlDoc)
        {
            XmlDocument Xml = new XmlDocument();
            Xml.Load(path);
            XmlNode node = Xml.FirstChild;
            if (xmlDoc != null)
                node = xmlDoc.ImportNode(node, true);
            return node;
        }

        public static XmlNamespaceManager CreateNameSpaceManager(XmlDocument doc)
        {
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
            var rootNode = doc.SelectSingleNode("/*");
            if (rootNode != null)
            {
                foreach (XmlAttribute attr in rootNode.Attributes)
                {
                    if (attr.Prefix == "xmlns") nsmgr.AddNamespace(attr.LocalName, attr.Value);
                }
            }
            return nsmgr;
        }

        public static string GetAttribute(XmlNode node, string attribute)
        {
            if (node.Attributes[attribute] != null)
            {
                return node.Attributes[attribute].Value;
            }
            return null;
        }
        public static string GetNodeValue(XmlNode node)
        {
            if (node != null && node.FirstChild != null)
            {
                return node.FirstChild.Value;
            }
            return null;
        }
        public static bool? GetNodeValueBoolNullable(XmlNode node)
        {
            string s = GetNodeValue(node);
            bool? b = CS.General_v3.Util.Other.TextToBoolNullable(s);
            return b;
        }
        public static bool GetNodeValueBool(XmlNode node, bool defaultValue = false)
        {
            bool? b = GetNodeValueBoolNullable(node);
            return b.GetValueOrDefault(defaultValue);
        }
        public static double GetNodeValueDouble(XmlNode node, double defaultValue = 0)
        {
            double? dbl = GetNodeValueDoubleNullable(node, defaultValue);
            if (dbl.HasValue)
            {
                return dbl.Value;
            }
            else
            {
                return defaultValue;
            }

        }
        public static double? GetNodeValueDoubleNullable(XmlNode node)
        {
            return GetNodeValueDoubleNullable(node, null);
        }

        public static double? GetNodeValueDoubleNullable(XmlNode node, double? defaultValue = null)
        {
            string s = GetNodeValue(node);
            if (!string.IsNullOrEmpty(s))
            {
                return Conversion.ToDouble(s);
            }
            else
            {
                return defaultValue;
            }
        }

        public static int GetNodeValueInt(XmlNode node, int defaultValue = 0)
        {
            int? intValue = GetNodeValueIntNullable(node, defaultValue);
            if (intValue.HasValue) return intValue.Value;
            return defaultValue;
        }
        public static int? GetNodeValueIntNullable(XmlNode node)
        {
            return GetNodeValueIntNullable(node, null);
        }
        public static int? GetNodeValueIntNullable(XmlNode node, int? defaultValue = null)
        {
            string s = GetNodeValue(node);
            if (!string.IsNullOrEmpty(s))
            {
                return Conversion.ToInt32(s);
            }
            else
            {
                return defaultValue;
            }
        }

        public static XmlElement GetXmlElementWithText(XmlDocument xmlDoc, string name, string text)
        {
            XmlElement xmlElem = xmlDoc.CreateElement(name);
            xmlElem.AppendChild(xmlDoc.CreateTextNode(text));

            return xmlElem;
        }
        public static string SerializeObjectToXmlString(object o)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(o.GetType());
            MemoryStream ms = new MemoryStream();

            
            xmlSerializer.Serialize(ms, o);
            ms.Seek(0, SeekOrigin.Begin);
            string xml = CS.General_v3.Util.IO.GetStringFromStream(ms);
            ms.Dispose();
            return xml;
            
        }
        public static T DeSerializeObjectFromXmlString<T>(string xml)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
            
            //StringBuilder sb = new StringBuilder();
            //StringWriter stringWriter = new StringWriter(sb);
            //stringWriter.Write(xml);
            StringReader sr = new StringReader(xml);
            object obj = xmlSerializer.Deserialize(sr);
//            ms.Dispose();
            return (T)obj;

        }
        public static void SerializeObjectToXMLFile(object o, string path)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(o.GetType());
            FileStream fs = new FileStream(path,  FileMode.Create, FileAccess.Write);
            xmlSerializer.Serialize(fs, o);
            fs.Close();
            fs.Dispose();
        }
        public static T DeserializeObjectFromXMLFile<T>(string path)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
            var retType = (T)xmlSerializer.Deserialize(fs);
            fs.Close();
            fs.Dispose();
            return retType;
            
        }


        public static string GetNodeInnerText(XmlNode node)
        {
            if (node != null)
            {
                return node.InnerText;
            }
            return null;
        }
    }
}
