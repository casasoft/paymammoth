using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Web.UI.WebControls;

namespace CS.General_v3.Util
{
    public static class Date
    {
        public enum DATETIME_FORMAT
        {
            /// <summary>
            /// ex. 28/06/2012
            /// </summary>
            ShortDate,
            /// <summary>
            /// ex. 28 June 2012
            /// </summary>
            LongDate,
            /// <summary>
            /// ex. 28 June 2012 12:51
            /// </summary>
            FullDateShortTime,
            /// <summary>
            /// ex. 28 June 2012 12:51:03
            /// </summary>
            FullDateLongTime,
            /// <summary>
            /// ex. 28/06/2012 12:51
            /// </summary>
            GeneralDateShortTime,
            /// <summary>
            /// ex. 28/06/2012 12:51:14
            /// </summary>
            GeneralDateLongTime,
            /// <summary>
            /// ex. 28 June
            /// </summary>
            MonthDayPattern,
            /// <summary>
            /// ex. 2012-06-28T12:51:42.3808593+02:00
            /// </summary>
            RoundTripDateTimePattern,
            /// <summary>
            /// ex. Thu, 28 Jun 2012 12:51:36 GMT
            /// </summary>
            RFC1123,
            /// <summary>
            /// ex. 2012-06-28T12:52:04
            /// </summary>
            SortableDate,
            /// <summary>
            /// ex. 28/06/2012 12:51
            /// </summary>
            ShortDateShortTime,
            /// <summary>
            /// ex. 28/06/2012 12:51:51
            /// </summary>
            ShortDateLongTime,
            /// <summary>
            /// ex. 28/06/2012
            /// </summary>
            ShortTime,
            /// <summary>
            /// ex. 12:51:27
            /// </summary>
            LongTime,
            /// <summary>
            /// ex. 2012-06-28 12:52:13Z
            /// </summary>
            UniversalSortableDateTime,
            /// <summary>
            /// ex. 28 June 2012 10:52:08
            /// </summary>
            UniversalFullDateTime,
            /// <summary>
            /// ex. June 2012
            /// </summary>
            YearMonthPattern,
            /// <summary>
            /// ex. 28 Jun 2012
            /// </summary>
            DayShortMonthYear,
            /// <summary>
            /// ex. 28 Jun 2012 12:50
            /// </summary>
            DayShortMonthYearShortTime,
            /// <summary>
            /// ex. 28 Jun
            /// </summary>
            DayShortMonth,
            /// <summary>
            /// ex. Wednesday 28th June 2012
            /// </summary>
            FullLongDate
        }
        /// <summary>
        /// Converts a regular DateTime to a RFC822 date string.
        /// </summary>
        /// <returns>The specified date formatted as a RFC822 date string.</returns>
        public static string GetDateAsRFC822(DateTime date)
        {
            int offset = TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now).Hours;
            string timeZone = "+" + offset.ToString().PadLeft(2, '0');

            if (offset < 0)
            {
                int i = offset * -1;
                timeZone = "-" + i.ToString().PadLeft(2, '0');
            }

            return date.ToString("ddd, dd MMM yyyy HH:mm:ss " + timeZone.PadRight(5, '0'));
        }
        public static string ConvertDoubleTimeToString(double num, bool includeSeconds = false)
        {
            double hours = Math.Floor(num);
            double minutes = (num - hours) * 60.0;
            double seconds = (minutes - Math.Floor(minutes)) * 60.0;
            int h = (int)Math.Floor(hours);
            int m = (int)Math.Floor(minutes);

            string mStr = m.ToString();
            if (m == 0)
            {
                mStr += "0";
            }
            if (includeSeconds)
            {
                int S = (int)Math.Floor(seconds);

                return h.ToString() + ":" + mStr + ":" + S.ToString();
            }
            else
            {
                return h.ToString() + ":" + mStr;
            }
        }

        public static DateTime? TryParseNullable(string date)
        {
            DateTime? result = null;
            if(!string.IsNullOrWhiteSpace(date))
            {
                DateTime tmp;
                bool parseResult = false;
                parseResult = DateTime.TryParse(date, out tmp);
                if(parseResult)
                {
                    result = tmp;
                }
            }
            return result;
        }


        public static bool DateIsAfterNowBy(DateTime dateToCmp, int hours, int minutes, int seconds)
        {
            DateTime nowToCompare = CS.General_v3.Util.Date.Now.AddHours(hours).AddMinutes(minutes).AddSeconds(seconds);
            return (dateToCmp > nowToCompare);
        }
        public static bool DateIsBeforeNowBy(DateTime dateToCmp, int hours, int minutes, int seconds)
        {
            DateTime nowToCompare = CS.General_v3.Util.Date.Now.AddHours(-hours).AddMinutes(-minutes).AddSeconds(-seconds);
            return (dateToCmp< nowToCompare);
        }
        /// <summary>
        /// This method creates a list of hours in relation to the time interval provided.
        /// </summary>
        /// <param name="is24Hr">Boolean whether the list will display time as (22:00 or 10pm)</param>
        /// <param name="timeInterval">The time interval (9.30 - 10.30 - 11.00)</param>
        /// <returns></returns>
        public static IEnumerable<ListItem> CreateListOfHours(bool is24Hr = false, int timeIntervalInMinutes = 60)
        {
            List<ListItem> coll = new List<ListItem>();
            List<string> hoursResult = new List<string>();
            int hours = 0;
            string resultTime = "";

            if (timeIntervalInMinutes == 0)
            {
                timeIntervalInMinutes = 60;
            }

            if (is24Hr)
            {
                hours = 24*60;
                coll.AddRange(listOfHours(hours, timeIntervalInMinutes));
            }

            else
            {
                hours = 12 * 60;
                for (int i = 0; i < 2; i++)
                {
                    coll.AddRange(listOfHours(hours, timeIntervalInMinutes, i));
                }
            }
            return coll;
        }

        private static List<ListItem> listOfHours(int hours, int timeIntervalInMinutes, int increment = 0)
        {
            List<ListItem> resultsListItem = new List<ListItem>();
            string hourSuffix = "";
            string noon = "";
            for (int i = 0; i < hours; i++)
            {
                StringBuilder result = new StringBuilder();
                string time = "";
                if (i % timeIntervalInMinutes == 0)
                {
                    ListItem li = new ListItem();
                    TimeSpan t = TimeSpan.FromMinutes(i);
                    
                    if (increment != 0)
                    {
                        hourSuffix = " pm";
                        if (t.Hours == 0)
                        {
                            noon = (string.Format("{0:D2}:{1:D2}", 12, t.Minutes));
                        }
                    }
                    else if(increment == 0 && hours == 720)
                    {
                        hourSuffix = " am";
                    }

                    if ((!String.IsNullOrEmpty(noon)) && t.Hours == 0 && increment > 0)
                    {
                        time = noon;
                        result.Append(noon);
                    }
                    else
                    {
                        time = (string.Format("{0:D2}:{1:D2}", t.Hours, t.Minutes));
                        result.Append(time);
                    }

                    if (!String.IsNullOrEmpty(hourSuffix))
                    {
                        result.Append(hourSuffix);
                    }
                    li.Text = result.ToString();
                    li.Value = time;
                    resultsListItem.Add(li);
                }
            }
            return resultsListItem;
        }

        public static bool IsBeforeNow(this DateTime date)
        {
            DateTime nowDate = Now;
            return date <= nowDate;
        }
        public static bool IsBefore(this DateTime date, DateTime dateToCompare)
        {
            return date < dateToCompare;
        }
        public static bool IsBeforeOrEqual(this DateTime date, DateTime dateToCompare)
        {
            return date <= dateToCompare;
        }
        public static bool IsAfterOrEqual(this DateTime date, DateTime dateToCompare)
        {
            return date >= dateToCompare;
        }
        public static bool IsAfter(this DateTime date, DateTime dateToCompare)
        {
            return date > dateToCompare;
        }
        public static bool IsBetweenDates(this DateTime date, DateTime dateStart, DateTime dateEnd)
        {
            return (date >= dateStart && date <= dateEnd);
        }
        public static bool IsNotBetweenDates(this DateTime date, DateTime dateStart, DateTime dateEnd)
        {
            return (date <= dateStart || date >= dateEnd);
        }
        /// <summary>
        /// Checks whether a date range, e.g 01/01/2011 - 30/01/2011, is ALL or partially inside another date range. E.g this would return true for
        /// DateStart/End: 01/05/2011 - 31/05/2011.  Range to compare: 01/04/2011 - 15/05/2011, as it overlaps in the 01/05 - 15/05 period.  s
        /// </summary>
        /// <param name="dateStart">Start Date </param>
        /// <param name="dateEnd">End Date</param>
        /// <param name="dateRangeToCompareStart">Start date of range to check</param>
        /// <param name="dateRangeToCompareEnd">End date of range to check</param>
        /// <returns></returns>
        public static bool IsDateRangeInsideDateRange(DateTime dateStart, DateTime dateEnd, DateTime dateRangeToCompareStart, DateTime dateRangeToCompareEnd)
        {
            if (dateStart <= dateRangeToCompareEnd &&
                dateEnd >= dateRangeToCompareStart)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Checks whether a date range, e.g 01/01/2011 - 30/01/2011, is ALL inside another date range.
        /// </summary>
        /// <param name="dateStart">Start Date </param>
        /// <param name="dateEnd">End Date</param>
        /// <param name="dateRangeToCompareStart">Start date of range to check</param>
        /// <param name="dateRangeToCompareEnd">End date of range to check</param>
        /// <returns></returns>
        public static bool IsDateRangeEntirelyInsideDateRange(DateTime dateStart, DateTime dateEnd, DateTime dateRangeToCompareStart, DateTime dateRangeToCompareEnd)
        {
            if (dateEnd <= dateRangeToCompareEnd &&
                dateStart >= dateRangeToCompareStart)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Checks whether a date range, e.g 01/01/2011 - 30/01/2011, is ALL inside another date range.
        /// </summary>
        /// <param name="dateStart">Start Date </param>
        /// <param name="dateEnd">End Date</param>
        /// <param name="dateRangeToCompareStart">Start date of range to check</param>
        /// <param name="dateRangeToCompareEnd">End date of range to check</param>
        /// <returns></returns>
        public static bool IsDateRangeOutsideDateRange(DateTime dateStart, DateTime dateEnd, DateTime dateRangeToCompareStart, DateTime dateRangeToCompareEnd)
        {
            if (dateStart > dateRangeToCompareEnd ||
                dateEnd < dateRangeToCompareStart)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ValidateAge(DateTime birthday, int age)
        {
            return birthday.AddYears(age) <= DateTime.Now;
        }
        public static double GetYearsFrom(DateTime dateFrom)
        {
            return GetYearsFrom(dateFrom, DateTime.Now);
        }

        public static double GetYearsFrom(DateTime dateFrom, DateTime dateTo)
        {
            TimeSpan tsFrom = new TimeSpan(dateFrom.Ticks);
            TimeSpan tsTo = new TimeSpan(dateTo.Ticks);
            TimeSpan result = tsTo - tsFrom;
            return result.Days / 365d;
            /*
            int tot = dateTo.Year - dateFrom.Year - 1;
            if (dateTo.Month > dateFrom.Month ||
                (dateTo.Month == dateFrom.Month && dateTo.Day >= dateFrom.Day))
            {
                tot++;
            }
            return tot;*/
        }
        public static int CalculateAgeBasedOnDate(DateTime dateOfBirth, DateTime dateToCompareWith)
        {
            int age = dateToCompareWith.Year - dateOfBirth.Year - 1;
            if ((dateToCompareWith.Month == dateOfBirth.Month && dateToCompareWith.Day >= dateOfBirth.Day) ||
                (dateToCompareWith.Month > dateOfBirth.Month))
            {//bday passed
                age++;
            }
            return age;
        }
        public static int CalculateAgeBasedOnToday(DateTime dateOfBirth)
        {
            return CalculateAgeBasedOnDate(dateOfBirth, Now);
        }
        public static string ConvertDateTimeFormatToCode(DATETIME_FORMAT formatValue)
        {
            
            string s = "";
            switch (formatValue)
            {
                case DATETIME_FORMAT.ShortDate: s = "d"; break;
                case DATETIME_FORMAT.ShortDateShortTime: s = "d t"; break;
                case DATETIME_FORMAT.ShortDateLongTime: s = "d T"; break;
                case DATETIME_FORMAT.LongDate: s = "D"; break;
                case DATETIME_FORMAT.FullDateShortTime: s = "f"; break;
                case DATETIME_FORMAT.FullDateLongTime: s = "F"; break;
                case DATETIME_FORMAT.GeneralDateShortTime: s = "g"; break;
                case DATETIME_FORMAT.GeneralDateLongTime: s = "G"; break;
                case DATETIME_FORMAT.MonthDayPattern: s = "M"; break;
                case DATETIME_FORMAT.RoundTripDateTimePattern: s = "O"; break;
                case DATETIME_FORMAT.RFC1123: s = "R"; break;
                case DATETIME_FORMAT.SortableDate: s = "s"; break;
                case DATETIME_FORMAT.ShortTime: s = "t"; break;
                case DATETIME_FORMAT.LongTime: s = "T"; break;
                case DATETIME_FORMAT.UniversalSortableDateTime: s = "u"; break;
                case DATETIME_FORMAT.UniversalFullDateTime: s = "U"; break;
                case DATETIME_FORMAT.YearMonthPattern: s = "Y"; break;
                case DATETIME_FORMAT.DayShortMonthYear: s = "dd MMM yyyy"; break;
                case DATETIME_FORMAT.DayShortMonthYearShortTime: s = "dd MMM yyyy t"; break;
                case DATETIME_FORMAT.DayShortMonth: s = "dd MMM"; break;
                case DATETIME_FORMAT.FullLongDate: s = "dddd MMMM dd yyyy"; break;
                default:
                    throw new InvalidOperationException("Invalid code <" + formatValue + ">");
            }
            return s;
            
        }

        /// <summary>
        /// Converts a date to a different timezone.
        /// </summary>
        /// <param name="date">Date</param>
        /// <param name="localGmt">The local gmt of the given date</param>
        /// <param name="resultGmt">the required gmt</param>
        /// <returns></returns>
        public static DateTime ConvertDateToDifferentTimezone(DateTime date, double localGmt, double resultGmt)
        {
            double gmtOffset = resultGmt - localGmt;

            DateTime resultDate = date.AddHours(gmtOffset);

            return resultDate;


        }
        public static TimeZoneInfo GetTimeZoneFromID(string timezoneID)
        {
            try
            {
                var tz = TimeZoneInfo.FindSystemTimeZoneById(timezoneID);
                return tz;
            }
            catch (Exception ex)
            {
                if (ex is TimeZoneNotFoundException || ex is InvalidTimeZoneException)
                {
                    //Fuck it, not found so return 0
                    return null;
                }
                else
                {
                    //still an error
                    throw;
                }
            }
        }
        private static void __ConvertDateToDifferentTimezoneTest()
        {

            DateTime d = new DateTime(2011, 02, 20, 16, 0, 0);
            DateTime result;
            result = ConvertDateToDifferentTimezone(d, 0, 1); //from UK to malta time - should be 17:00
            result = ConvertDateToDifferentTimezone(d, 1, 0); //from Malta to UK time - should be 15:00
            result = ConvertDateToDifferentTimezone(d, 1.5, 0); //from Malta to UK time minus 30 mins - should be 14:30
            result = ConvertDateToDifferentTimezone(d, 0, 0); //same like here




        }

        public static double GetServerTimezoneOffsetHours()
        {
            return TimeZone.CurrentTimeZone.GetUtcOffset(new DateTime()).TotalHours;
        }
        public static double GetLocalGmt()
        {
            DateTime date = DateTime.Now;
            var gmtOffset = date.Subtract(date.ToUniversalTime());
            
            double gmtOffsetHours = gmtOffset.TotalHours;
            return gmtOffsetHours;
        }
        /// <summary>
        /// Converts a given local date to the required GMT.  Local date assumens the GMT of the server time.  Thus, for example if server is using GMT +1, and you want to convert to GMT +5, and the given date is
        /// 16:00, it will result in 20:00 (+4 hours difference)
        /// </summary>
        /// <param name="date">The local date</param>
        /// <param name="resultGmt">Required GMT </param>
        /// <returns></returns>
        public static DateTime ConvertLocalDateToDifferentTimezone(DateTime date, double resultGmt)
        {

            

            double gmtOffsetHours = GetLocalGmt();

            return ConvertDateToDifferentTimezone(date, gmtOffsetHours, resultGmt); ;


        }

        private static void __ConvertLocalDateToDifferentTimezone()
        {

            DateTime d = new DateTime(2011, 02, 20, 16, 0, 0);
            DateTime result;
            result = ConvertLocalDateToDifferentTimezone(d, 1); //should remain the same
            result = ConvertLocalDateToDifferentTimezone(d, 0); //to UK time, 15:00
            result = ConvertLocalDateToDifferentTimezone(d, 6); //5 hours from here, 21:00
            




        }


        public static string FormatDateTime(this DateTime dateOriginal, string dateFormat)
        {
            return dateOriginal.ToString(dateFormat);
            //return string.Format(dateFormat, dateOriginal);
        }

        public static string FormatDateTime(this DateTime dateOriginal, DATETIME_FORMAT formatType, IFormatProvider culture = null, 
            bool showGmt = false, double? localGmtOffset = null, double? resultGmtOffset = null )
        {
            StringBuilder sbDate = new StringBuilder();
            DateTime date = dateOriginal;
            if (!localGmtOffset.HasValue) localGmtOffset = GetLocalGmt();
            
            if (resultGmtOffset.HasValue)
            {
                double localGmt = (localGmtOffset.HasValue ? localGmtOffset.Value : GetLocalGmt());
                date = ConvertDateToDifferentTimezone(date, localGmt, resultGmtOffset.Value);
                
            }
            else
            {
                resultGmtOffset = localGmtOffset;
                
            }
            

            string s = ConvertDateTimeFormatToCode(formatType);

            string[] formatParts = s.Split(' ');
            
            foreach (string formatPart in formatParts)
            {
                if ( sbDate.Length >0) sbDate.Append(" ");
                sbDate.Append(date.ToString(formatPart, culture));
               
            }
            if (showGmt)
            {
                int hours = (int)resultGmtOffset.Value;
                int minutes = (int)Math.Round((double)(resultGmtOffset - (double)hours) * 60d);

                sbDate.Append(" [");
                if (hours > 0) sbDate.Append("+");
                sbDate.Append( hours.ToString("00") + ":" + minutes.ToString("00") + "]");
            }
            return sbDate.ToString();
        }

        public static int GetWeekNumberForDate(this DateTime date)
        {
              CultureInfo ciCurr = CultureInfo.CurrentCulture;
              int weekNum = ciCurr.Calendar.GetWeekOfYear(date, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
              return weekNum;
        }


        public static bool IsDateIncludedInRange(DateTime dateFrom, DateTime dateTo, DateTime dateToCheck, bool checkOnlyDates = true)
        {
            DateTime dFrom = dateFrom;
            DateTime dTo = dateTo;
            DateTime dCheck = dateToCheck;
            if (checkOnlyDates)
            {
                dFrom = dFrom.Date;
                dTo = dTo.Date;
                dCheck = dCheck.Date;
            }
            bool included = false;
            if (dCheck >= dFrom && dCheck <= dTo)
                included = true;
            return included;

        }

        /// <summary>
        /// Parses a total amount of seconds into time, e.g 100 seconds are converted into 00:01:40
        /// </summary>
        /// <param name="seconds"></param>
        /// <returns></returns>
        public static string ParseSecondsToFormattedShortTime(long totalSeconds)
        {
            int hours, minutes, seconds;
            _parseSecondsIntoHoursMinutesSeconds(totalSeconds, out hours, out minutes, out seconds);

            string s = hours.ToString("00") + ":" + minutes.ToString("00") + ":" + seconds.ToString("00");
            return s;


        }
        private static void _parseSecondsIntoHoursMinutesSeconds(long totalSeconds, out int hours, out int minutes, out int seconds)
        {
            int secondsInHour = 60 * 60;
            int secondsInMinute = 60;

            long remainingSec = totalSeconds;
            hours = (int)(remainingSec / (long)secondsInHour);
            remainingSec -= (hours * secondsInHour);

            minutes = (int)(remainingSec / (long)secondsInMinute);
            remainingSec -= (minutes * secondsInMinute);

            seconds = (int)remainingSec;
            


        }

        /// <summary>
        /// Parses a total amount of seconds into time, e.g 100 seconds are converted into 00:01:40
        /// </summary>
        /// <param name="seconds"></param>
        /// <returns></returns>
        public static string ParseSecondsToFormattedLongTime(long totalSeconds)
        {
            int hours, minutes, seconds;
            _parseSecondsIntoHoursMinutesSeconds(totalSeconds, out hours, out minutes, out seconds);

            string s = hours.ToString("#,0") + " hour(s), " + minutes.ToString() + " minute(s) and " + seconds + " seconds.";
            return s;


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="date"></param>
        /// <param name="format"></param>
        /// <param name="strictlyExactDateFormat">This makes sure that the format exactly must be adhered to, if it is false then for example 'yyyy' and 'yy' match also the same</param>
        /// <returns></returns>
        public static DateTime? ParseDate(string date, string format, bool strictlyExactDateFormat = false)
        {
            DateTime? result = null;
            DateTime d;
            if (DateTime.TryParseExact(date, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out d))
                result = d;
            
            if (!result.HasValue && !strictlyExactDateFormat && format.IndexOf("yyyy") != -1)
            {
                format = format.Replace("yyyy", "yy");
                result = ParseDate(date, format, strictlyExactDateFormat);
            }
            return result;
        }
        /// <summary>
        /// Shifts the day of week, so that Monday is index 0, not 1.
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        public static int ShiftDayOfWeekValueToMonday(DayOfWeek d)
        {
            int tmp = (int)d;
            tmp--;
            if (tmp == -1)
                tmp = 6;
            return tmp;
        }
        /// <summary>
        /// Gets the maximum days in a month, according to year
        /// </summary>
        /// <param name="month"></param>
        /// <param name="currYear"></param>
        /// <returns></returns>
        public static int GetMaxDaysInMonth(int month,int currYear)
        {
            
            switch (month)
            {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    return 31;
                    
                case 2:
                    bool leapYear = false;
                    if (currYear % 4 == 0)
                    {
                        leapYear = true;
                        if (currYear % 100 == 0)
                        {
                            if (currYear % 400 != 0)
                                leapYear = false;
                        }
                    }
                    if (!leapYear)
                        return 28;
                    else
                        return 29;
                default:
                    return 30;
                
            }
            
            
        }

        public static DateTime? CustomDate { get; set; }

        /// <summary>
        /// Returns the current time, but shifted to the local GMT as specified by Server_GMT
        /// </summary>
        /// <returns>The shifted date time</returns>
        public static DateTime Now
        
        {
            get
            {
                
                if (CustomDate == null)
                {
                    //throw new Exception("This server gmt: " + Settings.ServerGMT);
                    //return new DateTime(2010, 09, 20);

                    //Updated by Mark 2011-08-18
                    return DateTime.Now;

                    return (DateTime.Now.AddHours((double)Settings.Others.ServerGMT));
                }
                else
                    return CustomDate.Value;
            }
        }

        /// <summary>
        /// Returns the time differnece, in seconds between one time and another
        /// </summary>
        /// <param name="dt1"></param>
        /// <param name="dt2"></param>
        /// <returns>The time difference, in seconds.</returns>
        public static int TimeDiff(DateTime dt1, DateTime dt2)
        {
            int days = DateDiff(dt1, dt2);
            int result = 0;
            int hour1, hour2, min1, min2, sec1, sec2;
            result = (days * 60 * 60 * 24); // x 60 seconds x 60 minutes x 24 hours
            DateTime tmp1 = dt1, tmp2 = dt2;
            hour1 = dt1.Hour;
            hour2 = dt2.Hour;
            min1 = dt1.Minute;
            min2 = dt2.Minute;
            sec1 = dt1.Second;
            sec2 = dt2.Second;

            while (hour1 != hour2)
            {
                if (hour1 < hour2)
                    hour1++;
                else
                    hour1--;
                result += (60 * 60); // 60 seconds x 60 minutes;
            }
            while (min1 != min2)
            {
                if (min1 < min2)
                    min1++;
                else
                    min1--;
                result += (60); // 60 seconds;
            }
            while (sec1 != sec2)
            {
                if (sec1 < sec2)
                    sec1++;
                else
                    sec1--;
                result++; // 1 seconds;
            }

            return result;

        }

        /// <summary>
        ///  /// Returns the number of days from one date to another.  Example from 10/10/10 to 12/10/10 is 2 days.
        /// </summary>
        /// <param name="dt1"></param>
        /// <param name="dt2"></param>
        /// <returns>the number of days</returns>
        public static int DateDiff(DateTime date1, DateTime date2)
        {
            DateTime dt1 = date1.Date;
            DateTime dt2 = date2.Date;
            DateTime tmp = dt1;
            int add;
            int res = 0;
            add = -dt1.CompareTo(dt2);
            while (tmp.CompareTo(dt2) != 0)
            {
                tmp = tmp.AddDays((double)add);
                res++;
            }
            return res;

        }
        public static DateTime GetNextDayOfWeekAfter(DateTime d, DayOfWeek dayOfWeek)
        {
            DateTime curr = d;
            while (curr.DayOfWeek != dayOfWeek)
            {
                curr = curr.AddDays(1);
            }
            return curr;
        }
        /// <summary>
        /// Returns Monday, Tuesday, etc from a number (0 = Mon, 1 = Tue, etc)
        /// </summary>
        /// <param name="DayID">Day ID</param>
        /// <returns>returns day name</returns>
        public static string GetDayNameFromDayIndex(int DayID)
        {
            switch (DayID)
            {
                case 0: return "Monday";
                case 1: return "Tuesday";
                case 2: return "Wednesday";
                case 3: return "Thursday";
                case 4: return "Friday";
                case 5: return "Saturday";
                case 6: return "Sunday";

            }
            throw new InvalidOperationException("Day ID not valid! Please enter a number from 0 - 6, where 0 is Monday and 6 is Sunday");
        }
        public static int GetAmtDaysDifferenceInteger(DateTime from, DateTime to)
        {

            return (int)GetAmtDaysDifference(from, to);

        }
        public static double GetAmtDaysDifference(DateTime from, DateTime to)
        {

            TimeSpan tsFrom = DateTimeToTimeSpan(from);
            TimeSpan tsTo = DateTimeToTimeSpan(to);
            TimeSpan diff = tsTo - tsFrom;
            return diff.TotalDays;

        }
        public static double GetAmtDaysFromNowTillDate(DateTime date)
        {
            return GetAmtDaysDifference(DateTime.Now, date);
            TimeSpan tsNow = DateTimeToTimeSpan(DateTime.Now);
            TimeSpan tsDate = DateTimeToTimeSpan(date);
            TimeSpan diff = tsDate - tsNow;
            return diff.TotalDays;

        }
        public static TimeSpan DateTimeToTimeSpan(DateTime date)
        {
            return new TimeSpan(date.Ticks);
        }
        /// <summary>
        /// Convert an amount of years, months and days to Util.Text.
        /// </summary>
        /// <example>
        /// Parameters 10, 3, 5 will change to 10 years, 3 months and 5 days
        ///            3, 5 will change to 3 years and 5 months
        /// </example>
        /// <param name="years"></param>
        /// <param name="months"></param>
        /// <param name="days"></param>
        /// <returns></returns>
        public static string YearsMonthsDaysToText(int years, int months, int days)
        {
            string s = "";
            if (years > 0)
            {
                s += years + " year";
                if (years > 1) s += s;
            }
            if (months > 0)
            {
                if (years > 0)
                {
                    if (days == 0)
                    {
                        s += " and ";
                    }
                    else
                    {
                        s += ", ";
                    }
                }
                s += months + " month";
                if (months > 1) s += "s";
            }
            if (days > 0)
            {
                if (years > 0 || months > 0)
                {
                    s += " and ";
                }
                s += days + " day";
                if (days > 1) s += "s";
            }
            return s;
        }

        public static string DateForFlash(DateTime date)
        {
            return date.ToString("yyyy-MM-dd HH:mm:ss");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="date">Mon May 10 18:29:32 GMT 0200 2010</param>
        /// <returns></returns>
        public static DateTime? DateFromFlashUTC(string date)
        {
           // Mon May 10 16:37:55 2010
            if (!string.IsNullOrEmpty(date))
            {
                date = date.Replace("UTC", "+00:00");
            }

            DateTime? d = ParseDate(date, "ddd MMM dd HH:mm:ss yyyy zzz");
            if (d == null)
            {
                d = ParseDate(date, "ddd MMMM dd HH:mm:ss yyyy zzz");
            }
            return d;
        }
        /// <summary>
        /// Returns the first day with the given day of the week, after the specified date.  If the date given is the same as day of the week required, that is returned.
        /// </summary>
        /// <param name="date">Date to start from, inclusive.</param>
        /// <param name="dayOfWeek">Day of the week, e.g Monday</param>
        /// <returns>The date with the next day of the week as required</returns>
        public static DateTime GetFirstDayAfterDate(DateTime date, DayOfWeek dayOfWeek)
        {
            DateTime curr = date;
            while (curr.DayOfWeek != dayOfWeek)
            {
                curr = curr.AddDays(1);
            }
            return curr;
        }
        /// <summary>
        /// Returns the first day with the given day of the week, before the specified date.  If the date given is the same as day of the week required, that is returned.
        /// </summary>
        /// <param name="date">Date to start from, inclusive.</param>
        /// <param name="dayOfWeek">Day of the week, e.g Monday</param>
        /// <returns>The date with the next day of the week as required</returns>
        public static DateTime GetFirstDayBeforeDate(DateTime date, DayOfWeek dayOfWeek)
        {
            DateTime curr = date;
            while (curr.DayOfWeek != dayOfWeek)
            {
                curr = curr.AddDays(-1);
            }
            return curr;
        }

        /// <summary>
        /// Date must be in the format of Mon, 13 Oct 2008 10:45 am 
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime GetDateFromRFC822(string date)
        {
            string format = null;
            int colonIndex = date.IndexOf(":");
            int ttIndex = -1;
            if (colonIndex != -1)
            {
                //Has time
                ttIndex = date.ToLower().IndexOf("am", colonIndex);
                if (ttIndex == -1) ttIndex = date.ToLower().IndexOf("pm", colonIndex);
            }

            if (ttIndex == -1 && colonIndex != -1)
            {
                //No AM/PM
                format = "ddd, d MMM yyyy hh:mm";
                date = date.Substring(0, colonIndex + 2);
            }
            else if (ttIndex != -1 && colonIndex != -1)
            {
                format = "ddd, d MMM yyyy h:mm tt";
                date = date.Substring(0, ttIndex + 2);

            }
            if (colonIndex == -1)
            {
                //No Colon
                format = "ddd, d MMM yyyy";
                date = date.Substring(0, Math.Min(format.Length, date.Length));
            }

            
            DateTime d = DateTime.ParseExact(date,
                               format, System.Globalization.CultureInfo.InvariantCulture);
            return d;

        }

        /// <summary>
        /// Get total week days (Mon,Tue,Wed,Thur,Fri) between range
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        public static int GetTotalWeekdaysDuring(DateTime dateFrom, DateTime dateTo)
        {
            int tot = 0;
            DateTime curr = dateFrom.Date;
            while (curr <= dateTo.Date)
            {
                if (curr.DayOfWeek != DayOfWeek.Saturday && curr.DayOfWeek != DayOfWeek.Sunday)
                {
                    tot++;
                }
                curr = curr.AddDays(1);
            }
            return tot;
        }
        /// <summary>
        /// Get total week ends between range (Sat, SUn);
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns></returns>
        public static int GetTotalWeekendsDuring(DateTime dateFrom, DateTime dateTo)
        {
            int tot = 0;
            DateTime curr = dateFrom.Date;
            while (curr <= dateTo.Date)
            {
                if (curr.DayOfWeek == DayOfWeek.Saturday || curr.DayOfWeek == DayOfWeek.Sunday)
                {
                    tot++;
                }
                curr = curr.AddDays(1);
            }
            return tot;
        }
        public static DateTime GetEndDateForMonth(DateTime date)
        {
            return GetEndDateForMonth(date.Year, date.Month);
        }
        public static DateTime GetEndDateForMonth(int year, int month)
        {
         
            DateTime d = new DateTime(year, month, 1,23,59,59);
            d = d.AddMonths(1);
            d = d.AddDays(-1);
            return d;

        }
        public static DateTime? DateFromISO8601String(string d)
        {
            string format = "yyyy-MM-dd";
            DateTime? parsedDate = null;
           
            parsedDate = ParseDate(d, format);
            if (parsedDate == null) 
            {
                format += " HH:mm:ss";
                parsedDate = ParseDate(d, format);
            }
            return parsedDate;
            
        }
        public static string DateToISO8601String(DateTime d, bool includeTime)
        {
            string format = "yyyy-MM-dd";
            if (includeTime)
            {
                format += " HH:mm:ss";
            }
            return d.ToString(format);
        }
        public static DateTime ConvertFromUnixTimestamp(double timestamp)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return origin.AddSeconds(timestamp);
        }


        public static double ConvertToUnixTimestamp(DateTime date)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            TimeSpan diff = date - origin;
            return Math.Floor(diff.TotalSeconds);
        }

        /// <summary>
        /// Returns the start of day of a givne date, e.g 13/07/2012 00:00:00 (midnight)
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime GetStartOfDay(this DateTime dateTime)
        {
            return dateTime.Date;
            
        }

        /// <summary>
        /// Returns the end of day of the given date, e.g 13/07/2012 23:59:59 
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime GetEndOfDay(this DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, 23, 59, 59);
            
        }

    }
}
