﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;
using CS.General_v3.Classes.Caching;

namespace CS.General_v3.Util
{
    public static class CachingUtil
    {
       



        public const string ALL_PAGES_PARAM = "AllPages";
        public static void InvalidateAllPagesOuputCachingDependency()
        {
            if (CS.General_v3.Util.PageUtil.CheckRequestExistsAtContext())
            {
                CacheInvalidator.Instance.InvalidateCacheDependencies(ALL_PAGES_PARAM);

            }
        }
        public static void InvalidateCacheDependency(params string[] keys)
        {
            CacheInvalidator.Instance.InvalidateCacheDependencies(keys);
        }

        public static CacheDependency GetCacheDependency(params string[] keys)
        {
            Cache cache = HttpRuntime.Cache;
            if (cache != null)
            {
                foreach (var key in keys)
                {
                    if (cache[key] == null)
                    {
                        cache[key] = DateTime.Now;
                    }
                }
            }
            var dep = new System.Web.Caching.CacheDependency(null, keys);
            return dep;
        }

        
        public static T GetItemFromCache<T>(string key)
        {
            
            object value = HttpRuntime.Cache.Get(key);
            T item = default(T);
            if (value != null)
            {
                item = (T)value;
            }
            return item;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="data"></param>
        /// <param name="timeToExpireFromLastAccessed"></param>
        /// <param name="context"></param>
        /// <param name="dependencies">Add dependencies to invalidate the cache - Use GetOutputCacheDependencyForFactories()</param>
        public static void AddItemToCache(string key, object data, TimeSpan timeToExpireFromLastAccessed, CacheDependency dependencies = null)
        {
            
            HttpRuntime.Cache.Add(key, data, dependencies, System.Web.Caching.Cache.NoAbsoluteExpiration, timeToExpireFromLastAccessed, CacheItemPriority.Default, null);
        }
        public static void RemoveItemFromCache(string key)
        {
            
            HttpRuntime.Cache.Remove(key);
        }
    }
}
