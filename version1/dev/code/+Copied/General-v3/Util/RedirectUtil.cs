﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web;
namespace CS.General_v3.Util
{
    public static class RedirectUtil
    {
        public static void Redirect301(System.Web.UI.Page page, string url)
        {
            page.Response.Status = "301 Moved Permanently";
            page.Response.AddHeader("Location", url);
        }
    }
}
