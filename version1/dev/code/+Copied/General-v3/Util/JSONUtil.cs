﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CS.General_v3.Util
{
    public static class JSONUtil
    {
        public static string Encode(string str)
        {
            str = str.Replace("\r", "#R#");
            str = str.Replace("\n", "#N#");
            str = str.Replace("\t", "#T#");
            return str;
        }
        public static T Deserialise<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);

        }
        public static string Serialize<T>(T obj, NullValueHandling nullValueHandling = NullValueHandling.Ignore, Formatting formatting = Formatting.None)
        {
            JsonSerializer serializer = new JsonSerializer();
            serializer.NullValueHandling = nullValueHandling;
            serializer.Converters.Add(new JavaScriptDateTimeConverter());
            StringWriter sw = new StringWriter();
            JsonWriter jw = new JsonTextWriter(sw);
            serializer.Serialize(jw, obj);
            return sw.ToString();

            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = nullValueHandling;
            return JsonConvert.SerializeObject(obj, formatting, new JavaScriptDateTimeConverter());
            return JsonConvert.SerializeObject(obj, formatting, settings);
            
        }
    }
}
