﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Remoting.Metadata.W3cXsd2001;

namespace CS.General_v3.Util
{
    using Classes.ConversionUnits;

    public static class Conversion
    {
        public static string ConvertByteArrayToHex(byte[] by)
        {
            SoapHexBinary shb = new SoapHexBinary(by);
           return shb.ToString();
        }
        /// <summary>
        /// It is important that the hex string contains an even amount of characters, and only characters from 0 - 9, ABCDEF
        /// </summary>
        /// <param name="hex"></param>
        /// <returns></returns>
        public static byte[] ConvertHexStringToByteArray(string hex)
        {
            if (hex.Length % 2 != 0)
                throw new InvalidOperationException("Length must be even");
            SoapHexBinary shb = SoapHexBinary.Parse(hex);
            return shb.Value;
        }


        public static double FahrenheitToCelsius(double fahrenheit)
        {
            return (double)((double)5 / (double)9) * (double)(fahrenheit - (double)32);
        }
        public static double CelsiusToFahrenheit(double celsius)
        {
            return ((double)9 / (double)5) * celsius + (double)32;
        }
        public static DateTime ToDateTime(object o)
        {
            return Convert.ToDateTime(o);
        }
        public static double ToDouble(object o)
        {

            if (o is string)
            {
                o = checkStringForNumber((string)o);
            }
            return Convert.ToDouble(o, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
        }
        private static string checkStringForNumber(string s)
        {
            if (!string.IsNullOrEmpty(s) && s[0] == '.')
            {
                s = "0" + s;
            }
            if (!s.Contains('.'))
                s = s.Replace(",", ".");
            if (string.IsNullOrEmpty(s))
                s = "0";
            return s;
        }
        public static int ToInt32(object o)
        {
            if (o is string)
            {
                o = checkStringForNumber((string)o);
            }
            return Convert.ToInt32(o);
        }
        public static long ToLong(object o)
        {
            if (o is string)
            {
                o = checkStringForNumber((string)o);
            }
            return Convert.ToInt64(o);
        }
        public static double CmToInches(double cm)
        {
            return cm * 0.393700787;
        }

        public static byte[] ConvertStringToByteArray(string saltInHex)
        {
            List<byte> list = new List<byte>();
            for (int i = 0; i < saltInHex.Length; i++)
            {
                list.Add((byte)saltInHex[i]);
            }
            return list.ToArray();
        }

        public static double ConvertVolumeUnits(Enums.VOLUME_UNITS fromUnit, Enums.VOLUME_UNITS toUnit, double originalValue)
        {
            UnitConvertorLinear<Enums.VOLUME_UNITS> convertor = new UnitConvertorLinear<Enums.VOLUME_UNITS>();
            return convertor.ConvertValueToUnit(originalValue, fromUnit, toUnit);

            /*

            //if both TO and FROM units are identical, return the current value and skip all logic
            if (fromUnit == toUnit)
            {
                return originalValue;
            }

            double result = originalValue;
            //convert the value to CM^3 (if not already so)
            if (fromUnit != Enums.VOLUME_UNITS.CubicCentimetre)
            {
                switch (fromUnit)
                {
                    case Enums.VOLUME_UNITS.CubicCentimetre:
                        result = originalValue; //already CM^3, no need to convert again.
                        break;
                    case Enums.VOLUME_UNITS.CubicMetre:
                        result = originalValue * 1000000;
                        break;
                    default: throw new InvalidOperationException("Unit type <" + fromUnit + "> not yet implemented!");
                }
            }

            //convert unit
            switch (toUnit)
            {
                case Enums.VOLUME_UNITS.CubicCentimetre:
                    //do nothing - already is in correct format
                    break;
                case Enums.VOLUME_UNITS.CubicMetre:
                    result = result / 1000000;
                    break;
                default: throw new InvalidOperationException("Unit type <" + fromUnit + "> not yet implemented!");
            }
            return result;*/
        }

        public static double ConvertWeightUnits(Enums.WEIGHT_UNITS fromUnit, Enums.WEIGHT_UNITS toUnit, double originalValue)
        {
            UnitConvertorLinear<Enums.WEIGHT_UNITS> convertor = new UnitConvertorLinear<Enums.WEIGHT_UNITS>();
            return convertor.ConvertValueToUnit(originalValue, fromUnit, toUnit);
            /*

            //if both TO and FROM units are identical, return the current value and skip all logic
            if (fromUnit == toUnit)
            {
                return originalValue;
            }

            double result = originalValue;

            //convert the value to grams (if not already so)
            if (fromUnit != Enums.WEIGHT_UNITS.Grams)
            {
                switch (fromUnit)
                {
                    case Enums.WEIGHT_UNITS.Grams:
                        result = originalValue; //already G, no need to convert again.
                        break;
                    case Enums.WEIGHT_UNITS.Kilograms:
                        result = originalValue * 1000;
                        break;
                    case Enums.WEIGHT_UNITS.Ounce:
                        result = originalValue * 28.3495231;
                        break;
                    case Enums.WEIGHT_UNITS.Pound:
                        result = originalValue * 453.6;
                        break;
                    case Enums.WEIGHT_UNITS.Tonne:
                        result = originalValue * 1000000;
                        break;
                    default: throw new InvalidOperationException("Unit type <" + fromUnit + "> not yet implemented!");
                }
            }

            //convert unit
            switch (toUnit)
            {
                case Enums.WEIGHT_UNITS.Grams:
                    //do nothing - already is in correct format
                    break;
                case Enums.WEIGHT_UNITS.Kilograms:
                    result = result / 1000;
                    break;
                case Enums.WEIGHT_UNITS.Ounce:
                    result = result / 28.3495231;
                    break;
                case Enums.WEIGHT_UNITS.Pound:
                    result = result / 453.6;
                    break;
                case Enums.WEIGHT_UNITS.Tonne:
                    result = result / 1000000;
                    break;
                default: throw new InvalidOperationException("Unit type <" + fromUnit + "> not yet implemented!");
            }
            return result;*/
        }

        public static double ConvertLengthUnits(Enums.LENGTH_UNITS fromUnit, Enums.LENGTH_UNITS toUnit, double originalValue)
        {

            UnitConvertorLinear<Enums.LENGTH_UNITS> convertor = new UnitConvertorLinear<Enums.LENGTH_UNITS>();
            return convertor.ConvertValueToUnit(originalValue, fromUnit, toUnit);
            /*
            //if both TO and FROM units are identical, return the current value and skip all logic
            if (fromUnit == toUnit)
            {
                return originalValue;
            }

            double result = originalValue;

            //convert the value to grams (if not already so)
            if (fromUnit != Enums.LENGTH_UNITS.Centimetre)
            {
                switch (fromUnit)
                {
                    case Enums.LENGTH_UNITS.Centimetre:
                        result = originalValue; //already cm, no need to convert again.
                        break;
                    case Enums.LENGTH_UNITS.Kilometre:
                        result = originalValue * 100000;
                        break;
                    case Enums.LENGTH_UNITS.Metre:
                        result = originalValue * 100;
                        break;
                    case Enums.LENGTH_UNITS.Mile:
                        result = originalValue * 160934.4;
                        break;
                    case Enums.LENGTH_UNITS.Millimetre:
                        result = originalValue / 10;
                        break;
                    default: throw new InvalidOperationException("Unit type <" + fromUnit + "> not yet implemented!");
                }
            }

            //convert unit
            switch (toUnit)
            {
                case Enums.LENGTH_UNITS.Centimetre:
                    //do nothing - already is in correct format
                    break;
                case Enums.LENGTH_UNITS.Kilometre:
                    result = originalValue / 100000;
                    break;
                case Enums.LENGTH_UNITS.Metre:
                    result = originalValue / 100;
                    break;
                case Enums.LENGTH_UNITS.Mile:
                    result = originalValue / 160934.4;
                    break;
                case Enums.LENGTH_UNITS.Millimetre:
                    result = originalValue * 10;
                    break;
                default: throw new InvalidOperationException("Unit type <" + fromUnit + "> not yet implemented!");
            }
            return result;*/
        }
    }
}
