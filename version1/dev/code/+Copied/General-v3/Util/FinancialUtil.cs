﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Util
{
    public static class FinancialUtil
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="priceIncTax"></param>
        /// <param name="TaxRate">VAT Rate, as a number from 0 - 100.</param>
        /// <returns></returns>
        public static double GetTaxOnlyAmountFromPriceIncTax(double priceIncTax, double TaxRate)
        {
            double priceExcVAT = GetPriceExcTaxFromPriceIncTax(priceIncTax, TaxRate);
            return priceIncTax - priceExcVAT;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="priceExcTax"></param>
        /// <param name="TaxRate">VAT Rate, as a number from 0 - 100.</param>
        /// <returns></returns>
        public static double GetPriceIncTaxFromPriceExcTax(double priceExcTax, double TaxRate)
        {
            double vatPerc = 1d + (TaxRate / 100d);
            return priceExcTax * vatPerc;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="priceIncTax"></param>
        /// <param name="TaxRate">VAT Rate, as a number from 0 - 100.</param>
        /// <returns></returns>
        public static double GetPriceExcTaxFromPriceIncTax(double priceIncTax, double TaxRate)
        {
            double vatPerc = 1d + (TaxRate / (double)100);
            double priceExcVAT = priceIncTax / vatPerc;
            return priceExcVAT;
        }


    }
}
