﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;

namespace CS.General_v3.Util
{
    public static class QueryStringUtil
    {
        public static T? GetEnumFromQS<T>(string key, T? defaultValue = null, NameValueCollection qs = null) where T : struct, IConvertible
        {
            var enumValue = CS.General_v3.Util.EnumUtils.EnumValueNullableOf<T>(GetStringFromQS(key, qs), false);
            return enumValue;
        }
        public static int? GetIntFromQS(string key, int? defaultValue = null, NameValueCollection qs = null)
        {
            return (int?)GetDoubleFromQS(key, defaultValue, qs);
        }
        public static long? GetLongFromQS(string key, long? defaultValue = null, NameValueCollection qs = null)
        {
            return (long?)GetDoubleFromQS(key, defaultValue, qs);
        }
        public static double? GetDoubleFromQS(string key, double? defaultValue = null, NameValueCollection qs = null)
        {
            
            string sVal = GetStringFromQS(key, qs);
            if (CS.General_v3.Util.NumberUtil.IsNumeric(sVal))
            {
                double val = double.Parse(sVal);
                return val;
            }
            else
            {
                return defaultValue;
            }
        }
        public static string GetStringFromQS(string key, NameValueCollection qs = null)
        {
            if (qs == null)
            {
                qs = CS.General_v3.Util.PageUtil.GetRequestQueryString();
            }
            return qs[key];
        }
        public static bool GetBoolFromQS(string key, bool defaultValue = false, NameValueCollection qs = null)
        {
            var val = GetBoolNullableFromQS(key, defaultValue, qs);
            if (val.HasValue)
            {
                return val.Value;
            }
            return defaultValue;
        }
        /// <summary>
        /// Important, Types can be only basic data types, such as string, int, long etc..
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="delimiter"></param>
        /// <param name="qs"></param>
        /// <returns></returns>
        public static List<T> GetListOfBasicDataTypeFromQS<T>(string key, char delimiter = ',', NameValueCollection qs = null)
        {
            string sVal = GetStringFromQS(key, qs);
            List<T> list = new List<T>();
            if (!string.IsNullOrEmpty(sVal))
            {
                string[] values = sVal.Split(delimiter);

                foreach (string value in values)
                {
                    list.Add(CS.General_v3.Util.Other.ConvertStringToBasicDataType<T>(value));
                }
            }
            return list;
        }
        public static bool? GetBoolNullableFromQS(string key, bool? defaultValue = null, NameValueCollection qs = null)
        {

            string sBool = GetStringFromQS(key, qs);
            if (sBool != null)
            {
                bool val = ConvertStringToBool(sBool);
                return val;
            }
            else
            {
                return defaultValue;
            }
        }
        public static bool ConvertStringToBool(string sBool)
        {
            return sBool != null && sBool == "1" || sBool.ToLower() == "true" || sBool.ToLower() == "yes";
        }

    }
}
