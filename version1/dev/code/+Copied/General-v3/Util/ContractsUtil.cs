﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.Exceptions;

namespace CS.General_v3.Util
{
    public static class ContractsUtil
    {

        private const string defaultErrMsg = "Contract requirement failed";

        private static ContractException getErrorException(string errorMsg)
        {
            if (string.IsNullOrWhiteSpace(errorMsg)) errorMsg = defaultErrMsg;
            ContractException ex = new ContractException(errorMsg);
            return ex;
                
        }
        public static void RequiresNotNullable(object o, string errorMsg)
        {
            if (o == null)
                throw getErrorException(errorMsg);
        }
        public static void Requires(bool b, string errorMsg = null)
        {
            if (!b)
            {
                throw getErrorException(errorMsg);
            }
        }

    }
}
