using System;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;

namespace CS.General_v3.Util
{
    public static class Process
    {
        public class MyProcess : IDisposable
        {
            #region old code
            /*
            public void Start3(bool asynchronous)
            {
                ProcessStartInfo psi = new ProcessStartInfo();
                psi.FileName = Path;
                psi.Arguments = this.Arguments;
                psi.RedirectStandardError = true;
                psi.RedirectStandardOutput = true;
                psi.UseShellExecute = false;
                psi.WindowStyle = ProcessWindowStyle.Hidden;
                using (System.Diagnostics.Process process = new System.Diagnostics.Process())
                {
                    process.StartInfo = psi;
                    process.EnableRaisingEvents = true;
                    process.Exited += new EventHandler(process_Exited);
                    process.Start();
                    process.WaitForExit();
                    int exitCode = process.ExitCode;
                    string output = process.StandardOutput.ReadToEnd();
                    string output2 = process.StandardError.ReadToEnd();


                    process.Close();
                }
            }
            void process_Exited(object sender, EventArgs e)
            {
                if (this.ProcessFinished != null)
                    this.ProcessFinished(this, null);
            }
            private void startProcess()
            {

                _p.Start();
                string tmp = "";
                while (!_p.HasExited)
                {
                    //if (!this.Output.EndOfStream)

                    tmp = this.Error.ReadToEnd();
                    _ErrorText.Append(tmp);

                    tmp = this.Output.ReadToEnd();
                    _OutputText.Append(tmp);
                    //if (!this.Error.EndOfStream)

                    if (!_p.HasExited)
                        System.Threading.Thread.Sleep(100);
                }
                tmp = "";
                if (!this.Error.EndOfStream)
                    tmp = this.Error.ReadToEnd();
                _ErrorText.Append(tmp);
                tmp = "";
                if (!this.Output.EndOfStream)
                    tmp = this.Output.ReadToEnd();
                _OutputText.Append(tmp);

                _p.Dispose();
                if (!this.RunAsynchronously)
                {
                    _executingThread.Interrupt();

                }
                else
                {
                    if (this.ProcessFinished != null)
                        this.ProcessFinished(this, null);

                }

            }
            */
            #endregion
            public event EventHandler ProcessFinished;
            public bool RunAsynchronously { get; set; }
            private System.Diagnostics.Process _p = null;
            private ProcessStartInfo _startInfo = null;
            public string Path { get; set; }
            public string Arguments { get; set; }
            private FileStream _ErrorFile;
            private StreamWriter _errorFileSw;
            public string ErrorFilePath { get; private set; }
            private FileStream _OutputFile;
            private StreamWriter _outputFileSw;
            public string OutputFilePath { get; private set; }
            private void initFiles()
            {
                {
                    ErrorFilePath = CS.General_v3.Util.IO.GetTempFilename(".txt");
                    _ErrorFile = new FileStream(ErrorFilePath, FileMode.Create, FileAccess.Write, FileShare.Read);
                    _errorFileSw = new StreamWriter(_ErrorFile);
                }
                {
                    OutputFilePath = CS.General_v3.Util.IO.GetTempFilename(".txt");
                    _OutputFile = new FileStream(OutputFilePath, FileMode.Create, FileAccess.Write, FileShare.Read);
                    _outputFileSw = new StreamWriter(_OutputFile);
                }

            }

            private System.Threading.Thread _executingThread;
           

            
            public bool HasExited
            {
                get
                {
                    return _p.HasExited;
                }
            }
            public StreamReader Error
            {
                get
                {
                    return _p.StandardError;
                }
            }
            public StreamReader Output
            {
                get
                {
                    return _p.StandardOutput;

                }
            }

            private bool _finishedReadingOutput = false;
            private bool _finishedReadingError = false;
            private bool finished = false;
            private void checkFinishedProcess()
            {
                if (_finishedReadingError && _finishedReadingOutput)
                {
                    if (this.ProcessFinished != null)
                        this.ProcessFinished(this, null);
                    if (_p != null)
                    {
                        try
                        {
                            _p.Dispose();
                        }
                        catch (Exception ex)
                        {
                            
                        }
                    }
                    _p = null;
                    finished = true;
                    
                }
            }
            private void stdOutputReader()
            {
                string str = null;
                do
                {
                    str = _p.StandardOutput.ReadToEnd();
                    if (_outputFileSw != null)
                    {
                        _outputFileSw.Write(str);
                    }

                    // do something with str

                } while (str != null && !_p.HasExited);
                _finishedReadingOutput = true;
                checkFinishedProcess();
            }
            private void stdErrorReader()
            {
                string str = null;
                do
                {
                    str = _p.StandardError.ReadToEnd();
                    _errorFileSw.Write(str);
                    
                    // do something with str

                } while (str != null && !_p.HasExited);
                _finishedReadingError= true;
                checkFinishedProcess();
            }
            private System.Threading.Thread threadStdOutReader;
            private System.Threading.Thread threadStdErrReader;
            public string WorkingDirectory { get; set; }
            public void Start(bool asynchronous)
            {
                _finishedReadingError = false;
                _finishedReadingOutput = false;
                initFiles();
                
                this._executingThread = System.Threading.Thread.CurrentThread;
                this.RunAsynchronously = asynchronous;
                string args = this.Arguments;
                if (args == "")
                    args = null;
                _startInfo = new ProcessStartInfo(Path, args);
                if (!string.IsNullOrWhiteSpace(WorkingDirectory)) _startInfo.WorkingDirectory = WorkingDirectory;
                
                
                _startInfo.RedirectStandardError = true;
                _startInfo.RedirectStandardOutput = true;
                //_startInfo.UseShellExecute = false;
                _startInfo.UseShellExecute = false;
                _startInfo.CreateNoWindow = true;

                _p = new System.Diagnostics.Process();
                _p.StartInfo = _startInfo;
                
                _p.EnableRaisingEvents = true;
                _p.Exited += new EventHandler(_p_Exited);
                _p.ErrorDataReceived += new DataReceivedEventHandler(_p_ErrorDataReceived);
                                
                _p.Start();
                
                //System.Threading.Thread thread = new System.Threading.Thread(new System.Threading.ThreadStart(startProcess));
                //threa2d.Start();
                threadStdOutReader = new System.Threading.Thread(new System.Threading.ThreadStart(stdOutputReader));
                threadStdErrReader = new System.Threading.Thread(new System.Threading.ThreadStart(stdErrorReader));
                threadStdOutReader.Start();
                threadStdErrReader.Start();

                if (!RunAsynchronously)
                {
                    //while (_p != null && !_p.HasExited && !finished)
                        while (!finished)
                    {
                        System.Threading.Thread.Sleep(100);
                    }
                    
                    closeFiles();
                    //CS.General_v3.Util.ThreadUtil.Pause();
                }
                
                
                
            }
            public void Start()
            {
                Start(RunAsynchronously);

            }

            void _p_ErrorDataReceived(object sender, DataReceivedEventArgs e)
            {
                

            }

            void _p_Exited(object sender, EventArgs e)
            {

                
                
            }

            public MyProcess(string path)
                : this(path, "")
            {

            }
            public MyProcess(string path, string args)
            {
                this.Path = path;
                this.Arguments = args;
                this.RunAsynchronously = false;
            }


            private void closeFiles()
            {
                if (this._OutputFile != null)
                {
                    this._outputFileSw.Close();
                    this._OutputFile.Close();
                    this._outputFileSw.Dispose();
                    this._OutputFile.Dispose();
                    this._outputFileSw = null;
                    this._OutputFile = null;
                }
                if (this._ErrorFile != null)
                {
                    this._errorFileSw.Close();
                    this._ErrorFile.Close();
                    this._errorFileSw.Dispose();
                    this._ErrorFile.Dispose();
                    this._errorFileSw = null;
                    this._ErrorFile = null;
                }

            }


            #region IDisposable Members

            public void Dispose()
            {
                closeFiles();
                CS.General_v3.Util.IO.DeleteFile(this.ErrorFilePath);
                CS.General_v3.Util.IO.DeleteFile(this.OutputFilePath);
            }

            #endregion

            public string GetOutputText()
            {
                StreamReader sr = new StreamReader(_OutputFile);
                
                string s = sr.ReadToEnd();
                sr.Dispose();
                return s;
            }
            public string GetErrorText()
            {
                StreamReader sr = new StreamReader(_ErrorFile);

                string s = sr.ReadToEnd();
                sr.Dispose();
                return s;
            }
        }
        
        public static bool IsSingleInstance()
        {
            string sModName,sProcName;
            int totalInstances;
            
            sModName = System.Diagnostics.Process.GetCurrentProcess().MainModule.ModuleName;
            sProcName = System.IO.Path.GetFileNameWithoutExtension(sModName);
            totalInstances = System.Diagnostics.Process.GetProcessesByName(sProcName).Length;
            return (totalInstances == 1);
        }
    }
}
