﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSharpCode.SharpZipLib.Zip;
using System.IO;

namespace CS.General_v3.Util
{
    public static class ZIPUtil
    {
        public static void ExtractZIP(string zipPath, string dirToExtractTo)
        {
            FastZip zip = new FastZip();
            //zipPath = @"d:\test_extract.zip";
           // dirToExtractTo = @"d:\testextract\";
            CS.General_v3.Util.IO.CreateDirectory(dirToExtractTo);
            
            zip.ExtractZip(zipPath, dirToExtractTo, FastZip.Overwrite.Always, null,null,null,false);

        }
        /// <summary>
        /// Creates a zip file with the content string provided.
        /// </summary>
        /// <param name="zipPath">ZIP Path</param>
        /// <param name="content">Content String</param>
        /// <param name="contentFileExtension">Filename of content in zip file</param>
        /// <param name="compressionLevel">Compression (0-9), 9 being highest</param>
        public static void CreateZIPFileFromFolder(string zipPath, string folderToZip, bool recurse = true, string fileFilter= null, int compressionLevel = 5)
        {
            CS.General_v3.Util.IO.CreateDirectory(zipPath);
            FileStream fsOut = new FileStream(zipPath, FileMode.Create, FileAccess.Write);

            ZipOutputStream zipOutputStream = new ZipOutputStream(fsOut);


            folderToZip = CS.General_v3.Util.IO.EnsurePathEndsWithSlash(folderToZip);


            zipOutputStream.SetLevel(compressionLevel);
            DirectoryInfo dir = new DirectoryInfo(folderToZip);
            var allFiles = dir.GetFiles("*", SearchOption.AllDirectories);
            foreach (FileInfo f in allFiles)
            {
                string entryName = f.FullName.Substring(folderToZip.Length);

                ZipEntry entry = new ZipEntry(entryName);
                zipOutputStream.PutNextEntry(entry);
                byte[] byFiles =CS.General_v3.Util.IO.LoadFileAsByteArray(f.FullName);
                zipOutputStream.Write (byFiles,0,byFiles.Length);
                //entry.CompressionMethod = compressionMethod;

            }
            zipOutputStream.Finish();
            zipOutputStream.Close();

            //CS.General_v3.Util.IO.GetFilesInDirByRegExp(folderToZip,"*", System.Text.RegularExpressions.RegexOptions.

            //ZipEntry z;
            
            
            //FastZip zip = new FastZip();
            //ICSharpCode.SharpZipLib.Zip.ZipFile.Create(
            //zip.CreateZip(zipPath, folderToZip, recurse: recurse, fileFilter: fileFilter);
            
        }

        /// <summary>
        /// Creates a zip file with the content string provided.
        /// </summary>
        /// <param name="zipPath">ZIP Path</param>
        /// <param name="content">Content String</param>
        /// <param name="contentFileExtension">Filename of content in zip file</param>
        public static void CreateZIPFile(string zipPath, string content, string contentFileName)
        {
            if (string.IsNullOrEmpty(contentFileName))
                throw new InvalidOperationException("Content Filename must be specified");
            
            string tmpFolderPath = System.IO.Path.GetTempPath() + System.IO.Path.GetRandomFileName() + CS.General_v3.Util.Random.GetString(20, 20) + "\\";
            CS.General_v3.Util.IO.CreateDirectory(tmpFolderPath);



            CS.General_v3.Util.IO.SaveToFile(tmpFolderPath + contentFileName, content);
            CreateZIPFileFromFolder(zipPath, tmpFolderPath, recurse: false);
            //zip.CreateZip(zipPath, tmpFolderPath, false, null);
            CS.General_v3.Util.IO.EmptyDirectory(tmpFolderPath);
            System.IO.Directory.Delete(tmpFolderPath);
        }

        public static void CreateZIPFile(string zipPath, IEnumerable<FileInfo> files)
        {
            List<string> list = new List<string>();
            foreach (var f in files)
            {
                list.Add(f.FullName);
            }
            CreateZIPFile(zipPath, list);
        }
        public static void CreateZIPFile(string zipPath, IEnumerable<string> files)
        {

            string tempPath = CS.General_v3.Util.IO.CreateTempDir();
            foreach (var s in files)
            {
                string newPath = tempPath + CS.General_v3.Util.IO.GetFilenameAndExtension(s);

                CS.General_v3.Util.IO.CopyFile(s, newPath);

            }

            CreateZIPFileFromFolder(zipPath, tempPath);

            CS.General_v3.Util.IO.DeleteDirectory(tempPath);



        }

    }
}
