﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.DbObjects;
using CS.General_v3.Classes.NHibernateClasses.NHManager;
using NHibernate;
using System.Reflection;
using NHibernate.Criterion;

using System.Collections;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.NHibernateClasses.Transactions;


namespace CS.General_v3.Util
{
    public static class nHibernateUtil
    {
        public static MyTransaction BeginTransactionFromSessionInCurrentContext()
        {
            return GetCurrentSessionFromContext().BeginTransaction();
        }

        public static MyNHSessionBase GetCurrentSessionFromContext()
        {
            return NHClasses.NhManager.GetCurrentSessionFromContext();
        }
        public static MyNHSessionBase CreateNewSessionForCurrentContext()
        {
            return NHClasses.NhManager.CreateNewSessionForContextAndReturn();
        }
        public static MyNHSessionBase DisposeCurrentSessionInContext()
        {
            return NHClasses.NhManager.DisposeCurrentSessionInContext();
        }
        public static MyNHSessionBase DisposeCurrentSessionInContextAndCreateNewOne()
        {
            DisposeCurrentSessionInContext();
            return CreateNewSessionForCurrentContext();
        }

        public static Order GetRandomOrder()
        {

            return new CS.General_v3.Classes.NHibernateClasses.Classes.RandomOrder();
        }

        public static TData GetPropertyValueFromListOfStatesAndNames<TItem, TData>(System.Linq.Expressions.Expression<Func<TItem, TData>> selector, object[] states, object[] propertyNames)
        {
            PropertyInfo p = CS.General_v3.Util.ReflectionUtil<TItem>.GetPropertyBySelector(selector);
            string name = p.Name;
            object obj = GetPropertyValueFromListOfStatesAndNames(name, states, propertyNames);
            return (TData)obj;

        }

        public static object GetPropertyValueFromListOfStatesAndNames(string propertyName, object[] states, object[] propertyNames)
        {
            for (int i = 0; i < propertyNames.Length; i++)
            {
                if (propertyNames[i] != null)
                {
                    if (string.Compare(propertyNames[i].ToString(), propertyName, true) == 0)
                    {
                        return states[i];

                    }
                }
            }
            return null;
        }
        public static bool UpdatePropertyFromListOfStatesAndNames<T>(System.Linq.Expressions.Expression<Func<T, object>> selector , object[] states, object[] propertyNames, object newValue)
        {
            
            PropertyInfo pInfo = CS.General_v3.Util.ReflectionUtil<T>.GetPropertyBySelector(selector);
            return UpdatePropertyFromListOfStatesAndNames(pInfo.Name, states, propertyNames, newValue);
            
        }
       
        public static bool UpdatePropertyFromListOfStatesAndNames(string propertyName, object[] states, object[] propertyNames, object newValue)
        {
            
            for (int i = 0; i < propertyNames.Length; i++)
            {
                if (propertyNames[i] != null)
                {
                    if (string.Compare(propertyNames[i].ToString(), propertyName, true) == 0)
                    {
                        states[i] = newValue;
                        return true;
                        
                    }
                }
            }
            return false;

        }

        public static int GetCountFromCriteria(ICriteria crit)
        {
            int cnt = 0;
            try
            {

                var countCrit = (ICriteria)crit.Clone();

                countCrit.SetProjection(Projections.RowCount());


                cnt = countCrit.UniqueResult<int>();

            }
            catch (Exception ex)
            {
                int k = 5;
                throw;
            }
            return cnt;
        }

        public static int GetCountFromDetachedCriteria(DetachedCriteria crit)
        {
            int cnt = 0;
            try
            {
                var countCrit = crit.CloneDetachedCriteria();

                countCrit.SetProjection(Projections.RowCount());

                var nhSession = NHClasses.NhManager.GetCurrentSessionFromContext();
                var tmp = countCrit.GetExecutableCriteria(nhSession.GetAsISession());
                cnt = tmp.UniqueResult<int>();

            }
            catch (Exception ex)
            {
                int k = 5;
                throw;
            }
            return cnt;
        }
        public static DetachedCriteria CloneDetachedCriteria(this DetachedCriteria crit)
        {
            return CriteriaTransformer.Clone(crit);
        }
        public static IEnumerable<TItem> LimitQueryByPrimaryKeysAndReturnResultAndTotalCount<TItem>(this IQueryOver<TItem, TItem> mainQuery, 
             int pageNum, int pageSize, out int totalResults) where TItem : class
        {
            
            return LimitCriteriaByPrimaryKeysAndReturnResultAndTotalCount<TItem>(mainQuery.RootCriteria,  pageNum, pageSize, out totalResults);

        }
        
        public static IEnumerable<TItem> LimitCriteriaByPrimaryKeysAndReturnResultAndTotalCount<TItem>(this NHibernate.ICriteria criteria, 
             int pageNum, int pageSize, out int totalResults, bool useFutures = true) where TItem : class
        {

            string primaryKeyName = CS.General_v3.Util.ReflectionUtil<IBaseDbObject>.GetPropertyBySelector(item => item.ID).Name; ;
            
            if (pageSize <= 0) pageSize = Int32.MaxValue - 1;
            var nhSession = NHClasses.NhManager.GetCurrentSessionFromContext();
            var totalResultsCriteria = (ICriteria)criteria.Clone();
            //var resultCriteria = (ICriteria)criteria.Clone();
            //var pagingCriteria = (ICriteria)criteria.Clone();

            IList list = null;
            IList ids = null;
            totalResults = 0;

            var pKeyIDName = Projections.Property(primaryKeyName);
            var pKeyProjection = Projections.Distinct(pKeyIDName);


            
            
            totalResultsCriteria.SetProjection(Projections.CountDistinct(pKeyIDName.PropertyName));

            IEnumerable<int> oTotalResults = null;
            if (useFutures && nhSession is ISession)
                oTotalResults = totalResultsCriteria.Future<int>();
            else
                oTotalResults = totalResultsCriteria.List<int>();
            //totalResults = Convert.ToInt32(oTotalResults);
            
            var results = LimitCriteriaByPrimaryKeysAndReturnResult<TItem>(criteria,  pageNum, pageSize, useFutures);

            totalResults = oTotalResults.FirstOrDefault();

            return results;
   
            


        }

        /// <summary>
        /// Gets an assocation link criteria by path.  If it does not exist, it is created
        /// </summary>
        /// <param name="rootCriteria">The root criteria</param>
        /// <param name="projection">The projection, e.g ItemGroupBase.CategoyrLinks</param>
        /// <returns>Criteria</returns>
        public static ICriteria GetAssociationLinkCriteriaByPath(this ICriteria rootCriteria, IPropertyProjection projection)
        {
            var crit = rootCriteria.GetCriteriaByPath(projection.PropertyName);
            if (crit == null)
                crit = rootCriteria.CreateCriteria(projection.PropertyName);
            return crit;
        }
        /// <summary>
        /// Gets an assocation link criteria by path.  If it does not exist, it is created
        /// </summary>
        /// <param name="rootCriteria">The root criteria</param>
        /// <param name="projection">The projection, e.g ItemGroupBase.CategoyrLinks</param>
        /// <returns>Criteria</returns>
        public static ICriteria GetAssociationLinkCriteriaByPath(this ICriteria rootCriteria, params PropertyInfo[] properties)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var p in properties)
            {
                if (sb.Length > 0)
                    sb.Append(".");
                sb.Append(p.Name);
                
            }

            string path = sb.ToString();
            return GetAssociationLinkCriteriaByPath(rootCriteria, path);
        }
        /// <summary>
        /// Gets an assocation link criteria by path.  If it does not exist, it is created
        /// </summary>
        /// <param name="rootCriteria">The root criteria</param>
        /// <param name="projection">The projection, e.g ItemGroupBase.CategoyrLinks</param>
        /// <returns>Criteria</returns>
        public static ICriteria GetAssociationLinkCriteriaByPath(this ICriteria rootCriteria, string path)
        {
           
           
            var crit = rootCriteria.GetCriteriaByPath(path);
            if (crit == null)
                crit = rootCriteria.CreateCriteria(path);
            return crit;
        }
        /// <summary>
        /// Gets an assocation link criteria by path.  If it does not exist, it is created
        /// </summary>
        /// <param name="rootCriteria">The root criteria</param>
        /// <param name="projection">The projection, e.g ItemGroupBase.CategoyrLinks</param>
        /// <returns>Criteria</returns>
        public static ICriteria GetAssociationLinkCriteriaByAlias(this ICriteria rootCriteria, IPropertyProjection projection, string alias = null)
        {
            if (alias == null)
                alias = projection.PropertyName;

            var crit = rootCriteria.GetCriteriaByAlias(alias);
            if (crit == null)
                crit = rootCriteria.CreateCriteria(projection.PropertyName, alias);
            return crit;
        }

       /// <summary>
        /// Gets an assocation link criteria by path.  If it does not exist, it is created
       /// </summary>
        /// <typeparam name="TMain">The main type for which to start the association link</typeparam>
        /// <param name="rootCriteria">The root criteria</param>
        /// <param name="selector">Selector, e.g ItemGroupBase.CategoryLinks</param>
        /// <returns>Criteria</returns>
        public static ICriteria GetAssociationLinkCriteriaByPath<TMain>(this ICriteria rootCriteria, System.Linq.Expressions.Expression<Func<TMain, object>> selector)
            where TMain : IBaseDbObject
        {
            var p = Projections.Property<TMain>(selector);
            return GetAssociationLinkCriteriaByPath(rootCriteria, p);
        }

        public static string GetProjectionPathForAssociation<TMain, TSubItem>(System.Linq.Expressions.Expression<Func<TMain, object>> selectorMain, 
            System.Linq.Expressions.Expression<Func<TSubItem, object>> selectorSubItem)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Projections.Property(selectorMain).PropertyName);
            sb.Append(".");
            sb.Append(Projections.Property(selectorSubItem).PropertyName);
            return sb.ToString();
        }

        /// <summary>
        /// Gets an assocation link criteria by path.  If it does not exist, it is created
        /// </summary>
        /// <param name="rootCriteria">The root criteria</param>
        /// <param name="selector">Selector, e.g ItemGroupBase.CategoryLinks</param>
        /// <returns>Criteria</returns>
        public static ICriteria GetAssociationLinkCriteriaByAlias<T>(this ICriteria rootCriteria, System.Linq.Expressions.Expression<Func<T, object>> selector)
            where T : IBaseDbObject
        {
            var p = Projections.Property<T>(selector);
            return GetAssociationLinkCriteriaByAlias(rootCriteria, p);
        }

        /*public static IEnumerable<TItem> LimitCriteriaByPrimaryKeysAndReturnResult<TItem>(this NHibernate.ICriteria criteria,
           int pageNum, int pageSize) where TItem: class
        {
            return LimitCriteriaByPrimaryKeysAndReturnResult<TItem>(criteria, pageNum, pageSize);
        }*/

        //public static void LimitQueryByPrimaryKeys(this NHibernate.IQueryOver query, string primaryKeyName, int pageNum, int pageSize)
        //{
        //    LimitCriteriaByPrimaryKeys(query.UnderlyingCriteria, primaryKeyName, pageNum, pageSize);
        //}

        public static void LimitCriteriaByPrimaryKeys(this NHibernate.ICriteria criteria, int pageNum, int pageSize)
        {
            string primaryKeyName = CS.General_v3.Util.ReflectionUtil<IBaseDbObject>.GetPropertyBySelector(item => item.ID).Name; ;
            
            var session = NHClasses.NhManager.GetCurrentSessionFromContext();
            if (pageSize <= 0) pageSize = Int32.MaxValue - 1;
        //    var nhSession = NHClasses.NhManager.GetCurrentSessionFromContext();
            var pagingCriteria = (ICriteria)criteria.Clone();
            IList ids = null;
            var pKeyIDName = Projections.Property(primaryKeyName);  
            var pKeyProjection = Projections.Distinct(pKeyIDName); 
            {
                {
                    //paging
                    pagingCriteria.SetProjection(pKeyProjection); //sets the primary key distinct projection
                    if (pageSize > 0)
                    {

                        if (pageNum < 1)
                            pageNum = 1;
                        int skipAmt = (pageNum - 1) * pageSize;
                        pagingCriteria.SetFirstResult(skipAmt);
                        pagingCriteria.SetMaxResults(pageSize); 

                        ids = pagingCriteria.List(); //this returns the distinct list of IDs which should be returned for the given page & size

                    }
                }
            }
            {
                if (ids != null && ids.Count > 0)
                {
                    criteria.Add(Expression.In(pKeyIDName, ids));   //adds the primary key restriction
                    var crit = criteria;
                    crit.SetResultTransformer(new NHibernate.Transform.DistinctRootEntityResultTransformer());
                }
                else
                {
                    criteria.Add(Expression.Eq(pKeyIDName, 0)); //this is added specifically so that the main criteria returns NO results
                    criteria.Add(Expression.Eq(pKeyIDName, 1));
                }
            }
        }
        public static IEnumerable<TItem> LimitQueryByPrimaryKeysAndReturnResult<TItem>(this NHibernate.IQueryOver<TItem> query, 
           int pageNum, int pageSize) where TItem : class
        {
           return LimitCriteriaByPrimaryKeysAndReturnResult<TItem>(query.UnderlyingCriteria,  pageNum, pageSize);
        }
        public static IEnumerable<TItem> LimitQueryByPrimaryKeysAndReturnResult<TItem>(this NHibernate.IQueryOver<TItem, TItem> query, 
           int pageNum, int pageSize) where TItem : class
        {
            return LimitCriteriaByPrimaryKeysAndReturnResult<TItem>(query.UnderlyingCriteria,  pageNum, pageSize);
        }

        public static IEnumerable<TItem> LimitCriteriaByPrimaryKeysAndReturnResult<TItem>(this NHibernate.ICriteria criteria, 
           int pageNum, int pageSize, bool useFutures = true) where TItem:class
        {

            var session = NHClasses.NhManager.GetCurrentSessionFromContext();

            var resultsCriteria = (ICriteria)criteria.Clone();
            LimitCriteriaByPrimaryKeys(resultsCriteria,  pageNum, pageSize);
            

            IEnumerable<TItem> list = null;
            try
            {
                if (useFutures && session is ISession)
                    list = resultsCriteria.Future<TItem>();
                else
                    list = resultsCriteria.List<TItem>();


             
            }
            catch (Exception ex)
            {
                int k = 5;
                throw;
            }
            return list;


        }
        public static int __LimitQueryByPrimaryKeysAndGetTotalCount<TItem>(this QueryOver<TItem, TItem> mainQuery, QueryOver<TItem, TItem> newEmptyQuery,
            System.Linq.Expressions.Expression<Func<TItem, object>> pkeySelector,
             int pageNum, int pageSize)
        {

            var pKeyProjection = Projections.Property<TItem>(pkeySelector);
            //q = q.Fetch(item => item.AlbumDiscTracks).Eager;
            //q.DetachedCriteria.SetFetchMode("AlbumDiscTracks", FetchMode.Select);
            //q.DetachedCriteria.SetFetchMode("TrackPrices", FetchMode.Select);

            mainQuery.Select(pKeyProjection);


            //q.RootCriteria.SetResultTransformer(new NHibernate.Transform.DistinctRootEntityResultTransformer());




            newEmptyQuery.DetachedCriteria.Add(Subqueries.PropertyIn(pKeyProjection.PropertyName, mainQuery.DetachedCriteria));




            int totalResults = CS.General_v3.Util.nHibernateUtil.PageQueryAndGetTotalCount(newEmptyQuery, pageNum, pageSize);
            return totalResults;

        }
        public static int PageQueryAndGetTotalCount<TItem>(this QueryOver<TItem, TItem> q, int pageNum, int pageSize)
        {
            var qTmp = q.Clone();
            var clonedCrit = q.DetachedCriteria.CloneDetachedCriteria();
            int total = GetCountFromDetachedCriteria(clonedCrit);
            if (pageSize > 0)
            {
                if (pageNum < 1)
                    pageNum = 1;
                int skipAmt = (pageNum - 1) * pageSize;
                q.Skip(skipAmt).Take(pageSize);
            }
            return total;
        }


        /// <summary>
        /// Returns an OR-ed expression of all the criterions. Performs an expression for each item in criteriaList.
        /// </summary>
        /// <typeparam name="TSearchType"></typeparam>
        /// <typeparam name="TCriteriaType"></typeparam>
        /// <param name="criteriaList"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static ICriterion GetOrExpression<TSearchType, TCriteriaType>(IEnumerable<TCriteriaType> criteriaList, System.Linq.Expressions.Expression<Func<TSearchType, bool>> expression)
        {
            List<ICriterion> list = new List<ICriterion>();
            foreach (var crit in criteriaList)
            {
                list.Add(Expression.Where<TSearchType>(expression));
            }

            return GetOrExpression(list);
        }
        /// <summary>
        /// Returns an OR-ed expression of all the criterions
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public static ICriterion GetOrExpression(IEnumerable<ICriterion> criteria)
        {
            ICriterion result = null;
            if (criteria != null)
            {
                ICriterion lastCrit = null;

                var list = CS.General_v3.Util.ListUtil.GetListFromEnumerator(criteria);
                if (list.Count > 0)
                {
                    result = list[0];
                    for (int i = 1; i < list.Count; i++)
                    {
                        result = Expression.Or(lastCrit, list[i]);
                    }
                }
            }
            return result;
        }

        public static void DeleteAllItemsFromTableInBulk(Type type)
        {
            Type lastType = CS.General_v3.Util.ReflectionUtil.GetLastTypeInInheritanceChain(type, null);
            var session = NHClasses.NhManager.GetCurrentSessionFromContext();
            string tbName = lastType.Name;
            //tbName = tbName.Substring(0, tbName.Length - 4);//remove Base
            string SQL = "Delete from `" + tbName + "`;";
            var cmd = session.Connection.CreateCommand();
            cmd.CommandText = SQL;
            cmd.ExecuteNonQuery();
            
        }
        public static DetachedCriteria GetDetachedCriteriaForLastType(Type t)
        {
            Type lastType = CS.General_v3.Util.ReflectionUtil.GetLastTypeInInheritanceChain(t, null);
            return DetachedCriteria.For(lastType);
        }

        public static DetachedCriteria GetDetachedCriteriaForLastType<T>()
        {

            return GetDetachedCriteriaForLastType(typeof(T));

        }
        public static DetachedCriteria GetDetachedCriteriaForLastType(Type t, string aliasName )
        {
            Type lastType = CS.General_v3.Util.ReflectionUtil.GetLastTypeInInheritanceChain(t, null);
            return DetachedCriteria.For(lastType, aliasName);
        }

        public static DetachedCriteria GetDetachedCriteriaForLastType<T>(string aliasName )
        {
            
            return GetDetachedCriteriaForLastType(typeof(T), aliasName);
             
        }
    }
}
