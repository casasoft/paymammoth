﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Util
{
    public static class TreeUtil
    {

        private static void _getChildrenAndSubChildrenFromItem<T>(List<T> currentList, T item, Func<T, IEnumerable<T>> childrenDelegate)
        {
            currentList.Add(item);
            IEnumerable<T> children = childrenDelegate(item);
            if (children != null)
            {
                foreach (T child in children)
                {
                    if (!currentList.Contains(child))
                    {
                        _getChildrenAndSubChildrenFromItem(currentList, child, childrenDelegate);
                    }
                    else
                    {
                        int K = 5;
                    }
                }
            }
        }

        public static IEnumerable<T> GetChildrenAndSubChildrenFromItem<T>(T item, Func<T, IEnumerable<T>> childrenDelegate)
        {
            return GetChildrenAndSubChildrenFromItem(CS.General_v3.Util.ListUtil.GetListFromSingleItem(item), childrenDelegate);
        }
        public static IEnumerable<T> GetChildrenAndSubChildrenFromItem<T>(IEnumerable<T> items, Func<T, IEnumerable<T>> childrenDelegate)
        {
            List<T> list = new List<T>();
            foreach (var item in items)
            {
                _getChildrenAndSubChildrenFromItem<T>(list, item, childrenDelegate);
            }
            return list;

        }

        public static IEnumerable<T> GetParentHierarchy<T>(T item, Func<T, T> parentDelegate) where T : class
        {
            List<T> list = new List<T>();
            while (parentDelegate(item) != null)
            {
                item = parentDelegate(item);
                list.Add(item);
            }
            return list;
        }

    }
}
