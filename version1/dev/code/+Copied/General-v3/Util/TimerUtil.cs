﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using CS.General_v3.Classes.Timers;

namespace CS.General_v3.Util
{
    public class TimerUtil
    {
       
        /// <summary>
        /// Calls a method at the specified date time.
        /// </summary>
        /// <param name="callDate"></param>
        /// <param name="methodToCall"></param>
        /// <returns></returns>
        public static SpecificDateTimer CallMethodAtSpecifiedDateTime(DateTime callDate, SpecificDateTimer.CallDelegate methodToCall)
        {
            
            SpecificDateTimer tmr = new SpecificDateTimer(callDate);
            tmr.MethodToCall += methodToCall;
            tmr.Start();
            return tmr;
        }
    }
}
