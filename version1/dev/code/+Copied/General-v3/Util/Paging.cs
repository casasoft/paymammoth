using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Web.UI.WebControls;
using System.Collections.Specialized;

namespace CS.General_v3.Util
{
    public static class Paging
    {
        /// <summary>
        /// Splits a list, and gets the current page.  Returns NULL if list is non existant
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static IList<T> SplitList<T>(IList<T> list, int pageNo, int pageSize)
        {
            List<T> newList = new List<T>();
            newList.AddRange(list.Skip((pageNo - 1) * pageSize).Take(pageSize));
            return newList;

            //int from, to,count;
            //if (pageSize > 0)
            //{
            //    from = (pageNo - 1) * pageSize;
            //    to = from + pageSize;
            //    if (to > list.Count)
            //        to = list.Count;
            //    count = to - from;
            //    if (from < list.Count)
            //    {
            //        return list.GetRange(from, count);
            //    }
            //    else
            //    {
            //        return new List<T>();
            //    }
            //}
            //else
            //{
            //    return list;
            //}
            ///*for (int i = from; i < to; i++)
            //{
            //    tmpList.Add(list[i]);
            //}
            //return tmpList;*/

        }
        /// <summary>
        /// Returns the total amount of pages
        /// </summary>
        /// <param name="count"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static int TotalPages(int count, int pageSize)
        {
            if (pageSize > 0)
            {
                double tmp = (double)count / (double)pageSize;
                tmp += (double)0.99;
                tmp = Math.Truncate(tmp);
                return (int)tmp;
            }
            else
            {
                if (count > 0)
                    return 1;
                else
                    return 0;
            }

        }
        /// <summary>
        /// Returns the total amount of pages
        /// </summary>
        /// <param name="count"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static int TotalPages(ICollection list, int pageSize)
        {
            return TotalPages(list.Count, pageSize);

        }
        /// <summary>
        /// 
        /// 
        /// </summary>
        /// <param name="count"></param>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static int GetFromIndex(int count, int pageNo, int pageSize)
        {
            int tmp = 0;
            if (count > 0)
            {
                tmp = ((pageNo - 1) * pageSize) + 1;
                if (tmp > count)
                {
                    tmp = count;
                }
            }
                
            return tmp;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="count"></param>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static int GetToIndex(int count, int pageNo, int pageSize)
        {
            int tmp = 0;
            if (count > 0)
            {
                tmp = GetFromIndex(count, pageNo, pageSize);
                tmp += (pageSize-1);
                if (tmp > count)
                    tmp = count;
            }
            return tmp;

        }
        /// <summary>
        /// Returns the from an to index of the pages.  From starts from 1
        /// </summary>
        /// <param name="count"></param>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        public static void GetFromToIndices(int count, int pageNo, int pageSize, out int from, out int to)
        {
            from = GetFromIndex(count, pageNo, pageSize);
            to = GetToIndex(count, pageNo, pageSize);
        }

        /// <summary>
        /// Returns various paging information
        /// </summary>
        /// <param name="pgSize">Page Size</param>
        /// <param name="currPage">Current Page Number (starting from 1)</param>
        /// <param name="totalItems"></param>
        /// <param name="totalPagesToShow">Total Pages to show, for showing the pages.  For example, to show 4 5 [6] 7 8, you would enter 5.  The results would be
        /// a page from index of '4' and a page end index of '8'.  It will also take into account if it is near end or near start</param>
        /// <param name="fromIndex">From index of listing, for use in From XX to YY of ZZ</param>
        /// <param name="toIndex">To index of listing, for use in From XX to YY of ZZ</param>
        /// <param name="totalPages">Total pages in listing</param>
        /// <param name="FromPageIndex">The from page index, to be used for showing pages</param>
        /// <param name="ToPageIndex">The to page index, to be used for showing pages</param>
        public static void GetPagingInfo(int pgSize, int currPage, int totalItems, int totalPagesToShow, out int fromIndex, out int toIndex, out int totalPages,
            out int FromPageIndex, out int ToPageIndex)
        {
            if (totalPagesToShow % 2 == 0)
                totalPagesToShow--;
            fromIndex= ((currPage - 1) * pgSize) + 1;
            toIndex = (currPage * pgSize);
            if (fromIndex > totalItems)
                fromIndex = totalItems;
            if (toIndex > totalItems)
                toIndex = totalItems;

            currPage = (fromIndex + pgSize - 1) / pgSize;
            totalPages = (totalItems + pgSize - 1) / pgSize;


            //render pages
            FromPageIndex= currPage - ((totalPagesToShow - 1) / 2);
            ToPageIndex= currPage + ((totalPagesToShow - 1) / 2);
            if (FromPageIndex < 1)
                FromPageIndex = 1;
            if (ToPageIndex > totalPages)
                ToPageIndex = totalPages;
            FromPageIndex -= (totalPagesToShow - (ToPageIndex - FromPageIndex + 1));
            if (FromPageIndex < 1)
                FromPageIndex = 1;
            ToPageIndex += (totalPagesToShow - (ToPageIndex - FromPageIndex + 1));
            if (ToPageIndex > totalPages)
                ToPageIndex = totalPages;
        }

    }
}
