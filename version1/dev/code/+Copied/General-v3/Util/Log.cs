using System;
using System.Collections.Generic;
using System.Text;
using System.IO;


namespace CS.General_v3.Util
{
    public static class Log
    {
        //local
        //public static string path = "D:\\Work\\Clients\\businesses.com.mt\\Site\\log.txt";

        //online
        public static string path = "D:\\websites\\businesses.com.mt\\log.txt";

        private static string getTimestamp()
        {
            string str = CS.General_v3.Util.Date.Now.ToString("dd-MM-yyyy hh:mm:sstt") + " - ";
            return str;
        }

        public static void WriteToLog(string line,bool addTimestamp)
        {
            FileStream fs = null;
            StreamWriter sw = null;
            try
            {
                fs = new FileStream(path, FileMode.Append, FileAccess.Write);
                sw = new StreamWriter(fs);
                string str = "";
                if (addTimestamp)
                    str += getTimestamp();
                str += line;
                sw.Write(str);
                sw.Close();
                sw.Dispose();
                fs.Close();
                fs.Dispose();
            }
            catch 
            {

            }
            finally
            {
                if (fs != null)
                    fs.Dispose();
                if (sw != null)
                    sw.Dispose();
            }
        }
        public static void WriteLine(string line, bool addTimestamp)
        {
            WriteToLog(line + "\r\n",addTimestamp);
        }
        public static void Write(string str, bool addTimestamp)
        {

            WriteToLog(str, addTimestamp);

        }
        public static void LogStartTime(string Description)
        {
            LogStartTime(Description, 0);
        }
        public static void LogEndTime(string Description)
        {
            LogEndTime(Description, 0);
        }
        public static void LogStartTime(string Description, int ID)
        {
            long currMicroSec = CS.General_v3.Util.Date.Now.Ticks * 100 / 1000;
            long currMS = currMicroSec / 1000;
            string str = Description + " [Started @ " + currMicroSec.ToString("0,0") + "micro secs (" + currMS.ToString("0,0") + "ms)]";
            WriteLine(str, true);
        }
        public static void LogEndTime(string Description, int ID)
        {
            long totMicroSec = CS.General_v3.Util.Stopwatch.EndTimer(ID) / 1000;
            long totMS = totMicroSec / 1000;
            string str = Description + " [Finished @ " + totMicroSec.ToString("0,0") + "micro secs (" + totMS.ToString("0,0") + "ms)]";
            WriteLine(str, true);
        }

    }
}
