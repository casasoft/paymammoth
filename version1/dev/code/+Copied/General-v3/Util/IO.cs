using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Runtime.Serialization.Formatters.Binary;
namespace CS.General_v3.Util
{
    public static class IO
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="path">Server.MapPath</param>
        /// <param name="methodToCallOnChange"></param>
        /// <param name="watchAttributes">Whether to watch the actual attributes of files and folders</param>
        /// <param name="includeSubDirectories"></param>
        /// <returns></returns>
        public static FileSystemWatcher WatchFileForChanges(string path, FileSystemEventHandler methodToCallOnChange,
            bool watchAttributes = false , bool includeSubDirectories = true)
        {

            if (methodToCallOnChange == null)
                throw new InvalidOperationException("Util.IO.WatchFile:: methodToCallOnChange cannot be null");

            string fileName = GetFilenameAndExtension(path);
            string dirName = GetDirName(path);

            FileSystemWatcher fileWatcher = new FileSystemWatcher(dirName);

            NotifyFilters filters = NotifyFilters.Size | NotifyFilters.LastWrite | NotifyFilters.LastAccess | NotifyFilters.FileName ;
            if (watchAttributes) filters = filters | NotifyFilters.Attributes;

            fileWatcher.Filter = fileName;
            fileWatcher.NotifyFilter = filters;
            fileWatcher.IncludeSubdirectories = includeSubDirectories;
            fileWatcher.Changed += methodToCallOnChange;
            fileWatcher.EnableRaisingEvents = true;
            
            return fileWatcher;
        }
        public static string GetMIMETypeFromFilename(string filename)
        {
            return Data.GetMIMEContentType(filename);
        }
        public static void StopFileWatcher(FileSystemWatcher fileWatch)
        {
            fileWatch.EnableRaisingEvents = false;
            fileWatch.Dispose();
        }

        static void fileWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Moves file from A to B, and overwrites any existing files.  No errors are given
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        public static void MoveFile(string from, string to)
        {
            if (from.Contains("/")) from = CS.General_v3.Util.PageUtil.MapPath(from);
            if (to.Contains("/")) to = CS.General_v3.Util.PageUtil.MapPath(to);
            CS.General_v3.Util.IO.CreateDirectory(to);
            if (File.Exists(from))
            {
                DeleteFile(to);
                File.Move(from, to);
            }
            
            
        }
        public static string EnsurePathEndsWithSlash(string path, string slashType = null)
        {


            if (!string.IsNullOrEmpty(path))
            {
                if (slashType == null)
                {
                    if (path.Contains("/"))
                        slashType = "/";
                    else
                        slashType = "\\";
                }

                if (!path.EndsWith(slashType))
                    path += slashType;
            }
            return path;
        }
        public static string EnsurePathStartsAndEndsWithSlash(string path, string slashType = null)
        {
            string s = EnsurePathStartsWithSlash(path, slashType);
            s = EnsurePathEndsWithSlash(path, slashType);
            return s;
        }
        public static string EnsurePathDoesNotStartWithSlash(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {

                if (path.StartsWith("\\") || path.StartsWith("/"))
                    path = path.Remove(0, 1);
            }
            return path;
        }
        public static string EnsurePathDoesNotEndWithSlash(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {

                if (path.EndsWith("\\") || path.EndsWith("/"))
                    path = path.Substring(0, path.Length -1);
            }
            return path;
        }
        public static string EnsurePathDoesNotStartButEndsWithSlash(string path, string slashType = null)
        {
            
            path = EnsurePathDoesNotStartWithSlash(path);
            path = EnsurePathEndsWithSlash(path, slashType);
            
            return path;
        }
        public static string EnsurePathStartsWithSlash(string path, string slashType = null)
        {


            if (!string.IsNullOrEmpty(path))
            {
                if (slashType == null)
                {
                    if (path.Contains("/"))
                        slashType = "/";
                    else
                        slashType = "\\";
                }

                if (!path.StartsWith(slashType))
                    path += slashType;
            }
            return path;
        }

        public static System.Text.Encoding GetFileTextEncoding(byte[] bom)
        {
            System.Text.Encoding enc = null;
            if (bom.Length >= 3 && bom[0] == 0xef && bom[1] == 0xbb && bom[2] == 0xbf)
            {
                enc = System.Text.Encoding.UTF8;
            }
            else if (bom.Length >= 2 && bom[0] == 0xff && bom[1] == 0xfe)
            {
                enc = System.Text.Encoding.Unicode;
            }
            else if (bom.Length >= 2 && bom[0] == 0xfe && bom[1] == 0xff)
            {
                enc = System.Text.Encoding.Unicode;
            }
            else if ((bom.Length >= 4 && bom[0] == 0 && bom[1] == 0 && bom[2] == 0xfe && bom[3] == 0xff))
            {
                enc = System.Text.Encoding.Unicode;
            }
            else
            {
                enc = System.Text.Encoding.UTF8;
            }
            return enc;

        }


        /// <summary>
        /// Retrieves files from dir based on extensions.  
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="extensions">Extensions can include even separators like ',', ';','|'</param>
        /// <returns></returns>
        public static List<FileInfo> GetFilesInDirByExtensions(string dir, params string[] extensions)
        {
            return GetFilesInDirByExtensions(new DirectoryInfo(dir), extensions);
        }
        /// <summary>
        /// Retrieves files from dir based on extensions.  
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="extensions">Extensions can include even separators like ',', ';','|'</param>
        /// <returns></returns>
        public static List<FileInfo> GetFilesInDirByExtensions(DirectoryInfo dir, params string[] extensions)
        {
            string pattern = @".*?\.(";

            for (int i=0; i < extensions.Length;i++)
            {
                var extensionStr = extensions[i];
                string[] tokens = extensionStr.Split(new string[] {",",";","|"}, StringSplitOptions.RemoveEmptyEntries);
                foreach (var ext in tokens)
                {
                    if (!string.IsNullOrEmpty(pattern))
                        pattern += "|";
                    pattern += ext.Trim();
                }
            }
            pattern += ")";

            var files = FindFilesInFolder(dir.FullName, pattern, true, true);
            return files;

        }
        public static List<FileInfo> GetFilesInDirByRegExp(string dir, string regExp, RegexOptions regexOptions)
        {
            return GetFilesInDirByRegExp(new DirectoryInfo(dir),regExp,regexOptions);
        }
        public static List<FileInfo> GetFilesInDirByRegExp(DirectoryInfo dir, string regExp, RegexOptions regexOptions)
        {
            return FindFilesInFolder(dir.FullName, regExp, true, true);
        }
        public static string ConvertByteArrayToHex(byte[] byArray)
        {
            StringBuilder hex = new StringBuilder(byArray.Length * 2);
            foreach (byte b in byArray)
            {
                hex.AppendFormat("{0:x2}", b);
            }
            return hex.ToString();
        }
        public static byte[] ConvertHexStringToByteArray(string hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        public static string ConvertStringToFilename(string s)
        {
            return ConvertStringToFilename(s, 999);
        }
        public static string ConvertStringToFilename(string s,int maxLen)
        {
            
            if (s == null)
                s = "";
            if (s.Length > maxLen)
                s = s.Substring(0, maxLen);
            s = CS.General_v3.Util.Text.ReplaceTexts(s, "-and-", "&");
            s = CS.General_v3.Util.Text.ReplaceTexts(s, "-", " ",".....","....","...","..", "+", "\\", "/", ":", "*", "?", "\"", "<", ">", "|","#","%");
            s = CS.General_v3.Util.Text.ReplaceTexts(s, "-", "--", "---", "----", "-----");
            s = CS.General_v3.Util.Text.ReplaceTexts(s, "-", "--", "---", "----", "-----");
            s = CS.General_v3.Util.Text.ReplaceTexts(s, "-", "--", "---", "----", "-----");
            while (s.StartsWith("."))
            {
                s = s.Remove(0, 1);
            }

            return s;
        }
        public static long GetFileSizeInBytes(string path)
        {
            long size = 0;
            if (File.Exists(path))
            {
                FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
                size = fs.Length;
                fs.Dispose();
            }
            return size;

        }
        public static int GetFileSizeInKB(string path)
        {
            long bytes = GetFileSizeInBytes(path);
            long kb = bytes / 1024;
            return (int)kb;
            

        }
        /// <summary>
        /// Empties a directory, recursively
        /// </summary>
        /// <param name="path"></param>
        public static void EmptyDirectory(string path)
        {
            EmptyDirectory(path, true);
        }

        /// <summary>
        /// Empty Directory
        /// </summary>
        /// <param name="path"></param>
        /// <param name="recursive"></param>
        public static void EmptyDirectory(string path, bool recursive)
        {

            DirectoryInfo dir = new DirectoryInfo(path);
            FileInfo[] files = dir.GetFiles();
            for (int i = 0; i < files.Length; i++)
            {
                files[i].Delete();
            }
            
            if (recursive)
            {
                DirectoryInfo[] dirs = dir.GetDirectories();
                foreach (var d in dirs)
                {
                    EmptyDirectory(d.FullName, true);
                }
            }

        }
        /// <summary>
        /// Deletes a file, without any errors
        /// </summary>
        /// <param name="path"></param>
        public static void DeleteFile(string path)
        {
            if (path.Contains("/"))
                path = CS.General_v3.Util.PageUtil.MapPath(path);
            try
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
            }
            catch
            {

            }
            
        }
        /// <summary>
        /// Deletes a directory, recursively
        /// </summary>
        /// <param name="path"></param>
        public static void DeleteDirectory(string path)
        {
            DeleteDirectory(path, true);
        }
        public static void DeleteDirectory(string path, bool recursive)
        {
            DirectoryInfo dir = new DirectoryInfo(path);
            if (recursive)
            {
                DirectoryInfo[] dirs = dir.GetDirectories();
                foreach (var d in dirs)
                {
                    DeleteDirectory(d.FullName, true);
                }
            }
            EmptyDirectory(path, recursive);
            System.IO.Directory.Delete(path);
            

        }
        /// <summary>
        /// Returns the contents of the specified file.  File must be a full path.
        /// </summary>
        /// <param name="filename">Filename (Full path, using Server.MapPath)</param>
        /// <returns>The contents of the file</returns>
        public static string LoadFile(string filename)
        {
            return LoadFile(filename, System.Text.Encoding.UTF8);

        }
        /// <summary>
        /// Returns the contents of the specified file.  File must be a full path.
        /// </summary>
        /// <param name="filename">Filename (Full path, using Server.MapPath)</param>
        /// <returns>The contents of the file</returns>
        public static string LoadFile(string filename, System.Text.Encoding encoding)
        {
            if (File.Exists(filename))
            {
                FileStream file = new FileStream(filename, FileMode.Open, FileAccess.Read);
                StreamReader sr = new StreamReader(file,encoding);
                string tmp = sr.ReadToEnd();
                sr.Close();
                file.Close();
                file.Dispose();
                return (tmp);
            }
            else
                return null;

        }
        /// <summary>
        /// Creates a temporary dir and returns the path. Guarantees uniqueness
        /// </summary>
        /// <returns></returns>
        public static string CreateTempDir()
        {
            string tmpPath = System.IO.Path.GetTempPath();
            string folder;
            while (true)
            {
                folder = tmpPath + CS.General_v3.Util.Random.GetGUID(true)  + "_" + CS.General_v3.Util.Random.GetString(5, 5) + "\\";
                if (!System.IO.Directory.Exists(folder))
                {
                    CreateDirectory(folder);
                    break;
                }
            }
            return folder;
        }
        /// <summary>
        /// Creates a temporary dir and returns the path. Guarantees uniqueness
        /// </summary>
        /// <returns></returns>
        public static string GetTempFilename(string extension)
        {
            if (extension.StartsWith(".")) extension = extension.Substring(1);
            string tmpPath = System.IO.Path.GetTempPath();
            string path= "";
            while (true)
            {

                string filename = CS.General_v3.Util.Random.GetString(70, 70) + "." + extension;
                path = tmpPath + filename;
                if (!System.IO.File.Exists(path))
                {
                    SaveToFile(path, " ");
                    break;
                }
                
            }
            return path;
        }
        /// <summary>
        /// Returns the directory name of a specified path.  For example:
        /// 
        /// C:\Karl\Test\1.jpg returns "C:\Karl\Test\"
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static string GetDirName(string filename)
        {
            int indexOf = filename.LastIndexOf('\\');
            if (indexOf == -1)
                indexOf = filename.LastIndexOf('/');
            if (indexOf != -1)
                indexOf++;
            if (indexOf > -1)
                return (filename.Substring(0, indexOf));
            else
                return null;
        }
        /// <summary>
        /// Returns ONLY the filename (excluding extension)
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string GetFilenameOnly(string path)
        {
            if (path != null)
            {
                path = GetFilenameAndExtension(path);
                int dotPos = path.LastIndexOf('.');
                if (dotPos != -1)
                {
                    path = path.Substring(0, dotPos);
                }
            }
            return path;
        }
        /// <summary>
        /// Returns ONLY the filename (excluding extension)
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string GetFilenameAndExtension(string path)
        {
            if (path != null)
            {
                path = path.Replace("/", "\\");
                if (path == null) path = "";
                int slashPos = path.LastIndexOf("\\");
                if (slashPos != -1)
                    path = path.Substring(slashPos + 1);
            }
            return path;
        }
        public static string ChangeExtension(string path, string newExtension)
        {
            string s = GetDirName(path) + GetFilenameOnly(path) + "." + newExtension;
            return s;
        }
        /// <summary>
        /// Returns the extension of the file, example 'photo.jpg' returns 'jpg'
        /// </summary>
        /// <param name="filename">Filename</param>
        /// <returns>Extension only (without dot)</returns>
        public static string GetExtension(string filename)
        {
            if (filename == null) filename = "";
            int pos;
            pos = filename.LastIndexOf(".");
            string ext = "";
            if (pos > -1)
                ext = (filename.Substring(pos + 1));
            else
                ext = filename;
            return ext.ToLower();

        }

        public static Enums.MEDIA_ITEM_TYPE GetFileExtensionType(string filename)
        {
            var extension = GetExtension(filename);
            if(extension == "jpg" || extension == "png" || extension == "gif" || extension == "bmp")
            {
                return Enums.MEDIA_ITEM_TYPE.Image;
            }
            if (extension == "doc" || extension == "docx")
            {
                return Enums.MEDIA_ITEM_TYPE.Document;
            }
            if (extension == "wmv" || extension == "mov" || extension == "mp4" || extension == "flv")
            {
                return Enums.MEDIA_ITEM_TYPE.Video;
            }
            if (extension == "mp3" || extension == "wav" || extension == "mp3a" || extension == "wma")
            {
                return Enums.MEDIA_ITEM_TYPE.Music;
            }
            if (extension == "xls" || extension == "xlsx")
            {
                return Enums.MEDIA_ITEM_TYPE.Excel;
            }
            if (extension == "ppt" || extension == "pptx")
            {
                return Enums.MEDIA_ITEM_TYPE.Powerpoint;
            }
            if (extension == "pdf")
            {
                return Enums.MEDIA_ITEM_TYPE.PDF;
            }
            else
            {
                return Enums.MEDIA_ITEM_TYPE.General;
            }

        }
        /// <summary>
        /// Saves to file, appends if exists
        /// </summary>
        /// <param name="path"></param>
        /// <param name="txt"></param>
        public static void AppendToFile(string path, string txt)
        {
            string dir = path.Substring(0, path.LastIndexOf('\\') + 1);
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            FileStream fs = new FileStream(path, FileMode.Append);
            StreamWriter sw = new StreamWriter(fs);
            sw.Write(txt);
            sw.Close();
            fs.Close();

        }
        /// <summary>
        /// Saves to file. Creates a new one
        /// </summary>
        /// <param name="path"></param>
        /// <param name="txt"></param>
        public static void SaveToFile(string path, string txt)
        {
            path = path.Replace("/", "\\");


            string dir = path.Substring(0, path.LastIndexOf('\\') + 1);
            if (!string.IsNullOrEmpty(dir) && !Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            FileStream fs = new FileStream(path, FileMode.Create);
            StreamWriter sw = new StreamWriter(fs);
            sw.Write(txt);
            sw.Close();
            fs.Close();

        }
        public static void SaveToFile(string path, byte[] file)
        {
            string dir = path.Substring(0, path.LastIndexOf('\\') + 1);
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            FileStream fs = new FileStream(path, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs);
            bw.Write(file);
            bw.Close();
            fs.Close();

        }
        public static byte[] LoadFileAsByteArray(string path)
        {
            ArrayList content = new ArrayList();
            FileStream reader = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            int cnt = 0;
            int tot = 0;
            byte[] buffer;
            do
            {
                buffer = new byte[8192];
                cnt = reader.Read(buffer, 0, buffer.Length);
                tot += cnt;
                content.Add(buffer);

            } while (cnt == buffer.Length);
            byte[] all = new byte[tot];
            int k = 0;
            for (int i = 0; i < content.Count; i++)
            {
                if (tot == 0) break;
                byte[] currBuffer = (byte[])content[i];
                for (int j = 0; j < currBuffer.Length; j++)
                {
                    all[k] = currBuffer[j];
                    k++;
                    tot--;
                    if (tot == 0) break;
                }
                currBuffer = null;
            }
            content.Clear();
            reader.Close();

            return all;
        }
        public static byte[] ReadStreamIntoByteArray(Stream s)
        {
            MemoryStream ms = new MemoryStream();
            byte[] buffer = new byte[512 * 1024];
            int readCnt = 0;
            do
            {
                readCnt = s.Read(buffer, 0, buffer.Length);
                if (readCnt > 0)
                {
                    ms.Write(buffer, 0, readCnt);
                }
            }
            while (readCnt > 0);
            byte[] data = new byte[ms.Length];
            ms.Seek(0, SeekOrigin.Begin);
            ms.Read(data,0,(int)data.Length);
            return data;

        }

        /// <summary>
        /// Saves a stream to a file. Overwrites any existing file
        /// </summary>
        /// <param name="inStream">Stream</param>
        /// <param name="path">Path of file</param>
        public static void SaveStreamToFile(Stream inStream, string path)
        {
            Directory.CreateDirectory(CS.General_v3.Util.IO.GetDirName(path));
            FileStream tmpFile = File.Open(path, FileMode.Create, FileAccess.Write);
            //StreamWriter writer = new StreamWriter(tmpFile);
            //StreamReader reader = new StreamReader(inStream);
            byte[] buffer = new byte[32768];
            inStream.Seek(0, SeekOrigin.Begin);
            while (inStream.Position < inStream.Length)
            {
                int cnt = inStream.Read(buffer, 0, buffer.Length);
                tmpFile.Write(buffer, 0, cnt);
            }
            tmpFile.Close();
            tmpFile.Dispose();
        }

        /// <summary>
        /// Creates a directory, from a path that may also include a filename.  E.g 'c:\test\floor.jpg'
        /// </summary>
        /// <param name="dirPath">The path</param>
        public static void CreateDirectory(string dirPath)
        {
            int lastSlash = NormaliseToLocalPath( dirPath).LastIndexOf("\\");
            if (lastSlash > -1)
            {
                int dotPos = dirPath.IndexOf(".", lastSlash);
                if (dotPos > -1)
                    dirPath = dirPath.Substring(0, lastSlash + 1);
                if (!Directory.Exists(dirPath))
                    Directory.CreateDirectory(dirPath);
            }


        }
        public static string GetStringFromStream(Stream s)
        {
            return GetStringFromStream(s, System.Text.Encoding.UTF7);
        }
        public static string GetStringFromStream(Stream s, System.Text.Encoding encoding)
        {
            StreamReader sr = new StreamReader(s, encoding,true);
            string str = sr.ReadToEnd();
            sr.Close();
            return str;
        }
        private static void findFilesInFolder(DirectoryInfo dir, string filename, bool filenameIncludesRegExp, bool recursive, List<FileInfo> files)
        {
            FileInfo[] dirFiles = dir.GetFiles();
            for (int i = 0; i < dirFiles.Length; i++)
            {
                string currFile = dirFiles[i].Name;
                string regExp = "^" + (filenameIncludesRegExp ? filename : CS.General_v3.Util.Text.ForRegExp(filename) + "$");
                var match = Regex.Match(currFile,regExp, RegexOptions.IgnoreCase);
                if (match.Success)
                {
                    files.Add(dirFiles[i]);
                }
            }
            if (recursive)
            {
                DirectoryInfo[] dirsInDir = dir.GetDirectories();
                foreach( var d in dirsInDir)
                {
                    findFilesInFolder(d,filename,filenameIncludesRegExp,recursive,files);
                }
            }

        }
        public static List<FileInfo> FindFilesInFolder(string directory, string filename, bool filenameIncludesRegExp, bool recursive)
        {
            DirectoryInfo dir = new DirectoryInfo(directory);
            List<FileInfo> list = new List<FileInfo>();
            findFilesInFolder(dir, filename, filenameIncludesRegExp, recursive, list); 
            return list;
        }
        public static System.Text.Encoding GetEncodingBasedOnFilename(string filename)
        {
            System.Text.Encoding enc = null;
            string ext = GetExtension(filename);
            switch (ext)
            {
                case "csv": enc = System.Text.Encoding.UTF7; break;
                    
            }
            return enc;
        }
        public static void CopyDirectoryTo(string fromDir, string toDir)
        {
            if (!fromDir.EndsWith("\\")) fromDir += "\\";
            if (!toDir.EndsWith("\\")) toDir += "\\";
            if (Directory.Exists(fromDir))
            {
                DirectoryInfo fromDirInfo = new DirectoryInfo(fromDir);
                CreateDirectory(toDir);
                FileInfo[] files = fromDirInfo.GetFiles();
                for (int i = 0; i < files.Length; i++)
                {
                    string toPath = toDir + files[i].Name;
                    try
                    {
                        files[i].CopyTo(toPath);
                    }
                    catch { }
                }
                DirectoryInfo[] subDirs = fromDirInfo.GetDirectories();
                for (int i = 0; i < subDirs.Length; i++)
                {
                    CopyDirectoryTo(subDirs[i].FullName, toDir + subDirs[i].Name);
                }


            }
        }
        public static void MoveToOrCopyDirectory(string fromDir, string toDir)
        {
            if (Directory.Exists(toDir)) DeleteDirectory(toDir);
            if (Directory.Exists(fromDir))
            {
                DirectoryInfo fromDirInfo = new DirectoryInfo(fromDir);
                bool ok = false;
                try
                {
                    fromDirInfo.MoveTo(toDir);
                    ok = true;
                }
                catch
                {
                    ok = false;
                }
                if (!ok)
                {
                    CopyDirectoryTo(fromDir, toDir);
                }


            }
        }
        public static string GetApplicationRootLocalFolder(bool returnCustomFolderIfFilled = true)
        {
           // CS.General_v3.Util.ApplicationUtil.ApplicationPath
            string rootPath = "";
            if (!string.IsNullOrEmpty(CS.General_v3.Settings.Others.LocalRootFolderCustom) && returnCustomFolderIfFilled)
            {
                rootPath = CS.General_v3.Settings.Others.LocalRootFolderCustom;
            }
            else if (CS.General_v3.Util.Other.IsWebApplication)
            {
                rootPath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
            }
            else
            {
                rootPath = Directory.GetCurrentDirectory();
                //Karl changed this to the above on 29/09/2010 as it does not work for test environments, as executable is in the \ProgramFiles folder not relateive to project
                //rootPath = System.Windows.Forms.Application.ExecutablePath;
                //rootPath = rootPath.Substring(0, rootPath.LastIndexOf(@"\") + 1);
            }
            if (!rootPath.EndsWith("\\")) rootPath += "\\";
            return rootPath;
        }
        /// <summary>
        /// Returns the web path of directory and filename.
        /// 
        /// E.g. http://www.domain.com/directory/filename
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string GetWebPath(string directory, string fileName)
        {
            directory = RemoveDuplicateSlashesFromPath(directory).Replace("\\", "/");
            fileName = RemoveDuplicateSlashesFromPath(fileName).Replace("\\", "/");
            if (fileName.IndexOf("/") != -1)
            {
                fileName = fileName.Substring(fileName.LastIndexOf("/") + 1);
            }
            string domain = CS.General_v3.Util.PageUtil.GetApplicationBaseUrl().GetURL(fullyQualified: true, includeScheme: false, appendQueryString: false);
            if (!domain.EndsWith("/"))
            {
                domain += "/";
            }
            if (directory.StartsWith("/"))
            {
                directory = directory.Remove(0, 1);
            }
            if (!directory.EndsWith("/"))
            {
                directory += "/";
            }
            return domain + directory + fileName;
        }

        public static string ConvertPathToShorterPathWithEllipses(string path, int charsToShowOnLeft, int charsToShowOnRight)
        {
            string txt = "";
            if (path.Length <= (charsToShowOnLeft + charsToShowOnRight))
                txt =path;
            else
            {
                txt = path.Substring(0, charsToShowOnLeft) + "..." +
                    path.Substring(path.Length - charsToShowOnRight);
            }
            return txt;
        }

        /// <summary>
        /// Converts a relative path to an absolute path, relative from a location.  E.g, ..\karl\test.xml from c:\work\data would result in c:\work\karl\test.xml
        /// </summary>
        /// <param name="relPath">Relative path to convert to an absolute path</param>
        /// <param name="relativeFrom">Relative location to resolve path from. If path is null, it is taken from application path</param>
        /// <returns>Absolute path</returns>
        public static string ConvertRelativePathToAbsolute(string relPath, string relativeFrom = null)
        {
            if (relativeFrom == null)
                relativeFrom = CS.General_v3.Util.ApplicationUtil.ApplicationPath;

            relPath = relPath ?? "";
            string origSlashChar = "\\";
            if (relativeFrom.Contains("/")) origSlashChar = "/";

            if (origSlashChar == "/")
            {
                relPath = relPath.Replace("\\", "/");
                relativeFrom = relativeFrom.Replace("\\", "/");
            }
            else
            {
                relPath = relPath.Replace("/", "\\");
                relativeFrom = relativeFrom.Replace("/", "\\");
            }

            relativeFrom = GetDirName(relativeFrom ?? "");
            relativeFrom = relativeFrom.Replace("\\", "/");
            relativeFrom = relativeFrom.Substring(0, relativeFrom.Length - 1);
            relPath = relPath.Replace("\\", "/");

            string path = relativeFrom;
            while (relPath.StartsWith("../"))
            {
                int lastSlash = path.LastIndexOf('/');
                if (lastSlash > -1)
                {
                    path = path.Substring(0, lastSlash); //remove one directory
                    relPath = relPath.Substring(3);// remove the ../
                }
                else
                {
                    throw new InvalidOperationException("Invalid relative path <" + relPath + "> from path <" + relativeFrom + ">");
                }
            }
            if (relPath.StartsWith("/"))
                relPath = relPath.Substring(1);
            path += "/" + relPath; // add the remaining path
            path = path.Replace("/", origSlashChar);
            return path;


        }
        /// <summary>
        /// Converts an absolute path to a relative one, if possible, with respective to a relative location.  E.g, c:\karl\test.xml from c:\work would result in ..\karl\test.xml
        /// </summary>
        /// <param name="path">Path which is to be converted to the relative one.</param>
        /// <param name="relativeFrom">Path from which the resulting path will be relative</param>
        /// <returns>Relative Path</returns>
        public static string ConvertAbsolutePathToRelativePath(string path, string relativeFrom)
        {
            path = path ?? "";
            
            char originalSlashType = '\\';
            if (path.Contains("/")) originalSlashType = '/';

            


            
            relativeFrom = relativeFrom ?? "";

            if (originalSlashType == '/')
            {
                path = path.Replace("\\", "/");
                relativeFrom = relativeFrom.Replace("\\", "/");
            }
            else
            {
                path = path.Replace("/", "\\");
                relativeFrom = relativeFrom.Replace("/", "\\");
            }

            
            relativeFrom = GetDirName(relativeFrom);

            int sameLength = 0;

            for (int i = 1; i < relativeFrom.Length && i < path.Length; i++)
            {


                string pathPart = path.Substring(0, i);
                string relPart = relativeFrom.Substring(0, i);
                if (string.Compare(pathPart, relPart, true) != 0)
                {
                    break;
                }
                if (relativeFrom[i] == originalSlashType)
                    sameLength = i;



            }
            string result = null;
            if (sameLength > 0)
            {

                int totalSlashCount = 0;
                for (int i = sameLength + 1; i < relativeFrom.Length; i++)
                {
                    char c = relativeFrom[i];
                    if (c == originalSlashType)
                        totalSlashCount++;
                }

                for (int i = 0; i < totalSlashCount; i++)
                {
                    result += ".." + originalSlashType;
                }
                result += path.Substring(sameLength + 1);
            }
            else
            {
                result = path;
            }
            return result;


        }

        /// <summary>
        /// This method will add the current directory if the path is relative, e.g 'mysqldump.exe'.  THis will result in say 'c:\app\testing\mysqldump.exe'
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string AddCurrentDirectoryIfPathDoesNotIncludeADir(string path)
        {


            string s = path;
            if (!string.IsNullOrWhiteSpace(s))
            {
                if (!s.Contains(@":\"))
                {
//this means this is a relative path from the current working directory
                    s = System.Environment.CurrentDirectory + @"\" +s;


                }
                s = CS.General_v3.Util.IO.RemoveDuplicateSlashesFromPath(s);
                s = CS.General_v3.Util.IO.NormaliseToLocalPath(s);
            }
            return s;
        }

        public static string RemoveDuplicateSlashesFromPath(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                CS.General_v3.Util.Text.ReplaceTexts(path, "/", "///", "//");
                CS.General_v3.Util.Text.ReplaceTexts(path, "\\", "\\\\\\", "\\\\"); 
            }
            return path;
        }

        public static void CopyFile(string from, string newPath, bool overwriteIfExists = true)
        {
            
            if (File.Exists(from) && string.Compare(from,newPath,true) != 0)
            {
                CreateDirectory(newPath);
                File.Copy(from, newPath, overwriteIfExists);
            }
        }
        /// <summary>
        /// Checks the path, and normalizes it to a local path.  Any / are replaced to a \, and any duplicates also to just a \.  If it starts with a '/', it is resolved using map path first
        /// </summary>
        /// <param name="copyToFolder"></param>
        /// <returns></returns>
        public static string NormaliseToLocalPath(string folder)
        {
            if (!string.IsNullOrEmpty(folder))
            {
                folder = folder.Replace("//", "/");
                folder = folder.Replace("//", "/");
                if (folder.StartsWith("/"))
                    folder = CS.General_v3.Util.PageUtil.MapPath(folder);
                folder = folder.Replace("/", "\\");
                folder = folder.Replace(@"\\\", @"\");
                folder = folder.Replace(@"\\", @"\");
            }
           
            return folder;
        }

        public static void SerializeObjectToFile(string localPath, object obj)
        {
            CreateDirectory(localPath);
            FileStream fs = File.Open(localPath, FileMode.Create, FileAccess.Write);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs, obj);
            fs.Dispose();
            
        }
        public static T DeserializeObjectFromFile<T>(string localPath) where T : class
        {
            return (T)DeserializeObjectFromFile(localPath);
        }
        public static object DeserializeObjectFromFile(string localPath) 
        {
            object obj = null;
            if (File.Exists(localPath))
            {

                FileStream fs = File.Open(localPath, FileMode.Open, FileAccess.Read);
                BinaryFormatter bf = new BinaryFormatter();
                try
                {
                    obj = bf.Deserialize(fs);
                }
                catch (Exception ex)
                {
                    obj = null;
                }
                fs.Dispose();


            }
            return obj;
        }

        public static bool CheckIfFileMatchesExtensions(string filePath, IEnumerable<string> extensions)
        {
            
            bool ok = true;
            if (extensions != null && extensions.Count()>0)
            {
                ok = false;
                string ext = GetExtension(filePath);
                foreach (var cmpExt in extensions)
                {
                    string s = cmpExt;
                    if (s.StartsWith("."))
                        s = CS.General_v3.Util.Text.RemoveLastNumberOfCharactersFromString(s, 1);
                    if (string.Compare(ext, s, true) == 0)
                    {
                        ok = true;
                        break;
                    }
                }
            }
            return ok;

        }
    }
}
