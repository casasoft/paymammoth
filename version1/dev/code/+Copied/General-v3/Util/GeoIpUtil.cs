﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using CS.General_v3.Classes.GeoIP;

namespace CS.General_v3.Util
{
    public static class GeoIpUtil
    {
        public class MaxMindCountryLookup
        {
            private FileStream fileInput;
            private static long COUNTRY_BEGIN = 16776960;
            private static string[] countryCode = 
								{ "--","AP","EU","AD","AE","AF","AG","AI","AL","AM","AN","AO","AQ","AR","AS","AT","AU","AW","AZ","BA","BB","BD","BE","BF","BG","BH","BI","BJ","BM","BN","BO","BR","BS","BT","BV","BW","BY","BZ","CA","CC","CD","CF","CG","CH","CI","CK","CL","CM","CN","CO","CR","CU","CV","CX","CY","CZ","DE","DJ","DK","DM","DO","DZ",
									"EC","EE","EG","EH","ER","ES","ET","FI","FJ","FK","FM","FO","FR","FX","GA","GB","GD","GE","GF","GH","GI","GL","GM","GN","GP","GQ","GR","GS","GT","GU","GW","GY","HK","HM","HN","HR","HT","HU","ID","IE","IL","IN","IO","IQ","IR","IS","IT","JM","JO","JP","KE","KG","KH","KI","KM","KN","KP","KR","KW","KY","KZ",
									"LA","LB","LC","LI","LK","LR","LS","LT","LU","LV","LY","MA","MC","MD","MG","MH","MK","ML","MM","MN","MO","MP","MQ","MR","MS","MT","MU","MV","MW","MX","MY","MZ","NA","NC","NE","NF","NG","NI","NL","NO","NP","NR","NU","NZ","OM","PA","PE","PF","PG","PH","PK","PL","PM","PN","PR","PS","PT","PW","PY","QA",
									"RE","RO","RU","RW","SA","SB","SC","SD","SE","SG","SH","SI","SJ","SK","SL","SM","SN","SO","SR","ST","SV","SY","SZ","TC","TD","TF","TG","TH","TJ","TK","TM","TN","TO","TP","TR","TT","TV","TW","TZ","UA","UG","UM","US","UY","UZ","VA","VC","VE","VG","VI","VN","VU","WF","WS","YE","YT","CS","ZA","ZM","ZR","ZW","A1","A2"};
            private static string[] countryName = 
								{"N/A","Asia/Pacific Region","Europe","Andorra","United Arab Emirates","Afghanistan","Antigua and Barbuda","Anguilla","Albania","Armenia","Netherlands Antilles","Angola","Antarctica","Argentina","American Samoa","Austria","Australia","Aruba","Azerbaijan","Bosnia and Herzegovina","Barbados","Bangladesh","Belgium",
									"Burkina Faso","Bulgaria","Bahrain","Burundi","Benin","Bermuda","Brunei Darussalam","Bolivia","Brazil","Bahamas","Bhutan","Bouvet Island","Botswana","Belarus","Belize","Canada","Cocos (Keeling) Islands","Congo, The Democratic Republic of the","Central African Republic","Congo","Switzerland","Cote D'Ivoire",
									"Cook Islands","Chile","Cameroon","China","Colombia","Costa Rica","Cuba","Cape Verde","Christmas Island","Cyprus","Czech Republic","Germany","Djibouti","Denmark","Dominica","Dominican Republic","Algeria","Ecuador","Estonia","Egypt","Western Sahara","Eritrea","Spain","Ethiopia","Finland","Fiji","Falkland Islands (Malvinas)",
									"Micronesia, Federated States of","Faroe Islands","France","France, Metropolitan","Gabon","United Kingdom","Grenada","Georgia","French Guiana","Ghana","Gibraltar","Greenland","Gambia","Guinea","Guadeloupe","Equatorial Guinea","Greece","South Georgia and the South Sandwich Islands","Guatemala","Guam","Guinea-Bissau","Guyana",
									"Hong Kong","Heard Island and McDonald Islands","Honduras","Croatia","Haiti","Hungary","Indonesia","Ireland","Israel","India","British Indian Ocean Territory","Iraq","Iran, Islamic Republic of","Iceland","Italy","Jamaica","Jordan","Japan","Kenya","Kyrgyzstan","Cambodia","Kiribati","Comoros","Saint Kitts and Nevis",
									"Korea, Democratic People's Republic of","Korea, Republic of","Kuwait","Cayman Islands","Kazakstan","Lao People's Democratic Republic","Lebanon","Saint Lucia","Liechtenstein","Sri Lanka","Liberia","Lesotho","Lithuania","Luxembourg","Latvia","Libyan Arab Jamahiriya","Morocco","Monaco","Moldova, Republic of","Madagascar",
									"Marshall Islands","Macedonia","Mali","Myanmar","Mongolia","Macau","Northern Mariana Islands","Martinique","Mauritania","Montserrat","Malta","Mauritius","Maldives","Malawi","Mexico","Malaysia","Mozambique","Namibia","New Caledonia","Niger","Norfolk Island","Nigeria","Nicaragua","Netherlands",
									"Norway","Nepal","Nauru","Niue","New Zealand","Oman","Panama","Peru","French Polynesia","Papua New Guinea","Philippines","Pakistan","Poland","Saint Pierre and Miquelon","Pitcairn Islands","Puerto Rico","Palestinian Territory","Portugal","Palau","Paraguay","Qatar","Reunion","Romania","Russian Federation","Rwanda","Saudi Arabia",
									"Solomon Islands","Seychelles","Sudan","Sweden","Singapore","Saint Helena","Slovenia","Svalbard and Jan Mayen","Slovakia","Sierra Leone","San Marino","Senegal","Somalia","Suriname","Sao Tome and Principe","El Salvador","Syrian Arab Republic","Swaziland","Turks and Caicos Islands","Chad","French Southern Territories","Togo",
									"Thailand","Tajikistan","Tokelau","Turkmenistan","Tunisia","Tonga","East Timor","Turkey","Trinidad and Tobago","Tuvalu","Taiwan","Tanzania, United Republic of","Ukraine","Uganda","United States Minor Outlying Islands","United States","Uruguay","Uzbekistan","Holy See (Vatican City State)","Saint Vincent and the Grenadines",
									"Venezuela","Virgin Islands, British","Virgin Islands, U.S.","Vietnam","Vanuatu","Wallis and Futuna","Samoa","Yemen","Mayotte","Serbia and Montenegro","South Africa","Zambia","Zaire","Zimbabwe","Anonymous Proxy","Satellite Provider"};


            public MaxMindCountryLookup(string fileName)
            {
                try
                {
                    fileInput = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                }
                catch (FileNotFoundException e)
                {
                    Console.WriteLine("File " + fileName + " not found.");
                }
            }

            public string lookupCountryCode(string str)
            {
                IPAddress addr;
                try
                {
                    addr = IPAddress.Parse(str);
                }
                catch (FormatException e)
                {
                    return "--";
                }
                return lookupCountryCode(addr);
            }

            private long addrToNum(IPAddress addr)
            {
                long ipnum = 0;


                //byte[] b = BitConverter.GetBytes( addr.Address); //this was previously as this line, but it is deprecated and thus converted to next line. Test it out.
                byte[] b = addr.GetAddressBytes();
                for (int i = 0; i < 4; ++i)
                {
                    long y = b[i];
                    if (y < 0)
                    {
                        y += 256;
                    }
                    ipnum += y << ((3 - i) * 8);
                }
                Console.WriteLine(ipnum);
                return ipnum;
            }

            public string lookupCountryCode(IPAddress addr)
            {
                string code = null;
                int index = (int)seekCountry(0, addrToNum(addr), 31);
                if (index < countryCode.Length)
                    code = countryCode[index];
                return code;

            }

            public string lookupCountryName(string str)
            {
                IPAddress addr;
                try
                {
                    addr = IPAddress.Parse(str);
                }
                catch (FormatException e)
                {
                    return "N/A";
                }
                return lookupCountryName(addr);
            }

            public string lookupCountryName(IPAddress addr)
            {
                return (countryName[(int)seekCountry(0, addrToNum(addr), 31)]);
            }

            private long seekCountry(long offset, long ipnum, int depth)
            {
                byte[] buf = new byte[6];
                long[] x = new long[2];
                if (depth == 0)
                {
                    Console.WriteLine("Maxmind GeoIP: Error seeking country.");
                }
                try
                {
                    fileInput.Seek(6 * offset, 0);
                    fileInput.Read(buf, 0, 6);
                }
                catch (IOException e)
                {
                    Console.WriteLine("Maxmind GeoIP: IO Exception");
                }
                for (int i = 0; i < 2; i++)
                {
                    x[i] = 0;
                    for (int j = 0; j < 3; j++)
                    {
                        int y = buf[i * 3 + j];
                        if (y < 0)
                        {
                            y += 256;
                        }
                        x[i] += (y << (j * 8));
                    }
                }

                if ((ipnum & (1 << depth)) > 0)
                {
                    if (x[1] >= COUNTRY_BEGIN)
                    {
                        return x[1] - COUNTRY_BEGIN;
                    }
                    return seekCountry(x[1], ipnum, depth - 1);
                }
                else
                {
                    if (x[0] >= COUNTRY_BEGIN)
                    {
                        return x[0] - COUNTRY_BEGIN;
                    }
                    return seekCountry(x[0], ipnum, depth - 1);
                }
            }
        }



        private static string getMaxMindGeoIPDBLocation()
        {
            string path = "";
            if (CS.General_v3.Util.Other.IsWebApplication)
            {
                string s = Enums.SETTINGS_ENUM.Others_MaxMind_GeoIP_DBLocation.GetSettingFromDatabase();
                s = s.Replace("\\", "/");
                path = CS.General_v3.Util.PageUtil.MapPath(s);
            }
            else
            {
                string s = Enums.SETTINGS_ENUM.Others_MaxMind_GeoIP_DBLocation.GetSettingFromDatabase();
                s = s.Replace("/", "\\");
                path = CS.General_v3.Util.ApplicationUtil.ApplicationPath + s;
            }
            return path;
        }

        public static CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? GetIPLocationCountryCode(string ip = null, bool storeInSession = true)
        {
            var ipLoc = GetIPLocation(ip, storeInSession);
            if (ipLoc != null)
            {
                return ipLoc.CountryCodeEnum;
            }
            return null;
        }

        public static IPLocationInfo GetIPLocation(string ip = null, bool storeInSession = true)
        {

            bool result = true;
            // double dLongtitute, dLatitude;
            IPLocationInfo loc = GetIPLocationHostIP(ip: ip, storeInSession: storeInSession);

            if (loc == null || (loc != null && (string.IsNullOrEmpty(loc.CountryCode) || loc.CountryCode.ToUpper() == "XX")))
            {
                result = false;
                if (loc == null)
                    loc = new IPLocationInfo();
                if (CS.General_v3.Settings.Others.IsInLocalTesting)
                {
                    loc.CountryCode = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_To2LetterCode(Enums.ISO_ENUMS.Country_ISO3166.Malta);
                    loc.CountryCodeEnum = Enums.ISO_ENUMS.Country_ISO3166.Malta;
                    result = true;
                }
                else
                {
                    loc.City = "";

                    //dLongtitute = dLatitude = double.MinValue;

                    string countryCode, countryName;

                    result = GetIPLocationMaxMind(out countryCode, out countryName);
                    if (result)
                    {
                        loc.CountryCode = countryCode;
                        loc.CountryName = countryName;
                    }
                    if (loc.CountryCode == "--")
                    {
                        loc.CountryCode = "";
                        loc.CountryName = "";
                    }
                    if (!string.IsNullOrWhiteSpace(loc.CountryCode))
                    {
                        loc.CountryCodeEnum = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_FromCode(loc.CountryCode);


                    }
                }

            }
            if (!result)
                loc = null;
            return loc;
        }
        public static bool GetIPLocationMaxMind(out string countryCode, out string countryName, string ip = null)
        {
            if (ip == null)
                ip = CS.General_v3.Util.PageUtil.GetUserIP();
            MaxMindCountryLookup cLookup = new MaxMindCountryLookup(getMaxMindGeoIPDBLocation());
            countryCode = cLookup.lookupCountryCode(ip);
            countryName = cLookup.lookupCountryName(ip);
            return true;
        }
        public const string HOSTIP_URL = "http://api.hostip.info/?ip=";



        private static IPLocationInfo _SessionIPInfo
        {
            get
            {
                return (IPLocationInfo)CS.General_v3.Util.PageUtil.GetSessionObject("GeoIP_User_SessionIPInfo");
            }
            set
            {
                CS.General_v3.Util.PageUtil.SetSessionObjectFromSerializableItem("GeoIP_User_SessionIPInfo", value);
            }
        }
        public static IPLocationInfo GetIPLocationHostIP(string ip = null, bool storeInSession = true)
        {

            storeInSession = storeInSession && !CS.General_v3.Util.PageUtil.IsUserUsingFakeIP();

            IPLocationInfo loc = null;
            if (storeInSession)
                loc = _SessionIPInfo;
            bool result = true;
            if (loc == null)
            {
                result = false;
                loc = new IPLocationInfo();
                if (storeInSession)
                    _SessionIPInfo = loc;




                //countryCode = countryName = cityName = null;
                //longtitude = latitude = float.MinValue;
                try
                {
                    if (ip == null)
                        ip = CS.General_v3.Util.PageUtil.GetUserIP();
                    string url = HOSTIP_URL + ip;
                    XmlDocument doc = new XmlDocument();
                    doc.Load(url);
                    XmlNamespaceManager nsManager = CS.General_v3.Util.XML.CreateNameSpaceManager(doc);
                    nsManager.AddNamespace("default_ns", "http://www.hostip.info/api");


                    XmlNode hostIP = doc.SelectSingleNode("HostipLookupResultSet/gml:featureMember/Hostip", nsManager);
                    loc.IP = ip;
                    XmlNode node = null;
                    node = hostIP.SelectSingleNode("gml:name", nsManager); if (node != null) loc.City = node.InnerText;
                    node = hostIP.SelectSingleNode("countryName", nsManager); if (node != null) loc.CountryName = node.InnerText;
                    node = hostIP.SelectSingleNode("countryAbbrev", nsManager); if (node != null) loc.CountryCode = node.InnerText;


                    node = hostIP.SelectSingleNode("ipLocation/gml:PointProperty/gml:Point/gml:coordinates", nsManager);
                    if (!string.IsNullOrWhiteSpace(loc.CountryCode))
                    {
                        loc.CountryCodeEnum = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_FromCode(loc.CountryCode);
                    }
                    if (node != null)
                    {
                        string[] coords = node.InnerText.Split(',');
                        loc.Longtitude = Convert.ToDouble(coords[0]);
                        loc.Latitude = Convert.ToDouble(coords[1]);

                    }
                    result = true;
                }
                catch
                {
                    result = false;
                }
            }
            if (!result)
                loc = null;
            return loc;

        }

    }
}
