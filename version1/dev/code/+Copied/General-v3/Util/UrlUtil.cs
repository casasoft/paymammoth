﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Extensions;
namespace CS.General_v3.Util
{
    public static class UrlUtil
    {
        public const int DEFAULT_HTTP_PORT = 80;
        public const int DEFAULT_HTTPS_PORT = 443;


        public static bool UrlIncludesProtocol(string url)
        {
            bool ok = false;
            
            if (url != null)
            {
                int startPos = 0;
                List<string> protocols = getListOfProtocols();
                for (int i = 0; i < protocols.Count; i++)
                {
                    int protocolLen = protocols[i].Length + +"://".Length;

                    if (url.ToLower().StartsWith(protocols[i] + "://"))
                    {
                        ok = true;
                        break;
                    }


                }
            }
            return ok;
            
        }
        private static void _test_UrlIncludesProtocol()
        {
            string url;
            bool result = false;
            url = "http://www.maltacarsonline.com/hello";
            result = UrlIncludesProtocol(url);
            if (result != true) throw new InvalidOperationException("test failed");

            url = "https://www.maltacarsonline.com/hello";
            result = UrlIncludesProtocol(url);
            if (result != true) throw new InvalidOperationException("test failed");

            url = "ftp://www.maltacarsonline.com/hello";
            result = UrlIncludesProtocol(url);
            if (result != true) throw new InvalidOperationException("test failed");

            url = "www.maltacarsonline.com/hello";
            result = UrlIncludesProtocol(url);
            if (result != false) throw new InvalidOperationException("test failed");

            url = "";
            result = UrlIncludesProtocol(url);
            if (result != false) throw new InvalidOperationException("test failed");

            url = "www.maltacarsonline.com";
            result = UrlIncludesProtocol(url);
            if (result != false) throw new InvalidOperationException("test failed");

        }
        public static string RemoveProtocolFromUrl(string url)
        {
            if (url != null)
            {
                List<string> protocols = getListOfProtocols();
                for (int i = 0; i < protocols.Count; i++)
                {
                    int protocolLen = protocols[i].Length + +"://".Length;

                    if (url.ToLower().StartsWith(protocols[i] + "://"))
                    {
                        
                        url = url.Substring(protocolLen);
                        break;
                    }
                }
            }
            return url;


        }




        /// <summary>
        /// Returns the base url, without the / from a url.  E.g http://www.maltacarsonline.com/testing/test.html results in http://www.maltacarsonline.com
        /// </summary>
        /// <param name="url"></param>
        /// <param name="includeScheme">Whether to include http/s</param>
        /// <returns></returns>
        public static string GetBaseUrl(string url, bool includeScheme = true)
        {
            string result = url;
            if (result != null)
            {
                int startPos = 0;
                if (includeScheme)
                {
                
                    List<string> protocols = getListOfProtocols();
                    for (int i = 0; i < protocols.Count; i++)
                    {
                        int protocolLen = protocols[i].Length + +"://".Length;

                        if (url.ToLower().StartsWith(protocols[i] + "://"))
                        {
                            startPos = protocolLen;
                            
                            break;
                        }


                    }
                }
                else
                {
                    url = RemoveProtocolFromUrl(url);
                }

                int slashPos = url.IndexOf('/', startPos);
                if (slashPos > -1)
                {
                    result = url.Substring(0, slashPos);
                }
                else
                {
                    result = url;
                }
            }
            return result;
        }

        private static List<string> getListOfProtocols()
        {
            List<string> protocols = new List<string>();
            protocols.Add("http");
            protocols.Add("https");
            protocols.Add("ftp");
            return protocols;

        }
        /// <summary>
        /// Returns the protocol from a url, e.g http, https, ftp.  Without the ://
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string GetProtocolFromUrl(string url)
        {
            string protocol = null;
            if (url != null)
            {
                int colonSlashSlashPos = url.IndexOf("://");
                if (colonSlashSlashPos > -1 && colonSlashSlashPos < 8)
                {
                    protocol = url.Substring(0, colonSlashSlashPos);
                }

            }
            return protocol;
        }

        /// <summary>
        /// Returns the relative url, without the base url from a url.  E.g http://www.maltacarsonline.com/testing/test.html results in /testing/test.html
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string GetRelativeUrl(string url)
        {
            string result = "/";
            if (url!= null)
            {
                int startPos = 0;
                List<string> protocols = getListOfProtocols();
                for (int i = 0; i < protocols.Count; i++)
                {
                    int protocolLen = protocols[i].Length + +"://".Length;

                    if (url.ToLower().StartsWith(protocols[i] + "://"))
                    {
                        startPos = protocolLen;
                        break;
                    }


                }
                int slashPos = url.IndexOf('/', startPos);
                if (slashPos > -1)
                {
                    result = url.Substring(slashPos);
                }
                else
                {
                    result = "/";
                }
            }
            return result;
        }


        private static void _test_GetRelativeUrl()
        {

            string url;
            string result = null;
            url = "http://www.maltacarsonline.com/hello";
            result = GetRelativeUrl(url);
            if (result != "/hello") throw new InvalidOperationException("test failed");

            url = "www.maltacarsonline.com/hello2";
            result = GetRelativeUrl(url);
            if (result != "/hello2") throw new InvalidOperationException("test failed");


            url = "";
            result = GetRelativeUrl(url);
            if (result != "/") throw new InvalidOperationException("test failed");

            url = null;
            result = GetRelativeUrl(url);
            if (result != "/") throw new InvalidOperationException("test failed");

            url = "http://www.maltacarsonline.com/";
            result = GetRelativeUrl(url);
            if (result != "/") throw new InvalidOperationException("test failed");

            url = "http://www.maltacarsonline.com";
            result = GetRelativeUrl(url);
            if (result != "/") throw new InvalidOperationException("test failed");

            url = "www.maltacarsonline.com";
            result = GetRelativeUrl(url);
            if (result != "/") throw new InvalidOperationException("test failed");

            url = "acarsonline.com";
            result = GetRelativeUrl(url);
            if (result != "/") throw new InvalidOperationException("test failed");
        }


        public static int? ParsePortFromUrl(string url)
        {
            int? result = null;
            if (url != null)
            {
                string baseUrl = UrlUtil.GetBaseUrl(url, includeScheme: false);
                int colonPos = baseUrl.IndexOf(':');
                if (colonPos > -1)
                {
                    string sPort = baseUrl.Substring(colonPos + 1);
                    int port = 0;
                    if (int.TryParse(sPort, out port))
                        result = port;
                }
            }
            return result;

        }
       

        private static void _test_parsePortFromUrl()
        {
            string url;
            int? result = null;
            url = "http://www.maltacarsonline.com/hello";
            result =ParsePortFromUrl(url);
            if (result != null) throw new InvalidOperationException("test failed");

            url = "http://www.maltacarsonline.com:8080/hello";
            result = ParsePortFromUrl(url);
            if (result != 8080) throw new InvalidOperationException("test failed");

            url = "https://www.maltacarsonline.com:8080/hello";
            result = ParsePortFromUrl(url);
            if (result != 8080) throw new InvalidOperationException("test failed");

            url = "https://www.maltacarsonline.com";
            result = ParsePortFromUrl(url);
            if (result != null) throw new InvalidOperationException("test failed");

            url = "www.maltacarsonline.com:8090";
            result = ParsePortFromUrl(url);
            if (result != 8090) throw new InvalidOperationException("test failed");
        }



        private static void _test_getBaseUrl()
        {
            string url = "http://www.maltacarsonline.com/testing/test.html";
            string result = GetBaseUrl(url);
            if (result != "http://www.maltacarsonline.com") throw new InvalidOperationException("Test failed");

            url = "www.maltacarsonline.com/testing/test.html";
            result = GetBaseUrl(url);
            if (result != "www.maltacarsonline.com") throw new InvalidOperationException("Test failed");

            url = "http://www.maltacarsonline.com";
            result = GetBaseUrl(url);
            if (result != "http://www.maltacarsonline.com") throw new InvalidOperationException("Test failed");

            url = "http://www.maltacarsonline.com";
            result = GetBaseUrl(url, includeScheme: false);
            if (result != "www.maltacarsonline.com") throw new InvalidOperationException("Test failed");

                    url = "www.maltacarsonline.com/testing/test.html";
            result = GetBaseUrl(url, includeScheme: false);
            if (result != "www.maltacarsonline.com") throw new InvalidOperationException("Test failed");

        }

        public static bool CheckWhetherUrlIsSecure(string url)
        {
            bool isSecure = false;
            if (url != null && url.ToLower().StartsWith("https://"))
                isSecure = true;
            return isSecure;


        }
        private static void _test_CheckWhetherUrlIsSecure()
        {
            string url = "http://www.maltacarsonline.com/testing/test.html";
            bool secure = CheckWhetherUrlIsSecure(url);
            if (secure!= false) throw new InvalidOperationException("Test failed");

            url = "https://www.maltacarsonline.com/testing/test.html";
            secure = CheckWhetherUrlIsSecure(url);
            if (secure != true) throw new InvalidOperationException("Test failed");

            url = "ftp://www.maltacarsonline.com/testing/test.html";
            secure = CheckWhetherUrlIsSecure(url);
            if (secure != false) throw new InvalidOperationException("Test failed");

            url = "";
            secure = CheckWhetherUrlIsSecure(url);
            if (secure != false) throw new InvalidOperationException("Test failed");

            url = null;
            secure = CheckWhetherUrlIsSecure(url);
            if (secure != false) throw new InvalidOperationException("Test failed");

        }
        public static bool CheckForYoutubeLinks(string link)
        {
            bool result = false;
            if (link != null)
            {
                if (link.IndexOf("youtube.") != -1)
                {
                    result = true;
                }
                if (link.IndexOf("youtu.be") != -1)
                {
                    result = true;
                }
            }
            return result;
        }
        public static bool CompareURLs(string url1, string url2)
        {
            url1 = CS.General_v3.Util.PageUtil.ConvertRelativeUrlToAbsoluteUrl(url1);
            url2 = CS.General_v3.Util.PageUtil.ConvertRelativeUrlToAbsoluteUrl(url2);
            return string.Compare(url1, url2, true) == 0;
        }

        public static string ConvertTextToHttpLink(string value, bool requiresEndSlash = true)
        {
            

            string s = value;
            if (!string.IsNullOrWhiteSpace(s))
            {
                if (!s.ToLower().StartsWith("http://") && s.ToLower().StartsWith("https://"))
                {
                    s = "http://" + s;

                }
                if (requiresEndSlash && !s.EndsWith("/"))
                    s += "/";
            }
            
            return s;
        }

        public static string GetYoutubeIDFromLink(string link)
        {
            string ID = "";
            if (!String.IsNullOrEmpty(link))
            {
                if (link.ContainsCaseInsensitive(".be"))
                {
                    System.Text.RegularExpressions.Regex regYoutubeBe = new System.Text.RegularExpressions.Regex("(.*)/(.*)", System.Text.RegularExpressions.RegexOptions.Multiline | System.Text.RegularExpressions.RegexOptions.Singleline);
                    var regexItem = regYoutubeBe.Match(link);
                    ID = regexItem.Groups[2].Value;
                }
                else
                {
                    System.Text.RegularExpressions.Regex regYoutubeBe = new System.Text.RegularExpressions.Regex("(.*)v=(.*?)(&|$)(.*)", System.Text.RegularExpressions.RegexOptions.Multiline | System.Text.RegularExpressions.RegexOptions.Singleline);
                    var regexItem = regYoutubeBe.Match(link);
                    ID = regexItem.Groups[2].Value;
                }
            }
            return ID;
        }


        public static string GetYoutubeURLForEmbedding(string link, bool newVersion = true, bool modeTransparent = true)
        {
            var ID = GetYoutubeIDFromLink(link);
            StringBuilder sbYoutubeLink = new StringBuilder();
            sbYoutubeLink.Append(newVersion ? "http://youtube.com/embed/" : "http://youtube.com/v/");
            sbYoutubeLink.Append(ID);
            if(modeTransparent)
            {
                sbYoutubeLink.Append("?wmode=transparent");
            }
            return sbYoutubeLink.ToString();
        }


        public static string RemovePortFromUrl(string _pageURL)
        {
            string s = _pageURL;
                
            if (_pageURL != null)
            {
                //s = UrlUtil.RemoveProtocolFromUrl(_pageURL);
                int startPos = 0;
                List<string> protocols = getListOfProtocols();
                for (int i =0; i < protocols.Count; i++)
                {
                    string protocol = protocols[i];
                    if (s.ToLower().StartsWith(protocol + "://"))
                    {
                        startPos = protocol.Length + "://".Length;
                        break;

                    }
                }
                int colonPos = s.IndexOf(':', startPos);
                if (colonPos > -1)
                {
                    
                    int slashPos = s.IndexOf('/', colonPos);
                    if (slashPos == -1)
                        slashPos = s.Length;
                    s = s.Remove(colonPos, (slashPos - colonPos));


                }


            }
            return s;
            
        }
    }
}
