using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using System.Data;
using System.Data.OleDb;

namespace CS.General_v3.Util.Encoding
{
    public static partial class Encoding
    {
        public static string ParseExcelFileIntoCSV(Stream excelFile, string filename)
        {
            string ext = CS.General_v3.Util.IO.GetExtension(filename);
            string path = CS.General_v3.Util.IO.GetTempFilename(ext);
            CS.General_v3.Util.IO.SaveStreamToFile(excelFile, path);
            CS.General_v3.Util.Encoding.CSV.CSVGenerator csvGen = new CS.General_v3.Util.Encoding.CSV.CSVGenerator();
            if (ext == "xls")
            {
                string ExcelFilename = path;
                DataTable worksheets;
                string connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;" + @"Data Source=" + ExcelFilename + ";" + @"Extended Properties=""Excel 8.0;HDR=Yes;IMEX=1""";
                using (OleDbConnection connection = new OleDbConnection(connectionString))
                {
                    connection.Open();
                    worksheets = connection.GetSchema("Tables");
                    foreach (DataRow row in worksheets.Rows)
                    {
                        // For Sheets: 0=Table_Catalog,1=Table_Schema,2=Table_Name,3=Table_Type
                        // For Columns: 0=Table_Name, 1=Column_Name, 2=Ordinal_Position
                        string SheetName = (string)row[2];
                        OleDbCommand command = new OleDbCommand(@"SELECT * FROM [" + SheetName + "]", connection);
                        OleDbDataAdapter oleAdapter = new OleDbDataAdapter();
                        oleAdapter.SelectCommand = command;
                        DataTable dt = new DataTable();
                        oleAdapter.FillSchema(dt, SchemaType.Source);
                        oleAdapter.Fill(dt);
                        for (int r = 0; r < dt.Rows.Count; r++)
                        {
                            var dr = dt.Rows[r];
                            CS.General_v3.Util.Encoding.CSV.CSVLine csvLine = new CS.General_v3.Util.Encoding.CSV.CSVLine();
                            for (int j = 0; j < dt.Columns.Count; j++)
                            {
                                string s = dr[j].ToString();
                                csvLine.AddTokens(s);
                            }
                            csvGen.AddLine(csvLine);
                        }
                    }
                }
            }

            else
            {
                throw new InvalidOperationException("Cannot parse 'xlsx' files (Office 2007 format) - Only 'xls' files");
            }
            return csvGen.ToString();

        }
        
        public enum ENCODING_TYPE
        {
            Normal, UTF8
        }
        public enum ENDIAN_TYPE
        {
            LittleEndian,
            BigEndian
        }
        public static uint GetUnsignedIntegerFromByteArray(byte[] by, int index, ENDIAN_TYPE endianType = ENDIAN_TYPE.BigEndian)
        {
            by = convertArrayToProperFormat(by, index, sizeof(uint), endianType);
            return BitConverter.ToUInt32(by,  0);
        }
        public static ulong GetUnsignedLongFromByteArray(byte[] by, int index, ENDIAN_TYPE endianType = ENDIAN_TYPE.BigEndian)
        {
            by = convertArrayToProperFormat(by, index, sizeof(ulong), endianType);
            return BitConverter.ToUInt64(by,  0);
        }
        public static ushort GetUnsignedShortFromByteArray(byte[] by, int index, ENDIAN_TYPE endianType = ENDIAN_TYPE.BigEndian)
        {
            by = convertArrayToProperFormat(by, index, sizeof(ushort), endianType);
            
            return BitConverter.ToUInt16(by,  0);
        }

        public static int GetIntegerFromByteArray(byte[] by, int index, ENDIAN_TYPE endianType = ENDIAN_TYPE.BigEndian)
        {
            by = convertArrayToProperFormat(by, index, sizeof(int), endianType);

            return BitConverter.ToInt32(by, 0);
        }
        public static long GetLongFromByteArray(byte[] by, int index, ENDIAN_TYPE endianType = ENDIAN_TYPE.BigEndian)
        {
            by = convertArrayToProperFormat(by, index, sizeof(long), endianType);
            return BitConverter.ToInt64(by,  0);
        }
        public static short GetShortFromByteArray(byte[] by, int index, ENDIAN_TYPE endianType = ENDIAN_TYPE.BigEndian)
        {
            by = convertArrayToProperFormat(by, index, sizeof(short), endianType);
            return BitConverter.ToInt16(by,  0);
        }
        public static byte[] ConvertToByteArray(uint num, ENDIAN_TYPE endianType = ENDIAN_TYPE.BigEndian)
        {
            byte[] by = BitConverter.GetBytes(num);
            by = convertArrayToProperFormat(by, 0, sizeof(uint), endianType);
            return by;
        }
        public static byte[] ConvertToByteArray(ushort num, ENDIAN_TYPE endianType = ENDIAN_TYPE.BigEndian)
        {
            byte[] by = BitConverter.GetBytes(num);
            by = convertArrayToProperFormat(by, 0, sizeof(ushort), endianType);
            return by;
        }
        public static byte[] ConvertToByteArray(ulong num, ENDIAN_TYPE endianType = ENDIAN_TYPE.BigEndian)
        {
            byte[] by = BitConverter.GetBytes(num);
            by = convertArrayToProperFormat(by, 0, sizeof(ulong), endianType);
            return by;
        }
        public static byte[] ConvertToByteArray(int num, ENDIAN_TYPE endianType = ENDIAN_TYPE.BigEndian)
        {
            
            byte[] by = BitConverter.GetBytes(num);
            by = convertArrayToProperFormat(by, 0, sizeof(int),endianType);
            return by;
        }
        public static byte[] ConvertToByteArray(long num, ENDIAN_TYPE endianType = ENDIAN_TYPE.BigEndian)
        {
            byte[] by = BitConverter.GetBytes(num);
            by = convertArrayToProperFormat(by, 0, sizeof(long),endianType);
            return by;
        }
        public static byte[] ConvertToByteArray(short num, ENDIAN_TYPE endianType = ENDIAN_TYPE.BigEndian)
        {
            byte[] by = BitConverter.GetBytes(num);
            by = convertArrayToProperFormat(by, 0, sizeof(short), endianType);
            return by;
        }
        
        private static byte[] convertArrayToProperFormat(byte[] by, int index, int count, ENDIAN_TYPE endianType)
        {
            byte[] byNew = new byte[count];
            for (int i=0; i < count; i++)
            {
                byNew[i] = by[index + i];
            }
            
            
            bool toLittleEndian = (endianType == ENDIAN_TYPE.LittleEndian);
            if (BitConverter.IsLittleEndian != toLittleEndian)
            {
                byNew = reverseByteArray(byNew);
            }
            return byNew;
        }
        private static byte[] reverseByteArray(byte[] by)
        {
            byte[] revBy = new byte[by.Length];
            for (int i = 0; i < by.Length; i++)
            {
                revBy[by.Length - i - 1] = by[i];
            }
            return revBy;
        }

    }
        
}
