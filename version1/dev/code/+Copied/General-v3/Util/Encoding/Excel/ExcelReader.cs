﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.IO;
using CS.General_v3.Extensions;
namespace CS.General_v3.Util.Encoding.Excel
{
    public class ExcelReader
    {
        public List<List<object>> LoadFromExcelFile(Stream fs, string filename, string sheetName = null)
        {
            string ext = CS.General_v3.Util.IO.GetExtension(filename);
            string tmpFile = CS.General_v3.Util.IO.GetTempFilename(ext);
            CS.General_v3.Util.IO.SaveStreamToFile(fs, tmpFile);
            return LoadFromExcelFile(tmpFile, sheetName);
        }

        private OleDbConnection conn = null;

        private List<string> getDefaultSchemaNames()
        {
            List<string> defaultSheetValues = new List<string>();
            defaultSheetValues.Add("Sheet1");
            return defaultSheetValues;
        }

        private string getFirstDefaultSchemaNameFromConnection()
        {
            string sheetName = null;
            try
            {
                // Get the name of the first worksheet:
                DataTable dbSchema = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                if (dbSchema == null || dbSchema.Rows.Count < 1)
                {
                    throw new Exception("Error: Could not determine the name of the first worksheet.");
                }
                sheetName = dbSchema.Rows[0]["TABLE_NAME"].ToString();


            }
            catch (Exception ex)
            {
                sheetName = null;
            }
            finally
            {
                
            }
            if (sheetName.EndsWith("$"))
                sheetName = sheetName.Substring(0, sheetName.Length - 1);
            return sheetName;
        }

        private string getNextDefaultSchemaName()
        {
            defaultSheetValueIndex++;
            var list = getDefaultSchemaNames();
            if (defaultSheetValueIndex < list.Count)
                return list[defaultSheetValueIndex];
            else
                return null;
            
        }
        private int defaultSheetValueIndex = -1;
        public List<List<object>> LoadFromExcelFile(string filepath, string sheetName = null)
        {
            defaultSheetValueIndex = -1;
            string path = filepath;
            string connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source="+path+@";Extended Properties=""Excel 12.0 Xml;HDR=No;IMEX=1""";
            /*
             * 
             * If you want to read the column headers into the result set (using HDR=NO even though there is a header) and the column data is numeric, use IMEX=1 to avoid crash.

To always use IMEX=1 is a safer way to retrieve data for mixed data columns. Consider the scenario that one Excel file might work fine cause that file's data causes the driver to guess one data type while another file, containing other data, causes the driver to guess another data type. This can cause your app to crash
             * */
          
           

            

            List<List<object>> fileRows = new List<List<object>>();

            bool retry = false;
           
            bool useDefaultSheetNames = (sheetName == null); //use default sheet names to try and find
           // var defaultSheetNames = getDefaultSchemaNames();
            try
            {
                conn = new OleDbConnection(connectionString);
                conn.Open();
                if (sheetName == null)
                {
                    sheetName = getFirstDefaultSchemaNameFromConnection();
                    if (sheetName == null)
                        sheetName = getNextDefaultSchemaName();
                }
                do
                {
                    try
                    {


                        string sql = "Select * From [" + sheetName + "$]";

                        OleDbCommand cmd = new OleDbCommand(sql, conn);
                        
                        OleDbDataReader data = cmd.ExecuteReader();
                        while (data.Read())
                        {
                            List<object> rowData = new List<object>();
                            int fieldCount = data.FieldCount;
                            for (int i = 0; i < fieldCount; i++)
                            {
                                object val = data.GetValue(i);
                                rowData.Add(val);
                            }
                            fileRows.Add(rowData);

                        }
                        conn.Dispose();
                    }
                    catch (Exception ex)
                    {
                        retry = false;
                        if (useDefaultSheetNames && ex.Message.ContainsCaseInsensitive("Make sure the object exists and that you spell its name and the path name correctly")) //if sheet does not exist, and using default names, try next one.
                        {
                            sheetName = getNextDefaultSchemaName();
                            if (sheetName != null)
                            {
                                retry = true;
                            }
                        }
                        if (!retry)
                            throw;
                    }
                    finally
                    {
                        
                    }
                }
                while (retry);
            }
            catch (Exception ex)
            {
                if (ex.Message.ContainsCaseInsensitive("provider is not registered"))
                {
                    throw new InvalidOperationException("Could not find provider. Make sure that you have the Microsoft Access 2007 Data Connectivity components installed", ex);
                }
                else if (ex.Message.ContainsCaseInsensitive("is not a valid name. Make sure that it does not include "))
                {
                    throw new InvalidOperationException("Sheet name is invalid.  Make sure the worksheet name is ONLY made up of alpha-numeric characters (A-Z, 0-9), and does NOT include spaces, invalid characters, and punctuation. Also, it must not be longer than 30 characters.", ex);
                }
                throw;

            }
            finally
            {
                if (conn != null)
                {
                    conn.Dispose();
                    conn = null;
                }
            }

            return fileRows;

        }

    }
}
