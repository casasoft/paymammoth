using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;

namespace CS.General_v3.Util.Encoding
{
    
        public static partial class XML
        {
            public static string Encode(object o)
            {
                if (o is bool)
                    return EncodeBool((bool)o);
                else if (o is double || o is float)
                    return EncodeDouble((double)o);
                else if (o is int)
                    return EncodeInt((int)o);
                else if (o is DateTime)
                    return EncodeDate((DateTime)o);
                else
                    return o.ToString();
            }
            public static string EncodeBool(bool b)
            {
                if (b)
                    return "true";
                else
                    return "false";
            }
            public static string EncodeDouble(double d)
            {
                return d.ToString("0.00000000");
            }
            public static string EncodeInt(int i)
            {
                return i.ToString();
            }
            public static string EncodeDate(DateTime d)
            {
                return d.ToString("yyyy-MM-dd HH:mm:ss");
            }
            public static string Decode(XmlNode n)
            {
                if (n != null)
                    return Decode(n.InnerText);
                else
                    return Decode("");
            }
            public static string Decode(string s)
            {
                return s;
            }
            public static bool DecodeBool(XmlNode n)
            {
                string s = Decode(n).ToLower();
                return s == "true";
            }
            public static int DecodeInt(XmlNode n)
            {
                string s = Decode(n);
                return CS.General_v3.Util.Conversion.ToInt32(s);
            }
            public static double DecodeDouble(XmlNode n)
            {
                string s = Decode(n);
                return CS.General_v3.Util.Conversion.ToDouble(s);
            }
            public static DateTime DecodeDate(XmlNode n)
            {
                string s = Decode(n);
                int year = 1, month = 1, day = 1, hour = 0, min = 0, sec = 0;
                if (!string.IsNullOrEmpty(s))
                {
                    year = CS.General_v3.Util.Conversion.ToInt32(s.Substring(0, 4));
                    month = CS.General_v3.Util.Conversion.ToInt32(s.Substring(5, 2));
                    day = CS.General_v3.Util.Conversion.ToInt32(s.Substring(8, 2));
                    hour = CS.General_v3.Util.Conversion.ToInt32(s.Substring(11, 2));
                    min = CS.General_v3.Util.Conversion.ToInt32(s.Substring(14, 2));
                    sec = CS.General_v3.Util.Conversion.ToInt32(s.Substring(17, 2));
                }
                DateTime d = new DateTime(year, month, day, hour, min, sec);
                return d;
            }

        }
        

    
        
}
