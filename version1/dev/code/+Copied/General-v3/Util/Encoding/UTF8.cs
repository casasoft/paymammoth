using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;

namespace CS.General_v3.Util.Encoding
{
  
        
      

        public static partial class UTF8
        {
            public static string Encode(string data)
            {
                UTF8Encoding encoding = new UTF8Encoding();
                byte[] encodedData = encoding.GetBytes(data);
                StringBuilder sb = new StringBuilder(encodedData.Length);
                for (int i = 0; i < encodedData.Length; i++)
                {
                    sb.Append((char)encodedData[i]);
                }
                return sb.ToString();
            }
            public static string Decode(string data)
            {
                char[] encodedData = data.ToCharArray();
                UTF8Encoding encoding = new UTF8Encoding();
                byte[] bData = CS.General_v3.Util.Other.charArrayToByteArray(encodedData);
                return encoding.GetString(bData);
            }
        }

    
        
}
