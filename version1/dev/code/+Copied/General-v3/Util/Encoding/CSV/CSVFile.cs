using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using CS.General_v3.Util.Encoding.Excel;

namespace CS.General_v3.Util.Encoding.CSV
{
 
            
    public partial class CSVFile : List<CSVLine>
    {
        public static CSVFile LoadFromStream(Stream data, string filename)
        {
            CSVFile file = new CSVFile();
            if (!file.LoadDataFromStream(data, filename))
                file = null;
            return file;

        }
       
        public CSVFile()
        {
          
        }
        public CSVFile(string DataToParse, string delimeter = ",")
        {
            this.LoadDataFromText(DataToParse);
        }
        public bool LoadFromFile(string filepath)
        {
            bool ok = false;
            if (!string.IsNullOrWhiteSpace(filepath) && File.Exists(filepath))
            {
                FileStream fs = new FileStream(filepath, FileMode.Open, FileAccess.Read);
                ok = LoadDataFromStream(fs, filepath);
                fs.Dispose();
                //fs.Close();
            }
            return ok;


        }

        public bool LoadDataFromStream(Stream data, string filename)
        {
            bool ok = true;
            string ext = CS.General_v3.Util.IO.GetExtension(filename);
            switch (ext)
            {
                case "xls":
                case "xlsx":
                    {
                        ExcelReader reader = new ExcelReader();
                        var values = reader.LoadFromExcelFile(data, filename);
                        LoadDataFromObjectValues(values);
                        break;
                    }
                case "csv":
                case "txt":
                    {

                        StreamReader sr = new StreamReader(data);
                        string content = sr.ReadToEnd();
                        LoadDataFromText(content);
                    }
                    break;
                default:
                    {
                        ok = false;
                        break;
                    }
            }
            return ok;

        }
        public void LoadDataFromObjectValues(IEnumerable<IEnumerable<object>> data)
        {
            foreach (var row in data)
            {
                CSVLine line = new CSVLine();
                line.ParseFromObjectValues(row);
                this.Add(line);
            }
            
        }
        public void LoadDataFromText(string s)
        {
            string[] lines = s.Split(new string[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
            for (int i = 0; i < lines.Length; i++)
            {
                CSVLine line = new CSVLine(lines[i]);
                this.Add(line);
            }
        }
        

        public new CSVLine this[int index]
        {
            get
            {
                CSVLine line = new CSVLine();
                if (index < this.Count)
                    line = base[index];
                return line;
            }
            set
            {
                while (base.Count < index)
                {
                    base.Add(new CSVLine());
                }
                base[index] = value;
            }
        }

        public bool CheckIfAtLeastOneValueInColumnIsFilled(int columnIndex, int startLineIndex)
        {
            bool b = false;
            for (int i = startLineIndex; i < this.Count; i++)
            {
                CSVLine line = this[i];
                if (!string.IsNullOrEmpty(line[columnIndex]))
                {
                    b = true;
                    break;
                }
            }
            return b;
        }

        public string GetFileAsString(string delimeter = ",")
        {
            StringBuilder sb = new StringBuilder();
            foreach (var line in this)
            {
                sb.AppendLine(line.GetAsString(delimeter));
            }
            return sb.ToString();
        }

        public char[] GetFileAsCharArray()
        {
            return GetFileAsString().ToCharArray();
        }

        public void SaveFileTo(string path)
        {
            var s = GetFileAsString();
            CS.General_v3.Util.IO.SaveToFile(path, s);
        }

        public void ForceDownload(string filename)
        {
            char[] chArray = this.GetFileAsCharArray();
            CS.General_v3.Util.Web.ForceDownload(chArray, CS.General_v3.Util.Data.GetMIMEContentType(".csv"), filename);
        }

        
    }
    

            

       
}
