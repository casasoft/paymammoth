using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;

namespace CS.General_v3.Util.Encoding.CSV
{
    
        
       
            public partial class CSVGenerator
            {
                public CSVFile Lines { get; private set; }
                

                public CSVGenerator()
                {
                    this.Lines = new CSVFile();
                }
                public void AddLine(params string[] tokens)
                {
                    CSVLine line = new CSVLine();
                    line.AddTokens(tokens);
                    AddLine(line);
                }
                public void AddLines(List<CSVLine> lines)
                {
                    foreach (var line in lines)
                    {
                        this.Lines.Add(line);
                    }
                }
                public void AddLine(CSVLine line)
                {
                    this.Lines.Add(line);
                }
                public void Save(string path)
                {
                    string s = this.ToString();
                    CS.General_v3.Util.IO.SaveToFile(path, s);
                }
                public string GetCSVFileAsString()
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (var l in this.Lines)
                    {
                        sb.AppendLine(l.ToString());
                    }
                    return sb.ToString();
                }
                public override string ToString()
                {
                    return GetCSVFileAsString();
                }
            }
            

        
}
