using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;

namespace CS.General_v3.Util.Encoding.CSV
{
 
            
    public partial class CSVLine
    {
        
        public List<string> Tokens
        {
            get;
            private set;
        }
        public CSVLine()
        {
          
            Tokens = new List<string>();
            ResetMarkerPosition();
        }
        public CSVLine(string csvLine) : this()
        {
            ParseFromLine(csvLine);
        }
        public CSVLine(params string[] tokens)
            : this()
        {
            AddTokens(tokens);
            
        }
        public void AddTokens(params string[] tokens)
        {
            for (int i = 0; i < tokens.Length; i++)
            {
                this.Tokens.Add(tokens[i]);

            }
        }

     

        public string GetAsString(string delimeter = ",")
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < this.Tokens.Count; i++)
            {
                if (i > 0)
                    sb.Append(delimeter);
                sb.Append(CS.General_v3.Util.Encoding.CSV.CSV.Encode(this.Tokens[i]));

            }
            return sb.ToString();
        }

        public override string ToString()
        {
            return GetAsString(null);
        }
        public string this[int index]
        {
            get
            {
                string s = "";
                if (index < this.Tokens.Count)
                    s = this.Tokens[index];
                return s;
            }
            set
            {
                if (this.Tokens == null)
                    this.Tokens = new List<string>();
                while (this.Tokens.Count < index + 1)
                {
                    this.Tokens.Add("");
                }
                
                this.Tokens[index] = value;
            }
        }
        public void ParseFromLine(string s)
        {
            string[] tokens = CSV.SplitCSVLineIntoTokens(s);
            this.Tokens = new List<string>();
            for (int i = 0; i < tokens.Length; i++)
            {
                string token = tokens[i] ?? "";
                token = token.Trim();
                this.Tokens.Add(token);
            }
        }
        public int Count
        {
            get
            {
                int cnt = 0;
                if (this.Tokens != null)
                    cnt = this.Tokens.Count;
                return cnt;
            }
        }

        public string GetToken(int index)
        {
            string s = null;
            if (index < Tokens.Count)
            {
                s = Tokens[index];
            }
            return s;
        }

        private int currMarkerPosition = -1;
        public string GetNextToken()
        {
            currMarkerPosition++;
            return PeekToken();
            
        }
        public string PeekToken()
        {
            string s = "";
            if (currMarkerPosition < this.Count)
                s = this[currMarkerPosition];
            return s;
            
        }

        public double? GetNextTokenAsDouble()
        {
            string s = GetNextToken();
            double? d = CS.General_v3.Util.Other.ConvertStringToBasicDataType<double?>(s);
            return d;
            
        }
        public int? GetNextTokenAsInt()
        {
            string s = GetNextToken();
            int? d = CS.General_v3.Util.Other.ConvertStringToBasicDataType<int?>(s);
            return d;

        }

        public void ResetMarkerPosition()
        {
            this.currMarkerPosition = -1;
        }
        public bool CheckIfAtLeastOneFilled()
        {
            bool b = false;
            for (int i = 0; i < this.Tokens.Count; i++)
            {
                if (!string.IsNullOrWhiteSpace(this.Tokens[i]))
                {
                    b = true;
                    break;
                }
            }
            return b;
        }

        public void ParseFromObjectValues(IEnumerable<object> row)
        {
            foreach (var obj in row)
            {
                string s = obj.ToString();
                this.Tokens.Add(s);
            }
            
        }

        public bool IsLineBlank()
        {
            for (int i=0; i < this.Tokens.Count;i++)
            {
                var t = this.Tokens[i] ?? "";
                if (!string.IsNullOrWhiteSpace(t))
                    return false;
                
            }
            return true;
            
        }
    }
    

            

       
}
