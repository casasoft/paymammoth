using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;

namespace CS.General_v3.Util.Encoding.CSV
{
    
        
        public static partial class CSV
        {
            

            public static string[] SplitCSVLineIntoTokens(string line)
            {
                bool foundStartQuote = false;
                bool foundEndQuote = false;
                for (int i = 0; i < line.Length; i++)
                {
                    if (line[i] == '"')
                    {
                        bool isDoubleQuote = false;
                        if (!foundStartQuote)
                            foundStartQuote = true;
                        else
                        {
                            if (i + 1 < line.Length - 1 && line[i + 1] == '"')
                                isDoubleQuote = true;
                            if (!isDoubleQuote)
                            {
                                foundStartQuote = false;
                            }
                            else
                            {
                                i++;
                            }
                        }
                    }
                    else if ((line[i] == ',' || line[i] == '\t') && foundStartQuote)
                    {
                        line = line.Substring(0, i) + "{[COMMA]}" + line.Substring(i + 1);
                        i += "{[COMMA]}".Length - 1;
                    }
                }

                string[] tokens = line.Split(new string[] {",","\t"},  StringSplitOptions.None);
                for (int i = 0; i < tokens.Length; i++)
                {
                    tokens[i] = tokens[i].Replace("{[COMMA]}", ",");
                    tokens[i] = Decode(tokens[i]);
                    tokens[i] = tokens[i].Trim();
                }
                return tokens;
            }
            /// <summary>
            /// Encodes a string into CSV format
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
            public static string Encode(string data)
            {
                if (data == null)
                    data = "";
                string s = data.Replace("\"", "\"\"");
                s = "\"" + s + "\"";
                return s;
            }
            public static string EncodeBool(bool b)
            {
                string s = "";
                if (b)
                    s = "true";
                else
                    s = "false";
                return Encode(s);
            }
            /// <summary>
            /// Decodes a string from CSV format to plain text
            /// </summary>
            /// <param name="data"></param>
            /// <returns></returns>
            public static string Decode(string data)
            {
                string s = data;
                if (s.Length > 0 && s[0] == '"')
                {
                    s = s.Remove(0, 1);
                    s = s.Remove(s.Length - 1, 1);
                }
                s = s.Replace("\"\"", "\"");
                return s;
            }
            public static bool DecodeBool(string data)
            {
                string s = Decode(data).ToLower();
                return (s == "true");
            }
        }
        

    
        
}
