﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;
using CS.General_v3.Controls.WebControls.Common.SWFObject.v2;
namespace CS.General_v3.Util
{
    using JavaScript.Data;

    public static class SWFObjectUtil
    {

        public static string GetSWFObjectV2JS(string flashURL, string clientID, string width, string height, string flashVersion, string expressInstallURL)
        {
            JSObject flashParams = new JSObject();
            
            flashParams.AddProperty("scale", SWFObject.ScaleToString(SWFObject.SCALE.NoScale));
            flashParams.AddProperty("allowfullscreen", true);

            JSObject flashAttributes = new JSObject();
            flashAttributes.AddProperty("id", clientID);

            return GetSWFObjectV2JS(flashURL, clientID, width, height, flashVersion, expressInstallURL, null, flashParams, flashAttributes); 
        }

        public static string GetSWFObjectV2JS(string flashURL, string clientID, string width, string height, string flashVersion, string expressInstallURL, JSObject flashVars, JSObject flashParams, JSObject flashAttributes)
        {
            if (flashAttributes == null) flashAttributes = new JSObject();
            flashAttributes.AddProperty("name", clientID);

            flashAttributes.AddProperty("id", clientID);

            string jsFlashVars = (flashVars == null ? "{}" : flashVars.GetJS());

            string js = "jQuery(function() { swfobject.embedSWF('" + flashURL + "', '" + clientID + "', '" + width + "', '" + height + "', '" + flashVersion + "', '" + expressInstallURL + "', " + jsFlashVars + ", " + (flashParams == null ? "{}" : flashParams.GetJS()) + ", " + (flashAttributes == null ? "{}" : flashAttributes.GetJS()) + ")});";
            return js;
        }
    }
}
