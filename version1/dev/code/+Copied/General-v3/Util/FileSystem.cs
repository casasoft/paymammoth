using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
namespace CS.General_v3.Util
{
    public static class FileSystem
    {
        /// <summary>
        /// Search all files in a directory
        /// </summary>
        /// <param name="path"></param>
        /// <param name="searchPattern">File name or multiple files seperated by a comma or semi colon</param>
        /// <returns></returns>
        public static List<FileInfo> GetAllFilesInDirectory(string path, string searchPattern)
        {
            searchPattern = searchPattern.Replace(".", "\\.");
            searchPattern = searchPattern.Replace("*", ".*");
            searchPattern = CS.General_v3.Util.Text.ReplaceTexts(searchPattern, "|", ", ", "; ", ",", ";");
            searchPattern = "(" + searchPattern + ")";
            return GetAllFilesInDirectoryByRegEx(path, searchPattern);
            //.txt,.jpg,.jpeg

            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="regExSearchPattern">Regular expression search pattern.  If you want to search for file extension, check it is at end by using $ sign</param>
        /// <returns></returns>
        public static List<FileInfo> GetAllFilesInDirectoryByRegEx(string path, string regExSearchPattern)
        {
            DirectoryInfo dir = new DirectoryInfo(path);
            Stack<DirectoryInfo> stack = new Stack<DirectoryInfo>();
            stack.Push(dir);
            List<FileInfo> allFiles = new List<FileInfo>();
            while (stack.Count > 0)
            {
                dir = stack.Pop();
                FileInfo[] files = dir.GetFiles();
                for (int i = 0; i < files.Length; i++)
                {
                    string fileName = files[i].Name;
                    if (Regex.Match(fileName, regExSearchPattern).Success)
                    {
                        allFiles.Add(files[i]);
                    }
                }
                DirectoryInfo[] dirs = dir.GetDirectories();
                for (int i = 0; i < dirs.Length; i++)
                {
                    stack.Push(dirs[i]);

                }
            }
            return allFiles;


        }

    }
}
