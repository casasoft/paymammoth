﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Reflection;
using System.Text.RegularExpressions;
using CS.General_v3.Classes.Reflection;
using System.Collections.Specialized;
using CS.General_v3.Classes.Reflection.HelperClasses;
using Iesi.Collections.Generic;

namespace CS.General_v3.Util
{
    public static class ReflectionUtil<T>
    {


        public static string GetPropertyName<TValue>(Expression<Func<T, TValue>> selector)
        {
            var pINfo = GetPropertyBySelector(selector);
            return pINfo.Name;


        }
        public static PropertyInfo GetPropertyBySelector<TValue>(Expression<Func<T, TValue>> selector)
        {
            return ReflectionUtil.GetPropertyBySelector((Expression)selector);


        }

    }
    public delegate object ReflectionUpdatePropertyHandler(object obj, MemberInfo property, object propertyValue);
    
    public static class ReflectionUtil
    {
        /// <summary>
        /// Return the string value of an Enum if the DescriptionAttribute tag is defined (requires System.ComponentModel)
        /// 
        ///[DescriptionAttribute("Food")]
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T GetAttributeFromEnumValue<T>(Enum value)
            where T : Attribute
        {
            Attribute attrib = GetAttributeFromEnumValue(value, typeof(T));
            return (T)attrib;
            
        }
      
        /// <summary>
        /// Return the string value of an Enum if the DescriptionAttribute tag is defined (requires System.ComponentModel)
        /// 
        ///[DescriptionAttribute("Food")]
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Attribute GetAttributeFromEnumValue(Enum value, Type attribType)
        {
            if (value == null) throw new InvalidOperationException("Enum value cannot be NULL");
            FieldInfo fi = value.GetType().GetField(value.ToString());
            if (fi != null)
            {
                object[] attributesList = fi.GetCustomAttributes(attribType, true);
                if (attributesList.Length > 0)
                {

                    return (Attribute)attributesList[0];
                }
            }
            return null;
        }

        public static object GetDefaultValueOfType(Type t)
        {
            object value = null;
            if (t.IsValueType)
            {
                value = Activator.CreateInstance(t);
            }
            return value;
        }

        public static void SerializeObjectToNameValueCollection<T>(NameValueCollection nv, T obj, bool convertEnumsToIntValue = false)
        {
            SerializeObjectToNameValueCollection(nv, typeof (T), obj, convertEnumsToIntValue);
        }

        public static void SerializeObjectToNameValueCollection(NameValueCollection nv, Type typeToSerialize, object obj,  bool convertEnumsToIntValue = false)
        {
            Type objType = obj.GetType();
            if (!typeToSerialize.IsAssignableFrom(objType))
                throw new InvalidOperationException("Object must implement Type <" + typeToSerialize.FullName +
                                                    " to be able to deserialize");
            var properties = GetAllPropertiesForType(typeToSerialize, false, false, true);
            foreach (var p in properties)
            {
                var value = p.GetValue(obj, null);
                string s = null;
                if (value != null)
                    s = CS.General_v3.Util.Other.ConvertBasicDataTypeToString(value, ifEnumConvertToIntValue: convertEnumsToIntValue);
                nv[p.Name] = s;
            }
            
        }
        public static void DeserializeObjectFromNameValueCollection<T>(NameValueCollection nv, T obj)
        {


            DeserializeObjectFromNameValueCollection(nv, typeof (T), obj);
        }

        public static void DeserializeObjectFromNameValueCollection(NameValueCollection nv, Type typeToDeserialize, object obj)
        {
            Type objType = obj.GetType();
            if (!typeToDeserialize.IsAssignableFrom(objType))
                throw new InvalidOperationException("Object must implement Type <" + typeToDeserialize.FullName +
                                                    " to be able to deserialize");
            //Type t = obj.GetType();
            var properties = GetAllPropertiesForType(typeToDeserialize, false, false, true);

            foreach (var p in properties)
            {
                string strValue = nv[p.Name];
                try
                {
                    object value = strValue;

                    if (p.PropertyType != typeof(string))
                        value = CS.General_v3.Util.Other.ConvertStringToBasicDataType(strValue, p.PropertyType);
                    if (p.CanWrite)
                        p.SetValue(obj, value, null);
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException("Error while trying to parse property '" + p.Name + "' with value <" + strValue + ">", ex);
                }
            }
        }

        public static void UpdateAllPropertiesOfObject(object obj, bool loadNonPublicTypes, bool loadStaticProperties, ReflectionUpdatePropertyHandler updateHandler)
        {
            Type t = obj.GetType();
            List<MemberInfo> propsAndFields = GetAllPropertiesAndFieldsForType(t, loadNonPublicTypes, loadStaticProperties, false);


            for (int i = 0; i < propsAndFields.Count; i++)
            {
                MemberInfo item = propsAndFields[i];
                if (updateHandler != null)
                {
                    object newValue = null;
                    if (item is PropertyInfo)
                    {
                        PropertyInfo prop = (PropertyInfo)item;
                        newValue = updateHandler(obj, prop, prop.GetValue(obj, null));
                        prop.SetValue(obj, newValue, null);
                    }
                    else if (item is FieldInfo)
                    {
                        FieldInfo field = (FieldInfo)item;
                        newValue = updateHandler(obj, field, field.GetValue(obj));
                        field.SetValue(obj, newValue);
                    }
                }
            }

        }

        /// <summary>
        /// Creates an instance of a generic type
        /// </summary>
        /// <param name="genericMainType">The main type, e.g List[]</param>
        /// <param name="genericParameters">The parameter type, e.g int.</param>
        /// <returns></returns>
        public static object CreateGenericType(Type genericMainType, IEnumerable<Type> genericParameters)
        {
            Type t = GetGenericType(genericMainType, genericParameters);
            return CreateObjectWithPrivateConstructor(t);
        }
        /// <summary>
        /// Creates an instance of a generic type
        /// </summary>
        /// <param name="genericMainType">The main type, e.g List[]</param>
        /// <param name="genericParameters">The parameter type, e.g int.</param>
        /// <returns></returns>
        public static object CreateGenericType(Type genericMainType, params Type[] genericParameters)
        {
            return CreateGenericType(genericMainType, (IEnumerable<Type>)genericParameters);
            
        }
        /// <summary>
        /// Returns a Generic Type. E.g List Of Int
        /// </summary>
        /// <param name="genericMainType"> The main type, e.g List[] </param>
        /// <param name="genericParameters">The parameter type, e.g int.</param>
        /// <returns></returns>
        public static Type GetGenericType(Type genericMainType, params Type[] genericParameters)
        {
            return GetGenericType(genericMainType, (IEnumerable<Type>)genericParameters);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="genericMainType">The main type, e.g List[]</param>
        /// <param name="genericParameters">The parameter type, e.g int.</param>
        /// <returns></returns>
        public static Type GetGenericType(Type genericMainType, IEnumerable<Type> genericParameters)
        {
            List<Type> listParmas = new List<Type>();
            listParmas.AddRange(genericParameters);
            Type result = genericMainType.MakeGenericType(listParmas.ToArray());

            return result;
        }




      


        /// <summary>
        /// Copies all the properties of a type, from the object 'from', to the object 'to'.
        /// </summary>
        /// <param name="t"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        public static void CopyProperties<T>(T from, T to)
        {
            Type fromType = typeof(T);
            
            //toType = to.GetType();
            {
                System.Reflection.PropertyInfo[] fromProps;

                fromProps = ReflectionUtil.GetAllPropertiesForType(fromType, loadNonPublicTypes: true, loadStaticProperties: false, loadAlsoFromInterfaces: true);

                for (int i = 0; i < fromProps.Length; i++)
                {
                    System.Reflection.PropertyInfo fromProp = fromProps[i];
                    try
                    {
                        if (fromProp.CanWrite)
                        {
                            
                            
                            object fromVal = fromProp.GetValue(from, null);
                            fromProp.SetValue(to, fromVal, null);
                            
                        }
                        //System.Reflection.PropertyInfo toProp = toType.GetProperty(fromProp.Name);
                        //if (fromProp.CanWrite && toProp != null && toProp.PropertyType == fromProp.PropertyType)
                        //{
                        //    object toCasted = Convert.ChangeType(to, fromType);

                        //    object fromVal = fromProp.GetValue(from, null);
                        //    toProp.SetValue(to, fromVal, null);
                        //}
                    }
                    catch
                    {

                    }


                }
            }
            

        }

        public static void CopyPropertiesAndFields<T>(T from, T to)
        {
            Type fromType = typeof(T);

            //toType = to.GetType();
            {

                var items = ReflectionUtil.GetAllPropertiesAndFieldsForType(fromType, loadNonPublicTypes: true, loadStaticProperties: false, loadAlsoFromInterfaces: true);

                foreach (var item in items)
                {
                    try
                    {

                        if (item is PropertyInfo)
                        {
                            PropertyInfo fromProp = (PropertyInfo)item;
                            if (fromProp.CanWrite)
                            {
                                object fromVal = fromProp.GetValue(from, null);
                                fromProp.SetValue(to, fromVal, null);
                            }
                        }
                        else if (item is FieldInfo)
                        {
                            FieldInfo field = (FieldInfo)item;
                            object fromVal = field.GetValue(from);
                            field.SetValue(to, fromVal);
                        }
                        
                    }
                    catch
                    {

                    }


                }
            }


        }

        

        /// <summary>
        /// If this will be called multiple times, create an instance of 'LastInheritanceChainTypeFinder' as it will cache base-types
        /// </summary>
        /// <param name="baseType"></param>
        /// <returns></returns>
        public static Type GetLastTypeInInheritanceChain(Type baseType, IEnumerable<Assembly> assemblies)
        {
            if (assemblies == null)
            {
                assemblies = CS.General_v3.Classes.Application.AppInstance.Instance.GetProjectAssemblies();
            }
            LastInheritanceChainTypeFinder.Instance.AddAssembliesToCheck(assemblies);
            var t = LastInheritanceChainTypeFinder.Instance.GetLastTypeFor(baseType);
            if (t == null || t.IsAbstract)
            {
                int k = 5;
            }
            return t;

        }
        public static object CreateObject(Type t)
        {

            return CreateObjectWithPrivateConstructor(t);
        }
        public static T CreateObjectWithPrivateConstructor<T>()
        {
            return (T)CreateObjectWithPrivateConstructor(typeof(T));

        }
        public static T CreateObjectWithPrivateConstructorForLastType<T>()
        {
            Type t = typeof(T);
            return (T)CreateObjectWithPrivateConstructorForLastType(t);
        }

        public static object CreateObjectWithPrivateConstructorForLastType(Type t)
        {
            Type lastType = GetLastTypeInInheritanceChain(t, null);
            return CreateObjectWithPrivateConstructor(lastType);
        }

        public static object CreateObjectWithPrivateConstructor(Type t)
        {
            ConstructorInfo constructor = null;

            try
            {
                // Binding flags exclude public constructors.
                constructor = t.GetConstructor(BindingFlags.Instance |
                                BindingFlags.NonPublic | BindingFlags.Public, null, new Type[0], null);
            }
            catch (Exception exception)
            {
                throw;
            }

            if (constructor == null || constructor.IsAssembly)
                // Also exclude internal constructors.
                throw new InvalidOperationException(string.Format("A private or " +
                        "protected constructor is missing for '{0}'.", t.Name));
            object _instance = null;
            try
            {
                _instance = constructor.Invoke(null);
            }
            catch (MemberAccessException ex)
            {
                var m = Regex.Match(ex.Message, "Cannot create an instance of (.*?)Factory because it is an abstract class.");
                if (m.Success)
                {
                    string factoryName = m.Groups[1].Value + "Factory";
                    string msg = ex.Message +
                                 " - If you are using this factory, make sure you mark it as 'UsedInProject' in the builder";
                    throw new InvalidOperationException(msg);
                }
                else
                {
                    throw;
                }


            }
            catch (Exception ex)
            {
                throw;
            }
            return _instance;
        }
        public static string GetNameFromType(Type t)
        {
            return t.Name;
        }
        public static string GetFullNameFromType(Type t, bool addAssembly = false)
        {
            string s = t.AssemblyQualifiedName;
            return s;
        }
        
        public static PropertyInfo GetPropertyBySelector(Expression selector)
        {
            return (PropertyInfo)GetMemberInfoBySelector(selector);


        }
        public static MethodInfo GetMethodBySelector(Expression selector)
        {
            return (MethodInfo)GetMemberInfoBySelector(selector);

        }
        public static MemberInfo GetMemberInfoBySelector(Expression selector)
        {
            Expression body = selector;
            if (body is LambdaExpression)
            {
                body = ((LambdaExpression)body).Body;
            }
            switch (body.NodeType)
            {
                case ExpressionType.Call:
                    return (((System.Linq.Expressions.MethodCallExpression)body).Method);
                case ExpressionType.MemberAccess:
                    return ((PropertyInfo)((MemberExpression)body).Member);
                case ExpressionType.Convert:
                    {
                        UnaryExpression unaryExp = (UnaryExpression)body;
                        var op = unaryExp.Operand;
                        System.Linq.Expressions.MemberExpression memberExp = (MemberExpression) op;
                        return memberExp.Member;
                        
                    }
                default:
                    throw new InvalidOperationException("Cannot retrieve member info from selector!");
            }


        }
        public static object GetPropertyOrFieldValue(MemberInfo property, object o)
        {
            if (property is PropertyInfo)
            {
                return ((PropertyInfo)property).GetValue(o, null);
            }
            else if (property is FieldInfo)
            {
                return ((FieldInfo)property).GetValue(o);

            }
            else
            {
                return null;
            }
        }

        public static object GetValueForObjectBySelector(object o, Expression selector)
        {
            object result = null;
            MemberInfo memberInfo = GetMemberInfoBySelector(selector);
            if (memberInfo is PropertyInfo)
            {
                PropertyInfo pInfo = (PropertyInfo)memberInfo;
                result =pInfo.GetValue(o, null);
            }
            else if (memberInfo is MethodInfo)
            {
                MethodInfo mInfo = (MethodInfo)memberInfo;
                result = mInfo.Invoke(o, null);

            }
            else
            {
                throw new InvalidOperationException("Cannot retrieve member info from selector!");
            }
            return result;

        }

        /// <summary>
        /// Returns all types with the given attribute
        /// </summary>
        /// <param name="attribType"></param>
        /// <param name="a">Assembly to load types from.  If left null, entry assembly is selected</param>
        /// <returns></returns>
        public static List<Type> GetAllTypesWithAttribute(Type attribType, Assembly a = null, bool checkInherited = false)
        {
            if (a == null) a = Assembly.GetEntryAssembly();
            List<Type> list = new List<Type>();
            list.AddRange(a.GetTypes());
            for (int i = 0; i < list.Count; i++)
            {
                var c = list[i];
                if (!CheckIfTypeContainsAttribute(c, attribType))
                {
                    list.RemoveAt(i);
                    i--;
                }
            }
            return list;
        }

        public static bool CheckIfTypeContainsAttribute(Type classType, Type attribType, bool checkInherited = false)
        {
            object[] attribs = classType.GetCustomAttributes(attribType, checkInherited);
            return attribs.Length > 0;
        }
        public static bool CheckIfPropertyContainsAttribute<TAttribType>(PropertyInfo propertyInfo, bool checkInherited = false)
        {
            Type attribType = typeof(TAttribType);
            return CheckIfPropertyContainsAttribute(propertyInfo, attribType, checkInherited);
        }

        public static bool CheckIfPropertyContainsAttribute(PropertyInfo propertyInfo, Type attribType, bool checkInherited = false)
        {
            
            object[] attribs = propertyInfo.GetCustomAttributes(attribType, checkInherited);
            return attribs.Length > 0;
        }

        public static Type GetTypeFromExecutingAssemblies(string className)
        {
            foreach (var a in CS.General_v3.Classes.Application.AppInstance.Instance.GetProjectAssemblies())
            {
                var t = GetTypeFromAssembly(a, className);
                if (t != null)
                    return t;
            }
            return null;
        }
        public static List<Type> GetTypesFromExecutingAssemblies(IEnumerable<string> classNames)
        {
            List<Type> list = new List<Type>();
            foreach (var className in classNames)
            {
                if (!string.IsNullOrWhiteSpace(className))
                {
                    string s = className.Trim();
                    foreach (var a in CS.General_v3.Classes.Application.AppInstance.Instance.GetProjectAssemblies())
                    {
                        var t = GetTypeFromAssembly(a, className);
                        if (t != null)
                            list.Add(t);
                            
                    }
                }

            }
            return list;

            return null;
        }

        public static List<Type> GetAllTypesWhichExtendOrImplement(Type typeToExtendOrImplement, bool dontLoadAbstractClasses = true)
        {
            List<Type> results = new List<Type>();
            List<Type> types = new List<Type>();
            foreach (var a in GetExecutingAssemblies())
            {
                types.AddRange(a.GetTypes());
            }
            foreach (var t in types)
            {
                if (((dontLoadAbstractClasses == false) ||  (dontLoadAbstractClasses && !t.IsAbstract)) &&
                    CheckIfTypeExtendsFrom(t, typeToExtendOrImplement))
                {
                    results.Add(t);
                }
            }
            return results;
        }

        public static Type GetTypeFromAssembly(Assembly a, string className)
        {
            {
                string pattern = @"(.*?)(\<.*?\>)";
                var match = Regex.Match(className, pattern, RegexOptions.Singleline);
                if (match.Success)
                {
                    string s = match.Groups[2].Value;
                    int cnt = 1;
                    for (int i = 0; i < s.Length; i++)
                    {
                        if (s[i] == ',')
                            cnt++;
                    }

                    className = match.Groups[1].Value + "`" + cnt;
                }
            }
            return a.GetType(className);
        }

        /// <summary>
        /// Returns the C# code for the data type, e.g System.Object.  Works also for generic Lists, e.g IList<Karl>
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetCSharpCodeForDataType(Type type)
        {
            string s = type.FullName;
            if (type.IsGenericType)
            {
                string typeName = type.FullName.Substring(0, type.FullName.IndexOf("`"));
                typeName += "<" + type.GetGenericArguments()[0].FullName + ">";
                s = typeName;
            }
            return s;
        }
        public static MethodInfo GetMethodFromType(Type type, string methodName)
        {
            return type.GetMethod(methodName, getBindingFlags(true, true));
        }
        private static BindingFlags getBindingFlags(bool loadNonPublicTypes, bool loadStaticProperties)
        {
            BindingFlags flags;
            flags = BindingFlags.Instance | BindingFlags.Public;
            if (loadStaticProperties) flags = flags | BindingFlags.Static;
            if (loadNonPublicTypes) flags = flags | BindingFlags.NonPublic;


            return flags;
        }
        public static List<MemberInfo> GetAllPropertiesAndMethodsForType(Type t, bool loadNonPublicTypes, bool loadStaticProperties,
            bool loadAlsoFromInterfaces)
        {
            List<MemberInfo> list = new List<MemberInfo>();
            list.AddRange(GetAllPropertiesForType(t, loadNonPublicTypes, loadStaticProperties, loadAlsoFromInterfaces));
            list.AddRange(GetAllMethodsForType(t, loadNonPublicTypes,loadStaticProperties));
            return list;

        }
        public static PropertyInfo[] GetAllPropertiesForType(Type t, bool loadNonPublicTypes, bool loadStaticProperties, 
            bool loadAlsoFromInterfaces)
        {
            HashedSet<PropertyInfo> properties = new HashedSet<PropertyInfo>();
            HashedSet<Type> typesToLoad = new HashedSet<Type>();
            typesToLoad.Add(t);
            if (!t.IsClass && loadAlsoFromInterfaces)
            {
                typesToLoad.AddAll(t.GetInterfaces().ToList());

            }
            foreach (var type in typesToLoad)
            {

                var pList = type.GetProperties(getBindingFlags(
                    loadNonPublicTypes: loadNonPublicTypes,
                    loadStaticProperties: loadStaticProperties));
                properties.AddAll(pList);
            }
            return properties.ToArray();

        }
        public static List<MemberInfo> GetAllPropertiesAndFieldsForType(Type t, bool loadNonPublicTypes, bool loadStaticProperties, bool loadAlsoFromInterfaces)
        {
            List<MemberInfo> propsAndFields = new List<MemberInfo>();
            propsAndFields.AddRange(GetAllPropertiesForType(t, loadNonPublicTypes, loadStaticProperties, loadAlsoFromInterfaces));
            propsAndFields.AddRange(GetAllFieldsForType(t));
            return propsAndFields;


        }

        public static List<FieldInfo> GetAllFieldsForType(Type t)
        {
            List<FieldInfo> fields = new List<FieldInfo>();

            //propsAndFields.AddRange(t.GetProperties(getBindingFlags(loadNonPublicTypes: loadNonPublicTypes, loadStaticProperties: loadStaticProperties)));
            fields.AddRange(t.GetFields());

            return fields;

        }

        public static MethodInfo[] GetAllMethodsForType(Type t, bool loadNonPublicTypes, bool loadStaticProperties)
        {
            return t.GetMethods(getBindingFlags(
                                                                loadNonPublicTypes: loadNonPublicTypes,
                                                                loadStaticProperties: loadStaticProperties));

        }
        public static PropertyInfo GetPropertyForType(Type t, string propertyName, bool loadNonPublicTypes, bool loadStaticProperties)
        {
            return t.GetProperty(propertyName, getBindingFlags(
                                                                loadNonPublicTypes: loadNonPublicTypes,
                                                                loadStaticProperties: loadStaticProperties));

        }
        public static IEnumerable<Assembly> GetExecutingAssemblies(bool excludeDotNetAssemblies = true)
        {
            List<Assembly> list = new List<Assembly>();
            list.AddRange(AppDomain.CurrentDomain.GetAssemblies());
            if (excludeDotNetAssemblies)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    var a = list[i];
                    if (CheckIfAssemblyIsADotNetFrameworkOne(a))
                    {
                        list.RemoveAt(i);
                        i--;
                    }
                }
            }
            return list;
        }
        /// <summary>
        /// Returns whether a type is a subclass of a generic type.  You cannot use the standard 'IsSubclassOf' to do this check as it will not work
        /// For example, checks whether a FrontendFactory is derived from baseFrontendFactory<,,,,>
        /// </summary>
        /// <param name="typeToCheck"></param>
        /// <param name="genericClass"></param>
        /// <returns></returns>
        public  static bool CheckIfTypeIsSubclassOfGenericType(Type typeToCheck, Type genericClass)
        {
            while (typeToCheck != typeof(object))
            {
                var cur = typeToCheck.IsGenericType ? typeToCheck.GetGenericTypeDefinition() : typeToCheck;
                if (genericClass == cur)
                {
                    return true;
                }
                typeToCheck = typeToCheck.BaseType;
            }
            return false;
        }

        public static bool CheckIfAssemblyIsADotNetFrameworkOne(Assembly a)
        {
            bool ok = false;
            List<string> namespaceHints = new List<string>();
            namespaceHints.Add("System");
            namespaceHints.Add("mscorlib");
            namespaceHints.Add("nunit");
            namespaceHints.Add("mysql");
            namespaceHints.Add("nhibernate");
            namespaceHints.Add("dynamicproxygenAssemblyV2");
            namespaceHints.Add("FluentNHibernate");
            namespaceHints.Add("iesi");
            namespaceHints.Add("castle");
            namespaceHints.Add("Microsoft");
            namespaceHints.Add("AxInterop");
            namespaceHints.Add("log4net");
            namespaceHints.Add("TestDriven");
            namespaceHints.Add("2ahmrrbh");
            for (int i = 0; i < namespaceHints.Count; i++)
            {
                if (a.FullName.ToLower().StartsWith(namespaceHints[i].ToLower() + ".") ||
                    (a.FullName.ToLower().StartsWith(namespaceHints[i].ToLower() + ",")))
                {
                    ok = true;
                    break;
                }
            }
            return ok;

        }
        public static List<PropertyInfo> GetAllPropertiesWithAttributesFromExecutingAssemblies(IEnumerable<Type> attribTypeList = null, bool excludeDotNetAssemblies = true)
        {

            IEnumerable<Assembly> assemblies = null;
            assemblies = GetExecutingAssemblies(excludeDotNetAssemblies);

            return GetAllPropertiesWithAttributes(attribTypeList, assemblies);
        }
        public static List<PropertyInfo> GetAllPropertiesWithAttributes(IEnumerable<Type> attribTypeList = null, IEnumerable<Assembly> assembliesToLoadTypesFrom = null)
        {
         //   System.Diagnostics.Contracts.Contract.Requires(attribType != null || (attribTypeList != null && attribTypeList.Count() > 0), "At least one attribute type must be filled in");
            //System.Diagnostics.Contracts.Contract.Requires(assembly != null || (assembliesToLoadTypesFrom != null && assembliesToLoadTypesFrom.Count() > 0), "At least one attribute type must be filled in");

            List<Assembly> assemblyList = new List<Assembly>();
           // if (assembly != null) assemblyList.Add(assembly);
            if (assembliesToLoadTypesFrom != null) assemblyList.AddRange(assembliesToLoadTypesFrom);

            List<Type> attribList = new List<Type>();
          //  if (attribType != null) attribList.Add(attribType);
            if (attribTypeList != null) attribList.AddRange(attribTypeList);
            List<PropertyInfo> properties = new List<PropertyInfo>();
            foreach (var a in assemblyList)
            {
                try
                {
                    foreach (var type in a.GetTypes())
                    {
                        var typeProperties = GetAllPropertiesWithAttributes(type, attribList);
                        properties.AddRange(typeProperties.ConvertAll<PropertyInfo>(x=>x.Property));
                    }
                }
                catch (Exception ex)
                {
                    //this can occur?
                    //sometimes i got a type load error due to Assembly
                }
            }

            return properties;
        }
      
        public static List<T> GetAllAttributesMappedToPropertiesAndMethodsInType<T>(Type t) where T: Attribute
        {
            List<T> result = new List<T>();
            {
                var bindingFlags = getBindingFlags(true, false);

                PropertyInfo[] propertyList = t.GetProperties(bindingFlags | BindingFlags.FlattenHierarchy);

                foreach (var currProperty in propertyList)
                {
                    bool ok = false;


                    object[] foundAttribs = currProperty.GetCustomAttributes(typeof (T), true);
                    result.AddRange(foundAttribs.Cast<T>());
                }
            }
            {
                var bindingFlags = getBindingFlags(true, false);

                MethodInfo[] methodList = t.GetMethods(bindingFlags | BindingFlags.FlattenHierarchy);

                foreach (var method in methodList)
                {
                    bool ok = false;


                    object[] foundAttribs = method.GetCustomAttributes(typeof(T), true);
                    result.AddRange(foundAttribs.Cast<T>());
                }
            }

            return result;

        }

        public static List<PropertyAttributeInfo> GetAllPropertiesWithAttributes(Type typeToLoadPropsFrom, Type attribType)
        {
            List<Type> list = new List<Type>();
            list.Add(attribType);
            return GetAllPropertiesWithAttributes(typeToLoadPropsFrom, list);
        }
        
        public static List<PropertyAttributeInfo> GetAllPropertiesWithAttributes(Type typeToLoadPropsFrom, IEnumerable<Type> attribTypes)
        {
            var bindingFlags = getBindingFlags(true, false);
            PropertyInfo[] propertyList = typeToLoadPropsFrom.GetProperties(bindingFlags | BindingFlags.FlattenHierarchy);
            List<PropertyAttributeInfo> list = new List<PropertyAttributeInfo>();
            foreach (var currProperty in propertyList)
            {
                bool ok = false;
                foreach (var currentA in attribTypes)
                {
                    object[] foundAttribs = currProperty.GetCustomAttributes(currentA, true);
                    if (foundAttribs.Length > 0)
                    {
                        ok = true;
                        PropertyAttributeInfo info = new PropertyAttributeInfo();
                        info.Property = currProperty;
                        info.Attributes.AddRange(foundAttribs.ToList().Cast<Attribute>());
                        list.Add(info);
                        break;
                    }
                }
                
            }
            return list;
        }
        public static TItem GetAttributeForMember<TItem>(MemberInfo p)
        {
            object[] list = p.GetCustomAttributes(typeof(TItem), true);
            if (list.Length > 0)
                return (TItem)list[0];
            else
                return default(TItem);
        }
        

        private static PropertyInfo getPropertyInfo(Type t, string propertyName, Type propertyReturnType = null)
        {
            PropertyInfo pInfo = null;
            if (propertyReturnType != null)
                pInfo = t.GetProperty(propertyName, propertyReturnType);
            else
                pInfo = t.GetProperty(propertyName);
            return pInfo;
        }

        public static object GetPropertyValueByName(object obj, string propertyName, Type propertyReturnType = null)
        {
            Type t = obj.GetType();
            PropertyInfo pInfo = getPropertyInfo(t, propertyName, propertyReturnType);
            if (pInfo == null)
                throw new InvalidOperationException("Property with name <" + propertyName + "> does not exist on Type <" + t.ToString() + ">");
            return pInfo.GetValue(obj, null);
        }
        public static void SetPropertyValueByName(object obj, string propertyName, object newValue, Type propertyReturnType = null)
        {
            Type t = obj.GetType();
            PropertyInfo pInfo = getPropertyInfo(t, propertyName, propertyReturnType);
            if (pInfo == null)
                throw new InvalidOperationException("Property with name <" + propertyName + "> does not exist on Type <" + t.ToString() + ">");
            pInfo.SetValue(obj, newValue, null);
        }
        /// <summary>
        /// This is the equivalent of doing 'obj is baseType'
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="baseType"></param>
        /// <returns></returns>
        public static bool CheckIfObjectExtendsFrom(object obj, Type baseType)
        {
            return CheckIfTypeExtendsFrom(obj.GetType(), baseType);
        }

        /// <summary>
        /// This is the equivalent of doing 'objType is baseType'
        /// </summary>
        /// <param name="objType"></param>
        /// <param name="baseType"></param>
        /// <returns></returns>
        public static bool CheckIfTypeExtendsFrom(Type objType, Type baseType)
        {
            return baseType.IsAssignableFrom(objType);

        }
        public static Type GetTypeWhichExtendsFromAndIsNotAbstract2(Assembly assembly, Type baseType)
        {
            var types = assembly.GetTypes();
            for (int i = 0; i < types.Length; i++)
            {
                var t = types[i];
                if (!t.IsAbstract && (t.IsAssignableFrom(baseType) || baseType.IsAssignableFrom(t)))
                {
                    return t;
                }
            }
            return null;
        }


        public static void SerializeObjectToFile(object obj, string path)
        {
            CS.General_v3.Util.IO.CreateDirectory(path);
            var fs = File.Open(path, FileMode.Create, FileAccess.Write);
                BinaryFormatter bf = new BinaryFormatter();

                bf.Serialize(fs, obj);
            
                fs.Dispose();
            
        }
        public static object DeserializeObjectFromFile(string path)
        {
            object o = null;
            if (File.Exists(path))
            {
                var fs = File.Open(path, FileMode.Open, FileAccess.Read);

                var bf = new BinaryFormatter();
                try
                {
                    o = bf.Deserialize(fs);
                }
                catch (Exception ex)
                {
                    o = null;
                }
                fs.Dispose();
            }
            return o;
        }
        public static T DeserializeObjectFromFile<T>(string path)
        {
            return (T)DeserializeObjectFromFile(path);

        }

        public static bool CheckIfTypeIsNullable(Type enumType)
        {
            return enumType.IsGenericType && enumType.GetGenericTypeDefinition() == typeof(System.Nullable<>);
            
        }
        public static bool CheckIfTypeAllowsNullValue(Type enumType)
        {
            bool b = !enumType.IsPrimitive || CheckIfTypeIsNullable(enumType);
            return b;

        }

        /// <summary>
        /// Checks whether a type is an enumeration.  If it is a nullable enum, it can check as well.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="checkAlsoIfNullable"></param>
        /// <returns></returns>
        public  static bool CheckIfTypeIsEnum(Type type, bool checkAlsoIfNullable = true)
        {
            bool isEnum = false;
            if (type.IsEnum)
                isEnum = true;
            else
            {
                if (checkAlsoIfNullable && CheckIfTypeIsNullable(type))
                {
                    var enumType = type.GetGenericArguments()[0];
                    if (enumType.IsEnum)
                    {
                        isEnum = true;
                    }
                }
            }
            return isEnum;
            
        }
        /// <summary>
        /// Returns the previous executing method, i.e 2 methods before this call.
        /// </summary>
        /// <returns></returns>
        public static string GetPreviousExecutingMethodName()
        {
            StackTrace stackTrace = new StackTrace();
            string methodName = null;
            MethodBase method = null;
            if (stackTrace.FrameCount > 2)
            {
                var frame = stackTrace.GetFrame(2);
                method = frame.GetMethod();
            }
            if (method != null)
            {
                methodName = method.DeclaringType.FullName + "." + method.Name;
            }
            return methodName;
            
        }
    }
}
