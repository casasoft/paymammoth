﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CS.General_v3.Util
{
    public static class SessionUtil
    {
        /// <summary>
        /// Retrieve an item from session and remove it.  Used to transmit messages
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetMessageAndRemove(string key)
        {
            object msg = GetObjectAndRemove(key);
            string sMsg = null;
            if (msg != null)
            {

                sMsg = msg.ToString();
                
            }

            return sMsg;
        }
        public static void RemoveObject(string key)
        {
            CS.General_v3.Util.PageUtil.ClearSessionObject(key);
        }
        public static object GetObjectAndRemove(string key)
        {
            object msg = System.Web.HttpContext.Current.Session[key];
            RemoveObject(key);

            return msg;
        }
        /// <summary>
        /// Set a message in the Session
        /// </summary>
        /// <param name="key"></param>
        /// <param name="message"></param>
        public static void SetMessage(string key, string message)
        {
            CS.General_v3.Util.PageUtil.SetSessionObject(key, message);
        }
        public static object GetObject(string key)
        {
            return CS.General_v3.Util.PageUtil.GetSessionObject(key);
        }
        public static T GetObject<T>(string key)
        {
            return (T)CS.General_v3.Util.PageUtil.GetSessionObject(key);
        }
    

        public static void SetObject(string key, string o)
        {
            CS.General_v3.Util.PageUtil.SetSessionObject(key, o);
        }
        public static void SetObject(string key, DateTime? o)
        {
            CS.General_v3.Util.PageUtil.SetSessionObject(key, o);
        }
        public static void SetObject(string key, double? o)
        {
            CS.General_v3.Util.PageUtil.SetSessionObject(key, o);
        }
        public static void SetObject(string key, long? o)
        {
            CS.General_v3.Util.PageUtil.SetSessionObject(key, o);
        }
        public static void SetObject(string key, int? o)
        {
            CS.General_v3.Util.PageUtil.SetSessionObject(key, o);
        }
        public static void SetObject(string key, float? o)
        {
            CS.General_v3.Util.PageUtil.SetSessionObject(key, o);
        }
        public static void SetObject(string key, byte? o)
        {
            CS.General_v3.Util.PageUtil.SetSessionObject(key, o);
        }
        public static void SetObject(string key, bool? o)
        {
            CS.General_v3.Util.PageUtil.SetSessionObject(key, o);
        }

        public static void SetObject(string key, Enum o)
        {
            CS.General_v3.Util.PageUtil.SetSessionObject(key, o);
        }
        public static void SetObject<T>(string key, List<T> o)
        {
            CS.General_v3.Util.PageUtil.SetSessionObject(key, o);
        }
    }
}
