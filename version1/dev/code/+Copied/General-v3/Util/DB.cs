﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Util
{
    public static class DB
    {
        /// <summary>
        /// Retrieves only the column name, e.g from 'Students.Name' returns 'name';
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string GetOnlyColName(string s)
        {
            return s.Substring(s.LastIndexOf('.') + 1);
        }
    }
}
