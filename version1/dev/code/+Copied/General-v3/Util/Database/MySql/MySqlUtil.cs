﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

namespace CS.General_v3.Util.Database.MySql
{
    public static class MySqlUtil
    {
        

        public static DbConnection CreateConnection(string host, string database, string user, string pass, int? port, string charset)
        {
            string connstr = GetConnectionString(host, database, user, pass, port, charset);

            return CreateConnection(connstr);
        }
        public static string GetConnectionString(string host, string database, string user, string pass, int? port, string characterSet)
        {
            string charSet = "";
            if (port <= 0) port = null;
            if (!string.IsNullOrEmpty(characterSet))
            {
                charSet = "charset=" + characterSet + ";";
            }

            string connstr = "Data Source=" + host + ";Database="
                        + database + ";User Id=" + user
                                       + ";Password=\"" + pass + "\";" + charSet + "Allow Zero Datetime=Yes;";
            if (port.HasValue)
            {
                connstr += "Port=" + port.Value + ";";
            }
            return connstr;

        }
        public static System.Data.Common.DbConnection CreateConnection(string connectionString)
        {
            string connstr = connectionString;
            MySqlConnection conn = null;
            bool retry = false;
            int times = 0;
            do
            {
                try
                {
                    conn = new MySqlConnection(connstr);
                    retry = false;
                }
                catch (MySqlException ex)
                {
                    if (ex.Message.Trim().ToLower() == "too many connections")
                    {
                        retry = (times < 10);
                        times++;
                        if (!retry)
                        {
                            throw ex;
                        }
                        else
                        {
                            System.Threading.Thread.Sleep(500); //wait half a second
                        }

                    }
                    else
                    {
                        throw ex;
                    }
                }
            } while (retry);
            //conn.Open();
            return conn;
        }
    }
}
