using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using System.Security.Cryptography.X509Certificates;
using System.Collections.Specialized;
using log4net;

namespace CS.General_v3.Util
{
    public static class Web
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(Web));

        public static HttpResponse Response
        {
            get
            {
                return (HttpResponse)System.Web.HttpContext.Current.Response;
            }
        }
        /// <summary>
        /// Forces a download through the browser. Sets the content type as 'application/EXT'
        /// </summary>
        /// <param name="filePath">Local Path of file not with 'http://' but local path with C://.... - User Server.MapPath</param>
        public static bool ForceDownload(string filePath)
        {
            string filename = "";
            filename = filePath.Substring(filePath.LastIndexOf("\\") + 1);
            string ext = "";
            if (filePath.LastIndexOf(".") > -1)
                ext = filePath.Substring(filePath.LastIndexOf(".") + 1);
            else
                ext = "xxx";
            return ForceDownload(filePath, CS.General_v3.Util.Data.GetMIMEContentType(filename),filename);
        }

        /// <summary>
        /// Forces a download through the browser. 
        /// </summary>
        /// <param name="filePath">Local Path of file</param>
        /// <param name="Response">Response</param>
        /// <param name="contentType">Content type, example: application/pdf, movie/mpg.  You can use CS.General_v3.Util.Data.getMIMEContentType()</param>
        public static bool ForceDownload(string filePath, string contentType)
        {
            string filename = "";
            filename = filePath.Substring(filePath.LastIndexOf("\\") + 1);
            return ForceDownload(filePath, contentType, filename);
        }
        /// <summary>
        /// Forces a download through the browser. 
        /// </summary>
        /// <param name="filePath">Local Path of file</param>
        /// <param name="Response">Response</param>
        /// <param name="contentType">Content type, example: application/pdf, movie/mpg.  You can use CS.General_v3.Util.Data.getMIMEContentType()</param>
        /// <param name="filename">The filename of the item</param>
        public static bool ForceDownload(string filePath,string contentType, string filename)
        {
            try
            {
                if (string.IsNullOrEmpty(contentType))
                    contentType = CS.General_v3.Util.Data.GetMIMEContentType(filename);


                //string filename = "";
                //filename = path.Substring(path.LastIndexOf("\\") + 1);
                Response.Clear();
                Response.AddHeader("Content-disposition", "attachment; filename=\"" + filename + "\"");
                Response.AddHeader("Content-Type", contentType);
                Response.ContentType = contentType;

                Response.TransmitFile(filePath);

                Response.End();
                return true;
            }
            catch (FileNotFoundException ex)
            {
                return false;
            }


        }

        /// <summary>
        /// Forces a download through the browser. 
        /// </summary>
        /// <param name="data">Character array containing data</param>
        /// <param name="Response">Response</param>
        /// <param name="contentType">Content type, example: application/pdf, movie/mpg.  You can use CS.General_v3.Util.Data.getMIMEContentType(). If null, takes default value</param>
        /// <param name="filename">The filename of the item</param>
        public static bool ForceDownload(char[] data, string contentType, string filename)
        {
            
            byte[] b = new byte[data.Length];
            for (int i = 0; i < b.Length; i++)
            {
                b[i] = (byte)data[i];
            }
            return ForceDownload(b,  contentType, filename);
            
        }

        

        /// <summary>
        /// Forces a download through the browser. 
        /// </summary>
        /// <param name="data">Byte array containing data</param>
        /// <param name="Response">Response</param>
        /// <param name="contentType">Content type, example: application/pdf, movie/mpg.  You can use CS.General_v3.Util.Data.getMIMEContentType().  If null, takes default value</param>
        /// <param name="filename">The filename of the item</param>
        public static bool ForceDownload(byte[] data, string contentType, string filename)
        {
            try
            {
                if (contentType == null)
                    contentType = CS.General_v3.Util.Data.GetMIMEContentType(filename);

                //string filename = "";
                //filename = path.Substring(path.LastIndexOf("\\") + 1);
                Response.Clear();
                Response.AddHeader("Content-disposition", "attachment; filename=\"" + filename + "\"");
                Response.AddHeader("Content-Type", contentType);
                Response.ContentType = contentType;

                Response.BinaryWrite(data);
                Response.End();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        
        /// <summary>
        /// Does an HTTP get and returns the result
        /// </summary>
        /// <param name="url">URL</param>
        /// <param name="contentType">Content-Type, example 'text/xml'. Can be left null</param>
        /// <param name="headers">List of headers to append. If left null, no headers are added, just the default ones</param>
        /// <returns></returns>
        public static string HTTPGet(string url, string contentType = null, WebHeaderCollection headers = null, NameValueCollection qsParams = null,
            string userAgent = "UserAgent")
        {
            string respContentType = "";
            return HTTPGet(url, contentType, headers, qsParams ,out respContentType, userAgent: userAgent);
        }
        /// <summary>
        /// Does an HTTP get and returns the result
        /// </summary>
        /// <param name="url">URL</param>
        /// <param name="contentType">Content-Type, example 'text/xml'. Can be left null</param>
        /// <param name="headers">List of headers to append. If left null, no headers are added, just the default ones</param>
        /// <returns></returns>
        public static string HTTPGet(string url, string contentType, WebHeaderCollection headers, NameValueCollection qsParams, out string responseContentType,
            
            string userAgent = null)
        {
            CS.General_v3.Classes.URL.URLClass pgUrl = new CS.General_v3.Classes.URL.URLClass(url);
            pgUrl.AddQuerystringParamsFromNameValueColl(qsParams);
            string urlToPostTo = pgUrl.GetURL(fullyQualified: true);
            WebResponse response = HTTPRequest(urlToPostTo, Enums.HTTP_METHOD.GET, headers, (string)null, contentType, null, null, userAgent: userAgent);
            responseContentType = response.ContentType;
            StreamReader sr = new StreamReader(response.GetResponseStream());
            string respStr = sr.ReadToEnd();
            sr.Close(); sr.Dispose();

            response.Close();
            return respStr;
        }

        

        /// <summary>
        /// Submits an HTTP request, as a POST and with default headers
        /// </summary>
        /// <param name="URL">url to send to</param>
        /// <param name="formParameters">form paramters as a name value collection</param>
        /// <returns>the response</returns>
        public static string HTTPPost(string URL, System.Collections.Specialized.NameValueCollection formParameters)
        {
            WebHeaderCollection headers = new WebHeaderCollection();

            headers.Add("Cache-Control", "no-cache");
            //client.Headers.Add("Accept-Encoding", "gzip, deflate");
            headers.Add("Accept-Language", "en-us");
            headers.Add("Content-Type", "application/x-www-form-urlencoded");
            return HTTPPost(URL, headers, formParameters);
            
        }

        public static string HTTPPost(string URL,
            WebHeaderCollection headers, System.Collections.Specialized.NameValueCollection formParameters)
        {
            WebHeaderCollection respHeaders = null;
            return HTTPPost(URL, headers, formParameters, out respHeaders);
        }
        public static string HTTPPost(string URL,
            WebHeaderCollection headers, System.Collections.Specialized.NameValueCollection formParameters, out WebHeaderCollection responseHeaders)
        {
            WebHeaderCollection outputHeaders = null;
            string postData = ListUtil.ConvertNameValueCollectionToString(formParameters, "&", "=", (item => CS.General_v3.Util.PageUtil.UrlEncode(item)),
                (item => CS.General_v3.Util.PageUtil.UrlEncode(item)));
            return HTTPPost(URL, headers, postData, out responseHeaders);
        }

        public static string HTTPPost(string URL, WebHeaderCollection headers, string postData)
        {
            WebHeaderCollection respHeaders = null;
            return HTTPPost(URL, headers, postData, out respHeaders);
        }
        public static string HTTPPost(string URL, WebHeaderCollection headers, string postData, out WebHeaderCollection responseHeaders)
        {
            WebResponse resp = HTTPRequest(URL, Enums.HTTP_METHOD.POST, headers, postData,
                null, null, null);
            responseHeaders = resp.Headers;
            StreamReader sr = new StreamReader(resp.GetResponseStream());
            string responseStr = sr.ReadToEnd();
            sr.Close(); sr.Dispose();
            resp.Close();
            return responseStr;
        }
        
        /// <summary>
        /// Submits an HTTP request 
        /// </summary>
        /// <param name="URL">url to send to</param>
        /// <param name="method">method, one of 'POST' or 'GET'</param>
        /// <param name="headers">a collection of headers</param>
        /// <param name="postData">post data</param>
        /// <param name="contentType">content type.  If POST and null, defaults to 'application/x-www-form-urlencoded'</param>
        /// <param name="formParameters">form paramters as a name value collection</param>
        /// <param name="responseHeaders">The response headers</param>
        /// <returns>the response</returns>
        public static HttpWebResponse HTTPRequest(string URL, Enums.HTTP_METHOD method,
            WebHeaderCollection headers, NameValueCollection postData, string contentType = null, int? timeout = null, X509Certificate x509 = null,
            string userAgent = "UserAgent")
        {
            string s = CS.General_v3.Util.ListUtil.ConvertNameValueCollectionToString(postData);
            return HTTPRequest(URL, method, headers, s, contentType, timeout, x509, userAgent);
        }

        /// <summary>
        /// Submits an HTTP request 
        /// </summary>
        /// <param name="URL">url to send to</param>
        /// <param name="method">method, one of 'POST' or 'GET'</param>
        /// <param name="headers">a collection of headers</param>
        /// <param name="postData">post data</param>
        /// <param name="contentType">content type.  If POST and null, defaults to 'application/x-www-form-urlencoded'</param>
        /// <param name="formParameters">form paramters as a name value collection</param>
        /// <param name="responseHeaders">The response headers</param>
        /// <returns>the response</returns>
        public static HttpWebResponse HTTPRequest(string URL, Enums.HTTP_METHOD method, 
            WebHeaderCollection headers, string postData, string contentType = null, int? timeout = null, X509Certificate x509 = null, 
            string userAgent = "UserAgent")
        {
            postData = postData ?? "";
            
            
            
            if (!string.IsNullOrEmpty(postData) && method == Enums.HTTP_METHOD.GET)
                throw new InvalidOperationException("Cannot send a GET request and include POST data");

            if (contentType == null && headers != null && !string.IsNullOrEmpty(headers["content-type"]))
                contentType = headers["content-type"];
            if (contentType == null && method == Enums.HTTP_METHOD.POST)
                contentType = "application/x-www-form-urlencoded";
            if (!timeout.HasValue) timeout = 100000;
            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(URL);
            
            objRequest.Timeout = timeout.Value;
            if (headers != null)
            {
                for (int i=0; i < headers.AllKeys.Length;i++)
                {
                    string key = headers.AllKeys[i];
                    string value = headers[key];
                    if (key.ToLower() != "content-type")
                    {
                        objRequest.Headers[key] = value;
                    }
                        
                }
            }
            if (!string.IsNullOrEmpty(userAgent))
                objRequest.UserAgent = userAgent;
            objRequest.ContentType = contentType;
            objRequest.Method = EnumUtils.StringValueOf(method);

            //var enc = System.Text.Encoding.UTF8;

            //int totalByteCount = enc.GetByteCount(postData);
            objRequest.ContentLength = postData.Length;
            if (null != x509)
            {
                objRequest.ClientCertificates.Add(x509);
            }
            if (method == Enums.HTTP_METHOD.POST)
            {
                StreamWriter sw = new StreamWriter(objRequest.GetRequestStream());
                sw.Write(postData);
                sw.Close(); sw.Dispose();
            }
            HttpWebResponse response = (HttpWebResponse)objRequest.GetResponse();
            return response;
           

        }
       

        public static CookieCollection GetCookiesFromString(string s)
        {
            CookieCollection coll = new CookieCollection();
            string[] cookies = s.Split(',');
            for (int i = 0; i < cookies.Length; i++)
            {
                string c = cookies[i];
                string[] tokens = c.Split(';');
                string cookieValue = tokens[0].Trim();
                int equalPos = cookieValue.IndexOf('=');
                if (equalPos > -1)
                {
                    string name = cookieValue.Substring(0, equalPos);
                    string value = cookieValue.Substring(equalPos + 1);
                    coll.Add(new Cookie(name, value));
                }
            }
            return coll;
        }

        public static byte[] DownloadData(string url)
        {
            string contentType = null;
            return DownloadData(url, out contentType);

        }
        public static byte[] DownloadData(string url, out string contentType) 
        {
            if (url != null)
                url = url.Trim();
            if (!IsFromHttpOrHttps(url))
            {
                //Relative path
                contentType = CS.General_v3.Util.Data.GetMIMEContentType(url);
                return CS.General_v3.Util.IO.LoadFileAsByteArray(System.Web.HttpContext.Current.Server.MapPath(url));

            }
            else
            {
                //Online
                System.Net.WebClient webClient = new WebClient();
                byte[] by = webClient.DownloadData(url);
                contentType = webClient.ResponseHeaders["Content-Type"];
                webClient.Dispose();
                return by;
            }
        }
        public static MemoryStream DownloadDataAsStream(string url)
        {
            byte[] data = DownloadData(url);
            MemoryStream ms = new MemoryStream(data);
            return ms;
        }
        public static void DownloadFile(string url, string filename)
        {
            System.Net.WebClient webClient = new WebClient();
            webClient.DownloadFile(url, filename);
            webClient.Dispose();
            
        }
        public static bool IsFromHttpOrHttps(string url)
        {
            return url.StartsWith("http://") || url.StartsWith("https://");
        }
    }
}
