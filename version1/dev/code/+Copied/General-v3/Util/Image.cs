using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using Gif.Components;

namespace CS.General_v3.Util
{
    public static class Image
    {
        public static void AddWatermark(string imagePath, string watermarkPath,
            List<uint> TransparentColors, bool setTransparentToTopmostLeftPixel, float transparencyValue,
            CS.General_v3.Enums.CROP_IDENTIFIER watermarkPosition, int paddingFromBorder)
        {
            if (watermarkPath.Contains("/")) watermarkPath = PageUtil.MapPath(watermarkPath);
            System.Drawing.Image objWatermark = System.Drawing.Image.FromFile(watermarkPath);
            AddWatermark(imagePath, objWatermark, TransparentColors, setTransparentToTopmostLeftPixel, transparencyValue, watermarkPosition, paddingFromBorder);
            objWatermark.Dispose();
            //IO.DeleteFile(imagePath);
            //result.Save(imagePath);
            
        }
        public static void AddWatermark(string imagePath, System.Drawing.Image objWatermark,
            List<uint> TransparentColors, bool setTransparentToTopmostLeftPixel, float transparencyValue,
            CS.General_v3.Enums.CROP_IDENTIFIER watermarkPosition, int paddingFromBorder)
        {
            if (imagePath.Contains("/")) imagePath = PageUtil.MapPath(imagePath);
            System.Drawing.Image objImage = System.Drawing.Image.FromFile(imagePath);
            Bitmap result = AddWatermark(objImage, objWatermark, TransparentColors, setTransparentToTopmostLeftPixel, transparencyValue, watermarkPosition, paddingFromBorder);
            objImage.Dispose();
            IO.DeleteFile(imagePath);
            ImageFormat imageFormat = CS.General_v3.Util.Image.GetImageFormatFromFilename(imagePath);
            result.Save(imagePath, imageFormat);
            
        }
        public static Color GetColorNotInImage(System.Drawing.Bitmap bmp)
        {
            System.Collections.Hashtable ht = new System.Collections.Hashtable();
            for (int x = 0; x < bmp.Width; x++)
            {
                for (int y = 0; y < bmp.Height; y++)
                {
                    Color clr = bmp.GetPixel(x, y);
                    uint clrVal = CS.General_v3.Util.Colors.RGB.GetUIntValueFromColor(clr);
                    ht[clrVal] = clrVal;
                }
            }
            bool ok = false;
            uint rndVal = 0;
            do
            {
                
                rndVal = (uint)CS.General_v3.Util.Random.GetInt(0, 0xFFFFFF);
                if (ht[rndVal] == null)
                    ok = true;
            }
            while (!ok);
            return CS.General_v3.Util.Colors.RGB.GetColorFromUInt(rndVal);
        }
        /// <summary>
        /// Adds a watermark to an image
        /// </summary>
        /// <param name="objImage">Image</param>
        /// <param name="objWatermark">Watermark</param>
        /// <param name="TransparentColor"></param>
        /// <param name="setTransparentToTopmostLeftPixel"></param>
        /// <param name="transparencyValue"></param>
        /// <returns></returns>
        public static System.Drawing.Bitmap AddWatermark(System.Drawing.Image objImage, System.Drawing.Image objWatermark,
            List<uint> TransparentColors, bool setTransparentToTopmostLeftPixel, float transparencyValue,
            CS.General_v3.Enums.CROP_IDENTIFIER watermarkPosition, int paddingFromBorder)
        {
            //if (imagePath.Contains("/")) imagePath = CS.General_v3.Util.PageUtil.MapPath(imagePath);
            //if (watermarkPath.Contains("/")) watermarkPath = CS.General_v3.Util.PageUtil.MapPath(watermarkPath);

            //Creating Dynamic Watermark on image
            System.Drawing.Bitmap bmpWatermark = new Bitmap(objWatermark,objWatermark.Width,objWatermark.Height);
            int height = objImage.Height;//Actual image width
            int width = objImage.Width;//Actual image height
            if ((TransparentColors == null)&& setTransparentToTopmostLeftPixel)
            {
                if (TransparentColors == null)
                    TransparentColors = new List<uint>();
                for (int x = 0; x <= 2; x++)
                {
                    for (int y = 0; y <= 2; y++)
                    {
                        TransparentColors.Add(CS.General_v3.Util.Colors.RGB.GetUIntValueFromColor(bmpWatermark.GetPixel(x, y)));
                    }
                }
            }
            Color nonExistantColor = Color.White;
            if (TransparentColors != null)
            {
                foreach (var clr in TransparentColors)
                {
                    bmpWatermark.MakeTransparent(CS.General_v3.Util.Colors.RGB.GetColorFromUInt(clr));
                }
                nonExistantColor = GetColorNotInImage(bmpWatermark);
               // nonExistantColor = Color.FromArgb(0, 255, 255, 0);
            }
            System.Drawing.Bitmap bmpImage = new System.Drawing.Bitmap(objImage, width, height);// create bitmap with same size of Actual image
            System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bmpImage);
            
            bmpWatermark = ResizeImage(bmpWatermark, new SolidBrush(nonExistantColor), objImage.Width, objImage.Height, false);
            if (TransparentColors != null)
            {
                bmpWatermark.MakeTransparent(nonExistantColor);
            }

            System.Drawing.Bitmap bmpTransparent = new Bitmap(bmpWatermark.Width,bmpWatermark.Height);
            System.Drawing.Graphics gWatermark = System.Drawing.Graphics.FromImage(bmpTransparent);
            
            ColorMatrix colorMatrix = new ColorMatrix();
            colorMatrix.Matrix33 = transparencyValue; ////the 3rd element of the 4th row represents the transparency

            //an ImageAttributes object is used to set all //the alpha //values.
            //This is done by initializing a color matrix and setting the alpha scaling value in the matrix.
            //The address of //the color matrix is passed to the SetColorMatrix method of the //ImageAttributes object, 
            //and the //ImageAttributes object is passed to the DrawImage method of the Graphics object.

            ImageAttributes imgAttributes = new ImageAttributes();
            imgAttributes.SetColorMatrix(colorMatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
            int watermarkX = 0, watermarkY = 0;
            //fill watermark X position
            switch (watermarkPosition)
            {
                case Enums.CROP_IDENTIFIER.BottomMiddle:
                case Enums.CROP_IDENTIFIER.MiddleMiddle:
                case Enums.CROP_IDENTIFIER.TopMiddle:
                    watermarkX = (bmpImage.Width - bmpWatermark.Width) / 2; break;
                case Enums.CROP_IDENTIFIER.BottomLeft:
                case Enums.CROP_IDENTIFIER.MiddleLeft:
                case Enums.CROP_IDENTIFIER.TopLeft:
                    watermarkX = 0 + paddingFromBorder; break;
                case Enums.CROP_IDENTIFIER.BottomRight:
                case Enums.CROP_IDENTIFIER.MiddleRight:
                case Enums.CROP_IDENTIFIER.TopRight:
                    watermarkX = (bmpImage.Width - bmpWatermark.Width) - paddingFromBorder; break;
            }
            //fill watermark X position
            switch (watermarkPosition)
            {
                case Enums.CROP_IDENTIFIER.BottomLeft:
                case Enums.CROP_IDENTIFIER.BottomMiddle:
                case Enums.CROP_IDENTIFIER.BottomRight:
                    watermarkY = (bmpImage.Height - bmpWatermark.Height) - paddingFromBorder; break;
                case Enums.CROP_IDENTIFIER.MiddleLeft:
                case Enums.CROP_IDENTIFIER.MiddleMiddle:
                case Enums.CROP_IDENTIFIER.MiddleRight:
                    watermarkY = (bmpImage.Height - bmpWatermark.Height) / 2; break;
                case Enums.CROP_IDENTIFIER.TopLeft:
                case Enums.CROP_IDENTIFIER.TopMiddle:
                case Enums.CROP_IDENTIFIER.TopRight:
                    watermarkY = paddingFromBorder; break;
            }
            if (watermarkY < 0) watermarkY = 0;
            if (watermarkX < 0) watermarkX = 0;
            if (watermarkX > bmpImage.Width - bmpWatermark.Width) watermarkX = bmpImage.Width - bmpWatermark.Width;
            if (watermarkY > bmpImage.Height - bmpWatermark.Height) watermarkY = bmpImage.Height - bmpWatermark.Height;
            

            gWatermark.DrawImage(bmpWatermark, new Rectangle(0, 0, bmpTransparent.Width, bmpTransparent.Height),
                0, 0, bmpTransparent.Width, bmpTransparent.Height, GraphicsUnit.Pixel, imgAttributes);
            gWatermark.Dispose();
           // g.DrawImage(bmpTransparent, watermarkX, watermarkY);
            g.DrawImage(bmpWatermark,new Rectangle(watermarkX,watermarkY,bmpWatermark.Width,bmpWatermark.Height),
                0,0,bmpWatermark.Width,bmpWatermark.Height, GraphicsUnit.Pixel, imgAttributes);

            g.Dispose();
            bmpWatermark.Dispose();
            bmpTransparent.Dispose();

            //g.DrawImage(bmpWatermark, new Point(watermarkX, watermarkY));

            return bmpImage;
            /*
            /*
            //Creates a System.Drawing.Color structure from the four ARGB component
            //(alpha, red, green, and blue) values. Although this method allows a 32-bit value
            // to be passed for each component, the value of each component is limited to 8 bits.
            //create Brush
            SolidBrush brush = new SolidBrush(Color.FromArgb(113, 255, 255, 255));
            //Adding watermark text on image
            g.DrawString("Raju.M C# Programmer Kerala,India.Dynamic WaterMark sample", new Font("Arial", 18, System.Drawing.FontStyle.Bold), brush, 0, 100);
            //save image with Watermark image/picture
            //bitmapimage.Save("watermark-image.jpg"); //if u want to save image
            Response.ContentType = "image/jpeg";
            bitmapimage.Save(Response.OutputStream, ImageFormat.Jpeg);*/
        }

        public static List<string> GetImageFilesExtensions()
        {
            List<string> list = new List<string>();
            list.Add("jpg");
            list.Add("jpeg");
            list.Add("bmp");
            list.Add("gif");
            list.Add("tif");
            list.Add("tiff");
            list.Add("png");
            return list;
        }
        public static bool CheckImageFileExtension(string filename)
        {
            List<string> allowed = GetImageFilesExtensions();
            string ext = CS.General_v3.Util.IO.GetExtension(filename);
            ext = ext.ToLower();
            ext = ext.Replace(".", "");
            return (allowed.Contains(ext));

        }
        
       
        public static Bitmap GetBitmap(string path)
        {
            FileStream f = new FileStream(path, FileMode.Open, FileAccess.Read);

            Bitmap bmp = new Bitmap(f);
            f.Close();
            return bmp;
        }
        private static bool dummyCallBackFunc()
        {
            return false;

        }

        /// <summary>
        /// Crops an image, from a location to the specified width/height
        /// </summary>
        /// <param name="path">Path of source</param>
        /// <param name="width">Width to be cropped.</param>
        /// <param name="height">Height to be cropped</param>
        /// <param name="identifier">Identifier of where to crop.  This shows where it must cut, for example if it is 
        /// TopLeft, it will crop and save in SAVETO the image found in the TopLeft corner of the dimensions specified </param>
        /// <param name="saveTo">Location cropped image to</param>
        public static void CropImage(string path, int width, int height, Enums.CROP_IDENTIFIER identifier, string saveTo)
        {
            int imgW = 800, imgH = 800;
            int id = (int)identifier;
            int x, y;
            getImageSize(path, out imgW, out imgH);
            x = 0;
            y = 0;
            if (width < imgW || height < imgH)
            {
                if (width > imgW)
                    width = imgW;
                if (height > imgH)
                    height = imgH;
                if (width == 0)
                {
                    x = 0;
                    width = imgW;
                }
                else if (width < imgW)
                {
                    switch (id % 3)
                    {
                        case 1: //left horizontal
                            x = 0;
                            break;
                        case 2: //middle horizontal
                            x = Convert.ToInt32(Math.Floor(((double)(imgW - width) / 2)));
                            break;
                        case 0: //right horizontal
                            x = imgW - width;
                            break;
                    }
                }
                if (height == 0)
                {
                    y = 0;
                }
                else if (height < imgH)
                {
                    if (id <= 3)
                    {
                        y = 0;
                    }
                    else if (id <= 6)
                    {
                        y = Convert.ToInt32(Math.Floor(((double)(imgH - height) / 2)));
                    }
                    else
                    {
                        y = imgH - height;
                    }
                }
                MemoryStream memS = CropImage(path, x, y, width, height, PixelFormat.Format24bppRgb,
                     SmoothingMode.AntiAlias, PixelOffsetMode.HighQuality);
                CS.General_v3.Util.IO.SaveStreamToFile(memS, saveTo);
                /*
                byte[] cropImageBytes = new byte[memS.Length];
                memS.Read(cropImageBytes, 0, (int) memS.Length);
                //byte[] cropImageBytes = memS.GetBuffer();
                File.Delete(saveTo);
                FileStream fs = new FileStream(saveTo, FileMode.Create, FileAccess.Write);
                fs.Write(cropImageBytes, 0, cropImageBytes.Length);
                fs.Close();*/
                memS.Close();
                //fs.Dispose();
                memS.Dispose();
                //memS = null;
                //fs = null;


            }
            else
            {
                if (path.ToLower() != saveTo.ToLower())
                {
                    File.Delete(saveTo);
                    File.Copy(path, saveTo);
                }
            }

        }

        /// <summary>
        /// Crops an image, from a location to the specified width/height, and saves it to the same file
        /// </summary>
        /// <param name="path">Path of source</param>
        /// <param name="width">Width to be cropped.</param>
        /// <param name="height">Height to be cropped</param>
        /// <param name="identifier">Identifier of where to crop.  This shows where it must cut, for example if it is 
        /// TopLeft, it will crop and save in SAVETO the image found in the TopLeft corner of the dimensions specified </param>

        public static void CropImage(string path, int width, int height, Enums.CROP_IDENTIFIER identifier)
        {
            CropImage(path, width, height, identifier, path);


        }

        /// <summary>
        /// Resizes an image first to the biggest possible so that at least it fills the place, then crops it based on the identifier.
        /// If Identifier is set to none, no cropping is done and performs like 'ResizeImage'.
        /// </summary>
        /// <param name="origPath"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="toPath"></param>
        /// <param name="identifier">Crop Identifier</param>
        public static void ResizeAndCropImage(string origPath, int width, int height, string toPath, Enums.CROP_IDENTIFIER identifier)
        {
            if (identifier == Enums.CROP_IDENTIFIER.None)
            {
                ResizeImage(origPath, width, height, toPath);

            }
            else
            {
                ResizeImageBiggest(origPath, width, height, toPath);
                CropImage(toPath, width, height, identifier);
            }
        }

        /// <summary>
        /// Crops an image, with the specified parameters
        /// </summary>
        /// <param name="path">Input image path</param>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        /// <param name="w">Width of crop box</param>
        /// <param name="h">Height of crop target</param>
        /// <param name="saveTo">Path to save image to</param>
        /// <param name="pixelFormat">Format of the pixel, e.g 24bits per pixel (RGB)</param>
        /// <param name="smoothingMode">Smoothing mode, example High Antialiasing</param>
        /// <param name="pixelQuality">Choose the pixel quality (High Quality/low render speed or vice versa)</param>
        /// <returns>A memory stream with the cropped image</returns>
        public static void CropImage(string path, int x, int y, int w, int h, string saveTo,
                    System.Drawing.Imaging.PixelFormat pixelFormat, System.Drawing.Drawing2D.SmoothingMode smoothingMode,
            PixelOffsetMode pixelQuality)
        {
            
            MemoryStream mm = CropImage(path, x, y, w, h, pixelFormat, smoothingMode, pixelQuality);
            File.Delete(saveTo);
            FileStream fs = new FileStream(saveTo, FileMode.CreateNew, FileAccess.Write);
            byte[] buffer = mm.GetBuffer();
            fs.Write(buffer, 0, buffer.Length);
            fs.Dispose();
            fs = null;
            mm.Dispose();
        }

        /// <summary>
        /// Crops an image, with the specified parameters
        /// </summary>
        /// <param name="path">Input image path</param>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        /// <param name="width">Width of crop box</param>
        /// <param name="height">Height of crop target</param>
        /// <param name="pixelFormat">Format of the pixel, e.g 24bits per pixel (RGB)</param>
        /// <param name="smoothingMode">Smoothing mode, example High Antialiasing</param>
        /// <param name="pixelQuality">Choose the pixel quality (High Quality/low render speed or vice versa)</param>
        /// <returns>A memory stream with the cropped image</returns>
        public static MemoryStream CropImage(string path, int x, int y, int width, int height,
                    System.Drawing.Imaging.PixelFormat pixelFormat, System.Drawing.Drawing2D.SmoothingMode smoothingMode,
            PixelOffsetMode pixelQuality)
        {

            System.Drawing.Image img = System.Drawing.Image.FromFile(path);

            MemoryStream mm = null;
            var imageFormat = GetImageFormatFromFilename(path);
            if (imageFormat != System.Drawing.Imaging.ImageFormat.Gif)
            {
                var bitmap = CropImage(img, x, y, width, height, pixelFormat, smoothingMode, pixelQuality);
                mm = new MemoryStream();

                bitmap.Save(mm, GetImageFormatFromFilename(path));
                bitmap.Dispose();
                bitmap = null;

                
            }
            else
            {
                mm= cropGIFImage(path, x, y, width, height, pixelFormat, smoothingMode, pixelQuality);
            }
            img.Dispose();
            img = null;
            return mm;
        }
        /*
        /// <summary>
        /// Crops an image, with the specified parameters
        /// </summary>
        /// <param name="path">Input image path</param>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        /// <param name="width">Width of crop box</param>
        /// <param name="height">Height of crop target</param>
        /// <param name="pixelFormat">Format of the pixel, e.g 24bits per pixel (RGB)</param>
        /// <param name="smoothingMode">Smoothing mode, example High Antialiasing</param>
        /// <param name="pixelQuality">Choose the pixel quality (High Quality/low render speed or vice versa)</param>
        /// <returns>A memory stream with the cropped image</returns>
        public static MemoryStream CropImage(string path, int x, int y, int width, int height,
                    System.Drawing.Imaging.PixelFormat pixelFormat, System.Drawing.Drawing2D.SmoothingMode smoothingMode,
            PixelOffsetMode pixelQuality)
        {


            System.Drawing.Image img = System.Drawing.Image.FromFile(path);
            int w = width;
            int h = height;
            if (w > img.Width) w = img.Width;
            if (h > img.Height) h = img.Height;
            Bitmap bitmap = new Bitmap(w, h, pixelFormat);
            System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bitmap);
            g.SmoothingMode = smoothingMode;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.PixelOffsetMode = pixelQuality;
            g.DrawImage(img, new Rectangle(0, 0, w, h), x, y, w, h, GraphicsUnit.Pixel);
            MemoryStream mm = new MemoryStream();

            bitmap.Save(mm, GetImageFormatFromFilename(path));
            img.Dispose();
            bitmap.Dispose();
            g.Dispose();
            bitmap = null;
            g = null;
            img = null;

            return mm;
        }*/

        /// <summary>
        /// Crops an image, with the specified parameters
        /// </summary>
        /// <param name="path">Input image path</param>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        /// <param name="width">Width of crop box</param>
        /// <param name="height">Height of crop target</param>
        /// <param name="pixelFormat">Format of the pixel, e.g 24bits per pixel (RGB)</param>
        /// <param name="smoothingMode">Smoothing mode, example High Antialiasing</param>
        /// <param name="pixelQuality">Choose the pixel quality (High Quality/low render speed or vice versa)</param>
        /// <returns>A memory stream with the cropped image</returns>
        public static Bitmap CropImage(System.Drawing.Image img, int x, int y, int width, int height,
                    System.Drawing.Imaging.PixelFormat pixelFormat, System.Drawing.Drawing2D.SmoothingMode smoothingMode,
            PixelOffsetMode pixelQuality)
        {
            int w = width;
            int h = height;
            if (w > img.Width) w = img.Width;
            if (h > img.Height) h = img.Height;
            Bitmap bitmap = new Bitmap(w, h, pixelFormat);

            System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bitmap);
            g.SmoothingMode = smoothingMode;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.PixelOffsetMode = pixelQuality;
            g.DrawImage(img, new Rectangle(0, 0, w, h), x, y, w, h, GraphicsUnit.Pixel);
            return bitmap;
        }
        /// <summary>
        /// Crops an image, with the specified parameters
        /// </summary>
        /// <param name="path">Input image path</param>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        /// <param name="width">Width of crop box</param>
        /// <param name="height">Height of crop target</param>
        /// <param name="pixelFormat">Format of the pixel, e.g 24bits per pixel (RGB)</param>
        /// <param name="smoothingMode">Smoothing mode, example High Antialiasing</param>
        /// <param name="pixelQuality">Choose the pixel quality (High Quality/low render speed or vice versa)</param>
        /// <returns>A memory stream with the cropped image</returns>
        private static MemoryStream cropGIFImage(string path, int x, int y, int width, int height,
                    System.Drawing.Imaging.PixelFormat pixelFormat, System.Drawing.Drawing2D.SmoothingMode smoothingMode,
            PixelOffsetMode pixelQuality)
        {

            int delay =0;
            var frames = GetGIFFrames(path, out delay);
            for (int i = 0; i < frames.Count; i++)
            {
                frames[i] = CropImage(frames[i], x, y, width, height, pixelFormat, smoothingMode, pixelQuality);
            }
            string tmpFile = Util.IO.GetTempFilename("gif");
            SaveAsGIFImage(frames, delay, tmpFile);
            var byArray = CS.General_v3.Util.IO.LoadFileAsByteArray(tmpFile);
            MemoryStream ms = new MemoryStream(byArray);
            return ms;
        }




        /// <summary>
        /// Retreives the size of an image, in 2 output params.  If file does not exist, they are returned as 0.
        /// </summary>
        /// <param name="path">Local path, with Server.MapPath</param>
        /// <param name="width">Width</param>
        /// <param name="height">Height</param>
        public static void getImageSize(string path, out int width, out int height)
        {
            if (path.Contains("/")) path = CS.General_v3.Util.PageUtil.MapPath(path);
            if (File.Exists(path))
            {
                FileStream f = new FileStream(path, FileMode.Open, FileAccess.Read);
                try
                {
                    Bitmap bmp = new Bitmap(f);
                    width = bmp.Width;
                    height = bmp.Height;
                    bmp.Dispose();
                    bmp = null;
                }
                catch (ArgumentException ex)
                {
                    width = 0;
                    height = 0;
                }

                f.Close();

                
                f.Dispose();
                f = null;
                
            }
            else
            {
                width = height = 0;
            }

        }
        
        public static void ResizeImage(string src, int maxWidth, int maxHeight, string to)
        {
            FileStream f = new FileStream(src, FileMode.Open);
            string tmpTo = to;
            if (src == to)
            {
                tmpTo = IO.GetTempFilename(CS.General_v3.Util.IO.GetExtension(to));
            }
            ResizeImage(f, maxWidth, maxHeight, tmpTo, false, GetImageFormatFromFilename(to));
            f.Close();
            f.Dispose();
            f = null;
            if (src == to)
            {
                File.Delete(src);
                if (File.Exists(tmpTo))
                {
                    File.Move(tmpTo, src);
                }
            }
            //f = null;
        }
       
        /// <summary>
        /// Resizes image to completely fill the MAXWIDTH x MAXHEIGHT box
        /// </summary>
        /// <param name="src"></param>
        /// <param name="maxWidth"></param>
        /// <param name="maxHeight"></param>
        /// <param name="to"></param>
        public static void ResizeImageBiggest(string src, int maxWidth, int maxHeight, string to)
        {
            FileStream f = new FileStream(src, FileMode.Open);
            string tmpTo = to;
            if (src == to)
            {
                tmpTo = IO.GetDirName(src) + "tmp" + String.Format("{0:ddMMyyyyhhmmss}", CS.General_v3.Util.Date.Now);
            }
            ResizeImage(f, maxWidth, maxHeight, tmpTo, true,GetImageFormatFromFilename(src));
            f.Close();
            f.Dispose();
            f = null;
            if (src == to)
            {
                File.Delete(src);
                File.Move(tmpTo, src);
            }

        }
        public static Bitmap ResizeImage(System.Drawing.Image img, int maxWidth, int maxHeight, bool fillsBox)
        {
            return ResizeImage(img, Brushes.White, maxWidth, maxHeight, fillsBox);
        }
        public static Bitmap ResizeImage(System.Drawing.Image img, Brush transparentColor, int maxWidth, int maxHeight, bool fillsBox)
        {
            Bitmap bmp = new Bitmap(img, img.Width, img.Height);
            return ResizeBitmap(bmp,transparentColor, maxWidth, maxHeight, fillsBox);

        }
        public static Bitmap ResizeBitmap(Bitmap bmp, int maxWidth, int maxHeight, bool fillsBox)
        {
            return ResizeBitmap(bmp, Brushes.White, maxWidth, maxHeight, fillsBox);
        }
        public static Bitmap ResizeBitmap(Bitmap bmp,Brush transparentColor, int maxWidth, int maxHeight, bool fillsBox)
        {
            int width, height;
            
            double widthRatio = 1, heightRatio = 1, ratio;
           //| Bitmap bmp = new Bitmap(src);

            if (maxWidth > 0)
                widthRatio = ((double)bmp.Width / (double)maxWidth);
            if (maxHeight > 0)
                heightRatio = ((double)bmp.Height / (double)maxHeight);
            if (!fillsBox)
            {
                if (widthRatio > heightRatio)
                    ratio = widthRatio;
                else
                    ratio = heightRatio;
            }
            else
            {
                if (widthRatio > heightRatio)
                    ratio = heightRatio;
                else
                    ratio = widthRatio;

            }
            if (ratio < 1)
                ratio = 1;
            //Directory.CreateDirectory(IO.GetDirName(to));
            Bitmap resized = null;
            if (ratio > 1)
            {
                ratio = 1 / ratio;
                width = Int32.Parse((Math.Floor((double)bmp.Width / ratio)).ToString());
                height = Int32.Parse((Math.Floor((double)bmp.Height / ratio)).ToString());
                resized = CreateThumbnail(bmp,transparentColor, ratio);
                bmp.Dispose();
                bmp = resized;
                //resized.Save(to, System.Drawing.Imaging.ImageFormat.Jpeg);
                //resized.Dispose();
            }
            else
            {
                
            }
            return bmp;

        }

        public static System.Drawing.Imaging.ImageFormat GetImageFormatFromFilename(string filename)
        {
            string ext = IO.GetExtension(filename).ToLower();
            switch (ext)
            {
                case "gif": return System.Drawing.Imaging.ImageFormat.Gif;
                case "png": return System.Drawing.Imaging.ImageFormat.Png;
                case "tiff":
                case "tif": return System.Drawing.Imaging.ImageFormat.Tiff;
                case "ico": return System.Drawing.Imaging.ImageFormat.Icon;
            }
            return System.Drawing.Imaging.ImageFormat.Jpeg;

        }

        /// <summary>
        /// Resizes an image to a maximum size.  Also changes format to JPEG if not.
        /// </summary>
        /// <param name="src">Source from where to load image</param>
        /// <param name="maxWidth">Maximum width. If left 0, width can be unlimited</param>
        /// <param name="maxHeight">Maximum height.  If left 0, height can be unlimited</param>
        /// <param name="to">The location of where to save the resized image</param>
        public static void ResizeImage(Stream src, int maxWidth, int maxHeight, string to, bool fillsBox,
            System.Drawing.Imaging.ImageFormat imageFormat)
        {
            try
            {
                
                if (imageFormat != System.Drawing.Imaging.ImageFormat.Gif)
                {
                    Bitmap bmp = new Bitmap(src);
                    bmp = ResizeBitmap(bmp, maxWidth, maxHeight, fillsBox);
                    Directory.CreateDirectory(IO.GetDirName(to));
                    bmp.Save(to, imageFormat);
                    bmp.Dispose();

                    //resized = null;
                    bmp = null;
                }
                else
                {
                    string tmpFile = Util.IO.GetTempFilename("gif");
                    CS.General_v3.Util.IO.SaveStreamToFile(src,tmpFile);
                    resizeGIFImage(tmpFile, to, maxWidth, maxHeight, fillsBox);
                    Util.IO.DeleteFile(tmpFile);
                }
            }
            catch (Exception ex)
            {
                
            }

        }


        public static System.Drawing.Bitmap CreateThumbnail(string path, double ratio)
        {
            FileStream fs = new FileStream(path,FileMode.Open);
            Bitmap resized = CreateThumbnail(fs, ratio);
            fs.Close();
            return resized;
        }
        public static System.Drawing.Bitmap CreateThumbnail(Stream stream, double ratio)
        {
            return CreateThumbnail(new Bitmap(stream), ratio);
        }
        public static System.Drawing.Bitmap CreateThumbnail(System.Drawing.Bitmap loBMP, double ratio)
        {
            return CreateThumbnail(loBMP, Brushes.White, ratio);
        }
        public static System.Drawing.Bitmap CreateThumbnail(System.Drawing.Bitmap loBMP, Brush transparentColor, double ratio)
        {
            int  height;
            System.Drawing.Bitmap bmpOut = null;
            try
            {
                ImageFormat loFormat = loBMP.RawFormat;
                decimal lnRatio = Convert.ToDecimal(ratio);
                int lnNewWidth = Convert.ToInt32(loBMP.Width * ratio);
                int lnNewHeight = Convert.ToInt32(loBMP.Height * ratio);
                

                bmpOut = new Bitmap(lnNewWidth, lnNewHeight);
                System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bmpOut);
                
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                Color clrTransparent = (new Pen(transparentColor).Color);
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
               // g.FillRectangle(transparentColor, 0, 0, lnNewWidth, lnNewHeight);
               // bmpOut.MakeTransparent(clrTransparent);
                g.DrawImage(loBMP, 0, 0, lnNewWidth, lnNewHeight);

                
                loBMP.Dispose();
            }
            catch
            {
                return null;
            }
            return bmpOut;
        }
        private static void resizeGIFImage(string originalPath, string resizeTo, int maxWidth, int maxHeight, bool fillsBox)
        {
            int h = 0;
            int delay = 0;
            var frames = GetGIFFrames(originalPath, out delay);
            if (frames.Count > 0)
            {
                for (int i = 0; i < frames.Count; i++)
                {
                    frames[i]= ResizeBitmap(frames[i], maxWidth, maxHeight, fillsBox);
                }
                SaveAsGIFImage(frames, delay, resizeTo);
            }

        }
        /// <summary>
        /// Saves frames into a GIF animation
        /// </summary>
        /// <param name="frames"></param>
        /// <param name="frameAnimationDelay"></param>
        /// <param name="path"></param>
        public static void SaveAsGIFImage(List<Bitmap> frames, int frameAnimationDelay, string path )
        {
            AnimatedGifEncoder e = new AnimatedGifEncoder();
            e.SetSize(frames[0].Width, frames[0].Height);
            e.Start(path);
            e.SetDelay(frameAnimationDelay);
            e.SetRepeat(0);
            for (int i = 0; i < frames.Count; i++)
            {
                e.AddFrame(frames[i]);
            }
            e.Finish();
        }
        public static string MakeImageForFlash(string imageUrl)
        {
            if (imageUrl.StartsWith("/"))
            {
                imageUrl = CS.General_v3.Util.PageUtil.GetApplicationBaseUrl() + imageUrl.Substring(1, imageUrl.Length - 1);
            }
            return imageUrl;
        }


        /// <summary>
        /// Returns a list of bitmaps with all the frames of the GIF
        /// </summary>
        /// <param name="localPath">Path of GIF</param>
        /// <param name="frameAnimationDelay">Animation Delay between frames</param>
        /// <returns></returns>
        public static List<System.Drawing.Bitmap> GetGIFFrames(string localPath, out int frameAnimationDelay)
        {
            
            GifDecoder gifDecoder = new GifDecoder();
            gifDecoder.Read(localPath);
            List<Bitmap> list = new List<Bitmap>();
            int frameCount = gifDecoder.GetFrameCount();
            frameAnimationDelay = 0;
            if (frameCount > 0)
            {

                frameAnimationDelay = gifDecoder.GetDelay(0);
                for (int i = 0, count = frameCount; i < count; i++)
                {
                    System.Drawing.Image frame = gifDecoder.GetFrame(i);

                    System.Drawing.Bitmap bmp = new Bitmap(frame);

                    list.Add(bmp);
                }
            }
            return list;
        }
    }


}
