﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace CS.General_v3.Util
{
    public static class GoogleAnalyticsUtil
    {
        public static string GetGoogleAnalyticsCodeForImpression(string trackerVarName, string ID, string Title, string Category, bool synchronousCode = true, string label="Impression")
        {
            Category = CS.General_v3.Util.Text.forJS(Category);
            Title = CS.General_v3.Util.Text.forJS(Title);
            /*string s = trackerVarName + "._trackEvent('" +  + "', '" + + " [" + this.ID + "]" + "'" +
                ", 'Impression');\r\n";*/

            if (synchronousCode)
                return CS.General_v3.JavaScript.GoogleAnalytics.APIUtil.GetTrackEventJS_Sync(Category, Title + " [" + ID + "]", label, null, trackerVarName);
            else
                return CS.General_v3.JavaScript.GoogleAnalytics.APIUtil.GetTrackEventJS_ASync(Category, Title + " [" + ID + "]", label, null, trackerVarName);

            //return s;
        }
        public static string GetGoogleAnalyticsCodeForHit(string trackerVarName, string ID, string Title, string Category, bool synchronousCode = true, string label = "Hit")
        {
            //return "alert(_gaq);";
            Category = CS.General_v3.Util.Text.forJS(Category);
            Title = CS.General_v3.Util.Text.forJS(Title);
            /*string s = trackerVarName + "._trackEvent('" +  + "', '" + + " [" + this.ID + "]" + "'" +
                ", 'Impression');\r\n";*/

            if (synchronousCode)
                return CS.General_v3.JavaScript.GoogleAnalytics.APIUtil.GetTrackEventJS_Sync(Category, Title + " [" + ID + "]", label, null, trackerVarName);
            else
                return CS.General_v3.JavaScript.GoogleAnalytics.APIUtil.GetTrackEventJS_ASync(Category, Title + " [" + ID + "]", label, null, trackerVarName);

            /*
            string s = trackerVarName + "._trackEvent('" + CS.General_20090518.Util.Text.forJS(Category) + "', '" + CS.General_20090518.Util.Text.forJS(this.Title) + " [" + this.ID + "]"+ "'" +
                ", 'Hit');\r\n";
            return s;*/
        }
        /*public static void AttachGoogleAnalyticsCodeForImpressionToPage(bool synchronous, string ID, string Title, string trackerVarName, string Category, Page pg)
        {
            string js = GetGoogleAnalyticsCodeForImpression(trackerVarName, ID, Title, Category, synchronous);
            pg.ClientScript.RegisterStartupScript(this.GetType(), "advert_page_" + ID, js, true);
        }*/
    }
}
