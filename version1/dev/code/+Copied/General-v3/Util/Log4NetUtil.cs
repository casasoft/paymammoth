﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.IO;
using log4net;

namespace CS.General_v3.Util
{
    public static class Log4NetUtil
    {
        public static void ConfigureForDefaultWebApp()
        {
            List<string> pathsToUse = new List<string>();

            string path = CS.General_v3.Util.IO.GetApplicationRootLocalFolder();

            
            if (CS.General_v3.Util.Other.IsLocalTestingMachine)
            {
                pathsToUse.Add(path + "App_Data/log4net.config.user.xml");
            }
            else
            {
                pathsToUse.Add(path + "App_Data/log4net.config.online.xml");
            }
            pathsToUse.Add(path + "App_Data/log4net.config.xml");
            Configure(pathsToUse.ToArray());
            


        }
        public static void Configure(params string[] xmlFiles)
        {
            bool ok = false;
            for (int i = 0; i < xmlFiles.Length; i++)
            {
                string xmlFile = CS.General_v3.Util.IO.NormaliseToLocalPath(xmlFiles[i]);


                if (File.Exists(xmlFile))
                {
                    try
                    {
                        FileInfo _xmlFileInfo = new FileInfo(xmlFile);
                        log4net.Config.XmlConfigurator.ConfigureAndWatch(_xmlFileInfo);
                        ok = true;
                        break;
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }

        }

        //public static void LogMsgIncMethod(this log4net.ILog _log, Enums.LOG4NET_MSG_TYPE msgType, string msg, Exception exception = null)
        //{
        //    StackTrace stackTrace = new StackTrace();
        //    var frame = stackTrace.GetFrame(1);
        //    var method = frame.GetMethod();
            
        //    string methodName = method.DeclaringType.FullName + "." + method.Name;

        //    string s = "[" + methodName + "] " + msg;
        //    LogMsg(_log, msgType, s, exception);
        //}

        public static void LogMsg(this log4net.ILog _log, Enums.LOG4NET_MSG_TYPE msgType, string msg, Exception exception = null)
        {
            
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(_log, "Log cannot be null");

            switch (msgType)
            {
                case Enums.LOG4NET_MSG_TYPE.Debug: _log.Debug(msg, exception); break;
                case Enums.LOG4NET_MSG_TYPE.Error: _log.Error(msg, exception); break;
                case Enums.LOG4NET_MSG_TYPE.Fatal: _log.Fatal(msg, exception); break;
                case Enums.LOG4NET_MSG_TYPE.Warn: _log.Warn(msg, exception); break;
                case Enums.LOG4NET_MSG_TYPE.Info: _log.Info(msg, exception); break;
                default:
                    throw new InvalidOperationException("Value not mapped < " + msgType + ">");
            
            }
            
        }
        public static bool IsEnabled(ILog log, Enums.LOG4NET_MSG_TYPE msgType)
        {
            switch (msgType)
            {
                case Enums.LOG4NET_MSG_TYPE.Debug: return log.IsDebugEnabled;
                case Enums.LOG4NET_MSG_TYPE.Error: return log.IsErrorEnabled;
                case Enums.LOG4NET_MSG_TYPE.Fatal: return log.IsFatalEnabled;
                case Enums.LOG4NET_MSG_TYPE.Warn: return log.IsWarnEnabled;
                case Enums.LOG4NET_MSG_TYPE.Info: return log.IsInfoEnabled;
            }
            throw new InvalidOperationException("Value not mapped < " + msgType + ">");
        }
        public static bool IsEnabled(object target, Enums.LOG4NET_MSG_TYPE msgType)
        {
            var logger = GetLoggerForObject(target);
            return IsEnabled(logger, msgType);
            
        }

        public static ILog GetLoggerForObject(object target)
        {
            ILog logger = null;
            if (target != null)
            {
                logger = log4net.LogManager.GetLogger(target.GetType());
                
            }
            else
            {
                logger = log4net.LogManager.GetLogger("AnonymousTarget");
                
            }
            return logger;
        }

        public static void LogMsgByObject(object target, Enums.LOG4NET_MSG_TYPE msgType, string msg, Exception exception)
        {
           

            var logger = GetLoggerForObject(target);
            LogMsg(logger, msgType, msg, exception);
        }

    }
}
