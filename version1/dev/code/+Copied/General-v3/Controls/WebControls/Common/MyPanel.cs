﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.CSS;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.Web.UI.WebControls ;
namespace CS.General_v3.Controls.WebControls.Common
{
    public class MyPanel : System.Web.UI.WebControls.Panel, IBaseWebControl
    {
        public int __GlobalCtrlIdentifier = Util.Other.GetNextControlGlobalIdentifier();

        private BaseWebControlFunctionality _functionality = null;

        public MyPanel()
            : base()
        {
            if(__GlobalCtrlIdentifier == 430)
                
            {
                int k = 5;
            }
            init();
        }


        private void init()
        {
            _functionality = new BaseWebControlFunctionality(this);
            
        }
        public CSSManagerForControl CssManager { get { return _functionality.CssManager; } }

        public override string CssClass
        {
            get
            {
                if (_functionality == null)
                    return base.CssClass;
                else
                    return _functionality.CssClass;
                
                
            }
            set
            {
                _functionality.CssClass = value;
                base.CssClass = _functionality.CssClass;
                
            }
        
        
        }





        #region IBaseWebControl Members


        public BaseWebControlFunctionality WebControlFunctionality
        {
            get { return _functionality; }
        }


        #endregion

        #region IBaseWebControl Members


        public string RollOverClass
        {
            get { return _functionality.RollOverClass; }
            set { _functionality.RollOverClass = value; }
        }


        public string StringID
        {
            get { return _functionality.RollOverClass; }
            set { _functionality.RollOverClass = value; }
        }

        public WebControl Control
        {
            get { return _functionality.Control; }
        }

        public string OnClientBlur
        {
            get { return _functionality.OnClientBlur; }
            set { _functionality.OnClientBlur = value; }
        }

        public string OnClientClick
        {
            get { return _functionality.OnClientClick; }
            set { _functionality.OnClientClick = value; }
        }

        public string OnClientChange
        {
            get { return _functionality.OnClientChange; }
            set { _functionality.OnClientChange = value; }
        }

        public string OnClientLoad
        {
            get { return _functionality.OnClientLoad; }
            set { _functionality.OnClientLoad = value; }
        }




        #region IMyFormWebControl Members


        Control IBaseControl.Control
        {
            get
            {
                return this.Control;
            }
        }

        BaseControlFunctionality IBaseControl.WebControlFunctionality
        {
            get
            {
                return this.WebControlFunctionality;
            }
        }
        
        #endregion




        #endregion
    }
}
