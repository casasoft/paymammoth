﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using CS.General_v3.Classes.CSS;

namespace CS.General_v3.Controls.WebControls.Common
{
   
    public class MyUnorderedList : BaseWebControl
    {


        public MyUnorderedList():base( HtmlTextWriterTag.Ul)
        {
            
        }

        public void AddListItem(string text, string cssClass)
        {
            MyListItem li = new MyListItem(text, cssClass);
            this.Controls.Add(li);
            
        }

    }
}
