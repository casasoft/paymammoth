using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;

namespace CS.General_v3.Controls.WebControls.Common
{


    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyRadioButton runat=server></{0}:MyRadioButton>")]
    public class MyRadioButton : RadioButton, General.IMyRadioButton
    {
        public void FocusGood()
        {
            _functionality.FocusGood();
        }

        public override string ID
        {
            get
            {
                
                return base.ID;
            }
            set
            {
                base.ID = value;
            }
        }
        public string GetFormValueAsStr()
        {
            string s = "No";

            if (this.FormValue)
                s = "Yes";
            return s;
        }
        public string StringID { get; set; }
        protected General.MyFormWebControlFunctionality _functionality = null;
        public MyRadioButton()
            : base()
        {

            _functionality = new CS.General_v3.Controls.WebControls.Common.General.MyFormWebControlFunctionality(this, ClientIDSeparator);
            this.HasValidation = true;
            this.CssManager.AddClass("radio-button-container");
        }
        public string ClientName
        {
            get
            {
                return _functionality.ClientName;
            }
        }

        #region IMyFormWebControl Members
        public bool HasValidation
        {
            get
            {
                return _functionality.HasValidation;

            }
            set
            {
                _functionality.HasValidation = value;

            }
        }

        public virtual void SetValue(object o)
        {
            this.Text = _functionality.SetValue(o);


        }

        public override string ValidationGroup
        {
            get
            {
                return _functionality.ValidationGroup;
            }
            set
            {
                _functionality.ValidationGroup = value;
            }
        }
        [Category("Title")]
        [DefaultValue("")]
        [Description("Title to show for validation")]
        public string Title
        {
            get
            {
                return _functionality.Title;
            }

            set
            {
                _functionality.Title = value;
                /*
                */
            }
        }
        /// <summary>
        /// The value retrieved from the posted Form
        /// </summary>
        public object FormValueObject
        {
            get
            {
                
                return _functionality.FormValue;
            }


        }
        public object Value
        {
            get
            {
                return this.Checked;
            }
            set
            {
                if (value is bool)
                {
                    this.Checked = (bool)value;
                }
            }
        }

        public Enums.DATA_TYPE DataType
        {
            get
            {
                return Enums.DATA_TYPE.Bool;
            }
        }

        public bool FormValue
        {
            get { 
                //2011-02-21 Mark modified this to fix GroupName across
                //diff controls
                string val = CS.General_v3.Util.PageUtil.GetFormVariable(this.GroupName);
                return val == this.ClientID;
            }

        }

        [Category("Validation")]
        [DefaultValue(false)]
        [Description("Whether to validate control on blur or not")]
        public bool DoNotValidateOnBlur
        {
            get
            {
                return _functionality.DoNotValidateOnBlur;
            }

            set
            {
                _functionality.DoNotValidateOnBlur = value;

            }
        }
        #region classes
        [Category("Javascript")]
        [DefaultValue(false)]
        [Description("Whether to use default CSS classes or not")]
        public bool UseDefaultCSSClasses
        {
            get
            {
                return _functionality.UseDefaultCSSClasses;
            }

            set
            {
                _functionality.UseDefaultCSSClasses = value;

            }
        }
          public string Class_Disabled { get { return _functionality.Class_Disabled; } set { _functionality.Class_Disabled = value; } }
          public string Class_onFocus { get { return _functionality.Class_Focus; } set { _functionality.Class_Focus = value; } }
          public string Class_onError { get { return _functionality.Class_Error; } set { _functionality.Class_Error = value; } }
          public string Class_onOver { get { return _functionality.Class_Over; } set { _functionality.Class_Over = value; } }
          public string Class_ReadOnly { get { return _functionality.Class_ReadOnly; } set { _functionality.Class_ReadOnly = value; } }
          public string Class_Required { get { return _functionality.Class_Required; } set { _functionality.Class_Required = value; } }





         #endregion

        #endregion
          protected override void OnPreRender(EventArgs e)
          {
              _functionality.ThrowErrorIfIDIsNull();
              base.OnPreRender(e);
          }
        #region IMyWebControl Members

        public object Tag
        {
            get { return _functionality.Tag; }
            set { _functionality.Tag = value; }
        }

        public string OnClientClick { get { return _functionality.OnClientClick; } set { _functionality.OnClientClick = value; } }
        public string OnClientLoad { get { return _functionality.OnClientLoad; } set { _functionality.OnClientLoad = value; } }
        public string OnClientChange { get { return _functionality.OnClientChange; } set { _functionality.OnClientChange = value; } }
        public string OnClientBlur { get { return _functionality.OnClientBlur; } set { _functionality.OnClientBlur = value; } }
        public string HelpID
        {
            get
            {
                return _functionality.HelpID;
            }
            set
            {
                _functionality.HelpID = value;
            }
        }

        public string HelpMsg
        {
            get
            {
                return _functionality.HelpMsg;
            }
            set
            {
                _functionality.HelpMsg = value;
            }
        }

        public string RollOverClass { get { return _functionality.RollOverClass; } set { _functionality.RollOverClass = value; } }


        #endregion

        protected override void Render(HtmlTextWriter writer)
        {
            if (this.Page.IsPostBack)
            {
                this.Checked = this.FormValue;
            }
            if (OnClientClick != "")
            {
                Attributes["onclick"] += OnClientClick;
            }

            string tmp = Text;
            Text = "<label for='" + ClientID.Replace("\\", "\\\\").Replace("'", "\\'") + "'>" +
                    tmp + "</label>";
            base.Render(writer);
            Text = tmp;
            if (string.IsNullOrEmpty(GroupName))
            {
                throw new Exception("Please specify Groupname");
            }
            //2011-02-22 Mark added this to set name to 'groupname' and value to this client ID
            Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID + "td", "jQuery('#" + this.ClientID + "').attr('name', '" + GroupName + "').val('"+this.ClientID+"');\n", true);
        }

        #region IMyRadioButton Members

        

        #endregion

        #region IMyFormWebControl Members


        public bool Required
        {
            get
            {
                return false;
            }
            set
            {
                _functionality.Required = false;
            }
        }

        #endregion

        public new WebControl Control
        {
            get { return this; }
        }

        public FormFieldsValidationParams.FormFieldValidationSubGroupParams SubGroupParams { get { return WebControlFunctionality.SubGroupParams; } set { WebControlFunctionality.SubGroupParams = value; } }
        public int? GetFormValueAsIntNullable()
        {
            string s = this.GetFormValueAsStr();
            int? result = null;
            int num;
            if (Int32.TryParse(s, out num))
                result = num;
            return result;

        }





        #region IMyFormWebControl Members


        public General.MyFormWebControlFunctionality WebControlFunctionality
        {
            get { throw new NotImplementedException(); }
        }

        #endregion

        #region IBaseWebControl Members

        public General_v3.Classes.CSS.CSSManagerForControl CssManager
        {
            get { return _functionality.CssManager; }
        }

        BaseWebControlFunctionality IBaseWebControl.WebControlFunctionality
        {
            get { return _functionality; }
        }

        #endregion

        #region IBaseControl Members

        Control IBaseControl.Control
        {
            get { return this.Control; }
        }

        BaseControlFunctionality IBaseControl.WebControlFunctionality
        {
            get { return _functionality; }
        }

        #endregion

        #region IMyFormWebControl Members


        public FormFieldsValidationParams ValidationParams
        {
            get
            {
                return this.WebControlFunctionality.ValidationParams;
            }
            set { this.WebControlFunctionality.UpdateValidationParams(value); }
        }

        public FormFieldsCSSClasses FieldCssClasses
        {
            get
            {
                return this.WebControlFunctionality.FieldCssClasses;
            }
            set
            {
                this.WebControlFunctionality.FieldCssClasses = value;
            }
        }

        #endregion       
    
        #region IMyFormWebControl Members


        public bool ReadOnly
        {
            get
            {
                return this.Enabled;
            }
            set
            {
                this.Enabled = value;
            }
        }

        #endregion
    }
}
