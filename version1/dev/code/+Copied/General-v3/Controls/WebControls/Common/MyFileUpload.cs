using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using CS.General_v3.Controls.WebControls.Common.General;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;


namespace CS.General_v3.Controls.WebControls.Common
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyFileUpload runat=server></{0}:MyFileUpload>")]
    public class MyFileUpload : FileUpload, CS.General_v3.Controls.WebControls.Common.General.IMyFormWebControl
    {
        public void FocusGood()
        {
            
            _functionality.FocusGood();
        }

        public override string ID
        {
            get
            {
                return base.ID;
            }
            set
            {
                base.ID = value;
            }
        }
        public string GetFormValueAsStr()
        {
            throw new InvalidOperationException("This operation is not allowed on a 'MyFileUpload' control");
            
        }
        protected General.MyFormWebControlFunctionality _functionality = null;
        public MyFileUpload()
            : base()
        {
            init();            
        }
        public MyFileUpload(string ID, string Title, bool Required)
            : this (ID,Title,Required,null)
        {

        }
        public MyFileUpload(string ID, string Title, bool Required, int? width)
            : base()
        {
            init();
            this.ID = this.StringID = ID;
            this.Title = Title;
            this.Required = Required;
            if (width != null)
                this.Width = width.Value;

            
        }

        private void init()
        {
            _functionality = new CS.General_v3.Controls.WebControls.Common.General.MyFormWebControlFunctionality(this, ClientIDSeparator);
            _functionality.IsFile = true;
            this.HasValidation = true;
        }

        protected override void Render(HtmlTextWriter writer)
        {



            if (Columns > 0)
            {
                Attributes["size"] = Columns.ToString();
            }

            base.Render(writer);
        }
        public string ClientName
        {
            get
            {
                return _functionality.ClientName;
            }
        }

#region IMyFileUpload Members
        public bool Required
        {
            get { return _functionality.Required; }
            set { _functionality.Required = value; }
        }

        private bool _IsImage = false;
        /// <summary>
        /// Set wheter the file being uploaded is an image
        /// </summary>
        public bool IsImage
        {
            get
            {
                return _IsImage;
            }
            set
            {
                _IsImage = value;
                if (value)
                {
                    this.FileExtensionsAllowedString = "jpg,jpeg,gif,tif,tiff,png,bmp";
                }
            }
        }

        private int _columns;
        [Category("Appearance")]
        [DefaultValue(0)]
        [Description("The size of the field.  If 0 or empty, no size.")]
        public int Columns
        {
            get
            {
                return _columns;
            }

            set
            {
                _columns = value;

            }
        }

public string  GetExtension()
{
    string ret = FileName;
    int pos = ret.LastIndexOf(".");
    ret = ret.Substring(pos + 1);
    return ret;
}

public string  GetFilenameOnly()
{
    string ret = FileName;
    int pos = ret.LastIndexOf(".");
    ret = ret.Substring(0, pos);
    return ret;

}

public string  SaveTemporary()
{
    System.Random rnd = new System.Random();

    string tmpPath;
    do
    {
        int tmpInt = rnd.Next();
        tmpPath = "temp-" + CS.General_v3.Util.Date.Now.ToString("ddMMyyyyhhmmss") + tmpInt.ToString() + "." + GetExtension();
        tmpPath = this.Page.Server.MapPath(tmpPath);
    } while (File.Exists(tmpPath));
    SaveAs(tmpPath);
    return tmpPath;
}

/// <summary>
/// Saves the file,and creates the directory if it does not exist
/// </summary>
/// <param name="filename">The path</param>
public new void SaveAs(string filename)
{
    string path = filename.Substring(0, filename.LastIndexOf('\\')) + "\\";
    if (!Directory.Exists(path))
    {
        Directory.CreateDirectory(path);
    }
    FileStream fs = new FileStream(filename, FileMode.Create, FileAccess.Write);
    fs.Write(this.FileBytes, 0, this.FileBytes.Length);
    fs.Close();
    fs.Dispose();
    //base.SaveAs(filename);
}

public new Stream FileContent
{
    get
    {
        Stream s = (Stream)UploadedFileStream;
        if (s == null)
        {
            s = new MemoryStream();
        }

        return (Stream)s;
    }
}
private byte[] _FileBytes = null;
public new byte[] FileBytes
{
    get
    {
        if (_FileBytes == null)
        {
            if (FileContent != null)
            {
                _FileBytes = new byte[FileContent.Length];
                int readBytes = FileContent.Read(_FileBytes, 0, _FileBytes.Length);

            }
            else
            {
                _FileBytes = new byte[0];
            }
        }
        return _FileBytes;

    }

}

private Stream _uploadedFileStream = null;
public Stream UploadedFileStream
{
    get
    {
        //if (false && !CS.General_v3.FileUploader.UploadManager.Instance.Initialised)
        //{
        //    string errMsg = "File Upload Manager has not yet been initialised\r\n\r\n";
        //    errMsg += "It must be initialised at startup, ideally in 'global.asax'. Instructions and Sample code can be found\r\n";
        //    errMsg += "In 'FileUploader.FileSystemProcessor'.";

        //    throw new InvalidOperationException(errMsg);

        //}
 //Instance
        
        _uploadedFileStream = base.FileContent;
        return _uploadedFileStream;
    }
}
[Category("Validation")]
[Description("The file extensions that are allowed (seperated using ',' or '|')")]
[Browsable(false)]
public List<string> FileExtensionsAllowed
{
    get
    {
        if (!string.IsNullOrEmpty(FileExtensionsAllowedString))
        {
            List<string> tmp = new List<string>();
            string[] s = FileExtensionsAllowedString.Split(',', '|');
            for (int i = 0; i < s.Length; i++)
                tmp.Add(s[i]);
            return tmp;
        }
        else
            return _functionality.FileExtensionsAllowed;
    }

    set
    {
        _functionality.FileExtensionsAllowed = value;
    }
}

/// <summary>
/// A string representation (for design-time support), for a list of extensions.  Can be seperated by a | or a ,.
/// </summary>
public string FileExtensionsAllowedString
{
    get
    {
        return _functionality.FileExtensionsAllowedString;
    }
    set
    {
        _functionality.FileExtensionsAllowedString = value;
    }
}

#endregion



    
        #region IMyFormWebControl Members

[Category("Validation")]
[DefaultValue(false)]
[Description("Whether to validate control on blur or not")]
public bool DoNotValidateOnBlur
{
    get
    {
        return _functionality.DoNotValidateOnBlur;
    }

    set
    {
        _functionality.DoNotValidateOnBlur = value;

    }
}

        public bool HasValidation
        {
            get
            {
                return _functionality.HasValidation;

            }
            set
            {
                _functionality.HasValidation = value;

            }
        }

        public virtual void SetValue(object o)
        {
            //does nothing
            throw new InvalidOperationException("Cannot call this function on a file upload!");

        }

        public string ValidationGroup
        {
            get
            {
                return _functionality.ValidationGroup;
            }
            set
            {
                _functionality.ValidationGroup = value;
            }
        }
        [Category("Title")]
        [DefaultValue("")]
        [Description("Title to show for validation")]
        public string Title
        {
            get
            {
                return _functionality.Title;
            }

            set
            {
                _functionality.Title = value;
                /*
                */
            }
        }
        /// <summary>
        /// The value retrieved from the posted Form
        /// </summary>
        public Stream FormValue
        {
            get
            {
                return this.FileContent;
                
            }

        }

        #region classes
        [Category("Javascript")]
        [DefaultValue(false)]
        [Description("Whether to use default CSS classes or not")]
        public bool UseDefaultCSSClasses
        {
            get
            {
                return _functionality.UseDefaultCSSClasses;
            }

            set
            {
                _functionality.UseDefaultCSSClasses = value;

            }
        }
        /// <summary>
        /// Returns whether the user has uploaed a file
        /// </summary>
        public bool HasUploadedFile
        {
            get
            {
                return this.UploadedFileStream != null;
            }
        }
        public string Class_Disabled { get { return WebControlFunctionality.Class_Disabled; } set { WebControlFunctionality.Class_Disabled = value; } }
        public string Class_onFocus { get { return WebControlFunctionality.Class_Focus; } set { WebControlFunctionality.Class_Focus = value; } }
        public string Class_onError { get { return WebControlFunctionality.Class_Error; } set { WebControlFunctionality.Class_Error = value; } }
        public string Class_onOver { get { return WebControlFunctionality.Class_Over; } set { WebControlFunctionality.Class_Over = value; } }
        public string Class_ReadOnly { get { return WebControlFunctionality.Class_ReadOnly; } set { WebControlFunctionality.Class_ReadOnly = value; } }
        public string Class_Required { get { return WebControlFunctionality.Class_Required; } set { WebControlFunctionality.Class_Required = value; } }

        #endregion

        #endregion

        #region IMyWebControl Members

        public object Tag
        {
            get { return _functionality.Tag; }
            set { _functionality.Tag = value; }
        }

        public string OnClientClick { get { return _functionality.OnClientClick; } set { _functionality.OnClientClick = value; } }
        public string OnClientLoad { get { return _functionality.OnClientLoad; } set { _functionality.OnClientLoad = value; } }
        public string OnClientChange { get { return _functionality.OnClientChange; } set { _functionality.OnClientChange = value; } }
        public string OnClientBlur { get { return _functionality.OnClientBlur; } set { _functionality.OnClientBlur = value; } }
        public string HelpID
        {
            get
            {
                return _functionality.HelpID;
            }
            set
            {
                _functionality.HelpID = value;
            }
        }

        public string HelpMsg
        {
            get
            {
                return _functionality.HelpMsg;
            }
            set
            {
                _functionality.HelpMsg = value;
            }
        }

        public string RollOverClass { get { return _functionality.RollOverClass; } set { _functionality.RollOverClass = value; } }


        #endregion



        #region IMyFormWebControl Members


        public object FormValueObject
        {
            get { return FileContent; }
        }

        public object Value
        {
            get
            {
                return FileName;
            }
            set
            {
              //  throw new NotSupportedException();
            }
        }

        #endregion

        #region IMyWebControl Members


        public string StringID { get; set; }

        #endregion

        public WebControl Control
        {
            get { return this; }
        }

        public FormFieldsValidationParams.FormFieldValidationSubGroupParams SubGroupParams { get { return WebControlFunctionality.SubGroupParams; } set { WebControlFunctionality.SubGroupParams = value; } }

        public Enums.DATA_TYPE DataType
        {
            get
            {
                return Enums.DATA_TYPE.String;
            }
        }
        public int? GetFormValueAsIntNullable()
        {
            throw new InvalidOperationException();
        }

        #region IBaseWebControl Members

        public General_v3.Classes.CSS.CSSManagerForControl CssManager
        {
            get { return _functionality.CssManager; }
        }

        public MyFormWebControlFunctionality WebControlFunctionality
        {
            get { return _functionality; }
        }

        #endregion

        #region IMyFormWebControl Members


   

        #region IMyFormWebControl Members


        Control IBaseControl.Control
        {
            get
            {
                return this.Control;
            }
        }

        BaseControlFunctionality IBaseControl.WebControlFunctionality
        {
            get
            {
                return this.WebControlFunctionality;
            }
        }
        BaseWebControlFunctionality IBaseWebControl.WebControlFunctionality
        {
            get
            {
                return this.WebControlFunctionality;
            }
        }
        #endregion

        #endregion

        #region IMyFormWebControl Members


        public FormFieldsValidationParams ValidationParams
        {
            get
            {
                return this.WebControlFunctionality.ValidationParams;
            }
            set { this.WebControlFunctionality.UpdateValidationParams(value); }
        }

        public FormFieldsCSSClasses FieldCssClasses
        {
            get
            {
                return this.WebControlFunctionality.FieldCssClasses;
            }
            set
            {
                this.WebControlFunctionality.FieldCssClasses = value;
            }
        }

        #endregion
    }
}
