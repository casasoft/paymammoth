using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.General_v3.Controls.WebControls.Common
{

    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyTxtBoxNumeric runat=server></{0}:MyTxtBoxNumeric>")]

    public abstract class MyTxtBoxNumeric : MyTxtBox
    {
        public enum NUMERIC_RANGE
        {
            All = 0,
            PositiveOnly = 100,
            NegativeOnly = 200
        }
            

        public MyTxtBoxNumeric()
            : base()
        {
            _functionality.IsNumerical = true;
            
            
        }

        public NUMERIC_RANGE AllowedNumbers
        {
            get
            {
                if (_functionality.PositiveNumsOnly)
                {
                    return NUMERIC_RANGE.PositiveOnly;
                }
                else if (_functionality.NegativeNumsOnly)
                {
                    return NUMERIC_RANGE.NegativeOnly;
                }
                return NUMERIC_RANGE.All;
            }
            set
            {
                switch (value)
                {
                    case NUMERIC_RANGE.All: _functionality.NegativeNumsOnly = _functionality.PositiveNumsOnly = false; break;
                    case NUMERIC_RANGE.NegativeOnly: _functionality.NegativeNumsOnly = true; _functionality.PositiveNumsOnly = false; break;
                    case NUMERIC_RANGE.PositiveOnly: _functionality.PositiveNumsOnly = true; _functionality.NegativeNumsOnly = false; break;
                }
            }
           
        }
        public new double? FormValue
        {
            get
            {
                double v = 0;
                if (Double.TryParse(base.FormValue, out v))
                {
                    return v;
                }
                return null;
            }
        }
        public override object FormValueObject
        {
            get
            {
                return this.FormValue;
            }
        }
        public double? NumberFrom
        {
            get
            {
                return _functionality.NumberFrom;
            }
            set
            {
                _functionality.NumberFrom = value;
            }
        }
        public double? NumberTo
        {
            get
            {
                return _functionality.NumberFrom;
            }
            set
            {
                _functionality.NumberTo = value;
            }
        }
        public override object Value
        {
            get
            {
                string s = (string)base.Value;
                double? num = null;
                if (!string.IsNullOrEmpty(s))
                    num = Util.Conversion.ToDouble(s);
                return num;
            }
            set
            {
                SetValue(value);
            }
        }

        public override Enums.DATA_TYPE DataType
        {
            get
            {
                return Enums.DATA_TYPE.Double;
            }
        }
    }
}
