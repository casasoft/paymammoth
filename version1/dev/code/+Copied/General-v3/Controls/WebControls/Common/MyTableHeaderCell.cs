using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.General_v3.Controls.WebControls.Common
{

    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyTableHeaderCell runat=server></{0}:MyTableCell>")]

    public class MyTableHeaderCell : TableHeaderCell, IBaseWebControl

    {
        public MyTableHeaderCell() 
        {
            
            this._webControlFunctionality = new BaseWebControlFunctionality(this);
        }
        #region IBaseWebControl Members
        private BaseWebControlFunctionality _webControlFunctionality = null;
        public CS.General_v3.Classes.CSS.CSSManagerForControl CssManager
        {
            get { return _webControlFunctionality.CssManager; }
        }
        public new string CssClass
        {
            get
            {
                return _webControlFunctionality.CssClass;
            }
            set
            {
                _webControlFunctionality.CssClass = value;
                base.CssClass = _webControlFunctionality.CssClass;
            }
        }

        public string InnerText
        {
            get
            {
                return _webControlFunctionality.InnerText;
            }
            set
            {
                _webControlFunctionality.InnerText = value;
            }
        }

        public string InnerHtml
        {
            get
            {
                return _webControlFunctionality.InnerHtml;
            }
            set
            {
                _webControlFunctionality.InnerHtml = value;
            }
        }
        #endregion

        #region IBaseWebControl Members


        public BaseWebControlFunctionality WebControlFunctionality
        {
            get { return _webControlFunctionality; }
        }

        #endregion



        public string RollOverClass { get { return _webControlFunctionality.RollOverClass; } set { _webControlFunctionality.RollOverClass = value; } }
        public string StringID { get { return _webControlFunctionality.StringID; } set { _webControlFunctionality.StringID = value; } }
        public string OnClientBlur { get { return _webControlFunctionality.OnClientBlur; } set { _webControlFunctionality.OnClientBlur = value; } }
        public string OnClientClick { get { return _webControlFunctionality.OnClientClick; } set { _webControlFunctionality.OnClientClick = value; } }
        public string OnClientChange { get { return _webControlFunctionality.OnClientChange; } set { _webControlFunctionality.OnClientChange = value; } }
        public string OnClientLoad { get { return _webControlFunctionality.OnClientLoad; } set { _webControlFunctionality.OnClientLoad = value; } }



        #region IBaseWebControl Members


        public WebControl Control { get { return this; } }

        #endregion

        #region IBaseControl Members

        Control IBaseControl.Control { get { return this; } }

        BaseControlFunctionality IBaseControl.WebControlFunctionality { get { return _webControlFunctionality; } }
        #endregion
    }
}
