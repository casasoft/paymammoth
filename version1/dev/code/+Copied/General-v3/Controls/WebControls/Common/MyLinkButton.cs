using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common.General;

namespace CS.General_v3.Controls.WebControls.Common
{
    [DefaultProperty("Text")]
    [ParseChildren(true, "Text")]
    [ToolboxData("<{0}:MyLinkButton runat=server></{0}:MyLinkButton>")]
    public class MyLinkButton : BaseButtonWebControl, General.IMyButtonWebControl
    {
        private LinkButton _btnLink = new LinkButton();

        //private General.MyButtonWebControl _myWebControl = null;
        private MyButton _btnButton = new MyButton();
        public MyLinkButton()
            : this("")
        {
            this.WebControlFunctionality.FieldCssClasses.cssClassButton = "link-button";
        }
        protected override BaseWebControlFunctionality getWebControlFunctionality()
        {
            return new MyButtonWebControlFunctionality(this);
        }
        public void AddClassToAnchor(string cssClass)
        {
            _btnLink.Attributes["class"] += " " + cssClass;
        }

        public MyLinkButton(string ID)
            : base( HtmlTextWriterTag.A, ID)
        {

            this.ID = ID;
            // _myWebControl = new CS.General_v3.Controls.WebControls.Common.General.MyButtonWebControl(this, ClientIDSeparator);
            this.ValidationDefault = true;
            //this.WebControlFunctionality.CssManager.Clear();
           // this.WebControlFunctionality.CssManager.AddClass("link-button");
        }
        protected override void OnPreRender(EventArgs e)
        {
           // this.CssManager.Clear();
            if (!string.IsNullOrEmpty(this.Text))
            {
                this.Controls.Add(new Literal() { Text = this.Text });
            }
            if (!string.IsNullOrEmpty(_title))
            {
                this.Attributes["title"] = _title;
            }
            base.OnPreRender(e);
        }
        private string _title;
        public override string Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
            }
        }
     
        
        /*

        void _btnButton_Click(object sender, EventArgs e)
        {
            WebControlFunctionality.TriggerClickEvent();
        }

       
        

        protected override void OnLoad(EventArgs e)
        {
            _btnButton.ID = this.ID + "_button";
            _btnButton.Click += new EventHandler(_btnButton_Click);
            _btnButton.Style.Add("display", "none");
            _btnButton.ConfirmMessage = ConfirmMessage;
            this.Controls.Add(_btnButton);

            _btnLink.Attributes["href"] = "javascript:document.getElementById('" + _btnButton.ClientID + "').click();";
            _btnLink.Text = WebControlFunctionality.Text;
            this.Controls.Add(_btnLink);


            base.OnLoad(e);
        }

        #region IMyButtonWebControl Members


        string IMyButtonWebControl.PostBackUrl
        {
            get
            {
                return WebControlFunctionality.PostBackUrl;
            }
            set
            {
                WebControlFunctionality.PostBackUrl = value;
            }
        }
        */
        public new MyButtonWebControlFunctionality WebControlFunctionality
        {
            get { return (MyButtonWebControlFunctionality)base.WebControlFunctionality; }
        }

        //#endregion
    }
}
