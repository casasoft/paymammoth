using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.General_v3.Controls.WebControls.Common
{

    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyTxtBoxTextString runat=server></{0}:MyTxtBoxText>")]

    public abstract class MyTxtBoxTextString : MyTxtBoxText
    {
       

        public MyTxtBoxTextString()
            : base()
        {

        }

        
        /// <summary>
        /// Can contain alphabetical, digits & underscores
        /// </summary>
        public bool IsAlphabetical
        {
            get
            {
                return _functionality.IsAlphaNumerical;

            }
            set
            {
                _functionality.IsAlphaNumerical = value;

            }
        }

        public int? MaxWords
        {
            get
            {
                int maxWords = 0;
                if (Int32.TryParse(Attributes["maxWords"], out maxWords)) {
                    return maxWords;
                }
                return null;
            }
            set
            {
                if (value != null)
                {
                    Attributes["maxWords"] = value.ToString();
                }
                else
                {
                    Attributes.Remove("maxWords");

                }
            }
        } 
    }
}
