using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing.Design;
using CS.General_v3.JavaScript.Data;
namespace CS.General_v3.Controls.WebControls.Common.SWFObject.v2
{
    using JavaScript.Data;


    public class SWFObject : MyDiv
    {
        public MyDiv DivFlash { get; set; }

        public enum WMODE
        {
            Opaque,
            Window,
            Transparent,
            GPU,
            Direct,
            None
        }
        public enum SCALE
        {
            ShowAll,
            NoBorder,
            ExactFit,
            NoScale,
            None
        }
        public enum ALLOW_SCRIPT_ACCESS
        {
            None,
            Always,
            SameDomain
        }

        public class FUNCTIONALITY
        {
            /// <summary>
            /// The Flash Player - defaults to 9.0.0
            /// </summary>
            public string FlashVersion { get; set; }
            public bool UseExpressInstall { get; set; }
            public string ExpressInstallUrl { get; set; }
            //public string HtmlContainerId { get; set; }
            public string FlashUrl { get; set; }
            public Control AlternateContent { get; set; }
            public Unit FlashWidth { get; set; }
            public Unit FlashHeight { get; set; }

            public WMODE WindowMode { get; set; }
            public SCALE Scale { get; set; }
            public string FlashContentId { get; set; }
            public string Class { get; set; }
            public string Name { get; set; }
            public bool? AllowFullscreen { get; set; }
            public ALLOW_SCRIPT_ACCESS AllowScriptAccess { get; set; }
            public bool? Menu { get; set; }
            public JSObject FlashVars { get; private set; } 
            /// <summary>
            /// Format #RRGGBB
            /// </summary>
            public string BackgroundColor { get; set; }

            protected SWFObject _control;
            public FUNCTIONALITY(SWFObject control)
            {
                _control = control;
                this.FlashVars = new JSObject();
                this.FlashWidth = new Unit(100, UnitType.Percentage);
                this.FlashHeight = new Unit(100, UnitType.Percentage);
            }

        }










        public FUNCTIONALITY Functionality { get; private set; }

       
        public SWFObject(string flashUrl, string flashVersion = "9.0.0", string expressInstall = "/_common/static/flash/v1/swfobject/2.2/expressInstall.swf")
        {
            this.DivFlash = new MyDiv();
           
            this.Functionality = getFunctionality();
            this.Functionality.AllowScriptAccess = ALLOW_SCRIPT_ACCESS.None;
            this.Functionality.WindowMode = WMODE.None;
            this.Functionality.Scale = SCALE.None;
            this.Functionality.FlashVersion = flashVersion;
            this.Functionality.FlashUrl = flashUrl;

            this.Functionality.UseExpressInstall = true;
            this.Functionality.ExpressInstallUrl = expressInstall;
            this.Functionality.AllowFullscreen = true;

            this.Functionality.AlternateContent = new MyParagraph() { InnerHtml = "You need to have <a href='http://www.adobe.com/go/EN_US-H-GET-FLASH' target='_blank'>Adobe Flash Player v" + flashVersion + "</a> to view this content.  <a href='http://www.adobe.com/go/EN_US-H-GET-FLASH' target='_blank'>Get Adobe Flash Player.</a>" };
            
        }
        protected virtual FUNCTIONALITY getFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private void registerJS()
        {
            this.Controls.Add(DivFlash);
            DivFlash.ID = this.ClientID + "flashContainer";
            MyDiv divFlash = new MyDiv();
            this.Controls.Add(divFlash);
            divFlash.ID = this.ClientID + "flashContainer";
            if (this.Functionality.AlternateContent != null)
            {
                DivFlash.Controls.Add(this.Functionality.AlternateContent);
            }
            JSObject flashParams = new JSObject();
            if (this.Functionality.WindowMode != WMODE.None)
            {
                flashParams.AddProperty("wmode", WmodeToString(this.Functionality.WindowMode));
            }
            if (this.Functionality.Scale != SCALE.None)
            {
                flashParams.AddProperty("scale", ScaleToString(this.Functionality.Scale));
            }
            if (!String.IsNullOrEmpty(this.Functionality.BackgroundColor))
            {
                flashParams.AddProperty("bgcolor", this.Functionality.BackgroundColor);
            }
            if (this.Functionality.AllowFullscreen.HasValue)
            {
                flashParams.AddProperty("allowfullscreen", this.Functionality.AllowFullscreen.Value);
            }
            if (this.Functionality.Menu.HasValue)
            {
                flashParams.AddProperty("menu", this.Functionality.Menu.Value);
            }
            JSObject flashAttributes = new JSObject();
            flashAttributes.AddProperty("id", this.Functionality.FlashContentId);
            flashAttributes.AddProperty("name", this.Functionality.Name);
            if (String.IsNullOrEmpty(this.Functionality.FlashUrl))
            {
                throw new Exception("Please specify FlashUrl, the path to the .swf content");
            }


            string widthStr = this.Functionality.FlashWidth.ToString();

            string heightStr = this.Functionality.FlashHeight.ToString();
            string js = CS.General_v3.Util.SWFObjectUtil.GetSWFObjectV2JS(this.Functionality.FlashUrl, DivFlash.ClientID, widthStr,
                heightStr, this.Functionality.FlashVersion, this.Functionality.ExpressInstallUrl, this.Functionality.FlashVars, flashParams, flashAttributes);
            //string js = "swfobject.embedSWF('" + this.FlashUrl + "', '" + this.ClientID + "', '" + widthStr + "', '" + heightStr + "', '" + FlashVersion + "', '" + ExpressInstallUrl + "', " + FlashVars.GetJS() + ", " + flashParams.GetJS() + ", " + flashAttributes.GetJS() + ");";


            //if (!CS.General_v3.Util.Other.IsLocalTestingMachine)
           // {
            this.Page.ClientScript.RegisterStartupScript(this.GetType(), DivFlash.ClientID, js, true);
           // }




        }
        public static string WmodeToString(WMODE mode)
        {
            switch (mode)
            {
                case WMODE.Direct: return "direct";
                case WMODE.GPU: return "gpu";
                case WMODE.Opaque: return "opaque";
                case WMODE.Transparent: return "transparent";
                case WMODE.Window: return "window";

            }
            return null;
        }
        public static string ScaleToString(SCALE scale)
        {
            switch (scale)
            {
                case SCALE.ExactFit: return "exactfit";
                case SCALE.NoBorder: return "noborder";
                case SCALE.NoScale: return "noscale";
                case SCALE.ShowAll: return "showall";
            }
            return null;
        }

        protected override void OnLoad(EventArgs e)
        {
            
            registerJS();
            base.OnLoad(e);
        }
     
    }
}
