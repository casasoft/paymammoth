using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing.Design;
namespace CS.General_v3.Controls.WebControls.Common.SWFObject.v1
{
    public class NameValueTypeConvertor : System.ComponentModel.TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return base.CanConvertFrom(context, sourceType);
        }
        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {

            return base.ConvertFrom(context, culture, value);
        }
        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            return base.ConvertTo(context, culture, value, destinationType);
        }
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            return base.CanConvertTo(context, destinationType);
        }
        public override bool IsValid(ITypeDescriptorContext context, object value)
        {
            return base.IsValid(context, value);
        }

    }



    [DefaultProperty("File")]
    [ToolboxData("<{0}:SWFObject runat=server></{0}:SWFObject>")]
    public class SWFObject : WebControl
    {
        public enum FLASH_QUALITY
        {
            Low,  High, AutoLow,AutoHigh,Best,Default
        }
        public enum BACKGROUND_TYPE
        {
            Window,Opaque,Transparent,Default
        }
        public SWFObject()
        {
        }
        
        public bool InDesignMode
        {
            get
            {
                return (System.Web.HttpContext.Current == null);
            }
        }

        public static string BackgroundTypeToText(BACKGROUND_TYPE type)
        {
            switch (type)
            {
                case BACKGROUND_TYPE.Opaque: return "opaque";
                case BACKGROUND_TYPE.Transparent: return "transparent";
                case BACKGROUND_TYPE.Window: return "window";
            }
            return "";
        }
        public static string FlashQualityToText(FLASH_QUALITY quality)
        {
            switch (quality)
            {
                case FLASH_QUALITY.High: return "high";
                case FLASH_QUALITY.Low: return "low";
                case FLASH_QUALITY.AutoLow: return "autolow";
                case FLASH_QUALITY.AutoHigh: return "autohigh";
                case FLASH_QUALITY.Best: return "best";

                
            }
            return "null";
        }
        private BACKGROUND_TYPE _BackgroundType = BACKGROUND_TYPE.Default;
        [Category("SWFObject Parameters")]
        [Description("Establishes whether the objects that accompany the Flash animation are visible.")]
        public BACKGROUND_TYPE BackgroundType { get { return _BackgroundType; } set { _BackgroundType = value; } }


        private string _SWFObjectID
        {
            get
            {
                return this.ID + "_so";
            }
        }
        

        private string _DetectKeyVariableName = "detectFlash";
        [Category("SWFObject Parameters")]
        [Description("This is the url variable name the SWFObject script will look for when bypassing the detection. Default is 'detectflash'. Example: To bypass the Flash detection and simply write the Flash movie to the page, you could add ?detectflash=false to the url of the document containing the Flash movie.")]
        public string DetectKeyVariableName
        {
            get { return _DetectKeyVariableName;}
            set { _DetectKeyVariableName = value;}
        }
        private string _RedirectURLIfNotCorrectVersion = "";
        [Category("SWFObject Parameters")]
        [Description("If you wish to redirect users who don't have the correct plug-in version, use this parameter and they will be redirected.")]
        public string RedirectURLIfNotCorrectVersion
        {
            get { return _RedirectURLIfNotCorrectVersion;}
            set { _RedirectURLIfNotCorrectVersion = value;}
        }

        private FLASH_QUALITY _FlashQuality= FLASH_QUALITY.Default;
        [Category("SWFObject Parameters")]
        [Description("The quality of the flash movie.")]
        public FLASH_QUALITY FlashQuality { get { return _FlashQuality; } set { _FlashQuality = value;}}

        private bool _UseDefer = false;

        [Category("SWFObject Parameters")]
        [Description("Whether to add the 'defer=\"defer\"' property to the javascript 'script' tag")]
        public bool UseDefer
        {
          get { return _UseDefer; }
          set { _UseDefer = value; }
        }

        private string _SWFHeight = "640";
        [Category("SWFObject Parameters")]
        [Description("The height of the flash movie. Can be in percentage")]
        public new string SWFHeight
        {
	          get 
	        {
                return _SWFHeight;
	        }
	          set 
	        {
                _SWFHeight = value;
	        }
        }
        private string _SWFWidth = "100%";
        [Category("SWFObject Parameters")]
        [Description("The width of the flash movie. Can be in percentage")]
        public string SWFWidth
        {
	          get 
	        {
                return _SWFWidth;
	        }
	          set 
	        {
                _SWFWidth = value;
	        }
        }

        private string _SWFPath;

        [Category("SWFObject Parameters")]
        [Description("The file path and name to your swf file.")]
        public string SWFPath
        {
            get { return _SWFPath; }
            set { _SWFPath = value; }
        }
        private string _FlashVersionRequired;

        [Category("SWFObject Parameters")]
        [Description("The required player version for your Flash content. This can be a string in the format of 'majorVersion.minorVersion.revision'. An example would be: \"6.0.65\""+
            ". Or you can just require the major version, such as '6'.")]
        public string FlashVersionRequired
        {
            get { return _FlashVersionRequired; }
            set { _FlashVersionRequired = value; }
        }
        private string _ElementID;
        [Category("SWFObject Parameters")]
        [Description("The HTML element in which to show the Flash movie")]
        public string ElementID
        {
            get { return _ElementID; }
            set { _ElementID = value; }
        }
        private string _SWFID;
        [Category("SWFObject Parameters")]
        [Description("The ID of your object or embed tag. The embed tag will also have this value set as it's name attribute for files that take advantage of swliveconnect.")]
        public string SWFID
        {
            get { return _SWFID; }
            set { _SWFID = value; }
        }
        private System.Drawing.Color _BackgroundColor;

        [Category("SWFObject Parameters")]
        [Description("The background color of your Flash movie.")]
        public System.Drawing.Color BackgroundColor
        {
            get { return _BackgroundColor; }
            set { _BackgroundColor = value; }
        }

        

        private NameValueCollection _Variables;
        [Category("SWFObject Parameters")]
        [Description("Extra variables to be added to the flash, in the form of <addVariable>")]
        public NameValueCollection Variables
        {
            get 
            {
                if (_Variables == null)
                    _Variables = new NameValueCollection();
                return _Variables; 
            }
            set { _Variables = value; }
        }

        private NameValueCollection _ExtraParameters = new NameValueCollection();

        [Category("SWFObject Parameters")]
        [Description("Extra paramters to be added to the flash, in the form of <addParam>")]
        [TypeConverter(typeof(NameValueTypeConvertor))]
        public NameValueCollection ExtraParameters
        {
            get 
            {
                return _ExtraParameters;
            
            }
        }
        private bool _UseExpressInstall = true;

        [Category("SWFObject Parameters")]
        [Description("The file path and name to your swf file.")]
        public bool UseExpressInstall
        {
            get { return _UseExpressInstall; }
            set { _UseExpressInstall = value; }
        }
        private string _ExpressInstallRedirectURL;

        [Category("SWFObject Parameters")]
        [Description("The file path and name to your swf file.")]
        public string ExpressInstallRedirectURL
        {
            get { return _ExpressInstallRedirectURL; }
            set { _ExpressInstallRedirectURL = value; }
        }



        protected override void RenderContents(HtmlTextWriter writer)
        {
            
            if (!InDesignMode)
            {

                string s = "";
                s += "<script ";
                if (UseDefer)
                    s += "defer=\"defer\"";
                string backColor = "#FFFFFF";
                string sDetectKey = "null", sQuality = "null", sXIRedirect = "null", sRedirect = "null";
                if (DetectKeyVariableName != null && DetectKeyVariableName != "")
                    sDetectKey = "'" + DetectKeyVariableName + "'";
                if (FlashQuality != FLASH_QUALITY.Default)
                    sQuality = "'" + FlashQualityToText(FlashQuality) + "'";
                if (RedirectURLIfNotCorrectVersion != null && RedirectURLIfNotCorrectVersion != "")
                    sRedirect = "'" + RedirectURLIfNotCorrectVersion + "'";
                if (ExpressInstallRedirectURL != null && ExpressInstallRedirectURL != "")
                    sXIRedirect = "'" + ExpressInstallRedirectURL + "'";

                s += " type=\"text/javascript\">";
                s += "var " + this._SWFObjectID + " = new SWFObject('" +
                    SWFPath + "','" + SWFID + "','" + SWFWidth + "','" + SWFHeight+ "','" + FlashVersionRequired + "','" + backColor +
                    "'," + sQuality + "," + sXIRedirect + "," + sRedirect + "," + sDetectKey + ");";
                if (ElementID == "" || ElementID == null)
                {
                    throw new InvalidDataException("Please fill in Element ID of SWFObject:" + this.ID);
                }
                if (SWFPath == "" || SWFPath == null)
                {
                    throw new InvalidDataException("Please fill in SWF Path of SWFObject:" + this.ID);
                }

                //add parameters
                if (BackgroundType != BACKGROUND_TYPE.Default)
                {
                    s += this._SWFObjectID + ".addParam('wmode','"+ BackgroundTypeToText(BackgroundType) + "');";
                }
                if (_ExtraParameters != null)
                {
                    for (int i = 0; i < ExtraParameters.Count; i++)
                    {
                        s += this._SWFObjectID + ".addParam('"+_ExtraParameters.Keys[i]+"','"+_ExtraParameters[i]+"');";
                    }
                }
                //add variables

                if (_Variables != null)
                {
                    for (int i = 0; i < _Variables.Count; i++)
                    {
                        s += this._SWFObjectID + ".addVariable('" + _Variables.Keys[i] + "','" + _Variables[i] + "');";
                    }
                }

                s += this._SWFObjectID + ".write('"+this.ElementID+"');";
                
                s += "</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(),this._SWFObjectID + "_js", s);
                
            }
            else
            {
                string s = "";
                s = "{SWFOBJECT: " + this.SWFPath + "}";
                writer.Write(s);
            }
        }
    }
}
