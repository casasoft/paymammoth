using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.General_20101215.Controls.WebControls.Common
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyLabel runat=server></{0}:MyLabel>")]
    public class MyLabel2 : WebControl, General.IMyWebControl
    {
        public override string ID
        {
            get
            {
                return base.ID;
            }
            set
            {
                base.ID = value;
            }
        }
        public string StringID { get; set; }
        private BaseWebControl _functionality = null;
        public MyLabel2() : base()
        {
            
            _functionality = new CS.General_20101215.Controls.WebControls.Common.General.MyWebControl(this, ClientIDSeparator);
        }
        public string RollOverClass { get { return _functionality.RollOverClass; } set { _functionality.RollOverClass = value; } }
        

        [Bindable(true)]
        [Category("Data Storage")]
        [DefaultValue("")]
        [Localizable(true)]
        [Description("Extra Information")]

        public object Tag
        {
            get
            {
                return _functionality.Tag;
            }

            set
            {
                _functionality.Tag = value;

            }
        }
        public bool DoNotCopyHelpMsgFromAttachedControl { get; set; }
        public bool DoNotAttachToControl { get; set; }
        public Control ForControl {get;set;}
        private string _ForControlID = null;
        public string ForControlID
        {
            get
            {
                return _ForControlID;
            }
            set
            {
                _ForControlID = value;
            }
        }
        public class NextControlFinder
        {
            public NextControlFinder(Control c)
            {
                this.Control = c;
            }
            public Control Control {get;set;}
            private Queue<Control> queue = new Queue<Control>();
            private List<Control> doneList = new List<Control>();
            public Control FindNext()
            {
                Control curr = Control;
                queue.Enqueue(curr);
                while (queue.Count > 0)
                {
                    curr = queue.Dequeue();
                    if (curr != Control && isControlAFormControl(curr))
                    {
                        return curr;
                    }
                    for (int i = 0; i < curr.Controls.Count; i++)
                    {
                        if (isControlAFormControl(curr.Controls[i]))
                            return curr.Controls[i];
                        else
                            queue.Enqueue(curr.Controls[i]);
                    }
                    doneList.Add(curr);
                    if (curr.Parent != null)
                    {
                        bool foundCurr = false;
                        for (int i = 0; i < curr.Parent.Controls.Count; i++)
                        {
                            Control tmpC = curr.Parent.Controls[i];
                            if (tmpC == curr)
                                foundCurr = true;
                            else
                            {
                                if (foundCurr && !doneList.Contains(tmpC))
                                    queue.Enqueue(tmpC);
                            }
                        }
                    }
                }
                return null;
            }
            private bool isControlAFormControl(Control c)
            {
                return (c is General.IMyFormWebControl);
            }
        }
        
        private Control getNextControlAfterLabel()
        {
            NextControlFinder finder = new NextControlFinder(this);
            Control c = finder.FindNext();
            return c;

        }
        private string getForControlID()
        {
            
            if (ForControl != null)
                return ForControl.ClientID;
            else
            {
                Control c = null;
                if (!string.IsNullOrEmpty(ForControlID))
                {
                    c = CS.General_20101215.Util.Other.FindControl(Page, ForControlID);
                    if (c == null)
                        throw new InvalidOperationException("Control with id '" + ForControlID + "' set as 'for' property for label '" + this.ID + "' does not exist!");
                }
                else
                {
                    c = getNextControlAfterLabel();
                }
                if (c != null)
                {
                    if (!this.DoNotCopyHelpMsgFromAttachedControl)
                    {
                        this.HelpID = ((General.IMyWebControl)c).HelpID;
                        this.HelpMsg= ((General.IMyWebControl)c).HelpMsg;
                    }
                    return c.ClientID;
                }
                else
                    return null;
            }
        }

        public string HelpID { get { return _functionality.HelpID; } set { _functionality.HelpID= value; } }
        public string HelpMsg { get { return _functionality.HelpMsg; } set { _functionality.HelpMsg= value; } }

        public string OnClientClick { get { return _functionality.OnClientClick; } set { _functionality.OnClientClick = value; } }
        public string OnClientLoad { get { return _functionality.OnClientLoad; } set { _functionality.OnClientLoad = value; } }
        public string OnClientChange { get { return _functionality.OnClientChange; } set { _functionality.OnClientChange = value; } }
        public string OnClientBlur { get { return _functionality.OnClientBlur; } set { _functionality.OnClientBlur = value; } }
        public string Text { get; set; }

        private void renderLabel(HtmlTextWriter writer)
        {
            WebControl wc = new WebControl(HtmlTextWriterTag.Label);
            wc.ID = this.ClientID;
            wc.CssClass = this.CssClass;
            CS.General_20101215.Util.Other.CopyAttribsToControl(this.Attributes, wc);
            Literal litTitle = new Literal();
            litTitle.Text = this.Text;
            wc.Controls.Add(litTitle);
            wc.Enabled = this.Enabled;
            wc.BorderColor = this.BorderColor;
            wc.BorderStyle = this.BorderStyle;
            wc.BackColor = this.BackColor;
            wc.BorderWidth = this.BorderWidth;
            wc.Height = this.Height;
            wc.ForeColor = this.ForeColor;
            CS.General_20101215.Util.Other.CopyStylesToControl(this.Style, wc.Style);
            wc.Width = this.Width;
            wc.Visible = this.Visible;
            //wc.Controls.Add(new Literal()
            wc.RenderControl(writer);
            //wc.RenderBeginTag(writer);
            
            //wc.Attributes.Render(writer);
            //writer.Write(this.Text);
            //wc.RenderEndTag(writer);

        }
        protected override void Render(HtmlTextWriter writer)
        {
            if (!DoNotAttachToControl)
            {
                string forControlID = getForControlID();
                if (!string.IsNullOrEmpty(forControlID))
                    Attributes["for"] = forControlID;
            }
            
            renderLabel(writer);
            _functionality.Render(writer);
            
            
        }

        #region IMyWebControl Members


        public new WebControl Control
        {
            get { return this; }
        }

        #endregion
    }
}
