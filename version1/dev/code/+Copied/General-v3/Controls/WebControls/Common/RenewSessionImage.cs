using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace CS.General_v3.Controls.WebControls.Common
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:RenewSessionImage runat=server></{0}:RenewSessionImage>")]
    public class RenewSessionImage : WebControl
    {
        private string _RenewURL = "/renewSession.aspx";
        private int _RenewInterval = 600; //10minutes
        private bool _ResolveClientURL = true;
        [Description("The URL to the page that calls Page.SendRenewSessionImage()")]
        public string RenewURL
        {
            get
            {
                return _RenewURL;
            }
            set
            {
                _RenewURL = value;
            }
        }
        [Description("Whether to resolve the client URL")]
        public bool ResolveURL
        {
            get
            {
                return _ResolveClientURL;
            }
            set
            {
                _ResolveClientURL = value;
            }
        }

        /// <summary>
        /// The interval for renewing the session, in seconds
        /// </summary>
        [Description("The interval for renewing the session, in seconds")]
        [DefaultValue(60)]
        public int RenewInterval
        {
            get
            {
                return _RenewInterval;
            }
            set
            {
                _RenewInterval = value;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }


        protected override void Render(HtmlTextWriter writer)
        {
            string sClass = "";
            string url;
            if (_ResolveClientURL)
                url = ResolveClientUrl(_RenewURL);
            else
                url = _RenewURL;
            if (CssClass != "" && CssClass != null)
            {
                sClass = " class=\"" + CssClass + "\" ";
            }
            StringBuilder sb = new StringBuilder();
            

            sb.AppendLine("<script type=\"text/javascript\" language=\"Javascript\">");
            sb.AppendLine("function __autoRenewSession() {");
            //sb.AppendLine("alert('Renewing session image');");
            sb.AppendLine("var img = document.getElementById(__renewSessionImageId);");
            sb.AppendLine("var rndNum = Math.floor(Math.random()*1111111111111111111);");
            sb.AppendLine("img.src = __renewSessionURL + '?rndnum=' + rndNum;");
            sb.AppendLine("}");
            sb.AppendLine("window.setInterval('__autoRenewSession()', " + (RenewInterval * 1000) + ");");
            
            sb.AppendLine("var __renewSessionImageId = 'renewSessionImage';");
            sb.AppendLine("var __renewSessionURL = '" + url + "';");
            sb.AppendLine("var __renewSessionInterval = " + (RenewInterval * 1000) + ";");
            sb.AppendLine("</script>");
            sb.AppendLine("<img src=\"" + url + "\" " + sClass + " name=\"renewSessionImage\" id=\"renewSessionImage\">");
            writer.Write(sb.ToString());

        }
    }
}
