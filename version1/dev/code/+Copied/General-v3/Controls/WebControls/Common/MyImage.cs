using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.General_v3.JavaScript.Classes.ShadowBox;
using System.Web.UI.HtmlControls;


namespace CS.General_v3.Controls.WebControls.Common
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyImage runat=server></{0}:MyImage>")]
    public class MyImage : BaseWebControl, IBaseWebControl
    {

        //public object Tag { get { return _functionality.Tag; } set { _functionality.Tag = value; } }
        public string LongDesc { get; set; }

        public string Rel { get; set; }
        /// <summary>
        /// Sets this image with a shadow box.  It is important then that you include JS / CSS file.
        /// </summary>
        public string ShadowBoxGalleryID { get; set; }

        private BaseWebControlFunctionality _functionality = null;
        
        public MyImage()
            : base( )
        {
            _functionality = new BaseWebControlFunctionality(this);
          
            this.OnClickClickButton = null;
        }
        private bool _resolveClientHRef;
        //private System.Web.UI.AttributeCollection _Attributes = null;
        public string OnClickClickButton { get; set; }
        //private string _JSAfterControl = "";
        public string _aHref = "";
        public Enums.HREF_TARGET _aHrefTarget = Enums.HREF_TARGET.Self;
        private string _rollOverImage = "";
        private string _showAltIn = "";
        private string _ImageUrl = "";
        private string _AlternateText = "";
//        private string _Style = "";
        
        [Category("Image")]
        [DefaultValue("")]
        [Description("Whether to resolve the URL to the client's location")]
        private bool _ResolveImageURLToClient = false;
        public bool ResolveImageURLToClient
        {
            get { return _ResolveImageURLToClient; }
            set { _ResolveImageURLToClient = value; }
        }
        private int _Width;
        public new int Width
        {
            get
            {
                return _Width;
            }
            set
            {
                _Width = value;
            }
        }
        public new string BorderWidth
        {
            get
            {
             return "";   
            }
            set
            {}
        }
        public new string BorderStyle
        {
            get
            {
                return "";
            }
            set
            { }
        }

        


        /*public string CssClass
        {
            get
            {
                return _myWebControl.CssClass;
            }
            set
            {
                _myWebControl.CssClass = value;
            }
        }
/*
        public string Style
        {
            get
            {
                return _Style;
            }
            set
            {
                _Style = value;
            }
        }*/

        /// <summary>
        /// Gets the list of attributes
        /// </summary>
        /*public new System.Web.UI.AttributeCollection Attributes
        {
            get
            {
                if (_Attributes == null)
                    _Attributes = new System.Web.UI.AttributeCollection(//);
                return _Attributes;
            }
        }*/
        public string AlternateText
        {
            get
            {
                return _AlternateText;
            }
            set
            {
                _AlternateText = value;
            }
        }


        [UrlProperty("*")]
        public string ImageUrl
        {
            get
            {
                return _ImageUrl;
            }
            set
            {
                _ImageUrl = value;
                if (String.IsNullOrEmpty(_ImageUrl))
                {
                 //   _ImageUrl = "#[NULL]";
                }
            }
        }
        


        [Bindable(true)]
        [Category("HTML")]
        [DefaultValue("")]
        [Localizable(true)]
        [Description("The link that the image should point to")]

        public string HRef
        {
            get
            {
                return _aHref;
            }
            
            set
            {
                _aHref = value;
            }
        }

        [Bindable(true)]
        [Category("HTML")]
        [DefaultValue(Enums.HREF_TARGET.Self)]
        [Localizable(true)]
        [Description("The link that the image should point to")]

        public Enums.HREF_TARGET HRefTarget
        {
            get
            {
                return _aHrefTarget;
            }

            set
            {
                _aHrefTarget = value;
            }
        }

        [Bindable(true)]
        [Category("Javascript")]
        [DefaultValue("")]
        [Localizable(true)]
        [Description("Resolve HRef to Client URL")]

        public bool ResolveClientHRef
        {
            get
            {
                return _resolveClientHRef;
            }

            set
            {
                _resolveClientHRef = value;
            }
        }

        
        [Bindable(true)]
        [Category("Javascript")]
        [DefaultValue("")]
        [Localizable(true)]
        [Description("The location of the image to show, on mouseover of the image")]
        
        public string RollOverImage
        {
            get
            {
                return _rollOverImage;
            }

            set
            {
                _rollOverImage = value;
            }
        }

        [Bindable(true)]
        [Category("Javascript")]
        [DefaultValue("")]
        [Localizable(true)]
        [Description("The CLIENT ID of the tag to show it's ALT tag in once mouse is over image")]
        public string ShowAltIn
        {
            get
            {
                return _showAltIn;
            }

            set
            {
                _showAltIn = value;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }
        private HtmlImage createImage()
        {
            HtmlImage img = new HtmlImage();
            string imgURL = ImageUrl == null ? "" : ImageUrl;
            if (!string.IsNullOrEmpty(this.CssClass))
            {
                img.Attributes["class"] = this.CssManager.ToString();// +"-image " + this.CssClass;
            }
            if (ResolveImageURLToClient)
                img.Src = ResolveClientUrl(imgURL);
            else
                img.Src = imgURL;
            img.Alt = AlternateText;
            if (!string.IsNullOrEmpty(LongDesc))
            {
                
                img.Attributes["longdesc"] = LongDesc;
            }
            

            return img;
        }
        private MyAnchor createAnchor()
        {
            MyAnchor a = new MyAnchor();
            if (!string.IsNullOrEmpty(this.CssClass))
            {
                this.CssManager.AppendCssClasses("-link");
                a.CssClass = this.CssManager.ToString();// +"-link";
                //a.CssManager.AddClass(this.CssClass);
            }

            if (!string.IsNullOrEmpty(Rel))
            {
                a.Attributes["rel"] = Rel;
            }
            if (!_resolveClientHRef)
                a.Href = _aHref;
            else
                a.Href = ResolveClientUrl(_aHref);

           
            if (_AlternateText != null)
                 a.Title = _AlternateText;

            string target = "";
            a.HRefTarget = _aHrefTarget;

            return a;
        }
        private void outputJS()
        {

            if (!String.IsNullOrEmpty(this.RollOverImage) || !String.IsNullOrEmpty(this.OnClickClickButton))
            {
                string js = "";
                js += "var tmpImage = new js.com.cs." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION + ".UI.Image('" + this.ClientID + "');\r\n";
                if (!string.IsNullOrEmpty(this.RollOverImage))
                {
                    string path = this.RollOverImage;
                    if (ResolveImageURLToClient)
                        path = ResolveClientUrl(path);
                    js += "tmpImage.ImageRollover = '" + CS.General_v3.Util.Text.forJS(path) + "';";
                }
                if (!string.IsNullOrEmpty(this.OnClickClickButton))
                {
                    js += "tmpImage.SetOnClick_ClickButton('" + CS.General_v3.Util.Text.forJS(this.OnClickClickButton) + "');";
                }
                //js += "}";
                CS.General_v3.Util.JSUtilOld.AddJSScriptToPage(js);
                //_JSAfterControl += (_myWebControl.getElementFields("_tmpImage.element"));

                //this.Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID + "_imageJS", _JSAfterControl, true);
                //writer.Write(CS.General_v3.Util.Text.MakeJavaScript(_JSAfterControl));
            }
        }
        protected override void Render(HtmlTextWriter writer)
        {
            /* Create Image */

            this.Controls.Clear();
            HtmlImage img = createImage();

            if (!string.IsNullOrEmpty(HRef))
            {
                //Contains link
                MyAnchor a = createAnchor();
                a.Controls.Add(img);
                this.Controls.Add(a);
            }
            else
            {
                this.Controls.Add(img);
            }
            outputJS();
            base.Render(writer);
            //Commented by Mark 2011-02-23 to make the code better using controls not render HTML


/*
            if (String.IsNullOrEmpty(_ImageUrl))
            {
                _ImageUrl = null; //Set default value
            }
            base.Render(writer);
           
            if (this.Attributes["onmouseover"] == null)
                this.Attributes["onmouseover"] = "";
            if (this.Attributes["onmouseout"] == null)
                this.Attributes["onmouseout"] = "";

            //Attributes["style"] += _Style;
            Attributes["class"] += CssClass;
            if (_Width > 0)
            {
                Attributes.CssStyle["width"] = _Width.ToString() + "px";
            }
            if (!Height.IsEmpty)
            {
                Attributes.CssStyle["height"] = Height.Value.ToString() + "px";
            }
            if (_showAltIn != "")
            {
                this.Attributes["onmouseover"] += ";getElem('" + _showAltIn + "').innerHTML = this.alt;";
                this.Attributes["onmouseout"] += ";getElem('" + _showAltIn + "').innerHTML = '&nbsp;'";
            }

           


            if (_aHref != "")
            {
                writer.WriteBeginTag("a");
                if (!string.IsNullOrEmpty(Rel))
                {
                    writer.WriteAttribute("rel", Rel);
                }
                if (!_resolveClientHRef)
                    writer.WriteAttribute("href", _aHref);
                else
                    writer.WriteAttribute("href", ResolveClientUrl(_aHref));
                if (_AlternateText != null)
                    writer.WriteAttribute("title", _AlternateText);
                string target = "";
                switch (_aHrefTarget)
                {
                    case Enums.HREF_TARGET.Self:
                        target = "_self";
                        break;
                    case Enums.HREF_TARGET.Blank:
                        target = "_blank";
                        break;
                    case Enums.HREF_TARGET.Parent:
                        target = "_parent";
                        break;
                }
                writer.WriteAttribute("target", target);
                writer.Write(HtmlTextWriter.TagRightChar);
            }
            #region renderImage
            /*if (_rollOverImage != "")
            {
                this.Attributes["onmouseover"] += "changeimg(this,'over'); ";
                this.Attributes["onmouseout"] += "changeimg(this,'out'); ";
            }*/

            /*
            writer.WriteBeginTag("img");
            Attributes["id"] = this.ClientID;
            if (this.CssClass != "" && this.CssClass != null)
            {
                Attributes["class"] = this.CssClass;
            }
            if (ImageUrl == null)
                ImageUrl = "";
            if (ResolveImageURLToClient)
                Attributes["src"] = ResolveClientUrl(ImageUrl);
            else
                Attributes["src"] = ImageUrl;
            Attributes["alt"] = AlternateText;
            if (Attributes["alt"] == "") Attributes["alt"] = " ";
            Attributes["title"] = AlternateText;
            Attributes["longdesc"] = LongDesc == null ? _aHref : LongDesc ;
            Attributes.Render(writer);
            writer.Write(" />");
            #endregion
            
            if (_aHref != "")
            {
                writer.WriteEndTag("a");
            }

            if (!String.IsNullOrEmpty(this.RollOverImage) || !String.IsNullOrEmpty(this.OnClickClickButton))
            {
                _JSAfterControl += "_tmpImage = new js.com.cs."+CS.General_v3.Settings.Others.JAVASCRIPT_VERSION+".UI.Image('" + this.ClientID + "');";
                if (!string.IsNullOrEmpty(this.RollOverImage))
                {
                    string path = this.RollOverImage;
                    if (ResolveImageURLToClient)
                        path = ResolveClientUrl(path);
                    _JSAfterControl += "_tmpImage.ImageRollover = '" + CS.General_v3.Util.Text.forJS(path) + "';";
                }
                if (!string.IsNullOrEmpty(this.OnClickClickButton))
                {
                    _JSAfterControl += "_tmpImage.SetOnClick_ClickButton('" + CS.General_v3.Util.Text.forJS(this.OnClickClickButton) + "');";
                }
                //_JSAfterControl += (_myWebControl.getElementFields("_tmpImage.element"));

                writer.Write(CS.General_v3.Util.Text.MakeJavaScript(_JSAfterControl));
            }
            */



        }
        private void initShadowbox()
        {
            if (!string.IsNullOrEmpty(ShadowBoxGalleryID))
            {
                this.Rel = ShadowBoxGallery.GetRelValue(this.AlternateText, ShadowBoxGalleryID);
                ShadowBoxUtil.InitShadowBox();
            }
        }
        protected override void OnLoad(EventArgs e)
        {
            initShadowbox();
            base.OnLoad(e);
        }

        public new WebControl Control
        {
            get { return this; }
        }

        

    }
}
