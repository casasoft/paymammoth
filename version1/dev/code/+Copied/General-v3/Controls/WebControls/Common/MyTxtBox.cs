using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common.General;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;

namespace CS.General_v3.Controls.WebControls.Common
{

    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyTxtBox runat=server></{0}:MyTxtBox>")]

    public abstract class MyTxtBox : TextBox, IMyFormWebControl
    {
        //private bool _focusOnLoad = false;


        public string GetFormValueAsStr()
        {
            return this.FormValue;
        }
        public string StringID { get { return _functionality.StringID; } set { _functionality.StringID = value; } }
        public string ClientName
        {
            get
            {
                return _functionality.ClientName;
            }
        }

        private General.MyFormWebControlFunctionality __functionality = null;
        protected General.MyFormWebControlFunctionality _functionality
        {
            get
            {
                if (__functionality == null)
                {
                    __functionality = createFunctionality();
                }
                return __functionality;
            }
        }

        protected MyTxtBox()
            : base()
        {

        }


        protected virtual MyFormWebControlFunctionality createFunctionality()
        {
            return new MyFormWebControlFunctionality(this, ClientIDSeparator);
        }

        public bool Required
        {
            get { return _functionality.Required; }
            set { _functionality.Required = value; }
        }

        public virtual void SetValue(object o)
        {
            if (o is DateTime)
            {
                int k = 5;
            }
            this.Text = _functionality.SetValue(o);


        }
        public string InitialEmptyText
        {
            get { return _functionality.InitialEmptyText; }
            set
            {
                _functionality.InitialEmptyText = value;
            }
        }

        [Category("Validation")]
        [DefaultValue(false)]
        [Description("Whether to validate control on blur or not")]
        public bool DoNotValidateOnBlur
        {
            get
            {
                return _functionality.DoNotValidateOnBlur;
            }

            set
            {
                _functionality.DoNotValidateOnBlur = value;

            }
        }

        [Category("Validation")]
        [DefaultValue("")]
        [Description("Value of textbox must NOT be contained in the list. Delimter is by default \",\", but can be specified through the 'Delimeter' attribute")]
        public List<string> ValueIn
        {
            get
            {
                return _functionality.ValueIn.ConvertAll<string>(item => item.ToString());
            }

            set
            {
                _functionality.ValueIn = value.ConvertAll<object>(item => (object)item);

            }
        }

        public bool AutoCompleteDisabled { get; set; }

        [Category("Validation")]
        [DefaultValue("")]
        [Description("Value of textbox must NOT be contained in the list. Delimter is by default \",\", but can be specified through the 'Delimeter' attribute")]

        public List<string> ValueNotIn
        {
            get
            {
                return _functionality.ValueNotIn.ConvertAll<string>(item => item.ToString());
            }

            set
            {
                _functionality.ValueNotIn = value.ConvertAll<object>(item => (object)item);

            }
        }




        #region IMyWebControl Members

        public object Tag
        {
            get { return _functionality.Tag; }
            set { _functionality.Tag = value; }
        }

        public string OnClientClick { get { return _functionality.OnClientClick; } set { _functionality.OnClientClick = value; } }
        public string OnClientLoad { get { return _functionality.OnClientLoad; } set { _functionality.OnClientLoad = value; } }
        public string OnClientChange { get { return _functionality.OnClientChange; } set { _functionality.OnClientChange = value; } }
        public string OnClientBlur { get { return _functionality.OnClientBlur; } set { _functionality.OnClientBlur = value; } }
        public string HelpID
        {
            get
            {
                return _functionality.HelpID;
            }
            set
            {
                _functionality.HelpID = value;
            }
        }

        public string HelpMsg
        {
            get
            {
                return _functionality.HelpMsg;
            }
            set
            {
                _functionality.HelpMsg = value;
            }
        }

        public string RollOverClass { get { return _functionality.RollOverClass; } set { _functionality.RollOverClass = value; } }


        #endregion

        #region IMyFormWebControl Members


        public override string ValidationGroup
        {
            get
            {
                return _functionality.ValidationGroup;
            }
            set
            {
                _functionality.ValidationGroup = value;
            }
        }
        [Category("Title")]
        [DefaultValue("")]
        [Description("Title to show for validation")]
        public string Title
        {
            get
            {
                return _functionality.Title;
            }

            set
            {
                _functionality.Title = value;
                /*
                */
            }
        }
        /// <summary>
        /// The value retrieved from the posted Form
        /// </summary>
        public virtual object FormValueObject
        {
            get
            {

                return _functionality.FormValue;
            }

        }
        public virtual object Value
        {
            get
            {
                return this.Text;
            }
            set
            {
                SetValue(value);

            }
        }
        #region classes
        [Category("Javascript")]
        [DefaultValue(false)]
        [Description("Whether to use default CSS classes or not")]
        public bool UseDefaultCSSClasses
        {
            get
            {
                return _functionality.UseDefaultCSSClasses;
            }

            set
            {
                _functionality.UseDefaultCSSClasses = value;

            }
        }
        public string Class_Disabled { get { return _functionality.Class_Disabled; } set { _functionality.Class_Disabled = value; } }
        public string Class_onFocus { get { return _functionality.Class_Focus; } set { _functionality.Class_Focus = value; } }
        public string Class_onError { get { return _functionality.Class_Error; } set { _functionality.Class_Error = value; } }
        public string Class_onOver { get { return _functionality.Class_Over; } set { _functionality.Class_Over = value; } }
        public string Class_ReadOnly { get { return _functionality.Class_ReadOnly; } set { _functionality.Class_ReadOnly = value; } }
        public string Class_Required { get { return _functionality.Class_Required; } set { _functionality.Class_Required = value; } }



        #endregion

        public string FormValue
        {
            get
            {
                return _functionality.FormValue;
            }
        }

        #endregion



        #region IMyFormWebControl Members

        public bool HasValidation
        {
            get
            {
                return _functionality.HasValidation;

            }
            set
            {
                _functionality.HasValidation = value;

            }
        }

        #endregion


        protected override void Render(HtmlTextWriter writer)
        {

            //this.Attributes["id"] = this.ClientID;
            //Attributes["onblur"] += ";" + _onClientBlur + ";";
            base.Render(writer);


        }
        protected override void OnPreRender(EventArgs e)
        {
            _functionality.ThrowErrorIfIDIsNull();

            if (AutoCompleteDisabled)
            {
                this.Attributes["autocomplete"] = "off";
            }

            base.OnPreRender(e);
        }
        protected override void OnLoad(EventArgs e)
        {
            if (string.IsNullOrEmpty(this.Text) && !string.IsNullOrEmpty(this._functionality.InitValue))
            {
                this.Text = _functionality.InitValue;
            }
            base.OnLoad(e);
        }
        public WebControl Control
        {
            get { return this; }
        }
        public FormFieldsValidationParams.FormFieldValidationSubGroupParams SubGroupParams { get { return WebControlFunctionality.SubGroupParams; } set { WebControlFunctionality.SubGroupParams = value; } }

        public void FocusGood()
        {

            _functionality.FocusGood();

        }

        public virtual Enums.DATA_TYPE DataType
        {
            get
            {
                return Enums.DATA_TYPE.String;
            }
        }
        public int? GetFormValueAsIntNullable()
        {
            return (int?)GetFormValueAsLongNullable();

        }
        public long? GetFormValueAsLongNullable()
        {
            string s = this.GetFormValueAsStr();
            long? result = null;
            long num;
            if (long.TryParse(s, out num))
                result = num;
            return result;

        }
        #region IBaseWebControl Members

        public General_v3.Classes.CSS.CSSManagerForControl CssManager
        {
            get { return _functionality.CssManager; }
        }

        public MyFormWebControlFunctionality WebControlFunctionality
        {
            get { return _functionality; }
        }

        #endregion

        #region IMyFormWebControl Members




        #endregion

        #region IBaseWebControl Members


        BaseWebControlFunctionality IBaseWebControl.WebControlFunctionality
        {
            get { return (BaseWebControlFunctionality)this.WebControlFunctionality; }
        }

        #endregion




        #region IBaseControl Members

        Control IBaseControl.Control { get { return this; } }

        BaseControlFunctionality IBaseControl.WebControlFunctionality { get { return _functionality; } }
        #endregion
        #region IMyFormWebControl Members


        public FormFieldsValidationParams ValidationParams
        {
            get
            {
                return this.WebControlFunctionality.ValidationParams;
            }
            set { this.WebControlFunctionality.UpdateValidationParams(value); }
        }

        public FormFieldsCSSClasses FieldCssClasses
        {
            get
            {
                return this.WebControlFunctionality.FieldCssClasses;
            }
            set
            {
                this.WebControlFunctionality.FieldCssClasses = value;
            }
        }

        #endregion


    }
}
