﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace CS.General_v3.Controls.WebControls.Common
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyDiv runat=server></{0}:MyDiv>")]
    
    public class MyFieldset : BaseWebControl
    {
        public MyLegend Legend { get; private set; }
        public string Title { get { return Legend.InnerHtml; } set { Legend.InnerHtml = value; } }
        public MyFieldset()
            : base("fieldset")
        {
            Legend = new MyLegend();
            Controls.Add(Legend);
        }
        protected override void OnLoad(EventArgs e)
        {
            if (String.IsNullOrEmpty(Legend.InnerHtml))
            {
                Legend.Parent.Controls.Remove(Legend);
            }
            base.OnLoad(e);
        }
        
        

    }
}
