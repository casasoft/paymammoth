﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace CS.General_v3.Controls.WebControls.Common
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyDiv runat=server></{0}:MyDiv>")]
    
    public abstract class MyInnerHtmlControl : BaseWebControl
    {
        private Literal _ltl = new Literal();

        public MyInnerHtmlControl()
            : base()
        {
            init();
        }
        public MyInnerHtmlControl(System.Web.UI.HtmlTextWriterTag tagName)
            : base(tagName)
        {
            init();

        }
        public MyInnerHtmlControl(string tagName)
            : base(tagName)
        {
            init();

        }
        private void init()
        {
            Controls.Add(_ltl);
        }

        public string InnerHtml { get { return _ltl.Text; } set { _ltl.Text = value; } }
        

    }
}
