﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.WebControls.Common
{
    public class MyPanel3Grid : MyPanel9Grid
    {
        public MyPanel3Grid() 
        {
            hideUnusedCells();
        }

        private void hideUnusedCells() 
        {
            this.TrBottom.Visible = this.TrTop.Visible = false;
        }

    }
}
