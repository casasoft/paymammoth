﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web.UI.WebControls;
using System.Web.UI;
namespace CS.General_v3.Controls.WebControls.Common.Dojo
{
    [ToolboxData("{0}:DojoComboBox runat='server' />")]
    public class DojoComboBox : MyTxtBoxTextSingleLine
    {
        public enum FormMethod { Post, Get }

        public string PostUrl { get; set; }
        public FormMethod PostMethod { get; set; }
        public bool HasDownArrow { get; set; }
        public bool HighlightMatch { get; set; }
        public bool AutoComplete { get; set; }
        public DojoComboBox(string ID, string Title, bool required, string helpMessage)
            : base(ID,Title,required,helpMessage)
        
        {
            this.Attributes["Name"] = ID;
            PostMethod = FormMethod.Post;
            this.HighlightMatch = false;
            this.AutoComplete = false;
        }

        private void initJS()
        {
            if (String.IsNullOrEmpty(PostUrl))
            {
                throw new Exception("Please Specify PostUrl where the ComboBox will post to get results.  AJAX page must return DataStoreResult item in JSON format.");
            }

            string js = "new js.com.BeeGuides.UI.QuickSearchKeywords('"+this.ClientID+"', '"+this.PostUrl+"');";
           /* js += "dojo.require('dijit.form.ComboBox');";
            js += "dojo.require('dojox.data.QueryReadStore');";
            js += "dojo.addOnL3oad(function() {";
            string storeJS = "new dojox.data.QueryReadStore({url:'"+PostUrl+"', requestMethod:'"+(PostMethod== FormMethod.Post ? "post" : "get")+"'})";

            CS.General_v3.JavaScript.Data.JSObject objParams = new CS.General_v3.JavaScript.Data.JSObject();
            objParams.AddProperty("hasDownArrow", HasDownArrow, false);
            objParams.AddProperty("value", this.Value, true);
            objParams.AddProperty("store", storeJS, false,false, false);
            objParams.AddProperty("autoComplete", AutoComplete, false);
            objParams.AddProperty("highlightMatch", (HighlightMatch ? "first" : "none"), true);


           

            //js += "dojo.byId('"+this.ClientID+"').style.backgroundColor = 'red';";
            js += "try { ";
            js += "new dijit.form.ComboBox("+objParams.GetJS()+", dojo.byId('" + this.ClientID + "'));";
            js += "dojo.byId('" + this.ClientID + "').name = '" + CS.General_v3.Util.Forms.GetFormVariableID(this, ClientIDSeparator) + "';";
            js += "new js.com.cs."+CS.General_v3.Settings.Others.JAVASCRIPT_VERSION+".UI.Forms.DojoComboBox('" + this.ClientID + "');";
            js += "} catch (e) { }";
            js += "}); ";*/
            js = CS.General_v3.Util.JSUtilOld.MakeJavaScript(js, true);
            
            this.Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID + "comboboxJs", js);
            /*dojo.require("dijit.form.ComboBox");
	dojo.require("dojox.data.QueryReadStore")
	
	
	var store = new dojox.data.QueryReadStore({url:"/ajax/getKeywords.aspx", requestMethod:"post"});
	this._combo = new dijit.form.ComboBox({value:'test', store:store}, dojo.byId(txt))		*/
        }

        protected override void OnLoad(EventArgs e)
        {
            
            initJS();
            //this.Attributes["name"] = "asdfasd";//this.ClientID;
            base.OnLoad(e);
        }

    }
}
