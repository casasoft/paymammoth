﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace CS.General_v3.Controls.WebControls.Common.General
{
    public interface IMyTxtBox<TValue> : IMyFormWebControl
    {
        void SetValue(object o);
        List<string> ValueIn { get; set; }
        List<string> ValueNotIn { get; set; }
        string ValueSameAsTxtBoxID { get; set; }
        IMyTxtBox<TValue> ValueSameAsTxtBoxCtrl { get; set; }
        string GetValueSameAsTxtBoxClientID();

    }
}
