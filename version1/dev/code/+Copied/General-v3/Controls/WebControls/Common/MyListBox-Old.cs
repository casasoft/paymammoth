using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.General_v3.Classes.CSS;
using CS.General_v3.Controls.WebControls.Common.General;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;

namespace CS.General_v3.Controls.WebControls.Common
{
   

    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyListBox runat=server></{0}:MyListBox>")]
    public class MyListBoxOld: ListBox, General.IMyFormWebControl
    {
        public override string ID
        {
            get
            {
                return base.ID;
            }
            set
            {
                base.ID = value;
            }
        }
        public string GetFormValueAsStr()
        {
            return this.FormValue;
        }
        public string StringID { get; set; }
        public object Tag { get { return _functionality.Tag; } set { _functionality.Tag = value; } }
        
        private General.MyFormWebControlFunctionality _functionality = null;
        public MyListBoxOld()
            : base()
        {
            _functionality = new CS.General_v3.Controls.WebControls.Common.General.MyFormWebControlFunctionality(this, ClientIDSeparator);
            this.Size = 4;

        }

        public bool AllowMultiple {get;set;}
        /// <summary>
        /// Alias for 'Rows'
        /// </summary>
        public int Size
        {
            get
            {
                return this.Rows;
            }
            set
            {
                this.Rows = value;
            }
        }

        private string _Delimeter = ",";

        public string ClientName
        {
            get
            {
                return _functionality.ClientName;
            }
        }

        [Category("Javascript")]
        [DefaultValue("")]
        [Description("The ID of the help tag")]
        public string HelpID
        {
            get
            {
                return _functionality.HelpID;
            }

            set
            {
                _functionality.HelpID = value;
            }
        }

        [Category("Validation")]
        [DefaultValue(false)]
        [Description("Whether to validate control on blur or not")]
        public bool DoNotValidateOnBlur
        {
            get
            {
                return _functionality.DoNotValidateOnBlur;
            }

            set
            {
                _functionality.DoNotValidateOnBlur = value;

            }
        }
        [Category("Validation")]
        [DefaultValue("")]
        [Description("Delimeter to use to seperate values in other textboxes specified either in 'ValueInTxtBox' or 'ValueNotInTxtBox'.  Must be left either empty or strictly 3 characters")]
        public string Delimeter
        {
            get
            {
                return _Delimeter;
            }

            set
            {
                _Delimeter = value;

            }
        }
        [Category("Javascript")]
        [DefaultValue("")]
        [Description("The message to show in the help tag")]
        public string HelpMsg
        {
            get
            {
                return _functionality.HelpMsg;
            }

            set
            {
                _functionality.HelpMsg = value;
            }
        }

       /* [Category("Validation")]
        [DefaultValue("")]
        [Description("Whether control is required")]
        public string RequiredGroup
        {
            get
            {
                return _functionality.Required;
            }

            set
            {
                _functionality.RequiredGroup = value;
            }
        }*/

        /// <summary>
        /// The value retrieved from the posted Form
        /// </summary>
        public object FormValueObject
        {
            get
            {
                return _functionality.FormValue;
            }

        }
        // IMPLEMENT THIS
        public object Value
        {
            get
            {
                return this.FormValue;
            }
            set
            {

                if (value == null)
                    this.SelectedIndex = -1;
                else
                {
                    int index;
                    if (int.TryParse(value.ToString(), out index))
                        this.SelectedIndex = index;
                    else
                        this.SelectedIndex = -1;
                }
                
            }
        }

        public string FormValue
        {
            get
            {
                return _functionality.FormValue;
            }
        }

        [Category("Required")]
        [DefaultValue(false)]
        [Description("Is Required")]
        public bool Required
        {
            get
            {
                return _functionality.Required;
            }

            set
            {
                _functionality .Required= value;
                /*
                */
            }
        }


        [Category("Title")]
        [DefaultValue("")]
        [Description("Title to show for validation")]
        public string Title
        {
            get
            {
                return _functionality.Title;
            }

            set
            {
                _functionality .Title= value;
                /*
                */
            }
        }


        [Category("Validation")]
        [DefaultValue(true)]
        [Description("Has Validation")]
        public bool HasValidation
        {
            get
            {
                return _functionality.HasValidation;
            }

            set
            {
                _functionality.HasValidation = value;
                /*
                */
            }
        }

        /// <summary>
        /// The  group.  The set of controls by which to validate.  Is controlled
        /// by the value of the textbox 'form_group'.  Only currently set
        /// group is validated.  
        /// </summary>
        /// 
        [Category("Validation")]
        [DefaultValue("main")]
        [Description("The validation group. The set of controls by which to validate. Is controlled by the value of the textbox 'form_validateGroup'.")]
        public override string ValidationGroup
        {
            get
            {
                return _functionality.ValidationGroup;
            }

            set
            {
                _functionality.ValidationGroup = value;

            }
        }
        public string OnClientClick { get { return _functionality.OnClientClick; } set { _functionality.OnClientClick = value; } }
        public string OnClientLoad { get { return _functionality.OnClientLoad; } set { _functionality.OnClientLoad = value; } }
        public string OnClientChange { get { return _functionality.OnClientChange; } set { _functionality.OnClientChange = value; } }
        public string OnClientBlur { get { return _functionality.OnClientBlur; } set { _functionality.OnClientBlur = value; } }
        public string RollOverClass { get { return _functionality.RollOverClass; } set { _functionality.RollOverClass = value; } }



        #region classes
        [Category("Javascript")]
        [DefaultValue(false)]
        [Description("Whether to use default CSS classes or not")]
        public bool UseDefaultCSSClasses
        {
            get
            {
                return _functionality.UseDefaultCSSClasses;
            }

            set
            {
                _functionality.UseDefaultCSSClasses = value;

            }
        }
        public string Class_Disabled { get { return _functionality.Class_Disabled; } set { _functionality.Class_Disabled = value; } }
        public string Class_onFocus { get { return _functionality.Class_Focus; } set { _functionality.Class_Focus = value; } }
        public string Class_onError { get { return _functionality.Class_Error; } set { _functionality.Class_Error = value; } }
        public string Class_onOver { get { return _functionality.Class_Over; } set { _functionality.Class_Over = value; } }
        public string Class_ReadOnly { get { return _functionality.Class_ReadOnly; } set { _functionality.Class_ReadOnly = value; } }
        public string Class_Required { get { return _functionality.Class_Required; } set { _functionality.Class_Required = value; } }

        #endregion



        [Category("JavaScript")]
        [DefaultValue("")]
        [Description("Button to click when an item is chosen from the list")]
        public string onClientChangeClickButton
        {
            get
            {
                return _functionality.OnClientChangeClickButton;
            }

            set
            {
                _functionality .OnClientChangeClickButton= value;
            }
        }




        
        public List<ListItem> getFormListItems()
        {
            string s = FormValue;
            int currPos = 0;
            List<ListItem> list = new List<ListItem>();
            if (s != "")
            {
                for (int i = 0; i < Items.Count; i++)
                {
                    currPos = s.IndexOf(Items[i].Value);
                    bool selected = true;
                    if (currPos > -1)
                    {
                        if (currPos > 0)
                        {
                            selected = (s[currPos - 1] == ',');
                        }
                        if (selected && ((currPos + Items[i].Value.Length) < s.Length))
                        {
                            selected = (s[currPos + Items[i].Value.Length] == ',');
                        }
                        if (selected)
                            list.Add(Items[i]);
                    }
                }
            }
            return list;
        }



        protected override void Render(HtmlTextWriter writer)
        {
            if (this.AllowMultiple)
                Attributes["multiple"] = "multiple";
            //Attributes["size"] = this.Size.ToString();
           // Attributes["size2"] = this.Size.ToString();
            this.Rows = this.Size;
            if (!string.IsNullOrEmpty(_functionality.OnClientClick ))
            {
                Attributes["onchange"] += ";" + _functionality.OnClientClick;
                //Attributes["onclick"] += ";" + onchange;
                Attributes["onkeyup"] += ";" + _functionality.OnClientClick;
            }

            double oldWidth = 0;
            if (!this.Width.IsEmpty)
            {
                oldWidth = this.Width.Value;
                this.Width = new Unit(oldWidth + 7);
            }


            base.Render(writer);
            
            if (!this.Width.IsEmpty)
                this.Width = new Unit(oldWidth);
        }
        public new WebControl Control
        {
            get { return this; }
        }
        public FormFieldsValidationParams.FormFieldValidationSubGroupParams SubGroupParams { get { return WebControlFunctionality.SubGroupParams; } set { WebControlFunctionality.SubGroupParams = value; } }

        public Enums.DATA_TYPE DataType
        {
            get
            {
                return Enums.DATA_TYPE.String;
            }
        }


        #region IMyFormWebControl Members


        public void FocusGood()
        {
            _functionality.FocusGood();
        }

        #endregion
        public int? GetFormValueAsIntNullable()
        {
            string s = this.GetFormValueAsStr();
            int? result = null;
            int num;
            if (Int32.TryParse(s, out num))
                result = num;
            return result;

        }

        #region IBaseWebControl Members

        public CSSManagerForControl CssManager
        {
            get { return WebControlFunctionality.CssManager; }
        }

        public MyFormWebControlFunctionality WebControlFunctionality
        {
            get { return _functionality; }
        }

        #endregion

        #region IBaseWebControl Members


        BaseWebControlFunctionality IBaseWebControl.WebControlFunctionality
        {
            get { return WebControlFunctionality; }
        }

        #endregion

        #region IMyFormWebControl Members


        Control IBaseControl.Control
        {
            get
            {
                return this.Control;
            }
        }

        BaseControlFunctionality IBaseControl.WebControlFunctionality
        {
            get
            {
                return this.WebControlFunctionality;
            }
        }
      
        #endregion

        #region IMyFormWebControl Members


        public FormFieldsValidationParams ValidationParams
        {
            get
            {
                return this.WebControlFunctionality.ValidationParams;
            }
            set { this.WebControlFunctionality.UpdateValidationParams(value); }
        }

        public FormFieldsCSSClasses FieldCssClasses
        {
            get
            {
                return this.WebControlFunctionality.FieldCssClasses;
            }
            set
            {
                this.WebControlFunctionality.FieldCssClasses = value;
            }
        }

        #endregion

        
    }
}
