using System;
using System.Collections;
using System.Text;
using System.ComponentModel;
using System.Web.UI.HtmlControls;

namespace CS.General_v3.Controls.WebControls.Common
{

    public enum FIELD_TYPE_VALUE
    {
        Integer = 1,
        Double = 2,
        String = 3,
        Boolean = 4,
        Date = 5,
        DateTime = 6
    }

 

    //public class ParentChildItemConverter : TypeConverter
    //{
    //    public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
    //    {
    //        if (sourceType == typeof(string))
    //        {
    //            return true;
    //        }
    //        return base.CanConvertFrom(context, sourceType);
    //    }

    //    public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
    //    {
    //        if (value is string)
    //        {
    //            try
    //            {
    //                string itemList = (string)value;
    //                string[] items = itemList.Split(',');
    //                ParentChildItem item = new ParentChildItem();
    //                item.itemID = items[0];
    //                item.parentID = items[1];
    //                item.divID = items[2];
    //                switch (items[3])
    //                {
    //                    case "onmouseclick":
    //                        item.EventType = ParentChildItem.POPUP_EVENTTYPE.MouseClick;
    //                        break;
    //                    case "onmousedown":
    //                        item.EventType = ParentChildItem.POPUP_EVENTTYPE.MouseDown;
    //                        break;
    //                    case "onmouseup":
    //                        item.EventType = ParentChildItem.POPUP_EVENTTYPE.MouseUp;
    //                        break;

    //                }
    //                return item;

    //            }
    //            catch { }
    //            throw new ArgumentException("The arguments to convert to a ParentChildItem where invalid");
    //        }
    //        return base.ConvertFrom(context, culture, value);
    //    }

    //    public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
    //    {
    //        if (value is ParentChildItem)
    //        {
    //            if (destinationType == typeof(string))
    //            {
    //                ParentChildItem item = (ParentChildItem)value;
    //                string result = "";
    //                result = item.itemID + "," + item.parentID + "," +
    //                    item.divID + ",";
    //                switch (item.EventType)
    //                {
    //                    case ParentChildItem.POPUP_EVENTTYPE.MouseClick:
    //                        result += "onmouseclick";
    //                        break;
    //                    case ParentChildItem.POPUP_EVENTTYPE.MouseDown:
    //                        result += "onmousedown";
    //                        break;
    //                    case ParentChildItem.POPUP_EVENTTYPE.MouseUp:
    //                        result += "onmouseup";
    //                        break;
    //                }
    //                return result;
                    
    //            }
    //        }
    //        return base.ConvertTo(context, culture, value, destinationType);
    //    }

    //}
    
    
    //[TypeConverter(typeof(ParentChildItemConverter))]
    public class ParentChildItem
    {
        
        

        public enum POPUP_EVENTTYPE
        {
            MouseClick = 1,
            MouseDown = 2,
            MouseUp = 3
        }

        private string _parent = null;
        private string _div = null;
        private string _item = null;
        public POPUP_EVENTTYPE _eventType = POPUP_EVENTTYPE.MouseClick;

        public ParentChildItem(string parent, string item, string div,
                               POPUP_EVENTTYPE eventType)
        {
            _parent = parent;
            _item = item;
            _div = div;
            _eventType = eventType;
        }

        public ParentChildItem()
        {
            _parent = "";
            _item = "";
            _div = "";
            _eventType = POPUP_EVENTTYPE.MouseClick;
        }

        /// <summary>
        /// The ID of the parent
        /// </summary>
        public string Parent
        {
            get
            {
                return _parent;
            }
            set
            {
                _parent = value;
            }
        }


        public string Item
        {
            get
            {
                return _item;
            }
            set
            {
                _item = value;
            }
        }

        /// <summary>
        /// The panel to show
        /// </summary>
        public string Div
        {
            get
            {
                return _div;
            }
            set
            {
                _div = value;
            }
        }

        /// <summary>
        /// The type of event that triggers the popup menu
        /// </summary>
        public POPUP_EVENTTYPE EventType
        {
            get
            {
                return _eventType;
            }
            set
            {
                _eventType = value;
            }
        }

      
    }

}
