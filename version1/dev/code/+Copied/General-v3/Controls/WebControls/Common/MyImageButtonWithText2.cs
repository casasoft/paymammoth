using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace CS.General_20090518.Controls.WebControls.Common
{
    [DefaultProperty("ImageUrl")]
    [ToolboxData("<{0}:MyImageButtonWithText runat=server></{0}:MyImageButtonWithText>")]
    [DefaultEvent("Click")]
    public class MyImageButtonWithText2 : Table, INamingContainer, General.IMyButtonWebControl
    {
        private HtmlTable tb = new HtmlTable();
        public MyImageButtonWithText2()
            : base()
        {

            imgImage = new MyImage();
            link = new MyLink();
            btnImage = new MyImageButton();
            btnLink = new MyLinkButton();
            this.CellPadding = this.CellSpacing = 0;
        }
        
        public enum TEXT_ALIGN
        {
            Left, Right
        }
        public enum BUTTON_MODE
        {
            Link, Button, None
        }

        public string HelpID
        {
            get
            {
                return btnImage.HelpID;
            }
            set
            {
                btnLink.HelpID = btnImage.HelpID = value;
            }
        }

        public string HelpMsg
        {
            get
            {
                return btnImage.HelpMsg;
            }
            set
            {
                btnLink.HelpMsg= btnImage.HelpMsg = value;
            }
        }
        private CS.General_20090518.Controls.WebControls.Common.MyImage imgImage = null;
        private CS.General_20090518.Controls.WebControls.Common.MyLink link = null;

        private CS.General_20090518.Controls.WebControls.Common.MyImageButton btnImage = null;
        private CS.General_20090518.Controls.WebControls.Common.MyLinkButton btnLink = null;


        

        private string _AlternateText = "";

        public string AlternateText
        {
            get { return imgImage.AlternateText; }
            set { imgImage.AlternateText = value; }
        }

       

        private TEXT_ALIGN _TextAlign = TEXT_ALIGN.Right;

        public TEXT_ALIGN TextAlign
        {
            get { return _TextAlign; }
            set { _TextAlign = value; }
        }

        
        private string _ImageUrlDisabled = "";

        public string ImageUrlDisabled
        {
            get { return _ImageUrlDisabled; }
            set { _ImageUrlDisabled = value; }
        }


        public string ImageUrl
        {
            get { return imgImage.ImageUrl; }
            set { imgImage.ImageUrl= value; }
        }
        private string _RolloverImageUrl = "";

        public string RolloverImageUrl
        {
            get { return imgImage.RollOverImage; }
            set { imgImage.RollOverImage= value; }
        }
        private string _Text = "";

        public string Text
        {
            get { return _Text; }
            set { _Text = value; }
        }

        public string CssClass { get; set; }

        private string _HRef = "";

        public string HRef
        {
            get { return _HRef; }
            set { _HRef = value; }
        }
        private CS.General_20090518.Controls.WebControls.Common.HREF_TARGET _HRefTarget = HREF_TARGET.Self;

        public CS.General_20090518.Controls.WebControls.Common.HREF_TARGET HRefTarget
        {
            get { return _HRefTarget; }
            set { _HRefTarget = value; }
        }

        public string ValidationGroup
        {
            get { return btnImage.ValidationGroup; }
            set { btnLink.ValidationGroup = btnImage.ValidationGroup = value; }
        }
        public bool ValidationDefault
        {
            get { return btnImage.ValidationDefault; }
            set { btnLink.ValidationDefault  =btnImage.ValidationDefault = value; }
        }


        public string OnClientLoad { get { return btnImage.OnClientLoad; } set { btnLink.OnClientLoad= btnImage.OnClientLoad = value; } }
        public string OnClientChange { get { return btnImage.OnClientChange; } set { btnLink.OnClientChange = btnImage.OnClientChange = value; } }
        public string OnClientBlur { get { return btnImage.OnClientBlur; } set { btnLink.OnClientBlur = btnImage.OnClientBlur = value; } }

        public string PostBackUrl
        {
            get { return btnImage.PostBackUrl; }
            set { btnImage.PostBackUrl = btnLink.PostBackUrl = value; }
        }
        public bool ShowProgressBar
        {
            get { return btnImage.ShowProgressBar; }
            set { btnImage.ShowProgressBar = btnLink.ShowProgressBar = value; }
        }
        public string OnClientClick
        {
            get { return btnImage.OnClientClick; }
            set { btnImage.OnClientClick = btnLink.OnClientClick = value; }
        }
        private bool _ResolveImageSrcToClient = false;

        public bool ResolveImageSrcToClient
        {
            get { return btnImage.ResolveImageURLToClient; }
            set { btnImage.ResolveImageURLToClient = value; }
        }
        public string ConfirmMessage
        {
            get { return btnImage.ConfirmMessage; }
            set { btnImage.ConfirmMessage = btnLink.ConfirmMessage = value; }
        }
        public string ConfirmMessageHighlightRowClass
        {
            get { return btnImage.ConfirmMessageHighlightRowClass; }
            set { btnImage.ConfirmMessageHighlightRowClass = value; }
        }
        
        public string ConfirmMessageHighlightRowID
        {
            get { return btnImage.ConfirmMessageHighlightRowID; }
            set { btnImage.ConfirmMessageHighlightRowID= value; }
        }
        private BUTTON_MODE _ButtonMode = BUTTON_MODE.Button;

        public BUTTON_MODE ButtonMode
        {
            get { return _ButtonMode; }
            set { _ButtonMode = value; }
        }
        private bool _NoValidation = false;

        public bool NoValidation
        {
            get { return btnImage.NoValidation; }
            set { btnImage.NoValidation = btnLink.NoValidation= value; }
        }
        public object Tag
        {
            get { return btnImage.Tag; }
            set { btnImage.Tag = btnLink.Tag = value; }
        }
        public object Tag2
        {
            get { return btnImage.Tag2; }
            set { btnImage.Tag2 = btnLink.Tag2 = value; }
        }
        public object Tag3
        {
            get { return btnImage.Tag3; }
            set { btnImage.Tag3 = btnLink.Tag3 = value; }
        }

        

        public event EventHandler Click = null;

        #region public exposed properties
        
        public bool InDesignMode
        {
            get
            {
                return System.Web.HttpContext.Current == null;
            }
        }

        public string ClientImageID
        {
            get
            {
                return base.ClientID + "_image";
            }


        }
        public string ClientLinkID
        {
            get
            {
                return base.ClientID + "_link";
            }
        }
        public string ClientTableID
        {
            get
            {
                return base.ClientID + "_table";
            }
        }
        #endregion



        protected override void OnInit(EventArgs e)
        {

            base.OnInit(e);
        }

        bool createdControls = false;
        protected override void CreateChildControls()
        {
            createControls();
            //renderControls();
            


        }
        private void createControls()
        {
            if (!String.IsNullOrEmpty(HRef))
            {
                ButtonMode = BUTTON_MODE.Link; //This is a link
            }
                  




            if (!createdControls)
            {
                tb.ID = "table";
                this.Controls.Add(tb);
            }
            tb.Rows.Clear();
            tb.Attributes["class"] = CssClass;
            HtmlTableRow row = new HtmlTableRow();
            HtmlTableCell cell = new HtmlTableCell();
            cell.Attributes["class"] = "icon";
            row.Cells.Add(cell);
            cell = new HtmlTableCell();
            row.Cells.Add(cell);
            cell.Attributes["class"] = "text";
            tb.Rows.Add(row);
            if (this.ButtonMode == BUTTON_MODE.Button)
            {
                if (btnImage != null)
                {
                    btnImage.ID = "image";
                    btnLink.ID = "link";
                    if (this.Enabled)
                    {
                        btnImage.Click += new EventHandler(btnImage_Click);
                        btnLink.Click += new EventHandler(btnLink_Click);
                    }
                
                }


                if (this.TextAlign == TEXT_ALIGN.Left)
                {
                    tb.Rows[0].Cells[0].Controls.Add(btnLink);
                    tb.Rows[0].Cells[1].Controls.Add(btnImage);
                    //tb.Rows[0].Cells[0].Attributes["class"] = CellTextClass;
                    //tb.Rows[0].Cells[1].Attributes["class"] = CellImageClass;
                }
                else
                {
                    tb.Rows[0].Cells[0].Controls.Add(btnImage);
                    tb.Rows[0].Cells[1].Controls.Add(btnLink);
                    //tb.Rows[0].Cells[0].Attributes["class"] = CellImageClass;
                    //tb.Rows[0].Cells[1].Attributes["class"] = CellTextClass;

                }
                

                //tb.Attributes["onmouseup"] = "document.getElementById('" + this.btnImage.ClientButtonID + "').click();";
            }
            else if (this.ButtonMode == BUTTON_MODE.Link)
            {
                if (imgImage == null)
                {
                    imgImage.ID = "image";
                    link.ID = "link";

                }
                if (this.TextAlign == TEXT_ALIGN.Left)
                {
                    tb.Rows[0].Cells[0].Controls.Add(link);
                    tb.Rows[0].Cells[1].Controls.Add(imgImage);
                    
                    if (TextMargin > 0)
                    {
                        tb.Rows[0].Cells[0].Style.Add("padding-right", TextMargin + "px");
                    }
                }
                else
                {

                    tb.Rows[0].Cells[0].Controls.Add(imgImage);
                    tb.Rows[0].Cells[1].Controls.Add(link);
                    if (TextMargin > 0)
                    {
                        tb.Rows[0].Cells[0].Style.Add("padding-left", TextMargin + "px");
                    }
                }
               
            }
            else if (this.ButtonMode == BUTTON_MODE.None)
            {
                if (imgImage == null)
                {
                    imgImage.ID = "image";
                    link.ID = "link";

                }
                
                if (this.TextAlign == TEXT_ALIGN.Left)
                {
                    tb.Rows[0].Cells[0].InnerHtml = _Text;
                    tb.Rows[0].Cells[1].Controls.Add(imgImage);

                }
                else
                {

                    tb.Rows[0].Cells[0].Controls.Add(imgImage);
                    tb.Rows[0].Cells[1].InnerHtml = _Text;
                }
            }
            createdControls = true;
        }
        private void renderControls()
        {
               


            //createControls();
            if (this.Enabled)
            {
                if (ButtonMode != BUTTON_MODE.None)
                    btnImage.Attributes["style"] += "cursor:pointer;";
            }
           /* if (this.Enabled)
                tb.Attributes["class"] = this.TableClass;
            else
                tb.Attributes["class"] = this.TableClassDisabled;*/

            imgImage.ImageUrl = btnImage.ImageUrl = this.ImageUrl;
           // btnLink.CssClass = this.TextClassDisabled;

            imgImage.ResolveImageURLToClient = btnImage.ResolveImageURLToClient = this.ResolveImageSrcToClient;
            btnImage.RollOverImage = imgImage.RollOverImage = RolloverImageUrl;
            //if (this.ButtonMode == BUTTON_MODE.Button)
            //{
            /*
            if (this.Enabled)
            {
                
                btnImage.RollOverImage = this.RolloverImageUrl;
                btnImage.CssClass = this.ImageClass;
                btnLink.CssClass = this.TextClass;
            }
            else
            {
                btnImage.ImageUrl = this.ImageUrlDisabled;
                btnImage.CssClass = this.ImageClassDisabled;
               

            }*/
            
            link.Text = btnLink.Text = this.Text;
            if (String.IsNullOrEmpty(this.AlternateText))
                imgImage.AlternateText = btnImage.AlternateText = this.Text;

            imgImage.HRef = link.HRef = this.HRef;
            imgImage.HRefTarget = link.Target = this.HRefTarget;

               // btnImage.Attributes["style"] = ImageStyle;
              //  btnLink.Attributes["style"] = TextStyle;
                
                //tb.Attributes["onmouseup"] = "document.getElementById('" + this.btnImage.ClientButtonID + "').click();";
            //}
            //else if (this.ButtonMode == BUTTON_MODE.Link)
            //{
                
               // imgImage.Attributes["style"] = ImageStyle;
               // link.Attributes["style"] = TextStyle;
                /*
                link.Text = this.Text;
                imgImage.AlternateText = this.Text;
                if (this.Enabled)
                {
                    imgImage.ImageUrl = this.ImageUrl;
                    imgImage.RollOverImage = this.RolloverImageUrl;
                    imgImage.CssClass = this.ImageClass;
                    link.CssClass = this.TextClass;
                    link.HRef = this.HRef;
                    link.Target = this.HRefTarget;
                }
                else
                {
                    imgImage.ImageUrl = this.ImageUrlDisabled;
                    imgImage.CssClass = this.ImageClassDisabled;
                    link.CssClass = this.TextClassDisabled;
                    

                }
                imgImage.ResolveImageURLToClient = this.ResolveImageSrcToClient;
                */
                /*if (this.HRefTarget == HREF_TARGET.Self)
                    tb.Attributes["onmouseup"] = "window.location = '" + this.HRef + "';";
                else
                    tb.Attributes["onmouseup"] = "openPopup('" + this.HRef + "');";*/
            //}
            /*if (TableClassRollover != "" && Enabled)
            {
                string imgID = "";
                if (this.ButtonMode == BUTTON_MODE.Button)
                {
                    imgID = btnImage.ClientImageID;
                }
                else
                {
                    imgID = imgImage.ClientID;
                }
                tb.Attributes["onmouseover"] = "this.className = '" + TableClassRollover + "'; dojo.byId('" + imgID + "').imageElem.imageMouseOver();";
                tb.Attributes["onmouseout"] = "this.className = '" + TableClass + "'; dojo.byId('" + imgID + "').imageElem.imageMouseOut();";
            }*/
            //TableStyle += "cursor:pointer;";
           // tb.Attributes["style"] = TableStyle;

        }

        void btnImage_Click(object sender, EventArgs e)
        {
            if (Click != null)
                Click(this, e);
        }

        void btnLink_Click(object sender, EventArgs e)
        {
            if (Click != null)
                Click(this, e);
        }
         
        protected override void Render(HtmlTextWriter writer)
        {

            renderControls();
            tb.Attributes["class"] = "icon-and-text";
            base.Render(writer);

        }
        

        void btn_Click(object sender, EventArgs e)
        {
            if (Click != null)
                Click(this, e);

        }

        public int TextMargin { get; set; }



        #region IMyWebControl Members


        public string RollOverClass
        {
            get
            {
                if (ButtonMode == BUTTON_MODE.Button)
                {
                    return btnImage.RollOverClass;
                }
                else
                {
                    return imgImage.RollOverClass;
                }
            }
            set
            {
                if (ButtonMode == BUTTON_MODE.Button)
                {
                    btnImage.RollOverClass = value;
                }
                else
                {
                    imgImage.RollOverClass = value;
                }
            }
        }

        public string StringID
        {
            get
            {
                if (ButtonMode == BUTTON_MODE.Button)
                {
                    return btnImage.StringID;
                }
                else
                {
                    return imgImage.StringID;
                }
            }
            set
            {
                if (ButtonMode == BUTTON_MODE.Button)
                {
                    btnImage.StringID = value;
                }
                else
                {
                    imgImage.StringID = value;
                }
            }
        }

        #endregion
        public new WebControl Control
        {
            get { return this; }
        }
    }
}
