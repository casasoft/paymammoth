using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.General_20090518.Controls.WebControls.Common
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyAnchor runat=server />")]
    public class MyAnchor2 : WebControl
    {
        public override string ID
        {
            get
            {
                
                if (string.IsNullOrEmpty(base.ID))
                    _myWebControl.CheckControlID();
                return base.ID;
            }
            set
            {
                base.ID = value;
            }
        }
        public string StringID { get; set; }
        public string RollOverClass { get { return _myWebControl.RollOverClass; } set { _myWebControl.RollOverClass = value; } }
        
        [Bindable(true)]
        [Category("Data Storage")]
        [DefaultValue("")]
        [Localizable(true)]
        [Description("Extra Information")]

        public object Tag
        {
            get
            {
                
                return _myWebControl.Tag;
            }

            set
            {
                _myWebControl.Tag = value;

            }
        }
        public bool InDesignMode
        {
            get
            {
                bool b = true;
                try
                {
                    HttpRequest request = System.Web.HttpContext.Current.Request;
                    if (request == null)
                        b = true;
                    else
                        b = false;
                }
                catch
                {
                    b = true;
                }
                return b;
            }
        }

        public string OnClientClick { get { return _myWebControl.OnClientClick; } set { _myWebControl.OnClientClick = value; } }
        public string OnClientLoad { get { return _myWebControl.OnClientLoad; } set { _myWebControl.OnClientLoad = value; } }
        public string OnClientChange { get { return _myWebControl.OnClientChange; } set { _myWebControl.OnClientChange = value; } }
        public string OnClientBlur { get { return _myWebControl.OnClientBlur; } set { _myWebControl.OnClientBlur = value; } }
        
        protected override void Render(HtmlTextWriter writer)
        {
            
            if (!InDesignMode)
            {
                string style = "style=\"" + "position:relative; left:-2000px" + "\"";
                //style = "";
                writer.WriteBeginTag("a");
                writer.WriteAttribute("id", ID);
                writer.Write(style);
                writer.Write(">");
                writer.Write("Anchor");
                writer.WriteEndTag("a");
            }
            else
            {
                writer.Write("{ANCHOR:" + ID + "}");
            }
            _myWebControl.Render(writer);
        }

        public MyAnchor2()
            : base()
        {
            _myWebControl = new CS.General_20090518.Controls.WebControls.Common.General.MyWebControl(this,ClientIDSeparator);
        }

        #region IMyWebControl Members
        public CS.General_20090518.Controls.WebControls.Common.General.MyWebControl _myWebControl = null;
        public string HelpID 
        {
            get
            {
                return _myWebControl.HelpID;
            }
            set
            {
                _myWebControl.HelpID = value;
            }
        }

        public string HelpMsg
        {
            get
            {
                return _myWebControl.HelpMsg;
            }
            set
            {
                _myWebControl.HelpMsg = value;
            }
        }

        #endregion

        public new WebControl Control
        {
            get { return this; }
        }
    }


}
