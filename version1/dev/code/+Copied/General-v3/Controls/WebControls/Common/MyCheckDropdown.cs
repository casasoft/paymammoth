using System;

using System.Collections.Generic;

using System.ComponentModel;

using System.Text;

using System.Web;

using System.Web.UI;

using System.Web.UI.WebControls;

using System.Drawing;

using System.Security.Permissions;

using System.Collections;





namespace CS.General_v3.Controls.WebControls.Common
{

    [DefaultProperty("Text")]

    [ToolboxData("<{0}:MyCheckDropdown runat=\"server\"> </{0}:MyCheckDropdown>")]

    public class MyCheckDropdown : CompositeControl
    {

        private TreeView m_TreeView;

        private Label m_Label;

        private ArrayList aItems = new ArrayList();

        private ArrayList aSelectedItems = new ArrayList();



        [Bindable(true)]

        [Category("Appearance")]

        [DefaultValue("Extending the DropDown control")]

        [Localizable(true)]

        public virtual String Title
        {

            get
            {

                if (ViewState["Title"] == null)

                    return ("No Title");



                return ((string)ViewState["Title"]);

            }

            set
            {

                ViewState["Title"] = value;

            }

        }



        public string Children
        {

            get
            {

                if (ViewState["Children"] == null)

                    return ("");



                return ((string)ViewState["Children"]);

            }

            set
            {

                ViewState["Children"] = value;

            }

        }



        [Bindable(true)]

        [Category("Appearance")]

        [DefaultValue("Extending the DropDown control")]

        [Localizable(true)]

        public void Items(ArrayList aList)
        {

            aItems = aList;

            for (int i = 0; i < aItems.Count; i++)
            {

                TreeNode oNode = new TreeNode(aItems[i].ToString());

                m_TreeView.Nodes.Add(oNode);

            }

        }



        public ArrayList GetSelectedItems()
        {

            return (aSelectedItems);

        }



        protected override void CreateChildControls()
        {

            m_TreeView = new TreeView();

            m_TreeView.ExpandImageUrl = "/_common/static/images/icons/v1/dropdown.gif";

            m_TreeView.CollapseImageUrl = "/_common/static/images/icons/v1/dropdown.gif";

            m_TreeView.ShowCheckBoxes = TreeNodeTypes.Leaf;

            m_TreeView.ForeColor = Color.Black;

            m_TreeView.TreeNodeCheckChanged += new TreeNodeEventHandler(m_TreeView_TreeNodeCheckChanged);

            m_TreeView.TreeNodeCollapsed += new TreeNodeEventHandler(m_TreeView_TreeNodeCollapsed);



            m_Label = new Label();



            TreeNode oNode = new TreeNode(Title);

            m_TreeView.Nodes.Add(oNode);

            if (Children.Length > 0)
            {

                ArrayList aChildren = GetChildren(Children);

                for (int t = 0; t < aChildren.Count; t++)
                {

                    TreeNode oNodeSub = new TreeNode(aChildren[t].ToString());

                    oNode.ChildNodes.Add(oNodeSub);

                }

            }



            this.Controls.Add(m_TreeView);

            this.Controls.Add(m_Label);



        }



        private ArrayList GetChildren(string sChild)
        {

            ArrayList oArray = new ArrayList();



            while (sChild.IndexOf(",") != -1)
            {

                int len = sChild.IndexOf(",");

                string sTemp = sChild.Substring(0, len);

                oArray.Add(sTemp);

                sChild = sChild.Substring(len + 1);

            }



            oArray.Add(sChild);



            return (oArray);

        }



        protected override void OnPreRender(EventArgs e)
        {

            base.OnPreRender(e);

            /*if (Page != null)

                Page.RegisterRequiresPostBack(this);

             * */

        }



        [

        Category("Action"),

        Description("Raised when the user checks.")

        ]

        protected void m_TreeView_TreeNodeCheckChanged(object sender, TreeNodeEventArgs e)
        {

            BuildLabel(m_TreeView, m_Label);

        }



        [

        Category("Action"),

        Description("Raised when the user collapses the treeview.")

        ]

        protected void m_TreeView_TreeNodeCollapsed(object sender, TreeNodeEventArgs e)
        {

            BuildLabel(m_TreeView, m_Label);

        }



        private void BuildLabel(TreeView oView, Label oLabel)
        {

            aSelectedItems.Clear();

            oLabel.Text = "";



            for (int i = 0; i < oView.Nodes.Count; i++)
            {

                for (int j = 0; j < oView.Nodes[i].ChildNodes.Count; j++)
                {

                    if (oView.Nodes[i].ChildNodes[j].Checked == true)
                    {

                        if (oLabel.Text.Length > 0)

                            oLabel.Text += ",";



                        oLabel.Text += oView.Nodes[i].ChildNodes[j].Text;

                        aSelectedItems.Add(oView.Nodes[i].ChildNodes[j].Text);

                    }

                }

            }

        }



        protected override void Render(HtmlTextWriter writer)
        {

            AddAttributesToRender(writer);



            writer.AddAttribute(

                HtmlTextWriterAttribute.Cellpadding,

                "1", false);

            writer.RenderBeginTag(HtmlTextWriterTag.Table);



            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            writer.RenderBeginTag(HtmlTextWriterTag.Td);

            m_TreeView.RenderControl(writer);

            writer.RenderEndTag();

            writer.RenderBeginTag(HtmlTextWriterTag.Td);

            m_Label.RenderControl(writer);

            writer.RenderEndTag();

            writer.RenderEndTag();



            writer.RenderEndTag();

        }



    }

}