﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace CS.General_v3.Controls.WebControls.Common.General
{
    using JavaScript.Data;
    using JavaScript.Data;

    public class MyButtonWebControlFunctionality : MyBaseFormWebControlFunctionality
    {

        public class ButtonParamsJS : JavaScriptObject
        {
            public bool validateFormOnClick = true;
            public string validationGroup;
            public bool defaultButton;
            public string confirmMessage;
            public bool isLinkButton;
            public bool buttonForAjax = false;
        }

        public event EventHandler Click;


        public bool doNotRenderElement = false;

        public ButtonParamsJS ButtonParams { get; private set; }

        public MyButtonWebControlFunctionality(WebControl control)
            : base(control) 
        {
            this.ButtonParams = new ButtonParamsJS();
            this.ButtonParams.isLinkButton = control is MyLinkButton;
            init();
            
            
        }
        private void checkID()
        {
            if (string.IsNullOrEmpty(Control.ID))
            {
                throw new InvalidOperationException("A button must always have an ID set to a value");
            }
        }
        private void initHandlers()
        {
            this.Control.PreRender += new EventHandler(Control_PreRender);
        }

        void Control_PreRender(object sender, EventArgs e)
        {
            
            checkID(); 
            checkPostBackUrl();

            attachJsClickEvent();
            addJSToPage();

        }

       

     

        public string Text { get { string s = (string)this.Control.Attributes["value"]; return s;} set { this.Control.Attributes["value"] = value; } }


        

        private void attachASPPostbackJS()
        {
            //this

            string js = Control.Page.ClientScript.GetPostBackEventReference(Control, "Click");
            this.OnClientClick += js;
        }
        private void attachValidationJS()
        {
            this.OnClientClick += "js.com.cs."+CS.General_v3.Settings.Others.JAVASCRIPT_VERSION+".UI.FormsOld.FormsCollectionOld.get_instance().validateCurrentForm();";
        }

        private void attachJsClickEvent()
        {
            if (string.IsNullOrEmpty(this.OnClickHref))
            {
                if (this.Click != null)
                {
                    attachASPPostbackJS();
                }
                else
                {
                    attachValidationJS();
                }
            }
            else
            {
                updateOnClickHrefJs();
            }

        }
       
      

        #region IPostBackEventHandler Members

        public void RaisePostBackEvent(string eventArgument)
        {
            switch (eventArgument)
            {
                case "Click":
                    if (this.Click != null)
                    {
                        this.Click(this, null);
                    }
                    break;
            }
        }

        #endregion




        private void init()
        {
          //  this.AddFieldJS = false;
            initHandlers();
            this.ValidationGroup = "main";
            Control.Attributes["type"] = "button";
        }

        public string PostBackUrl { get; set; }
        public bool UseSubmitBehavior { get; set; }
        public bool DisableButtonOnSubmitForm { get; set; }
        public string DisableButtonOnSubmitFormText { get; set; }
        public string ConfirmMessage
        {
            get { return this.ButtonParams.confirmMessage; }
            set { this.ButtonParams.confirmMessage = value; }
        }
        public string ConfirmMessageHighlightRowClass { get; set; }
        public string ConfirmMessageHighlightRowID { get; set; }
        public bool NoValidation { get; set; }
        private StringBuilder _JSAfterControl = new StringBuilder();
        public object Tag2 { get; set; }
        public object Tag3 { get; set; }
        public bool ValidationDefault
        {
            get { return this.ButtonParams.validateFormOnClick; }
            set { this.ButtonParams.validateFormOnClick = value; }
        }
        public bool ShowProgressBar { get; set; }

      
       private void addJSToPage()
        {
            if (Control.Visible)
            {
                
               /* _JSAfterControl.Append("new js.com.cs." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION + ".UI.Forms.Button('" + Control.ClientID + "',true,'" + this.ValidationGroup +
                    "'," + CS.General_v3.Util.Text.forJS(this.ValidationDefault) + "," + (String.IsNullOrEmpty(ConfirmMessage) ? "null" : "'" + ConfirmMessage + "'") + ");");
                */
                //this.ButtonParams.defaultButton = true;
                this.ButtonParams.validationGroup = this.ValidationGroup;

                string js = "new js.com.cs." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION + ".UI.FormsOld.Button('" + Control.ClientID + "'," + this.ButtonParams.GetJsObject().GetJS() + ");";


                CS.General_v3.Util.JSUtilOld.AddJSScriptToPage(js);
                //writer.Write(js);
            }
        }

        private void checkPostBackUrl()
        {
            if (!string.IsNullOrEmpty(this.PostBackUrl))
            {
                string JS = ";document.forms[0].action = \"" + this.PostBackUrl + "\";";
                JS += "document.forms[0].submit();";
                OnClientClick += JS;

            }
        }


        private void updateOnClickHrefJs()
        {
            if (!string.IsNullOrEmpty(OnClickHref))
            {
                this.OnClientClick += "window.location = '" + OnClickHref + "';";
            }
        }

        internal void TriggerClickEvent(object sender = null, EventArgs e = null)
        {
            if (sender == null) sender = Control;

            if (Click != null) {
                Click(sender, e);
            }
        }
        public bool HasClickEventBeenAttached { get { return this.Click != null; } }

        public string Value { get { return Control.Attributes["value"]; } set { Control.Attributes["value"] = value; } }

        public string OnClickHref { get; set; }

      
    }
}
