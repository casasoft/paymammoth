using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.General_v3.Controls.WebControls.Common
{

    

    public partial class TinyMCE : TextBox, General.IMyFormWebControl
    {
        public class ButtonControlReferenceList : List<BUTTON_CONTROL_REFERENCE>
        {
            public override string ToString()
            {
                StringBuilder sb = new StringBuilder();
                foreach (var item in this)
                {
                    if (sb.Length > 0)
                        sb.Append(",");
                    sb.Append(ButtonControlReferenceToCode(item));
                }
                return sb.ToString();
            }
            public void Add(params BUTTON_CONTROL_REFERENCE[] values)
            {
                foreach (var value in values)
                {
                    base.Add(value);
                }
            }
        }
        public void FocusGood()
        {
            base.Focus();
        }
        public int? GetFormValueAsIntNullable()
        {
            throw new InvalidOperationException();
        }
        
    }
}
