using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common.General;
namespace CS.General_v3.Controls.WebControls.Common
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyButton runat=server></{0}:MyButton>")]
    public class MyButton : BaseButtonWebControl, General.IMyButtonWebControl
    {
        public MyButton()
            : base(HtmlTextWriterTag.Input, "")
        {
            
        }
        public MyButton(string id)
            : base(HtmlTextWriterTag.Input, id)
        {
            
        }
        /// <summary>
        /// This is used to show the hover text
        /// </summary>
        public string Title { get; set; }
        private void checkHoverText()
        {
            string s = this.Title;
            if (string.IsNullOrWhiteSpace(s))
                s = this.Text;
            this.Attributes["alt"] = s;
            this.Attributes["title"] = s;
            
        }

        protected override void OnPreRender(EventArgs e)
        {
            checkHoverText();
            if (string.IsNullOrEmpty(this.ID))
            {
                throw new Exception("ID cannot be null for form elements");
            }
            base.OnPreRender(e);
        }

       // public new string Value { get; set; }
    }
}
