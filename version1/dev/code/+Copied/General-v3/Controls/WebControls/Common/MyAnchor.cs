using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

using CS.General_v3.Classes.CSS;
using Iesi.Collections.Generic;

namespace CS.General_v3.Controls.WebControls.Common
{
    
    [ToolboxData("<{0}:MyAnchor runat=server></{0}:MyAnchor>")]
    
    public class MyAnchor : BaseWebControl
    {
        private Enums.HREF_TARGET _aHrefTarget;
        private Literal _text = new Literal();
        public MyAnchor()
            : base(HtmlTextWriterTag.A)
        {
            _aHrefTarget = Enums.HREF_TARGET.Self;
            Controls.Add(_text);
            this.RelTags = new HashedSet<Enums.ANCHOR_REL_VALUE>();
            
        }
        private void checkRelTags()
        {
            StringBuilder sb = new StringBuilder();
            if (RelTags != null && RelTags.Count > 0)
            {
                foreach (var tag in RelTags)
                {
                    if (sb.Length > 0) sb.Append(", ");
                    sb.Append(CS.General_v3.Util.EnumUtils.StringValueOf(tag));
                }
                this.Rel = sb.ToString();
            }
        }

        public HashedSet<CS.General_v3.Enums.ANCHOR_REL_VALUE> RelTags { get; set; }
        public string Rel
        {
            get { return this.Attributes["rel"]; }
            set { this.Attributes["rel"] = value; }
        }
        public string Text
        {
            get
            {
                return this.InnerText;
            }
            set
            {
                this.InnerText = value;
            }
        }
        
        public string InnerText
        {
            get
            {
                return CS.General_v3.Util.Text.HtmlDecode(this.InnerHtml);
            }
            set
            {
                this.InnerHtml = CS.General_v3.Util.Text.HtmlEncode(value);
            }
        }
        public string InnerHtml
        {
            get
            {
                return _text.Text;
            }
            set
            {
                this.Controls.Clear();
                _text.Text = value;
                Controls.Add(_text);
            }
        }
        public string Href
        {
            get
            {
                return this.Attributes["href"] as string;
            }
            set
            {
                this.Attributes["href"] = value;
            }
        }
        public string Title
        {
            get
            {
                return this.Attributes["title"] as string;
            }
            set
            {
                this.Attributes["title"] = value;
            }
        }
        public Enums.HREF_TARGET HRefTarget
        {
            get
            {
                return _aHrefTarget;
            }

            set
            {
                _aHrefTarget = value;
            }
        }
        private void initHrefTarget()
        {
            string target = null;
            switch (_aHrefTarget)
            {
                case Enums.HREF_TARGET.Self:
                    target = "_self";
                    break;
                case Enums.HREF_TARGET.Blank:
                    target = "_blank";
                    break;
                case Enums.HREF_TARGET.Parent:
                    target = "_parent";
                    break;
            }
            if (!string.IsNullOrEmpty(target))
            {
                this.Attributes["target"] = target;
            }
        }
        protected override void OnLoad(EventArgs e)
        {
            initHrefTarget();
            base.OnLoad(e);
        }
        protected override void OnPreRender(EventArgs e)
        {
            checkRelTags();
            base.OnPreRender(e);
        }

    }
}
