﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;


namespace CS.General_v3.Controls.WebControls.Common.General
{
    public interface IMyFormWebControl : IMyBaseFormWebControl
    {



        object Value { get; set; }

        bool DoNotValidateOnBlur { get; set; }
        string ClientName { get; }
        bool HasValidation { get; set; }

        FormFieldsValidationParams.FormFieldValidationSubGroupParams SubGroupParams { get; set; }





        FormFieldsValidationParams ValidationParams { get; set; }
        FormFieldsCSSClasses FieldCssClasses { get; set; }

        object FormValueObject { get; }

        int? GetFormValueAsIntNullable();

        string GetFormValueAsStr();
        bool Required { get; set; }
        bool Enabled { get; set; }
        //object OriginalValueObject { get; }
        
        string ID { get; set; }
        
        Enums.DATA_TYPE DataType { get;  }
        void FocusGood();

        new MyFormWebControlFunctionality WebControlFunctionality { get;  }

        //void Render(HtmlTextWriter writer);
        //////////


    }
}
