﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_20101215.Controls.Classes.MediaGallery;
using CS.General_20101215.Classes.MediaItems;
namespace CS.General_20101215.Controls.WebControls.Common
{
    public class MyFileUploadWithMediaItem : MyFileUpload
    {
        public CS.General_20101215.Classes.MediaItems.IMediaItem MediaItem { get; set; }
        public MyFileUploadWithMediaItem(IMediaItem mediaItem)
            : base()
        {
            this.MediaItem = mediaItem;
        }
        public MyFileUploadWithMediaItem(string ID, string Title, bool Required, IMediaItem mediaItem)
            : this(ID,Title,Required,null,mediaItem)
        {

        }
        public MyFileUploadWithMediaItem(string ID, string Title, bool Required, int? width, IMediaItem mediaItem)
            : base(ID, Title, Required,width)
        {
            this.MediaItem = mediaItem;
        }
    }
}
