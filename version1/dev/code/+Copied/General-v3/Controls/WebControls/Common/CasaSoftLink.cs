﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.WebControls.Common
{
    public class CasaSoftLink : MyDiv
    {


        private void initText()
        {
            this.CssManager.AddClass("casasoft-link");
            this.InnerHtml = @"<a href='http://www.casasoft.com.mt' target='_blank' title='web design malta'>
web design</a> & <a href='http://www.casasoft.com.mt' target='_blank' title='web design malta'>website development</a>
by <a class='casasoft-text' title=""Web design and development from Malta and Europe - Web Applications, Rich Interface Applications (RIA), Flash Development, Content Management Systems (CMS)"" target=""_blank"" href=""http://www.casasoft.com.mt"">
                <span class='casasoft-text-casa'>Casa</span><span class='casasoft-text-soft'>Soft</span> (Malta)</a>";
        }

        protected override void OnLoad(EventArgs e)
        {
            initText();
            base.OnLoad(e);
        }

    }
}
