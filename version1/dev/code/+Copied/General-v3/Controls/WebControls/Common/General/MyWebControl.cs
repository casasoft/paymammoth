﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace CS.General_20101215.Controls.WebControls.Common.General
{
    public class MyWebControl : BaseWebControl, IMyWebControl
    {
        public virtual new void Focus()
        {
            this.FocusGood();
        }

        public virtual void FocusGood()
        {
            string jsFunction = "";
            if (CS.General_20101215.Settings.Others.JScriptFramework == Enums.JSCRIPT_FRAMEWORK.JQuery)
            {
                jsFunction = "jQuery";
            }
            else
            {
                throw new Exception("Please specify a valid JScriptFramework in settings.config");
            }

            string js = jsFunction + "(function() { try { document.getElementById('" + this.Control.ClientID + "').focus(); } catch(e) {} });";
            js = CS.General_20101215.Util.JSUtil.MakeJavaScript(js, true);
            Page pg = this.Control.Page;
            if (pg == null)
                pg = CS.General_20101215.Util.PageUtil.GetCurrentPage();
            if (pg != null)
            {
                pg.ClientScript.RegisterStartupScript(this.GetType(), this.Control.ClientID + "Focus", js);
            }

        }
        public Object Tag { get; set; }
        public string StringID { get; set; }
        private WebControl _Control = null;
        public WebControl Control
        {
            get
            {
                
                return _Control;
            }
            set
            {
                _Control = value;
                
            }
        }
        public WebControl WebControl
        {
            get
            {
                if (Control is WebControl)
                    return (WebControl)Control;
                else
                    return new WebControl(HtmlTextWriterTag.Div);

            }
            
        }
        
        public char ClientIDSeparator { get; set; }
        public string HelpID { get; set; }
        public string HelpMsg { get; set; }
        public string OnClientBlur { get; set; }
        public string OnClientLoad { get; set; }
        public virtual string OnClientClick { get; set; }
        public string CssStyle { get; set; }
        public string RollOverClass { get; set; }
        public string OnClientChange { get; set; }
        public string ClientID 
        {
            get
            {
                return Control.ClientID;
            }
        }
        public void CheckControlID()
        {
            return;


                string id = "ctrl";
                int index = 0;
                if (this.Control.Parent != null)
                {
                    for (int i = 0; i < this.Control.Parent.Controls.Count; i++)
                    {
                        if (this.Control.Parent.Controls[i] == this.Control)
                        {
                            index = i;
                            break;
                        }
                    }
                }
                id+="_"+index;
                this.Control.ID = id;
            
        }
        public MyWebControl(HtmlTextWriterTag tag)
            : base(tag)
        {
            init(this, base.ClientIDSeparator);
        }

        public MyWebControl(WebControl control, char clientIDSeparator)
            : base()
        {
            
            init(control, clientIDSeparator);
        }
        private void init(WebControl control, char clientIDSeparator)
        {
            if (control == null) control = this;
            if (clientIDSeparator == 0) clientIDSeparator = this.ClientIDSeparator;
                
            this.Control = control;
            this.ClientIDSeparator = clientIDSeparator;
            this.Control.PreRender += new EventHandler(control_PreRender);
        }
        void control_PreRender(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Control.ID))
            {
                //Control.ID = CS.General_20101215.Util.Random.getString(25, 40);
            }

        }
        public string getElementFields(string varName)
        {
            StringBuilder js = new StringBuilder();
            /*
            if (!string.IsNullOrEmpty(this.RollOverClass))
            {
                js.Append(varName + ".classRollover = '" + CS.General_20101215.Util.Text.forJS(this.RollOverClass) + "';");

            }
            if (!string.IsNullOrEmpty(this.HelpMsg))
            {
                js.Append(varName + ".HelpMessage.message = '" + CS.General_20101215.Util.Text.forJS(this.HelpMsg) + "';");
                js.Append(varName + ".HelpMessage.helpElementID = '" + CS.General_20101215.Util.Text.forJS(this.HelpID) + "';");
            }*/
            return js.ToString();
        }
        
        protected void renderElement(HtmlTextWriter writer)
        {
            StringBuilder js = new StringBuilder();
            string tmp = "";
            if (Control.Visible && !(this is MyFormWebControl) && !(this is MyButtonWebControl) && !(Control is MyImage))
            {
                //js.Append("_tmpElement = new com.cs.ui.Element('" + CS.General_20101215.Util.Text.forJS(Control.ClientID) + "');");
                //js.Append(getElementFields("_tmpElement"));
                
            
            
                writer.Write(CS.General_20101215.Util.Text.MakeJavaScript(js.ToString(), true));
            }
        }
        private bool _attachedAttribs = false;
        private void attachAttributes()
        {
            if (!_attachedAttribs)
            {
                _attachedAttribs = true;
                if (WebControl != null)
                {
                    if (WebControl is MyLink)
                    {

                        MyLink tmp = (MyLink)WebControl; 
                        int k = 5;
                        
                    }

                    WebControl.Attributes["name"] = Control.ClientID;
                    if (WebControl.Attributes["onmouseover"] == null)
                        WebControl.Attributes["onmouseover"] = "";
                    if (WebControl.Attributes["onmouseout"] == null)
                        WebControl.Attributes["onmouseout"] = "";
                    if (!string.IsNullOrEmpty(CssStyle))
                    {
                        WebControl.Attributes["style"] += ";" + CssStyle;
                    }
                    if (!string.IsNullOrEmpty(OnClientClick))
                    {
                        WebControl.Attributes["onclick"] += OnClientClick;
                    }

                    if (!string.IsNullOrEmpty(OnClientChange))
                    {
                        WebControl.Attributes["onchange"] += OnClientChange;
                    }
                    if (!string.IsNullOrEmpty(OnClientBlur))
                    {
                        WebControl.Attributes["onblur"] += OnClientBlur;
                    }
                }
            }
        }
        public override void RenderControl(HtmlTextWriter writer)
        {
            attachAttributes();
            
            base.RenderControl(writer);
            Render(writer);
            
        }
        public virtual void Render(HtmlTextWriter writer)
        {
            renderElement(writer);
            attachAttributes();
        }


        #region IMyWebControl Members



        #endregion
    }
}
