using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.General_v3.Controls.WebControls.Common
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyLink runat=server></{0}:MyLink>")]
    public class MyLink : BaseWebControl, IBaseWebControl
    {
        public override string ID
        {
            get
            {
                return base.ID;
            }
            set
            {
                base.ID = value;
            }
        }
        private BaseWebControlFunctionality _functionality = null;
        public MyLink()
            : this("")
        {
            
        }
        public MyLink(string id)
            : base()
        {
            this.ID = id;
            _functionality = new BaseWebControlFunctionality(this);
        }
        
        private bool _ResolveClientHref = true;
        private string _href = "";
        private Enums.HREF_TARGET _target = Enums.HREF_TARGET.Self;
        private string _txt = "";
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        [Description("Renders the anchor even if it is empty, for bookmark purposes")]
        public bool IsAnchorBookmark { get; set; }
        private string _anchorClass = "";
        // string _class = "";
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        [Description("Class to attach to anchor element.  If empty, same as CssClass.  ")]
        public string AnchorClass
        {
            get
            {
                return _anchorClass;
            }

            set
            {
                _anchorClass = value;

            }
        }
        /*public string OnClientClick { get { return _functionality.OnClientClick; } set { _functionality.OnClientClick = value; } }
        public string OnClientLoad { get { return _functionality.OnClientLoad; } set { _functionality.OnClientLoad = value; } }
        public string OnClientChange { get { return _functionality.OnClientChange; } set { _functionality.OnClientChange = value; } }
        public string OnClientBlur { get { return _functionality.OnClientBlur; } set { _functionality.OnClientBlur = value; } }*/
       /* [Bindable(true)]
        [Category("Data Storage")]
        [DefaultValue("")]
        [Localizable(true)]
        [Description("Extra Information")]
        public object Tag
        {
            get
            {
                return _functionality.Tag;
            }

            set
            {
                _functionality.Tag = value;

            }
        }
        public string HelpID
        {
            get
            {
                return _functionality.HelpID;
            }
            set
            {
                _functionality.HelpID = value;
            }
        }

        public string HelpMsg
        {
            get
            {
                return _functionality.HelpMsg;
            }
            set
            {
                _functionality.HelpMsg = value;
            }
        }*/
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        [Description("Text to display.  If empty is same as HRef")]
        public string Text
        {
            get
            {
                return _txt;
            }

            set
            {
                _txt = value;

            }
        }

        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        [Description("Text to display.  If empty is same as HRef")]
        public string Title {get;set;}

        [Bindable(true)]
        [Category("Functionality")]
        [DefaultValue(true)]
        [Localizable(true)]
        [Description("Whether to resolve the URL, according to the current locaiton")]
        public bool ResolveClientHRef
        {
            get
            {
                return _ResolveClientHref;
            }

            set
            {
                _ResolveClientHref = value;

            }
        }
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        [Description("The URL to link to. Can be left empty")]
        public string HRef
        {
            get
            {
                return _href;
            }

            set
            {
                _href = value;

            }
        }

        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        [Description("The target for the HRef attribute")]
        public Enums.HREF_TARGET Target
        {
            get
            {
                return _target;
            }

            set
            {
                _target = value;
            }
        }



        protected override void Render(HtmlTextWriter writer)
        {
            string target = "";
            string txt = Text;
            string tmp;
            if (Text == "")
                Text = _href;
            tmp = Text;
            if (CssClass == "") CssClass = AnchorClass;
            

            if (_href != "" || IsAnchorBookmark)
            {
                HyperLink link = new HyperLink();
                switch (_target)
                {
                    case Enums.HREF_TARGET.Self:
                        //target = "_self";
                        target = "";
                        break;
                    case Enums.HREF_TARGET.Blank:
                        target = "_blank";
                        break;
                    case Enums.HREF_TARGET.Parent:
                        target = "_parent";
                        break;
                }
                link.Target = target;
                if (AnchorClass != "")
                    link.CssClass = AnchorClass;
                else
                    link.CssClass = CssClass;
                link.Attributes["title"] = Title;
                
                if (ResolveClientHRef && !String.IsNullOrEmpty(_href))
                    link.NavigateUrl = ResolveClientUrl(_href);
                else
                    link.NavigateUrl = (_href);
                link.Text = this.Text;
                link.Style.Value = this.Style.Value;
                foreach (string key in this.Attributes.Keys)
                {
                    link.Attributes[key] = this.Attributes[key];
                }
                link.ID = this.ClientID;
                link.Attributes["onclick"] = this.OnClientClick;
                link.Attributes["onchange"] = this.OnClientChange;

                link.RenderControl(writer);
            }
            else
            {
                Label lbl = new Label();
                lbl.Style.Value = this.Style.Value;
                foreach (string key in this.Attributes.Keys)
                {
                    lbl.Attributes[key] = this.Attributes[key];
                }
                
                tmp = "";
                if (CssClass != "")
                {
                    lbl.CssClass = CssClass;
                }
                lbl.Text = this.Text;
                lbl.RenderControl(writer);
            }
            base.Render(writer);
            
        }

        public new WebControl Control
        {
            get { return this; }
        }

    }
}
