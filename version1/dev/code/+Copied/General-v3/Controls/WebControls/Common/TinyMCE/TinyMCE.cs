using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;

namespace CS.General_v3.Controls.WebControls.Common
{

    [DefaultProperty("Text")]
    [ToolboxData("<{0}:TinyMCE runat=server></{0}:TinyMCE>")]

    public partial class TinyMCE : TextBox, General.IMyFormWebControl
    {
        public enum BUTTON_CONTROL_REFERENCE
        {
            Bold,
            Italic,
            Underline,
            StrikeThrough,
            JustifyLeft,
            JustifyCenter,
            JustifyRight,
            JustifyFull,
            BullList,
            NumList,
            OutDent,
            InDent,
            Cut,
            Copy,
            Paste,
            Undo,
            Redo,
            Link,
            UnLink,
            Image,
            CleanUp,
            Help,
            Code,
            hr,RemoveFormat,
            FormatSelect,
            FontSelect,
            FontSizeSelect,
            StyleSelect,
            Sub,
            Sup,
            ForeColor,
            BackColor,
            ForeColorPicker,
            BackColorPicker,
            CharMap,
            VisualAid,
            Anchor,
            NewDocument,
            BlockQuote,
            Separator,
            /// <summary>
            /// Only used for 'paste' plugin
            /// </summary>
            PasteText,
            /// <summary>
            /// Only used for 'paste' plugin
            /// </summary>
            PasteWord,
            /// <summary>
            /// Only used for 'paste' plugin
            /// </summary>
            SelectAll,
            /// <summary>
            /// Only used if 'searchreplace' plugin is enabled
            /// </summary>
            Search,
            /// <summary>
            /// Only used if 'searchreplace' plugin is enabled
            /// </summary>
            Replace,
            /// <summary>
            /// Only used if 'insertdatetime' plugin is enabled
            /// </summary>
            InsertDate,
            /// <summary>
            /// Only used if 'insertdatetime' plugin is enabled
            /// </summary>
            InsertTime,
            /// <summary>
            /// Only used if 'table' plugin is enabled
            /// </summary>
            TableControls,
            /// <summary>
            /// Only used if 'table' plugin is enabled
            /// </summary>
            Table,
            /// <summary>
            /// Only used if 'table' plugin is enabled
            /// </summary>
            Row_Props,
            /// <summary>
            /// Only used if 'table' plugin is enabled
            /// </summary>
            Cell_Props,
            /// <summary>
            /// Only used if 'table' plugin is enabled
            /// </summary>
            Delete_Col,
            /// <summary>
            /// Only used if 'table' plugin is enabled
            /// </summary>
            Delete_Row,
            /// <summary>
            /// Only used if 'table' plugin is enabled
            /// </summary>
            Col_After,
            /// <summary>
            /// Only used if 'table' plugin is enabled
            /// </summary>
            Col_Before,
            /// <summary>
            /// Only used if 'table' plugin is enabled
            /// </summary>
            Row_After,
            /// <summary>
            /// Only used if 'table' plugin is enabled
            /// </summary>
            Row_Before,
            /// <summary>
            /// Only used if 'table' plugin is enabled
            /// </summary>
            Split_Cells,
            /// <summary>
            /// Only used if 'table' plugin is enabled
            /// </summary>
            Merge_Cells,
            /// <summary>
            /// Only used if 'directionality' plugin is enabled
            /// </summary>
            LeftToRight,
            /// <summary>
            /// Only used if 'directionality' plugin is enabled
            /// </summary>
            RightToLeft,
            /// <summary>
            /// Only used if 'layer' plugin is enabled
            /// </summary>
            MoveForward,

            /// <summary>
            /// Only used if 'layer' plugin is enabled
            /// </summary>
            MoveBackward,
            /// <summary>
            /// Only used if 'layer' plugin is enabled
            /// </summary>
            Absolute,
            /// <summary>
            /// Only used if 'layer' plugin is enabled
            /// </summary>
            InsertLayer,
            /// <summary>
            /// Only used if 'save' plugin is enabled
            /// </summary>
            Save,
            /// <summary>
            /// Only used if 'save' plugin is enabled
            /// </summary>
            Cancel,
            /// <summary>
            /// Only used if 'xhtmlxtras' plugin is enabled
            /// </summary>
            Cite,
            /// <summary>
            /// Only used if 'xhtmlxtras' plugin is enabled
            /// </summary>
            Abbr,
            /// <summary>
            /// Only used if 'xhtmlxtras' plugin is enabled
            /// </summary>
            Acronym,
            /// <summary>
            /// Only used if 'xhtmlxtras' plugin is enabled
            /// </summary>
            Ins,
            /// <summary>
            /// Only used if 'xhtmlxtras' plugin is enabled
            /// </summary>
            Del,
            /// <summary>
            /// Only used if 'xhtmlxtras' plugin is enabled
            /// </summary>
            Attribs,
            /// <summary>
            /// Only used if 'template' plugin is enabled
            /// </summary>
            Template,
            /// <summary>
            /// Only used if 'emotions' plugin is enabled
            /// </summary>
            Emotions,
            /// <summary>
            /// Only used if 'fullscreen' plugin is enabled
            /// </summary>
            FullScreen,
             /// <summary>
            /// Only used if 'media' plugin is enabled
            /// </summary>
            Media,
            /// <summary>
            /// Only used if 'preview' plugin is enabled
            /// </summary>
            Preview,
            /// <summary>
            /// Only used if 'print' plugin is enabled
            /// </summary>
            Print,


        }
        public static string ButtonControlReferenceToCode(BUTTON_CONTROL_REFERENCE value)
        {
            string s = "";
            switch (value)
            {
                case BUTTON_CONTROL_REFERENCE.LeftToRight: s = "ltr"; break;
                case BUTTON_CONTROL_REFERENCE.RightToLeft: s = "rtl"; break;

            }
            if (string.IsNullOrEmpty(s))
            {
                s = (Enum.GetName(typeof(BUTTON_CONTROL_REFERENCE),value));
            }
            s = s.ToLower();
            return s;
        }
        public string GetFormValueAsStr()
        {
            
            return this.FormValue;
        }
        protected General.MyFormWebControlFunctionality _functionality = null;
        public TinyMCE()
        {
            _functionality = new CS.General_v3.Controls.WebControls.Common.General.MyFormWebControlFunctionality(this, ClientIDSeparator);
            AddAllowedElement("iframe");
            AddAllowedElement("big");
            AddAllowedElement("small");

            this.ConvertFontsToSpans = false;
            this.SourceEditorHeight = 670;
            this.SourceEditorWidth = 900;
            this.ForceHexStyleColors = true;
            this.ForceBROnNewLines = false;
            this.ForcedRootBlock = false;
            this.KeepStyles = true;
            this.SourceEditorWrap = true;
            this.ApplySourceFormatting = true;
            this.ConvertNewlinesToBRs = false;
            this.FixContentDupliaction = true;
            this.FixListElements = true;
            this.FixTableElements = true;
            this.FixNesting = true;
            this.ForceParagraphsOnNewLines = true;
            //this.Preformatted = true;
            this.RemoveTrailingNonBreakingSpaces = true;
        }
        public TinyMCE(string id, string title, bool required, string helpMessage,
            int? width, int? rows, string text)
            :this()
        {
            this.ID = id;
            StringID = id;
            this.Title = title;
            this.Required = required;
            this.HelpMsg = helpMessage;
            if (width != null)
                this.Width = width.Value;
            if (rows != null)
                this.Rows = rows.Value;
            this.Value = text;
        }
        
        #region Publics

        public enum THEME_TYPE
        {
            Simple,Advanced
        }
        public enum TOOLBAR_LOCATION
        {
            Top, Bottom
        }
        
        //Standard buttons:
        //----------------
        //bold, italic, underline, strikethrough, justifyleft, justifycenter, justifyright,justifyfull, bullist, numlist, outdent
        //indent, cut,copy,paste,undo,redo, link,unlink,image,cleanup,help,code,hr,removeformat,formatselect,fontselect,fontsizeselect
        //styleselect,sub,sup,forecolor,backcolor,forecolorpicker,backcolorpicker,charmap,visualaid,anchor,newdocument,seperator,|
        //##############################################################################################################################

        //MouseOverImage:: advimage
        //ContextMenu :: contextmenu
        //Emoticons: emotions [buttons:emotions]
        //FullPage: fullpage [buttons:fullscreen]
        //Media:  media - Allows embedding of media items, like flash, quicktime,shockwave, etc
        //InsertDateTime: insertdatetime - allows inserting of date/time [buttons: insertdate,inserttime]
        //AdvancedLink: advlink - allows advanced linking
        //PasteFromWord: paste - allows pasting plain text, and directly from word [buttons: pastetext,pasteword,selectall]
        //Preview:  preview - allows previewing the thing: [buttons: preview]
        //Print: print - allow printing [ buttons:print]
        //SaveButton: save button, to submit form  [ buttons: save,cancel]
        //SearchReplace: allow searching & replacing of text [butotns: search,replace]
        //Table: allow table managements [buttons: tablecontrols]

        //theme_advanced_styles: [Title]=[css title]


        #endregion
        #region Privates



        private object _tag = null;

        private THEME_TYPE _ThemeType = THEME_TYPE.Advanced;
        private TOOLBAR_LOCATION _ToolbarLocation = TOOLBAR_LOCATION.Top;
        private List<string> _ExtraAllowedElements = null;
        private bool _PluginMouseOverImage = false;
        private bool _PluginContextMenu = true;
        private bool _PluginAdvancedLink = false;
        private bool _PluginEmoticons = false;
        private bool _PluginFullPage = true;
        private bool _PluginMedia = false;
        private bool _PluginInsertDateTime = false;
        private bool _PluginPasteFromWord = true;
        private bool _PluginPreview = true;
        private bool _PluginPrint = false;
        private bool _PluginSaveButton = false;
        private bool _PluginSearchReplace = true;
        private bool _PluginTable = true;

        private string _FormValue = "";
        private string _title = "";
        private string _group = "main";
        private string _TinyMCEJSFile = "/includes/tiny_mce/tiny_mce.js";
        private string _AllowedStyles = "";
        private string _ContentCSSFile = "";
        private string _AllowedFonts = "";
        private string _AllowedFontSizes = "";
        private string _AllowedTextColors = "";
        private string _AllowedBackgroundColors = "";
        private string _AllowedBlockFormats = "";
        private int _PluginPreviewWidth = 550;
        private int _PluginPreviewHeight = 600;
        private int _SourceEditorWidth = 500;
        private int _SourceEditorHeight = 400;

        private bool _SourceEditorWrap = true;
        private bool _hasValidation = true;
        
        #endregion
        #region Properties

        [Category("TinyMCE Properties")]
        [Description("Whether to show accessibility warnings or not")]
        public bool AccessibilityWarnings { get; set; }
        [Category("TinyMCE Properties")]
        [Description("Whether to auto resize to the boundaries of the control")]
        public bool AutoResize { get; set; }
        
        [Category("TinyMCE Properties")]
        [Description("A string of classnames that are to be converted into TinyMCE editors.  Must be used in conjunction with Mode=SpecificTextAreas")]
        public string EditorSelector { get; set; }
        

        [Category("TinyMCE Properties")]
        [Description("This option enables keeping the current text style when you press enter/return on non-IE browsers. This feature is enabled by default but can be disabled by setting the value to false. ")]
        public bool KeepStyles { get; set; }

        [Category("TinyMCE Content Output")]
        [Description("This option enables you to tell TinyMCE to apply some source formatting to the output HTML code. With source formatting, the output HTML code is indented and formatted. Without source formatting, the output HTML is more compact. ")]
        public bool ApplySourceFormatting { get; set; }
        [Category("TinyMCE Content Output")]
        [Description("This option enables you to specify that TinyMCE should remove any traling &nbsp; characters in block elements if you start to write inside them. Paragraphs are default padded with a &nbsp; and if you write text into such paragraphs the space will remain. Setting this option to true will remove the space. This option is set to false by default since the cursor jumps a bit in Gecko browsers. ")]
        public bool RemoveTrailingNonBreakingSpaces { get; set; }

        private bool _ConvertFontsToSpans = false;
        [Category("TinyMCE Content Output")]
        [Description("If you set this option to true, TinyMCE will convert all font elements to span elements and generate span elements instead of font elements. This option should be used in order to get more W3C compatible code, since font elements are deprecated. How sizes get converted can be controlled by the font_size_classes and font_size_style_values options.")]
        public bool ConvertFontsToSpans
        {
            get { return _ConvertFontsToSpans; }
            set { _ConvertFontsToSpans = value; }
        }

        [Category("TinyMCE Content Output")]
        [Description("If you set this option to true, newline characters codes get converted into br elements.")]
        public bool ConvertNewlinesToBRs { get; set; }

        [Category("TinyMCE Content Output")]
        [Description("This option is available since MSIE has a serious bug where the DOM tree sometimes can include the same node twice. This produces duplication of content when the cleanup process runs. This option is enabled by default and removes any duplicated nodes. ")]
        public bool FixContentDupliaction { get; set; }

        [Category("TinyMCE Content Output")]
        [Description("This option enables you to specify that list elements UL/OL is to be converted to valid XHTML. This option is disabled by default.  ")]
        public bool FixListElements { get; set; }
        [Category("TinyMCE Content Output")]
        [Description("This option enables you to specify that table elements should be moved outside paragraphs or other block elements. If you enable this option block elements will be split into two chunks when a table is found within it, see the example below for details. This option is disabled by default. ")]
        public bool FixTableElements { get; set; }
        [Category("TinyMCE Content Output")]
        [Description("This option controls if invalid contents should be corrected before insertion in IE. IE has a bug that produced an invalid DOM tree if the input contents aren't correct so this option tries to fix this using preprocessing of the HTML string. This option is disabled by default since it might be a bit slow. ")]
        public bool FixNesting { get; set; }

        [Category("TinyMCE Content Output")]
        [Description("This option enables you to disable/enable the creation of paragraphs on return/enter in Mozilla/Firefox. The default value of this option is true.")]
        public bool ForceParagraphsOnNewLines { get; set; }

        [Category("TinyMCE Content Output")]
        [Description("If you enable this feature, whitespace such as tabs and spaces will be preserved. Much like the behavior of a <pre> element. This can be handy when integrating TinyMCE with webmail clients. This option is disabled by default.")]
        public bool Preformatted { get; set; }
        
        
        [Category("TinyMCE Content Output")]
        [Description("If you set this option to true, TinyMCE will force BR elements on newlines instead of inserting paragraphs. This option is set to false by default since paragraphs is a much better concept. BR elements should only be used when you really have to (mostly never). Also as of 3.x the forced_root_block option is enabled by default so if you really want to disable paragraphs disable that one as well.")]
        public bool ForceBROnNewLines { get; set; }

        [Category("TinyMCE Content Output")]
        [Description("This option enables you to make sure that any non block elements or text nodes are wrapped in block elements. For example <strong>something</strong> will result in output like: <p><strong>something</strong></p>. This option is enabled by default as of 3.0a1. ")]
        public bool ForcedRootBlock { get; set; }


        [Category("TinyMCE Content Output")]
        [Description("This option enables you to control TinyMCE to force the color format to use hexadecimal instead of rgb strings. It converts for example color: rgb(255, 255, 0) to #FFFF00. This option is set to true by default since otherwice MSIE and Firefox would differ in this behavior. ")]
        public bool ForceHexStyleColors { get; set; }



        [Category("Validation")]
        [Description("Whether to validate control on blur or not")]
        public bool DoNotValidateOnBlur
        {
            get
            {
                return _functionality.DoNotValidateOnBlur;
            }

            set
            {
                _functionality.DoNotValidateOnBlur = value;

            }
        }
        public bool InDesignMode
        {
            get
            {
                return (System.Web.HttpContext.Current == null);
            }
        }
        
        [Category("TinyMCE Plugins")]
        [DefaultValue("")]
        [Description("Content CSS File location")]
        public string ContentCSSFile { get { return _ContentCSSFile; } set { _ContentCSSFile = value; } }


        [Category("TinyMCE Plugins")]
        [DefaultValue(false)]
        [Description("Allow mouse over/out image swapping")]
        public bool PluginMouseOverImage { get { return _PluginMouseOverImage; } set { _PluginMouseOverImage = value; } }

        [Category("TinyMCE Plugins")]
        [DefaultValue(true)]
        [Description("Context Menu")]
        public bool PluginContextMenu { get { return _PluginContextMenu; } set { _PluginContextMenu = value; } }
        
        [Category("TinyMCE Plugins")]
        [DefaultValue(false)]
        [Description("Allow insertion of smilies. [Buttons: emotions]")]
        public bool PluginEmoticons { get { return _PluginEmoticons; } set { _PluginEmoticons = value; } }
        
        [Category("TinyMCE Plugins")]
        [DefaultValue(true)]
        [Description("Allow resizing to full screen [Buttons: fullscreen]")]
        public bool PluginFullPage { get { return _PluginFullPage; } set { _PluginFullPage = value; } }
        
        [Category("TinyMCE Plugins")]
        [DefaultValue(false)]
        [Description("Allow adding of media like flash, quicktime, etc [buttons: media]")]
        public bool PluginMedia { get { return _PluginMedia; } set { _PluginMedia = value; } }
        
        [Category("TinyMCE Plugins")]
        [DefaultValue(false)]
        [Description("Allow insertion of date / time [buttons: insertdate,inserttime]")]
        public bool PluginInsertDateTime { get { return _PluginInsertDateTime; } set { _PluginInsertDateTime = value; } }

        [Category("TinyMCE Plugins")]
        [DefaultValue(false)]
        [Description("More advanced link dialog, supporting popup windows & targets")]
        public bool PluginAdvancedLink { get { return _PluginAdvancedLink; } set { _PluginAdvancedLink = value; } }
        
        [Category("TinyMCE Plugins")]
        [DefaultValue(true)]
        [Description("Allows pasting directly from word or plain Util.Text. [Buttons: pastetext,pasteword,selectall]")]
        public bool PluginPasteFromWord { get { return _PluginPasteFromWord; } set { _PluginPasteFromWord = value; } }
        
        [Category("TinyMCE Plugins")]
        [DefaultValue(true)]
        [Description("Allows previewing document [Buttons: preview]")]
        public bool PluginPreview { get { return _PluginPreview; } set { _PluginPreview = value; } }

        [Category("TinyMCE Plugins")]
        [DefaultValue(550)]
        [Description("Width of preview document")]
        public int PluginPreviewWidth { get { return _PluginPreviewWidth; } set { _PluginPreviewWidth = value; } }
        [Category("TinyMCE Plugins")]
        [DefaultValue(600)]
        [Description("Height of preview document")]
        public int PluginPreviewHeight { get { return _PluginPreviewHeight; } set { _PluginPreviewHeight = value; } }
        
        [Category("TinyMCE Plugins")]
        [DefaultValue(false)]
        [Description("Adds printing button. [buttons: print]")]
        public bool PluginPrint { get { return _PluginPrint; } set { _PluginPrint = value; } }
        
        [Category("TinyMCE Plugins")]
        [DefaultValue(false)]
        [Description("Allow posting of form by clicking on a save button [Buttons:save,cancel]")]
        public bool PluginSaveButton { get { return _PluginSaveButton; } set { _PluginSaveButton = value; } }
        
        [Category("TinyMCE Plugins")]
        [DefaultValue(true)]
        [Description("Add search/replace functionality [Buttons: search,replace]")]
        public bool PluginSearchReplace { get { return _PluginSearchReplace; } set { _PluginSearchReplace = value; } }

        [Category("TinyMCE Plugins")]
        [DefaultValue(true)]
        [Description("Add table modification controls [ Buttons: tablecontrols]")]
        public bool PluginTable { get { return _PluginTable; } set { _PluginTable = value; } }

        [Category("TinyMCE Configuration")]
        [DefaultValue(900)]
        [Description("Width of TinyMCE Editor")]
        public int SourceEditorWidth { get { return _SourceEditorWidth; } set { _SourceEditorWidth = value; } }
        [Category("TinyMCE Configuration")]
        [DefaultValue(650)]
        [Description("Height of TinyMCE Editor")]
        public int SourceEditorHeight { get { return _SourceEditorHeight; } set { _SourceEditorHeight = value; } }
        [Category("TinyMCE Configuration")]
        [DefaultValue(true)]
        [Description("Whether TinyMCE Editor wraps or not")]
        public bool SourceEditorWrap { get { return _SourceEditorWrap; } set { _SourceEditorWrap = value; } }
        

        [Category("TinyMCE Configuration")]
        [DefaultValue(THEME_TYPE.Advanced)]
        [Description("Theme type of TinyMCE")]
        public THEME_TYPE ThemeType
        {
            get
            {
                return _ThemeType;
            }

            set
            {
                _ThemeType = value;
            }
        }
        [Category("TinyMCE Configuration")]
        [Description("Location of toolbar")]
        public TOOLBAR_LOCATION ToolbarLocation
        {
            get
            {
                return _ToolbarLocation;
            }

            set
            {
                _ToolbarLocation = value;
            }
        }


        [Category("TinyMCE Configuration")]
        [DefaultValue("/includes/tiny_mce/tiny_mce.js")]
        [Description("Location of 'tiny_mce.js' file for TinyMCE to work")]
        public string TinyMCEJSFile
        {
            get
            {
                return _TinyMCEJSFile;
            }

            set
            {
                _TinyMCEJSFile = value;
            }
        }

        


        #endregion
        #region Methods
        public void AddAllowedStyle(string Name, string CSSClass)
        {
            _AllowedStyles += Name + "=" + CSSClass + ";";
        }
        public void AddAllowedFont(string Name, string FontName)
        {
            _AllowedFonts += Name + "=" + FontName + ";";
        }
        public void AddAllowedFontSize(string Name, string fontSize)
        {
            _AllowedFontSizes += Name + "=" + fontSize + ";";
        }
        public void AddAllowedTextColor(string Color)
        {
            if (Color.StartsWith("#"))
                Color = Color.Remove(0,1);
            if (!string.IsNullOrEmpty(_AllowedTextColors))
                _AllowedTextColors += ",";
            _AllowedTextColors += Color;
            
        }
        public void AddAllowedBackgroundColor(string Color)
        {
            if (Color.StartsWith("#"))
                Color = Color.Remove(0,1);
            if (!string.IsNullOrEmpty(_AllowedTextColors))
                _AllowedBackgroundColors += ",";
            _AllowedBackgroundColors += Color;
            
        }
        /// <summary>
        /// This option should contain a comma separated list of formats that will be available in the format drop down list. The default value of this option is "p,address,pre,h1,h2,h3,h4,h5,h6". This option is only available if the advanced theme is used.
        /// This list is used to populate available formats in the formatselect dropdown. 
        /// </summary>
        /// <param name="tagName"></param>
        public void AddAllowedBlockFormat(string tagName)
        {
            
            if (!string.IsNullOrEmpty(_AllowedBlockFormats))
                _AllowedBlockFormats += ",";
            _AllowedBlockFormats += tagName;

        }
        public void AddAllowedElement(string tag)
        {
            if (_ExtraAllowedElements == null)
                _ExtraAllowedElements = new List<string>();
            _ExtraAllowedElements.Add(tag);
            
        }
        private string getListToString(List<string> list)
        {
            string s = "";
            for (int i = 0; i < list.Count; i++)
            {
                if (i > 0)
                    s += ",";
                s += list[i];
            }
            return s;
        }

        #endregion
        
        private string addTinyMCEProperty(string js, string name ,bool property)
        {
            js += name + " : " + (property ? "true" : "false") + ",";
            return js;
        }
        private string addTinyMCEProperty(string js, string name, int property)
        {
            if (property > 0)
            {
                js += name + " : " + property + ",";
            }
            return js;
        }
        private string addTinyMCEProperty(string js, string name, string property)
        {
            if (!string.IsNullOrEmpty(property))
            {
                js += name + " : \"" + property + "\",";
            }
            return js;
        }
        private void InitTinyMCE()
        {
            if (Visible)
            {
                ButtonControlReferenceList buttonsRow1 = new ButtonControlReferenceList(), buttonsRow2 = new ButtonControlReferenceList(), buttonsRow3 = new ButtonControlReferenceList();
                ButtonControlReferenceList buttonsRow1Before = new ButtonControlReferenceList(), buttonsRow2Before = new ButtonControlReferenceList(), buttonsRow3Before = new ButtonControlReferenceList();
                Page.ClientScript.RegisterClientScriptInclude(this.GetType(), "tinymce_javascript_include", TinyMCEJSFile);
                string JS = "";
                JS += "<script language=\"javascript\" type=\"text/javascript\">";
                JS += "tinyMCE.init({";
                #region Configuration
                if (ThemeType == THEME_TYPE.Simple)
                {
                    JS += "theme : \"simple\",";
                }
                else
                {
                    JS += "theme : \"advanced\",";
                }
                if (ToolbarLocation == TOOLBAR_LOCATION.Top)
                {
                    JS += "theme_advanced_toolbar_location : \"top\",";
                }
                else
                {
                    JS += "theme_advanced_toolbar_location : \"bottom\",";
                }
                if (ContentCSSFile != "" && ContentCSSFile != null)
                {
                    JS += "content_css : \"" + ContentCSSFile + "\",";
                }
                if (_AllowedStyles != "")
                {
                    JS += "theme_advanced_styles : \"" + _AllowedStyles + "\",";
                }
                string b = "false";
                if (_SourceEditorWrap)
                    b = "true";

                if (_ExtraAllowedElements != null && _ExtraAllowedElements.Count > 0)
                {

                    JS += "extended_valid_elements : \"";
                    for (int i =0; i < _ExtraAllowedElements.Count; i++)
                    {
                        if (i >0)
                            JS += ",";
                        JS += _ExtraAllowedElements[i] + "[*]";
                    }
                    JS += "\",";

                }
                JS += "body_class : \"tinymce_editor\",";
                JS = addTinyMCEProperty(JS,"accessibility_warnings",this.AccessibilityWarnings);
                JS = addTinyMCEProperty(JS,"auto_resize",this.AutoResize);
                JS = addTinyMCEProperty(JS,"editor_selector",this.EditorSelector);
                JS = addTinyMCEProperty(JS,"keep_styles",this.KeepStyles);
                JS = addTinyMCEProperty(JS,"theme_advanced_blockformats",this._AllowedBlockFormats);
                JS = addTinyMCEProperty(JS,"theme_advanced_source_editor_height",this.SourceEditorHeight);
                JS = addTinyMCEProperty(JS,"theme_advanced_source_editor_width",this.SourceEditorWidth);
                JS = addTinyMCEProperty(JS,"theme_advanced_source_editor_wrap",this.SourceEditorWrap);
                JS = addTinyMCEProperty(JS,"apply_source_formatting",this.ApplySourceFormatting);
                JS = addTinyMCEProperty(JS, "preformatted", this.Preformatted);
                JS = addTinyMCEProperty(JS, "convert_fonts_to_spans", this.ConvertFontsToSpans);
                JS = addTinyMCEProperty(JS,"convert_newlines_to_brs",this.ConvertNewlinesToBRs);
                JS = addTinyMCEProperty(JS,"fix_content_duplication",this.FixContentDupliaction);
                JS = addTinyMCEProperty(JS,"fix_list_elements",this.FixListElements);
                JS = addTinyMCEProperty(JS,"fix_table_elements",this.FixTableElements);
                JS = addTinyMCEProperty(JS,"fix_nesting",this.FixNesting);
                JS = addTinyMCEProperty(JS, "remove_trailing_nbsp", this.RemoveTrailingNonBreakingSpaces);
                JS = addTinyMCEProperty(JS, "force_p_newlines", this.ForceParagraphsOnNewLines);
                JS = addTinyMCEProperty(JS,"force_br_newlines",this.ForceBROnNewLines);
                JS = addTinyMCEProperty(JS,"force_hex_style_colors",this.ForceHexStyleColors);

                if (!Width.IsEmpty)
                    JS += "width: \"" + Width + "\",";
                if (!Height.IsEmpty)
                    JS += "height: \"" + Height + "\",";
                #endregion
                #region Plugins
                string plugins = "";
                if (PluginContextMenu) plugins += ",contextmenu";
                if (PluginEmoticons) { plugins += ",emotions"; buttonsRow3.Add(BUTTON_CONTROL_REFERENCE.Emotions); }
                if (PluginFullPage) { plugins += ",fullscreen"; buttonsRow1.Add(BUTTON_CONTROL_REFERENCE.FullScreen); }
                if (PluginMedia) { plugins += ",media"; buttonsRow2Before.Add(BUTTON_CONTROL_REFERENCE.Media); };
                if (PluginInsertDateTime) { plugins += ",insertdatetime";  buttonsRow3.Add( BUTTON_CONTROL_REFERENCE.InsertDate, BUTTON_CONTROL_REFERENCE.InsertTime); }
                if (PluginAdvancedLink) plugins += ",advlink";
                if (PluginPasteFromWord) { plugins += ",paste"; buttonsRow2Before.Add( BUTTON_CONTROL_REFERENCE.PasteText,BUTTON_CONTROL_REFERENCE.PasteWord, BUTTON_CONTROL_REFERENCE.SelectAll); }
                if (PluginSaveButton) { plugins += ",save"; buttonsRow1Before.Add( BUTTON_CONTROL_REFERENCE.Save,BUTTON_CONTROL_REFERENCE.Cancel); }
                if (PluginPreview) { plugins += ",preview"; buttonsRow2Before.Add( BUTTON_CONTROL_REFERENCE.Preview); }
                if (PluginPrint) { plugins += ",print"; buttonsRow2Before.Add(BUTTON_CONTROL_REFERENCE.Print); }
                if (PluginSearchReplace) { plugins += ",searchreplace"; buttonsRow2Before.Add( BUTTON_CONTROL_REFERENCE.Search, BUTTON_CONTROL_REFERENCE.Replace); }
                if (PluginTable) { plugins += ",table";  buttonsRow3.Add( BUTTON_CONTROL_REFERENCE.TableControls); }
                plugins += ",advimage";
                if (plugins != "")
                    plugins = plugins.Remove(0, 1);
                

                //MouseOverImage:: advimage
                //ContextMenu :: contextmenu
                //Emoticons: emotions
                //FullPage: fullpage [fullscreen]
                //Media:  media - Allows embedding of media items, like flash, quicktime,shockwave, etc
                //InsertDateTime: insertdatetime - allows inserting of date/time [buttons: insertdate,inserttime]
                //AdvancedLink: advlink - allows advanced linking
                //PasteFromWord: paste - allows pasting plain text, and directly from word [buttons: pastetext,pasteword,selectall]
                //Preview:  preview - allows previewing the thing: [buttons: preview]
                //Print: print - allow printing [ buttons:print]
                //SaveButton: save button, to submit form  [ buttons: save,cancel]
                //SearchReplace: allow searching & replacing of text [buttons: search,replace]
                //Table: allow table managements [buttons: tablecontrols]
                if (plugins != "")
                {
                    JS += "plugins : \"" + plugins + "\",";
                }
                #endregion
                
                #region Plugin Settings
                if (PluginPreview)
                {
                    JS += "plugin_preview_width : \"" + PluginPreviewWidth.ToString() + "\",";
                    JS += "plugin_preview_height : \"" + PluginPreviewHeight.ToString() + "\",";
                }
                if (PluginInsertDateTime)
                {
                    JS += "plugin_insertdate_dateFormat : \"%d-%m-%Y\",";
                }
                buttonsRow1Before.Add(BUTTON_CONTROL_REFERENCE.FontSelect);
                buttonsRow1Before.Add(BUTTON_CONTROL_REFERENCE.ForeColor);
                buttonsRow1Before.Add(BUTTON_CONTROL_REFERENCE.FontSizeSelect);
                if (buttonsRow1.Count > 0) JS += "theme_advanced_buttons1_add : \"" + buttonsRow1.ToString() + "\",";
                if (buttonsRow2.Count > 0) JS += "theme_advanced_buttons2_add : \"" + buttonsRow2.ToString() + "\",";
                if (buttonsRow3.Count > 0) JS += "theme_advanced_buttons3_add : \"" + buttonsRow3.ToString() + "\",";
                if (buttonsRow1Before.Count > 0) JS += "theme_advanced_buttons1_add_before : \"" + buttonsRow1Before.ToString() + "\",";
                if (buttonsRow2Before.Count > 0) JS += "theme_advanced_buttons2_add_before : \"" + buttonsRow2Before.ToString() + "\",";
                if (buttonsRow3Before.Count > 0) JS += "theme_advanced_buttons3_add_before : \"" + buttonsRow3Before.ToString() + "\",";
                #endregion
                JS += "    mode : \"none\"";
                JS += " });";
                JS += "</script>";
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "tinymce_javascript_init", JS);
            }

        }
        protected override void OnInit(EventArgs e)
        {
            
            base.OnInit(e);
            
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (!InDesignMode)
            {
                InitTinyMCE();
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            
            this.TextMode = TextBoxMode.MultiLine;
            string JS = "";
            
            /*
            writer.WriteBeginTag("input");
            writer.WriteAttribute("type", "text");
            writer.WriteAttribute("columns", "1");
            writer.WriteAttribute("value", "form");
            writer.WriteAttribute("id", ClientID + "_required");
            writer.WriteAttribute("style", "position: absolute; visibility:hidden; left: -1000px; padding: 0px 0px 0px 0px; width:1px;");
            writer.Write(HtmlTextWriter.SelfClosingTagEnd);
            */
            /*
            writer.WriteBeginTag("input");
            writer.WriteAttribute("type", "text");
            writer.WriteAttribute("columns", "1");
            writer.WriteAttribute("value", "false");
            writer.WriteAttribute("id", ClientID + "_err");
            writer.WriteAttribute("style", "position: absolute; visibility:hidden; left: -1000px; padding: 0px 0px 0px 0px; width:1px;");
            writer.Write(HtmlTextWriter.SelfClosingTagEnd);*/


            base.Render(writer);
            if (this.Visible)
            {
                JS = "\n";
                JS += "tinyMCE.execCommand(\"mceAddControl\", true, \"" + ClientID + "\");";
                JS = CS.General_v3.Util.Text.MakeJavaScript(JS, true);
                writer.Write(JS);
                Page.ClientScript.RegisterClientScriptInclude("tinymce_javascript", TinyMCEJSFile);
            }
            
        }


        #region IMyFormWebControl Members


        public override string ValidationGroup
        {
            get
            {
                return _functionality.ValidationGroup;
            }
            set
            {
                _functionality.ValidationGroup = value;
            }
        }
        [Category("Title")]
        [DefaultValue("")]
        [Description("Title to show for validation")]
        public string Title
        {
            get
            {
                return _functionality.Title;
            }

            set
            {
                _functionality.Title = value;
                /*
                */
            }
        }
        /// <summary>
        /// The value retrieved from the posted Form
        /// </summary>
        public virtual object FormValueObject
        {
            get
            {

                return _functionality.FormValue;
            }

        }
        public virtual object Value
        {
            get
            {
                return this.Text;
            }
            set
            
            {
                string s = "";
                if (value != null)
                    s = value.ToString();
                this.Text = s;

            }
        }
        #region classes
        [Category("Javascript")]
        [DefaultValue(false)]
        [Description("Whether to use default CSS classes or not")]
        public bool UseDefaultCSSClasses
        {
            get
            {
                return _functionality.UseDefaultCSSClasses;
            }

            set
            {
                _functionality.UseDefaultCSSClasses = value;

            }
        }
        public string Class_Disabled { get { return _functionality.Class_Disabled; } set { _functionality.Class_Disabled = value; } }
        public string Class_onFocus { get { return _functionality.Class_Focus; } set { _functionality.Class_Focus = value; } }
        public string Class_onError { get { return _functionality.Class_Error; } set { _functionality.Class_Error = value; } }
        public string Class_onOver { get { return _functionality.Class_Over; } set { _functionality.Class_Over = value; } }
        public string Class_ReadOnly { get { return _functionality.Class_ReadOnly; } set { _functionality.Class_ReadOnly = value; } }
        public string Class_Required { get { return _functionality.Class_Required; } set { _functionality.Class_Required = value; } }

        #endregion

        public string FormValue
        {
            get
            {
                return _functionality.FormValue;
            }
        }

        #endregion



        #region IMyFormWebControl Members

        public bool HasValidation
        {
            get
            {
                return _functionality.HasValidation;

            }
            set
            {
                _functionality.HasValidation = value;

            }
        }

        #endregion


        #region IMyWebControl Members

        public object Tag
        {
            get { return _functionality.Tag; }
            set { _functionality.Tag = value; }
        }

        public string OnClientClick { get { return _functionality.OnClientClick; } set { _functionality.OnClientClick = value; } }
        public string OnClientLoad { get { return _functionality.OnClientLoad; } set { _functionality.OnClientLoad = value; } }
        public string OnClientChange { get { return _functionality.OnClientChange; } set { _functionality.OnClientChange = value; } }
        public string OnClientBlur { get { return _functionality.OnClientBlur; } set { _functionality.OnClientBlur = value; } }
        public string HelpID
        {
            get
            {
                return _functionality.HelpID;
            }
            set
            {
                _functionality.HelpID = value;
            }
        }

        public string HelpMsg
        {
            get
            {
                return _functionality.HelpMsg;
            }
            set
            {
                _functionality.HelpMsg = value;
            }
        }

        public string RollOverClass { get { return _functionality.RollOverClass; } set { _functionality.RollOverClass = value; } }


        #endregion

        #region IMyFormWebControl Members

        public string ClientName
        {
            get { return _functionality.ClientName; }
        }

        public bool Required
        {
            get
            {
                return _functionality.Required;
                
            }
            set
            {
                _functionality.Required = true;
                
            }
        }

        #endregion

        #region IMyWebControl Members


        public string StringID {get;set;}
        #endregion
        public WebControl Control
        {
            get { return this; }
        }

        public FormFieldsValidationParams.FormFieldValidationSubGroupParams SubGroupParams { get { return WebControlFunctionality.SubGroupParams; } set { WebControlFunctionality.SubGroupParams = value; } }

        public Enums.DATA_TYPE DataType
        {
            get
            {
                return Enums.DATA_TYPE.String;
            }
        }

        #region IMyFormWebControl Members


        public General.MyFormWebControlFunctionality WebControlFunctionality
        {
            get { return _functionality; }
        }

        #endregion

        #region IBaseWebControl Members

        public General_v3.Classes.CSS.CSSManagerForControl CssManager
        {
            get { throw new NotImplementedException(); }
        }

        BaseWebControlFunctionality IBaseWebControl.WebControlFunctionality
        {
            get { throw new NotImplementedException(); }
        }

        #endregion

        #region IBaseControl Members

        Control IBaseControl.Control
        {
            get { return this; }
        }

        BaseControlFunctionality IBaseControl.WebControlFunctionality
        {
            get { return _functionality; }
        }

        #endregion

        #region IMyFormWebControl Members


        public FormFieldsValidationParams ValidationParams
        {
            get
            {
                return this.WebControlFunctionality.ValidationParams;
            }
            set { this.WebControlFunctionality.UpdateValidationParams(value); }

        }

        public FormFieldsCSSClasses FieldCssClasses
        {
            get
            {
                return this.WebControlFunctionality.FieldCssClasses;
            }
            set
            {
                this.WebControlFunctionality.FieldCssClasses = value;
            }
        }

        #endregion
    }
}
