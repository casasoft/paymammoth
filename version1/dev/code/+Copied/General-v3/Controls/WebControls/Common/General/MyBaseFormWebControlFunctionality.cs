﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;
namespace CS.General_v3.Controls.WebControls.Common.General
{
    public class MyBaseFormWebControlFunctionality : BaseWebControlFunctionality
    {
        private FormFieldsValidationParams _validationParams;
        public virtual FormFieldsValidationParams ValidationParams
        {
            get {
                if (_validationParams == null)
                {
                    _validationParams = createValidationParams();
                }
                return _validationParams;
            }
            set { _validationParams = value; }

        }
        public void UpdateValidationParams(FormFieldsValidationParams p)
        {
            _validationParams = p;
        }

        public FormFieldsCSSClasses FieldCssClasses { get; set; }
        public bool UseDefaultCSSClasses { get; set; }

        public MyBaseFormWebControlFunctionality(WebControl control)
            : base(control)
        {
            
            this.FieldCssClasses = new FormFieldsCSSClasses();
        }

        protected virtual FormFieldsValidationParams createValidationParams()
        {
            return new FormFieldsValidationParams();
        }



        public string Class_Required { get { return FieldCssClasses.cssClassRequired; } set { FieldCssClasses.cssClassRequired = value; } }
        public string Class_ReadOnly { get { return FieldCssClasses.cssClassReadOnly; } set { FieldCssClasses.cssClassReadOnly = value; } }
        public string Class_Disabled { get { return FieldCssClasses.cssClassDisabled; } set { FieldCssClasses.cssClassDisabled = value; } }
        public string Class_Focus { get { return FieldCssClasses.cssClassFocus; } set { FieldCssClasses.cssClassFocus = value; } }
        public string Class_Error { get { return FieldCssClasses.cssClassError; } set { FieldCssClasses.cssClassError = value; } }
        public string Class_Over { get { return FieldCssClasses.cssClassOver; } set { FieldCssClasses.cssClassOver = value; } }
        public object Tag { get; set; }

        public string HelpID { get; set; }
        public string HelpMsg { get; set; }
        public virtual string ValidationGroup
        {
            get
            {
                return ValidationParams.validationGroup;
            }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    ValidationParams.validationGroup = value;
                }
                else
                {
                    ValidationParams.validationGroup = "main";// Default
                }
                
            }
        }
        private string _title;
        public string Title
        {
            get
            {
                string tmp;
                if (_title != "")
                    tmp = _title;
                else
                    tmp = Control.ID;
                return tmp;
            }

            set
            {
                _title = value;

            }
        }

        public virtual void FocusGood()
        {
            this.Control.PreRender -= Control_PreRenderSetFocus; // jsut in case
            this.Control.PreRender += new EventHandler(Control_PreRenderSetFocus);
            /*string jsFunction = "";
            if (CS.General_v3.Settings.Others.JScriptFramework == Enums.JSCRIPT_FRAMEWORK.JQuery)
            {
                jsFunction = "jQuery";
            }
            else
            {
                throw new Exception("Please specify a valid JScriptFramework in settings.config");
            }

            string js = jsFunction + "(function() { try { document.getElementById('" + this.Control.ClientID + "').focus(); } catch(e) {} });";
            js = CS.General_v3.Util.JSUtil.MakeJavaScript(js, true);
            Page pg = this.Control.Page;
            if (pg == null)
                pg = CS.General_v3.Util.PageUtil.GetCurrentPage();
            if (pg != null)
            {
                pg.ClientScript.RegisterStartupScript(this.GetType(), this.Control.ClientID + "Focus", js);
            }*/

        }

        void Control_PreRenderSetFocus(object sender, EventArgs e)
        {
            string js = "jQuery('#"+Control.ClientID+"').focus();\r\n";
            CS.General_v3.Util.PageUtil.GetCurrentPage().ClientScript.RegisterStartupScript(this.GetType(), this.Control.ClientID + "Focus", js, true);
            
        }

    }
}
