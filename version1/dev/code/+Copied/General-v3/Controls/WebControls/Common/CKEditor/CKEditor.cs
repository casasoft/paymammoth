/*
 * FCKeditor - The text editor for Internet - http://www.fckeditor.net
 * Copyright (C) 2003-2007 Frederico Caldeira Knabben
 *
 * == BEGIN LICENSE ==
 *
 * Licensed under the terms of any of the following licenses at your
 * choice:
 *
 *  - GNU General Public License Version 2 or later (the "GPL")
 *    http://www.gnu.org/licenses/gpl.html
 *
 *  - GNU Lesser General Public License Version 2.1 or later (the "LGPL")
 *    http://www.gnu.org/licenses/lgpl.html
 *
 *  - Mozilla Public License Version 1.1 or later (the "MPL")
 *    http://www.mozilla.org/MPL/MPL-1.1.html
 *
 * == END LICENSE ==
 *
 * This is the FCKeditor Asp.Net control.
 */

using System ;
using System.Web.UI ;
using System.Collections.Generic;
using System.Web.UI.WebControls ;
using System.ComponentModel ;
using System.Text;
using System.Text.RegularExpressions ;
using System.Globalization ;
using System.Security.Permissions ;

namespace CS.General_v3.Controls.WebControls.Common
{
	
	[ DefaultProperty("Value") ]
	[ ValidationProperty("Value") ]
	[ ToolboxData("<{0}:CKeditor runat=server></{0}:CKeditor>") ]
	[ ParseChildren(false) ]
	public partial class CKEditor : MyTxtBoxTextMultiLine, CS.General_v3.Controls.WebControls.Common.General.IMyFormWebControl
	{
        public CKEditor()
            : this("")
        {
            
        }
        public CKEditor(string ID)
        {
            this._myWebControl = new CS.General_v3.Controls.WebControls.Common.General.MyFormWebControlFunctionality(this, ClientIDSeparator);
            this.ID = ID;
            this.ConfigParameters = new CONFIG_PARAMETERS(this);
            //Do not change these defaults
            this.ConfigParameters.BodyClass = "html-container";
            this.ConfigParameters.EnterMode = CONFIG_PARAMETERS.ENTER_MODE.NewBR;
           
            this.ConfigParameters.ContentCSSFiles = new List<string>() { "/css/main.css" };
            
        }
        

        public CONFIG_PARAMETERS ConfigParameters {get;set;}
        private void renderJS()
        {

            var configObj = this.ConfigParameters.GetAsJSObject();
            string varname = "__editor_" + this.ID;
            string js = "";
           
            js += varname + " = CKEDITOR.replace('" + this.ClientID + "'," + configObj.GetJS(false) + ");\r\n";
            if (ConfigParameters.UseCKFinder)
            {
                js += "CKFinder.SetupCKEditor( "+varname+", '"+ConfigParameters.CKFinder_FolderPath+"' );\r\n";
            }
           // js += "CKEDITOR.replace('" + this.ClientID + "');";

            Page.ClientScript.RegisterStartupScript(this.GetType(), this.ID, js, true);
        }
        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);
            renderJS();
            
        }
        protected General.MyFormWebControlFunctionality _myWebControl = null;
       

        #region IMyWebControl Members

       

        public new WebControl Control
        {
            get { return (WebControl)this; }
        }

        #endregion


        Control IBaseControl.Control
        {
            get
            {
                return this.Control;
            }
        }

        BaseControlFunctionality IBaseControl.WebControlFunctionality
        {
            get
            {
                return this.WebControlFunctionality;
            }
        }
    }
}
