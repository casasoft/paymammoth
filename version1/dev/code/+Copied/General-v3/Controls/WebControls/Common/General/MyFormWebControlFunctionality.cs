﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;
using CS.General_v3.JavaScript.Data;
namespace CS.General_v3.Controls.WebControls.Common.General
{
    using JavaScript.Data;

    public class MyFormWebControlFunctionality : MyBaseFormWebControlFunctionality
    {
        

        public string ClientName
        {
            get
            {
                return CS.General_v3.Util.Forms.GetFormVariableID(this.Control, ClientIDSeparator);
            }
        }
       
        public string CssStyle { get; set; }


        public bool AddFieldJS { get; set; }



        public string JSFieldClassName { get; set; }
        public JavaScriptObject JSExtraParams{ get; set; }

        private bool _attachedAttribs;

        public char ClientIDSeparator { get; set; }
        public MyFormWebControlFunctionality(WebControl control, char clientIDSeparator)
            : base(control)
        {
            if (__GlobalCtrlIdentifier == 321)
            {
                int k = 5;
            }
            this.ClientIDSeparator = clientIDSeparator;
            this.UseDefaultCSSClasses = true;
            this.ValidationGroup = "main";
            this.AddFieldJS = true;
            JSFieldClassName = "FieldSingleItem";
            
            this.MaxLength = 0;
            this.MinLength = 0;
            
         init();

        }
        private void initHandlers()
        {
            this.Control.PreRender += new EventHandler(Control_PreRender);
        }

        void Control_PreRender(object sender, EventArgs e)
        {
            attachAttributes();


            addFieldJS();
        }
        
        private void addFieldJS()
        {
            
            if (Control.Visible && AddFieldJS && this.ValidationParams.HasSomeFormOfValidation())
            {

                FormFieldsValidationParams.FormFieldValidationSubGroupParams jsFieldsSubGroupParams = ValidationParams.subGroupParams;
              //  ValidationParams.showJQueryMultiSelect = true;
                string version = CS.General_v3.Settings.Others.JAVASCRIPT_VERSION;
                string js = "new js.com.cs." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION + ".UI.FormsOld." + JSFieldClassName + 
                    "('" + Control.ClientID + "', '" + CS.General_v3.Util.Text.forJS(this.Title) + "', " + ValidationParams.GetJsObject().GetJS() + ", " + FieldCssClasses.GetJsObject().GetJS();
                if (this.JSExtraParams != null)
                {
                    js += ", " + this.JSExtraParams.GetJsObject().GetJS();
                }
                
                js += ");";
                //Defer initialization to end
                CS.General_v3.Util.JSUtilOld.AddJSScriptToPage(js);


       //          public Field(DOMElement element, string title, FormFieldValidationParams validationParams, FieldCSSClasses customCSSClasses)
       /*
                js.AppendLine("_tmpField = new js.com.cs." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION + ".UI.Forms.Field('" + CS.General_v3.Util.Text.forJS(Control.ClientID) + "'," +
                    "'" + CS.General_v3.Util.Text.forJS(this.Title) + "'," +
                    objParams.GetJsObject().GetJS() + ",'" + CS.General_v3.Util.Text.forJS(this.ValidationGroup) + "'," +
                    objClasses.GetJsObject().GetJS() + "," + jsFieldsSubGroupParams.GetJsObject(false).GetJS() + ");");
                if (!string.IsNullOrEmpty(this.InitValue))
                    js.AppendLine("_tmpField.initValue = '" + CS.General_v3.Util.Text.forJS(this.InitValue) + "';");*/

                //js.Append("_tmpField.init();");

            }
        }
        private void init()
        {
            initHandlers();
        }
        /*protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }
        public override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);
            renderFieldElem(writer);
     	    
        }
        */


/*
        protected void getFieldElemFields(out CS.General_v3.JavaScript.Data.JSObject jsParams,
            out CS.General_v3.JavaScript.Data.JSObject jsClasses)
        {
            jsParams = new CS.General_v3.JavaScript.Data.JSObject();
            jsClasses = new CS.General_v3.JavaScript.Data.JSObject();

            /*CLASS_NOFOCUS: 'field-nofocus',
	CLASS_NOFOCUS_OVER: 'field-nofocus-over',
	CLASS_FOCUS: 'field-focus',
	CLASS_ERROR: 'field-error',
	CLASS_ERROR_OVER: 'field-error-over',
	CLASS_ERROR_FOCUS: 'field-errorfocus',
	CLASS_DISABLED: 'field-disabled',
	CLASS_READONLY: 'field-readonly',
	CLASS_REQUIRED: 'required' //This is 

            string delim = "#$#";
            
            StringBuilder js = new StringBuilder();
            
            //            if (this.Delimeter != "")
            //              js.Append("_tmpField._VP.Delimeter = '" + CS.General_v3.Util.Text.forJS(this.Delimeter) + "';");
            
            
            if (!string.IsNullOrEmpty(this.Class_Disabled)) jsClasses.AddProperty("CLASS_DISABLED",this.Class_Disabled,true);
            if (!string.IsNullOrEmpty(this.Class_onError) ) jsClasses.AddProperty("CLASS_ERROR",this.Class_onError,true);
            if (!string.IsNullOrEmpty(this.Class_onErrorOver))jsClasses.AddProperty("CLASS_ERROR_OVER",this.Class_onErrorOver,true);
            if (!string.IsNullOrEmpty(this.Class_onErrorFocus)) jsClasses.AddProperty("CLASS_ERROR_FOCUS",this.Class_onErrorFocus,true);
            if (!string.IsNullOrEmpty(this.Class_onFocus)) jsClasses.AddProperty("CLASS_FOCUS",this.Class_onFocus,true);

            /* 2009-08-26 Commented by Mark to let user include initial CSS class with element
             * if (string.IsNullOrEmpty(this.Class_onNormal) && !string.IsNullOrEmpty(this.Control.CssClass))
            {
                this.Class_onNormal = this.Control.CssClass;
            }

            if (!string.IsNullOrEmpty(this.Class_onNormal)) jsClasses.AddProperty("CLASS_NOFOCUS",this.Class_onNormal,true);
            if (!string.IsNullOrEmpty(this.Class_onNormalOver)) jsClasses.AddProperty("CLASS_NOFOCUS_OVER", this.Class_onNormalOver, true);
            if (!string.IsNullOrEmpty(this.Class_Required)) jsClasses.AddProperty("CLASS_REQUIRED",this.Class_Required,true);
            if (!string.IsNullOrEmpty(this.Class_ReadOnly)) jsClasses.AddProperty("CLASS_READONLY",this.Class_ReadOnly,true);




            
            if (this.HasValidation)
            {
                /*
 * > isRequired (Boolean)
 * > doNotValidateOnBlur (Boolean)
 * > isEmail (Boolean)  
 * > isNumber (Boolean)  
 * > integersOnly (Boolean) 
 * > positiveOnly (Boolean) 
 * > negativeOnly (Boolean) 
 * > numFrom (Number) 
 * > numTo (Number) 
 * > isDate (Boolean) 
 * > dateFormat (String) Defaults to 'dd/MM/yyyy' (see: http://www.unicode.org/reports/tr35/tr35-4.html#Date_Format_Patterns)  
 * > dateFrom (Date) 
 * > dateTo (Date) 
 * > isIPAddress(Boolean) 
 * > isCreditCardNumber (Boolean) 
 * > isAlphaNumeric (Boolean) 
 * > minLength (Number) 
 * > maxLength (Number) 
 * > maxWords (Number) 
 * > fileExtensionsAllowed (String) e.g. gif, bmp, tif... 
 * > valueIn (Array) 
 * > valueNotIn (Array) 
 * > valueCaseSensitive (Boolean) Defaults to false (Applies to valueIn, valueNotIn)
 * > regExpPattern (String/RegExp) 
 * > alwaysAllowedValues (Array) - A list of values that escape validation
 * > dateBeforeTxt (String/Element) - A string/element that this text box value must be before
 * > initValue (String) - The initial value of the textbox
                 
                 
                jsParams.AddProperty("doNotValidateOnBlur", this.DoNotValidateOnBlur, true);

                jsParams.AddProperty("isRequired",this.Required,false);
                jsParams.AddProperty("dateFrom",this.DateFrom, true);
                jsParams.AddProperty("dateTo",this.DateTo,true);
                jsParams.AddProperty("integersOnly",this.IntegersOnly,false);
                jsParams.AddProperty("negativeOnly",this.NegativeNumsOnly,false);
                jsParams.AddProperty("positiveOnly",this.PositiveNumsOnly,false);
                jsParams.AddProperty("numFrom",this.NumberFrom,false);
                jsParams.AddProperty("numTo",this.NumberTo,false);
                jsParams.AddProperty("isDate",this.IsDate,false);
                jsParams.AddProperty("isEmail",this.IsEmail,false);
                jsParams.AddProperty("isWebsite",this.IsWebsite ,false);
                jsParams.AddProperty("isNumber",this.IsNumerical,false);
                jsParams.AddProperty("isAlphaNumeric", this.IsAlphabetical,false);
                jsParams.AddProperty("initialEmptyText", this.InitialEmptyText);
                if (this.MinLength > 0)
                    jsParams.AddProperty("minLength", this.MinLength,false);
                if (this.MaxLength > 0)
                    jsParams.AddProperty("maxLength", this.MaxLength, false);
                //if (!string.IsNullOrEmpty (this.InitValue)) js.Append("_tmpField._VP.InitValue = '" + this.InitValue + "';");
                    
                if (!string.IsNullOrEmpty(GetDateBeforeClientID()))
                {
                    jsParams.AddProperty("dateBeforeTxt", GetDateBeforeClientID(),true);
                }
                string tmp = "";
                if (this.allowedValues != null && this.allowedValues.Count > 0)
                {
                    CS.General_v3.JavaScript.Data.JSArray jsArray = new CS.General_v3.JavaScript.Data.JSArray(null);
                    jsArray.AddRange(this.allowedValues);
                    jsParams.AddProperty("alwaysAllowedValues",jsArray.GetJS(),false);
                    
                }
                
                //if (this.HasTime) js.Append("_tmpField._VP.HasTime = true;");
//                if (this.Required) js.Append("_tmpField._VP.Required = true;");

                //if (!string.IsNullOrEmpty (this.RequiredGroup)) js.Append("_tmpField._VP.RequiredGroup = '" + CS.General_v3.Util.Text.forJS(this.RequiredGroup) + "';");
                //if ( (this.IsFile)) js.Append("_tmpField._VP.IsFile = true;");
                //if (!string.IsNullOrEmpty (this.GetTimeFieldCtrlClientID())) 
                {
                    //CS.General_v3.Controls.WebControls.Common.MyTxtBox txtTimeField = (CS.General_v3.Controls.WebControls.Common.MyTxtBox)CS.General_v3.Util.Other.FindControl(Control.Page, this.TimeField);
                    //js.Append("_tmpField._VP.DateBeforeTxt = '" + txtDateBefore.ClientID + "';");
                }
                if (this.ValueIn != null && this.ValueIn.Count > 0)
                {
                    CS.General_v3.JavaScript.Data.JSArray jsArray = new CS.General_v3.JavaScript.Data.JSArray(null);
                    jsArray.AddRange(this.ValueIn);
                    jsParams.AddProperty("valueIn",jsArray.GetJS(),false);
                    
                }
                if (this.ValueNotIn != null && this.ValueNotIn.Count > 0)
                {
                     CS.General_v3.JavaScript.Data.JSArray jsArray = new CS.General_v3.JavaScript.Data.JSArray(null);
                    jsArray.AddRange(this.ValueNotIn);
                    jsParams.AddProperty("valueNotIn",jsArray.GetJS(),false);
                }
                /*if (!string.IsNullOrEmpty(this.GetValueSameAsTxtBoxClientID()))
                {
                    js.Append("_tmpField._VP.SameAsTxt = '" + this.GetValueSameAsTxtBoxClientID() + "';");
                }
                if (this.FileExtensionsAllowed != null && this.FileExtensionsAllowed.Count > 0)
                {
                     CS.General_v3.JavaScript.Data.JSArray jsArray = new CS.General_v3.JavaScript.Data.JSArray(null);
                    
                    jsArray.AddRange(this.FileExtensionsAllowed,true);
                    jsParams.AddProperty("fileExtensionsAllowed",jsArray.GetJS(),false,false,false);
                }
            }

            /*
            
            if (!string.IsNullOrEmpty(this.OnClientChangeClickButton))
            {
                Control c = CS.General_v3.Util.Other.FindControl(Control.Page, this.OnClientChangeClickButton);
                if (c != null)
                {
                    js.Append("_tmpField.onChangeClickButton = '" + CS.General_v3.Util.Text.forJS(c.ClientID) + "';");

                }
                else
                {
                    throw new InvalidOperationException("Button with ID '" + this.OnClientChangeClickButton + "' to click on change of dropdown '" + 
                        Control.ID + "' does not exist");
                }
            }
            
        }

*/
        /*private CS.General_v3.JavaScript.Data.JSObject getGroupParams()
        {
            CS.General_v3.JavaScript.Data.JSObject obj = new CS.General_v3.JavaScript.Data.JSObject();
                    
            if (!String.IsNullOrEmpty(this.GroupID))
            {
                

                obj.AddProperty("groupID", this.GroupID, true);
                switch (GroupType)
                {
                    case GROUP_TYPE.None: throw new Exception("Please provide group type");
                    case GROUP_TYPE.AtLeastOneRequired: obj.AddProperty("atLeastOneIsRequired", "true", false); break;
                    case GROUP_TYPE.SameValues: obj.AddProperty("sameValues", "true", false); break;
                }
                        
            }
            return obj;
        }*/
        /*private void renderFieldElem(HtmlTextWriter writer)
        {
            StringBuilder js = new StringBuilder();
            string tmp = "";
            
            if (Control.Visible)
            {
                
                FormFieldsValidationParams.FIELD_SUBGROUP_TYPE jsGroupParams = getGroupParams();
               

                CS.General_v3.JavaScript.Data.JSObject objParams,objClasses;
                getFieldElemFields(out objParams,out objClasses);

                js.AppendLine("_tmpField = new js.com.cs."+CS.General_v3.Settings.Others.JAVASCRIPT_VERSION+".UI.Forms.Field('" + CS.General_v3.Util.Text.forJS(Control.ClientID) + "'," +
                    "'" + CS.General_v3.Util.Text.forJS(this.Title) + "'," +
                    objParams.GetJS() + ",'" + CS.General_v3.Util.Text.forJS(this.ValidationGroup) + "'," +
                    objClasses.GetJS() + ","+jsGroupParams.GetJS(false)+");");
                if (!string.IsNullOrEmpty(this.InitValue))
                    js.AppendLine("_tmpField.initValue = '" + CS.General_v3.Util.Text.forJS(this.InitValue) + "';");
                //js.Append("_tmpField.init();");
                
                Control.Page.ClientScript.RegisterStartupScript(this.GetType(), Control.ClientID, CS.General_v3.Util.Text.MakeJavaScript(js.ToString(), true));
                //writer.Write(CS.General_v3.Util.Text.MakeJavaScript(js.ToString(), true));
            }
        
        }*/
        

        #region IMyFormWebControl Members
        #region classes


        public string SetValue(object o)
        {
            string s = null;
            if (o != null)
            {
                if (o is double || o is float)
                {
                    double d = (double)o;

                    //todo: karl - 03/jan/2012 - this should be moved to the webcomponents one
                    s = d.ToString("0.00");
                    //s = CS.General_v3.Classes.Application.AppInstance.Instance.CurrentCultureManager.FormatNumberWithCurrentCulture(d, Util.NumberUtil.NUMBER_FORMAT_TYPE.General);
                }

                else if (o is string || o is char || o is int || o is long)
                {
                    s = o.ToString();
                }
                else if (o is DateTime)
                {
                    DateTime d = (DateTime)o;
                    Util.Date.DATETIME_FORMAT formatType = Util.Date.DATETIME_FORMAT.ShortDate;
                    if (this.HasTime)
                        formatType = Util.Date.DATETIME_FORMAT.GeneralDateShortTime;

                    //todo: karl - 03/jan/2012 - this should be moved to the webcomponents one
                    s = d.ToString("dd/MM/yyyy HH:mm:ss");
                    //s = CS.General_v3.Classes.Application.AppInstance.Instance.CurrentCultureManager.FormatDateTimeWithCurrentCulture(d, formatType);
                    
                }

                else if (o is Enum)//enum
                {
                    int i = (int)o;
                    s = i.ToString();
                }
                else if (o is bool)
                {
                    s = o.ToString();
                }
                else
                {
                    throw new InvalidOperationException("Invalid type");
                }

            }
            return s;
        }
        #endregion

        

        
        

        public double FormValueDbl
        {
            get
            {
                if (!string.IsNullOrEmpty(FormValue))
                    return Convert.ToDouble(FormValue);
                else
                    return double.MinValue;
            }
        }
        public double? FormValueDblNullable
        {
            get
            {
                if (!string.IsNullOrEmpty(FormValue))
                    return Convert.ToDouble(FormValue);
                else
                    return null;
            }
        }
        public int FormValueInt
        {
            get
            {
                if (!string.IsNullOrEmpty(FormValue))
                    return Convert.ToInt32(FormValue);
                else
                    return Int32.MinValue;
            }
        }
        public int? FormValueIntNullable
        {
            get
            {
                if (!string.IsNullOrEmpty(FormValue))
                    return Convert.ToInt32(FormValue);
                else
                    return null;
            }
        }
        public DateTime FormValueDate
        {
            get
            {
                if (!string.IsNullOrEmpty(FormValue))
                    return Convert.ToDateTime(FormValue);
                else
                    return DateTime.MinValue;

            }
        }
        public DateTime? FormValueDateNullable
        {
            get
            {
                if (!string.IsNullOrEmpty(FormValue))
                    return Convert.ToDateTime(FormValue);
                else
                    return null;

            }
        }
        public bool FormValueBool
        {
            get
            {
                return CS.General_v3.Util.Other.ConvertStringToBasicDataType<bool>(FormValue);
            }
        }


        private string getFormValue(bool ifStaticRemoveUpTillLastUnderscore)
        {
            string id = CS.General_v3.Util.Forms.GetFormVariableID(Control, ClientIDSeparator, ifStaticRemoveUpTillLastUnderscore);
            string ret = null;
            if (CS.General_v3.Util.PageUtil.GetFormVariable(id) != null)
                ret = CS.General_v3.Util.PageUtil.GetFormVariable(id).ToString();
            if (ret != null && ret == this.InitialEmptyText)
                ret = null;
            return ret;
        }

        /// <summary>
        /// The value retrieved from the posted Form
        /// </summary>
        public string FormValue
        {
            get
            {
                //2011-03-02: Amended by Mark due to static Client IDs

                string ret = getFormValue(false);
                
                if (ret == null && this.Control.ClientIDMode == ClientIDMode.Static)
                {
                    //Try other option
                    ret = getFormValue(true);
                }

                //2011-31-5: Amended by Andre as there is no need for control to be in static client id mode.. this will 
                //not work on getting a checkbox control form value if the checkbox is not in client id mode equals to static
                //if (Control.ClientIDMode == ClientIDMode.Static)
                //{
                //2011-06-17: Commented by Mark since if FormValue is empty, it means that it is empty not to 
                //read the current text or selected value as that hasn't been submitted to the Form
                    /*if (string.IsNullOrEmpty(ret))
                    {
                        if (Control is ITextControl)
                        {
                            ret = ((ITextControl)Control).Text;
                        }
                        else if (Control is ListControl)
                        {
                            ret = ((ListControl)Control).SelectedValue;
                        }
                        else if (Control is ICheckBoxControl)
                        {
                            ret = ((ICheckBoxControl)Control).Checked.ToString().ToLower();
                        }
                        else
                        {
                        }

                    }*/
                //End comment by mark

                //}
                return ret;
            }

        }
        public bool HasTime { get; set; }
        public bool IsFile { get; set; }

        

        public string onClientBlur { get; set; }
        public string OnClientChangeClickButton { get; set; }


        public string TimeFieldID {get;set;}
        public IMyTxtBox<DateTime> TimeFieldCtrl { get; set; }
        public string GetTimeFieldCtrlClientID()
        {
            if (TimeFieldCtrl != null)
                return TimeFieldCtrl.Control.ClientID;
            else
                return TimeFieldID;
        }


        public DateTime? DateFrom { get { return ValidationParams.dateFrom; } set { ValidationParams.dateFrom = value; } }
        public DateTime? DateTo { get { return ValidationParams.dateTo; } set { ValidationParams.dateTo = value; } }


        public bool HasValidation { get { return ValidationParams.hasValidation; } set { ValidationParams.hasValidation = value; } }

        public bool DoNotValidateOnBlur { get { return ValidationParams.doNotValidateOnBlur; } set { ValidationParams.doNotValidateOnBlur = value; } }

        public bool Required { get { return ValidationParams.isRequired; } set { ValidationParams.isRequired = value; } }

        public bool IsDate { get { return ValidationParams.isDate; } set { ValidationParams.isDate = value; } }
        public string InitialEmptyText { get { return ValidationParams.initialEmptyText; } set { ValidationParams.initialEmptyText = value; } }

        public string InitValue { get; set; }

        public bool IsNumerical { get { return ValidationParams.isNumber; } set { ValidationParams.isNumber = value; } }

        public bool IsAlphaNumerical { get { return ValidationParams.isAlphaNumeric; } set { ValidationParams.isAlphaNumeric = value; } }


        public int MinLength { get { return ValidationParams.minLength; } set { ValidationParams.minLength = value; } }
        public int MaxLength { get { return ValidationParams.maxLength; } set { ValidationParams.maxLength = value; } }

        public bool IsEmail { get { return ValidationParams.isEmail; } set { ValidationParams.isEmail = value; } }

        public List<object> ValueNotIn { get { return ValidationParams.valueNotIn; } set { ValidationParams.valueNotIn = value; } }
        public List<object> ValueIn { get { return ValidationParams.valueIn; } set { ValidationParams.valueIn = value; } }


        public double? NumberFrom { get { return ValidationParams.numFrom; } set { ValidationParams.numFrom = value; } }
        public double? NumberTo { get { return ValidationParams.numTo; } set { ValidationParams.numTo = value; } }

        public FormFieldsValidationParams.FormFieldValidationSubGroupParams SubGroupParams { get { return ValidationParams.subGroupParams; } set { ValidationParams.subGroupParams = value; } }


        public bool IsWebsite { get { return ValidationParams.isWebsite; } set { ValidationParams.isWebsite = value; } }


        public bool IntegersOnly { get { return ValidationParams.integersOnly; } set { ValidationParams.integersOnly = value; } }

        public bool NegativeNumsOnly { get { return ValidationParams.negativeOnly; } set { ValidationParams.negativeOnly = value; } }
        public bool PositiveNumsOnly { get { return ValidationParams.positiveOnly; } set { ValidationParams.positiveOnly = value; } }


        /// <summary>
        /// A string representation (for design-time support), for a list of extensions.  Can be seperated by a | or a ,.
        /// </summary>
        public string FileExtensionsAllowedString
        {
            get
            {
                return CS.General_v3.Util.Text.JoinList(FileExtensionsAllowed, ", ");
            }
            set
            {
                FileExtensionsAllowed = new List<string>();
                if (!string.IsNullOrEmpty(value))
                {
                    string[] s = value.Split(',', '|');
                    for (int i = 0; i < s.Length; i++)
                        FileExtensionsAllowed.Add(s[i]);
                }
               
            }
        }
        
        public List<string> FileExtensionsAllowed
        {
            get
            {
                return ValidationParams.fileExtensionsAllowed;
                
            }

            set
            {
                ValidationParams.fileExtensionsAllowed = value;
            }
        }
        #endregion

        #region IMyFormWebControl Members



               

      /*
        private string parseValue(string s)
        {
            s = s.Replace("&lt;", "<");
            s = s.Replace("&gt;", ">");
            s = s.Replace("&amp;", "&");
            return s;



        }
        public string GetFormValueFromForm()
        {
            string id = CS.General_v3.Util.Forms.GetFormVariableID(this.Control,ClientIDSeparator);
            string ret = "";
            if (CS.General_v3.Util.PageUtil.GetFormVariable(id) != null)
                ret = CS.General_v3.Util.PageUtil.GetFormVariable(id).ToString();
            ret = parseValue(ret);

            return ret;
        }*/
        

       /* public object Value
        {
            get
            {
                return this.Control.Attributes["value"];
            }
            set
            {
                
                this.Control.Attributes["value"] = value != null ? value.ToString() : null;
            }
        }*/


      

        #endregion

        public Enums.DATA_TYPE DataType { get; set; }

   

  








        

        private void attachAttributes()
        {
            if (!_attachedAttribs)
            {
                _attachedAttribs = true;
                if (Control != null)
                {
                    
                    //Control.Attributes["name"] = Control.ClientID;
                    if (Control.Attributes["onmouseover"] == null)
                    {
                        Control.Attributes["onmouseover"] = "";
                    }
                    if (Control.Attributes["onmouseout"] == null)
                    {
                        Control.Attributes["onmouseout"] = "";
                    }
                    if (!string.IsNullOrEmpty(CssStyle))
                    {
                        Control.Attributes["style"] += ";" + CssStyle;
                    }
                    if (!string.IsNullOrEmpty(OnClientClick))
                    {
                        Control.Attributes["onclick"] += OnClientClick;
                    }

                    if (!string.IsNullOrEmpty(OnClientChange))
                    {
                        Control.Attributes["onchange"] += OnClientChange;
                    }
                    if (!string.IsNullOrEmpty(OnClientBlur))
                    {
                        Control.Attributes["onblur"] += OnClientBlur;
                    }
                }
            }
        }
        public void ThrowErrorIfIDIsNull()
        {
            if (CS.General_v3.Util.PageUtil.IsLocalhost() && string.IsNullOrEmpty(this.Control.ID))
            {
                //If user is not in CMS
                if (this.Control.Page != null && this.Control.Page.Request.Url.PathAndQuery.IndexOf("/cms/") == -1)
                {
                    throw new Exception("Please specify ID");
                }
            }
        }
    }
}
