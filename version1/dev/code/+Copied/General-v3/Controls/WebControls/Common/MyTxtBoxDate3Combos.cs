using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common.General;
using CS.General_v3.JavaScript.Data;

namespace CS.General_v3.Controls.WebControls.Common
{
    using JavaScript.Data;

    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyTxtBoxDate runat=server></{0}:MyTxtBoxDate>")]

    public class MyTxtBoxDate3Combos : BaseFormWebControl
    {
        public class MyTxtBoxDate3CombosJSParams : JavaScriptObject
        {
            public DateTime? showDateFrom;
            public DateTime? showDateTo; 

            public string unselectedTextDay = "--";
            public string unselectedTextMonth = "--";
            public string unselectedTextYear = "----";
        }

        public class FUNCTIONALITY
        {
           

            private MyTxtBoxDate3Combos _control;

            public MyDropDownList CmbDay { get; private set; }
            public MyDropDownList CmbMonth { get; private set; }
            public MyDropDownList CmbYear { get; private set; }

            public string UnselectedTextDay { get; private set; }
            public string UnselectedTextMonth { get; private set; }
            public string UnselectedTextYear { get; private set; }

            public DateTime ShowDateFrom { get; set; }
            public DateTime ShowDateTo { get; set; }

            public DateTime? InitialDate { get; set; }

            public DateTime? Value
            {
                get
                {
                    return _control.Value;
                }
                set
                {
                    _control.Value = value;
                }
            }

            public FUNCTIONALITY(MyTxtBoxDate3Combos control)
            {
                _control = control;
                this.CmbDay = new MyDropDownList() { ID = "cmbDay", CssClass="date-3-combo-day" };
                this.CmbMonth = new MyDropDownList() { ID = "cmbMonth", CssClass = "date-3-combo-month" };
                this.CmbYear = new MyDropDownList() { ID = "cmbYear", CssClass = "date-3-combo-year" };
                this.ShowDateFrom = DateTime.Now.AddYears(-100);
                this.ShowDateTo = DateTime.Now.AddYears(100);
                this.UnselectedTextDay = this.UnselectedTextMonth = "--";
                this.UnselectedTextYear = "----";
                createValues();
            }
            private void createValues()
            {
                if (!string.IsNullOrEmpty(this.UnselectedTextDay))
                {
                    this.CmbDay.Items.Add(new ListItem() { Text = this.UnselectedTextDay, Value = "" });
                }
                if (!string.IsNullOrEmpty(this.UnselectedTextMonth))
                {
                    this.CmbMonth.Items.Add(new ListItem() { Text = this.UnselectedTextMonth, Value = "" });
                }
                for (int i = 1; i <= 31; i++)
                {
                    this.CmbDay.Items.Add(new ListItem() { Text = i.ToString("00"), Value = i.ToString() });
                }
                for (int i = 1; i <= 12; i++)
                {
                    this.CmbMonth.Items.Add(new ListItem() { Text = i.ToString("00"), Value = i.ToString() });
                }
                
            }
        }

        public FUNCTIONALITY Functionality { get; private set; }

        public MyTxtBoxDate3Combos()
        
        {
            this.CssManager.AddClass("date-3-combobox-container");
            this.Functionality = getFunctionality();
            this.WebControlFunctionality.JSFieldClassName = "FieldDate3Combos";// this is not a normal field 

        }
        protected FUNCTIONALITY getFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        private DateTime? getValue()
        {
            string sDay = this.Functionality.CmbDay.FormValue;
            string sMonth = this.Functionality.CmbMonth.FormValue;
            string sYear = this.Functionality.CmbYear.FormValue;
            if (CS.General_v3.Util.NumberUtil.AreNumeric(sDay, sMonth, sYear))
            {
                int day, month, year;
                day = int.Parse(sDay);
                month = int.Parse(sMonth);
                year = int.Parse(sYear);
                DateTime dt = new DateTime(year, month, day);
                return dt;

            }
            return null;
        }
        private void setValue(DateTime? date)
        {
            if (date.HasValue) {
                this.Functionality.CmbDay.SelectedValue = date.Value.Day.ToString();
                this.Functionality.CmbMonth.SelectedValue = date.Value.Month.ToString();
                this.Functionality.CmbYear.SelectedValue = date.Value.Year.ToString();
            }
            else {
                this.Functionality.CmbDay.SelectedIndex = -1;
                this.Functionality.CmbMonth.SelectedIndex = -1;
                this.Functionality.CmbYear.SelectedIndex = -1;
            }
            this.Functionality.InitialDate = date;
        }
        protected override object valueAsObject
        {
            get
            {
                return Value;
            }
            set
            {
                if (value == null || value is DateTime?)
                {
                    this.Value = (DateTime?)value;
                }
                else if (value is DateTime)
                {
                    this.Value = (DateTime)Value;
                }
                else
                {
                    throw new NotSupportedException("Value must be a DateTime");
                }
            }
        }

        public DateTime? Value
        {
            get
            {
                return getValue();
            }
            set
            {
                setValue(value);
            }
        }
        private void initYearValues()
        {
            this.Functionality.CmbYear.Items.Clear();
            if (!string.IsNullOrEmpty(this.Functionality.UnselectedTextYear))
            {
                this.Functionality.CmbYear.Items.Add(new ListItem() { Text = this.Functionality.UnselectedTextYear, Value = "" });
            }
            for (int i = this.Functionality.ShowDateTo.Year; i >= this.Functionality.ShowDateFrom.Year; i--)
            {
                this.Functionality.CmbYear.Items.Add(new ListItem()
                {
                     Text = i.ToString(), 
                     Value = i.ToString()
                });
            }
            if (this.Functionality.InitialDate.HasValue) {
                this.Functionality.CmbYear.SelectedValue = this.Functionality.InitialDate.Value.Year.ToString();
            }
        }
        private void showControls()
        {
            this.Controls.Add(this.Functionality.CmbDay);
            this.Controls.Add(this.Functionality.CmbMonth);
            this.Controls.Add(this.Functionality.CmbYear);
        }
        private void passJsParams()
        {
            this.WebControlFunctionality.JSExtraParams = new MyTxtBoxDate3CombosJSParams()
            {
                showDateFrom = this.Functionality.ShowDateFrom,
                showDateTo = this.Functionality.ShowDateTo
            };
        }
        protected override void OnLoad(EventArgs e)
        {
            this.passJsParams();
            initYearValues();
            showControls();
            base.OnLoad(e);
        }

        public override object FormValueObject
        {
            get
            {
                return Value;
            }
        }
    }
}
