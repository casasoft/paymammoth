﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.General_20101215.Controls.WebControls.Common.General
{
    public interface IMyWebControl
    {
        //Object Tag { get; set; }
        
        string ClientID { get; }
        string CssClass { get; set; }
        
        /// <summary>
        /// An ID to reference the WebControl with
        /// </summary>
        string StringID { get; set; }
        WebControl Control { get;}

    }
}
