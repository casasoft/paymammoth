using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.General_v3.Controls.WebControls.Common
{
    using General;
    using Specialized.FormFieldsClasses;

    public class MyTxtBoxTextPasswordFunctionality : MyFormWebControlFunctionality
    {
        public new FormFieldsValidationParamsPassword ValidationParams {
            get { return (FormFieldsValidationParamsPassword)base.ValidationParams; }
        }


        public MyTxtBoxTextPasswordFunctionality(WebControl control, char clientIDSeparator)
        : base(control, clientIDSeparator)
        {

        }

        protected override FormFieldsValidationParams createValidationParams()
        {
            return new FormFieldsValidationParamsPassword();
        }
    }
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyTxtBoxTextPassword runat=server></{0}:MyTxtBoxTextPassword>")]

    public class MyTxtBoxTextPassword : MyTxtBoxTextString
    {
        public new MyTxtBoxTextPasswordFunctionality WebControlFunctionality
        {
            get { return (MyTxtBoxTextPasswordFunctionality) base.WebControlFunctionality; }
        }

        public MyTxtBoxTextPassword() :
            base()
        {


            init();

        }
        

        public MyTxtBoxTextPassword(string ID, string Title, bool Required, string HelpMessage) :
            this(ID,Title,Required,HelpMessage,null,null)
        {

            init();
        }
        public MyTxtBoxTextPassword(string ID, string Title, bool Required, string HelpMessage, int? width, string initValue) :
            base()
        {
            if (width != null)
            {
                this.Width = width.Value;
            }

            this.ID = this.StringID = ID;
            this.Title = Title;
            this.SetValue(initValue);

            this.Required = Required;
            this.HelpMsg = HelpMessage;
            init();
        }

        private void init()
        {
            this.TextMode = TextBoxMode.Password;
            this.WebControlFunctionality.JSFieldClassName = "FieldPassword";
        }

        public bool ShowPasswordStrength { get { return this.WebControlFunctionality.ValidationParams.showPasswordStrength; } set { this.WebControlFunctionality.ValidationParams.showPasswordStrength = value; } }

        /// <summary>
        /// Bitwise password strength
        /// </summary>
        public PASSWORD_STRENGTH_CHARACTERS PasswordStrengthType { get { return this.WebControlFunctionality.ValidationParams.passwordStrengthType; } set { this.WebControlFunctionality.ValidationParams.passwordStrengthType = value; } }
        public string PasswordCannotBeLikeFieldID { get { return this.WebControlFunctionality.ValidationParams.passwordCannotBeLikeFieldID; } set { this.WebControlFunctionality.ValidationParams.passwordCannotBeLikeFieldID = value; } }


            protected override MyFormWebControlFunctionality createFunctionality()
        {
            return new MyTxtBoxTextPasswordFunctionality(this, ClientIDSeparator);
        }
        
    }
}
