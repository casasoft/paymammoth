using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.General_v3.Controls.WebControls.Common
{

    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyTable runat=server></{0}:MyTable>")]

    public class MyTable : Table, IBaseWebControl
    {
        private void init()
        {
            this.MyRows = new MyTableRowCollection(this.Rows);
        }
        public MyTable(string cssClass)
        {
            this.CellPadding = this.CellSpacing = 0;
            this._functionality = getFunctionality();
            this.CssManager.AddClass(cssClass);
            init();
        }


        protected virtual BaseWebControlFunctionality getFunctionality()
        {
            return new BaseWebControlFunctionality(this); 
        }


        public MyTable() : this ("")
        {
            init();
            
        }
        public MyTableRow AddRow(string cssClass)
        {
            MyTableRow tr = new MyTableRow();
            
            this.Rows.Add(tr);
            if (!string.IsNullOrEmpty(cssClass))
                tr.CssManager.AddClass(cssClass);
            return tr;
        }
        public MyTableRow AddRow()
        {
            return AddRow("");
        }
        #region IBaseWebControl Members
        private BaseWebControlFunctionality _functionality = null;
        public CS.General_v3.Classes.CSS.CSSManagerForControl CssManager
        {
            get { return _functionality.CssManager; }
        }
        public new string CssClass
        {
            get
            {
                return _functionality.CssClass;
            }
            set
            {
                _functionality.CssClass = value;
                base.CssClass = _functionality.CssClass;
                
            }
        }
        public new MyTableRowCollection MyRows { get;private set;}
        

        #endregion


        #region IBaseWebControl Members


        public BaseWebControlFunctionality WebControlFunctionality
        {
            get { return _functionality; }
        }

        #endregion

        #region IBaseWebControl Members


       

        public WebControl Control
        {
            get { return this; }
        }

        public string RollOverClass { get { return _functionality.RollOverClass; } set { _functionality.RollOverClass = value; } }
        public string StringID { get { return _functionality.StringID; } set { _functionality.StringID = value; } }
        public string OnClientBlur { get { return _functionality.OnClientBlur; } set { _functionality.OnClientBlur = value; } }
        public string OnClientClick { get { return _functionality.OnClientClick; } set { _functionality.OnClientClick = value; } }
        public string OnClientChange { get { return _functionality.OnClientChange; } set { _functionality.OnClientChange = value; } }
        public string OnClientLoad { get { return _functionality.OnClientLoad; } set { _functionality.OnClientLoad = value; } }
        


        #endregion

        #region IBaseControl Members

        Control IBaseControl.Control
        {
            get { return this; }
        }

        BaseControlFunctionality IBaseControl.WebControlFunctionality
        {
            get { return _functionality; }
        }

        #endregion

       /* public new TableRowCollection Rows
        {
            get
            {
                throw new NotSupportedException("Please use the indexor with this class or AddRow method");
            }

        }*/
       

        public MyTableRow this[int i]
        {
            get
            {
                return (MyTableRow)base.Rows[i];
            }
        }
    }
}
