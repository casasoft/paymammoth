﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;

namespace CS.General_v3.Controls.WebControls.Common
{
    using JavaScript.Data;

    public class JSRedirectControllerParameters : JavaScriptObject
    {
        public string url;
        public static void InitJSinPage(JSRedirectControllerParameters p)
        {
            string js = "new js.com.cs."+CS.General_v3.Settings.Others.JAVASCRIPT_VERSION+".Controls.RedirectController('" + p.url + "');";
            CS.General_v3.Util.JSUtilOld.AddJSScriptToPage(js);
        }
    }
}
