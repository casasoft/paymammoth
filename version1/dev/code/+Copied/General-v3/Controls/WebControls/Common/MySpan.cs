﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CS.General_v3.Controls.WebControls;
using System.Web.UI;

namespace CS.General_v3.Controls.WebControls.Common
{
    public class MySpan : MyInnerHtmlControl
    {
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cssClass"></param>
        /// <param name="parentToAddTo">The parent control.  If specified, this control is added to the Controls collection of the parent</param>
        public MySpan(string cssClass = null, Control parentToAddTo = null)
            : base(  System.Web.UI.HtmlTextWriterTag.Span)
        {
            this.CssManager.AddClass(cssClass);
            if (parentToAddTo != null)
                parentToAddTo.Controls.Add(this);
        }
       

    }
}
