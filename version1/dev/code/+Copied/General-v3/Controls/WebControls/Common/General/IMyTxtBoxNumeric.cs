﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace CS.General_v3.Controls.WebControls.Common.General
{
    public interface IMyTxtBoxNumeric : IMyTxtBox<double>
    {
        double FormValueDbl { get; }
        double? FormValueDblNullable { get; }
        int FormValueInt { get; }
        int? FormValueIntNullable { get; }
        bool NegativeNumsOnly { get; set; }
        double? NumberFrom { get; set; }
        double? NumberTo { get; set; }
        bool PositiveNumsOnly { get; set; }
    }
}

