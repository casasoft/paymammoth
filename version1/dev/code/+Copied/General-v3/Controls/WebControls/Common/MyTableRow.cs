using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.General_v3.Controls.WebControls.Common
{

    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyTableRow runat=server></{0}:MyTableRow>")]

    public class MyTableRow :TableRow, IBaseWebControl
    {
        public MyTableRow()
        {
            this._functionality = new BaseWebControlFunctionality(this);
        }
        #region IBaseWebControl Members
        /// <summary>
        /// Adds a cell, and adds the controls to the cell
        /// </summary>
        /// <param name="controls">Controls to add</param>
        /// <returns></returns>
        public MyTableCell AddCell(string cssClass, params Control[] controls)
        {
            return addCell(cssClass, null, controls);
        }
        /// <summary>
        /// Adds an empty cell
        /// </summary>
        /// <param name="innerHtml">inner HTML of cell</param>
        /// <returns></returns>
        public MyTableCell AddCell()
        {
            return addCell(null, null);
        }
        /// <summary>
        /// Adds a cell, and sets the text as its innerHtml property
        /// </summary>
        /// <param name="innerHtml">inner HTML of cell</param>
        /// <returns></returns>
        public MyTableCell AddCell(string cssClass)
        {
            return addCell(cssClass, null);
        }
        /// <summary>
        /// Adds a cell, and sets the text as its innerHtml property
        /// </summary>
        /// <param name="innerHtml">inner HTML of cell</param>
        /// <returns></returns>
         public MyTableCell AddCell(string cssClass,string innerHtml)
         {
             return addCell(cssClass, innerHtml, null);
         }
        /// <summary>
         /// Adds a cell, and adds the controls to the cell
        /// </summary>
        /// <param name="innerHtml"></param>
        /// <param name="controls"></param>
        /// <returns></returns>
         private MyTableCell addCell(string cssClass, string innerHtml, params Control[] controls)
        {
            MyTableCell td = new MyTableCell();
            this.Cells.Add(td);
             
            if (controls != null && controls.Length > 0)
            {
                if (controls != null)
                {
                    foreach (var ctrl in controls)
                    {
                        if (ctrl != null)
                        {
                            td.Controls.Add(ctrl);
                        }
                    }
                }
                if (!string.IsNullOrEmpty(innerHtml))
                {
                    td.InnerHtml = innerHtml;
                }
            }
            else 
            {
                 td.InnerHtml = innerHtml;   
            }
            if (!string.IsNullOrEmpty(cssClass))
                td.CssManager.AddClass(cssClass);
            return td;
        }
        private BaseWebControlFunctionality _functionality = null;
        public CS.General_v3.Classes.CSS.CSSManagerForControl CssManager
        {
            get { return _functionality.CssManager; }
        }
        public new string CssClass
        {
            get
            {
                return _functionality.CssClass;
            }
            set
            {
                _functionality.CssClass = value;
                base.CssClass = _functionality.CssClass;
            }
        }


        #endregion

        #region IBaseWebControl Members


        public BaseWebControlFunctionality WebControlFunctionality
        {
            get
            {
                return _functionality;
            }
           
        }

        #endregion



        public string RollOverClass { get { return _functionality.RollOverClass; } set { _functionality.RollOverClass = value; } }
        public string StringID { get { return _functionality.StringID; } set { _functionality.StringID = value; } }
        public string OnClientBlur { get { return _functionality.OnClientBlur; } set { _functionality.OnClientBlur = value; } }
        public string OnClientClick { get { return _functionality.OnClientClick; } set { _functionality.OnClientClick = value; } }
        public string OnClientChange { get { return _functionality.OnClientChange; } set { _functionality.OnClientChange = value; } }
        public string OnClientLoad { get { return _functionality.OnClientLoad; } set { _functionality.OnClientLoad = value; } }



        #region IBaseWebControl Members


        public WebControl Control { get { return this; } }

        #endregion

        #region IBaseControl Members

        Control IBaseControl.Control { get { return this; } }

        BaseControlFunctionality IBaseControl.WebControlFunctionality { get { return _functionality; } }
        #endregion

/*
        public new TableCellCollection Cells
        {
            get
            {
                throw new NotSupportedException("Please use the indexor with this class or AddCell method");
            }

        }*/

        public MyTableCell this[int i]
        {
            get {
                return (MyTableCell)base.Cells[i];
            }
        }


    }
}
