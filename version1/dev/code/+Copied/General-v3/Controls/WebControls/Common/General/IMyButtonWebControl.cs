﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace CS.General_v3.Controls.WebControls.Common.General
{
    public interface IMyButtonWebControl : IMyBaseFormWebControl
    {
        bool ValidationDefault { get; set; }
        bool ShowProgressBar { get; set; }
        //void Render(HtmlTextWriter writer);
        object Tag2 { get; set; }
        object Tag3 { get; set; }
         string ConfirmMessage { get; set; }
         string ConfirmMessageHighlightRowClass { get; set; }
         string ConfirmMessageHighlightRowID { get; set; }
         bool NoValidation { get; set; }
         string PostBackUrl { get; set; }

         new MyButtonWebControlFunctionality WebControlFunctionality { get; }
        
    }
}
