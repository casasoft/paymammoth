﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace CS.General_v3.Controls.WebControls.Common.General
{
    public interface IMyTxtBoxText : IMyTxtBox<string>
    {
        bool IsAlphabetical { get; set; }
        bool IsEmail { get; set; }
        bool IsWebsite { get; set; }




    }
}
