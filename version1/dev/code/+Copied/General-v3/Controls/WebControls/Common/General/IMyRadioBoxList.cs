﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace CS.General_v3.Controls.WebControls.Common.General
{
    public interface IMyRadioBoxList : IMyFormWebControl
    {
        List<string> GetFormValues();
        string GetFormValue(int id);
        



    }
}
