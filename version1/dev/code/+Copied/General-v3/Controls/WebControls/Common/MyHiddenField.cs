using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common.General;

namespace CS.General_v3.Controls.WebControls.Common
{
    

    public class MyHiddenField : BaseFormWebControl, General.IMyFormWebControl
    {
       
        private General.MyFormWebControlFunctionality _myWebControl = null;
        

        public MyHiddenField(string id = null): base("input")
        {
            this.ID = id;
            _myWebControl = new CS.General_v3.Controls.WebControls.Common.General.MyFormWebControlFunctionality(this,ClientIDSeparator);
            this.Attributes["type"] = "hidden";
            _myWebControl.HasValidation = true;
            this.ValidationParams.validateEvenIfNotVisible = true;
        }


        protected override BaseWebControlFunctionality getWebControlFunctionality()
        {
            return new MyFormWebControlFunctionality(this, this.ClientIDSeparator);
        }
        
       
        protected override void Render(HtmlTextWriter writer)
        {
            this.Attributes["name"] = CS.General_v3.Util.Forms.GetFormVariableID(this, this.ClientIDSeparator);

            base.Render(writer);
        }
        public string Text
        {
            get
            {
                return this.Value;
            }
            set
            {
                this.Value = value;
            }
        }
        #region IMyWebControl Members

        public object Tag
        {
            get { return _myWebControl.Tag; }
            set { _myWebControl.Tag = value; }
        }

      



        #endregion
       
        public virtual void SetValue(object o)
        {
            this.Text = _myWebControl.SetValue(o);


        }
        #region IMyFormWebControl Members


       
        
        #region classes
       

        #endregion

        public string FormValue
        {
            get
            {
                return _myWebControl.FormValue;
            }
        }

        #endregion



        #region IMyFormWebControl Members

      

        #endregion



        #region IMyFormWebControl Members

       
        #endregion

        #region IMyWebControl Members



        public new WebControl Control
        {
            get { return this; }
        }

        #endregion


        #region IBaseControl Members


        public new MyFormWebControlFunctionality WebControlFunctionality
        {
            get { return (MyFormWebControlFunctionality)base.WebControlFunctionality; }
        }

        #endregion

        protected override object valueAsObject
        {
            get
            {
                return this.Value;
            }
            set
            {
                this.Value = (string)value;
            }
        }
        public new string Value
        {
            get 
            {
                return this.Attributes["value"];
            
            }
            set
            {
                this.Attributes["value"] = value;
            
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            _myWebControl.ThrowErrorIfIDIsNull();
            base.OnPreRender(e);
        }


        #region IMyBaseFormWebControl Members


        public new string ValidationGroup
        {
            get
            {
                return this.WebControlFunctionality.ValidationParams.validationGroup;
            }
            set
            {
                this.WebControlFunctionality.ValidationParams.validationGroup = Value;
            }
        }

        #endregion
    }
}
