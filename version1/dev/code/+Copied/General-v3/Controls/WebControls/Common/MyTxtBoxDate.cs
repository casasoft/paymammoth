using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common.General;

namespace CS.General_v3.Controls.WebControls.Common
{
    using Specialized.FormFieldsClasses;

    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyTxtBoxDate runat=server></{0}:MyTxtBoxDate>")]

    public class MyTxtBoxDate : MyTxtBox
    {

        public MyTxtBoxDate()
            : base()
        {
            
            _functionality.IsDate = true;
            
        }
        public FormFieldsValidationParams.SHOW_CALENDAR_TYPE JQueryUICalendarShowType
        {
            get { return _functionality.ValidationParams.jQueryUICalendarShowType; }
            set { _functionality.ValidationParams.jQueryUICalendarShowType = value; }
        }
        public bool JQueryUICalendarShowButtonPanel
        {
            get { return _functionality.ValidationParams.jQueryUICalendarShowButtonPanel; }
            set { _functionality.ValidationParams.jQueryUICalendarShowButtonPanel = value; }
        }
        public string JQueryUICalendarCustomContainerCSSClass
        {
            get { return _functionality.ValidationParams.jQueryUICalendarCustomContainerCSSClass; }
            set { _functionality.ValidationParams.jQueryUICalendarCustomContainerCSSClass = value; }
        }


            public new string GetFormValueAsStr()
        {
            string s = null;
            if (this.FormValue != null)
            {
                s = this.FormValue.Value.ToString("dd/MM/y");
                if (this.HasTime)
                    s += " " + this.FormValue.Value.ToString("HH:mm"); 
            }
            return s;
        }
        public MyTxtBoxDate(string ID, string Title, bool Required, string HelpMessage, DateTime? DateFrom, DateTime? DateTo, bool HasTime)
            : this(ID,Title,Required,HelpMessage,DateFrom,DateTo,HasTime,null)
        {

        }
        public MyTxtBoxDate(string ID, string Title, bool Required, string HelpMessage, DateTime? DateFrom, DateTime? DateTo, bool HasTime, DateTime? initValue)
            : base()
        {
            _functionality.IsDate = true;
            this.StringID = ID;
            this.Title = Title;
            this.ID = ID;
            this.DateFrom = DateFrom;
            this.DateTo = DateTo;
            this.HasTime = HasTime;
            this.Required = Required;
            this.HelpMsg = HelpMessage;
            SetValue(initValue);

        }

        public override object Value
        {
            get
            {
                string s = (string)base.Value;
                DateTime? date = null;

                if (!string.IsNullOrEmpty(s))
                    date = Util.Conversion.ToDateTime(s);

                return date;
            }
            set
            {
                

                SetValue(value);
                
            }
        }
        
        public override object FormValueObject
        {
            get
            {
                return this.FormValue;
            }
        }
      #region IMyTxtBoxDate Members
 /* 
        public string DateBeforeCtrlID
        {
            get
            {
                return _functionality.DateBeforeCtrlID;
                
            }
            set
            {
                _functionality.DateBeforeCtrlID = value;
            }
        }
        public CS.General_v3.Controls.WebControls.Common.MyTxtBoxDate DateBeforeCtrl
        {
            get
            {
                return _functionality.DateBeforeCtrl;
            }
            set
            {
                _functionality.DateBeforeCtrl = value;
            }
        }
        public string GetDateBeforeClientID()
        {
            return _functionality.GetDateBeforeClientID();
        }
        */

        public DateTime? DateFrom
        {
            get
            {
                return _functionality.DateFrom;
                
            }
            set
            {
                _functionality.DateFrom = value;
            }
        }

        public DateTime? DateTo
        {
            get
            {
                return _functionality.DateTo;
            }
            set
            {

                _functionality.DateTo = value;
            }
        }

        public new DateTime? FormValue
        {
            get { return _functionality.FormValueDateNullable; }
        }

        public bool HasTime
        {
            get
            {
                return _functionality.HasTime;
            }
            set
            {
                _functionality.HasTime = value;
            }
        }

        public string TimeFieldID
        {
            get
            {
                return _functionality.TimeFieldID;
            }
            set
            {
                _functionality.TimeFieldID = value;
            }
        }

        public IMyTxtBox<DateTime> TimeFieldCtrl
        {
            get
            {
                return _functionality.TimeFieldCtrl;
            }
            set
            {
                _functionality.TimeFieldCtrl = value;
            }
        }
        public string GetTimeFieldCtrlClientID()
        {
            if (TimeFieldCtrl != null)
                return TimeFieldCtrl.Control.ClientID;
            else
                return TimeFieldID;
        }
        #endregion

        public override Enums.DATA_TYPE DataType
        {
            get
            {
                return Enums.DATA_TYPE.DateTimeNullable;
            }
        }
    }
}
