﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace CS.General_v3.Controls.WebControls.Common
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyDiv runat=server></{0}:MyDiv>")]
    
    public class MyDiv : BaseWebControl
    {
        private Literal _text = new Literal();
        public MyDiv()
            : this("")
        {
            
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="cssClass"></param>
        /// <param name="parentToAddTo">The parent control.  If specified, this control is added to the Controls collection of the parent</param>
        public MyDiv(string text = null, string cssClass = null, Control parentToAddTo = null): base("div")
        {
            this.InnerHtml = text;
            this.CssManager.AddClass(cssClass);
            if (parentToAddTo != null)
                parentToAddTo.Controls.Add(this);
            Controls.Add(_text);
        }
        public string Text
        {
            get {
                return CS.General_v3.Util.Text.HtmlDecode(InnerHtml);
            }
            set { this.InnerHtml = CS.General_v3.Util.Text.HtmlEncode(value); }
        }


        public string InnerHtml
        {
            get
            {
                return _text.Text;
            }
            set
            {
                _text.Text = value;
            }
        }

    }
}
