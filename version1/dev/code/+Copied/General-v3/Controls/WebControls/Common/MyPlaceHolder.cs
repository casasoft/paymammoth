﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.CSS;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.Web.UI.WebControls ;
namespace CS.General_v3.Controls.WebControls.Common
{
    public class MyPlaceHolder : System.Web.UI.WebControls.PlaceHolder, IBaseControl
    {
        private BaseWebControlFunctionality _functionality = null;

        public MyPlaceHolder()
            : base()
        {
            init();
        }


        private void init()
        {
            
        }



        #region IBaseControl Members

        public Control Control
        {
            get { return this; }
        }

        public BaseControlFunctionality WebControlFunctionality
        {
            get { return _functionality; }
        }

        #endregion
    }
}
