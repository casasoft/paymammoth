using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.General_v3.Controls.WebControls.Common
{

    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyTxtBoxNumericDouble runat=server></{0}:MyTxtBoxNumericDouble>")]

    public class MyTxtBoxNumericDouble : MyTxtBoxNumeric
    {
        public new string GetFormValueAsStr()
        {
            string s = null;
            if (this.FormValue != null)
            {
                s = this.FormValue.Value.ToString("0.00");
            }
            return s;
        }
        public MyTxtBoxNumericDouble()
            : base()
        {
           
            
        }

        public MyTxtBoxNumericDouble(string ID, string Title, bool Required, string HelpMessage, double? numFrom, double? numTo, NUMERIC_RANGE allowedNumbers)
            : this(ID,Title,Required,HelpMessage,numFrom,numTo,allowedNumbers,
            null,null)
        {

        }
        //

        public MyTxtBoxNumericDouble(string ID, string Title, bool Required, string HelpMessage, double? numFrom, double? numTo, NUMERIC_RANGE allowedNumbers,
            int? width, double? value)


            :base()
        {
            this.ID = this.StringID = ID;
            this.Title = Title;
            this.NumberFrom = numFrom;
            this.NumberTo = numTo;
            this.AllowedNumbers = allowedNumbers;
            if (width != null)
                this.Width = width.Value;
            this.SetValue(value);

            this.Required = Required;
            this.HelpMsg = HelpMessage;

        }
        public override Enums.DATA_TYPE DataType
        {
            get
            {
                return Enums.DATA_TYPE.Double;
            }
        }
        public override object FormValueObject
        {
            get
            {
                return this.FormValue;
                
            }
        }
    }
}
