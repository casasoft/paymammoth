﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;
namespace CS.General_v3.Controls.WebControls.Common
{
    [ToolboxData("{0}:MyBr runat='server' />")]
    public class MyBr : WebControl
    {
        public MyBr()
            : base("br")
        {

        }
        public MyBr(bool clearBoth)
            : this()
        {
            if (clearBoth)
            {
                this.Style.Add("clear", "both");
            }
        }

    }
}
