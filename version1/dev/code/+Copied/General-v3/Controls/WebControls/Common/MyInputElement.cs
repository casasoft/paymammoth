using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace CS.General_v3.Controls.WebControls.Common
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyInputElement runat=server></{0}:MyInputElement>")]
    public class MyInputElement : BaseWebControl
    {
        public MyInputElement()
            : base(HtmlTextWriterTag.Input)
        {

        }
        
    }
}
