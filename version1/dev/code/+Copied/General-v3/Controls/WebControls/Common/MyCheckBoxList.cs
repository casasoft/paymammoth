using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using CS.General_v3.Controls.WebControls.Common.General;


namespace CS.General_v3.Controls.WebControls.Common
{

    [ToolboxData("<{0}:MyCheckBoxList runat=server></{0}:MyCheckBoxList>")]
    public class MyCheckBoxList : BaseFormWebControl, General.IMyCheckBoxList
    {
        /// <summary>
        /// Controls to add after each checkbox.
        /// </summary>
        public List<Control> ControlsToAppendToCheckboxCells { get; private set; }


        public override string GetFormValueAsStr()
        {
            return CS.General_v3.Util.Text.AppendStrings(",", this.FormValue);
        }
        private Table _table = new Table();

        private MyHiddenField _hiddenFieldValidation = new MyHiddenField();

        private List<MyCheckBox> _checkboxes = new List<MyCheckBox>();
        private List<string> _checkBoxValues = new List<string>();
        public override string ID
        {
            get
            {
                return base.ID;
            }
            set
            {
                base.ID = value;
            }
        }
       
        //protected General.MyFormWebControlFunctionality _functionality = null;



        private List<ListItem> _items = new List<ListItem>();
        public List<ListItem>  Items
        {
            get
            {
                return _items;
            }
        }
        public RepeatDirection RepeatDirection { get; set; }



        public int CellPadding { get { return _table.CellPadding; } set { _table.CellPadding = value; } }
        public int CellSpacing { get { return _table.CellSpacing; } set { _table.CellSpacing = value; } }

        public RepeatLayout RepeatLayout { get; set; }
        public int RepeatColumns { get; set; }
        public MyCheckBoxList()
            : base()
        {
            
            ControlsToAppendToCheckboxCells = new List<Control>();
            //this.Items = new ListItemCollection();
            this.CssClass = "checkbox-list";
            this.RepeatColumns = 3;
            this.RepeatDirection = RepeatDirection.Horizontal;
            this.WebControlFunctionality.AddFieldJS = false;
            init();
        }

        public MyCheckBoxList(string ID, string Title)
            : this()
        {

            
            this.ID = this.StringID = ID;
            this.Title = Title;

        }
        private void init()
        {
            //_functionality = new CS.General_v3.Controls.WebControls.Common.General.MyFormWebControlFunctionality(this, ClientIDSeparator);
            this.HasValidation = true;
        }

        protected override BaseWebControlFunctionality getWebControlFunctionality()
        {
            return new MyFormWebControlFunctionality(this, this.ClientIDSeparator);
        }
        
        
        public int CheckBoxCellPadding { get; set; }
        public int CheckBoxCellSpacing { get; set; }
        public int? CheckBoxMarginText { get; set; }
        private string outputNVColl(NameValueCollection coll)
        {
            string str = "";
            IEnumerator enumerator = coll.Keys.GetEnumerator();
            while (enumerator.MoveNext())
            {
                string key = Convert.ToString(enumerator.Current);
                str += " " + key + "=\"" + coll[key] + "\"";
            }
            return str;
        }

        

        private void outputTable()
        {
            if (RepeatColumns == 0) RepeatColumns = 4;
            List<ListItem> items = new List<ListItem>();
            items.AddRange(this.Items);
            List<List<ListItem>> subsets = CS.General_v3.Util.ListUtil.SplitListIntoMultipleLists(items, this.RepeatColumns, 
                RepeatDirection == RepeatDirection.Vertical);

            //HtmlTable tbl = new HtmlTable();
            _table.CssClass = this.CssClass;
            int count = 0;
            for (int i = 0; i < subsets.Count; i++)
            {
                TableRow tr = new TableRow();
                _table.Rows.Add(tr);
                for (int j = 0; j < subsets[i].Count; j++)
                {
                    //Cells
                    ListItem item = subsets[i][j];
                    
                    TableCell td = new TableCell();
                    tr.Controls.Add(td);
                    if (item != null)
                    {
                        MyDiv divCheckbox = new MyDiv();
                        divCheckbox.CssManager.AddClass("checkbox-container");
                        td.Controls.Add(divCheckbox);
                        CS.General_v3.Controls.WebControls.Common.MyCheckBox chk = new MyCheckBox();
                        //Keep this!
                        chk.Attributes["chkValue"] = item.Value;
                        chk.CellPadding = CheckBoxCellPadding;
                        chk.CellSpacing = CheckBoxCellSpacing;
                        chk.MarginText = CheckBoxMarginText;
                        
                        chk.ID = this.ID + "_" + count;
                        //chk.ID = (this.ClientID + "_" + count).Replace("_", "$");
                        chk.Text = item.Text;
                        if (item.Selected == true)
                        {
                            int k = 5;
                        }
                        chk.Checked = item.Selected;
                        chk.Enabled = item.Enabled;

                        

                        // chk.Attributes = item.Attributes;
                        //chk.ID = count.ToString();
                        divCheckbox.Controls.Add(chk);
                        //chk.ID = CS.General_v3.Util.Forms.GetFormVariableID(chk, ClientIDSeparator) + "$" + count;
                        
                        if (ControlsToAppendToCheckboxCells != null && count < ControlsToAppendToCheckboxCells.Count)
                        {
                            if (ControlsToAppendToCheckboxCells[count] != null)
                            {
                                divCheckbox.Controls.Add(ControlsToAppendToCheckboxCells[count]);
                            }
                        }
                        _checkboxes.Add(chk);
                        _checkBoxValues.Add(item.Value);
                    }
                    else
                    {
                        td.Text = "&nbsp;";
                    }
                    count++;

                }



            }

            //tbl.RenderControl(writer);
            this.Controls.Add(_table);
            //writer.Write(CS.General_v3.Util.ControlUtil.RenderControl(tbl,false));
        }
        
        private void initRequiredJS()
        {
            if (this.Required)
            {
                _hiddenFieldValidation.ValidationGroup = this.ValidationGroup;
                _hiddenFieldValidation.Required = this.Required;
                _hiddenFieldValidation.Title = this.Title;
                this.Controls.Add(_hiddenFieldValidation);
                string js = "new js.com.cs."+CS.General_v3.Settings.Others.JAVASCRIPT_VERSION+".UI.FormsOld.CheckBoxListRequired('" + this.ClientID + "', '" + this._hiddenFieldValidation.ClientID + "');";
                CS.General_v3.Util.JSUtilOld.AddJSScriptToPage(js);
                //js = CS.General_v3.Util.JSUtil.MakeJavaScript(js, true);
                //this.Controls.Add(new Literal() { Text = js });
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            
            outputTable();
            initRequiredJS();
            /*_hiddenFieldValidation.Required = this.Required;
            _hiddenFieldValidation.ValidationGroup = this.ValidationGroup;
            _hiddenFieldValidation.Title = this.Title;
            this.Controls.Add(_hiddenFieldValidation);*/
            base.OnLoad(e);
        }


        #region IMyCheckBoxList Members
        /// <summary>
        /// The value retrieved from the posted Form
        /// </summary>
       
        public List<string> FormValue
        {
            get
            {
                return GetFormValues();
            }
        }
        public override object FormValueObject
        {
            get
            {
                return this.FormValue;
            }
        }
        protected override object valueAsObject
        {
            get
            {
                return this.Value;
            }
            set
            {
                this.Value = value;
            
            }
        }
        public new object Value
        {
            get
            {
                List<string> values = new List<string>();
                for (int i = 0; i < this.Items.Count; i++)
                {
                    ListItem item = this.Items[i];
                    if (item.Selected)
                    {
                        if (String.IsNullOrEmpty(item.Value))
                        {
                            values.Add(item.Text);
                        }
                        else
                        {
                            values.Add(item.Value);
                        }
                    }
                }
                return values;
            }
            set
            {

                return;
                
                //for (int i = 0; i< Items.Count; i++)
                //{
                //    Items[i].Selected = false;
                //}
                //if (value != null)
                //{
                //    IList list;
                //    if (value is IList)
                //    {
                //        list = (IList)value;
                //    }
                //    else 
                //    {
                //        list = new List<object>();
                //        list.Add(value);
                //    }
                //    for (int i = 0; i < list.Count; i++)
                //    {
                //        object obj = list[i];
                //        string val = obj.ToString();
                //        for (int j = 0; j < this.Items.Count; j++)
                //        {
                //            ListItem item = this.Items[j];
                //            if (!String.IsNullOrEmpty(item.Value) && val == item.Value || (String.IsNullOrEmpty(item.Value) && val == item.Text))
                //            {
                //                item.Selected = true;
                //                break;
                //            }
                //        }

                //    }
                //}
                
                
            }
        }
        public List<string> GetFormValues()
        {
            List<string> values = new List<string>();
            for (int i = 0; i < _checkboxes.Count; i++)
            {
                MyCheckBox chk = _checkboxes[i];
                if (chk.FormValue)
                {
                    values.Add(_checkBoxValues[i]);
                }
            }
            return values;

            /*List<string> list = new List<string>();
            for (int i = 0; i < Items.Count; i++)
            {

                string s = GetFormValue(i);
                if (!string.IsNullOrEmpty(s))
                    list.Add(s);
            }
            return list;*/
        }
        /// <summary>
        /// The form value of the check box item.
        /// </summary>
        /// <param name="id">ID of checkbox</param>
        /// <returns>empty string if not checked, value if checked</returns>
        public string GetFormValue(int index)
        {
            return _checkboxes[index].Value.ToString();
        }

        #endregion
        #region IMyFormWebControl Members
       

        public virtual void SetValue(object o)
        {
            /*this.Text = */this.WebControlFunctionality.SetValue(o);
            //22/04/09 Removed by Mark


        }

        #endregion

        #region IMyWebControl Members

        public object Tag
        {
            get { return this.WebControlFunctionality.Tag; }
            set { this.WebControlFunctionality.Tag = value; }
        }


        

        #endregion

        public new WebControl Control
        {
            get { return this; }
        }



        #region IMyBaseFormWebControl Members



        #endregion

        #region IBaseControl Members


        public new MyFormWebControlFunctionality WebControlFunctionality
        {
            get { return (MyFormWebControlFunctionality)base.WebControlFunctionality; }
        }

        #endregion

        protected override void OnPreRender(EventArgs e)
        {
            this.WebControlFunctionality.ThrowErrorIfIDIsNull();
            base.OnPreRender(e);
        }
    }
}
