using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.General_v3.Controls.WebControls.Common
{

    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyTxtBoxWebsite runat=server></{0}:MyTxtBoxWebsite>")]

    public class MyTxtBoxWebsite : MyTxtBoxText
    {
        private bool _prefixWithHTTP = false;

        public bool IsWebsite
        {
            get
            {
                return this.ValidationParams.isWebsite;
            }
            set
            {
                this.ValidationParams.isWebsite = value;
            }
        }

        public MyTxtBoxWebsite() :
            base()
        {
            _functionality.IsWebsite = true;
            
        }
        public MyTxtBoxWebsite(string ID, string Title, bool Required, string HelpMessage, int? width, string text)
            : base ()
        {
            if (width != null)
                this.Width = width.Value;
            this.ID = this.StringID = ID;
            this.Title = Title;
            this.Text = text;
            _functionality.IsWebsite = true;
            this.Required = Required;
            this.HelpMsg = HelpMessage;
        }
        public MyTxtBoxWebsite(string ID, string Title, bool required, string HelpMessage) :
            this(ID,Title,required,HelpMessage,null,null)
        {
            

        }
        /// <summary>
        /// Prefixes the value of the text box with 'http://' if the value is empty
        /// </summary>
        public bool PrefixWithHttpIfEmpty
        {
            get
            {
                return _prefixWithHTTP;
            }
            set
            {
                _prefixWithHTTP = value;
                if (value && String.IsNullOrEmpty(this.Text))
                {
                    this.Text = "http://";
                }

            }
        }

    }
}
