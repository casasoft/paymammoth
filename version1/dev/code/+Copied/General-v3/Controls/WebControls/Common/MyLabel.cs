﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;
namespace CS.General_v3.Controls.WebControls.Common
{
    public class MyLabel : BaseWebControl
    {
        
        
        /// <summary>
        /// Deprecated.  Only for compatibility purposes
        /// </summary>
        public Control ForControl { get; set; }

        private Literal _innerHtml = new Literal();
        public MyLabel()
            : base("label")
        {
            init();
            
            
        }
        public MyLabel(string text)
            : this()
        {
            init();
            _innerHtml.Text = text;
        }
        public MyLabel(General.IMyFormWebControl control)
            : base("label")
        {
            init();
            if (!string.IsNullOrEmpty(control.Title))
            {
                _innerHtml.Text = control.Title + ": ";
            }
            ForControl = control.Control;
        }
        private void init()
        {
            Controls.Add(_innerHtml);
        }
        public string InnerHtml { get { return _innerHtml.Text; } set { _innerHtml.Text = value; } }
        public string Text { get { return InnerHtml; } set { InnerHtml = value; } }
        protected override void OnLoad(EventArgs e)
        {
            if (ForControl != null)
            {
                this.Attributes["for"] = this.ForControl.ClientID;
            }
            base.OnLoad(e);
        }

    }
}
