﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI;

namespace CS.General_v3.Controls.WebControls.Common
{
    public class MyHeading : BaseWebControl
    {
        private HtmlGenericControl _heading;
        private Literal _text = new Literal();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="headingType"></param>
        /// <param name="text"></param>
        /// <param name="cssClass"></param>
        /// <param name="parentControlToAddTo">The parent control.  If specified, this control is added to the Controls collection of the parent</param>
        public MyHeading(int headingType, string text, string cssClass = null, Control parentControlToAddTo = null): base("h"+headingType)
        {
            if (parentControlToAddTo != null)
                parentControlToAddTo.Controls.Add(this);
            this.CssManager.AddClass(cssClass);
            this.Controls.Add(_text);
            _text.Text = text;
        }
        public string InnerHtml
        {
            get
            {
                return _text.Text;
            }
            set
            {
                _text.Text = value;
            }
        }

    }
}
