using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
namespace CS.General_v3.Controls.WebControls.Common
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyPopUpMenu runat=server></{0}:MyPopUpMenu>")]

    public class MyPopUpMenu : HiddenField
    {
        public enum POPUP_DIRECTION
        {
            Left = 1,
            Right = 2,
            Top = 3,
            Bottom = 4
        }

        private List<ParentChildItem> _items = new List<ParentChildItem>();
        private int _showDelay = 50;
        private int _hideDelay = 50;
        private POPUP_DIRECTION _popUpDirection = POPUP_DIRECTION.Right;
        private int _spaceX = 1;
        private int _spaceY = 1;
        

        [Category("Settings")]
        [DefaultValue(50)]
        public int ShowDelay
        {
            get
            {
                return _showDelay;
            }
            set
            {
                _showDelay = value;
                if (_showDelay % 50 > 25)
                {
                    _showDelay -= _showDelay % 50;
                    _showDelay += 50;
                }

            }
        }

        [Category("Settings")]
        [DefaultValue(50)]
        public int HideDelay
        {
            get
            {
                return _hideDelay;
            }
            set
            {
                _hideDelay = value;
                if (_hideDelay % 50 > 25)
                {
                    _hideDelay -= _hideDelay % 50;
                    _hideDelay += 50;
                }

            }
        }

        [Category("Settings")]
        public POPUP_DIRECTION PopUpDirection
        {
            get
            {
                return _popUpDirection;
            }
            set
            {
                _popUpDirection = value;
            }
        }

        [Category("Settings")]
        [DefaultValue(1)]
        public int SpaceX
        {
            get
            {
                return _spaceX;
            }
            set
            {
                _spaceX = value;
            }
        }


        [Category("Settings")]
        [DefaultValue(1)]
        public int SpaceY
        {
            get
            {
                return _spaceY;
            }
            set
            {
                _spaceY = value;
            }
        }

        public void AddPopupMenu(HtmlControl item, HtmlControl div, HtmlControl parent)
        {
            AddPopupMenu(item, item.ID, div, div.ID,parent, parent.ID, ParentChildItem.POPUP_EVENTTYPE.MouseClick);
        }

        public void AddPopupMenu(HtmlControl item, HtmlControl div)
        {
            AddPopupMenu(item, item.ID,div, div.ID,null,"", ParentChildItem.POPUP_EVENTTYPE.MouseClick);
        }

        public void AddPopupMenu(HtmlControl item, string itemID, 
                                HtmlControl div, string divID,
                                HtmlControl parent, string parentID,
                                ParentChildItem.POPUP_EVENTTYPE eventType)
        {
            string sevent = "";
            if (itemID == "") itemID = item.ID;
            if (divID == "") divID = div.ID;
            if (parent != null && parentID == "") parentID = parent.ID;

            //HtmlControl ctrl = (HtmlControl)FindControl(x.itemID);
            switch (eventType)
            {
                case ParentChildItem.POPUP_EVENTTYPE.MouseClick:
                    sevent = "onmouseclick";
                    break;
                case ParentChildItem.POPUP_EVENTTYPE.MouseDown:
                    sevent = "onmousedown";
                    break;
                case ParentChildItem.POPUP_EVENTTYPE.MouseUp:
                    sevent = "onmouseup";
                    break;
            }

            //sevent = x.EventType.ToString();
            item.Attributes[sevent] = ID + ".mouseClick('" + itemID + "','" + divID + "');";
            item.Attributes["onmouseout"] = ID + ".mouseOut('" + itemID + "','" + divID + "');";
            item.Attributes["onmouseover"] = ID + ".mouseOver('" + itemID + "','" + divID + "');";

            div.Attributes["onmouseout"] = ID + ".mouseOut('" + itemID + "','" + divID + "');";
            div.Attributes["onmouseover"] = ID + ".mouseOver('" + itemID + "','" + divID + "');";
            div.Style["visibility"] = "hidden";
            div.Style["position"] = "absolute";
            div.Style["z-index"] = "999";
            _items.Add(new ParentChildItem(parentID, itemID, divID, eventType));

                    
            

        }

        public override void RenderControl(HtmlTextWriter writer)
        {
            HtmlForm frm = null;
            string JS = "";
            
            JS += "var " + ID + " = new popUpMenu('" + ID + "');";
                       

            JS += ID + ".spaceX = " + SpaceX + ";";
            JS += ID + ".spaceY = " + SpaceX + ";";
            JS += ID + ".showDelay = " + ShowDelay + ";";
            JS += ID + ".hideDelay = " + ShowDelay + ";";
            string slideTo = "";
            switch (PopUpDirection)
            {
                case POPUP_DIRECTION.Bottom:
                    slideTo = "bottom";
                    break;
                case POPUP_DIRECTION.Left:
                    slideTo = "left";
                    break;
                case POPUP_DIRECTION.Right:
                    slideTo = "right";
                    break;
                case POPUP_DIRECTION.Top:
                    slideTo = "top";
                    break;
            }
            JS += ID + ".slideTo = '" + slideTo + "';";

            if (Page != null)
            {
                frm = Page.Form;
            }
            if (frm != null)
            {
                foreach (ParentChildItem x in _items)
                {
                    if (x.Parent != null)
                        JS += ID + ".setParent('" + x.Parent + "','" + x.Item + "');";
                }
                string txt;

                txt = "<script type=\"text/javascript\">\n" +
                            JS +
                            "\n</script>";
                txt = txt.Replace(";", ";\n");
                txt = txt.Replace("}", "}\n");
                txt = txt.Replace("{", "{\n");
                writer.Write(txt);

            }

        }


    }

}
