﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using CS.General_v3.Classes.CSS;

namespace CS.General_v3.Controls.WebControls.Common
{
   
    public class MyListItem : BaseWebControl
    {

        public MyListItem(string txt = null, string css = null)
            : base(HtmlTextWriterTag.Li)
        {
            _lit = new Literal();
            this.Text = txt;
            this.CssClass = css;
        }
        public MyListItem():base(  HtmlTextWriterTag.Li)
        {
            _lit = new Literal();
        }
        //public new string CssClass
        //{
        //    get
        //    {
        //        base.css
        //        return this.Attributes["class"];
        //    }
        //    set
        //    {
        //        this.Attributes["class"] = value;
        //    }
        //}
        private Literal _lit = null;
        public string Text
        {
            get
            {
                return _lit.Text;
            }
            set
            {
                _lit.Text = value;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            this.Controls.Add(_lit);
            base.OnLoad(e);
        }
        

    }
}
