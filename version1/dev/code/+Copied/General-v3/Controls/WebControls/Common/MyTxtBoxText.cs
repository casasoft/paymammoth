using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.General_v3.Controls.WebControls.Common
{

    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyTxtBoxText runat=server></{0}:MyTxtBoxText>")]

    public abstract class MyTxtBoxText : MyTxtBox
    {
         public static MyTxtBoxText CreateTxtBoxBasedOnStringDataType(CS.General_v3.Enums.STRING_DATA_TYPE dataType)
        {
            switch (dataType)
            {
                case Enums.STRING_DATA_TYPE.Email: return new MyTxtBoxEmail();
                case Enums.STRING_DATA_TYPE.MultiLine: return new MyTxtBoxTextMultiLine();
                case Enums.STRING_DATA_TYPE.Password: return new MyTxtBoxTextPassword();
                case Enums.STRING_DATA_TYPE.SingleLine: return new MyTxtBoxTextSingleLine();
                case Enums.STRING_DATA_TYPE.Website: return new MyTxtBoxWebsite();

            }
            throw new InvalidOperationException("Invalid string data type");
            
        }
        public MyTxtBoxText():
            base()
        {
            
        }
        public new string GetFormValueAsStr()
        {
            return this.FormValue;
        }
    }
}
