using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.General_v3.Controls.WebControls.Common
{

    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyTxtBoxEmail runat=server></{0}:MyTxtBoxEmail>")]

    public class MyTxtBoxEmail : MyTxtBoxText
    {
        public MyTxtBoxEmail() :
            base()
        {
            _functionality.IsEmail = true;
            
            
        }
        public MyTxtBoxEmail(string ID, string Title, bool Required, string HelpMessage) :
            this(ID,Title,Required,HelpMessage,null,null)
        {

        }
        public MyTxtBoxEmail(string ID, string Title, bool Required, string HelpMessage, int? fieldWidth, string text) :
            base()
        {
            if (fieldWidth != null)
                this.Width = fieldWidth.Value;
            this.SetValue(text);
            this.ID = this.StringID = ID;
            this.Title = Title;
            this.Required = Required;
            this.HelpMsg = HelpMessage;
            _functionality.IsEmail = true;


        }

    }
}
