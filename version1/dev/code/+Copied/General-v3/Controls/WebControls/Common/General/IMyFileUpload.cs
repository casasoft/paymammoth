﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace CS.General_v3.Controls.WebControls.Common.General
{
    public interface IMyFileUpload : IMyFormWebControl
    {
        bool IsImage { get; set; }
        int Columns { get; set; }
        string GetExtension();
        string GetFilenameOnly();
        void SaveAs(string path);
        string SaveTemporary();
        System.IO.Stream UploadedFileStream { get; }
        List<string> FileExtensionsAllowed {get;set;}
        string FileExtensionsAllowedStr {get;set;}
        byte[] FileBytes {get;}
        System.IO.Stream FileContent {get;}



    }
}

