﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace CS.General_v3.Controls.WebControls.Common.General
{
    public interface IMyTxtBoxDate : IMyTxtBox<DateTime>
    {
        CS.General_v3.Controls.WebControls.Common.MyTxtBoxDate DateBeforeCtrl { get; set; }
        string DateBeforeCtrlID { get; set; }
        string GetDateBeforeClientID();
        DateTime? DateFrom { get; set; }
        DateTime? DateTo { get; set; }
        DateTime FormValueDate { get; }
        DateTime? FormValueDateNullable { get; }
        bool HasTime { get; set; }
        string TimeFieldID { get; set; }
        IMyTxtBox<DateTime> TimeFieldCtrl { get; set; }
        string GetTimeFieldCtrlClientID();



    }
}
