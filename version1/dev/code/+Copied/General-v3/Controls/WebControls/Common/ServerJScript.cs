using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace CS.General_v3.Controls.WebControls.Common
{

    [ToolboxData("<{0}:ServerJScript runat=server></{0}:ServerJScript>")]

    public class ServerJScript : Control
    {
        public ServerJScript()
        {
            this.ResolveClientURLEnabled = true;
        }
        private string _src = string.Empty;
        private string _language = "javascript";
        private string _type = "text/javascript";
        private bool _Defer = false;
        private bool _CommentForIE7 = false;
        private string HTML_CODE = "<script language=\"{0}\" src=\"{1}\" type=\"{2}\"></script>";

        public bool Defer
        {
            get { return _Defer; }
            set { _Defer = value;}

        }
        public bool CommentForIE7
        {
            get
            {
                return _CommentForIE7;
            }
            set
            {
                _CommentForIE7 = value;
            }
        }
        public string src
        {
            get { return _src; }
            set { _src = value; }
        }

        public string language
        {
            get { return _language; }
            set { _language = value; }
        }

        public string type
        {
            get { return _language; }
            set { _language = value; }
        }

        protected bool InDesignMode
        {
            get
            {
                return (System.Web.HttpContext.Current == null);
            }
        }
        public bool ResolveClientURLEnabled { get; set; }
        protected override void Render(HtmlTextWriter writer)
        {
            
            if (!InDesignMode)
            {

                if (!_src.ToLower().Contains("http://") )
                {
                    if ((ResolveClientURLEnabled && !File.Exists(CS.General_v3.Util.PageUtil.MapPath(ResolveClientUrl(_src)))) ||
                        (!ResolveClientURLEnabled && !File.Exists(CS.General_v3.Util.PageUtil.MapPath(_src))))
                    {
                        Exception ex = new Exception("Include file does not exist! [" + _src + "]");
                        throw ex;
                    }
                }
                string sDefer = "";
                if (Defer)
                    sDefer = " defer=\"defer\" ";
                HTML_CODE = "";
                if (CommentForIE7)
                    HTML_CODE += "\r\n<!--[if lt IE 7]>\r\n";
                HTML_CODE += "<script" + sDefer + " language=\"{0}\" src=\"{1}\" type=\"{2}\"></script>";
                if (CommentForIE7)
                    HTML_CODE += "\r\n<![endif]-->\r\n";
                writer.Write(string.Format(HTML_CODE, _language, ResolveClientUrl(_src), _type));
                
            }
            else
            {
                writer.Write("JavaScript::{" + _src + "}");
            }
        }
    }

}
