using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common.General;
using CS.General_v3.JavaScript.Data;

namespace CS.General_v3.Controls.WebControls.Common
{
    using JavaScript.Data;

    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyTxtBoxTextPhoneNumber runat=server></{0}:MyTxtBoxTextPhoneNumber>")]

    public class MyTxtBoxTextPhoneNumber : BaseFormWebControl
    {

        public class MyTxtBoxTextPhoneNumberJSParams : JavaScriptObject
        {
            public string internationalCountryCodeSuffix = "-";
            public string internationalCountryCodePrefix = "+";

        }

        public class FUNCTIONALITY
        {
            public List<int> InternationalCountryCallingCodes { get; set; }

            public string CountryCodeSuffix { get; set; }

            private string _countryCodePrefix = "+";

            public string CountryCodePrefix
            {
                get { return _countryCodePrefix; }
                set {
                    _countryCodePrefix = value;
                    /*
                    for (int i = 0; i < this.InternationalCountryCodes.Count; i++)
                    {
                        this.InternationalCountryCodes[i].Text = value + this.InternationalCountryCodes[i].Value;
                    }*/
                }
            }




            public string InitialValueInternationalCountryCode { get; set; }
            public string InitialValuePhoneNumber { get; set; }

            public int MinLength { get; set; }
            public int MaxLength { get; set; }

            public MyDropDownList CmbInternationalCodes { get { return _control._cmbInternationalCodes; } }
            public MyTxtBoxNumericInteger TxtPhoneNumber { get { return _control._txtPhoneNumber; } }

            private MyTxtBoxTextPhoneNumber _control;
            public FUNCTIONALITY(MyTxtBoxTextPhoneNumber control)
            {
                _control = control;
                this.CountryCodeSuffix = "-";
                this.InternationalCountryCallingCodes = CS.General_v3.Util.Data.GetCountryCallingCodesIntegers();
                this.CountryCodePrefix = "+";
                MinLength = 5;
                MaxLength = 15;
            }
            
        }

        public FUNCTIONALITY Functionality { get; private set; }

        protected MyDropDownList _cmbInternationalCodes;
        protected MyTxtBoxNumericInteger _txtPhoneNumber;

        public MyTxtBoxTextPhoneNumber()
            : base()
        {
            this.Functionality = getFunctionality();

            _cmbInternationalCodes = new MyDropDownList()
            {
                ID = "cmbIntCodes"
            };
            _txtPhoneNumber = new MyTxtBoxNumericInteger()
            {
                ID = "txtPhoneNum"
            };


            this.CssManager.AddClass("form-field-phone-container");
            createControls();
            this.WebControlFunctionality.JSFieldClassName = "FieldPhoneNumber";// this is not a normal field 
        }

        protected FUNCTIONALITY getFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private void createControls()
        {
            _cmbInternationalCodes.Value = this.Functionality.InitialValueInternationalCountryCode;
            _txtPhoneNumber.Value = this.Functionality.InitialValuePhoneNumber;

            _txtPhoneNumber.ValidationParams.minLength = this.Functionality.MinLength;
            _txtPhoneNumber.ValidationParams.maxLength = this.Functionality.MaxLength;

            //_txtPhoneNumber.WebControlFunctionality.AddFieldJS = _cmbInternationalCodes.WebControlFunctionality.AddFieldJS = false; // dont add separate validation
            _txtPhoneNumber.ValidationParams.doNotValidateOnBlur = _cmbInternationalCodes.DoNotValidateOnBlur = true;
            _txtPhoneNumber.ValidationParams.hasValidation = _cmbInternationalCodes.ValidationParams.hasValidation = false;
        }
        private void createCountryCodesListItemCollection()
        {
            _cmbInternationalCodes.Items.Clear();
            for (int i = 0; i < this.Functionality.InternationalCountryCallingCodes.Count; i++)
            {
                ListItem item = new ListItem()
                {
                    Text = this.Functionality.CountryCodePrefix + this.Functionality.InternationalCountryCallingCodes[i],
                    Value = this.Functionality.InternationalCountryCallingCodes[i].ToString()
                };
                _cmbInternationalCodes.Items.Add(item);
            }
        }
        private int? parseInternationalCountryCode(string code)
        {
            if (!string.IsNullOrEmpty(code))
            {
                if (code.StartsWith(this.Functionality.CountryCodePrefix))
                {
                    code = code.Substring(this.Functionality.CountryCodePrefix.Length);
                }
                if (CS.General_v3.Util.NumberUtil.IsNumeric(code))
                {
                    return int.Parse(code);
                }
            }
            return null;
        }
        private void showControls()
        {
            createCountryCodesListItemCollection();
            this.Controls.Add(_cmbInternationalCodes);
            this.Controls.Add(_txtPhoneNumber);

            int? initialValue = parseInternationalCountryCode(this.Functionality.InitialValueInternationalCountryCode);
            if (initialValue.HasValue) {
                _cmbInternationalCodes.Value = initialValue.Value.ToString();
            }
        }
        private void passJsParams()
        {
            this.WebControlFunctionality.JSExtraParams = new MyTxtBoxTextPhoneNumberJSParams()
            {
                internationalCountryCodeSuffix = this.Functionality.CountryCodeSuffix
            };
        }
        protected override void OnLoad(EventArgs e)
        {
            this.passJsParams();
            showControls();
            base.OnLoad(e);
            
        }

        public override object FormValueObject
        {
            get
            {
                return _cmbInternationalCodes.FormValue + this.Functionality.CountryCodeSuffix + _txtPhoneNumber.FormValue;
            }
        }

        public int FormValueInternationalCountryCode
        {
            get
            {
                return int.Parse(_cmbInternationalCodes.FormValue);
            }
        }
        public long? FormValuePhoneNumber
        {
            get
            {
                return _txtPhoneNumber.FormValue;
            }
        }
        public string Value
        {
            get
            {
                return this._cmbInternationalCodes.Value + this.Functionality.CountryCodeSuffix + _txtPhoneNumber.Value;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    string[] parts = value.Split(this.Functionality.CountryCodeSuffix.ToCharArray());
                    string intCode = parts[0];
                    string phoneNum = parts[1];
                    
                    int? nIntCode = parseInternationalCountryCode(intCode);
                    if (nIntCode.HasValue)
                    {
                       
                        _cmbInternationalCodes.Value = nIntCode.ToString();   
                    }
                    {   
                        _txtPhoneNumber.Value = phoneNum;
                    }
                    this.Functionality.InitialValueInternationalCountryCode = intCode;
                    this.Functionality.InitialValuePhoneNumber = phoneNum;

                }
                else
                {
                    _cmbInternationalCodes.SelectedIndex = 0;
                    _txtPhoneNumber.Value = "";
                }

            }


        }
        protected override object valueAsObject
        {
            get
            {
                return this.Value;
            }
            set
            {
                if (value == null) value = "";
                this.Value = value.ToString();
                
            }
        }

        public override string GetFormValueAsStr()
        {
            return this.Functionality.CountryCodePrefix + _cmbInternationalCodes.FormValue + this.Functionality.CountryCodeSuffix + _txtPhoneNumber.FormValue;
        }
        public override int? GetFormValueAsIntNullable()
        {
            string s = _cmbInternationalCodes.FormValue + _txtPhoneNumber.FormValue;
            int num;
            if (int.TryParse(s, out num))
            {
                return num;
            }
            else
            {
                return null;
            }

        }

        
    }
}
