using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
namespace CS.General_v3.Controls.WebControls.Common
{
    [DefaultProperty("File")]
    [ToolboxData("<{0}:Include runat=server></{0}:Include>")]
    public class Include : WebControl
    {
        string _file = "";

        [Category("Appearance")]
        [DefaultValue("")]
        public string File
        {
            get
            {
                return _file;
            }

            set
            {
                _file = value;
            }
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            
            if (_file != "")
            {
                FileInfo f = new FileInfo(CS.General_v3.Util.PageUtil.MapPath(_file));
                //FileInfo f = new FileInfo("G:\\LocalHost\\propertyplanet.eu\\default2.aspx");
                if (f.Exists)
                {
                    FileStream stream = f.OpenRead();
                    StreamReader sr = new StreamReader(stream);
                    writer.Write(sr.ReadToEnd());
                    sr.Close();
                    stream.Close();
                }
                else
                    writer.Write("<<!== INCLUDE FILE DOES NOT EXISTS ("+_file+") ==!>>");
            }
            else
            {
                writer.Write("<< NO FILE SPECIFIED>>");
            }

           // base.Render(writer);   
        }
    }
}
