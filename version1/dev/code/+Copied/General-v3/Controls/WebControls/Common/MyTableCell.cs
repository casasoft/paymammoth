using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.General_v3.Controls.WebControls.Common
{
    using JavaScript.jQuery.Plugins.Tooltips.QTip;

    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyTableCell runat=server></{0}:MyTableCell>")]

    public class MyTableCell : TableCell, IBaseWebControl
    {
        
        public MyTableCell() 
        {
            
            this._functionality = new BaseWebControlFunctionality(this);
        }

        #region IBaseWebControl Members
        private BaseWebControlFunctionality _functionality = null;
        public CS.General_v3.Classes.CSS.CSSManagerForControl CssManager
        {
            get { return _functionality.CssManager; }
        }
        public new string CssClass
        {
            get
            {
                return _functionality.CssClass;
            }
            set
            {
                _functionality.CssClass = value;
                base.CssClass = _functionality.CssClass;
                
            }
        }
        
        public string InnerText
        {
            get
            {
                return _functionality.InnerText;
            }
            set
            {
                _functionality.InnerText = value;
            }
        }

        public string InnerHtml
        {
            get
            {
                return _functionality.InnerHtml;
            }
            set
            {
                _functionality.InnerHtml = value;
            }
        }
        protected override void OnPreRender(EventArgs e)
        {
            if (this.Controls.Count == 0)
            {
               // this.Controls.Add(new Literal() { Text = "&nbsp;" });
            }
            base.OnPreRender(e);
        }
       
        #endregion

        #region IBaseWebControl Members


        public BaseWebControlFunctionality WebControlFunctionality
        {
            get { return _functionality; }
        }

        #endregion



        public string RollOverClass { get { return _functionality.RollOverClass; } set { _functionality.RollOverClass = value; } }
        public string StringID { get { return _functionality.StringID; } set { _functionality.StringID = value; } }
        public string OnClientBlur { get { return _functionality.OnClientBlur; } set { _functionality.OnClientBlur = value; } }
        public string OnClientClick { get { return _functionality.OnClientClick; } set { _functionality.OnClientClick = value; } }
        public string OnClientChange { get { return _functionality.OnClientChange; } set { _functionality.OnClientChange = value; } }
        public string OnClientLoad { get { return _functionality.OnClientLoad; } set { _functionality.OnClientLoad = value; } }



        #region IBaseWebControl Members


        public WebControl Control { get { return this; } }

        #endregion

        #region IBaseControl Members

        Control IBaseControl.Control { get { return this; } }

        BaseControlFunctionality IBaseControl.WebControlFunctionality { get { return _functionality; } }

        #endregion



        public new jQueryQTip ToolTip
        {
            get { return _functionality.Tooltip; }
            set { _functionality.Tooltip = value; }
        }
    }
}
