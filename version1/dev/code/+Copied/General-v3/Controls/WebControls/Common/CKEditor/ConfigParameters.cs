/*
 * FCKeditor - The text editor for Internet - http://www.fckeditor.net
 * Copyright (C) 2003-2007 Frederico Caldeira Knabben
 *
 * == BEGIN LICENSE ==
 *
 * Licensed under the terms of any of the following licenses at your
 * choice:
 *
 *  - GNU General Public License Version 2 or later (the "GPL")
 *    http://www.gnu.org/licenses/gpl.html
 *
 *  - GNU Lesser General Public License Version 2.1 or later (the "LGPL")
 *    http://www.gnu.org/licenses/lgpl.html
 *
 *  - Mozilla Public License Version 1.1 or later (the "MPL")
 *    http://www.mozilla.org/MPL/MPL-1.1.html
 *
 * == END LICENSE ==
 *
 * This is the FCKeditor Asp.Net control.
 */

using System ;
using System.Web.UI ;
using System.Collections.Generic;
using System.Web.UI.WebControls ;
using System.ComponentModel ;
using System.Text;
using System.Text.RegularExpressions ;
using System.Globalization ;
using System.Security.Permissions ;

namespace CS.General_v3.Controls.WebControls.Common
{
    using JavaScript.Data;


    public partial class CKEditor : MyTxtBoxTextMultiLine, CS.General_v3.Controls.WebControls.Common.General.IMyFormWebControl
	{
        
        public partial class CONFIG_PARAMETERS
        {
            private CKEditor _ckEditor = null;
            private void initDefaultToolbars()
            {
                {//simple
                    string toolbarName = DefaultToolbarToText(DEFAULT_TOOLBARS.Simple);
                    //toolbarName = "simplistictoolbar";
                    TOOLBAR t = new TOOLBAR(toolbarName);
                    t.AddLine(TOOLBAR_BUTTON.Cut, TOOLBAR_BUTTON.Copy, TOOLBAR_BUTTON.Paste, TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.Maximize, TOOLBAR_BUTTON.ShowBlocks
                        );
                    t.AddLine(TOOLBAR_BUTTON.Bold, TOOLBAR_BUTTON.Italic, TOOLBAR_BUTTON.Underline, TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.JustifyLeft, TOOLBAR_BUTTON.JustifyCenter, TOOLBAR_BUTTON.JustifyRight, TOOLBAR_BUTTON.JustifyBlock, TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.Outdent, TOOLBAR_BUTTON.Indent, TOOLBAR_BUTTON.Blockquote, TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.NumberedList, TOOLBAR_BUTTON.BulletedList, TOOLBAR_BUTTON.Table, TOOLBAR_BUTTON.Toolbar_Separator);
/*                        
                        
                        TOOLBAR_BUTTON.Font, TOOLBAR_BUTTON.FontSize, TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.NumberedList, TOOLBAR_BUTTON.BulletedList, TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.Outdent, TOOLBAR_BUTTON.Indent, TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.Image);
                    t.AddLine(TOOLBAR_BUTTON.Format, TOOLBAR_BUTTON.Bold, TOOLBAR_BUTTON.Italic, TOOLBAR_BUTTON.Underline, TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.Subscript, TOOLBAR_BUTTON.Superscript, TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.BGColor, TOOLBAR_BUTTON.TextColor, TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.JustifyLeft, TOOLBAR_BUTTON.JustifyCenter, TOOLBAR_BUTTON.JustifyRight, TOOLBAR_BUTTON.JustifyBlock, TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.Link, TOOLBAR_BUTTON.Unlink, TOOLBAR_BUTTON.Anchor, TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.Table, TOOLBAR_BUTTON.SpecialChar);
                    t.AddLine(TOOLBAR_BUTTON.Styles, TOOLBAR_BUTTON.Format, TOOLBAR_BUTTON.Font, TOOLBAR_BUTTON.FontSize, TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.TextColor, TOOLBAR_BUTTON.BGColor, TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.Maximize, TOOLBAR_BUTTON.ShowBlocks, TOOLBAR_BUTTON.Toolbar_Separator, TOOLBAR_BUTTON.Source);*/
                    this.Toolbars.Add(t);
                }
                {//full
                    TOOLBAR t = new TOOLBAR(DefaultToolbarToText(DEFAULT_TOOLBARS.Full));
                    t.AddLine(TOOLBAR_BUTTON.Source, TOOLBAR_BUTTON.Toolbar_Separator,TOOLBAR_BUTTON.Save,TOOLBAR_BUTTON.NewPage,TOOLBAR_BUTTON.Preview,
                        TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.Templates,TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.Cut,TOOLBAR_BUTTON.Copy,TOOLBAR_BUTTON.Paste,TOOLBAR_BUTTON.PasteText,TOOLBAR_BUTTON.PasteFromWord,TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.Print,TOOLBAR_BUTTON.SpellChecker,TOOLBAR_BUTTON.Scayt,TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.Undo,TOOLBAR_BUTTON.Redo,TOOLBAR_BUTTON.Toolbar_Separator,TOOLBAR_BUTTON.Find,TOOLBAR_BUTTON.Replace,
                        TOOLBAR_BUTTON.Toolbar_Separator,TOOLBAR_BUTTON.SelectAll,TOOLBAR_BUTTON.RemoveFormat,TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.Form,TOOLBAR_BUTTON.Checkbox,
                        TOOLBAR_BUTTON.Radio,TOOLBAR_BUTTON.TextField,TOOLBAR_BUTTON.Textarea,TOOLBAR_BUTTON.Select,TOOLBAR_BUTTON.Button,
                        TOOLBAR_BUTTON.ImageButton,TOOLBAR_BUTTON.HiddenField);

                    t.AddLine(TOOLBAR_BUTTON.Bold,TOOLBAR_BUTTON.Italic,TOOLBAR_BUTTON.Underline,TOOLBAR_BUTTON.Strike,TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.Subscript,TOOLBAR_BUTTON.Superscript,TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.NumberedList,TOOLBAR_BUTTON.BulletedList,TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.Outdent,TOOLBAR_BUTTON.Indent,TOOLBAR_BUTTON.Blockquote,TOOLBAR_BUTTON.Toolbar_Separator,TOOLBAR_BUTTON.JustifyLeft,
                        TOOLBAR_BUTTON.JustifyCenter,TOOLBAR_BUTTON.JustifyRight,TOOLBAR_BUTTON.JustifyBlock,TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.Link,TOOLBAR_BUTTON.Unlink,TOOLBAR_BUTTON.Anchor,TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.Image,TOOLBAR_BUTTON.Flash,TOOLBAR_BUTTON.Table,TOOLBAR_BUTTON.HorizontalRule,TOOLBAR_BUTTON.Smiley,TOOLBAR_BUTTON.SpecialChar,
                        TOOLBAR_BUTTON.PageBreak);
                    
                    t.AddLine(TOOLBAR_BUTTON.Styles,TOOLBAR_BUTTON.Format,TOOLBAR_BUTTON.Font,TOOLBAR_BUTTON.FontSize,TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.TextColor,TOOLBAR_BUTTON.BGColor,TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.Maximize,TOOLBAR_BUTTON.ShowBlocks,TOOLBAR_BUTTON.Toolbar_Separator,TOOLBAR_BUTTON.About);
                        
                    this.Toolbars.Add(t);
                }
                {//standard
                    TOOLBAR t = new TOOLBAR(DefaultToolbarToText(DEFAULT_TOOLBARS.Standard));
                    t.AddLine(TOOLBAR_BUTTON.Cut,TOOLBAR_BUTTON.Copy,TOOLBAR_BUTTON.Paste,TOOLBAR_BUTTON.PasteText,TOOLBAR_BUTTON.PasteFromWord,
                        TOOLBAR_BUTTON.Toolbar_Separator,TOOLBAR_BUTTON.SpellChecker,TOOLBAR_BUTTON.Scayt,TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.Undo,TOOLBAR_BUTTON.Redo,TOOLBAR_BUTTON.Toolbar_Separator,TOOLBAR_BUTTON.Find,TOOLBAR_BUTTON.Replace,
                        TOOLBAR_BUTTON.Toolbar_Separator,TOOLBAR_BUTTON.SelectAll,TOOLBAR_BUTTON.RemoveFormat,TOOLBAR_BUTTON.Toolbar_Separator, 
                        TOOLBAR_BUTTON.Templates,TOOLBAR_BUTTON.Toolbar_Separator );

                    t.AddLine(TOOLBAR_BUTTON.Bold,TOOLBAR_BUTTON.Italic,TOOLBAR_BUTTON.Underline,TOOLBAR_BUTTON.Strike,TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.Subscript,TOOLBAR_BUTTON.Superscript,TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.NumberedList,TOOLBAR_BUTTON.BulletedList,TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.Outdent,TOOLBAR_BUTTON.Indent,TOOLBAR_BUTTON.Blockquote,TOOLBAR_BUTTON.Toolbar_Separator,TOOLBAR_BUTTON.JustifyLeft,
                        TOOLBAR_BUTTON.JustifyCenter,TOOLBAR_BUTTON.JustifyRight,TOOLBAR_BUTTON.JustifyBlock,TOOLBAR_BUTTON.Toolbar_Separator);
                    t.AddLine(TOOLBAR_BUTTON.Link,TOOLBAR_BUTTON.Unlink, TOOLBAR_BUTTON.Anchor,TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.Image, TOOLBAR_BUTTON.MediaEmbed, TOOLBAR_BUTTON.Table, TOOLBAR_BUTTON.HorizontalRule, TOOLBAR_BUTTON.Smiley, TOOLBAR_BUTTON.SpecialChar,
                        TOOLBAR_BUTTON.PageBreak);

                    t.AddLine(TOOLBAR_BUTTON.Format, TOOLBAR_BUTTON.Styles/*, TOOLBAR_BUTTON.Font, TOOLBAR_BUTTON.FontSize, TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.TextColor,TOOLBAR_BUTTON.BGColor*/,TOOLBAR_BUTTON.Toolbar_Separator,
                        TOOLBAR_BUTTON.Maximize,TOOLBAR_BUTTON.ShowBlocks,TOOLBAR_BUTTON.Toolbar_Separator,TOOLBAR_BUTTON.Source);
                        
                    this.Toolbars.Add(t);
                }
                
            }
            private void initDefaultFonts()
            {
                this.Font_Names.Add("Tahoma");
                this.Font_Names.Add("Arial");
                this.Font_Names.Add("Helvetica");
                this.Font_Names.Add("Verdana");
                this.Font_Names.Add("Sans-Serif");
            }
            private void initDefaultFontSizes()
            {
                this.FontSize_Sizes.Add("Small", "10px");
                this.FontSize_Sizes.Add("Normal", "11px");
                this.FontSize_Sizes.Add("Large", "14px");
                this.FontSize_Sizes.Add("Very Large", "18px");
                
            }
            private void initDefaultFormatTags()
            {
                this.Format_Tags.AddRange(new string[] { "p", /*"h1",*/ "h2", "h3", "h4" });
            }
            private void initDefaultCKFileBrowser()
            {
                this.FileBrowser_BrowseUrl =
                   this.FileBrowser_FlashBrowseUrl =
                   this.FileBrowser_ImageBrowseUrl = "/_common/vs/ckeditor/v1/fileBrowser.aspx";

                this.FileBrowser_FlashUploadUrl =
                    this.FileBrowser_ImageUploadUrl =
                    this.FileBrowser_UploadUrl = "/_common/vs/ckeditor/v1/upload.aspx";

                    
            }
            private void init()
            {
                initDefaultFormatTags();
                initDefaultToolbars();
                initDefaultFonts();
                initDefaultFontSizes();
                initDefaultCKFileBrowser();
            }
            public CONFIG_PARAMETERS(CKEditor ckEditor)
            {
                _ckEditor = ckEditor;
                this.Format_Tags = new List<string>();
                ColorButton_Colors = new List<string>();
                this.Toolbars = new List<TOOLBAR>();
                this.ExtraPlugins = new List<string>();
                this.ExtraPlugins.Add("MediaEmbed");
                this.Font_Names = new ARRAY_ITEM_LIST();
                this.CKFinder_FolderPath = "/includes/ckfinder/";
                this.UseCKFinder = false;
                this.ContentCSSFiles = new List<string>();
                this.FontSize_Sizes = new ARRAY_ITEM_LIST();
                this.TabSpaces = 5;
                init();
                this.SetDefaultToolbar(DEFAULT_TOOLBARS.Standard);
                    
            }
            public JSObject GetAsJSObject()
            {
                if (string.IsNullOrEmpty(this.Width) && _ckEditor.Width != null && !_ckEditor.Width.IsEmpty)
                    this.Width = _ckEditor.Width.ToString();
                if (string.IsNullOrEmpty(this.Height) && _ckEditor.Height != null && !_ckEditor.Height.IsEmpty)
                    this.Height = _ckEditor.Height.ToString();


                JSObject js = new JSObject();
                
                if (AutoUpdateElement.HasValue) js.AddProperty("autoUpdateElement", AutoUpdateElement);
                if (this.BaseFloatZIndex.HasValue) js.AddProperty("baseFloatZIndex", BaseFloatZIndex);
                if (this.BaseHref != null) js.AddProperty("baseHref", BaseHref);
                if (this.ColorButton_BackStyle != null) js.AddProperty("colorButton_backStyle", ColorButton_BackStyle);
                if (this.ColorButton_Colors != null) js.AddProperty("colorButton_colors", CS.General_v3.Util.ListUtil.ConvertListToString(ColorButton_Colors,","));
                if (this.BodyClass != null) js.AddProperty("bodyClass", this.BodyClass);
                if (this.BodyID != null) js.AddProperty("bodyId", this.BodyID);
                if (this.DocType != null) js.AddProperty("docType", this.DocType);
                if (this.ColorButton_EnableMore != null) js.AddProperty("colorButton_enableMore", ColorButton_EnableMore);
                if (this.ColorButton_ForeStyle != null) js.AddProperty("colorButton_foreStyle", ColorButton_ForeStyle);
                if (this.ContentCSSFiles != null) js.AddProperty("contentsCss", ContentCSSFiles);
                if (this.CustomConfigFilePath != null) js.AddProperty("customConfig", CustomConfigFilePath);
                if (this.Dialog_BackgroundCoverColor != null) js.AddProperty("dialog_backgroundCoverColor", Dialog_BackgroundCoverColor);
                if (this.DisableNativeSpellChecker != null) js.AddProperty("disableNativeSpellChecker", DisableNativeSpellChecker);
                if (this.DisableObjectResizing != null) js.AddProperty("disableObjectResizing", DisableObjectResizing);
                if (this.FileBrowser_BrowseUrl != null) js.AddProperty("filebrowserBrowseUrl", FileBrowser_BrowseUrl);
                if (this.FileBrowser_ImageBrowseUrl != null) js.AddProperty("filebrowserImageBrowseUrl", FileBrowser_ImageBrowseUrl);
                if (this.FileBrowser_FlashUploadUrl != null) js.AddProperty("filebrowserFlashUploadUrl", FileBrowser_FlashUploadUrl);
                if (this.FileBrowser_FlashBrowseUrl != null) js.AddProperty("filebrowserFlashBrowseUrl", FileBrowser_FlashBrowseUrl);
                if (this.FileBrowser_UploadUrl != null) js.AddProperty("filebrowserUploadUrl", FileBrowser_UploadUrl);
                if (this.FileBrowser_ImageUploadUrl != null) js.AddProperty("filebrowserImageUploadUrl", FileBrowser_ImageUploadUrl);
                if (this.FileBrowser_WindowWidth != null) js.AddProperty("filebrowserWindowWidth", FileBrowser_WindowWidth);
                if (this.FileBrowser_WindowHeight != null) js.AddProperty("filebrowserWindowHeight", FileBrowser_WindowHeight);
                if (this.EnterMode != null) js.AddProperty("enterMode", EnterModeToCode(this.EnterMode.Value));
                if (this.ExtraPlugins != null) js.AddProperty("extraPlugins", CS.General_v3.Util.ListUtil.ConvertListToString(ExtraPlugins, ","));
                if (this.Find_Highlight != null) js.AddProperty("find_highlight", Find_Highlight);
                if (this.Font_DefaultLabel != null) js.AddProperty("font_defaultLabel", Font_DefaultLabel);
                if (this.Format_Tags != null && this.Format_Tags.Count > 0)
                {
                    string s = CS.General_v3.Util.ListUtil.ConvertListToString(Format_Tags,";");
                    if (s.EndsWith(";"))
                        s = s.Substring(0,s.Length-1);
                    js.AddProperty("format_tags",s );
                }
                if (this.Font_Names != null) js.AddProperty("font_names", Font_Names.ToString()); //array
                if (this.Font_Style != null) js.AddProperty("font_style", Font_Style);
                if (this.FontSize_DefaultLabel != null) js.AddProperty("fontSize_defaultLabel", FontSize_DefaultLabel);
                if (this.FontSize_Sizes != null) js.AddProperty("fontSize_sizes", FontSize_Sizes.ToString()); //array
                if (this.ForcePasteAsPlainText != null) js.AddProperty("forcePasteAsPlainText", ForcePasteAsPlainText);
                if (this.FullPage != null) js.AddProperty("fullPage", FullPage);
                if (this.Height != null) js.AddProperty("height", Height);
                if (this.Menu_SubMenuDelay != null) js.AddProperty("menu_subMenuDelay", Menu_SubMenuDelay);
                if (this.PasteFromWordKeepsStructure != null) js.AddProperty("pasteFromWordKeepsStructure", PasteFromWordKeepsStructure);
                if (this.Resize_Enabled != null) js.AddProperty("resize_enabled", Resize_Enabled);
                if (this.Resize_MaxHeight != null) js.AddProperty("resize_maxHeight", Resize_MaxHeight);
                if (this.Resize_MaxWidth != null) js.AddProperty("resize_maxWidth", Resize_MaxWidth);
                if (this.Resize_MinHeight != null) js.AddProperty("resize_minHeight", Resize_MinHeight);
                if (this.Resize_MinWidth != null) js.AddProperty("resize_minWidth", Resize_MinWidth);
                if (this.ShiftEnterMode != null) js.AddProperty("shiftEnterMode", EnterModeToCode(ShiftEnterMode.Value));
                if (this.Skin != null) js.AddProperty("skin", Skin);
                if (this.StartUp_HasFocus != null) js.AddProperty("startupFocus", StartUp_HasFocus);
                if (this.StylesCombo_StyleSet != null) js.AddProperty("stylesCombo_stylesSet", StylesCombo_StyleSet);
                //if (this.TabSpaces != null) js.AddProperty("tabSpaces", TabSpaces);
                js.AddProperty("tabSpaces", TabSpaces);
                if (this.TemplateFiles != null && this.TemplateFiles.Count > 0)
                {
                    JSArray arrTemplates = new JSArray();
                    JSArray arrTemplateFiles = new JSArray();

                    for (int i =0; i < TemplateFiles.Count; i++)
                    {
                        var t = TemplateFiles[i];
                        arrTemplates.AddItem(t.Title);
                        arrTemplateFiles.AddItem(t.URL);
                    }

                    js.AddProperty("templates", arrTemplates);
                    js.AddProperty("templates_files", arrTemplateFiles);
                }
                if (this.Theme != null) js.AddProperty("theme", Theme);
                if (this.Width != null) js.AddProperty("width", Width);
                var toolbarArr = getToolbarJS();
                if (toolbarArr != null)
                {
                    
                    js.AddProperty("toolbar", toolbarArr);
                }
                return js;
            }
            private JSArray getToolbarJS()
            {
                for (int i = 0; i < Toolbars.Count; i++)
                {
                    if (Toolbars[i].Name == this.ToolbarName)
                    {
                        return Toolbars[i].GetAsJSArray();
                    }
                }
                return null;

            }
            #region Properties

            public string FileBrowser_BrowseUrl { get; set; }
            
            public string FileBrowser_ImageBrowseUrl { get; set; }
            public string FileBrowser_FlashBrowseUrl { get; set; }
            public string FileBrowser_UploadUrl { get; set; }
            public string FileBrowser_ImageUploadUrl { get; set; }
            public string FileBrowser_FlashUploadUrl { get; set; }
            public string FileBrowser_WindowWidth{ get; set; }
            public string FileBrowser_WindowHeight { get; set; }
            /// <summary>
            /// A list of semi colon separated style names (by default tags) representing the style definition for each entry to be displayed in the Format combo in the toolbar. Each entry must have its relative definition configuration in a setting named "format_(tagName)". For example, the "p" entry has its definition taken from config.format_p. 
            /// </summary>
            public List<string> Format_Tags { get; set; }
            public List<TOOLBAR> Toolbars { get; set; }
            /// <summary>
            /// The URL path for the custom configuration file to be loaded. If not overloaded with inline configurations, it defaults to the "config.js" file present in the root of the CKEditor installation directory.
///
///CKEditor will recursively load custom configuration files defined inside other custom configuration files. 
            /// </summary>
            public string CustomConfigFilePath { get; set; }
            /// <summary>
            /// Whether the replaced element (usually a textarea) is to be updated automatically when posting the form containing the editor. 
            /// </summary>
            public bool? AutoUpdateElement { get; set; }
            /// <summary>
            /// The base Z-index for floating dialogs and popups. 
            /// </summary>
            public int? BaseFloatZIndex { get; set; }
            /// <summary>
            /// The base href URL used to resolve relative and absolute URLs in the editor content. 
            /// </summary>
            public string BaseHref { get; set; }
            /// <summary>
            /// Sets the "class" attribute to be used on the body element of the editing area
            /// </summary>
            public string BodyClass { get; set; }
            /// <summary>
            /// Sets the "id" attribute to be used on the body element of the editing area.
            /// </summary>
            public string BodyID { get; set; }
            /// <summary>
            /// Sets the doctype to be used when loading the editor content as HTML.
            /// </summary>
            public string DocType { get; set; }
            public string CKFinder_FolderPath { get; set; }
            public bool UseCKFinder { get; set; }
            /// <summary>
            /// Holds the style definition to be used to apply the text background color. 
            /// </summary>
            public string ColorButton_BackStyle { get; set; }
            /// <summary>
            /// Defines the colors to be displayed in the color selectors. It's a string containing the hexadecimal notation for HTML colors, without the "#" prefix. 
            /// </summary>
            public List<string> ColorButton_Colors { get; set; }
            /// <summary>
            /// Whether to enable the "More Colors..." button in the color selectors. 
            /// </summary>
            public bool? ColorButton_EnableMore { get; set; }
            /// <summary>
            /// Holds the style definition to be used to apply the text foreground color. 
            /// </summary>
            public string ColorButton_ForeStyle { get; set; }
            /// <summary>
            /// The CSS file(s) to be used to apply style to the contents. It should reflect the CSS used in the final pages where the contents are to be used. 
            /// </summary>
            public List<string> ContentCSSFiles { get; set; }
            /// <summary>
            /// The color of the dialog background cover. It should be a valid CSS color string. 
            /// </summary>
            public string Dialog_BackgroundCoverColor { get; set; }
            /// <summary>
            /// Disables the built-in spell checker while typing natively available in the browser (currently Firefox and Safari only).
///Even if word suggestions will not appear in the CKEditor context menu, this feature is useful to help quickly identifying misspelled words.
///This setting is currently compatible with Firefox only due to limitations in other browsers. 
            /// </summary>
            public string DisableNativeSpellChecker { get; set; }
            /// <summary>
            /// Disables the ability of resize objects (image and tables) in the editing area. 
            /// </summary>
            public bool? DisableObjectResizing {get;set;}
            public enum ENTER_MODE
            {
                NewParagraph,
                NewBR,
                NewDiv
            }
            public string EnterModeToCode(ENTER_MODE mode)
            {
                switch (mode)
                {
                    case ENTER_MODE.NewBR: return "CKEDITOR.ENTER_BR";
                    case ENTER_MODE.NewParagraph: return "CKEDITOR.ENTER_P";
                    case ENTER_MODE.NewDiv: return "CKEDITOR.ENTER_DIV";
                }
                return "";
            }
            /// <summary>
            /// Sets the behavior for the ENTER key. It also dictates other behaviour rules in the editor, like whether the <br> element is to be used as a paragraph separator when indenting text. The allowed values are the following constants, and their relative behavior: 
            /// </summary>
            public ENTER_MODE? EnterMode { get; set; }
            /// <summary>
            /// List of additional plugins to be loaded. This is a tool setting which makes it easier to add new plugins, whithout having to touch and possibly breaking the CKEDITOR.config.plugins setting. 
            /// </summary>
            public List<string> ExtraPlugins { get; set; }
            /// <summary>
            /// Defines the style to be used to highlight results with the find dialog. 
            /// </summary>
            public string Find_Highlight { get; set; }
            /// <summary>
            /// The text to be displayed in the Font combo is none of the available values matches the current cursor position or text selection. 
            /// </summary>
            public string Font_DefaultLabel { get; set; }
            /// <summary>
            /// The list of fonts names to be displayed in the Font combo in the toolbar. Entries are separated by semi-colons (;), 
            /// while it's possible to have more than one font for each entry, 
            /// in the HTML way (separated by comma).
            /// A display name may be optionally defined by prefixing the entries with the name and the slash character. 
            /// For example, "Arial/Arial, Helvetica, sans-serif" will be displayed as "Arial" in the list,
            /// but will be outputted as "Arial, Helvetica, sans-serif". 
            /// </summary>
            public ARRAY_ITEM_LIST Font_Names { get; set; }
            /// <summary>
            /// The style definition to be used to apply the font in the text. 
            /// </summary>
            public string Font_Style { get; set; }
            /// <summary>
            /// The text to be displayed in the Font Size combo is none of the available values matches the current cursor position or text selection.
            /// </summary>
            public string FontSize_DefaultLabel { get; set; }

            public class ARRAY_ITEM_LIST : List<ARRAY_ITEM>
            {
                public void Add(string title)
                {
                    Add(title, title);
                }
                public void Add(string title, string value)
                {
                    this.Add(new ARRAY_ITEM(title, value));
                }
                public override string ToString()
                {
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < this.Count; i++)
                    {
                        sb.Append(this[i].ToString());
                    }
                    return sb.ToString();
                }
            }
            public class ARRAY_ITEM
            {
                public string Title { get; set; }
                public string Value { get; set; }
                public ARRAY_ITEM(string title, string value)
                {
                    this.Title = title;
                    this.Value = value;
                }
                public override string ToString()
                {
                    string s = "";
                    if (!string.IsNullOrEmpty(this.Title))
                        s = this.Title + "/";
                    s += this.Value;
                    s += ";";
                    return s;
                    
                    
                }
            }
            /// <summary>
            /// The list of fonts size to be displayed in the Font Size combo in the toolbar. 
            /// Entries are separated by semi-colons (;). Any kind of "CSS like" size can be used, 
            /// like "12px", "2.3em", "130%", "larger" or "x-small". 
            /// A display name may be optionally defined by prefixing the entries with the name and the slash character. 
            /// For example, "Bigger Font/14px" will be displayed as "Bigger Font" in the list, but will be outputted as "14px".
            /// </summary>
            public ARRAY_ITEM_LIST FontSize_Sizes { get; set; }
            /// <summary>
            /// Whether to force all pasting operations to insert on plain text into the editor, loosing any formatting information possibly available in the source text. 
            /// </summary>
            public bool? ForcePasteAsPlainText { get; set; }
            /// <summary>
            /// Indicates whether the contents to be edited are being inputted as a full HTML page. A full page includes the <html>, <head> and <body> tags. The final output will also reflect this setting, including the <body> contents only if this setting is disabled. 
            /// </summary>
            public bool? FullPage { get; set; }
            /// <summary>
            /// The editor height, in CSS size format or pixel integer. 
            /// </summary>
            public string Height { get; set; }
            /// <summary>
            /// The amount of time, in milliseconds, the editor waits before showing submenu options when moving the mouse over options that contains submenus, like the "Cell Properties" entry for tables. 
            /// </summary>
            public int? Menu_SubMenuDelay { get; set; }
            /// <summary>
            /// Whether to keep structure markup (<h1>, <h2>, etc.) or replace it with elements that create more similar pasting results when pasting content from Microsoft Word into the Paste from Word dialog. 
            /// </summary>
            public bool? PasteFromWordKeepsStructure { get; set; }
            /// <summary>
            /// Whether to enable the resizing feature. If disabed the resize handler will not be visible.

            /// </summary>
            public bool? Resize_Enabled { get; set; }
            /// <summary>
            /// The maximum editor height, in pixels, when resizing it with the resize handle. 
            /// </summary>
            public int? Resize_MaxHeight { get; set; }
            /// <summary>
            /// The maximum editor width, in pixels, when resizing it with the resize handle. 
            /// </summary>
            public int? Resize_MaxWidth { get; set; }
            /// <summary>
            /// The minimum editor height, in pixels, when resizing it with the resize handle. 
            /// </summary>
            public int? Resize_MinHeight { get; set; }
            /// <summary>
            /// The minimum editor width, in pixels, when resizing it with the resize handle. 
            /// </summary>
            public int? Resize_MinWidth { get; set; }
            /// <summary>
            /// Just like the CKEDITOR.config.enterMode setting, it defines the behavior for the SHIFT+ENTER key. The allowed values are the following constants, and their relative behavior:
            /// </summary>
            public ENTER_MODE? ShiftEnterMode { get; set; }
            /// <summary>
            /// The skin to load. It may be the name of the skin folder inside the editor installation path, or the name and the path separated by a comma.
            /// </summary>
            public string Skin { get; set; }
            /// <summary>
            /// Sets whether the editor should have the focus when the page loads. 
            /// </summary>
            public bool? StartUp_HasFocus { get; set; }
            /// <summary>
            /// The "styles definition set" to load into the styles combo. The styles may be defined in the page containing the editor, or can be 
            /// loaded on demand from an external file when opening the styles combo for the fist time. In the second case, if this setting contains only a name, 
            /// the styles definition file will be loaded from the "styles" folder inside the stylescombo plugin folder. 
            /// Otherwise, this setting has the "name:url" 
            /// syntax, making it possible to set the URL from which loading the styles file. 
            /// E.g    default:/css/ckeditor-styles.js
            /// </summary>
            public string StylesCombo_StyleSet { get; set; }
            /// <summary>
            /// Intructs the editor to add a number of spaces (&nbsp;) to the text when hitting the TAB key. If set to zero, the TAB key will be used to move the cursor focus to the next element in the page, out of the editor focus. 
            /// </summary>
            public int TabSpaces { get; set; }
            public class TEMPLATE_FILE
            {
                public string Title { get; set; }
                public string URL { get; set; }
                public TEMPLATE_FILE(string title, string url)
                {
                    this.Title = title;
                    this.URL = url;
                }
            }
            public string ToolbarName { get; set; }
            public void SetDefaultToolbar(DEFAULT_TOOLBARS toolbar)
            {
                this.ToolbarName = DefaultToolbarToText(toolbar);
            }
            public List<TEMPLATE_FILE> TemplateFiles { get; set; }
            /// <summary>
            /// The theme to be used to build the UI. 
            /// </summary>
            public string Theme { get; set; }
            /// <summary>
            /// The editor width in CSS size format or pixel integer. 
            /// </summary>
            public string Width {get;set; }
            #endregion
        }
        
    }
}
