using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CS.General_v3.Classes.CSS;

namespace CS.General_v3.Controls.WebControls.Common
{

    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyHtmlTableRow runat=server></{0}:MyHtmlTableRow>")]

    public class MyHtmlTableRow :HtmlTableRow, IBaseControl
    {
        public MyHtmlTableRow()
        {
            _functionality = new BaseHtmlControlFunctionality(this);
        }

        #region IBaseWebControl Members
        /// <summary>
        /// Adds a cell, and adds the controls to the cell
        /// </summary>
        /// <param name="controls">Controls to add</param>
        /// <returns></returns>
        public MyHtmlTableCell AddCell(string cssClass, params Control[] controls)
        {
            return addCell(cssClass, null, controls);
        }
        /// <summary>
        /// Adds an empty cell
        /// </summary>
        /// <param name="innerHtml">inner HTML of cell</param>
        /// <returns></returns>
        public MyHtmlTableCell AddCell()
        {
            return addCell(null, null);
        }
        /// <summary>
        /// Adds a cell, and sets the text as its innerHtml property
        /// </summary>
        /// <param name="innerHtml">inner HTML of cell</param>
        /// <returns></returns>
        public MyHtmlTableCell AddCell(string cssClass)
        {
            return addCell(cssClass, null);
        }
        /// <summary>
        /// Adds a cell, and sets the text as its innerHtml property
        /// </summary>
        /// <param name="innerHtml">inner HTML of cell</param>
        /// <returns></returns>
        public MyHtmlTableCell AddCell(string cssClass, string innerHtml)
         {
             return addCell(cssClass, innerHtml, null);
         }
        /// <summary>
         /// Adds a cell, and adds the controls to the cell
        /// </summary>
        /// <param name="innerHtml"></param>
        /// <param name="controls"></param>
        /// <returns></returns>
        private MyHtmlTableCell addCell(string cssClass, string innerHtml, params Control[] controls)
        {
            MyHtmlTableCell td = new MyHtmlTableCell();
            this.Cells.Add(td);
             
            if (controls != null && controls.Length > 0)
            {
                if (controls != null)
                {
                    foreach (var ctrl in controls)
                    {
                        if (ctrl != null)
                        {
                            td.Controls.Add(ctrl);
                        }
                    }
                }
                if (!string.IsNullOrEmpty(innerHtml))
                {
                    td.InnerHtml = innerHtml;
                }
            }
            else 
            {
                 td.InnerHtml = innerHtml;   
            }
            if (!string.IsNullOrEmpty(cssClass))
                td.CssManager.AddClass(cssClass);
            return td;
        }
        public CS.General_v3.Classes.CSS.CSSManagerForControl CssManager
        {
            get { return _functionality.CssManager; }
        }
        public string CssClass
        {
            get
            {
                return _functionality.CssClass;
            }
            set
            {
                _functionality.CssClass = value;
            }
        }


        #endregion
        private BaseHtmlControlFunctionality _functionality = null;






        #region IBaseControl Members

        Control IBaseControl.Control
        {
            get { return this; }
        }

        BaseControlFunctionality IBaseControl.WebControlFunctionality
        {
            get { return _functionality; }
        }

        #endregion
    }
}
