using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.General_v3.Controls.WebControls.Common
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyEmail runat=server></{0}:MyEmail>")]
    public class MyEmail : Literal 
    {
        private string _Style = "";

        public string Style { get { return _Style; } set { _Style = value; } }

        private string _CssClass = "";
        public string CssClass
        {
            get { return _CssClass; }
            set { _CssClass = value; }
        }


        private bool _ConvertEmailToAntiSpam = true;
        public bool ConvertEmailToAntiSpam
        {
            get { return _ConvertEmailToAntiSpam; }
            set { _ConvertEmailToAntiSpam = value; }
        }

        private bool _MakeAnchorLink = true;
        public bool MakeAnchorLink
        {
            get { return _MakeAnchorLink; }
            set { _MakeAnchorLink = value; }
        }
        /*private string _AnchorText = "";
        public string AnchorText
        {
            get { return _AnchorText; }
            set { _AnchorText = value; }
        }*/
        private string _EmailSubject = "";
        public string EmailSubject
        {
            get { return _EmailSubject; }
            set { _EmailSubject = value; }
        }

        private string _Email = "";
        public string Email
        {
            get
            {
                return _Email;
            }
            set
            {
                _Email = value;
            }
        }

        public string Title { get; set; }

        
        public override void  RenderControl(HtmlTextWriter writer)
        {
            string data;
            string txt = this.Text;
            if (txt == "" || txt == null)
                txt = this.Email;
            if (HttpContext.Current != null)
            {
                if (ConvertEmailToAntiSpam)
                {
                    
                    data = CS.General_v3.Util.Text.EncodeEmailForSpam(this.Email, txt, this.EmailSubject,
                        this.MakeAnchorLink, this.CssClass, Title);
                }
                else
                {
                    data = this.Text;
                }
            }
            else
            {
                data = txt;
            }
            if (Style != "" && Style != null)
            {
                data = "<span style=\"" + Style + "\">" + data + "</span>";
            }
            writer.Write(data);
        }
        /*
        public override void RenderControl2(HtmlTextWriter writer)
        {
            string data;
            if (HttpContext.Current != null)
            {
                if (ConvertEmailToAntiSpam)
                {
                    string tmp = "";
                    if (MakeAnchorLink)
                    {
                        tmp = "<a ";
                        if (CssClass != "")
                            tmp += "class=\"" + CssClass + "\" ";
                        tmp += "href=\"mailto:" + this.Text;
                        if (EmailSubject != "")
                            tmp += "?subject=" + EmailSubject;
                        tmp += "\">";
                        if (AnchorText != "")
                            tmp += AnchorText;
                        else
                            tmp += this.Text;
                        tmp += "</a>";
                    }
                    else
                    {
                        if (CssClass != "")
                            tmp += "<span class=\"" + CssClass + "\">";
                        tmp += this.Text;
                        if (CssClass != "")
                            tmp += "</span>";
                    }
                    data = "document.write(";
                    for (int i = 0; i < tmp.Length; i++)
                    {
                        if (i > 0)
                            data += " + ";
                        string curr = tmp[i].ToString();
                        if (curr == "\"")
                            curr = "\\\"";

                        data += "\"" + curr + "\"";
                    }
                    data += ");";
                    data = CS.General_v3.Util.Text.makeJavaScript(data, false);
                }
                else
                {
                    data = this.Text;
                }
            }
            else
            {
                if (MakeAnchorLink && AnchorText != "")
                    data = AnchorText;
                else
                    data = this.Text;
            }
            if (Style != "" && Style != null)
            {
                data = "<span style=\"" + Style + "\">" + data + "</span>";
            }
            writer.Write(data);
        }*/
    }
}
