using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.General_v3.Controls.WebControls.Common
{

    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyTxtBoxTextSingleLine runat=server></{0}:MyTxtBoxTextSingleLine>")]

    public class MyTxtBoxTextSingleLine : MyTxtBoxTextString
    {
        public MyTxtBoxTextSingleLine() :
            base()
        
        {
            this.TextMode = TextBoxMode.SingleLine;
            
        }
        public MyTxtBoxTextSingleLine(string ID, string Title, bool Required, string HelpMessage)
            : this(ID, Title, Required, HelpMessage, null, "")
        { }
        public MyTxtBoxTextSingleLine(string ID, string Title, bool Required, string HelpMessage, int? width,  string text) :
            base()
        {
            if (width != null)
                this.Width = width.Value;
            this.ID = this.StringID = ID;
            this.Title = Title;
            this.TextMode = TextBoxMode.SingleLine;
            this.Required = Required;
            this.HelpMsg = HelpMessage;
            
            this.SetValue(text);
        }
    }
}
