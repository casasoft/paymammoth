﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CS.General_v3.Classes.CSS;
namespace CS.General_v3.Controls.WebControls.Common
{
    public class MyPanel9Grid : MyPlaceHolder       
    {
        public string TestID { get; set; }
        public int CellPadding { get; set; }
        public int CellSpacing { get; set; }
        public string CssClass { get; set; }

        public MyTable Table { get; set; }

        public MyTableCell TdTopLeft { get; set; }
        public MyTableCell TdTop { get; set; }
        public MyTableCell TdTopRight { get; set; }
        public MyTableCell TdRight { get; set; }
        public MyTableCell TdBottomRight { get; set; }
        public MyTableCell TdBottom { get; set; }
        public MyTableCell TdBottomLeft { get; set; }
        public MyTableCell TdLeft { get; set; }
        public MyTableCell TdMiddle { get; set; }
        public MyTableRow TrTop { get; set; }
        public MyTableRow TrMiddle { get; set; }
        public MyTableRow TrBottom { get; set; }
        protected bool _rendered;

        public CSSManagerForControl CssManager { get { return this.Table.CssManager; } }
        /// <summary>
        /// A panel which is used to include a 9-slice-scaling, like glows, rounded corners etc...  This uses a table not a div
        /// </summary>
        public MyPanel9Grid()
        {
            
            //this.CssManager.AddClass("panel");
            CellSpacing = CellPadding = 0;
            Table = new MyTable();
            //_cssManager = new CS.General_v3.Classes.CSS.CSSManager(Table);
            Table.CssManager.AddClass("panel");
            TdBottom = new MyTableCell();
            TdBottomLeft = new MyTableCell();
            TdBottomRight = new MyTableCell();
            TdLeft = new MyTableCell();
            TdRight = new MyTableCell();
            TdTop = new MyTableCell();
            TdTopLeft = new MyTableCell();
            TdTopRight = new MyTableCell();
            TdMiddle = new MyTableCell();
            TrTop = new MyTableRow();
            TrMiddle = new MyTableRow();
            TrBottom = new MyTableRow();
            TrTop.TableSection = TableRowSection.TableHeader;
            TrMiddle.TableSection = TableRowSection.TableBody;
            TrBottom.TableSection = TableRowSection.TableFooter;
            TrTop.Cells.Add(TdTopLeft);
            TrTop.Cells.Add(TdTop);
            TrTop.Cells.Add(TdTopRight);
            TrMiddle.Cells.Add(TdLeft);
            TrMiddle.Cells.Add(TdMiddle);
            TrMiddle.Cells.Add(TdRight);
            TrBottom.Cells.Add(TdBottomLeft);
            TrBottom.Cells.Add(TdBottom);
            TrBottom.Cells.Add(TdBottomRight);
            Table.Rows.Add(TrTop);
            Table.Rows.Add(TrMiddle);
            Table.Rows.Add(TrBottom);

            TdTop.CssClass = "t";
            TdTopLeft.CssClass = "tl";
            TdTopRight.CssClass = "tr";
            TdRight.CssClass = "r";
            TdBottomRight.CssClass = "br";
            TdBottom.CssClass = "b";
            TdBottomLeft.CssClass = "bl";
            TdLeft.CssClass = "l";
            TdMiddle.CssClass = "m";
            Table.CellPadding = Table.CellSpacing = 0;
        }
        
        protected override void OnPreRender(EventArgs e)
        {
            ForceRender();
            base.OnPreRender(e);
        }
        public virtual void ForceRender()
        {
            if (!_rendered)
            {
                this.CssManager.AddClass(this.CssClass);

                List<Control> ct = new List<Control>();
                for (int i = 0; i < Controls.Count; i++)
                {
                    ct.Add(Controls[i]);
                }
                // Table.CssClass = CssClass;
                //ct.AddRange(Controls);
                // Controls.Clear();
                Controls.Add(Table);
                for (int i = 0; i < ct.Count; i++)
                {
                    TdMiddle.Controls.Add(ct[i]);
                }
                _rendered = true;
            }
        }
        /*
        protected override void OnPreRender(EventArgs e)
        {
            
            base.OnPreRender(e);
        }
        protected override void Render(HtmlTextWriter writer)
        {
            
            //Controls.Add(Table);
            base.Render(writer);

        }
        protected override void RenderChildren(System.Web.UI.HtmlTextWriter writer)
        {
            
            base.RenderChildren(writer);
            //TdMiddle.RenderControl(writer);
            /*MyTable tbl = new MyTable();
            Controls.Add(tbl);
            tbl.Controls.Add(TrTop);
            writer.WriteBeginTag("table");
            writer.WriteAttribute("cellpadding", CellPadding.ToString());
            writer.WriteAttribute("cellspacing", CellSpacing.ToString());
            TdMiddle.RenderControl(writer);
            writer.WriteAttribute("class", CssClass);

            if (!string.IsNullOrEmpty(CssClassSuffix))
            {
                CssClassSuffix = " " + CssClassSuffix;
            }
            writer.Write("<thead><tr><td class='tl" + CssClassSuffix + "'>&nbsp;</td><td class='t" + CssClassSuffix + "'>&nbsp;</td><td class='tr" + CssClassSuffix + "'>&nbsp;</td></tr></thead>");
            writer.Write("<tfoot><tr><td class='bl" + CssClassSuffix + "'>&nbsp;</td><td class='b" + CssClassSuffix + "'>&nbsp;</td><td class='br" + CssClassSuffix + "'>&nbsp;</td></tr></tfoot>");
            writer.Write("<tbody><tr><td class='l" + CssClassSuffix + "'>&nbsp;</td><td class='m" + CssClassSuffix + "'>");
            base.RenderChildren(writer);
            writer.Write("</td><td class='r" + CssClassSuffix + "'>&nbsp;</td></tr></tbody></table>");
        }
    */
    }
}
