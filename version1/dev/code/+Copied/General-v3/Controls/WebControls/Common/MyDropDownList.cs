using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.General_v3.JavaScript.Data;
using CS.General_v3.Controls.WebControls.Common.General;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;

namespace CS.General_v3.Controls.WebControls.Common
{
    using System.Linq;
    using JavaScript.Data;

    /*
    * To set groups in the drop down box, you need to set the OptionGroup attribute to the title of the parent for
    * each item that is under the group
    * */

    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyDropDownList runat=server></{0}:MyDropDownList>")]
    public class MyDropDownList : DropDownList, General.IMyDropDownList, IPostBackEventHandler
    {
        public class MyDropDownListJsParams : JavaScriptObject
        {
            public bool onChangeRedirectToValue { get; set;}
        }


       

        public bool HtmlEncodeTextValues { get; set; }

        public void FocusGood()
        {
            
            _functionality.FocusGood();
        }
        public void AddListItems(ListItemCollection coll)
        {
            AddListItems(coll.Cast<ListItem>());
        }
        public void AddListItems(IEnumerable<ListItem> coll)
        {
            if (coll != null)
            {
                foreach (System.Web.UI.WebControls.ListItem c in coll)
                {
                    ListItem li = new ListItem(c.Text, c.Value);
                    li.Selected = c.Selected;
                    this.Items.Add(li);

                }
            }
        }
        

        protected override void OnLoad(EventArgs e)
        {
            checkSelectedIndexChangedEvent();
           // this.TextChanged += new EventHandler(MyDropDownList_TextChanged2);
            //base.SelectedIndexChanged += new EventHandler(MyDropDownList_SelectedIndexChanged3);
           /* btnTest = new MyButton(this.ID + "_testing");
            btnTest.Text = "test!!!!";
            
            //this.Page.Form.Controls.Add(btnTest);
            btnTest.Click += new EventHandler(btnTest_Click);
            btnTest.Style["position"] = "absolute";
            btnTest.Style["left"] = "400px";
            btnTest.Style["top"] = "600px";
            //initBtnTextChanged();*/
            base.OnLoad(e);
        }

        
        protected override void OnInit(EventArgs e)
        {
            
            base.Page.Init +=new EventHandler(Page_Init);
            base.Page.PreLoad += new EventHandler(Page_Init);
            base.Page.LoadComplete += new EventHandler(Page_Init);
            base.Page.Load += new EventHandler(Page_Init);
          
            base.OnInit(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            _functionality.ThrowErrorIfIDIsNull();
            base.OnPreRender(e);
        }
        public override string ID
        {
            get
            {
                return base.ID;
            }
            set
            {
                base.ID = value;
            }
        }

        private MyButton btnSelectedIndexChanged2 = null;
      
        /*
        private void initBtnSelectedIndexChanged()
        {
            
                
            //if (btnSelectedIndexChanged2 == null && this.SelectedIndexChanged != null)
                if (btnSelectedIndexChanged2 == null )
            {
                btnSelectedIndexChanged2 = new MyButton(this.ID + "_btnSelectedIndexChanged22");
                btnSelectedIndexChanged2.ID = this.ID + "_btnSelectedIndexChanged22";
                btnSelectedIndexChanged2.ValidationGroup = this.ID + "_btnSelectedIndexChanged22";
                this.ValidationGroup = btnSelectedIndexChanged2.ValidationGroup;
                btnSelectedIndexChanged2.Click += new EventHandler(btnSelectedIndexChanged_Click);
                
                btnSelectedIndexChanged2.Text = "Submit dropdown select";
                btnSelectedIndexChanged2.Style["position"] = "absolute";
                btnSelectedIndexChanged2.Style["left"] = "-500px;";
                btnSelectedIndexChanged2.Style["top"] = "-800px;";
               // this.Page.Form.Controls.Add(btnSelectedIndexChanged2);
               // this.OnClientChange += "document.getElementById('" + btnSelectedIndexChanged2.ClientID + "').click();";
            }
        }
        */

        void Page_Init(object sender, EventArgs e)
        {

           
                //initBtnSelectedIndexChanged();
           
            
            
        }
        

       // private MyButton btnTextChanged = null;
        public string GetFormValueAsStr()
        {
            //string test = base.SelectedValue;
            
                return this.FormValue;
            
        }
        public string StringID { get; set; }
        protected General.MyFormWebControlFunctionality _functionality { get; private set; }
        public MyDropDownList()
            : base()
        {
            init();
        }
        public MyDropDownList(string ID, string Title, bool Required)
            : this(ID,Title,Required,null,null)
        {

        }
        public MyDropDownList(string ID, string Title, bool Required, int? width, object initValue)
            : base()
        {init();
            this.ID = this.StringID = ID;
            this.Title = Title;
            this.Required = Required;
            SetValue(initValue);
            if (width != null)
                this.Width = width.Value;
        }
        private void initHandlers()
        {
            
        }
        private void init()
        {
            this.AutoSetToFormValue = true;
            _functionality = createFunctionality();
            this.HasValidation = true;
        }

        protected virtual CS.General_v3.Controls.WebControls.Common.General.MyFormWebControlFunctionality createFunctionality()
        {
            return new MyFormWebControlFunctionality(this, ClientIDSeparator);
        }

        [Category("Form")]
        [DefaultValue(true)]
        [Description("Automatically set its text value to its postback form value if it exists")]

        public bool AutoSetToFormValue { get; set; }

        [Category("Validation")]
        [DefaultValue(false)]
        [Description("Whether to validate control on blur or not")]
        public bool DoNotValidateOnBlur
        {
            get
            {
                return _functionality.DoNotValidateOnBlur;
            }

            set
            {
                _functionality.DoNotValidateOnBlur = value;

            }
        }

        //public new event EventHandler SelectedIndexChanged;
        public new event EventHandler TextChanged;
        protected override void RenderContents(HtmlTextWriter writer)
        {


            string currentOptionGroup;
            List<string> renderedOptionGroups = new List<string>();

            foreach (ListItem item in this.Items)
            {
                if (item.Attributes["OptionGroup"] == null ||
                    item.Attributes["OptionGroup"] == "")
                {
                    RenderListItem(item, writer);
                }
                else
                {
                    currentOptionGroup = item.Attributes["OptionGroup"];

                    if (renderedOptionGroups.Contains(currentOptionGroup))
                    {
                        RenderListItem(item, writer);
                    }
                    else
                    {
                        if (renderedOptionGroups.Count > 0)
                        {
                            RenderOptionGroupEndTag(writer);
                        }

                        RenderOptionGroupBeginTag(currentOptionGroup, writer);
                        renderedOptionGroups.Add(currentOptionGroup);

                        RenderListItem(item, writer);
                    }
                }
            }

            if (renderedOptionGroups.Count > 0)
            {
                RenderOptionGroupEndTag(writer);
            }


        }

        public new event EventHandler SelectedIndexChanged;


        private void checkSelectedIndexChangedEvent()
        {
            if (this.SelectedIndexChanged != null)
            {
                
                

                string js =Page.ClientScript.GetPostBackEventReference(this,"SelectedIndexChanged");
                js += ";";
                this.OnClientChange += js;
                this.OnClientClick += js;
                
            }
        }
        private void checkAutoPostback()
        {
            this.AutoPostBack = false;
        /*    if (this.SelectedIndexChanged != null)
            {
                this.AutoPostBack = true;
                
            }
            if (this.TextChanged != null)
            {
                this.AutoPostBack = true;

            }
         */
        }

        void MyDropDownList_TextChanged(object sender, EventArgs e)
        {
            if (TextChanged != null)
                this.TextChanged(sender, e);
        }
        private void initJS(HtmlTextWriter writer)
        {
            if (this.Visible && _functionality.IsVisibleEvenInParents())
            {

                JSArray arr = new JSArray();
                for (int i = 0; i < Items.Count; i++)
                {
                    if (Items[i].Enabled == false)
                    {
                        arr.AddItem(i);
                    }
                }

                MyDropDownListJsParams jsParams = new MyDropDownListJsParams();
                jsParams.onChangeRedirectToValue = OnChangeRedirectToValue;

                string js = "new js.com.cs."+CS.General_v3.Settings.Others.JAVASCRIPT_VERSION+".Controls.Form.MyDropDownList('" + this.ClientID + "', " + arr.GetJS() + "," + jsParams.GetJsObject().GetJS() + ")";
                js = CS.General_v3.Util.JSUtilOld.GetDeferredJSScriptThroughJQuery(js);

                js = CS.General_v3.Util.JSUtilOld.MakeJavaScript(js, true);
                writer.Write(js);
            }
        }

        private void checkTextHtmlEncoding()
        {
            if (!this.HtmlEncodeTextValues)
            {
                for (int i = 0; i < this.Items.Count; i++)
                {
                    var li = this.Items[i];
                    string t = li.Text;
                    string htmlDecoded = CS.General_v3.Util.Text.HtmlDecode(t);
                    li.Text = t;

                }
            }
        }

        public override void RenderControl(HtmlTextWriter writer)
        {
            checkTextHtmlEncoding();
            checkAutoPostback();
            
            try
            {
                if (AutoSetToFormValue && Page.IsPostBack)
                {
                    for (int i = 0; i < this.Items.Count; i++)
                    {
                        if (this.Items[i].Value == this.FormValue)
                        {
                            this.Text = this.FormValue;
                            break;
                        }
                    }
                }
            }
            catch { }

            /*if (_class_onBlur == "" && _class_onFocus != "" && CssClass != "")
                Class_onBlur = CssClass;
            if (_class_onMouseOut == "" && _class_onMouseOver != "" && CssClass != "")
                Class_onMouseOut = CssClass;*/

            if (!string.IsNullOrEmpty(OnClientChange))
            {
                Attributes["onkeyup"] += ";" + OnClientChange;
                Attributes["onchange"] += ";" + OnClientChange;
            }
            double oldWidth = 0;
            if (!this.Width.IsEmpty)
            {
                oldWidth = this.Width.Value;
                this.Width = new Unit(oldWidth + 7);
            }

            base.RenderControl(writer);

            if (!this.Width.IsEmpty)
                this.Width = new Unit(oldWidth);

            initJS(writer);
        }
        /*
        void MyDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SelectedIndexChanged != null)
                SelectedIndexChanged(sender, e);
        }*/
        #region Render Opt Groups

        private void RenderOptionGroupBeginTag(string name, HtmlTextWriter writer)
        {
            writer.WriteBeginTag("optgroup");
            writer.WriteAttribute("label", name);
            writer.Write(HtmlTextWriter.TagRightChar);
            writer.WriteLine();
        }

        private void RenderOptionGroupEndTag(HtmlTextWriter writer)
        {
            writer.WriteEndTag("optgroup");
            writer.WriteLine();
        }
        public bool DoNotConvertSpacesToNonBreaking { get; set; }
        private void RenderListItem(ListItem item, HtmlTextWriter writer)
        {
            writer.WriteBeginTag("option");
            writer.WriteAttribute("value", item.Value, true);

            if (item.Selected)
            {
                writer.WriteAttribute("selected", "selected", false);
            }

            foreach (string key in item.Attributes.Keys)
            {
                writer.WriteAttribute(key, item.Attributes[key]);
            }

            writer.Write(HtmlTextWriter.TagRightChar);
            string htmlValue = item.Text;
            if (!DoNotConvertSpacesToNonBreaking)
                htmlValue = htmlValue.Replace(" ", "&nbsp;");
            if (this.HtmlEncodeTextValues)
                htmlValue = CS.General_v3.Util.Text.HtmlEncode(htmlValue);
            writer.Write(htmlValue);
            //HttpUtility.HtmlEncode(htmlValue, writer);
            writer.WriteEndTag("option");
            writer.WriteLine();
        }
        #endregion

        #region IMyFormWebControl Members
        public bool HasValidation
        {
            get
            {
                return _functionality.HasValidation;

            }
            set
            {
                _functionality.HasValidation = value;

            }
        }

        public virtual void SetValue(object o)
        {
            string value = _functionality.SetValue(o);
            this.Text = value;


        }

        public override string ValidationGroup
        {
            get
            {
                return _functionality.ValidationGroup;
            }
            set
            {
                _functionality.ValidationGroup = value;
            }
        }
        [Category("Title")]
        [DefaultValue("")]
        [Description("Title to show for validation")]
        public string Title
        {
            get
            {
                return _functionality.Title;
            }

            set
            {
                _functionality.Title = value;
                /*
                */
            }
        }
        /// <summary>
        /// The value retrieved from the posted Form
        /// </summary>
        public object FormValueObject
        {
            get
            {
                return _functionality.FormValue;
            }

        }
        public object Value
        {
            get
            {
                for (int i = 0; i < Items.Count; i++)
                {
                    ListItem item = Items[i];
                    if (item.Selected)
                    {
                        if (!String.IsNullOrEmpty(item.Value))
                        {
                            return item.Value;
                        }
                        else
                        {
                            return item.Text;
                        }
                    }
                }
                return null;
            }
            set
            {
                SetValue(value);
                /*
                string val = value.ToString();
                for (int i = 0; i < Items.Count; i++)
                {
                    ListItem item = Items[i];

                    if ((!String.IsNullOrEmpty(item.Value) && item.Value == val) || (String.IsNullOrEmpty(item.Value) && item.Text == val))
                    {
                        item.Selected = true;
                    }
                    else
                    {
                        item.Selected = false;
                    }
                }*/
            }
        }
        public string FormValue
        {
            get
            {
                if (this.ClientIDMode == System.Web.UI.ClientIDMode.Static)
                {
                    return base.SelectedValue;
                }
                else
                {

                    return _functionality.FormValue;
                }
            }
        }
        public int FormValueAsInt
        {
            get
            {
                return FormValueAsIntNullable.GetValueOrDefault(0);
            }
        }
        public long FormValueAsLong
        {
            get
            {
                return FormValueAsLongNullable.GetValueOrDefault(0);
            }
        }
        public T GetFormValueAsEnum<T>() where T: struct, IConvertible
        {
            return CS.General_v3.Util.EnumUtils.EnumValueOf<T>(FormValue);
        }
        public T? GetFormValueAsEnumNullable<T>() where T : struct, IConvertible
        {
            
            return CS.General_v3.Util.EnumUtils.EnumValueNullableOf<T>(FormValue);
        }
        public int? FormValueAsIntNullable
        {
            get
            {
                return (int?)FormValueAsLongNullable;
            }
        }
        public long? FormValueAsLongNullable
        {
            get
            {
                long? num = null;
                long iVal = 0;
                if (long.TryParse(FormValue, out iVal))
                {
                    num = iVal;
                }
                return num;
            }
        }
        #region classes
        [Category("Javascript")]
        [DefaultValue(false)]
        [Description("Whether to use default CSS classes or not")]
        public bool UseDefaultCSSClasses
        {
            get
            {
                return _functionality.UseDefaultCSSClasses;
            }

            set
            {
                _functionality.UseDefaultCSSClasses = value;

            }
        }
        public string Class_Disabled { get { return WebControlFunctionality.Class_Disabled; } set { WebControlFunctionality.Class_Disabled = value; } }
        public string Class_onFocus { get { return WebControlFunctionality.Class_Focus; } set { WebControlFunctionality.Class_Focus = value; } }
        public string Class_onError { get { return WebControlFunctionality.Class_Error; } set { WebControlFunctionality.Class_Error = value; } }
        public string Class_onOver { get { return WebControlFunctionality.Class_Over; } set { WebControlFunctionality.Class_Over = value; } }
        public string Class_ReadOnly { get { return WebControlFunctionality.Class_ReadOnly; } set { WebControlFunctionality.Class_ReadOnly = value; } }
        public string Class_Required { get { return WebControlFunctionality.Class_Required; } set { WebControlFunctionality.Class_Required = value; } }

        #endregion

        #endregion

        #region IMyWebControl Members

        public object Tag
        {
            get { return _functionality.Tag; }
            set { _functionality.Tag = value; }
        }

        public string OnClientClick { get { return _functionality.OnClientClick; } set { _functionality.OnClientClick = value; } }
        public string OnClientLoad { get { return _functionality.OnClientLoad; } set { _functionality.OnClientLoad = value; } }
        public string OnClientChange { get { return _functionality.OnClientChange; } set { _functionality.OnClientChange = value; } }
        public string OnClientBlur { get { return _functionality.OnClientBlur; } set { _functionality.OnClientBlur = value; } }
        public string HelpID
        {
            get
            {
                return _functionality.HelpID;
            }
            set
            {
                _functionality.HelpID = value;
            }
        }

        public string HelpMsg
        {
            get
            {
                return _functionality.HelpMsg;
            }
            set
            {
                _functionality.HelpMsg = value;
            }
        }

        public string RollOverClass { get { return _functionality.RollOverClass; } set { _functionality.RollOverClass = value; } }


        #endregion

        public string ClientName
        {
            get
            {
                return _functionality.ClientName;
                
            }
        }
      
        


        #region IMyDropDownList Members
        public bool Required
        {
            get { return _functionality.Required; }
            set { _functionality.Required = value; }
        }

        

        #endregion

        public new WebControl Control
        {
            get { return this; }
        }

        public FormFieldsValidationParams.FormFieldValidationSubGroupParams SubGroupParams { get { return WebControlFunctionality.SubGroupParams; } set { WebControlFunctionality.SubGroupParams = value; } }

        public Enums.DATA_TYPE DataType
        {
            get
            {
                return Enums.DATA_TYPE.String;
            }
        }

        
        #region IPostBackEventHandler Members

        public void RaisePostBackEvent(string eventArgument)
        {
            switch (eventArgument)
            {
                case "SelectedIndexChanged":
                    this.SelectedIndexChanged(this, null);
                    break;
            }
        }

        #endregion


        public int? GetFormValueAsIntNullable()
        {
            return this.FormValueAsIntNullable;
            
        }

        public bool OnChangeRedirectToValue { get; set; }

        #region IBaseWebControl Members

        public General_v3.Classes.CSS.CSSManagerForControl CssManager
        {
            get { return _functionality.CssManager; }
        }

        public MyFormWebControlFunctionality WebControlFunctionality
        {
            get { return _functionality; }
        }

        #endregion

        #region IMyFormWebControl Members


       

        #endregion

        #region IMyFormWebControl Members


        Control IBaseControl.Control
        {
            get
            {
                return this.Control;
            }
        }

        BaseControlFunctionality IBaseControl.WebControlFunctionality
        {
            get
            {
                return this.WebControlFunctionality;
            }
        }
        BaseWebControlFunctionality IBaseWebControl.WebControlFunctionality
        {
            get
            {
                return this.WebControlFunctionality;
            }
        }
        #endregion

        #region IMyFormWebControl Members


        public FormFieldsValidationParams ValidationParams
        {
            get
            {
                return this.WebControlFunctionality.ValidationParams;
            }
            set { this.WebControlFunctionality.UpdateValidationParams(value); }
        }

        public FormFieldsCSSClasses FieldCssClasses
        {
            get
            {
                return this.WebControlFunctionality.FieldCssClasses;
            }
            set
            {
                this.WebControlFunctionality.FieldCssClasses = value;
            }
        }

        #endregion
    }
}
