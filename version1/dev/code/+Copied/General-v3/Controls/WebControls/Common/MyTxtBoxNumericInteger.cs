using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.General_v3.Controls.WebControls.Common
{

    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyTxtBoxNumericInteger runat=server></{0}:MyTxtBoxNumericInteger>")]

    public class MyTxtBoxNumericInteger : MyTxtBoxNumeric
    {
        public new string GetFormValueAsStr()
        {
            string s = null;
            if (this.FormValue != null)
            {
                s = this.FormValue.Value.ToString();
            }
            return s;
        }
        public MyTxtBoxNumericInteger()
            : base()
        {
            _functionality.IntegersOnly = true;
            
        }

        public MyTxtBoxNumericInteger(string ID, string Title, bool Required, string HelpMessage, int? numFrom, int? numTo, NUMERIC_RANGE allowedNumbers,
            int? width, int? value)
            :base()
        {
            if (width != null)
                this.Width = width.Value;
            if (value != null)
                this.SetValue(value);
            this.ID = this.StringID = ID;
            this.Title = Title;
            this.NumberFrom = numFrom;
            this.NumberTo = numTo;
            this.AllowedNumbers = allowedNumbers;
            this.Required = Required;
            this.HelpMsg = HelpMessage;
        }
        public override object Value
        {
            get
            {
                double? d = (double?)base.Value;
                int? num = null;
                if (d != null)
                    num = (int)d.Value;
                return num;
            }
            set
            {
                SetValue(value);
            }
        }
        public new int? NumberFrom
        {
            get
            {
                return (int?)base.NumberFrom;
            }
            set
            {
                base.NumberFrom = value;
            }
        }
        public new int? NumberTo
        {
            get
            {
                return (int?)base.NumberTo;
            }
            set
            {
                base.NumberTo = value;
            }
        }
        public new long? FormValue
        {
            get
            {
                long v = 0;
                if (base.FormValue != null)
                {
                    v = (long)base.FormValue.Value;
                    return v;
                }
                else
                {
                    return null;
                }
            }
        }
        public override object FormValueObject
        {
            get
            {
                return this.FormValue;
            }
        }
        public override Enums.DATA_TYPE DataType
        {
            get
            {
                return Enums.DATA_TYPE.Integer;
            }
        }

    }
}
