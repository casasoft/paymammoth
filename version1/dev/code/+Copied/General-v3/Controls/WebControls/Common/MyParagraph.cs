﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CS.General_v3.Controls.WebControls;
namespace CS.General_v3.Controls.WebControls.Common
{
    public class MyParagraph : MyInnerHtmlControl
    {
       
        public MyParagraph()
            : this(null)
        {

        }
        public MyParagraph(string text)
            : base("p")
        {
            this.InnerHtml = text;
        }
        

    }
}
