using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.General_v3.Controls.WebControls.Common
{

    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyTxtBoxTextMultiLine runat=server></{0}:MyTxtBoxTextMultiLine>")]

    public class MyTxtBoxTextMultiLine : MyTxtBoxTextString
    {
        public MyTxtBoxTextMultiLine() :
            base()
        {
            this.TextMode = TextBoxMode.MultiLine;
            
            
        }

        public MyTxtBoxTextMultiLine(string ID, string Title, bool Required, string HelpMessage, int Rows)
            : this(ID,Title,Required,HelpMessage,null,Rows,"")
        {

        }
        public MyTxtBoxTextMultiLine(string ID, string Title, bool Required, string HelpMessage, int? Width, int Rows, string text) :
            base()
        {
            if (Width != null)
                this.Width = Width.Value;
            this.ID = this.StringID = ID;
            this.Title = Title;
            this.TextMode = TextBoxMode.MultiLine;
            this.Rows = Rows;
            this.SetValue(text);
            this.Required = Required;
            this.HelpMsg = HelpMessage;
        }
      
    }
}
