using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CS.General_v3.Classes.CSS;
using System.IO;
using CS.General_v3.Controls.WebControls.Common.General;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;

namespace CS.General_v3.Controls.WebControls.Common
{


    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyCheckBox runat=server></{0}:MyCheckBox>")]
    public class MyCheckBox : CheckBox, General.IMyCheckBox
    {

        public int __GlobalCtrlIdentifier = Util.Other.GetNextControlGlobalIdentifier();

        public void FocusGood()
        {
            _functionality.FocusGood();
        }
        
        public override string ID
        {
            get
            {
                return base.ID;
            }
            set
            {
                base.ID = value;
            }
        }
        public string GetFormValueAsStr()
        {
            string s = "No";
            if (this.FormValue)
            {
                s = "Yes";
            }
            return s;
        }
        public string StringID { get; set; }
        protected General.MyFormWebControlFunctionality _functionality = null;
        public MyCheckBox()
            : base()
        
        {
            
            this.CssClass = "checkbox";
            init();
            
        }

        public MyCheckBox(string ID, string Title = null)
            : this(ID,Title,false)
        {
            this.CssClass = "checkbox";
        }
        public MyCheckBox(string ID, string Title, bool Checked)
            : base()
        {
            this.CssClass = "checkbox";
            init();
            this.ID= this.StringID = ID;
            this.Title = Title;
            this.SetValue(Checked);
        }
        private void init()
        {
            _functionality = new CS.General_v3.Controls.WebControls.Common.General.MyFormWebControlFunctionality(this, ClientIDSeparator);
            this.HasValidation = true;
            initCssManager();
        }
        private void initCssManager()
        {
            if (CssManager == null) CssManager = new CSSManagerForControl(this);
        }
        public CSSManagerForControl CssManager { get; private set; }

        public new string CssClass
        {
            get
            {
                initCssManager();
                return CssManager.ToString();
            }
            set
            {
                initCssManager();
                CssManager.Clear(); CssManager.AddClass(value);
            }
        }

        #region IMyFormWebControl Members

        [Category("Validation")]
        [DefaultValue(false)]
        [Description("Whether to validate control on blur or not")]
        public bool DoNotValidateOnBlur
        {
            get
            {
                return _functionality.DoNotValidateOnBlur;
            }

            set
            {
                _functionality.DoNotValidateOnBlur = value;

            }
        }
        public bool HasValidation
        {
            get
            {
                return _functionality.HasValidation;

            }
            set
            {
                _functionality.HasValidation = value;

            }
        }

        public virtual void SetValue(string o)
        {
            bool b = false;
            if (o is string)
            {
                string s = (string)o;
                s = s.ToLower();
                b = (s == "1" || s == "yes" || s == "true");
            }
            this.Checked = b;

        }
        public virtual void SetValue(object o)
        {
            /* The following code has been added, so as when the checbox is first 
             * initated and object is null; it is firstly set to false.
             */

            if (o == null)
            {
                o = false;
            }

            if (o is bool)
                SetValue((bool)o);
            else if (o is string)
                SetValue((string)o);
            else
                throw new InvalidOperationException("Invalid type");
        }
        public virtual void SetValue(bool o)
        {
            bool b = false;
                b = (bool)o;
            this.Checked = b;


        }

        public override string ValidationGroup
        {
            get
            {
                return _functionality.ValidationGroup;
            }
            set
            {
                _functionality.ValidationGroup = value;
            }
        }
        [Category("Title")]
        [DefaultValue("")]
        [Description("Title to show for validation")]
        public string Title
        {
            get
            {
                return _functionality.Title;
            }

            set
            {
                _functionality.Title = value;
                /*
                */
            }
        }
        /// <summary>
        /// The value retrieved from the posted Form
        /// </summary>
        public object FormValueObject
        {
            get
            {
                return FormValue;
            }

        }
        public object Value
        {
            get
            {
                return this.Checked;
            }
            set
            {
                SetValue(value);
            }
        }
        public bool FormValue
        {
            get { return _functionality.FormValueBool; }
        }

        #region classes
        [Category("Javascript")]
        [DefaultValue(false)]
        [Description("Whether to use default CSS classes or not")]
        public bool UseDefaultCSSClasses
        {
            get
            {
                return _functionality.UseDefaultCSSClasses;
            }

            set
            {
                _functionality.UseDefaultCSSClasses = value;

            }
        }
        public string Class_Disabled { get { return Functionality.Class_Disabled; } set { Functionality.Class_Disabled = value; } }
        public string Class_onFocus { get { return Functionality.Class_Focus; } set { Functionality.Class_Focus = value; } }
        public string Class_onError { get { return Functionality.Class_Error; } set { Functionality.Class_Error = value; } }
        public string Class_onOver { get { return Functionality.Class_Over; } set { Functionality.Class_Over = value; } }
        public string Class_ReadOnly { get { return Functionality.Class_ReadOnly; } set { Functionality.Class_ReadOnly = value; } }
        public string Class_Required { get { return Functionality.Class_Required; } set { Functionality.Class_Required = value; } }

        #endregion

        #endregion

        #region IMyWebControl Members

        public object Tag
        {
            get { return _functionality.Tag; }
            set { _functionality.Tag = value; }
        }

        public string OnClientClick { get { return _functionality.OnClientClick; } set { _functionality.OnClientClick = value; } }
        public string OnClientLoad { get { return _functionality.OnClientLoad; } set { _functionality.OnClientLoad = value; } }
        public string OnClientChange { get { return _functionality.OnClientChange; } set { _functionality.OnClientChange = value; } }
        public string OnClientBlur { get { return _functionality.OnClientBlur; } set { _functionality.OnClientBlur = value; } }
        public string HelpID
        {
            get
            {
                return _functionality.HelpID;
            }
            set
            {
                _functionality.HelpID = value;
            }
        }

        public string HelpMsg
        {
            get
            {
                return _functionality.HelpMsg;
            }
            set
            {
                _functionality.HelpMsg = value;
            }
        }

        public string RollOverClass { get { return _functionality.RollOverClass; } set { _functionality.RollOverClass = value; } }


        #endregion
        public string ClientName
        {
            get
            {
                return _functionality.ClientName;
            }
        }
        public int CellPadding { get; set; }
        public int CellSpacing { get; set; }
        public int? MarginText { get; set; }
        #region IMyCheckBox Members

        

        #endregion

        public bool Required { get { return _functionality.Required; } set { _functionality.Required = value; } }
        public WebControl Control
        {
            get { return this; }
        }

        public FormFieldsValidationParams.FormFieldValidationSubGroupParams SubGroupParams { get { return Functionality.SubGroupParams; } set { Functionality.SubGroupParams = value; } }


        protected override void Render(HtmlTextWriter writer)
        {
            base.CssClass = this.CssClass;
            base.Render(writer);

        }

        public Enums.DATA_TYPE DataType
        {
            get
            {
                return Enums.DATA_TYPE.Bool;
            }
        }
        public int? GetFormValueAsIntNullable()
        {
            throw new InvalidOperationException();
        }

        #region IBaseWebControl Members


        public MyFormWebControlFunctionality Functionality
        {
            get { return _functionality; }
            set { _functionality = value; }

        }

        #endregion

        #region IMyFormWebControl Members


        General.MyFormWebControlFunctionality General.IMyFormWebControl.WebControlFunctionality
        {
            get { return Functionality; }
            
        }

        Control IBaseControl.Control
        {
            get
            {
                return this.Control;
            }
        }

        BaseControlFunctionality IBaseControl.WebControlFunctionality
        {
            get
            {
                return this.Functionality;
            }
        }
        BaseWebControlFunctionality IBaseWebControl.WebControlFunctionality
        {
            get
            {
                return this.Functionality;
            }
        }
        #endregion

        #region IMyFormWebControl Members


        public FormFieldsValidationParams ValidationParams
        {
            get
            {
                return this.Functionality.ValidationParams;
            }
            set { this.Functionality.UpdateValidationParams(value); }
        }

        public FormFieldsCSSClasses FieldCssClasses
        {
            get
            {
                return this.Functionality.FieldCssClasses;
            }
            set
            {
                this.Functionality.FieldCssClasses = value;
            }
        }

        #endregion

        protected override void OnPreRender(EventArgs e)
        {
            this._functionality.ThrowErrorIfIDIsNull();
            base.OnPreRender(e);
        }
    }
}
