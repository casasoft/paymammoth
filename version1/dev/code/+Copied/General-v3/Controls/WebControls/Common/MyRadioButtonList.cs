using CS.General_v3.Classes.CSS;
using System;
using System.Collections.Specialized;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;

namespace CS.General_v3.Controls.WebControls.Common
{


    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyRadioButtonList runat=server></{0}:MyRadioButtonList>")]
    public class MyRadioButtonList : RadioButtonList, General.IMyFormWebControl
    {
        public void FocusGood()
        {
            _functionality.FocusGood();
            
        }
        
        public override string ID
        {
            get
            {
                return base.ID;
            }
            set
            {
                base.ID = value;
            }
        }
        public string GetFormValueAsStr()
        {
            return this.FormValue;
        }
        public string StringID { get; set; }
        protected General.MyFormWebControlFunctionality _functionality = null;
        public MyRadioButtonList()
            : base()
        {
            init();
            _functionality.AddFieldJS = false; // this is not a normal field
        }

        public void AddItemRange(IEnumerable<ListItem> collection)
        {
            foreach (ListItem item in collection)
            {
                this.Items.Add(item);
            }
        }

        public MyRadioButtonList(string ID, string Title, bool Required)
            : base()
        {
            init();
            this.StringID = ID;
            this.Title = Title;
            this.Required = Required;
        }
        private void init()
        {
            _functionality = new CS.General_v3.Controls.WebControls.Common.General.MyFormWebControlFunctionality(this, ClientIDSeparator);
            CssManager.AddClass("radio-button-list");
            this.HasValidation = true;
        }
        public string ClientName
        {
            get
            {
                
                return _functionality.ClientName;
            }
        }

        public CSSManagerForControl CssManager { get { return _functionality.CssManager; } }

       
        private string outputNVColl(NameValueCollection coll)
        {
            string str = "";
            IEnumerator enumerator = coll.Keys.GetEnumerator();
            while (enumerator.MoveNext())
            {
                string key = Convert.ToString(enumerator.Current);
                str += " " + key + "=\"" + coll[key] + "\"";
            }
            return str;
        }

        private void FixRender(HtmlTextWriter writer)
        {

            string strAttributes, myHTML;
            int index, endIndex;
            HtmlTextWriter htmlTW, liHtmlTW;
            StringWriter liSW, SW;
            StringBuilder liSB, SB;
            List<NameValueCollection> attributes = new List<NameValueCollection>();

            for (int i = 0; i < Items.Count; i++)
            {
                NameValueCollection list = new NameValueCollection();

                IEnumerator attribKeys = Items[i].Attributes.Keys.GetEnumerator();
                while (attribKeys.MoveNext())
                {
                    string key = Convert.ToString(attribKeys.Current);
                    list.Add(key, Items[i].Attributes[key]);
                }

                attributes.Add(list);
                Items[i].Attributes.Clear();
            }


            SB = new StringBuilder();
            SW = new StringWriter(SB);
            htmlTW = new HtmlTextWriter(SW);
            base.Render(htmlTW);
            myHTML = SB.ToString();
            liSB = new StringBuilder();
            liSW = new StringWriter(liSB);
            liHtmlTW = new HtmlTextWriter(liSW);
            index = 0;
            for (int idx = 0; idx < attributes.Count; idx++)
            {
                string visibleVal = attributes[idx]["visible"];
                if (visibleVal != null) visibleVal = visibleVal.ToLower();
                if (visibleVal == "false")
                {
                    index = myHTML.IndexOf("<tr>", index);
                    endIndex = myHTML.IndexOf("</tr>", index) + "</tr>".Length;
                    myHTML = myHTML.Remove(index, (endIndex - index));
                }
                else
                {
                    index = (myHTML.IndexOf("<input ", index) + "<input ".Length);
                    liSB.Remove(0, liSB.Length);
                    liHtmlTW.Write(outputNVColl(attributes[idx]));
                    //attributes[idx].Render(liHtmlTW);
                    strAttributes = liSB.ToString() + " ";
                    myHTML = myHTML.Insert(index, strAttributes);

                }
            }
            string js = "js.com.cs."+CS.General_v3.Settings.Others.JAVASCRIPT_VERSION+".Controls.Form.MyRadioButton.updateRadioListInitialChecked('" + this.ClientID + "');";
            js = CS.General_v3.Util.JSUtilOld.GetDeferredJSScriptThroughJQuery(js);
            myHTML += "<script type='text/javascript'>"+js+"</script>";


            writer.Write(myHTML);
        }


        [Category("Validation")]
        [DefaultValue(false)]
        [Description("Whether to validate control on blur or not")]
        public bool DoNotValidateOnBlur
        {
            get
            {
                return _functionality.DoNotValidateOnBlur;
            }

            set
            {
                _functionality.DoNotValidateOnBlur = value;

            }
        }
        protected override void OnPreRender(EventArgs e)
        {
            _functionality.ThrowErrorIfIDIsNull();
            if (this.ReadOnly) {
                for (int i = 0; i < Items.Count; i++)
                {
                    Items[i].Enabled = false;
                }
            }
            base.OnPreRender(e);
        }
        protected override void Render(HtmlTextWriter writer)
        {
            CellPadding = CellSpacing = 0;
            if (!string.IsNullOrEmpty(OnClientChange))
            {
                for (int i = 0; i < Items.Count; i++)
                {
                    string tmp = OnClientChange;
                    if (Items[i].Attributes["onclick"] != null && Items[i].Attributes["onclick"].Length > 0)
                    {
                        if (Items[i].Attributes["onclick"].Substring(Items[i].Attributes["onclick"].Length - 1) != ";")
                            tmp = ";" + tmp;
                    }
                    if (tmp.Substring(tmp.Length - 1) != ";")
                        tmp += ";";
                    Items[i].Attributes["onclick"] += tmp;
                    //Items[i].Attributes["onclick"] += ";" + tmp + ";";
                }
            }
           // _myWebControl.Render(writer);
            FixRender(writer);

        }

        #region IMyFormWebControl Members
        public bool HasValidation
        {
            get
            {
                return _functionality.HasValidation;

            }
            set
            {
                _functionality.HasValidation = value;

            }
        }

        public virtual void SetValue(object o)
        {
            this.Text = _functionality.SetValue(o);


        }

        public override string ValidationGroup
        {
            get
            {
                return _functionality.ValidationGroup;
            }
            set
            {
                _functionality.ValidationGroup = value;
            }
        }
        [Category("Title")]
        [DefaultValue("")]
        [Description("Title to show for validation")]
        public string Title
        {
            get
            {
                return _functionality.Title;
            }

            set
            {
                _functionality.Title = value;
                /*
                */
            }
        }
        /// <summary>
        /// The value retrieved from the posted Form
        /// </summary>
        public object FormValueObject
        {
            get
            {
                return FormValue;
            }

        }
        public string FormValue
        {
            get
            {
                return _functionality.FormValue;
            }
        }
        public object Value
        {
            get
            {
                
                for (int i = 0; i < this.Items.Count; i++)
                {
                    ListItem item = Items[i];
                    if (item.Selected)
                    {
                        if (!String.IsNullOrEmpty(item.Value))
                        {
                            return item.Value;
                        }
                        else
                        {
                            return item.Text;
                        }
                    }
                }
                return null;
            }
            set
            {
                if (value == null) value = "";

                string val =  value.ToString();
                for (int i = 0; i < this.Items.Count; i++)
                {
                    ListItem item = Items[i];
                    if ((!String.IsNullOrEmpty(item.Value) && item.Value == val) || (String.IsNullOrEmpty(item.Value) && item.Text == val))
                    {
                        item.Selected = true;
                        break;
                    }
                }
                
            }
        }


        #region classes
        [Category("Javascript")]
        [DefaultValue(false)]
        [Description("Whether to use default CSS classes or not")]
        public bool UseDefaultCSSClasses
        {
            get
            {
                return _functionality.UseDefaultCSSClasses;
            }

            set
            {
                _functionality.UseDefaultCSSClasses = value;

            }
        }
        public string Class_Disabled { get { return _functionality.Class_Disabled; } set { _functionality.Class_Disabled = value; } }
        public string Class_onFocus { get { return _functionality.Class_Focus; } set { _functionality.Class_Focus = value; } }
        public string Class_onError { get { return _functionality.Class_Error; } set { _functionality.Class_Error = value; } }
        public string Class_onOver { get { return _functionality.Class_Over; } set { _functionality.Class_Over = value; } }
        public string Class_ReadOnly { get { return _functionality.Class_ReadOnly; } set { _functionality.Class_ReadOnly = value; } }
        public string Class_Required { get { return _functionality.Class_Required; } set { _functionality.Class_Required = value; } }

        #endregion

        #endregion

        #region IMyWebControl Members

        public object Tag
        {
            get { return _functionality.Tag; }
            set { _functionality.Tag = value; }
        }

        public string OnClientClick { get { return _functionality.OnClientClick; } set { _functionality.OnClientClick = value; } }
        public string OnClientLoad { get { return _functionality.OnClientLoad; } set { _functionality.OnClientLoad = value; } }
        public string OnClientChange { get { return _functionality.OnClientChange; } set { _functionality.OnClientChange = value; } }
        public string OnClientBlur { get { return _functionality.OnClientBlur; } set { _functionality.OnClientBlur = value; } }
        public string HelpID
        {
            get
            {
                return _functionality.HelpID;
            }
            set
            {
                _functionality.HelpID = value;
            }
        }

        public string HelpMsg
        {
            get
            {
                return _functionality.HelpMsg;
            }
            set
            {
                _functionality.HelpMsg = value;
            }
        }

        public string RollOverClass { get { return _functionality.RollOverClass; } set { _functionality.RollOverClass = value; } }


        #endregion

        public bool Required { get { return false; } set { _functionality.Required = false; } }

        public WebControl Control
        {
            get { return this; }
        }

        public FormFieldsValidationParams.FormFieldValidationSubGroupParams SubGroupParams { get { return WebControlFunctionality.SubGroupParams; } set { WebControlFunctionality.SubGroupParams = value; } }


        public Enums.DATA_TYPE DataType
        {
            get
            {
                return Enums.DATA_TYPE.String;
            }
        }
        public int? GetFormValueAsIntNullable()
        {
            string s = this.GetFormValueAsStr();
            int? result = null;
            int num;
            if (Int32.TryParse(s, out num))
                result = num;
            return result;

        }

        #region IMyFormWebControl Members


        public General.MyFormWebControlFunctionality WebControlFunctionality
        {
            get { return _functionality; }
        }

        #endregion

        #region IBaseWebControl Members


        BaseWebControlFunctionality IBaseWebControl.WebControlFunctionality
        {
            get { return _functionality; }
        }

        #endregion

        #region IBaseControl Members

        Control IBaseControl.Control
        {
            get { return this; }
        }

        BaseControlFunctionality IBaseControl.WebControlFunctionality
        {
            get { return _functionality; }
        }

        #endregion
        #region IMyFormWebControl Members


        public FormFieldsValidationParams ValidationParams
        {
            get
            {
                return this.WebControlFunctionality.ValidationParams;
            }
            set { this.WebControlFunctionality.UpdateValidationParams(value); }
        }

        public FormFieldsCSSClasses FieldCssClasses
        {
            get
            {
                return this.WebControlFunctionality.FieldCssClasses;
            }
            set
            {
                this.WebControlFunctionality.FieldCssClasses = value;
            }
        }

        #endregion

        #region IMyFormWebControl Members


        public bool ReadOnly
        {
            get;
            set;
        }

        #endregion
    }
}
