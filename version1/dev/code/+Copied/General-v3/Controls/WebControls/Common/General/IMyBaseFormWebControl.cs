﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;


namespace CS.General_v3.Controls.WebControls.Common.General
{
    public interface IMyBaseFormWebControl : IBaseWebControl
    {
        string Title { get; set; }

        string HelpID { get; set; }
        string HelpMsg { get; set; }

        string ValidationGroup { get; set; }
        string Class_Disabled { get; set; }
        string Class_onFocus { get; set; }
        string Class_onError { get; set; }
        string Class_onOver { get; set; }
        string Class_ReadOnly { get; set; }
        string Class_Required { get; set; }
        bool UseDefaultCSSClasses { get; set; }


    }
}
