/************************************************************************
 *  DropDownCheckList ASP.NET server control
 *
 *  written by Mike Ellison, 20-September-2005
 *
 ************************************************************************
 *  Copyright (c) 2005 Mike Ellison.  All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions are met:
 *  
 *    * Redistributions of source code must retain the above copyright notice, 
 *      this list of conditions, the following disclaimer, and the following
 *      acknowledgements. 
 *  	
 *    * Redistributions in binary form must reproduce the above copyright notice, 
 *      this list of conditions and the following disclaimer in the documentation 
 *  	and/or other materials provided with the distribution.
 *  	
 *    * The name of the author may not be used to endorse or promote products 
 *      derived from this software without specific prior written permission. 
 *  
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED 
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 *  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, 
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT 
 *  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY 
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  Acknowledgements:
 *      - The "shim" technique used to support Internet Explorer 6.x
 *        is courtesy Joe King of Coalesys, Inc., 
 *        http://dotnetjunkies.com/WebLog/jking/archive/2003/07/21/488.aspx
 *
 *      - Javascript functions findPosX and findPosY for locating objects absolutely
 *        are courtesy Peter-Paul Koch, http://www.quirksmode.org
 *  
 ************************************************************************/

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.IO;
using System.Collections.Specialized;

namespace CS.General_v3.Controls.WebControls.Common
{
    /// <summary>
    /// Creates a multi selection check box group which is displayed and hidden
    /// on the client through the use of a custom drop-down box; choices can be 
    /// dynamically created by binding the control to a data source
    /// </summary>
    /// 
    /// <remarks>
    /// <para>
    /// The <b>DropDownCheckList</b> was designed as a subclass of 
    /// <see cref="CheckBoxList">CheckBoxList</see>, with a client-side library intended to provide 
    /// cross-platform compatibility, at least with the modern versions of major browsers.  
    /// It has been specifically tested with Internet Explorer 6.x, Netscape 7.x and 8.x, 
    /// and Firefox 1.x.  The JavaScript file <i>DropDownCheckList.js</i> contains the client-side 
    /// object definition for the DropDownCheckList.  To use the control, first copy the 
    /// file <i>DropDownCheckList.js</i> to the following directory:
    /// <pre>wwwroot\aspnet_client\UNLV_IAP_WebControls\DropDownCheckList</pre>
    /// </para>
    /// 
    /// <para>
    /// Several properties are available to customize the display of the control.  Use the 
    /// <see cref="DisplayBoxCssClass">DisplayBoxCssClass</see> and/or 
    /// <see cref="DisplayBoxCssStyle">DisplayBoxCssStyle</see> properties to specify CSS 
    /// styling for the display box.  Likewise, <see cref="DisplayTextCssClass">DisplayTextCssClass</see> 
    /// and <see cref="DisplayTextCssStyle">DisplayTextCssStyle</see> specify the CSS styling for 
    /// the display text, and <see cref="CheckListCssClass">CheckListCssClass</see> 
    /// and <see cref="CheckListCssStyle">CheckListCssStyle</see> denote CSS styling 
    /// for the checklist box.
    /// </para>
    /// 
    /// <para>
    /// The rendering of a drop-down image in the display box is dependent on the 
    /// <see cref="DropImageSrc">DropImageSrc</see> and <see cref="DropImagePosition">DropImagePosition</see> 
    /// properties.  If <see cref="DropImageSrc">DropImageSrc</see> is blank, or 
    /// <see cref="DropImagePosition">DropImagePosition</see> is set to <b>NoImage</b>, no image 
    /// is displayed in the drop-down box.  If <see cref="DropImageSrc">DropImageSrc</see> 
    /// is specified, the position of the image will either be <b>Left</b>, <b>Right</b>, or <b>Both</b>, 
    /// as indicated by <see cref="DropImagePosition">DropImagePosition</see>.
    /// </para>
    ///  
    /// <para>
    /// The control's behavior is defined through several additional properties.  
    /// The <see cref="DropDownMode">DropDownMode</see> property determines how the 
    /// checklist will display.  If set to <b>Inline</b>, the checklist will expand 
    /// within the surrounding HTML.  If set to <b>OnTop</b> or <b>OnTopWithShim</b>, 
    /// the checklist will be absolutely positioned on top of other HTML content.  
    /// The "shim" option exists to offer compatibility with Internet Explorer 6.x and 
    /// provides the ability to drop-down a checklist on top of windowed controls in IE.  
    /// </para>
    /// 
    /// <para>
    /// The <see cref="Separator">Separator</see> property indicates the character(s) used 
    /// to separate listed items.  <see cref="DisplayTextWidth">DisplayTextWidth</see> specifies 
    /// the maximum width in pixels available in the display box for listing checked options.  
    /// When the listed options exceed this width, the text is truncated and the 
    /// <see cref="TruncateString">TruncateString</see> property is appended.  Typically, 
    /// <see cref="TruncateString">TruncateString</see> is an ellipsis (�) but may be set 
    /// to other text or a blank string if desired.  If you prefer to allow the display box 
    /// to expand rather than truncate the text, set 
    /// <see cref="DisplayTextWidth">DisplayTextWidth</see> to -1.  
    /// </para>
    /// 
    /// <para>
    /// To indicate text to display when no options are checked, set the 
    /// <see cref="TextWhenNoneChecked">TextWhenNoneChecked</see> property to the desired string.   
    /// You may also specify whether checkbox labels or values (the Text and Value properties 
    /// of a given ListItem respectively) are listed in the display box by setting the 
    /// <see cref="DisplayTextList">DisplayTextList</see> property to either <b>Labels</b> 
    /// or <b>Values</b>.
    /// </para>
    ///  
    /// <para>
    /// As a subclass of <see cref="CheckBoxList">CheckBoxList</see>, databinding properties such as 
    /// <see cref="ListControl.DataSource">DataSource</see> and the <see cref="ListControl.Items">Items</see> 
    /// collection are inherited.  Upon form submission, the developer may inspect the 
    /// <see cref="ListControl.Items">Items</see> collection for selected checkboxes, just as one would with 
    /// a <see cref="CheckBoxList">CheckBoxList</see>.  The DropDownCheckList also exposes two 
    /// overloaded utility methods:  
    /// <see cref="SelectedLabelsToString">SelectedLabelsToString()</see> and 
    /// <see cref="SelectedValuesToString">SelectedValuesToString()</see>.  
    /// Each returns the selected items, listed as a single concatenated string.  
    /// The overloads allow the developer to specify list separators and text delimiters.
    /// </para>
    /// </remarks>
    /// 
    /// <example>
    /// Here is an example of a complete .aspx page which uses the DropDownCheckList control, 
    /// demonstrating several properties and the methods
    /// <see cref="SelectedLabelsToString">SelectedLabelsToString()</see> 
    /// and <see cref="SelectedValuesToString">SelectedValuesToString()</see>.  
    /// <pre><![CDATA[
    /// <%@ Page Language="c#" AutoEventWireup="true" %>
    /// <%@ Register TagPrefix="cc1" 
    ///              Namespace="UNLV.IAP.WebControls" 
    ///              Assembly="DropDownCheckList" %>
    /// 
    /// <script runat="server">
    ///     void Page_Load(object o, EventArgs e)
    ///     {
    ///         lblResults.Text = "";
    ///     }
    ///     
    ///     void btnSubmit_Click(object o, EventArgs e)
    ///     {
    ///         string sLabels = dd.SelectedLabelsToString(", ");
    ///         string sValues = dd.SelectedValuesToString(", ", "'");
    ///         
    ///         lblResults.Text = "Selected Items:  " + sLabels
    ///           + "<br />"
    ///           + "Values: " + sValues;
    ///     }
    ///     
    ///     void btnClear_Click(object o, EventArgs e)
    ///     {
    ///         dd.SelectedValue = null;
    ///     }
    ///     
    /// </script>
    /// 
    /// 
    /// <html>
    ///   <head>
    ///     <title>DropDownCheckList Sample</title>
    ///     <style>
    ///       .boxStyle
    ///       {
    ///         border           : 2px solid darkBlue;
    ///         background-color : lightBlue;
    ///         padding          : 8px;
    ///       }
    ///     </style>
    ///   </head>
    ///   
    ///   <body>
    ///     <form runat="server">
    ///     
    ///         <h3>DropDownCheckList Sample</h3>
    ///         <p>Click the drop-down box to select options</p>
    ///         
    ///         <cc1:DropDownCheckList id="dd" runat="server" 
    ///                 RepeatColumns       = "2"
    ///                 DropImageSrc        = "dropImage.gif"
    ///                 DropImagePosition   = "Right"
    ///                 DropDownMode        = "OnTopWithShim"
    ///                 CheckListCssClass   = "boxStyle"
    ///                 CheckListCssStyle   = ""
    ///                 DisplayTextCssStyle = "font-family: Tahoma;"
    ///                 DisplayTextWidth    = "180"
    ///                 DisplayTextList     = "Labels"
    ///                 Separator           = ", "
    ///                 TruncateString      = "..."
    ///                 TextWhenNoneChecked = "--select--"
    ///             >
    ///             <asp:ListItem text="North" value="N" />
    ///             <asp:ListItem text="South" value="S" />
    ///             <asp:ListItem text="East"  value="E" />
    ///             <asp:ListItem text="West"  value="W" />
    ///             <asp:ListItem text="Northeast" value="NE" />
    ///             <asp:ListItem text="Southeast" value="SE" />
    ///             <asp:ListItem text="Northwest" value="NW" />
    ///             <asp:ListItem text="Southwest" value="SW" />            
    ///         </cc1:DropDownCheckList>      
    ///         
    ///         <p>
    ///             <asp:Button id="btnSubmit" runat="server" text="Submit Choices"
    ///                         onClick="btnSubmit_Click" />
    /// 
    ///             <asp:Button id="btnClear" runat="server" text="Clear Choices"
    ///                         onClick="btnClear_Click" />
    ///         </p>
    ///         
    ///         <p><asp:Label id="lblResults" runat="server" /></p>
    ///     
    ///     </form>
    ///   </body>
    ///   
    /// </html>
    /// ]]></pre>
    /// </example>
    [ToolboxData("<{0}:MyDropDownCheckList runat=server></{0}:MyDropDownCheckList>")
    , DefaultProperty("TextWhenNoneChecked")
    ]
    public class MyDropDownCheckList : System.Web.UI.WebControls.CheckBoxList
    {
        #region Inner Classes
        /// <summary>
        /// Utility class for working with Cascading Style Sheet (CSS)
        /// style attributes
        /// </summary>
        /// <remarks>
        /// The <b>CssStyleUtility</b> class encapsulates a
        /// <see cref="StringDictionary">StringDictionary</see>, allowing for
        /// the setting and retrieval of CSS style properties.  The constructor
        /// takes a string parameter, representing the text in a style attribute,
        /// and parses the string to populate the internal StringDictionary.
        /// The <see cref="StyleTable">StyleTable</see> property provides access
        /// to the internal dictionary, and the <see cref="ToString">ToString()</see>
        /// method outputs the items of the dictionary as a complete CSS style string.
        /// </remarks>
        public class CssStyleUtility
        {
            // this class would probably not be necessary if we could
            // use the CssStyleCollection instead; too bad we can't.
            // we'll use this class to assist in handling css style
            // properties, particularly when we need to add our own
            // style attributes

            private StringDictionary _table = new StringDictionary();

            /// <summary>
            /// Returns the internal dictionary of style properties
            /// encapsulated within this CssStyleUtility object
            /// </summary>
            public StringDictionary StyleTable
            {
                get { return _table; }
            }

            /// <summary>
            /// Constructs a CssStyleUtility object, given a string of text
            /// in the form of a CSS style attribute (in the form of
            /// "key1: value1; key2: value2;" etc.)
            /// </summary>
            /// <param name="cssStyleString">the CSS style attribute text to be parsed</param>
            public CssStyleUtility(string cssStyleString)
            {
                // cssStyleString should be something like this:
                // "border: 1px solid gray; background-color: blue;"
                // parse it
                string[] arr = cssStyleString.Split(';');
                foreach (string sPair in arr)
                {
                    string[] arr2 = sPair.Split(':');
                    // valid key and value pair?
                    if (arr2.Length == 2)
                    {
                        // include in our internal hashtable
                        _table.Add(arr2[0].Trim().ToLower(), arr2[1].Trim());
                    }
                }

            }

            /// <summary>
            /// Joins the property items within this CssStyleUtility object
            /// to return a single text string in the form of a CSS style attribute
            /// </summary>
            /// <returns>the CSS style attribute text</returns>
            public override string ToString()
            {
                string sReturn = "";
                foreach (string key in _table.Keys)
                {
                    if (sReturn.Length > 0) sReturn += "; ";
                    sReturn += string.Format("{0}: {1}", key, _table[key]);
                }
                return sReturn;
            }

        }
        #endregion
        #region Enums
        /// <summary>
        /// Supplies options for determining if labels or values will be listed in 
        /// the display box for checked boxes.
        /// </summary>
        public enum DisplayTextListEnum
        {
            /// <summary>
            /// Indicates that labels should be listed in the display text for checked boxes.
            /// </summary>
            Labels = 0,

            /// <summary>
            /// Indicates that values should be listed in the display text for checked boxes.
            /// </summary>
            Values = 1,

        }
        /// <summary>
        /// Supplies options for determining if the checklist will drop down inline with other
        /// HTML items on the page, or on top of other HTML content.
        /// </summary>
        public enum DropDownModeEnum
        {
            /// <summary>
            /// Renders the checklist using a CSS "position: relative" style attribute, placing
            /// the checklist inline with other HTML content
            /// </summary>
            Inline = 0,

            /// <summary>
            /// Renders the checklist using a CSS "position: absolute" style attribute, placing
            /// the checklist on top of other HTML content
            /// </summary>
            OnTop = 1,

            /// <summary>
            /// Renders the checklist using a CSS "position: absolute" style attribute, with
            /// an additional &lt;iframe&gt; tag which acts as a shim; this allows for Internet
            /// Explorer versions 5.5 and greater to properly render the checklist above 
            /// other windowed controls.
            /// </summary>
            OnTopWithShim = 2
        }
        /// <summary>
        /// Supplies options for specifying the position for rendering the drop-down image
        /// in a DropDownCheckList control, relative to the control's display box.
        /// </summary>
        public enum DropImagePositionEnum
        {
            /// <summary>
            /// Positions the drop-down image to the left of the display box
            /// </summary>
            Left = 0,

            /// <summary>
            /// Positions the drop-down image to the right of the display box
            /// </summary>
            Right = 1,

            /// <summary>
            /// Renders the drop-down image both left and right of the display box
            /// </summary>
            Both = 2,

            /// <summary>
            /// Specifies a drop-down image should not be rendered
            /// </summary>
            NoImage = 3
        }
        #endregion
        #region Constants
        private const string kLOCATION_JSCLIENTCODE = "/includes/DropDownCheckList.js";

        private const string kCATEGORY_DISPLAYBOX = "DisplayBox";
        private const string kCATEGORY_DISPLAYTEXT = "DisplayText";
        private const string kCATEGORY_CHECKLIST = "CheckList";
        private const string kCATEGORY_CONFIGURATION = "Configuration";
        private const string kCATEGORY_OTHER = "Other Inherited CheckBoxList Properties";
        
        private const DropDownModeEnum kDEFAULT_DROPDOWNMODE = DropDownModeEnum.OnTopWithShim;
        private const string kDEFAULT_DISPLAYBOXCSSCLASS = "";
        private const string kDEFAULT_DISPLAYBOXCSSSTYLE = "border: 1px solid #9999CC; cursor: pointer;";
        private const string kDEFAULT_DROPIMGSRC = "";
        private const DropImagePositionEnum kDEFAULT_DROPIMAGEPOSITION = DropImagePositionEnum.Right;
        private const int kDEFAULT_DISPLAYTEXTWIDTH = 150;
        private const string kDEFAULT_CHECKLISTCSSCLASS = "";
        private const string kDEFAULT_CHECKLISTCSSSTYLE = "overflow: auto; border: 1px solid black; padding: 4px; background-color: #EFEFEF;";
        private const string kDEFAULT_TRUNCATESTRING = "...";
        private const string kDEFAULT_SEPARATOR = ", ";
        private const string kDEFAULT_TEXTWHENNONECHECKED = "";
        private const string kDEFAULT_DISPLAYTEXTCSSCLASS = "";
        private const string kDEFAULT_DISPLAYTEXTCSSSTYLE = "";
        private const int kDEFAULT_DISPLAYTEXTPADDINGBOTTOM = 0;
        private const int kDEFAULT_DISPLAYTEXTPADDINGTOP = 0;
        private const int kDEFAULT_DISPLAYTEXTPADDINGRIGHT = 4;
        private const int kDEFAULT_DISPLAYTEXTPADDINGLEFT = 4;
        private const DisplayTextListEnum kDEFAULT_DISPLAYTEXTLIST = DisplayTextListEnum.Labels;

        #endregion

        #region Private Members

        private DropDownModeEnum _dropDownMode = kDEFAULT_DROPDOWNMODE;
        private string _displayBoxCssClass = kDEFAULT_DISPLAYBOXCSSCLASS;
        private string _displayBoxCssStyle = kDEFAULT_DISPLAYBOXCSSSTYLE;
        private string _dropImageSrc = kDEFAULT_DROPIMGSRC;
        private DropImagePositionEnum _dropImagePosition = kDEFAULT_DROPIMAGEPOSITION;
        private int _displayTextWidth = kDEFAULT_DISPLAYTEXTWIDTH;
        private string _checkListCssClass = kDEFAULT_CHECKLISTCSSCLASS;
        private string _checkListCssStyle = kDEFAULT_CHECKLISTCSSSTYLE;
        private string _clientCodeLocation = kLOCATION_JSCLIENTCODE;
        private string _truncateString = kDEFAULT_TRUNCATESTRING;
        private string _separator = kDEFAULT_SEPARATOR;
        private string _textWhenNoneChecked = kDEFAULT_TEXTWHENNONECHECKED;
        private string _displayTextCssClass = kDEFAULT_DISPLAYTEXTCSSCLASS;
        private string _displayTextCssStyle = kDEFAULT_DISPLAYTEXTCSSSTYLE;
        private int _displayTextPaddingBottom = kDEFAULT_DISPLAYTEXTPADDINGBOTTOM;
        private int _displayTextPaddingRight = kDEFAULT_DISPLAYTEXTPADDINGRIGHT;
        private int _displayTextPaddingTop = kDEFAULT_DISPLAYTEXTPADDINGTOP;
        private int _displayTextPaddingLeft = kDEFAULT_DISPLAYTEXTPADDINGLEFT;
        private DisplayTextListEnum _displayTextList = kDEFAULT_DISPLAYTEXTLIST;

        #endregion

        #region DisplayBox properties

        /// <summary>
        /// Specifies whether the drop-down checklist will appear inline or on top of other
        /// HTML content
        /// </summary>
        [Browsable(true), Category(kCATEGORY_DISPLAYBOX),
        DefaultValue(kDEFAULT_DROPDOWNMODE)]
        public DropDownModeEnum DropDownMode
        {
            get { return _dropDownMode; }
            set { _dropDownMode = value; }
        }

        /// <summary>
        /// Gets or sets the Cascading Style Sheet (CSS) class used when rendering
        /// the display box
        /// </summary>
        [Browsable(true), Category(kCATEGORY_DISPLAYBOX),
        DefaultValue(kDEFAULT_DISPLAYBOXCSSCLASS)]
        public string DisplayBoxCssClass
        {
            get { return _displayBoxCssClass; }
            set { _displayBoxCssClass = value; }
        }

        /// <summary>
        /// Gets or sets the Cascading Style Sheet (CSS) style attribute used
        /// when rendering the display box
        /// </summary>
        [Browsable(true), Category(kCATEGORY_DISPLAYBOX),
        DefaultValue(kDEFAULT_DISPLAYBOXCSSSTYLE)]
        public string DisplayBoxCssStyle
        {
            get { return _displayBoxCssStyle; }
            set { _displayBoxCssStyle = value; }
        }


        /// <summary>
        /// Gets or sets the <b>src</b> attribute of the image that is rendered when 
        /// <see cref="DropImagePosition">DropImagePosition</see> is set to
        /// <see cref="DropImagePositionEnum.Left">Left</see>,
        /// <see cref="DropImagePositionEnum.Right">Right</see>, or
        /// <see cref="DropImagePositionEnum.Both">Both</see>
        /// </summary>
        /// <remarks>
        /// By default, this property is <see cref="String.Empty">String.Empty</see>,
        /// which means that no image is rendered.
        /// </remarks>
        [Browsable(true), Category(kCATEGORY_DISPLAYBOX),
        DefaultValue(kDEFAULT_DROPIMGSRC)]
        public string DropImageSrc
        {
            get { return _dropImageSrc; }
            set { _dropImageSrc = value; }
        }

        /// <summary>
        /// Gets or sets the position relative to the display box where the 
        /// drop-down image is rendered
        /// </summary>
        /// <remarks>
        /// To specify the <b>src</b> attribute for the drop-down image,
        /// set the <see cref="DropImageSrc">DropImageSrc</see> property.
        /// </remarks>
        [Browsable(true), Category(kCATEGORY_DISPLAYBOX),
        DefaultValue(kDEFAULT_DROPIMAGEPOSITION)]
        public DropImagePositionEnum DropImagePosition
        {
            get { return _dropImagePosition; }
            set { _dropImagePosition = value; }
        }

        #endregion

        #region DisplayText properties

        /// <summary>
        /// Gets or sets the maximum width in pixels of text in the display box
        /// </summary>
        /// <remarks>
        /// When the checked boxes make a string of text that exceeds the
        /// <code>DisplayTextWidth</code> amount in width, the display text
        /// is truncated to the maximum width, and the 
        /// <see cref="TruncateString">TruncateString</see> is appended 
        /// (by default, an ellipsis).  To allow the string to expand to its
        /// full width without truncating, set <code>DisplayTextWidth</code>
        /// to -1.
        /// </remarks>
        [Browsable(true), Category(kCATEGORY_DISPLAYTEXT),
        DefaultValue(kDEFAULT_DISPLAYTEXTWIDTH)]
        public int DisplayTextWidth
        {
            get { return _displayTextWidth; }
            set { _displayTextWidth = value; }
        }


        /// <summary>
        /// Gets or sets the amount of pixels to pad between the display text and the
        /// left edge of the display box (or the drop-down image if present)
        /// </summary>
        [Browsable(true), Category(kCATEGORY_DISPLAYTEXT),
        DefaultValue(kDEFAULT_DISPLAYTEXTPADDINGLEFT)]
        public int DisplayTextPaddingLeft
        {
            get { return _displayTextPaddingLeft; }
            set { _displayTextPaddingLeft = value; }
        }


        /// <summary>
        /// Gets or sets the amount of pixels to pad between the display text and the
        /// top edge of the display box
        /// </summary>
        [Browsable(true), Category(kCATEGORY_DISPLAYTEXT),
        DefaultValue(kDEFAULT_DISPLAYTEXTPADDINGTOP)]
        public int DisplayTextPaddingTop
        {
            get { return _displayTextPaddingTop; }
            set { _displayTextPaddingTop = value; }
        }


        /// <summary>
        /// Gets or sets the amount of pixels to pad between the display text and the
        /// right edge of the display box (or the drop-down image if present)
        /// </summary>
        [Browsable(true), Category(kCATEGORY_DISPLAYTEXT),
        DefaultValue(kDEFAULT_DISPLAYTEXTPADDINGRIGHT)]
        public int DisplayTextPaddingRight
        {
            get { return _displayTextPaddingRight; }
            set { _displayTextPaddingRight = value; }
        }


        /// <summary>
        /// Gets or sets the amount of pixels to pad between the display text and the
        /// bottom edge of the display box
        /// </summary>
        [Browsable(true), Category(kCATEGORY_DISPLAYTEXT),
        DefaultValue(kDEFAULT_DISPLAYTEXTPADDINGBOTTOM)]
        public int DisplayTextPaddingBottom
        {
            get { return _displayTextPaddingBottom; }
            set { _displayTextPaddingBottom = value; }
        }


        /// <summary>
        /// Gets or sets the Cascading Style Sheet (CSS) class used when rendering
        /// the display text		
        /// </summary>
        [Browsable(true), Category(kCATEGORY_DISPLAYTEXT),
        DefaultValue(kDEFAULT_DISPLAYTEXTCSSSTYLE)]
        public string DisplayTextCssStyle
        {
            get { return _displayTextCssStyle; }
            set { _displayTextCssStyle = value; }
        }


        /// <summary>
        /// Gets or sets the Cascading Style Sheet (CSS) style attribute used when rendering
        /// the display text		
        /// </summary>
        [Browsable(true), Category(kCATEGORY_DISPLAYTEXT),
        DefaultValue(kDEFAULT_DISPLAYTEXTCSSCLASS)]
        public string DisplayTextCssClass
        {
            get { return _displayTextCssClass; }
            set { _displayTextCssClass = value; }
        }


        /// <summary>
        /// Gets or sets the text to display when no items are checked		
        /// </summary>
        [Browsable(true), Category(kCATEGORY_DISPLAYTEXT),
        DefaultValue(kDEFAULT_TEXTWHENNONECHECKED)]
        public string TextWhenNoneChecked
        {
            get { return _textWhenNoneChecked; }
            set { _textWhenNoneChecked = value; }
        }


        /// <summary>
        /// Gets or sets the text to use as a separater when listing checked choices
        /// in the display text
        /// </summary>
        [Browsable(true), Category(kCATEGORY_DISPLAYTEXT),
        DefaultValue(kDEFAULT_SEPARATOR)]
        public string Separator
        {
            get { return _separator; }
            set { _separator = value; }
        }


        /// <summary>
        /// Gets or sets the text to append to the truncated display text 
        /// when the checked choices produce a string to wide for the display box
        /// </summary>
        [Browsable(true), Category(kCATEGORY_DISPLAYTEXT),
        DefaultValue(kDEFAULT_TRUNCATESTRING)]
        public string TruncateString
        {
            get { return _truncateString; }
            set { _truncateString = value; }
        }



        /// <summary>
        /// Gets or sets the option of displaying either labels or values
        /// for checked boxes
        /// </summary>
        [Browsable(true), Category(kCATEGORY_DISPLAYTEXT),
        DefaultValue(kDEFAULT_DISPLAYTEXTLIST)]
        public DisplayTextListEnum DisplayTextList
        {
            get { return _displayTextList; }
            set { _displayTextList = value; }
        }


        #endregion

        #region CheckList Properties

        /// <summary>
        /// Gets or sets the Cascading Style Sheet (CSS) class used when rendering
        /// the checklist box		
        /// </summary>
        [Browsable(true), Category(kCATEGORY_CHECKLIST),
        DefaultValue(kDEFAULT_CHECKLISTCSSCLASS)]
        public string CheckListCssClass
        {
            get { return _checkListCssClass; }
            set { _checkListCssClass = value; }
        }


        /// <summary>
        /// Gets or sets the Cascading Style Sheet (CSS) style attribute used when rendering
        /// the checklist box		
        /// </summary>
        [Browsable(true), Category(kCATEGORY_CHECKLIST),
        DefaultValue(kDEFAULT_CHECKLISTCSSSTYLE)]
        public string CheckListCssStyle
        {
            get { return _checkListCssStyle; }
            set { _checkListCssStyle = value; }
        }



        // other inherited properties of a CheckBoxList

        /// <summary>
        /// Inherited from <see cref="CheckBoxList.CellPadding">CheckBoxList</see>
        /// </summary>
        [Browsable(true), Category(kCATEGORY_OTHER),
        ]
        public override int CellPadding
        {
            get { return base.CellPadding; }
            set { base.CellPadding = value; }
        }


        /// <summary>
        /// Inherited from <see cref="CheckBoxList.CellSpacing">CheckBoxList</see>
        /// </summary>
        [Browsable(true), Category(kCATEGORY_OTHER),
        ]
        public override int CellSpacing
        {
            get { return base.CellSpacing; }
            set { base.CellSpacing = value; }
        }


        /// <summary>
        /// Inherited from <see cref="WebControl.Height">CheckBoxList</see>
        /// </summary>
        [Browsable(true), Category(kCATEGORY_OTHER),
        ]
        public override Unit Height
        {
            get { return base.Height; }
            set { base.Height = value; }
        }


        /// <summary>
        /// Inherited from <see cref="WebControl.Width">CheckBoxList</see>
        /// </summary>
        [Browsable(true), Category(kCATEGORY_OTHER),
        ]
        public override Unit Width
        {
            get { return base.Width; }
            set { base.Width = value; }
        }


        /// <summary>
        /// Inherited from <see cref="CheckBoxList.RepeatColumns">CheckBoxList</see>
        /// </summary>
        [Browsable(true), Category(kCATEGORY_CHECKLIST),
        ]
        public override int RepeatColumns
        {
            get { return base.RepeatColumns; }
            set { base.RepeatColumns = value; }
        }


        /// <summary>
        /// Inherited from <see cref="CheckBoxList.RepeatDirection">CheckBoxList</see>
        /// </summary>
        [Browsable(true), Category(kCATEGORY_CHECKLIST),
        ]
        public override RepeatDirection RepeatDirection
        {
            get { return base.RepeatDirection; }
            set { base.RepeatDirection = value; }
        }


        /// <summary>
        /// Inherited from <see cref="CheckBoxList.RepeatLayout">CheckBoxList</see>
        /// </summary>
        [Browsable(true), Category(kCATEGORY_CHECKLIST),
        ]
        public override RepeatLayout RepeatLayout
        {
            get { return base.RepeatLayout; }
            set { base.RepeatLayout = value; }
        }


        /// <summary>
        /// Inherited from <see cref="WebControl.BackColor">CheckBoxList</see>
        /// </summary>
        [Browsable(true), Category(kCATEGORY_OTHER),
        ]
        public override System.Drawing.Color BackColor
        {
            get { return base.BackColor; }
            set { base.BackColor = value; }
        }


        /// <summary>
        /// Inherited from <see cref="WebControl.BorderColor">CheckBoxList</see>
        /// </summary>
        [Browsable(true), Category(kCATEGORY_OTHER),
        ]
        public override System.Drawing.Color BorderColor
        {
            get { return base.BorderColor; }
            set { base.BorderColor = value; }
        }


        /// <summary>
        /// Inherited from <see cref="WebControl.BorderStyle">CheckBoxList</see>
        /// </summary>
        [Browsable(true), Category(kCATEGORY_OTHER),
        ]
        public override BorderStyle BorderStyle
        {
            get { return base.BorderStyle; }
            set { base.BorderStyle = value; }
        }


        /// <summary>
        /// Inherited from <see cref="WebControl.BorderWidth">CheckBoxList</see>
        /// </summary>
        [Browsable(true), Category(kCATEGORY_OTHER),
        ]
        public override Unit BorderWidth
        {
            get { return base.BorderWidth; }
            set { base.BorderWidth = value; }
        }


        /// <summary>
        /// Inherited from <see cref="WebControl.CssClass">CheckBoxList</see>
        /// </summary>
        [Browsable(true), Category(kCATEGORY_OTHER),
        ]
        public override string CssClass
        {
            get { return base.CssClass; }
            set { base.CssClass = value; }
        }


        /// <summary>
        /// Inherited from <see cref="WebControl.Font">CheckBoxList</see>
        /// </summary>
        [Browsable(true), Category(kCATEGORY_OTHER),
        ]
        public override FontInfo Font
        {
            get { return base.Font; }
        }


        /// <summary>
        /// Inherited from <see cref="WebControl.ForeColor">CheckBoxList</see>
        /// </summary>
        [Browsable(true), Category(kCATEGORY_OTHER),
        ]
        public override System.Drawing.Color ForeColor
        {
            get { return base.ForeColor; }
            set { base.ForeColor = value; }
        }


        /// <summary>
        /// Inherited from <see cref="CheckBoxList.TextAlign">CheckBoxList</see>
        /// </summary>
        [Browsable(true), Category(kCATEGORY_OTHER),
        ]
        public override TextAlign TextAlign
        {
            get { return base.TextAlign; }
            set { base.TextAlign = value; }
        }


        /// <summary>
        /// Inherited from <see cref="WebControl.AccessKey">CheckBoxList</see>
        /// </summary>
        [Browsable(true), Category(kCATEGORY_OTHER),
        ]
        public override string AccessKey
        {
            get { return base.AccessKey; }
            set { base.AccessKey = value; }
        }


        /// <summary>
        /// Inherited from <see cref="ListControl.AutoPostBack">CheckBoxList</see>
        /// </summary>
        [Browsable(true), Category(kCATEGORY_OTHER),
        ]
        public override bool AutoPostBack
        {
            get { return base.AutoPostBack; }
            set { base.AutoPostBack = value; }
        }


        /// <summary>
        /// Inherited from <see cref="WebControl.TabIndex">CheckBoxList</see>
        /// </summary>
        [Browsable(true), Category(kCATEGORY_OTHER),
        ]
        public override short TabIndex
        {
            get { return base.TabIndex; }
            set { base.TabIndex = value; }
        }


        /// <summary>
        /// Inherited from <see cref="WebControl.ToolTip">CheckBoxList</see>
        /// </summary>
        [Browsable(true), Category(kCATEGORY_OTHER),
        ]
        public override string ToolTip
        {
            get { return base.ToolTip; }
            set { base.ToolTip = value; }
        }


        #endregion

        #region Configuration Properties

        /// <summary>
        /// Returns or sets the location of the client JavaScript code file
        /// </summary>
        /// <remarks>
        /// <para>The <b>ClientCodeLocation</b> path is the url written as the
        /// <b>src</b> attribute of a &lt;script&gt; tag as the control
        /// is rendered.  By default, this location is 
        /// <b>/aspnet_client/UNLV_IAP_WebControls/DropDownCheckList/DropDownCheckList.js</b>
        /// </para>
        /// <para>
        /// If the client javascript file is not in the location specified by 
        /// <b>ClientCodeLocation</b>, client-side javascript errors will occur
        /// and the control will not function properly.
        /// </para>
        /// </remarks>
        [Browsable(true), Category(kCATEGORY_CONFIGURATION),
        DefaultValue(kLOCATION_JSCLIENTCODE)]
        public string ClientCodeLocation
        {
            get { return _clientCodeLocation; }
            set { _clientCodeLocation = value; }
        }


        #endregion

        #region Client-side Script Generation (OnPreRender)
        /// <summary>
        /// Generates a string containing a &lt;script&gt; tag identifying the
        /// client-side javascript code to include on the page; this string
        /// is registered with the page when a DropDownCheckList control is present.
        /// </summary>
        /// <returns>the &lt;script&gt; tag in a string</returns>
        protected string ClientJavascriptCodeScript()
        {
            string sSrc = this.ClientCodeLocation;
            string sTemplate = "<script language='JavaScript1.2' src='{0}' ></script>\n";
            return string.Format(sTemplate, sSrc);
        }

        /// <summary>
        /// Returns a consistent template for a client-side 
        /// &lt;script&gt; tag
        /// </summary>
        /// <returns>the string</returns>
        protected string GetScriptTemplate()
        {
            return "<script language='JavaScript1.2'>{0}</script>\n";
        }

        /// <summary>
        /// Returns a string of javascript code to create
        /// and initialize a DropDownCheckList object on the client;
        /// this script is registered with the Page.
        /// </summary>
        /// <returns>the javascript code</returns>
        protected string ClientInitializeScript()
        {
            string sId = this.ClientID;

            // client code to construct the object, according to the javascript constructor:
            // function DDCL_DropDownCheckList(id, textWhenNone, separator, truncateString, dropDownMode, allowExpand, displayList)
            // e.g.
            //var ddcl_obj_ddcl1 = new DDCL_DropDownCheckList('DDCL1', 'Select Items...', '; ', '...', DDCL_DROPDOWNMODE_ONTOPWITHSHIM, false, DDCL_DISPLAYTEXTLIST_LABELS);               

            string sConstruct = string.Format(
                "var ddcl_obj_{0} = new DDCL_DropDownCheckList('{0}', '{1}', '{2}', '{3}', {4}, {5}, {6});"
                , sId
                , _textWhenNoneChecked
                , _separator
                , _truncateString
                , Convert.ToInt32(_dropDownMode)
                , (_displayTextWidth > 0 ? "false" : "true")
                , Convert.ToInt32(_displayTextList)
                );

            return string.Format(GetScriptTemplate(), sConstruct);
        }


        /// <summary>
        /// Registers client-side scripting code with the Page
        /// </summary>
        /// <param name="e">Event arguments</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            // the main Javascript object should be rendered only
            // once no matter how many controls need it.
            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "DDCL_MAINCODE"
                , ClientJavascriptCodeScript());

            // for this control, generate the necessary 
            // client-side initialization script and register 
            // with the page
            this.Page.ClientScript.RegisterStartupScript(this.GetType(),
                this.ClientID + "_Initialize"
                , ClientInitializeScript());

        }
        #endregion

        #region Rendering

        /// <summary>
        /// Renders the drop-down image specified by 
        /// <see cref="DropImageSrc">DropImageSrc</see>
        /// </summary>
        /// <param name="output">the writer accepting the rendered output</param>
        protected void RenderDropImage(HtmlTextWriter output)
        {
            output.Write("<td>");

            output.Write("<img");
            output.WriteAttribute("id", this.ClientID + "_img");
            output.WriteAttribute("align", "absmiddle");
            output.WriteAttribute("src", _dropImageSrc);
            output.WriteAttribute("border", "0");
            output.Write("/>");

            output.Write("</td>");
        }

        /// <summary>
        /// Modifies the HTML code as rendered through the inheritence from CheckBoxList
        /// to add additional custom attributes to each checkbox
        /// </summary>
        /// <param name="sHtml">the rendered HTML for the CheckBoxList</param>
        /// <returns>the modified HTML as a string</returns>
        protected string ModifyRenderedCheckboxes(string sHtml)
        {

            // use a regular expression to identify in the form:
            //<input id="DropDownCheckList1_0" type="checkbox" 
            string s = string.Format(
                "input id=\"{0}_(?<index>\\d+)\"\\s+type=\"checkbox\"\\s+"
                , this.ClientID);

            Regex r = new Regex(s, RegexOptions.IgnoreCase);

            MatchCollection matches = r.Matches(sHtml);
            foreach (Match m in matches)
            {
                try
                {
                    int index = Convert.ToInt32(m.Groups["index"].Value);
                    sHtml = Regex.Replace(sHtml, m.Value, m.Value + " value=\"" + this.Items[index].Value + "\" ");
                }
                catch
                {  /* do nothing */ }
            }

            return sHtml;
        }


        /// <summary>
        /// Renders the DropDownCheckList control
        /// </summary>
        /// <param name="output">the writer accepting the rendered output</param>
        protected override void Render(HtmlTextWriter output)
        {

            // render the display box as an html table;
            // we're using .Write() here rather than .RenderBeginTag()
            // to ensure divs are treated as divs in all cases, and
            // that all attributes are written.

            // here is a sample of the HTML to be rendered:
            /*
                    <table id="DDCL2_displaybox" cellpadding="0" cellspacing="0" border="0" style="padding: 1px; border: 1px solid #9999CC; cursor: pointer;">
                      <tr>
                        <td width="148" style="padding-left: 4px;">              
                          <div id="DDCL2_boundingbox" style="overflow: hidden;">
                              <span id="DDCL2_text" style="font-family: Tahoma; font-size: 10pt;">
                              </span>
                          </div>
                        </td>
                        <td>
                          <img id="DDCL2_img" align="absmiddle" src="expand.gif" border="0" />
                        </td>
                      </tr>
                    </table>      
            */

            output.Write("<table");
            output.WriteAttribute("id", this.ClientID + "_displaybox");
            output.WriteAttribute("cellpadding", "0");
            output.WriteAttribute("cellspacing", "0");
            output.WriteAttribute("border", "0");
            if (_displayBoxCssClass != "")
                output.WriteAttribute("class", _displayBoxCssClass);
            if (_displayBoxCssStyle != "")
                output.WriteAttribute("style", _displayBoxCssStyle);
            output.Write(">");
            output.Write("<tr>");

            // render the image at the left?
            if ((_dropImagePosition == DropImagePositionEnum.Left
                || _dropImagePosition == DropImagePositionEnum.Both)
                && _dropImageSrc != "")
                RenderDropImage(output);


            // render the text box cell and padding
            output.Write("<td");
            if (_displayTextWidth > 0)
                output.WriteAttribute("width", _displayTextWidth.ToString());

            CssStyleUtility pads = new CssStyleUtility("");
            if (_displayTextPaddingLeft > -1)
                pads.StyleTable["padding-left"] = _displayTextPaddingLeft.ToString() + "px";
            if (_displayTextPaddingTop > -1)
                pads.StyleTable["padding-top"] = _displayTextPaddingTop.ToString() + "px";
            if (_displayTextPaddingRight > -1)
                pads.StyleTable["padding-right"] = _displayTextPaddingRight.ToString() + "px";
            if (_displayTextPaddingBottom > -1)
                pads.StyleTable["padding-bottom"] = _displayTextPaddingBottom.ToString() + "px";

            if (pads.StyleTable.Count > 0)
                output.WriteAttribute("style", pads.ToString());

            output.Write(">");

            // render the text box cell contents; start with the boundingbox
            // force this to be a div
            output.Write("<div");
            output.WriteAttribute("id", this.ClientID + "_boundingbox");
            if (_displayTextWidth > 0)
                output.WriteAttribute("style", "overflow: hidden;");
            output.Write(">");

            // next render the span that will contain the display text
            output.Write("<span");
            output.WriteAttribute("id", this.ClientID + "_text");
            if (_displayTextCssClass != "")
                output.WriteAttribute("class", _displayTextCssClass);
            if (_displayTextCssStyle != "")
                output.WriteAttribute("style", _displayTextCssStyle);
            output.Write(">");

            // we'll put some content in the span so Mozilla can get 
            // the boundingbox width correctly; typically this is
            // &nbsp; if we're in design mode though we would
            // want different text to display
            if (this.Context == null)
            {
                // this is design mode; there is no current HttpContext;
                // output either the TextWhenNone value, or the ID
                if (this.TextWhenNoneChecked != string.Empty)
                    output.Write(this.TextWhenNoneChecked);
                else
                    output.Write("[" + this.ID + "]");
            }
            else
            {
                // this is runtime; we have an HttpContext; 
                // output a simple &nbsp; so Mozilla can get the 
                // width of the bounding box correctly
                output.Write("&nbsp;");
            }

            output.Write("</span>");
            output.Write("</div>"); // </div>
            output.Write("</td>"); // </td>

            // render the image at the right?
            if ((_dropImagePosition == DropImagePositionEnum.Right
                || _dropImagePosition == DropImagePositionEnum.Both)
                && _dropImageSrc != "")
                RenderDropImage(output);


            output.Write("</tr>");
            output.Write("</table>");


            // now render the checklist div if this is runtime; 
            // start with appropriate style attributes
            if (this.Context != null)
            {
                output.Write("<div");
                output.WriteAttribute("id", this.ClientID + "_checkboxes");
                if (_checkListCssClass != "")
                    output.WriteAttribute("class", _checkListCssClass);

                CssStyleUtility css = new CssStyleUtility(_checkListCssStyle);
                css.StyleTable["display"] = "none";
                if (_dropDownMode == DropDownModeEnum.Inline)
                    css.StyleTable["position"] = "relative";
                else
                {
                    css.StyleTable["position"] = "absolute";
                    css.StyleTable["z-index"] = "32767";
                }
                output.WriteAttribute("style",
                    css.ToString());

                output.Write(">");

                // next, render the contents of the checkboxlist;
                // do we need to render values?
                if (_displayTextList == DisplayTextListEnum.Values)
                {
                    // if so, we need to render to a string first
                    // so we can include our own values attribute
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter wr = new HtmlTextWriter(sw);
                    base.Render(wr);
                    string sHtml = sw.ToString();
                    wr.Close();
                    sw.Close();
                    // now modify the code to include custom attributes				
                    sHtml = ModifyRenderedCheckboxes(sHtml);
                    // and write to the output stream
                    output.Write(sHtml);
                }
                else
                {
                    // if we're not rendering custom value attributes,
                    // just output the checkboxes to the output stream
                    base.Render(output);
                }

                // close off by rendering the ending div tag
                output.Write("</div>");

                // if we want the shim technique, render that now as an iframe
                if (_dropDownMode == DropDownModeEnum.OnTopWithShim)
                {
                    // force the write so all the attributes we need are written
                    output.Write("<iframe");
                    output.WriteAttribute("id", this.ClientID + "_shim");
                    output.WriteAttribute("src", "javascript: false;");
                    output.WriteAttribute("scrolling", "no");
                    output.WriteAttribute("frameborder", "0");
                    output.WriteAttribute("style", "position:absolute; top:0px; left:0px; display:none;");
                    output.Write(">");
                    output.Write("</iframe>");
                }

            }

        }

        #endregion

        #region Methods to retrieve selected choices as a string

        /// <summary>
        /// Generates a string of text labels from the selected items, using the
        /// control's <see cref="Separator">Separator</see> property to separate
        /// listed items
        /// </summary>
        /// <returns>the string of selected text labels</returns>
        public string SelectedLabelsToString()
        {
            return RenderSelectedItemsToString(true, this.Separator, "");
        }

        /// <summary>
        /// Generates a string of text labels from the selected items
        /// </summary>
        /// <param name="separator">text used to separate listed items</param>
        /// <returns>the string of selected values</returns>
        public string SelectedLabelsToString(string separator)
        {
            return RenderSelectedItemsToString(true, separator, "");
        }

        /// <summary>
        /// Generates a string of delimited text labels from the selected items
        /// </summary>
        /// <param name="separator">text used to separate listed items</param>
        /// <param name="delimiter">text used to delimit each listed item</param>
        /// <returns>the string of selected values</returns>
        public string SelectedLabelsToString(string separator, string delimiter)
        {
            
            return RenderSelectedItemsToString(true, separator, delimiter);
        }


        /// <summary>
        /// Generates a string of values from the selected items, using the
        /// control's <see cref="Separator">Separator</see> property to separate
        /// listed items
        /// </summary>
        /// <returns>the string of selected values</returns>
        public string SelectedValuesToString()
        {
            return RenderSelectedItemsToString(false, this.Separator, "");
        }

        /// <summary>
        /// Generates a string of values from the selected items
        /// </summary>
        /// <param name="separator">text used to separate listed items</param>
        /// <returns>the string of selected values</returns>
        public string SelectedValuesToString(string separator)
        {
            return RenderSelectedItemsToString(false, separator, "");
        }

        /// <summary>
        /// Generates a string of delimited values from the selected items
        /// </summary>
        /// <param name="separator">text used to separate listed items</param>
        /// <param name="delimiter">text used to delimit each listed item</param>
        /// <returns>the string of selected values</returns>
        public string SelectedValuesToString(string separator, string delimiter)
        {
            return RenderSelectedItemsToString(false, separator, delimiter);
        }


        /// <summary>
        /// Utility method for generating a string from the selected items
        /// </summary>
        /// <param name="bRenderLabel">true to use labels in the resultant string, false to use values </param>
        /// <param name="sep">the separator to use when listing items</param>
        /// <param name="delim">text delimiter to use </param>
        /// <returns>a string of items delimited with <i>sep</i></returns>
        protected string RenderSelectedItemsToString(bool bRenderLabel, string sep, string delim)
        {
            string sReturn = string.Empty;

            foreach (ListItem item in Items)
            {
                if (item.Selected)
                {
                    if (sReturn.Length > 0) sReturn += sep;
                    if (bRenderLabel)
                        sReturn += delim + item.Text + delim;
                    else
                        sReturn += delim + item.Value + delim;
                }
            }

            return sReturn;
        }

        #endregion

    }
}
