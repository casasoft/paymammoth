using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace CS.General_v3.Controls.WebControls.Common
{

    [DefaultProperty("Text")]
    [ToolboxData("<{0}:MyHtmlTableCell runat=server></{0}:MyHtmlTableCell>")]

    public class MyHtmlTableCell : HtmlTableCell, IBaseControl
    {

        public MyHtmlTableCell() 
        {

            _functionality = new BaseHtmlControlFunctionality(this);
        }

        #region IBaseWebControl Members
        private BaseHtmlControlFunctionality _functionality = null;
        public CS.General_v3.Classes.CSS.CSSManagerForControl CssManager
        {
            get { return _functionality.CssManager; }
        }
        public new string CssClass
        {
            get
            {
                return _functionality.CssClass;
            }
            set
            {
                _functionality.CssClass = value;
               // base.CssClass = _functionality.CssClass;
                
            }
        }
        protected override void OnPreRender(EventArgs e)
        {
            if (this.Controls.Count == 0)
            {
               // this.Controls.Add(new Literal() { Text = "&nbsp;" });
            }
            base.OnPreRender(e);
        }
       
        #endregion






        #region IBaseControl Members

        Control IBaseControl.Control { get { return this; } }

        BaseControlFunctionality IBaseControl.WebControlFunctionality { get { return _functionality; } }

        #endregion
    }
}
