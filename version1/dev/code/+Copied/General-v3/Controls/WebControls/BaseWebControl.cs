﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.CSS;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.Web.UI.WebControls ;
using CS.General_v3.JavaScript.jQuery.Plugins.Tooltips.QTip;
namespace CS.General_v3.Controls.WebControls
{
    using Classes.VisibleController;

    public class BaseWebControl : System.Web.UI.WebControls.WebControl, IBaseWebControl
    {
        private VisibleController __visibleController;
        private VisibleController _visibleController
        {
            get
            {
                if (__visibleController == null)
                {
                    __visibleController = new VisibleController(this);
                }
                return __visibleController;
            }
        }

        public int __GlobalCtrlIdentifier = Util.Other.GetNextControlGlobalIdentifier();

        private BaseWebControlFunctionality _webControlFunctionality = null;

        public BaseWebControl()
            : base()
        {
            init();
        }
        public BaseWebControl(System.Web.UI.HtmlTextWriterTag tagName)
            : base(tagName)
        {
            init();
        }
        public BaseWebControl(string tagName)
            : base(tagName)
        {
            init();
        }

        private void init()
        {
            _webControlFunctionality = getWebControlFunctionality();

        }

        protected virtual BaseWebControlFunctionality getWebControlFunctionality()
        {
            return new BaseWebControlFunctionality(this);
        }




        public CSSManagerForControl CssManager { get { return _webControlFunctionality.CssManager; } }

        public override string CssClass
        {
            get
            {
                if (_webControlFunctionality == null)
                    return base.CssClass;
                else
                    return _webControlFunctionality.CssClass;


            }
            set
            {
                _webControlFunctionality.CssClass = value;
                base.CssClass = _webControlFunctionality.CssClass;

            }


        }




        #region IBaseWebControl Members



        #endregion

        #region IBaseWebControl Members


        public BaseWebControlFunctionality WebControlFunctionality
        {
            get { return _webControlFunctionality; }
        }


        #endregion

        #region IBaseWebControl Members


        public string RollOverClass
        {
            get
            {
                return _webControlFunctionality.RollOverClass;
            }
            set
            {
                _webControlFunctionality.RollOverClass = value;
            }
        }

        public string StringID
        {
            get
            {
                return _webControlFunctionality.StringID;
            }
            set
            {
                _webControlFunctionality.StringID = value;

            }
        }

        public WebControl Control
        {
            get { return this; }
        }

        public string OnClientBlur
        {
            get
            {
                return _webControlFunctionality.OnClientBlur;
            }
            set
            {
                _webControlFunctionality.OnClientBlur = value;
            }
        }

        public string OnClientClick
        {
            get
            {
                return _webControlFunctionality.OnClientClick;
            }
            set
            {
                _webControlFunctionality.OnClientClick = value;
            }
        }

        public string OnClientChange
        {
            get
            {
                return _webControlFunctionality.OnClientChange;
            }
            set
            {
                _webControlFunctionality.OnClientChange = value;
            }
        }

        public string OnClientLoad
        {
            get
            {
                return _webControlFunctionality.OnClientLoad;
            }
            set
            {
                _webControlFunctionality.OnClientLoad = value;

            }
        }

        #endregion

        #region IBaseControl Members

        Control IBaseControl.Control
        {
            get { return this; }
        }

        BaseControlFunctionality IBaseControl.WebControlFunctionality
        {
            get { return (BaseControlFunctionality)this.WebControlFunctionality; }
        }

        #endregion
        protected override void OnPreRender(EventArgs e)
        {
            addTagComments();
            base.OnPreRender(e);
        }
        private void addTagComments()
        {
            /*if (CS.General_v3.Settings.Others.IsInLocalTesting)
            {

                Literal litBefore = new Literal();
                litBefore.Text = "<!-- " + CS.General_v3.Util.ReflectionUtil<object>.GetNameFromType(this.GetType()) + " {Start} -->";
                this.Controls.AddAt(0, litBefore);
                Literal litAfter = new Literal();
                litAfter.Text = "<!-- " + CS.General_v3.Util.ReflectionUtil<object>.GetNameFromType(this.GetType()) + " {End} -->";
                this.Controls.Add(litAfter);


            }*/
        }

        public new jQueryQTip ToolTip
        {
            get { return _webControlFunctionality.Tooltip; }
            set { _webControlFunctionality.Tooltip = value; }
        }

        public override bool Visible
        {
            get
            {
                return base.Visible;
            }
            set
            {
                base.Visible = value;
                _visibleController.SetVisible(value);
            }
        }
    }
}
