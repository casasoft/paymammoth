﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI;
using CS.General_20101215.Classes.CSS;
namespace CS.General_20101215.Controls.WebControls
{
    public class _baseWebControl
    {
        private void init()
        {
            this.CssManager = new CSSManager(this.Control);
        }
        public _baseWebControl(WebControl control)
        {
            this.Control  = control;
            this.Control.PreRender += new EventHandler(Control_PreRender);
            init();
        }

        private void checkRemoveHtmlLiteral()
        {
            if (_InnerHtmlLiteral != null &&
                ((this.Control.Controls.Count == 1 && this.Control.Controls[0] != _InnerHtmlLiteral) ||
                (this.Control.Controls.Count > 1)))
            {
                this.Control.Controls.Remove(_InnerHtmlLiteral);
            }
        }

        private void Control_PreRender(object sender, EventArgs e)
        {
            checkRemoveHtmlLiteral();
                
        }
        
        public WebControl Control { get; set; }

        public CSSManager CssManager { get; private set; }

        private string _CssClass = null;
        public string CssClass
        {
            get
            {
                return CssManager.ToString();
                    
            }
            set
            {
                CssManager.Clear(false);
                CssManager.AddClass(false, value);
            }
        }
        
        private string _InnerHtml = "";
        private Literal _InnerHtmlLiteral = null;
        private void updateHtmlLiteral(string s)
        {
            
            if ((this.Control.Controls.Count > 1) ||
                (_InnerHtmlLiteral == null && this.Control.Controls.Count > 0) ||
                (_InnerHtmlLiteral != null && this.Control.Controls.Count == 1 && this.Control.Controls[0] != _InnerHtmlLiteral))
            {
                throw new InvalidOperationException("In order to set InnerHtml/InnerText, you must not have any controls added to this control");
            }
            if (_InnerHtmlLiteral == null)
            {
                _InnerHtmlLiteral = new Literal();
                this.Control.Controls.Add(_InnerHtmlLiteral);
            }

            _InnerHtmlLiteral.Text = s;


        }
        public string InnerHtml
        {
            get
            {
                return _InnerHtml;
            }
            set
            {
                if (this.Control.Controls.Count > 0)
                {
                    int k = 5;
                }
                _InnerHtml = value;

                updateHtmlLiteral(_InnerHtml);
            }
        }
        public string InnerText
        {
            get
            {
                return System.Web.HttpUtility.HtmlDecode(InnerHtml);
            }
            set
            {
                InnerHtml = System.Web.HttpUtility.HtmlEncode(value);
            }
        }
        
    }
}
