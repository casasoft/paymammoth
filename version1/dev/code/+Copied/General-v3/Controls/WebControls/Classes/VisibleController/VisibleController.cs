﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace CS.General_v3.Controls.WebControls.Classes.VisibleController
{
    using System.Web;

    public class VisibleController
    { 
        private Control _prevHiddenParentControl;
        private int? _prevHiddenParentControlIndex;

        private Control _control;
        public VisibleController(Control control)
        {
            _control = control;
        }
        public void SetVisible(bool visible)
        {
            if (!visible)
            {
                //Hidden, remove it from DOM

                if (_control.Parent != null)
                {
                    try
                    {
                        _prevHiddenParentControlIndex = _control.Parent.Controls.IndexOf(_control);
                        _prevHiddenParentControl = _control.Parent;
                        CS.General_v3.Util.ControlUtil.RemoveControlFromParentContainer(_control);
                    }
                    catch (HttpException ex)
                    {
                        //Couldn't remove it due to current ASP.Net Lifecycle stage
                        _prevHiddenParentControlIndex = -1;
                        _prevHiddenParentControl = null;
                    }
                }
            }
            else
            {
                //If shown, and previously had a parent
                if (_prevHiddenParentControl != null && _prevHiddenParentControlIndex.HasValue)
                {
                    _prevHiddenParentControl.Controls.AddAt(_prevHiddenParentControlIndex.Value, _control);
                    _prevHiddenParentControl = null;
                    _prevHiddenParentControlIndex = null;
                }
            }
        }
    }
}
