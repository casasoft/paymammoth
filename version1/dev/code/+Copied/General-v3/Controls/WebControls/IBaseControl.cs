﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.CSS;
using System.Web.UI.WebControls;
using System.Web.UI;
namespace CS.General_v3.Controls.WebControls
{
    public interface IBaseControl 
    {
        
        

        
        Control Control { get; }
        BaseControlFunctionality WebControlFunctionality { get; }
    }
}
