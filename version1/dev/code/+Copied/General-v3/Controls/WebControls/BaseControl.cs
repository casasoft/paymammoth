﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.CSS;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.Web.UI.WebControls ;
namespace CS.General_v3.Controls.WebControls
{
    public class BaseControl : Control, IBaseControl
    {
        public int __GlobalCtrlIdentifier = Util.Other.GetNextControlGlobalIdentifier();

        private void addTagComments()
        {
            if (CS.General_v3.Settings.Others.IsInLocalTesting)
            {

                Literal litBefore = new Literal();
                litBefore.Text = "<!-- " + CS.General_v3.Util.ReflectionUtil.GetNameFromType(this.GetType()) + " {Start} -->";
                this.Controls.AddAt(0, litBefore);
                Literal litAfter = new Literal();
                litAfter.Text = "<!-- " + CS.General_v3.Util.ReflectionUtil.GetNameFromType(this.GetType()) + " {End} -->";
                this.Controls.Add(litAfter);


            }
        }
        protected override void OnPreRender(EventArgs e)
        {
            addTagComments();
            base.OnPreRender(e);
        }
        private BaseControlFunctionality _functionality = null;

        public BaseControl() : base() {
            init();
        }
        

        private void init()
        {
            _functionality = getFunctionality();
            
        }

        protected virtual BaseControlFunctionality getFunctionality()
        {
            return new BaseControlFunctionality(this);
        }





        #region IBaseControl Members

        public Control Control
        {
            get { return this; }
        }




        public BaseControlFunctionality WebControlFunctionality
        {
            get { return _functionality; }
        }

        #endregion
    }
}
