﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.General_20101215.Controls.WebControls.Common;
using CS.General_20101215.Classes.MediaItems;
using CS.General_20101215.Controls.WebControls.Specialized;
namespace CS.General_20101215.Controls.WebControls.General
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:NewsItemForm runat=server></{0}:NewsItemForm>")]
    public class NewsItemForm : WebControl
    {
        public NewsItemForm()
        {
            formFields = new CS.General_20101215.Controls.WebControls.Specialized.FormFields();
            this.Controls.Add(formFields);
            this.HasShortDescription = true;
            this.HasArchive = false;
        }
        public delegate void ShowMessageHandler(string message);
        public event ShowMessageHandler ShowMessage;
        public delegate void SaveEventHandler(bool AddAnother);
        public event SaveEventHandler Save;
        public bool HasShortDescription { get; set; }
        public bool HasArchive { get; set; }
        [Browsable(false)]
        public CS.General_20101215.Classes.NewsItems.INewsItem NewsItem { get; set; }
        
        
        public CS.General_20101215.Controls.WebControls.Specialized.FormFields formFields = null;
        public void RenderFormFields()
        {
            int FIELD_WIDTH = 300;
            if (NewsItem == null)
            {
                throw new InvalidOperationException("Please set 'NewsItem' of NewsItemForm");
            }

            formFields.AddDateTime("txtDatePosted", "Date Posted", true, null, FIELD_WIDTH, NewsItem.DatePosted);
            formFields.AddString("txtTitle", "Title", true, null, FIELD_WIDTH, NewsItem.Title);
            if (this.HasShortDescription)
                formFields.AddStringMultiline("txtShortDescription", "Short Description", true, null,6, FIELD_WIDTH, NewsItem.Title);
            formFields.AddStringMultiline("txtFullDescription", "Full Description", true, null,15, FIELD_WIDTH, NewsItem.Title);
            formFields.AddFileUploadWithMediaItem("txtImage", "Image", false, null, FIELD_WIDTH, NewsItem.Image,
                new FormFields.MediaItemHandler(deleteImage),null);
            if (this.HasArchive)
            {
                formFields.AddBool("chkArchive", "Archive", true, NewsItem.Archive);

            }
            if (NewsItem.ID <= 0)
            { // if new member
                formFields.AddBool("chkAddAnother", "Add Another?", false,true);
            }


            formFields.ClickSubmit += new EventHandler(formFields_ClickSubmit);
        }
        private void deleteImage(IMediaItem photo)
        {
            NewsItem.Image.Delete();
            if (ShowMessage != null)
                ShowMessage("Image has been deleted successfully");
            CS.General_20101215.Classes.URL.URLClass url = new CS.General_20101215.Classes.URL.URLClass();
            url.TransferTo();
        }
        void formFields_ClickSubmit(object sender, EventArgs e)
        {
            bool isEdit = NewsItem.ID > 0;
            NewsItem.Title = formFields.GetFormValueStr("txtTitle");
            if (this.HasShortDescription)
                NewsItem.ShortDescription = formFields.GetFormValueStr("txtShortDescription");
            if (this.HasArchive)
                NewsItem.Archive = formFields.GetFormValueBool("chkArchive");
            NewsItem.FullDescription = formFields.GetFormValueStr("txtFullDescription");
            NewsItem.DatePosted = (DateTime)formFields.GetFormValue("txtDatePosted");
            NewsItem.Save();
            MyFileUploadWithMediaItem txtUpload = (MyFileUploadWithMediaItem)formFields.GetControl("txtImage");
            if (txtUpload.FileContent.Length > 0)
            {
                NewsItem.Image.SetMediaItem(txtUpload.FileContent,txtUpload.FileName);
                NewsItem.Save();
            }
            bool addAnother = false;
            if (!isEdit)
                addAnother = formFields.GetFormValueBool("chkAddAnother");
            if (Save != null)
                Save(addAnother);
            
        }
        protected override void OnInit(EventArgs e)
        {
            
            base.OnInit(e);
        }
        
    }
}
