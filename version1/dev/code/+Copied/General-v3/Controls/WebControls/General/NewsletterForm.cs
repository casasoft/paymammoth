﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.General_20101215.Controls.WebControls.Common;
using CS.General_20101215.Controls.WebControls.Specialized;
namespace CS.General_20101215.Controls.WebControls.General
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:NewsletterForm runat=server></{0}:NewsletterForm>")]
    public class NewsletterForm : WebControl
    {
        public NewsletterForm()
        {
            formFields = new CS.General_20101215.Controls.WebControls.Specialized.FormFields(this.ID + "_newsletterformfields");
            this.Controls.Add(formFields);

        }
        public delegate void SaveEventHandler(bool AddAnother);
        public event SaveEventHandler Save;

        [Browsable(false)]
        public CS.General_20101215.Classes.Newsletter.INewsletterMember NewsletterMember { get; set; }
        
        
        public CS.General_20101215.Controls.WebControls.Specialized.FormFields formFields = null;
        public void RenderFormFields()
        {
            int FIELD_WIDTH = 300;
            if (NewsletterMember == null)
            {
                throw new InvalidOperationException("Please set newsletter member of NewsletterForm");
            }

            formFields.AddString("txtName","Name",true,null,FIELD_WIDTH,NewsletterMember.Name);
            formFields.AddString("txtEmail", "Email", true, null,FIELD_WIDTH, NewsletterMember.Email);
            if (NewsletterMember.ID <= 0)
            { // if new member
                formFields.AddBool("chkAddAnother", "Add Another?", false,true);
            }


            formFields.ClickSubmit += new EventHandler(formFields_ClickSubmit);
        }

        void formFields_ClickSubmit(object sender, EventArgs e)
        {
            bool isEdit = NewsletterMember.ID > 0;
            NewsletterMember.Name = formFields.GetFormValueStr("txtName");
            NewsletterMember.Email = formFields.GetFormValueStr("txtEmail");
            NewsletterMember.Save();
            bool addAnother = false;
            if (!isEdit)
                addAnother = formFields.GetFormValueBool("chkAddAnother");
            if (Save != null)
                Save(addAnother);
            
        }
        protected override void OnInit(EventArgs e)
        {
            
            base.OnInit(e);
        }
        
    }
}
