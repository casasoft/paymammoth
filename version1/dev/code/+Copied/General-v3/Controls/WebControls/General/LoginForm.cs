﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.General_20101215.Controls.WebControls.Common;
using CS.General_20101215.Controls.WebControls.Specialized;
namespace CS.General_20101215.Controls.WebControls.General
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:LoginForm runat=server></{0}:LoginForm>")]
    public class LoginForm : WebControl
    {
        public LoginForm()
        {
           
        }

        public delegate void LoginEventHandler(Enums.LOGIN_STATUS loginStatus,
            CS.General_20101215.Classes.Login.IUser loggedUser,
            string message);
        public event LoginEventHandler Login;

        [Browsable(false)]
        public CS.General_20101215.Classes.Login.WebUserSessionLoginManager UserLogin { get; set; }
        
        
        private CS.General_20101215.Controls.WebControls.Specialized.FormFields formFields = null;
        private void renderFormFields()
        {
            formFields = new CS.General_20101215.Controls.WebControls.Specialized.FormFields();
            formFields.ID = this.ID + "_loginForm";
            this.Controls.Add(formFields);

            formFields.AddString("txtUser","Username",true,null,null,null);
            formFields.AddString( Enums.STRING_DATA_TYPE.Password,"txtPass", "Password", true, null, null, null);


            formFields.ClickSubmit += new EventHandler(formFields_ClickSubmit);
        }

        void formFields_ClickSubmit(object sender, EventArgs e)
        {
            if (UserLogin == null)
                throw new InvalidOperationException("LoginForm:: Please set 'UserLogin' to an class that implements ILoginUser");
            if (Login == null)
                throw new InvalidOperationException("LoginForm:: Please attach to 'Login' event");
            string user = (string)formFields.GetFormValue("txtUser");
            string pass = (string)formFields.GetFormValue("txtPass");
            Enums.LOGIN_STATUS loginStatus = UserLogin.Login(user, pass, false, true);
            string msg = "";
            if (loginStatus == Enums.LOGIN_STATUS.Ok)
            {
                msg = "Logged in successfully";
            }
            else
            {
                msg = "Invalid username/password";

            }
            if (Login != null)
            {
                Login(loginStatus, UserLogin.GetLoggedInUser(), msg);
            }
            
        }
        protected override void OnInit(EventArgs e)
        {
            if (formFields == null)
                renderFormFields();
            base.OnInit(e);
        }
        
    }
}
