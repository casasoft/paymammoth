﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_20090518.Controls.WebControls.Common;

namespace CS.General_20090518.Controls.WebControls.Specialized.MediaGallery.v2
{
    public class MediaGalleryItemSections : MyDiv
    {
        public class FUNCTIONALITY
        {
            public event MediaGalleryItemSection.FUNCTIONALITY.UploadSectionItemsHandler OnUploadItems;

            private MediaGalleryItemSections _ui;

            public List<MediaGalleryItemSectionData> Sections { get; private set; }

            public FUNCTIONALITY(MediaGalleryItemSections ui)
            {
                _ui = ui;
                Sections = new List<MediaGalleryItemSectionData>();
            }

            private void createSections()
            {
                for (int i = 0; i < Sections.Count; i++)
                {
                    MediaGalleryItemSectionData data = Sections[i];
                    MediaGalleryItemSection sectionUI = new MediaGalleryItemSection();
                    sectionUI.Functionality.Data = data;
                    _ui.Controls.Add(sectionUI);

                    sectionUI.Functionality.OnUploadItems += new MediaGalleryItemSection.FUNCTIONALITY.UploadSectionItemsHandler(Functionality_OnUploadItems);
                }
            }

            void Functionality_OnUploadItems(List<MyFileUpload> fileUploads, MediaGalleryItemSection section)
            {
                if (OnUploadItems != null)
                {
                    OnUploadItems(fileUploads, section);
                }
            }
            public void Init()
            {
                createSections();
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public MediaGalleryItemSections()
        {
            Functionality = new FUNCTIONALITY(this);
            this.CssManager.AddClass("cs-media-gallery-sections");
        }
        protected override void OnLoad(EventArgs e)
        {
            Functionality.Init();
            base.OnLoad(e);
        }
    }
}
