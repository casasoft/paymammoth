﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web.UI;

using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Classes.SearchParams;

namespace CS.General_v3.Controls.WebControls.Specialized.Paging
{
    [ToolboxData("<{0}:PagingBar runat=server></{0}:PagingBar>")]
    public class PagingBar_TypeOne : CS.General_v3.Controls.WebControls.BaseWebControl
    {
        public class FUNCTIONALITY
        {
            
            private int _showAmtPages = 3;
            private string _nounSingular = "item";
            private string _nounPlural = "items";
            private string _noResultsText = "No Results";

            private string _qsVarPage = null;
            private string _qsVarShowAmt = null;


            private PagingBar_TypeOne _pagingBar;
            public FUNCTIONALITY(PagingBar_TypeOne pagingBar)
            {
                _pagingBar = pagingBar;
            }

            private void initDefaultShowAmts()
            {
                ShowAmts = new List<int>();
                ShowAmts.Add(10);
                ShowAmts.Add(25);
                ShowAmts.Add(50);
                ShowAmts.Add(100);
                ShowAmts.Add(250);
              //  ShowAmts.Add(Int32.MaxValue);
            }

            public void Init()
            {
                render();
            }

            private void initShowingItemsText(HtmlTableCell td)
            {
                td.Attributes["class"] = "cs-paging-bar-showing";
                if (AmtItems > 0)
                {
                    td.InnerHtml = "Showing ";
                    if (AmtItems == 1)
                    {
                        td.InnerHtml += _nounSingular;
                    }
                    else
                    {
                        td.InnerHtml += _nounPlural;
                    }
                    td.InnerHtml += " " + (this.GetFromItemIndex()) + " - " + (this.GetToItemIndex());
                    td.InnerHtml += " of " + TotalItems.ToString("#,#");
                }
                else
                {
                    td.InnerHtml = NoResultsText;
                }
            }

            private int GetToItemIndex()
            {
                long to = 0;
                to = (long)this.GetFromItemIndex() + (long)SelectedShowAmt;
                to--;
                if (to > this.TotalItems)
                    to = this.TotalItems;
                return (int) to;
            }


            private void initShowComboBox(HtmlTableCell td)
            {
                td.Attributes["class"] = "cs-paging-bar-combobox";
                if (ShowAmts == null) initDefaultShowAmts();


                MyDropDownList cmb = new MyDropDownList();
                cmb.ID = "cmbShowAmount" + "_" + this._pagingBar.ID;
                td.Controls.Add(cmb);
                cmb.OnChangeRedirectToValue = true;


                for (int i = 0; i < this.ShowAmts.Count; i++)
                {
                    CS.General_v3.Classes.URL.URLClass url = new General_v3.Classes.URL.URLClass();

                    
                    ListItem item = new ListItem();
                    int value = this.ShowAmts[i];
                    if (value == Int32.MaxValue)
                    {
                        item.Text = "Show ALL";
                    }
                    else
                    {
                        item.Text = "Show " + value.ToString();
                    }
                    url[QueryStringVarNameShowAmt] = value;
                    item.Value = url.ToString();
                    item.Selected = value == this.SelectedShowAmt;

                    cmb.Items.Add(item);

                }


                //string js = "new js.com.cms."+CS.General_v3.Settings.Others.JavaScript_CMS_Version+".ShowAmtComboBox('" + cmb.ClientID + "', '" + QueryStringVarNameShowAmt + "', '" + QueryStringVarNamePage + "');";


                //CS.General_v3.Util.PageUtil.GetCurrentPage().ClientScript.RegisterClientScriptBlock(this.GetType(), this._pagingBar.ClientID + "_cmbShow", js, true);




            }

            private void initPages(HtmlTableCell td)
            {
                td.Attributes["class"] = "cs-paging-bar-pages";
                NameValueCollection pages = new NameValueCollection();


                this.AmtPages = (int)Math.Ceiling((double)this.TotalItems / (double)this.SelectedShowAmt);
                pages.Add("<< First", SelectedPage == 1 ? null : "1");
                pages.Add("< Prev", SelectedPage == 1 ? null : (SelectedPage - 1).ToString());
                int startPageIndex = Math.Max(1, SelectedPage - ShowAmtPages);

                int endPageIndex = Math.Min(AmtPages, SelectedPage + ShowAmtPages);
                for (int i = startPageIndex; i <= endPageIndex; i++)
                {
                    pages.Add(i.ToString(), i.ToString());
                }
                pages.Add("Next >", SelectedPage == AmtPages ? null : (SelectedPage + 1).ToString());
                pages.Add("Last >> ", SelectedPage == AmtPages ? null : AmtPages.ToString());

                string html = "";
                for (int i = 0; i < pages.Count; i++)
                {
                    string label = pages.GetKey(i);
                    string pageValue = pages[i];
                    if (i > 0)
                    {
                        html += " | ";
                    }
                    HtmlAnchor a = new HtmlAnchor();

                    a.InnerHtml = label;




                    if (pageValue != null)
                    {

                        int pg = 0;
                        Int32.TryParse(pageValue, out pg);

                        if (pg == SelectedPage)
                        {
                            a.Attributes["class"] = "cs-paging-bar-selected";
                            a.Title = "Current Page - Page " + pageValue;
                        }
                        else
                        {
                            a.Title = "Go to page " + pageValue;

                            CS.General_v3.Classes.URL.URLClass url = new General_v3.Classes.URL.URLClass();
                            

                            url.QS[QueryStringVarNamePage] = pageValue;
                            a.HRef = url.ToString();
                        }
                        html += CS.General_v3.Util.ControlUtil.RenderControl(a, true);
                    }
                    else
                    {
                        HtmlGenericControl span = new HtmlGenericControl("span");
                        span.InnerHtml = label;
                        span.Attributes["class"] = "cs-paging-bar-disabled";
                        html += CS.General_v3.Util.ControlUtil.RenderControl(span, true);

                    }


                }
                td.InnerHtml = html;


            }


            private void render()
            {
                HtmlTable tbl = new HtmlTable();
                tbl.CellPadding = tbl.CellSpacing = 0;
                tbl.Attributes["class"] = "cs-paging-bar";
                this._pagingBar.Controls.Add(tbl);
                HtmlTableRow tr = new HtmlTableRow();
                tbl.Rows.Add(tr);
                tr.Cells.Add(new HtmlTableCell());
                tr.Cells.Add(new HtmlTableCell());
                tr.Cells.Add(new HtmlTableCell());

                initShowingItemsText(tr.Cells[0]);
                initPages(tr.Cells[1]);
                initShowComboBox(tr.Cells[2]);

            }

            /// <summary>
            /// The noun to which describe the items.  Defaults to 'Item'.  Used to show "Showing Items 1 - 10 of 100"
            /// </summary>
            public string NounSingular { get { return this._nounSingular; } set { this._nounSingular = value; } }
            /// <summary>
            /// The noun to which describe the items in plural.  Defaults to 'Items'.  Used to show "Showing Items 1 - 10 of 100"
            /// </summary>
            public string NounPlural { get { return this._nounPlural; } set { this._nounPlural = value; } }
            public int TotalItems { get; set; }
            /// <summary>
            /// 0 based index.  The amount passed here will be incremented by one.  
            /// </summary>
            public int GetFromItemIndex()
            {
                long from = 0;
                if (this.TotalItems > 0)
                {
                    from = ((long)(this.SelectedPage - 1) * (long)this.SelectedShowAmt) + 1;
                }
                return (int)from;

            }
            public int AmtItems { get; set; }
            /// <summary>
            /// The total amount of pages to show
            /// </summary>
            public int AmtPages { get; set; }

            /// <summary>
            /// The amount of pages to show.  Defaults to 3.   This will show 3 page numbers to the left and to the right.  
            /// Thus if you are for example in page 8, it will show 5 6 7 [8] 9 10 11.  etc..
            /// </summary>
            public int ShowAmtPages { get { return _showAmtPages; } set { this._showAmtPages = value; } }

            /// <summary>
            /// The key of the querystring variable to show for updating page.  By default it will set to the item's client ID with '_pg' appended at the end.
            /// This might be a bit ugly, but that guarantees uniqueness since there can be multiple paging in a page.  Change this to 'pg' if you want it to
            /// show something more nice and you only have one paging bar in the page.
            /// </summary>
            public string QueryStringVarNamePage
            {
                get
                {
                    if (_qsVarPage == null)
                    {
                        QueryStringVarNamePage = this._pagingBar.ClientID + "_pg";
                    }
                    return _qsVarPage;
                }
                set
                {
                    _qsVarPage = value;
                }

            }
            /// <summary>
            /// The key of the QS variable to show for updating show amt.  Defaults to the ClientID of listing with '_showAmt' appended to it.
            /// </summary>
            public string QueryStringVarNameShowAmt
            {
                get
                {
                    if (_qsVarShowAmt == null)
                    {
                        QueryStringVarNameShowAmt = this._pagingBar.ClientID + "_showAmt";
                    }
                    return _qsVarShowAmt;
                }
                set
                {
                    _qsVarShowAmt = value;
                }
            }
            private List<int> _ShowAmts = null;
            /// <summary>
            /// The list of show amounts to be shown in the combo box.  Defaults to 10, 25, 50, 100, 250.  (ALL is always added at the end of the list)
            /// </summary>
            public List<int> ShowAmts
            {
                get
                {
                    if (_ShowAmts == null)
                        initDefaultShowAmts();
                    return _ShowAmts;
                }
                set
                {
                    _ShowAmts = value;
                }
            }


            public int? DefaultShowAmount { get; set; }
            /// <summary>
            /// The user selection of Show Amt from the combo box
            /// </summary>
            public int SelectedShowAmt
            {
                get
                {

                    int amt = 0;
                    if (Int32.TryParse(CS.General_v3.Util.PageUtil.GetVariableFromQuerystring(QueryStringVarNameShowAmt), out amt))
                    {
                        //Value In QS
                        SelectedShowAmt = amt;
                    }
                    string sessionKey = this._pagingBar.ClientID + "_showAmt";
                    if (System.Web.HttpContext.Current.Session[sessionKey] != null)
                    {
                        amt = (int)System.Web.HttpContext.Current.Session[sessionKey];
                        return amt;
                    }
                    else if (DefaultShowAmount.HasValue)
                    {
                        return DefaultShowAmount.Value;
                    }
                    else
                    {
                        //Return middle value
                        int midValue = this.ShowAmts.Count / 2;
                        return this.ShowAmts[midValue];
                    }
                }
                set
                {
                    System.Web.HttpContext.Current.Session[this._pagingBar.ClientID + "_showAmt"] = value;
                }
            }
            /// <summary>
            /// The selected page by the paging bar.  Taken either from Querystring according to key set or session
            /// </summary>
            public int SelectedPage
            {
                get
                {

                    int pg = 0;
                    if (Int32.TryParse(CS.General_v3.Util.PageUtil.GetVariableFromQuerystring(QueryStringVarNamePage), out pg))
                    {
                        //Value In QS
                        return pg;
                    }
                    else
                    {
                        return 1;
                    }
                }
            }
            /// <summary>
            /// Text displayed when no results are added to the listing item.
            /// </summary>
            public string NoResultsText { get { return this._noResultsText; } set { this._noResultsText = value; } } 
        }







        public FUNCTIONALITY Functionality { get; set; }
        public PagingBar_TypeOne()
        {
            Functionality = new FUNCTIONALITY(this);
        }



        

        protected override void OnLoad(EventArgs e)
        {
            Functionality.Init();
            base.OnLoad(e);
            
        }

        public string NounSingular { get { return this.Functionality.NounSingular; } set { Functionality.NounSingular = value; } }
        /// <summary>
        /// The noun to which describe the items in plural.  Defaults to 'Items'.  Used to show "Showing Items 1 - 10 of 100"
        /// </summary>
        public string NounPlural { get { return this.Functionality.NounPlural; } set { this.Functionality.NounPlural = value; } }
        public int TotalItems { get { return Functionality.TotalItems; } set { Functionality.TotalItems = value; } }
        /// <summary>
        /// 0 based index.  The amount passed here will be incremented by one.  
        /// </summary>
        //public int FromItemIndex { get { return Functionality.FromItemIndex; } set { Functionality.FromItemIndex = value; } }
        public int AmtItems { get { return Functionality.AmtItems; } set { Functionality.AmtItems = value; } }
        /// <summary>
        /// The total amount of pages to show
        /// </summary>
        public int AmtPages { get { return Functionality.AmtPages; } set { Functionality.AmtPages = value; } }

        /// <summary>
        /// The amount of pages to show.  Defaults to 3.   This will show 3 page numbers to the left and to the right.  
        /// Thus if you are for example in page 8, it will show 5 6 7 [8] 9 10 11.  etc..
        /// </summary>
        public int ShowAmtPages { get { return Functionality.ShowAmtPages; } set { Functionality.ShowAmtPages = value; } }

        /// <summary>
        /// The key of the querystring variable to show for updating page.  By default it will set to the item's client ID with '_pg' appended at the end.
        /// This might be a bit ugly, but that guarantees uniqueness since there can be multiple paging in a page.  Change this to 'pg' if you want it to
        /// show something more nice and you only have one paging bar in the page.
        /// </summary>
        public string QueryStringVarNamePage { get { return Functionality.QueryStringVarNamePage; } set { Functionality.QueryStringVarNamePage = value; } }
        /// <summary>
        /// The key of the QS variable to show for updating show amt.  Defaults to the ClientID of listing with '_showAmt' appended to it.
        /// </summary>
        public string QueryStringVarNameShowAmt { get { return Functionality.QueryStringVarNameShowAmt; } set { Functionality.QueryStringVarNameShowAmt = value; } }
        /// <summary>
        /// The list of show amounts to be shown in the combo box.  Defaults to 10, 25, 50, 100, 250.  (ALL is always added at the end of the list)
        /// </summary>
        public List<int> ShowAmts { get { return Functionality.ShowAmts; } set { Functionality.ShowAmts = value; } }

        public int? DefaultShowAmount { get { return Functionality.DefaultShowAmount; } set { Functionality.DefaultShowAmount = value; } }
        
        /// <summary>
        /// The user selection of Show Amt from the combo box
        /// </summary>
        public int SelectedShowAmt { get { return Functionality.SelectedShowAmt; } set { Functionality.SelectedShowAmt = value; } }
        /// <summary>
        /// The selected page by the paging bar.  Taken either from Querystring according to key set or session
        /// </summary>
        public int SelectedPage { get { return Functionality.SelectedPage; }  }
        /// <summary>
        /// Text displayed when no results are added to the listing item.
        /// </summary>
        public string NoResultsText { get { return Functionality.NoResultsText; } set { Functionality.NoResultsText = value; } }

    }
}
