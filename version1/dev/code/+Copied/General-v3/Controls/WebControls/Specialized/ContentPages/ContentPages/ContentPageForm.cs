﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using CS.General_20101215.Controls.WebControls.Common;
using CS.General_20101215.Controls.Classes.MediaGallery;
using CS.General_20101215.Util;
using CS.General_20101215.Controls.WebControls.Specialized.TreeStructureClasses;
using CS.General_20101215.Controls.WebControls.Specialized.FormFieldsClasses;
using CS.General_20101215.Classes.URL;

namespace CS.General_20101215.Controls.WebControls.Specialized.ContentPages.ContentPages
{
    [ToolboxData("<{0}:ContentPageForm runat=server></{0}:ContentPageForm>")]    
    public class ContentPageForm : FormFields
    {
        public new class FUNCTIONALITY : Form
        {
            public enum HTML_EDITOR_TYPE
            {
                CKEditor
            }
            public HTML_EDITOR_TYPE HTMLEditorType { get; set; }
            public string EditorCSSFile { get; set; }
            private ContentPageForm _contentPageForm = null;
            public FUNCTIONALITY(ContentPageForm contentPageForm)
            {
                _contentPageForm = contentPageForm;
                this.HTMLEditorType = HTML_EDITOR_TYPE.CKEditor;
                this.QueryStringPageParam = "page";
                this.QueryStringParentPageParam = "parent";
            }
            #region Properties
            public IContentPage ContentPage { get; set; }
            public string ItemTitle { get; set; }
            public string QueryStringPageParam { get; set; }
            public string QueryStringParentPageParam { get; set; }
            public string ListingPageURL { get; set; }
            public bool IsEdit
            {
                get
                {
                    return GetPageIDFromQueryString() != 0;
                }
            }

            #endregion
            #region Methods
            internal void showMessage(string msg)
            {
                if (ShowMessage != null)
                    ShowMessage(msg);
            }
            public int GetPageIDFromQueryString()
            {
                return CS.General_20101215.Util.PageUtil.RQint(QueryStringPageParam);
            }
            public int GetParentPageIDFromQueryString()
            {
                return CS.General_20101215.Util.PageUtil.RQint(QueryStringParentPageParam);
            }

            public IContentPage GetParentPage()
            {
                return LoadPage(GetParentPageIDFromQueryString());
            }
            internal void loadItem()
            {
                if (IsEdit)
                    loadItemFromParent();
                else
                {
                    IContentPage parent = GetParentPage();
                    this.ContentPage = parent.CreateSubPage();
                }
            }
            private void loadItemFromParent()
            {
                IContentPage parent = GetParentPage();
                
                IContentPage item = null;
                int itemID = GetPageIDFromQueryString();
                item = LoadPage(itemID);
                //item = (IContentPage)parent.GetChildPages().FindElemObj(i => (((IContentPage)i).ID == itemID));
                this.ContentPage = item;



            }
            #endregion

            #region Delegates
            public delegate IContentPage CreateLoadPageEvent(int ID);
            public delegate void ShowMessageEvent(string msg);
            #endregion
            #region Events
            public event ShowMessageEvent ShowMessage;
            public event CreateLoadPageEvent LoadPage;
            #endregion

            public CS.General_20101215.Controls.WebControls.Common.FCKEditor.FCKeditor GetFCKEditorForHTMLEditor()
            {
                return (CS.General_20101215.Controls.WebControls.Common.FCKEditor.FCKeditor)this._contentPageForm.GetControl("txtText");
            }
            public CS.General_20101215.Controls.WebControls.Common.CKEditor GetCKEditorForHTMLEditor()
            {
                return (CS.General_20101215.Controls.WebControls.Common.CKEditor)this._contentPageForm.GetControl("txtText");
            }


            internal void checkParameters()
            {
                StringBuilder sb = new StringBuilder();
                if (this.LoadPage == null)
                {
                    sb.AppendLine("Please attach to 'LoadPage'");
                }

                if (string.IsNullOrEmpty(this.ListingPageURL))
                {
                    sb.AppendLine("Please fill in 'ListingPageURL'");
                }
                if (sb.Length > 0)
                {
                    string s = "ContentPages.ContentPageForm:: " + sb.ToString();
                    throw new InvalidOperationException(s);
                }
            }
        }
        public FUNCTIONALITY Functionality { get; set; }
        /* HOW TO USE
        * ----------
        * 
        * Parameters to fill
        * 
        *  - ItemTitle
        *  - ListingPageUrl
        *  
        * Events to Attach
        *  
        *  - LoadFolder
        *  
        *  
        * Optional
        * 
        * - QueryStringItemParam
        * - QueryStringParentFolderParam
        */
        public ContentPageForm()
        {
            this.Functionality = new FUNCTIONALITY(this);
            
            
        }
        #region Constants / Statics
        

        #endregion


        public virtual void addExtraFormFields()
        {
            var pg = this.Functionality.ContentPage;

            this.AddString("txtMetaKeywords", "Keywords", false,
                "Keywords are tags that give a general idea what the content of this page is about - " +
                "Used by search engines like Google, Ask, etc to help analyse the contents of the page",
                600, (pg != null ? pg.MetaKeywords : ""));
            this.AddStringMultiline("txtMetaDescription", "Description", false,
                "A short summary of what this page is about - " +
                "Used by search engines like Google, Ask, etc to help analyse the contents of the page",6,
                600, (pg != null ? pg.MetaDescription : ""));
           




            
                var ck = this.AddCKEditor("txtText", "Text", true, null, 800, 600, pg != null ? pg.HtmlText : "");
                if (!string.IsNullOrEmpty(this.Functionality.EditorCSSFile))
                    ck.ConfigParameters.ContentCSSFiles.Add(this.Functionality.EditorCSSFile);

            
        }

        public virtual void saveExtraFields()
        {
            var pg = this.Functionality.ContentPage;
            pg.HtmlText = this.GetFormValueStr("txtText");
            pg.MetaDescription = this.GetFormValueStr("txtMetaDescription");
            pg.MetaKeywords = this.GetFormValueStr("txtMetaKeywords");
           pg.LastEditedDate = CS.General_20101215.Util.Date.Now;
           
        }

        

        protected override void OnInit(EventArgs e)
        {

            
            base.OnInit(e);
        }

        
        /// <summary>
        /// Renders the tree
        /// </summary>
        public void renderForm()
        {
            IContentPage parent = Functionality.GetParentPage();
            IContentPage item = this.Functionality.ContentPage;


            this.AddString("txtTitle", "Title", true, null, 400, item != null ? item.Title : "");
            this.AddInteger("txtPriority", "Priority", true, null, 80, item != null ? item.Priority : 0);
            this.AddBool("chkVisible", "Visible", false, (item != null ? item.Visible : true));
            addExtraFormFields();
            if (!Functionality.IsEdit)
                this.AddBool("chkAddAnother", "Add Another", false, true);
            this.ClickSubmit += new EventHandler(MenuItemForm_ClickSubmit);
        }
        
        void MenuItemForm_ClickSubmit(object sender, EventArgs e)
        {

            IContentPage item = this.Functionality.ContentPage;

            item.Title = this.GetFormValueStr("txtTitle");
            item.Priority = this.GetFormValueInt("txtPriority");
            item.Visible = this.GetFormValueBool("chkVisible");
            saveExtraFields();
            item.Save();
            bool addAnother = false;
            if (Functionality. IsEdit)
                Functionality.showMessage(Functionality.ItemTitle + " edited successfully");
            else
                Functionality.showMessage(Functionality.ItemTitle + " added successfully");

            if (!Functionality.IsEdit) addAnother = this.GetFormValueBool("chkAddAnother");
            URLClass url = new URLClass();
            if (Functionality.IsEdit || (!Functionality.IsEdit && !addAnother))
            {
                url[Functionality.QueryStringPageParam] = null;
                url[Functionality.QueryStringParentPageParam] = null;
                url.RedirectTo(this.Functionality.ListingPageURL);
            }
            else
            {
                url.TransferTo();
            }

             


        }
        
        protected override void OnLoad(EventArgs e)
        {
            Functionality.checkParameters();
            Functionality.loadItem();
            renderForm();
            base.OnLoad(e);
        }

        


 
    }
}
