﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Controls.Classes.MediaGallery;
using CS.General_v3;
using CS.General_v3.Controls.WebControls.Classes;

namespace CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructure
{
    [ToolboxData("<{0}:TreeStructureClass runat=server></{0}:TreeStructure>")]    
    public class TreeStructureClass : TreeStructureBasic.TreeStructureBasicClass
    {
        /* HOW TO USE
         * ----------
         * 
         * Parameters to fill
         * 
         *  - RootFolders
         *  - ImageFolder
         *  - EditFolderPageName
         *  - EditItemPageName
         *  - ItemTitle
         *  - FolderTitle
         *  - CSSFile
         *  
         * Optional
         *  
         *  - QuerystringFolderIDParam
         *  - QuerystringParentFolderIDParam
         *  - QuerystringItemIDParam
         */
        public class FUNCTIONALITY : TreeStructureBasic.TreeStructureBasicClass.FUNCTIONALITY
        {
            private TreeStructureClass _tree = null;
            public FUNCTIONALITY(TreeStructureClass tree)
                : base(tree)
            {
                this._tree = tree;

                

                this.ItemTitle = "Item";
                this.PaddingPerLevel = 30;
                this.ShowPriorityAfterName = true;
            }
            #region Delegates
           
            #endregion
            #region Events
            public delegate void ShowMessageEvent(string msg);
            public event ShowMessageEvent ShowMessage;
            #endregion
            #region Methods

            internal void showMessage(string msg)
            {
                if (ShowMessage != null)
                    ShowMessage(msg);
            }
            #endregion
            #region Properties
            public new List<ITreeItem> RootItems { get; set; }
            private void parseRootItems()
            {
                base.RootItems = new List<CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructureBasic.ITreeItemBasic>();
                foreach (var item in this.RootItems)
                {
                    parseTreeItem(item);
                    base.RootItems.Add(item);
                }
            }
            private ExtraButton GetExtraButton()
            {
                ExtraButton btn = new ExtraButton();
                btn.CssClass = "tree_button";
                return btn;
            }
            private void parseTreeItem(ITreeItem item)
            {
                ExtraButton extraBtn = null;
                if (!item.AddedExtraButtons)
                {
                    if (item.AllowUpdate)
                    {
                        extraBtn = GetExtraButton();
                        extraBtn.Href = item.EditURL;
                        extraBtn.ImageUrl_Over = this.ImageURL_Edit_Over;
                        extraBtn.ImageUrl_Up = this.ImageURL_Edit_Up;
                        extraBtn.Title = "Edit " + this.ItemTitle;
                        item.ExtraButtons.Add(extraBtn);
                    }
                    else
                    {
                        item.ExtraButtons.Add(null);
                    }
                    if (item.AllowDelete)
                    {
                        extraBtn = GetExtraButton();
                        extraBtn.Click += new EventHandler(btnRootItem_Delete);
                        extraBtn.ImageUrl_Over = this.ImageURL_Delete_Over;
                        extraBtn.ImageUrl_Up = this.ImageURL_Delete_Up;
                        extraBtn.Title = "Delete " + this.ItemTitle;

                        extraBtn.ConfirmMessageOnClick = item.Message_ConfirmDelete;
                        item.ExtraButtons.Add(extraBtn);
                    }
                    else
                    {
                        item.ExtraButtons.Add(null);
                    }
                    if (item.AllowAddSubItems)
                    {
                        extraBtn = GetExtraButton();
                        extraBtn.Href = item.AddNewItemURL;
                        extraBtn.ImageUrl_Over = this.ImageURL_AddSubItems_Over;
                        extraBtn.ImageUrl_Up = this.ImageURL_AddSubItems_Up;
                        extraBtn.Title = "Add new " + this.ItemTitle;
                        item.ExtraButtons.Add(extraBtn);
                    }
                    else
                    {
                        item.ExtraButtons.Add(null);
                    }
                    item.AddedExtraButtons = true;
                }
                foreach (var child in item.GetChildTreeItems())
                {
                    parseTreeItem((ITreeItem) child);
                }
            }

            void btnRootItem_Delete(object sender, EventArgs e)
            {
                MyImageButton btn = (MyImageButton)sender;
                ITreeItem item = (ITreeItem)btn.Tag;
                item.Remove();
                showMessage(item.Message_DeleteOK);

            }



            public override void RenderTree()
            {
                parseRootItems();
                base.RenderTree();
            }
           
            public string ImageURL_Edit_Up { get; set; }
            public string ImageURL_Edit_Over { get; set; }
            public string ImageURL_Delete_Up { get; set; }
            public string ImageURL_Delete_Over { get; set; }
            public string ImageURL_AddSubItems_Up { get; set; }
            public string ImageURL_AddSubItems_Over { get; set; }
            


            #endregion



            


        }
        public new FUNCTIONALITY Functionality
        {
            get
            {
                return (FUNCTIONALITY)base.Functionality;
            }
            set
            {
                base.Functionality = value;
            }
        }



        public TreeStructureClass()
        {
            base.Functionality = new FUNCTIONALITY(this);
            
           
        }
        #region Constants / Statics
        

        #endregion
        

        protected override void OnInit(EventArgs e)
        {

            
            base.OnInit(e);
        }

        


 
    }
}
