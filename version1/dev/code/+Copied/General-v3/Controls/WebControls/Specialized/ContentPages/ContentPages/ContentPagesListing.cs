﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using CS.General_20101215.Controls.WebControls.Common;
using CS.General_20101215.Controls.Classes.MediaGallery;
using CS.General_20101215;
using CS.General_20101215.Controls.WebControls.Classes;
using CS.General_20101215.Controls.WebControls.Specialized.TreeStructureClasses;
using CS.General_20101215.Controls.WebControls.Specialized.Hierarchy;
namespace CS.General_20101215.Controls.WebControls.Specialized.ContentPages.ContentPages
{
    [ToolboxData("<{0}:ContentPagesListing runat=server></{0}:ContentPagesListing>")]
    public class ContentPagesListing : TreeStructureClasses.TreeStructureBasic.TreeStructureBasicClass
    {

        private class TreeItem : TreeStructureClasses.TreeStructureBasic.ITreeItemBasic
        {

            public TreeItem(ContentPagesListing listing)
            {
                this._listing = listing;
            }

            private ContentPagesListing _listing = null;
            public IContentPage CurrentPage { get; set; }

            public void LoadFromContentPage(IContentPage pg)
            {
                this.CurrentPage = pg;

                this.ID = ((IHierarchy)pg).ID;
                this.Priority = pg.Priority;
                this.AllowAddSubItems = pg.AllowAddSubItems;
                this.AllowDelete = pg.AllowDelete;
                this.AllowUpdate = pg.AllowUpdate;
                this.ExtraButtons = new List<ExtraButton>();
                this.Message_Confirm = "Are you sure you want to remove this page, and all of its sub-items?";
                this.Title = pg.Title;
                this.EditURL = _listing.Functionality.EditPageName + "?" + _listing.Functionality.QuerystringIDParam + "=" + ((IHierarchy)pg).ID;

                
                

                if (pg.IsPageVisibleAccordingToParent())
                {
                    if (pg.ContainsContent())
                    {
                        this.ImageURL = _listing.Functionality.GetImageUrl("folder_content.gif");
                    }
                    else
                    {
                        this.ImageURL = _listing.Functionality.GetImageUrl("folder.gif");
                    }
                }
                else
                {
                    if (pg.ContainsContent())
                    {
                        this.ImageURL = _listing.Functionality.GetImageUrl("folder_content_dim.gif");
                    }
                    else
                    {
                        this.ImageURL = _listing.Functionality.GetImageUrl("folder_dim.gif");
                    }
                }
                if (this.AllowAddSubItems)
                {
                    ExtraButton btn = new ExtraButton();
                    btn.Href = _listing.Functionality.EditPageName + "?" + _listing.Functionality.QuerystringParentIDParam + "=" + ((IHierarchy)pg).ID;
                    btn.Title = "Add page/section";
                    btn.ImageUrl_Up = _listing.Functionality.GetImageUrl("add_folder_up.gif");
                    btn.ImageUrl_Over = _listing.Functionality.GetImageUrl("add_folder_over.gif");
                    this.ExtraButtons.Add(btn);
                }
                else
                {
                    this.ExtraButtons.Add(null);
                }
                if (this.AllowUpdate)
                {
                    ExtraButton btn = new ExtraButton();
                    btn.Href = _listing.Functionality.EditPageName + "?" + _listing.Functionality.QuerystringIDParam + "=" + ((IHierarchy)pg).ID;
                    btn.Title = "Edit";
                    btn.ImageUrl_Up = _listing.Functionality.GetImageUrl("edit_up.gif");
                    btn.ImageUrl_Over = _listing.Functionality.GetImageUrl("edit_over.gif");
                    this.ExtraButtons.Add(btn);
                }
                else
                {
                    this.ExtraButtons.Add(null);
                }
                if (this.AllowDelete)
                {
                    ExtraButton btn = new ExtraButton();
                    btn.Click += new EventHandler(btnDelete_Click);
                    btn.Title = "Delete";
                    btn.ConfirmMessageOnClick = "Are you sure you want to delete this page and all of its sub-pages?";

                    btn.ImageUrl_Up = _listing.Functionality.GetImageUrl("delete_up.gif");
                    btn.ImageUrl_Over = _listing.Functionality.GetImageUrl("delete_over.gif");
                    this.ExtraButtons.Add(btn);
                }
                else
                {
                    this.ExtraButtons.Add(null);
                }
                
            }

            void btnDelete_Click(object sender, EventArgs e)
            {
                MyImageButton btn = (MyImageButton)sender;
                TreeItem item = (TreeItem)btn.Tag;
                item.CurrentPage.Remove();
                _listing.Functionality.showMessage("Page deleted successfully");
            }
           

            #region ITreeItem Members

            public long ID{get;set;}
            
            public string Title { get; set; }

            public int Priority { get; set; }
            public string ImageURL { get; set; }

            public string EditURL { get; set; }
            public bool AllowUpdate { get; set; }
            public bool AllowDelete { get; set; }
            

            public void Remove()
            {
                if (CurrentPage != null)
                    CurrentPage.Remove();
                
            }

            public void Save()
            {
                if (CurrentPage != null)
                    CurrentPage.Save();

                
            }
            

            public List<TreeStructureClasses.TreeStructureBasic.ITreeItemBasic> GetChildTreeItems()
            {
                List<TreeStructureClasses.TreeStructureBasic.ITreeItemBasic> list = new List<TreeStructureClasses.TreeStructureBasic.ITreeItemBasic>();
                if (CurrentPage != null)
                {
                    IList<IContentPage> children = CurrentPage.GetChildPages();
                    for (int i = 0; i < children.Count; i++)
                    {
                        IContentPage pg = children[i];
                        TreeItem item = new TreeItem(_listing);
                        item.LoadFromContentPage(pg);
                        list.Add(item);
                    }
                    
                }
                return list;
            }

            public List<ExtraButton> ExtraButtons {get; private set;}
            

            public string Message_Delete { get; set; }
            public string Message_Confirm { get; set; }

            #endregion

            #region ITreeItem Members


            public string AddNewItemURL
            {
                get
                {
                    return "";
                }
                set
                {
                    
                }
            }

            public bool AllowAddSubItems {get;set;}
            #endregion

            #region ITreeItemBasic Members


            public virtual void AddExtraButtons()
            {
                
            }

            #endregion

            #region ITreeItemBasic Members


            public string LinkURL
            {
                get { return ""; }
            }

            #endregion
        }
        /* HOW TO USE
         * ----------
         * 
         * Parameters to fill
         * 
         *  - RootFolders
         *  - ImageFolder
         *  - EditFolderPageName
         *  - EditItemPageName
         *  - ItemTitle
         *  - FolderTitle
         *  - CSSFile
         *  
         * Optional
         *  
         *  - QuerystringFolderIDParam
         *  - QuerystringParentFolderIDParam
         *  - QuerystringItemIDParam
         */
        public class FUNCTIONALITY : CS.General_20101215.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructureBasic.TreeStructureBasicClass.FUNCTIONALITY
        {
            private ContentPagesListing _listing;
            public FUNCTIONALITY(ContentPagesListing listing) : base(listing)
            {
                _listing = listing;

                this.QuerystringParentIDParam = "parent";
                this.QuerystringIDParam = "page";
                this.ImageFolder = "/_common/images/components/v1/tree/";
                this.ImageURL_Delete_Over = "delete_over.gif";
                this.ImageURL_Delete_Up= "delete_up.gif";
                this.ImageURL_Edit_Over = "edit_over.gif";
                this.ImageURL_Edit_Up = "edit_up.gif";
                
                this.PaddingPerLevel = 30;
                this.ShowPriorityAfterName = true;

            }

            #region Constants / Statics


            #endregion
            #region Delegates
            public delegate void ShowMessageEvent(string msg);
            #endregion
            #region Events
            public event ShowMessageEvent ShowMessage;

            #endregion
            #region Methods
            internal void showMessage(string msg)
            {
                if (ShowMessage != null)
                    ShowMessage(msg);
            }

            #endregion
            #region Properties
            public string ImageURL_Edit_Up { get; set; }
            public string ImageURL_Edit_Over { get; set; }
            public string ImageURL_Delete_Up { get; set; }
            public string ImageURL_Delete_Over { get; set; }
           // public string ImageURL_Add_Up { get; set; }
           // public string ImageURL_Add_Over { get; set; }
            /*public string ImageURL_Edit_Up { get { return _listing.FunctionalityBase.ImageURL_Edit_Up; } set { _listing.FunctionalityBase.ImageURL_Edit_Up = GetImageUrl(value); } }
            public string ImageURL_Edit_Over { get { return _listing.FunctionalityBase.ImageURL_Edit_Over; } set { _listing.FunctionalityBase.ImageURL_Edit_Over = GetImageUrl(value); } }
            public string ImageURL_Delete_Up { get { return _listing.FunctionalityBase.ImageURL_Delete_Up; } set { _listing.FunctionalityBase.ImageURL_Delete_Up = GetImageUrl(value); } }
            public string ImageURL_Delete_Over { get { return _listing.FunctionalityBase.ImageURL_Delete_Over; } set { _listing.FunctionalityBase.ImageURL_Delete_Over = GetImageUrl(value); } }
            public bool ShowPriorityAfterName { get { return _listing.FunctionalityBase.ShowPriorityAfterName; } set { _listing.FunctionalityBase.ShowPriorityAfterName = value; } }*/
            private List<IContentPage> _RootItems = null;
            /// <summary>
            /// Root Folders
            /// </summary>
            public new List<IContentPage> RootItems
            {
                get
                {
                    return _RootItems;
                }
                set
                {
                    _RootItems = value;
                    //_listing.Functionality.RootItems = GetRootFoldersAsITreeItems();
                    
                }
            }
            protected List<TreeStructureClasses.TreeStructureBasic.ITreeItemBasic> GetRootFoldersAsITreeItems()
            {
                List<TreeStructureClasses.TreeStructureBasic.ITreeItemBasic> list = new List<TreeStructureClasses.TreeStructureBasic.ITreeItemBasic>();
                foreach (var pg in RootItems)
                {
                    TreeItem treeItem = new TreeItem(_listing);
                    treeItem.LoadFromContentPage(pg);
                    list.Add(treeItem);
                }
                return list;

            }
            /// <summary>
            /// Location of CSS file
            /// </summary>
            //public string CSSFile { get { return _listing.FunctionalityBase.CSSFile; } set { _listing.FunctionalityBase.CSSFile = value; } }
            /// <summary>
            /// Padding per level
            /// </summary>
            //public int PaddingPerLevel { get { return _listing.FunctionalityBase.PaddingPerLevel; } set { _listing.FunctionalityBase.PaddingPerLevel = value; } }
            private string _ImageFolder;
            /// <summary>
            /// Folder where images are stored
            /// </summary>
            public string ImageFolder { get { return _ImageFolder; } set { _ImageFolder = value; } }
            /// <summary>
            /// Edit/Add folder page name
            /// </summary>
            public string EditPageName { get; set; }
            
            /// <summary>
            /// The query string parameter for the folder id, in the 
            /// </summary>
            public string QuerystringIDParam { get; set; }

            /// <summary>
            /// The query string parameter for the parent folder id, in the 
            /// </summary>
            public string QuerystringParentIDParam { get; set; }
            
            /// <summary>
            /// Title of 'item', example 'Link'
            /// </summary>
            public string ItemTitle { get; set; }
            /// <summary>
            /// Title of 'folder', example Link Section. 
            /// </summary>
            public string FolderTitle { get; set; }

            #endregion


          
            private HtmlTable tbTree = null;
            private int ctrlIndex = 0;
            public string GetImageUrl(string filename)
            {
                return this.ImageFolder + filename;
            }


            public override void RenderTree()
            {
                
                base.RenderTree();
            }

            //private void parseRootItems()
            //{
            //    base.RootItems = new List<CS.General_20101215.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructureBasic.ITreeItemBasic>();
            //    foreach (var item in this.RootItems)
            //    {
            //        TreeItem treeItem = new TreeItem(this._listing);
            //        treeItem.LoadFromContentPage(item);
            //        //parseFolder(item);
            //        base.RootItems.Add(treeItem);
            //    }
            //}
            /*
            private void parseFolder(TreeItem item)
            {
                TreeStructureClasses.TreeStructureBasic.ExtraButton extraBtn = null;


                foreach (var child in item.GetChildTreeItems())
                {
                    if (child is IMenuFolder)
                        parseTreeFolder((IMenuFolder)child);
                    else if (child is IMenuItem)
                    {
                        parseTreeItem((IMenuItem)child);
                    }

                }
            }*/


            public override void checkParameters()
            {
                //_listing.Functionality.checkParameters();
                StringBuilder sb = new StringBuilder();
                if (string.IsNullOrEmpty(this.CSSFile))
                    sb.AppendLine("Please specify 'CSSFile'");
                if (string.IsNullOrEmpty(this.ImageFolder))
                    sb.AppendLine("Please specify 'ImageFolder'");
                if (string.IsNullOrEmpty(this.EditPageName))
                    sb.AppendLine("Please specify 'EditPageName'");
                
                if (string.IsNullOrEmpty(this.FolderTitle))
                    sb.AppendLine("Please specify 'FolderTitle'");
                if (string.IsNullOrEmpty(this.ItemTitle))
                    sb.AppendLine("Please specify 'ItemTitle'");
                if (string.IsNullOrEmpty(this.ImageURL_Delete_Over))
                    sb.AppendLine("Please specify 'ImageURL_Delete_Over'");
                if (string.IsNullOrEmpty(this.ImageURL_Delete_Up))
                    sb.AppendLine("Please specify 'ImageURL_Delete_Up'");
                if (string.IsNullOrEmpty(this.ImageURL_Edit_Over))
                    sb.AppendLine("Please specify 'ImageURL_Edit_Over'");
                if (string.IsNullOrEmpty(this.ImageURL_Edit_Up))
                    sb.AppendLine("Please specify 'ImageURL_Edit_Up'");
                if (this.RootItems == null)
                {
                    sb.AppendLine("Please set 'RootItems'");
                }
                if (sb.Length > 0)
                {
                    string s = "ContentPages.ContentPagesListing:: " + sb.ToString();
                    throw new InvalidOperationException(s);
                }
                parseRootItems();
            }
           
        }
        public new FUNCTIONALITY Functionality
        {
            get
            {
                return (FUNCTIONALITY)base.Functionality;
            }
            set
            {
                base.Functionality = value;
            }

        }
        //protected TreeStructureClasses.TreeStructure.FUNCTIONALITY FunctionalityBase {get { return base.Functionality; }}

        public ContentPagesListing()
        {
            base.Functionality = new FUNCTIONALITY(this);
            //this.Functionality = new FUNCTIONALITY(this);
            
        }

        protected override void OnLoad(EventArgs e)
        {


            base.OnLoad(e);
        }

        protected override void OnInit(EventArgs e)
        {


            base.OnInit(e);
        }

        
        

        
            


 
    }
}
