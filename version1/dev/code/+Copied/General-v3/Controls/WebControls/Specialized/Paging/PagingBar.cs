﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Classes.SearchParams;

namespace CS.General_v3.Controls.WebControls.Specialized.Paging
{
    public class PagingBar : MyDiv
    {
        public class FUNCTIONALITY
        {
            /// <summary>
            /// Pages start from 1, NOT 0
            /// </summary>
            public int? SelectedPageNum { get; set; }
            public int? TotalResults { get; set; }
            public int? PageSize { get; set; }
            public string ResultsTextSingular { get; set; }
            public string ResultsTextPlural { get; set; }
            public string NoResultsText { get; set; }
            public bool InsertInTable { get; set; }
            public bool ShowResultsText { get; set; }

            /// <summary>
            /// This will be something of the form Showing [FROM] - [TO] of [ITEMS].
            /// 
            /// The tags are taken from the constants in this PagingBar.TAG_SHOWING_RESULTS_FROM etc...
            /// </summary>
            public string ShowingResultsFullText { get; set; }
            /// <summary>
            /// The marker to be added where it should indicate that other pages exist
            /// </summary>
            public string ContinuationMarkerHtml { get; set; }
            /// <summary>
            /// The amount of pages to show to the left/right of the selected page.  For example, if 2 is set
            /// and the selected page is 6, then the pages shown are ...4, 5, [6], 7, 8...
            /// </summary>
            public int AmtNearestPagesShown { get; set; }
            public string ButtonNextText { get; set; }
            public string ButtonLastText { get; set; }
            public string ButtonFirstText { get; set; }
            public string ButtonPrevText { get; set; }
            public string PagesSeparatorText { get; set; }

            //public ListItemCollection SortByComboBoxValues { get; set; }
            //public ListItemCollection ShowAmtComboBoxValues { get; set; }

            /// <summary>
            /// The Url to use in the links.  If set to null, it will take the URL of the current page
            /// </summary>
            public string Url { get; set; }
            /// <summary>
            /// The parameter to use in the querystring
            /// </summary>
            public string QuerystringParamPage { get; set; }


        }
        public const string TAG_SHOWING_RESULTS_FROM = "[FROM]";
        public const string TAG_SHOWING_RESULTS_TO = "[TO]";
        public const string TAG_SHOWING_RESULTS_TOTAL = "[TOTAL]";
        public const string TAG_SHOWING_RESULTS_ITEMS = "[ITEMS]";

        public FUNCTIONALITY Functionality { get; set; }
        //public MyDropDownList CmbSortBy { get; set; }
        //public MyDropDownList CmbShowAmt { get; set; }

        //Use these to seperate them if you want
        public MyDiv DivResultsText { get; private set; }
        public MyDiv DivPagingBar { get; private set; }

        /// <summary>
        /// Interface with this instance to retrieve all methods / properties
        /// </summary>
        public IPagingInfo SearchParamsPagingInfo { get; set; }

        public PagingBar()
        {
            DivResultsText = new MyDiv();
            DivPagingBar = new MyDiv();

            //CmbSortBy = new MyDropDownList();
            //CmbShowAmt = new MyDropDownList();

            Functionality = new FUNCTIONALITY();
            Functionality.ShowResultsText = true;
            Functionality.AmtNearestPagesShown = 2;
            Functionality.ResultsTextSingular = "result";
            Functionality.ResultsTextPlural = "results";
            Functionality.ButtonFirstText = "&lt;&lt; First";
            Functionality.ButtonPrevText = "&lt; Prev";
            Functionality.ButtonNextText = "Next &gt;";
            Functionality.ButtonLastText = "Last &gt;&gt;";
            Functionality.PagesSeparatorText = " | ";
            Functionality.QuerystringParamPage = "pg";
            Functionality.ShowingResultsFullText = "Showing <strong>" + TAG_SHOWING_RESULTS_FROM + " - " + TAG_SHOWING_RESULTS_TO + "</strong> of <strong>" + TAG_SHOWING_RESULTS_TOTAL + "</strong> " + TAG_SHOWING_RESULTS_ITEMS;
            Functionality.NoResultsText = "No results";

            this.CssManager.AddClass("paging-bar");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchParams">If you provide the SearchParams, then this class will liaise with the search params to automatically
        /// update values from querystring, such as page number, page size and selected page.  This will make life much easier</param>
        public PagingBar(SearchParamsPagingInfo_v2 searchParamsPagingInfo)
            : this()
        {
            this.SearchParamsPagingInfo = searchParamsPagingInfo;
        }
        private void initResultsText()
        {
            if (Functionality.ShowResultsText)
            {
                if (Functionality.SelectedPageNum == null || Functionality.TotalResults == null || Functionality.PageSize == null)
                {
                    throw new Exception("Please specify SelectedPageNUm, Total Results and PageSize");
                }
                if (Functionality.SelectedPageNum == 0) Functionality.SelectedPageNum = 1;


                MyDiv divResultsText = DivResultsText;
                divResultsText.CssManager.AddClass("paging-bar-text");

                string text = "";
                if (Functionality.TotalResults == 0)
                {
                    text = Functionality.NoResultsText;
                }
                else
                {

                    int fromIndex = (Functionality.SelectedPageNum.Value - 1) * Functionality.PageSize.Value + 1;
                    int toIndex = Math.Min(Functionality.TotalResults.Value, Functionality.SelectedPageNum.Value * Functionality.PageSize.Value);
                    if (Functionality.PageSize.Value == 0) toIndex = Functionality.TotalResults.Value;
                    text = Functionality.ShowingResultsFullText;
                    text = text.Replace(TAG_SHOWING_RESULTS_FROM, fromIndex.ToString("#,0"));
                    text = text.Replace(TAG_SHOWING_RESULTS_TO, toIndex.ToString("#,0"));
                    text = text.Replace(TAG_SHOWING_RESULTS_TOTAL, Functionality.TotalResults.Value.ToString("#,0"));
                    text = text.Replace(TAG_SHOWING_RESULTS_ITEMS, (Functionality.TotalResults == 1 ? Functionality.ResultsTextSingular : Functionality.ResultsTextPlural));
                    
                }
                divResultsText.InnerHtml = text;

                Controls.Add(divResultsText);
            }
        }
        
        private void initPages()
        {
            ListItemCollection Pages = new ListItemCollection();

            int amtPages = (int)Math.Ceiling((double)Functionality.TotalResults.Value / (double)Functionality.PageSize);

            if (Functionality.SelectedPageNum > 1)
            {
                //Can have prev
                Pages.Add(new ListItem(Functionality.ButtonFirstText, "1"));
                Pages.Add(new ListItem(Functionality.ButtonPrevText, (Functionality.SelectedPageNum.Value-1).ToString()));
                if (!String.IsNullOrEmpty(Functionality.ContinuationMarkerHtml))
                {
                    Pages.Add((string)null); // Add marker
                }
            }
            int fromIndex = Math.Max(1, Functionality.SelectedPageNum.Value - Functionality.AmtNearestPagesShown);
            int toIndex = Math.Min(amtPages, Functionality.SelectedPageNum.Value + Functionality.AmtNearestPagesShown);
            for (int i = fromIndex; i <= toIndex; i++)
            {
                Pages.Add(new ListItem(i.ToString(), i.ToString()) { Selected = i == Functionality.SelectedPageNum.Value });
            }
            if (Functionality.SelectedPageNum < amtPages)
            {
                if (Functionality.ContinuationMarkerHtml != null)
                {
                    Pages.Add((string)null); // Add marker
                }
                Pages.Add(new ListItem(Functionality.ButtonNextText, (Functionality.SelectedPageNum + 1).ToString()));
                Pages.Add(new ListItem(Functionality.ButtonLastText, amtPages.ToString()));
            }

            MyDiv divLinks = DivPagingBar;
            divLinks.CssManager.AddClass("paging-bar-pages");


            CS.General_v3.Classes.URL.URLClass url = null;
            if (!String.IsNullOrEmpty(Functionality.Url)) {
                url = new General_v3.Classes.URL.URLClass(Functionality.Url);
            }
            else {
                url = new General_v3.Classes.URL.URLClass();
            }

            HtmlTable tbl = null;
            if (Functionality.InsertInTable)
            {
                tbl = new HtmlTable();
                tbl.Rows.Add(new HtmlTableRow());
                divLinks.Controls.Add(tbl);
            }



            //Create items
            for (int i = 0; i < Pages.Count; i++)
            {
                if (String.IsNullOrEmpty(Pages[i].Text) && String.IsNullOrEmpty(Pages[i].Value))
                {
                    //Show continuation dots
                    addPage(tbl, divLinks, new Literal() { Text = Functionality.ContinuationMarkerHtml });
                    continue;
                }
                if (i > 0 && !String.IsNullOrEmpty(Functionality.PagesSeparatorText))
                {
                    divLinks.Controls.Add(new Literal() { Text = Functionality.PagesSeparatorText });
                }

                HtmlAnchor a = new HtmlAnchor();
                a.InnerHtml = Pages[i].Text;
                if (Pages[i].Selected) {
                    a.Attributes["class"] = "selected";
                }
                url.QS.Set(Functionality.QuerystringParamPage, Pages[i].Value);
                a.HRef = url.ToString();
                addPage(tbl, divLinks, a);

                int temp;
                if (!int.TryParse(Pages[i].Text, out temp))
                {
                    a.Attributes["class"] = "arrow";
                }

                
            }
            Controls.Add(divLinks);

        }
        private void addPage(HtmlTable tbl, MyDiv divLinks, Control c)
        {
            if (Functionality.InsertInTable)
            {
                HtmlTableCell td = new HtmlTableCell();
                tbl.Rows[0].Cells.Add(td);
                td.Controls.Add(c);
            }
            else
            {
                divLinks.Controls.Add(c);
            }
        }
        private void initComboboxes()
        {

        }
        private void updateFromSearchParams()
        {
            if (SearchParamsPagingInfo != null)
            {
                //Update parameters from the specified search params
                this.Functionality.QuerystringParamPage = this.SearchParamsPagingInfo.GetPageNo_QuerystringVar();
                this.Functionality.SelectedPageNum = this.SearchParamsPagingInfo.PageNo;
                this.Functionality.PageSize = this.SearchParamsPagingInfo.ShowAmount;
            }
        }
        protected void initControls()
        {
            updateFromSearchParams();   
            initResultsText();
            initPages();
            initComboboxes();
        }
        
        protected override void OnLoad(EventArgs e)
        {
            initControls();
            
            base.OnLoad(e);
        }
    }
}
