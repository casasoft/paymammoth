﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Controls.WebControls.Common.General;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data
{
    public class FormFieldDateData : FormFieldBaseTypeData<DateTime?>
    {
        public DateTime? DateFrom { get { return _validationParams.dateFrom; } set { _validationParams.dateFrom = value; } }
        public DateTime? DateTo { get { return _validationParams.dateTo; } set { _validationParams.dateTo = value; } }
        public bool ShowTime { get; set; }
        public FormFieldDateData()
        {
            this.RowCssClassBase = "form-row-date";
            _validationParams.isDate = true;
        }

        public new MyTxtBoxDate GetField()
        {
            return (MyTxtBoxDate)base.GetField();
        }
        
        protected override IMyFormWebControl createFieldControl()
        {
            MyTxtBoxDate txt = new MyTxtBoxDate();
            txt.HasTime = this.ShowTime;
            return txt;
            
        }
      
        public override DateTime? GetFormValue()
        {
            return GetField().FormValue;
        }
        
    }
}
