﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Util.Encoding.CSV;

namespace CS.General_v3.Controls.WebControls.Specialized.Reporting.v1
{
    internal abstract class BaseReportSectionBase : MyDiv
    {

         
				
		#region BaseReportSectionBase Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            public IBaseReportSectionDataBase Section { get; set; }
            protected BaseReportSectionBase _item = null;
            internal FUNCTIONALITY(BaseReportSectionBase item)
            {
                CS.General_v3.Util.ContractsUtil.RequiresNotNullable(item , "item");
                this._item = item;
            }
            
        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion




        public BaseReportSectionBase()
        {
        }
        
        private void initHeader()
        {
            MyDiv divHeader = new MyDiv();
            divHeader.CssManager.AddClass("cs-report-section-header-container");
            this.Controls.Add(divHeader);

            if (!string.IsNullOrWhiteSpace(this.Functionality.Section.Title))
            {
                MyHeading hTitle = new MyHeading(2, this.Functionality.Section.Title);
                hTitle.CssManager.AddClass("cs-report-section-title");
                divHeader.Controls.Add(hTitle);
            }
            if (!string.IsNullOrWhiteSpace(this.Functionality.Section.DescriptionHTML))
            {
                MyDiv divDesc = new MyDiv();
                divDesc.CssManager.AddClass("cs-report-section-desc-container");
                divDesc.InnerHtml = this.Functionality.Section.DescriptionHTML;

                divHeader.Controls.Add(divDesc);
            }
        }
        public CSVFile ExportAsCsv()
        {
            ReportSectionToCsvExporter exporter = new ReportSectionToCsvExporter(_tbl);
            return exporter.ExportToCsv();
        }

        private void renderCsvDownloadLink(Control parent)
        {
            
            aDownloadAsCsv = new MyButton("aDownloadAsCsv_Section" + this.Functionality.Section.SectionNumber);
            aDownloadAsCsv.Text = "Download as Csv";
            aDownloadAsCsv.Click += new EventHandler(aDownloadAsCsv_Click);
            aDownloadAsCsv.CssManager.AddClass("download-csv");
            parent.Controls.Add(aDownloadAsCsv);
        }
        protected abstract BaseReportSectionTableBase getTableData();
        private BaseReportSectionTableBase _tbl = null;
        private MyButton aDownloadAsCsv = null;
        private void initTableData()
        {
            MyDiv divTable = new MyDiv();
            {
                renderCsvDownloadLink(divTable);
                
            }
            divTable.CssManager.AddClass("cs-report-section-table-container");
            BaseReportSectionTableBase tbl = getTableData();
            _tbl = tbl;
            tbl.Functionality.Section = this.Functionality.Section;
            divTable.Controls.Add(tbl);
            this.Controls.Add(divTable);
        }

        void aDownloadAsCsv_Click(object sender, EventArgs e)
        {
            var file =  ExportAsCsv();
            string filename = "report-" + CS.General_v3.Util.Date.Now.ToString("ddMMyyyyHHmmss") + ".csv";
            var tmpFile =  CS.General_v3.Util.IO.GetTempFilename("csv");
            CS.General_v3.Util.IO.SaveToFile(tmpFile,file.GetFileAsString());
            CS.General_v3.Util.Web.ForceDownload(tmpFile, CS.General_v3.Util.Data.GetMIMEContentType(filename), filename);
        }
        private void initFooter()
        {
            if (!string.IsNullOrWhiteSpace(this.Functionality.Section.FooterHTML))
            {
                MyDiv divFooter = new MyDiv();
                divFooter.CssManager.AddClass("cs-report-section-footer-container");
                divFooter.InnerHtml = this.Functionality.Section.FooterHTML;
                this.Controls.Add(divFooter);
            }
        }

        private void init()
        {
            initHeader();
            initTableData();
            initFooter();
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            init();
           
        }
    }
}
