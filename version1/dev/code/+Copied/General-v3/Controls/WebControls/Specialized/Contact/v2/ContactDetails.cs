﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
//using CS.General_v3;
using CS.General_v3.Classes.MediaItems;
using System.Web.UI;
using CS.General_v3.Controls.Classes.Forms;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Controls.WebControls;
using CS.General_v3.Controls.WebControls.Common.General;


namespace CS.General_v3.Controls.WebControls.Specialized.Contact.v2
{

    public class ContactDetails : MyDiv
    {
        public class ContactDetail
        {
            public string Label { get; set; }
            public string Identifier { get; set; }
            public object Value { get; set; }
        }

        public class FUNCTIONALITY
        {
            public string Title { get; set; }
            public bool AddTitle { get; set; }
            /// <summary>
            /// Deprecated.  Use Opening Hours Html instead
            /// </summary>
            public Dictionary<string, string> OpeningHoursTable { get; set; }

            public string OpeningHoursHtml { get; set; }

            public ContactDetail Tel { get; set; }
            public ContactDetail Mob { get; set; }
            public ContactDetail Email { get; set; }
            public ContactDetail Address { get; set; }
            public ContactDetail Fax { get; set; }
            public ContactDetail Skype { get; set; }
            
            /* View full map link */

            private bool _viewMapLink = false;

            public bool ShowViewMapLink
            {
                get
                {
                    return _viewMapLink;
                }
                set
                {
                    _viewMapLink = value;
                }
            }

            public string FullMapLink { get; set; }

            /* End of view full map link */

            public string ContactUsOnlineText { get; set; }
            public string ContactUsOnlineLink { get; set; }

            public double? MapLat { get; set; }
            public double? MapLng { get; set; }
            public int? MapZoomLevel { get; set; }
            public string MapText { get; set; }
            public Enums.GOOGLE_MAP_TYPE MapType { get; set; }

            public List<ContactDetail> ContactDetails { get; private set; }

            private ContactDetails _control;
            public FUNCTIONALITY(ContactDetails ui)
            {
                _control = ui;
                AddTitle = true;
                MapZoomLevel = 14;
                MapType = Enums.GOOGLE_MAP_TYPE.Roadmap;
                ContactDetails = new List<ContactDetail>();
                Tel = new ContactDetail() { Label = "Tel:", Identifier = "tel" };
                Fax = new ContactDetail() { Label = "Fax:", Identifier = "fax" };
                Mob = new ContactDetail() { Label = "Mob:", Identifier = "mob" };
                Email = new ContactDetail() { Label = "Email:", Identifier = "email" };
                Address = new ContactDetail() { Label = "Address:", Identifier = "address" };
                Skype = new ContactDetail() { Label = "Skype:", Identifier = "skype" };
                MapText = "Our offices are marked by the marker below:";
                ContactDetails.Add(Tel);
                ContactDetails.Add(Fax);
                ContactDetails.Add(Mob);
                ContactDetails.Add(Email);
                ContactDetails.Add(Skype);
                ContactDetails.Add(Address);
                Title = "Contact Details";
                _control.CssManager.AddClass("contact-details");
                ContactUsOnlineText = "Online Enquiry Form";

            }
            public void CopyFromGeneralSettings()
            {
                Tel.Value = Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Telephone.GetSettingFromDatabase();
                Mob.Value = Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Mobile.GetSettingFromDatabase();
                Fax.Value = Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Fax.GetSettingFromDatabase();
                if (!String.IsNullOrEmpty(Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Skype.GetSettingFromDatabase()))
                {
                    Skype.Value = new HtmlAnchor() { HRef = "skype:" + Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Skype.GetSettingFromDatabase(), InnerHtml = Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Skype.GetSettingFromDatabase() };

                }
                Email.Value = new MyEmail() { Email = Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Email.GetSettingFromDatabase() };
                Address.Value = Settings.CompanyInfo.GetAddressAsOneLine(",<br />");
                if (Enums.SETTINGS_ENUM.CompanyInfo_GoogleMaps_Latitude.GetSettingFromDatabase<double?>() != null)
                {
                    MapLat = Enums.SETTINGS_ENUM.CompanyInfo_GoogleMaps_Latitude.GetSettingFromDatabase<double?>(); ;
                }
                if (Enums.SETTINGS_ENUM.CompanyInfo_GoogleMaps_Longtitude.GetSettingFromDatabase<double?>() != null)
                {
                    MapLng = Enums.SETTINGS_ENUM.CompanyInfo_GoogleMaps_Longtitude.GetSettingFromDatabase<double?>();
                }
                if (Enums.SETTINGS_ENUM.CompanyInfo_GoogleMaps_ZoomLevel.GetSettingFromDatabase<int?>() != null)
                {
                    MapZoomLevel = Enums.SETTINGS_ENUM.CompanyInfo_GoogleMaps_ZoomLevel.GetSettingFromDatabase<int?>();
                }
            }
            private void createControls()
            {
                if (AddTitle)
                {
                    HtmlGenericControl s = new HtmlGenericControl("span");
                    s.InnerText = Title;
                    s.Attributes["class"] = "contact-details-title";
                    _control.Controls.Add(s);
                }

                MyTable tbl = new MyTable();
                for (int i = 0; i < ContactDetails.Count; i++)
                {
                    ContactDetail cd = ContactDetails[i];
                    if (cd.Value != null && !String.IsNullOrEmpty(cd.Value.ToString()))
                    {
                        Control control = null;
                        if (cd.Value is Control)
                        {
                            control = (Control)cd.Value;

                        }
                        else
                        {
                            string value = cd.Value.ToString();
                            control = new Literal() { Text = value };
                        }
                        MyTableRow tr = tbl.AddRow("contact-details-row");
                        if (!String.IsNullOrEmpty(cd.Identifier))
                        {
                            tr.CssManager.AddClass(cd.Identifier);
                        }
                        tr.AddCell("label", new MyLabel() { Text = cd.Label });
                        tr.AddCell(cd.Identifier, control);

                    }
                }
                
                if (tbl.Rows.Count > 0)
                {
                    _control.Controls.Add(tbl);
                }

                /// opening hours table

                if (OpeningHoursHtml != null)
                {
                    if (AddTitle)
                    {
                        HtmlGenericControl s = new HtmlGenericControl("span");
                        s.InnerText = "Opening Hours";
                        s.Attributes["class"] = "contact-details-title";
                        _control.Controls.Add(s);
                    }
                    Literal litHtml = new Literal();
                    litHtml.Text = this.OpeningHoursHtml;
                    _control.Controls.Add(litHtml);
                }
                else
                {
                    if (OpeningHoursTable != null)
                    {
                        HtmlTable table = new HtmlTable();
                        table.CellPadding = 0;
                        table.CellSpacing = 0;
                        foreach (var item in OpeningHoursTable)
                        {
                            HtmlTableRow row = new HtmlTableRow();
                            HtmlTableCell cellDay = new HtmlTableCell();
                            cellDay.InnerText = item.Key;
                            cellDay.Attributes["class"] = "day";
                            HtmlTableCell cellValue = new HtmlTableCell();
                            cellValue.InnerText = item.Value;
                            row.Cells.Add(cellDay);
                            row.Cells.Add(cellValue);
                            table.Rows.Add(row);
                        }

                        if (table.Rows.Count > 0)
                        {
                            _control.Controls.Add(table);
                        }
                    }
                }

                if (!string.IsNullOrEmpty(ContactUsOnlineLink))
                {
                    MyParagraph pLink = new MyParagraph();
                    pLink.Controls.Add(new HtmlAnchor() { InnerHtml = ContactUsOnlineText, HRef = ContactUsOnlineLink });
                    pLink.CssManager.AddClass("contact-us-online");
                    _control.Controls.Add(pLink);
                }
                if (MapLat != null && MapLat.HasValue && MapLng != null && MapLng.HasValue)
                {
                    if (AddTitle)
                    {
                        HtmlGenericControl s = new HtmlGenericControl("span");
                        s.InnerText = "Location Map";
                        s.Attributes["class"] = "contact-details-title";
                        _control.Controls.Add(s);
                    }

                    MyDiv divMap = new MyDiv();
                    divMap.CssManager.AddClass("contact-details-google-map");
                    if (!string.IsNullOrEmpty(MapText))
                    {
                        _control.Controls.Add(new MyParagraph(MapText) { CssClass = "map-text" });
                    }
                    //Google maps
                    GoogleMaps.v3.GoogleMap map = new CS.General_v3.Controls.WebControls.Specialized.GoogleMaps.v3.GoogleMap();
                    //
                    map.Properties.Lat = MapLat.Value;
                    map.Properties.Lng = MapLng.Value;
                    map.Properties.MapType = MapType;
                    map.Properties.ZoomLevel = MapZoomLevel;
                    map.ID = this._control.ClientID + "googleMap";
                    map.Properties.Markers.Add(new CS.General_v3.Controls.WebControls.Specialized.GoogleMaps.v3.GoogleMarker() { Lat = MapLat.Value, Lng = MapLng.Value });

                    divMap.Controls.Add(map);

                    if (_viewMapLink)
                    {
                        HtmlAnchor anch = new HtmlAnchor();
                        anch.HRef = FullMapLink;
                        anch.Attributes["rel"] = "shadowbox[contactLink]";
                        anch.Attributes["class"] = "view-full-map-link";
                        divMap.Controls.Add(anch);
                    }

                    _control.Controls.Add(divMap);
                }
            }
            public void Init()
            {
                createControls();
            }
        }
        public new FUNCTIONALITY Functionality { get; private set; }
        public ContactDetails()
        {
            Functionality = new FUNCTIONALITY(this);
        }
        public ContactDetails(bool copyFromGeneralSettings)
            : this()
        {
            if (copyFromGeneralSettings)
            {
                Functionality.CopyFromGeneralSettings();
            }
        }
        protected override void OnLoad(EventArgs e)
        {
            Functionality.Init();
            base.OnLoad(e);
        }

    }
}
