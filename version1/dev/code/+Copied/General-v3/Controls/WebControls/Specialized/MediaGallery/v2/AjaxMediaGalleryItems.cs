﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_20101215.Controls.WebControls.Common;

namespace CS.General_20101215.Controls.WebControls.Specialized.MediaGallery.v2
{
    public class AjaxMediaGalleryItems : MyDiv
    {
        public class FUNCTIONALITY
        {
            private AjaxMediaGalleryItems _ui;

            public List<AjaxMediaGalleryItemData> Items { get; private set; }

            public FUNCTIONALITY(AjaxMediaGalleryItems ui)
            {
                _ui = ui;
                Items = new List<AjaxMediaGalleryItemData>();
            }
            private void createItems()
            {
                for (int i = 0; i < Items.Count; i++)
                {
                    AjaxMediaGalleryItem itemUI = new AjaxMediaGalleryItem();
                    itemUI.Functionality.Data = Items[i];
                    _ui.Controls.Add(itemUI);
                }
            }
            public void Init()
            {
                createItems();

            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public AjaxMediaGalleryItems()
        {
            Functionality = new FUNCTIONALITY(this);
            this.CssManager.AddClass("cs-media-gallery-items");
        }
        protected override void OnLoad(EventArgs e)
        {
            Functionality.Init();
            base.OnLoad(e);
        }
    }
}
