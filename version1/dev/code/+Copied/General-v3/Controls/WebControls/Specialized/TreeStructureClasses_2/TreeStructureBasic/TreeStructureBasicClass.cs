﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Controls.Classes.MediaGallery;
using CS.General_v3;
using CS.General_v3.Controls.WebControls.Classes;
namespace CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructureBasic
{
    [ToolboxData("<{0}:TreeStructureBasicClass runat=server></{0}:TreeStructureBasic>")]
    public class TreeStructureBasicClass : CS.General_v3.Controls.WebControls.BaseWebControl
    {
        /* HOW TO USE
         * ----------
         * 
         * Parameters to fill
         * 
         *  - RootFolders
         *  - ImageFolder
         *  - EditFolderPageName
         *  - EditItemPageName
         *  - ItemTitle
         *  - FolderTitle
         *  - CSSFile
         *  
         * Optional
         *  
         *  - QuerystringFolderIDParam
         *  - QuerystringParentFolderIDParam
         *  - QuerystringItemIDParam
         */
        public class FUNCTIONALITY
        {
            private TreeStructureBasicClass _tree = null;
            public FUNCTIONALITY(TreeStructureBasicClass tree)
            {
                this._tree = tree;

                this.CSSFile = "/_common/css/v1/components/tree.css";

                this.ItemTitle = "Item";
                this.PaddingPerLevel = 30;
                this.ShowPriorityAfterName = true;
            }
            #region Delegates
            
            #endregion
            #region Events
            

            #endregion
            #region Methods
            

            #endregion
            #region Properties
            public bool ShowPriorityAfterName { get; set; }
            /// <summary>
            /// Root Folders
            /// </summary>
            public List<ITreeItemBasic> RootItems { get; set; }
            /// <summary>
            /// Location of CSS file
            /// </summary>
            public string CSSFile { get; set; }
            /// <summary>
            /// A description of the item that this tree structure is portraying, e.g Category
            /// </summary>
            public string ItemTitle { get; set; }
            /// <summary>
            /// Padding per level, pixels
            /// </summary>
            public int PaddingPerLevel { get; set; }
            /*
            public string ImageURL_Edit_Up { get; set; }
            public string ImageURL_Edit_Over { get; set; }
            public string ImageURL_Delete_Up { get; set; }
            public string ImageURL_Delete_Over { get; set; }
            public string ImageURL_Add_Up { get; set; }
            public string ImageURL_Add_Over { get; set; }
            */


            #endregion



            private void renderCSSFile()
            {
                HtmlGenericControl css = new HtmlGenericControl("link");
                _tree.Controls.Add(css);
                css.Attributes["rel"] = "stylesheet";
                css.Attributes["type"] = "text/css";
                css.Attributes["href"] = this.CSSFile;

            }

            private HtmlTable tbTree = null;
            

            private int ctrlIndex = 0;
            private int totalMaxExtraButtons = 0;
            private void countTotalExtraButtons(ITreeItemBasic curr)
            {
                if (curr != null && curr.ExtraButtons != null && curr.ExtraButtons.Count > totalMaxExtraButtons)
                {
                    totalMaxExtraButtons = curr.ExtraButtons.Count;
                }
                List<ITreeItemBasic> list = null;
                if (curr == null)
                {
                    list = this.RootItems;
                }
                else
                {
                    list = curr.GetChildTreeItems();
                }

                foreach (var item in list)
                {
                    countTotalExtraButtons(item);
                }

            }

            private HtmlTableCell createButtonTd()
            {
                HtmlTableCell td = new HtmlTableCell();
                td.Style["width"] = "20px";
                td.Attributes["class"] = "button";
                return td;

            }
            private void processHrefButton(MyImage img, string imageUp, string imageOver)
            {
                img.Style["cursor"] = "pointer";
                img.ImageUrl = imageUp;
                img.RollOverImage = imageOver;



            }

            private int _renderCount = 0;
            private void addExtraButtons(HtmlTableRow tr, ITreeItemBasic item)
            {
                _renderCount++;
                if (item.ExtraButtons != null)
                {
                    int index = 0;
                    int emptyCells = this.totalMaxExtraButtons - item.ExtraButtons.Count;
                    for (int i = 0; i < emptyCells; i++)
                    {
                        HtmlTableCell td = createButtonTd();
                        td.InnerHtml = "&nbsp;";
                        tr.Cells.Insert(index, td); index++;
                        
                    }
                    for (int i=0; i < item.ExtraButtons.Count; i++)
                    {
                        var btn = item.ExtraButtons[i];

                        HtmlTableCell td = createButtonTd();
                        if (btn != null)
                        {
                            MyImageButton imgBtn = new MyImageButton(_tree.ID + "_extrabutton_" + item.ID + "_" + i + "_" + _renderCount);
                            if (btn.clickHandler != null)
                                imgBtn.Click += btn.clickHandler;
                            else
                            {
                                imgBtn.FunctionAsImage = true;
                                imgBtn.HRef = btn.Href;
                            }
                            if (!string.IsNullOrEmpty(btn.CssClass))
                                imgBtn.CssClass = btn.CssClass;
                            else
                                imgBtn.CssClass = "button";
                            imgBtn.ConfirmMessage = btn.ConfirmMessageOnClick;
                            imgBtn.ImageUrl = btn.ImageUrl_Up;
                            imgBtn.RollOverImage = btn.ImageUrl_Over;
                            
                            imgBtn.AlternateText = btn.Title;
                            imgBtn.Tag = item;
                            td.Controls.Add(imgBtn);
                        }
                        else
                        {
                            td.InnerHtml = "&nbsp;";
                        }
                        tr.Cells.Insert(index, td); index++;
                    }
                }
            }
            private HtmlTable createButtonTable(HtmlTableRow tr, ITreeItemBasic parentItem, ITreeItemBasic item)
            {
                
                HtmlTable tbButtons = new HtmlTable(); tbButtons.Attributes["class"] = "buttons";
                HtmlTableRow trButtons = new HtmlTableRow(); tbButtons.Rows.Add(trButtons); trButtons.Attributes["class"] = "buttons";
                /*
                if (oneHasAllowUpdate)
                {
                    HtmlTableCell tdButton = createButtonTd(); trButtons.Cells.Add(tdButton);
                    tdButton.InnerHtml = "&nbsp;";
                    if (item.AllowAdd)
                    {
                        tdButton.Controls.Clear();
                        MyImage img = new MyImage(); tdButton.Controls.Add(img);
                        processHrefButton(img, this.ImageURL_Add_Up, this.ImageURL_Add_Over);
                        img.AlternateText = "Add new " + ItemTitle;
                        img.HRef = item.AddNewItemURL;

                    }
                }
                if (oneHasAllowUpdate)
                {
                    HtmlTableCell tdButton = createButtonTd(); trButtons.Cells.Add(tdButton);
                     tdButton.InnerHtml = "&nbsp;";
                    if (item.AllowUpdate)
                    {
                        tdButton.Controls.Clear();
                        MyImage img = new MyImage(); tdButton.Controls.Add(img);
                        processHrefButton(img, this.ImageURL_Edit_Up,this.ImageURL_Edit_Over);
                        img.AlternateText = "Edit " + item.Title;
                        img.HRef = item.EditURL;
                        
                    }
                }
                if (oneHasAllowDelete)
                {
                    HtmlTableCell tdButton = createButtonTd(); trButtons.Cells.Add(tdButton);
                    tdButton.Attributes["class"] = "button"; tdButton.InnerHtml = "&nbsp;";
                    
                    if (item.AllowDelete)
                    {
                        tdButton.Controls.Clear();

                        MyImageButton btnDelete = new MyImageButton();
                        tdButton.Controls.Add(btnDelete);
                        btnDelete.Style["cursor"] = "pointer";
                        btnDelete.ImageUrl = this.ImageURL_Delete_Up;
                        btnDelete.RollOverImage = this.ImageURL_Delete_Over;
                        btnDelete.AlternateText = "Delete " + item.Title;
                        btnDelete.ID = _tree.ID + "_btnDelete_" + ctrlIndex;
                        btnDelete.Tag = item;
                        btnDelete.ConfirmMessage = !string.IsNullOrEmpty(item.Message_Confirm) ? item.Message_Confirm : "Are you sure you want to delete '" + item.Title + "'?";
                        btnDelete.Click += new EventHandler(btnDelete_Click);
                    }
                    

                }
                */
                addExtraButtons(trButtons, item);
                return tbButtons;
            }
            /*
            void btnDelete_Click(object sender, EventArgs e)
            {
                int k = 5;
                MyImageButton btn = (MyImageButton)sender;
                ITreeItem item = (ITreeItem)btn.Tag;
                item.Remove();
                string msg = !string.IsNullOrEmpty(item.Message_Delete) ? item.Message_Delete : "'" + item.Title + "' deleted successfully";
                showMessage(msg);
                CS.General_v3.Classes.URL.URLClass.RedirectToSamePage();
            }

            */
           
            private HtmlTableCell createDetails(ITreeItemBasic parentItem, ITreeItemBasic item, int level)
            {
                HtmlTableCell tdDetails = new HtmlTableCell();
                tdDetails.Attributes["class"] = "tree details";
                HtmlTable tb = new HtmlTable();
                tdDetails.Controls.Add(tb);
                tb.Attributes["class"] = "details";

                HtmlTableRow tr = new HtmlTableRow(); tr.Attributes["class"] = "tree";
                tb.CellPadding = 0;
                tb.CellSpacing = 0;
                tb.Rows.Add(tr);

                HtmlTableCell td;

                
                td = new HtmlTableCell();
                tr.Cells.Add(td); 
                td.Attributes["class"] = "tree icon";

                int paddingLeft = level * PaddingPerLevel;

                td.Style["padding-left"] = paddingLeft + "px";
                MyImage img = new MyImage(); img.ID = "imgIcon_" + ctrlIndex; td.Controls.Add(img);
                img.ImageUrl = item.ImageURL;
                img.AlternateText = item.Title;
                td = new HtmlTableCell(); tr.Cells.Add(td); td.Attributes["class"] = "tree name";
                string name = item.Title;
                if (ShowPriorityAfterName && item.Priority != null)
                    name += " [" + item.Priority + "]";
                if (string.IsNullOrEmpty(item.LinkURL))
                {
                    td.InnerText = name;
                }
                else
                {
                    img.HRef = item.LinkURL;
                    MyLink linkTitle = new MyLink();
                    linkTitle.Text = name;
                    linkTitle.HRef = item.LinkURL;
                    td.Controls.Add(linkTitle);
                }

                return tdDetails;
            }
            private void renderItem(ITreeItemBasic parentItem, ITreeItemBasic item, int level)
            {
                HtmlTableRow tr = new HtmlTableRow();
                tbTree.Rows.Add(tr);
                tr.Attributes["class"] = "tree";
                item.AddExtraButtons();
                tr.Cells.Add(createDetails(parentItem, item, level));

                HtmlTableCell td;
                td = new HtmlTableCell(); tr.Cells.Add(td); td.Attributes["class"] = "tree buttons";

                td.Controls.Add(createButtonTable(tr, parentItem, item));
                ctrlIndex++;
                List<ITreeItemBasic> list = item.GetChildTreeItems();
                foreach (var child in list)
                {
                    renderItem(item, child, level + 1);
                }
                
            }

            private void renderTree()
            {
                tbTree = new HtmlTable(); _tree.Controls.Add(tbTree); tbTree.ID = "tbTree_" + _tree.ID;
                tbTree.Attributes["class"] = "tree";
                var list = this.RootItems;
                foreach (var f in list)
                {
                    renderItem(null, f, 0);
                }


            }

            private void renderPriorityNote()
            {
                if (ShowPriorityAfterName)
                {
                    CS.General_v3.Controls.WebControls.Common.MyParagraph p = new MyParagraph("Note: The number in brackets is the priority value");
                    p.Style["border"] = "solid 1px #1CA1F9";
                    p.Style["background-color"] = "#F2F6F9";
                    p.Style["padding"] = "4px";
                    _tree.Controls.Add(p);
                }
            }

            /// <summary>
            /// Renders the tree
            /// </summary>
            public virtual void RenderTree()
            {
                checkParameters();
                countTotalExtraButtons(null);
                //checkAllowItems(null);
                renderCSSFile();
                renderTree();
                renderPriorityNote();
            }
            public virtual void checkParameters()
            {
                StringBuilder sb = new StringBuilder();
                if (string.IsNullOrEmpty(this.CSSFile))
                    sb.AppendLine("Please specify 'CSSFile'");
                if (this.RootItems == null)
                {
                    sb.AppendLine("Please set 'RootItems'");
                }
                if (sb.Length > 0)
                {
                    string s = "TreeStructure.TreeStructure:: " + sb.ToString();
                    throw new InvalidOperationException(s);
                }
            }
            


        }
        public FUNCTIONALITY Functionality {get; set;}



        public TreeStructureBasicClass()
        {
            this.Functionality = new FUNCTIONALITY(this);
           
        }
        #region Constants / Statics
        

        #endregion
        

        protected override void OnInit(EventArgs e)
        {

            
            base.OnInit(e);
        }

       

 
    }
}
