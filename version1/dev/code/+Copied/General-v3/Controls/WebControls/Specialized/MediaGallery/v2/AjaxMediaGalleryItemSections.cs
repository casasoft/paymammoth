﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_20101215.Controls.WebControls.Common;

namespace CS.General_20101215.Controls.WebControls.Specialized.MediaGallery.v2
{
    public class AjaxMediaGalleryItemSections : MyDiv
    {
        public class FUNCTIONALITY
        {
            public event AjaxMediaGalleryItemSection.FUNCTIONALITY.UploadSectionItemsHandler OnUploadItems;

            private AjaxMediaGalleryItemSections _ui;

            public List<AjaxMediaGalleryItemSectionData> Sections { get; private set; }

            public FUNCTIONALITY(AjaxMediaGalleryItemSections ui)
            {
                _ui = ui;
                Sections = new List<AjaxMediaGalleryItemSectionData>();
            }

            private void createSections()
            {
                for (int i = 0; i < Sections.Count; i++)
                {
                    AjaxMediaGalleryItemSectionData data = Sections[i];
                    AjaxMediaGalleryItemSection sectionUI = new AjaxMediaGalleryItemSection();
                    sectionUI.Functionality.Data = data;
                    _ui.Controls.Add(sectionUI);

                    sectionUI.Functionality.OnUploadItems += new AjaxMediaGalleryItemSection.FUNCTIONALITY.UploadSectionItemsHandler(Functionality_OnUploadItems);
                }
            }

            void Functionality_OnUploadItems(List<MyFileUpload> fileUploads, AjaxMediaGalleryItemSection section)
            {
                if (OnUploadItems != null)
                {
                    OnUploadItems(fileUploads, section);
                }
            }
            public void Init()
            {
                createSections();
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public AjaxMediaGalleryItemSections()
        {
            Functionality = new FUNCTIONALITY(this);
            this.CssManager.AddClass("cs-media-gallery-sections");
        }
        protected override void OnLoad(EventArgs e)
        {
            Functionality.Init();
            base.OnLoad(e);
        }
    }
}
