﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common.SWFObject.v2;

namespace CS.General_v3.Controls.WebControls.Specialized.RotatingBanner.v1
{
    public class FlashRotatingBannerV1 : SWFObject
    {
        public new class FUNCTIONALITY : SWFObject.FUNCTIONALITY
        {
            public List<FlashRotatingBannerV1ItemData> MediaItems { get; private set; }
            public string XmlMetaDataFile { get; set; }
            protected new FlashRotatingBannerV1 _control { get { return (FlashRotatingBannerV1)base._control; } }
            public FUNCTIONALITY(FlashRotatingBannerV1 control)
                : base(control)
            {
                this.MediaItems = new List<FlashRotatingBannerV1ItemData>();
            }
        }

        public new FUNCTIONALITY Functionality { get { return (FUNCTIONALITY)base.Functionality; } }

        public FlashRotatingBannerV1(string rotatingBannerSwfURL = "/_common/static/flash/v1/RotatingBanner/RotatingBannerComponent.swf", string flashVersion = "9.0.0")
            : base(rotatingBannerSwfURL, flashVersion, null)
        {
            this.CssManager.AddClass("flash-rotating-banner");
        }
        protected override SWFObject.FUNCTIONALITY getFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        private void outputParams()
        {
            if (string.IsNullOrEmpty(this.Functionality.XmlMetaDataFile))
            {
                throw new Exception("Please specify location of XML meta data file of slidehow.  Sample XML meta data file exists in location in _common");
            }
            this.Functionality.FlashVars.AddProperty("xmlMetaDataFile", this.Functionality.XmlMetaDataFile);
            for (int i = 0; i < this.Functionality.MediaItems.Count; i++)
            {
                FlashRotatingBannerV1ItemData mediaItem = this.Functionality.MediaItems[i];
                this.Functionality.FlashVars.AddProperty("itemURL" + i, mediaItem.ItemURL);

                if (!string.IsNullOrEmpty(mediaItem.ItemLink))
                {
                    this.Functionality.FlashVars.AddProperty("itemLink" + i, mediaItem.ItemLink);
                    this.Functionality.FlashVars.AddProperty("itemLinkTarget" + i, CS.General_v3.Util.EnumUtils.StringValueOf(mediaItem.ItemLinkTarget));
                }

                this.Functionality.FlashVars.AddProperty("delaySeconds" + i, mediaItem.DelaySeconds);
                if (mediaItem.SwfFileBackgroundColor.HasValue)
                {
                    this.Functionality.FlashVars.AddProperty("swfBackColor" + i, mediaItem.SwfFileBackgroundColor.Value);
                }
                this.Functionality.FlashVars.AddProperty("swfDurationSec" + i, mediaItem.SwfFilePlaybackDurationLengthSeconds);


                /* var url:String = li.parameters["itemURL" + count];
           var link:String = li.parameters["itemLink" + count];
           var linkTarget:String = li.parameters["itemLinkTarget" + count];
           var delaySeconds:Number = parseFloat(li.parameters["delaySeconds" + count]);?
                      
                      
           var swfPlaybackBackgroundColor:Number = parseFloat(li.parameters["swfBackColor" + count]);
           var swfPlaybackDurationSec:Number = parseFloat(li.parameters["swfDurationSec" + count]);
                 */
            }
        }
        protected override void OnLoad(EventArgs e)
        {
            outputParams();
            base.OnLoad(e);
        }



    }
}
