﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;

namespace CS.General_v3.Controls.WebControls.Specialized.Invoice
{
    using UserControls;

    public class InvoiceTemplate : MyDiv
    {

        #region Invoice details control reference

        private BaseUserControl _invoiceDetailsControl;
            
        #endregion


        #region Public varible data

        public string InvoiceText = "Invoice";
        public string ClientDetailsText = "Client Details";
        public string InvoiceDetailsText = "Invoice Details";
        public string VATText = "* Price includes 18% VAT";
        #endregion

        #region Private Constants

        private const string INVOICE_TEL_TEXT = "<strong>Tel: </strong>";
        private const string INVOICE_MAIL_TEXT = "<strong>Email: </strong>";
        private const string INVOICE_INVOICENO_TEXT = "<strong>Invoice No: </strong>";
        private const string INVOICE_DATE_TEXT = "<strong>Date: </strong>";
        private const string INVOICE_AUTHCODE_TEXT = "<strong>Auth. Code: </strong>";
               
        #endregion

        #region Private data variables
        private InvoiceInfo _invoiceInfo;
        #endregion

        #region Private content variables

        private MyDiv _headerDiv;
        private MyDiv _middleClientInvoiceDetailsDiv;
        private MyDiv _bottomInvoiceDetailsDiv;
        private MyDiv _footerDiv;

        private MyTable _priceValueTable;

        #endregion

        public InvoiceTemplate(InvoiceInfo invoiceInformation, BaseUserControl control, MyTable priceValueTable)
        {
            _invoiceInfo = invoiceInformation;
            _invoiceDetailsControl = control;
            _priceValueTable = priceValueTable;
            init();
        }

        #region Init function
        
        private void init()
        {
            initCssStyling();
            initRender();
        }                    

        #endregion

        #region Rendering functions

        private void initRender()
        {
            renderHeader();
            renderMiddleInvoiceClientDetails();
            renderInvoiceDetails();
            renderFooterSection();
            addSections();
        }

        #region Add all sections to current container

        private void addSections()
        {
            if (_headerDiv != null)
            {
                _headerDiv.CssClass = "invoice-header";
                Controls.Add(_headerDiv);
            }
            
            if (_middleClientInvoiceDetailsDiv != null)
            {
                _middleClientInvoiceDetailsDiv.CssClass = "invoice-middle-content";
                Controls.Add(_middleClientInvoiceDetailsDiv);
            }

            if (_bottomInvoiceDetailsDiv != null)
            {
                _bottomInvoiceDetailsDiv.CssClass = "invoice-bottom-details";
                Controls.Add(_bottomInvoiceDetailsDiv);
            }

            if (_footerDiv != null)
            {
                _footerDiv.CssClass = "invoice-footer";
                Controls.Add(_footerDiv);
            }
        }

        #endregion

        #region Render footer section
        private void renderFooterSection()
        {
            _footerDiv = new MyDiv();
            MySpan wrapper = new MySpan() { CssClass = "vat-text-wrapper", InnerHtml = VATText };
            MyDiv priceWrapper = new MyDiv()
            {
                CssClass = "invoice-price-wrapper"
            };
            if (_priceValueTable != null)
            {
                priceWrapper.Controls.Add(_priceValueTable);
            }
            _footerDiv.Controls.Add(wrapper);
            _footerDiv.Controls.Add(priceWrapper);
            Controls.Add(_footerDiv);
        }
        #endregion
        
        #region Render Main Invoice details (Control wrapper)
        private void renderInvoiceDetails()
        {
            _bottomInvoiceDetailsDiv = new MyDiv();
            MyDiv div = new MyDiv() { CssClass = "invoice-main-details-control-wrapper" };
            div.Controls.Add(_invoiceDetailsControl);
            _bottomInvoiceDetailsDiv.Controls.Add(div);
        }
        #endregion

        #region Render Middle Invoice Client Details

        private void renderMiddleInvoiceClientDetails()
        {
            _middleClientInvoiceDetailsDiv = new MyDiv();
            generateMiddleInvoiceClientDetailsHeader();
            MyDiv div = new MyDiv() { CssClass = "invoice-details-middle-main-content" };
            generateClientDetails(div);
            generateInvoiceDetails(div);
            div.Controls.Add(new MyDiv() { CssClass = "clear" });
            _middleClientInvoiceDetailsDiv.Controls.Add(div);
        }

        private void generateInvoiceDetails(MyDiv div)
        {
            MyDiv currentDiv = new MyDiv() { CssClass = "invoice-middle-invoice-details" };
            currentDiv.Controls.Add(new MySpan() { InnerHtml = InvoiceDetailsText, CssClass = "invoice-middle-title-main" });
            MyDiv subDiv = new MyDiv() { CssClass = "invoice-details-wrapper-content" };
            subDiv.Controls.Add(generateInvoiceDetailsTable());
            currentDiv.Controls.Add(subDiv);
            div.Controls.Add(currentDiv);
        }

        private MyTable generateInvoiceDetailsTable()
        {
            MyTable t = new MyTable();
            MyTableRow r1 = new MyTableRow();
            MyTableRow r2 = new MyTableRow();

            MyTableCell c1 = new MyTableCell() { InnerHtml = INVOICE_INVOICENO_TEXT, CssClass = "invoice-label" };
            MyTableCell c2 = new MyTableCell() { InnerHtml = _invoiceInfo.InvoiceNumber };
            MyTableCell c3 = new MyTableCell() { InnerHtml = INVOICE_DATE_TEXT, CssClass = "invoice-label" };
            MyTableCell c4 = new MyTableCell() { InnerHtml = _invoiceInfo.Date.ToShortDateString() };

            r1.Cells.Add(c1);
            r1.Cells.Add(c2);

            r2.Cells.Add(c3);
            r2.Cells.Add(c4);

            t.Rows.Add(r1);
            t.Rows.Add(r2);

            return t;
        }

        private void generateClientDetails(MyDiv div)
        {
            MyDiv currentDiv = new MyDiv() { CssClass = "invoice-middle-client-details" };
            currentDiv.Controls.Add(new MySpan() { InnerHtml = ClientDetailsText, CssClass = "invoice-middle-title-main" });
            currentDiv.Controls.Add(new MyDiv() { InnerHtml = _invoiceInfo.ClientDetails, CssClass = "invoice-middle-client-details-inner" });
            div.Controls.Add(currentDiv);
        }

        private void generateMiddleInvoiceClientDetailsHeader()
        {         
            MyDiv div = new MyDiv() { CssClass = "middle-invoice-top-wrapper" };
            div.Controls.Add(new MySpan() { InnerHtml = InvoiceText, CssClass = "invoice-text-middle-top" });
            div.Controls.Add(new MySpan() { InnerHtml = _invoiceInfo.InvoiceNumber });
            div.Controls.Add(new MyDiv() { CssClass = "clear" });
            _middleClientInvoiceDetailsDiv.Controls.Add(div);
        }

        #endregion
        
        #region Render header

        private void renderHeader()
        {
            _headerDiv = new MyDiv();
            renderLogo();
            renderHeaderDetails();
            _headerDiv.Controls.Add(new MyDiv() { CssClass = "clear" });
            Controls.Add(_headerDiv);
        }

        private void renderLogo()
        {
            if (!string.IsNullOrEmpty(_invoiceInfo.LogoURL))
            {
                MyImage img = new MyImage()
                {
                    ImageUrl = _invoiceInfo.LogoURL, AlternateText = _invoiceInfo.WebsiteTitle, HRef = _invoiceInfo.LogoURLHref, CssClass = "invoice-website-logo"
                };
                _headerDiv.Controls.Add(img);
            }
        }

        private void renderHeaderDetails()
        {
            MyDiv hDiv = new MyDiv() { CssClass="invoice-header-details-div" };
            renderMainTitle(hDiv);
            renderAddress(hDiv);
            renderTel(hDiv);
            renderMail(hDiv);
            renderAuthCode(hDiv);
            _headerDiv.Controls.Add(hDiv);
        }

        private void renderMainTitle(MyDiv div) { HeaderContentRenderer(_invoiceInfo.WebsiteTitle, "invoice-website-title", div); }
        private void renderAddress(MyDiv div) { HeaderContentRenderer(_invoiceInfo.Address, "invoice-address", div); }
        private void renderTel(MyDiv div) { HeaderContentRenderer(_invoiceInfo.Tel, "invoice-tel", div, INVOICE_TEL_TEXT); }
        private void renderMail(MyDiv div) { HeaderContentRenderer(_invoiceInfo.Email, "invoice-email", div, INVOICE_MAIL_TEXT, setAsMailItem: true); }
        private void renderAuthCode(MyDiv div) { HeaderContentRenderer(_invoiceInfo.AuthorizationCode, "invoice-authcode", div, INVOICE_AUTHCODE_TEXT); }

        private void HeaderContentRenderer(string valueToRender, string cssClass, MyDiv containerDiv, string BoldText = "", bool setAsMailItem = false) 
        {
            if (setAsMailItem)
            {
                valueToRender = Util.Text.EncodeEmailForSpam(valueToRender, valueToRender, null, true, null, null);
            }

            if (!string.IsNullOrEmpty(valueToRender))
            {
                containerDiv.Controls.Add(new MySpan() { CssClass = cssClass, InnerHtml = BoldText + valueToRender });
            }
        }
        

        #endregion
                
        #endregion
        
        #region Set properties to main div (inhereted)

        private void initCssStyling()
        {
            this.CssClass = "invoice-main-wrapper";
        }

        #endregion
    }
}
