﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;

namespace CS.General_v3.Controls.WebControls.Specialized.ShoppingCart
{
    public class InvoiceTemplate : MyDiv
    {
        private InvoiceInfo _invoiceInformation;
        private MyDiv _topWrapperDivReference;
        
        public MyDiv CartWrapperDiv { get; set; }
        
        public InvoiceTemplate(InvoiceInfo invoiceInformation, MyDiv cartWrapper)
        {
            CartWrapperDiv = cartWrapper;
            CssClass = "invoice-template-wrapper";
            _invoiceInformation = invoiceInformation;
            init();
        }

        public void init() 
        {
            initRenderData();
            loadCartPlaceHolder();
            //loadDeliveryDetails();
        }
        
        private void loadCartPlaceHolder()
        {
            Controls.Add(CartWrapperDiv);         
        }

        private void initRenderData()
        {
            renderTopWrapper();
            renderTitle();
            renderDate();
            renderAuthCode();
            renderClear();
        }

        private void renderClear()
        {
            _topWrapperDivReference.Controls.Add(new MyBr() { CssClass = "clear" });
        }

        private void renderTopWrapper()
        {
            _topWrapperDivReference = new MyDiv() { CssClass = "invoice-template-top-data-wrapper" };
            Controls.Add(_topWrapperDivReference);
        }

        private void renderAuthCode()
        {
            MySpan authCodeSpan = new MySpan() { CssClass = "invoice-template-auth-code-span" };
            authCodeSpan.InnerHtml = "Authorisation Code: " + _invoiceInformation.AuthorizationCode;
            _topWrapperDivReference.Controls.Add(authCodeSpan);
        }

        private void renderDate()
        {
            MySpan dateSpan = new MySpan() { CssClass = "invoice-template-date-span" };
            dateSpan.InnerHtml = _invoiceInformation.Date;
            _topWrapperDivReference.Controls.Add(dateSpan);
        }

        private void renderTitle()
        {
            MyImage imgTitle = new MyImage() { CssClass = "invoice-template-title" };
            imgTitle.ImageUrl = _invoiceInformation.TopImageUrl;
            _topWrapperDivReference.Controls.Add(imgTitle);
        }

        

    }
}
