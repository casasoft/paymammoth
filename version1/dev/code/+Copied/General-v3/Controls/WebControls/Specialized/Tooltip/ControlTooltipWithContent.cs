﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.JavaScript.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.General_v3.Controls.WebControls.Specialized.Tooltip
{
    public class ControlTooltipWithContent : ControlTooltip
    {
        public class ControlTooltipWithContentJSParams : ControlTooltipJSParams
        {

        }


        public new class FUNCTIONALITY : ControlTooltip.FUNCTIONALITY
        {
            protected new ControlTooltipWithContent _control { get { return (ControlTooltipWithContent)base._control; } set { base._control = value; } }

            public new ControlTooltipWithContentJSParams Parameters { get { return (ControlTooltipWithContentJSParams)base.Parameters; } protected set { base.Parameters = value; } }
            public Control TooltipContent { get; set; }
            public string CssClassTooltipDiv { get; set; }

            /// <summary>
            /// You can't use this
            /// </summary>
            public new WebControl Tooltip { get { throw new InvalidOperationException("Tooltip is set automatically, just set content"); } set { throw new InvalidOperationException("Tooltip is set automatically, just set content"); } }

            public FUNCTIONALITY(ControlTooltipWithContent control):base(control)
            {
                this.Parameters = new ControlTooltipWithContentJSParams();
                this.CssClassTooltipDiv = "tooltip-content-container";
            }

            internal void setBaseTooltip(WebControl control)
            {
                base.Tooltip = control;
            }
        }
        public new FUNCTIONALITY Functionality { get { return (FUNCTIONALITY)base.Functionality; } }
        public ControlTooltipWithContent()
        {
        }

        protected override ControlTooltip.FUNCTIONALITY getFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private void createTooltipContent()
        {
            MyDiv divTooltip = new MyDiv();
            divTooltip.CssManager.AddClass(this.Functionality.CssClassTooltipDiv);
            if (this.Functionality.TooltipContent == null) throw new Exception("Please specify TooltipContent - the content to show INSIDE the tooltip");


            divTooltip.Controls.Add(this.Functionality.TooltipContent);
            this.Functionality.setBaseTooltip(divTooltip);
        }
        protected override void OnPreRender(EventArgs e)
        {
            createTooltipContent();   
            base.OnPreRender(e);
        }

    }
}
