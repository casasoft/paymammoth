﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.WebControls.Specialized.MediaGallery
{
    public interface IGallerySection
    {
        string Name { get; set; }
        int ID { get; set; }
        int Priority { get; set; }
        IGalleryFolder FindGalleryFolderByID(int folderID);
        List<IGalleryFolder> GetGalleryFolders();
        IGalleryFolder CreateRootFolder();

    }
}
