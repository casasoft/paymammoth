﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using CS.General_v3.Controls.WebControls.Common;

namespace CS.General_v3.Controls.WebControls.Specialized.CodaSlider
{
    public class CodaSlider : MyDiv
    {
        public class FUNCTIONALITY
        {
            private CodaSlider _control;
            public FUNCTIONALITY(CodaSlider control)
            {
                _control = control;
            }
            /// <summary>
            /// Each slide content is a Div - i.e. it can be anything which goes into a Div Control
            /// </summary>
            public IEnumerable<MyDiv> SliderPanels { get; set; }
            /// <summary>
            /// If you want to auto-generate the navigation tabs, this should be left empty,otherwise
            /// it should supply the content of each navigation tab. Index(1) of CustomPanelTabContent
            /// should match Index(1) of SliderPanels, etc etc.
            /// </summary>
            public IEnumerable<MyListItem> CustomPanelTabContent { get; set; }

            public int CodaSliderID { get; set; }

            public string CodaSliderInitJSCode { get; set; }

            public bool? autoHeight { get; set; }
            public int? autoHeightEaseDuration { get; set; }
            public string autoHeightEaseFunction { get; set; }
            public bool? autoSlide { get; set; }
            public int? autoSlideInterval { get; set; }
            public bool? autoSlideStopWhenClicked { get; set; }
            public bool? crossLinking { get; set; }
            public bool? cufonRefresh { get; set; }
            public bool? dynamicArrows { get; set; }
            public string dynamicArrowLeftText { get; set; }
            public string dynamicArrowRightText { get; set; }
            public bool? dynamicTabs { get; set; }
            public string dynamicTabsAlign { get; set; }
            public string dynamicTabsPosition { get; set; }
            public string externalTriggerSelector { get; set; }
            public int? firstPanelToLoad { get; set; }
            public string panelTitleSelector { get; set; }
            public int? slideEaseDuration { get; set; }
            public string slideEaseFunction { get; set; }
        }

        public FUNCTIONALITY Functionality { get; private set; }
        public CodaSlider()
        {
            this.Functionality = getFunctionality();
        }
        protected virtual FUNCTIONALITY getFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            if (Functionality.CodaSliderID != null)
            {
                generateCodaSlider();
                generateInitCode();
            }
        }

        private void generateInitCode()
        {
            string options = string.Empty;
            List<string> optionsList = new List<string>();
            #region PopulateOptions
            if (this.Functionality.autoHeight.HasValue)
            {
                optionsList.Add("autoHeight: " + this.Functionality.autoHeight.ToString().ToLower());
            }
            if (this.Functionality.autoHeightEaseDuration.HasValue)
            {
                optionsList.Add("autoHeightEaseDuration: " + this.Functionality.autoHeightEaseDuration.ToString().ToLower());
            }
            if (!string.IsNullOrWhiteSpace(this.Functionality.autoHeightEaseFunction))
            {
                optionsList.Add("autoHeightEaseFunction: " + this.Functionality.autoHeightEaseFunction);
            }
            if (this.Functionality.autoSlide.HasValue)
            {
                optionsList.Add("autoSlide: " + this.Functionality.autoSlide.ToString().ToLower());
            }
            if (this.Functionality.autoSlideInterval.HasValue)
            {
                optionsList.Add("autoSlideInterval: " + this.Functionality.autoSlideInterval.ToString().ToLower());
            }
            if (this.Functionality.autoSlideStopWhenClicked.HasValue)
            {
                optionsList.Add("autoSlideStopWhenClicked: " + this.Functionality.autoSlideStopWhenClicked.ToString().ToLower());
            }
            if (this.Functionality.crossLinking.HasValue)
            {
                optionsList.Add("crossLinking: " + this.Functionality.crossLinking.ToString().ToLower());
            }
            if (this.Functionality.cufonRefresh.HasValue)
            {
                optionsList.Add("cufonRefresh: " + this.Functionality.cufonRefresh.ToString().ToLower());
            }
            if (this.Functionality.dynamicArrows.HasValue)
            {
                optionsList.Add("dynamicArrows: " + this.Functionality.dynamicArrows.ToString().ToLower());
            }
            if (!string.IsNullOrWhiteSpace(this.Functionality.dynamicArrowLeftText))
            {
                optionsList.Add("dynamicArrowLeftText: " + this.Functionality.dynamicArrowLeftText);
            }
            if (!string.IsNullOrWhiteSpace(this.Functionality.dynamicArrowRightText))
            {
                optionsList.Add("dynamicArrowRightText: " + this.Functionality.dynamicArrowRightText);
            }
            if (this.Functionality.dynamicTabs.HasValue)
            {
                optionsList.Add("dynamicTabs: " + this.Functionality.dynamicTabs.ToString().ToLower());
            }
            if (!string.IsNullOrWhiteSpace(this.Functionality.dynamicTabsAlign))
            {
                optionsList.Add("dynamicTabsAlign: " + this.Functionality.dynamicTabsAlign);
            }
            if (!string.IsNullOrWhiteSpace(this.Functionality.dynamicTabsPosition))
            {
                optionsList.Add("dynamicTabsPosition: " + this.Functionality.dynamicTabsPosition);
            }
            if (!string.IsNullOrWhiteSpace(this.Functionality.externalTriggerSelector))
            {
                optionsList.Add("externalTriggerSelector: " + this.Functionality.externalTriggerSelector);
            }
            if (this.Functionality.firstPanelToLoad.HasValue)
            {
                optionsList.Add("firstPanelToLoad: " + this.Functionality.firstPanelToLoad.ToString().ToLower());
            }
            if (!string.IsNullOrWhiteSpace(this.Functionality.panelTitleSelector))
            {
                optionsList.Add("panelTitleSelector: " + this.Functionality.panelTitleSelector);
            }
            if (this.Functionality.slideEaseDuration.HasValue)
            {
                optionsList.Add("slideEaseDuration: " + this.Functionality.slideEaseDuration.ToString().ToLower());
            }
            if (!string.IsNullOrWhiteSpace(this.Functionality.slideEaseFunction))
            {
                optionsList.Add("slideEaseFunction: " + this.Functionality.slideEaseFunction);
            }
            options = CS.General_v3.Util.Text.AppendStrings(", ", optionsList);
            

            #endregion

            string _jsString = "<script type=\"text/javascript\"> $().ready(function (){ $('#codaSlider" +
                               this.Functionality.CodaSliderID + "').codaSlider({" + options +
                               "});});</script>";
            //this.Functionality.CodaSliderInitJSCode = _jsString;
            MySpan jsSpan = new MySpan();
            jsSpan.InnerHtml = _jsString;
            this.Controls.Add(jsSpan);
        }

        private void generateCodaSlider()
        {
            MyDiv mainSliderWrapper = generateMainCodaSliderWrapper();
            MyDiv panels = generateSliderPanels();
            MyDiv navTabs = (Functionality.CustomPanelTabContent != null) ? generateCustomPanelTabContent() : null;
            mainSliderWrapper.Controls.Add(panels);
            if (!Functionality.dynamicArrows.Value)
            {
                mainSliderWrapper.Controls.Add(navTabs);
            }
            this.Controls.Add(mainSliderWrapper);
        }

        private MyDiv generateSliderPanels()
        {
            MyDiv panelsMainWrapper = new MyDiv();
            panelsMainWrapper.CssClass = "coda-slider preload";
            panelsMainWrapper.ID = "codaSlider" + this.Functionality.CodaSliderID.ToString();
            panelsMainWrapper.ClientIDMode = ClientIDMode.Static;

            foreach (MyDiv panelDiv in this.Functionality.SliderPanels)
            {
                MyDiv panelWrapper = new MyDiv();
                panelWrapper.CssClass = "panel-wrapper";

                MyDiv panel = new MyDiv();
                panel.CssClass = "panel";

                panelWrapper.Controls.Add(panelDiv);
                panel.Controls.Add(panelWrapper);
                panelsMainWrapper.Controls.Add(panel);
            }
            return panelsMainWrapper;
        }

        private MyDiv generateCustomPanelTabContent()
        {
            MyDiv navTabsMainWrapper = new MyDiv();
            navTabsMainWrapper.ID = "coda-nav-" + this.Functionality.CodaSliderID.ToString();
            navTabsMainWrapper.ClientIDMode = ClientIDMode.Static;
            navTabsMainWrapper.CssClass = "coda-nav";

            MyUnorderedList ulNavTabs = new MyUnorderedList();
            ulNavTabs.ID = "ulProductsListing";

            foreach (MyListItem li in this.Functionality.CustomPanelTabContent)
            {
                ulNavTabs.Controls.Add(li);
            }

            navTabsMainWrapper.Controls.Add(ulNavTabs);
            return navTabsMainWrapper;
        }

        private MyDiv generateMainCodaSliderWrapper()
        {
            MyDiv wrapper = new MyDiv();
            wrapper.CssClass = "coda-slider-wrapper";
            return wrapper;
        }
    }
}
