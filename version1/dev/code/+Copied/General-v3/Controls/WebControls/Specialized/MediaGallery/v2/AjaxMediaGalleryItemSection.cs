﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_20101215.Controls.WebControls.Common;

namespace CS.General_20101215.Controls.WebControls.Specialized.MediaGallery.v2
{
    public class AjaxMediaGalleryItemSection : MyDiv
    {
        public class FUNCTIONALITY
        {
            public delegate void UploadSectionItemsHandler(List<MyFileUpload> fileUploads, AjaxMediaGalleryItemSection section);
            public event UploadSectionItemsHandler OnUploadItems;
            private AjaxMediaGalleryItemSection _ui;

            public AjaxMediaGalleryItemSectionData Data { get; set; }

            public FUNCTIONALITY(AjaxMediaGalleryItemSection ui)
            {
                _ui = ui;
            }
            private void createItems()
            {
                MyDiv divTitle = new MyDiv();
                divTitle.CssManager.AddClass("cs-media-gallery-section-title");
                divTitle.InnerHtml = Data.Title;
                _ui.Controls.Add(divTitle);

                AjaxMediaGalleryItems galleryItems = new AjaxMediaGalleryItems();
                galleryItems.Functionality.Items.AddRange(Data.Items);
                _ui.Controls.Add(galleryItems);

                if (Data.CanUpload) {
                    AjaxMediaGalleryItemSectionUpload uploadSection = new AjaxMediaGalleryItemSectionUpload();
                    uploadSection.Functionality.Title = Data.UploadTitle;
                    uploadSection.Functionality.Text = Data.UploadText;
                    uploadSection.Functionality.SubmitButtonText = Data.UploadButtonText;

                    

                    uploadSection.Functionality.MaxUpload = Data.MaxAmtItemUploaders;
                    uploadSection.Functionality.MaxUploadText = Data.MaxUploadText.Replace("[TOTAL]", Data.MaxAmtItems.ToString());
                    uploadSection.Functionality.CantUploadMoreText = Data.CantUploadMore.Replace("[TOTAL]", Data.MaxAmtItems.ToString());
                    uploadSection.Functionality.OnUploadSubmit += new AjaxMediaGalleryItemSectionUpload.FUNCTIONALITY.OnUploadSubmitHander(Functionality_OnUploadSubmit);
                    uploadSection.ID = "cs-media-gallery-section-upload";
                    _ui.Controls.Add(uploadSection);
                    
                    Data.AlternateUploadContent = uploadSection;
                }
            }

            void Functionality_OnUploadSubmit(List<MyFileUpload> fileUploads)
            {
                if (OnUploadItems != null)
                {
                    OnUploadItems(fileUploads, _ui);
                }
            }
            public void Init()
            {
                createItems();    
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public AjaxMediaGalleryItemSection()
        {
            Functionality = new FUNCTIONALITY(this);
            this.CssManager.AddClass("cs-media-gallery-section");
        }
        protected override void OnLoad(EventArgs e)
        {
            Functionality.Init();
            base.OnLoad(e);
        }
    }
}
