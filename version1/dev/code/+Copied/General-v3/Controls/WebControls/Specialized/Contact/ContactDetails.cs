﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
//using CS.General_20101215;
using CS.General_20101215.Classes.MediaItems;
using System.Web.UI;
using CS.General_20101215.Controls.Classes.Forms;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CS.General_20101215.Controls.WebControls.Common;
using CS.General_20101215.Controls.WebControls;
using CS.General_20101215.Controls.WebControls.Common.General;


namespace CS.General_20101215.Controls.WebControls.Specialized.Contact
{

    public class ContactDetails : MyDiv
    {
        public class ContactDetail
        {
            public string Label { get; set; }
            public string Identifier { get; set; }
            public object Value { get; set; }
        }

        public class FUNCTIONALITY
        {
            public string Title { get; set; }
            public int HeadingType { get; set; }
            public bool AddTitle { get; set; }
            public Dictionary<string, string> OpeningHoursTable { get; set; }

            public ContactDetail Tel { get; set; }
            public ContactDetail Mob { get; set; }
            public ContactDetail Email { get; set; }
            public ContactDetail Address { get; set; }
            public ContactDetail Fax { get; set; }
            public ContactDetail Skype { get; set; }

            public string ContactUsOnlineText { get; set; }
            public string ContactUsOnlineLink { get; set; }

            public double? MapLat { get; set; }
            public double? MapLng { get; set; }
            public int? MapZoomLevel { get; set; }
            public string MapText { get; set; }
            public Enums.GOOGLE_MAP_TYPE MapType { get; set; }

            public List<ContactDetail> ContactDetails { get; private set; }

            private ContactDetails _control;
            public FUNCTIONALITY(ContactDetails ui)
            {
                _control = ui;
                AddTitle = true;
                MapZoomLevel = 14;
                MapType = Enums.GOOGLE_MAP_TYPE.Roadmap;
                ContactDetails = new List<ContactDetail>();
                Tel = new ContactDetail() { Label = "Tel:", Identifier = "tel" };
                Fax = new ContactDetail() { Label = "Fax:", Identifier = "fax" };
                Mob = new ContactDetail() { Label = "Mob:", Identifier = "mob" };
                Email = new ContactDetail() { Label = "Email:", Identifier = "email" };
                Address = new ContactDetail() { Label = "Address:", Identifier = "address" };
                Skype = new ContactDetail() { Label = "Skype:", Identifier = "skype" };
                MapText = "Our offices are marked by the marker below:";
                ContactDetails.Add(Tel);
                ContactDetails.Add(Fax);
                ContactDetails.Add(Mob);
                ContactDetails.Add(Email);
                ContactDetails.Add(Skype);
                ContactDetails.Add(Address);
                HeadingType = 2;
                Title = "Contact Details";
                _control.CssManager.AddClass("contact-details");
                ContactUsOnlineText = "Online Enquiry Form";

            }
            public void CopyFromGeneralSettings()
            {
                Tel.Value = Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Telephone.GetSettingValue();
                Mob.Value = Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Mobile.GetSettingValue();
                Fax.Value = Settings.CompanyInfo.Fax;
                if (!String.IsNullOrEmpty(Settings.CompanyInfo.Skype))
                {
                    Skype.Value = new HtmlAnchor() { HRef = "skype:" + Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Skype.GetSettingValue(), InnerHtml = Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Skype.GetSettingValue() };

                }
                Email.Value = new MyEmail() { Email = Settings.CompanyInfo.Email };
                Address.Value = Settings.CompanyInfo.GetAddressAsOneLine(",<br />");
                if (Settings.CompanyInfo.GoogleLat != null)
                {
                    MapLat = Settings.CompanyInfo.GoogleLat;
                }
                if (Settings.CompanyInfo.GoogleLat != null)
                {
                    MapLng = Settings.CompanyInfo.GoogleLng;
                }
                if (Settings.CompanyInfo.GoogleZoomLevel != null)
                {
                    MapZoomLevel = Enums.SETTINGS_ENUM.CompanyInfo_GoogleMaps_ZoomLevel.GetSettingValue<int?>();
                }
            }
            private void createControls()
            {
                if (AddTitle)
                {
                    MyHeading h = new MyHeading(HeadingType, Title);
                    _control.Controls.Add(h);
                }

                MyTable tbl = new MyTable();
                for (int i = 0; i < ContactDetails.Count; i++)
                {
                    ContactDetail cd = ContactDetails[i];
                    if (cd.Value != null && !String.IsNullOrEmpty(cd.Value.ToString()))
                    {
                        Control control = null;
                        if (cd.Value is Control)
                        {
                            control = (Control)cd.Value;

                        }
                        else
                        {
                            string value = cd.Value.ToString();
                            control = new Literal() { Text = value };
                        }
                        MyTableRow tr = tbl.AddRow("contact-details-row");
                        if (!String.IsNullOrEmpty(cd.Identifier))
                        {
                            tr.CssManager.AddClass(cd.Identifier);
                        }
                        tr.AddCell("label", new MyLabel() { Text = cd.Label });
                        tr.AddCell(cd.Identifier, control);

                    }
                }

                if (tbl.Rows.Count > 0)
                {
                    _control.Controls.Add(tbl);
                }

                /// opening hours table

                if (OpeningHoursTable != null)
                {
                    if (AddTitle)
                    {
                        MyHeading h = new MyHeading(HeadingType, "Opening Hours");
                        _control.Controls.Add(h);
                    }

                    HtmlTable table = new HtmlTable();

                    foreach (var item in OpeningHoursTable)
                    {
                        HtmlTableRow row = new HtmlTableRow();
                        HtmlTableCell cellDay = new HtmlTableCell();
                        cellDay.InnerText = item.Key;
                        cellDay.Attributes["class"] = "day";
                        HtmlTableCell cellValue = new HtmlTableCell();
                        cellValue.InnerText = item.Value;
                        row.Cells.Add(cellDay);
                        row.Cells.Add(cellValue);
                        table.Rows.Add(row);
                    }

                    if (table.Rows.Count > 0)
                    {
                        _control.Controls.Add(table);
                    }
                }

                if (!string.IsNullOrEmpty(ContactUsOnlineLink))
                {
                    MyParagraph pLink = new MyParagraph();
                    pLink.Controls.Add(new HtmlAnchor() { InnerHtml = ContactUsOnlineText, HRef = ContactUsOnlineLink });
                    pLink.CssManager.AddClass("contact-us-online");
                    _control.Controls.Add(pLink);
                }
                if (MapLat != null && MapLat.HasValue && MapLng != null && MapLng.HasValue)
                {
                    if (AddTitle)
                    {
                        MyHeading h = new MyHeading(HeadingType, "Location");
                        _control.Controls.Add(h);
                    }

                    MyDiv divMap = new MyDiv();
                    divMap.CssManager.AddClass("contact-details-google-map");
                    if (!string.IsNullOrEmpty(MapText))
                    {
                        _control.Controls.Add(new MyParagraph(MapText) { CssClass = "map-text" });
                    }
                    //Google maps
                    GoogleMaps.v3.GoogleMap map = new CS.General_20101215.Controls.WebControls.Specialized.GoogleMaps.v3.GoogleMap();
                    //
                    map.Properties.Lat = MapLat.Value;
                    map.Properties.Lng = MapLng.Value;
                    map.Properties.MapType = MapType;
                    map.Properties.ZoomLevel = MapZoomLevel;
                    map.ID = this._control.ClientID + "googleMap";
                    map.Properties.Markers.Add(new CS.General_20101215.Controls.WebControls.Specialized.GoogleMaps.v3.GoogleMarker() { Lat = MapLat.Value, Lng = MapLng.Value });

                    divMap.Controls.Add(map);
                    _control.Controls.Add(divMap);
                }
            }
            public void Init()
            {
                createControls();
                
            }
        }
        public new FUNCTIONALITY Functionality { get; private set; }
        public ContactDetails()
        {
            Functionality = new FUNCTIONALITY(this);
        }
        public ContactDetails(bool copyFromGeneralSettings)
            : this()
        {
            if (copyFromGeneralSettings)
            {
                Functionality.CopyFromGeneralSettings();
            }
        }
        protected override void OnLoad(EventArgs e)
        {
            Functionality.Init();
            base.OnLoad(e);
        }

    }
}
