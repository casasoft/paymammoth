﻿using System;
namespace CS.General_v3.Controls.WebControls.Specialized.ShoppingCart
{
    public interface IProductItem 
    {
        long ID { get; }
        string Details { get; }
        string ImageURL { get;}

        string Colour { get; }
        string Size { get; }

        string Name { get; }
        int Quantity { get; }
        double TotalPrice { get; }
        double UnitPrice { get; }
        string ItemURL { get; }

    }
}
