﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_20090518.Controls.WebControls.Common;
using CS.General_20090518.JavaScript.jQuery.Plugins.Uploadify;
using System.Collections.Specialized;
using CS.General_20090518.JavaScript.Interfaces;
using CS.General_20090518.Util;

namespace CS.General_20090518.Controls.WebControls.Specialized.MediaGallery.v2
{
    public class MediaGallerySettings : IJavaScriptObject
    {
        public List<MediaGalleryItemSectionData> sections = new List<MediaGalleryItemSectionData>();

        /*settings.deleteScript = "/ajax/tmp/deleteTempImage.ashx";
            settings.reorderScript = "/ajax/tmp/reorderImages.ashx";
            settings.cropScript = "/ajax/tmp/cropTempImage.ashx";
            settings.captionScript = "/ajax/tmp/updateCaption.ashx";*/

        public string deleteScript = "/ajax/tmp/deleteTempImage.ashx";
        public string cropScript = "/ajax/tmp/cropTempImage.ashx";
        public string reorderScript = "/ajax/tmp/reorderImages.ashx";
        public UploadifySettings uploadifySettings = new UploadifySettings();
        public string uploadScriptSectionIdParam = "sectionID";
        public string uploadScriptResponseUploadedMediaItemParam = "item";




        public string deleteScriptIdParam = "id";
        public NameValueCollection deleteScriptExtraParams = new NameValueCollection();
        public string deleteScriptResponseIdParam = "id";
        public string deleteScriptResponseSuccessParam = "success";
        public string deleteScriptResponseSuccessValue = "true";



        public string cropScriptParamId = "id";
        public string cropScriptParamCropTypeId = "cropType";
        public string cropScriptResponseSuccessParam = "success";
        public string cropScriptResponseSuccessValue = "true";
        public string cropScriptParamLeft = "x";
        public string cropScriptParamTop = "y";
        public string cropScriptParamBottom = "y2";
        public string cropScriptParamRight = "x2";
        public string cropScriptErrorMessage = "An error has been encountered whilst cropping image.  Please try again.";
        public NameValueCollection cropScriptExtraParams = new NameValueCollection();

        public string reorderScriptOrderParam = "order";
        public NameValueCollection reorderScriptExtraParams = new NameValueCollection();
        public string reorderScriptResponseSuccessParam = "success";
        public string reorderScriptResponseSuccessValue = "true";
        public string reorderItemsErrorMessage = "An error has been encountered while trying to reorder images.  Please try again.";

        public string confirmDeleteMessage = "Are you sure you want to delete the selected item?";
        public string deleteItemErrorMessage = "An error has been encountered while trying to delete image.  Please try again.";

        public string captionScript = "/ajax/tmp/updateCaption.ashx";
        public string captionScriptParamValue = "value";
        public string captionOverTooltip = "Click on caption to edit caption.  Press ENTER to save changes.  Press ESC to cancel changes";


        /*(
        public string deleteScript;
        public string cropScript;
        public string reorderScript;
        public _UploadifySettings uploadifySettings;
        public string uploadScriptSectionIdParam = "sectionID";
        public string uploadScriptResponseUploadedMediaItemParam = "item";




        public string deleteScriptIdParam = "id";
        public Dictionary deleteScriptExtraParams = new Dictionary();
        public string deleteScriptResponseIdParam = "id";
        public string deleteScriptResponseSuccessParam = "success";
        public string deleteScriptResponseSuccessValue = "true";



        public string cropScriptParamId = "id";
        public string cropScriptParamCropTypeId = "cropType";
        public string cropScriptResponseSuccessParam = "success";
        public string cropScriptResponseSuccessValue = "true";
        public string cropScriptParamLeft = "x";
        public string cropScriptParamTop = "y";
        public string cropScriptParamBottom = "y2";
        public string cropScriptParamRight = "x2";
        public string cropScriptErrorMessage = "An error has been encountered whilst cropping image.  Please try again.";
        public Dictionary cropScriptExtraParams = new Dictionary();

        public string reorderScriptOrderParam = "order";
        public Dictionary reorderScriptExtraParams = new Dictionary();
        public string reorderScriptResponseSuccessParam = "success";
        public string reorderScriptResponseSuccessValue = "true";
        public string reorderItemsErrorMessage = "An error has been encountered while trying to reorder images.  Please try again.";

        public string confirmDeleteMessage = "Are you sure you want to delete the selected item?";
        public string deleteItemErrorMessage = "An error has been encountered while trying to delete image.  Please try again.";

        public string captionScript;
        public string captionScriptParamValue = "value";
        public string captionOverTooltip = "Click on caption to edit caption.  Press ENTER to save changes.  Press ESC to cancel changes";*/

        #region IJavaScriptObject Members

        public JavaScript.Data.JSObject GetJsObject(bool forFlash = false)
        {
            return JSUtil.GetJSObjectFromTypeWithReflection(this, forFlash);
        }

        #endregion
    }
}
