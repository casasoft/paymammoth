﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace CS.General_v3.Controls.WebControls.Specialized.MediaGallery
{
    public abstract class AjaxMediaGalleryUploadifyBaseHandler : CS.General_v3.HTTPHandlers.BaseAjaxHandler
    {
        //public AjaxMediaGalleryItemData UploadedItemData { get; set; }
        protected string _paramItem = "item";
        
        protected override void processRequest(System.Web.HttpContext context)
        {

            
           
            
            var f = context.Request.Files[0];
            AjaxMediaGalleryItemData item = UploadItem(context, f);

            this.AddProperty(_paramItem, item);



            base.processRequest(context);
            
        }

        /// <summary>
        /// The request to handle the upload of the item.
        /// 
        /// You must specify other parameters with the 'scriptData' parameter of the AjaxMediaGalleryItemSectionData so that
        /// they are appended to the form variables.
        /// </summary>
        /// <param name="fileContent"></param>
        /// <returns></returns>
        protected abstract AjaxMediaGalleryItemData UploadItem(HttpContext context, HttpPostedFile fileContent);
    }
}
