﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.Interfaces.Hierarchy;

namespace CS.General_v3.Controls.WebControls.Specialized.Hierarchy
{
    public class HierarchyData : CS.General_v3.Controls.WebControls.Specialized.Hierarchy.IHierarchy, CS.General_v3.Classes.Interfaces.Hierarchy.IHierarchyNavigation
    {
        public HierarchyData()
        {
            Visible = true;
            _children = new List<HierarchyData>();
        }

        #region IHierarchy Members

        public string Title
        {
            get;
            set;
        }

        public string Href
        {
            get;
            set;
        }


        public HierarchyData Parent { get; private set; }

        private IList<HierarchyData> _children;

        public void AddChild(IHierarchy child)
        {
            _children.Add((HierarchyData)child);

            HierarchyData childParentHierarchy = (HierarchyData)child;
            childParentHierarchy.Parent = this;
        }


        public IHierarchy ParentHierarchy
        {
            get { return Parent; }
        }

        #endregion

        #region IHierarchy Members


        public bool Selected { get; set; }

        #endregion

        /// <summary>
        /// Returns the full category name e.g. Laser Printers > Colour
        /// </summary>
        /// <param name="hierarchyItem"></param>
        /// <param name="delimiter"></param>
        /// <param name="reverseOrder"></param>
        /// <returns></returns>
        public static string GetFullCategoryName(IHierarchy hierarchyItem, string delimiter, bool reverseOrder, bool includeRoot)
        {
            if (hierarchyItem == null)
            {
                return null;
            }
            else
            {
                //return hierarchyItem.GetFullHierarchyName(delimiter, reverseOrder, includeRoot);
                string s = hierarchyItem.Title;
                if (hierarchyItem.GetParents().FirstOrDefault() != null && (includeRoot || (hierarchyItem.GetParents().FirstOrDefault().GetParents().FirstOrDefault() != null)))
                {
                    if (reverseOrder)
                    {
                        return s + delimiter + GetFullCategoryName(hierarchyItem.GetParents().FirstOrDefault(), delimiter, reverseOrder, includeRoot);
                    }
                    else
                    {
                        return GetFullCategoryName(hierarchyItem.GetParents().FirstOrDefault(), delimiter, reverseOrder, includeRoot) + delimiter + s;
                    }
                }
                else
                {
                    return s;
                }
            }
        }

        /// <summary>
        /// Returns the full category name e.g. Laser Printers > Colour
        /// </summary>
        /// <param name="hierarchyItem"></param>
        /// <param name="delimiter"></param>
        /// <param name="reverseOrder"></param>
        /// <returns></returns>
        public static string GetFullCategoryIDs(IHierarchy hierarchyItem, string delimiter, bool reverseOrder, bool includeRoot)
        {
            if (hierarchyItem == null)
            {
                return null;
            }
            else
            {

                string s = hierarchyItem.ID.ToString();
                if (hierarchyItem.GetParents().FirstOrDefault() != null && (includeRoot || (hierarchyItem.GetParents().FirstOrDefault().GetParents().FirstOrDefault() != null)))
                {
                    if (reverseOrder)
                    {
                        return s + delimiter + GetFullCategoryIDs(hierarchyItem.GetParents().FirstOrDefault(), delimiter, reverseOrder, includeRoot);
                    }
                    else
                    {
                        return GetFullCategoryIDs(hierarchyItem.GetParents().FirstOrDefault(), delimiter, reverseOrder, includeRoot) + delimiter + s;
                    }
                }
                else
                {
                    return s;
                }
            }
        }
        #region IHierarchy Members


        public string GetFullHierarchyName(string delimiter, bool reverseOrder, bool includeRoot)
        {
            return GetFullCategoryName(this, delimiter, reverseOrder, includeRoot);
        }

        #endregion

        #region IHierarchy Members

        public long ID
        {
            get;
            set;
        }

        #endregion

        #region IHierarchy Members


        public bool Visible
        {
            get;
            set;
        }

        #endregion

        public Enums.HREF_TARGET HrefTarget
        {
            get;
            set;
        }


        #region IHierarchy Members


        public IEnumerable<IHierarchy> GetChildren()
        {
            return this._children;

        }

        public IEnumerable<IHierarchy> GetParents()
        {
            return CS.General_v3.Util.ListUtil.GetListFromSingleItem(this.ParentHierarchy);
        }

        #endregion


        IEnumerable<General_v3.Classes.Interfaces.Hierarchy.IHierarchy> General_v3.Classes.Interfaces.Hierarchy.IHierarchy.GetChildren()
        {
            return (IEnumerable<General_v3.Classes.Interfaces.Hierarchy.IHierarchy>)GetChildren();
        }

        IEnumerable<General_v3.Classes.Interfaces.Hierarchy.IHierarchy> General_v3.Classes.Interfaces.Hierarchy.IHierarchy.GetParents()
        {
            return (IEnumerable<General_v3.Classes.Interfaces.Hierarchy.IHierarchy>)GetParents();
        }

        IEnumerable<General_v3.Classes.Interfaces.Hierarchy.IHierarchyNavigation> General_v3.Classes.Interfaces.Hierarchy.IHierarchyNavigation.GetChildren()
        {
            return _children;
        }

        IEnumerable<General_v3.Classes.Interfaces.Hierarchy.IHierarchyNavigation> General_v3.Classes.Interfaces.Hierarchy.IHierarchyNavigation.GetParents()
        {
            return CS.General_v3.Util.ListUtil.GetListFromSingleItem(this.Parent);
        }


        IHierarchyNavigation IHierarchyNavigation.GetMainParent()
        {
            return (General_v3.Classes.Interfaces.Hierarchy.IHierarchyNavigation)GetParents().FirstOrDefault();
        }


        General_v3.Classes.Interfaces.Hierarchy.IHierarchy General_v3.Classes.Interfaces.Hierarchy.IHierarchy.GetMainParent()
        {
            return (General_v3.Classes.Interfaces.Hierarchy.IHierarchy)GetParents().FirstOrDefault();
        }
    }
}
