﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.JavaScript.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.General_v3.Controls.WebControls.Specialized.Tooltip
{
    using JavaScript.Data;

    public class ControlTooltip : MyDiv
    {
        public class ControlTooltipJSParams : JavaScriptObject
        {
            /// <summary>
            /// You do not need to provide this, given that you specify the Tooltip content
            /// </summary>
            public string tooltipContentID;
            /// <summary>
            /// You do not need to provide this.
            /// </summary>
            public string tooltipIconID;

            public int marginX;
            public int marginY;
            public int fadeDurationMs = 300;
            public CS.General_v3.JavaScript.Classes.JSEnums.POSITION alignPosition = JavaScript.Classes.JSEnums.POSITION.Right;
            public string cssClassRollover = "tooltip-icon-over";
            public string cssClassNormal = "tooltip-icon";
            public string cssClassTooltipContent = "tooltip-content";

        }


        public class FUNCTIONALITY
        {
            protected ControlTooltip _control;

            public ControlTooltipJSParams Parameters { get; protected set; }
            public string CssClassTooltipIcon { get; set; }
            public WebControl Tooltip { get; set; }
            public FUNCTIONALITY(ControlTooltip control)
            {
                _control = control;
                this.CssClassTooltipIcon = "tooltip-icon-control";
                
                this.Parameters = new ControlTooltipJSParams();
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public ControlTooltip()
        {
            this.Functionality = getFunctionality();
        }

        protected virtual FUNCTIONALITY getFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private void addJS()
        {
            if (this.Functionality.Tooltip == null) {
                throw new Exception("Please specify tooltipContent of tooltip");
            }
            this.Controls.Add(this.Functionality.Tooltip);
            this.Functionality.Parameters.tooltipContentID = this.Functionality.Tooltip.ClientID;
            this.Functionality.Parameters.tooltipIconID = this.ClientID;


            string js = "new js.com.cs."+CS.General_v3.Settings.Others.JAVASCRIPT_VERSION+".UI.Tooltip.ControlTooltip("+this.Functionality.Parameters.GetJsObject().GetJS()+");";
            CS.General_v3.Util.JSUtilOld.AddJSScriptToPage(js);
            //js = CS.General_v3.Util.JSUtil.MakeJavaScript(js, true);
            //this.Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID + "_tooltipJS", js);
        }

        protected override void OnLoad(EventArgs e)
        {
            this.CssManager.AddClass(this.Functionality.CssClassTooltipIcon);
            
            base.OnLoad(e);
        }
        protected override void OnPreRender(EventArgs e)
        {
            addJS();
            base.OnPreRender(e);
        }

    }
}
