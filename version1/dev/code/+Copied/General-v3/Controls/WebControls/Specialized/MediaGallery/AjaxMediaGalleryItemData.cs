﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;
using CS.General_v3.JavaScript.Interfaces;
namespace CS.General_v3.Controls.WebControls.Specialized.MediaGallery
{
    public class AjaxMediaGalleryItemData : IJavaScriptObject
    {
        public int ID { get; set; }
        public bool CanDelete { get; set; }
        public bool CanCrop { get; set; }
        public bool CanReorder { get; set; }
        public bool CanEditCaption { get; set; }

        public string ThumbURL { get; set; }
        public string ItemURL { get; set; }
        public string Caption { get; set; }
        public string SectionName { get; set; }
        public int? Width { get; set; }
        public int? Height { get; set; }

        /// <summary>
        /// If item is an image and can crop in different sizes, fill this in with the respective sizes
        /// </summary>
        public List<AjaxMediaGalleryItemCropData> CropSizes { get; set; }

        public AjaxMediaGalleryItemData()
        {
            CropSizes = new List<AjaxMediaGalleryItemCropData>();
        }

   

        

        /* public bool canDelete;
        public bool canCrop;
        public bool canReorder;

        public string thumbURL;
        public string itemURL;
        public string caption;
        public string sectionName;
        public string id;
        public int width;
        public int height;

        public MediaGalleryItemCropData[] cropSizes;*/

        #region IJavaScriptObject Members

        public IJavaScriptSerializable GetJsObject(bool forFlash = false)
        {
            JSObject obj = forFlash ? new JSObjectFlash() : new JSObject();
            obj.AddProperty("id", ID.ToString());
            obj.AddProperty("canDelete", CanDelete);
            obj.AddProperty("canCrop", CanCrop);
            obj.AddProperty("canReorder", CanReorder);
            obj.AddProperty("canEditCaption", CanEditCaption);
            obj.AddProperty("thumbURL", ThumbURL);
            obj.AddProperty("itemURL", ItemURL);
            obj.AddProperty("caption", Caption);
            obj.AddProperty("sectionName", SectionName);
            obj.AddProperty("width", Width);
            obj.AddProperty("height", Height);
            JSArray arrCrop = new JSArray();
            if (CropSizes != null)
            {
                for (int i = 0; i < CropSizes.Count; i++)
                {
                    arrCrop.AddItem(CropSizes[i].GetJsObject());
                }
            }
            obj.AddProperty("cropSizes", arrCrop);
            return obj;
        }

        #endregion
    }
}
