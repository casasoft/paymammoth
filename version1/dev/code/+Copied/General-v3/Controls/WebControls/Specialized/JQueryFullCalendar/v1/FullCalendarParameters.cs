﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;

namespace CS.General_v3.Controls.WebControls.Specialized.JQueryFullCalendar.v1
{
    using JavaScript.Data;

    public class FullCalendarParameters : JavaScriptObject
    {
        public string divCalendarID;
        public bool onChangeViewRefreshCufon;

        public _jQueryFullCalendarOptions options = new _jQueryFullCalendarOptions();
    }
}
