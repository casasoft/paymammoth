﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_20101215.Controls.Classes.Forms;
//using CS.General_20101215.Collections.Specialized.FormFields;
using System.Web.UI;

using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CS.General_20101215.Controls.WebControls.Common;
using CS.General_20101215.Controls.WebControls;
using CS.General_20101215.Controls.WebControls.Common.General;

namespace CS.General_20101215.Controls.WebControls.Specialized
{
    /// <summary>
    /// Similar to form fields but wherever there is a numeric or date, it shows as two fields to enable ranges
    /// </summary>
    [ToolboxData("<{0}:FormFieldsSearch runat=server></{0}:FormFieldsSearch>")]   
    public class FormFieldsSearch : FormFields
    {
        /*
        protected override MyTxtBox addTextBox(MyTxtBox txt, HtmlTableRow tr, HtmlTableCell td)
        {

            if (txt is MyTxtBoxNumeric || txt is MyTxtBoxDate)
            {
                //These will need to set up a RANGE not just one field.
               // _items.RemoveAt(_items.Count - 1); //Remove the last entered as we're going to replace that with ItemRange
                MyTxtBox txt2 = null;
                
                if (txt is MyTxtBoxNumericDouble)
                    txt2 = new MyTxtBoxNumericDouble();
                else if (txt is MyTxtBoxNumericInteger)
                    txt2 = new MyTxtBoxNumericInteger();
                else if (txt is MyTxtBoxDate)
                    txt2 = new MyTxtBoxDate();
                CS.General_20101215.Util.Other.CloneShallow(txt,txt2);
                System.Collections.IEnumerator enumerator = txt.Attributes.Keys.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    string key = (string)enumerator.Current;
                    txt2.Attributes[key] = txt.Attributes[key];
                }
                td.Controls.Clear(); // Clear all controls as we need to add a table

                HtmlTable tbl = new HtmlTable();
                tbl.CellPadding = tbl.CellSpacing = 0;
                td.Controls.Add(tbl);
                tbl.Rows.Add(new HtmlTableRow());
                HtmlTableCell tdFrom = new HtmlTableCell();
                HtmlTableCell tdMiddle = new HtmlTableCell();
                HtmlTableCell tdTo = new HtmlTableCell();
                tbl.Rows[0].Cells.Add(tdFrom);
                tbl.Rows[0].Cells.Add(tdMiddle);
                tbl.Rows[0].Cells.Add(tdTo);

                tdFrom.Controls.Add(txt);
                tdMiddle.InnerHtml = "&nbsp;-&nbsp;";
                tdTo.Controls.Add(txt2);

                //_items.Add(new FormFieldItemRange(tr, txt, txt2));

            }
            else
            {
                base.addTextBox(txt, tr, td); //Add a text box to the TD and adds it to the list of items

            }
            return txt;

        }

        /// <summary>
        /// Get the value of the field by ID.  If it is in search mode and the field ID is a number,
        /// or is a date, then a list of values are passed for the FROM - TO
        /// </summary>
        /// <param name="fieldID"></param>
        /// <returns></returns>
        public object GetValueTo(string fieldID)
        {
            FormFieldItem item = GetFormFieldItem(fieldID);
            if (!(item is FormFieldItemRange))
            {
                throw new InvalidCastException("To be able to get the 'to' value, it must be a field that supports from/to, i.e Numeric or Date");
            }
            if (item != null)
            {
                return ((FormFieldItemRange)item).ValueTo;
            }
            return null;

        }*/

    }
}
