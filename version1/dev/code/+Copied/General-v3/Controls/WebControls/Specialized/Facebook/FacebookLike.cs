﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using System.Web;
using System.Web.UI.WebControls;

namespace CS.General_v3.Controls.WebControls.Specialized.Facebook
{

    #region Facebook Like button Enumerations
    /// <summary>
    /// Type of layout.
    /// </summary>
    public enum LikeButtonLayout
    {
        Standard,
        ButtonCount,
        BoxCount
    }

    /// <summary>
    /// Verb to display with button
    /// </summary>
    public enum LikeButtonVerb
    {
        Like,
        Recommend
    }

    /// <summary>
    /// Color scheme for like button
    /// </summary>
    public enum LikeButtonColorScheme
    {
        Light,
        Dark
    }
    #endregion

    public class FacebookLike : MyDiv
    {
        #region Private variables
        private string FaceBookIFrame = "<iframe src=\"http://www.facebook.com/plugins/like.php?href={0}&amp;layout={1}&amp;show_faces={2}&amp;action={3}&amp;colorscheme={4}\" scrolling=\"no\" frameborder=\"0\" style=\"border:none; overflow:hidden;\" allowTransparency=\"{5}\"></iframe>";
        #endregion

        #region Public properties
        public new FUNCTIONALITY Functionality { get; set; }
        #endregion
        
        #region Functionality class
        /// <summary>
        /// Wraps up the various FacebookLike class properties
        /// </summary>
        public class FUNCTIONALITY 
        {
            private FacebookLike _ctrl;
            private string _mainCssClass = "facebook-like-main-wrapper";

            public FUNCTIONALITY(FacebookLike ctrl)
            {
                _ctrl = ctrl;
            }

            #region Public Properties

            /// <summary>
            /// Sets the main CSS class of the Facebook like wrapper
            /// </summary>
            /// <value>Default value: facebook-like-main-wrapper</value>
            public string MainWrapperCssClass { 
                internal get { return _mainCssClass; }
                set { _mainCssClass = value; }
            }

            /// <summary>
            /// Gets or sets URL of page to associate with button
            /// </summary>
            public string Url { get; set; }

            /// <summary>
            /// Gets or sets width of layout.
            /// </summary>
            //public int Width { get;set; }

            /// <summary>
            /// Type of layout to use for the area.
            /// </summary>
            public LikeButtonLayout Layout { get; set; }

            /// <summary>
            /// Gets or sets verb to display
            /// </summary>
            public LikeButtonVerb Verb { get; set; }

            /// <summary>
            /// Gets or sets color scheme for Like button
            /// </summary>
            public LikeButtonColorScheme ColorScheme { get; set; }

            /// <summary>
            /// Gets or sets option if profile picture to show
            /// next to comment or not.
            /// </summary>
            public bool ShowFaces { get; set; }

            /// <summary>
            /// Gets or sets reference tag to include with the link.
            /// </summary>
            /// <remarks>
            /// The length of this tag can not be more than 50 characters.
            /// </remarks>
            public string RefTag { get; set; }
            #endregion
        }
        #endregion


        public FacebookLike(bool loadPresets = true)
        {
            Functionality = new FUNCTIONALITY(this);
            loadFacebookLikePresets(loadPresets);
            setFacebookLikeDivCssClass();
        }

        private void loadFacebookLikePresets(bool loadPresets)
        {
            Functionality.Layout = LikeButtonLayout.Standard;
            Functionality.ColorScheme = LikeButtonColorScheme.Light;
           // Functionality.Width = 450;
            Functionality.ShowFaces = true;
            Functionality.Verb = LikeButtonVerb.Like;
        }

        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            initRender();
        }

        private void initRender()
        {
            renderFacebookControl();
        }

        private void renderFacebookControl()
        {
            ValidateSettings();
            Literal facebookLiteral = new Literal();
            facebookLiteral.Mode = LiteralMode.PassThrough;

            facebookLiteral.Text = string.Format(FaceBookIFrame,
                GetUrl(),
                GetLayout(),
                Functionality.ShowFaces ? "true" : "false",
                //Functionality.Width,
                GetAction(),
                GetColorScheme(),
                //35, // Height
                //Functionality.Width,
                //35, // Height
                "true"); // Transparency
                
            Controls.Add(facebookLiteral);
        }

        #region Private functionality
        private void ValidateSettings()
        {
            if (string.IsNullOrEmpty(this.Functionality.Url))
            {
                this.Functionality.Url = CS.General_v3.Util.PageUtil.GetCurrentUrlLocation().GetURL(fullyQualified: true);
            }
            this.Functionality.Url = CS.General_v3.Util.PageUtil.GetAbsoluteUrlFromRelative(this.Functionality.Url);
            /*if (Functionality.Width <= 50)
            {
                Functionality.Width = 450;
            }*/
        }

        private void setFacebookLikeDivCssClass() 
        {
            CssClass = Functionality.MainWrapperCssClass;
        }

        private string GetUrl()
        {



            return HttpUtility.UrlEncode(Functionality.Url);
        }

        private string GetLayout()
        {
            switch (Functionality.Layout)
            {
                case LikeButtonLayout.ButtonCount:
                    return "button_count";
                case LikeButtonLayout.BoxCount:
                    return "box_count";
                default:
                    return "standard";
            }
        }

        private string GetColorScheme()
        {
            switch (Functionality.ColorScheme)
            {
                case LikeButtonColorScheme.Dark:
                    return "dark";
                default:
                    return "light";
            }
        }

        private string GetAction()
        {
            switch (Functionality.Verb)
            {
                case LikeButtonVerb.Recommend:
                    return "recommend";
                default:
                    return "like";
            }
        }
        #endregion

    }
}


        
    

