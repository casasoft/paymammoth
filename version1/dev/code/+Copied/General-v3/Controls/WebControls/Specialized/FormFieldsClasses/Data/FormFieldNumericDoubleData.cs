﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common.General;
using CS.General_v3.Controls.WebControls.Common;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data
{
    
    public class FormFieldNumericDoubleData : FormFieldNumericBaseData<double?>
    {
        
        public FormFieldNumericDoubleData()
        {
            this.RowCssClassBase = "form-row-double";

        }
        public new MyTxtBoxNumericDouble GetField()
        {
            return (MyTxtBoxNumericDouble)base.GetField();
        }
        protected override IMyFormWebControl createFieldControl()
        {
            
                MyTxtBoxNumericDouble _txtDouble = new MyTxtBoxNumericDouble();
            return _txtDouble;
        }
        public override double? GetFormValue()
        {
            return this.GetField().FormValue;
        }
    }
}
