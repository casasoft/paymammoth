﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web.UI.HtmlControls;
using CS.General_v3.Controls.WebControls.Common;

namespace CS.General_v3.Controls.WebControls.Specialized.BottomBar
{
    public class BottomColumnSiteMap : BottomColumn
    {

        public new class FUNCTIONALITY : BottomColumn.FUNCTIONALITY
        {

            private MyDiv _wrapperDIV;
            public HierarchicalControl HierarchyControl { get; set; }

            public FUNCTIONALITY(BottomColumnSiteMap col)
                : base(col)
            {
                HierarchyControl = new HierarchicalControl();
            }

            private void initHeading()
            {
                HtmlGenericControl titleSpan = new HtmlGenericControl("span");
                titleSpan.Attributes["class"] = "title";
                titleSpan.InnerText = Title;
                _wrapperDIV.Controls.Add(titleSpan);
            }


            private void generateWrapper()
            {
                _wrapperDIV = new MyDiv();
                _wrapperDIV.Attributes["class"] = "site-map-wrapper";
                PlaceHolder.Controls.Add(_wrapperDIV);
            }

            private void generateSiteMap()
            {
                if (HierarchyControl.Properties.Items.Count() > 0)
                {
                    
                    _wrapperDIV.Controls.Add(HierarchyControl);
                }
            }
            
            internal override void Init()
            {
                generateWrapper();
                initHeading();
                generateSiteMap();
                base.Init();
            }

        }
        public new FUNCTIONALITY Functionality
        {
            get { return (FUNCTIONALITY)base.Functionality; }
            set
            {
                base.Functionality = value;
            }
        }

        public BottomColumnSiteMap()
        {
            Functionality = new FUNCTIONALITY(this);
        }

    }
}
