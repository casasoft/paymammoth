﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_20101215.Controls.WebControls.Common;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI;

using CS.General_20101215.Controls.WebControls.Specialized.ContentPages.ContentPages;
namespace CS.General_20101215.Controls.WebControls.Specialized.Navigation
{
    /// <summary>
    /// The ContentPageMenu is a generic menu structure which is usually
    /// used at the bottom of the website.  It consists of a title (h3)tag 
    /// and the child pages are displayed in UL's and LI's underneath.
    /// 
    /// Use CSS to style accordingly
    /// </summary>
    public class ContentPageMenu : MyDiv
    {
        public class PropertiesClass
        {
            
            public PropertiesClass()
            {
                HeadingType = 3;
                AddLinkAlsoToEmptyPages = true;
            }

            public IContentPage ContentPage { get; set; }
            public int HeadingType { get; set; }
            public int MaxDepth { get; set; }
            /// <summary>
            /// Add a link also to pages which has ContainsContent() = false
            /// </summary>
            public bool AddLinkAlsoToEmptyPages { get; set; }
            
            
        }

        public PropertiesClass Properties { get; set; }

        public ContentPageMenu()
        {
            this.Properties = new PropertiesClass();
            this.CssClass = "content-page-menu";
        }

        private HtmlGenericControl getChildUl(IList<IContentPage> ChildPages, int depth)
        {
            HtmlGenericControl ul = new HtmlGenericControl("ul");

            for (int i = 0; i < ChildPages.Count; i++)
            {
                HtmlGenericControl li = new HtmlGenericControl("li");
                IContentPage cp = ChildPages[i];
                if (Properties.AddLinkAlsoToEmptyPages || cp.ContainsContent())
                {
                    HtmlAnchor aHref = new HtmlAnchor();
                    aHref.InnerHtml = aHref.Title = cp.Title;
                    aHref.HRef = cp.GetUrl();
                    li.Controls.Add(aHref);
                }
                else
                {
                    li.Controls.Add(new Literal() { Text = cp.Title });
                }
                if (cp.HasChildPages() && (Properties.MaxDepth == 0 || depth < Properties.MaxDepth))
                {
                    li.Controls.Add(getChildUl(cp.GetChildPages(), depth + 1));
                }
                ul.Controls.Add(li);
            }

            return ul;
        }

        private void initControls()
        {
            MyHeading heading = new MyHeading(Properties.HeadingType, null);
            if (Properties.AddLinkAlsoToEmptyPages ||  Properties.ContentPage.ContainsContent())
            {
                HtmlAnchor aHeading = new HtmlAnchor();
                aHeading.InnerHtml = aHeading.Title = Properties.ContentPage.Title;
                aHeading.HRef = Properties.ContentPage.GetUrl();
                heading.Controls.Add(aHeading);
            }
            else
            {
                //Not clickable
                heading.InnerHtml = Properties.ContentPage.Title;
            }
            Controls.Add(heading);
            if (Properties.ContentPage.HasChildPages()) {
                Controls.Add(getChildUl(Properties.ContentPage.GetChildPages(), 1));
            }
        }
        protected override void OnLoad(EventArgs e)
        {
            initControls();
            base.OnLoad(e);
        }
    }
}
