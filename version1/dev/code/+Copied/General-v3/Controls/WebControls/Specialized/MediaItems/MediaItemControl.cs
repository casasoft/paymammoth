﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_20101215.Controls.WebControls.Common.General;
using CS.General_20101215.Controls.WebControls.Common;
using CS.General_20101215.Classes.MediaItems;

namespace CS.General_20101215.Controls.WebControls.Specialized.MediaItems
{
    public class MediaItemControl: MyTable, IMyFormWebControl
    {
        public delegate bool OnDeleteHandler(MediaItemControl sender, IMediaItem itemToDelete);
        public event OnDeleteHandler OnDelete_Before;
        public event EventHandler OnDelete_After;


        private MyTableRow trImage = null;
        private MyFileUpload txtUpload;
        private MyImage imgMediaItem;
        private MyButton btnDelete;
        public class MediaItemControlFunctionality
        {
            private MediaItemControl _ctrl = null;
            public MediaItemControlFunctionality(MediaItemControl ctrl)
            {
                this._ctrl = ctrl;
            }
            public bool IsRequired { get; set; }
            public IMediaItem MediaItem { get; set; }
            public MyFileUpload GetFileUploadControl()
            {
                return _ctrl.txtUpload;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            renderTable();
            base.OnLoad(e);
        }
        private void renderTable()
        {
            this.CssManager.AddClass("media-item-control");
            {
                if (this.Functionality.MediaItem != null && !string.IsNullOrWhiteSpace(this.Functionality.MediaItem.GetMediaItemUrl()))
                {
                    imgMediaItem.ImageUrl = this.Functionality.MediaItem.GetThumbnailImageUrl();
                    imgMediaItem.HRef = this.Functionality.MediaItem.GetMediaItemUrl();
                    imgMediaItem.HRefTarget = Enums.HREF_TARGET.Blank;
                    var imageRow = this.AddRow("media-item-control-image-row");
                    trImage = imageRow;
                    imageRow.AddCell("media-item-control-image", imgMediaItem);
                    if (!this.Functionality.IsRequired)
                    {
                        imageRow.AddCell("media-item-control-delete", btnDelete);
                    }
                    else
                    {
                        imageRow.AddCell("media-item-control-delete", "&nbsp;");
                    }
                }
            }
            {
                var fileUploadRow = this.AddRow("media-item-control-upload");
                var tdUpload = fileUploadRow.AddCell("media-item-control-upload", txtUpload);
                tdUpload.ColumnSpan = 2;
            }
        }

        protected virtual MediaItemControlFunctionality createFunctionality()
        {
            return new MediaItemControlFunctionality(this);
        }
        public MediaItemControlFunctionality Functionality { get; private set; }
        private void initControls()
        {
            txtUpload = new MyFileUpload();
            txtUpload.ID =  this.ID + "txtFileUpload_MediaItem";
            imgMediaItem = new MyImage();
            btnDelete = new MyButton(this.ID + "_btnDelete");
            btnDelete.Text = "Delete";
            btnDelete.Click += new EventHandler(btnDelete_Click);

        }

        void btnDelete_Click(object sender, EventArgs e)
        {
            bool delete = true;
            if (this.OnDelete_Before != null)
                delete = OnDelete_Before(this, this.Functionality.MediaItem);
            if (delete)
            {
                this.Functionality.MediaItem.Delete();
                trImage.Visible = false;
                if (OnDelete_After != null)
                    OnDelete_After(this, e);
            }

            
        }
        protected MyFormWebControlFunctionality _formWebControlFunctionality = null;

        public MediaItemControl() : this(null,null)
        {

        }
        public MediaItemControl(string id, IMediaItem item)
        {
            this.ID = id;
            _formWebControlFunctionality = new MyFormWebControlFunctionality(this, this.ClientIDSeparator);
           // _formWebControlFunctionality.JSFieldClassName = "FieldMediaItem";
            _formWebControlFunctionality.AddFieldJS = false;
            this.Functionality = createFunctionality();
            this.Functionality.MediaItem = item;
            initControls();
        }


        #region IMyFormWebControl Members

        
        object IMyFormWebControl.Value
        {
            get
            {
                return txtUpload.FileContent;
                
            }
            set
            {
                int k = 5;
            }
        }

        public bool DoNotValidateOnBlur
        {
            get
            {
                return _formWebControlFunctionality.DoNotValidateOnBlur;
            }
            set
            {
                _formWebControlFunctionality.DoNotValidateOnBlur =value;
            }
        }

        public string ClientName
        {
            get { return _formWebControlFunctionality.ClientName; }
        }

        public bool HasValidation
        {
            get
            {
                return _formWebControlFunctionality.HasValidation;
            }
            set
            {
                _formWebControlFunctionality.HasValidation = value;
            }
        }

        public FormFieldsClasses.FormFieldsValidationParams.FormFieldValidationSubGroupParams SubGroupParams
        {
            get
            {
                return _formWebControlFunctionality.SubGroupParams;
            }
            set
            {
                _formWebControlFunctionality.SubGroupParams = value;
            }

        }

        public FormFieldsClasses.FormFieldsValidationParams ValidationParams
        {
            get
            {
                return _formWebControlFunctionality.ValidationParams;
            }
            set { _formWebControlFunctionality.UpdateValidationParams(value); }

        }

        public FormFieldsClasses.FormFieldsCSSClasses FieldCssClasses
        {
            get
            {
                return _formWebControlFunctionality.FieldCssClasses;
            }
            set
            {
                _formWebControlFunctionality.FieldCssClasses = value;
            }

        }

        public object FormValueObject
        {
            get
            {
                return txtUpload.FormValue;
            }
        }

        public int? GetFormValueAsIntNullable()
        {
            throw new NotImplementedException();
        }

        public string GetFormValueAsStr()
        {
            throw new NotImplementedException();
        }

        public bool Required
        {
            get
            {
                return _formWebControlFunctionality.Required;
            }
            set
            {
                _formWebControlFunctionality.Required = value;
            }
        }

        public Enums.DATA_TYPE DataType
        {
            get { return Enums.DATA_TYPE.FileUploadWithMediaItem; }
        }

        public void FocusGood()
        {
            _formWebControlFunctionality.FocusGood();
            
        }

        public new MyFormWebControlFunctionality WebControlFunctionality
        {
            get { return (MyFormWebControlFunctionality)_formWebControlFunctionality.WebControlFunctionality; }
        }

        #endregion

        #region IMyBaseFormWebControl Members

        public string Title
        {
            get
            {
                return _formWebControlFunctionality.Title;
                
            }
            set
            {
                _formWebControlFunctionality.Title = value;
            }
        }

        public string HelpID
        {
            get
            {
                return _formWebControlFunctionality.HelpID;

            }
            set
            {
                _formWebControlFunctionality.HelpID = value;
            }

        }

        public string HelpMsg
        {
            get
            {
                return _formWebControlFunctionality.HelpMsg;

            }
            set
            {
                _formWebControlFunctionality.HelpMsg = value;
            }

        }

        public string ValidationGroup
        {
            get
            {
                return _formWebControlFunctionality.ValidationGroup;

            }
            set
            {
                _formWebControlFunctionality.ValidationGroup = value;
            }

        }

        public string Class_Disabled
        {
            get
            {
                return _formWebControlFunctionality.Class_Disabled;

            }
            set
            {
                _formWebControlFunctionality.Class_Disabled = value;
            }

        }

        public string Class_onFocus
        {
            get
            {
                return _formWebControlFunctionality.Class_Focus;

            }
            set
            {
                _formWebControlFunctionality.Class_Focus = value;
            }

        }

        public string Class_onError
        {
            get
            {
                return _formWebControlFunctionality.Class_Error;

            }
            set
            {
                _formWebControlFunctionality.Class_Error = value;
            }

        }

        public string Class_onOver
        {
            get
            {
                return _formWebControlFunctionality.Class_Over;

            }
            set
            {
                _formWebControlFunctionality.Class_Over = value;
            }

        }

        public string Class_ReadOnly
        {
            get
            {
                return _formWebControlFunctionality.Class_ReadOnly;

            }
            set
            {
                _formWebControlFunctionality.Class_ReadOnly = value;
            }

        }

        public string Class_Required
        {
            get
            {
                return _formWebControlFunctionality.Class_Required;

            }
            set
            {
                _formWebControlFunctionality.Class_Required = value;
            }

        }

        public bool UseDefaultCSSClasses
        {
            get
            {
                return _formWebControlFunctionality.UseDefaultCSSClasses;

            }
            set
            {
                _formWebControlFunctionality.UseDefaultCSSClasses = value;
            }

        }

        #endregion

        #region IBaseControl Members

        public new System.Web.UI.Control Control
        {
            get { return this.txtUpload; }
        }

        BaseControlFunctionality IBaseControl.WebControlFunctionality
        {
            get { return _formWebControlFunctionality.WebControlFunctionality; }
            
        }

        #endregion
    }
}
