﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_20101215.Controls.WebControls.Specialized.ContentPages
{
    public interface IPage : MenuTreeStructure.IMenuItem
    {
        string Text { get; set; }
        bool IsHTML { get; }
    }
        
}
