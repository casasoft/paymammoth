﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructure
{
    public abstract class TreeItem: TreeStructureBasic.TreeItemBasic,  ITreeItem
    {

        public bool AddedExtraButtons { get; set; }
        public abstract string EditURL { get; }

        
        public abstract bool AllowUpdate { get; }
        public abstract bool AllowDelete { get; }
        public abstract bool AllowAddSubItems { get; }
        public virtual void Remove()
        {

        }
        public virtual void Save()
        {
        }
        
        /// <summary>
        /// Delete message to show.  If left null, default message is shown
        /// </summary>
        public virtual string Message_DeleteOK { get { return "Deleted successfully"; } }
        /// <summary>
        /// Confirm message to show.  If left null, default message is shown
        /// </summary>
        public virtual string Message_ConfirmDelete { get { return "Are you sure you want to delete?"; } }



        #region ITreeItem Members

        public virtual bool CanRemove(out string errorMessage)
        {
            errorMessage = "";
            return true;
        }
        #endregion
    }
        
}
