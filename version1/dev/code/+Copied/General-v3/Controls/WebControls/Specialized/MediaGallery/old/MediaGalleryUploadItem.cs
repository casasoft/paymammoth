﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
namespace CS.General_v3.Controls.WebControls.Specialized.MediaGallery
{
    public class MediaGalleryUploadItem
    {
        public MyFileUpload TxtUpload { get; set; }
        public MyTxtBoxTextSingleLine TxtCaption { get; set; }
        
        public MediaGalleryUploadItem(MyFileUpload txtUpload, MyTxtBoxTextSingleLine txtCaption)
        {
            TxtUpload = txtUpload;
            TxtCaption = txtCaption;
        }
    }
}
