﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.WebControls.Specialized.MediaGallery
{

    internal class MediaItemLinkButton : MediaItemButton
    {
        public MediaGallery.FUNCTIONALITY.MediaItemHrefRetriever HrefRetriever { get; set; }
        public MediaItemLinkButton(MediaGallery.FUNCTIONALITY.MediaItemHrefRetriever hrefRetriever, string id) : base(id)
        {
            this.HrefRetriever = hrefRetriever;
        }
        public MediaItemLinkButton() { }
    }
}
