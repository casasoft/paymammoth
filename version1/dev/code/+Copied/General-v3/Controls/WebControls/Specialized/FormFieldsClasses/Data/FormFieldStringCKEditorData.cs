﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Controls.WebControls.Common.General;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data
{
    public class FormFieldStringCKEditorData : FormFieldStringMultilineData
    {

        public FormFieldStringCKEditorData()
        {
            this.RowCssClassBase = "form-row-ckeditor";
        }
        public new CKEditor GetField()
        {
            return (CKEditor)(IMyFormWebControl)base.GetField();
        }

        protected override IMyFormWebControl createFieldControl()
        {
                CKEditor _ckEditor = new CKEditor();
            _ckEditor.Rows = this.Rows;

            
            return _ckEditor;
        }
        public override string GetFormValue()
        {
            return this.GetField().FormValue;
        }
    }
}
