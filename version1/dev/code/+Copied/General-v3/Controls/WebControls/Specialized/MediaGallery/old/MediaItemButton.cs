﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;

namespace CS.General_v3.Controls.WebControls.Specialized.MediaGallery
{

    public class MediaItemButton : MyButton
    {
        public MediaItemButton() : base("")
        {

        }
        public MediaItemButton(string id) : base(id)
        {

        }
    }
}
