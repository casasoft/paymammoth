﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Controls.Classes.MediaGallery;
using System.IO;
using CS.General_v3.Classes.MediaItems;
using CS.General_v3.Util;
namespace CS.General_v3.Controls.WebControls.Specialized.MediaGallery
{
    [ToolboxData("<{0}:MediaGallery runat=server></{0}:MediaGallery>")]
    public class MediaGallery : CS.General_v3.Controls.WebControls.BaseWebControl
    {
        /* Properties to set
         * -----------------
         * REQUIRED
         * ========
         * 
         * AddItem(IGalleryItem)
         * StatusMessageUpdate(msg) [event]
         * UploadItems (event)
         * DeleteSelectedMediaItemsClick (event)
         * 
         * OPTIONAL
         * ========
         * AmtColumns (default: 4)
         * CellSpacing (default: 5)
         * DeleteIconImageUrl_Up (default: /_common/images/icons/v1/icon_delete_up.png)
         * DeleteIconImageUrl_Over (default: /_common/images/icons/v1/icon_delete_over.png)
         * 
         * DoNotShowCaption
         * DoNotShowCheckbox
         * MaxAmountUpload (default: 5)
         * MaxImageHeight (default: 120)
         * MaxImageWidth (default: 120)
         * MainImageCellPadding (default: 12)
         * NoCaptionText (default: 'No Caption Available')
         * SaveCaptions (event)
         * ShowDeleteButtonPerItem (default: false)
         * 
         */

        public class FUNCTIONALITY
        {
            private MediaGallery _gallery = null;

            public event EventHandler OnUploadItemsClicked;
            public event EventHandler OnDeleteSelectedItemsClicked;
            public event EventHandler OnSaveCaptionsClicked;

            /// <summary>
            /// Fired when a button of this media gallery has been clicked.  Any button ( upload.. save captions.. delete... )
            /// </summary>
            public event EventHandler OnButtonClicked;
            private string _ValidationGroup = null;

            public string ValidationGroup
            {
                get
                {
                    if (!string.IsNullOrEmpty(_ValidationGroup))
                        return _ValidationGroup;
                    else
                        return _gallery.ClientID;
                }
                set
                {
                    _ValidationGroup = value;
                }
            }


            public FUNCTIONALITY(MediaGallery gallery)
            {
                _gallery = gallery;
                this.ShowCaptions = true;
                this.ShowLargeImage = true;
                UploadTitle = "Upload";
                this.Editable = true;
                this.FileExtensionsAllowed = new List<string>();
                this.FileExtensionsAllowed.AddRange(CS.General_v3.Util.Image.GetImageFilesExtensions());
                this.FileExtensionsAllowed.AddRange(CS.General_v3.Util.VideoUtil.GetVideoFilesExtensions());
                this.MaxImageHeight = 120;
                this.MaxImageWidth = 120;
                this.MainImageCellPadding = 12;

                this.EnableDeleteSelectedButton = true;
                this.EnableCaptionsPerItem = true;
                this.EnableDeleteButtonPerItem = true;
                this.EnablePriorityPerItem = true;
                this.EnableSetMainImagePerItem = false;

                this.NoItemsInGalleryText = "No files found in gallery";
            }
            internal void onLoad()
            {
                initContainer();
                initMainImage();
                checkRequiredVars();
                initDefaultButtons();
                if (_items.Count > 0)
                {
                    initItems();
                    if (Editable)
                    {
                        addGeneralButtons();
                    }

                    initFLVPlayer();
                    initJS();
                    if (Editable)
                    {
                        initSaveButton();
                    }
                }
                else
                {
                    showNoFilesInGallery();
                    _tblGallery.Visible = false;
                }
                if (Editable)
                {
                    initUploadItems();
                }
            }
            #region Delegates
            public delegate void StatusMessageUpdateHandler(string msg);
            public delegate string MediaItemHrefRetriever(IMediaItem item);
            public delegate void MediaItemHandler(IMediaItem item);
            public delegate void MediaItemsHandler(List<IMediaItem> items);
            public delegate void UploadItemsHandler(List<MediaGalleryUploadItemFile> items);
            #endregion
            //private string _deleteIconImageURL_Up = "/_common/images/icons/v1/icon_delete_up.png";
            //private string _deleteIconImageURL_Over = "/_common/images/icons/v1/icon_delete_over.png";
            private string _deleteIconCssClass = "media-gallery-button-delete";
            private string _mainPictureCssClass = "media-gallery-button-main-picture";

            //private string _mainPictureIconImageURL_Up = "/_common/images/icons/v1/icon_picture_main_up.png";
            //private string _mainPictureIconImageURL_Over = "/_common/images/icons/v1/icon_picture_main_over.png";

            private string _noCaptionText = "No Caption Available";
            

            private string _flvPlayerSwfUrl = "/_common/flash/v1/flvplayer.swf";
            private string _flvPlayerExpressInstallSwfUrl = "/_common/flash/v1/swfobject/expressInstall.swf";
            private string _flvPlayerSkinSwfUrl = "/_common/flash/v1/SkinOverPlaySeekMute.swf";


            public string MaxUploadReachedText { get; set; }
            public string NoItemsInGalleryText { get; set; }

            private bool _editable;

            public bool Editable
            {
                get
                {
                    return _editable;
                }
                set
                {
                    _editable = value;
                }
            }
            public string UploadTitle { get; set; }
            public bool EnableDeleteSelectedButton { get; set; }
            public bool EnableDeleteButtonPerItem { get; set; }
            public bool EnablePriorityPerItem { get; set; }
            public bool EnableCaptionsPerItem { get; set; }
            public bool EnableSetMainImagePerItem { get; set; }

            public System.Web.UI.WebControls.ListItemCollection ExtraValueChoices { get; set; }
            public bool ShowExtraValues { get; set; }
            public string ExtraValue_Title { get; set; }
            public bool ShowLargeImage { get; set; }
            public bool ShowCaptions { get; set; }
            //public bool ShowDeleteButtonPerItem { get; set; }
            //public bool ShowSetMainImageButtonPerItem { get; set; }


            //public event MediaItemHandler DeleteMediaItemClick;
            //public event MediaItemsHandler DeleteSelectedMediaItemsClick;
            //public event MediaItemsHandler SaveCaptions;
            public event MediaGallery.FUNCTIONALITY.UploadItemsHandler UploadItems;
            public event MediaGallery.FUNCTIONALITY.StatusMessageUpdateHandler StatusMessageUpdate;

            private int _amtColumns = 4;
            private int _cellSpacing = 5;

            private int _maxAmtUpload = 5;




            private List<IMediaItem> _items = new List<IMediaItem>();
            private List<MyButton> _generalButtons = new List<MyButton>();
            private List<MediaItemButton> _itemButtons = new List<MediaItemButton>();

            private List<MediaGalleryItem> _galleryItems = new List<MediaGalleryItem>();
            private List<MediaGalleryUploadItem> _galleryUploadItems = new List<MediaGalleryUploadItem>();

            protected HtmlGenericControl _div;
            protected MyTable _tblGallery;
            private MyImage _imgMain;
            private MyTableCell _tdCaption;
            private MyTableCell _tdImage;
            private HtmlGenericControl _divFLV;




            private void initContainer()
            {

                _div = new HtmlGenericControl("div");
                _tblGallery = new MyTable();
                _tblGallery.CellPadding = 0;
                _tblGallery.CellSpacing = CellSpacing;
                _tblGallery.Attributes["class"] = _div.Attributes["class"] = "cs-image-gallery";
                _div.Controls.Add(_tblGallery);
                _gallery.Controls.Add(_div);


            }

            private void initFLVPlayer()
            {
                if (!DoNotInitializeFLVPlayer)
                {

                    string jsFLV = "";

                    CS.General_v3.JavaScript.Data.JSObject jsFlashVars = new CS.General_v3.JavaScript.Data.JSObject();
                    CS.General_v3.JavaScript.Data.JSObject jsParams = new CS.General_v3.JavaScript.Data.JSObject();
                    CS.General_v3.JavaScript.Data.JSObject jsAttributes = new CS.General_v3.JavaScript.Data.JSObject();

                    jsFlashVars.AddProperty("skin", _flvPlayerSkinSwfUrl, true);
                    if (_items.Count > 0)
                    {
                        IMediaItem firstItem = _items[0];
                        if (!String.IsNullOrEmpty(firstItem.GetLargeImageUrl()))
                        {
                            string extension = firstItem.GetLargeImageUrl().Substring(firstItem.GetLargeImageUrl().LastIndexOf("."));
                            if (extension.ToLower() == ".flv")
                            {
                                //Video
                                jsFlashVars.AddProperty("video", firstItem.GetLargeImageUrl(), true);
                            }
                        }
                    }
                    HtmlGenericControl divFLVContainer = new HtmlGenericControl("div");
                    divFLVContainer.Style.Add("display", "none"); // Hide it
                    HtmlGenericControl divFLV = new HtmlGenericControl("div");
                    if (MaxImageWidth > 0)
                    {
                        // divFLVContainer.Style.Add("width", MaxImageWidth + "px");
                        divFLV.Style.Add("width", MaxImageWidth + "px");
                    }
                    if (MaxImageHeight > 0)
                    {
                        //  divFLVContainer.Style.Add("height", MaxImageHeight + "px");
                        divFLV.Style.Add("height", MaxImageHeight + "px");
                    }
                    divFLVContainer.Controls.Add(divFLV);
                    _divFLV = divFLV;
                    if (ShowLargeImage)
                    {
                        //_tdImage.Controls.Add(divFLVContainer);
                    }

                    jsParams.AddProperty("bgcolor", "#000000", true);
                    jsAttributes.AddProperty("id", divFLV.ClientID, true);
                    jsAttributes.AddProperty("name", divFLV.ClientID, true);

                    jsFLV = "swfobject.embedSWF('" + _flvPlayerSwfUrl + "', '" + divFLV.ClientID + "', '" + MaxImageWidth + "', '" + MaxImageHeight + "', '9.0.0', '" + _flvPlayerExpressInstallSwfUrl + "', " + jsFlashVars.GetJS() + ", " + jsParams.GetJS() + ", " + jsAttributes.GetJS() + ");";

                    //jsFLV = CS.General_v3.Util.JSUtil.MakeJavaScript(jsFLV, true);
                    //this._gallery.Page.ClientScript.RegisterStartupScript(this.GetType(), this._gallery.ClientID + "_FLV", jsFLV);
                }
            }

            private void initMainImage()
            {
                if (ShowLargeImage)
                {
                    MyTableRow trImage = _tblGallery.AddRow();

                    
                    _tdImage = trImage.AddCell("main-image");
                    MyTableRow trCaption = _tblGallery.AddRow();

                    _tdCaption = trCaption.AddCell("main-image-caption");
                    
                    _imgMain = new MyImage();
                    _imgMain.ImageUrl = "";
                    //_tdImage.Controls.Add(_imgMain);

                    _tdImage.ColumnSpan = _tdCaption.ColumnSpan = AmtColumns;
                }
            }


            

            private void addItemButtons(int rowIndex, MyTableCell tdButtons, IMediaItem item)
            {

                for (int j = 0; j < this._itemButtons.Count; j++)
                {
                    
                    var currBtn = this._itemButtons[j];
                    MediaItemButton itemButton = (MediaItemButton) CS.General_v3.Util.Other.CloneShallow(currBtn,true);
                    /*
                    if (currBtn is MediaItemSubmitButton)
                    {
                        MediaItemSubmitButton tmpBtn = (MediaItemSubmitButton)currBtn;
                        itemButton = new MediaItemSubmitButton(tmpBtn.ItemHandler, "");
                    }
                    else if (currBtn is MediaItemLinkButton)
                    {
                        MediaItemLinkButton tmpBtn = (MediaItemLinkButton)currBtn;
                        itemButton = new MediaItemLinkButton(tmpBtn.HrefRetriever, "");
                    }

                    CS.General_v3.Util.Other.Clone(this._itemButtons[j], itemButton);*/
                    itemButton.ID = getID("itemButton_" + rowIndex  +"_"+ j + "_" + item.Identifier);
                    itemButton.Tag = item;
                    itemButton.ValidationGroup = tdButtons.ClientID;
                    if (itemButton is MediaItemSubmitButton)
                    {
                        //Submit

                        itemButton.Click += new EventHandler(itemButton_Click);


                    }
                    else if (itemButton is MediaItemLinkButton)
                    {
                        //Link button 
                        MediaItemLinkButton linkButton = (MediaItemLinkButton)itemButton;
                        string URL = linkButton.HrefRetriever(item);
                        //linkButton.FunctionAsImage = true;
                        linkButton.OnClickHref = URL;
                        //linkButton.HRefTarget = Enums.HREF_TARGET.Blank;
                    }

                    tdButtons.Controls.Add(itemButton);

                }
            }
            /// <summary>
            /// Triggered whenever a button of an item is clicked
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            void itemButton_Click(object sender, EventArgs e)
            {
                MediaItemSubmitButton btn = (MediaItemSubmitButton)sender;
                IMediaItem item = (IMediaItem)btn.Tag;

                if (OnButtonClicked != null)
                {
                    OnButtonClicked(this, null);
                }

                btn.ItemHandler(item);
            }


            private void initSettings()
            {
                if (ShowLargeImage)
                {
                    if (MaxImageHeight > 0)
                    {

                        _tdImage.Style.Add("height", (MaxImageHeight + MainImageCellPadding * 2) + "px"); //Padding just in case
                        _imgMain.Style.Add("max-height", MaxImageHeight + "px");
                    }
                    if (MaxImageWidth > 0)
                    {
                        _tdImage.Style.Add("max-width", MaxImageWidth + "px");
                        _imgMain.Style.Add("max-width", (MaxImageWidth + MainImageCellPadding * 2) + "px");
                    }
                }
            }

            private void initItemRows()
            {

                List<List<IMediaItem>> rows = CS.General_v3.Util.ListUtil.SplitListIntoMultipleLists(this._items, AmtColumns,  true, true);
               for (int i=0; i < rows.Count;i++)
               {
                   initItemRow(i, rows[i]);
               }

            }
            private MyTableCell initItemCell(int index, MyTableRow tr, IMediaItem item)
            {
                if (item != null)
                {
                    if (String.IsNullOrEmpty(item.Caption))
                    {
                        item.Caption = _noCaptionText;
                    }
                    if (index == 0 && ShowLargeImage)
                    {
                        //Init first main image
                        _imgMain.ImageUrl = item.GetLargeImageUrl();
                        _imgMain.AlternateText = item.Caption;

                        if (!String.IsNullOrEmpty(item.GetOriginalItemUrl()))
                        {
                            _imgMain.HRef = item.GetOriginalItemUrl();
                            _imgMain.HRefTarget = Enums.HREF_TARGET.Blank;
                        }
                        _tdCaption.InnerHtml = item.Caption;
                    }



                    MyTableCell tdImage = tr.AddCell("thumb");

                    MyTable tblImage = new MyTable();
                    tdImage.Controls.Add(tblImage);
                    tblImage.CellPadding = tblImage.CellSpacing = 0;

                    MyTableRow trImage = tblImage.AddRow();

                    trImage.AddCell("image");

                    MyImage imgThumb = new MyImage();
                    imgThumb.ImageUrl = item.GetThumbnailImageUrl();
                    imgThumb.AlternateText = item.Caption;
                    imgThumb.ID = getID("thumb" + index);
                    trImage.Cells[0].Controls.Add(imgThumb);

                    MediaGalleryItem galleryItem = new MediaGalleryItem(item, null, imgThumb, null);
                    //FIELDS
                    if (Editable && !DoNotShowCaption || !DoNotShowPriority)
                    {
                        MyTableRow trFields = tblImage.AddRow("caption");
                        MyTableCell tdFields = trFields.AddCell(null);


                        MyTable tblFields = new MyTable();
                        int totalCells = 0;
                        MyTableRow trPriorityCaption = tblFields.AddRow();
                        tdFields.Controls.Add(tblFields);
                        tblFields.CellPadding = tblFields.CellSpacing = 0;
                        if (!DoNotShowPriority)
                        {
                            totalCells++;
                            MyTxtBoxNumericInteger txtPriority = new MyTxtBoxNumericInteger();
                            txtPriority.ID = getID("txtPriority_" + index);
                            txtPriority.Text = item.Priority.ToString();
                            MyTableCell td = trPriorityCaption.AddCell("priority");
                            td.Controls.Add(txtPriority);
                            galleryItem.TxtPriority = txtPriority;
                            txtPriority.ValidationGroup = "captions";
                            txtPriority.Width = 20;
                        }
                        if (!DoNotShowCaption)
                        {
                            totalCells++;
                            MyTxtBoxTextSingleLine txtCaption = new MyTxtBoxTextSingleLine();
                            txtCaption.ID = getID("txtCaption_" + index);
                            txtCaption.Text = item.Caption;
                            txtCaption.Width = 85;
                            txtCaption.Attributes["helpMessage"] = "Caption";
                            MyTableCell td = trPriorityCaption.AddCell(null, txtCaption);
                            galleryItem.TxtCaption = txtCaption;
                            txtCaption.ValidationGroup = "captions";
                        }
                        if (ShowExtraValues)
                        {
                            MyTableRow trExtraValueChoice = tblFields.AddRow();
                            MyTableCell tdExtraValueChoice = trExtraValueChoice.AddCell("extra-value-choice");
                            tdExtraValueChoice.ColumnSpan = totalCells;
                            MyDropDownList cmb = new MyDropDownList(getID("cmbExtraValueChoice_" + index), this.ExtraValue_Title, false, 105, null);
                            cmb.AddListItems(this.ExtraValueChoices);
                            cmb.SetValue(item.ExtraValueChoice);
                            //cmb.SelectedIndex = 2;
                            cmb.ValidationGroup = "captions";
                            galleryItem.CmbExtraValueChoice = cmb;
                            tdExtraValueChoice.Controls.Add(cmb);

                        }


                    }
                    if (Editable && this._itemButtons.Count > 0)
                    {
                        MyTableRow trButtons = tblImage.AddRow();
                        MyTableCell tdButtons = trButtons.AddCell("buttons");

                        addItemButtons(index, tdButtons, item);


                    }
                    if (Editable && !DoNotShowCheckbox || this._generalButtons.Count == 0)
                    {
                        MyTableRow trCheckbox = tblImage.AddRow();

                        MyCheckBox chk = new MyCheckBox();
                        MyTableCell tdCheckbox = trCheckbox.AddCell("checkbox", chk);
                        galleryItem.Checkbox = chk;
                        chk.ID = getID("chk" + index);
                    }

                    _galleryItems.Add(galleryItem);
                    return tdImage;
                }
                else
                {
                    MyTableCell tdEmpty = tr.AddCell("empty", "&nbsp;");
                    return tdEmpty;

                }
                
            }
            private void initItemRow(int rowIndex, List<IMediaItem> rowItems)
            {
                MyTableRow tr = _tblGallery.AddRow();
                for (int i = 0; i < rowItems.Count; i++)
                {

                    IMediaItem item = rowItems[i];
                    int itemIndex =(rowIndex * AmtColumns)+i;
                    initItemCell(itemIndex, tr,item);
                }
            }

            private void initItems()
            {
                initSettings();



                //MyTableRow tr = null;
                if (this._items.Count > 0)
                {
                    initItemRows();
                }
            }

            private void initDefaultButtons()
            {

                if (EnableDeleteButtonPerItem)
                {
                    MyButton btnDelete = this.AddItemButton("Delete Item", _deleteIconCssClass, new MediaGallery.FUNCTIONALITY.MediaItemHandler(deleteMediaItemClick));
                    btnDelete.ConfirmMessage = "Are you sure you want to delete this item?";

                }
                if (EnableSetMainImagePerItem)
                {
                    MyButton btnSetMainImage = this.AddItemButton("Set Main Image", _mainPictureCssClass, new MediaGallery.FUNCTIONALITY.MediaItemHandler(setMainImageClick));
                }
                if (EnableDeleteSelectedButton)
                {
                    MyButton btnDeleteText = this.AddGeneralButton("Delete Selected Items", 
                        _deleteIconCssClass, DeleteSelectedMediaItemsClick);
                    btnDeleteText.ID = getID("btnDeleteSelected");
                    btnDeleteText.ConfirmMessage = "Are you sure you want to delete the selected items?";
                }

            }
            private string getID(string ID)
            {
                return _gallery.ID + "_" + ID;
            }
            private void setMainImageClick(IMediaItem item)
            {

                item.SetAsMainItem();
                submitStatusMessageEventAndTransfer("Main image updated successfully");
            }
            private void submitStatusMessageEventAndTransfer(string msg)
            {
                if (StatusMessageUpdate != null)
                {
                    StatusMessageUpdate(msg);
                    CS.General_v3.Classes.URL.URLClass.TransferToSamePage();
                }
            }
            private void deleteMediaItemClick(IMediaItem item)
            {

                List<IMediaItem> list = new List<IMediaItem>();
                list.Add(item);
                DeleteSelectedMediaItemsClick(list);

                submitStatusMessageEventAndTransfer("Item deleted successfully");
            }

            public void DeleteSelectedMediaItemsClick(List<IMediaItem> items)
            {
                if (items.Count > 0)
                {
                    for (int i = 0; i < items.Count; i++)
                    {
                        items[i].Delete();
                    }

                    submitStatusMessageEventAndTransfer(items.Count + " item" + (items.Count == 1 ? "" : "s") + " deleted successfully");
                }
            }

            private void addGeneralButtons()
            {
                if (Editable)
                {
                    MyTableRow trButtons = _tblGallery.AddRow();
                    MyTableCell tdButtons = trButtons.AddCell("general-buttons");
                    tdButtons.ColumnSpan = AmtColumns;


                    MyTable tblButtons = new MyTable();
                    tdButtons.Controls.Add(tblButtons);
                    tblButtons.AddRow();
                    for (int i = 0; i < this._generalButtons.Count; i++)
                    {
                        MyTableCell tdButton = new MyTableCell();
                        tblButtons.Rows[0].Cells.Add(tdButton);
                        
                        
                        MyButton btn = _generalButtons[i];
                        tdButton.Controls.Add(btn);

                        btn.Click += new EventHandler(generalButton_Click);
                        btn.ValidationGroup = btn.ClientID;
                    }

                }
            }

            /// <summary>
            /// Triggered when a general button is clicked
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            void generalButton_Click(object sender, EventArgs e)
            {
                MyButton btn = (MyButton)sender;
                MediaGallery.FUNCTIONALITY.MediaItemsHandler itemsHandler = (MediaGallery.FUNCTIONALITY.MediaItemsHandler)btn.Tag;

                List<IMediaItem> selectedItems = SelectedItems;

                if (OnButtonClicked != null)
                {
                    OnButtonClicked(this, null);
                }

                itemsHandler(selectedItems);

            }

            private void initJS2()
            {
                string jsVarName = this._gallery.ClientID + "_Gallery";
                string js = "";
                js += "var " + jsVarName + " = new js.com.cs.v3.UI.Images.MediaGallery();";

                CS.General_v3.JavaScript.Data.JSArray images = new CS.General_v3.JavaScript.Data.JSArray(null);
                for (int i = 0; i < _galleryItems.Count; i++)
                {
                    MediaGalleryItem item = _galleryItems[i];

                    images.AddItem("new js.com.cs.v3.UI.Images.MediaItem('" + item.Item.GetThumbnailImageUrl() + "','" + item.Item.GetLargeImageUrl() + "', '" + item.Item.GetOriginalItemUrl() + "', '" + item.Item.Caption + "')", false);
                }
                CS.General_v3.JavaScript.Data.JSArray thumbIDs = new CS.General_v3.JavaScript.Data.JSArray(null);
                for (int i = 0; i < _galleryItems.Count; i++)
                {
                    MediaGalleryItem item = _galleryItems[i];
                    //[document.getElementById('thumb1'),'thumb2','thumb3'];
                    thumbIDs.AddItem(item.ImgThumbnail.ClientID, true);
                }
                string jsDivFlvId = "null";
                if (_divFLV != null)
                {
                    jsDivFlvId = "'" + _divFLV.ClientID + "'";
                    //Contains FLV Player

                }
                js += "new js.com.cs.v3.UI.Images.MediaGallery(" + jsVarName + "," + jsDivFlvId + ");";
                js += jsVarName + ".init(" + images.GetJS(true) + ", '" + _imgMain.ClientID + "', " + thumbIDs.GetJS(true) + ", false, 0, '" + _tdCaption.ClientID + "');";




                js = CS.General_v3.Util.JSUtil.MakeJavaScript(js, true);
                this._gallery.Page.ClientScript.RegisterStartupScript(this.GetType(), this._gallery.ClientID, js);
                //this.init = function(images, elemImage, elemThumbs, addToDOM, startIndex, elemCaption) {
            }


            private void initJS()
            {
                if (ShowLargeImage)
                {
                    string jsVarName = this._gallery.ClientID + "_Gallery";
                    string js = "";
                    js += "var " + jsVarName + " = new js.com.cs.v3.UI.Images.MediaGallery();";

                    CS.General_v3.JavaScript.Data.JSArray images = new CS.General_v3.JavaScript.Data.JSArray(null);
                    for (int i = 0; i < _galleryItems.Count; i++)
                    {
                        MediaGalleryItem item = _galleryItems[i];

                        images.AddItem("new js.com.cs.v3.UI.Images.MediaItem('" + item.Item.GetThumbnailImageUrl() + "','" + item.Item.GetLargeImageUrl() + "', '" + item.Item.GetOriginalItemUrl() + "', '" + item.Item.Caption + "')", false);
                    }
                    CS.General_v3.JavaScript.Data.JSArray thumbIDs = new CS.General_v3.JavaScript.Data.JSArray(null);
                    for (int i = 0; i < _galleryItems.Count; i++)
                    {
                        MediaGalleryItem item = _galleryItems[i];
                        //[document.getElementById('thumb1'),'thumb2','thumb3'];
                        thumbIDs.AddItem(item.ImgThumbnail.ClientID, true);
                    }
                    string jsDivFlvId = "null";
                    if (_divFLV != null)
                    {
                        jsDivFlvId = "'" + _divFLV.ClientID + "'";
                        //Contains FLV Player

                    }
                    
                    js += jsVarName + ".init(" + images.GetJS(true) + ", '" + _tdImage.ClientID + "', " + thumbIDs.GetJS(true) + ", false, 0, '" + _tdCaption.ClientID + "');";




                    js = CS.General_v3.Util.JSUtil.MakeJavaScript(js, true);
                    this._gallery.Page.ClientScript.RegisterStartupScript(this.GetType(), this._gallery.ClientID, js);
                    //this.init = function(images, elemImage, elemThumbs, addToDOM, startIndex, elemCaption) {

                }
            }
            private void initSaveButton()
            {
                if (Editable && (EnablePriorityPerItem && !DoNotShowPriority) || (EnableCaptionsPerItem && !DoNotShowCaption))
                {
                    HtmlGenericControl p = new HtmlGenericControl("p");

                    p.Attributes["class"] = "buttons";

                    MyButton btnSave = new MyButton();
                    btnSave.Text = "Save ";
                    if (EnableCaptionsPerItem) btnSave.Text += "Captions";
                    if (EnablePriorityPerItem)
                    {
                        if (EnableCaptionsPerItem) btnSave.Text += " / ";
                        btnSave.Text += "Priorities";
                    }


                    btnSave.ID = getID("btnSaveCaptionsAndPriorities");
                    p.Controls.Add(btnSave);
                    _div.Controls.Add(p);
                    btnSave.ValidationGroup = "captions";
                    btnSave.Click += new EventHandler(btnSaveCaptionsAndPriorities_Click);
                }
            }

            void btnSaveCaptionsAndPriorities_Click(object sender, EventArgs e)
            {
                for (int i = 0; i < _galleryItems.Count; i++)
                {
                    MediaGalleryItem item = _galleryItems[i];
                    bool save = false;
                    if (string.Compare(item.Item.Caption, _noCaptionText, true) == 0)
                    {
                        item.Item.Caption = "";
                        save = true;
                    }
                    if (this.ShowExtraValues && (string.Compare(item.Item.ExtraValueChoice, item.CmbExtraValueChoice.FormValue,true) != 0))
                    {
                        item.Item.ExtraValueChoice = item.CmbExtraValueChoice.FormValue;
                        save = true;
                    }
                    if (EnableCaptionsPerItem && item.Item.Caption != item.TxtCaption.FormValue && string.Compare(item.TxtCaption.FormValue, _noCaptionText, true) != 0)
                    {
                        item.Item.Caption = item.TxtCaption.FormValue;
                        save = true;
                    }
                    if (EnablePriorityPerItem && item.TxtPriority != null && item.Item.Priority != item.TxtPriority.FormValue.GetValueOrDefault(0))
                    {
                        item.Item.Priority = item.TxtPriority.FormValue.GetValueOrDefault(0);
                        save = true;
                    }
                    if (save)
                    {
                        item.Item.Save();
                    }
                }
                string msg = "";
                if (EnableCaptionsPerItem && EnablePriorityPerItem)
                {
                    msg = "Captions and priorities updated successfully";
                }
                else if (EnableCaptionsPerItem)
                {
                    msg = "Captions updated successfully";
                }
                else if (EnablePriorityPerItem)
                {
                    msg = "Priorities updated successfully";
                }
                if (OnSaveCaptionsClicked != null)
                {
                    OnSaveCaptionsClicked(this, null);
                }
                if (OnButtonClicked != null)
                {
                    OnButtonClicked(this, null);
                }
                submitStatusMessageEventAndTransfer(msg);
                //SaveCaptions(_items);
            }

            private void initAmountUpload(HtmlTable tblUpload)
            {
                tblUpload.Rows.Add(new HtmlTableRow());

                HtmlTableCell tdUpload = new HtmlTableCell();
                tblUpload.Rows[0].Cells.Add(tdUpload);

                tdUpload.ColSpan = (DoNotShowCaption) ? 2 : 4;
                tdUpload.Attributes["class"] = "amount-upload";

                HtmlTable tblAmtUpload = new HtmlTable();
                tdUpload.Controls.Add(tblAmtUpload);
                tblAmtUpload.CellPadding = tblAmtUpload.CellSpacing = 0;
                tblAmtUpload.Rows.Add(new HtmlTableRow());
                HtmlTableCell tdLabel = new HtmlTableCell();
                HtmlTableCell tdDropdown = new HtmlTableCell();
                tblAmtUpload.Rows[0].Cells.Add(tdLabel);
                tblAmtUpload.Rows[0].Cells.Add(tdDropdown);




                MyDropDownList cmb = new MyDropDownList();
                tdDropdown.Controls.Add(cmb);
                cmb.Title = "Amount to Upload";
                MyLabel label = new MyLabel(cmb);

                tdLabel.Controls.Add(label);

                for (int i = 1; i <= MaxAmountUpload; i++)
                {
                    cmb.Items.Add(new System.Web.UI.WebControls.ListItem(i.ToString(), i.ToString()));
                }

                label.Attributes["for"] = cmb.ClientID;


                string jsFunction = "";
                jsFunction += "function showCmbs" + this._gallery.ClientID + "(value) {\r\n";
                //jsFunction += " debugger;\r\n";
                jsFunction += "  var maxAmt = " + this.MaxAmountUpload + ";\r\n";
                jsFunction += "  var prefix = '" + this._gallery.ClientID + "_row_';\r\n";
                jsFunction += "  for (var i = 0;i<maxAmt;i++) {\r\n";
                jsFunction += "    var row = document.getElementById(prefix+i);\r\n";
                jsFunction += "    row.style.display = (i < value) ? '' : 'none';\r\n";
                jsFunction += "  }";

                jsFunction += "}\r\n";
                jsFunction += "showCmbs" + this._gallery.ClientID + "(parseInt(document.getElementById('" + cmb.ClientID + "').value));";

                jsFunction = CS.General_v3.Util.JSUtil.MakeJavaScript(jsFunction, false);
                this._gallery.Page.ClientScript.RegisterStartupScript(this.GetType(), this._gallery.ClientID + "_upload", jsFunction);

                cmb.Attributes["onchange"] = "showCmbs" + this._gallery.ClientID + "(parseInt(this.value));";
            }

            private void initUploadRows(HtmlTable tblUpload, string validationGroup)
            {
                for (int i = 0; i < MaxAmountUpload; i++)
                {
                    HtmlTableRow tr = new HtmlTableRow();
                    tblUpload.Rows.Add(tr);
                    tr.Attributes["class"] = "row_" + (i % 2);
                    HtmlTableCell tdLabelUpload = new HtmlTableCell();

                    HtmlTableCell tdUpload = new HtmlTableCell();
                    tdUpload.Attributes["class"] = "fileupload";
                    tr.Cells.Add(tdLabelUpload);
                    tr.Cells.Add(tdUpload);

                    tdLabelUpload.Attributes["class"] = "label";
                    MyFileUpload txtUpload = new MyFileUpload();
                    txtUpload.Title = "Item";
                    MyLabel lblUpload = new MyLabel(txtUpload);
                    if (this.FileExtensionsAllowed != null && this.FileExtensionsAllowed.Count > 0)
                    {
                        txtUpload.FileExtensionsAllowed = this.FileExtensionsAllowed;
                    }
                    txtUpload.ID = getID("_txtUpload_" + i);
                    txtUpload.Required = i == 0;
                    txtUpload.ValidationGroup = validationGroup;

                    tdLabelUpload.Controls.Add(lblUpload);
                    tdUpload.Controls.Add(txtUpload);
                    MediaGalleryUploadItem uploadItem = new MediaGalleryUploadItem(txtUpload, null);
                    if (!DoNotShowCaption)
                    {
                        HtmlTableCell tdLabelCaption = new HtmlTableCell();
                        tdLabelCaption.Attributes["class"] = "label";
                        HtmlTableCell tdCaption = new HtmlTableCell();
                        tdCaption.Attributes["class"] = "caption";
                        tr.Cells.Add(tdLabelCaption);
                        tr.Cells.Add(tdCaption);



                        MyTxtBoxTextSingleLine txtCaption = new MyTxtBoxTextSingleLine();
                        txtCaption.ID =getID("_txtUploadCaption_" + i);
                        txtCaption.Title = "Caption";
                        txtCaption.ValidationGroup = validationGroup;
                        MyLabel lblCaption = new MyLabel(txtCaption);
                        tdLabelCaption.Controls.Add(lblCaption);
                        tdCaption.Controls.Add(txtCaption);
                        lblCaption.Attributes["for"] = txtCaption.ClientID;
                        uploadItem.TxtCaption = txtCaption;
                    }
                    tr.ID = getID("row_" + i.ToString());
                    _galleryUploadItems.Add(uploadItem);

                }

                /*
                 *  <tr class="row_0">
                    <td>
                        <label>
                            Item:</label>
                    </td>
                    <td class="fileupload">
                        <CSControls:MyFileUpload runat="server" />
                    </td>
                    <td>
                        <label>
                            Caption:</label>
                    </td>
                    <td class="caption">
                        <CSControls:MyTxtBoxTextSingleLine runat="server"></CSControls:MyTxtBoxTextSingleLine>
                    </td>
                </tr>*/
            }
            private void initUploadButton(HtmlTable tblUpload, string validationGroup)
            {
                MyButton btnUploadItems = new MyButton();
                btnUploadItems.Text = "Upload Items";
                btnUploadItems.ID = getID("_btnUploadItems");
                HtmlTableRow tr = new HtmlTableRow();
                tblUpload.Rows.Add(tr);
                tr.Cells.Add(new HtmlTableCell());
                tr.Cells[0].ColSpan = (DoNotShowCaption) ? 2 : 4;
                tr.Cells[0].Controls.Add(btnUploadItems);
                tr.Cells[0].Attributes["class"] = "buttons";

                btnUploadItems.ValidationGroup = validationGroup;
                //tblUpload.Controls.Add(btnUploadItems);
                btnUploadItems.Click += new EventHandler(btnUploadItems_Click);

            }

            private List<MediaGalleryUploadItemFile> checkExtractedDirectory(System.IO.DirectoryInfo dirInfo)
            {
                List<MediaGalleryUploadItemFile> list = new List<MediaGalleryUploadItemFile>();
                System.IO.FileInfo[] files = dirInfo.GetFiles();
                foreach (var file in files)
                {
                    string ext = file.Extension;
                    if (ext != null)
                    {
                        ext = ext.ToLower();
                        if (ext.StartsWith("."))
                            ext = ext.Remove(0, 1);
                    }
                    if (CS.General_v3.Util.Image.CheckImageFileExtension(ext) || CS.General_v3.Util.VideoUtil.CheckVideoExtension(ext))
                    {
                        MediaGalleryUploadItemFile mediaUploadFile = new MediaGalleryUploadItemFile();

                        System.IO.FileStream fs = file.OpenRead();
                        System.IO.MemoryStream ms = new System.IO.MemoryStream();
                        byte[] by = new byte[fs.Length];
                        fs.Read(by, 0, by.Length);
                        ms.Write(by, 0, by.Length);
                        fs.Close();

                        mediaUploadFile.Stream = ms;
                        mediaUploadFile.Caption = "";
                        //mediaUploadFile.Caption = CS.General_v3.Util.IO.GetFilename(file.Name);
                        mediaUploadFile.Filename = file.Name;
                        list.Add(mediaUploadFile);

                    }
                }
                System.IO.DirectoryInfo[] dirs = dirInfo.GetDirectories();
                foreach (var dir in dirs)
                {
                    list.AddRange(checkExtractedDirectory(dir));
                }
                return list;
            }

            private List<MediaGalleryUploadItemFile> extractZIPFile(Stream stream)
            {
                bool isTempFolder = false;
                string tmpExtractFolder = this.TemporaryZIPExtractFolder;
                if (string.IsNullOrEmpty(tmpExtractFolder))
                {
                    isTempFolder = true;
                    tmpExtractFolder = CS.General_v3.Util.IO.CreateTempDir();
                }
                string tmpDir = tmpExtractFolder + CS.General_v3.Util.Random.GetString(50, 50) + "\\";
                string tmpFilename = CS.General_v3.Util.Random.GetString(50, 50) + ".zip";
                string zipPath = tmpDir + tmpFilename;
                CS.General_v3.Util.IO.SaveStreamToFile(stream, zipPath);
                CS.General_v3.Util.ZIPUtil.ExtractZIP(zipPath, tmpDir);
                System.IO.DirectoryInfo dirInfo = new System.IO.DirectoryInfo(tmpDir);
                List<MediaGalleryUploadItemFile> list = checkExtractedDirectory(dirInfo);
                CS.General_v3.Util.IO.EmptyDirectory(tmpDir);
                //System.IO.Directory.Delete(tmpDir, true);
                return list;
            }
            void btnUploadItems_Click(object sender, EventArgs e)
            {
                List<MediaGalleryUploadItemFile> list = new List<MediaGalleryUploadItemFile>();
                for (int i = 0; i < _galleryUploadItems.Count; i++)
                {

                    MediaGalleryUploadItem item = _galleryUploadItems[i];
                    string ext = CS.General_v3.Util.IO.GetExtension(item.TxtUpload.FileName).ToLower();
                    if (item.TxtUpload.FileBytes.Length > 0)
                    {
                        if (this.AllowZIPUpload && ext == "zip")
                        {
                            list.AddRange(extractZIPFile(item.TxtUpload.FileContent));
                        }
                        else
                        {
                            list.Add(new MediaGalleryUploadItemFile(item));
                            
                        }
                    }
                }
                if (OnUploadItemsClicked != null)
                {
                    OnUploadItemsClicked(this, null);
                }
                if (OnButtonClicked != null)
                {
                    OnButtonClicked(this, null);
                }
                if (list.Count > 0)
                {
                    UploadItems(list);
                }
            }
            private void initUploadItems()
            {
                if (Editable && UploadItems != null && (MaxAmountUpload > 0 || !String.IsNullOrEmpty(MaxUploadReachedText)))
                {
                    string validationGroup = "";
                    if (!string.IsNullOrEmpty(this.ValidationGroup))
                        validationGroup = this.ValidationGroup;
                    else
                        validationGroup =this._gallery.ClientID + "_upload";
                    

                    HtmlGenericControl h2 = new HtmlGenericControl("h2");
                    h2.Attributes["class"] = "upload-title";
                    h2.InnerHtml = UploadTitle;
                    _div.Controls.Add(h2);

                    if (MaxAmountUpload > 0)
                    {
                        HtmlGenericControl divPanel = new HtmlGenericControl("div");
                        divPanel.Attributes["class"] = "panel";
                        _div.Controls.Add(divPanel);
                        HtmlTable tblUpload = new HtmlTable();
                        tblUpload.CellSpacing = 1;
                        tblUpload.CellPadding = 0;
                        tblUpload.Attributes["class"] = "media-upload";
                        divPanel.Controls.Add(tblUpload);
                        initAmountUpload(tblUpload);

                        initUploadRows(tblUpload, validationGroup);

                        initUploadButton(tblUpload, validationGroup);
                    }
                    else
                    {
                        _div.Controls.Add(new MyParagraph(MaxUploadReachedText));
                    }
                }
            }
            private void checkRequiredVars()
            {
                StringBuilder sb = new StringBuilder();

                if (!string.IsNullOrEmpty(TemporaryZIPExtractFolder) && TemporaryZIPExtractFolder.Contains("/"))
                {
                    TemporaryZIPExtractFolder = CS.General_v3.Util.PageUtil.MapPath(TemporaryZIPExtractFolder);
                    //sb.AppendLine("'TemporaryZipExtractFolder' must contain a LOCAL path");
                }
                if (AllowZIPUpload && this.FileExtensionsAllowed != null &&
                    (this.FileExtensionsAllowed.FindElem((item => (item.ToLower() == "zip"))) == null))
                {
                    this.FileExtensionsAllowed.Add("zip");
                }
                if (sb.Length > 0)
                {
                    throw new InvalidOperationException("CS.General_v3.Controls.WebControls.Specialized.MediaGallery.MediaGallery:: \r\n\r\n" + sb.ToString());
                }
            }
            private void showNoFilesInGallery()
            {
                HtmlGenericControl p = new HtmlGenericControl("p");
                p.InnerHtml = NoItemsInGalleryText;
                _div.Controls.Add(p);
            }
            

            /// <summary>
            /// Adds a media item to the gallery.  Gallery can contain images, pdfs, documents, videos etc.
            /// 
            /// Thumbanil URL is the url for the thumbnail.
            /// GetLargeImageUrl() is the URL for the large image (It must be an image ALSO for documents, pdfs etc... ONLY in FLV files, it must be the actual FLV video file
            /// ItemURL, in case of images it must be the large image URL, in case of documents, the .doc, .pdf etc...
            /// </summary>
            /// <param name="item"></param>
            public void AddItem(IMediaItem item)
            {
                //get the largest width/height;

                string localPath = CS.General_v3.Util.PageUtil.MapPath(item.GetLargeImageUrl());
                int width, height;
                CS.General_v3.Util.Image.getImageSize(localPath, out width, out height);
                if (width > this.MaxImageWidth)
                    this.MaxImageWidth = width;
                if (height > this.MaxImageHeight)
                    this.MaxImageHeight = height;

                _items.Add(item);
            }
            /// <summary>
            /// Adds a general button which is triggered for multiple items.  IT will keep track of checkboxes selected when this is clicked.
            /// </summary>
            /// <param name="title"></param>
            /// <param name="imageIcon"></param>
            /// <param name="imageIconRollover"></param>
            /// <param name="clickHandler">Handler to be triggered with list of items selected</param>
            /// <returns></returns>
            public MyButton AddGeneralButton(string title, string cssClass, MediaGallery.FUNCTIONALITY.MediaItemsHandler clickHandler, int? index = null)
            {
                if (index == null) index = 0;
                MyButton btn = new MyButton();
                btn.Text = title;

                //btn.ImageUrl = imageIcon;
                //btn.RolloverImageUrl = imageIconRollover;

                this._generalButtons.Insert(index.Value, btn);
                btn.Tag = clickHandler;
                return btn;
            }
            /// <summary>
            /// Adds a button which is shown for EVERY thumbnail.  The button is a link button and does not submit form.
            /// </summary>
            /// <param name="title"></param>
            /// <param name="imageIcon"></param>
            /// <param name="imageIconRollover"></param>
            /// <param name="hrefRetriever">The delegate reference which will return the URL to be redirected to on click of button</param>
            /// <returns></returns>
            public MediaItemButton AddItemLinkButton(string title, string imageIcon, string imageIconRollover, MediaGallery.FUNCTIONALITY.MediaItemHrefRetriever hrefRetriever, int? index = null)
            {
                if (index == null) index = 0;
                MediaItemLinkButton btn = new MediaItemLinkButton(hrefRetriever,getID("btnItemLinkButton_" + index));

                btn.Text = title;

                //btn.ImageUrl = imageIcon;

                //btn.RollOverImage = imageIconRollover;


                this._itemButtons.Insert(index.Value, btn);
                return btn;
            }
            
            /// <summary>
            /// Adds a button which is shown for EVERY thumbnail.  The button submits the form.
            /// </summary>
            /// <param name="title"></param>
            /// <param name="imageIcon"></param>
            /// <param name="imageIconRollover"></param>
            /// <param name="clickHandler">The handler which is called whenever the button is clicked</param>
            /// <returns></returns>
            public MediaItemButton AddItemButton(string title, string cssClass, MediaGallery.FUNCTIONALITY.MediaItemHandler clickHandler, int? index = null)
            {
                if (index == null) index = _itemButtons.Count;

                MediaItemSubmitButton btn = new MediaItemSubmitButton(clickHandler,
                    getID("btnItemButton_" + index));

                btn.Text = title;
                //btn.ImageUrl = imageIcon;
               // btn.RollOverImage = imageIconRollover;
                this._itemButtons.Insert(index.Value, btn);
                return btn;
            }

            public int MaxImageWidth { get; set; }
            public int MaxImageHeight { get; set; }
            public int MainImageCellPadding { get; set; }
            public bool AllowZIPUpload { get; set; }

            public int AmtColumns { get { return _amtColumns; } set { _amtColumns = value; } }
            public int CellSpacing { get { return _cellSpacing; } set { _cellSpacing = value; } }

            public bool DoNotShowCaption { get { return !EnableCaptionsPerItem; } set { EnableCaptionsPerItem = !value; } }
            public bool DoNotShowPriority { get { return !EnablePriorityPerItem; } set { EnablePriorityPerItem = !value; } }

            public bool DoNotShowCheckbox { get; set; }

            private string _TemporaryZIPExtractFolder = null;
            /// <summary>
            /// The temporary zip folder to upload to.   A LOCAL path must be specified. If not specified, a temporary directory will be created.
            /// </summary>
            public string TemporaryZIPExtractFolder
            {
                get
                {



                    return _TemporaryZIPExtractFolder;
                }
                set
                {
                    _TemporaryZIPExtractFolder = value;
                    if (_TemporaryZIPExtractFolder != null && !_TemporaryZIPExtractFolder.EndsWith("\\"))
                        _TemporaryZIPExtractFolder += "\\";
                }


            }
            public string DeleteIconImageCssClass { get { return _deleteIconCssClass; } set { _deleteIconCssClass = value; } }

            public List<IMediaItem> SelectedItems
            {
                get
                {
                    List<IMediaItem> items = new List<IMediaItem>();
                    for (int i = 0; i < this._galleryItems.Count; i++)
                    {
                        MediaGalleryItem item = _galleryItems[i];
                        if (item.Checkbox.Checked)
                        {
                            items.Add(item.Item);
                        }
                    }
                    return items;

                }
            }

            public string NoCaptionText { get { return _noCaptionText; } set { _noCaptionText = value; } }
            public bool DoNotInitializeFLVPlayer { get; set; }
            public List<string> FileExtensionsAllowed { get; set; }
            public string FlvPlayerSwfUrl { get { return _flvPlayerSwfUrl; } set { _flvPlayerSwfUrl = value; } }
            public string FlvPlayerSkinSwfUrl { get { return _flvPlayerSkinSwfUrl; } set { _flvPlayerSkinSwfUrl = value; } }
            public int MaxAmountUpload { get { return _maxAmtUpload; } set { _maxAmtUpload = value; } }

            public int AmtItems { get { return _items.Count; } }
        }
        public FUNCTIONALITY Functionality { get; set; }
        

        public MediaGallery(): this("")
        {

        }
        public MediaGallery(string ID)
        {
            this.Functionality = new FUNCTIONALITY(this);
            this.ID = ID;
            


        }
        protected override void OnInit(EventArgs e)
        {

            base.OnInit(e);
        }
        protected override void OnLoad(EventArgs e)
        {
            Functionality.onLoad();
            base.OnLoad(e);
        }

    }
}
