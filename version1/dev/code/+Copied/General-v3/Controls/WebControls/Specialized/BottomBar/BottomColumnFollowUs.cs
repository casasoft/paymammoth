﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;
using CS.General_v3.Controls.WebControls.Common;

namespace CS.General_v3.Controls.WebControls.Specialized.BottomBar
{
    public class BottomColumnFollowUs : BottomColumn
    {

        public new class FUNCTIONALITY : BottomColumn.FUNCTIONALITY
        {
            private MyDiv _wrapperDIV;

            public string BecomeFanHREF { get; set; }
            public string JoinGroupHREF { get; set; }
            public string TwitterFollowUsHref { get; set; }
            public string BecomeFanText { get; set; }
            public string JoinGroupText { get; set; }
            public string TwitterFollowUsText { get; set; }
            
            public FUNCTIONALITY(BottomColumnFollowUs col)
                : base(col)
            {
                BecomeFanText = "Become a fan!";
                JoinGroupText = "Join our group!";
                TwitterFollowUsText = "Follow us!";
            }

            private void renderFollowUsColumn()
            {
                generateWrapper();
                initHeading();
                renderBecomeAFan();
                renderJoinGroup();
                renderTwitterFollowUs();
            }

            private void initHeading()
            {
                HtmlGenericControl titleSpan = new HtmlGenericControl("span");
                titleSpan.Attributes["class"] = "title";
                titleSpan.InnerText = Title;
                _wrapperDIV.Controls.Add(titleSpan);
            }


            private void generateWrapper()
            {
                _wrapperDIV = new MyDiv();
                _wrapperDIV.Attributes["class"] = "follow-us-wrapper";
                PlaceHolder.Controls.Add(_wrapperDIV);
            }

            private void renderBecomeAFan()
            {
                if (!string.IsNullOrEmpty(BecomeFanHREF))
                {
                    MyDiv div = new MyDiv();
                    div.Attributes["class"] = "become-a-fan-wrapper";
                    div.Controls.Add(new MyDiv() { CssClass = "become-a-fan-back-wrapper" });
                    div.Controls.Add(new HtmlAnchor() { InnerText = BecomeFanText, HRef = BecomeFanHREF, Target="_blank" });
                    _wrapperDIV.Controls.Add(div);
                }
            }

            private void renderJoinGroup()
            {
                if (!string.IsNullOrEmpty(JoinGroupHREF))
                {
                    MyDiv div = new MyDiv();
                    div.Attributes["class"] = "join-group-wrapper";
                    div.Controls.Add(new MyDiv() { CssClass = "join-group-back-wrapper" });
                    div.Controls.Add(new HtmlAnchor() { InnerText = JoinGroupText, HRef = JoinGroupHREF, Target = "_blank" });
                    _wrapperDIV.Controls.Add(div);
                }
            }

            private void renderTwitterFollowUs()
            {
                if (!string.IsNullOrEmpty(TwitterFollowUsHref))
                {
                    MyDiv div = new MyDiv();
                    div.Attributes["class"] = "twitter-wrapper";
                    div.Controls.Add(new MyDiv() { CssClass = "twitter-back-wrapper" });
                    div.Controls.Add(new HtmlAnchor() { InnerText = TwitterFollowUsText, HRef = TwitterFollowUsHref, Target = "_blank" });
                    _wrapperDIV.Controls.Add(div);
                }
            }

            internal override void Init()
            {
                renderFollowUsColumn();
                base.Init();
            }

        }
        public new FUNCTIONALITY Functionality
        {
            get { return (FUNCTIONALITY)base.Functionality; }
            set
            {
                base.Functionality = value;
            }
        }

        public BottomColumnFollowUs()
        {
            Functionality = new FUNCTIONALITY(this);
        }

    }
}
