﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;

namespace CS.General_v3.Controls.WebControls.Specialized
{
    using JavaScript.Data;

    public enum INFINITE_SCROLLER_DIRECTION
    {
        Up = 0,
        Down = 1,
        Left = 2,
        Right = 3
    }

    public class InfiniteScrollerParameters : JavaScriptObject
    {
        public string divContainerID;
        public int delayBetweenMovementInMS = 30;
        public int pixelsToMove = 3;
        public bool centerImagesIfSmallerThanContainer = true;
        public INFINITE_SCROLLER_DIRECTION direction = INFINITE_SCROLLER_DIRECTION.Left;

        public static void InitInfiniteScroller(InfiniteScrollerParameters p)
        {
            string js = "new js.com.cs."+CS.General_v3.Settings.Others.JAVASCRIPT_VERSION+".UI.InfiniteScroller.InfiniteScroller(" + p.GetJsObject().GetJS() + ");";
            CS.General_v3.Util.JSUtilOld.AddJSScriptToPage(js);
        }

    }
}
