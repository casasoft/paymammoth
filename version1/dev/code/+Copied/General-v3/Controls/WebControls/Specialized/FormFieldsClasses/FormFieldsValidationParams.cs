﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;
using System.Text.RegularExpressions;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses
{
    using JavaScript.Data;

    public class FormFieldsValidationParams : FormFieldsValidationParamsBase
    {
        public enum SHOW_CALENDAR_TYPE
        {
            None = 0,
            Focus = 100,
            Button = 200,
            Both = 300
        }
        public enum FIELD_SUBGROUP_TYPE
        {
            None = 0,
            /// <summary>
            /// All the fields with the same field group ID must have same values
            /// </summary>
            SameValues = 100,
            /// <summary>
            /// All the fields within the same field group must have at least one required
            /// </summary>
            AtLeastOneRequired = 200
        }
        public class jQueryMultiSelectParamters : JavaScriptObject
        {
            

            public bool sortable = true;
            public bool searchable = true;

            public bool doubleClickable = true;
            public string animated = "fast";
            public string show = "slideDown";
            public string hide = "slideUp";
            public double dividerLocation = 0.5;
        }

        public class FormFieldValidationSubGroupParams : JavaScriptObject
        {

            public FormFieldValidationSubGroupParams(string groupID, FIELD_SUBGROUP_TYPE groupType )
            {

                System.Diagnostics.Contracts.Contract.Requires(!string.IsNullOrEmpty(groupID));
                this.groupID = groupID;
                this.groupType = groupType;
            }
           
            /// <summary>
            /// The group of fields IDs
            /// </summary>
            public string groupID { get; set; }
            /// <summary>
            /// The group type
            /// </summary>
            public FIELD_SUBGROUP_TYPE groupType { get; set; }

            
        }
        public bool isRequired = false;
        public bool isEmail = false;
        public bool isNumber = false;
        public bool positiveOnly = false;
        public bool isWebsite = false;
        public bool negativeOnly = false;
        public bool integersOnly = false;
        public double? numFrom;
        public double? numTo;
        public DateTime? dateFrom;
        public DateTime? dateTo;
        public bool isCurrency = false;
        public string currencySymbol = "€";
        public bool isCreditCardNumber = false;
        public bool isIPAddress = false;
        public bool isDate = false;



        

        /// <summary>
        /// Whether to show the calendar icon on the right.  Must have jQuery UI embedded
        /// </summary>
        public SHOW_CALENDAR_TYPE jQueryUICalendarShowType = SHOW_CALENDAR_TYPE.Both;
        /// <summary>
        /// Whether you would like to add the button panel where there is TODAY & DONE
        /// </summary>
        public bool jQueryUICalendarShowButtonPanel = true;
        public string jQueryUICalendarCustomContainerCSSClass = "cs-custom-theme";


        public bool valueCaseSensitive = false;
        public string dateFormat = "dd/mm/yyyy";
        public int minLength = 0;
        public int maxLength = 0;
        public string initialEmptyText;


        private bool? _showJQueryMultiSelect;
        public bool? showJQueryMultiSelect
        {
            get { return _showJQueryMultiSelect; }
            set { _showJQueryMultiSelect = value; }
        }

        public jQueryMultiSelectParamters jQueryMultiSelectParams = new jQueryMultiSelectParamters();
        public FormFieldValidationSubGroupParams subGroupParams;
        public bool showValidationIcon = true;

        /// <summary>
        /// A comma-seperated string of file extensions allowed, e.g gif, bmp, tif...
        /// </summary>
        public List<string> fileExtensionsAllowed;
        public int maxWords = 0;
        public bool isAlphaNumeric = false;
        public List<object> valueIn;
        public List<object> valueNotIn;

        public Regex regExpPattern;

        public override bool HasSomeFormOfValidation()
        {
            


            bool hasSomeFormOfValidation = base.HasSomeFormOfValidation();
            if (!hasSomeFormOfValidation)
            {
                if ((this.fileExtensionsAllowed != null && this.fileExtensionsAllowed.Count > 0) ||
                    this.integersOnly || 
                    this.isAlphaNumeric ||
                    this.isCreditCardNumber || 
                    this.isCurrency || 
                    this.isDate ||
                    this.isEmail || 
                    this.isIPAddress || 
                    this.isNumber || 
                    this.isRequired ||
                    this.isWebsite || 
                    this.maxLength != 0 || 
                    this.maxWords != 0 ||
                    this.minLength != 0 || 
                    this.regExpPattern != null || 
                    this.subGroupParams != null ||
                    (this.valueIn != null && this.valueIn.Count > 0) ||
                    (this.valueNotIn != null && this.valueNotIn.Count > 0) ||
                    !string.IsNullOrWhiteSpace(this.initialEmptyText))
                {
                    hasSomeFormOfValidation = true;
                }
            }
            return true;
            return hasSomeFormOfValidation;
        }





    }
}
