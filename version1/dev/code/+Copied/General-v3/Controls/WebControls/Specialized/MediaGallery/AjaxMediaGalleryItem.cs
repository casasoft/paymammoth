﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;

namespace CS.General_v3.Controls.WebControls.Specialized.MediaGallery
{
    public class AjaxMediaGalleryItem : MyDiv
    {
        public class FUNCTIONALITY
        {
            private AjaxMediaGalleryItem _ui;

            public AjaxMediaGalleryItemData Data { get; set; }

            public FUNCTIONALITY(AjaxMediaGalleryItem ui)
            {
                _ui = ui;
            }
            private void initItem()
            {
                MyImage img = new MyImage();
                img.ImageUrl = Data.ThumbURL;
                img.AlternateText = Data.Caption;
                img.HRef = Data.ItemURL;
                img.CssManager.AddClass("cs-media-gallery-item-image");
                img.HRefTarget = Enums.HREF_TARGET.Blank;
                _ui.Controls.Add(img);

                MyDiv divCaption = new MyDiv();
                divCaption.InnerHtml = Data.Caption;
                divCaption.CssManager.AddClass("cs-media-gallery-item-caption");
                _ui.Controls.Add(divCaption);

            }
            public void Init()
            {
                initItem();   
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public AjaxMediaGalleryItem()
        {
            Functionality = new FUNCTIONALITY(this);
            this.CssManager.AddClass("cs-media-gallery-item");
        }
        protected override void OnLoad(EventArgs e)
        {
            Functionality.Init();
            base.OnLoad(e);
        }
    }
}
