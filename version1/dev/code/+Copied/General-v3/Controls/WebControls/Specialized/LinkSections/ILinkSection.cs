﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_20101215.Controls.WebControls.Specialized.LinkSections
{
    public interface ILinkSection : MenuTreeStructure.IMenuFolder
    {
        

        
        string Description { get; set; }
    }
        
}
