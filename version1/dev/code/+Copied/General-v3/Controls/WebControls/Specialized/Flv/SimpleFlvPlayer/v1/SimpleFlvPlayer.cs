﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing.Design;
using CS.General_v3.JavaScript.Data;
using CS.General_v3.Controls.WebControls.Common.SWFObject.v2;
namespace CS.General_v3.Controls.WebControls.Specialized.Flv.SimpleFlvPlayer.v1
{
    public class SimpleFlvPlayer :BaseWebControl
    {
        public string File { get; set; }
        public bool LoopPlayback { get; set; }
        public string Href { get; set; }
        public bool MaintainAspectRatio { get; set; }

        public string SimpleFlvPlayerUrl { get; set; }
        public string ExpressInstallUrl { get; set; }
        public SWFObject.WMODE SwfObjectWindowMode { get; set; }

        public SimpleFlvPlayer(string flvUrl, Unit width, Unit height)
            : base("div")
        {
            SwfObjectWindowMode = SWFObject.WMODE.None;
            File = flvUrl;
            this.MaintainAspectRatio = true;
            SimpleFlvPlayerUrl = "/_common/static/flash/v1/flv/SimpleFlvPlayer.swf";
            ExpressInstallUrl = "/_common/static/flash/v1/swfobject/expressInstall.swf";
            this.Width = width;
            this.Height = height;
            this.CssManager.AddClass("simple-flv-player");
        }


        private void initSwfObject()
        {
            SWFObject swfObject = new SWFObject(SimpleFlvPlayerUrl, "8.0.0", ExpressInstallUrl);
            swfObject.Functionality.FlashWidth = Width;
            swfObject.Functionality.FlashHeight = Height;
            swfObject.Functionality.WindowMode = SwfObjectWindowMode;
            swfObject.Functionality.FlashVars.AddProperty("file", File);
            swfObject.Functionality.FlashVars.AddProperty("loop", LoopPlayback);
            swfObject.Functionality.FlashVars.AddProperty("maintainaspectratio", MaintainAspectRatio);
            if (!String.IsNullOrEmpty(Href))
            {
                swfObject.Functionality.FlashVars.AddProperty("href", Href);
            }

            Controls.Add(swfObject);
        }
        protected override void OnLoad(EventArgs e)
        {
            initSwfObject();
            base.OnLoad(e);
        }
    }
}
