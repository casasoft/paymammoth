﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common.General;
using CS.General_v3.Controls.WebControls.Common;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data
{
    
    public class FormFieldNumericIntegerData : FormFieldNumericBaseData<long?>
    {

        public FormFieldNumericIntegerData()
        {
            _validationParams.integersOnly = true;
            this.RowCssClassBase = "form-row-integer";
        }
        public new MyTxtBoxNumericInteger GetField()
        {
            return (MyTxtBoxNumericInteger)base.GetField();
        }
       protected override IMyFormWebControl createFieldControl()
        {
            
                MyTxtBoxNumericInteger _txtInteger = new MyTxtBoxNumericInteger();
            
            return _txtInteger;
        }
        public override long? GetFormValue()
        {
            return this.GetField().FormValue;
            
        }
    }
}
