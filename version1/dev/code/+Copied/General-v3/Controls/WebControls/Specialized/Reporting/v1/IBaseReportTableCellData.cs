﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using CS.General_v3.Classes.CSS;

namespace CS.General_v3.Controls.WebControls.Specialized.Reporting.v1
{
    public interface IBaseReportTableCellData : IBaseReportTableCellDataBase
    {
        string InnerHTML { get;  }
        Control Control { get;  }

    }
}
