﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.WebControls.Specialized.Invoice
{
    public class InvoiceInfo
    {
        public string LogoURL;
        public string Address;
        public string Tel;
        public string Email;
        public string ClientDetails;
        public string InvoiceNumber;
        public DateTime Date;
        public List<InvoiceDetails> InvoiceDetailsTemp;
        public string AuthorizationCode;
        public string WebsiteTitle;
        public string LogoURLHref = "/";
        public double TotalPrice;
    }
}
