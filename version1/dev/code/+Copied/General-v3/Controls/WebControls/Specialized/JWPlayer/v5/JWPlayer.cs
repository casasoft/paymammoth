﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Design;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CS.General_v3.Controls.WebControls.Common;

using CS.General_v3.Util;
using CS.General_v3.Controls.WebControls.Common.SWFObject.v2;
namespace CS.General_v3.Controls.WebControls.Specialized.JWPlayer.v5
{
    public class JWPlayer : SWFObject {

        public enum JWPLAYER_CONTROL_BAR_POSITION {
            Bottom,
            Over,
            None
        }
        public enum JWPLAYER_REPEAT
        {
           None, List, Always, Single
        }



        public bool AutoStart { get { return this.Functionality.FlashVars.GetPropertyValueAsBool("autostart") == true; } set { this.Functionality.FlashVars.AddProperty("autostart", value); } }

        private JWPLAYER_CONTROL_BAR_POSITION _controlBar;
        public JWPLAYER_CONTROL_BAR_POSITION ControlBar
        {
            get { return _controlBar; }
            set
            {
                _controlBar = value;
                this.Functionality.FlashVars.AddProperty("controlbar", value.ToString().ToLower());
            }
        }

        private JWPLAYER_REPEAT _repeat;
        public JWPLAYER_REPEAT Repeat
        {
            get { return _repeat; }
            set
            {
                _repeat = value;
                this.Functionality.FlashVars.AddProperty("repeat", value.ToString().ToLower());
            }
        }
        public JWPlayer(string videoFileURL, string jwPlayerPath, string flashVersion, string expressInstall)
            : base(jwPlayerPath, flashVersion, expressInstall)
        {
            this.Functionality.AllowFullscreen = true;
            this.Functionality.AllowScriptAccess = ALLOW_SCRIPT_ACCESS.Always;
            this.Functionality.FlashVars.AddProperty("file", videoFileURL);

        }

        public JWPlayer(string videoFileURL)
            : this(videoFileURL, "/_common/static/flash/v1/jwplayer/v5/player.swf", "9", "/_common/static/flash/v1/swfobject/2.1/expressInstall.swf")
        {
        }
        


 
    }
}
