﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using System.Web.UI.HtmlControls;

namespace CS.General_v3.Controls.WebControls.Specialized.BottomBar
{
    public class BottomBar : MyDiv
    {
        private MyTable _tblFooter = new MyTable(cssClass: "footer-table");
        private MyTableRow _trFooterRow = new MyTableRow();
        private MyTableCell _tdSiteMap = new MyTableCell();
        private MyTableCell _tdFollowUs = new MyTableCell();
        private MyTableCell _tdOnlineShopping = new MyTableCell();
        private MyTableCell _tdDelivery = new MyTableCell();
        private MyTableCell _tdContactDetails = new MyTableCell();
        private MyTableCell _tdOpeningHours = new MyTableCell();

        public BottomColumnSiteMap SiteMap { get; set; }
        public BottomColumnFollowUs FollowUs { get; set; }
        public BottomColumnOnlineShopping OnlineShopping { get; set; }
        public BottomColumnDelivery Delivery { get; set; }
        public BottomColumnContact ContactDetails { get; set; }
        public BottomColumnOpeningHours OpeningHours { get; set; }
        
        public BottomBar()
        {
            init();
        }

        protected override void OnLoad(EventArgs e)
        {
            showColumns();
            base.OnLoad(e);
        }

        private void init()
        {
            initCss();
            loadDefault();
            appendClear();
        }

        private void initCss()
        {
            _tdSiteMap.CssClass = "footer-table-site-map";
            _tdOpeningHours.CssClass = "footer-table-opening-hours";
            _tdFollowUs.CssClass = "footer-table-follow-us";
            _tdOnlineShopping.CssClass = "footer-table-online-shopping";
            _tdDelivery.CssClass = "footer-table-delivery";
            _tdContactDetails.CssClass = "footer-table-contact-details";
        }

        private void appendClear()
        {
            HtmlGenericControl div = new HtmlGenericControl("div");
            div.Attributes["class"] = "clear";
            Controls.Add(div);
        }
        
        private void loadDefault()
        {
            SiteMap = new BottomColumnSiteMap();
            FollowUs = new BottomColumnFollowUs();
            ContactDetails = new BottomColumnContact();
            OnlineShopping = new BottomColumnOnlineShopping();
            Delivery = new BottomColumnDelivery();
            OpeningHours = new BottomColumnOpeningHours();
        }

        private void showColumns()
        {
            showSiteMapColumn();
            showFollowUsColumn();
            showOnlineShoppingColumn();
            showDelivery();
            showContactDetailsColumn();
            showOpeningHoursColumn();
            _tblFooter.Controls.Add(_trFooterRow);
            Controls.Add(_tblFooter);
        }

        private void showSiteMapColumn()
        {
            if (SiteMap != null)
            {
                _tdSiteMap.Controls.Add(SiteMap);
                _trFooterRow.Controls.Add(_tdSiteMap);
            }
        }

        private void showFollowUsColumn()
        {
            if (FollowUs != null)
            {
                _tdFollowUs.Controls.Add(FollowUs);
                _trFooterRow.Controls.Add(_tdFollowUs);
            }
        }

        private void showOnlineShoppingColumn()
        {
            if (OnlineShopping != null)
            {
                _tdOnlineShopping.Controls.Add(OnlineShopping);
                _trFooterRow.Controls.Add(_tdOnlineShopping);
            }
        }

        private void showContactDetailsColumn()
        {
            if (ContactDetails != null)
            {
                _tdContactDetails.Controls.Add(ContactDetails);
                _trFooterRow.Controls.Add(_tdContactDetails);
            }
        }

        private void showOpeningHoursColumn()
        {
            if (OpeningHours != null)
            {
                _tdOpeningHours.Controls.Add(OpeningHours);
                _trFooterRow.Controls.Add(_tdOpeningHours);
            }
        }

        private void showDelivery()
        {
            if (Delivery != null)
            {
                _tdDelivery.Controls.Add(Delivery);
                _trFooterRow.Controls.Add(_tdDelivery);
            }
        }
        
    }
}
