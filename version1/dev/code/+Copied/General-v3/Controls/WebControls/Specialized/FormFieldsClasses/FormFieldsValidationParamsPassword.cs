﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;
using System.Text.RegularExpressions;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses
{
    [Flags]
    public enum PASSWORD_STRENGTH_CHARACTERS
    {
        None = 1,
        Alpha = 2,
        BothLetterCase = 4,
        Numerics = 8,
        SpecialCharacters = 16
    }

    public class FormFieldsValidationParamsPassword : FormFieldsValidationParams
    {
        public bool showPasswordStrength = false;
        /// <summary>
        /// Mainly used to exclude from being like username.  Specify field ID
        /// </summary>
        public string passwordCannotBeLikeFieldID;
        public PASSWORD_STRENGTH_CHARACTERS passwordStrengthType = PASSWORD_STRENGTH_CHARACTERS.None;
        public string passwordCannotBeLikeFieldErrorMsg;
    }
}
