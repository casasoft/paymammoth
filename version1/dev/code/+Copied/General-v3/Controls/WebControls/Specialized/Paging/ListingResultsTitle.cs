﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;

namespace CS.General_v3.Controls.WebControls.Specialized.Paging
{
    public class ListingResultsTitle : MyDiv
    {
        public FUNCTIONALITY Functionality { get; set; }

        public class FUNCTIONALITY
        {
            private ListingResultsTitle _ctrl;
            private string _resultTextSingular = "result";
            private string _resultsTextPlural = "results";
        
            public FUNCTIONALITY(ListingResultsTitle ctrl)
            {
                _ctrl = ctrl;
            }
            public int ResultsTotal { get; set; }
            public int SelectedPageNum { get; set; }
            public int PageSize { get; set; }
          
            public string ResultTextSingular {
                get {
                    return _resultTextSingular;
                }
                set {
                    _resultTextSingular = value;
                }
            }

            public string ResultsTextPlural
            {
                get
                {
                    return _resultsTextPlural;
                }
                set
                {
                    _resultsTextPlural = value;
                }
            }

        }

        public ListingResultsTitle()
        {
            Functionality = new FUNCTIONALITY(this);
            setMainCss();
        }

        private void setMainCss()
        {
            this.CssClass = "paging-bar-text";
        }

        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            MySpan spanText = new MySpan();
            spanText.InnerHtml += "Showing <strong>";
            addItemToSpan(spanText, ((Functionality.SelectedPageNum - 1) * Functionality.PageSize + 1).ToString());
            spanText.InnerHtml += " - ";
            addItemToSpan(spanText, Math.Min(Functionality.ResultsTotal, Functionality.SelectedPageNum * Functionality.PageSize).ToString());
            spanText.InnerHtml += "</strong> of <strong>";
            addItemToSpan(spanText, Functionality.ResultsTotal.ToString());
            spanText.InnerHtml += "</strong> ";
            addItemToSpan(spanText, (Functionality.ResultsTotal == 1 ? Functionality.ResultTextSingular : Functionality.ResultsTextPlural));
            this.Controls.Add(spanText);
        }

        private void addItemToSpan(MySpan spanText, string text)
        {
            spanText.InnerHtml += text;
        }


    }
}
