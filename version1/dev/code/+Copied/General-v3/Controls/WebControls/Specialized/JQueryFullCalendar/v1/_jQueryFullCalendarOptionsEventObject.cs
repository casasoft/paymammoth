﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;

namespace CS.General_v3.Controls.WebControls.Specialized.JQueryFullCalendar.v1
{
    using JavaScript.Data;

    public class _jQueryFullCalendarOptionsEventObject : JavaScriptObject
    {
        /// <summary>
        /// String/Integer - Optional
        /// 
        /// Uniquely identifies the given event. Different instances of repeating events should all have the same id.
        /// </summary>
        public string id;

        /// <summary>
        /// String - Required
        /// 
        /// The text on an event's element
        /// </summary>
        public string title;

        /// <summary>
        /// Boolean - Optional
        /// 
        /// Whether an event occurs at a specific time-of-day. This property affects whether an event's time is shown.
        /// Also, in the agenda views, determines if it is displayed in the "all-day" section.
        /// 
        /// When specifying Event Objects for events or eventSources, omitting this property will make it inherit from
        /// allDayDefault, which is normally true.
        /// </summary>
        public bool allDay;

        /// <summary>
        /// DateTime - Required
        /// 
        /// The date/time an event begins.
        /// 
        /// When specifying Event Objects for events or eventSources, you may specify a string in IETF format
        /// (ex: "Wed, 18 Oct 2009 13:00:00 EST"), a string in ISO8601 format (ex: "2009-11-05T13:15:30Z") or
        /// a UNIX timestamp.
        /// </summary>
        public DateTime start;

        /// <summary>
        /// DateTime - Optional
        /// 
        /// The date/time an event ends.
        /// 
        /// As with start, you may specify it in IETF, ISO8601, or UNIX timestamp format.
        /// 
        /// If an event is all-day...
        /// the end date is inclusive. This means an event with start Nov 10 and end Nov 12 will span 3 days on the calendar.
        /// 
        /// If an event is NOT all-day...
        /// the end date is exclusive. This is only a gotcha when your end has time 00:00. It means your event ends on
        /// midnight, and it will not span through the next day.
        /// </summary>
        public DateTime end;

        /// <summary>
        /// String - Optional
        /// 
        /// A URL that will be visited when this event is clicked by the user. For more information on controlling this
        /// behavior, see the eventClick callback.
        /// </summary>
        public string url;

        /// <summary>
        /// String/Array - Optional
        /// 
        /// A CSS class (or array of classes) that will be attached to this event's element.
        /// </summary>
        public string className;

        /// <summary>
        /// Boolean - Optional
        /// 
        /// Overrides the master editable option for this single event.
        /// </summary>
        public bool editable;

        /// <summary>
        /// Event Source Object - Automatically populated
        /// 
        /// A reference to the event source that this event came from.
        /// </summary>
        public object source;

        /// <summary>
        /// String - Optional
        /// 
        /// Sets an event's background and border color just like the calendar-wide eventColor option.
        /// </summary>
        public string color;

        /// <summary>
        /// String - Optional
        /// 
        /// Sets an event's background color just like the calendar-wide eventBackgroundColor option.
        /// </summary>
        public string backgroundColor;

        /// <summary>
        /// String - Optional
        /// 
        /// Sets an event's border color just like the the calendar-wide eventBorderColor option.
        /// </summary>
        public string borderColor;

        /// <summary>
        /// String - Optional
        /// 
        /// Sets an event's text color just like the calendar-wide eventTextColor option.
        /// </summary>
        public string textColor;
    }
}
