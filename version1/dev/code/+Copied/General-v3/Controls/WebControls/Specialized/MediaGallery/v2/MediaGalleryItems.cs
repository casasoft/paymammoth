﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_20090518.Controls.WebControls.Common;

namespace CS.General_20090518.Controls.WebControls.Specialized.MediaGallery.v2
{
    public class MediaGalleryItems : MyDiv
    {
        public class FUNCTIONALITY
        {
            private MediaGalleryItems _ui;

            public List<MediaGalleryItemData> Items { get; private set; }

            public FUNCTIONALITY(MediaGalleryItems ui)
            {
                _ui = ui;
                Items = new List<MediaGalleryItemData>();
            }
            private void createItems()
            {
                for (int i = 0; i < Items.Count; i++)
                {
                    MediaGalleryItem itemUI = new MediaGalleryItem();
                    itemUI.Functionality.Data = Items[i];
                    _ui.Controls.Add(itemUI);
                }
            }
            public void Init()
            {
                createItems();

            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public MediaGalleryItems()
        {
            Functionality = new FUNCTIONALITY(this);
            this.CssManager.AddClass("cs-media-gallery-items");
        }
        protected override void OnLoad(EventArgs e)
        {
            Functionality.Init();
            base.OnLoad(e);
        }
    }
}
