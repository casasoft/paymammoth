﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;
using System.Text.RegularExpressions;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses
{
    using JavaScript.Data;

    public class FormFieldsCSSClasses : JavaScriptObject
    {
        public string cssClassReadOnly = "readonly";
        public string cssClassFocus = "focus";
        public string cssClassOver = "over";
        public string cssClassError = "error";
        public string cssClassDisabled = "disabled";
        public string cssClassRequired = "required";
        public string cssClassButton = "button";

        // These are copied from FormFields before adding the row
        public string cssClassTdLabel = null;
        public string cssClassTdField = null;
        public string cssClassTdHelpIcon = null;
        public string cssClassTdValidationIcon = null;
        public string cssClassValidationIconLoading = null;
        public string cssClassValidationIconSuccess = null;
        public string cssClassValidationIconError = null;
        public string cssClassValidationIconDim = null;
        public string cssClassTrRow = null;
        public string cssClassTrRowRequired = null;



    }
}
