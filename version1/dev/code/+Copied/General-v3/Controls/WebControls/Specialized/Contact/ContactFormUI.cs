﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
//using CS.General_v3;
using CS.General_v3.Classes.MediaItems;
using System.Web.UI;
using CS.General_v3.Controls.Classes.Forms;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Controls.WebControls;
using CS.General_v3.Controls.WebControls.Common.General;

using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;

namespace CS.General_v3.Controls.WebControls.Specialized.Contact
{

    public class ContactFormUI : FormFields
    {
        public class ContactFormField
        {
            public string Label { get; set; }
            public IMyFormWebControl Control { get; set; }
            public bool Enabled { get; set; }

            public ContactFormField()
            {
                Enabled = true;
            }
        }

        public new class FUNCTIONALITY : FormFields.FUNCTIONALITY
        {

            public delegate void EnquirySubmitHandler(string name, string email, string tel, string company, string website, string enquiry);
            public event EnquirySubmitHandler EnquirySubmit;
            protected new ContactFormUI _control
            {
                get
                {
                    return (ContactFormUI)base._control;
                }
            }
            public ContactFormField Name { get; set; }
            public ContactFormField Email { get; set; }
            public ContactFormField Tel { get; set; }
            public ContactFormField Company { get; set; }
            public ContactFormField Website { get; set; }
            public ContactFormField Enquiry { get; set; }


            public string SubmitText
            {
                get { return _control.ButtonSubmitText; }
                set { _control.ButtonSubmitText = value; }
            }


            public List<ContactFormField> Fields { get; private set; }
            public FUNCTIONALITY(ContactFormUI ui) : base(ui)
                
            {
                
                SubmitText = "Submit Enquiry";
                Name = new ContactFormField() { Label = "Full Name", Control = new MyTxtBoxTextSingleLine() { Required = true } };
                Email = new ContactFormField() { Label = "Email",  Control = new MyTxtBoxEmail() { Required = true } };
                Tel = new ContactFormField() { Label = "Contact No.",  Control = new MyTxtBoxTextSingleLine() { Required = false } };
                Company = new ContactFormField() { Label = "Company",  Control = new MyTxtBoxTextSingleLine(), Enabled = false };
                Website = new ContactFormField() { Label = "Website",  Control = new MyTxtBoxWebsite(), Enabled = false };
                Enquiry = new ContactFormField() { Label = "Enquiry", Control = new MyTxtBoxTextMultiLine() { Required = true, Rows = 8 } };
                Fields = new List<ContactFormField>() { Name, Email, Tel, Company, Website, Enquiry };

            }
            private void createControls()
            {
                
                for (int i = 0; i < Fields.Count; i++)
                {
                    IMyFormWebControl ctrl = Fields[i].Control;
                    ctrl.Title = Fields[i].Label;
                    if (Fields[i].Enabled)
                    {
                        ctrl.ID = this._control.ClientID + "_" + i;
                        _control.AddGenericLabelAndControl(Fields[i].Label, Fields[i].Control.Required, ctrl.Control);
                    }
                }
                this._control.ClickSubmit += new EventHandler(_form_ClickSubmit);
                _control.ButtonSubmitText = SubmitText;
            }

            void _form_ClickSubmit(object sender, EventArgs e)
            {
                if (EnquirySubmit != null)
                {
                    EnquirySubmit(
                        Name.Enabled ? Name.Control.FormValueObject.ToString() : null,
                        Email.Enabled ? Email.Control.FormValueObject.ToString() : null,
                        Tel.Enabled ? Tel.Control.FormValueObject.ToString() : null,
                        Company.Enabled ? Company.Control.FormValueObject.ToString() : null,
                        Website.Enabled ? Website.Control.FormValueObject.ToString() : null,
                        Enquiry.Enabled ? Enquiry.Control.FormValueObject.ToString() : null);

                }

            }

            public void Init()
            {
                createControls();
            }
        }
        public new FUNCTIONALITY Functionality { get; private set; }
        public ContactFormUI()
        {
            Functionality = new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            Functionality.Init();
            base.OnLoad(e);
        }
    }
}
