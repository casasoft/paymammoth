﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using CS.General_20101215.Controls.WebControls.Common;
using CS.General_20101215.Controls.Classes.MediaGallery;
using CS.General_20101215;
namespace CS.General_20101215.Controls.WebControls.Specialized.LinkSections
{
    [ToolboxData("<{0}:LinkSectionForm runat=server></{0}:LinkSectionForm>")]    
    public class LinkSectionForm : MenuTreeStructure.MenuFolderForm
    {

        /* HOW TO USE
        * ----------
        * 
        * Parameters to fill
        * 
        *  - FolderTitle
        *  - ListingPageUrl
        *  
        * Events to Attach
        *  
        *  - LoadFolder
        *  
        *  
        * Optional
        * 
        * - QueryStringFolderParam
        * - Has Description 
        */
        public LinkSectionForm()
        {
            base.LoadFolder += new CS.General_20101215.Controls.WebControls.Specialized.MenuTreeStructure.MenuFolderForm.CreateLoadFolderEvent(LinkSectionForm_LoadFolder);
            this.HasDescription = true;   
        }

        CS.General_20101215.Controls.WebControls.Specialized.MenuTreeStructure.IMenuFolder LinkSectionForm_LoadFolder(int ID)
        {
            return LoadFolder(ID);
        }
        #region Constants / Statics
        

        #endregion
        #region Delegates
        public delegate ILinkSection CreateLoadFolderEvent(int ID);
        public event CreateLoadFolderEvent LoadFolder;
        
        #endregion
        #region Events
        
        #endregion
        #region Methods
        
        #endregion
        #region Properties
        public bool HasDescription { get; set; }
        public new ILinkSection Folder
        {
            get
            {
                return (ILinkSection) base.Folder;
            }
        }
        #endregion
        

        protected override void OnInit(EventArgs e)
        {

            
            base.OnInit(e);
        }
        
      public override void  addExtraFormFields()
        {
            ILinkSection section = this.Folder;
          if (HasDescription)
                this.AddStringMultiline("txtDescription","Description",false,null,6,400, (section !=null ? section.Description : ""));
 	         base.addExtraFormFields();
        }
       
        public override void  saveExtraFields()
        {
            ILinkSection section = this.Folder;
            if (HasDescription)
                section.Description = this.GetFormValueStr("txtDescription");
 	         base.saveExtraFields();
        }

        
        protected override void OnLoad(EventArgs e)
        {
            
            base.OnLoad(e);
        }

        


 
    }
}
