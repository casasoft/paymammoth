﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;
using CS.General_v3.Controls.WebControls.Common;
using System.Web.UI.WebControls;

namespace CS.General_v3.Controls.WebControls.Specialized.BottomBar
{
    public class BottomColumnOpeningHours : BottomColumn
    {

        public new class FUNCTIONALITY : BottomColumn.FUNCTIONALITY
        {

            private MyDiv _wrapperDIV;
            //public Dictionary<string, string> OpeningHoursTable { get; set; }
            public string OpeningHoursHtml { get; set; }

            public FUNCTIONALITY(BottomColumnOpeningHours col)
                : base(col)
            {
                //OpeningHoursTable = new Dictionary<string, string>();
            }

            private void generateOpeningHoursTable()
            {
                if (!string.IsNullOrWhiteSpace(OpeningHoursHtml)) {
                    _wrapperDIV.Controls.Add(generateOpeningHoursControl());
                }
            }
            
            private void initHeading()
            {
                HtmlGenericControl titleSpan = new HtmlGenericControl("span");
                titleSpan.Attributes["class"] = "title";
                titleSpan.InnerText = Title;
                _wrapperDIV.Controls.Add(titleSpan);
            }

            private void generateWrapper()
            {
                _wrapperDIV = new MyDiv();
                _wrapperDIV.Attributes["class"] = "opening-hours-wrapper";
                PlaceHolder.Controls.Add(_wrapperDIV);
            }

            private Literal generateOpeningHoursControl()
            {
                Literal lit = new Literal();
                lit.Text = this.OpeningHoursHtml;
                return lit;
                
            }
            /*
            private HtmlTable generateTableFromDictionary()
            {
                HtmlTable table = new HtmlTable();
                table.CellPadding = 0;
                table.CellSpacing = 0;

                foreach (var item in OpeningHoursTable)
                {
                    HtmlTableRow row = new HtmlTableRow();
                    HtmlTableCell cellDay = new HtmlTableCell();
                    cellDay.InnerText = item.Key;
                    cellDay.Attributes["class"] = "day";
                    HtmlTableCell cellValue = new HtmlTableCell();
                    cellValue.InnerText = item.Value;
                    row.Cells.Add(cellDay);
                    row.Cells.Add(cellValue);
                    table.Rows.Add(row);
                }

                return table;
            }
            */
            internal override void Init()
            {
                generateWrapper();
                initHeading();
                generateOpeningHoursTable();
                base.Init();
            }

        }
        public new FUNCTIONALITY Functionality
        {
            get { return (FUNCTIONALITY)base.Functionality; }
            set
            {
                base.Functionality = value;
            }
        }

        public BottomColumnOpeningHours()
        {
            Functionality = new FUNCTIONALITY(this);
        }

    }
}
