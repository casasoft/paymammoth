﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common.General;
using CS.General_v3.Controls.WebControls.Common;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data
{
    public abstract class FormFieldStringBaseData : FormFieldBaseTypeData<string>
    {
        public static FormFieldStringBaseData GetFormFieldStringBaseDataFromStringDataType(CS.General_v3.Enums.STRING_DATA_TYPE stringDataType)
        {
            
            switch (stringDataType)
            {
                case Enums.STRING_DATA_TYPE.Email: return new FormFieldStringEmailData();
                case Enums.STRING_DATA_TYPE.MultiLine: return new FormFieldStringMultilineData();
                case Enums.STRING_DATA_TYPE.Password: return new FormFieldStringPasswordData();
                case Enums.STRING_DATA_TYPE.SingleLine: return new FormFieldStringData();
                case Enums.STRING_DATA_TYPE.Website: return new FormFieldStringWebsiteData();
                case Enums.STRING_DATA_TYPE.Html: return new FormFieldStringHtmlData();

            }
            throw new InvalidOperationException("Invalid string data type");

        }
        public bool AutoCompleteDisabled { get; set; }
        public int MinLength { get { return _validationParams.minLength; } set { _validationParams.minLength = value; } }
        public int MaxLength { get { return _validationParams.maxLength; } set { _validationParams.maxLength = value; } }

        public new MyTxtBoxTextSingleLine GetField()
        {
            return (MyTxtBoxTextSingleLine)base.GetField();
        }

        public FormFieldStringBaseData()
        {
            
        }
        protected override IMyFormWebControl createFieldControl()
        {

             MyTxtBoxTextSingleLine txt = new MyTxtBoxTextSingleLine();
             txt.AutoCompleteDisabled = this.AutoCompleteDisabled;
            return txt;
                
        }
        
        public override string GetFormValue()
        {
            return GetField().GetFormValueAsStr();
        }
    }
}
