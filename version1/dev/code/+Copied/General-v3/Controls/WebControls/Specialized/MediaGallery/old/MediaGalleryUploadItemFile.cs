﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
namespace CS.General_v3.Controls.WebControls.Specialized.MediaGallery
{
    public class MediaGalleryUploadItemFile
    {
        public System.IO.Stream Stream {get;set;}
        public string Filename {get;set;}
        public string Caption {get;set;}
        
        public MediaGalleryUploadItemFile()
        {

        }
        public MediaGalleryUploadItemFile(MediaGalleryUploadItem item)
        {
            FillFromMediaUploadItem(item);
        }
        public void FillFromMediaUploadItem(MediaGalleryUploadItem item)
        {
            this.Stream = item.TxtUpload.FileContent;
            this.Filename = item.TxtUpload.FileName;
            this.Caption = "";
            
            if (item.TxtCaption != null)
            {
                this.Caption = item.TxtCaption.FormValue;
            }

        }

    }
}
