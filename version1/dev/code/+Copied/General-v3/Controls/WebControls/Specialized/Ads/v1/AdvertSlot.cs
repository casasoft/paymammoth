﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing.Design;
using CS.General_20101215.Controls.WebControls.Common;
using CS.General_20101215.Controls.WebControls.Common.SWFObject.v2;
using CS.General_20101215.Controls.WebControls.Specialized.Flv.SimpleFlvPlayer.v1;
namespace CS.General_20101215.Controls.WebControls.Specialized.Ads
{
    public class AdvertSlot : MyDiv
    {
        public class FUNCTIONALITY
        {
            private AdvertSlot _slot;
            public FUNCTIONALITY(AdvertSlot ad)
            {
                _slot = ad;
            }
        }

        public new FUNCTIONALITY Functionality { get; private set; }
        public static string GOOGLE_ANALYTICS_TRACKER_VAR_NAME = "_gaq";//"pageTracker";
        public static string GOOGLE_ANALYTICS_EVENT_CATEGORY = "Adverts";

        private int _width;
        private int _height;
        private string _itemUrl;
        private string _href;
        
        private string _transparentMaskImageUrl;
        public string BackgroundColorHex { get; set; }
        public string FlashVersion { get; set; }
        public string ExpressInstallUrl { get; set; }
        public string FlashFlvPlayeUrl { get; set; }
        public bool GoogleAnalyticsSynchronousCode { get; set; }

        public CS.General_20101215.Classes.Adverts.Advert _advert = null;
        public AdvertSlot(CS.General_20101215.Classes.Adverts.Advert advert, int width, int height, string itemURL, string href, string transparentMaskImageUrl, bool googleAnalyticsSynchronousCode)
            
        {
            this.GoogleAnalyticsSynchronousCode = googleAnalyticsSynchronousCode;
            _advert = advert;
            FlashVersion = "8.0.0";
            ExpressInstallUrl = "/_common/flash/v1/swfobject/expressInstall.swf";
            FlashFlvPlayeUrl = "/_common/flash/v1/flv/player.swf";
            _transparentMaskImageUrl = transparentMaskImageUrl;
            _width = width;
            _height = height;
            _itemUrl = itemURL;
            _href = href;
            this.CssManager.AddClass("ad-slot");
            this.Style.Add("position", "relative");
           
        }

        private void initImage()
        {
            MyImage imgAdvert = new MyImage();
            imgAdvert.ImageUrl = _itemUrl;
            Controls.Add(imgAdvert);
        }
        private void initSwf()
        {
            
            SWFObject swfObject = new SWFObject(_itemUrl, FlashVersion, ExpressInstallUrl);
            swfObject.Functionality.FlashWidth = new Unit(_width, UnitType.Pixel);
            swfObject.Functionality.FlashHeight =  new Unit(_height,  UnitType.Pixel);
            //swfObject.FlashVars["clickTAG"] = _href;
            swfObject.Functionality.WindowMode = SWFObject.WMODE.Transparent;
            if (!string.IsNullOrEmpty(BackgroundColorHex))
            {
                swfObject.Functionality.BackgroundColor = BackgroundColorHex;
                this.Style.Add("background-color", BackgroundColorHex);
            }
            
            Controls.Add(swfObject);
        }
        private void initFlv()
        {
            SimpleFlvPlayer flvPlayer = new SimpleFlvPlayer(_itemUrl, new Unit(_width, UnitType.Pixel), new Unit(_height, UnitType.Pixel));
            flvPlayer.LoopPlayback = true;
            flvPlayer.SwfObjectWindowMode = SWFObject.WMODE.Transparent;
            flvPlayer.MaintainAspectRatio = false;
            Controls.Add(flvPlayer);
        }

        private void initItem()
        {
            if (_itemUrl.ToLower().EndsWith(".swf"))
            {
                initSwf();
            }
            else if (_itemUrl.ToLower().EndsWith(".flv"))
            {
                initFlv();
            }
            else
            {
                initImage();
            }
        }
        private void initMask()
        {
            if (!string.IsNullOrEmpty(_href))
            {
                MyImage imgMask = new MyImage();
                imgMask.ImageUrl = _transparentMaskImageUrl;

                imgMask.Style.Add("width", _width + "px");
                imgMask.Style.Add("height", _height + "px");
                imgMask.Style.Add("position", "absolute");
                imgMask.Style.Add("left", "0px");
                imgMask.Style.Add("top", "0px");
                Controls.Add(imgMask);
                imgMask.AlternateText = _advert.Title + "(" + CS.General_20101215.Util.Date.Now.ToString() + ")";
                
                imgMask.HRef = _href;
                string googleJs = _advert.GetGoogleAnalyticsCodeForHit(GOOGLE_ANALYTICS_TRACKER_VAR_NAME, GOOGLE_ANALYTICS_EVENT_CATEGORY, GoogleAnalyticsSynchronousCode);
                imgMask.OnClientClick = googleJs;
                imgMask.HRefTarget = Enums.HREF_TARGET.Blank;
            }
            else
            {
                int k = 5;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            initItem();
            initMask();
            base.OnLoad(e);
        }
    }
}
