﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.Web.UI.WebControls;


using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Controls.WebControls.Specialized.ListingClasses;

namespace CS.General_v3.Controls.WebControls.Specialized
{


    /// <summary>
    /// This will show a listing control for CMS.  All you need to do is set the DataSource, set the ItemDataBound handler and call DataBind()
    /// </summary>
    [ToolboxData("<{0}:Listing runat=server></{0}:Listing>")]   
    public class Listing : BaseWebControl
    {
        public class FUNCTIONALITY
        {
            public FUNCTIONALITY(Listing listing)
            {
                _listing = listing;

               /* this.DefaultImage_DeleteCssClass =      "listing-button-delete";
                this.DefaultImage_EditCssClass =        "listing-button-edit";
                this.DefaultImage_ExpandCssClass =      "listing-button-expand";
                this.DefaultImage_UnExpandCssClass =    "listing-button-unexpand";*/

                this.DefaultImage_DeleteUp = "/_common/static/images/icons/v1/icon_delete_up.png";
                this.DefaultImage_DeleteOver = "/_common/static/images/icons/v1/icon_delete_over.png";
                this.DefaultImage_EditUp = "/_common/static/images/icons/v1/icon_edit_up.png";
                this.DefaultImage_EditOver = "/_common/static/images/icons/v1/icon_edit_over.png";
                this.DefaultImage_Expand = "/_common/static/images/icons/v1/icon_expand.gif";
                this.DefaultImage_UnExpand = "/_common/static/images/icons/v1/icon_unexpand.gif";
            }

            internal protected string _queryStringSortByKey = null;
            internal protected string _queryStringSortTypeKey = null;
            internal protected List<ColumnData> _columns = new List<ColumnData>();

            internal protected IEnumerable<object> _dataSource;
            internal protected List<WebControl> _generalButtons = new List<WebControl>();
            //private List<ListingItemImageButton> _itemButtons = new List<ListingItemImageButton>();
            internal protected List<IListingButtonInfo> _itemButtons = new List<IListingButtonInfo>();

            /// <summary>
            /// Triggered when the delete selected button is clicked
            /// </summary>
            public event ItemsHandler DeleteClick;
            internal void _callDeleteClick(List<ListingItemRow> rows)
            {
                if (DeleteClick != null)
                    DeleteClick(rows);
            }
            internal bool _deleteClickIsAttached()
            {
                return (DeleteClick != null);
            }
            internal ItemsHandler _getDeleteClick()
            {
                return DeleteClick;
            }
            public event ItemHrefRetriever EditHrefRetriever;
            internal bool _EditHrefRetrieverIsAttached()
            {
                return (EditHrefRetriever != null);
            }
            internal ItemHrefRetriever _getEditHrefRetreiver()
            {
                return EditHrefRetriever;
            }
            internal string _callEditHrefRetriever(object o)
            {
                if (EditHrefRetriever != null)
                    return EditHrefRetriever(o);
                else
                    return null;
            }
            public event ItemHandler ItemDataBound;
            internal ItemHandler _getItemDataBound()
            {
                return ItemDataBound;
            }
            
            internal bool _ItemDataBoundIsAttached()
            {
                return (ItemDataBound != null);
            }
            internal bool _callItemDataBound(ListingItemRow row)
            {
                bool ok = true;
                if (ItemDataBound != null)
                    ok =ItemDataBound(row);
                return ok;
            }
            /// <summary>
            /// Whether to show an extra expandable row, and a plus/minus image to the left of the row
            /// </summary>
            public bool ShowExpandableRow { get; set; }
            /// <summary>
            /// Whether the expanded row is initially visible or not
            /// </summary>
            public bool InitExpandableRowVisible { get; set; }
            protected Listing _listing = null;
            
            /// <summary>
            /// The list of selected items
            /// </summary>
            public List<ListingItemRow> SelectedItems
            {
                get
                {
                    List<ListingItemRow> selectedItems = new List<ListingItemRow>();
                    for (int i = 0; i < this._items.Count; i++)
                    {
                        ListingItemRow row = this._items[i];
                        if (row.CheckBox.Checked)
                        {
                            selectedItems.Add(row);
                        }
                    }
                    return selectedItems;
                }
            }

            /*public string DefaultImage_DeleteCssClass { get; set; }
            public string DefaultImage_EditCssClass { get; set; }
            public string DefaultImage_ExpandCssClass { get; set; }
            public string DefaultImage_UnExpandCssClass { get; set; }*/

            public string DefaultImage_DeleteUp { get; set; }
            public string DefaultImage_DeleteOver { get; set; }
            public string DefaultImage_EditUp { get; set; }
            public string DefaultImage_EditOver { get; set; }
            public string DefaultImage_Expand { get; set; }
            public string DefaultImage_UnExpand { get; set; }


            internal List<ListingItemRow> _items = new List<ListingItemRow>();
            public List<ListingItemRow> Items { get { return _items; } }
            /// <summary>
            /// Returns the column Title which to sort with
            /// </summary>
            public string SortBy
            {
                get
                {

                    if (!String.IsNullOrEmpty(CS.General_v3.Util.PageUtil.GetVariableFromQuerystring(QueryStringSortByKey)))
                    {
                        //Sort by is passed in query string
                        string ID = CS.General_v3.Util.PageUtil.GetVariableFromQuerystring(QueryStringSortByKey);
                        SortBy = ID;
                    }
                    if (CS.General_v3.Util.PageUtil.GetSessionObject<string>(CS.General_v3.Util.PageUtil.SessionKey + "_" + QueryStringSortByKey) != null)
                    {
                        return CS.General_v3.Util.PageUtil.GetSessionObject<string>(CS.General_v3.Util.PageUtil.SessionKey + "_" + QueryStringSortByKey);
                    }
                    return null;
                }
                set
                {
                    CS.General_v3.Util.PageUtil.SetSessionObject(CS.General_v3.Util.PageUtil.SessionKey + "_" + QueryStringSortByKey, value);
                }
            }
            /// <summary>
            /// Returns the sort type method
            /// </summary>
            public CS.General_v3.Enums.SORT_TYPE SortType
            {
                get
                {
                    CS.General_v3.Enums.SORT_TYPE sortType = Enums.SORT_TYPE.Ascending;
                    if (!String.IsNullOrEmpty(CS.General_v3.Util.PageUtil.GetVariableFromQuerystring(QueryStringSortTypeKey)))
                    {
                        //Sort by is passed in query string

                        sortType = (CS.General_v3.Enums.SORT_TYPE)Enum.Parse(typeof(CS.General_v3.Enums.SORT_TYPE), CS.General_v3.Util.PageUtil.GetVariableFromQuerystring(QueryStringSortTypeKey));
                    }
                    else if (CS.General_v3.Util.PageUtil.GetSessionObject(CS.General_v3.Util.PageUtil.SessionKey + "_" + QueryStringSortTypeKey) != null)
                    {
                        sortType = (CS.General_v3.Enums.SORT_TYPE)CS.General_v3.Util.PageUtil.GetSessionObject(CS.General_v3.Util.PageUtil.SessionKey + "_" + QueryStringSortTypeKey);
                    }
                    return sortType;
                }
                set
                {
                    CS.General_v3.Util.PageUtil.SetSessionObject(CS.General_v3.Util.PageUtil.SessionKey + "_" + QueryStringSortTypeKey, value);
                }
            }

            public IEnumerable<object> DataSource
            {
                get
                {

                    
                    
                    return this._dataSource;
                }
                set
                {
                    this._dataSource = value;
                }

            }


            /// <summary>
            /// The querystring variable name for sorting by. By default (null), it will b the ClientID + "_sortby"
            /// </summary>
            public string QueryStringSortByKey
            {
                get
                {
                    if (String.IsNullOrEmpty(_queryStringSortByKey))
                    {
                        return _listing.ClientID + "_sortby";
                    }
                    else
                    {
                        return _queryStringSortByKey;
                    }
                }
                set
                {
                    this._queryStringSortByKey = value;
                }
            }
            /// <summary>
            /// The querystring variable name for sorting type. By default (null), it will be the ClientID + "_sorttype"
            /// </summary>
            public string QueryStringSortTypeKey
            {
                get
                {
                    if (String.IsNullOrEmpty(_queryStringSortTypeKey))
                    {
                        return _listing.ClientID + "_sorttype";
                    }
                    else
                    {
                        return _queryStringSortTypeKey;
                    }
                }
                set
                {
                    this._queryStringSortTypeKey = value;
                }
            }



            
            /// <summary>
            /// Adds a general button which applies to ALL selected items in the listing.
            /// </summary>
            /// <param name="title"></param>
            /// <param name="imageUp">Relative or full path to image</param>
            /// <param name="imageOver">Relative or full path to image</param>
            /// <param name="clickHandler">The click handler to be triggered when it is clicked</param>
            /// <returns>The button reference which is going to be added.  This is used so that you can add whatever properties you want</returns>
            public MyAnchor AddGeneralLink(string title, string cssClass, string link, int? index = null)
            {
                if (!index.HasValue) index = this._generalButtons.Count;
                MyAnchor a = new MyAnchor();

                a.ID = "btnLink_" + _listing.ID + "_" + index;
                a.InnerText = title;
                a.Href = link;

                this._generalButtons.Insert(index.Value, a);
                return a;

            }
            /// <summary>
            /// Adds a general button which applies to ALL selected items in the listing.
            /// </summary>
            /// <param name="title"></param>
            /// <param name="imageUp">Relative or full path to image</param>
            /// <param name="imageOver">Relative or full path to image</param>
            /// <param name="clickHandler">The click handler to be triggered when it is clicked</param>
            /// <param name="clickHandler">The click handler to be triggered when it is clicked</param>
            /// <param name="index">The index where to place the button</param>
            /// <returns>The button reference which is going to be added.  This is used so that you can add whatever properties you want</returns>
            public ListingItemGeneralButton AddGeneralButton(string title, string imageURL, ItemsHandler clickHandler, int? index = null)
            {
                if (!index.HasValue) index = this._generalButtons.Count;
                ListingItemGeneralButton btn = new ListingItemGeneralButton();
                btn.ID = "btnGeneralListing_" + _listing.ID + "_" + index;
                btn.Text = title;
                btn.IconImageUrl = imageURL;

                //btn.ImageUrl = imageUp;
               // btn.RolloverImageUrl = imageOver;

                //btn.Click += GeneralButton_Click;

                this._generalButtons.Insert(index.Value, btn);
                btn.Tag = clickHandler;
                return btn;

            }



            /// <summary>
            /// Adds a button which is used as a link.
            /// </summary>
            /// <param name="title"></param>
            /// <param name="imageUp">Relative or full path to image</param>
            /// <param name="imageOver">Relative or full path to image</param>
            /// <param name="hrefRetriever">The handler which is called to retrieve the URL to be set to the Href of each button.  It must be a delegate which returns a string and takes as parameter IDBObject</param>
            /// <param name="index">Index to add button at</param>
            /// <returns></returns>
            public ListingItemImageLinkButton AddItemLinkButton(string title, string imageURL, ItemHrefRetriever hrefRetriever, int? index = null)
            {
                if (index == null)
                {
                    index = this._itemButtons.Count;
                }
                ListingItemImageLinkButton btn = new ListingItemImageLinkButton(hrefRetriever);

                btn.Text = title;
                btn.IconImageUrl = imageURL;
                btn.ID = "btnLink_Listing_" + _listing.ID + "_" + index;
                //btn.ImageUrl = imageUp;
                //btn.RollOverImage = imageOver;


                this._itemButtons.Insert(index.Value, btn);
                return btn;
            }
            /// <summary>
            /// Adds a button which is used as a link.
            /// </summary>
            /// <param name="title"></param>
            /// <param name="imageUp">Relative or full path to image</param>
            /// <param name="imageOver">Relative or full path to image</param>
            /// <param name="hrefRetriever">The handler which is called to retrieve the URL to be set to the Href of each button.  It must be a delegate which returns a string and takes as parameter IDBObject</param>
            /// <param name="index">Index to add button at</param>
            /// <returns></returns>
            public MyAnchor AddItemLinkButtonText(string title, ItemHrefRetriever hrefRetriever, int? index = null, string cssClass = null)
            {
                if (index == null)
                {
                    index = this._itemButtons.Count;
                }

                ListingItemTextLinkButton btn = new ListingItemTextLinkButton(hrefRetriever);
                btn.CssManager.AddClass(cssClass);
                btn.Title = title;
                btn.InnerHtml = CS.General_v3.Util.Text.HtmlEncode(title);
                btn.ID = "btnLink_Text_Listing_" + _listing.ID + "_" + index;
                //            btn.Text = btn.AlternateText = title;
                //          btn.ID = "btnLink_Listing_" + _listing.ID + "_" + index;
                //        btn.ImageUrl = imageUp;
                //      btn.RollOverImage = imageOver;


                this._itemButtons.Insert(index.Value, btn);
                return btn;
            }

            
            /// <summary>
            /// Adds a button which is added with every item listing on the right.
            /// </summary>
            /// <param name="title">Title of button</param>
            /// <param name="imageUp">Relative or full path to image</param>
            /// <param name="imageOver">Relative or full path to image</param>
            /// <param name="clickHandler">Handler to be executed on click</param>
            /// <param name="index">The index where to add the button</param>
            /// <returns>Reference to the TEMPLATE button.  You can use this to set whatever extra properties you want to the button, e.g. Confirm Message etc...</returns>

            public MyButton AddItemButton(string title, string cssClass, ItemHandler clickHandler, int? index = null)
            {
                if (index == null) index = _itemButtons.Count;

                ListingItemImageSubmitButton btn = new ListingItemImageSubmitButton(clickHandler);
                btn.ID = "btnItemListing_" + _listing.ID + "_" + index;
                btn.Text = title;
              
                //btn.ImageUrl = imageUp;
                //btn.RollOverImage = imageOver;
                this._itemButtons.Insert(index.Value, btn);
                return btn;
            }




            public void AddColumn(ColumnData column)
            {
                this._columns.Add(column);
            }
            public void AddColumn(ColumnData column, int index)
            {
                this._columns.Insert(index, column);
            }


            public virtual void DataBind()
            {
                if (_dataSource == null)
                {
                    throw new InvalidOperationException("Specialized.Listing:: Please fill in DataSource");
                }
                if (ItemDataBound == null)
                {
                    throw new InvalidCastException("Specialized.Listing:: Please attach to 'ItemDataBound'");
                }
                _listing.dataBind();
            }



        }
        public FUNCTIONALITY Functionality { get; private set; }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }



        protected void dataBind()
        {
            initControls();
            initDefaultButtons();
            initHeader();
            initTitlesAndColGroup();
            initItems();
            initExpandableRows();
            initFooter();
        }






       


        #region Statics/Constants

        private const string CHECKBOX_SELECTOR_PREFIX = "chkSelector";
        #endregion
        #region Delegates/Events

        public delegate string ItemHrefRetriever(object obj);
        public delegate bool ItemHandler(ListingItemRow row);
        public delegate void ItemsHandler(List<ListingItemRow> selectedItems);


        
        

        #endregion
        #region Methods



        /// <summary>
        /// Initialize default buttons if at least one handler is attached to them
        /// </summary>
        private void initDefaultButtons()
        {
            if (this.Functionality._deleteClickIsAttached())
            {
                MyButton imgDelete = this.Functionality.AddGeneralButton("Delete Selected",
                     this.Functionality.DefaultImage_DeleteUp, this.Functionality._getDeleteClick(), 0);
                imgDelete.ConfirmMessage = "Are you sure you want to delete the selected items?";
                imgDelete.ValidationGroup = "none";
            }
            if (this.Functionality._EditHrefRetrieverIsAttached())
            {
                var btn = this.Functionality.AddItemLinkButton("", this.Functionality.DefaultImage_EditUp, this.Functionality._getEditHrefRetreiver(), 0);
                btn.Title = "View More Info / Edit";
            }
        }



        /// <summary>
        /// The header where the general buttons reside
        /// </summary>
        private void initHeader()
        {
            if (this.Functionality._generalButtons.Count > 0)
            {
                thTHead.ColSpan = _totalColumns;
                thTHead.Attributes["class"] += " listing-heading-buttons";
                for (int i = 0; i < this.Functionality._generalButtons.Count; i++)
                {
                    WebControl btn = this.Functionality._generalButtons[i];
                    btn.CssClass = "general-button";
                    //btn2.ImageUrl = btn.ImageUrl;
                    
                    thTHead.Controls.Add(btn);
                    if (btn is MyButton)
                    {
                        MyButton myButton = (MyButton)btn;
                        myButton.Click += GeneralButton_Click;
                    }

                }
            }
            else
            {
                tHead.Visible = false;
            }

        }

        private void initFooter()
        {
            thTFoot.ColSpan = _totalColumns;
            if (atLeastOneExpandableRow)
                thTFoot.ColSpan++;
        }


        /// <summary>
        /// Triggered whenever a button associated with an item is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ItemButton_Click(object sender, EventArgs e)
        {
            ListingItemImageSubmitButton btn = (ListingItemImageSubmitButton)sender;


            ListingItemRow row = (ListingItemRow)btn.Tag;

            btn.SubmitHandler(row);
        }




        private void GeneralButton_Click(object sender, EventArgs e)
        {
            MyButton btn = (MyButton)sender;

            ItemsHandler clickHandler = (ItemsHandler)btn.Tag;

            clickHandler(this.Functionality.SelectedItems);

        }

        private bool isColumnCheckbox(int columnIndex)
        {
            return columnIndex == 0 && this.Functionality._generalButtons.Count > 0;
        }
        private bool isColumnDataCell(int columnIndex)
        {
            int startIndex = (this.Functionality._generalButtons.Count == 0) ? 0 : 1;
            int endIndex = startIndex + this.Columns - 1;
            return columnIndex >= startIndex && columnIndex <= endIndex;
        }
        private bool isColumnButton(int columnIndex)
        {
            int endIndex = this.Columns;
            if (this.Functionality._generalButtons.Count > 0)
            {
                endIndex++;
            }
            return columnIndex >= endIndex;
        }


        private void initTitleCell(HtmlTableCell th, ColumnData column)
        {
            
            
            if (column.Sortable)
            {
                HtmlTable tbl = new HtmlTable();

                HtmlTableRow tr = new HtmlTableRow();
                tbl.Rows.Add(tr);
                tr.Cells.Add(new HtmlTableCell());
                tr.Cells.Add(new HtmlTableCell());
                tr.Cells.Add(new HtmlTableCell());
                th.Controls.Add(tbl);
                tbl.Attributes["class"] = "sort";
                tbl.Style.Add("width", "100%");
                tbl.CellPadding = 0;
                tbl.CellSpacing = 0;

                tr.Cells[0].InnerHtml = column.Title;
                tr.Cells[1].Attributes["class"] = tr.Cells[2].Attributes["class"] = "sort-arrow";

                MyImage btnSortAsc = new MyImage();
                MyImage btnSortDesc = new MyImage();
                btnSortAsc.ImageUrl = "/_common/static/images/icons/v1/arrow_sort_ASC_up.png";
                btnSortAsc.RollOverImage = "/_common/static/images/icons/v1/arrow_sort_ASC_over.png";

                btnSortDesc.ImageUrl = "/_common/static/images/icons/v1/arrow_sort_DESC_up.png";
                btnSortDesc.RollOverImage = "/_common/static/images/icons/v1/arrow_sort_DESC_over.png";

                CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass();
                url.QS[this.Functionality.QueryStringSortByKey] = column.Title;
                url.QS[this.Functionality.QueryStringSortTypeKey] = CS.General_v3.Enums.SORT_TYPE.Ascending.ToString();
                btnSortAsc.HRef = url.ToString();

                url.QS[this.Functionality.QueryStringSortTypeKey] = CS.General_v3.Enums.SORT_TYPE.Descending.ToString();
                btnSortDesc.HRef = url.ToString();



                tr.Cells[1].Controls.Add(btnSortAsc);
                tr.Cells[2].Controls.Add(btnSortDesc);


                string sortColumnTitle = this.Functionality.SortBy;
                //INIT SORTING
                if ((String.IsNullOrEmpty(sortColumnTitle) && column.DefaultSort != CS.General_v3.Enums.SORT_TYPE.None) ||
                    (!String.IsNullOrEmpty(sortColumnTitle) && String.Compare(sortColumnTitle, column.Title, true) == 0))
                {
                    //Sort by this

                    if ( this.Functionality.SortType == CS.General_v3.Enums.SORT_TYPE.Ascending)
                    {
                        btnSortAsc.RollOverImage = null;
                        btnSortAsc.ImageUrl = "/_common/static/images/icons/v1/arrow_sort_ASC_selected.png";
                        btnSortAsc.Style.Add("cursor", "default");
                    }
                    else
                    {
                        btnSortDesc.RollOverImage = null;
                        btnSortDesc.ImageUrl = "/_common/static/images/icons/v1/arrow_sort_DESC_selected.png";
                        btnSortDesc.Style.Add("cursor", "default");
                    }
                }


            }
            else
            {
                th.InnerHtml = column.Title;
            }
            CS.General_v3.Classes.CSS.CSSManagerForControl cssManager = new CS.General_v3.Classes.CSS.CSSManagerForControl(th);
            cssManager.AddClass(column.CssClass);

        }
        /// <summary>
        /// This will parse the column sorting and will set only one.  If nothing is set, then the first one is set to ascending
        /// </summary>
        private void parseColumnsSorting()
        {
            bool foundDefaultSort = false;
            for (int i = 0; i < this.Functionality._columns.Count; i++)
            {
                ColumnData col = this.Functionality._columns[i];
                if (col.Sortable == false) continue; // omit this
                if (!foundDefaultSort)
                {
                    if (col.DefaultSort != CS.General_v3.Enums.SORT_TYPE.None)
                    {
                        foundDefaultSort = true;

                    }
                }
                else
                {
                    col.DefaultSort = CS.General_v3.Enums.SORT_TYPE.None;
                }

            }
            if (!foundDefaultSort)
            {
                for (int i = 0; i < this.Functionality._columns.Count; i++)
                {
                    ColumnData col = this.Functionality._columns[i];
                    if (col.Sortable)
                    {
                        col.DefaultSort = CS.General_v3.Enums.SORT_TYPE.Ascending;
                    }
                }
            }
        }

        /// <summary>
        /// Initalizes the column titles and column group widths
        /// </summary>
        private void initTitlesAndColGroup()
        {
            trColumnHeader = new HtmlTableRow();
            HtmlTableRow tr = trColumnHeader;
            tBody.Controls.Add(tr);
            int index = 0;
            for (int i = 0; i < this._totalColumns; i++)
            {
                HtmlTableCell th = new HtmlTableCell("th");
                HtmlGenericControl col = new HtmlGenericControl("col");
                colGroup.Controls.Add(col);
                tr.Cells.Add(th);

                if (isColumnCheckbox(i))
                {
                    //Add Select All Checkbox
                    CheckBox chkSelectAll = new CheckBox();
                    chkSelectAll.ID = this.ID + "_chkSelectAll";
                    th.Controls.Add(chkSelectAll);
                    col.Attributes["width"] = "16";



                    string js = "new js.com.cms." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION + ".ListingCheckboxSelector('" + chkSelectAll.ClientID + "','" + this.ClientID + "_" + CHECKBOX_SELECTOR_PREFIX + "'," + this.Functionality._dataSource.Count() + ");";
                    CS.General_v3.Util.PageUtil.GetCurrentPage().ClientScript.RegisterClientScriptBlock(this.GetType(), this.ClientID + "_" + CHECKBOX_SELECTOR_PREFIX, js, true);


                }
                else if (isColumnDataCell(i))
                {

                    //Columns with normal titles
                    ColumnData column = this.Functionality._columns[index];
                    if (!String.IsNullOrEmpty(column.Width))
                    {
                        col.Attributes["width"] = column.Width;
                    }

                    initTitleCell(th, column);



                    index++;
                }
                else if (isColumnButton(i))
                {
                    col.Attributes["width"] = "16";
                    //Button Columns
                    th.InnerHtml = "&nbsp;";
                }
            }
        }
        private void initItems()
        {
            IEnumerator enumerator = this.Functionality.DataSource.GetEnumerator();
            int i = 0;
            while (enumerator.MoveNext())
            {

                object obj = enumerator.Current;
                HtmlTableRow tr = new HtmlTableRow();
                HtmlTableRow trExpandableRow = new HtmlTableRow();
                trExpandableRow.Visible = false;
                //tBody.Controls.Add(tr);
                //tBody.Controls.Add(trExpandableRow);
                tr.Attributes["class"] = "row_" + (i % 2);

                List<HtmlTableCell> dataCells = new List<HtmlTableCell>();
                int btnCount = 0;
                ListingItemRow row = new ListingItemRow(tr, obj, dataCells, i);
                row.ShowExpandableRow = this.Functionality.ShowExpandableRow;
                row.InitExpandableRowVisible = this.Functionality.InitExpandableRowVisible;
                row.ExpandableRow = trExpandableRow;

                for (int j = 0; j < _totalColumns; j++)
                {
                    HtmlTableCell td = new HtmlTableCell();

                    tr.Cells.Add(td);
                    if (isColumnCheckbox(j))
                    {
                        CheckBox chk = null;
                        chk = new CheckBox();
                        chk.ID = this.ID + "_" + CHECKBOX_SELECTOR_PREFIX + "_" + i; //To identify checkboxes with
                        td.Controls.Add(chk);
                        td.Attributes["class"] = "checkbox-selector";
                        row.CheckBox = chk;

                    }
                    else if (isColumnDataCell(j))
                    {


                        // td.InnerHtml = "Data";
                        dataCells.Add(td);


                    }
                    else if (isColumnButton(j))
                    {

                        //ListingItemImageButton btnTemplate = this._itemButtons[btnCount];
                        IListingButtonInfo btnTemplate = this.Functionality._itemButtons[btnCount];
                        btnCount++;

                        {
                            string cssClass = btnTemplate.CssClass;
                            if (!string.IsNullOrEmpty(cssClass))
                                cssClass += " ";
                            cssClass += "listing_row_button";
                            btnTemplate.CssClass = cssClass;
                        }




                        IListingButtonInfo btn = null;


                        if (btnTemplate is ListingItemImageLinkButton)
                        {
                            //A link button
                            ListingItemImageLinkButton linkButton = (ListingItemImageLinkButton)btnTemplate;

                            ListingItemImageLinkButton newBtn = new ListingItemImageLinkButton(linkButton.HrefRetriever);
                            newBtn.ID = "btnRowItem_" + this.ID + "_" + i + "_" + j;
                            string Href = linkButton.HrefRetriever(obj);
                            newBtn.Href = Href;
                            newBtn.Title = linkButton.Title;
                            
                            newBtn.IconImageUrl = linkButton.IconImageUrl;
                            //newBtn.FunctionAsImage = true;
                            
                           // newBtn.RollOverImage = linkButton.RollOverImage; //\
                           // newBtn.ImageUrl = linkButton.ImageUrl; //
                            //newBtn.AlternateText = linkButton.Text; //
                            btn = newBtn;
                        }
                        else if (btnTemplate is ListingItemImageSubmitButton)
                        {
                            //A button which submits
                            ListingItemImageSubmitButton linkButton = (ListingItemImageSubmitButton)btnTemplate;

                            ListingItemImageSubmitButton newBtn = new ListingItemImageSubmitButton(linkButton.SubmitHandler);
                            newBtn.ID = "btnRowSubmit_" + this.ID + "_" + i + "_" + j;
                            newBtn.Tag = row;
                            newBtn.Title = linkButton.Title;
                            newBtn.Click += new EventHandler(ItemButton_Click);
                           // newBtn.RollOverImage = linkButton.RollOverImage; //
                           // newBtn.ImageUrl = linkButton.ImageUrl; //
                           // newBtn.AlternateText = linkButton.Text; //
                            btn = newBtn;
                        }
                        else if (btnTemplate is ListingItemTextLinkButton)
                        {
                            ListingItemTextLinkButton linkButton = (ListingItemTextLinkButton)btnTemplate;

                            ListingItemTextLinkButton newBtn = new ListingItemTextLinkButton(linkButton.HrefRetriever);

                            newBtn.ID = "btnRowTextBtn_" + this.ID + "_" + i + "_" + j;
                            //                            btn = new ListingItemTextLinkButton(linkButton.HrefRetriever);
                            //newBtn.AlternateText = linkButton.Text; //
                            //                            btn.ID = "btnRowSubmit_" + this.ID + "_" + i + "_" + j;
                            string Href = linkButton.HrefRetriever(obj);
                            newBtn.Href = Href;
                            newBtn.Title = linkButton.Title;
                            
                            newBtn.Tag = row;
                            btn = newBtn;
                            //btn.Click += new EventHandler(ItemButton_Click);
                        }
                        WebControl btnAsWebControl = (WebControl)btn;
                        btnAsWebControl.Style.Add("cursor", "pointer");

                        btnAsWebControl.CssClass = btnTemplate.CssClass;
                        //btn.HRefTarget = btnTemplate.HRefTarget; //
                        btn.NoValidation = btnTemplate.NoValidation; //
                        //btn.RollOverImage = btnTemplate.RollOverImage; //
                        btn.Text = btnTemplate.Text; //
                        btn.ValidationGroup = btnTemplate.ValidationGroup; //
                        btn.Tag = btnTemplate.Tag; //
                        //btn.ImageUrl = btnTemplate.ImageUrl; //

                        td.Controls.Add(btnAsWebControl);

                    }
                }
                
                if (this.Functionality._callItemDataBound(row))
                {
                    tBody.Controls.Add(tr);
                    tBody.Controls.Add(trExpandableRow);
                    this.Functionality._items.Add(row);
                
                }
                

                i++;


            }
        }

        private void initExpandableRows()
        {
            StringBuilder expandJS = new StringBuilder();
            atLeastOneExpandableRow = false;
            foreach (var item in this.Functionality._items)
            {
                if (item.ShowExpandableRow)
                {
                    atLeastOneExpandableRow = true;
                    break;
                }
            }
            if (atLeastOneExpandableRow)
            {
                {
                    HtmlTableCell th = new HtmlTableCell("th");
                    HtmlGenericControl col = new HtmlGenericControl("col");
                    colGroup.Controls.AddAt(0,col);
                    col.Attributes["width"] = "16";
                    trColumnHeader.Cells.Insert(0, th);
                }
                thTHead.ColSpan = _totalColumns+1;
                thTFoot.ColSpan = _totalColumns + 1;
                foreach (var item in this.Functionality._items)
                {
                    HtmlTableCell tdExpand = new HtmlTableCell();
                    item.Row.Cells.Insert(0, tdExpand);
                    tdExpand.InnerHtml = "&nbsp;";
                    if (item.ShowExpandableRow)
                    {
                        tdExpand.Controls.Clear();
                        MyDiv imgExpand = new MyDiv();
                        imgExpand.CssClass = "imgExpand";
                        tdExpand.Controls.Add(imgExpand);
                        if (item.InitExpandableRowVisible)
                        {
                            //imgExpand.ImageUrl = this.Functionality.DefaultImage_UnExpand;
                        }
                        else
                        {
                            //imgExpand.ImageUrl = this.Functionality.DefaultImage_Expand; 
                            item.ExpandableRow.Style["display"] = "none";
                        }
                        item.ExpandableRow.Visible = true;
                        if (item.ExpandableRow.Cells.Count == 1)
                        {
                            item.ExpandableRow.Cells[0].ColSpan = item.Row.Cells.Count;
                        }
                        expandJS.AppendLine("new com.cs.ui.ImageElementToggler('" + imgExpand.ClientID + "','" + item.ExpandableRow.ClientID + "'"
                            + ",'" +  this.Functionality.DefaultImage_Expand + "','" +  this.Functionality.DefaultImage_UnExpand + "');");
                    }
                }

            }
            if (expandJS.Length > 0)
            {
                string js = "com.cs.require('com.cs.ui.ImageElementToggler');\r\n";
                js += expandJS.ToString();
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "expandJS", js, true);
            }
        }
        protected virtual void initControls()
        {
            HtmlGenericControl tbl = new HtmlGenericControl("table");
            this.Controls.Add(tbl);
            tbl.Attributes["class"] = "listing";
            tbl.Attributes["cellpadding"] = "0";
            tbl.Attributes["cellspacing"] = "1";
            colGroup = new HtmlGenericControl("colgroup");
            tbl.Controls.Add(colGroup);
            tHead = new HtmlGenericControl("thead");
            tbl.Controls.Add(tHead);
            HtmlTableRow trThead = new HtmlTableRow();
            tHead.Controls.Add(trThead);
            thTHead = new HtmlTableCell("th");
            trThead.Cells.Add(thTHead);
            tBody = new HtmlGenericControl("tbody");
            tbl.Controls.Add(tBody);
            HtmlGenericControl tFoot = new HtmlGenericControl("tfoot");
            tbl.Controls.Add(tFoot);
            HtmlTableRow trFoot = new HtmlTableRow();
            tFoot.Controls.Add(trFoot);
            thTFoot = new HtmlTableCell("th");
            trFoot.Cells.Add(thTFoot);
            initJS(tbl);

        }
        private void initJS(HtmlGenericControl tbl)
        {
            string js = "new js.com.cms." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION + ".Listing('" + tbl.ClientID + "');";

            this.Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID + "_listing", js, true);
        }

        #endregion
        #region Properties
        private HtmlTableRow trColumnHeader = null;
        private bool atLeastOneExpandableRow { get; set; }
        
       
        


        //The amount of DATA columns in the listing
        public int Columns
        {
            get
            {
                return this.Functionality._columns.Count;
            }
        }

        protected HtmlGenericControl colGroup;
        protected HtmlGenericControl tHead;
        protected HtmlTableCell thTHead;
        protected HtmlGenericControl tBody;
        protected HtmlTableCell thTFoot;
        
       
        

        private System.Web.HttpRequest Request
        {
            get
            {

                return System.Web.HttpContext.Current.Request;
            }
        }
        private System.Web.SessionState.HttpSessionState Session
        {
            get
            {
                return System.Web.HttpContext.Current.Session;
            }
        }

        private int _totalColumns
        {
            get
            {
                int cols = 0;
                if (this.Functionality._generalButtons.Count > 0)
                {
                    //This means that the checkbox column is shown
                    cols++;
                }
                cols += this.Functionality._columns.Count;
                cols += this.Functionality._itemButtons.Count;
                return cols;
            }
        }
        #endregion
        public Listing()
        {
            this.Functionality = createFunctionality();


        }
        



    }
}
