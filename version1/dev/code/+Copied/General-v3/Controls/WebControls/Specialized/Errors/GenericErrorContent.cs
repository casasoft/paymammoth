﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using System.Web.UI.WebControls;
namespace CS.General_v3.Controls.WebControls.Specialized.Errors
{
    public class GenericErrorContent : BaseWebControl
    {
        public class PropertiesClass
        {
            public string Url { get; set; }
            public string ContactUrl { get; set; }
        }
        public PropertiesClass Properties { get; set; }
        public GenericErrorContent()
        {
            Properties = new PropertiesClass();

        }
        public GenericErrorContent(string errorURL, string contactURL)
        {
            Properties = new PropertiesClass();
            Properties.Url = errorURL;
            Properties.ContactUrl = contactURL;
        }
        private void initContent()
        {
            
            string content = "<p>An error has been encountered while servicing your request.  We apologise for this inconvenience.</p>";
            content += "<p>Suggestions:</p>";
            content += "<ul><li><a href='"+Properties.Url+"'>Retry</a> later, in a few minutes.</li>";
            content += "<li><a href='"+Properties.ContactUrl+"'>Contact us</a> should the problem persists.</li>";
            content += "</ul>";

            Controls.Add(new Literal() { Text = content });

        }
        protected override void OnLoad(EventArgs e)
        {
            if (String.IsNullOrEmpty(Properties.Url))
            {
                Properties.Url = CS.General_v3.Util.PageUtil.GetVariableFromQuerystring("aspxerrorpath");
            }
            initContent();
            base.OnLoad(e);
        }
    }
}
