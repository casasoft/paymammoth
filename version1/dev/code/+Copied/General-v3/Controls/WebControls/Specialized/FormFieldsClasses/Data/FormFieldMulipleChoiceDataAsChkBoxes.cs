﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common.General;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data
{
    public class FormFieldMulipleChoiceDataAsChkBoxes : FormFieldListBaseTypeData<List<string>>
    {
        public FormFieldMulipleChoiceDataAsChkBoxes()
        {

            this.RowCssClassBase = "form-row-checkbox-list";
            this.TotalColumns = 3;
            this.RepeatDirection = System.Web.UI.WebControls.RepeatDirection.Horizontal;
        }

        public int TotalColumns { get; set; }
        public RepeatDirection RepeatDirection { get; set; }
        public new MyCheckBoxList GetField()
        {
            return (MyCheckBoxList)base.GetField();
        }
        protected override IMyFormWebControl createFieldControl()
        {
            MyCheckBoxList _chkList = new MyCheckBoxList();
            _chkList.RepeatColumns = this.TotalColumns;
            _chkList.RepeatDirection = this.RepeatDirection;
            _chkList.WebControlFunctionality.AddFieldJS = false;
            foreach (ListItem item in this.ListItems)
            {
                ListItem li = new ListItem(item.Text, item.Value);
                li.Selected = item.Selected;
                _chkList.Items.Add(li);

            }

            return _chkList;
        }
        public override List<string> GetFormValue()
        {
            return this.GetField().FormValue;
            
        }
        
    }
}
