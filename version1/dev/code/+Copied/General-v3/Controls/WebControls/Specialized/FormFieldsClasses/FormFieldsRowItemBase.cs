﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data;
using CS.General_v3.Controls.WebControls.Common.General;
using System.Web.UI;
using CS.General_v3.Classes.CSS;
using CS.General_v3.Controls.WebControls.Specialized.Tooltip;
using CS.General_v3.JavaScript.jQuery.Plugins.Tooltips.QTip;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses
{
    public class FormFieldsRowItemBase : MyTableRow
    {
        public class FUNCTIONALITY
        {
            public MyTableCell TdLabel { get; internal set; }
            public MyTableCell TdHelpIcon { get; internal set; }
            public MyTableCell TdField { get; internal set; }
            public MySpan SpanSubtitle { get; internal set; }
            public MyTableCell TdValidationIcon { get; internal set; }
            public MySpan ElemValidationIcon { get; internal set; }
            public MyLabel Label { get; internal set; }
            public ControlTooltipWithHtml HelpMessageTooltip { get; internal set; }

            private FormFieldsRowItemBase _control;
            public FUNCTIONALITY(FormFieldsRowItemBase control)
            {
                _control = control;
                
            }
        }

        protected List<FormFields.FormFieldsColumnData> _columnLayout;

        protected FormFields _formFields;
        protected string _label;
        protected string _labelSubtitle;
        protected Control _fieldControl;
        protected string _helpMessage;
        protected _jQueryQTipOptions _helpMessageQTipOptions;
        protected bool _showValidationIcon;

        public FUNCTIONALITY Functionality { get; private set; }

        public FormFieldsRowItemBase(

            List<FormFields.FormFieldsColumnData> ColumnLayout, 
            FormFields formFields,
            string label,
            string labelSubtitle,
            Control field,
            string helpMessage,
            bool showValidationIcon, 
            _jQueryQTipOptions helpMessageQTipOptions

            )
        {
            this.Functionality = getFunctionality();
            
            this.CssManager.AddClass(formFields.Functionality.CssClassTrRowItem);
            
            _showValidationIcon = showValidationIcon;
            _label = label;
            _labelSubtitle = labelSubtitle;
            _fieldControl = field;
            _helpMessage = helpMessage;
            _helpMessageQTipOptions = helpMessageQTipOptions;
            
            _columnLayout = ColumnLayout;
            _formFields = formFields;

            
        }
        public void TempInit()
        {
            init();
        }
        protected virtual FUNCTIONALITY getFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        
        private void initColumns()
        {
            for (int i = 0; i < _columnLayout.Count; i++)
            {
                FormFields.FormFieldsColumnData col = _columnLayout[i];
                switch (col.ColumnType)
                {
                    case FormFields.FIELDS_COL_TYPE.Label: addLabel(col); break;
                    case FormFields.FIELDS_COL_TYPE.Field: addField(col); break;
                    case FormFields.FIELDS_COL_TYPE.HelpIcon: addHelpIcon(col); break;
                    case FormFields.FIELDS_COL_TYPE.ValidationIcon: addValidationIcon(col); break;
                    case FormFields.FIELDS_COL_TYPE.Empty: addEmptyCol(col); break;
                }
            }
        }

        
        private MyTableCell addCell(FormFields.FormFieldsColumnData colData)
        {
            MyTableCell td = new MyTableCell();
            if (!string.IsNullOrEmpty(colData.CssClass))
            {
                td.CssManager.AddClass(colData.CssClass);
            }
            td.ColumnSpan = colData.ColSpan;
            this.Cells.Add(td);
            return td;

        }
        private void addEmptyCol(FormFields.FormFieldsColumnData col)
        {
            MyTableCell td = addCell(col);

        }
        
        private void addLabel(FormFields.FormFieldsColumnData colData)
        {
            this.Functionality.TdLabel = addCell(colData);
            this.Functionality.TdLabel.CssManager.AddClass(_formFields.Functionality.CssClassTdLabel);

            MyLabel lblTitle = new MyLabel();
            this.Functionality.Label = lblTitle;
            lblTitle.ForControl = _fieldControl;
            lblTitle.InnerHtml = _label;
           // lblTitle.InnerHtml += _formFields.Functionality.LabelSuffix;

            this.Functionality.TdLabel.Controls.Add(lblTitle);

            if (!string.IsNullOrEmpty(_labelSubtitle))
            {
                MySpan spanSubtitle = new MySpan();
                spanSubtitle.CssClass = _formFields.Functionality.SubtitleCssClass;
                spanSubtitle.InnerHtml = _labelSubtitle;
                this.Functionality.SpanSubtitle = spanSubtitle;
                this.Functionality.TdLabel.Controls.Add(spanSubtitle);
            }
        }
        private void addHelpIcon(FormFields.FormFieldsColumnData colData)
        {
            this.Functionality.TdHelpIcon = addCell(colData);
            this.Functionality.TdHelpIcon.CssManager.AddClass(_formFields.Functionality.CssClassTdHelpIcon);
            if (!string.IsNullOrEmpty(_helpMessage))
            {
                if (_helpMessageQTipOptions != null)
                {
                    _helpMessageQTipOptions.content = new JavaScript.jQuery.Plugins.Tooltips.QTip._jQueryQTipOptionsContent();
                    _helpMessageQTipOptions.content.text = this._helpMessage;
                    MyDiv divIcon = new MyDiv();
                    divIcon.CssManager.AddClass("tooltip-icon-control");
                    divIcon.ToolTip = new JavaScript.jQuery.Plugins.Tooltips.QTip.jQueryQTip();
                    divIcon.ToolTip.Options = _helpMessageQTipOptions;
                    this.Functionality.TdHelpIcon.Controls.Add(divIcon);
                    
                }
                else
                {
                    ControlTooltipWithHtml tooltip = new ControlTooltipWithHtml();
                    tooltip.Functionality.TooltipText = _helpMessage;
                    this.Functionality.HelpMessageTooltip = tooltip;
                    this.Functionality.TdHelpIcon.Controls.Add(tooltip);
                }
            }
        }
        private void addField(FormFields.FormFieldsColumnData colData)
        {
            this.Functionality.TdField = addCell(colData);
            this.Functionality.TdField.CssManager.AddClass(_formFields.Functionality.CssClassTdField);
            if (_fieldControl != null)
            {
                this.Functionality.TdField.Controls.Add(_fieldControl);
            }

        }
        private void addValidationIcon(FormFields.FormFieldsColumnData colData)
        {
            this.Functionality.TdValidationIcon = addCell(colData);
            this.Functionality.TdValidationIcon.CssManager.AddClass(_formFields.Functionality.CssClassTdValidationIcon);
            //Validation Icon is then added by JS
            /*if (_showValidationIcon)
            {
                MySpan spanValidationIcon = new MySpan();
                TdValidationIcon.Controls.Add(spanValidationIcon);
                ElemValidationIcon = spanValidationIcon;
                
            }*/
        }
        /// <summary>
        /// You must manually call this method or override it
        /// </summary>
        protected virtual void init()
        {
            initColumns();
        }

        protected override void OnLoad(EventArgs e)
        {
//            init();
            base.OnLoad(e);
        }

        public virtual void OnRendered()
        {

        }




    }
}
