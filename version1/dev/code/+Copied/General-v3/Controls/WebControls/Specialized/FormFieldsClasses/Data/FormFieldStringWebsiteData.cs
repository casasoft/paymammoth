﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data
{
    public class FormFieldStringWebsiteData : FormFieldStringBaseData
    {

        public FormFieldStringWebsiteData()
        {
            _validationParams.isWebsite = true;
            this.RowCssClassBase = "form-row-website";
        }

    }
}
