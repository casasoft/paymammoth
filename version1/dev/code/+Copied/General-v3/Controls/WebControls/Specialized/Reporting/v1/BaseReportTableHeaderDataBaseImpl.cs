﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.WebControls.Specialized.Reporting.v1
{
    public class BaseReportTableHeaderDataBaseImpl : IBaseReportTableHeaderDataBase
    {
        #region IBaseReportTableHeaderData Members
        public BaseReportTableHeaderDataBaseImpl()
        {
            this.HeaderCellContent = new BaseReportTableCellDataImpl();
        }

        public BaseReportTableCellDataImpl HeaderCellContent { get; set; }

        IBaseReportTableCellData IBaseReportTableHeaderDataBase.HeaderCellContent
        {
            get { return this.HeaderCellContent; }
        }

        

        #endregion
    }


}
