﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.WebControls.Specialized.Reporting.v1
{
    public class BaseReportTableHeaderDataImpl :BaseReportTableHeaderDataBaseImpl,  IBaseReportTableHeaderData
    {
        public BaseReportTableHeaderDataImpl()
        {
            this.TotalCellContent = new BaseReportTableCellDataImpl();
        }

        #region IBaseReportTableHeaderData Members

        public BaseReportTableCellDataImpl TotalCellContent
        {
            get;
            set;
        }

        #endregion

        #region IBaseReportTableHeaderData Members

        IBaseReportTableCellData IBaseReportTableHeaderData.TotalCellContent
        {
            get { return this.TotalCellContent; }
        }

        #endregion

    }


}
