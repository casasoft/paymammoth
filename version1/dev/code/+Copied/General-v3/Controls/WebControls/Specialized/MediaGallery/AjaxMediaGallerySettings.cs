﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.JavaScript.jQuery.Plugins.Uploadify;
using System.Collections.Specialized;
using CS.General_v3.JavaScript.Interfaces;
using CS.General_v3.Util;

namespace CS.General_v3.Controls.WebControls.Specialized.MediaGallery
{
    public class AjaxMediaGallerySettings : IJavaScriptObject
    {
        public List<AjaxMediaGalleryItemSectionData> sections = new List<AjaxMediaGalleryItemSectionData>();

        /*settings.deleteScript = "/ajax/tmp/deleteTempImage.ashx";
            settings.reorderScript = "/ajax/tmp/reorderImages.ashx";
            settings.cropScript = "/ajax/tmp/cropTempImage.ashx";
            settings.captionScript = "/ajax/tmp/updateCaption.ashx";*/
        /// <summary>
        /// The handler URL which deletes the thumbnail.  This script will receive one parameter 'id'.
        /// The exact name of the parameter is specified using the 'deleteScriptIdParam'
        /// 
        /// This handler URL expects to return one variable output in JSON format which is 'success' and the value must be 'true'.
        /// The parameter return name can be changed from 'deleteScriptResponseSuccessParam' while the paramter return value may be changed from 'deleteScriptResponseSuccessValue'
        /// </summary>
        public string deleteScript = "/ajax/deleteTempImage.ashx";
        /// <summary>
        /// The handler URL which crops the image.  This script is called multiple times for every cropping procedure
        /// for every cropping size required (small, medium, large...) This script will receive 6 parameters which are:
        /// 
        /// - id - (Can be changed from cropScriptParamId)
        /// - cropType - (Can be changed from cropScriptParamId) - This is the type of cropping being performed, e.g. 'small', or 'medium'
        /// - x - (Can be changed from cropScriptParamCropTypeId)
        /// - y - (Can be changed from cropScriptParamTop)
        /// - x2 - (Can be changed from cropScriptParamRight)
        /// - y2 - (Can be changed from cropScriptParamBottom)
        /// 
        /// The handler must return the following parameters in JSON format:
        /// - success (Can be changed from cropScriptResponseSuccessParam) and must have value 'true' as specified in reorderScriptResponseSuccessValue
        /// 
        /// If you want to change the crop error message, set the 'cropScriptErrorMessage'
        /// </summary>
        public string cropScript = "/ajax/cropTempImage.ashx";
        /// <summary>
        /// The handler URL which sets the reordering of items.  
        /// 
        /// This script will receive 1 parameter which is:
        /// 
        /// - order - (Can be changed from reorderScriptOrderParam) - This will have the list of IDs of items in the required order
        /// - sectionID -  (Can be changed from reorderScriptSectionIDParam)
        /// 
        /// The handler must return the following parameters in JSON format:
        /// - success (Can be changed from reorderScriptResponseSuccessParam) and must have value 'true' as specified in reorderScriptResponseSuccessValue
        /// 
        /// If you want to change the error message, set the 'reorderItemsErrorMessage'
        /// 
        /// IMPORTANT: Best is to make a handler which will extend the base handler AjaxMediaGalleryUploadBaseHandler
        /// </summary>
        public string reorderScript = "/ajax/reorderImages.ashx";

        public UploadifySettings uploadifySettings = new UploadifySettings();
        public string uploadScriptSectionIdParam = "sectionID";
        public string uploadScriptResponseUploadedMediaItemParam = "item";
        public string uploadScriptProblemsUploadingWithAlternateUpload = "If you are having problems uploading using our Flash Uploader, <a href='javascript:'>switch to the basic file uploader</a>.";
        public string uploadScriptProblemsUploadingWithoutAlternateUpload = "If you are having problems uploading using our Flash Uploader, please contact the administrators.";
        /// <summary>
        /// Specify a URL which explains in more depth how to use the advanced file uploader
        /// </summary>
        public string uploadScriptHelpURL = null;
        public string uploadScriptHelpURLText = "Visit the <a href='[URL]' target='_blank'>uploader help & tips section</a> for more information on using the advanced Flash Uploader.";


        public string deleteScriptIdParam = "id";
        public NameValueCollection deleteScriptExtraParams = new NameValueCollection();
        public string deleteScriptResponseIdParam = "id";
        public string deleteScriptResponseSuccessParam = "success";
        public string deleteScriptResponseSuccessValue = "true";


        public string cropScriptParamId = "id";
        public string cropScriptParamCropTypeId = "cropType";
        public string cropScriptResponseSuccessParam = "success";
        public string cropScriptResponseSuccessValue = "true";
        public string cropScriptParamLeft = "x";
        public string cropScriptParamTop = "y";
        public string cropScriptParamBottom = "y2";
        public string cropScriptParamRight = "x2";
        public string cropScriptErrorMessage = "An error has been encountered whilst cropping image.  Please try again.";
        public NameValueCollection cropScriptExtraParams = new NameValueCollection();

        public string reorderScriptOrderParam = "order";
        public string reorderScriptSectionIDParam = "sectionID";
        public NameValueCollection reorderScriptExtraParams = new NameValueCollection();
        public string reorderScriptResponseSuccessParam = "success";
        public string reorderScriptResponseSuccessValue = "true";
        public string reorderItemsErrorMessage = "An error has been encountered while trying to reorder images.  Please try again.";

        public string confirmDeleteMessage = "Are you sure you want to delete the selected item?";
        public string deleteItemErrorMessage = "An error has been encountered while trying to delete image.  Please try again.";

        /// <summary>
        /// The handler URL which updates the caption of the items  
        /// 
        /// This script will receive 1 parameter which is:
        /// 
        /// - id - (Can be changed from captionScriptParamId) - The ID of the item being edited
        /// - value - (Can be changed from captionScriptParamValue) - The new value of the caption
        /// 
        /// If you want to change the error message, set the 'captionOverTooltip'
        /// </summary>
        public string captionScript = "/ajax/updateCaption.ashx";
        public string captionScriptParamId = "id";
        public string captionScriptParamValue = "value";
        public string captionOverTooltip = "Click on caption to edit caption.  Press ENTER to save changes.  Press ESC to cancel changes";
        public string captionUpdateIndicator = "Updating Caption...";

        public string noItemsAvailableText = "No images available.";


        /*(
        public string deleteScript;
        public string cropScript;
        public string reorderScript;
        public _UploadifySettings uploadifySettings;
        public string uploadScriptSectionIdParam = "sectionID";
        public string uploadScriptResponseUploadedMediaItemParam = "item";




        public string deleteScriptIdParam = "id";
        public Dictionary deleteScriptExtraParams = new Dictionary();
        public string deleteScriptResponseIdParam = "id";
        public string deleteScriptResponseSuccessParam = "success";
        public string deleteScriptResponseSuccessValue = "true";



        public string cropScriptParamId = "id";
        public string cropScriptParamCropTypeId = "cropType";
        public string cropScriptResponseSuccessParam = "success";
        public string cropScriptResponseSuccessValue = "true";
        public string cropScriptParamLeft = "x";
        public string cropScriptParamTop = "y";
        public string cropScriptParamBottom = "y2";
        public string cropScriptParamRight = "x2";
        public string cropScriptErrorMessage = "An error has been encountered whilst cropping image.  Please try again.";
        public Dictionary cropScriptExtraParams = new Dictionary();

        public string reorderScriptOrderParam = "order";
        public Dictionary reorderScriptExtraParams = new Dictionary();
        public string reorderScriptResponseSuccessParam = "success";
        public string reorderScriptResponseSuccessValue = "true";
        public string reorderItemsErrorMessage = "An error has been encountered while trying to reorder images.  Please try again.";

        public string confirmDeleteMessage = "Are you sure you want to delete the selected item?";
        public string deleteItemErrorMessage = "An error has been encountered while trying to delete image.  Please try again.";

        public string captionScript;
        public string captionScriptParamValue = "value";
        public string captionOverTooltip = "Click on caption to edit caption.  Press ENTER to save changes.  Press ESC to cancel changes";*/

        #region IJavaScriptObject Members

        public IJavaScriptSerializable GetJsObject(bool forFlash = false)
        {
            return JSUtilOld.GetJSObjectFromTypeWithReflection(this, forFlash);
        }

        #endregion
    }
}
