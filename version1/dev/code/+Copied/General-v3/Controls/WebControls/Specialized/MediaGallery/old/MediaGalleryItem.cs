﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Controls.Classes.MediaGallery;
using CS.General_v3.Classes.MediaItems;
namespace CS.General_v3.Controls.WebControls.Specialized.MediaGallery
{
    internal class MediaGalleryItem
    {
        public IMediaItem Item { get; set; }
        public MyCheckBox Checkbox { get; set; }
        public MyImage ImgThumbnail { get; set; }
        public MyTxtBoxTextSingleLine TxtCaption { get; set; }
        public MyTxtBoxNumericInteger TxtPriority { get; set; }
        public MyDropDownList CmbExtraValueChoice { get; set; }
        public MediaGalleryItem(IMediaItem item, MyCheckBox chk, MyImage imgThumbnail, MyTxtBoxTextSingleLine txtCaption)
        {
            this.Item = item;
            this.Checkbox = chk;
            this.ImgThumbnail = imgThumbnail;
            this.TxtCaption = txtCaption;
        }
    }
}
