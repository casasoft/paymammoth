﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace CS.General_20101215.Controls.WebControls.Specialized.MediaGallery.v2
{
    public abstract class AjaxMediaGalleryReorderBaseHandler : CS.General_20101215.HTTPHandlers.BaseAjaxHandler
    {
        /// <summary>
        /// The parameter where to load the list of IDs in order, IDs are seperated by a comma
        /// </summary>
        protected string _paramOrder = "order";
        protected string _paramSectionID = "sectionID";
        protected string _paramSuccess = "success";

        public override void ProcessRequest(System.Web.HttpContext context)
        {
            string orderIDs = context.Request.QueryString[_paramOrder];
            string sectionID = context.Request.QueryString[_paramSectionID];
            string[] listOrderIDs = orderIDs.Split(',');

            bool ok = ReorderItems(context, listOrderIDs, sectionID);
            this.AddProperty(_paramSuccess, ok);
            

            base.ProcessRequest(context);
        }

        public abstract bool ReorderItems(HttpContext context, string[] itemIDsWithOrder, string sectionID);
    }
}
