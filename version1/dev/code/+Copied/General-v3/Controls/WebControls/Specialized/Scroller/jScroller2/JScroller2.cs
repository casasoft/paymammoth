﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using System.Web.UI;

namespace CS.General_v3.Controls.WebControls.Specialized.Scroller.jScroller2
{
    public class JScroller2 : MyDiv
    {
        public enum JSCROLLER_DIRECTION {
            Left,
            Top,
            Right,
            Bottom
        }
        public class FUNCTIONALITY
        {
            private JScroller2 _control;

            public Control Control { get; set; }
            /// <summary>
            /// Copy of the same control (if not, it will take HTML of initial Control)
            /// </summary>
            public Control ControlEndlessScrollingClone { get; set; }

            public JSCROLLER_DIRECTION Direction { get; set; }
            /// <summary>
            /// 0 - 1000
            /// </summary>
            public int Speed { get; set; }

            public int? DelaySec { get; set; }
            public bool AutoStopOnMouseEnterAndLeave { get; set; }
            public bool TurnOffOWindowLeaveEvents { get; set; }
            public bool DynamicContent { get; set; }
            public bool EndlessScrolling { get; set; }

            public int? RefreshTimeMS { get; set; }

            public string CssClass { get; set; }
            public FUNCTIONALITY(JScroller2 control)
            {
                _control = control;
                this.AutoStopOnMouseEnterAndLeave = true;
                this.TurnOffOWindowLeaveEvents = false;
                this.Speed = 50;
                this.CssClass = "cs-jscroller";
            }
        }

        public FUNCTIONALITY Functionality { get; private set; }
        /// <summary>
        /// To use this scroller class, you need to set within Functionality:
        /// - Control: The control to scroll
        /// - CssClass: The css class given to this scroller
        /// 
        /// In CSS You must:
        /// - Provide dimensions for this container to the css class selector, such as
        ///  width: 980px; height: 42px; overflow: hidden;
        /// - Make sure that the inner content has got whitespace:nowrap.  Inner content CSS class is '[css_class]-content'
        /// </summary>
        public JScroller2()
        {
            this.Functionality = getFunctionality();
        }
        private string getDirectionCssClass()
        {
            switch(this.Functionality.Direction)
            {
                case JSCROLLER_DIRECTION.Left: return "jscroller2_left";
                case JSCROLLER_DIRECTION.Right: return "jscroller2_right";
                case JSCROLLER_DIRECTION.Top: return "jscroller2_top";
                case JSCROLLER_DIRECTION.Bottom: return "jscroller2_bottom";
            }
            return null;
        }

        private void initControls()
        {
            this.CssManager.AddClass(this.Functionality.CssClass);
            MyDiv divContent = new MyDiv();
            this.Controls.Add(divContent);
            divContent.CssManager.AddClass(this.Functionality.CssClass + "-content");
            divContent.CssManager.AddClass("jscroller2_speed-" + this.Functionality.Speed);
            if (this.Functionality.DelaySec.HasValue)
            {
                divContent.CssManager.AddClass("jscroller2_delay-" + this.Functionality.DelaySec.Value);
            }
            if (this.Functionality.AutoStopOnMouseEnterAndLeave)
            {
                divContent.CssManager.AddClass("jscroller2_mousemove");
            }
            if (this.Functionality.TurnOffOWindowLeaveEvents)
            {
                divContent.CssManager.AddClass("jscroller2_ignoreleave");
            }
            divContent.Controls.Add(this.Functionality.Control);
            divContent.CssManager.AddClass(getDirectionCssClass());
            initEndlessScrolling(divContent);
            initRefreshTime();
        }
        private void initEndlessScrolling(MyDiv divContent)
        {
            if (this.Functionality.EndlessScrolling)
            {
                
                MyDiv divContent2 = new MyDiv();
                

                divContent2.CssManager.AddClass(getDirectionCssClass() + "_endless");
                if (this.Functionality.ControlEndlessScrollingClone == null)
                {
                    string htmlCopy = CS.General_v3.Util.ControlUtil.RenderInnerContents(divContent);
                    divContent2.InnerHtml = htmlCopy;

                }
                else
                {
                    divContent2.Controls.Add(this.Functionality.ControlEndlessScrollingClone);
                }
                this.Controls.Add(divContent2);
            }
        }

        private void initRefreshTime()
        {
            if (this.Functionality.RefreshTimeMS.HasValue)
            {
                string js = "ByRei_jScroller2.config.refreshtime = "+this.Functionality.RefreshTimeMS+";";
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "jscrollerrefreshtime", js, true);
            }
        }

        protected virtual FUNCTIONALITY getFunctionality()
        {
            return new FUNCTIONALITY(this);
        }



        protected override void OnLoad(EventArgs e)
        {
            initControls();
            base.OnLoad(e);
        }

    }
}
