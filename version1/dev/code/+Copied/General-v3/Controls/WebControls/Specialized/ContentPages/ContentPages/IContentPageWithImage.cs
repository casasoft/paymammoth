﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_20101215.JavaScript.Data;
using CS.General_20101215.Controls.WebControls.Specialized.Hierarchy;
namespace CS.General_20101215.Controls.WebControls.Specialized.ContentPages.ContentPages
{
    public interface IContentPageWithImage : IContentPage
    {
        string GetThumbnailImageUrl();
        string GetNormalImageUrl();
        new IList<IContentPageWithImage> Children { get;  }
    }
        
}
