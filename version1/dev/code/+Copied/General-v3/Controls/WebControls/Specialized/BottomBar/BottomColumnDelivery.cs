﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;
using CS.General_v3.Controls.WebControls.Common;

namespace CS.General_v3.Controls.WebControls.Specialized.BottomBar
{
    public class BottomColumnDelivery : BottomColumn
    {

        public new class FUNCTIONALITY : BottomColumn.FUNCTIONALITY
        {

            public MyDiv WrapperDIV;
            public string HTMLText { get; set; }

            public FUNCTIONALITY(BottomColumnDelivery col)
                : base(col)
            {
                
            }

            private void generateHtmlText()
            {
                MyDiv div = new MyDiv();
                div.InnerHtml = HTMLText;
                WrapperDIV.Controls.Add(div);
            }

            private void initHeading()
            {
                HtmlGenericControl titleSpan = new HtmlGenericControl("span");
                titleSpan.Attributes["class"] = "title";
                titleSpan.InnerText = Title;
                WrapperDIV.Controls.Add(titleSpan);
            }


            private void generateWrapper()
            {
                WrapperDIV = new MyDiv();
                WrapperDIV.Attributes["class"] = "delivery-wrapper";
                PlaceHolder.Controls.Add(WrapperDIV);
            }

            internal override void Init()
            {
                generateWrapper();
                initHeading();
                generateHtmlText();
                base.Init();
            }

        }
        public new FUNCTIONALITY Functionality
        {
            get { return (FUNCTIONALITY)base.Functionality; }
            set
            {
                base.Functionality = value;
            }
        }

        public BottomColumnDelivery()
        {
            Functionality = new FUNCTIONALITY(this);
        }

    }
}
