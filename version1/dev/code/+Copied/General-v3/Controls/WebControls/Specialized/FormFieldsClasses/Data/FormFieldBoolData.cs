﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Controls.WebControls.Common.General;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data
{
    public class FormFieldBoolData : FormFieldBaseTypeData<bool?>
    {

        
        public FormFieldBoolData()
        {
            this.RowCssClassBase = "form-row-checkbox";
        }
        public new MyCheckBox GetField()
        {
            return (MyCheckBox)(IMyFormWebControl)base.GetField();
        }
        protected override IMyFormWebControl createFieldControl()
        {
            
                MyCheckBox _chkBox = new MyCheckBox();
                if (InitialValue != null) {
                    _chkBox.Checked = InitialValue.Value;
                }
            
            return _chkBox;

        }
        
        public override bool? GetFormValue()
        {
            return this.GetField().FormValue;
            
        }
        
    }
}
