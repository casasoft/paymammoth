﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using CS.General_20090518.Controls.WebControls.Common;
using CS.General_20090518.Controls.Classes.MediaGallery;
using CS.General_20090518;
namespace CS.General_20090518.Controls.WebControls.Specialized.MenuTreeStructure
{
    [ToolboxData("<{0}:MenuTreeStructure runat=server></{0}:MenuTreeStructure>")]    
    public class MenuTreeStructure : CS.General_20090518.Controls.WebControls.Common.BaseWebControl
    {
        

        public MenuTreeStructure()
        {
            

        }
        #region Constants / Statics
        

        #endregion
        #region Delegates
        public delegate void ShowMessageEvent(string msg);
        #endregion
        #region Events
        public event ShowMessageEvent ShowMessage;

        #endregion
        #region Methods
        private void showMessage(string msg)
        {
            if (ShowMessage != null)
                ShowMessage(msg);
        }

        #endregion
        #region Properties
        public IMenuFolder RootFolder { get; set; }
        public string CSSFile { get; set; }
        public int PaddingPerLevel { get; set; }
        public string ImageFolder { get; set; }
        public string EditFolderPageName {get;set;}
        public string EditItemPageName {get;set;}
        public string EditFolderPage_FolderIDParam {get;set;}
        public string EditItemPage_FolderIDParam {get;set;}
        public string EditItemPage_ItemIDParam {get;set;}
        public string ItemTitle {get;set;}
        public string FolderTitle { get; set; }

        #endregion
        

        protected override void OnInit(EventArgs e)
        {

            
            base.OnInit(e);
        }
        private void renderCSSFile()
        {
            HtmlGenericControl css = new HtmlGenericControl("link");
            this.Controls.Add(css);
            css.Attributes["rel"] = "stylesheet";
            css.Attributes["type"] = "text/css";
            css.Attributes["href"] = this.CSSFile;

        }

        private HtmlTable tbTree = null;
        private int ctrlIndex = 0;
        private string getImageUrl(string filename)
        {
            return this.ImageFolder + filename;
        }
        private HtmlTable createButtonTable(IMenuItem item)
        {
            bool isFolder = (item is IMenuFolder);
            IMenuFolder folder = null;
            if (isFolder)
                folder = (IMenuFolder)item;

            HtmlTable tbButtons = new HtmlTable(); tbButtons.Attributes["class"] = "buttons";
            HtmlTableRow trButtons = new HtmlTableRow(); tbButtons.Rows.Add(trButtons); trButtons.Attributes["class"] = "buttons";
            



            {
                HtmlTableCell tdButton = new HtmlTableCell(); trButtons.Cells.Add(tdButton); 
                tdButton.Attributes["class"] = "button"; tdButton.InnerHtml = "&nbsp;";
                if (isFolder && folder.AllowAddFolders)
                {
                    tdButton.Controls.Clear();
                    MyImage imgCreateFolder = new MyImage(); tdButton.Controls.Add(imgCreateFolder);
                    imgCreateFolder.ImageUrl = getImageUrl("add_folder_up.gif");
                    imgCreateFolder.RollOverImage= getImageUrl("add_folder_over.gif");
                    imgCreateFolder.HRef = EditFolderPageName + "?" + EditItemPage_FolderIDParam + "=" + folder.ID;
                }
            }
            {
                HtmlTableCell tdButton = new HtmlTableCell(); trButtons.Cells.Add(tdButton); 
                tdButton.Attributes["class"] = "button"; tdButton.InnerHtml = "&nbsp;";
                if (isFolder &&folder.AllowAddItems)
                {
                    tdButton.Controls.Clear();
                    MyImage imgCreateItem = new MyImage(); tdButton.Controls.Add(imgCreateItem);
                    imgCreateItem.ImageUrl = getImageUrl("add_item_up.gif");
                    imgCreateItem.RollOverImage= getImageUrl("add_item_over.gif");
                    imgCreateItem.HRef = EditItemPageName + "?" + EditItemPage_FolderIDParam + "=" + folder.ID;
                }
            }
            {
                HtmlTableCell tdButton = new HtmlTableCell(); trButtons.Cells.Add(tdButton); 
                tdButton.Attributes["class"] = "button"; tdButton.InnerHtml = "&nbsp;";
                
                tdButton.Controls.Clear();
                MyImageButton btnDelete = new MyImageButton(); tdButton.Controls.Add(btnDelete);
                
                btnDelete.ImageUrl = getImageUrl("delete_up.gif");
                btnDelete.RollOverImage= getImageUrl("delete_over.gif");
                btnDelete.ID = "btnDelete_" + ctrlIndex;
                btnDelete.Tag = item;
                btnDelete.Click +=new EventHandler(btnDelete_Click);
                
                
            }
            return tbButtons;
        }

        void btnDelete_Click(object sender, EventArgs e)
        {
            MyImageButton btn = (MyImageButton)sender;
            IMenuItem item = (IMenuItem)btn.Tag;
            item.Remove();
            if (item is IMenuItem)
                showMessage(ItemTitle +" deleted successfully");
            else
                showMessage(FolderTitle+ " deleted successfully");
            CS.General_20090518.Page.URL.TransferToSamePage();
        }
        private void renderItem(IMenuItem item, int level)
        {
            HtmlTableRow tr = new HtmlTableRow();
            tbTree.Rows.Add(tr);
            tr.Attributes["class"] = "tree";
            HtmlTableCell td;
            td = new HtmlTableCell(); tr.Cells.Add(td); td.Attributes["class"] = "tree icon";

            int paddingLeft = level * PaddingPerLevel;

            td.Style["padding-left"] = paddingLeft + "px";
            MyImage img = new MyImage(); img.ID = "imgIcon_" + ctrlIndex;

            if (item is IMenuFolder)
            {
                if (item.Visible)
                    img.ImageUrl = getImageUrl("folder.gif");
                else
                    img.ImageUrl = getImageUrl("folder_dim.gif");
            }
            else
            {
                if (item.Visible)
                    img.ImageUrl = getImageUrl("item.gif");
                else
                    img.ImageUrl = getImageUrl("item_dim.gif");

            }
            img.AlternateText = item.Title;

            td = new HtmlTableCell(); tr.Cells.Add(td); td.Attributes["class"] = "tree name";
            td.InnerText = item.Title;

            td = new HtmlTableCell(); tr.Cells.Add(td); td.Attributes["class"] = "tree buttons";

            td.Controls.Add(createButtonTable(item));
            if (item is IMenuFolder)
            {
                IMenuFolder folder = (IMenuFolder)item;
                List<IMenuFolder> subFolders = folder.GetSubFolders();
                foreach (var f in subFolders)
                {
                    renderItem(f, level + 1);
                }

                List<IMenuItem> subItems = folder.GetSubItems();
                foreach (var subItem in subItems)
                {
                    renderItem(subItem, level + 1);
                }

            }

        }
        
        private void renderTree()
        {
            tbTree = new HtmlTable(); this.Controls.Add(tbTree); tbTree.ID = "tbTree_" + this.ID;
            tbTree.Attributes["class"] = "tree";
            renderItem(this.RootFolder, 0);

            
        }
        
        protected override void OnLoad(EventArgs e)
        {
            
            
            base.OnLoad(e);
        }

        


 
    }
}
