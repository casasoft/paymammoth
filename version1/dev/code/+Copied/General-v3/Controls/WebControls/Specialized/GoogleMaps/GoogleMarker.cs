﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace CS.General_v3.Controls.WebControls.Specialized.GoogleMaps
{
    public class GoogleMarker
    {
        private string _textBoxStoreLocationID;
        private string _contentHtml;
        private Control _content; 

        public bool Draggable { get; set; }
        public string AlternateText { get; set; }
        public string IconImageURL { get; set; }
        public string TextBoxStoreLocationId
        {
            get
            {
                if (TextBoxStoreLocation == null)
                {
                    return _textBoxStoreLocationID;
                }
                else
                {
                    return TextBoxStoreLocation.ClientID;
                }
            }
            set
            {
                _textBoxStoreLocationID = value;
            }
        }
        public Control TextBoxStoreLocation { get; set; }

        public double? Lat { get; set; }
        public double? Lng { get; set; }

        public string Address { get; set; }

        public string ContentHtml
        {
            get
            {
                return _contentHtml;
            }
            set
            {
                value = value.Replace("\r", "");
                value = value.Replace("\n", "");
                value = value.Replace("\t", "");
                _contentHtml = value;

            }
        }
        public Control Content
        {
            get
            {
                return _content;
            }
            set
            {
                _content = value;
                this.ContentHtml = CS.General_v3.Util.ControlUtil.RenderControl(_content, true);
            }
        }
        public int ContentMaxWidth { get; set; }
        public bool ShowContentInitially { get; set; }
        public bool ShowContentOnRollOver { get; set; }
        public bool HideContentOnRollOut { get; set; }
        public bool DoNotToggleContentOnClick { get; set; }
        /// <summary>
        /// Whether you want the marker to be moved whenever user clicks on map holder.
        /// </summary>
        public bool MoveMarkerOnMapClick { get; set; }


            //htmlContent, maxWidth, openByDefault, openOnMouseOver, closeOnMouseOut, doNotToggleOnClick
        /// <summary>
        /// Fill in either lat/lng or address.  Lat/Lng are given importance
        /// </summary>
        /// <param name="lat"></param>
        /// <param name="lng"></param>
        /// <param name="Adddress"></param>
        public GoogleMarker(double? lat, double? lng, string Address)
        {
            if (lat == 0 && lng == 0)
            {
                lat = lng = null;
            }
            this.Lat = lat;
            this.Lng = lng;
            this.Address = Address;
           // this.ShowContentOnRollOver = true;
           // this.HideContentOnRollOut = true;
            
            
        }



        
    }
}
