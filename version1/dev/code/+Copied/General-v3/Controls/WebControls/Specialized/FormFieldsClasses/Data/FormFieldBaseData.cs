﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Controls.WebControls.Common.General;
using System.Web.UI;
using CS.General_v3.Classes.CSS;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using CS.General_v3.Util;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data
{
    using JavaScript.jQuery.Plugins.Tooltips.QTip;

    public abstract class FormFieldBaseData 
    {
        
        public string ID { get; set; }
        public string RowCssClassBase { get; set; }
        public string RowCssClass { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public bool ReadOnly { get; set; }
        public bool Required { get { return _validationParams.isRequired; } set { _validationParams.isRequired = value; } }
        public bool DoNotValidateOnBlur { get { return _validationParams.doNotValidateOnBlur; } set { _validationParams.doNotValidateOnBlur = value; } }
        public string InitialEmptyText { get { return _validationParams.initialEmptyText; } set { _validationParams.initialEmptyText = value; } }
        public List<object> ValueIn { get { return _validationParams.valueIn; } set { _validationParams.valueIn = value; } }
        public List<object> ValueNotIn { get { return _validationParams.valueNotIn; } set { _validationParams.valueNotIn = value; } }
        public bool ShowValidationIcon { get { return _validationParams.showValidationIcon; } set { _validationParams.showValidationIcon = value; } }
        public Regex RegularExpression { get { return _validationParams.regExpPattern; } set { _validationParams.regExpPattern = value; } }
        public string HelpMessage { get; set; }
        public _jQueryQTipOptions HelpMessageQtip2Options { get; set; }
        public object InitialValue { get; set; }
        public int? Width { get; set; }
        public int? Height { get; set; }
        public FormFieldsCSSClasses FieldCssClasses { get; set; }

        public CSSManagerForControl FormFieldCssClass
        {
            get
            {
                return this.GetField().CssManager;
            }
        }

        /// <summary>
        /// The usual validation group to group these fields in a form
        /// </summary>
        public string ValidationGroup { get { return _validationParams.validationGroup; } set { _validationParams.validationGroup = value; } }
        public string CustomValidationAjaxHandlerURL { get { return _validationParams.customValidationAjaxHandlerURL; } set { _validationParams.customValidationAjaxHandlerURL = value; } }
        public Control ValidationIconElement { get; set; }

        /// <summary>
        /// The group validation to group various fields together in a subgroup within a form
        /// </summary>
        public FormFieldsValidationParams.FormFieldValidationSubGroupParams ValidationSubGroup { get { return _validationParams.subGroupParams; } set { _validationParams.subGroupParams = value;  } }


        private FormFieldsValidationParams __validationParams;
        protected FormFieldsValidationParams _validationParams
        {
            get {
                if (__validationParams == null)
                {
                    __validationParams = createValidationParams();
                }
                return __validationParams;
            }
        }
            /*public void SetValidationParams(FormFieldsValidationParams parameters)
        {      
            System.Diagnostics.Contracts.Contract.Requires(parameters != null);
            _validationParams = parameters;
        }*/

        protected IMyFormWebControl _field {get; private set;}
            

        public FormFieldBaseData()
        {
            this.FieldCssClasses = new FormFieldsCSSClasses();
            
        }
        /// <summary>
        /// Override this with specific parameters if required
        /// </summary>
        /// <returns></returns>
        protected virtual FormFieldsValidationParams createValidationParams()
        {
            return new FormFieldsValidationParams();
        }
        public List<long> GetFormValueAsListOfLong(string delimiter = ",")
        {
            var value = (string)GetField().FormValueObject;

            if (value == null)
            {
                return null;
            }
            else
            {
                return Text.SplitTextIntoLongs(value, delimiter);
            }
        }
        public object GetFormValueAsObject()
        {
            return GetField().FormValueObject;
        }
        public string GetFormValueAsString()
        {
            return (string)GetField().FormValueObject;
        }
        public int GetFormValueAsInteger()
        {
            return (int)GetField().FormValueObject;
        }
        public long GetFormValueAsLong()
        {
            return (long)GetField().FormValueObject;
        }
        public char GetFormValueAsChar()
        {
            return (char)GetField().FormValueObject;
        }
        public bool GetFormValueAsBoolean()
        {
            return (bool)GetField().FormValueObject;
        }
        public double GetFormValueAsDouble()
        {
            return (double)GetField().FormValueObject;
        }
        public float GetFormValueAsFloat()
        {
            return (float)GetField().FormValueObject;
        }

        /// <summary>
        /// Return the actual field control
        /// </summary>
        /// <returns></returns>
        protected abstract IMyFormWebControl createFieldControl();

        protected virtual void initFieldParams()
        {
            _field.ValidationParams = _validationParams;
            _field.FieldCssClasses = this.FieldCssClasses;
            _field.Value = this.InitialValue;
            _field.Title = Title;
            _field.ID = this.ID;
        }
        public MyFileUpload GetFieldAsMyFileUpload()
        {
            return (MyFileUpload)GetField();
        }

        public IMyFormWebControl GetField()
        {
            return getFieldAsFormWebControl();
        }
        protected IMyFormWebControl getFieldAsFormWebControl()
        {
            if (_field == null)
            {
                _field = createFieldControl();
                _field.Enabled = !ReadOnly;
                if (ReadOnly)
                {
                    _field.CssManager.AddClass(_field.Class_ReadOnly);
                }
                if (Width.HasValue)
                {
                    WebControl webCtrl = (WebControl)_field;
                    webCtrl.Width = Width.Value;

                }
                if (Height.HasValue)
                {
                    WebControl webCtrl = (WebControl)_field;
                    webCtrl.Height = Height.Value;
                }

            }
            initFieldParams();
            return _field;
        }
        public bool LoadInitialValueFromPostback { get; set; }

        public virtual void OnRendered()
        {
            if (this.LoadInitialValueFromPostback && _field.FormValueObject != null)
            {
                _field.Value = _field.FormValueObject;
            }

            

        }

    }
}
