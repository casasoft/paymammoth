﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common.General;
using CS.General_v3.Controls.WebControls.Common;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data
{
    public class FormFieldNumericCreditCardData : FormFieldNumericIntegerData
    {
        public FormFieldNumericCreditCardData()
        {
            _validationParams.isCreditCardNumber = true;
            this.RowCssClassBase = "form-row-credit-card";
        }

        
        

    }
}
