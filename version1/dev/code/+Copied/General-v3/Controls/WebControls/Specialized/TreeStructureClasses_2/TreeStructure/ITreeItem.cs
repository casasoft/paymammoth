﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructure
{
    public interface ITreeItem : TreeStructureBasic.ITreeItemBasic
    {
        //int ParentID { get; set; }
        
        string EditURL { get;  }
        string AddNewItemURL { get; }
        bool AllowUpdate { get;  }
        bool AllowDelete { get;  }
        //bool AllowAdd { get; }
        bool AllowAddSubItems { get; }
        bool CanRemove(out string errorMsg);
        void Remove();
        void Save();
        
        /// <summary>
        /// Delete message to show.  If left null, default message is shown
        /// </summary>
        string Message_DeleteOK { get; }
        /// <summary>
        /// Confirm message to show.  If left null, default message is shown
        /// </summary>
        string Message_ConfirmDelete { get;  }


        bool AddedExtraButtons { get; set; }
    }
        
}
