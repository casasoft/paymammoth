﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace CS.General_v3.Controls.WebControls.Specialized.MediaGallery
{
    public abstract class AjaxMediaGalleryDeleteBaseHandler : CS.General_v3.HTTPHandlers.BaseAjaxHandler
    {
        /// <summary>
        /// The parameter where to load the list of IDs in order, IDs are seperated by a comma
        /// </summary>
        protected string _paramID = "id";
        protected string _paramResponseItemId = "id";
        protected string _paramResponseSuccess = "success";

        protected override void processRequest(System.Web.HttpContext context)
        {
            string id = context.Request.QueryString[_paramID];


            bool ok = DeleteItem(context, id);
            this.AddProperty(_paramResponseSuccess, ok);
            this.AddProperty(_paramResponseItemId, id);


            base.processRequest(context);
        }

        public abstract bool DeleteItem(HttpContext context, string itemID);
    }
}
