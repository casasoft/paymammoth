﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;

namespace CS.General_v3.Controls.WebControls.Specialized.HtmlReports
{
    public class HtmlReport : MyPanel
    {
        public class FUNCTIONALITY
        {
            public HtmlReportHeader Header { get; set; }
            

            private HtmlReport _control;

            public FUNCTIONALITY(HtmlReport control)
            {
                _control = control;
                Header = new HtmlReportHeader();
                _control.Controls.Add(Header);
            }
            public void OnLoad()
            {
                _control.CssManager.AddClass("html-report");

            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public HtmlReport()
        {
            Functionality = new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            Functionality.OnLoad();
            base.OnLoad(e);
        }
    }
}
