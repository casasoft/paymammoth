﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common.General;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data
{
    public class FormFieldListSingleChoiceRadioData : FormFieldListBaseTypeData<string>
    {
        public RepeatDirection RadioButtonRepeatDirection { get; set; }
        public RepeatLayout RadioButtonRepeatLayout { get; set; }
        public int RadioButtonRepeatColoumns { get; set; }

        public FormFieldListSingleChoiceRadioData()
        {

            this.RowCssClassBase = "form-row-radio-list";

        }
        public new MyRadioButtonList GetField()
        {
            return (MyRadioButtonList)base.GetField();
        }
        protected override IMyFormWebControl createFieldControl()
        {

            MyRadioButtonList _rdList = new MyRadioButtonList();
            _rdList.AddItemRange(this.ListItems.ToArray());
            _rdList.RepeatDirection = RadioButtonRepeatDirection;
            _rdList.RepeatLayout = RadioButtonRepeatLayout;
            _rdList.RepeatColumns = RadioButtonRepeatColoumns;
            _rdList.SelectedValue = this.InitialValue;
            _rdList.Value = this.InitialValue;
            return _rdList;

        }
        public override string GetFormValue()
        {
            return this.GetField().FormValue;
            
        }
        
    }
}
