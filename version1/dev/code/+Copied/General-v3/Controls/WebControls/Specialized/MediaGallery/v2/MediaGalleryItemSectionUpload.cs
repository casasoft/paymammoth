﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_20090518.Controls.WebControls.Common;

namespace CS.General_20090518.Controls.WebControls.Specialized.MediaGallery.v2
{
    public class MediaGalleryItemSectionUpload : MyDiv
    {
        public class FUNCTIONALITY
        {
            public delegate void OnUploadSubmitHander(List<MyFileUpload> fileUploads);
            public event OnUploadSubmitHander OnUploadSubmit;

            private MediaGalleryItemSectionUpload _ui;

            private FormFields _formUploads;

            private List<MyFileUpload> _fileUploads = new List<MyFileUpload>();

            public int MaxUpload { get; set; }

            public FUNCTIONALITY(MediaGalleryItemSectionUpload ui)
            {
                _ui = ui;
                MaxUpload = 5;
            }
            private void createUploads()
            {
                string id = _ui.ClientID +"formUploads";
                _formUploads = new FormFields(id);
                for (int i = 0; i < MaxUpload; i++)
                {
                    var fileUpload = _formUploads.AddFileUpload(id + i, "Item " + i, i == 0, null, null);
                    _fileUploads.Add(fileUpload);
                }
                _formUploads.ClickSubmit += new EventHandler(_formUploads_ClickSubmit);
                _ui.Controls.Add(_formUploads);

            }

            void _formUploads_ClickSubmit(object sender, EventArgs e)
            {
                List<MyFileUpload> fileUploads = new List<MyFileUpload>();
                for (int i = 0; i < _fileUploads.Count; i++)
                {
                    if (_fileUploads[i].HasUploadedFile)
                    {
                        fileUploads.Add(_fileUploads[i]);
                    }
                }
                if (fileUploads.Count > 0)
                {
                    if (OnUploadSubmit != null)
                    {
                        OnUploadSubmit(fileUploads);
                    }
                }
            }
            public void Init()
            {
                createUploads();
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public MediaGalleryItemSectionUpload()
        {
            Functionality = new FUNCTIONALITY(this);
            this.CssManager.AddClass("cs-media-gallery-upload");
        }
        protected override void OnLoad(EventArgs e)
        {
            Functionality.Init();
            base.OnLoad(e);
        }
    }
}
