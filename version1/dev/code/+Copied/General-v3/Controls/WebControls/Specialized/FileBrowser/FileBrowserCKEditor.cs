﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common;

using CS.General_v3;
using CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructureBasic;
using System.IO;
namespace CS.General_v3.Controls.WebControls.Specialized.FileBrowser
{
    public class FileBrowserCKEditor : FileBrowser
    {
        public new class FUNCTIONALITY : FileBrowser.FUNCTIONALITY
        {
            public static string QueryStringParam_CKEditorFunctionName { get { return "CKEditorFuncNum"; } }
            public static string QueryStringParam_CKEditorLanguageCode { get { return "langCode"; } }
            public static string QueryStringParam_CKEditorInstanceName { get { return "CKEditor"; } }
            public static string CKEditor_FunctionName { get { return CS.General_v3.Util.PageUtil.GetVariableFromQuerystring(QueryStringParam_CKEditorFunctionName); } }
            public static string CKEditor_LanguageCode { get { return CS.General_v3.Util.PageUtil.GetVariableFromQuerystring(QueryStringParam_CKEditorLanguageCode); } }
            public static string CKEditor_InstanceName { get { return CS.General_v3.Util.PageUtil.GetVariableFromQuerystring(QueryStringParam_CKEditorInstanceName); } }

           
            
            private FileBrowserCKEditor _fileBrowser = null;
            public FUNCTIONALITY(FileBrowserCKEditor fileBrowser) : base(fileBrowser)
            {
                
                _fileBrowser = fileBrowser;
                
                this.GetFileLinkURL += new GetFileLinkURLDelegate(FileBrowser_GetFileLinkURL);
            }

            private string FileBrowser_GetFileLinkURL(FileInfo fileInfo)
            {
                string relPathLocal = fileInfo.FullName;
                string rootPath = GetRootFolder_Local();
                string relPath = this.RootFolder + "/" + relPathLocal.Substring(rootPath.Length);
                relPath = relPath.Replace("\\","/");
                relPath = relPath.Replace("//","/");
                relPath = System.Web.HttpUtility.HtmlEncode(relPath);

                return "javascript:setImage('" + relPath + "');";
            }

            private void renderJS()
            {
                StringBuilder js = new StringBuilder();
                js.AppendLine("function setImage(imgLoc)");
                js.AppendLine("{");
                js.AppendLine("  window.opener.CKEDITOR.tools.callFunction(" + CKEditor_FunctionName  + ",imgLoc);");
                js.AppendLine("  window.close();");
                js.AppendLine("}");
                this._fileBrowser.Page.ClientScript.RegisterClientScriptBlock(_fileBrowser.GetType(),"setImage",js.ToString(),true);

            }
            public override void Render()
            {
                renderJS();
                base.Render();
            }

        }
        public new FUNCTIONALITY Functionality 
        {
            get
            {
                return (FUNCTIONALITY)base.Functionality;
            }
            set
            {
                base.Functionality = value;
            }
        }
        public FileBrowserCKEditor(string id ) : base (id)
        {
            base.Functionality = new FUNCTIONALITY(this);
        }

        protected override void OnLoad(EventArgs e)
        {
            
            base.OnLoad(e);
        }

    }
}
