﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Util.Encoding.CSV;
using System.Web.UI;

namespace CS.General_v3.Controls.WebControls.Specialized.Reporting.v1
{
    public class ReportSectionToCsvExporter
    {
        public ReportSectionToCsvExporter(Table tb)
        {
            this.Data = tb;
            
        }

        public Table Data { get; private set; }
        private CSVFile file = null;
        
        //private void exportTopHeading()
        //{
        //    headersLeft = new List<IBaseReportTableHeaderDataBase>();
        //    if (Data.TableData.HeadersLeft != null) headersLeft = Data.TableData.HeadersLeft.ToList();
        //    CSVLine headerLine = new CSVLine();
        //    if (headersLeft.Count > 0)
        //    {
        //        headerLine.AddTokens("-");
        //    }
        //    foreach (var h in Data.TableData.HeadersTop)
        //    {
                
        //        headerLine.AddTokens(getCellContent(h.HeaderCellContent));
        //    }
        //    if (headersLeft.Count > 0)
        //    {
        //        headerLine.AddTokens("Totals");
        //    }
            
        //    file.Add(headerLine);
            

        //}

        private string getCellContent(Control ctrl)
        {
            string html = CS.General_v3.Util.ControlUtil.RenderControl(ctrl);
            string txt = CS.General_v3.Util.Text.ConvertHTMLToPlainText(html, replaceLinksWithHrefIfNotMatching: false);
            return txt;
        }


        private void exportContent()
        {
            for (int i = 0; i < this.Data.Rows.Count; i++)
            {
                var row = this.Data.Rows[i];
                CSVLine line = new CSVLine();
                for (int j = 0; j < row.Cells.Count; j++)
                {
                    var cell = row.Cells[j];
                    line.AddTokens(getCellContent(cell));
                }
                file.Add(line);
            }

        }


        public CSVFile ExportToCsv()
        {
            file = new CSVFile();
            //exportTopHeading();
            exportContent();
            //exportFooter();
            return file;
        }


    }
}
