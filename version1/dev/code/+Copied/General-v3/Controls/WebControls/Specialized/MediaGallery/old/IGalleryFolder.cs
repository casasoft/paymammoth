﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.MediaItems;

namespace CS.General_v3.Controls.WebControls.Specialized.MediaGallery
{
    public interface IGalleryFolder
    {
        /// <summary>
        /// Extra Parameter
        /// </summary>
        string String1 { get; set; }
        /// <summary>
        /// Extra Parameter
        /// </summary>
        string String2 { get; set; }
        /// <summary>
        /// Extra Parameter
        /// </summary>
        string String3 { get; set; }

        string Name { get; set; }
        string Description { get; set; }
        /// <summary>
        /// A Date that can be assigned to the folder, example the date of the event
        /// </summary>
        DateTime FolderDate { get; set; }
        void Remove();
        void Save();
        int ID { get; set; }
        int Priority { get; set; }
        bool IsFeatured { get; set; }
        bool CanDelete { get; set; }
        bool AllowAddItems { get; set; }
        bool AllowEdit { get; set; }
        bool AllowAddFolders { get; set; }
        IGalleryFolder GetParentFolder();
        List<IGalleryFolder> GetSubFolders();
        IGalleryFolder CreateNewFolder();
        List<IMediaItem> GetMediaItems();
        IMediaItem CreateNewMediaItem(string filename);
    }
}
