﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.WebControls.Specialized.GoogleMaps.v3
{
    public class FullGoogleMapComponent : GoogleMap
    {
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }

        public string MarkerTitle { get; set; }

        public FullGoogleMapComponent(double? latitude = null, double? longitude = null)
        {
            this.Latitude = latitude;
            this.Longitude = longitude;

            getMarkerFromSettings();
        }

        private void getMarkerFromSettings()
        {
            Properties.Lat = Latitude.HasValue ? Latitude.Value : Enums.SETTINGS_ENUM.CompanyInfo_GoogleMaps_Latitude.GetSettingFromDatabase<double?>();
            Properties.Lng = Longitude.HasValue ? Longitude.Value : Enums.SETTINGS_ENUM.CompanyInfo_GoogleMaps_Longtitude.GetSettingFromDatabase<double?>();

            Properties.ZoomLevel = Enums.SETTINGS_ENUM.CompanyInfo_GoogleMaps_ZoomLevel.GetSettingFromDatabase<double?>();

            GoogleMarker marker = new GoogleMarker();

            if (!String.IsNullOrEmpty(MarkerTitle))
            {
                marker.Title = MarkerTitle;
            }
            else
            {
                Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_CompanyName.GetSettingFromDatabase();
            }
            marker.Lat = Properties.Lat;
            marker.Lng = Properties.Lng;

            this.Properties.Markers.Add(marker);
        }
    }

}
