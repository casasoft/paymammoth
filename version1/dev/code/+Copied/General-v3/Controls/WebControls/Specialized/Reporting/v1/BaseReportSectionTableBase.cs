﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using CS.General_v3.Controls.WebControls.Common;
using System.Web.UI.WebControls;
using CS.General_v3.Classes.CSS;

namespace CS.General_v3.Controls.WebControls.Specialized.Reporting.v1
{
    internal abstract class BaseReportSectionTableBase : MyTable
    {

        #region BaseReportSectionTableBase Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            public IBaseReportSectionDataBase Section { get; set; }

            protected BaseReportSectionTableBase _item = null;
            internal FUNCTIONALITY(BaseReportSectionTableBase item)
            {
                CS.General_v3.Util.ContractsUtil.RequiresNotNullable(item, "item");
                this._item = item;
            }

        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion
		
			

        public BaseReportSectionTableBase()
        {
            
        }

        protected abstract bool hasTotalVertical();
        protected abstract bool hasTotalHorizontal();
        protected abstract bool hasTotalVerticalAndHorizontal();
        private int _totalRows;
        private int _totalCols;

        private int getAmtCols()
        {
            int cols = 0;
            bool addedTotal = false;
            if (this.Functionality.Section.TableData.HeadersLeft != null && this.Functionality.Section.TableData.HeadersLeft.Count() > 0)
            {
                cols++;
                //Check total of headers
                foreach (var headerCellData in this.Functionality.Section.TableData.HeadersLeft)
                {

                    if (hasTotalHorizontal())
                    {
                        cols++;
                        addedTotal = true;
                        break;
                    }
                }
            }
            //Check middle cells
            int middleCols = 0;
            //check top headers
            if (this.Functionality.Section.TableData.HeadersTop != null && this.Functionality.Section.TableData.HeadersTop.Count() > 0)
            {
                middleCols = this.Functionality.Section.TableData.HeadersTop.Count();
            }
            //Check middle data cells
            foreach (var row in this.Functionality.Section.TableData.DataRows)
            {
                middleCols = Math.Max(row.Cells.Count(), middleCols);
            }
            cols += middleCols;
            if (!addedTotal && hasTotalVerticalAndHorizontal())
            {
                cols++;
                addedTotal = true;
            }
            /*if (this.Functionality.Section.TableData.HeadersTop != null && this.Functionality.Section.TableData.HeadersTop.Count() > 0)
            {
                cols += this.Functionality.Section.TableData.HeadersTop.Count();
            }
            else
            {
                //No headers top so take it from the data
                int maxDataCols = 0;
                foreach(var dataRow in this.Functionality.Section.TableData.DataRows) {
                    if (dataRow.Cells != null)
                    {
                        maxDataCols = Math.Max(maxDataCols, dataRow.Cells.Count());
                    }
                }
                cols += maxDataCols;
            }*/
            return cols;
        }
        private int getAmtRows()
        {
            int rows = 0;
            bool addedTotal = false;
            if (this.Functionality.Section.TableData.HeadersTop != null &&
                this.Functionality.Section.TableData.HeadersTop.Count() > 0)
            {
                rows++;
                //Check total of headers
                foreach (var headerCellData in this.Functionality.Section.TableData.HeadersTop)
                {

                    if (hasTotalVertical())
                    {
                        rows++;
                        addedTotal = true;
                        break;
                    }
                }
            }
            if (!addedTotal && hasTotalVerticalAndHorizontal())
            {
                rows++;
                addedTotal = true;
            }

            int dataRowsAmt = 0;

            if (this.Functionality.Section.TableData.HeadersLeft != null &&
                this.Functionality.Section.TableData.HeadersLeft.Count() > 0)
            {
                dataRowsAmt = this.Functionality.Section.TableData.HeadersLeft.Count();
            }
            //No headers top so take it from the data
            if (this.Functionality.Section.TableData.DataRows != null)
            {
                dataRowsAmt = Math.Max(dataRowsAmt, this.Functionality.Section.TableData.DataRows.Count());
            }
            rows += dataRowsAmt;

            return rows;
        }

        private void initTableData()
        {
            //MyTableRow
            int rows = getAmtRows();
            int cols = getAmtCols();
            bool containsTopHeaders = this.containsTopHeaders();
            bool containsLeftHeaders = this.containsLeftHeaders();
            for (int y = 0; y < rows; y++)
            {
                MyTableRow r = this.AddRow("cs-report-table-row");
                r.CssManager.AddClass((y % 2 == 0) ? "cs-report-table-row-even" : "cs-report-table-row-odd");
                
                if (containsTopHeaders && y == 0)
                {
                    r.CssManager.AddClass("cs-report-table-row-header");
                }
                for (int x = 0; x < cols; x++)
                {
                    MyTableCell c = r.AddCell("cs-report-table-cell");
                    c.CssManager.AddClass((x % 2 == 0) ? "cs-report-table-cell-even" : "cs-report-table-cell-odd");
                    if (containsTopHeaders && y == 0 || containsLeftHeaders && x == 0)
                    {
                        c.CssManager.AddClass("cs-report-table-header-cell");
                    }
                }
            }
            _totalCols = cols;
            _totalRows = rows;
        }
        private bool containsLeftHeaders()
        {
            return this.Functionality.Section.TableData.HeadersLeft != null && this.Functionality.Section.TableData.HeadersLeft.Count() > 0;
        }
        private bool containsTopHeaders()
        {
            return this.Functionality.Section.TableData.HeadersTop != null && this.Functionality.Section.TableData.HeadersTop.Count() > 0;
        }

        private void init()
        {
            initTableData();
            initTopHeadersAndTotals();
            initLeftHeadersAndTotals();
            initDataCells();
            initTotalHeadersBothCell();
        }

        protected abstract void parseHeaderTotalContent(IBaseReportTableHeaderDataBase header, bool vertical, int index, out Control totalContent, out CSSManager cssClasses);

        //private void validateContent()
        //{
        //    int dataRowsCount = this.Functionality.Section.TableData.DataRows.Count();
        //    int maxDataRowsCells = 0;
        //    foreach (var row in this.Functionality.Section.TableData.DataRows)
        //    {
        //        maxDataRowsCells = Math.Max(row.Cells.Count(), maxDataRowsCells);
        //    }
        //    if (this.Functionality.Section.TableData.HeadersLeft != null && this.Functionality.Section.TableData.HeadersLeft.Count() > maxDataRowsCells)
        //    {
                
        //    }
        //}

        private void initTopHeadersAndTotals()
        {
            if (containsTopHeaders())
            {
                int startIndex = 0;
                if (containsLeftHeaders())
                {
                    startIndex++;
                }
                int i = 0;
                foreach (var headerData in this.Functionality.Section.TableData.HeadersTop)
                {
                    int index = startIndex + i;
                    if (headerData != null)
                    {
                        initCell(this[0][index], headerData.HeaderCellContent, "cs-report-table-top-header-cell");
                        Control headerTotal;
                        CSSManager headerTotalCSS;
                        parseHeaderTotalContent(headerData, true, i, out headerTotal, out headerTotalCSS);
                        if (headerTotal != null)
                        {
                            initCell(this[_totalRows - 1][index], headerTotal, headerTotalCSS,
                                     "cs-report-table-bottom-total-cell");
                        }
                    }
                    i++;
                }
            }
        }
        private void initLeftHeadersAndTotals()
        {
            if (containsLeftHeaders())
            {
                int startIndex = 0;
                if (containsTopHeaders())
                {
                    startIndex++;
                }
                int i = 0;
                foreach (var headerData in this.Functionality.Section.TableData.HeadersLeft)
                {
                    int index = startIndex + i;
                    initCell(this[index][0], headerData.HeaderCellContent, "cs-report-table-left-header-cell");

                    Control headerTotal;
                    CSSManager headerTotalCSS;
                    parseHeaderTotalContent(headerData, false, i, out headerTotal, out headerTotalCSS);
                    if (headerTotal != null)
                    {
                        initCell(this[index][this._totalCols-1], headerTotal, headerTotalCSS,
                                 "cs-report-table-right-total-cell");
                    }

                   
                    i++;
                }
            }
        }
        private void initDataCells()
        {
            if (this.Functionality.Section.TableData.DataRows != null && this.Functionality.Section.TableData.DataRows.Count() > 0)
            {
                int startX = containsLeftHeaders() ? 1 : 0;
                int startY = containsTopHeaders() ? 1 : 0;
                int y = 0;
                foreach (var rowData in this.Functionality.Section.TableData.DataRows)
                {
                    int x = 0;
                    var row = this[y + startY];
                    string customCss = rowData.CustomCss.ToString();
                    if (!string.IsNullOrWhiteSpace(customCss))
                        row.CssManager.AddClass(customCss);
                    foreach(var cellData in rowData.Cells) {
                        this.initCell(row[x + startX], cellData, "cs-report-table-data-cell");
                        x++;
                    }
                    y++;
                }
            }
        }

        protected abstract void getVerticalAndHorizontalTotalContent(out Control content, out CSSManager cssClasses);

        private void initTotalHeadersBothCell()
        {
            Control content;
            CSSManager css;
            getVerticalAndHorizontalTotalContent(out content, out css);

            if (content != null)
            {
                initCell(this[_totalRows - 1][_totalCols - 1], content, css, "cs-report-table-total-cell");
            }
        }
        
        private void initCell(MyTableCell cell, IBaseReportTableCellDataBase cellData, string extraCSS)
        {
            initCell(cell, cellData.GetCellContent(), cellData.CssManager, extraCSS);
        }
        private void initCell(MyTableCell cell, Control cellContent, CSSManager cellCSSManager, string extraCSS)
        {
            bool hasContent = cellContent != null;
            if (cellContent != null)
            {
                cell.Controls.Add(cellContent);
            }

            cell.CssManager.AddClass(hasContent ? "cs-report-table-cell-content" : "cs-report-table-cell-empty");
            if (!string.IsNullOrWhiteSpace(extraCSS))
            {
                cell.CssManager.AddClass(extraCSS);
            }

            if (cellCSSManager != null)
            {
                cell.CssManager.AddClass(cellCSSManager.ToString());
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            //validateContent();
            this.CssManager.AddClass("cs-report-table");
            init();
            base.OnLoad(e);
        }
    }
}
