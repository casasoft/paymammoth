﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using CS.General_20101215.Controls.WebControls.Common;
using CS.General_20101215.Controls.Classes.MediaGallery;
using CS.General_20101215.Util;
namespace CS.General_20101215.Controls.WebControls.Specialized.LinkSections
{
    [ToolboxData("<{0}:LinkItemForm runat=server></{0}:LinkItemForm>")]    
    public class LinkItemForm : MenuTreeStructure.MenuItemForm
    {

        /* HOW TO USE
        * ----------
        * 
        * Parameters to fill
        * 
        *  - ItemTitle
        *  - ListingPageUrl
        *  
        * Events to Attach
        *  
        *  - LoadFolder
        *  
        *  
        * Optional
        * 
        * - QueryStringItemParam
        * - QueryStringParentFolderParam
        */
        public LinkItemForm()
        {
            this.ItemTitle = "Link";
            base.LoadFolder += new CS.General_20101215.Controls.WebControls.Specialized.MenuTreeStructure.MenuItemForm.CreateLoadFolderEvent(LinkItemForm_LoadFolder);
            
        }

        CS.General_20101215.Controls.WebControls.Specialized.MenuTreeStructure.IMenuFolder LinkItemForm_LoadFolder(int ID)
        {
            return LoadFolder(ID);
        }
        #region Constants / Statics
        

        #endregion
        #region Delegates
        public delegate ILinkSection CreateLoadFolderEvent(int ID);
        #endregion
        #region Events
        public new event CreateLoadFolderEvent LoadFolder;
        #endregion
        #region Methods


        public ILinkSection GetParentFolder()
        {
            return LoadFolder(GetParentFolderIDFromQueryString());
        }
        public ILinkItem LoadItemFromParent()
        {
            ILinkSection parent = GetParentFolder();

            ILinkItem item = null;
            int itemID = GetItemIDFromQueryString();
            item = (ILinkItem)parent.GetSubItems().FindElemObj(obj => ((ILinkItem)obj).ID == itemID);

            return item;


        }
        #endregion
        #region Properties
        public ILinkItem LinkItem
        {
            get { return (ILinkItem)base.MenuItem; }
        }
        public new ILinkItem MenuItem
        {
            get { return LinkItem;}
        }
        public new ILinkItem PageItem
        {
            get { return LinkItem;}
        }
        

        #endregion
        

        protected override void OnInit(EventArgs e)
        {

            
            base.OnInit(e);
        }

        public override void addExtraFormFields()
        {
            ILinkItem linkItem = this.LinkItem;
            this.AddString("txtURL", "URL", true, "URL must start with an http://", 400, linkItem != null ? linkItem.URL : "");
            this.AddStringMultiline("txtDescription", "Description", true,null,6, 400, linkItem != null ? linkItem.Description : "");
            
            base.addExtraFormFields();
        }

        public override void saveExtraFields()
        {
            ILinkItem linkItem = this.MenuItem;
            linkItem.URL = this.GetFormValueStr("txtURL");
            linkItem.Description = this.GetFormValueStr("txtDescription");
            base.saveExtraFields();
        }



        protected override void OnLoad(EventArgs e)
        {
            
            base.OnLoad(e);
        }

        


 
    }
}
