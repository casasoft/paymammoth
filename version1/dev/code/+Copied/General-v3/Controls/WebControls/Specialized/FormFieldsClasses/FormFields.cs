﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using CS.General_v3;
using CS.General_v3.Classes.MediaItems;
using System.Web.UI;
using CS.General_v3.Controls.Classes.Forms;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Controls.WebControls;
using CS.General_v3.Controls.WebControls.Common.General;

using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data;
using System.ComponentModel;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses
{
    using JavaScript.jQuery.Plugins.Tooltips.QTip;

    [DefaultProperty("Text")]
    [ToolboxData("<{0}:FormFields runat=server></{0}:FormFields>")]
    public class FormFields : MyDiv
    {
        public delegate _jQueryQTipOptions HelpMessageQtip2OptionsHandler();


        public enum FIELDS_COL_TYPE
        {
            Label,
            HelpIcon,
            Field,
            ValidationIcon,
            Empty
        }
        public class FormFieldsColumnData
        {
            public FIELDS_COL_TYPE ColumnType { get; set; }
            public int ColSpan { get; set; }
            /// <summary>
            /// If this is filled in, then this CSS class is appended to the CSS classes of the TD
            /// </summary>
            public string CssClass { get; set; }

            public FormFieldsColumnData()
            {
                ColSpan = 1;
            }
        }




        #region FormFields Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public class FUNCTIONALITY
        {
            protected FormFields _item = null;
            

            public event HelpMessageQtip2OptionsHandler CreateHelpMessageQTip2Options;
            protected FormFields _control { get; private set; }
            public string ID { get; set; }
            public string RequiredCharacter { get; set; }
            public string LabelSuffix { get; set; }

            public string CssClassTdLabel { get; set; }
            public string CssClassTdField { get; set; }
            public string CssClassTdHelpIcon { get; set; }
            public string CssClassTdValidationIcon { get; set; }
            public string CssClassValidationIconSuccess { get; set; }
            public string CssClassValidationIconError { get; set; }
            public string CssClassValidationIconDim { get; set; }
            public string CssClassValidationIconLoading { get; set; }
            public string CssClassTrRowItem { get; set; }
            public string CssClassTrRowItemRequired { get; set; }
            public string CssClassTrRowItemReadonly { get; set; }
            public string CssClassButtonSubmit { get; set; }
            public string CssClassButtonCancel { get; set; }

            public string CssClassTrRowButtons { get; set; }
            public string SubtitleCssClass { get; set; }
            public MyButton ButtonSubmit { get; private set; }
            public MyButton ButtonCancel { get; private set; }

            public string ButtonSubmitText { get; set; }
            public string ButtonCancelText { get; set; }

            public bool ShowValidationIcon { get; set; }
            //public string ButtonSubmitExtraCssClass { get; set; }
            //public string ButtonCancelExtraCssClass { get; set; }
            public string ButtonsHelpMessage { get; set; }
            public _jQueryQTipOptions ButtonsHelpMessageQTip2Options { get; set; }

            public string ValidationGroup { get; set; }

            public event EventHandler ClickSubmit { add { _control._clickSubmit += value; } remove { _control._clickSubmit -= value; } }
            public event EventHandler ClickCancel { add { _control._clickCancel += value; } remove { _control._clickCancel -= value; } }

            private Dictionary<string, FormFieldsRowItemField> _hashFields;

            public List<MyButton> FormButtons { get; private set; }

            /// <summary>
            /// The order of the columns for the normal field
            /// </summary>
            public List<FormFieldsColumnData> ColumnLayout = new List<FormFieldsColumnData>() 
            { 
                new FormFieldsColumnData() { ColumnType = FIELDS_COL_TYPE.Label },
                new FormFieldsColumnData() { ColumnType = FIELDS_COL_TYPE.HelpIcon },
                new FormFieldsColumnData() { ColumnType = FIELDS_COL_TYPE.Field },
                new FormFieldsColumnData() { ColumnType = FIELDS_COL_TYPE.ValidationIcon }
            };


            public List<FormFieldsRowItemBase> FormFieldRows { get; private set; }

            public FUNCTIONALITY(FormFields control)
            {
                CS.General_v3.Util.ContractsUtil.RequiresNotNullable(control, "Form fields is required");
                this._item = control;
                this.ShowValidationIcon = true;

                _control = control;
                _hashFields = new Dictionary<string, FormFieldsRowItemField>();
                FormFieldRows = new List<FormFieldsRowItemBase>();

                this.RequiredCharacter = "*";
                this.LabelSuffix = ": ";

                this.ButtonCancel = new MyButton();
                this.ButtonSubmit = new MyButton();

                this.FormButtons = new List<MyButton>() { this.ButtonSubmit, this.ButtonCancel };

                this.CssClassTdLabel = "form-label";
                this.CssClassTdField = "form-field";
                this.CssClassTdHelpIcon = "form-help-icon";
                this.CssClassTdValidationIcon = "form-validation-icon";
                this.CssClassTrRowItem = "form-row";
                this.CssClassTrRowItemReadonly = "form-row-readonly";
                this.CssClassTrRowItemRequired = "form-row-required";
                this.CssClassTrRowButtons = "form-row-buttons";

                this.CssClassValidationIconSuccess = "validation-icon-success";
                this.CssClassValidationIconError = "validation-icon-error";
                this.CssClassValidationIconDim = "validation-icon-dim";
                this.CssClassValidationIconLoading = "validation-icon-loading";

                this.CssClassButtonSubmit = "button-submit";
                this.CssClassButtonCancel = "button-cancel";

                this.SubtitleCssClass = "form-label-subtitle";
                this.ButtonSubmitText = "Submit";

                this.ButtonCancelText = "Cancel";
                this.ValidationGroup = "main";


            }

            public FormFieldsRowItemField AddGenericFormFieldItem(FormFieldBaseData fieldData)
            {

               /* if (fieldData.ID == "cmsProperty_ContentTags")
                {
                    int k = 5;
                }*/
                if (string.IsNullOrEmpty(fieldData.ID))
                {
                    throw new Exception("ID of field cannot be null");
                }
                if (_hashFields.ContainsKey(fieldData.ID))
                {
                    throw new Exception("Field with ID '" + fieldData.ID + "' already exists");
                }
                /*if (string.IsNullOrWhiteSpace(fieldData.ValidationGroup))
                {
                    fieldData.ValidationGroup = this.ValidationGroup;
                }*/

                //Copy the values to the CSS classes of the Field
                fieldData.FieldCssClasses.cssClassTdField = this.CssClassTdField;
                fieldData.FieldCssClasses.cssClassTdHelpIcon = this.CssClassTdHelpIcon;
                fieldData.FieldCssClasses.cssClassTdLabel = this.CssClassTdLabel;
                fieldData.FieldCssClasses.cssClassTdValidationIcon = this.CssClassTdValidationIcon;
                fieldData.FieldCssClasses.cssClassTrRow = this.CssClassTrRowItem;
                fieldData.FieldCssClasses.cssClassTrRowRequired = this.CssClassTrRowItemRequired;
                fieldData.FieldCssClasses.cssClassValidationIconSuccess = this.CssClassValidationIconSuccess;
                fieldData.FieldCssClasses.cssClassValidationIconLoading = this.CssClassValidationIconLoading;
                fieldData.FieldCssClasses.cssClassValidationIconError = this.CssClassValidationIconError;
                fieldData.FieldCssClasses.cssClassValidationIconDim = this.CssClassValidationIconDim;
                fieldData.FieldCssClasses.cssClassReadOnly = this.CssClassTrRowItemReadonly;

                if (fieldData.HelpMessageQtip2Options == null && this.CreateHelpMessageQTip2Options != null)
                {
                    fieldData.HelpMessageQtip2Options = this.CreateHelpMessageQTip2Options();
                }
                FormFieldsRowItemField row = new FormFieldsRowItemField(ColumnLayout, _control, fieldData);
                //row.FieldData

                _hashFields[fieldData.ID] = row;
                this.FormFieldRows.Add(row);
                return row;
            }
            public FormFieldsRowItemField AddDouble(FormFieldNumericDoubleData fieldData)
            {



                return AddGenericFormFieldItem(fieldData);
            }
            public FormFieldsRowItemField AddStringGeneric(FormFieldStringBaseData fieldData)
            {
                return AddGenericFormFieldItem(fieldData);
            }
            public FormFieldsRowItemField AddString(FormFieldStringData fieldData)
            {
                return AddGenericFormFieldItem(fieldData);
            }
            public FormFieldsRowItemField AddStringMultiline(FormFieldStringMultilineData fieldData)
            {
                return AddGenericFormFieldItem(fieldData);
            }
            public FormFieldsRowItemField AddStringEmail(FormFieldStringEmailData fieldData)
            {
                return AddGenericFormFieldItem(fieldData);
            }
            public FormFieldsRowItemField AddStringHtmlEditor(FormFieldStringHtmlData fieldData)
            {
                return AddGenericFormFieldItem(fieldData);
            }
            public FormFieldsRowItemField AddStringPassword(FormFieldStringPasswordData fieldData)
            {
                return AddGenericFormFieldItem(fieldData);
            }
            public FormFieldsRowItemField AddStringPhoneNumber(FormFieldStringPhoneNumber fieldData)
            {
                return AddGenericFormFieldItem(fieldData);
            }

            public FormFieldsRowItemField AddStringWebsite(FormFieldStringWebsiteData fieldData)
            {
                return AddGenericFormFieldItem(fieldData);
            }
            public FormFieldsRowItemField AddInteger(FormFieldNumericIntegerData fieldData)
            {
                return AddGenericFormFieldItem(fieldData);
            }
            public FormFieldsRowItemField AddListSingleChoiceDropdown(FormFieldListSingleChoiceDropdownData fieldData)
            {
                return AddGenericFormFieldItem(fieldData);
            }

            public FormFieldsRowItemField Add3DateCombo(FormFieldDate3Combo fieldData)
            {
                return AddGenericFormFieldItem(fieldData);
            }

            public FormFieldsRowItemField AddListSingleChoiceRadio(FormFieldListSingleChoiceRadioData fieldData)
            {
                return AddGenericFormFieldItem(fieldData);
            }
            public FormFieldsRowItemField AddListMultipleChoice(FormFieldMulipleChoiceDataAsChkBoxes fieldData)
            {
                return AddGenericFormFieldItem(fieldData);
            }
            public FormFieldsRowItemField AddListMultipleChoice(FormFieldMulipleChoiceListBox fieldData)
            {
                return AddGenericFormFieldItem(fieldData);
            }
            public FormFieldsRowItemField AddSingleChoiceListRadioButtons(FormFieldListSingleChoiceRadioData fieldData)
            {
                return AddGenericFormFieldItem(fieldData);
            }
            public FormFieldsRowItemField AddDateTime(FormFieldDateData fieldData)
            {
                return AddGenericFormFieldItem(fieldData);
            }
            public FormFieldsRowItemField AddBool(FormFieldBoolData fieldData)
            {
                return AddGenericFormFieldItem(fieldData);
            }
            public MyButton AddButton(MyButton button, bool addAfterDefaultButtons = true)
            {
                this.FormButtons.Insert(addAfterDefaultButtons ? this.FormButtons.Count : 0, button);
                return button;
            }
            public FormFieldsRowItemField AddFileUpload(FormFieldFileUploadData fieldData)
            {
                return AddGenericFormFieldItem(fieldData);
            }
            public FormFieldsRowItemBase AddGenericRow(FormFieldsRowItemBase rowData)
            {

                this.FormFieldRows.Add(rowData);
                return rowData;
            }
            public FormFieldsRowItemField AddCKEditor(FormFieldStringCKEditorData fieldData)
            {
                return AddGenericFormFieldItem(fieldData);
            }
            public FormFieldsRowItemField GetRowItemFieldByID(string ID)
            {
                if (_hashFields.ContainsKey(ID))
                {
                    return _hashFields[ID];
                }
                else
                {
                    return null;
                }
            }
            public T GetFormValueByID<T>(string ID)
            {
                FormFieldsRowItemField row = _hashFields[ID];
                if (row == null)
                {
                    throw new Exception("Field with ID '" + ID + "' not found! (Maybe it has not yet been added to FormFields?)");
                }
                else
                {
                    return (T)row.Functionality.FieldData.GetFormValueAsObject();
                }
            }
            /// <summary>
            /// Call this method to add buttons at current place of rows
            /// </summary>
            public void ForceAddButtonsRow()
            {
                _control.addButtonsToForm(false);


            }

        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion
		
			



       
        protected MyTable _tblFormFields;
        private FormFieldsRowItemButtons _rowButtons;

        protected event EventHandler _clickCancel;
        protected event EventHandler _clickSubmit;

       

        public FormFields(string id = null)
        {
            
            
            _tblFormFields = new MyTable();
            
            
            
            this.CssManager.AddClass("form");

            if (!string.IsNullOrEmpty(id))
            {
                this.ID = id;
            }
        }
       
        private void renderRows()
        {
            _tblFormFields.CssManager.AddClass("form-table");
            this.Controls.Add(_tblFormFields);
            //_rows = new List<FormFieldsRowBaseItem>();

            for (int i = 0; i < this.Functionality.FormFieldRows.Count; i++)
            {
                FormFieldsRowItemBase rowItem = this.Functionality.FormFieldRows[i];

                if (rowItem is FormFieldsRowItemField)
                {
                    //Basic field
                    FormFieldsRowItemField field = (FormFieldsRowItemField)rowItem;
                    if (field.Functionality.FieldData.ValidationGroup == null && !string.IsNullOrEmpty(this.Functionality.ValidationGroup))
                    {
                        field.Functionality.FieldData.ValidationGroup = this.Functionality.ValidationGroup;
                    }
                }

                if (!this.Functionality.ShowValidationIcon && rowItem.Functionality.TdValidationIcon != null)
                {
                    rowItem.Functionality.TdValidationIcon.Visible = false;
                }
                _tblFormFields.Rows.Add(rowItem);
            }
            for (int i = 0; i < this.Functionality.FormFieldRows.Count; i++)
            {
                FormFieldsRowItemBase rowItem = this.Functionality.FormFieldRows[i];
                rowItem.OnRendered();
            }
        }

        private MyButton initButton(MyButton btnButton, string id, string text, EventHandler handler)
        {
            btnButton.ID = id;
            btnButton.Click += handler;
            btnButton.Text = text;
            return btnButton;
        }
        //private MyButton createButton(string id, string text, EventHandler handler)
        //{
        //    MyButton btnButton = new MyButton(id);
        //    btnButton.Click += handler;
        //    btnButton.Text = text;
        //    return btnButton;
        //}

        

        /// <summary>
        /// Add the default buttons (Submit / Cancel)
        /// </summary>
        /// <param name="addOnlyIfNotYetAdded">Add only if the buttons have not yet been added to display list</param>
        protected void addButtonsToForm(bool addOnlyIfNotYetAdded)
        {
            bool wasCreated = _rowButtons != null;
            if (_rowButtons == null)
            {
                if (_clickSubmit == null)
                {
                    this.Functionality.FormButtons.Remove(this.Functionality.ButtonSubmit); 
                }
                if (_clickCancel == null)
                {
                    this.Functionality.FormButtons.Remove(this.Functionality.ButtonCancel);
                }

                if (this.Functionality.FormButtons.Count > 0)
                {

                    _rowButtons = new FormFieldsRowItemButtons(this.Functionality.ColumnLayout, this, this.Functionality.ButtonsHelpMessage, this.Functionality.ButtonsHelpMessageQTip2Options);
                    if (this._clickSubmit != null)
                    {
                        MyButton btnSubmit = initButton(this.Functionality.ButtonSubmit, this.ID + "_btnSubmit", this.Functionality.ButtonSubmitText, _clickSubmit);

                        btnSubmit.ValidationGroup = this.Functionality.ValidationGroup;
                        btnSubmit.CssManager.AddClass(this.Functionality.CssClassButtonSubmit);
                        
                        //btnSubmit.CssManager.AddClass(this.Functionality.ButtonSubmitExtraCssClass);

                        //_rowButtons.Functionality.Buttons.Add(btnSubmit);
                        //this.Functionality.FormButtons.Remove(btnSubmit);
                    }
                    if (this._clickCancel != null)
                    {

                        MyButton btnCancel = initButton(this.Functionality.ButtonCancel, this.ID + "_btnCancel", this.Functionality.ButtonCancelText, _clickCancel);
                        btnCancel.ValidationGroup = this.Functionality.ValidationGroup + "-cancel";
                        btnCancel.CssManager.AddClass(this.Functionality.CssClassButtonCancel);
                       // btnCancel.CssManager.AddClass(this.Functionality.ButtonCancelExtraCssClass);

                       // _rowButtons.Functionality.Buttons.Add(btnCancel);
                        //this.Functionality.FormButtons.Remove(btnCancel);
                    }

                    for (int i = 0; i < this.Functionality.FormButtons.Count; i++)
                    {
                        _rowButtons.Functionality.Buttons.Add(this.Functionality.FormButtons[i]);
                    }

                }
            }
            if ((!wasCreated || !addOnlyIfNotYetAdded) && (_rowButtons != null))
            {
                this.Functionality.FormFieldRows.Remove(_rowButtons); //Just in case it is found

                this.Functionality.FormFieldRows.Add(_rowButtons); //Add it
            }
            
        }

        private bool _rendered = false;

        public void ForceRender()
        {
            renderFormFields();
        }

        protected void renderFormFields()
        {
            if (!_rendered)
            {
                if (string.IsNullOrEmpty(this.ID))
                {
                    throw new Exception("Please specify ID of FormFields");
                }
                _rendered = true;
                addButtonsToForm(true);
                renderRows();
                
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            
            renderFormFields();
            

            base.OnLoad(e);
        }
        





        #region Backwards-Compatible

        public event EventHandler ClickSubmit { add { Functionality.ClickSubmit += value; } remove { Functionality.ClickSubmit -= value; } }
        public event EventHandler ClickCancel { add { Functionality.ClickCancel += value; } remove { Functionality.ClickCancel -= value; } }

        [Obsolete("Use FormFields.Functionality")]
        public IMyFormWebControl AddString(string id, string title, bool required, string helpMessage, int? width, string initValue, bool createHelpMessageIconJS = true)
        {
            return this.Functionality.AddString(new FormFieldStringData() { ID = id, Width = width, Title = title, Required = required, HelpMessage = helpMessage, InitialValue = initValue }).Functionality.Field;
        }
        [Obsolete("Use FormFields.Functionality")]
        public IMyFormWebControl AddString(string id, string title, bool required, string helpMessage, int? width, string initValue, ListItemCollection choice, bool createHelpMessageIconJS = true)
        {
            return AddString(id, title, required, helpMessage, width, initValue, choice.Cast<ListItem>(), createHelpMessageIconJS);
        }
        [Obsolete("Use FormFields.Functionality")]
        public IMyFormWebControl AddString(string id, string title, bool required, string helpMessage, int? width, string initValue, IEnumerable<ListItem> choice, bool createHelpMessageIconJS = true)
        {
            return this.Functionality.AddListSingleChoiceDropdown(new FormFieldListSingleChoiceDropdownData() { ID = id, Width = width, Title = title, Required = required, HelpMessage = helpMessage, InitialValue = initValue, 
                ListItems = choice.ToList() }).Functionality.Field;
        }
        [Obsolete("Use FormFields.Functionality")]
        public IMyFormWebControl AddString(CS.General_v3.Enums.STRING_DATA_TYPE stringType, string id, string title, bool required,
            string helpMessage, int? width, string initValue, bool createHelpMessageIconJS = true)
        {

            FormFieldStringBaseData fs = null;

            switch (stringType)
            {
                case Enums.STRING_DATA_TYPE.Email: fs = new FormFieldStringEmailData(); break;
                case Enums.STRING_DATA_TYPE.MultiLine: fs = new FormFieldStringMultilineData(); break;
                case Enums.STRING_DATA_TYPE.Password: fs = new FormFieldStringPasswordData(); break;
                case Enums.STRING_DATA_TYPE.SingleLine: fs = new FormFieldStringData(); break;
                case Enums.STRING_DATA_TYPE.Website: fs = new FormFieldStringWebsiteData(); break;
            }

            fs.ID = id;
            fs.Title = title;
            fs.Required = required;
            fs.HelpMessage = helpMessage;
            fs.InitialValue = initValue;
            fs.Width = width;
            return this.Functionality.AddStringGeneric(fs).Functionality.Field;

            /*if (stringType == Enums.STRING_DATA_TYPE.MultiLine)
            {
                //return AddStringMultiline(id, title, required, helpMessage, 5, width, initValue);
                throw new InvalidOperationException("Please call 'AddStringMultiline' to add a multi-line string, and do not set the enum value");
            }
            else if (stringType == Enums.STRING_DATA_TYPE.SingleLine)
            {
                return AddString(id, title, required, null, null, initValue, createHelpMessageIconJS);
                throw new InvalidOperationException("Please do not call this overriden method for 'AddString' when adding a 'SingleLine'");
            }
            HtmlTableRow tr;
            this.Functionality.AddString( new FormFieldStringData() { typ
            return addString(stringType, id, title, required, helpMessage, 0, width, initValue, null, out tr);*/
        }
        [Obsolete("Use FormFields.Functionality")]
        public CKEditor AddCKEditor(string ID, string title, bool required,
           string helpMessage, int? width, int? height, string initValue)
        {
            var ckEditor = this.Functionality.AddCKEditor(new FormFieldStringCKEditorData()
            {
                ID = ID,
                Title = title,
                Required = required,
                HelpMessage = helpMessage,
                InitialValue = initValue, Width = width
            });

            return (CKEditor)ckEditor.Functionality.Field;

            /*string ctrlID = getControlID(ID);
            CS.General_v3.Controls.WebControls.Common.CKEditor ckEditor =
                new CKEditor(ctrlID);
            if (width.HasValue)
                ckEditor.Width = width.Value;
            if (!height.HasValue) height = 500;
            ckEditor.Height = height.Value;

            ckEditor.Text = initValue;



            HtmlTableRow tr;
            HtmlTableCell tdControl;
            MyLabel label;
            createFieldRow(title, required, out tr, out tdControl, out label);
            label.Control = ckEditor;
            //label.Attributes["for"] = fckEditor.ClientID;
            //tinymce.ValidationGroup = this.ValidationGroup;
            //tinymce.Attributes["helpMessage"] = helpMessage;
            tdControl.Controls.Add(ckEditor);
            formFieldItems.Add(new FormFieldItem(ID, tr, ckEditor, Enums.DATA_TYPE.String));
            fieldAdded(tr, ckEditor, helpMessage);
            return ckEditor;*/
        }
        [Obsolete("Use FormFields.Functionality")]
        public IMyFormWebControl AddStringMultiline(string id, string title, bool required,
            string helpMessage, int Rows, int? width, string initValue, bool createHelpMessageIconJS = true)
        {
            return Functionality.AddStringMultiline(new FormFieldStringMultilineData()
            {
                ID = id,
                Width = width,
                Title = title,
                Required = required,
                HelpMessage = helpMessage,
                InitialValue = initValue
            }).Functionality.Field;
            //HtmlTableRow tr;
            //return addString(Enums.STRING_DATA_TYPE.MultiLine, id, title, required, helpMessage, Rows, width, initValue, null, out tr, createHelpMessageIconJS);
        }

        [Obsolete("Use FormFields.Functionality")]
        public IMyFormWebControl AddDouble(string id, string title, bool required, string helpMessage, int? width, double? initValue)
        {
            return this.Functionality.AddDouble(new FormFieldNumericDoubleData() { Width = width, ID = id, Title = title, Required = required, HelpMessage = helpMessage, InitialValue = initValue }).Functionality.Field;
            //return addInteger(id, title, required, helpMessage, null, null, MyTxtBoxNumeric.NUMERIC_RANGE.All,
            //width, false, false, initValue, null, false, null);
        }
        [Obsolete("Use FormFields.Functionality")]
        public IMyFormWebControl AddInteger(string id, string title, bool required, string helpMessage, int? width, int? initValue)
        {
            return this.Functionality.AddInteger(new FormFieldNumericIntegerData() { Width=width, ID = id, Title = title, Required = required, HelpMessage = helpMessage, InitialValue = initValue }).Functionality.Field;
            //return addInteger(id, title, required, helpMessage, null, null, MyTxtBoxNumeric.NUMERIC_RANGE.All,
                //width, false, false, initValue, null, false, null);
        }
        /*
        [Obsolete("Use FormFields.Functionality")]
        public IMyFormWebControl AddEnum(string id, string title, bool required, string helpMessage, int? width, int? initValue, Type enumType, 
            bool addBlankItemOnTop = false, Enums.ENUM_SORT_BY enumValuesSortBy= Enums.ENUM_SORT_BY.NameAscending, bool addSpacesToCamelCasedEnums = true)
        {
            string blankValue = null;
            if (addBlankItemOnTop)
                blankValue = "0";
            var listCOll=  CS.General_v3.Util.EnumUtils.GetListItemCollectionFromEnum(enumType, blankValue, enumValuesSortBy, addSpacesToCamelCasedEnums);
            return this.AddInteger(id, title, required, helpMessage, width, initValue, listCOll.Cast<ListItem>());
        }*/
        /*
        public IMyFormWebControl AddEnumMultipleChoice<TEnum>(string id, string title, bool required, string helpMessage,
            Enums.ENUM_SORT_BY enumValuesSortBy = Enums.ENUM_SORT_BY.NameAscending, bool addSpacesToCamelCasedEnums = true, IEnumerable<TEnum> initValues = null) where TEnum : struct
        {

            return AddEnumMultipleChoice(id, title, required, helpMessage, typeof(TEnum), enumValuesSortBy, addSpacesToCamelCasedEnums, initValues.Cast<Enum>());
        }*/
        /*
        [Obsolete("Use FormFields.Functionality")]
        public IMyFormWebControl AddEnumMultipleChoice(string id, string title, bool required, string helpMessage, Type enumType,
            Enums.ENUM_SORT_BY enumValuesSortBy = Enums.ENUM_SORT_BY.NameAscending, bool addSpacesToCamelCasedEnums = true, IEnumerable<Enum> initValues = null)
        {
            
            string blankValue = null;
           
            var listColl = CS.General_v3.Util.EnumUtils.GetListItemCollectionFromEnum(enumType, blankValue, enumValuesSortBy, addSpacesToCamelCasedEnums);
            if (initValues != null)
            {
                foreach (var initVal in initValues)
                {
                    for (int i = 0; i < listColl.Count; i++)
                    {
                        var li = listColl[i];
                        if (li.Value == (Convert.ToInt32(initVal)).ToString())
                        {
                            li.Selected = true;
                        }
                    }
                }
            }

            return this.AddIntegerMultipleChoice(id, title, helpMessage, required, listColl.Cast<ListItem>());
        }*/
        [Obsolete("Use FormFields.Functionality")]
        public IMyFormWebControl AddInteger(string id, string title, bool required, string helpMessage, int? width, int? initValue, ListItemCollection coll)
        {
            return AddInteger(id, title, required, helpMessage, width, initValue, coll.Cast<ListItem>());
        }

        [Obsolete("Use FormFields.Functionality")]
        public IMyFormWebControl AddInteger(string id, string title, bool required, string helpMessage, int? width, int? initValue, IEnumerable<ListItem> coll)
        {
            return this.Functionality.AddListSingleChoiceDropdown(new FormFieldListSingleChoiceDropdownData()
            {
                ID = id,
                Width = width,
                Title = title,
                Required = required,
                HelpMessage = helpMessage,
                InitialValue = initValue.ToString(),
                ListItems = coll.ToList()
            }).Functionality.Field;

            //return addInteger(id, title, required, helpMessage, null, null, MyTxtBoxNumeric.NUMERIC_RANGE.All,
                //width, false, false, initValue, null, false, coll);
        }
        [Obsolete("Use FormFields.Functionality")]
        public IMyFormWebControl AddIntegerMultipleChoice(string id, string title, string helpMessage, bool required, ListItemCollection coll)
        {
            return AddIntegerMultipleChoice(id, title, helpMessage, required, coll.Cast<ListItem>());
        }

        //22/04/2009 Mark added required parameter
        [Obsolete("Use FormFields.Functionality")]
        public IMyFormWebControl AddIntegerMultipleChoice(string id, string title, string helpMessage, bool required, IEnumerable<ListItem> coll)
        {
            return this.Functionality.AddListMultipleChoice(new FormFieldMulipleChoiceDataAsChkBoxes()
            {
                ID = id,
                Title = title,
                HelpMessage = helpMessage,
                Required = required,
                ListItems = coll.ToList()
            }).Functionality.Field;
            //return addInteger(id, title, required, helpMessage, null, null, MyTxtBoxNumeric.NUMERIC_RANGE.All,
                //null, false, true, null, null, false, coll);
        }
        [Obsolete("Use FormFields.Functionality")]
        public IMyFormWebControl AddIntegerMultipleChoiceAsListBox(string id, string title, string helpMessage, bool required, ListItemCollection coll, int? width = null, int? height = null,
            bool showAsJQueryMultiPicker = false, bool JQueryMultiSelect_Searchable = true, bool JQueryMultiSelect_Sortable = true)
        {
            return AddIntegerMultipleChoiceAsListBox(id, title, helpMessage, required, coll.Cast<ListItem>(), width, height, showAsJQueryMultiPicker, JQueryMultiSelect_Searchable, JQueryMultiSelect_Sortable); 
        }

        //22/04/2009 Mark added required parameter
        [Obsolete("Use FormFields.Functionality")]
        public IMyFormWebControl AddIntegerMultipleChoiceAsListBox(string id, string title, string helpMessage, bool required, IEnumerable<ListItem> coll, int? width = null, int? height = null,
            bool showAsJQueryMultiPicker = false, bool JQueryMultiSelect_Searchable = true, bool JQueryMultiSelect_Sortable = true)
        {
            return this.Functionality.AddListMultipleChoice(new FormFieldMulipleChoiceListBox()
            {
                ID = id,
                Title = title,
                Width = width,
                Height = height,
                HelpMessage = helpMessage,
                Required = required,
                ListItems = coll.ToList(),
                JQueryMultiSelect = showAsJQueryMultiPicker,
                JQueryMultiSelect_Searchable = JQueryMultiSelect_Searchable,
                JQueryMultiSelect_Sortable = JQueryMultiSelect_Sortable
            }).Functionality.Field;
            //return addInteger(id, title, required, helpMessage, null, null, MyTxtBoxNumeric.NUMERIC_RANGE.All,
            //null, false, true, null, null, false, coll);
        }

        [Obsolete("Use FormFields.Functionality")]
        public IMyFormWebControl AddInteger(string id, string title, bool required, string helpMessage, int? numFrom, int? numTo,
            MyTxtBoxNumeric.NUMERIC_RANGE numericRange,
            int? width, int? initValue, bool nullable)
        {
            return this.Functionality.AddInteger(new FormFieldNumericIntegerData()
            {
                ID = id,
                Title = title,
                Required = required,
                HelpMessage = helpMessage,
                NumFrom = numFrom,
                NumTo = numTo,
                Width = width,
                RangeType = (FormFieldNumericIntegerData.NUMERIC_RANGE_TYPE)(int)numericRange,
                InitialValue = initValue
            }).Functionality.Field;
            //return addInteger(id, title, required, helpMessage, numFrom, numTo, numericRange,
             //   width, false, false, initValue, null, nullable, null);

        }

        [Obsolete("Use FormFields.Functionality")]
        public IMyFormWebControl AddBool(string ID, string title, bool required, bool initValue, string helpMessage = null)
        {
            return this.Functionality.AddBool(new FormFieldBoolData() { ID = ID, Title = title, Required = required, InitialValue = initValue, HelpMessage = helpMessage }).Functionality.Field;

        }
        [Obsolete("Use FormFields.Functionality")]
        public IMyFormWebControl GetControl(string fieldID)
        {
            var row = Functionality.GetRowItemFieldByID(fieldID);
            if (row != null)
            {
                return row.Functionality.Field;
            }
            else
            {
                return null;
            }
            
        }
        [Obsolete("Use FormFields.Functionality.GetFormValueByID<>")]
        public object GetFormValue(string fieldID, bool throwErrorIfNotExists = true)
        {
            
            var row = this.Functionality.GetRowItemFieldByID(fieldID);
            if (row != null)
            {
                return row.Functionality.Field.FormValueObject;
            }
            else
            {
                return null;
            }
        }



        [Obsolete("Use FormFields.Functionality.GetFormValueByID<>")]
        public string GetFormValueStr(string fieldID, bool throwErrorIfNotExists = true)
        {
            var row = this.Functionality.GetRowItemFieldByID(fieldID);
            if (row != null)
            {
                return row.Functionality.Field.GetFormValueAsStr();
            }
            else
            {
                return null;
            }
            

        }
        [Obsolete("Use FormFields.Functionality.GetFormValueByID<>")]
        public bool GetFormValueBool(string fieldID, bool throwErrorIfNotExists = true)
        {
            
            return bool.Parse(GetFormValue(fieldID, throwErrorIfNotExists).ToString());
        }
        [Obsolete("Use FormFields.Functionality.GetFormValueByID<>")]
        public List<int> GetFormValueListInt(string fieldID, bool throwErrorIfNotExists = true)
        {
            IEnumerable<object> list = (IEnumerable<object>)GetFormValue(fieldID, throwErrorIfNotExists);

            List<int> l = new List<int>();
            if (list != null)
            {
                foreach (var x in list)
                {
                    l.Add(int.Parse(x.ToString()));
                }
            }
            return l;

        }
        [Obsolete("Use FormFields.Functionality.GetFormValueByID<>")]
        public int GetFormValueInt(string fieldID, bool throwErrorIfNotExists = true)
        {
            return GetFormValueIntNullable(fieldID, throwErrorIfNotExists).GetValueOrDefault(0);
            
        }
        [Obsolete("Use FormFields.Functionality.GetFormValueByID<>")]
        public int? GetFormValueIntNullable(string fieldID, bool throwErrorIfNotExists = true)
        {
            return (int?)GetFormValueLongNullable(fieldID, throwErrorIfNotExists);
        }
        [Obsolete("Use FormFields.Functionality.GetFormValueByID<>")]
        public long? GetFormValueLongNullable(string fieldID, bool throwErrorIfNotExists = true)
        {
            var val = GetFormValue(fieldID, throwErrorIfNotExists);
            if (val != null)
            {
                long lng = 0;
                if (long.TryParse((string)val, out lng))
                    return lng;
                else
                    return null;
               
            }
            else
            {
                return null;
            }
        }
        [Obsolete("Use FormFields.Functionality.GetFormValueByID<>")]
        public double GetFormValueDouble(string fieldID, bool throwErrorIfNotExists = true)
        {
            return double.Parse(GetFormValue(fieldID, throwErrorIfNotExists).ToString());
            
        }
        [Obsolete("Use FormFields.Functionality.GetFormValueByID<>")]
        public DateTime GetFormValueDate(string fieldID, bool throwErrorIfNotExists = true)
        {
            object o = GetFormValue(fieldID, throwErrorIfNotExists: throwErrorIfNotExists);
            return Util.Conversion.ToDateTime(o);
        }
        [Obsolete("Use FormFields.Functionality")]
        public MyTableRow AddGenericLabelAndControl(string title, bool required, Control c)
        {
            MyTableCell td = null;
            return AddGenericLabelAndControl(title, required, c, out td);
        }
        [Obsolete("Use FormFields.Functionality")]
        public MyTableRow AddGenericLabelAndControl(string title, bool required, Control c, out MyTableCell tdControl)
        {

            var row = new FormFieldsRowItemBase(this.Functionality.ColumnLayout, this, title, null, c, null, false, null);



            tdControl = row.Functionality.TdField;
            row.TempInit();
            this.Functionality.AddGenericRow(row);


            return row;
        }
        public string ButtonSubmitText { get { return this.Functionality.ButtonSubmitText; } set { this.Functionality.ButtonSubmitText = value; } }
        public string ButtonCancelText { get { return this.Functionality.ButtonCancelText; } set { this.Functionality.ButtonCancelText = value; } }


        [Obsolete("Use FormFields.Functionality")]
        public MyFileUpload AddFileUpload(string ID, string title, bool required, string helpMessage, int? width)
        {

            var row = this.Functionality.AddFileUpload(new FormFieldFileUploadData() { Width = width, ID = ID, Title = title, Required = required, HelpMessage = helpMessage });
            return (MyFileUpload)row.Functionality.Field;
        }




        [Obsolete("Use FormFields.Functionality")]
        public IMyFormWebControl AddDateTime(string id, string title, bool required, string helpMessage, int? width, DateTime? initValue)
        {

            MyLabel lbl;
            return AddDateTime(id, title, required, helpMessage, width, initValue, out lbl);
            //MyLabel label;
            //return addDateTime(id, title, required, helpMessage, width, false, null, null, false, initValue, null, false, out label)[0];

        }

        [Obsolete("Use FormFields.Functionality")]
        public IMyFormWebControl AddDateTime(string id, string title, bool required, string helpMessage, int? width, DateTime? initValue, out MyLabel label)
        {
            var row = this.Functionality.AddDateTime(new FormFieldDateData()
            {
                ID = id,
                Title = title,
                Required = required,
                HelpMessage = helpMessage,
                InitialValue = initValue
            });
            label = row.Functionality.Label;
            return row.Functionality.Field;


        }
        /*public List<MyTxtBoxDate> AddDateTime(string id, string title, bool required, string helpMessage, int? width,
            DateTime? fromDate, DateTime? toDate, bool hasTime, DateTime? initValue, bool nullable)
        {
            MyLabel label;
            return addDateTime(id, title, required, helpMessage, width, false, fromDate, toDate, hasTime, initValue, null, nullable, out label);
        }*/
        /*public List<MyTxtBoxDate> AddDateTime(string id, string title, bool required, string helpMessage, int? width,
            DateTime? fromDate, DateTime? toDate, bool hasTime, DateTime? initValueFrom, DateTime? initValueTo, bool nullable)
        {
            MyLabel label;
            return addDateTime(id, title, required, helpMessage, width, true, fromDate, toDate, hasTime, initValueFrom, initValueTo, nullable, out label);
        }*/

        public MyFileUpload GetControlAsMyFileUpload(string fieldID)
        {
            return (MyFileUpload)GetControl(fieldID);

        }

        public string ValidationGroup { get { return this.Functionality.ValidationGroup; } set { this.Functionality.ValidationGroup = value; }}

        #endregion


        public List<T> GetFormValueAsListOfEnums<T>(string fieldID, bool throwErrorIfDoesNotExist = true) where T: struct
        {
            string s = GetFormValueStr(fieldID, throwErrorIfNotExists: throwErrorIfDoesNotExist);
            return CS.General_v3.Util.EnumUtils.GetListOfEnumsByValuesFromString<T>(s);
        }

        public T GetFormValueAsEnum<T>(string fieldID, bool throwErrorIfDoesNotExist = true) where T : struct
        {
            var list = GetFormValueAsListOfEnums<T>(fieldID, throwErrorIfDoesNotExist: throwErrorIfDoesNotExist);
            return list.FirstOrDefault();
            
        }
        public T GetFormValueAsEnumNullable<T>(string fieldID, bool throwErrorIfDoesNotExist = true) 
        {
            string s = GetFormValueStr(fieldID, throwErrorIfNotExists: throwErrorIfDoesNotExist);
            var list = CS.General_v3.Util.EnumUtils.GetListOfEnumsByValuesFromString(typeof(T), s);
            if (list.Count > 0)
                return (T)(object)list[0];
            else
                return (default(T));



        }
    }
}
