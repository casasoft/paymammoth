﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Controls.WebControls.Specialized.OpeningHours;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data
{
    public class FormFieldOpeningHourItem : FormFieldBaseTypeData<MyOpeningHourItem.OpeningHoursData>
    {
        private MyOpeningHourItem _txt = new MyOpeningHourItem();
        
        public FormFieldOpeningHourItem()
        {
        }

        public override MyOpeningHourItem.OpeningHoursData GetFormValue()
        {
            return _txt.Value;
        }

        protected override Common.General.IMyFormWebControl createFieldControl()
        {
            return _txt;
        }
    }
}
