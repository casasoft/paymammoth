﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using System.Web.UI.WebControls;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data
{
    public class FormFieldDate3Combo : FormFieldBaseTypeData<DateTime?>
    {
        private MyTxtBoxDate3Combos _txt = new MyTxtBoxDate3Combos();

        public DateTime ShowDateFrom { get { return _txt.Functionality.ShowDateFrom; } set { _txt.Functionality.ShowDateFrom = value; } }
        public DateTime ShowDateTo { get { return _txt.Functionality.ShowDateTo; } set { _txt.Functionality.ShowDateTo = value; } }
        public FormFieldDate3Combo()
        {
         
            this.RowCssClassBase = "form-3-date-combo";
            init();
        }
        private void init()
        {
            this._validationParams.isDate = true;
        }

        protected override Common.General.IMyFormWebControl createFieldControl()
        {


            return _txt;
           
        }


        public override DateTime? GetFormValue()
        {
            return _txt.Value;
        }
    }
}
