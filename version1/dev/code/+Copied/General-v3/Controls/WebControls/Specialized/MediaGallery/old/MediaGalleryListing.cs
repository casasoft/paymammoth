﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Controls.Classes.MediaGallery;
using CS.General_v3.Util;
using CS.General_v3.Classes.MediaItems;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;
namespace CS.General_v3.Controls.WebControls.Specialized.MediaGallery
{
    [ToolboxData("<{0}:MediaGalleryListing runat=server></{0}:MediaGalleryListing>")]    
    public class MediaGalleryListing : CS.General_v3.Controls.WebControls.BaseWebControl
    {
        /*
         * How to use component:
         * 
         * Set the properties (for further info see their respective comments):
         * 
         * CurrentFolder (Only   if 'LoadParametersFromQuerystring' is set to false)
         * CurrentSection (Only   if 'LoadParametersFromQuerystring' is set to false) 
         * AllSections
         * CSSFile
         * ImageFolder
         * 
         * Optional Properties
         * -------------------
         * MediaGallery - The exposed item of type 'MediaGallery'
         * StatusMsgChanged
         * LoadParamtersFromQuerystring
         * FolderHasDescription
         * AllowSectionChoice
         * AllowAddNewFolder
         * SortBy
         * 
         *----------------------------------------------- 
         * 
         * 
         * 
         * 
         */
        public enum SORT_BY
        {
            Name,
            Priority,
            Date
        }
        
        public MediaGalleryListing()
        {
            this.MediaGallery = new MediaGallery();
            this.LoadParametersFromQueryString = true;
            this.SortBy = SORT_BY.Date;
            this.SortType = Enums.SORT_TYPE.Descending;
            this.DeleteIconCssClass = "media-gallery-button-delete";
            this.AllowSectionChoice = false;
            //this.AllowAddNewFolder = true;
            //this.AllowEditFolder = true;
            this.FolderHasDescription = false;

            this.String1Label = "";
            this.String2Label = "";
            this.String3Label = "";
            this.String1Required = false;
            this.String2Required = false;
            this.String3Required = false;
        }
        #region Constants / Statics
        public static string QUERYSTRING_PARAM_SECTION = "gallerySectionID";
        public static string QUERYSTRING_PARAM_FOLDER= "galleryFolderID";

        #endregion
        #region Delegates
        public delegate void StatusMsgChangedHandler(string msg);
        #endregion
        #region Events
        /// <summary>
        /// An event that is triggered once a new status message needs to be shown
        /// </summary>
        public event StatusMsgChangedHandler StatusMsgChanged;
        #endregion
        #region Methods
        private void getCurrentSectionFromURL()
        {

        }

        #endregion
        #region Properties

        public string String1Label { get; set; }
        public string String2Label { get; set; }
        public string String3Label { get; set; }
        
        public bool String1Required { get; set; }
        public bool String2Required { get; set; }
        public bool String3Required { get; set; }


        public MediaGallery MediaGallery { get; set; }
        /// <summary>
        /// Whether to load parameters from the query string or not
        /// </summary>
        public bool LoadParametersFromQueryString { get; set; }
        /// <summary>
        /// Whetehr folder has description or not
        /// </summary>
        public bool FolderHasDescription { get; set; }
        /// <summary>
        /// Whehter you can choose the section or not
        /// </summary>
        public bool AllowSectionChoice { get; set; }
        /// <summary>
        /// Whether you can add a new folder or not.  If you can add, you must attach to 'CreateNewFolder'.
        /// </summary>
        public bool AllowAddNewFolder2 { get; set; }
        /// <summary>
        /// The Current Folder.  If left null, the first root folder of 'CurrentSection' is taken
        /// </summary>
        public IGalleryFolder CurrentFolder { get; set; }
        /// <summary>
        /// The current section.  If left null, the first one of 'AllSections' is taken
        /// </summary>
        public IGallerySection CurrentSection { get; set; }
        /// <summary>
        /// All Sections
        /// </summary>
        public List<IGallerySection> AllSections { get; set; }
        /// <summary>
        /// The sort by of the folders
        /// </summary>
        public SORT_BY SortBy { get; set; }
        public CS.General_v3.Enums.SORT_TYPE SortType { get; set; }

        public string DeleteIconCssClass { get; set; }
        /// <summary>
        /// The CSS file to use
        /// </summary>
        public string CSSFile { get; set; }
        private string _ImageFolder = null;
        /// <summary>
        /// The location of the images.  Must end with a '/'.
        /// </summary>
        public string ImageFolder
        {
            get { return _ImageFolder; }
            set
            {
                _ImageFolder = value;
                if (!string.IsNullOrEmpty(_ImageFolder))
                {
                    if (!_ImageFolder.EndsWith("/"))
                        _ImageFolder += "/";
                }
                
            }
        }
        #endregion
        #region Delegates
        
        #endregion

        private void loadValuesFromQuerystring()
        {
            string sSectionID = CS.General_v3.Util.PageUtil.GetQueryStringVariable(QUERYSTRING_PARAM_SECTION);
            string sFolderID = CS.General_v3.Util.PageUtil.GetQueryStringVariable(QUERYSTRING_PARAM_FOLDER);
            int sectionID = 0;
            int folderID = 0;
            if (!Int32.TryParse(sSectionID, out sectionID))
                sectionID = 0;
            if (!Int32.TryParse(sFolderID , out folderID))
                folderID= 0;
            CurrentSection = null;
            if (sectionID > 0)
            {
                CurrentSection = this.AllSections.FindElem(s => s.ID == sectionID);
            }

            if (CurrentSection == null && this.AllSections != null && this.AllSections.Count > 0)
            {
                CurrentSection = this.AllSections[0];
            }
            CurrentFolder = null;
            if (folderID > 0 && CurrentSection != null)
            {
                CurrentFolder = CurrentSection.FindGalleryFolderByID(folderID);
            }
            if (CurrentFolder == null && CurrentSection != null)
            {
                List<IGalleryFolder> folders = CurrentSection.GetGalleryFolders();
                if (folders.Count > 0)
                    CurrentFolder = folders[0];
                else
                {
                    CurrentFolder = CurrentSection.CreateRootFolder();
                }
            }

        }

        protected override void OnInit(EventArgs e)
        {

            
            base.OnInit(e);
        }
        private void renderCSSFile()
        {
            HtmlGenericControl css = new HtmlGenericControl("link");
            this.Controls.Add(css);
            css.Attributes["rel"] = "stylesheet";
            css.Attributes["type"] = "text/css";
            css.Attributes["href"] = this.CSSFile;

        }
        private void renderSectionChoice()
        {
            if (this.AllowSectionChoice)
            {
                HtmlTable tbSection = new HtmlTable();
                this.Controls.Add(tbSection);
                tbSection.Attributes["class"] = "gallerySection";
                tbSection.CellPadding = 2;
                tbSection.CellSpacing = 0;

                HtmlTableRow tr;
                HtmlTableCell td;
                tr = new HtmlTableRow();
                tbSection.Rows.Add(tr);
                td = new HtmlTableCell(); tr.Cells.Add(td);
                HtmlGenericControl lblSectionName = new HtmlGenericControl("label");
                td.Controls.Add(lblSectionName);
                lblSectionName.InnerText = "Section:";
                td = new HtmlTableCell();
                tr.Cells.Add(td);
                WebControls.Common.MyDropDownList cmbSections = new MyDropDownList();
                foreach (var section in this.AllSections)
                {
                    cmbSections.Items.Add(new System.Web.UI.WebControls.ListItem(section.Name, section.ID.ToString()));
                
                }
                cmbSections.Width = 200;
                cmbSections.ID = "cmbSections";
                if (CurrentSection != null)
                    cmbSections.SetValue(CurrentSection.ID);
                td.Controls.Add(cmbSections);
                lblSectionName.Attributes["for"] = cmbSections.ID;
                cmbSections.SelectedIndexChanged += new EventHandler(cmbSections_SelectedIndexChanged);
                //cmbSections.TextChanged +=new EventHandler(cmbSections_TextChanged);

            }
        }

        void cmbSections_SelectedIndexChanged(object sender, EventArgs e)
        {
            MyDropDownList cmb = (MyDropDownList)sender;
            CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass();
            url[QUERYSTRING_PARAM_SECTION] = cmb.FormValue;
            url[QUERYSTRING_PARAM_FOLDER] = null;

            url.RedirectTo();
            
        }

        private void renderTopLinks()
        {
            CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass();

            HtmlGenericControl divTopLinks = new HtmlGenericControl("div");
            this.Controls.Add(divTopLinks);
            divTopLinks.Attributes["class"] = "galleryTopLinks";

            List<IGalleryFolder> topLinks = new List<IGalleryFolder>();
            IGalleryFolder curr = this.CurrentFolder;
            while (curr != null)
            {
                topLinks.Add(curr);
                curr = curr.GetParentFolder();
            }
            topLinks.Reverse();


            for (int i = 0; i < topLinks.Count; i++)
            {
                IGalleryFolder f = topLinks[i];
                url[QUERYSTRING_PARAM_FOLDER] = f.ID;
                

                HtmlGenericControl divTopLink = new HtmlGenericControl("div"); divTopLinks.Controls.Add(divTopLink);
                MyLink aLink = new MyLink(); divTopLink.Controls.Add(aLink);
                aLink.CssClass = "galleryTopLink";
                aLink.HRef = url.ToString();
                aLink.Text = f.Name;
                /*

                divTopLink.Attributes["class"] = "galleryTopLink";
                
                HtmlGenericControl divImage = new HtmlGenericControl("div");
                divImage.Attributes["class"] = "galleryTopLinkImage";
                divTopLink.Controls.Add(divImage);
                MyImage imgTopLink = new MyImage();
                divImage.Controls.Add(imgTopLink);
                imgTopLink.ID = "imgTopLink";
                imgTopLink.CssClass = "imgGalleryTopLinkImage";
                imgTopLink.ImageUrl = this.ImageFolder + "folder-16x16.gif";

                HtmlGenericControl divLink = new HtmlGenericControl("div");
                divLink.Attributes["class"] = "galleryTopLinkLink";
                divTopLink.Controls.Add(divLink);
                MyLink aTopLink = new MyLink();
                aTopLink.ID = "aTopLink";
                aTopLink.CssClass = "galleryTopLinkLink";
                aTopLink.Text = f.Name;
                divLink.Controls.Add(aTopLink);*/

                if (i != topLinks.Count - 1) //if not last item add raquo
                {
                    HtmlGenericControl divSep = new HtmlGenericControl("div");
                    divSep.Attributes["class"] = "galleryTopLinkSeparator";

                    divTopLinks.Controls.Add(divSep);
                    divSep.InnerHtml = "&raquo;";
                }
            }

            
        }
        private List<IGalleryFolder> SortFolderList(List<IGalleryFolder> list )
        {
            int sortMult = 1;
            if (this.SortType == Enums.SORT_TYPE.Descending)
                sortMult = -1;
            if (this.SortBy == SORT_BY.Date)
            {
                list.Sort((l1, l2) => (l1.FolderDate.CompareTo(l2.FolderDate) * sortMult));
            }
            else if (this.SortBy == SORT_BY.Name)
            {
                list.Sort((l1, l2) => (l1.Name.CompareTo(l2.Name) * sortMult));
            }
            else if (this.SortBy == SORT_BY.Priority)
            {
                list.Sort((l1, l2) => (l1.Priority.CompareTo(l2.Priority) * sortMult));
            }
            return list;
        }
        private void renderGalleryFolders()
        {
            
            HtmlGenericControl divGallery = new HtmlGenericControl("div"); this.Controls.Add(divGallery);
            divGallery.Attributes["class"] = "gallery";
            HtmlGenericControl header = new HtmlGenericControl("h2"); divGallery.Controls.Add(header);
            header.InnerText = "Folders";
            
            List<IGalleryFolder> list = CurrentFolder.GetSubFolders();
            list = SortFolderList(list);
            
            if (list.Count > 0)
            {
                foreach (var folder in list)
                {
                    HtmlGenericControl divFolder = new HtmlGenericControl("div"); divGallery.Controls.Add(divFolder);
                    divFolder.Attributes["class"] = "galleryItem";

                    HtmlTable tbFolder = new HtmlTable(); divFolder.Controls.Add(tbFolder);
                    tbFolder.CellSpacing = 0;
                    tbFolder.CellPadding = 3;

                    tbFolder.Attributes["class"] = "galleryItem";
                    HtmlTableRow tr;
                    HtmlTableCell td;

                    tr = new HtmlTableRow(); tbFolder.Rows.Add(tr);

                    td = new HtmlTableCell(); tr.Cells.Add(td);
                    td.Attributes["class"] = "galleryItemImage";
                    MyImage imgFolder = new MyImage(); imgFolder.ID = "imgFolder_" + folder.ID; td.Controls.Add(imgFolder);
                    imgFolder.CssClass = "galleryItemImage";
                    imgFolder.ImageUrl = ImageFolder + "folder-48x48.gif";

                    tr = new HtmlTableRow(); tbFolder.Rows.Add(tr);
                    td = new HtmlTableCell(); tr.Cells.Add(td);
                    td.Attributes["class"] = "galleryItemFolderName";
                    MyLink aFolderName = new MyLink(); td.Controls.Add(aFolderName);
                    aFolderName.ID = "aFolderName_" + folder.ID;
                    aFolderName.CssClass = "galleryItemFolderName";
                    aFolderName.Text = folder.Name + "<br /> (" + folder.FolderDate.ToString("dd/MMM/yy") + ")";

                    tr = new HtmlTableRow(); tbFolder.Rows.Add(tr);
                    td = new HtmlTableCell(); tr.Cells.Add(td);
                    td.Attributes["class"] = "galleryItemButtons";
                    //delete button
                    HtmlGenericControl divBtnDelete = new HtmlGenericControl("div"); td.Controls.Add(divBtnDelete);
                    divBtnDelete.Attributes["class"] = "galleryItemButton";
                    MyButton btnDelete = new MyButton();
                    divBtnDelete.Controls.Add(btnDelete);
                    btnDelete.ID = "btnFolderDelete_" + folder.ID;
                    btnDelete.Tag = folder;
                    //btnDelete.ImageUrl = ImageFolder + "icon_delete_up.png";
                    //btnDelete.ImageUrl = ImageFolder + "icon_delete_over.png";
                    btnDelete.CssManager.AddClass(DeleteIconCssClass);
                    btnDelete.Click +=new EventHandler(btnDelete_Click);
                    btnDelete.ConfirmMessage = "Are you sure you want to delete this folder, and all of its contents?";
                    btnDelete.Style["cursor"] = "pointer";
                    btnDelete.Visible = folder.CanDelete;
                    CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass();
                    url[QUERYSTRING_PARAM_FOLDER] = folder.ID;

                    string folderLink = url.ToString();
                    aFolderName.HRef = folderLink;
                    imgFolder.HRef = folderLink;



                }
            }
            else
            {
                HtmlGenericControl div = new HtmlGenericControl("div");
                div.Attributes["class"] = "divNoFolders";
                div.InnerText = "No Folders in selected folder";
                divGallery.Controls.Add(div);
            }
            HtmlGenericControl clearBR = new HtmlGenericControl("br");
            clearBR.Style["clear"] = "both";
            divGallery.Controls.Add(clearBR);

        }

        void btnDelete_Click(object sender, EventArgs e)
        {
            MyButton btn = (MyButton)sender;
            IGalleryFolder folder = (IGalleryFolder)btn.Tag;
            folder.Remove();
            if (StatusMsgChanged != null)
                StatusMsgChanged("Folder has been removed successfully");

            CS.General_v3.Classes.URL.URLClass.RedirectToSamePage();
            
        }
        private FormFields formEditFolder = null;
        private void fillFolderForm(IGalleryFolder folder, FormFields form)
        {
            form.AddString("txtName", "Folder Name", true, null, 400, (folder != null ? folder.Name : ""));
            form.AddInteger("txtPriority", "Priority", true, null, 50, (folder != null ? (int?)folder.Priority: 0));
            form.AddDateTime("txtDate", "Folder Date", true, "The date of the event", 150, (folder != null ? (DateTime?)folder.FolderDate : null));

            if (!string.IsNullOrEmpty(String1Label))
            {
                form.AddString("txtString1", String1Label, String1Required, null, 400, (folder != null ? folder.String1 : ""));
            }
            if (!string.IsNullOrEmpty(String2Label))
            {
                form.AddString("txtString2", String2Label, String2Required, null, 400, (folder != null ? folder.String2 : ""));
            }
            if (!string.IsNullOrEmpty(String3Label))
            {
                form.AddString("txtString3", String3Label, String3Required, null, 400, (folder != null ? folder.String3 : ""));
            }


            if (this.FolderHasDescription)
            {
                form.AddStringMultiline("txtDescription", "Description", false, null, 6, 400, null);
            }
            form.AddBool("chkFeatured", "Featured", true, (folder != null ? folder.IsFeatured : false));
        }
        private void renderEditFolder()
        {
            if (this.CurrentFolder.AllowEdit)
            {
                MyDiv div = new MyDiv(); this.Controls.Add(div);
                MyHeading h2 = new MyHeading(2, "Edit Folder"); div.Controls.Add(h2);
                formEditFolder = new FormFields("formEditFolder"); div.Controls.Add(formEditFolder);
                fillFolderForm(this.CurrentFolder, formEditFolder);
                formEditFolder.ClickSubmit += new EventHandler(formEditFolder_ClickSubmit);
            }
        }

        void formEditFolder_ClickSubmit(object sender, EventArgs e)
        {
            setFolderDetailsFromForm(this.CurrentFolder,formEditFolder);
            this.CurrentFolder.Save();
            
        }
        private void setFolderDetailsFromForm(IGalleryFolder folder, FormFields form)
        {
            folder.Name = form.GetFormValueStr("txtName");
            folder.FolderDate = form.GetFormValueDate("txtDate");
            folder.Priority = form.GetFormValueInt("txtPriority");
            folder.IsFeatured = form.GetFormValueBool("chkFeatured");
            if (!string.IsNullOrEmpty(String1Label))
            {
                folder.String1 = form.GetFormValueStr("txtString1");
            }
            if (!string.IsNullOrEmpty(String2Label))
            {
                folder.String2 = form.GetFormValueStr("txtString2");
            }
            if (!string.IsNullOrEmpty(String3Label))
            {
                folder.String3 = form.GetFormValueStr("txtString3");
            }
            if (this.FolderHasDescription)
            {
                folder.Description = form.GetFormValueStr("txtDescription");
            }
        }
        private FormFields formAddNewFolder = null;
        private void renderAddNewFolder()
        {
            if (this.CurrentFolder.AllowAddFolders)
            {
                
                HtmlGenericControl div = new HtmlGenericControl("div"); this.Controls.Add(div);
                HtmlGenericControl header = new HtmlGenericControl("h2"); div.Controls.Add(header);
                header.InnerText = "Add New Folder";
                formAddNewFolder = new FormFields("formAddNewFolder"); div.Controls.Add(formAddNewFolder);
                formAddNewFolder.ID = "formAddNewFolder";
                fillFolderForm(null,formAddNewFolder);
                formAddNewFolder.ClickSubmit += new EventHandler(formAddNewFolder_ClickSubmit);
            }
            
        }

        void formAddNewFolder_ClickSubmit(object sender, EventArgs e)
        {
            IGalleryFolder folder = CurrentFolder.CreateNewFolder();
            setFolderDetailsFromForm(folder, formAddNewFolder);
            

            folder.Save();
            if (StatusMsgChanged != null)
                StatusMsgChanged("Folder has been created successfully");
            CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass();
            url[QUERYSTRING_PARAM_FOLDER] = folder.ID;
            url.RedirectTo();
        }
        private void checkParameters()
        {
            StringBuilder sb = new StringBuilder();
            if (this.AllSections == null || this.AllSections.Count == 0)
            {
                sb.AppendLine("'AllSections' cannot be null or empty");
            }
            if (string.IsNullOrEmpty(CSSFile))
            {
                sb.AppendLine("'CSSFile' is required");

            }
            if (string.IsNullOrEmpty(this.ImageFolder))
            {
                sb.AppendLine("'ImageFolder' is required");
            }
            if (sb.Length > 0)
            {
                throw new InvalidOperationException("CS.General_v3.Controls.WebControls.Specialized.MediaGallery.MediaGalleryListing: \r\n\r\n" + sb.ToString());
            }
            
        }
        private void renderMediaGalleryItems()
        {
            if (CurrentFolder.AllowAddItems)
            {
                HtmlGenericControl div = new HtmlGenericControl("div"); this.Controls.Add(div);
                div.Attributes["class"] = "media";
                HtmlGenericControl header = new HtmlGenericControl("h2"); div.Controls.Add(header);
                header.Attributes["class"] = "header_media";
                header.InnerText = "Media";
                MediaGallery gallery = this.MediaGallery; div.Controls.Add(gallery);
                gallery.Functionality.StatusMessageUpdate += new MediaGallery.FUNCTIONALITY.StatusMessageUpdateHandler(gallery_StatusMessageUpdate);
               // gallery.DeleteSelectedMediaItemsClick += new MediaGallery.MediaItemsHandler(gallery_DeleteSelectedMediaItemsClick);
               // gallery.SaveCaptions += new MediaGallery.MediaItemsHandler(gallery_SaveCaptions);
                gallery.Functionality.UploadItems += new MediaGallery.FUNCTIONALITY.UploadItemsHandler(gallery_UploadItems);
                List<IMediaItem> list = CurrentFolder.GetMediaItems();

                foreach (var item in list)
                {
                    gallery.Functionality.AddItem(item);
                }

            }
        }

        void gallery_StatusMessageUpdate(string msg)
        {
            if (StatusMsgChanged != null)
            {
                StatusMsgChanged(msg);
            }
        }

        void gallery_UploadItems(List<MediaGalleryUploadItemFile> items)
        {
            foreach (var item in items)
            {
                
                if (item.Stream.Length  > 0)
                {
                    IMediaItem mediaItem = CurrentFolder.CreateNewMediaItem(item.Filename);
                    if (mediaItem != null)
                    {
                        mediaItem.SetMediaItem(item.Stream, item.Filename);
                        mediaItem.Caption = item.Caption;
                        

                        mediaItem.Save();
                    }
                }
            }
            if (StatusMsgChanged != null)
                StatusMsgChanged("Media uploaded successfully");
            CS.General_v3.Classes.URL.URLClass.RedirectToSamePage();
        }

        void gallery_SaveCaptions(List<IMediaItem> items)
        {
            foreach (var item in items)
            {
                item.Save();
            }
            if (StatusMsgChanged != null)
                StatusMsgChanged("Media captions updated successfully");
            CS.General_v3.Classes.URL.URLClass.RedirectToSamePage();
        }

        void gallery_DeleteSelectedMediaItemsClick(List<IMediaItem> items)
        {
            foreach (var item in items)
            {
                item.Delete();
            }
            if (StatusMsgChanged != null)
                StatusMsgChanged("Media deleted successfully");
            CS.General_v3.Classes.URL.URLClass.RedirectToSamePage();
        }
        
        protected override void OnLoad(EventArgs e)
        {
            if (LoadParametersFromQueryString)
                loadValuesFromQuerystring();
            checkParameters();
            renderCSSFile();
            renderSectionChoice();
            
            renderTopLinks();
            renderEditFolder();
            renderGalleryFolders();
            renderAddNewFolder();
            
                renderMediaGalleryItems();
            
            base.OnLoad(e);
        }

        


 
    }
}
