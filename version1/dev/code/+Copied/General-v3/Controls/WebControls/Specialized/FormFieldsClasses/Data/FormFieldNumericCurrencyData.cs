﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common.General;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data
{
    public class FormFieldNumericCurrencyData : FormFieldNumericIntegerData
    {
        public string CurrencySymbol { get { return _validationParams.currencySymbol; } set { _validationParams.currencySymbol = value; } }

        public FormFieldNumericCurrencyData()
        {
            _validationParams.isCurrency = true;
            this.RowCssClassBase = "form-row-currency";
        }
        protected override IMyFormWebControl createFieldControl()
        {
            throw new NotImplementedException();
        }
    }
}
