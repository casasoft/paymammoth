﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common.General;
using CS.General_v3.Controls.WebControls.Common;
using System.IO;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data
{
    public class FormFieldFileUploadData : FormFieldBaseTypeDataBase<string, Stream>
    {
        public new string InitialEmptyText
        {
            get
            {
                throw new NotSupportedException("File Upload does not support such property");
            }
            set
            {
                throw new NotSupportedException("File Upload does not support such property");

            }
        }
        public FormFieldFileUploadData()
        {

        }
        public new MyFileUpload GetField()
        {
            return (MyFileUpload)base.GetField();
        }
        protected override IMyFormWebControl createFieldControl()
        {
            MyFileUpload txt = new MyFileUpload();
            return txt;
        }
        public override Stream GetFormValue()
        {
            return GetField().FormValue;
        }
    }
}
