﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Classes.Interfaces.Hierarchy;

namespace CS.General_v3.Controls.WebControls.Specialized.Navigation
{
    public class NavigationSideMenu : MyDiv
    {
        public class FUNCTIONALITY
        {
            private NavigationSideMenu _control;

            public IHierarchyNavigation Item { get; set; }

            public FUNCTIONALITY(NavigationSideMenu control)
            {
                _control = control;
            }
        }

        public FUNCTIONALITY Functionality { get; private set; }

        public NavigationSideMenu()
        {
            this.Functionality = getFunctionality();
        }
        protected FUNCTIONALITY getFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private void initParent()
        {

            IHierarchy parent = null;
            if (this.Functionality.Item != null)
            {
                parent = this.Functionality.Item.GetParents().FirstOrDefault();
            }
            if (parent != null)
            {
                MyDiv divParent = new MyDiv();
                divParent.CssManager.AddClass("navigation-side-menu-parent-wrapper");
                MyAnchor aParent = new MyAnchor();
                getAnchor(aParent, parent);
                divParent.Controls.Add(aParent);
                this.Controls.Add(divParent);

                initSiblings(parent);
            }
        }

        private MyAnchor getAnchor(MyAnchor anchor, IHierarchy node)
        {
            if (node.GetParents().FirstOrDefault() != null)
            {
                anchor.Href = node.Href;
            }
            MySpan spanContent = new MySpan();
            spanContent.Attributes["class"] = "span-content";

            MySpan spanInnerContent = new MySpan();
            spanInnerContent.Attributes["class"] = "span-inner-content";

            spanInnerContent.InnerHtml = node.Title;
            spanContent.Controls.Add(spanInnerContent);
            anchor.Controls.Add(spanContent);
            return anchor;
        }

        private void initSiblings(IHierarchy parent)
        {
            MyDiv divNavigationSideMenu = new MyDiv();
            divNavigationSideMenu.CssManager.AddClass("navigation-side-menu-content");
            
            MyUnorderedList ulLevel0 = new MyUnorderedList();
            ulLevel0.CssManager.AddClass("navigation-side-menu-level0");

            var children = parent.GetChildren();
            foreach (var child in children)
            {
                MyListItem liLevel0Sibling = new MyListItem();
                MyAnchor aLevel0Sibling = new MyAnchor();
                liLevel0Sibling.Controls.Add(getAnchor(aLevel0Sibling, child));
                liLevel0Sibling.CssManager.AddClass("navigation-side-menu-level0-sibling");

                if (child.Visible)
                {
                    if (child.ID == this.Functionality.Item.ID)
                    {
                        liLevel0Sibling.CssManager.AddClass("selected");
                        aLevel0Sibling.CssManager.AddClass("selected");

                        if (child.GetChildren().Count() > 0)
                        {
                            liLevel0Sibling.CssManager.AddClass("navigation-side-menu-level1-parent");
                            initSubChildren(child, liLevel0Sibling);
                        }
                    }
                    ulLevel0.Controls.Add(liLevel0Sibling);
                    divNavigationSideMenu.Controls.Add(ulLevel0);
                    this.Controls.Add(divNavigationSideMenu);
                }
            }
        }

        private void initSubChildren(IHierarchy child, MyListItem liLevel0Sibling)
        {
            MyUnorderedList ulLevel1 = new MyUnorderedList();
            ulLevel1.CssManager.AddClass("navigation-side-menu-level1");

            var children = child.GetChildren();
            foreach (var subChild in children)
            {
                if (subChild.Visible)
                {
                    MyListItem liLevel1Sibling = new MyListItem();
                    liLevel1Sibling.CssManager.AddClass("navigation-side-menu-level1-sibling");
                    MyAnchor aLevel1Sibling = new MyAnchor();
                    liLevel1Sibling.Controls.Add(getAnchor(aLevel1Sibling, subChild));
                    ulLevel1.Controls.Add(liLevel1Sibling);
                }
            }
            liLevel0Sibling.Controls.Add(ulLevel1);
        }

        private void init()
        {
            initParent();
        }

        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }
    }
}


