﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.WebControls.Specialized.Reporting.v1
{
    public interface IBaseReportTableData : IBaseReportTableDataBase
    {
        new IEnumerable<IBaseReportTableHeaderData> HeadersTop { get; }
        new IEnumerable<IBaseReportTableHeaderData> HeadersLeft { get; }
        IBaseReportTableCellData TotalBothHeadersCell { get; }
        new IEnumerable<IBaseReportTableRowData> DataRows { get; }


    }
}
