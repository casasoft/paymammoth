﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.WebControls.Specialized.Paging
{
    public interface IParamsInfo
    {
        IPagingInfo PagingInfo { get; set; }
    }
}
