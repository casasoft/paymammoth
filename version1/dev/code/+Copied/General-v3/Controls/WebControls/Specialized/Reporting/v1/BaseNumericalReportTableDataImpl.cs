﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.WebControls.Specialized.Reporting.v1
{
    public class BaseNumericalReportTableDataImpl : BaseReportTableDataBaseImpl, IBaseNumericalReportTableData
    {

        public BaseNumericalReportTableDataImpl()
        {
            this.ShowTotalHorizontalAndVertical = this.ShowTotalsHorizontal = this.ShowTotalsVertical = true;
            this.ThousandsDelimiter = ",";
            this.DecimalPlaces = 2;
        }

        public new List<BaseNumericalReportTableHeaderDataImpl> HeadersTop
        {
            get { return (List<BaseNumericalReportTableHeaderDataImpl>)base.HeadersTop; }
        }
        public new List<BaseNumericalReportTableHeaderDataImpl> HeadersLeft
        {
            get { return (List<BaseNumericalReportTableHeaderDataImpl>)base.HeadersLeft; }
        }



        protected override IEnumerable<BaseReportTableRowDataBaseImpl> createDataRows()
        {
            return new List<BaseNumericalReportTableRowDataImpl>();
        }


        #region IBaseNumericalReportTableData Members

        public new List<BaseNumericalReportTableRowDataImpl> DataRows
        {
            get { return (List<BaseNumericalReportTableRowDataImpl>)base.DataRows; }
        }

        #endregion

        #region IBaseNumericalReportTableData Members

        IEnumerable<IBaseNumericalReportTableRowData> IBaseNumericalReportTableData.DataRows
        {
            get { return this.DataRows; }
        }

        #endregion


        protected override IEnumerable<BaseReportTableHeaderDataBaseImpl> createHeaderRows()
        {
            return new List<BaseNumericalReportTableHeaderDataImpl>();
        }

        #region IBaseNumericalReportTableData Members

        IEnumerable<IBaseNumericalReportTableHeaderData> IBaseNumericalReportTableData.HeadersTop
        {
            get { return this.HeadersTop; }
        }

        IEnumerable<IBaseNumericalReportTableHeaderData> IBaseNumericalReportTableData.HeadersLeft
        {
            get { return this.HeadersLeft; }
        }

        #endregion


        #region IBaseNumericalReportTableData Members


        public bool ShowTotalsVertical
        {
            get;
            set;
        }

        public bool ShowTotalsHorizontal
        {
            get;
            set;
        }

        public bool ShowTotalHorizontalAndVertical
        {
            get;
            set;
        }

        public int? DecimalPlaces
        {
            get;
            set;
        }

        public string ThousandsDelimiter
        {
            get;
            set;
        }

        #endregion
    }
}
