﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;

namespace CS.General_v3.Controls.WebControls.Specialized.Paging
{
    public class ListingResultsTitleAndCombos : MyDiv
    {
        public FUNCTIONALITY Functionality { get; set; }
        public class FUNCTIONALITY
        {
            private ListingResultsTitleAndCombos _ctrl;
            private bool _addClearBoth = true;

            public FUNCTIONALITY(ListingResultsTitleAndCombos ctrl)
            {
                ctrl = _ctrl;
                Combos = new Combos();
                ListingResultsTitle = new ListingResultsTitle();
            }

            public ListingResultsTitle ListingResultsTitle { get; set; }
            public Combos Combos { get; set; }
            public bool AddClearBothAtEndContainer
            {
                get
                {
                    return _addClearBoth;
                }
                set
                {
                    _addClearBoth = value;
                }
            }
        }

        public ListingResultsTitleAndCombos() 
        {
            Functionality = new FUNCTIONALITY(this);
            setCssClass();       
        }

        private void setCssClass()
        {
            CssClass = "listing-results-title-and-combos";
        }
        
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            addControls();
        }

        private void addControls()
        {
            if (Functionality.ListingResultsTitle != null)
            {
                Controls.Add(Functionality.ListingResultsTitle);
            }
            if (Functionality.Combos != null)
            {
                Controls.Add(Functionality.Combos);
            }
            addClearBoth();
        }

        private void addClearBoth()
        {
            if (Functionality.AddClearBothAtEndContainer)
            {
                MyDiv div = new MyDiv() { CssClass = "clear" };
                Controls.Add(div);
            }
        }

    }
}
