﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Util.Encoding.CSV;

namespace CS.General_v3.Controls.WebControls.Specialized.Reporting.v1
{
    internal class BaseReportSection : BaseReportSectionBase
    {

       

        #region BaseReportSection Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */

        public new class FUNCTIONALITY : BaseReportSectionBase.FUNCTIONALITY
        {
            public new IBaseReportSectionData Section { get { return (IBaseReportSectionData)base.Section; } set { base.Section = value; } }
                protected new BaseReportSection _item { get { return (BaseReportSection)base._item; } }

            internal FUNCTIONALITY(BaseReportSection item)
                : base(item)
            {

            }

        }
        public new FUNCTIONALITY Functionality { get { return (FUNCTIONALITY)base.Functionality; } }

        protected override BaseReportSectionBase.FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        #endregion
		
			

        
       // #region BaseReportSection Functionality Class

        /* Use this class in web controls, to create custom logic, so that they are seperated from the loads
         * of methods/properites already available to a web control */
        /*
        public class FUNCTIONALITY
        {
            public IBaseReportSectionDataBase Section { get; set; }
            protected BaseReportSection _item = null;
            internal FUNCTIONALITY(BaseReportSection item)
            {
                CS.General_v3.Util.ContractsUtil.RequiresNotNullable(item);
                this._item = item;
            }

        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        private FUNCTIONALITY _functionality = null;
        public FUNCTIONALITY Functionality { get { if (_functionality == null) { _functionality = createFunctionality(); } return _functionality; } }

        #endregion
		
			

        public BaseReportSection()
        {
        }
        
        private void initHeader()
        {
            MyDiv divHeader = new MyDiv();
            divHeader.CssManager.AddClass("cs-report-section-header-container");
            this.Controls.Add(divHeader);

            if (!string.IsNullOrWhiteSpace(this.Functionality.Section.Title))
            {
                MyHeading hTitle = new MyHeading(2, this.Functionality.Section.Title);
                hTitle.CssManager.AddClass("cs-report-section-title");
                divHeader.Controls.Add(hTitle);
            }
            if (!string.IsNullOrWhiteSpace(this.Functionality.Section.DescriptionHTML))
            {
                MyDiv divDesc = new MyDiv();
                divDesc.CssManager.AddClass("cs-report-section-desc-container");
                divDesc.InnerHtml = this.Functionality.Section.DescriptionHTML;

                divHeader.Controls.Add(divDesc);
            }
        }

        protected virtual BaseReportSectionTableBase getTableData()
        {
            return new BaseReportSectionTable();
        }

        private void initTableData()
        {
            MyDiv divTable = new MyDiv();
            divTable.CssManager.AddClass("cs-report-section-table-container");
            BaseReportSectionTableBase tbl = getTableData();
            tbl.Functionality.Data = this.Functionality.Section.TableData;
            divTable.Controls.Add(tbl);
            this.Controls.Add(divTable);
        }
        private void initFooter()
        {
            if (!string.IsNullOrWhiteSpace(this.Functionality.Section.FooterHTML))
            {
                MyDiv divFooter = new MyDiv();
                divFooter.CssManager.AddClass("cs-report-section-footer-container");
                divFooter.InnerHtml = this.Functionality.Section.FooterHTML;
                this.Controls.Add(divFooter);
            }
        }

        private void init()
        {
            initHeader();
            initTableData();
            initFooter();
        }
        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }*/


        protected override BaseReportSectionTableBase getTableData()
        {
            return new BaseReportSectionTable();
        }
    }
}
