﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;
using CS.General_v3.JavaScript.Interfaces;
namespace CS.General_v3.Controls.WebControls.Specialized.MediaGallery
{
    public class AjaxMediaGalleryItemCropData : IJavaScriptObject
    {
        /// <summary>
        /// E.g. Small Image / Medium Image / Large Image
        /// </summary>
        public string Title;
        /// <summary>
        /// The ID with which to communicate to the script
        /// </summary>
        public string CropID;
        /// <summary>
        /// The original image URL
        /// </summary>
        public string ImageURL;

        /// <summary>
        /// Aspect ratio of w/h (e.g. 1 for square) 
        /// 
        /// if the image is a normal 1024x768, then the ratio is 1024/768 = 4/3 = 1.333333333
        /// </summary>
        public double? AspectRatio;

        public int? MaxWidth;
        public int? MinWidth;
        public int? MaxHeight;
        public int? MinHeight;

  

        /* public string title;
        public string cropID;
        public string imageUrl;

        /// <summary>
        /// Aspect ratio of w/h (e.g. 1 for square) 
        /// 
        /// if the image is a normal 1024x768, then the ratio is 1024/768 = 4/3 = 1.333333333
        /// </summary>
        public double aspectRatio;
         */

        #region IJavaScriptObject Members

        public IJavaScriptSerializable GetJsObject(bool forFlash = false)
        {
            JSObject obj = forFlash ? new JSObjectFlash() : new JSObject();
            obj.AddProperty("title", Title);
            obj.AddProperty("cropID", CropID);
            obj.AddProperty("imageUrl", ImageURL);
            if (MaxWidth.HasValue) obj.AddProperty("maxWidth", MaxWidth);
            if (MinWidth.HasValue) obj.AddProperty("minWidth", MinWidth);
            if (MaxHeight.HasValue) obj.AddProperty("maxHeight", MaxHeight);
            if (MinHeight.HasValue) obj.AddProperty("minHeight", MinHeight);
            if (AspectRatio.HasValue)
            {
                obj.AddProperty("aspectRatio", AspectRatio.Value);
            }
            return obj;
        }

        #endregion
    }
}
