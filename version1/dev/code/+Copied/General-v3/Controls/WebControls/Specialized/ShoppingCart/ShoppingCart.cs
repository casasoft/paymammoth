﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using CS.General_v3.Controls.WebControls.Specialized.ShoppingCart;
using CS.General_v3.Controls.WebControls.Common;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace CS.General_v3.Controls.WebControls.Specialized.ShoppingCart
{

    public class ShoppingCart : MyPlaceHolder
    {

        public delegate void ValuesDelegate(Dictionary<long, int> newItems);
        public delegate void ValuesDelegateCheckOut(Dictionary<long, int> newItems, int deliveryID);
        public event ValuesDelegate OnUpdateValues;
        public event ValuesDelegateCheckOut OnProceedToCheckout;

        
        /// <summary>
        /// Private variables 
        /// </summary>
        private bool ShowDeliveryData;
        private MyTable _mainTable;
        private bool _editable { get; set; }
        private IList<IProductItem> _cartItems;
        private IList<IDeliveryMethod> _deliveryMethods;
        private int? _deliveryMethod;
        private double _totalPrice;
        private double _shippingPrice;
        private double _netPrice;

        private string _deliveryFeeText;
        public long? DeliveryID
        {
            get {
                if (list != null)
                {
                    var deliveryValue = list.GetFormValueAsStr();
                    if (!string.IsNullOrWhiteSpace(deliveryValue))
                    {
                        long shippingValue;
                        if (long.TryParse(deliveryValue, out shippingValue))
                        {
                            return shippingValue;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                return null;
            }
        }
        private MyDropDownList list;
        private bool ShowProductItemImage;
        private bool _itemsEditable;
        public static string DELIVERY_QUERY_STRING = "deliveryID";

        public ShoppingCart(IList<IProductItem> cartItems, bool editable, IList<IDeliveryMethod> deliveryMethods, ShoppingCartDetails details, bool showDeliveryData = true, bool showProductItemImage = true)
        {
            ShowDeliveryData = showDeliveryData;

            ShowProductItemImage = showProductItemImage;
            _cartItems = cartItems;
            _editable = editable;
            _deliveryMethods = deliveryMethods;
            if (details.DeliveryID.HasValue)
            {
                _deliveryMethod = details.DeliveryID;
            }

            _netPrice = details.NetPrice;
            _shippingPrice = details.ShippingCost;
            _totalPrice = details.TotalPrice;
            _deliveryFeeText = details.DeliveryFeeText;
            _itemsEditable = details.ItemsInCartEditable;

            init();
        }

        private void init()
        {
            initRender();
        }

        private void UpdateValues(Dictionary<long, int> newItems)
        {
            if (OnUpdateValues != null)
            {
                OnUpdateValues(newItems);
            }
        }

        private void ProceedToCheckout(Dictionary<long, int> finalItems, int deliveryID)
        {
            if (OnProceedToCheckout != null)
            {
                    OnProceedToCheckout(finalItems, deliveryID);
            }
        }

        private void initRender()
        {
            initTable();
            bool itemsFound;
            fillTable(out itemsFound);

            if (itemsFound)
            {
                renderBottomSection();
            }
        }

        private void renderBottomSection()
        {
            MyButton updQuantities = null;

            MyDiv divWrapper = new MyDiv();
            MyDiv divShoppingCartDetails = new MyDiv(cssClass: "shopping-cart-details-wrapper");
            MyDiv divShoppingCartDetailsQuantities = new MyDiv(cssClass: "shopping-cart-details-quantities");
            MyDiv divShoppingCartFooter = new MyDiv(cssClass: "shopping-cart-footer-wrapper");
            MyDiv divShoppingCartDetailsTable = new MyDiv(cssClass: "shopping-cart-details-table");

            HtmlGenericControl spanFeeData = renderDeliveryFeeData();
            MyButton generateProceed = null;
            MyDiv clearDiv = new MyDiv();
            MySpan spanProductDeleteNote = null;
            MyTable checkoutDataTable = null;

            if (_itemsEditable)
            {
                updQuantities = setUpdateQuantities();
                checkoutDataTable = getEditableCheckoutData();
                clearDiv.CssClass = "clear";
                generateProceed = generateProceedToCheckout();
                spanProductDeleteNote = generateDeleteProductNote();
            }
            else
            {
                checkoutDataTable = getNonEditableCheckoutData();

            }


            divShoppingCartDetailsQuantities.Controls.Add(spanFeeData);
            if (updQuantities != null)
            {
                divShoppingCartDetailsQuantities.Controls.Add(updQuantities);
            }

            divShoppingCartDetailsTable.Controls.Add(checkoutDataTable);
            divShoppingCartDetails.Controls.Add(divShoppingCartDetailsQuantities);
            divShoppingCartDetails.Controls.Add(divShoppingCartDetailsTable);
            divShoppingCartDetails.Controls.Add(new MyDiv(cssClass:"clear"));

            if (generateProceed != null)
            {
                divShoppingCartFooter.Controls.Add(generateProceed);
            }

            if (spanProductDeleteNote != null)
            {
                divShoppingCartFooter.Controls.Add(spanProductDeleteNote);
                divShoppingCartFooter.Controls.Add(clearDiv);
            }
            divWrapper.Controls.Add(divShoppingCartDetails);
            divWrapper.Controls.Add(divShoppingCartFooter);
            Controls.Add(divWrapper);
        }

        private MySpan generateDeleteProductNote()
        {
            MySpan span = new MySpan("cart-delete-product");
            span.InnerHtml = "Should you want to remove an item, set Qty. to 0 and click on Update Quantities";
            return span;
        }

        private MyButton generateProceedToCheckout()
        {
            MyButton proceedToCheckout = new MyButton("proceedToCheckout");
            proceedToCheckout.CssClass = "cart-proceed-checkout";
            proceedToCheckout.Text = "Proceed to Checkout";
            proceedToCheckout.Click += new EventHandler(proceedToCheckout_Click);
            
            return proceedToCheckout;
        }

        void proceedToCheckout_Click(object sender, EventArgs e)
        {
            Dictionary<long, int> newItems = getNewVals();
                ProceedToCheckout(newItems, list.FormValueAsInt);
        }


        private MyTable getEditableCheckoutData()
        {
            MyTable tbl = new MyTable();
            tbl.CssClass = "checkout-data-table";
            if (ShowDeliveryData)
            {
                MyTableRow delivRow = generateDeliveryRow();
                tbl.Rows.Add(delivRow);
            }
            else
            {
                generateDeliveryRow();
            }

            MyTableRow totalRow = generateTotalRow();

            tbl.Rows.Add(totalRow);
            return tbl;
        }


        private MyTableRow generateNetPriceRow()
        {
            MyTableRow netPriceRow = new MyTableRow();
            netPriceRow.Attributes["class"] = "cart-net-price-row";
            MyTableCell netPriceCellLabel = new MyTableCell();
            netPriceCellLabel.InnerText = "Items Total:";
            netPriceCellLabel.CssClass = "label";

            MyTableCell netPriceCellValue = new MyTableCell();
           // netPriceCellValue.InnerHtml =  CS.General_v3.Classes.Application.AppInstance.Instance.CurrentCultureManager.FormatPriceAsHTML(_netPrice);
            netPriceCellValue.CssClass = "NetPrice";
            netPriceRow.Cells.Add(netPriceCellLabel);
            netPriceRow.Cells.Add(netPriceCellValue);
            return netPriceRow;
        }

        private MyTableRow generateShippingRow()
        {
            MyTableRow shippingRow = new MyTableRow();
            shippingRow.Attributes["class"] = "cart-shipping-price-row";
            MyTableCell shippingPriceLabelCell = new MyTableCell();
            shippingPriceLabelCell.InnerText = "Shipping:";
            shippingPriceLabelCell.CssClass = "label";

            MyTableCell shippingPriceCell = new MyTableCell();
        //    shippingPriceCell.InnerHtml = CS.General_v3.Classes.Application.AppInstance.Instance.CurrentCultureManager.FormatPriceAsHTML(_shippingPrice);
            shippingPriceCell.CssClass = "shippingPrice";
            shippingRow.Cells.Add(shippingPriceLabelCell);
            shippingRow.Cells.Add(shippingPriceCell);
            return shippingRow;
        }

        private MyTableRow generateTotalRow()
        {
            MyTableRow totalRow = new MyTableRow();
            totalRow.Attributes["class"] = "cart-total-price-row";
            MyTableCell totalCell = new MyTableCell();
            totalCell.InnerText = "Total:";
            totalCell.CssClass = "label";

            MyTableCell totalCellValue = new MyTableCell();
            //totalCellValue.InnerHtml = CS.General_v3.Classes.Application.AppInstance.Instance.CurrentCultureManager.FormatPriceAsHTML(_totalPrice);
            totalCellValue.CssClass = "totalPrice";
            totalRow.Cells.Add(totalCell);
            totalRow.Cells.Add(totalCellValue);
            return totalRow;
        }

        private MyTableRow generateDeliveryRow()
        {
            if (_editable)
            {
                MyTableRow delivRow = new MyTableRow();
                delivRow.Attributes["class"] = "cart-delivery-row";
                MyTableCell deliveryCell = new MyTableCell();
                deliveryCell.InnerText = "Delivery:";
                deliveryCell.CssClass = "label";

                MyTableCell deliveryCellCombo = new MyTableCell();
                list = new MyDropDownList("ddlist", "Delivery", true);
                list.OnChangeRedirectToValue = true;
                string selectedDeliveryID = getSelectedIDFromQueryString();
                list.Items.Add(new ListItem() { Text = "Select a delivery method...", Value = "" });
                foreach (var item in _deliveryMethods)
                {

                    ListItem listIt = new ListItem()
                                          {
                                              Text = item.Text,
                                              Value = "?" + DELIVERY_QUERY_STRING + "=" + item.ID.ToString(),
                                              Selected = item.ID.ToString() == selectedDeliveryID
                                          };
                    list.Items.Add(listIt);
                }

                deliveryCellCombo.Controls.Add(list);
                delivRow.Cells.Add(deliveryCell);
                delivRow.Cells.Add(deliveryCellCombo);

                return delivRow;
            }
            else
            {
                    if (_deliveryMethod != null)
                        return generateDeliveryRowNonEditable();
            }
            return null;
        }

        private string getSelectedIDFromQueryString()
        {
            string id = HttpContext.Current.Request.QueryString[DELIVERY_QUERY_STRING];
            return id;
        }

        private MyTable getNonEditableCheckoutData()
        {
            
            MyTable tbl = new MyTable();
            tbl.CssClass = "checkout-data-table";
            
            if (ShowDeliveryData)
            {
                MyTableRow delivRow = generateDeliveryRow();
            tbl.Rows.Add(delivRow);
          

            }

            MyTableRow netPriceRow = generateNetPriceRow();
            tbl.Rows.Add(netPriceRow);

            MyTableRow shippingRow = generateShippingRow();
            tbl.Rows.Add(shippingRow);

            MyTableRow totalRow = generateTotalRow();
            tbl.Rows.Add(totalRow);
            return tbl;
        }

        private MyTableRow generateDeliveryRowNonEditable()
        {
            
                MyTableRow delivRow = new MyTableRow();
                delivRow.Attributes["class"] = "cart-delivery-row";
                MyTableCell deliveryCell = new MyTableCell();
                deliveryCell.InnerText = "Delivery:";
                deliveryCell.CssClass = "label";

                MyTableCell deliveryCellCombo = new MyTableCell();
                HtmlGenericControl span = new HtmlGenericControl("span");
                var selectedDeliveryText = _deliveryMethods.Where((c) => c.ID == _deliveryMethod).Select((c) => c.Text).FirstOrDefault();
                span.InnerText = selectedDeliveryText;

                deliveryCellCombo.Controls.Add(span);
                delivRow.Cells.Add(deliveryCell);
                delivRow.Cells.Add(deliveryCellCombo);
            
            return delivRow;
        }

        private HtmlGenericControl renderDeliveryFeeData()
        {
            HtmlGenericControl span = new HtmlGenericControl();
            span.Attributes["class"] = "cart-delivery-fee-data";
            span.InnerHtml = _deliveryFeeText;
            return span;
        }

        private MyButton setUpdateQuantities()
        {
            MyButton updQuantitesButton = new MyButton("updateQuantites");
            updQuantitesButton.Attributes["class"] = "cart-update-quantites";
            updQuantitesButton.Click += new EventHandler(updQuantitesButton_Click);
            updQuantitesButton.Text = "Update Quantites";
            return updQuantitesButton;
        }

        private void updQuantitesButton_Click(object sender, EventArgs e)
        {
            Dictionary<long, int> updatedItems = getNewVals();
            UpdateValues(updatedItems);
            var currentPageClass = CS.General_v3.Util.PageUtil.GetCurrentUrlLocation();
            string currentPage = currentPageClass.GetURL(fullyQualified: false);
            CS.General_v3.Util.PageUtil.RedirectPage(currentPage);
        }

        private Dictionary<long, int> getNewVals()
        {
            Dictionary<long, int> updatedItems = new Dictionary<long, int>();
            List<int> values = new List<int>();
            foreach (MyTableRow row in _mainTable.Rows)
            {

                foreach (MyTableCell cell in row.Cells)
                {
                    foreach (Control ctrl in cell.Controls)
                    {
                        if (ctrl.GetType().Equals(typeof(MyTxtBoxNumericInteger)))
                        {
                            MyTxtBoxNumericInteger refr = (MyTxtBoxNumericInteger)ctrl;
                            values.Add(int.Parse(refr.Text));
                        }
                    }
                }
            }

            int count = 0;
            foreach (var item in _cartItems)
            {
                updatedItems.Add(item.ID, values[count]);
                count += 1;
            }

            return updatedItems;
        }

        private void initTable()
        {
            _mainTable = new MyTable("shopping-cart-items-wrapper");
        }

        private void fillTable(out bool itemsFound)
        {
            if (_cartItems.Count > 0)
            {
                itemsFound = true;
                renderItems();
            }
            else
            {
                itemsFound = false;
            }
        }

        private void renderItems()
        {
            generateTitlesRow();

            int counter = 0;
            foreach (var item in _cartItems)
            {
                counter += 1;
                MyTableRow currRow = new MyTableRow();
                currRow.Attributes["class"] = "cart-item-wrapper";

                MyTableCell firstCell = generateFirstCell(item);
                MyTableCell secondCell = generateSecondCell(item);
                MyTableCell thirdCell = generateThirdCell(item);
                MyTableCell fourthCell = generateFourthCell(item);

                if (counter % 2 == 0)
                {
                    currRow.Attributes["class"] += " even";   
                }
                else
                {
                    currRow.Attributes["class"] += " odd";
                }
                currRow.Cells.Add(firstCell);
                currRow.Cells.Add(thirdCell);
                currRow.Cells.Add(secondCell);
                currRow.Cells.Add(fourthCell);
                _mainTable.Rows.Add(currRow);
            }
            Controls.Add(_mainTable);
        }

        private MyTableCell generateFourthCell(IProductItem item)
        {
            MyTableCell cell = new MyTableCell();
            HtmlGenericControl span = new HtmlGenericControl("span");
            span.Attributes["class"] = "cart-item-total-price";
          //  span.InnerHtml = CS.General_v3.Classes.Application.AppInstance.Instance.CurrentCultureManager.FormatPriceAsHTML(item.TotalPrice);
            cell.Controls.Add(span);
            return cell;
        }

        private MyTableCell generateThirdCell(IProductItem item)
        {
            MyTableCell cell = new MyTableCell();
            HtmlGenericControl span = new HtmlGenericControl("span");
            span.Attributes["class"] = "cart-item-unit-price";
          //  span.InnerHtml = CS.General_v3.Classes.Application.AppInstance.Instance.CurrentCultureManager.FormatPriceAsHTML(item.UnitPrice);
            cell.Controls.Add(span);
            return cell;
        }

        private MyTableCell generateSecondCell(IProductItem item)
        {
            MyTableCell cell = new MyTableCell();

            if (_itemsEditable)
            {
                MyTxtBoxNumericInteger quantityTextBox = new MyTxtBoxNumericInteger("quantityTextBox" + item.ID, "Quantity", true, null, null, null, MyTxtBoxNumeric.NUMERIC_RANGE.PositiveOnly, null, null);
                quantityTextBox.Attributes["class"] = "cart-item-quantity-textbox";
                quantityTextBox.Text = item.Quantity.ToString();
                cell.Controls.Add(quantityTextBox);
            }
            else {
                HtmlGenericControl span = new HtmlGenericControl("span");
                span.Attributes["class"] = "cart-item-quantity";
                span.InnerText = item.Quantity.ToString();
                cell.Controls.Add(span);
            }
            return cell;
        }

        private MyTableCell generateFirstCell(IProductItem item)
        {
            MyTableCell cell = new MyTableCell();

            MyDiv divImageWrapper = new MyDiv(cssClass: "cart-item-image-wrapper");
            MyDiv divClear = new MyDiv(cssClass: "clear");

            MyDiv divDetailsWrapper = new MyDiv(cssClass: "cart-item-details-wrapper");

            MyImage itemImage = new MyImage();
            MyAnchor anch = new MyAnchor();
            anch.InnerHtml = item.Name;
            anch.CssClass = "cart-item-name";

            HtmlGenericControl spn = new HtmlGenericControl("span");

            StringBuilder stb = new StringBuilder();

            if (!String.IsNullOrEmpty(item.Colour))
            {
                stb.Append("<strong>Colour:</strong> " + item.Colour + ", ");
            }
            if (!String.IsNullOrEmpty(item.Size))
            {
                stb.Append("<strong>Size:</strong> " + item.Size);
            }

            spn.InnerHtml = stb.ToString();
            spn.Attributes["class"] = "cart-item-details";
          

            itemImage.HRef = anch.Href = item.ItemURL;
            itemImage.HRefTarget = anch.HRefTarget = Enums.HREF_TARGET.Blank;
            
            itemImage.ImageUrl = item.ImageURL;
           
            if(String.IsNullOrEmpty(item.ImageURL))
            {
                itemImage.ImageUrl = "/images/no-image-thumbnail.jpg";
            }

            itemImage.CssClass = "cart-item-image";
            divImageWrapper.Controls.Add(itemImage);
            divImageWrapper.Controls.Add(divClear);

            if (ShowProductItemImage)
            {
                cell.Controls.Add(divImageWrapper);
            }

            divDetailsWrapper.Controls.Add(anch);
            divDetailsWrapper.Controls.Add(spn);

            cell.Controls.Add(divDetailsWrapper);

            return cell;
        }

        private void generateTitlesRow()
        {
            MyTableRow headerRow = new MyTableRow();
            headerRow.Cells.Add(new MyTableCell() { Text = "Product", CssClass = "product-row" });
            headerRow.Cells.Add(new MyTableCell() { Text = "Unit Price", CssClass = "unit-row" });
            headerRow.Cells.Add(new MyTableCell() { Text = "Qty.", CssClass = "quantity-row" });
            headerRow.Cells.Add(new MyTableCell() { Text = "Total", CssClass = "total-row" });
            headerRow.Attributes["class"] = "cart-title-row";
            _mainTable.Rows.Add(headerRow);
        }

    }
}
