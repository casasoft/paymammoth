﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common.General;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data
{
    public class FormFieldListSingleChoiceDropdownData : FormFieldListBaseTypeData<string>
    {
        public FormFieldListSingleChoiceDropdownData()
        {

            this.RowCssClassBase = "form-row-list";

        }
        public new MyDropDownList GetField()
        {
            return (MyDropDownList)base.GetField();
        }
        protected override IMyFormWebControl createFieldControl()
        {
            
                MyDropDownList _cmbList = new MyDropDownList();
                _cmbList.AddListItems(this.ListItems);
            return _cmbList;
        }
        public override string GetFormValue()
        {
            return GetField().FormValue;
        }
        
    }
}
