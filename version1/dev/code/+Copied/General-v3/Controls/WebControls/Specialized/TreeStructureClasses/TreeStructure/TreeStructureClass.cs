﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using CS.General_v3.Controls.WebControls.Common;

using CS.General_v3;
using CS.General_v3.Controls.WebControls.Classes;
using CS.General_v3.Classes.HelperClasses;

namespace CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructure
{
    [ToolboxData("<{0}:TreeStructureClass runat=server></{0}:TreeStructureClass>")]    
    public class TreeStructureClass : TreeStructureBasic.TreeStructureBasicClass
    {
        /* HOW TO USE
         * ----------
         * 
         * Parameters to fill
         * 
         *  - RootFolders
         *  - ImageFolder
         *  - EditFolderPageName
         *  - EditItemPageName
         *  - ItemTitle
         *  - FolderTitle
         *  - CSSFile
         *  
         * Optional
         *  
         *  - QuerystringFolderIDParam
         *  - QuerystringParentFolderIDParam
         *  - QuerystringItemIDParam
         */
        public new class FUNCTIONALITY : TreeStructureBasic.TreeStructureBasicClass.FUNCTIONALITY
        {
            protected new TreeStructureClass _tree
            {
                get
                {
                    return (TreeStructureClass) base._tree;
                }
            }
            public FUNCTIONALITY(TreeStructureClass tree)
                : base(tree)
            {
                //this._tree = tree;

                

                this.ItemTitle = "Item";
                this.PaddingPerLevel = 30;
                this.ShowPriorityAfterName = true;
            }
            #region Delegates
           
            #endregion
            #region Events
            public delegate void ShowMessageEvent(OperationResult result);
            public event ShowMessageEvent ShowMessage;
            #endregion
            #region Methods

            internal void showMessage(OperationResult msg)
            {
                if (ShowMessage != null)
                    ShowMessage(msg);
            }
            #endregion
            #region Properties
            public new IEnumerable<ITreeItem> RootItems
            {
                get
                {
                    return (IEnumerable<ITreeItem>)base.RootItems;
                }
                set
                {
                    base.RootItems = value;
                }
            }

            private ExtraButton GetExtraButton()
            {
                ExtraButton btn = new ExtraButton();
                btn.CssClass = "tree_button";
                return btn;
            }
            public static ITreeItem _temp = null;
            private void parseTreeItem(ITreeItem item)
            {
                ExtraButton extraBtn = null;
                if (!item.AddedExtraButtons)
                {
                    if (item.AllowUpdate)
                    {
                        extraBtn = GetExtraButton();
                        extraBtn.Href = item.EditURL;
                        extraBtn.ImageUrl_Over = this.ImageURL_Edit_Over;
                        extraBtn.ImageUrl_Up = this.ImageURL_Edit_Up;
                        extraBtn.Title = "Edit " + this.ItemTitle;
                        
                        item.ExtraButtons.Add(extraBtn);
                    }
                    else
                    {
                        item.ExtraButtons.Add(null);
                    }
                    if (item.AllowDelete)
                    {
                        extraBtn = GetExtraButton();
                        extraBtn.Click += new EventHandler(btnRootItem_Delete);

                        extraBtn.ImageUrl_Over = this.ImageURL_Delete_Over;
                        extraBtn.ImageUrl_Up = this.ImageURL_Delete_Up;
                        extraBtn.Title = "Delete " + this.ItemTitle + " <" + item.Title + ">";

                        extraBtn.ConfirmMessageOnClick = "Are you sure you want to delete " + this.ItemTitle + " <" + item.Title + ">?";//item.Message_ConfirmDelete;
                        
                        item.ExtraButtons.Add(extraBtn);
                    }
                    else
                    {
                        item.ExtraButtons.Add(null);
                    }
                    if (item.AllowAddSubItems)
                    {
                        extraBtn = GetExtraButton();
                        extraBtn.Href = item.AddNewItemURL;
                        extraBtn.ImageUrl_Over = this.ImageURL_AddSubItems_Over;
                        extraBtn.ImageUrl_Up = this.ImageURL_AddSubItems_Up;
                        extraBtn.Title = "Add new " + this.ItemTitle;
                        item.ExtraButtons.Add(extraBtn);
                    }
                    else
                    {
                        item.ExtraButtons.Add(null);
                    }
                    item.AddedExtraButtons = true;
                }
                foreach (var child in item.GetChildTreeItems())
                {
                    
                    parseTreeItem((ITreeItem) child);
                }
            }

            void btnRootItem_Delete(object sender, EventArgs e)
            {
                MyButton btn = (MyButton)sender;
                ITreeItem item = (ITreeItem)btn.Tag;
                var result = item.Remove();
                showMessage(result);

            }



            public override void RenderTree()
            {
                
                foreach (var item in this.RootItems)
                {
                    parseTreeItem(item);
                }
                

                //parseRootItems();
                base.RenderTree();
            }
           
            public string ImageURL_Edit_Up { get; set; }
            public string ImageURL_Edit_Over { get; set; }
            public string ImageURL_Delete_Up { get; set; }
            public string ImageURL_Delete_Over { get; set; }
            public string ImageURL_AddSubItems_Up { get; set; }
            public string ImageURL_AddSubItems_Over { get; set; }
            


            #endregion



            


        }
        public new FUNCTIONALITY Functionality
        {
            get
            {
                return (FUNCTIONALITY)base.Functionality;
            }
            
        }
        protected override TreeStructureBasic.TreeStructureBasicClass.FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }


        public TreeStructureClass()
        {
            
            
           
        }
        #region Constants / Statics
        

        #endregion
        

        protected override void OnInit(EventArgs e)
        {

            
            base.OnInit(e);
        }

        


 
    }
}
