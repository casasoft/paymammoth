﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.WebControls.Specialized.RotatingBanner.v1
{
    public class FlashRotatingBannerV1ItemData
    {
        public string ItemURL { get; set; }
        public string ItemLink { get; set; }
        public CS.General_v3.Enums.HREF_TARGET ItemLinkTarget { get; set; }
        public double DelaySeconds { get; set; }
        public double SwfFilePlaybackDurationLengthSeconds { get; set; }
        public uint? SwfFileBackgroundColor { get; set; }

        public FlashRotatingBannerV1ItemData()
        {
            this.ItemLinkTarget = Enums.HREF_TARGET.Self;
        }

       /* var url:String = li.parameters["itemURL" + count];
				var link:String = li.parameters["itemLink" + count];
				var linkTarget:String = li.parameters["itemLinkTarget" + count];
				var delaySeconds:Number = parseFloat(li.parameters["delaySeconds" + count]);?*/
    }
}
