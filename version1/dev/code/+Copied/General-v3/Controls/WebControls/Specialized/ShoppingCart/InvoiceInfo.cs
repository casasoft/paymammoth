﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.WebControls.Specialized.ShoppingCart
{
    
    public class InvoiceInfo
    {
        
        public string Date { get; set; }
        public string AuthorizationCode { get; set; }
        public string DeliveryDetails { get; set; }
        public string TopImageUrl { get; set; }
    }

}
