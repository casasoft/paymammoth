﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Controls.WebControls.Common.General;

namespace CS.General_v3.Controls.WebControls.Specialized.ListingClasses
{
    public class ListingItemTextLinkButton : MyAnchor, IListingButtonInfo
    {
        public CS.General_v3.Controls.WebControls.Specialized.Listing.ItemHrefRetriever HrefRetriever { get; set; }
        public ListingItemTextLinkButton(CS.General_v3.Controls.WebControls.Specialized.Listing.ItemHrefRetriever HrefRetriever)
        {
            
            this.HrefRetriever = HrefRetriever;
        }

        public string ValidationGroup { get; set; }
        public bool NoValidation
        {
            get
            {
                return true;
            }
            set
            {
               
            }
        }

        public new string Text
        {
            get
            {
                return CS.General_v3.Util.Text.HtmlDecode(this.InnerHtml);
            }
            set
            {
                this.InnerHtml = CS.General_v3.Util.Text.HtmlEncode(value);
                
            }
        }

        public object Tag { get; set; }

        #region IListingButtonInfo Members


        public string IconImageUrl
        {
            get;
            set;
        }

        #endregion
    }
        
}
