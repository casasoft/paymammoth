﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Controls.WebControls.Common.General;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data
{
    public class FormFieldStringPasswordData : FormFieldStringData
    {

        protected new FormFieldsValidationParamsPassword _validationParams
        {
            get
            {
                return (FormFieldsValidationParamsPassword)base._validationParams;
            }
        }

        public FormFieldStringPasswordData()
        {

            this.RowCssClassBase = "form-row-password";

        }

        protected override FormFieldsValidationParams createValidationParams()
        {
            return new FormFieldsValidationParamsPassword();
        }

        public new MyTxtBoxTextPassword GetField()
        {
            return (MyTxtBoxTextPassword)(IMyFormWebControl)base.GetField();
        }
        protected override IMyFormWebControl createFieldControl()
        {
                MyTxtBoxTextPassword _txtText = new MyTxtBoxTextPassword();
                _txtText.AutoCompleteDisabled = this.AutoCompleteDisabled;
            return _txtText;
        }
        public string PasswordCannotBeLikeFieldID { get { return _validationParams.passwordCannotBeLikeFieldID; } set { _validationParams.passwordCannotBeLikeFieldID = value; } }
        public string PasswordCannotBeLikeFieldErrorMsg { get { return _validationParams.passwordCannotBeLikeFieldErrorMsg; } set { _validationParams.passwordCannotBeLikeFieldErrorMsg = value; } }
        public bool ShowPasswordStrength { get { return _validationParams.showPasswordStrength; } set { _validationParams.showPasswordStrength = value; } }
        public PASSWORD_STRENGTH_CHARACTERS PasswordStrengthType { get { return _validationParams.passwordStrengthType; } set { _validationParams.passwordStrengthType = value; } }
        
    }
}
