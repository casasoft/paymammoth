﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.JavaScript.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.General_v3.Controls.WebControls.Specialized.Tooltip
{
    public class ControlTooltipWithHtml : ControlTooltipWithContent
    {
        public class ControlTooltipWithHtmlJSParams : ControlTooltipWithContentJSParams
        {

        }


        public new class FUNCTIONALITY : ControlTooltipWithContent.FUNCTIONALITY
        {
            protected new ControlTooltipWithHtml _control { get { return (ControlTooltipWithHtml)base._control; } set { base._control = value; } }

            public new ControlTooltipWithHtmlJSParams Parameters { get { return (ControlTooltipWithHtmlJSParams)base.Parameters; } protected set { base.Parameters = value; } }
            public string TooltipText { get; set; }

            public new Control TooltipContent { get { throw new InvalidOperationException("Tooltip content is set automatically, just set TooltipText"); } set { throw new InvalidOperationException("Tooltip content is set automatically, just set TooltipText"); } }
            
            public FUNCTIONALITY(ControlTooltipWithHtml control):base(control)
            {
                this.Parameters = new ControlTooltipWithHtmlJSParams();
            }

            internal void setBaseTooltipContent(Control content)
            {
                base.TooltipContent = content;
            }
        }
        public new FUNCTIONALITY Functionality { get { return (FUNCTIONALITY)base.Functionality; } }
        public ControlTooltipWithHtml()
        {

        }

        protected override ControlTooltip.FUNCTIONALITY getFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

      
        protected override void OnPreRender(EventArgs e)
        {
            if (string.IsNullOrEmpty(this.Functionality.TooltipText))
            {
                throw new Exception("Please specify TooltipText");
            }
            this.Functionality.setBaseTooltipContent(new Literal() { Text = this.Functionality.TooltipText });
            base.OnPreRender(e);
        }

    }
}
