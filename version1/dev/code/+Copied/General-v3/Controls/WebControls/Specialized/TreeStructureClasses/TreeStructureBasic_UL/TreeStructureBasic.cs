﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CS.General_20090518.Controls.WebControls.Common;
using CS.General_20090518.Controls.Classes.MediaGallery;
using CS.General_20090518;
namespace CS.General_20090518.Controls.WebControls.Specialized.TreeStructure.TreeStructureBasic
{
    [ToolboxData("<{0}:TreeStructureBasic runat=server></{0}:TreeStructureBasic>")]    
    public class TreeStructureBasic : CS.General_20090518.Controls.WebControls.BaseWebControl
    {
        /* HOW TO USE
         * ----------
         * 
         * Parameters to fill
         * 
         *  - RootFolders
         *  - ImageFolder
         *  - EditFolderPageName
         *  - EditItemPageName
         *  - ItemTitle
         *  - FolderTitle
         *  - CSSFile
         *  
         * Optional
         *  
         *  - QuerystringFolderIDParam
         *  - QuerystringParentFolderIDParam
         *  - QuerystringItemIDParam
         */
        public class FUNCTIONALITY
        {
            private TreeStructureBasic _tree = null;
            public FUNCTIONALITY(TreeStructureBasic tree)
            {
                this._tree = tree;

                

                this.ItemTitle = "Item";
                this.PaddingPerLevel = 30;
                this.ShowPriorityAfterName = true;
            }
            #region Delegates
            
            #endregion
            #region Events
            

            #endregion
            #region Methods
            

            #endregion
            #region Properties
            public bool ShowPriorityAfterName { get; set; }
            /// <summary>
            /// Root Folders
            /// </summary>
            public List<ITreeItemBasic> RootItems { get; set; }
            /// <summary>
            /// Location of CSS file
            /// </summary>
            public string CSSFile { get; set; }
            /// <summary>
            /// A description of the item that this tree structure is portraying, e.g Category
            /// </summary>
            public string ItemTitle { get; set; }
            /// <summary>
            /// Padding per level, pixels
            /// </summary>
            public int PaddingPerLevel { get; set; }
            /*
            public string ImageURL_Edit_Up { get; set; }
            public string ImageURL_Edit_Over { get; set; }
            public string ImageURL_Delete_Up { get; set; }
            public string ImageURL_Delete_Over { get; set; }
            public string ImageURL_Add_Up { get; set; }
            public string ImageURL_Add_Over { get; set; }
            */


            #endregion



            private void renderCSSFile()
            {
                HtmlGenericControl css = new HtmlGenericControl("link");
                _tree.Controls.Add(css);
                css.Attributes["rel"] = "stylesheet";
                css.Attributes["type"] = "text/css";
                css.Attributes["href"] = this.CSSFile;

            }

            private BaseWebControl ulTree = null;
            

            private int ctrlIndex = 0;
            private int totalMaxExtraButtons = 0;
            private void countTotalExtraButtons(ITreeItemBasic curr)
            {
                if (curr != null && curr.ExtraButtons != null && curr.ExtraButtons.Count > totalMaxExtraButtons)
                {
                    totalMaxExtraButtons = curr.ExtraButtons.Count;
                }
                List<ITreeItemBasic> list = null;
                if (curr == null)
                {
                    list = this.RootItems;
                }
                else
                {
                    list = curr.GetChildTreeItems();
                }

                foreach (var item in list)
                {
                    countTotalExtraButtons(item);
                }

            }

            private HtmlTableCell createButtonTd()
            {
                HtmlTableCell td = new HtmlTableCell();
                td.Style["width"] = "20px";
                td.Attributes["class"] = "button";
                return td;

            }
            private void processHrefButton(MyImage img, string imageUp, string imageOver)
            {
                img.Style["cursor"] = "pointer";
                img.ImageUrl = imageUp;
                img.RollOverImage = imageOver;



            }
            private void addExtraButtons(HtmlTableRow tr, ITreeItemBasic item)
            {
                if (item.ExtraButtons != null)
                {
                    int index = 0;
                    int emptyCells = this.totalMaxExtraButtons - item.ExtraButtons.Count;
                    for (int i = 0; i < emptyCells; i++)
                    {
                        HtmlTableCell td = createButtonTd();
                        td.InnerHtml = "&nbsp;";
                        tr.Cells.Insert(index, td); index++;
                        
                    }
                    for (int i=0; i < item.ExtraButtons.Count; i++)
                    {
                        var btn = item.ExtraButtons[i];

                        HtmlTableCell td = createButtonTd();
                        if (btn != null)
                        {
                            MyImageButton imgBtn = new MyImageButton(_tree.ID + "_extrabutton_" + item.ID + "_" + i);
                            if (btn.clickHandler != null)
                                imgBtn.Click += btn.clickHandler;
                            else
                            {
                                imgBtn.FunctionAsImage = true;
                                imgBtn.HRef = btn.Href;
                            }
                            if (!string.IsNullOrEmpty(btn.CssClass))
                                imgBtn.CssClass = btn.CssClass;
                            else
                                imgBtn.CssClass = "button";
                            imgBtn.ConfirmMessage = btn.ConfirmMessageOnClick;
                            imgBtn.ImageUrl = btn.ImageUrl_Up;
                            imgBtn.RollOverImage = btn.ImageUrl_Over;
                            
                            imgBtn.AlternateText = btn.Title;
                            imgBtn.Tag = item;
                            td.Controls.Add(imgBtn);
                        }
                        else
                        {
                            td.InnerHtml = "&nbsp;";
                        }
                        tr.Cells.Insert(index, td); index++;
                    }
                }
            }
            private HtmlTable createButtonTable(ITreeItemBasic parentItem, ITreeItemBasic item)
            {
                
                HtmlTable tbButtons = new HtmlTable(); tbButtons.Attributes["class"] = "buttons";
                HtmlTableRow trButtons = new HtmlTableRow(); tbButtons.Rows.Add(trButtons); trButtons.Attributes["class"] = "buttons";
                /*
                if (oneHasAllowUpdate)
                {
                    HtmlTableCell tdButton = createButtonTd(); trButtons.Cells.Add(tdButton);
                    tdButton.InnerHtml = "&nbsp;";
                    if (item.AllowAdd)
                    {
                        tdButton.Controls.Clear();
                        MyImage img = new MyImage(); tdButton.Controls.Add(img);
                        processHrefButton(img, this.ImageURL_Add_Up, this.ImageURL_Add_Over);
                        img.AlternateText = "Add new " + ItemTitle;
                        img.HRef = item.AddNewItemURL;

                    }
                }
                if (oneHasAllowUpdate)
                {
                    HtmlTableCell tdButton = createButtonTd(); trButtons.Cells.Add(tdButton);
                     tdButton.InnerHtml = "&nbsp;";
                    if (item.AllowUpdate)
                    {
                        tdButton.Controls.Clear();
                        MyImage img = new MyImage(); tdButton.Controls.Add(img);
                        processHrefButton(img, this.ImageURL_Edit_Up,this.ImageURL_Edit_Over);
                        img.AlternateText = "Edit " + item.Title;
                        img.HRef = item.EditURL;
                        
                    }
                }
                if (oneHasAllowDelete)
                {
                    HtmlTableCell tdButton = createButtonTd(); trButtons.Cells.Add(tdButton);
                    tdButton.Attributes["class"] = "button"; tdButton.InnerHtml = "&nbsp;";
                    
                    if (item.AllowDelete)
                    {
                        tdButton.Controls.Clear();

                        MyImageButton btnDelete = new MyImageButton();
                        tdButton.Controls.Add(btnDelete);
                        btnDelete.Style["cursor"] = "pointer";
                        btnDelete.ImageUrl = this.ImageURL_Delete_Up;
                        btnDelete.RollOverImage = this.ImageURL_Delete_Over;
                        btnDelete.AlternateText = "Delete " + item.Title;
                        btnDelete.ID = _tree.ID + "_btnDelete_" + ctrlIndex;
                        btnDelete.Tag = item;
                        btnDelete.ConfirmMessage = !string.IsNullOrEmpty(item.Message_Confirm) ? item.Message_Confirm : "Are you sure you want to delete '" + item.Title + "'?";
                        btnDelete.Click += new EventHandler(btnDelete_Click);
                    }
                    

                }
                */
                addExtraButtons(trButtons, item);
                return tbButtons;
            }
            /*
            void btnDelete_Click(object sender, EventArgs e)
            {
                int k = 5;
                MyImageButton btn = (MyImageButton)sender;
                ITreeItem item = (ITreeItem)btn.Tag;
                item.Remove();
                string msg = !string.IsNullOrEmpty(item.Message_Delete) ? item.Message_Delete : "'" + item.Title + "' deleted successfully";
                showMessage(msg);
                CS.General_20090518.Util.Page.URL.RedirectToSamePage();
            }

            */

            private void createDetails(BaseWebControl liDetails, ITreeItemBasic parentItem, ITreeItemBasic item, int level)
            {
                
                
                liDetails.CssManager.AddClass("tree","details");
                //MyDiv divDetails = new MyDiv(); liDetails.Controls.Add(divDetails); liDetails.CssManager.AddClass("details");

                MyImage img = new MyImage(); img.ID = "imgIcon_" + ctrlIndex; liDetails.Controls.Add(img);
                img.ImageUrl = item.ImageURL;
                img.AlternateText = item.Title;
                MyDiv divTitle = new MyDiv(); liDetails.Controls.Add(divTitle); divTitle.CssManager.AddClass("details-title");
                //td = new HtmlTableCell(); tr.Cells.Add(td); td.Attributes["class"] = "tree name";
                string name = item.Title;
                if (ShowPriorityAfterName && item.Priority != null)
                    name += " [" + item.Priority + "]";
                
                
                divTitle.InnerHtml = System.Web.HttpUtility.HtmlEncode(name);
                

                
            }
            private BaseWebControl renderItem(ITreeItemBasic parentItem, ITreeItemBasic item, int level)
            {
                BaseWebControl li = new BaseWebControl(HtmlTextWriterTag.Li);
                item.AddExtraButtons();
                li.CssManager.AddClass("tree-details");


                createDetails(li, parentItem, item, level);

                li.Controls.Add(createButtonTable(parentItem,item));
                ctrlIndex++;
                List<ITreeItemBasic> list = item.GetChildTreeItems();
                if (list.Count > 0)
                {
                    BaseWebControl ul = new BaseWebControl(  HtmlTextWriterTag.Ul);

                    foreach (var child in list)
                    {
                        ul.Controls.Add(renderItem(item, child, level + 1));
                    }
                    li.Controls.Add(ul);
                }
                return li;
            }

            private void renderTree()
            {
                ulTree = new BaseWebControl(HtmlTextWriterTag.Ul); _tree.Controls.Add(ulTree); ulTree.ID = "ulTree_" + _tree.ID;
                ulTree.CssManager.AddClass("tree");
                var list = this.RootItems;
                foreach (var f in list)
                {
                    ulTree.Controls.Add(renderItem(null, f, 0));
                }


            }

            private void renderPriorityNote()
            {
                if (ShowPriorityAfterName)
                {
                    CS.General_20090518.Controls.WebControls.Common.MyParagraph p = new MyParagraph("Note: The number in brackets is the priority value");
                    p.Style["border"] = "solid 1px #1CA1F9";
                    p.Style["background-color"] = "#F2F6F9";
                    p.Style["padding"] = "4px";
                    _tree.Controls.Add(p);
                }
            }

            /// <summary>
            /// Renders the tree
            /// </summary>
            public virtual void RenderTree()
            {
                checkParameters();
                countTotalExtraButtons(null);
                //checkAllowItems(null);
                renderCSSFile();
                renderTree();
                renderPriorityNote();
            }
            public virtual void checkParameters()
            {
                StringBuilder sb = new StringBuilder();
                if (string.IsNullOrEmpty(this.CSSFile))
                    sb.AppendLine("Please specify 'CSSFile'");
                if (this.RootItems == null)
                {
                    sb.AppendLine("Please set 'RootItems'");
                }
                if (sb.Length > 0)
                {
                    string s = "TreeStructure.TreeStructure:: " + sb.ToString();
                    throw new InvalidOperationException(s);
                }
            }
            


        }
        public FUNCTIONALITY Functionality {get; set;}



        public TreeStructureBasic() : base("div")
        {
            this.Functionality = new FUNCTIONALITY(this);
           
        }
        #region Constants / Statics
        

        #endregion
        

        protected override void OnInit(EventArgs e)
        {

            
            base.OnInit(e);
        }

        


 
    }
}
