﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using CS.General_20101215;

using System.Web.UI;
using CS.General_20101215.Controls.Classes.Forms;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CS.General_20101215.Controls.WebControls.Common;
using CS.General_20101215.Controls.WebControls;
using CS.General_20101215.Controls.WebControls.Common.General;
using CS.General_20101215.Controls.Classes.MediaGallery;
namespace CS.General_20101215.Controls.WebControls.Specialized
{

    [ToolboxData("<{0}:FormFields runat=server></{0}:FormFields>")]    
    public class FormFields : BaseWebControl
    {
        private class FileUploadButtons
        {
            public MyImageButtonWithText BtnDelete { get; set; }
            public MyButton BtnUpload { get; set; }
            
        }

        private string _deleteImageButton = "/cms/common/imag2es/icon_delete_up.png";


        public FormFields()
        {
            this.ButtonSubmitText = "Submit";
            this.ButtonCancelText = "Cancel";
        }
        private List<IMyFormWebControl> _controls = new List<IMyFormWebControl>();
        protected List<FormFieldItem> _items = new List<FormFieldItem>();

        public string ButtonSubmitText { get; set; }
        public string ButtonCancelText { get; set; }

        protected HtmlTable _tableFields;
        protected MyButton _btnSubmit;
        protected MyButton _btnCancel;


        public delegate void MediaItemHandler(IMediaItem mediaItem);
        public delegate void MediaItemUploadHandler(MyFileUploadWithMediaItem fileUpload);

        public event EventHandler ClickSubmit;
        public event EventHandler ClickCancel;
        


        private bool containButtons
        {
            get
            {
                return !String.IsNullOrEmpty(ButtonSubmitText) || !String.IsNullOrEmpty(ButtonCancelText);
            }
        }

        /// <summary>
        /// Creates the textbox controls, add it to the table cell and adds it to the list of items
        /// </summary>
        /// <param name="field"></param>
        /// <param name="tr"></param>
        /// <param name="td"></param>
        /// <returns></returns>
        protected virtual TinyMCE addTinyMCE(TinyMCE tinyMCE, HtmlTableRow tr, HtmlTableCell td)
        {
            tinyMCE.Attributes["class"] = "tinymce";


            td.Controls.Add(tinyMCE);
            if (!String.IsNullOrEmpty(tinyMCE.HelpMsg))
            {
                tinyMCE.Attributes["helpMessage"] = tinyMCE.HelpMsg;
            }





            return tinyMCE;
        }


        /// <summary>
        /// Creates the textbox controls, add it to the table cell and adds it to the list of items
        /// </summary>
        /// <param name="field"></param>
        /// <param name="tr"></param>
        /// <param name="td"></param>
        /// <returns></returns>
        protected virtual MyTxtBox addTextBox(MyTxtBox txt, HtmlTableRow tr, HtmlTableCell td)
        {
            
           

            if (txt is MyTxtBoxText)
            {
                
                
              

                
                if (txt is MyTxtBoxTextMultiLine)
                {
                    td.Attributes["class"] = "multiline";
                    
                    

                }
                else if (txt is MyTxtBoxTextPassword)
                {
                    td.Attributes["class"] = "password";
                }
                else if (txt is MyTxtBoxEmail)
                {
                    td.Attributes["class"] = "email";
                    
                }
                else if (txt is MyTxtBoxWebsite)
                {
                    td.Attributes["class"] = "website";
                }
            }
            else if (txt is MyTxtBoxNumeric)
            {
                td.Attributes["class"] = "numeric";
                

            }
            else if (txt is MyTxtBoxDate)
            {
                td.Attributes["class"] = "date";
                if (txt.HelpMsg == null)
                {
                    txt.HelpMsg = "Please enter a date in the format - dd/mm/yyyy";
                }
            }

            td.Controls.Add(txt);
            if (!String.IsNullOrEmpty(txt.HelpMsg))
            {
                txt.Attributes["helpMessage"] = txt.HelpMsg;
            }
            _items.Add(new FormFieldItem(tr, txt));
            
            



            return txt;
        }

        private MyDropDownList addDropdownList(MyDropDownList control, HtmlTableRow tr, HtmlTableCell td)
        {
            td.Attributes["class"] = "select";
            td.Controls.Add(control);
            _items.Add(new FormFieldItem(tr, control));
            return control;
        }

        private MyCheckBox addCheckBox(MyCheckBox control, HtmlTableRow tr, HtmlTableCell td)
        {
            td.Attributes["class"] = "checkbox";
            control.Text = control.Title;
            td.Controls.Add(control);
            _items.Add(new FormFieldItem(tr, control));
            
            return control;
        }

        private MyRadioButton addRadioButton(MyRadioButton control, HtmlTableRow tr, HtmlTableCell td)
        {
            td.Attributes["class"] = "radiobutton";
            control.Text = control.Title;
            td.Controls.Add(control);
            _items.Add(new FormFieldItem(tr, control));
            
            return control;
        }

        private MyCheckBoxList addCheckBoxList(MyCheckBoxList control, HtmlTableRow tr, HtmlTableCell td)
        {
            td.Attributes["class"] = "checkbox-list";
            td.Controls.Add(control);
            _items.Add(new FormFieldItem(tr, control));
            return control;
        }

        private MyRadioButtonList addRadioButtonList(MyRadioButtonList control, HtmlTableRow tr, HtmlTableCell td)
        {
            td.Attributes["class"] = "radiobutton-list";
            td.Controls.Add(control);
            _items.Add(new FormFieldItem(tr, control));
            return control;
        }

        private FileUploadButtons addFileUpload(MyFileUploadWithMediaItem control, HtmlTableRow tr, HtmlTableCell td)
        {
            FileUploadButtons buttons = new FileUploadButtons();
            td.Attributes["class"] = "fileupload";

            IMediaItem mediaItem = control.MediaItem;
            HtmlTable tblUpload = new HtmlTable();
            tblUpload.CellPadding = tblUpload.CellSpacing = 0;

            HtmlTableRow trMediaItem = new HtmlTableRow();
            //File details
            if (control.MediaItem != null)
            {

                tblUpload.Rows.Add(trMediaItem);

                HtmlTableCell tdItem = new HtmlTableCell();
                trMediaItem.Cells.Add(tdItem);

                HtmlTable tblItem = new HtmlTable();
                tdItem.Controls.Add(tblItem);
                tblItem.CellSpacing = tblItem.CellPadding = 0;
                tblItem.Rows.Add(new HtmlTableRow());

                HtmlTableCell tdThumb = new HtmlTableCell();
                HtmlTableCell tdFileDetails = new HtmlTableCell();
                tblItem.Rows[0].Cells.Add(tdThumb);
                tblItem.Rows[0].Cells.Add(tdFileDetails);
                tdFileDetails.Attributes["class"] = "file-details";
                tdThumb.Attributes["class"] = "thumb";

                MyImage imgThumb = new MyImage();
                HtmlAnchor aFile = new HtmlAnchor();
                imgThumb.CssClass = "image";
                imgThumb.ImageUrl = mediaItem.ThumbnailImageURL;
                imgThumb.AlternateText = mediaItem.Caption;
                if (!String.IsNullOrEmpty(mediaItem.ItemURL) || !String.IsNullOrEmpty(mediaItem.LargeImageURL))
                {
                    aFile.HRef = imgThumb.HRef = !(String.IsNullOrEmpty(mediaItem.ItemURL)) ? mediaItem.ItemURL : mediaItem.LargeImageURL;
                    imgThumb.HRefTarget = HREF_TARGET.Blank;
                    aFile.Target = "_blank";
                    aFile.InnerHtml = imgThumb.HRef;

                    if (aFile.InnerHtml.IndexOf("/") != -1)
                    {
                        aFile.InnerHtml = aFile.InnerHtml.Substring(aFile.InnerHtml.LastIndexOf("/") + 1);
                    }

                    tdFileDetails.Controls.Add(aFile);
                    if (mediaItem.FileSizeKB > 0)
                    {
                        Literal brFileSize = new Literal();
                        brFileSize.Text = "<br />";
                        Literal ltlFileSize = new Literal();
                        ltlFileSize.Text = mediaItem.FileSizeKB + "kb";
                        tdFileDetails.Controls.Add(brFileSize);
                        tdFileDetails.Controls.Add(ltlFileSize);
                    }

                    Literal br = new Literal();
                    br.Text = "<br />";
                    
                    tdFileDetails.Controls.Add(br);

                    MyImageButtonWithText btnDelete = new MyImageButtonWithText();
                    btnDelete.ImageUrl = _deleteImageButton;
                    btnDelete.Text = "Delete";
                    tdFileDetails.Controls.Add(btnDelete);

                    buttons.BtnDelete = btnDelete;
                }
                tdThumb.Controls.Add(imgThumb);



                
            }

            HtmlTableRow trFileUpload = new HtmlTableRow();
            tblUpload.Rows.Add(trFileUpload);
            HtmlTableCell tdFileUpload = new HtmlTableCell();
            trFileUpload.Cells.Add(tdFileUpload);
            tdFileUpload.Controls.Add(control);

            td.Controls.Add(tblUpload);
            



            
            if (trMediaItem != null)
            {
                trMediaItem.Cells.Add(new HtmlTableCell());
            }
            
            trFileUpload.Cells.Add(new HtmlTableCell());

            MyButton btnUpload = new MyButton();
            btnUpload.Text = "Upload";

            if (!control.Required)
            {
                //If it is not required, then there is no validation for this control but it does not make any sense to upload an empty file
                btnUpload.ValidationGroup = control.ValidationGroup = control.ClientID;
                control.Required = true;
            }
            trFileUpload.Cells[1].Attributes["class"] = "upload-button";
            trFileUpload.Cells[1].Controls.Add(btnUpload);
            buttons.BtnUpload = btnUpload;


            _items.Add(new FormFieldItem(tr, control));
            
            return buttons;
        }


        private HtmlGenericControl addLabel(IMyFormWebControl control, HtmlTableCell td)
        {
            HtmlGenericControl label = new HtmlGenericControl("label");
            label.InnerHtml = control.Title;
            
            
            label.InnerHtml += ":";
            if (control.Required)
            {
                label.InnerHtml += " *";
            }
            td.Controls.Add(label);
            return label;
        }


        
        
        /// <summary>
        /// Show a control oin the form fields
        /// </summary>
        /// <param name="control">An object which you could use when adding a control.  For example when adding a FileUpload, it will return
        /// FileUploadButtons which will have reference to the delete button and the upload button if a media item is available</param>
        /// <returns></returns>
        private object showControlField(IMyFormWebControl control)
        {
            object obj = null;

            HtmlTableRow tr = new HtmlTableRow();

            int index = this._tableFields.Rows.Count;
            //if (containButtons) index--; // Since there is the row of buttons at the end

            this._tableFields.Rows.Insert(index, tr);
            HtmlTableCell tdLabel = new HtmlTableCell();
            HtmlTableCell tdControl = new HtmlTableCell();
            tr.Cells.Add(tdLabel);
            tr.Cells.Add(tdControl);
            HtmlGenericControl label =  addLabel(control, tdLabel);



            if (control is MyTxtBox)
            {
               addTextBox((MyTxtBox)control, tr, tdControl);
            }
            else if (control is MyDropDownList)
            {
                addDropdownList((MyDropDownList)control, tr, tdControl);
            }
            else if (control is MyCheckBox)
            {
                addCheckBox((MyCheckBox)control, tr, tdControl);
                tdLabel.Controls.Clear();
            }
            else if (control is MyCheckBoxList)
            {
                addCheckBoxList((MyCheckBoxList)control, tr, tdControl);

            }
            else if (control is MyRadioButton)
            {
                addRadioButton((MyRadioButton)control, tr, tdControl);
                tdLabel.Controls.Clear();
            }
            else if (control is MyRadioButtonList)
            {
                addRadioButtonList((MyRadioButtonList)control, tr, tdControl);
            }
            else if (control is MyFileUploadWithMediaItem)
            {
                FileUploadButtons buttons = addFileUpload((MyFileUploadWithMediaItem)control, tr, tdControl);
                obj = buttons;
            }
            else if (control is TinyMCE)
            {
                addTinyMCE((TinyMCE)control, tr, tdControl);
            }



            if (!String.IsNullOrEmpty(control.StringID))
            {
                tr.ID = "tr_" + control.StringID;
            }



            return obj;
        }


        private void addButtons()
        {
            if (containButtons)
            {
                HtmlTableRow trButtons = new HtmlTableRow();
                this._tableFields.Rows.Add(trButtons);
                trButtons.Cells.Add(new HtmlTableCell());
                trButtons.Cells.Add(new HtmlTableCell());
                if (ClickSubmit != null)
                {
                    MyButton btnSubmit = new MyButton();
                    btnSubmit.ValidationGroup = ValidationGroup;
                    btnSubmit.Text = ButtonSubmitText;
                    trButtons.Cells[1].Controls.Add(btnSubmit);

                    btnSubmit.Click += new EventHandler(btnSubmit_Click);
                    _btnSubmit = btnSubmit;
                }
                if (ClickCancel != null)
                {
                    MyButton btnCancel = new MyButton();
                    btnCancel.Text = ButtonCancelText;
                    btnCancel.ValidationGroup = ValidationGroup+"_cancelButton";
                    trButtons.Cells[1].Controls.Add(btnCancel);
                    btnCancel.Attributes["class"] = "button-grey";
                    
                    btnCancel.Click += new EventHandler(btnCancel_Click);
                    _btnCancel = btnCancel;
                }

            }
        }

        void btnCancel_Click(object sender, EventArgs e)
        {
            if (ClickCancel != null)
            {
                ClickCancel(sender, e);
            }
        }

        void btnSubmit_Click(object sender, EventArgs e)
        {
            if (ClickSubmit != null)
            {
                ClickSubmit(sender, e);
            }
            
        }


        private void initTable()
        {
            this._tableFields = new HtmlTable();
            if (String.IsNullOrEmpty(CssClass)) CssClass="form";
            
            this._tableFields.Attributes["class"] = CssClass;
            
            
            
            this.Controls.Add(this._tableFields);
        }
        protected override void OnInit(EventArgs e)
        {
            initTable();
            
            base.OnInit(e);
        }
        protected override void OnLoad(EventArgs e)
        {

            addButtons();
            
            base.OnLoad(e);
        }
        

        /// <summary>
        /// Add a field to the list of FormFields
        /// </summary>
        /// <param name="field"></param>
        public virtual IMyFormWebControl AddField(IMyFormWebControl control)
        {
            
            control.ValidationGroup = ValidationGroup;
            
            this._controls.Add(control);
            this.showControlField(control);
            return control;
        }

        public virtual IMyFormWebControl AddField(MyFileUploadWithMediaItem txtUpload, MediaItemHandler DeleteHandler, MediaItemUploadHandler UploadHandler)
        {

            txtUpload.ValidationGroup = ValidationGroup;

            this._controls.Add(txtUpload);
            FileUploadButtons buttons = (FileUploadButtons) this.showControlField(txtUpload);

            if (buttons.BtnDelete != null)
            {
                if (DeleteHandler == null)
                {
                    buttons.BtnDelete.Visible = false;
                }
                else {
                    buttons.BtnDelete.Tag = txtUpload.MediaItem;
                    buttons.BtnDelete.Tag2 = DeleteHandler;
                    buttons.BtnDelete.Click += new EventHandler(BtnDeleteMediaItem_Click);
                }
            }
            if (buttons.BtnUpload != null)
            {
                if (UploadHandler == null)
                {
                    buttons.BtnUpload.Visible = false;
                }
                else
                {
                    buttons.BtnUpload.Tag = txtUpload;
                    buttons.BtnUpload.Tag2 = UploadHandler;
                    buttons.BtnUpload.Click += new EventHandler(BtnUploadMediaItem_Click);
                }
            }
            return txtUpload;
        }

        void BtnUploadMediaItem_Click(object sender, EventArgs e)
        {
            MyButton btn = (MyButton)sender;
            MyFileUploadWithMediaItem fileUpload = (MyFileUploadWithMediaItem)btn.Tag;
            MediaItemUploadHandler handler = (MediaItemUploadHandler)btn.Tag2;

            handler(fileUpload);
        }

        void BtnDeleteMediaItem_Click(object sender, EventArgs e)
        {
            MyImageButtonWithText btn = (MyImageButtonWithText)sender;
            IMediaItem item = (IMediaItem)btn.Tag;
            MediaItemHandler handler = (MediaItemHandler)btn.Tag2;

            handler(item);

        }










        /// <summary>
        /// The CSS class to assign with the table.  Defaults to 'form' if set to null
        /// </summary>
        public string CssClass { get; set; }

        public IMyFormWebControl GetControl(string fieldID)
        {
            FormFieldItem item = GetFormFieldItem(fieldID);
            if (item != null)
            {
                return item.Control;
            }
            return null;
        }

        /// <summary>
        /// Get the respective form field item with the HtmlTableRow
        /// </summary>
        /// <param name="fieldID"></param>
        /// <returns></returns>
        public FormFieldItem GetFormFieldItem(string fieldID)
        {
            for (int i = 0; i < this._items.Count; i++)
            {
                FormFieldItem item = this._items[i];
                if (String.Compare(item.Control.StringID, fieldID, true) == 0)
                {
                    return item;
                }
            }
            return null;
        }

        /// <summary>
        /// Get the value of the field by ID.  If it is in search mode and the field ID is a number,
        /// or is a date, then a list of values are passed for the FROM - TO
        /// </summary>
        /// <param name="fieldID"></param>
        /// <returns></returns>
        public object GetValue(string fieldID)
        {
            FormFieldItem item = GetFormFieldItem(fieldID);

            if (item != null)
            {
                return item.Value;
            }
            return null;
            
        }
        public object GetFormValue(string fieldID)
        {
            FormFieldItem item = GetFormFieldItem(fieldID);

            if (item != null)
            {
                return item.FormValue;
            }
            return null;

        }
        public string GetFormValueStr(string fieldID)
        {
            object o = GetFormValue(fieldID);
            return (string)o;
        }
        public bool GetFormValueBool(string fieldID)
        {
            object o = GetFormValue(fieldID);
            return (bool)o;
        }

        public int GetFormValueInt(string fieldID)
        {
            object o = GetFormValue(fieldID);
            return Util.Conversion.ToInt32(o);
        }
        public double GetFormValueDouble(string fieldID)
        {
            object o = GetFormValue(fieldID);
            return Util.Conversion.ToDouble(o);
        }


        public string ValidationGroup { get; set; }



        public string DeleteImageButton { get { return _deleteImageButton; } set { _deleteImageButton = value; } }
    }
}
