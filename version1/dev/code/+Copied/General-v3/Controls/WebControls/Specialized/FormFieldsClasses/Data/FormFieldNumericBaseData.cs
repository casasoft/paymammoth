﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data
{

    public abstract class FormFieldNumericBaseData<T> : FormFieldBaseTypeData<T>
    {
        
        public enum NUMERIC_RANGE_TYPE
        {
            //Do not change the constants
            All = 0,
            PositiveOnly = 100,
            NegativeOnly = 200
        }
        public NUMERIC_RANGE_TYPE RangeType
        {
            get
            {
                return _validationParams.positiveOnly ? NUMERIC_RANGE_TYPE.PositiveOnly : NUMERIC_RANGE_TYPE.NegativeOnly;
            }
            set
            {

                _validationParams.positiveOnly = value == NUMERIC_RANGE_TYPE.PositiveOnly;
                _validationParams.negativeOnly = value == NUMERIC_RANGE_TYPE.NegativeOnly;

            }
        }


        public double? NumFrom { get { return _validationParams.numFrom; } set { _validationParams.numFrom = value; } }
        public double? NumTo { get { return _validationParams.numTo; } set { _validationParams.numTo = value; } }

        public FormFieldNumericBaseData()
        {
            _validationParams.isNumber = true;
        }

    }
}
