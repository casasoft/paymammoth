﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data;
using CS.General_v3.Controls.WebControls.Common.General;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses
{
    using JavaScript.jQuery.Plugins.Tooltips.QTip;

    public class FormFieldsRowItemField : FormFieldsRowItemBase
    {

        public new class FUNCTIONALITY : FormFieldsRowItemBase.FUNCTIONALITY
        {
            public IMyFormWebControl Field { get; internal set; }
            public FormFieldBaseData FieldData { get; internal set; }

            public FUNCTIONALITY(FormFieldsRowItemField control)
                : base(control)
            {
                
            }
        }

        public new FUNCTIONALITY Functionality { get { return (FUNCTIONALITY)base.Functionality; } }

        public FormFieldsRowItemField(List<FormFields.FormFieldsColumnData> columnLayout, FormFields formFields, FormFieldBaseData fieldData)
            : base(columnLayout, formFields,fieldData.Title, fieldData.SubTitle, null, fieldData.HelpMessage, fieldData.ShowValidationIcon, fieldData.HelpMessageQtip2Options)
        {
            this.CssManager.AddClass(fieldData.RowCssClass);
            this.CssManager.AddClass(fieldData.RowCssClassBase);
            this.Functionality.FieldData = fieldData;
            this.Functionality.Field = fieldData.GetField();
            initBaseData();
            init();
        }
        public override void OnRendered()
        {
            this.Functionality.FieldData.OnRendered();
            base.OnRendered();
        }

        protected override FormFieldsRowItemBase.FUNCTIONALITY getFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        private void initBaseData()
        {
            //Set control
            _fieldControl = this.Functionality.Field.Control;

            //Set label
            _label = this.Functionality.FieldData.Title + _formFields.Functionality.LabelSuffix;
            if (this.Functionality.FieldData.Required)
            {
                _label += _formFields.Functionality.RequiredCharacter;
                this.CssManager.AddClass(_formFields.Functionality.CssClassTrRowItemRequired);
            }

        }
        private void setValidationIcon()
        {
            if (this.Functionality.ElemValidationIcon != null)
            {
                this.Functionality.FieldData.ValidationIconElement = this.Functionality.ElemValidationIcon;
            }
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            setValidationIcon();
        }






    }
}
