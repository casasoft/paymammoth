﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_20090518.Controls.WebControls.Specialized.TreeStructure.TreeStructureBasic
{
    public abstract class TreeItemBasic : ITreeItemBasic
    {


      

        public virtual int ID {get;set;}

        public virtual string Title { get; set; }

        public virtual int Priority { get; set; }

        public virtual string ImageURL { get; set; }


        public virtual List<ITreeItemBasic> ChildItems { get; set; }

        public virtual List<ExtraButton> ExtraButtons { get; set; }
        public TreeItemBasic()
        {
            this.ExtraButtons = new List<ExtraButton>();
            this.ChildItems = new List<ITreeItemBasic>();
            this.ImageURL = "/_common/images/components/v1/tree/folder.gif";
        }

        


        public virtual List<ITreeItemBasic> GetChildTreeItems()
        {
            return this.ChildItems;
        }
        public virtual void AddExtraButtons()
        {

        }
        
    }
        
}
