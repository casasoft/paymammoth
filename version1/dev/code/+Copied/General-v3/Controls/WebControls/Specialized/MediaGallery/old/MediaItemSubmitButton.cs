﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.WebControls.Specialized.MediaGallery
{

    internal class MediaItemSubmitButton : MediaItemButton
    {
        
        public MediaGallery.FUNCTIONALITY.MediaItemHandler ItemHandler { get; set; }
        public MediaItemSubmitButton(MediaGallery.FUNCTIONALITY.MediaItemHandler ItemHandler, string id):base(id)
        {
            this.ItemHandler = ItemHandler;
        }
        public MediaItemSubmitButton() { }
    }
}
