﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.WebControls.Specialized.MenuTreeStructure
{
    public interface IMenuItem : TreeStructureClasses.TreeStructureBasic.ITreeItemBasic
    {
       
        bool Visible { get; set; }
        bool AllowUpdate { get;  }
        bool AllowDelete { get;  }
        bool AllowAddChildren { get; }
        string EditURL { get; }
       
        string Message_ConfirmDelete { get; }
        string Message_DeleteOK { get; }
        void Remove();
        void Save();

    }
        
}
