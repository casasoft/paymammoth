﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_20090518.Controls.WebControls.Specialized.TreeStructure.TreeStructureBasic
{
    public interface ITreeItemBasic
    {
        int ID { get; set; }
        //int ParentID { get; set; }
        string Title { get; set; }
        int Priority { get; set; }
        string ImageURL { get;  }
       // string EditURL { get;  }
      //  string AddNewItemURL { get;  }
       // bool AllowUpdate { get;  }
      //  bool AllowDelete { get;  }
       // bool AllowAdd { get; }
        //bool AllowAddSubItems { get; }
        //void Remove();
       // void Save();
        List<ITreeItemBasic> GetChildTreeItems();
        List<ExtraButton> ExtraButtons { get; }
        void AddExtraButtons();
        /// <summary>
        /// Delete message to show.  If left null, default message is shown
        /// </summary>
       // string Message_Delete { get; }
        /// <summary>
        /// Confirm message to show.  If left null, default message is shown
        /// </summary>
       // string Message_Confirm { get;  }

    }
        
}
