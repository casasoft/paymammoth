﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.WebControls.Specialized.Reporting.v1
{
    public class BaseNumericalReportSectionDataImpl : BaseReportSectionDataBaseImpl, IBaseNumericalReportSectionData
    {



        public new BaseNumericalReportTableDataImpl TableData { get { return (BaseNumericalReportTableDataImpl)base.TableData; } set { base.TableData = value; } }

        public BaseNumericalReportSectionDataImpl()
        {
            this.DecimalPlaces = 2;
            this.ThousandsDelimiter = ",";
            this.ShowTotalHorizontalAndVertical = this.ShowTotalsHorizontal = this.ShowTotalsVertical = true;
        }

        #region IBaseNumericalReportSectionData Members

        public int? DecimalPlaces
        {
            get;
            set;
        }

        public string ThousandsDelimiter
        {
            get;
            set;
        }

        IBaseNumericalReportTableData IBaseNumericalReportSectionData.TableData
        {
            get { return this.TableData; }
        }



        public bool ShowTotalsVertical
        {
            get;
            set;
        }

        public bool ShowTotalsHorizontal
        {
            get;
            set;
        }

        public bool ShowTotalHorizontalAndVertical
        {
            get;
            set;
        }

        #endregion
    }
}
