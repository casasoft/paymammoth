﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using System.Web.UI.HtmlControls;
using CS.General_v3.JavaScript.Classes.SliderNavigation.v2;
using System.Web.UI.WebControls;
namespace CS.General_v3.Controls.WebControls.Specialized.Hierarchy
{
    public class HierarchicalControl : MyDiv
    {



        public class PropertiesClass {

            
            public delegate void ListItemCreateHandler(HtmlGenericControl li, IHierarchy item,int depth, int index, HtmlAnchor aLink);
            public ListItemCreateHandler OnListItemCreate;


            public event ListItemCreateHandler OnListItemCreateEvent
            {
                add
                {
                    OnListItemCreate += value;
                }
                remove
                {
                    OnListItemCreate -= value;
                }
            }

            public string LinkPrefix { get; set; }

            public IEnumerable<IHierarchy> Items { get; set; }
            public int MaxDepth { get; set; }
            public int? SelectedId { get; set; }
            public string SelectedClass { get; set; }
            public bool ClickOnlyOnLeafNodes { get; set; }
            public bool Clickable { get; set; }
            public bool ShowNonVisible { get; set; }
            public string LastItemCssClass { get; set; }
            public bool ExpandParentsFromSelectedItem { get; set; }
            public bool CreateJQuerySliderNavigationMenu { get; set; }
            public SliderNavigationJSDetails SliderNavigationDetails { get; set; }

            public PropertiesClass()
            {
                this.ShowNonVisible = false;
                Items = new List<IHierarchy>();
                this.SliderNavigationDetails = new SliderNavigationJSDetails();
                Clickable = true;
                SelectedClass = "selected";
            }
        }
        public PropertiesClass Functionality
        {
            get
            {
                return this.Properties;
            }
            set
            {
                this.Properties = value;
            }
        }
        public PropertiesClass Properties { get; set; }


        private HtmlGenericControl _ul;

        public HtmlGenericControl UlElement
        {
            get { return _ul; }
        }

        public HierarchicalControl()
        {
            Properties = new PropertiesClass();
        }
        private static bool HasAnyVisibleChildren(IEnumerable<IHierarchy> items)
        {
            foreach (var item in items)
            {
                if (item.Visible)
                {
                    return true;
                }
            }

            return false;
        }
        public static HtmlGenericControl GetUL(IEnumerable<IHierarchy> items, int depth, int maxDepth, int? selectedID)
        {
            return GetUL(items, depth, maxDepth, selectedID, false, true, null);
        }
        public static HtmlGenericControl GetUL(IEnumerable<IHierarchy> items, int depth, int maxDepth, int? selectedID, PropertiesClass.ListItemCreateHandler OnItemCreateHandler)
        {
            return GetUL(items, depth, maxDepth, selectedID, false, true, OnItemCreateHandler);
        }
        public static HtmlGenericControl GetUL(IEnumerable<IHierarchy> items, int depth, int maxDepth, int? selectedID, bool clickOnlyOnLeafNodes, bool clickable, PropertiesClass.ListItemCreateHandler OnItemCreateHandler,
            bool showNonVisible = false, string lastCssClass = null, string linkPrefix = null)
        {
            HtmlGenericControl ul = new HtmlGenericControl("ul");
            ul.Attributes["class"] = "level-" + depth;
            int i = 0;
            foreach (var item in items) 
            {
                if (item.Visible || (!item.Visible && showNonVisible))
                {
                    MySpan content = new MySpan() { CssClass = "span-content" };
                    MySpan innerContent = new MySpan() { CssClass = "span-inner-content" };
                    content.Controls.Add(innerContent);

                    HtmlGenericControl li = new HtmlGenericControl("li");
                    ul.Controls.Add(li);
                    var children = item.GetChildren();
                    li.Attributes["class"] = "level-" + depth+"-item";
                    //add a css class to the last list item element at level-0
                    if (depth == 0 && !string.IsNullOrEmpty(lastCssClass) && item == items.Last())
                        li.Attributes["class"] += " " + lastCssClass;

                    HtmlAnchor aHref = null;
                    if (clickable && !String.IsNullOrEmpty(item.Href) && (!clickOnlyOnLeafNodes || (children == null || children.Count() == 0)))
                    {
                        //Contains link

                        if (!string.IsNullOrEmpty(linkPrefix))
                        {
                            li.Controls.Add(new Literal() { Text = linkPrefix });
                        }

                        aHref = new HtmlAnchor();
                        if (depth == 0)
                        {
                            aHref.Attributes["class"] += " level-0-anchor";                                                                         
                        }
                        aHref.Controls.Add(content);

                        li.Controls.Add(aHref);
                        innerContent.InnerHtml = item.Title;
                        //aHref.InnerHtml = aHref.Title = item.Title;
                        aHref.HRef = item.Href;
                        if (item.HrefTarget != Enums.HREF_TARGET.Blank || item.HrefTarget != Enums.HREF_TARGET.Self || item.HrefTarget != Enums.HREF_TARGET.Parent)
                        {
                            aHref.Target = CS.General_v3.Util.EnumUtils.StringValueOf(Enums.HREF_TARGET.Self);
                        }
                        else
                        {
                            aHref.Target = CS.General_v3.Util.EnumUtils.StringValueOf(item.HrefTarget);
                        }

                            if (item.Selected || (selectedID != null && selectedID.Value == item.ID))
                        {
                            aHref.Attributes["class"] += " selected";
                        }
                        if (children == null || children.Count() == 0)
                        {
                            aHref.Attributes["class"] += " leaf-node";
                        }
                    }
                    else
                    {
                        li.InnerHtml = linkPrefix + item.Title;
                    }
                    if (OnItemCreateHandler != null)
                    {
                        OnItemCreateHandler(li, item, depth, i, aHref);
                    }
                    if ((maxDepth == 0 || (depth + 1 < maxDepth)) && children != null && children.Count() > 0)
                    {
                        if (HasAnyVisibleChildren(children))
                        {
                            li.Controls.Add(GetUL(children, depth + 1, maxDepth, selectedID, clickOnlyOnLeafNodes, clickable, OnItemCreateHandler));
                        }
                    }
                    //i++;
                }
                i++;
            }
            return ul;
        }
        private void initSliderNavigationJS()
        {
            if (this.Functionality.CreateJQuerySliderNavigationMenu)
            {
                if (this.Functionality.SliderNavigationDetails == null)
                {
                    throw new Exception("Please specify SliderNavigationDetails since you want jQuery slider nav");
                }
                SliderNavigation nav = new SliderNavigation(_ul.ClientID, this.Functionality.SliderNavigationDetails, true);
            }
        }
        private void initControls()
        {
            HtmlGenericControl ul = GetUL(
                Properties.Items, 0, Properties.MaxDepth, Properties.SelectedId, Properties.ClickOnlyOnLeafNodes, Properties.Clickable, Properties.OnListItemCreate,
                showNonVisible: Properties.ShowNonVisible, lastCssClass: Properties.LastItemCssClass, linkPrefix: Properties.LinkPrefix);
            this.Controls.Add(ul);
            _ul = ul;
        }

        protected override void OnLoad(EventArgs e)
        {
            initControls();
            base.OnLoad(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            initSliderNavigationJS();
            base.OnPreRender(e);
        }


        public string ClientID_UL { get { return _ul.ClientID; } }
    }
}
