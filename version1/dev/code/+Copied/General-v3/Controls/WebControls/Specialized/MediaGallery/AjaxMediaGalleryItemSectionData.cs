﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;
using CS.General_v3.JavaScript.Interfaces;
using System.Web.UI;
namespace CS.General_v3.Controls.WebControls.Specialized.MediaGallery
{
    public class AjaxMediaGalleryItemSectionData :IJavaScriptObject
    {

        public static AjaxMediaGalleryItemSectionData GetDefaultSection()
        {
            var section = new AjaxMediaGalleryItemSectionData();
            //section.ID = 1;
            section.Title = "Gallery";
            section.CanUpload = true;
            section.UploadTitle = "Upload";
            section.UploadText = "";
            section.UploadButtonText = "Upload";
            return section;
        }

        public int ID { get; set; }
        public string Title { get; set; }
        public List<AjaxMediaGalleryItemData> Items { get; set; }
        public bool CanUpload { get; set; }

        public string NoFlashText { get; set; }

        public string UploadTitle { get; set; }
        
        public string UploadText { get; set; }
        public string CantUploadMore { get; set; }
        public string LimitExceeded { get; set; }



        public string UploadButtonText { get; set; }

        public int MaxAmtItems { get; set; }
        /// <summary>
        /// The maximum amount of file uploads to show
        /// </summary>
        public int MaxAmtItemUploaders { get; set; }
        /// <summary>
        /// Use the tags [REMAINING] and [TOTAL]
        /// 
        /// </summary>
        public string MaxUploadText { get; set; }
        public string RemainingUploadText { get; set; }

        public string SectionUploadInstructionsHtmlText { get; set; }
        public string SectionNormalInstructionsHtmlText { get; set; }

        /// <summary>
        /// This is used to place an alternate upload content in case Flash is not available.
        /// Such as normal file upload controls so that functionality is still available.
        /// </summary>

        /// 
        private Control _alternateUploadContent;
        public Control AlternateUploadContent
        {
            get
            {
                return _alternateUploadContent;
            }
            set
            {
                _alternateUploadContent = value;
            }
        }



        public AjaxMediaGalleryItemSectionData()
        {
            Items = new List<AjaxMediaGalleryItemData>();
            NoFlashText = "In order to use the multiple file uploader, you need to have Adobe Flash Player v9.0.24 (or above) installed.  Installing Adobe Flash Player is safe and very simple.  <br /><a href='http://get.adobe.com/flashplayer/'>Upgrade your Adobe Flash Player now and start using our advanced file uploader!</a>";
            UploadTitle = "Upload Files";
            UploadText = "";
            UploadButtonText = "Upload";
            CanUpload = true;
            MaxUploadText = "You can upload a maximum of [TOTAL] item(s).";
            RemainingUploadText = "You can upload <strong>[REMAINING] more photos</strong> from your maximum limit of [TOTAL] item(s).";
            MaxAmtItems = 0;
            MaxAmtItemUploaders = 4;
            CantUploadMore = "You cannot upload more item(s) since you have reached your limit of <strong>[TOTAL] total item(s)</strong>.  If you want to replace items, first delete items by clicking on the delete button.";
            LimitExceeded = "Uploading [UPLOAD] more item(s) will exceed your limit of [TOTAL] total item(s).  Either delete items or upload not more than [LIMIT] item(s).";
            SectionUploadInstructionsHtmlText = @"<h3>Instructions</h3>
            <ul>
                <li>If you have Adobe Flash Player v9.0.24 (or above) installed, you may upload items using our advanced file uploader.  Simply click on the grey BROWSE button and select files.  <strong>You may select multiple files to upload simultaneously</strong>.</li>
                <li>If you do not have Adobe Flash Player v9.0.24 (or above) installed, we recommend you <a target='_blank' href='http://get.adobe.com/flashplayer/'>upgrade your Flash Player</a>.  Or else, use our basic file uploader and select files one by one from below and click on Upload</li>
                <li>Once you have items in the gallery, you can re-order items by clicking on the drag icon and drag them to the required new order.</li>
                <li>To delete items, simply click on the delete icon underneath each thumbnail.</li>
                <li>To update the caption of each thumbnail, click on the caption text underneath the thumbnail and update the text.  Once ready press 'ENTER' and the text will be updated.</li>
                <li>Clicking on the thumbnail will enlarge the image to its full size.</li>
            </ul>";
            SectionNormalInstructionsHtmlText = @"
            <ul>
                <li>Clicking on the thumbnail will enlarge the image to its full size.</li>
            </ul>";
        }


        public void AddItem(AjaxMediaGalleryItemData item)
        {
            if (item.ID == 0)
            {
                throw new Exception("Please specify item ID");
            }
            Items.Add(item);
            item.SectionName = this.Title;
        }

        /* public bool canDelete;
        public bool canCrop;
        public bool canReorder;

        public string thumbURL;
        public string itemURL;
        public string caption;
        public string sectionName;
        public string id;
        public int width;
        public int height;

        public MediaGalleryItemCropData[] cropSizes;*/

        #region IJavaScriptObject Members

        public IJavaScriptSerializable GetJsObject(bool forFlash = false)
        {
            JSObject obj = forFlash ? new JSObjectFlash() : new JSObject();
            obj.AddProperty("id", ID.ToString());
            obj.AddProperty("title", Title);
            obj.AddProperty("maxAmtItems", MaxAmtItems);
            obj.AddProperty("maxAmtItemUploaders", MaxAmtItemUploaders);
            obj.AddProperty("maxUploadText", MaxUploadText);
            obj.AddProperty("remainingUploadText", RemainingUploadText);
            obj.AddProperty("cantUploadMore", CantUploadMore);
            obj.AddProperty("limitExceeded",LimitExceeded);
            obj.AddProperty("sectionUploadInstructionsHtmlText", SectionUploadInstructionsHtmlText);
            obj.AddProperty("sectionNormalInstructionsHtmlText", SectionNormalInstructionsHtmlText);


            JSArray arrItems = new JSArray();
            if (Items != null)
            {
                for (int i = 0; i < Items.Count; i++)
                {
                    arrItems.AddItem(Items[i].GetJsObject());
                }
            }
            obj.AddProperty("items", arrItems);
            obj.AddProperty("canUpload", CanUpload);
            obj.AddProperty("noFlashText", NoFlashText);
            if (AlternateUploadContent != null)
            {
                if (string.IsNullOrEmpty(AlternateUploadContent.ID))
                {
                    throw new Exception("Please specify ID of upload content since it must be retrieval from a selector in DOM");
                }
                else if (AlternateUploadContent.Parent == null)
                {
                    throw new Exception("AlternateUploadContent must be in DOM.");
                }
                else
                {
                    obj.AddProperty("alternateUploadContentID", AlternateUploadContent.ClientID);
                }
            }
            return obj;
        }

        #endregion
    }
}
