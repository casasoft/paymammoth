﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common;

using CS.General_v3;
namespace CS.General_v3.Controls.WebControls.Specialized.FileBrowser
{
    public class ContentPanel : MyDiv
    {
        public class FUNCTIONALITY
        {
            public FUNCTIONALITY(ContentPanel ContentPanel)
            {
                _ContentPanel = ContentPanel;
                this.ContentPanelItems = new List<ContentPanelItem>();
                this.ShowCheckboxes = true;
                this.Buttons = new List<CS.General_v3.Controls.WebControls.Classes.ExtraButton>();
                this.ShowDeleteButton = true;
            }
            public delegate void DeletedItemsDelegate(ContentPanel sender, List<ContentPanelItem> items);
            public List<CS.General_v3.Controls.WebControls.Classes.ExtraButton> Buttons {get;set;}
            public event DeletedItemsDelegate OnDeletedItems;
            public List<ContentPanelItem> ContentPanelItems {get;set;}
            private ContentPanel _ContentPanel = null;
            public bool ShowCheckboxes {get;set;}
            private bool _ShowDeleteButton = false;
            public bool ShowDeleteButton { get { return _ShowDeleteButton; } set { _ShowDeleteButton = value; } }
            private void addStandardButtons()
            {
                if (ShowDeleteButton)
                {
                    CS.General_v3.Controls.WebControls.Classes.ExtraButton btnDelete = new CS.General_v3.Controls.WebControls.Classes.ExtraButton();
                    btnDelete.Click += new EventHandler(btnDelete_Click);
                    btnDelete.Title = "Delete";
                    btnDelete.ConfirmMessageOnClick = "Are you sure you want to delete the selected item(s)?";
                    this.Buttons.Add(btnDelete);
                }
            }
            public List<ContentPanelItem> GetCheckedContentPanelItems()
            {
                List<ContentPanelItem> list = new List<ContentPanelItem>();
                foreach (var cp in this.ContentPanelItems)
                {
                    if (cp.Functionality.IsChecked())
                    {
                        list.Add(cp);
                    }
                }
                return list;
            }

            private void btnDelete_Click(object sender, EventArgs e)
            {
                var list = GetCheckedContentPanelItems();
                if (list.Count > 0)
                {
                    if (OnDeletedItems != null)
                        OnDeletedItems(_ContentPanel, list);
                }
                
            }
            private void renderContentPanelItems()
            {
                foreach (var item in this.ContentPanelItems)
                {
                    item.Functionality.ShowCheckbox = this.ShowCheckboxes;
                    item.Functionality.OnDelete += new ContentPanelItem.FUNCTIONALITY.OnDeletedItemDelegate(ContentPanelItem_OnDelete);
                    _ContentPanel.Controls.Add(item);
                }
            }
            private void renderButtonsBar()
            {
                
                MyDiv divButtons = new MyDiv();
                divButtons.CssManager.AddClass("filebrowser-buttonsBar");

                for (int i =0; i < this.Buttons.Count;i++)
                {
                    var btnItem = this.Buttons[i];
                    MyDiv divBtn = new MyDiv();
                    divButtons.CssManager.AddClass("filebrowser-button");
                    MyButton btn = new MyButton(_ContentPanel.ID + "_" + i);
                    btn.Click += btnItem.clickHandler;
                    btn.Title = btnItem.Title;
                    btn.Text = btnItem.Title;
                    btn.ConfirmMessage = btnItem.ConfirmMessageOnClick;
                    btn.ValidationGroup = "contentPaneDelete";
                    btn.CssClass = btnItem.CssClass;
                    divBtn.Controls.Add(btn);
                    divButtons.Controls.Add(divBtn);
                }
                _ContentPanel.Controls.Add(divButtons);
                
            }
            public void Render()
            {
                _ContentPanel.CssManager.AddClass("filebrowser-contentPanel");
                addStandardButtons();
                renderContentPanelItems();
                renderButtonsBar();
            }

            private void ContentPanelItem_OnDelete(ContentPanelItem sender)
            {
                if (OnDeletedItems != null)
                {
                    List<ContentPanelItem> list = new List<ContentPanelItem>();
                    list.Add(sender);
                    OnDeletedItems(_ContentPanel, list);
                }
            }

        }
        public new FUNCTIONALITY Functionality { get; set; }
        public ContentPanel()
        {
            this.Functionality = new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            this.Functionality.Render();
            base.OnLoad(e);
        }
    }
}
