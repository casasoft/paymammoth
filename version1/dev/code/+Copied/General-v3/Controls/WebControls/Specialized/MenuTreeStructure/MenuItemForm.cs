﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using CS.General_20101215.Controls.WebControls.Common;
using CS.General_20101215.Controls.Classes.MediaGallery;
using CS.General_20101215.Util;
using CS.General_20101215.Controls.WebControls.Specialized.FormFieldsClasses;
namespace CS.General_20101215.Controls.WebControls.Specialized.MenuTreeStructure
{
    [ToolboxData("<{0}:MenuItemForm runat=server></{0}:MenuItemForm>")]    
    public class MenuItemForm : FormFields
    {

        /* HOW TO USE
        * ----------
        * 
        * Parameters to fill
        * 
        *  - ItemTitle
        *  - ListingPageUrl
        *  
        * Events to Attach
        *  
        *  - LoadFolder
        *  
        *  
        * Optional
        * 
        * - QueryStringItemParam
        * - QueryStringParentFolderParam
        */
        public MenuItemForm()
        {
            this.QueryStringItemParam = "item";
            this.QueryStringParentFolderParam = "parent";
            
        }
        #region Constants / Statics
        

        #endregion
        #region Delegates
        public delegate IMenuFolder CreateLoadFolderEvent(int ID);
        public delegate void ShowMessageEvent(string msg);
        #endregion
        #region Events
        public event ShowMessageEvent ShowMessage;
        public event CreateLoadFolderEvent LoadFolder;
        #endregion
        #region Methods
        private void showMessage(string msg)
        {
            if (ShowMessage != null)
                ShowMessage(msg);
        }
        public int GetItemIDFromQueryString()
        {
            return CS.General_20101215.Util.PageUtil.RQint(QueryStringItemParam);
        }
        public int GetParentFolderIDFromQueryString()
        {
            return CS.General_20101215.Util.PageUtil.RQint(QueryStringParentFolderParam);
        }
        public IMenuFolder GetParentFolder()
        {
            return LoadFolder(GetParentFolderIDFromQueryString());
        }
        private void loadItem()
        {
            if (IsEdit)
                loadItemFromParent();
            else
            {
                IMenuFolder parent = GetParentFolder();
                this.MenuItem = parent.CreateSubItem();
            }
        }
        private void loadItemFromParent()
        {
            IMenuFolder parent = GetParentFolder();

            IMenuItem item = null;
            int itemID = GetItemIDFromQueryString();
            item = (IMenuItem)parent.GetSubItems().FindElemObj(i => (((IMenuItem)i).ID == itemID));
            this.MenuItem = item;
            


        }
        #endregion
        #region Properties
        public IMenuItem MenuItem { get; set; }
        public string ItemTitle { get; set; }
        public string QueryStringItemParam { get; set; }
        public string QueryStringParentFolderParam { get; set; }
        public string ListingPageURL { get; set; }
        public bool IsEdit 
        {
            get
            {
                return GetItemIDFromQueryString() != 0;
            }
        }

        #endregion
        

        protected override void OnInit(EventArgs e)
        {

            
            base.OnInit(e);
        }

        public virtual void addExtraFormFields()
        {

        }
        /// <summary>
        /// Renders the tree
        /// </summary>
        public void renderForm()
        {
            IMenuFolder parent = GetParentFolder();
            IMenuItem item = this.MenuItem;


            this.AddString("txtTitle", "Title", true, null, 400, item != null ? item.Title : "");
            this.AddInteger("txtPriority", "Priority", true, null, 80, item != null ? item.Priority : 0);
            this.AddBool("chkVisible", "Visible", false, (item != null ? item.Visible : true));
            addExtraFormFields();
            if (!IsEdit)
                this.AddBool("chkAddAnother", "Add Another", false, true);
            this.ClickSubmit += new EventHandler(MenuItemForm_ClickSubmit);
        }
        public virtual void saveExtraFields()
        {

        }
        void MenuItemForm_ClickSubmit(object sender, EventArgs e)
        {

            IMenuItem item = this.MenuItem;

            item.Title = this.GetFormValueStr("txtTitle");
            item.Priority = this.GetFormValueInt("txtPriority");
            item.Visible = this.GetFormValueBool("chkVisible");
            saveExtraFields();
            item.Save();
            bool addAnother = false;
            if (IsEdit)
                showMessage(ItemTitle + " edited successfully");
            else
                showMessage(ItemTitle + " added successfully");

            if (!IsEdit) addAnother = this.GetFormValueBool("chkAddAnother");
            CS.General_20101215.Classes.URL.URLClass url = new CS.General_20101215.Classes.URL.URLClass();
            if (IsEdit || (!IsEdit && !addAnother))
            {
                url[QueryStringItemParam] = null;
                url[QueryStringParentFolderParam] = null;
                url.RedirectTo(this.ListingPageURL);
            }
            else
            {
                url.TransferTo();
            }

             


        }
        private void checkParameters()
        {
            StringBuilder sb = new StringBuilder();
            if (this.LoadFolder == null)
            {
                sb.AppendLine("Please attach to 'LoadFolder'");
            }
            
            if (string.IsNullOrEmpty(this.ListingPageURL))
            {
                sb.AppendLine("Please fill in 'ListingPageURL'");
            }
            if (sb.Length > 0)
            {
                string s = "MenuTreeStructure.MenuItemForm:: " + sb.ToString();
                throw new InvalidOperationException(s);
            }
        }
        protected override void OnLoad(EventArgs e)
        {
            checkParameters();
            loadItem();
            renderForm();
            base.OnLoad(e);
        }

        


 
    }
}
