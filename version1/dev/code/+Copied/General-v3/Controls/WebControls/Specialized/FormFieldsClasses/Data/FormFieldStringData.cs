﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common.General;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data
{
    public class FormFieldStringData : FormFieldStringBaseData
    {
        public bool ValueCaseSensitive { get { return _validationParams.valueCaseSensitive; } set { _validationParams.valueCaseSensitive = value; } }
        public bool IsIPAddress { get { return _validationParams.isIPAddress; } set { _validationParams.isIPAddress = value; } }
        public bool IsAlphaNumeric { get { return _validationParams.isAlphaNumeric; } set { _validationParams.isAlphaNumeric = value; } }

        public int MaxWords { get { return _validationParams.maxWords; } set { _validationParams.maxWords = value; } }
        public List<string> FileExtensionsAllowed { get { return _validationParams.fileExtensionsAllowed; } set { _validationParams.fileExtensionsAllowed = value; } }

        public FormFieldStringData()
        {
            this.RowCssClassBase = "form-row-string";
        }
       
    }
}
