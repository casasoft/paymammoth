﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.WebControls.Specialized.ShoppingCart
{
    public class ShoppingCartDetails
    {
        public int? DeliveryID { get; set; }
        public double TotalPrice { get; set; }
        public double NetPrice { get; set; }

        public double ShippingCost { get; set; }
        public string DeliveryFeeText { get; set; }

        public bool ItemsInCartEditable
        {
            get;
            set;
        }
    }
}
