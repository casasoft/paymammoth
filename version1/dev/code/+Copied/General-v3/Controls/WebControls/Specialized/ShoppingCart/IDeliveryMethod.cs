﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.WebControls.Specialized.ShoppingCart
{
    public interface IDeliveryMethod
    {
        long ID { get;  }
        string Text { get; set; }
        
    }
}
