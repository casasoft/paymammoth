﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common.General;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data
{
    public abstract class FormFieldBaseTypeDataBase<TInitialValue, TFormValue> : FormFieldBaseData
    {


        public FormFieldBaseTypeDataBase()
        {
        }

        public abstract TFormValue GetFormValue();
        public new TInitialValue InitialValue { get { return (TInitialValue)base.InitialValue; } set { base.InitialValue = value; } }

    }
}
