﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using System.Web.UI;
using CS.General_v3.Controls.WebControls.Specialized.Paging;
using CS.General_v3.Classes.SearchParams;

namespace CS.General_v3.Controls.WebControls.Specialized.PagerContent
{
    public enum PagerRenderingType
    {
        RenderTopPager,
        RenderBottomPager,
        RenderBothPagers
    }

    public class PagerWithContent : MyDiv
    {
        public class FUNCTIONALITY
        {
            private PagerWithContent _ctrl = null;
            public FUNCTIONALITY(PagerWithContent ctrl)
            {
                _ctrl = ctrl;
            }
            public int TotalResults { get; set; }
            public int PageSize { get; set; }
            public int PageNo { get; set; }

        }

        public IList<Control> ControlCollection;
        public PagerRenderingType PagerRenderType;

        //public SearchParamsImplemented _currentSearchParams;

        public PagerWithContent()
            : base()
        {
            this.Functionality = new FUNCTIONALITY(this);
        }

        protected override void OnLoad(EventArgs e)
        {
            init();    
        }

        private void init()
        {
            initSearchParams();
            setCurrentDivCssClass();
            renderItems();
        }

        private void initSearchParams()
        {
          //  _currentSearchParams = new SearchParamsImplemented("currParams", CS.General_v3.Util.PageUtil.GetUrlLocationAndQuerystring());
        }

        private void setCurrentDivCssClass()
        {
            CssClass = "pager-with-content-wrapper";
        }

        private void renderItems()
        {
            switch (PagerRenderType)
            {
                case PagerRenderingType.RenderTopPager:
                    renderPager();
                    renderContent();
                    break;
                case PagerRenderingType.RenderBottomPager:
                    renderContent();
                    renderPager();
                    break;
                case PagerRenderingType.RenderBothPagers:
                    renderAllItems();
                    break;
            }
        }

        private void renderContent()
        {
            MyDiv contentWrapperDiv = new MyDiv() { CssClass = "pager-with-content-main-content-wrapper" };
            for (int i = 0; i < ControlCollection.Count; i++)
            {
                contentWrapperDiv.Controls.Add(ControlCollection[i]);
            }
            contentWrapperDiv.Controls.Add(getClearControl());
            Controls.Add(contentWrapperDiv);
        }


        private Control getClearControl()
        {
            MyDiv div = new MyDiv();
            div.Style.Add("clear", "both");
            return div;
        }
        public FUNCTIONALITY Functionality { get; private set; }
        
        private void renderPager()
        {
            PagingBreadcrumbsAndCombos pgb = new PagingBreadcrumbsAndCombos();
                     
            Specialized.Paging.PagingBar pg = pgb.Functionality.PagingBar;
            pg.Functionality.SelectedPageNum = this.Functionality.PageNo;

            pg.Functionality.TotalResults = this.Functionality.TotalResults;
            pg.Functionality.PageSize = this.Functionality.PageSize;
            pg.Functionality.PagesSeparatorText = string.Empty;
            Controls.Add(pgb);
        }
        
        private void renderAllItems()
        {
            renderPager();
            renderContent();
            renderPager();
        }


    }
}
