﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.WebControls.Specialized.Reporting.v1
{
    public class BaseReportSectionDataImpl : BaseReportSectionDataBaseImpl, IBaseReportSectionData
    {
        public BaseReportSectionDataImpl()
        {
            this.TableData = new BaseReportTableDataImpl();
        }

        public new BaseReportTableDataImpl TableData { get { return (BaseReportTableDataImpl) base.TableData; } set { base.TableData = value; }}



        #region IBaseReportSectionData Members

        IBaseReportTableData IBaseReportSectionData.TableData
        {
            get { return this.TableData; }
        }

        #endregion

    }
}
