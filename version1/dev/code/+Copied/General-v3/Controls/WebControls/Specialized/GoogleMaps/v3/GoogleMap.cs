﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using System.Web.UI.WebControls;
namespace CS.General_v3.Controls.WebControls.Specialized.GoogleMaps.v3
{
    public class GoogleMap : MyDiv
    {

        public class PropertiesClass
        {
            public double? Lat { get; set; }
            public double? Lng { get; set; }
            public double? ZoomLevel { get; set; }
            public Enums.GOOGLE_MAP_TYPE MapType { get; set; }
            public bool ConfirmVisible { get; set; }

            public List<GoogleMarker> Markers { get; set; }

            public PropertiesClass()
            {
                Markers = new List<GoogleMarker>();
                MapType = Enums.GOOGLE_MAP_TYPE.None;
            }

        }
        public PropertiesClass Properties { get; set; }


        private string varName { get { return this.ClientID + "_gmap"; } }

        public GoogleMap()
        {
            this.CssManager.AddClass("google-map");
            this.Properties = new PropertiesClass();

        }

        private string getMarkersJs()
        {
            string js = "";
            for (int i = 0; i < Properties.Markers.Count; i++)
            {
                js += varName + ".addMarker(" + Properties.Markers[i].GetMarkerOptionsObject().GetJS() + ");";
            }
            return js;


        }


        private void initMap()
        {
            if (!Properties.Lat.HasValue) throw new Exception("Please specify Lat");
            if (!Properties.Lng.HasValue) throw new Exception("Please specify Lng");
            if (!Properties.ZoomLevel.HasValue) throw new Exception("Please specify ZoomLevel");
            string mapType = Enums.GoogleMapTypeToString(Properties.MapType);
            if (String.IsNullOrEmpty(mapType)) mapType = "null";
            string js = "new js.com.cs.googlemaps."+CS.General_v3.Settings.Others.JAVASCRIPT_VERSION+".Classes.CSMap('" + this.ClientID + "', new google.maps.LatLng(" + Properties.Lat.Value + "," + Properties.Lng.Value + "), " + Properties.ZoomLevel.Value + "," + mapType + "," + Properties.ConfirmVisible.ToString().ToLower() + ");";

            string markersJs = getMarkersJs();
            if (!String.IsNullOrEmpty(markersJs))
            {
                //Contains markers
                js = "var "+varName+" = " + js;
                js += markersJs;
            }
            



            js = CS.General_v3.Util.JSUtilOld.MakeJavaScript(js, true);
            Controls.Add(new Literal() { Text = js });
           

        }

        protected override void OnLoad(EventArgs e)
        {
            initMap();
            
            //<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=malta&amp;sll=37.0625,-95.677068&amp;sspn=46.226656,79.013672&amp;ie=UTF8&amp;z=11&amp;ll=35.937496,14.375416&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=malta&amp;sll=37.0625,-95.677068&amp;sspn=46.226656,79.013672&amp;ie=UTF8&amp;z=11&amp;ll=" style="color:#0000FF;text-align:left">View Larger Map</a></small>

                //(DOMElement mapDiv, LatLng centerPosition, Number zoomLevel, MapTypeId mapTypeId, bool confirmVisible)
        

            base.OnLoad(e);
        }


    }
}
