﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;
using CS.General_v3.Controls.WebControls.Common;
namespace CS.General_v3.Controls.WebControls.Specialized.Errors
{
    public class ErrorPageNotFoundContent : BaseWebControl
    {
        public class PropertiesClass
        {
            public string Url { get; set; }
        }
        public PropertiesClass Properties { get; set; }
        public ErrorPageNotFoundContent()
        {
            Properties = new PropertiesClass();
        }
        public ErrorPageNotFoundContent(string Url)
        {
            Properties = new PropertiesClass();
            Properties.Url = Url;
        }

        private void initContent()
        {
            string page = Properties.Url;
            Controls.Add(new MyParagraph("The page - <strong>" + page + "</strong> - does not exist.  You could have either entered an incorrect URL or else the page might have been moved or deleted."));
            Controls.Add(new MyParagraph("Suggestions:"));

            HtmlGenericControl ulSuggestions = new HtmlGenericControl("ul");
            ulSuggestions.Controls.Add(new HtmlGenericControl("li") { InnerHtml = "Check the spelling of the address you have typed." });
            ulSuggestions.Controls.Add(new HtmlGenericControl("li") { InnerHtml = "Visit the <a href='/'>homepage</a>." });
            Controls.Add(ulSuggestions);


        }

        protected override void OnLoad(EventArgs e)
        {
            if (String.IsNullOrEmpty(Properties.Url))
            {
                Properties.Url = CS.General_v3.Util.PageUtil.GetVariableFromQuerystring("aspxerrorpath");
            }
            initContent();
            base.OnLoad(e);
        }


    }
}
