﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using System.Web.UI.HtmlControls;

namespace CS.General_v3.Controls.WebControls.Specialized.BottomBar
{
    public class BottomColumn : MyDiv
    {
        public class FUNCTIONALITY 
        {
            private BottomColumn _col;

            public string Title { get; set; }
            public MyDiv PlaceHolder { get; set; }

            public FUNCTIONALITY(BottomColumn col)
            {
                _col = col;
                PlaceHolder = new MyDiv();
            }
                      
            private void initPlaceHolder()
            {
                _col.Controls.Add(PlaceHolder);
            }

            internal virtual void Init()
            {
                initPlaceHolder();
            }
        }

        
        public new FUNCTIONALITY Functionality { get; set; }

        public BottomColumn()
        {
            Functionality = new FUNCTIONALITY(this);
        }

        protected override void OnLoad(EventArgs e)
        {
            Functionality.Init();
            base.OnLoad(e);
        }

        

    }
}
