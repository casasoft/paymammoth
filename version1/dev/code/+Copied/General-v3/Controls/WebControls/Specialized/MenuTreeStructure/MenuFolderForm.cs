﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using CS.General_20101215.Controls.WebControls.Common;
using CS.General_20101215.Controls.Classes.MediaGallery;
using CS.General_20101215;
using CS.General_20101215.Controls.WebControls.Specialized.FormFieldsClasses;
namespace CS.General_20101215.Controls.WebControls.Specialized.MenuTreeStructure
{
    [ToolboxData("<{0}:MenuFolderForm runat=server></{0}:MenuFolderForm>")]    
    public class MenuFolderForm : FormFields
    {

        /* HOW TO USE
        * ----------
        * 
        * Parameters to fill
        * 
        *  - FolderTitle
        *  - ListingPageUrl
        *  
        * Events to Attach
        *  
        *  - LoadFolder
        *  
        *  
        * Optional
        * 
        * - QueryStringFolderParam
        */
        public MenuFolderForm()
        {
            this.QueryStringFolderParam = "folder";
            this.QueryStringParentFolderParam = "parent";
            
        }
        #region Constants / Statics
        

        #endregion
        #region Delegates
        public delegate IMenuFolder CreateLoadFolderEvent(int ID);
        public delegate void ShowMessageEvent(string msg);
        #endregion
        #region Events
        public event ShowMessageEvent ShowMessage;
        public event CreateLoadFolderEvent LoadFolder;
        #endregion
        #region Methods
        private void showMessage(string msg)
        {
            if (ShowMessage != null)
                ShowMessage(msg);
        }
        public int FolderID
        {
            get
            {
                return CS.General_20101215.Util.PageUtil.RQint(QueryStringFolderParam);
            }
        }
        public int ParentFolderID
        {
            get
            {
                return CS.General_20101215.Util.PageUtil.RQint(QueryStringParentFolderParam);
            }
        }
        
        #endregion
        #region Properties
        public IMenuFolder Folder = null;
            
        public string FolderTitle { get; set; }
        public string QueryStringFolderParam { get; set; }
        public string QueryStringParentFolderParam { get; set; }
        public string ListingPageURL { get; set; }
        public bool IsEdit 
        {
            get
            {
                return FolderID != 0;
            }
        }

        #endregion
        

        protected override void OnInit(EventArgs e)
        {

            
            base.OnInit(e);
        }

        public virtual void addExtraFormFields()
        {

        }
        /// <summary>
        /// Renders the tree
        /// </summary>
        private void renderForm()
        {



            this.AddString("txtTitle", "Title", true, null, 400, Folder != null ? Folder.Title : "");
            this.AddInteger("txtPriority", "Priority", true, null, 80, Folder != null ? Folder.Priority : 0);
            this.AddBool("chkVisible", "Visible", false, (Folder != null ? Folder.Visible : true));
            addExtraFormFields();
            if (!IsEdit)
                this.AddBool("chkAddAnother", "Add Another", false, true);
            this.ClickSubmit += new EventHandler(MenuFolderForm_ClickSubmit);
        }
        public virtual void saveExtraFields()
        {

        }
        void MenuFolderForm_ClickSubmit(object sender, EventArgs e)
        {


            Folder.Title = this.GetFormValueStr("txtTitle");
            Folder.Priority = this.GetFormValueInt("txtPriority");
            Folder.Visible = this.GetFormValueBool("chkVisible");
            saveExtraFields();
            Folder.Save();
            bool addAnother = false;
            if (IsEdit)
                showMessage(FolderTitle + " edited successfully");
            else
                showMessage(FolderTitle + " added successfully");

            if (!IsEdit) addAnother = this.GetFormValueBool("chkAddAnother");
            CS.General_20101215.Classes.URL.URLClass url = new CS.General_20101215.Classes.URL.URLClass();
            if (IsEdit || (!IsEdit && !addAnother))
            {
                url[QueryStringFolderParam] = null;
                url[QueryStringParentFolderParam] = null;
                url.RedirectTo(this.ListingPageURL);
            }
            else
            {
                url.TransferTo();
            }

             


        }
        private void checkParameters()
        {
            StringBuilder sb = new StringBuilder();
            if (this.LoadFolder == null)
            {
                sb.AppendLine("Please attach to 'LoadFolder'");
            }
            
            if (string.IsNullOrEmpty(this.ListingPageURL))
            {
                sb.AppendLine("Please fill in 'ListingPageURL'");
            }
            if (sb.Length > 0)
            {
                string s = "MenuTreeStructure.MenuFolderForm:: " + sb.ToString();
                throw new InvalidOperationException(s);
            }
        }
        private void loadFolder()
        {
            if (IsEdit)
            {
                Folder = LoadFolder(FolderID);
            }
            else
            {
                IMenuFolder parent = LoadFolder(ParentFolderID);
                Folder = parent.CreateSubFolder();
            }
        }
        protected override void OnLoad(EventArgs e)
        {
            checkParameters();
            loadFolder();
            renderForm();
            base.OnLoad(e);
        }

        


 
    }
}
