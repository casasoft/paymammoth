﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using System.Web.UI.WebControls;
using CS.General_v3.JavaScript.Data;
namespace CS.General_v3.Controls.WebControls.Specialized.GoogleMaps.v3
{
    using JavaScript.Data;

    public class GoogleMarker
    {
        public double? Lat { get; set; }
        public double? Lng { get; set; }
        public string Title { get; set; }
        public string Icon { get; set; }
        public string Shadow { get; set; }
        public object Shape { get; set; }
        public string Cursor { get; set; }
        public bool? Clickable { get; set; }
        public bool? Draggable { get; set; }
        public bool? Visible { get; set; }
        public bool? Flat { get; set; }
        public int? ZIndex { get; set; }

        public JSObject GetMarkerOptionsObject()
        {
            JSObject obj = new JSObject();
            if (Lat.HasValue && Lng.HasValue) {
                obj.AddProperty("position", "new google.maps.LatLng("+Lat.Value+","+Lng.Value+")", false);
            }
            if (!String.IsNullOrEmpty(Title)) obj.AddProperty("title", Title);
            if (!String.IsNullOrEmpty(Icon)) obj.AddProperty("icon", Icon);
            if (!String.IsNullOrEmpty(Shadow)) obj.AddProperty("shadow", Shadow);
            //Added by Karl the 'Shape.ToString()' as it does not pass AddProperty validation since it is an object. check!)
            if (Shape != null) obj.addPropertyFromObject("shape", Shape);
            if (!String.IsNullOrEmpty(Shadow)) obj.AddProperty("cursor",Cursor);
            if (Clickable.HasValue) obj.AddProperty("clickable", Clickable.Value);
            if (Draggable.HasValue) obj.AddProperty("draggable", Draggable.Value);
            if (Visible.HasValue) obj.AddProperty("visible",Visible.Value);
            if (Flat.HasValue) obj.AddProperty("flat", Flat.Value);
            if (ZIndex.HasValue) obj.AddProperty("zIndex", ZIndex.Value);
            return obj;
        }

    }
}
