﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Util;
using System.Web.UI.WebControls;

namespace CS.General_v3.Controls.WebControls.Specialized.MediaGallery
{
    public class AjaxMediaGallery : MyDiv
    {
        public class FUNCTIONALITY
        {
            public bool AutomaticallyAddRequiredJavascriptFiles { get; set; }
            public event AjaxMediaGalleryItemSection.FUNCTIONALITY.UploadSectionItemsHandler OnUploadItems;

            private AjaxMediaGallery _gallery;

            /// <summary>
            /// All the settings of the media gallery can be found here
            /// </summary>
            public AjaxMediaGallerySettings Settings { get; set; }

            public FUNCTIONALITY(AjaxMediaGallery gallery)
            {
                _gallery = gallery;
                Settings = new AjaxMediaGallerySettings();
                this.AutomaticallyAddRequiredJavascriptFiles = true;
            }

            private void createSections()
            {
                AjaxMediaGalleryItemSections sectionsUI = new AjaxMediaGalleryItemSections();
                sectionsUI.Functionality.Sections.AddRange(Settings.sections);
                _gallery.Controls.Add(sectionsUI);
                sectionsUI.Functionality.OnUploadItems += new AjaxMediaGalleryItemSection.FUNCTIONALITY.UploadSectionItemsHandler(Functionality_OnUploadItems);
            }

            void Functionality_OnUploadItems(List<MyFileUpload> fileUploads, AjaxMediaGalleryItemSection section)
            {
                if (OnUploadItems != null)
                {
                    OnUploadItems(fileUploads, section);
                }
            }
            private void initJSIncludes()
            {
                if (AutomaticallyAddRequiredJavascriptFiles)
                {
                    _gallery.Page.ClientScript.RegisterClientScriptInclude("http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js", "http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js");
                    _gallery.Page.ClientScript.RegisterClientScriptInclude("/_common/static/flash/v1/swfobject/2.2/swfobject.js", "/_common/static/flash/v1/swfobject/2.2/swfobject.js");
                    _gallery.Page.ClientScript.RegisterClientScriptInclude("/_common/static/js/jQuery/plugins/uploadify/2.1.0/jquery.uploadify.v2.1.0.js", "/_common/static/js/jQuery/plugins/uploadify/2.1.0/jquery.uploadify.v2.1.0.js");
                    
                }
            }
            public void InitJS()
            {
                initJSIncludes();
                string js = @"js.com.cs."+CS.General_v3.Settings.Others.JAVASCRIPT_VERSION+".UI.MediaGallery.v1.MediaGallery.replaceDomElementWithMediaGallery('"+this._gallery.ClientID+@"', "+Settings.GetJsObject().GetJS()+@");";
                //js = JSUtil.MakeJavaScript(js, true);

                CS.General_v3.Util.JSUtilOld.AddJSScriptToPage(js);

               // _gallery.Controls.Add(new Literal() { Text = js });
                //this._gallery.Page.ClientScript.RegisterClientScriptBlock(_gallery.GetType(), _gallery.ClientID, js);
                //_gallery.Controls.Add(new Literal() { Text = js });
            }
            
            public void Init()
            {
                createSections();
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public AjaxMediaGallery()
        {
            Functionality = new FUNCTIONALITY(this);
            this.CssManager.AddClass("cs-media-gallery");
        }
        public AjaxMediaGallery(string cropScript, string reorderScript, string deleteScript, string uploadScript, string updateCaptionScript):this()
        {
            this.Functionality.Settings.cropScript = cropScript;
            this.Functionality.Settings.reorderScript = reorderScript;
            this.Functionality.Settings.deleteScript = deleteScript;
            this.Functionality.Settings.uploadifySettings.script = uploadScript;
            this.Functionality.Settings.captionScript = updateCaptionScript;
         
        }
        protected override void OnLoad(EventArgs e)
        {
            Functionality.Init();
            base.OnLoad(e);
        }
        protected override void OnPreRender(EventArgs e)
        {
            Functionality.InitJS();
            base.OnPreRender(e);
        }
    }
}
