﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_20090518.Controls.WebControls.Common;
using CS.General_20090518.Util;
using System.Web.UI.WebControls;

namespace CS.General_20090518.Controls.WebControls.Specialized.MediaGallery.v2
{
    public class MediaGallery : MyDiv
    {
        public class FUNCTIONALITY
        {
            public event MediaGalleryItemSection.FUNCTIONALITY.UploadSectionItemsHandler OnUploadItems;

            private MediaGallery _gallery;

            /// <summary>
            /// All the settings of the media gallery can be found here
            /// </summary>
            public MediaGallerySettings Settings { get; set; }

            public FUNCTIONALITY(MediaGallery gallery)
            {
                _gallery = gallery;
                Settings = new MediaGallerySettings();
            }

            private void createSections()
            {
                MediaGalleryItemSections sectionsUI = new MediaGalleryItemSections();
                sectionsUI.Functionality.Sections.AddRange(Settings.sections);
                _gallery.Controls.Add(sectionsUI);
                sectionsUI.Functionality.OnUploadItems += new MediaGalleryItemSection.FUNCTIONALITY.UploadSectionItemsHandler(Functionality_OnUploadItems);
            }

            void Functionality_OnUploadItems(List<MyFileUpload> fileUploads, MediaGalleryItemSection section)
            {
                if (OnUploadItems != null)
                {
                    OnUploadItems(fileUploads, section);
                }
            }
            private void initJS()
            {
                string js = "js.com.cs.v2.UI.MediaGallery.v1.MediaGallery.replaceDomElementWithMediaGallery('"+this._gallery.ClientID+"', "+Settings.GetJsObject().GetJS()+");";
                js = JSUtil.MakeJavaScript(js, true);
                _gallery.Controls.Add(new Literal() { Text = js });
            }
            public void Init()
            {
                createSections();
                initJS();
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public MediaGallery()
        {
            Functionality = new FUNCTIONALITY(this);
            this.CssManager.AddClass("cs-media-gallery");
        }
        protected override void OnLoad(EventArgs e)
        {
            Functionality.Init();
            base.OnLoad(e);
        }
    }
}
