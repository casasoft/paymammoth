﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data;
using CS.General_v3.Controls.WebControls.Common.General;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses
{
    using JavaScript.jQuery.Plugins.Tooltips.QTip;

    public class FormFieldsRowItemButtons : FormFieldsRowItemBase
    {
        public new class FUNCTIONALITY : FormFieldsRowItemBase.FUNCTIONALITY
        {
            public List<MyButton> Buttons { get; internal set; }

            public FUNCTIONALITY(FormFieldsRowItemButtons control)
                : base(control)
            {

            }
        }

        public new FUNCTIONALITY Functionality { get { return (FUNCTIONALITY)base.Functionality; } }

        private MyPlaceHolder _placeholderButtons = new MyPlaceHolder();
        public FormFieldsRowItemButtons(List<FormFields.FormFieldsColumnData> columnLayout, FormFields formFields, string helpMessage, _jQueryQTipOptions helpMessageQTipOptions)
            : base(columnLayout, formFields, null, null, null, helpMessage, false, helpMessageQTipOptions)
        {
            this.CssManager.AddClass(formFields.Functionality.CssClassTrRowButtons);
            this.Functionality.Buttons = new List<MyButton>();
            _fieldControl = _placeholderButtons;

            init();
           
        }

        protected override FormFieldsRowItemBase.FUNCTIONALITY getFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        private void addButtons()
        {
            //Set control
            

           
            for (int i = 0; i < this.Functionality.Buttons.Count; i++)
            {
                _placeholderButtons.Controls.Add(this.Functionality.Buttons[i]);
            }

        }
        protected override void init()
        {
            //createButtons();
            base.init();
        }
        protected override void OnLoad(EventArgs e)
        {
            addButtons();
            base.OnLoad(e);
        }
       
      






    }
}
