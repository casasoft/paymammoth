﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common.General;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data
{
    public class FormFieldMulipleChoiceListBox : FormFieldListBaseTypeData<string>
    {
        public FormFieldMulipleChoiceListBox()
        {
            this.Size = 10;
            
            this.JQueryMultiSelect_Sortable = true;
            this.JQueryMultiSelect_Searchable = true;
            this.JQueryMultiSelect = false;
            this.RowCssClassBase = "form-row-multichoice-list";

        }

        public new MyListBox GetField()
        {
            return (MyListBox)base.GetField();
        }
        public int Size { get; set; }

        public bool? JQueryMultiSelect { get { return _validationParams.showJQueryMultiSelect; } set { _validationParams.showJQueryMultiSelect = value; } }

        public bool JQueryMultiSelect_Sortable { get { return _validationParams.jQueryMultiSelectParams.sortable; } set { _validationParams.jQueryMultiSelectParams.sortable = value; } }
        public bool JQueryMultiSelect_Searchable { get { return _validationParams.jQueryMultiSelectParams.searchable; } set { _validationParams.jQueryMultiSelectParams.searchable = value; } }

        protected override IMyFormWebControl createFieldControl()
        {
            MyListBox _list = new MyListBox();
            _list.AllowMultiple = true;
            _list.Size = this.Size;
          
            _list.Attributes["multiple"] = "multiple";
            
            return _list;
        }
        protected override void initFieldParams()
        {
            MyListBox ctrl = (MyListBox)_field;
            foreach (ListItem li in this.ListItems)
            {
                if (li.Selected)
                {
                    int k = 5;
                }
            }
            ctrl.Items.Clear();
            ctrl.Items.AddRange(this.ListItems.ToArray());
            ctrl.JQueryMultiSelect = this.JQueryMultiSelect;
            ctrl.JQueryMultiSelect_Searchable = this.JQueryMultiSelect_Searchable;
            ctrl.JQueryMultiSelect_Sortable = this.JQueryMultiSelect_Sortable;
            base.initFieldParams();
        }
        public override string GetFormValue()
        {
            return this.GetField().FormValue;
            
        }
        
    }
}
