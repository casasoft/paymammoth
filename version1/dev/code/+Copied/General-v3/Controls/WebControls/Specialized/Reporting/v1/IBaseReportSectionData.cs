﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.WebControls.Specialized.Reporting.v1
{
    public interface IBaseReportSectionData : IBaseReportSectionDataBase
    {
       

        new IBaseReportTableData TableData { get; }

    }
}
