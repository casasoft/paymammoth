﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using CS.General_v3.Controls.WebControls.Common;

using CS.General_v3;
using CS.General_v3.Controls.WebControls.Classes;
using CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses;
namespace CS.General_v3.Controls.WebControls.Specialized.MenuTreeStructure
{
    [ToolboxData("<{0}:MenuTreeStructure runat=server></{0}:MenuTreeStructure>")]    
    public class MenuTreeStructure : TreeStructureClasses.TreeStructureBasic.TreeStructureBasicClass
    {
        private class TreeItem : TreeStructureClasses.TreeStructureBasic.ITreeItemBasic
        {
            public TreeItem(MenuTreeStructure menu)
            {
                this._menu = menu;

            }
            private MenuTreeStructure _menu = null;
            public IMenuFolder MenuFolder { get; set; }
            public IMenuItem MenuItem { get; set; }
            public void LoadFromMenuFolder(IMenuFolder folder)
            {
                MenuFolder = folder;
                this.ID = folder.ID;
                this.Priority = folder.Priority;
                this.AllowDelete = folder.AllowDelete;
                this.AllowUpdate = folder.AllowUpdate;
                this.ExtraButtons = new List<ExtraButton>();
                this.Message_Confirm = "Are you sure you want to remove this folder, and all of its sub-items?";
                this.Title = folder.Title;
                this.EditURL = _menu.Functionality.EditFolderPageName + "?" + _menu.Functionality.QuerystringFolderIDParam + "=" + folder.ID;
                
                if (folder.Visible)
                {
                    this.ImageURL = _menu.Functionality.GetImageUrl("folder.gif");
                }
                else
                {
                    this.ImageURL = _menu.Functionality.GetImageUrl("folder_dim.gif");
                }
                if (folder.AllowAddSubFolders)
                {
                    ExtraButton btn = new ExtraButton();
                    btn.Href = _menu.Functionality.EditFolderPageName + "?" + _menu.Functionality.QuerystringParentFolderIDParam + "=" + folder.ID;
                    btn.Title = "Add sub-folder";
                    btn.ImageUrl_Up = _menu.Functionality.GetImageUrl("add_folder_up.gif");
                    btn.ImageUrl_Over= _menu.Functionality.GetImageUrl("add_folder_over.gif");
                    this.ExtraButtons.Add(btn);
                }
                else
                {
                    this.ExtraButtons.Add(null);
                }
                if (folder.AllowAddSubItems)
                {
                    ExtraButton btn = new ExtraButton();
                    btn.Href = _menu.Functionality.EditItemPageName + "?" + _menu.Functionality.QuerystringParentFolderIDParam + "=" + folder.ID;
                    btn.Title = "Add sub-item";
                    btn.ImageUrl_Up = _menu.Functionality.GetImageUrl("add_item_up.gif");
                    btn.ImageUrl_Over = _menu.Functionality.GetImageUrl("add_item_over.gif");
                    this.ExtraButtons.Add(btn);
                }
                else
                {
                    this.ExtraButtons.Add(null);
                }
                if (this.AllowUpdate)
                {
                    ExtraButton btn = new ExtraButton();
                    btn.Href = _menu.Functionality.EditFolderPageName + "?" + _menu.Functionality.QuerystringFolderIDParam + "=" + folder.ID;
                    btn.Title = "Edit";
                    btn.ImageUrl_Up = _menu.Functionality.GetImageUrl("edit_up.gif");
                    btn.ImageUrl_Over = _menu.Functionality.GetImageUrl("edit_over.gif");
                    this.ExtraButtons.Add(btn);
                }
                else
                {
                    this.ExtraButtons.Add(null);
                }
                if (this.AllowDelete)
                {
                    ExtraButton btn = new ExtraButton();
                    btn.Click += new EventHandler(btnDeleteFolder_Click);
                    btn.Title = "Delete";
                    btn.ConfirmMessageOnClick = "Are you sure you want to delete this folder and all of its sub-pages?";

                    btn.ImageUrl_Up = _menu.Functionality.GetImageUrl("delete_up.gif");
                    btn.ImageUrl_Over = _menu.Functionality.GetImageUrl("delete_over.gif");
                    this.ExtraButtons.Add(btn);
                }
                else
                {
                    this.ExtraButtons.Add(null);
                }
            }
            public void LoadFromMenuItem(IMenuFolder parentFolder, IMenuItem item)
            {
                MenuItem = item;
                this.ID = item.ID;
                this.Priority = item.Priority;
                this.AllowDelete = item.AllowDelete;
                this.AllowUpdate = item.AllowUpdate;
                this.ExtraButtons = new List<ExtraButton>();
                this.Message_Confirm = "Are you sure you want to remove this item?";
                this.Title = item.Title;
                this.EditURL = _menu.Functionality.EditItemPageName + "?" +
                    _menu.Functionality.QuerystringParentFolderIDParam + "=" +
                    (parentFolder != null ? parentFolder.ID : 0) + "&" + _menu.Functionality.QuerystringItemIDParam + "=" + item.ID;
                
                if (item.Visible)
                {
                    this.ImageURL = _menu.Functionality.GetImageUrl("item.gif");
                }
                else
                {
                    this.ImageURL = _menu.Functionality.GetImageUrl("item_dim.gif");
                }

                if (this.AllowUpdate)
                {
                    ExtraButton btn = new ExtraButton();
                    btn.Href = this.EditURL;
                    btn.Title = "Edit";
                    btn.ImageUrl_Up = _menu.Functionality.GetImageUrl("edit_up.gif");
                    btn.ImageUrl_Over = _menu.Functionality.GetImageUrl("edit_over.gif");
                    this.ExtraButtons.Add(btn);
                }
                else
                {
                    this.ExtraButtons.Add(null);
                }
                if (this.AllowDelete)
                {
                    ExtraButton btn = new ExtraButton();
                    btn.Click += new EventHandler(btnDeletePage_Click);
                    btn.Title = "Delete Page";
                    btn.ConfirmMessageOnClick = "Are you sure you want to delete this page?";

                    btn.ImageUrl_Up = _menu.Functionality.GetImageUrl("delete_up.gif");
                    btn.ImageUrl_Over = _menu.Functionality.GetImageUrl("delete_over.gif");
                    this.ExtraButtons.Add(btn);
                }
                else
                {
                    this.ExtraButtons.Add(null);
                }
            }
            void btnDeleteFolder_Click(object sender, EventArgs e)
            {
                MyButton btn = (MyButton)sender;
                TreeItem item = (TreeItem)btn.Tag;
                string msg = item.Message_Delete;
                try
                {
                    item.MenuFolder.Remove();
                }
                catch (Exception ex)
                {
                    msg = "Error: " + ex.Message;
                }

                _menu.Functionality.showMessage(msg);
            }
            void btnDeletePage_Click(object sender, EventArgs e)
            {
                MyButton btn = (MyButton)sender;
                TreeItem item = (TreeItem)btn.Tag;
                
                string msg = item.Message_Delete;
                try
                {
                    item.MenuItem.Remove();
                }
                catch (Exception ex)
                {
                    msg = "Error: "+ ex.Message;
                }

                _menu.Functionality.showMessage(msg);
                
            }
            #region ITreeItem Members

            public long ID{get;set;}
            
            public string Title { get; set; }

            public int Priority { get; set; }
            public string ImageURL { get; set; }

            public string EditURL { get; set; }
            public bool AllowUpdate { get; set; }
            public bool AllowDelete { get; set; }
            

            public void Remove()
            {
                if (MenuFolder != null)
                    MenuFolder.Remove();
                if (MenuItem != null)
                    MenuItem.Remove();
            }

            public void Save()
            {
                if (MenuFolder != null)
                    MenuFolder.Save();

                if (MenuItem != null)
                    MenuItem.Save();
            }
            
            public IEnumerable<TreeStructureClasses.TreeStructureBasic.ITreeItemBasic> GetChildTreeItems()
            {
                List<TreeStructureClasses.TreeStructureBasic.ITreeItemBasic> list = new List<TreeStructureClasses.TreeStructureBasic.ITreeItemBasic>();
                if (MenuFolder != null)
                {
                    var folderList = MenuFolder.GetSubFolders();
                    for (int i=0; i < folderList.Count;i++)
                    {
                        IMenuFolder f = (IMenuFolder)folderList[i];
                        TreeItem item = new TreeItem(_menu);
                        item.LoadFromMenuFolder(f);
                        list.Add(item);
                    }
                    var itemList = MenuFolder.GetSubItems();
                    for (int i = 0; i < itemList.Count; i++)
                    {
                        IMenuItem menuItem = (IMenuItem)itemList[i];
                        TreeItem item = new TreeItem(_menu);
                        item.LoadFromMenuItem(MenuFolder, menuItem);
                        list.Add(item);
                    }
                }
                return list;
            }

            public List<ExtraButton> ExtraButtons {get; private set;}
           

            public string Message_Delete { get; set; }
            public string Message_Confirm { get; set; }

            #endregion

            #region ITreeItem Members


            public string AddNewItemURL
            {
                get
                {
                    return "";
                }
                set
                {
                    
                }
            }

            public bool AllowAdd
            {
                get { return false; }
            }

            #endregion

            #region ITreeItemBasic Members


            public virtual void AddExtraButtons()
            {
               
            }

            #endregion

            #region ITreeItemBasic Members


            public string LinkURL
            {
                get { return ""; }
            }

            #endregion
        }
        /* HOW TO USE
         * ----------
         * 
         * Parameters to fill
         * 
         *  - RootFolders
         *  - ImageFolder
         *  - EditFolderPageName
         *  - EditItemPageName
         *  - ItemTitle
         *  - FolderTitle
         *  - CSSFile
         *  
         * Optional
         *  
         *  - QuerystringFolderIDParam
         *  - QuerystringParentFolderIDParam
         *  - QuerystringItemIDParam
         */
        public new class FUNCTIONALITY : TreeStructureClasses.TreeStructureBasic.TreeStructureBasicClass.FUNCTIONALITY
        {
            private MenuTreeStructure _menu;
            public FUNCTIONALITY(MenuTreeStructure menu) : base(menu)
            {
                _menu = menu;

                this.QuerystringParentFolderIDParam = "parent";
                this.QuerystringFolderIDParam = "folder";
                this.QuerystringItemIDParam = "item";
                //this.ImageFolder = "/_common/static/images/components/v1/tree/";
                this.ImageURL_Delete_Over = "/_common/static/images/components/v1/tree/delete_over.gif";
                this.ImageURL_Delete_Up = "/_common/static/images/components/v1/tree/delete_up.gif";
                this.ImageURL_Edit_Over = "/_common/static/images/components/v1/tree/edit_over.gif";
                this.ImageURL_Edit_Up = "/_common/static/images/components/v1/tree/edit_up.gif";
                
                this.PaddingPerLevel = 30;
                this.ShowPriorityAfterName = true;

            }

            #region Constants / Statics


            #endregion
            #region Delegates
            public delegate void ShowMessageEvent(string msg);
            #endregion
            #region Events
            public event ShowMessageEvent ShowMessage;

            #endregion
            #region Methods
            internal void showMessage(string msg)
            {
                if (ShowMessage != null)
                    ShowMessage(msg);
            }

            #endregion
            #region Properties
            public string ImageURL_Edit_Up { get; set; }
            public string ImageURL_Edit_Over { get; set; }
            public string ImageURL_Delete_Up { get; set; }
            public string ImageURL_Delete_Over { get; set; }
            public string ImageURL_AddSubItems_Over { get; set; }
            public string ImageURL_AddSubItems_Up { get; set; }
            
            /*public string ImageURL_Edit_Up { get { return _menu.FunctionalityBase.ImageURL_Edit_Up; } set { _menu.FunctionalityBase.ImageURL_Edit_Up = GetImageUrl(value); } }
            public string ImageURL_Edit_Over { get { return _menu.FunctionalityBase.ImageURL_Edit_Over; } set { _menu.FunctionalityBase.ImageURL_Edit_Over = GetImageUrl(value); } }
            public string ImageURL_Delete_Up { get { return _menu.FunctionalityBase.ImageURL_Delete_Up; } set { _menu.FunctionalityBase.ImageURL_Delete_Up = GetImageUrl(value); } }
            public string ImageURL_Delete_Over { get { return _menu.FunctionalityBase.ImageURL_Delete_Over; } set { _menu.FunctionalityBase.ImageURL_Delete_Over = GetImageUrl(value); } }
             * 
             * */
            //public bool ShowPriorityAfterName { get { return base.show _menu.f.ShowPriorityAfterName; } set { _menu.FunctionalityBase.ShowPriorityAfterName = value; } }
            private List<IMenuFolder> _RootFolders = null;
            /// <summary>
            /// Root Folders
            /// </summary>
            public List<IMenuFolder> RootFolders
            {
                get
                {
                    return _RootFolders;
                }
                set
                {
                    _RootFolders = value;
                    _menu.Functionality.RootItems = GetRootFoldersAsITreeItems();
                    
                }
            }
            protected List<TreeStructureClasses.TreeStructureBasic.ITreeItemBasic> GetRootFoldersAsITreeItems()
            {
                List<TreeStructureClasses.TreeStructureBasic.ITreeItemBasic> list = new List<TreeStructureClasses.TreeStructureBasic.ITreeItemBasic>();
                foreach (var m in RootFolders)
                {
                    TreeItem treeItem = new TreeItem(_menu);
                    treeItem.LoadFromMenuFolder(m);
                    list.Add(treeItem);
                }
                return list;

            }
            /// <summary>
            /// Location of CSS file
            /// </summary>
            //public string CSSFile { get { return _menu.FunctionalityBase.CSSFile; } set { _menu.FunctionalityBase.CSSFile = value; } }
            /// <summary>
            /// Padding per level
            /// </summary>
            //public int PaddingPerLevel { get { return _menu.FunctionalityBase.PaddingPerLevel; } set { _menu.FunctionalityBase.PaddingPerLevel = value; } }
            //private string _ImageFolder;
            /// <summary>
            /// Folder where images are stored
            /// </summary>
           // public string ImageFolder { get { return _ImageFolder; } set { _ImageFolder = value; } }
            /// <summary>
            /// Edit/Add folder page name
            /// </summary>
            public string EditFolderPageName { get; set; }
            /// <summary>
            /// Edit/Add Item page name
            /// </summary>
            public string EditItemPageName { get; set; }
            /// <summary>
            /// The query string parameter for the folder id, in the 
            /// </summary>
            public string QuerystringFolderIDParam { get; set; }

            /// <summary>
            /// The query string parameter for the parent folder id, in the 
            /// </summary>
            public string QuerystringParentFolderIDParam { get; set; }
            /// <summary>
            /// The querystring parameter for the item id, in the edit item page
            /// </summary>
            public string QuerystringItemIDParam { get; set; }
            
            /// <summary>
            /// Title of 'folder', example Link Section. 
            /// </summary>
            public string FolderTitle { get; set; }

            #endregion


          
            private HtmlTable tbTree = null;
            private int ctrlIndex = 0;
            public string GetImageUrl(string filename)
            {
                
                return filename;
            }

            /*
            private void parseRootItems()
            {
                base.RootItems = new List<CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructureBasic.ITreeItemBasic>();
                foreach (var item in this.RootFolders)
                {
                    parseTreeFolder(item);
                    base.RootItems.Add(item);
                }
            }*/
            private ExtraButton GetExtraButton()
            {
                ExtraButton btn = new ExtraButton();
                btn.CssClass = "tree_button";
                return btn;
            }
            private void parseTreeItem(IMenuItem item)
            {
                ExtraButton extraBtn = null; 
                if (item.AllowUpdate)
                {
                    extraBtn = GetExtraButton();
                    extraBtn.Href = item.EditURL;
                    extraBtn.ImageUrl_Over = this.ImageURL_Edit_Over;
                    extraBtn.ImageUrl_Up = this.ImageURL_Edit_Up;
                    extraBtn.Title = "Edit " + this.ItemTitle;
                    item.ExtraButtons.Add(extraBtn);
                }
                else
                {
                    item.ExtraButtons.Add(null);
                }
                if (item.AllowDelete)
                {
                    extraBtn = GetExtraButton();
                    extraBtn.Click += new EventHandler(item_delete);
                    extraBtn.ImageUrl_Over = this.ImageURL_Delete_Over;
                    extraBtn.ImageUrl_Up = this.ImageURL_Delete_Up;
                    extraBtn.Title = "Delete " + this.ItemTitle;

                    extraBtn.ConfirmMessageOnClick = item.Message_ConfirmDelete;
                    item.ExtraButtons.Add(extraBtn);
                }
                else
                {
                    item.ExtraButtons.Add(null);
                }
                item.ExtraButtons.Add(null);
            }

            void item_delete(object sender, EventArgs e)
            {
                MyButton btn = (MyButton)sender;
                IMenuItem item = (IMenuItem)btn.Tag;
                item.Remove();
                showMessage(item.Message_DeleteOK);
            }
            private void parseTreeFolder(IMenuFolder item)
            {
                ExtraButton extraBtn = null;
                
                if (item.AllowUpdate)
                {
                    extraBtn = GetExtraButton();
                    extraBtn.Href = item.EditURL;
                    extraBtn.ImageUrl_Over = this.ImageURL_Edit_Over;
                    extraBtn.ImageUrl_Up = this.ImageURL_Edit_Up;
                    extraBtn.Title = "Edit " + this.ItemTitle;
                    item.ExtraButtons.Add(extraBtn);
                }
                else
                {
                    item.ExtraButtons.Add(null);
                }
                if (item.AllowDelete)
                {
                    extraBtn = GetExtraButton();
                    extraBtn.Click += new EventHandler(folder_delete);
                    extraBtn.ImageUrl_Over = this.ImageURL_Delete_Over;
                    extraBtn.ImageUrl_Up = this.ImageURL_Delete_Up;
                    extraBtn.Title = "Delete " + this.ItemTitle;

                    extraBtn.ConfirmMessageOnClick = item.Message_ConfirmDelete;
                    item.ExtraButtons.Add(extraBtn);
                }
                else
                {
                    item.ExtraButtons.Add(null);
                }
                if (item.AllowAddSubItems)
                {
                    extraBtn = GetExtraButton();
                    extraBtn.Href = item.AddNewItemURL;
                    extraBtn.ImageUrl_Over = this.ImageURL_AddSubItems_Over;
                    extraBtn.ImageUrl_Up = this.ImageURL_AddSubItems_Up;
                    extraBtn.Title = "Add new " + this.ItemTitle;
                    item.ExtraButtons.Add(extraBtn);
                }
                else
                {
                    item.ExtraButtons.Add(null);
                }
                foreach (var child in item.GetChildTreeItems())
                {
                    if (child is IMenuFolder)
                        parseTreeFolder((IMenuFolder)child);
                    else if (child is IMenuItem)
                    {
                        parseTreeItem((IMenuItem)child);
                    }
                        
                }
            }

            void folder_delete(object sender, EventArgs e)
            {
                MyButton btn = (MyButton)sender;
                IMenuFolder folder = (IMenuFolder)btn.Tag;
                folder.Remove();
                showMessage(folder.Message_DeleteOK);
            }
            public override void RenderTree()
            {
                checkParameters();
                //parseRootItems();
                base.RenderTree();
            }
           
            public new void checkParameters()
            {
                base.checkParameters();

                StringBuilder sb = new StringBuilder();
                if (string.IsNullOrEmpty(this.CSSFile))
                    sb.AppendLine("Please specify 'CSSFile'");
               // if (string.IsNullOrEmpty(this.ImageFolder))
               //     sb.AppendLine("Please specify 'ImageFolder'");
                if (string.IsNullOrEmpty(this.EditFolderPageName))
                    sb.AppendLine("Please specify 'EditFolderPageName'");
                if (string.IsNullOrEmpty(this.EditItemPageName))
                    sb.AppendLine("Please specify 'EditItemPageName'");
                if (string.IsNullOrEmpty(this.FolderTitle))
                    sb.AppendLine("Please specify 'FolderTitle'");
                if (string.IsNullOrEmpty(this.ItemTitle))
                    sb.AppendLine("Please specify 'ItemTitle'");
                if (string.IsNullOrEmpty(this.ImageURL_Delete_Over))
                    sb.AppendLine("Please specify 'ImageURL_Delete_Over'");
                if (string.IsNullOrEmpty(this.ImageURL_Delete_Up))
                    sb.AppendLine("Please specify 'ImageURL_Delete_Up'");
                if (string.IsNullOrEmpty(this.ImageURL_Edit_Over))
                    sb.AppendLine("Please specify 'ImageURL_Edit_Over'");
                if (string.IsNullOrEmpty(this.ImageURL_Edit_Up))
                    sb.AppendLine("Please specify 'ImageURL_Edit_Up'");
                if (this.RootFolders == null)
                {
                    sb.AppendLine("Please set 'RootFolder'");
                }
                if (sb.Length > 0)
                {
                    string s = "MenuTreeStructure.MenuTreeStructure:: " + sb.ToString();
                    throw new InvalidOperationException(s);
                }
            }
           
        }
        public new FUNCTIONALITY Functionality
        {
            get
            {
                return (FUNCTIONALITY)base.Functionality;
            }
            

        }

        public MenuTreeStructure()
        {
            //base.Functionality = new FUNCTIONALITY(this);
            //this.Functionality = new FUNCTIONALITY(this);
            
        }

        protected override void OnLoad(EventArgs e)
        {


            base.OnLoad(e);
        }

        protected override void OnInit(EventArgs e)
        {


            base.OnInit(e);
        }
            


 
    }
}
