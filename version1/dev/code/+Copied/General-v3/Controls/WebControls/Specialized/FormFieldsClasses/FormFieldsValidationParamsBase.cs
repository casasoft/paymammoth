﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;
using System.Text.RegularExpressions;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses
{
    using JavaScript.Data;

    public abstract class FormFieldsValidationParamsBase : JavaScriptObject
    {
        
        public string validationGroup;


        public bool doNotValidateOnBlur = false;
        public bool hasValidation = true;
        public bool validateEvenIfNotVisible = false;
        public bool cacheCustomValidationResults = false;
        public bool customValidationValueCaseSensitive = false;
        public List<string> customValidationOmitValues = new List<string>();

        /// <summary>
        /// Whether you would like to show the help message tooltip when there is an error, on hover of validation icon and on focus of field
        /// </summary>
        public bool showErrorHelpMessage = true;

        public int errMsgShowDurationBeforeHideMS = 3000;
        /// <summary>
        /// Just fill in the visual values, not the tooltip elementID
        /// </summary>
        public CS.General_v3.Controls.WebControls.Specialized.Tooltip.ControlTooltip.ControlTooltipJSParams errMsgTooltipParams = new Tooltip.ControlTooltip.ControlTooltipJSParams();

        /// <summary>
        /// The class to set with the error tooltip
        /// </summary>
        public string cssClassErrorTooltipContent = "tooltip-error-content";

        /// <summary>
        /// BaseAjaxValidationHandler item
        /// </summary>
        public string customValidationAjaxHandlerURL;

        /// <summary>
        /// Checks whether this validation parameters really has some validation because else JS will not be injected
        /// </summary>
        /// <returns></returns>
        public virtual bool HasSomeFormOfValidation()
        {
            if (this.hasValidation)
            {
                if (this.customValidationAjaxHandlerURL != null)
                {
                    return true;
                }
            }
            return false;
        }

    }
}
