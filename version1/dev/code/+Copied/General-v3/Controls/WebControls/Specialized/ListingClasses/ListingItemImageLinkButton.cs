﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;

namespace CS.General_v3.Controls.WebControls.Specialized.ListingClasses
{
    public class ListingItemImageLinkButton : MyAnchor, IListingButtonInfo
    {
        private ListingItemButtonBaseFunctionality _functionality;
        public CS.General_v3.Controls.WebControls.Specialized.Listing.ItemHrefRetriever HrefRetriever { get; set; }
        public ListingItemImageLinkButton(CS.General_v3.Controls.WebControls.Specialized.Listing.ItemHrefRetriever HrefRetriever)
        {
            this.HrefRetriever = HrefRetriever;
            _functionality = new ListingItemButtonBaseFunctionality(this);
        }


        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }

        #region IListingButtonInfo Members

        public bool NoValidation { get; set; }

        public new string Text
        {
            get
            {

                return this.InnerHtml;
            }
            set
            {
                this.InnerHtml = value;
                this.Title = value;
                
            }
        }


        public bool ValidationGroup { get; set; }

        public object Tag { get; set; }

        #endregion

        #region IListingButtonInfo Members


        string IListingButtonInfo.ValidationGroup { get; set; }
        #endregion

        #region IListingButtonInfo Members


        public string IconImageUrl
        {
            get;
            set;
        }

        #endregion
    }
        
}
