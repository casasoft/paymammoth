﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;

namespace CS.General_v3.Controls.WebControls.Specialized.ListingClasses
{
    public class ListingItemImageButton : MyButton, IListingButtonInfo
    {
        private ListingItemButtonBaseFunctionality _functionality;
        public ListingItemImageButton()
        {
            _functionality = new ListingItemButtonBaseFunctionality(this);
        }

        #region IListingButtonInfo Members


        public string IconImageUrl
        {
            get;
            set;
        }

        #endregion
    }
        
}
