﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CS.General_20101215.Controls.WebControls.Common;
using CS.General_20101215.Classes.SearchParams;
using CS.General_20101215.Classes.URL;
namespace CS.General_20101215.Controls.WebControls.Specialized.Paging
{
    public class PagingBar : MyDiv
    {
        public class PropertiesClass
        {
            /// <summary>
            /// Pages start from 1, NOT 0
            /// </summary>
            public int? SelectedPageNum { get; set; }
            public int? TotalResults { get; set; }
            public int? PageSize { get; set; }
            public string ResultsTextSingular { get; set; }
            public string ResultsTextPlural { get; set; }
            public string NoResultsText { get; set; }
            public bool InsertInTable { get; set; }
            public bool ShowResultsText { get; set; }

            /// <summary>
            /// This will be something of the form Showing [FROM] - [TO] of [ITEMS].
            /// 
            /// The tags are taken from the constants in this PagingBar.TAG_SHOWING_RESULTS_FROM etc...
            /// </summary>
            public string ShowingResultsFullText { get; set; }
            /// <summary>
            /// The marker to be added where it should indicate that other pages exist
            /// </summary>
            public string ContinuationMarkerHtml { get; set; }
            /// <summary>
            /// The amount of pages to show to the left/right of the selected page.  For example, if 2 is set
            /// and the selected page is 6, then the pages shown are ...4, 5, [6], 7, 8...
            /// </summary>
            public int AmtNearestPagesShown { get; set; }
            public string ButtonNextText { get; set; }
            public string ButtonLastText { get; set; }
            public string ButtonFirstText { get; set; }
            public string ButtonPrevText { get; set; }
            public string PagesSeparatorText { get; set; }

            //public ListItemCollection SortByComboBoxValues { get; set; }
            //public ListItemCollection ShowAmtComboBoxValues { get; set; }

            /// <summary>
            /// The Url to use in the links.  If set to null, it will take the URL of the current page
            /// </summary>
            public string Url { get; set; }
            /// <summary>
            /// The parameter to use in the querystring
            /// </summary>
            public string QuerystringParamPage { get; set; }

            


        }

        public const string TAG_SHOWING_RESULTS_FROM = "[FROM]";
        public const string TAG_SHOWING_RESULTS_TO = "[TO]";
        public const string TAG_SHOWING_RESULTS_TOTAL = "[TOTAL]";
        public const string TAG_SHOWING_RESULTS_ITEMS = "[ITEMS]";

        public PropertiesClass Properties { get; set; }
        //public MyDropDownList CmbSortBy { get; set; }
        //public MyDropDownList CmbShowAmt { get; set; }

        //Use these to seperate them if you want
        public MyDiv DivResultsText { get; private set; }
        public MyDiv DivPagingBar { get; private set; }

        /// <summary>
        /// Interface with this instance to retrieve all methods / properties
        /// </summary>
        public SearchParams SearchParams { get; set; }

        public PagingBar()
        {
            DivResultsText = new MyDiv();
            DivPagingBar = new MyDiv();

            //CmbSortBy = new MyDropDownList();
            //CmbShowAmt = new MyDropDownList();

            Properties = new PropertiesClass();
            Properties.ShowResultsText = true;
            Properties.AmtNearestPagesShown = 2;
            Properties.ResultsTextSingular = "result";
            Properties.ResultsTextPlural = "results";
            Properties.ButtonFirstText = "&lt;&lt; First";
            Properties.ButtonPrevText = "&lt; Prev";
            Properties.ButtonNextText = "Next &gt;";
            Properties.ButtonLastText = "Last &gt;&gt;";
            Properties.PagesSeparatorText = " | ";
            Properties.QuerystringParamPage = "pg";
            Properties.ShowingResultsFullText = "Showing <strong>" + TAG_SHOWING_RESULTS_FROM + " - " + TAG_SHOWING_RESULTS_TO + "</strong> of <strong>" + TAG_SHOWING_RESULTS_TOTAL + "</strong> " + TAG_SHOWING_RESULTS_ITEMS;
            

            this.CssManager.AddClass("paging-bar");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchParams">If you provide the SearchParams, then this class will liaise with the search params to automatically
        /// update values from querystring, such as page number, page size and selected page.  This will make life much easier</param>
        public PagingBar(SearchParams searchParams)
            : this()
        {
            this.SearchParams = searchParams;
        }
        private void initResultsText()
        {
            if (Properties.ShowResultsText)
            {
                if (Properties.SelectedPageNum == null || Properties.TotalResults == null || Properties.PageSize == null)
                {
                    throw new Exception("Please specify SelectedPageNUm, Total Results and PageSize");
                }
                if (Properties.SelectedPageNum == 0) Properties.SelectedPageNum = 1;


                MyDiv divResultsText = DivResultsText;
                divResultsText.CssManager.AddClass("paging-bar-text");

                string text = "";
                if (Properties.TotalResults == 0)
                {
                    text = Properties.NoResultsText;
                }
                else
                {

                    int fromIndex = (Properties.SelectedPageNum.Value - 1) * Properties.PageSize.Value + 1;
                    int toIndex = Math.Min(Properties.TotalResults.Value, Properties.SelectedPageNum.Value * Properties.PageSize.Value);
                    if (Properties.PageSize.Value == 0) toIndex = Properties.TotalResults.Value;
                    text = Properties.ShowingResultsFullText;
                    text = text.Replace(TAG_SHOWING_RESULTS_FROM, fromIndex.ToString("#,0"));
                    text = text.Replace(TAG_SHOWING_RESULTS_TO, toIndex.ToString("#,0"));
                    text = text.Replace(TAG_SHOWING_RESULTS_TOTAL, Properties.TotalResults.Value.ToString("#,0"));
                    text = text.Replace(TAG_SHOWING_RESULTS_ITEMS, (Properties.TotalResults == 1 ? Properties.ResultsTextSingular : Properties.ResultsTextPlural));

                }
                divResultsText.InnerHtml = text;

                Controls.Add(divResultsText);
            }
        }
        
        private void initPages()
        {
            ListItemCollection Pages = new ListItemCollection();

            int amtPages = (int)Math.Ceiling((double)Properties.TotalResults.Value / (double)Properties.PageSize);

            if (Properties.SelectedPageNum > 1)
            {
                //Can have prev
                Pages.Add(new ListItem(Properties.ButtonFirstText, "1"));
                Pages.Add(new ListItem(Properties.ButtonPrevText, (Properties.SelectedPageNum.Value-1).ToString()));
                if (!String.IsNullOrEmpty(Properties.ContinuationMarkerHtml))
                {
                    Pages.Add((string)null); // Add marker
                }
            }
            int fromIndex = Math.Max(1, Properties.SelectedPageNum.Value - Properties.AmtNearestPagesShown);
            int toIndex = Math.Min(amtPages, Properties.SelectedPageNum.Value + Properties.AmtNearestPagesShown);
            for (int i = fromIndex; i <= toIndex; i++)
            {
                Pages.Add(new ListItem(i.ToString(), i.ToString()) { Selected = i == Properties.SelectedPageNum.Value });
            }
            if (Properties.SelectedPageNum < amtPages)
            {
                if (Properties.ContinuationMarkerHtml != null)
                {
                    Pages.Add((string)null); // Add marker
                }
                Pages.Add(new ListItem(Properties.ButtonNextText, (Properties.SelectedPageNum + 1).ToString()));
                Pages.Add(new ListItem(Properties.ButtonLastText, amtPages.ToString()));
            }

            MyDiv divLinks = DivPagingBar;
            divLinks.CssManager.AddClass("paging-bar-pages");


            URLClass url = null;
            if (!String.IsNullOrEmpty(Properties.Url)) {
                url = new URLClass(Properties.Url);
            }
            else {
                url = new URLClass();
            }

            HtmlTable tbl = null;
            if (Properties.InsertInTable)
            {
                tbl = new HtmlTable();
                tbl.Rows.Add(new HtmlTableRow());
                divLinks.Controls.Add(tbl);
            }



            //Create items
            for (int i = 0; i < Pages.Count; i++)
            {
                if (String.IsNullOrEmpty(Pages[i].Text) && String.IsNullOrEmpty(Pages[i].Value))
                {
                    //Show continuation dots
                    addPage(tbl, divLinks, new Literal() { Text = Properties.ContinuationMarkerHtml });
                    continue;
                }
                if (i > 0 && !String.IsNullOrEmpty(Properties.PagesSeparatorText))
                {
                    divLinks.Controls.Add(new Literal() { Text = Properties.PagesSeparatorText });
                }

                HtmlAnchor a = new HtmlAnchor();
                a.InnerHtml = Pages[i].Text;
                if (Pages[i].Selected) {
                    a.Attributes["class"] = "selected";
                }
                url.QS.Set(Properties.QuerystringParamPage, Pages[i].Value);
                a.HRef = url.ToString();
                addPage(tbl, divLinks, a);

                int temp;
                if (!int.TryParse(Pages[i].Text, out temp))
                {
                    a.Attributes["class"] = "arrow";
                }

                
            }
            Controls.Add(divLinks);

        }
        private void addPage(HtmlTable tbl, MyDiv divLinks, Control c)
        {
            if (Properties.InsertInTable)
            {
                HtmlTableCell td = new HtmlTableCell();
                tbl.Rows[0].Cells.Add(td);
                td.Controls.Add(c);
            }
            else
            {
                divLinks.Controls.Add(c);
            }
        }
        private void initComboboxes()
        {

        }
        private void updateFromSearchParams()
        {
            if (SearchParams != null)
            {
                //Update parameters from the specified search params
                this.Properties.QuerystringParamPage = SearchParams.ParamPage.Param;
                this.Properties.SelectedPageNum = SearchParams.Page;
                this.Properties.PageSize = SearchParams.ShowAmt;
            }
        }
        protected void initControls()
        {
            updateFromSearchParams();   
            initResultsText();
            initPages();
            initComboboxes();
        }
        
        protected override void OnLoad(EventArgs e)
        {
            initControls();
            
            base.OnLoad(e);
        }
    }
}
