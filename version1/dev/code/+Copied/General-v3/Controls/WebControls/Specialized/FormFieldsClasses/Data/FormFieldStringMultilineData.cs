﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Controls.WebControls.Common.General;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data
{
    public class FormFieldStringMultilineData : FormFieldStringData
    {

        
        public FormFieldStringMultilineData()
        {
            this.RowCssClassBase = "form-row-multiline";
            this.Rows = 6;
        }
        public new MyTxtBoxTextMultiLine GetField()
        {
            return (MyTxtBoxTextMultiLine)(IMyFormWebControl) base.GetField();
        }
        public int Rows { get; set; }

        protected override IMyFormWebControl createFieldControl()
        {
            MyTxtBoxTextMultiLine txt = new MyTxtBoxTextMultiLine();
            txt.Rows = Rows;
            return txt;
        }
    }
}
