﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_20090518.JavaScript.Data;
using CS.General_20090518.JavaScript.Interfaces;
namespace CS.General_20090518.Controls.WebControls.Specialized.MediaGallery.v2
{
    public class MediaGalleryItemCropData : IJavaScriptObject
    {
        /// <summary>
        /// E.g. Small Image / Medium Image / Large Image
        /// </summary>
        public string Title;
        /// <summary>
        /// The ID with which to communicate to the script
        /// </summary>
        public string CropID;
        /// <summary>
        /// The original image URL
        /// </summary>
        public string ImageURL;

        /// <summary>
        /// Aspect ratio of w/h (e.g. 1 for square) 
        /// 
        /// if the image is a normal 1024x768, then the ratio is 1024/768 = 4/3 = 1.333333333
        /// </summary>
        public double? AspectRatio;

  

        /* public string title;
        public string cropID;
        public string imageUrl;

        /// <summary>
        /// Aspect ratio of w/h (e.g. 1 for square) 
        /// 
        /// if the image is a normal 1024x768, then the ratio is 1024/768 = 4/3 = 1.333333333
        /// </summary>
        public double aspectRatio;
         */

        #region IJavaScriptObject Members

        public JSObject GetJsObject(bool forFlash = false)
        {
            JSObject obj = forFlash ? new JSObjectFlash() : new JSObject();
            obj["title"] = Title;
            obj["cropID"] = CropID;
            obj["imageUrl"] = ImageURL;
            if (AspectRatio.HasValue)
            {
                obj["aspectRatio"] = AspectRatio.Value;
            }
            return obj;
        }

        #endregion
    }
}
