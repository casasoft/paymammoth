﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_20090518.Controls.WebControls.Specialized.TreeStructure
{
    public class ExtraButton
    {
        public string Title { get; set; }
        public string ImageUrl_Up { get; set; }
        public string ImageUrl_Over { get; set; }
        public string Href { get; set; }
        internal EventHandler clickHandler;
        public string ConfirmMessageOnClick { get; set; }

        public event EventHandler Click
        {
            add { clickHandler += value; }
            remove { clickHandler -= value; }
        }
        


    }
        
}
