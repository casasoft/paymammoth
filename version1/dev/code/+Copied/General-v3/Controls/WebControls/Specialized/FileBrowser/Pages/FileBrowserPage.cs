﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Specialized.FileBrowser;
namespace CS.General_v3.Controls.WebControls.Specialized.FileBrowser.Pages
{
    public abstract class FileBrowserPage : CS.General_v3.Classes.Pages.BasePage
    {
        protected FileBrowserCKEditor fileBrowser = null;
        public FileBrowserPage()
        {
            fileBrowser = new FileBrowserCKEditor("fileBrowser");
            
        }
        private void addFileBrowser()
        {
            FileBrowserCKEditor fb = fileBrowser;

            mainDiv.Controls.Add(fb);
            
            fb.Functionality.ImageURL_FolderSmall = "/_common/static/images/components/v1/filebrowser/icon_folder_small.gif";
            fb.Functionality.ImageURL_FolderLarge = "/_common/static/images/components/v1/filebrowser/icon_folder_large.png";
            fb.Functionality.ImageURL_File_Photo = "/_common/static/images/components/v1/filebrowser/icon_photo.png";
            fb.Functionality.ImageURL_File_Video = "/_common/static/images/components/v1/filebrowser/icon_video.png";
            fb.Functionality.ImageURL_File_Other = "/_common/static/images/components/v1/filebrowser/icon_file.png";
            fb.Functionality.ImageURL_Delete = "/_common/static/images/components/v1/filebrowser/delete.gif";


        }
        protected override void OnLoad(EventArgs e)
        {
            addFileBrowser();
            base.OnLoad(e);
        }

        protected virtual Control mainDiv { get { return null;  } }
        

    }
}
