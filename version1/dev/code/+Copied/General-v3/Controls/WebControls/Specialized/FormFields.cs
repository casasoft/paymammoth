﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using CS.General_20090518;
using CS.General_20090518.Classes.MediaItems;
using System.Web.UI;
using CS.General_20090518.Controls.Classes.Forms;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CS.General_20090518.Controls.WebControls.Common;
using CS.General_20090518.Controls.WebControls;
using CS.General_20090518.Controls.WebControls.Common.General;
using CS.General_20090518.Controls.Classes.MediaGallery;

namespace CS.General_20090518.Controls.WebControls.Specialized
{

    [ToolboxData("<{0}:FormFields runat=server></{0}:FormFields>")]
    public class FormFields : BaseWebControl
    {

        private class HelpMessageIconData
        {
            public string HelpMessage { get; set; }
            public IMyFormWebControl Control { get; set;}
            public string StringID { get; set; }
            

            //string helpMessage, IMyFormWebControl control, string stringID
        }

        private List<HelpMessageIconData> _helpMessageIcons = new List<HelpMessageIconData>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tr"></param>
        /// <param name="ctrl">The control or list of conrols (in case of date rage for ex)</param>
        /// <param name="helpMessage"></param>
        public delegate void ControlHandler(HtmlTableRow tr, object ctrl, string helpMessage);

        public event ControlHandler OnControlAdded;

        public int AmountColumns { get; set; }

        private bool _controlsAdded = false;


        public bool Editable { get; set; }

        /* To use this component:
         * 
         * Call the AddXXXXXXXXXX methods, like AddString, AddBool etc to add fields
         * 
         * Attach with the 'ClickSubmit', to attach to the Submit Handler
         * Attach with the 'ClickCancel', to attach to the Cancel Handler.  Buttons are only shown if attached to
         * 
         * Call 'GetFormValue', and its respective similar methods to get the values.  You must always pass the ID of the item created
         * */
        private const string ID_APPEND_TO_VALUE = "_to";
        private class FileUploadButtons
        {
            public MyImageButtonWithText BtnDelete { get; set; }
            public MyButton BtnUpload { get; set; }

        }
        private bool _buttonsAdded = false;
        private string _deleteImageButton = "/_common/images/icons/v1/icon_delete_up.png";

        private HtmlTableRow _currentRow;
        public bool GiveFocusToFirstField { get; set; }


        public MyButton ButtonSubmit { get { return _btnSubmit; } }
        public MyButton ButtonCancel { get { return _btnCancel; } }


        public override void Focus()
        {
            this.FocusGood();
        }
        public void FocusGood()
        {
            if (this._items != null && this._items.Count > 0)
            {

                this._items[0].Control.Control.Focus();

            }


            //CS.General_20090518.Util.Page.GotoAnchor(this.Page, aAnchorCtrl.ClientID);
            //base.Focus();
        }
        public FormFields()
            : this(null)
        {

        }
        public FormFields(string id)
            : base("div")
        {
            this.GiveFocusToFirstField = true;
            this.CustomButtons = new List<MyButton>();
            this.Editable = true;
            if (!string.IsNullOrEmpty(id))
                this.ID = id;
            AmountColumns = 1; // default
            initTable();
            this.CssClass = "form";
            if (!string.IsNullOrEmpty(this.ID))
                this.ValidationGroup = this.ID;

            this.LabelCellCSSClass = "label";
            //this.ValidationGroup = "formfields";
            this.ButtonSubmitText = "Submit";
            this.ButtonCancelText = "Cancel";



        }
        public override string ID
        {
            get
            {
                return base.ID;
            }
            set
            {
                base.ID = value;
                if (string.IsNullOrEmpty(ButtonSubmitId) && !string.IsNullOrEmpty(value))
                {
                    ButtonSubmitId = value + "_btnSubmit";
                }
                if (string.IsNullOrEmpty(ButtonCancelId) && !string.IsNullOrEmpty(value))
                {
                    ButtonCancelId = value + "_btnCancel";
                }
                //if ((this.ValidationGroup == "main" || string.IsNullOrEmpty(this.ValidationGroup)) && !string.IsNullOrEmpty(this.ID))
                //    this.ValidationGroup = this.ID;
            }
        }

        private List<IMyFormWebControl> _controls = new List<IMyFormWebControl>();
        protected List<FormFieldItem> _items = new List<FormFieldItem>();

        public string ButtonSubmitText { get; set; }
        public string ButtonCancelText { get; set; }
        public string ButtonSubmitId { get; set; }
        public string ButtonSubmitClientId { get { return this.ClientID + "_" + this.ButtonSubmitId; } }
        public string ButtonCancelClientId { get { return this.ClientID + "_" + this.ButtonCancelId; } }
        public string ButtonCancelId { get; set; }
        /// <summary>
        /// Used to always show submit button, irrespective of ClickSubmit event
        /// </summary>
        public bool ShowSubmitButtonAlways { get; set; }
        public string LabelCellCSSClass { get; private set; }
        protected HtmlTable _tableFields;

        public HtmlTable TableFields
        {
            get { return _tableFields; }
        }
        protected MyButton _btnSubmit = new MyButton();
        protected MyButton _btnCancel = new MyButton();

        private int _amtColumns = 1;

        public delegate void MediaItemHandler(IMediaItem mediaItem);
        public delegate void MediaItemUploadHandler(MyFileUploadWithMediaItem fileUpload);

        public string ClickSubmitJavascript { get; set; }
        public event EventHandler ClickSubmit;
        public event EventHandler ClickCancel;
        /* Cmmented on 14/05/2010 by Mark, if you find errors, remove comments*/
   // public event EventHandler OnSave;

    public List<MyButton> CustomButtons { get; set; }

    private bool containButtons
    {
        get
        {
            return !String.IsNullOrEmpty(ButtonSubmitText) || !String.IsNullOrEmpty(ButtonCancelText)
                || (CustomButtons != null && CustomButtons.Count > 0) || ShowSubmitButtonAlways;
        }
    }

    /// <summary>
    /// Creates the textbox controls, add it to the table cell and adds it to the list of items
    /// </summary>
    /// <param name="field"></param>
    /// <param name="tr"></param>
    /// <param name="td"></param>
    /// <returns></returns>
    protected virtual TinyMCE addTinyMCE(TinyMCE tinyMCE, HtmlTableRow tr, HtmlTableCell td)
    {
        tinyMCE.Attributes["class"] = "tinymce";


        td.Controls.Add(tinyMCE);
        if (!String.IsNullOrEmpty(tinyMCE.HelpMsg))
        {
            tinyMCE.Attributes["helpMessage"] = tinyMCE.HelpMsg;
        }





        return tinyMCE;
    }


    /// <summary>
    /// Creates the textbox controls, add it to the table cell and adds it to the list of items
    /// </summary>
    /// <param name="field"></param>
    /// <param name="tr"></param>
    /// <param name="td"></param>
    /// <returns></returns>
    protected virtual MyTxtBox addTextBox(MyTxtBox txt, HtmlTableRow tr, HtmlTableCell td)
    {

        Enums.DATA_TYPE dataType = Enums.DATA_TYPE.None;

        if (txt is MyTxtBoxText)
        {





            if (txt is MyTxtBoxTextMultiLine)
            {
                td.Attributes["class"] = "multiline";



            }
            else if (txt is MyTxtBoxTextPassword)
            {
                td.Attributes["class"] = "password";
            }
            else if (txt is MyTxtBoxEmail)
            {
                td.Attributes["class"] = "email";

            }
            else if (txt is MyTxtBoxWebsite)
            {
                td.Attributes["class"] = "website";
            }
        }
        else if (txt is MyTxtBoxNumeric)
        {
            td.Attributes["class"] = "numeric";


        }
        else if (txt is MyTxtBoxDate)
        {
            td.Attributes["class"] = "date";
            if (txt.HelpMsg == null)
            {
                txt.HelpMsg = "Please enter a date in the format - dd/mm/yyyy";
            }
        }

        tr.Attributes["class"] = td.Attributes["class"];
        td.Controls.Add(txt);
        if (!String.IsNullOrEmpty(txt.HelpMsg))
        {
            txt.Attributes["helpMessage"] = txt.HelpMsg;
        }
        // _items.Add(new FormFieldItem(tr, txt, Enums.DATA_TYPE.String));





        return txt;
    }

    private MyDropDownList addDropdownList(MyDropDownList control, HtmlTableRow tr, HtmlTableCell td)
    {
        tr.Attributes["class"] = "select";
        td.Attributes["class"] = "select";
        td.Controls.Add(control);
        //_items.Add(new FormFieldItem(tr, control));
        return control;
    }

    private MyCheckBox addCheckBox(MyCheckBox control, HtmlTableRow tr, HtmlTableCell td)
    {
        tr.Attributes["class"] = "checkbox";
        td.Attributes["class"] = "checkbox";
        control.Text = control.Title;
        td.Controls.Add(control);
        //_items.Add(new FormFieldItem(tr, control));

        return control;
    }

    private MyRadioButton addRadioButton(MyRadioButton control, HtmlTableRow tr, HtmlTableCell td)
    {
        tr.Attributes["class"] = "radiobutton";
        td.Attributes["class"] = "radiobutton";
        control.Text = control.Title;
        td.Controls.Add(control);
        // _items.Add(new FormFieldItem(tr, control));

        return control;
    }

    private MyCheckBoxList addCheckBoxList(MyCheckBoxList control, HtmlTableRow tr, HtmlTableCell td)
    {
        tr.Attributes["class"] = "checkbox-list";
        td.Attributes["class"] = "checkbox-list";
        td.Controls.Add(control);
        // _items.Add(new FormFieldItem(tr, control));
        return control;
    }

   /* private MyRadioButtonList addRadioButtonList(MyRadioButtonList control, HtmlTableRow tr, HtmlTableCell td)
    {
        tr.Attributes["class"] = "radiobutton-list";
        td.Attributes["class"] = "radiobutton-list";
        td.Controls.Add(control);
        //   _items.Add(new FormFieldItem(tr, control));
        return control;
    }*/

        private FileUploadButtons addFileUploadWithMediaItem(MyFileUploadWithMediaItem control, HtmlTableRow tr, HtmlTableCell td)
        {
            FileUploadButtons buttons = new FileUploadButtons();
            tr.Attributes["class"] = "fileupload";
            td.Attributes["class"] = "fileupload";

            IMediaItem mediaItem = control.MediaItem;
            HtmlTable tblUpload = new HtmlTable();
            tblUpload.CellPadding = tblUpload.CellSpacing = 0;

            HtmlTableRow trMediaItem = new HtmlTableRow();
            //File details
            if (control.MediaItem != null)
            {

                tblUpload.Rows.Add(trMediaItem);

                HtmlTableCell tdItem = new HtmlTableCell();
                trMediaItem.Cells.Add(tdItem);

                HtmlTable tblItem = new HtmlTable();
                tdItem.Controls.Add(tblItem);
                tblItem.CellSpacing = tblItem.CellPadding = 0;
                tblItem.Rows.Add(new HtmlTableRow());

                HtmlTableCell tdThumb = new HtmlTableCell();
                HtmlTableCell tdFileDetails = new HtmlTableCell();
                tblItem.Rows[0].Cells.Add(tdThumb);
                tblItem.Rows[0].Cells.Add(tdFileDetails);
                tdFileDetails.Attributes["class"] = "file-details";
                tdThumb.Attributes["class"] = "thumb";

                MyImage imgThumb = new MyImage();
                HtmlAnchor aFile = new HtmlAnchor();
                imgThumb.CssClass = "image";
                imgThumb.ImageUrl = mediaItem.ThumbnailImageURL;
                imgThumb.AlternateText = mediaItem.Caption;
                if (!String.IsNullOrEmpty(mediaItem.ItemURL) || !String.IsNullOrEmpty(mediaItem.LargeImageURL))
                {
                    aFile.HRef = imgThumb.HRef = !(String.IsNullOrEmpty(mediaItem.ItemURL)) ? mediaItem.ItemURL : mediaItem.LargeImageURL;
                    imgThumb.HRefTarget = HREF_TARGET.Blank;
                    aFile.Target = "_blank";
                    aFile.InnerHtml = imgThumb.HRef;

                    if (aFile.InnerHtml.IndexOf("/") != -1)
                    {
                        aFile.InnerHtml = aFile.InnerHtml.Substring(aFile.InnerHtml.LastIndexOf("/") + 1);
                    }

                    tdFileDetails.Controls.Add(aFile);
                    if (mediaItem.FileSizeKB > 0)
                    {
                        Literal brFileSize = new Literal();
                        brFileSize.Text = "<br />";
                        Literal ltlFileSize = new Literal();
                        ltlFileSize.Text = mediaItem.FileSizeKB + "kb";
                        tdFileDetails.Controls.Add(brFileSize);
                        tdFileDetails.Controls.Add(ltlFileSize);
                    }

                    Literal br = new Literal();
                    br.Text = "<br />";

                    tdFileDetails.Controls.Add(br);

                    MyImageButtonWithText btnDelete = new MyImageButtonWithText();
                    btnDelete.ValidationGroup = this.ID + "_btnDelete";
                    btnDelete.ImageUrl = _deleteImageButton;
                    btnDelete.Text = "Delete";
                    tdFileDetails.Controls.Add(btnDelete);

                    buttons.BtnDelete = btnDelete;
                }
                tdThumb.Controls.Add(imgThumb);




            }

            HtmlTableRow trFileUpload = new HtmlTableRow();
            tblUpload.Rows.Add(trFileUpload);
            HtmlTableCell tdFileUpload = new HtmlTableCell();
            trFileUpload.Cells.Add(tdFileUpload);
            tdFileUpload.Controls.Add(control);

            td.Controls.Add(tblUpload);





            if (trMediaItem != null)
            {
                trMediaItem.Cells.Add(new HtmlTableCell());
            }

            trFileUpload.Cells.Add(new HtmlTableCell());

            MyButton btnUpload = new MyButton();
            btnUpload.Text = "Upload";

            if (!control.Required)
            {
                //If it is not required, then there is no validation for this control but it does not make any sense to upload an empty file
                btnUpload.ValidationGroup = control.ValidationGroup = this.ClientID;
                //control.Required = true;
            }
            trFileUpload.Cells[1].Attributes["class"] = "upload-button";
            trFileUpload.Cells[1].Controls.Add(btnUpload);
            buttons.BtnUpload = btnUpload;


            _items.Add(new FormFieldItem(control.ID, tr, control, Enums.DATA_TYPE.FileUploadWithMediaItem));

            return buttons;
        }


        private HtmlGenericControl addLabel(IMyFormWebControl control, HtmlTableCell td)
        {
            HtmlGenericControl label = new HtmlGenericControl("label");
            label.InnerHtml = control.Title;

            if (!label.InnerHtml.EndsWith("?"))
            {
                label.InnerHtml += ":";
            }
            if (control.Required)
            {
                label.InnerHtml += " *";
            }
            td.Controls.Add(label);
            return label;
        }

        private HtmlTable createTableForFromTo(WebControl ctrlFrom, WebControl ctrlTo)
        {

            HtmlTable tbl = new HtmlTable();
            tbl.CellPadding = tbl.CellSpacing = 0;
            tbl.Rows.Add(new HtmlTableRow());
            HtmlTableCell tdFrom = new HtmlTableCell();
            HtmlTableCell tdMiddle = new HtmlTableCell();
            HtmlTableCell tdTo = new HtmlTableCell();
            tbl.Rows[0].Cells.Add(tdFrom);
            tbl.Rows[0].Cells.Add(tdMiddle);
            tbl.Rows[0].Cells.Add(tdTo);

            tdFrom.Controls.Add(ctrlFrom);
            tdMiddle.InnerHtml = "&nbsp;-&nbsp;";
            tdTo.Controls.Add(ctrlTo);
            return tbl;
        }

        private void createNextTableCells(string labelTitle, bool required, out HtmlTableRow tr, out HtmlTableCell tdControl, out MyLabel label)
        {
            if (_currentRow == null || (int)(_currentRow.Cells.Count / 2) == AmountColumns)
            {
                if (_currentRow != null)
                {

                }
                _currentRow = new HtmlTableRow();
                _tableFields.Rows.Add(_currentRow);



            }

            HtmlTableCell tdLabel = new HtmlTableCell();
            tdControl = new HtmlTableCell();
            _currentRow.Cells.Add(tdLabel);
            _currentRow.Cells.Add(tdControl);
            tdLabel.Attributes["class"] = LabelCellCSSClass;
            label = new MyLabel();
            label.Text = labelTitle;
            if (labelTitle != null && labelTitle.IndexOf(":") == -1 && !labelTitle.EndsWith("?"))
            {
                label.Text += ":";
            }
            if (required && Editable)
            {
                label.Text += " *";
            }
            tdLabel.Controls.Add(label);
            tr = _currentRow;

        }

        private void createFieldRow(string labelTitle, bool required, out HtmlTableRow tr, out HtmlTableCell tdControl, out MyLabel label)
        {
            createNextTableCells(labelTitle, required, out tr, out tdControl, out label);


        }


        private void addButtons()
        {
            _buttonsAdded = true;
            if (containButtons)
            {
                HtmlTableRow trButtons = new HtmlTableRow();

                if (ClickSubmit != null || ClickCancel != null || !string.IsNullOrEmpty(ClickSubmitJavascript) || ShowSubmitButtonAlways)
                {
                    this._tableFields.Rows.Add(trButtons);
                }


                trButtons.Cells.Add(new HtmlTableCell());
                trButtons.Cells.Add(new HtmlTableCell());

                trButtons.Cells[1].Attributes["class"] = "buttons";
                if (ClickSubmit != null || !string.IsNullOrEmpty(ClickSubmitJavascript) || ShowSubmitButtonAlways)
                {
                    MyButton btnSubmit = _btnSubmit;
                    btnSubmit.ID = ButtonSubmitId;
                    //btnSubmit.ID = this.ID + "_" + "btnSubmit";
                    btnSubmit.ValidationGroup = ValidationGroup;
                    btnSubmit.Text = ButtonSubmitText;
                    trButtons.Cells[1].Controls.Add(btnSubmit);
                    btnSubmit.Attributes["class"] = "button";
                    if (ClickSubmit != null)
                    {
                        btnSubmit.Click += new EventHandler(btnSubmit_Click);
                    }
                    if (!string.IsNullOrEmpty(ClickSubmitJavascript))
                    {
                        btnSubmit.OnClientClick = ClickSubmitJavascript;
                    }
                    _btnSubmit = btnSubmit;
                }
                if (ClickCancel != null)
                {
                    MyButton btnCancel = _btnCancel;
                    btnCancel.ID = ButtonCancelId;
                    btnCancel.Text = ButtonCancelText;
                    btnCancel.ValidationGroup = ValidationGroup + "_cancelButton";
                    trButtons.Cells[1].Controls.Add(btnCancel);
                    btnCancel.Attributes["class"] = "button-grey";
                    btnCancel.Click += new EventHandler(btnCancel_Click);
                    _btnCancel = btnCancel;
                }
                if (CustomButtons != null && CustomButtons.Count > 0)
                {
                    for (int i = 0; i < CustomButtons.Count; i++)
                    {
                        MyButton btn = CustomButtons[i];
                        if (string.IsNullOrEmpty(btn.ID))
                            btn.ID = this.ID + "_CustomButton_" + i;

                        btn.ValidationGroup = ValidationGroup;


                        trButtons.Cells[1].Controls.Add(btn);
                        btn.Attributes["class"] = "button";

                    }
                }

            }
        }

        void btnCancel_Click(object sender, EventArgs e)
        {
            if (ClickCancel != null)
            {
                ClickCancel(this, e);
            }
        }

        void btnSubmit_Click(object sender, EventArgs e)
        {
            if (ClickSubmit != null)
            {
                ClickSubmit(this, e);
            }
            /* Cmmented on 14/05/2010 by Mark, if you find errors, remove comments
             * if (OnSave != null)
            {
                OnSave(this, e);
            }*/
        }


        private void initTable()
        {
            this._tableFields = new HtmlTable();
            this._tableFields.CellPadding = this._tableFields.CellSpacing = 0;




            this.Controls.Add(this._tableFields);
        }
        protected override void OnInit(EventArgs e)
        {



            base.OnInit(e);
        }
        private MyLink aAnchorCtrl = null;
        private void addAnchorControl()
        {
            if (!String.IsNullOrEmpty(this.ID))
            {
                aAnchorCtrl = new MyLink();
                aAnchorCtrl.IsAnchorBookmark = true;
                aAnchorCtrl.ID = this.ID + "_anchor";
                this.Controls.AddAt(0, aAnchorCtrl);
            }
            else
            {
                throw new Exception("Please provide ID");
            }



        }
        private void updateValidationGroups()
        {
            foreach (var item in this._items)
            {
                IMyFormWebControl formControl = item.Control;
                formControl.ValidationGroup = this.ValidationGroup;

            }
        }
        private void checkValidationGroup()
        {
            //if (string.IsNullOrEmpty(this.ValidationGroup) || this.ValidationGroup == "main")
            if (string.IsNullOrEmpty(this.ValidationGroup))
                this.ValidationGroup = this.ID;
            updateValidationGroups();
        }
        private void addOnLoadControls()
        {
            if (!_controlsAdded)
            {
                checkValidationGroup();
                if (!Editable)
                {
                    CssManager.AddClass("non-editable");
                }


                addAnchorControl();
                AddCurrentRow(); //just in case
                AddDefaultButtons();
                this._tableFields.Attributes["class"] = CssClass;

                // _tableFields.CellSpacing = CellSpacing;
                giveFocusToFirstField();
                _controlsAdded = true;
            }
        }
        /// <summary>
        /// Used when you want to use IDs of buttons for example
        /// </summary>
        public void ForceOnLoadBefore()
        {
            addOnLoadControls();
        }
        protected override void OnLoad(EventArgs e)
        {

            addOnLoadControls();
            createHelpMessageIcons();
            base.OnLoad(e);
            //Trigger that items have been added

        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }
        private void giveFocusToFirstField()
        {
            if (_items.Count > 0 && GiveFocusToFirstField)
            {
                var itemFocus = _items[0];
                itemFocus.Control.FocusGood();
            }
        }
        protected override void RenderContents(HtmlTextWriter writer)
        {
            base.RenderContents(writer);
        }
        void btnTest_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }
        private void addControlFormField(HtmlTableRow tr, Control c)
        {
            if (c is IMyFormWebControl)
            {
                IMyFormWebControl formControl = (IMyFormWebControl)c;
                _items.Add(new FormFieldItem(formControl.ID, tr, formControl, formControl.DataType));
            }
        }
        public HtmlTableRow AddControl(Control c)
        {
            HtmlTableRow tr;
            HtmlTableCell tdControl;
            MyLabel label;
            createNextTableCells(null, false, out tr, out tdControl, out label);
            tdControl.Controls.Add(c);

            addControlFormField(tr, c);

            return tr;
        }
        public HtmlTableRow AddGenericLabelAndControl(string title, bool required, Control c)
        {
            HtmlTableCell td = null;
            return AddGenericLabelAndControl(title, required, c, out td);
        }
        public HtmlTableRow AddGenericLabelAndControl(string title, bool required, Control c, out HtmlTableCell tdControl)
        {
            HtmlTableRow tr;


            MyLabel label = new MyLabel();
            createNextTableCells(title, required, out tr, out tdControl, out label);
            if (label != null)
            {
                label.Control = c;
            }

            //label.Attributes["for"] = c.ID;
            tdControl.Controls.Add(c);
            addControlFormField(tr, c);


            return tr;
        }

        public void AddDefaultButtons()
        {
            if (!_buttonsAdded)
            {
                AddCurrentRow();
                addButtons();
            }
        }

        /// <summary>
        /// Add a field to the list of FormFields
        /// </summary>
        /// <param name="field"></param>
        public virtual IMyFormWebControl AddField2(IMyFormWebControl control)
        {

            control.ValidationGroup = ValidationGroup;

            this._controls.Add(control);
            //this.showControlField(control);
            return control;
        }

        public virtual IMyFormWebControl AddField2(MyFileUploadWithMediaItem txtUpload, MediaItemHandler DeleteHandler, MediaItemUploadHandler UploadHandler)
        {

            txtUpload.ValidationGroup = ValidationGroup;

            this._controls.Add(txtUpload);
            FileUploadButtons buttons = null;//(FileUploadButtons) this.showControlField(txtUpload);

            if (buttons.BtnDelete != null)
            {
                if (DeleteHandler == null)
                {
                    buttons.BtnDelete.Visible = false;
                }
                else
                {
                    buttons.BtnDelete.Tag = txtUpload.MediaItem;
                    buttons.BtnDelete.Tag2 = DeleteHandler;
                    buttons.BtnDelete.Click += new EventHandler(BtnDeleteMediaItem_Click);
                }
            }
            if (buttons.BtnUpload != null)
            {
                if (UploadHandler == null)
                {
                    buttons.BtnUpload.Visible = false;
                }
                else
                {
                    buttons.BtnUpload.Tag = txtUpload;
                    buttons.BtnUpload.Tag2 = UploadHandler;
                    buttons.BtnUpload.Click += new EventHandler(BtnUploadMediaItem_Click);
                }
            }
            return txtUpload;
        }

        void BtnUploadMediaItem_Click(object sender, EventArgs e)
        {
            MyButton btn = (MyButton)sender;
            MyFileUploadWithMediaItem fileUpload = (MyFileUploadWithMediaItem)btn.Tag;
            MediaItemUploadHandler handler = (MediaItemUploadHandler)btn.Tag2;

            handler(fileUpload);
        }

        void BtnDeleteMediaItem_Click(object sender, EventArgs e)
        {
            MyImageButtonWithText btn = (MyImageButtonWithText)sender;
            IMediaItem item = (IMediaItem)btn.Tag;
            MediaItemHandler handler = (MediaItemHandler)btn.Tag2;

            handler(item);

        }











        public MyFileUpload GetControlAsMyFileUpload(string fieldID)
        {
            return (MyFileUpload)GetControl(fieldID);

        }
        public IMyFormWebControl GetControl(string fieldID)
        {
            FormFieldItem item = GetFormFieldItem(fieldID);
            if (item != null)
            {
                return item.Control;
            }
            return null;
        }
        public IMyFormWebControl GetControlTo(string fieldID)
        {
            FormFieldItemRange item = (FormFieldItemRange)GetFormFieldItem(fieldID);
            if (item != null)
            {
                return item.ControlTo;
            }
            return null;
        }
        /// <summary>
        /// Get the respective form field item with the HtmlTableRow
        /// </summary>
        /// <param name="fieldID"></param>
        /// <returns></returns>
        public FormFieldItem GetFormFieldItem(string fieldID)
        {
            for (int i = 0; i < this._items.Count; i++)
            {
                FormFieldItem item = this._items[i];
                if (String.Compare(item.ID, fieldID, true) == 0)
                {
                    return item;
                }
            }
            return null;
        }
        /// <summary>
        /// Get the value of the field by ID.  If it is in search mode and the field ID is a number,
        /// or is a date, then a list of values are passed for the FROM - TO
        /// </summary>
        /// <param name="fieldID"></param>
        /// <returns></returns>
        public object GetValueTo(string fieldID)
        {
            FormFieldItemRange item = (FormFieldItemRange)GetFormFieldItem(fieldID);

            if (item != null)
            {
                return item.ValueTo;
            }
            return null;

        }
        /// <summary>
        /// Get the value of the field by ID.  If it is in search mode and the field ID is a number,
        /// or is a date, then a list of values are passed for the FROM - TO
        /// </summary>
        /// <param name="fieldID"></param>
        /// <returns></returns>
        public object GetValue(string fieldID)
        {
            FormFieldItem item = GetFormFieldItem(fieldID);

            if (item != null)
            {
                return item.Value;
            }
            return null;

        }
        public int GetValueAsInt(string fieldID)
        {
            object o = GetValue(fieldID);
            int result = 0;
            if (o is string)
            {
                result = Convert.ToInt32((string)o);
            }
            else
            {
                result = Convert.ToInt32(o);
            }
            return result;
        }
        public long GetValueAsLong(string fieldID)
        {
            object o = GetValue(fieldID);
            long result = 0;
            if (o is string)
            {
                result = Convert.ToInt64((string)o);
            }
            else
            {
                result = Convert.ToInt64(o);
            }
            return result;
        }
        public int? GetValueAsIntNullable(string fieldID)
        {
            object o = GetValue(fieldID);
            int? result = null;
            if (o is string)
            {
                string s = (string)o;
                int tmp;
                if (!string.IsNullOrEmpty(s) && Int32.TryParse(s, out tmp))
                    result = tmp;

            }
            else
            {
                if (o != null)
                    result = Convert.ToInt32(o);
            }
            return result;
        }
        public bool? GetValueAsBoolNullable(string fieldID)
        {
            object obj = GetValue(fieldID);
            bool? b = null;
            if (obj != null)
            {
                if (obj is bool)
                {
                    b = (bool)obj;
                }
                else if (obj is string)
                {
                    string s = (string)obj;

                    s = s.ToLower();
                    if (!string.IsNullOrEmpty(s))
                    {
                        b = (s == "1" || s == "yes" || s == "true");
                    }
                }
                else if (obj is int)
                {
                    int i = (int)obj;
                    b = (i > 0);

                }
            }
            return b;
        }
        public System.IO.Stream GetFormValueStream(string fieldID)
        {
            var c = GetControl(fieldID);
            MyFileUpload txt = (MyFileUpload)c;
            return txt.FileContent;
        }
        public string GetFormValueFilename(string fieldID)
        {
            var c = GetControl(fieldID);
            MyFileUpload txt = (MyFileUpload)c;
            return txt.FileName;
        }
        public object GetFormValue(string fieldID)
        {
            FormFieldItem item = GetFormFieldItem(fieldID);

            if (item != null)
            {

                object obj = item.FormValue;
                if (obj != null)
                {

                    if (item.DataType == Enums.DATA_TYPE.IntegerMultipleChoice)
                    {
                        List<string> listString = (List<string>)obj;
                        List<int> list = new List<int>();
                        foreach (var s in listString)
                        {
                            list.Add(Util.Conversion.ToInt32(s));
                        }
                        obj = list;
                    }
                }
                return obj;
            }
            else
            {
                throw new InvalidOperationException("Form field '" + fieldID + "' does not exist in <" + this.ClientID + ">");
            }
            return null;

        }
        public object GetFormValueTo(string fieldID)
        {
            FormFieldItemRange item = (FormFieldItemRange)GetFormFieldItem(fieldID);

            if (item != null)
            {
                return item.FormValueTo;
            }
            return null;

        }
        public string GetFormValueStr(string fieldID)
        {
            
            object o = GetFormValue(fieldID);
            return (string)o;
        }
        public bool GetFormValueBool(string fieldID)
        {
            object o = GetFormValue(fieldID);
            return (bool)o;
        }
        public List<int> GetFormValueListInt(string fieldID)
        {
            object o = GetFormValue(fieldID);
            return (List<int>)o;

        }
        public int GetFormValueInt(string fieldID)
        {
            object o = GetFormValue(fieldID);
            return Util.Conversion.ToInt32(o);
        }
        public int? GetFormValueIntNullable(string fieldID)
        {
            object o = GetFormValue(fieldID);
            if (o == null || string.IsNullOrEmpty(o.ToString()))
            {
                return null;
            }
            else
            {
                return Util.Conversion.ToInt32(o);
            }
        }
        public double GetFormValueDouble(string fieldID)
        {
            object o = GetFormValue(fieldID);
            return Util.Conversion.ToDouble(o);
        }
        public DateTime GetFormValueDate(string fieldID)
        {
            object o = GetFormValue(fieldID);
            return Util.Conversion.ToDateTime(o);
        }

        private string _ValidationGroup = null;
        public virtual string ValidationGroup
        {
            get
            {
                string s = _ValidationGroup;

                //if (string.IsNullOrEmpty(_ValidationGroup))
                //    s = this.ID;
                //: Mark changed this on 22/04/09 
                //if (string.IsNullOrEmpty(s))
                //    s = this.ID;
                return s;
            }
            set
            {
                _ValidationGroup = value;
            }
        }



        public string DeleteImageButton { get { return _deleteImageButton; } set { _deleteImageButton = value; } }

        #region Adding Controls
        public MyButton AddCustomButton(string text, EventHandler clickHandler, string clickJavascript)
        {
            MyButton btn = new MyButton();
            btn.Text = text;
            if (clickHandler != null)
                btn.Click += clickHandler;
            if (!string.IsNullOrEmpty(clickJavascript))
                btn.OnClientClick = clickJavascript;
            if (this.CustomButtons == null)
                this.CustomButtons = new List<MyButton>();
            this.CustomButtons.Add(btn);
            return btn;

        }

        public MyCheckBox AddBool(string ID, string title, bool required, bool initValue)
        {
            return AddBool(ID, title, required, initValue, null);
        }
        public MyCheckBox AddBool(string ID, string title, bool required, bool initValue, string helpMessage)
        {
            string ctrlID = getControlID(ID);


            HtmlTableRow tr;
            HtmlTableCell tdControl;
            MyLabel label;
            createFieldRow(title, required, out tr, out tdControl, out label);


            IMyFormWebControl ctrl = null;

            MyCheckBox chk = null;
            tdControl.Attributes["class"] = "checkbox";
            tr.Attributes["class"] = tdControl.Attributes["class"];
            if (Editable)
            {

                chk = new MyCheckBox(ctrlID, title, initValue);
                //chk.ID = id;
                tdControl.Controls.Add(chk);
                label.Control = chk;
                //label.Attributes["for"] = chk.ClientID;
                _items.Add(new FormFieldItem(ID, tr, chk, Enums.DATA_TYPE.Bool));
            }
            else
            {
                tdControl.InnerHtml = (initValue ? "Yes" : "No");
            }
            fieldAdded(tr, ctrl, helpMessage);

            return chk;
        }
        public MyDropDownList AddBoolNullable(string ID, string title, bool required, string helpMessage, int? width, bool? initValue)
        {
            string ctrlID = getControlID(ID);
            HtmlTableRow tr;
            HtmlTableCell tdControl;
            MyLabel label;
            createFieldRow(title, required, out tr, out tdControl, out label);
            IMyFormWebControl ctrl = null;
            tdControl.Attributes["class"] = "select";

            MyDropDownList cmb = new MyDropDownList(ctrlID, title, required, width, null);
            cmb.Items.Add(new ListItem("", ""));
            cmb.Items.Add(new ListItem("Yes", "1"));
            cmb.Items.Add(new ListItem("No", "0"));
            string s = "";
            if (initValue != null)
            {
                if (initValue.Value)
                    s = "1";
                else
                    s = "0";
            }
            cmb.SetValue(s);
            tdControl.Controls.Add(cmb);
            label.Control = cmb;
            //label.Attributes["for"] = cmb.ClientID;
            _items.Add(new FormFieldItem(ID, tr, cmb, Enums.DATA_TYPE.BoolNullable));
            fieldAdded(tr, cmb, helpMessage);
            return cmb;
        }
        public MyHiddenField AddHiddenField(string id, string title, bool required)
        {
            MyHiddenField f = new MyHiddenField();
            f.ID = id;
            f.Title = title;
            f.Required = required;
            //    f.ValidationGroup = this.ValidationGroup;
            _items.Add(new FormFieldItem(id, null, f, Enums.DATA_TYPE.String));
            AddControl(f);
            return f;

        }
        public IMyFormWebControl AddString(string id, string title, bool required, string helpMessage, int? width, string initValue, bool createHelpMessageIconJS = true)
        {
            HtmlTableRow tr;
            return addString(Enums.STRING_DATA_TYPE.SingleLine, id, title, required, helpMessage, 0, width, initValue, null,out tr, createHelpMessageIconJS);
        }
        public IMyFormWebControl AddString(string id, string title, bool required, string helpMessage, int? width, string initValue, ListItemCollection choice, bool createHelpMessageIconJS = true)
        {
            HtmlTableRow tr;
            return addString(Enums.STRING_DATA_TYPE.SingleLine, id, title, required, helpMessage, 0, width, initValue, choice, out tr, createHelpMessageIconJS);
        }
        public IMyFormWebControl AddString(CS.General_20090518.Enums.STRING_DATA_TYPE stringType, string id, string title, bool required,
            string helpMessage, int? width, string initValue, bool createHelpMessageIconJS = true)
        {
            if (stringType == Enums.STRING_DATA_TYPE.MultiLine)
            {
                //return AddStringMultiline(id, title, required, helpMessage, 5, width, initValue);
                throw new InvalidOperationException("Please call 'AddStringMultiline' to add a multi-line string, and do not set the enum value");
            }
            else if (stringType == Enums.STRING_DATA_TYPE.SingleLine)
            {
                return AddString(id, title, required, null, null, initValue, createHelpMessageIconJS);
                throw new InvalidOperationException("Please do not call this overriden method for 'AddString' when adding a 'SingleLine'");
            }
            HtmlTableRow tr;
            return addString(stringType, id, title, required, helpMessage, 0, width, initValue, null, out tr);
        }
        public IMyFormWebControl AddString(CS.General_20090518.Enums.STRING_DATA_TYPE stringType, string id, string title, bool required,
            string helpMessage, int? width, string initValue, out HtmlTableRow tr, bool createHelpMessageIconJS = true)
        {
            

            return addString(stringType, id, title, required, helpMessage, 0, width, initValue, null, out tr, createHelpMessageIconJS);
        }
        public IMyFormWebControl AddStringMultiline(string id, string title, bool required,
            string helpMessage, int Rows, int? width, string initValue, bool createHelpMessageIconJS = true)
        {
            HtmlTableRow tr;
            return addString(Enums.STRING_DATA_TYPE.MultiLine, id, title, required, helpMessage, Rows, width, initValue, null, out tr, createHelpMessageIconJS);
        }
        public TinyMCE AddTinyMCE(string ID, string title, bool required,
            string helpMessage, int Rows, int? width, string initValue)
        {
            string ctrlID = getControlID(ID);
            TinyMCE tinymce = new TinyMCE(ctrlID, title, required, helpMessage, width, Rows, initValue);
            HtmlTableRow tr;
            HtmlTableCell tdControl;
            MyLabel label;
            createFieldRow(title, required, out tr, out tdControl, out label);
            tinymce.ValidationGroup = this.ValidationGroup;
            tinymce.Attributes["helpMessage"] = helpMessage;
            tdControl.Controls.Add(tinymce);
            label.Control = tinymce;
            //label.Attributes["for"] = tinymce.ClientID;
            _items.Add(new FormFieldItem(ID, tr, tinymce, Enums.DATA_TYPE.String));
            fieldAdded(tr, tinymce, helpMessage);
            return tinymce;
        }
        public CS.General_20090518.Controls.WebControls.Common.FCKEditor.FCKeditor AddFCKEditor(string ID, string title, bool required,
            string helpMessage, int width, int height, string initValue)
        {
            string ctrlID = getControlID(ID);
            CS.General_20090518.Controls.WebControls.Common.FCKEditor.FCKeditor fckEditor =
                new CS.General_20090518.Controls.WebControls.Common.FCKEditor.FCKeditor(ctrlID, width, height, initValue);

            HtmlTableRow tr;
            HtmlTableCell tdControl;
            MyLabel label;
            createFieldRow(title, required, out tr, out tdControl, out label);
            label.Control = fckEditor;
            //label.Attributes["for"] = fckEditor.ClientID;
            //tinymce.ValidationGroup = this.ValidationGroup;
            //tinymce.Attributes["helpMessage"] = helpMessage;
            tdControl.Controls.Add(fckEditor);
            _items.Add(new FormFieldItem(ID, tr, fckEditor, Enums.DATA_TYPE.String));
            fieldAdded(tr, fckEditor, helpMessage);
            return fckEditor;
        }
        public CS.General_20090518.Controls.WebControls.Common.CKEditor AddCKEditor(string ID, string title, bool required,
           string helpMessage, int? width, int? height, string initValue)
        {
            string ctrlID = getControlID(ID);
            CS.General_20090518.Controls.WebControls.Common.CKEditor ckEditor =
                new CKEditor(ctrlID);
            if (width.HasValue)
                ckEditor.Width = width.Value;
            if (!height.HasValue) height = 500;
            ckEditor.Height = height.Value;

            ckEditor.Text = initValue;



            HtmlTableRow tr;
            HtmlTableCell tdControl;
            MyLabel label;
            createFieldRow(title, required, out tr, out tdControl, out label);
            label.Control = ckEditor;
            //label.Attributes["for"] = fckEditor.ClientID;
            //tinymce.ValidationGroup = this.ValidationGroup;
            //tinymce.Attributes["helpMessage"] = helpMessage;
            tdControl.Controls.Add(ckEditor);
            _items.Add(new FormFieldItem(ID, tr, ckEditor, Enums.DATA_TYPE.String));
            fieldAdded(tr, ckEditor, helpMessage);
            return ckEditor;
        }
        private string getControlID(string id)
        {
            string s = this.ID;
            if (!string.IsNullOrEmpty(s))
                s += "_";
            s += id;
            return s;
        }
        private string getListItemCollectionToString(ListItemCollection coll)
        {
            return getListItemCollectionToString(coll, ", ");
        }

        private string getListItemCollectionToString(ListItemCollection coll, string delimiter)
        {
            StringBuilder sb = new StringBuilder();
            int count = 0;
            for (int i = 0; i < coll.Count; i++)
            {
                if (coll[i].Selected)
                {
                    if (count > 0)
                    {
                        sb.Append(delimiter);
                    }
                    sb.Append(coll[i].Text);
                    count++;
                }
            }
            if (sb.Length == 0)
            {
                sb.Append("N/A");
            }
            return sb.ToString(); ;
        }

        private IMyFormWebControl addString(CS.General_20090518.Enums.STRING_DATA_TYPE stringType, string ID, string title, bool required,
            string helpMessage, int Rows, int? width, string initValue, ListItemCollection choice, out HtmlTableRow tr, bool createHelpMessageIconJS = true)
        {
            string ctrlID = getControlID(ID);
            //HtmlTableRow tr;
            HtmlTableCell tdControl;
            MyLabel label;
            createFieldRow(title, required, out tr, out tdControl, out label);
            IMyFormWebControl ctrl = null;
            if (!Editable)
            {
                if (String.IsNullOrEmpty(initValue))
                {
                    tdControl.InnerHtml = "/";
                }
                else
                {
                    if (choice == null)
                    {
                        if (stringType == Enums.STRING_DATA_TYPE.Website)
                        {
                            tdControl.InnerHtml = "<a href='" + initValue + "' title='Visit - " + initValue + "' target='_blank'>" + initValue + "</a>";
                        }
                        else if (stringType == Enums.STRING_DATA_TYPE.Email)
                        {
                            MyEmail email = new MyEmail();
                            email.Email = initValue;
                            email.Title = "Submit email to " + initValue;
                            tdControl.Controls.Add(email);
                        }
                        else
                        {
                            tdControl.InnerHtml = initValue;

                        }
                    }
                    else
                    {
                        tdControl.InnerHtml = getListItemCollectionToString(choice);
                    }

                }
            }
            else
            {


                if (choice != null)
                {
                    MyDropDownList cmb = new MyDropDownList(ctrlID, title, required);
                    cmb.ValidationGroup = this.ValidationGroup;
                    ctrl = cmb;
                    for (int i = 0; i < choice.Count; i++)
                    {
                        ListItem item = choice[i];
                        if (item.Value == initValue)
                        {
                            item.Selected = true;
                        }
                        cmb.Items.Add(item);
                    }
                    if (!string.IsNullOrEmpty(helpMessage))
                    {

                        cmb.Attributes["helpMessage"] = helpMessage;

                    }
                    tdControl.Controls.Add(cmb);
                    tdControl.Attributes["class"] = "select";
                }
                else
                {
                    MyTxtBox txt = null;
                    if (stringType == Enums.STRING_DATA_TYPE.MultiLine)
                    {
                        txt = new MyTxtBoxTextMultiLine(ctrlID, title, required, helpMessage, width, Rows, initValue);
                        tdControl.Attributes["class"] = "multiline";
                    }
                    else if (stringType == Enums.STRING_DATA_TYPE.Password)
                    {
                        txt = new MyTxtBoxTextPassword(ctrlID, title, required, helpMessage, width, initValue);
                        tdControl.Attributes["class"] = "password";
                    }
                    else if (stringType == Enums.STRING_DATA_TYPE.Email)
                    {
                        txt = new MyTxtBoxEmail(ctrlID, title, required, helpMessage, width, initValue);
                        tdControl.Attributes["class"] = "email";

                    }
                    else if (stringType == Enums.STRING_DATA_TYPE.Website)
                    {
                        txt = new MyTxtBoxWebsite(ctrlID, title, required, helpMessage, width, initValue);
                        tdControl.Attributes["class"] = "website";
                    }
                    else if (stringType == Enums.STRING_DATA_TYPE.SingleLine)
                    {
                        txt = new MyTxtBoxTextSingleLine(ctrlID, title, required, helpMessage, width, initValue);
                        tdControl.Attributes["class"] = "singleline";
                    }

                    txt.ValidationGroup = this.ValidationGroup;
                    tdControl.Controls.Add(txt);
                    if (!String.IsNullOrEmpty(helpMessage))
                    {
                        //txt.Attributes["helpMessage"] = helpMessage;
                    }
                    txt.ID = ctrlID;
                    ctrl = txt;
                }
                _items.Add(new FormFieldItem(ID, tr, ctrl, Enums.DATA_TYPE.String));
                if (label != null)
                {
                    label.Control = ctrl.Control;
                }
            }

            //label.Attributes["for"] = ctrl.Control.ID;
            tr.Attributes["class"] = tdControl.Attributes["class"];
            fieldAdded(tr, ctrl, helpMessage);
            if (!string.IsNullOrEmpty(helpMessage) && createHelpMessageIconJS)
            {
                initHelpMessage(helpMessage, ctrl, ID);
            }
            return ctrl;

        }
        public MyTxtBoxDate AddDateTime(string id, string title, bool required, string helpMessage, int? width, DateTime? initValue)
        {
            MyLabel label;
            return addDateTime(id, title, required, helpMessage, width, false, null, null, false, initValue, null, false, out label)[0];

        }

        public MyTxtBoxDate AddDateTime(string id, string title, bool required, string helpMessage, int? width, DateTime? initValue, out MyLabel label)
        {
            return addDateTime(id, title, required, helpMessage, width, false, null, null, false, initValue, null, false, out label)[0];

        }
        public List<MyTxtBoxDate> AddDateTime(string id, string title, bool required, string helpMessage, int? width,
            DateTime? fromDate, DateTime? toDate, bool hasTime, DateTime? initValue, bool nullable)
        {
            MyLabel label;
            return addDateTime(id, title, required, helpMessage, width, false, fromDate, toDate, hasTime, initValue, null, nullable, out label);
        }
        public List<MyTxtBoxDate> AddDateTime(string id, string title, bool required, string helpMessage, int? width,
            DateTime? fromDate, DateTime? toDate, bool hasTime, DateTime? initValueFrom, DateTime? initValueTo, bool nullable)
        {
            MyLabel label;
            return addDateTime(id, title, required, helpMessage, width, true, fromDate, toDate, hasTime, initValueFrom, initValueTo, nullable, out label);
        }
        private List<MyTxtBoxDate> addDateTime(string ID, string title, bool required, string helpMessage, int? width, bool hasRange,
            DateTime? fromDate, DateTime? toDate, bool hasTime, DateTime? initValueFrom, DateTime? initValueTo, bool nullable, out MyLabel label)
        {
            string ctrlID = getControlID(ID);
            List<MyTxtBoxDate> result = new List<MyTxtBoxDate>();
            HtmlTableRow tr;
            HtmlTableCell td;
            createFieldRow(title, required, out tr, out td, out label);
            td.Attributes["class"] = "date";
            tr.Attributes["class"] = td.Attributes["class"];
            Enums.DATA_TYPE dataType = Enums.DATA_TYPE.DateTime;
            if (Editable)
            {
                if (nullable)
                    dataType = Enums.DATA_TYPE.DateTimeNullable;

                if (helpMessage == null)
                {
                    helpMessage = "Please enter a date in the format - dd/mm/yyyy";
                }

                if (!hasRange)
                {
                    MyTxtBoxDate txt = new MyTxtBoxDate(ctrlID, title, required, helpMessage, fromDate, toDate, hasTime, initValueFrom);
                    txt.ID = ctrlID;
                    td.Controls.Add(txt);
                    label.Control = txt;
                    //label.Attributes["for"] = txt.ClientID;
                    txt.ValidationGroup = this.ValidationGroup;
                    if (!String.IsNullOrEmpty(helpMessage))
                    {
                       // txt.Attributes["helpMessage"] = helpMessage;
                    }
                    _items.Add(new FormFieldItem(ID, tr, txt, dataType));
                    result.Add(txt);
                }
                else
                {
                    //These will need to set up a RANGE not just one field.
                    // _items.RemoveAt(_items.Count - 1); //Remove the last entered as we're going to replace that with ItemRange
                    MyTxtBoxDate txtFrom = new MyTxtBoxDate(ctrlID, title, required, helpMessage, fromDate, toDate, hasTime, initValueFrom);
                    MyTxtBoxDate txtTo = new MyTxtBoxDate(ctrlID + ID_APPEND_TO_VALUE, title, required, helpMessage, fromDate, toDate, hasTime, initValueTo);
                    txtFrom.ValidationGroup = txtTo.ValidationGroup = this.ValidationGroup;
                    td.Controls.Clear(); // Clear all controls as we need to add a table
                    td.Controls.Add(createTableForFromTo(txtFrom, txtTo));
                    label.Control = txtFrom;
                    //label.Attributes["for"] = txtFrom.ClientID;
                    //txtFrom.ID = id;
                    //txtTo.ID = id + "_To";
                    _items.Add(new FormFieldItemRange(ID, tr, txtFrom, txtTo, dataType));
                    result.Add(txtFrom);
                    result.Add(txtTo);
                }
            }
            else
            {
                td.InnerHtml = initValueFrom.Value.ToString();
                if (initValueTo != null)
                {
                    td.InnerHtml += " - " + initValueTo.Value.ToString();
                }
            }
            fieldAdded(tr, result, helpMessage);
            for (int i = 0; i < result.Count; i++)
            {
                string id = ID;
                if (result.Count > 1)
                {
                    id += "-" + i;
                }
                initHelpMessage(helpMessage, result[i], id);
            }
            return result;
        }
        public MyFileUpload AddFileUpload(string ID, string title, bool required, string helpMessage, int? width)
        {
            string ctrlID = getControlID(ID);
            MyFileUpload txtUpload = new MyFileUpload(ctrlID, title, required, width);

            WebControl result = null;
            HtmlTableRow tr;
            HtmlTableCell tdControl;
            MyLabel label;
            createFieldRow(title, required, out tr, out tdControl, out label);
            IMyFormWebControl ctrl = null;



            txtUpload.ValidationGroup = this.ValidationGroup;
            tdControl.Controls.Add(txtUpload);
            if (!String.IsNullOrEmpty(helpMessage))
            {
                //txtUpload.Attributes["helpMessage"] = helpMessage;
            }
            txtUpload.ID = ctrlID;
            ctrl = txtUpload;
            result = txtUpload;

            _items.Add(new FormFieldItem(ID, tr, ctrl, Enums.DATA_TYPE.String));
            label.Control = ctrl.Control;
            //label.Attributes["for"] = ctrl.Control.ID;
            fieldAdded(tr, ctrl, helpMessage);
            initHelpMessage(helpMessage, ctrl, ID);
            return txtUpload;

        }

        public MyFileUploadWithMediaItem AddFileUploadWithMediaItem(string id, string title, bool required, string helpMessge, int? width,
            IMediaItem mediaItem, MediaItemHandler DeleteHandler, MediaItemUploadHandler UploadHandler)
        {

            bool mediaItemFilled = false;
            if (mediaItem.IsEmpty())
            {
                mediaItem = null;
            }
            else
            {
                mediaItemFilled = true;

            }


            MyFileUploadWithMediaItem txtUpload = new MyFileUploadWithMediaItem(id, title, required && !mediaItemFilled, width, mediaItem);
            txtUpload.ValidationGroup = ValidationGroup;
            HtmlTableRow tr;
            HtmlTableCell tdControl;
            MyLabel label;
            createFieldRow(title, required, out tr, out tdControl, out label);
            this._controls.Add(txtUpload);
            label.Control = txtUpload;
            //label.Attributes["for"] = txtUpload.ClientID;
            FileUploadButtons buttons = (FileUploadButtons)addFileUploadWithMediaItem(txtUpload, tr, tdControl);// this.showControlField(txtUpload);

            if (buttons.BtnDelete != null)
            {
                if (DeleteHandler == null || required)
                {
                    buttons.BtnDelete.Visible = false;
                }
                else
                {
                    buttons.BtnDelete.Tag = txtUpload.MediaItem;
                    buttons.BtnDelete.Tag2 = DeleteHandler;
                    buttons.BtnDelete.Click += new EventHandler(BtnDeleteMediaItem_Click);
                }
            }
            if (buttons.BtnUpload != null)
            {
                if (UploadHandler == null)
                {
                    buttons.BtnUpload.Visible = false;
                }
                else
                {
                    buttons.BtnUpload.Tag = txtUpload;
                    buttons.BtnUpload.Tag2 = UploadHandler;
                    buttons.BtnUpload.Click += new EventHandler(BtnUploadMediaItem_Click);
                }
            }
            fieldAdded(tr, txtUpload, helpMessge);
            return txtUpload;

        }
        public IMyFormWebControl AddDouble(string id, string title, bool required, string helpMessage, int? width, double? initValue)
        {
            return addDouble(id, title, required, helpMessage, null, null, MyTxtBoxNumeric.NUMERIC_RANGE.All,
                 width, false, initValue, null, false, null);
        }
        public IMyFormWebControl AddDouble(string id, string title, bool required, string helpMessage, int? width, double? initValue, ListItemCollection coll)
        {
            return addDouble(id, title, required, helpMessage, null, null, MyTxtBoxNumeric.NUMERIC_RANGE.All,
                width, false, initValue, null, false, coll);
        }
        public IMyFormWebControl AddDouble(string id, string title, bool required, string helpMessage, double? numFrom, double? numTo,
            MyTxtBoxNumeric.NUMERIC_RANGE numericRange,
            int? width, double? initValue, bool nullable)
        {
            return addDouble(id, title, required, helpMessage, numFrom, numTo, numericRange, width, false, initValue, null, nullable, null);

        }
        public IMyFormWebControl AddDouble(string id, string title, bool required, string helpMessage, double? numFrom, double? numTo,
            MyTxtBoxNumeric.NUMERIC_RANGE numericRange,
            int? width, double? initValueFrom, double? initValueTo, bool nullable)
        {
            return addDouble(id, title, required, helpMessage, numFrom, numTo, numericRange,
                width, true, initValueFrom, initValueTo, nullable, null);

        }

        private IMyFormWebControl addDouble(string ID, string title, bool required, string helpMessage, double? numFrom, double? numTo,
            MyTxtBoxNumeric.NUMERIC_RANGE numericRange,
            int? width, bool hasRange, double? initValueFrom, double? initValueTo, bool nullable, ListItemCollection choices)
        {
            IMyFormWebControl control = null;

            string ctrlID = getControlID(ID);
            Enums.DATA_TYPE dataType = Enums.DATA_TYPE.Double;
            if (nullable)
                dataType = Enums.DATA_TYPE.DoubleNullable;
            if (hasRange && choices != null)
            {
                throw new InvalidOperationException("Invalid arguments: You cannot set 'range' to true and also set 'choices'");
            }
            HtmlTableRow tr;
            HtmlTableCell tdControl;
            MyLabel label;
            createFieldRow(title, required, out tr, out tdControl, out label);
            if (!Editable)
            {

                tdControl.InnerHtml = initValueFrom.Value.ToString();
                if (initValueTo != null)
                {
                    tdControl.InnerHtml += " - " + initValueTo.Value.ToString();
                }
            }
            else
            {
                if (choices != null)
                {
                    MyDropDownList cmb = new MyDropDownList(ctrlID, title, required);
                    for (int i = 0; i < choices.Count; i++)
                    {
                        cmb.Items.Add(choices[i]);
                    }
                    if (!string.IsNullOrEmpty(helpMessage))
                    {

                        //cmb.Attributes["helpMessage"] = helpMessage;

                    }
                    cmb.ValidationGroup = this.ValidationGroup;
                    tdControl.Controls.Add(cmb);
                    label.Control = cmb;
                    tdControl.Attributes["class"] = "select";
                    _items.Add(new FormFieldItem(ID, tr, cmb, dataType));
                    control = cmb;
                }
                else
                {
                    MyTxtBox txt = null;
                    if (!hasRange)
                    {
                        txt = new MyTxtBoxNumericDouble(ctrlID, title, required, helpMessage, numFrom, numTo,
                            numericRange, width, initValueFrom);
                        txt.ValidationGroup = this.ValidationGroup;
                        tdControl.Controls.Add(txt);
                        label.Control = txt;
                        if (!String.IsNullOrEmpty(helpMessage))
                        {
                            //txt.Attributes["helpMessage"] = helpMessage;
                        }
                        _items.Add(new FormFieldItem(ID, tr, txt, dataType));
                    }
                    else
                    {
                        //These will need to set up a RANGE not just one field.
                        // _items.RemoveAt(_items.Count - 1); //Remove the last entered as we're going to replace that with ItemRange
                        txt = new MyTxtBoxNumericDouble(ctrlID, title, required, helpMessage, numFrom, numTo, numericRange,
                            width, initValueFrom);
                        MyTxtBoxNumericDouble txtFrom = (MyTxtBoxNumericDouble)txt;
                        MyTxtBoxNumericDouble txtTo = new MyTxtBoxNumericDouble(ctrlID + ID_APPEND_TO_VALUE, title, required, helpMessage, numFrom, numTo, numericRange,
                            width, initValueTo);
                        txtFrom.ValidationGroup = txtTo.ValidationGroup = this.ValidationGroup;
                        tdControl.Controls.Clear(); // Clear all controls as we need to add a table
                        tdControl.Controls.Add(createTableForFromTo(txtFrom, txtTo));
                        label.Control = txtFrom;
                        //label.Attributes["for"] = txtFrom.ClientID;
                        _items.Add(new FormFieldItemRange(ID, tr, txtFrom, txtTo, dataType));
                    }
                    control = txt;

                }

                tdControl.Attributes["class"] += "numeric";


                tr.Attributes["class"] = "numeric";

            }
            fieldAdded(tr, control, helpMessage);
            initHelpMessage(helpMessage, control, ID);
            return control;
        }


        public IMyFormWebControl AddInteger(string id, string title, bool required, string helpMessage, int? width, int? initValue)
        {
            return addInteger(id, title, required, helpMessage, null, null, MyTxtBoxNumeric.NUMERIC_RANGE.All,
                width, false, false, initValue, null, false, null);
        }
        public IMyFormWebControl AddInteger(string id, string title, bool required, string helpMessage, int? width, int? initValue, ListItemCollection coll)
        {
            return addInteger(id, title, required, helpMessage, null, null, MyTxtBoxNumeric.NUMERIC_RANGE.All,
                width, false, false, initValue, null, false, coll);
        }
        //22/04/2009 Mark added required parameter
        public IMyFormWebControl AddIntegerMultipleChoice(string id, string title, string helpMessage, bool required, ListItemCollection coll)
        {
            return addInteger(id, title, required, helpMessage, null, null, MyTxtBoxNumeric.NUMERIC_RANGE.All,
                null, false, true, null, null, false, coll);
        }

        public IMyFormWebControl AddInteger(string id, string title, bool required, string helpMessage, int? numFrom, int? numTo,
            MyTxtBoxNumeric.NUMERIC_RANGE numericRange,
            int? width, int? initValue, bool nullable)
        {
            return addInteger(id, title, required, helpMessage, numFrom, numTo, numericRange,
                width, false, false, initValue, null, nullable, null);

        }

        public IMyFormWebControl AddRadioButtonList(string id, string title, bool required, string helpMessage, int? width, string initValue, ListItemCollection items, RepeatLayout layout)
        {
            string ctrlID = getControlID(id);


            HtmlTableRow tr;
            HtmlTableCell tdControl;
            MyLabel label;
            createFieldRow(title, required, out tr, out tdControl, out label);


            IMyFormWebControl ctrl = null;

            MyRadioButtonList rd = null;
            tdControl.Attributes["class"] = "radiobutton-list";
            tr.Attributes["class"] = tdControl.Attributes["class"];
            if (Editable)
            {

                rd = new MyRadioButtonList(ctrlID, title, required);
                rd.RepeatLayout = layout;

                CS.General_20090518.Util.Data.FillComboBox(rd, items);
                rd.SelectedValue = initValue;
                if (string.IsNullOrEmpty(initValue))
                {
                    rd.SelectedValue = items[0].Value;
                }
                //chk.ID = id;
                tdControl.Controls.Add(rd);
                label.Control = rd;
                //label.Attributes["for"] = chk.ClientID;
                _items.Add(new FormFieldItem(id, tr, rd, Enums.DATA_TYPE.String));
            }
            else
            {
                tdControl.InnerHtml = (!string.IsNullOrEmpty(initValue) ? initValue : "N/A");
            }
            fieldAdded(tr, ctrl, helpMessage);
            initHelpMessage(helpMessage, ctrl, id);
            return rd;

        }
        public IMyFormWebControl AddInteger(string id, string title, bool required, string helpMessage, int? numFrom, int? numTo,
            MyTxtBoxNumeric.NUMERIC_RANGE numericRange,
            int? width, int? initValueFrom, int? initValueTo, bool nullable)
        {
            return addInteger(id, title, required, helpMessage, numFrom, numTo, numericRange,
                width, true, false, initValueFrom, initValueTo, nullable, null);

        }

        private void selectItemFromListCollection(ListItemCollection coll, string value)
        {
            for (int i = 0; i < coll.Count; i++)
            {
                coll[i].Selected = coll[i].Selected || (coll[i].Value == value);
            }
        }

        private IMyFormWebControl addInteger(string ID, string title, bool required, string helpMessage, int? numFrom, int? numTo,
            MyTxtBoxNumeric.NUMERIC_RANGE numericRange,
            int? width, bool hasRange, bool allowMultipleChoice, int? initValueFrom, int? initValueTo, bool nullable, ListItemCollection choices)
        {
            IMyFormWebControl ctrl = null;
            if (initValueFrom.HasValue && choices != null)
            {
                selectItemFromListCollection(choices, initValueFrom.Value.ToString());
            }
            string ctrlID = getControlID(ID);
            Enums.DATA_TYPE dataType = Enums.DATA_TYPE.Integer;
            if (nullable)
                dataType = Enums.DATA_TYPE.IntegerNullable;
            if (allowMultipleChoice)
                dataType = Enums.DATA_TYPE.IntegerMultipleChoice;
            if (hasRange && choices != null)
            {
                throw new InvalidOperationException("Invalid arguments: You cannot set 'range' to true and also set 'choices'");
            }
            HtmlTableRow tr;
            HtmlTableCell tdControl;
            MyLabel label;
            createFieldRow(title, required, out tr, out tdControl, out label);
            if (Editable)
            {
                if (choices != null)
                {
                    if (!allowMultipleChoice)
                    {
                        MyDropDownList cmb = new MyDropDownList(ctrlID, title, required);
                        ctrl = cmb;
                        cmb.ValidationGroup = this.ValidationGroup;
                        for (int i = 0; i < choices.Count; i++)
                        {
                            cmb.Items.Add(choices[i]);
                        }
                        cmb.SetValue(initValueFrom);
                        if (width.HasValue)
                        {
                            cmb.Width = width.GetValueOrDefault(150);
                        }
                        label.Control = cmb;
                    }
                    else
                    {
                        MyCheckBoxList chkList = new MyCheckBoxList(ctrlID, title);
                        ctrl = chkList;
                        chkList.Required = required;
                        chkList.RepeatColumns = 8;
                        chkList.RepeatDirection = RepeatDirection.Horizontal;
                        chkList.RepeatLayout = RepeatLayout.Table;
                        chkList.ValidationGroup = this.ValidationGroup;
                        foreach (ListItem item in choices)
                        {

                            chkList.Items.Add(item);
                        }


                        label.Control = chkList;
                        //label.Attributes["for"] = chkList.ClientID;
                    }

                    if (!string.IsNullOrEmpty(helpMessage))
                    {

                        //ctrl.Control.Attributes["helpMessage"] = helpMessage;

                    }
                    tdControl.Attributes["class"] = "select";
                    tdControl.Controls.Add(ctrl.Control);
                    _items.Add(new FormFieldItem(ID, tr, (IMyFormWebControl)ctrl, dataType));


                }
                else
                {
                    MyTxtBox txt = null;
                    if (!hasRange)
                    {
                        txt = new MyTxtBoxNumericInteger(ctrlID, title, required, helpMessage, numFrom, numTo,
                            numericRange, width, initValueFrom);
                        txt.ValidationGroup = this.ValidationGroup;
                        tdControl.Controls.Add(txt);
                        if (!String.IsNullOrEmpty(helpMessage))
                        {
                            //txt.Attributes["helpMessage"] = helpMessage;
                        }
                        _items.Add(new FormFieldItem(ID, tr, txt, dataType));
                        ctrl = txt;
                        label.Control = txt;
                        //label.Attributes["for"] = txt.ClientID;
                    }
                    else
                    {
                        //These will need to set up a RANGE not just one field.
                        // _items.RemoveAt(_items.Count - 1); //Remove the last entered as we're going to replace that with ItemRange
                        MyTxtBoxNumericInteger txtFrom = new MyTxtBoxNumericInteger(ctrlID, title, required, helpMessage, numFrom, numTo, numericRange,
                            width, initValueFrom);
                        MyTxtBoxNumericInteger txtTo = new MyTxtBoxNumericInteger(ctrlID + ID_APPEND_TO_VALUE, title, required, helpMessage, numFrom, numTo, numericRange,
                            width, initValueTo);
                        txtFrom.ValidationGroup = txtTo.ValidationGroup = this.ValidationGroup;
                        tdControl.Controls.Clear(); // Clear all controls as we need to add a table
                        tdControl.Controls.Add(createTableForFromTo(txtFrom, txtTo));

                        _items.Add(new FormFieldItemRange(ID, tr, txtFrom, txtTo, dataType));
                        ctrl = txtFrom;
                        label.Control = txtFrom;
                        //label.Attributes["for"] = txtFrom.ClientID;
                    }

                }
            }
            else
            {
                if (choices == null)
                {

                    tdControl.InnerHtml = "";
                    if (initValueFrom != null)
                    {
                        tdControl.InnerHtml = initValueFrom.Value.ToString();
                    }

                    if (initValueTo != null)
                    {
                        tdControl.InnerHtml += " - " + initValueTo.Value;
                    }
                }
                else
                {
                    tdControl.InnerHtml = getListItemCollectionToString(choices);
                }


                //throw new NotImplementedException("See how to format integer when not editable");
            }
            tdControl.Attributes["class"] += "numeric";
            fieldAdded(tr, ctrl, helpMessage);
            if (!string.IsNullOrEmpty(helpMessage))
            {
                initHelpMessage(helpMessage, ctrl, ID);
            }
            return ctrl;

        }
        private void createHelpMessageIcons()
        {
            for (int i = 0; i < _helpMessageIcons.Count; i++)
            {
                var data = _helpMessageIcons[i];
                createHelpMessageIcon(data.HelpMessage, data.Control, data.StringID);
            }
        }
        private void createHelpMessageIcon(string helpMessage, IMyFormWebControl control, string stringID)
        {

            return;
            //2010-06-26 - the 'control != null ' was added by Kalr
            if (!string.IsNullOrEmpty(helpMessage) && control != null)
            {
                helpMessage = helpMessage.Replace("\r", "");
                helpMessage = helpMessage.Replace("\n", "");

                //Escape ' since the string is contained in a '
                helpMessage = CS.General_20090518.Util.Text.forJS(helpMessage, "'");
                string customCSS = "";
                customCSS = "help-message-" + stringID;
                string js = "new js.com.cs.v2.UI.HelpMessageIcon.HelpMessageIcon('" + control.ClientID + "','" + helpMessage + "','" + customCSS + "');";
                js = CS.General_20090518.Util.JSUtil.MakeJavaScript(js, false);


                this.Page.ClientScript.RegisterStartupScript(this.GetType(), control.ClientID + "helpMessage", js);
                //control.Control.Parent.Controls.Add(new Literal() { Text = js });
            }
        }
        private void initHelpMessage(string helpMessage, IMyFormWebControl control, string stringID)
        {
            _helpMessageIcons.Add(new HelpMessageIconData() { HelpMessage = helpMessage, Control = control, StringID = stringID });
            
        }
        public void AddCurrentRow()
        {
            if (_currentRow != null && _currentRow.Cells.Count > 0)
            {
                int remainingCells = (AmountColumns * 2) - _currentRow.Cells.Count;
                if (remainingCells > 0)
                {
                    //Arrange colspan
                    _currentRow.Cells[_currentRow.Cells.Count - 1].ColSpan = remainingCells + 1;
                }

                _tableFields.Rows.Add(_currentRow);
                _currentRow = null;
            }
        }

        protected virtual void fieldAdded(HtmlTableRow tr, object ctrl, string helpMessage)
        {
            if (OnControlAdded != null)
            {
                OnControlAdded(tr, ctrl, helpMessage);
            }
        }
        #endregion

    }
}
