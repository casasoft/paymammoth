﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;

namespace CS.General_v3.Controls.WebControls.Specialized.Contact.v2
{
    public enum DataType
    {
        Anchor,
        Email,
        Normal
    }

    public class ContactDetailsWithIcons : MyDiv
    {
        private MyTable _contactDetailsTable;
        private string ContactLink { get; set; }

        public ContactDetailsWithIcons(string contactLink)
        {
            ContactLink = contactLink;
            CssClass = "contact-details-iconized";
            initDetails();
        }

        private void initDetails()
        {
            initTable();
        }

        private void initTable()
        {
            _contactDetailsTable = new MyTable();
            initCells();
            attachTable();
        }

        private void attachTable()
        {
            Controls.Add(_contactDetailsTable);
        }

        private void initCells()
        {
            initTelephone();
            initMobile();
            initEmail();
            initAddress();
            initContactLink();
        }

        private void initContactLink()
        {
            RenderRow("contact-link-cell", ContactLink, DataType.Anchor);
        }

        private void initAddress()
        {
            RenderRow("address-cell", CS.General_v3.Util.Text.ConvertPlainTextToHTML(Settings.CompanyInfo.GetAddressAsOneLine("\n")), DataType.Normal);
        }

        private void initEmail()
        {
            RenderRow("email-cell", Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Email.GetSettingFromDatabase(), DataType.Email);
        }

        private void initMobile()
        {
            RenderRow("mobile-cell", Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Fax.GetSettingFromDatabase(), DataType.Normal);
        }

        private void initTelephone()
        {
            RenderRow("telephone-cell", Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Telephone.GetSettingFromDatabase(), DataType.Normal);
        }

        private void RenderRow(string iconCssClass, string information, DataType datType)
        {
            if (datType == DataType.Normal)
            {
                renderNormalRow(iconCssClass, information);
            }
            else if (datType == DataType.Email)
            {
                renderEmailRow(iconCssClass, information);
            }
            else
            {
                if (datType == DataType.Anchor)
                {
                    renderAnchorRow(iconCssClass, information);
                }
            }
        }

        private void renderAnchorRow(string iconCssClass, string information)
        {
            MyTableRow anchorRow = new MyTableRow();
            MyTableCell leftCell = new MyTableCell() { CssClass = "icon-cell" };
            MyTableCell rightCell = new MyTableCell();

            MyDiv div = new MyDiv();
            div.CssManager.AddClass(iconCssClass);

            leftCell.Controls.Add(div);

            MyAnchor anch = new MyAnchor();
            anch.CssManager.AddClass("contact-link");
            anch.Href = ContactLink;
            anch.InnerHtml = information;

            rightCell.Controls.Add(anch);

            anchorRow.Cells.Add(leftCell);
            anchorRow.Cells.Add(rightCell);
            _contactDetailsTable.Rows.Add(anchorRow);
        }

        private void renderEmailRow(string iconCssClass, string information)
        {

            MyTableRow emailRow = new MyTableRow();
            MyTableCell leftCell = new MyTableCell() { CssClass = "icon-cell" };
            MyTableCell rightCell = new MyTableCell();

            MyDiv div = new MyDiv();
            div.CssManager.AddClass(iconCssClass);

            leftCell.Controls.Add(div);

            MyAnchor anch = new MyAnchor();
            anch.Href = information;
            anch.InnerHtml = information;

            rightCell.Controls.Add(anch);

            emailRow.Cells.Add(leftCell);
            emailRow.Cells.Add(rightCell);
            _contactDetailsTable.Rows.Add(emailRow);
        }

        private void renderNormalRow(string iconCssClass, string information)
        {
            MyTableRow normalRow = new MyTableRow();
            MyTableCell leftCell = new MyTableCell() { CssClass = "icon-cell" };
            MyTableCell rightCell = new MyTableCell();

            MyDiv div = new MyDiv();
            div.CssManager.AddClass(iconCssClass);

            leftCell.Controls.Add(div);

            rightCell.InnerHtml = information;

            normalRow.Cells.Add(leftCell);
            normalRow.Cells.Add(rightCell);
            _contactDetailsTable.Rows.Add(normalRow);
        }
       
    }
}
