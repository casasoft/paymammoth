﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.JavaScript.Data;
namespace CS.General_v3.Controls.WebControls.Specialized.GoogleMaps
{
    using JavaScript.Data;

    public class GoogleMap : MyDiv
    {
        public List<GoogleMarker> Markers { get; private set; }

        public string VarName { get; set; }
        public string APIKey { get; set; }
        public bool AddApiLoaderScriptTag { get; set; }

        public double MapsVersion { get; set; }
        public string CenterAddress { get; set; }
        public double? CenterLat { get; set; }
        public double? CenterLng { get; set; }
        public int InitZoomLevel { get; set; }
        public bool Draggable { get; set; }
        public string CursorCssStyle { get; set; }
        public bool MouseWheelEnabled { get; set; }

        public GoogleMap(string varName)
            : base()
        {
            this.Markers = new List<GoogleMarker>();
            this.VarName = varName;
            AddApiLoaderScriptTag = true;
            this.CssManager.AddClass("google-map");
            this.MapsVersion = 2;
            this.Draggable = true;
            this.MouseWheelEnabled = true;
            InitZoomLevel = 14;
        }
        public GoogleMap()
            : this(null)
        {

        }
        private void initLoader()
        {
            if (AddApiLoaderScriptTag)
            {
                //<script type="text/javascript" src="http://www.google.com/jsapi?key=ABQIAAAA3LxoXqMwANIqAJsJn2IygRSJ-1K8U1z7hCeXnHSUgtrCnN_S_xQxhwDsJAlU9_bSQacXavnm3-C46A"></script>
                if (String.IsNullOrEmpty(APIKey))
                {
                    throw new Exception("Please specify API Key for google maps or else set the AddApiLoaderScriptTag to false");
                }
                string js = "<script type='text/javascript' src='http://www.google.com/jsapi?key=" + APIKey + "'></script>";
                this.Controls.Add(new Literal() { Text = js });
                //this.Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID + "-script", js);
            }
        }

        private string getMarkerJS(GoogleMarker m)
        {
            string js = "";
            JSObject markerParams = new JSObject();
            if (!String.IsNullOrEmpty(m.TextBoxStoreLocationId))
            {
                markerParams.AddProperty("txtStorePosition", m.TextBoxStoreLocationId, true);
            }
            
            markerParams.AddProperty("draggable", m.Draggable.ToString().ToLower(), false);
            markerParams.AddProperty("moveMarkerOnMapClick", m.MoveMarkerOnMapClick.ToString().ToLower(), false);
            
            if (!String.IsNullOrEmpty(m.AlternateText))
            {
                markerParams.AddProperty("title", m.AlternateText, true);
            }
            if (!String.IsNullOrEmpty(m.IconImageURL))
            {
                markerParams.AddProperty("icon", m.IconImageURL, true);
            }

            if ((m.ShowContentOnRollOver || m.ShowContentInitially || !m.DoNotToggleContentOnClick) && !String.IsNullOrEmpty(m.ContentHtml))
            {
                //Possibility to show HTML

                markerParams.AddProperty("maxWidth", m.ContentMaxWidth, false);
                markerParams.AddProperty("htmlContent", m.ContentHtml.Replace("'", "\""), true);

                markerParams.AddProperty("openByDefault", m.ShowContentInitially, false);
                markerParams.AddProperty("openOnMouseOver", m.ShowContentOnRollOver, false);
                markerParams.AddProperty("closeOnMouseOut", m.HideContentOnRollOut, false);
                markerParams.AddProperty("doNotToggleOnClick", m.DoNotToggleContentOnClick, false);
                
                /*htmlContent
 * - maxWidth
 * - openByDefault
 * - openOnMouseOver
 * - closeOnMouseOut
 * - doNotToggleOnClick*/

            }






            if (m.Lat != null && m.Lng != null)
            {
                js += this.VarName + ".addMarker(new GLatLng(" + m.Lat.Value + "," + m.Lng.Value + "), " + markerParams.GetJS() + ")";
            }
            else if (!String.IsNullOrEmpty(m.Address))
            {
                //Query by address
                js += this.VarName + ".addMarkerAtAddress('"+CS.General_v3.Util.Text.forJS(m.Address)+"', " + markerParams.GetJS() + ", new GLatLng(10,20))";
            }
            else
            {
                throw new Exception("Please specify either Lat/Lng or else Address");
            }


            if (!String.IsNullOrEmpty(m.ContentHtml))
            {
                //Contains content
                if (m.ShowContentOnRollOver || m.ShowContentInitially || !m.DoNotToggleContentOnClick)
                {
                    //Possibility to show HTML

                    //js = "new com.cs.gmap.CSMarkerWithContent("+js+", '"+m.ContentHtml.Replace("'", "\"")+"', "+m.ContentMaxWidth+", "+m.ShowContentInitially.ToString().ToLower()+", "+m.ShowContentOnRollOver.ToString().ToLower()+","+m.HideContentOnRollOut.ToString().ToLower()+","+m.DoNotToggleContentOnClick.ToString().ToLower()+")";
                    //com.cs.gmap.CSMarkerWithContent = function(marker, htmlContent, maxWidth, openByDefault, openOnMouseOver, closeOnMomuseOut, doNotToggleOnClick) {
                }
            }

            js += ";";
            return js;
        }
        private string getMarkersJS()
        {
            string js = "";
            for (int i = 0; i < Markers.Count; i++)
            {
                js += getMarkerJS(Markers[i]);
            }
            return js;
        }
        private string getCenterMapJS() {
            if ((CenterLat != null && CenterLng != null) || (CenterAddress != null))
            {
                if (CenterAddress != null)
                {
                    //Address
                    string js = "";
                    js += VarName + ".centerMapOnAddress('"+CenterAddress.Replace("'", "\'")+"', "+InitZoomLevel.ToString()+");";
                    return js;
                }
                else
                {
                    //Lat/Lng
                    string js = "";
                    js += VarName + ".centerMap(new GLatLng(" + CenterLat.Value.ToString() + ", " + CenterLng.Value.ToString() + "), " + InitZoomLevel.ToString() + ");";
                    return js;
                }
            }
            else {
                throw new Exception("Please specify either CenterLat, CenterLng or else CenterAddress");
            }

            


        }
        private void initJS()
        {
            if (String.IsNullOrEmpty(VarName))
            {
                throw new Exception("Please provide VarName");
            }

            JSObject mapParams = new JSObject();
            mapParams.AddProperty("cursor", CursorCssStyle, true, true);
            mapParams.AddProperty("mapsVersion", MapsVersion.ToString(), true, true);
            mapParams.AddProperty("draggable", Draggable.ToString().ToLower(), false, true);
            mapParams.AddProperty("mouseWheel", MouseWheelEnabled.ToString().ToLower(), false, true);



            string js = "com.cs.require('com.cs.gmap.CSMap');";
            js += "var "+VarName+" = new com.cs.gmap.CSMap('"+this.ClientID+"',"+mapParams.GetJS()+");";
            js += "dojo.connect(" + VarName + ", 'onLoad', function() {";

            js += getCenterMapJS();

            js += getMarkersJS();
            
            js += "});";
            js += VarName + ".init();";
            js = CS.General_v3.Util.JSUtilOld.MakeJavaScript(js, true);

            this.Controls.Add(new Literal() { Text = js });
            //this.Page.ClientScript.RegisterStartupScript(this.GetType(), this.ClientID + "-map", js);
        }

        protected override void OnLoad(EventArgs e)
        {
            
            initLoader();
            initJS();
            base.OnLoad(e);
        }
    }
}
