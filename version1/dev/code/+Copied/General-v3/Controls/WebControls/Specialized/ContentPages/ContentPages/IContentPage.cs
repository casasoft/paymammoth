﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_20101215.JavaScript.Data;
using CS.General_20101215.Controls.WebControls.Specialized.Hierarchy;
namespace CS.General_20101215.Controls.WebControls.Specialized.ContentPages.ContentPages
{
    public interface IContentPage : TreeStructureClasses.TreeStructure.ITreeItem, IHierarchy
    {
        
        

        new string Title { get; set; }
        string HtmlText { get; set; }
        IList<IContentPage> Children { get; }
        IList<IContentPage> GetChildPages();
        IContentPage CreateSubPage();
        IContentPage GetParentPage();
        new bool Visible { get; set; }
        /// <summary>
        /// Returns whether page is visible according to parents
        /// </summary>
        /// <returns></returns>
        bool IsPageVisibleAccordingToParent();
        /// <summary>
        /// Returns the page in the hierarchy whose parent is the page passed
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        IContentPage GetParentPageWhoseParentIs(IContentPage page);
        string Identifier { get; set; }
        string MetaKeywords { get; set; }
        string MetaDescription { get; set; }
        DateTime LastEditedDate { get; set; }
        string CustomLink { get; set; }
        /// <summary>
        /// Whether content page has got content i.e. clickable
        /// </summary>
        /// <returns></returns>
        bool ContainsContent();
        /// <summary>
        /// Whether includes children pages
        /// </summary>
        /// <returns></returns>
        bool HasChildPages();
        /// <summary>
        /// Returns the url to which this content pages links
        /// </summary>
        /// <returns></returns>
        string GetUrl();

        JSObjectFlash GetJsObjectFlash();
        JSObjectFlash GetJsObjectFlash(int maxDepth);
        JSObjectFlash GetJsObjectFlash(int maxDepth, int currDepth);
        JSObject GetJsObject();
        JSObject GetJsObject(int maxDepth);
        JSObject GetJsObject(int maxDepth, int currDepth);
        JSObject GetJsObjectWithHtml(bool forFlash = false, int maxDepth = 0, int currDepth = 0);
        JSObject GetJsObjectFlashWithHtml(int maxDepth = 0, int currDepth = 0);

    }
        
}
