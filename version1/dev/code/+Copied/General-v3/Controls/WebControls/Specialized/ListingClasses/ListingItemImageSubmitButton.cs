﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.WebControls.Specialized.ListingClasses
{
    public class ListingItemImageSubmitButton : ListingItemImageButton
    {
        public CS.General_v3.Controls.WebControls.Specialized.Listing.ItemHandler SubmitHandler { get; set; }
        public ListingItemImageSubmitButton(CS.General_v3.Controls.WebControls.Specialized.Listing.ItemHandler SubmitHandler)
        {
            this.SubmitHandler = SubmitHandler;
        }
    }

}
