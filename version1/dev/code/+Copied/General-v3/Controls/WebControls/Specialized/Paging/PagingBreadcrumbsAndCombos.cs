﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Classes.SearchParams;
namespace CS.General_v3.Controls.WebControls.Specialized.Paging
{
    public class PagingBreadcrumbsAndCombos : MyDiv
    {
        public class FUNCTIONALITY
        {
            private PagingBreadcrumbsAndCombos _ui;
            public BreadcrumbsAndCombos BreadcrumbsAndCombos { get; private set; }
            public PagingBar PagingBar { get; private set; }

            public FUNCTIONALITY(PagingBreadcrumbsAndCombos ui)
            {
                _ui = ui;
                BreadcrumbsAndCombos = new BreadcrumbsAndCombos();
                PagingBar = new PagingBar();
            }
            private void initControls()
            {
                
                if ((BreadcrumbsAndCombos.Functionality.BreadCrumbs.Functionality.MenuItems != null &&
                    BreadcrumbsAndCombos.Functionality.BreadCrumbs.Functionality.MenuItems.Count > 0) ||
                    BreadcrumbsAndCombos.Functionality.CombosHasValues)
                {
                    _ui.Controls.Add(BreadcrumbsAndCombos);

                }
                _ui.Controls.Add(PagingBar);
            }
            public void Init()
            {
                initControls();
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }

        public PagingBreadcrumbsAndCombos()
        {
            Functionality = new FUNCTIONALITY(this);
            this.CssManager.AddClass("paging-breadcrumbs-combos");
        }
        public PagingBreadcrumbsAndCombos(IParamsInfo searchParams)
            : this()
        {
            Functionality.PagingBar.SearchParamsPagingInfo = searchParams.PagingInfo;
            Functionality.BreadcrumbsAndCombos.Functionality.SearchParams = searchParams;
        }
        protected override void OnLoad(EventArgs e)
        {
            Functionality.Init();
            
            base.OnLoad(e);
        }
    }
}
