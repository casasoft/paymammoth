﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using System.Web.UI.WebControls;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data
{
    public abstract class FormFieldListBaseTypeData<T> : FormFieldBaseTypeData<T>
    {
        public List<ListItem> ListItems { get; set; }
        public ListItemCollection ListItemCollection
        {
            set {
                this.ListItems = value.Cast<ListItem>().ToList();
            }
        }





            public FormFieldListBaseTypeData()
        {
            this.ListItems = new List<ListItem>();

        }

        

    }
}
