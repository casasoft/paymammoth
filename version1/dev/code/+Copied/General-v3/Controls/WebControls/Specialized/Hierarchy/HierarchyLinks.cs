﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
namespace CS.General_v3.Controls.WebControls.Specialized.Hierarchy
{
    /// <summary>
    /// Links in the format LINK1 | LINK 2 | Link 3 | etc
    /// </summary>
    public class HierarchyLinks : MyDiv
    {
        public class PropertiesClass {
            public List<IHierarchy> Items { get; set; }
            public string SeparatorText { get; set; }
            public PropertiesClass()
            {
                Items = new List<IHierarchy>();
                SeparatorText = " | ";
            }
        }
        public PropertiesClass Properties { get; set; }
        public HierarchyLinks()
        {
            Properties = new PropertiesClass();
            this.CssManager.AddClass("hierarchy-links");
        }
        private void createControls()
        {
            for (int i = 0; i < Properties.Items.Count; i++)
            {
                if (i > 0)
                {
                    Controls.Add(new Literal() { Text = "<span class='separator'>"+Properties.SeparatorText+"</span>" });
                }
                HtmlAnchor aLink = new HtmlAnchor();
                aLink.InnerHtml = Properties.Items[i].Title;
                aLink.HRef = Properties.Items[i].Href;
                aLink.Target = CS.General_v3.Util.EnumUtils.StringValueOf(Properties.Items[i].HrefTarget);
                if (Properties.Items[i].Selected)
                {
                    aLink.Attributes["class"] = "selected";
                }
                Controls.Add(aLink);
            }
        }
        
        private void initControls()
        {
            createControls();
        }

        protected override void OnLoad(EventArgs e)
        {
            initControls();
            base.OnLoad(e);
        }
    }
}
