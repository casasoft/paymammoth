﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using CS.General_20101215.Controls.WebControls.Common;
using CS.General_20101215.Controls.Classes.MediaGallery;
using CS.General_20101215;
namespace CS.General_20101215.Controls.WebControls.Specialized.ContentPages
{
    [ToolboxData("<{0}:PageFolderForm runat=server></{0}:PageFolderForm>")]    
    public class PageFolderForm : MenuTreeStructure.MenuFolderForm
    {

        /* HOW TO USE
        * ----------
        * 
        * Parameters to fill
        * 
        *  - FolderTitle
        *  - ListingPageUrl
        *  
        * Events to Attach
        *  
        *  - LoadFolder
        *  
        *  
        * Optional
        * 
        * - QueryStringFolderParam
        * - Has Description 
        */
        public PageFolderForm()
        {
            base.LoadFolder += new CS.General_20101215.Controls.WebControls.Specialized.MenuTreeStructure.MenuFolderForm.CreateLoadFolderEvent(LinkSectionForm_LoadFolder);
            //this.HasDescription = true;   
        }

        public new IPageFolder Folder
        {
            get
            {
                return (IPageFolder)base.Folder;
            }
        }

        CS.General_20101215.Controls.WebControls.Specialized.MenuTreeStructure.IMenuFolder LinkSectionForm_LoadFolder(int ID)
        {
            return LoadFolder(ID);
        }
        #region Constants / Statics
        

        #endregion
        #region Delegates
        public delegate IPageFolder CreateLoadFolderEvent(int ID);
        public event CreateLoadFolderEvent LoadFolder;
        
        #endregion
        #region Events
        
        #endregion
        #region Methods
        
        #endregion
        #region Properties
        
        #endregion
        

        protected override void OnInit(EventArgs e)
        {

            
            base.OnInit(e);
        }
        
      public override void  addExtraFormFields()
        {
             base.addExtraFormFields();
        }
       
        public override void  saveExtraFields()
        {
            
            base.saveExtraFields();
        }

        
        protected override void OnLoad(EventArgs e)
        {
            
            base.OnLoad(e);
        }

        


 
    }
}
