﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.WebControls.Specialized.Hierarchy
{
    /// <summary>
    /// This class checks a hierarchy for any parents which are 'illegal', and removes them
    /// </summary>
    public class HierarchyParentChecker
    {
        private IEnumerable<IHierarchy> _listToCheckIn;

        public void CheckIfParentAlreadyExists(IHierarchy h, IEnumerable<IHierarchy> listToCheckIn)
        {
            _listToCheckIn = listToCheckIn;
            
        }

    }
}
