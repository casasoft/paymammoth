﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using CS.General_20101215.Controls.WebControls.Common;
using CS.General_20101215.Controls.Classes.MediaGallery;
using CS.General_20101215;
namespace CS.General_20101215.Controls.WebControls.Specialized.ContentPages
{
    [ToolboxData("<{0}:PagesListing runat=server></{0}:PagesListing>")]    
    public class PagesListing : MenuTreeStructure.MenuTreeStructure
    {
        /* HOW TO USE
         * ----------
         * 
         * Parameters to fill
         * 
         *  - RootFolders
         *  - ImageFolder
         *  - EditFolderPageName
         *  - EditItemPageName
         *  - CSSFile
         *  
         * Optional
         *  
         *  - QuerystringFolderIDParam
         *  - QuerystringParentFolderIDParam
         *  - QuerystringItemIDParam
         */

        public PagesListing()
        {
            base.Functionality.ItemTitle = "Page";
            base.Functionality.FolderTitle = "Page Folder";

        }
        #region Constants / Statics
        

        #endregion
        #region Delegates
        
        #endregion
        #region Events
        

        #endregion
        #region Methods
        

        #endregion
        #region Properties
        

        #endregion
        

        protected override void OnInit(EventArgs e)
        {

            
            base.OnInit(e);
        }



        protected override void OnLoad(EventArgs e)
        {

            
            base.OnLoad(e);
        }

        


 
    }
}
