﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common;

using CS.General_v3;
using CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructureBasic;

namespace CS.General_v3.Controls.WebControls.Specialized.FileBrowser
{
    public class FileBrowserUI : MyDiv
    {
        public class FUNCTIONALITY
        {
            #region Events
            public delegate void OnDeletedItemsDelegate(FileBrowserUI sender, List<ContentPanelItem> itemsToDelete);
            public delegate void OnCreateNewFolderDelegate(FileBrowserUI sender, string folderName);
            public event OnDeletedItemsDelegate OnDeletedItems;
            public event UploadPanel.FUNCTIONALITY.UploadEventDelegate OnFileUpload;
            public event OnCreateNewFolderDelegate OnCreateNewFolder;
            #endregion
            #region Properties/Fields
            private MyTable tbMain = null;
            public bool AllowNewFolders { get; set; }
            public FolderPanel FolderPanel { get; private set; }
            public ContentPanel ContentPanel { get; private set; }
            public UploadPanel UploadPanel { get; private set; }
            public NewFolderPanel NewFolderPanel {get; private set;}
            public Navigation.NavigationBreadcrumbs NavigationBreadcrumb {get; private set;}
            public bool ShowCheckboxes { get; set; }
            public bool ShowDeleteButtonForCheckboxes { get; set; }
            public List<ContentPanelItem> ContentPanelItems { get; set; }
            private FileBrowserUI _fileBrowser = null;
            public List<ITreeItemBasic> RootFolders { get; set; }
            public List<string> FileTypesAllowed { get; set; }
            
            #endregion
            public void AddNavigationBreadCrumbItem(string title, string value)
            {
                this.NavigationBreadcrumb.Functionality.MenuItems.Add(new ListItem(title, value));
            }
            public FUNCTIONALITY(FileBrowserUI fileBrowser)
            {
                _fileBrowser = fileBrowser;
                this.FileTypesAllowed = new List<string>();
                this.AllowNewFolders = true;
                this.ContentPanelItems = new List<ContentPanelItem>();
                this.FolderPanel = new FolderPanel() { ID=_fileBrowser.ID + "_folderPanel"};
                this.ContentPanel = new ContentPanel() { ID=_fileBrowser.ID + "_contentPanel"};
                this.NewFolderPanel = new NewFolderPanel(_fileBrowser.ID + "_newFolderPanel");
                this.UploadPanel = new UploadPanel(_fileBrowser.ID + "_uploadPanel");
                this.NavigationBreadcrumb = new CS.General_v3.Controls.WebControls.Specialized.Navigation.NavigationBreadcrumbs();
                this.ContentPanel.Functionality.OnDeletedItems += new ContentPanel.FUNCTIONALITY.DeletedItemsDelegate(ContentPanel_OnDeletedItems);
                this.NewFolderPanel.Functionality.OnNewFolder += new NewFolderPanel.FUNCTIONALITY.NewFolderDelegate(NewFolderPanel_OnNewFolder);
            }

            void NewFolderPanel_OnNewFolder(NewFolderPanel sender, string folderName)
            {
                if (this.OnCreateNewFolder != null)
                    OnCreateNewFolder(_fileBrowser, folderName);
                
            }

            private  void ContentPanel_OnDeletedItems(ContentPanel sender, List<ContentPanelItem> items)
            {
                if (this.OnDeletedItems != null)
                    this.OnDeletedItems(_fileBrowser, items);
            }
            
            private void renderTable()
            {
                tbMain = new MyTable();
                _fileBrowser.Controls.Add(tbMain);
                var tr = tbMain.AddRow();
                tr.Cells.Add(FolderPanel);
                
                MyDiv divContent = new MyDiv();
                divContent.Controls.Add(NavigationBreadcrumb);
                divContent.Controls.Add(ContentPanel);
                divContent.Controls.Add(UploadPanel);
                divContent.Controls.Add(NewFolderPanel);
                tr.AddCell("divContent",divContent);

                
                
            }
            public void Render()
            {
                _fileBrowser.CssManager.AddClass("filebrowserUI");
                this.ContentPanel.Functionality.ShowCheckboxes = this.ShowCheckboxes;
                this.ContentPanel.Functionality.ShowDeleteButton = this.ShowDeleteButtonForCheckboxes;
                this.ContentPanel.Functionality.ContentPanelItems = this.ContentPanelItems;
                this.UploadPanel.Functionality.FileTypesAllowed = this.FileTypesAllowed;
                renderTable();
                
                UploadPanel.Functionality.OnFileUpload += new UploadPanel.FUNCTIONALITY.UploadEventDelegate(Functionality_OnFileUpload);
                
            }

            private void Functionality_OnFileUpload(UploadPanel sender, string filename, System.IO.Stream fileContent,
                int? widthPx, int? heightPx, bool fillBoxByCropping)
            {
                if (OnFileUpload != null)
                    OnFileUpload(sender, filename, fileContent,widthPx,heightPx,fillBoxByCropping);
            }
        }
        public FUNCTIONALITY Functionality { get; set; }
        public FileBrowserUI()
        {
            this.Functionality = new FUNCTIONALITY(this);
            

        }
        protected override void OnLoad(EventArgs e)
        {
            this.Functionality.Render();
            base.OnLoad(e);
        }

    }
}
