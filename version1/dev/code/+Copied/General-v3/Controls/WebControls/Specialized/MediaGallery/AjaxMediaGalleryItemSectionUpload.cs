﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using System.Web.UI.HtmlControls;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;

namespace CS.General_v3.Controls.WebControls.Specialized.MediaGallery
{
    public class AjaxMediaGalleryItemSectionUpload : MyDiv
    {
        public class FUNCTIONALITY
        {
            public delegate void OnUploadSubmitHander(List<MyFileUpload> fileUploads);
            public event OnUploadSubmitHander OnUploadSubmit;

            private AjaxMediaGalleryItemSectionUpload _ui;

            private FormFields _formUploads;

            private List<MyFileUpload> _fileUploads = new List<MyFileUpload>();

            public int MaxUpload { get; set; }
            public string MaxUploadText { get; set; }
            public string CantUploadMoreText { get; set; }
            public string Title { get; set; }
            public string Text { get; set; }
            public string SubmitButtonText { get; set; }

            public FUNCTIONALITY(AjaxMediaGalleryItemSectionUpload ui)
            {
                _ui = ui;
                MaxUpload = 5;
            }
            private void initText()
            {
                if (!string.IsNullOrEmpty(Title))
                {
                    MySpan spanTitle = new MySpan();
                    spanTitle.CssManager.AddClass("cs-media-gallery-upload-title");
                    spanTitle.InnerHtml = Title;
                    _ui.Controls.Add(spanTitle);
                }
                if (MaxUpload > 0)
                {
                    if (!string.IsNullOrEmpty(MaxUploadText))
                    {
                        MyParagraph pText = new MyParagraph();
                        pText.CssManager.AddClass("cs-media-gallery-upload-max-upload");
                        pText.InnerHtml = MaxUploadText;
                        _ui.Controls.Add(pText);
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(CantUploadMoreText))
                    {
                        MyParagraph pText = new MyParagraph();
                        pText.CssManager.AddClass("cs-media-gallery-upload-max-upload");
                        pText.InnerHtml = CantUploadMoreText;
                        _ui.Controls.Add(pText);
                    }
                }
                if (!string.IsNullOrEmpty(Text))
                {
                    MyParagraph pText = new MyParagraph();
                    pText.CssManager.AddClass("cs-media-gallery-upload-text");
                    pText.InnerHtml = Title;
                    _ui.Controls.Add(pText);
                }
            }
            private void createUploads()
            {
                if (MaxUpload > 0)
                {
                    string id = _ui.ClientID + "formUploads";
                    _formUploads = new FormFields(id);
                    for (int i = 0; i < MaxUpload; i++)
                    {
                        var fileUpload = _formUploads.AddFileUpload(id + i, "Item " + i, i == 0, null, null);
                        _fileUploads.Add(fileUpload);
                    }
                    _formUploads.ButtonSubmitText = SubmitButtonText;
                    _formUploads.ClickSubmit += new EventHandler(_formUploads_ClickSubmit);
                    _ui.Controls.Add(_formUploads);
                }
               
            }

    
            void _formUploads_ClickSubmit(object sender, EventArgs e)
            {
                List<MyFileUpload> fileUploads = new List<MyFileUpload>();
                for (int i = 0; i < _fileUploads.Count; i++)
                {
                    if (_fileUploads[i].HasUploadedFile)
                    {
                        fileUploads.Add(_fileUploads[i]);
                    }
                }
                if (fileUploads.Count > 0)
                {
                    if (OnUploadSubmit != null)
                    {
                        OnUploadSubmit(fileUploads);
                    }
                }
            }
            public void Init()
            {
                initText();
                createUploads();
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public AjaxMediaGalleryItemSectionUpload()
        {
            Functionality = new FUNCTIONALITY(this);
            this.CssManager.AddClass("cs-media-gallery-upload");
        }
        protected override void OnLoad(EventArgs e)
        {
            Functionality.Init();
            base.OnLoad(e);
        }
    }
}
