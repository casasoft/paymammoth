﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using CS.General_20101215.Controls.WebControls.Common;
using CS.General_20101215.Controls.Classes.MediaGallery;
using CS.General_20101215.Util;
namespace CS.General_20101215.Controls.WebControls.Specialized.ContentPages
{
    [ToolboxData("<{0}:PageForm runat=server></{0}:PageForm>")]    
    public class PageForm : MenuTreeStructure.MenuItemForm
    {
        public enum HTML_EDITOR_TYPE
        {
            FCKEditor,
            CKEditor
        }
        /* HOW TO USE
        * ----------
        * 
        * Parameters to fill
        * 
        *  - ItemTitle
        *  - ListingPageUrl
        *  
        * Events to Attach
        *  
        *  - LoadFolder
        *  
        *  
        * Optional
        * 
        * - QueryStringItemParam
        * - QueryStringParentFolderParam
        */
        public PageForm()
        {
            this.ItemTitle = "Page";
            base.LoadFolder += new CS.General_20101215.Controls.WebControls.Specialized.MenuTreeStructure.MenuItemForm.CreateLoadFolderEvent(LinkItemForm_LoadFolder);
            this.HTMLEditorType = HTML_EDITOR_TYPE.FCKEditor;
        }
        public HTML_EDITOR_TYPE HTMLEditorType { get; set; }
        public string EditorAreaCSS { get; set; }
        public string EditorBodyClass { get; set; }
        CS.General_20101215.Controls.WebControls.Specialized.MenuTreeStructure.IMenuFolder LinkItemForm_LoadFolder(int ID)
        {
            return LoadFolder(ID);
        }
        #region Constants / Statics
        

        #endregion
        #region Delegates
        public delegate IPageFolder CreateLoadFolderEvent(int ID);
        #endregion
        #region Events
        public new event CreateLoadFolderEvent LoadFolder;
        #endregion
        #region Methods


        public IPageFolder GetParentFolder()
        {
            return LoadFolder(GetParentFolderIDFromQueryString());
        }
        public IPage LoadItemFromParent()
        {
            IPageFolder parent = GetParentFolder();

            IPage item = null;
            int itemID = GetItemIDFromQueryString();
            item = (IPage)parent.GetSubItems().FindElemObj(obj => ((IPage)obj).ID == itemID);

            return item;


        }
        #endregion
        #region Properties
        public IPage PageItem
        {
            get
            {
                return (IPage)base.MenuItem;
            }


        }
        public new IPage MenuItem
        {
            get { return this.PageItem; }
        }

        #endregion
        

        protected override void OnInit(EventArgs e)
        {

            
            base.OnInit(e);
        }
        public CS.General_20101215.Controls.WebControls.Common.FCKEditor.FCKeditor GetFCKEditorForHTMLEditor()
        {
            return (CS.General_20101215.Controls.WebControls.Common.FCKEditor.FCKeditor)this.GetControl("txtText");
        }
        public CS.General_20101215.Controls.WebControls.Common.CKEditor GetCKEditorForHTMLEditor()
        {
            return (CS.General_20101215.Controls.WebControls.Common.CKEditor)this.GetControl("txtText");
        }
        public override void addExtraFormFields()
        {
            IPage pg = this.MenuItem;
            if (pg.IsHTML)
            {
                if (this.HTMLEditorType == HTML_EDITOR_TYPE.CKEditor) 
                {
                    var ck = this.AddCKEditor("txtText", "Text", true, null, 800, 600, pg != null ? pg.Text : "");
                }
            }
            else
                this.AddStringMultiline("txtText", "Text", true, null, 20, 600, pg != null ? pg.Text : ""); 
            
            base.addExtraFormFields();
        }

        public override void saveExtraFields()
        {
            IPage pg = (IPage)this.MenuItem;
            pg.Text = this.GetFormValueStr("txtText");

            base.saveExtraFields();
        }



        protected override void OnLoad(EventArgs e)
        {
            
            base.OnLoad(e);
        }

        


 
    }
}
