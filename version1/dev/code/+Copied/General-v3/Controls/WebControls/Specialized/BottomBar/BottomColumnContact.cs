﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Specialized.Contact.v2;
using System.Web.UI.HtmlControls;
using CS.General_v3.Controls.WebControls.Common;

namespace CS.General_v3.Controls.WebControls.Specialized.BottomBar
{
    public class BottomColumnContact : BottomColumn
    {

        public new class FUNCTIONALITY : BottomColumn.FUNCTIONALITY
        {

            private MyDiv WrapperDIV;
            public ContactDetails ContactDetails { get; set; }

            public FUNCTIONALITY(BottomColumnContact col)
                : base(col)
            {
                ContactDetails = new ContactDetails();
            }

            private void initHeading()
            {
                HtmlGenericControl titleSpan = new HtmlGenericControl("span");
                titleSpan.Attributes["class"] = "title";
                titleSpan.InnerText = Title;
                WrapperDIV.Controls.Add(titleSpan);
            }
            
            private void generateWrapper()
            {
                WrapperDIV = new MyDiv();
                WrapperDIV.Attributes["class"] = "contact-details-column-wrapper";
                PlaceHolder.Controls.Add(WrapperDIV);
            }

            private void generateContactColoumn()
            {
                WrapperDIV.Controls.Add(ContactDetails);
            }

            internal override void Init()
            {
                generateWrapper();
                initHeading();
                generateContactColoumn();
                base.Init();
            }
           
        }
        public new FUNCTIONALITY Functionality
        {
            get { return (FUNCTIONALITY)base.Functionality; }
            set
            {
                base.Functionality = value;
            }
        }
        public BottomColumnContact()
        {
            Functionality = new FUNCTIONALITY(this);
        }

    }
}
