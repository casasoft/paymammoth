﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common;

using CS.General_v3.Controls.WebControls.Specialized;
using CS.General_v3;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;
namespace CS.General_v3.Controls.WebControls.Specialized.FileBrowser
{
    public class UploadPanel : MyDiv
    {
        public UploadPanel(string id)
        {
            this.ID = id;
            this.Functionality = new FUNCTIONALITY(this);
        }
        public class FUNCTIONALITY
        {
            public FUNCTIONALITY(UploadPanel UploadPanel)
            {
                _UploadPanel = UploadPanel;
                this.ShowImageWidthHeight = true;
                this.FileTypesAllowed = new List<string>();
            }

            public bool ShowImageWidthHeight { get; set; }
            public delegate void UploadEventDelegate(UploadPanel sender,
                string filename, System.IO.Stream fileContent, int? widthPx, int? heightPx, bool fillBoxByCropping);
            public event UploadEventDelegate OnFileUpload;

            private UploadPanel _UploadPanel = null;
            private FormFields _form = null;
            public List<string> FileTypesAllowed { get; set; }
            private void renderHeading()
            {
                MyHeading hTitle = new MyHeading(1, "Upload");
                hTitle.CssManager.AddClass("uploadPanel-heading");
                _UploadPanel.Controls.Add(hTitle);
            }
            private void renderForm()
            {
                _form = new FormFields(_UploadPanel.ID + "_formUpload");
                var txtUpload = _form.AddFileUpload("txtUpload", "Upload", true, "Select file to upload", 200);
                //_form.ValidationGroup = "fileUploadFiles";
                
                txtUpload.FileExtensionsAllowed = this.FileTypesAllowed;

                if (ShowImageWidthHeight)
                {
                    _form.AddInteger("txtWidth", "Width (px)", false, "Width in pixels (Applies only to image files)",60,null);
                    _form.AddInteger("txtHeight", "Height (px)", false, "Height in pixels (Applies only to image files)", 60, null);
                    _form.AddBool("chkFillBoxByCropping", "Fill box:", false,false, "Whether image should fill exactly the specified box, by cropping the middle part");

                }
                _form.ButtonSubmitText = "Upload";
                _form.ClickSubmit += new EventHandler(_form_ClickSubmit);
                _UploadPanel.Controls.Add(_form);
            }

            void _form_ClickSubmit(object sender, EventArgs e)
            {
                MyFileUpload txt = _form.GetControlAsMyFileUpload("txtUpload");
                
                if (txt.FileContent.Length > 0)
                {
                    int? width = null;
                    int? height = null;
                    bool fillBox = false;
                    if (ShowImageWidthHeight)
                    {
                        width = Convert.ToInt32(_form.GetFormValue("txtWidth"));
                        height = Convert.ToInt32(_form.GetFormValue("txtHeight"));
                        fillBox = width != null && height != null && _form.GetFormValueBool("chkFillBoxByCropping");
                    }

                    if (OnFileUpload != null)
                        OnFileUpload(_UploadPanel, txt.FileName, txt.FileContent,width,height,fillBox);

                }
            }
            public void Render()
            {
                renderHeading();
                renderForm();
            }

        }
        public FUNCTIONALITY Functionality { get; set; }
        
        protected override void OnLoad(EventArgs e)
        {
            this.Functionality.Render();
            base.OnLoad(e);
        }
    }
}
