﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace CS.General_20101215.Controls.WebControls.Specialized.MediaGallery.v2
{
    public abstract class AjaxMediaGalleryDeleteBaseHandler : CS.General_20101215.HTTPHandlers.BaseAjaxHandler
    {
        /// <summary>
        /// The parameter where to load the list of IDs in order, IDs are seperated by a comma
        /// </summary>
        protected string _paramID = "id";
        protected string _paramResponseItemId = "id";
        protected string _paramResponseSuccess = "success";

        public override void ProcessRequest(System.Web.HttpContext context)
        {
            string id = context.Request.QueryString[_paramID];


            bool ok = DeleteItem(context, id);
            this.AddProperty(_paramResponseSuccess, ok);
            this.AddProperty(_paramResponseItemId, id);
            

            base.ProcessRequest(context);
        }

        public abstract bool DeleteItem(HttpContext context, string itemID);
    }
}
