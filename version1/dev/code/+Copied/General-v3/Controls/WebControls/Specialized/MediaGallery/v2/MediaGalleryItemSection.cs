﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_20090518.Controls.WebControls.Common;

namespace CS.General_20090518.Controls.WebControls.Specialized.MediaGallery.v2
{
    public class MediaGalleryItemSection : MyDiv
    {
        public class FUNCTIONALITY
        {
            public delegate void UploadSectionItemsHandler(List<MyFileUpload> fileUploads, MediaGalleryItemSection section);
            public event UploadSectionItemsHandler OnUploadItems;
            private MediaGalleryItemSection _ui;

            public MediaGalleryItemSectionData Data { get; set; }

            public FUNCTIONALITY(MediaGalleryItemSection ui)
            {
                _ui = ui;
            }
            private void createItems()
            {
                MyDiv divTitle = new MyDiv();
                divTitle.CssManager.AddClass("cs-media-gallery-section-title");
                divTitle.InnerHtml = Data.Title;
                _ui.Controls.Add(divTitle);

                MediaGalleryItems galleryItems = new MediaGalleryItems();
                galleryItems.Functionality.Items.AddRange(Data.Items);
                _ui.Controls.Add(galleryItems);

                if (Data.CanUpload) {
                    MediaGalleryItemSectionUpload uploadSection = new MediaGalleryItemSectionUpload();
                    _ui.Controls.Add(uploadSection);
                    uploadSection.Functionality.OnUploadSubmit += new MediaGalleryItemSectionUpload.FUNCTIONALITY.OnUploadSubmitHander(Functionality_OnUploadSubmit);
                }
            }

            void Functionality_OnUploadSubmit(List<MyFileUpload> fileUploads)
            {
                if (OnUploadItems != null)
                {
                    OnUploadItems(fileUploads, _ui);
                }
            }
            public void Init()
            {
                createItems();    
            }
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public MediaGalleryItemSection()
        {
            Functionality = new FUNCTIONALITY(this);
            this.CssManager.AddClass("cs-media-gallery-section");
        }
        protected override void OnLoad(EventArgs e)
        {
            Functionality.Init();
            base.OnLoad(e);
        }
    }
}
