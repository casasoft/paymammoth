﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data
{
    public class FormFieldStringHtmlData : FormFieldStringMultilineData
    {

        public FormFieldStringHtmlData()
        {
            this.RowCssClassBase = "form-row-html";
            this.Rows = 20;
        }
        public new CKEditor GetField()
        {
            return (CKEditor)base.GetField();
        }
        
        protected override Common.General.IMyFormWebControl createFieldControl()
        {
            CKEditor ck =new CKEditor();
            ck.Rows = this.Rows;
            
            return ck;

            
        }
    }
}
