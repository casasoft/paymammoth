﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS.General_v3.Controls.WebControls.Specialized.HtmlReports
{
    public class HtmlReportHeader : MyDiv

    {
        public class FUNCTIONALITY
        {
            public string LogoAlternateText { get; set; }
            public string LogoImageUrl { get; set; }

            public string Address { get; set; }
            public string Tel { get; set; }
            public string Email { get; set; }
            public string Fax { get; set; }

            public string TelLabel { get; set; }
            public string EmailLabel { get; set; }
            public string FaxLabel { get; set; }

            public string LabelSuffix { get; set; }

            public string ReportTitle { get; set; }


            private HtmlReportHeader _control;
            private MyDiv _divTopDetails;
            public FUNCTIONALITY(HtmlReportHeader control)
            {
                _control = control;

                TelLabel = "Tel";
                EmailLabel = "Email";
                FaxLabel = "Fax";
                LabelSuffix = ": ";

            }
            /// <summary>
            /// Load company details from settings file
            /// </summary>
            public void CopyDetailsFromSettingsFile()
            {
                Tel = Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Telephone.GetSettingFromDatabase();
                Address = Settings.CompanyInfo.GetAddressAsOneLine("<br />");
                Fax = Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Fax.GetSettingFromDatabase();
                Email = Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Email.GetSettingFromDatabase();
                LogoAlternateText = Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_CompanyName + " Logo";
                
            }
            private void initMainDiv()
            {
                _control.CssManager.AddClass("html-report-header");
                _divTopDetails = new MyDiv();
                _divTopDetails.CssManager.AddClass("html-report-header-top");
                _control.Controls.Add(_divTopDetails);
            }
            private void initLogo()
            {
                if (!string.IsNullOrEmpty(LogoImageUrl))
                {
                    MyDiv logoContainer = new MyDiv();
                    logoContainer.CssManager.AddClass("html-report-header-logo");
                    _divTopDetails.Controls.Add(logoContainer);
                    MyImage imgLogo = new MyImage();
                    imgLogo.AlternateText = LogoAlternateText;
                    imgLogo.ImageUrl = LogoImageUrl;
                    logoContainer.Controls.Add(imgLogo);

                }


            }
            private void initRightDetails()
            {
                MyDiv divRightDetails = new MyDiv();
                divRightDetails.CssManager.AddClass("html-report-header-right-container");
                _divTopDetails.Controls.Add(divRightDetails);

                addRightDetailsDiv(divRightDetails, getRightDetailsDiv("address", null, Address));
                addRightDetailsDiv(divRightDetails, getRightDetailsDiv("tel", TelLabel, Tel));
                addRightDetailsDiv(divRightDetails, getRightDetailsDiv("fax", FaxLabel, Fax));
                if (!string.IsNullOrEmpty(Email)) {
                    addRightDetailsDiv(divRightDetails, getRightDetailsDiv("email", EmailLabel, new MyEmail() { Email = Email }));
                }
                

            }
            private void initHeader()
            {
                if (!string.IsNullOrEmpty(ReportTitle))
                {
                    MyHeading title = new MyHeading(1, ReportTitle);
                    _control.Controls.Add(title);
                }
            }
            /// <summary>
            /// Adds the right details to the header.  If divDetails is null,
            /// it is not added
            /// </summary>
            /// <param name="divRightDetails"></param>
            /// <param name="divDetail"></param>
            private void addRightDetailsDiv(MyDiv divRightDetails, MyDiv divDetail)
            {
                if (divDetail != null)
                {
                    divRightDetails.Controls.Add(divDetail);
                }
                
            }
           
            private MyDiv getRightDetailsDiv(string cssClass, string label, string value)
            {
                if (!string.IsNullOrEmpty(value))
                {
                    return getRightDetailsDiv(cssClass, label, new Literal() { Text = value });
                }
                else
                {
                    return null;
                }
            }
            /// <summary>
            /// Retrieve the right details of the header
            /// </summary>
            /// <param name="cssClass"></param>
            /// <param name="label"></param>
            /// <param name="value"></param>
            /// <returns></returns>
            private MyDiv getRightDetailsDiv(string cssClass, string label, Control value)
            {
                MyDiv div = new MyDiv();
                div.CssManager.AddClass(cssClass);
                if (!string.IsNullOrEmpty(label))
                {
                    MySpan spanLabel = new MySpan();
                    spanLabel.CssManager.AddClass("html-report-header-right-label");
                    spanLabel.InnerHtml = label + LabelSuffix;
                    div.Controls.Add(spanLabel);
                }
                div.Controls.Add(value);
                return div;
            }
            private void initClear()
            {
                MyDiv divClear = new MyDiv();
                divClear.CssManager.AddClass("clear");
                _divTopDetails.Controls.Add(divClear);
            }
            private void initControls()
            {
                initMainDiv();
                initLogo();
                initRightDetails();
                initClear();
                initHeader();
            }
            public void OnLoad()
            {
                initControls();
            }

        }
        public FUNCTIONALITY Functionality { get; private set; }
        public HtmlReportHeader(bool copyDetailsFromSettings = true)
        {
            Functionality = new FUNCTIONALITY(this);
            if (copyDetailsFromSettings)
            {
                Functionality.CopyDetailsFromSettingsFile();
            }
        }
        protected override void OnLoad(EventArgs e)
        {
            Functionality.OnLoad();
            base.OnLoad(e);
        }




    }
}
