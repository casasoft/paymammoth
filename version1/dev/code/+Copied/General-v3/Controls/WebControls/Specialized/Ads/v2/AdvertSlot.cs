﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_20101215.Controls.WebControls.Common;
namespace CS.General_20101215.Controls.WebControls.Specialized.Ads.v2
{
    /// <summary>
    /// Creates a new advert slot which the clickable part is defined by an invisible transparent mask.  So that SWF files will be overridden with this link.
    /// </summary>
    public class AdvertSlot : MyDiv
    {
        public class FUNCTIONALITY
        {
            public MyMediaItem Item { get; set; }
            public string ImageMaskURL { get; set; }

            private AdvertSlot _slot;
            public FUNCTIONALITY(AdvertSlot slot)
            {
                ImageMaskURL = "/_common/images/general/transparent-mask.gif";
                _slot = slot;
            }
            public void Init()
            {
                MyDiv divAdContainer = new MyDiv();
                divAdContainer.Style.Add("position", "relative");
                //Create transparent mask
                MyImage imgMask = new MyImage();
                imgMask.ImageUrl = ImageMaskURL;

                if (Item != null)
                {
                    divAdContainer.Controls.Add(Item);
                    imgMask.Width = (int)Item.Functionality.Width.Value;
                    imgMask.Height = (int)Item.Functionality.Height.Value;
                    Item.Functionality.WindowMode = Common.SWFObject.v2.SWFObject.WMODE.Transparent; // SO that mask will be on top 
                    
                }
                imgMask.Style.Add("position", "absolute");
                divAdContainer.Controls.Add(imgMask);
                imgMask.Style.Add("left", "0px");
                imgMask.Style.Add("top", "0px");
                imgMask.HRef = Item.Href;
                imgMask.HRefTarget = Enums.HREF_TARGET.Blank;
                _slot.Width = Item.Functionality.Width;
                _slot.Height = Item.Functionality.Height;
                _slot.Controls.Add(divAdContainer);
                _slot.Controls.Add(Item);
               
            }
        }
        public new FUNCTIONALITY Functionality { get; private set; }

        public AdvertSlot()
        {
            this.Functionality = new FUNCTIONALITY(this);
            this.CssManager.AddClass("advert");
        }
        protected override void OnLoad(EventArgs e)
        {
            Functionality.Init();
            base.OnLoad(e);
        }


        
    }
}
