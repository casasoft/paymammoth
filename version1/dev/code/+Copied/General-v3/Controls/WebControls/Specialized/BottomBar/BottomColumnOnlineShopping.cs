﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using System.Web.UI.HtmlControls;

namespace CS.General_v3.Controls.WebControls.Specialized.BottomBar
{
 
    public class BottomColumnOnlineShopping : BottomColumn
    {

        public new class FUNCTIONALITY : BottomColumn.FUNCTIONALITY
        {

            private MyDiv _wrapperDIV; 
            public bool ShowPayPal { get; set; }
            public bool ShowOthers { get; set; }
            public string PayPalLink { get; set; }
            public string OthersLink { get; set; }
            public string PayPalAlt { get; set; }
            public string OthersAlt { get; set; }
            
            public FUNCTIONALITY(BottomColumnOnlineShopping col)
                : base(col)
            {
                ShowPayPal = ShowOthers = true;
            }

            private void generateOnlineShoppingEntities() {
                generateWrapper();
                initHeading();
                generatePayPal();
                generateOthers();
            }

            private void initHeading()
            {
                HtmlGenericControl titleSpan = new HtmlGenericControl("span");
                titleSpan.Attributes["class"] = "title";
                titleSpan.InnerText = Title;
                _wrapperDIV.Controls.Add(titleSpan);
            }


            private void generateWrapper()
            {
                _wrapperDIV = new MyDiv();
                _wrapperDIV.Attributes["class"] = "online-shopping-wrapper";
                PlaceHolder.Controls.Add(_wrapperDIV);
            }

            private void generatePayPal()
            {
                if (ShowPayPal && !string.IsNullOrEmpty(PayPalLink)) 
                {
                    MyImage img = new MyImage();
                    img.HRef = PayPalLink;
                    img.CssClass = "online-shopping-paypal";
                    img.ImageUrl = "/images/paypal.jpg";
                    img.AlternateText = PayPalAlt;
                    _wrapperDIV.Controls.Add(img);
                }
            }

            private void generateOthers()
            {
                if (ShowOthers && !string.IsNullOrEmpty(OthersLink))
                {
                    MyImage img = new MyImage();
                    img.HRef = OthersLink;
                    img.CssClass = "online-shopping-others";
                    img.ImageUrl = "/images/others.jpg";
                    img.AlternateText = OthersAlt;
                    _wrapperDIV.Controls.Add(img);
                }
            }

            internal override void Init()
            {
                generateOnlineShoppingEntities();
                base.Init();
            }


        }
        public new FUNCTIONALITY Functionality
        {
            get { return (FUNCTIONALITY)base.Functionality; }
            set
            {
                base.Functionality = value;
            }
        }

        public BottomColumnOnlineShopping()
        {
            Functionality = new FUNCTIONALITY(this);
        }

    }
}
