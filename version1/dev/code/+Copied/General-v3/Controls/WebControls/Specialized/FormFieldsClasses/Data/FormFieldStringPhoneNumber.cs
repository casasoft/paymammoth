﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using System.Web.UI.WebControls;

namespace CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses.Data
{
    public class FormFieldStringPhoneNumber : FormFieldStringBaseData
    {
        private MyTxtBoxTextPhoneNumber _txt = new MyTxtBoxTextPhoneNumber();

        public string CountryCodeSuffix { get { return _txt.Functionality.CountryCodeSuffix; } set { _txt.Functionality.CountryCodeSuffix = value; } }
        public string CountryCodePrefix { get { return _txt.Functionality.CountryCodePrefix; } set { _txt.Functionality.CountryCodePrefix = value; } }
        public List<int> InternationalCountryCodes { get { return _txt.Functionality.InternationalCountryCallingCodes; } set { _txt.Functionality.InternationalCountryCallingCodes = value; } }

        
        public FormFieldStringPhoneNumber()
        {
            this.RowCssClassBase = "form-row-phone";
            this.MinLength = 5;
            this.MaxLength = 15;
        }

        protected override Common.General.IMyFormWebControl createFieldControl()
        {


            return _txt;
           
        }
       
    }
}
