﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.WebControls.Specialized.MenuTreeStructure
{
    public interface IMenuFolder : IMenuItem
    {
        bool AllowAddSubFolders { get; }
        bool AllowAddSubItems { get; }
        IList  GetSubFolders();
        IList GetSubItems();

        IMenuFolder CreateSubFolder();
        IMenuItem CreateSubItem();
        string AddNewItemURL { get; }
    }
        
}
