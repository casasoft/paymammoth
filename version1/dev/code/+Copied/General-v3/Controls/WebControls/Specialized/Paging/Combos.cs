﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Classes.SearchParams;

namespace CS.General_v3.Controls.WebControls.Specialized.Paging
{
    public class Combos : MyDiv
    {

        public class FUNCTIONALITY
        {
            private Combos _ctrl;
            public FUNCTIONALITY(Combos ctrl)
            {
                _ctrl = ctrl;
                _combos = new List<MyDropDownList>();

                CmbShowAmounts = new MyDropDownList();
                CmbSortValues = new MyDropDownList();
                CmbShowAmounts.Title = "Show";
                CmbSortValues.Title = "Sort By";

                _combos.Add(CmbSortValues);
                _combos.Add(CmbShowAmounts);

                //Parameter of querystring
                CmbShowAmounts.Tag = SearchParams_v2.PARAM_SHOWAMT;
                CmbSortValues.Tag = SearchParams_v2.PARAM_SORTBY;
            }
            internal List<MyDropDownList> _combos;
                
            public MyDropDownList CmbShowAmounts { get; set; }
            public MyDropDownList CmbSortValues { get; set; }
            public IParamsInfo SearchParams { get; set; } 

            public bool CombosHasValues
            {
                get
                {
                    for (int i = 0; i < _combos.Count; i++)
                    {
                        if (_combos[i] != null && _combos[i].Items.Count > 0) return true;
                    }
                    return false;
                }
            }

            private int? _selectedShowAmount;
            public int? SelectedShowAmount
            {
                get
                {
                    return _selectedShowAmount;
                }
                set
                {
                    _selectedShowAmount = value;
                }
            }
            public string SelectedSortBy { get; set; }
                        
        }

        public Combos()
        {
            Functionality = new FUNCTIONALITY(this);
        }

         public Combos(IParamsInfo searchParams)
            : this()
        {
            this.Functionality.SearchParams = searchParams;
        }    


        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            initSearchParams();
            initCombos();
        }

        private void initSearchParams()
        {
            if (Functionality.SearchParams != null)
            {
                Functionality.SelectedSortBy = Functionality.SearchParams.PagingInfo.SortByValue;
                Functionality.SelectedShowAmount = Functionality.SearchParams.PagingInfo.ShowAmount;
            }
        }

        private void initCombos()
        {

            bool combosAdded = false;
            MyTable tblCombos = new MyTable();
            MyTableRow tr = tblCombos.AddRow();
            CssManager.AddClass("combos");
            for (int i = 0; i < Functionality._combos.Count; i++)
            {

                MyDropDownList cmb = Functionality._combos[i];

                if (cmb != null && cmb.Items.Count > 0)
                {
                    MyTableCell tdLabel = tr.AddCell();
                    MyTableCell tdCmb = tr.AddCell();
                    tdLabel.CssManager.AddClass("label");
                    tdCmb.CssManager.AddClass("combo");
                    MyLabel lbl = new MyLabel(cmb);
                    tdLabel.Controls.Add(lbl);

                    tdCmb.Controls.Add(cmb);

                    combosAdded = true;

                    CS.General_v3.Classes.URL.URLClass url = new General_v3.Classes.URL.URLClass();

                    string c = "VALLUUUEEE";
                    url.QS[(string)cmb.Tag] = c;
                    //cmb.OnClientChange =
                    cmb.Attributes["onchange"] = "window.location = '" + url.ToString().Replace(c, "'+this.value+'") + "';";




                }
            }
            if (Functionality.SelectedShowAmount != null)
            {
                Functionality.CmbShowAmounts.SelectedValue = Functionality.SelectedShowAmount.Value.ToString();

            }
            else
            {
                throw new Exception("Specify SelectedShowAmount");
            }
            if (Functionality.SelectedSortBy != null)
            {
                Functionality.CmbSortValues.SelectedValue = Functionality.SelectedSortBy;

            }
            else
            {
                throw new Exception("Specify SelectedSortBy");
            }
            if (combosAdded)
            {
                Controls.Add(tblCombos);
            }
        }


        public FUNCTIONALITY Functionality { get; set; }
    }
}
