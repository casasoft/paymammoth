﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.WebControls.Specialized.Paging
{
    public interface IPagingInfo
    {
        string GetPageNo_QuerystringVar();
        int? PageNo { get; set; }
        int? ShowAmount { get; set; }
        string SortByValue { get; set; }
    }
}
