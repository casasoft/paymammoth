﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using System.Web;
using CS.General_v3.Classes.GeoIP.IPInfoDB;
using CS.General_v3.Controls.WebControls.Common.General;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;

namespace CS.General_v3.Controls.WebControls.Specialized.Profile
{
    public enum ProfileMode { New, Edit };

    [ToolboxData("<{0}:RegistrationFields runat=server></{0}:FormFields>")]
    public class ProfileFields : FormFields
    {
        public delegate void OnSubmitHandler(FormValues values);
        public ProfileMode Mode { get; set; }

        public event OnSubmitHandler OnSubmit;


        public class FieldOptions
        {
            public string ID { get; set; }
            public string Title { get; set; }
            public bool Enabled { get; set; }
            public bool EditableOnUpdate { get; set; }
            public string NotEditableOnUpdateMessage { get; set; }
            public bool Required { get; set; }
            public object EditableInitialValue { get; set; }
        }

        public class FormValues
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string PhoneNumber { get; set; }
            public string AddressOne { get; set; }
            public string AddressTwo { get; set; }
            public string Locality { get; set; }
            public string LocalityMore { get; set; }

            public string PostCode { get; set; }
            public CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? Country { get; set; }
            public string Password { get; set; }
            public string FieldLocalityMore { get; set; }
        }

        public FieldOptions FieldFirstName { get; set; }
        public FieldOptions FieldLastName { get; set; }
        public FieldOptions FieldEmail { get; set; }
        public FieldOptions FieldPhoneNumber { get; set; }
        public FieldOptions FieldAddressOne { get; set; }
        public FieldOptions FieldAddressTwo { get; set; }
        public FieldOptions FieldLocality { get; set; }
        public FieldOptions FieldPostCode { get; set; }
        public FieldOptions FieldCountry { get; set; }
        public FieldOptions FieldPassword { get; set; }
        public FieldOptions FieldConfirmPassword { get; set; }
        public FieldOptions FieldLocalityMore { get; set; }

        public ProfileFields()
            : base("profileFields")
        {
            initDefaults();
        }

        private void initDefaults()
        {
            FieldFirstName = new FieldOptions() { ID = "firstName", Title = "First Name", Required = true, EditableOnUpdate = true, Enabled = true };
            FieldLastName = new FieldOptions() { ID = "lastName", Title = "Last Name", Required = true, EditableOnUpdate = true, Enabled = true };
            FieldEmail = new FieldOptions() { ID = "email", Title = "Email", Required = true, EditableOnUpdate = false, Enabled = true };
            FieldPhoneNumber = new FieldOptions() { ID = "phoneNumber", Title = "Phone Number", Required = false, EditableOnUpdate = true, Enabled = true };
            FieldAddressOne = new FieldOptions() { ID = "addressOne", Title = "Address 1", Required = true, EditableOnUpdate = true, Enabled = true };
            FieldAddressTwo = new FieldOptions() { ID = "addressTwo", Title = "Address 2", Required = false, EditableOnUpdate = true, Enabled = true };
            FieldLocality = new FieldOptions() { ID = "locality", Title = "Locality", Required = true, EditableOnUpdate = true, Enabled = true };
            FieldPostCode = new FieldOptions() { ID = "pCode", Title = "Postcode", Required = true, EditableOnUpdate = true, Enabled = true };
            FieldCountry = new FieldOptions() { ID = "country", Title = "Country", Required = true, EditableOnUpdate = true, Enabled = true };
            FieldPassword = new FieldOptions() { ID = "password", Title = "Password", Required = true, EditableOnUpdate = true, Enabled = true };
            FieldConfirmPassword = new FieldOptions() { ID = "confirmPassword", Title = "Confirm Password", Required = true, EditableOnUpdate = true, Enabled = true };
            FieldLocalityMore = new FieldOptions() { ID = "localityMore", Title = "Locality", Required = true, EditableOnUpdate = true, Enabled = true};
        }

        private void createFields()
        {
            if (Mode == ProfileMode.New)
            {
                generateFields();
                this.ButtonSubmitText = "Register";
                this.Functionality.ButtonSubmit.CssManager.AddClass("register-button");
            }
            else if (Mode == ProfileMode.Edit)
            {
                generateFields();
                //generateEditModeFields();
                this.ButtonSubmitText = "Update";
                this.Functionality.ButtonSubmit.CssManager.AddClass("update-button");
            }
            this.ClickSubmit += new EventHandler(RegistrationFields_ClickSubmit);
        }

        private void generateFields()
        {
            if (FieldFirstName.Enabled) this.AddString(FieldFirstName.ID, FieldFirstName.Title, FieldFirstName.Required, null, null, FieldFirstName.EditableInitialValue as string);
            if (FieldLastName.Enabled) this.AddString(FieldLastName.ID, FieldLastName.Title, FieldLastName.Required, null, null, FieldLastName.EditableInitialValue as string);
            if (FieldEmail.Enabled)
            {
                this.Functionality.AddStringEmail(new FormFieldsClasses.Data.FormFieldStringEmailData()
                {
                    ID = FieldEmail.ID,
                    Title = FieldEmail.Title,
                    Required = FieldEmail.Required,
                    CustomValidationAjaxHandlerURL = this.Mode == ProfileMode.New ?  "/ajax/checkEmailExists.ashx" : null,
                    HelpMessage = this.Mode == ProfileMode.Edit ? "Please contact administration should you need to change your email address." : null,
                    InitialValue = FieldEmail.EditableInitialValue as string, ReadOnly = this.Mode == ProfileMode.Edit
                });
                //this.AddString(Enums.STRING_DATA_TYPE.Email, FieldEmail.ID, FieldEmail.Title, FieldEmail.Required, null, null, null);
            }

            if (FieldPhoneNumber.Enabled) this.AddString(FieldPhoneNumber.ID, FieldPhoneNumber.Title, FieldPhoneNumber.Required, null, null, FieldPhoneNumber.EditableInitialValue as string);
            if (FieldAddressOne.Enabled) this.AddString(FieldAddressOne.ID, FieldAddressOne.Title, FieldAddressOne.Required, null, null, FieldAddressOne.EditableInitialValue as string);
            if (FieldAddressTwo.Enabled) this.AddString(FieldAddressTwo.ID, FieldAddressTwo.Title, FieldAddressTwo.Required, null, null, FieldAddressTwo.EditableInitialValue as string);

            
            string countryCode = IPInfoDB.GetCountryCode(CS.General_v3.Util.IPAddressUtil.GetUserIPAddress(),true);
            
            IMyFormWebControl locDropDown ;
            IMyFormWebControl locTextField;

            ///Locality field
           
            ListItemCollection locCol = new ListItemCollection();
            locCol = CS.General_v3.Util.Data.GetMalteseLocalities();

            locCol.Insert(0, new ListItem() { Text = "Choose Locality...", Value = string.Empty, Selected = true });
            if (FieldLocality.Enabled)
            {
                locDropDown = this.AddString(FieldLocality.ID, FieldLocality.Title, FieldLocality.Required, null, null, FieldLocality.EditableInitialValue as string, locCol.Cast<ListItem>());

                if (FieldLocalityMore.Enabled)
                {
                    locTextField = this.AddString(FieldLocalityMore.ID, FieldLocalityMore.Title, FieldLocalityMore.Required, null, null, FieldLocalityMore.EditableInitialValue as string);

                    locDropDown.CssClass += "locality-dropdown";
                    locTextField.CssClass += "locality-textfield";

                    if (countryCode == "MT")
                    {
                        locTextField.CssClass += " not-show";
                    }
                    else
                    {
                        locDropDown.CssClass += " not-show";
                    }
                }
            }

            ///End of locality field

            if (FieldPostCode.Enabled) this.AddString(FieldPostCode.ID, FieldPostCode.Title, FieldPostCode.Required, null, null, FieldPostCode.EditableInitialValue as string);

            ///Country field
            ListItemCollection couCol = new ListItemCollection();
            couCol = CS.General_v3.Util.Data.GetCountriesAsListWith2LetterCodes();
            if (FieldCountry.Enabled)
            {
                var countryControl = this.AddString(FieldCountry.ID, FieldCountry.Title, FieldCountry.Required, null, null, this.Mode == ProfileMode.New ? countryCode : FieldCountry.EditableInitialValue as string, couCol.Cast<ListItem>());
                countryControl.CssClass += "country-combo ";
            }
            ///End of country field
          
            if (FieldPassword.Enabled)
            {
                var passNormal = this.AddString(Enums.STRING_DATA_TYPE.Password, FieldPassword.ID, FieldPassword.Title, this.Mode == ProfileMode.New, null, null, null);
                var confirmPass = this.AddString(Enums.STRING_DATA_TYPE.Password, FieldConfirmPassword.ID, FieldConfirmPassword.Title, this.Mode == ProfileMode.New, null, null, null);
                confirmPass.SubGroupParams = new FormFieldsValidationParams.FormFieldValidationSubGroupParams("passwordsvalidation",FormFieldsValidationParams.FIELD_SUBGROUP_TYPE.SameValues );
                passNormal.SubGroupParams = confirmPass.SubGroupParams;

            }
        }

        private void generateEditModeFields()
        {
            IMyFormWebControl localityControl = null;

            if (FieldFirstName.Enabled) this.AddString(FieldFirstName.ID, FieldFirstName.Title, FieldFirstName.Required, null, null, FieldFirstName.EditableInitialValue as string);
            if (FieldLastName.Enabled) this.AddString(FieldLastName.ID, FieldLastName.Title, FieldLastName.Required, null, null, FieldLastName.EditableInitialValue as string);
            if (FieldEmail.Enabled)
            {

                this.Functionality.AddStringEmail(new FormFieldsClasses.Data.FormFieldStringEmailData()
                {
                    ID = FieldEmail.ID,
                    Title = FieldEmail.Title,
                    Required = FieldEmail.Required,
                    HelpMessage = "Please contact administration should you need to change your email address."
                });
                /*var em = this.AddString(Enums.STRING_DATA_TYPE.Email, FieldEmail.ID, FieldEmail.Title, FieldEmail.Required, null, null, FieldEmail.EditableInitialValue as string);
                em.HelpMsg = "Tet";
                em.Control.Enabled = false;
                HtmlGenericControl span = new HtmlGenericControl("span");
                span.Attributes["class"] = "profile-fields-not-editable";
                span.InnerText = "Contact administration to change Email";
                em.Control.Parent.Controls.Add(span);*/
            }
            if (FieldPhoneNumber.Enabled) this.AddString(FieldPhoneNumber.ID, FieldPhoneNumber.Title, FieldPhoneNumber.Required, null, null, FieldPhoneNumber.EditableInitialValue as string);
            if (FieldAddressOne.Enabled) this.AddString(FieldAddressOne.ID, FieldAddressOne.Title, FieldAddressOne.Required, null, null, FieldAddressOne.EditableInitialValue as string);
            if (FieldAddressTwo.Enabled) this.AddString(FieldAddressTwo.ID, FieldAddressTwo.Title, FieldAddressTwo.Required, null, null, FieldAddressTwo.EditableInitialValue as string);

            if (FieldCountry.Title == "Malta")
            {
                if (FieldLocality.Enabled)
                {
                    localityControl = this.AddString(FieldLocality.ID, FieldLocality.Title, FieldLocality.Required, null, null, FieldLocality.EditableInitialValue as string,
                                                                                                                                        General_v3.Util.Data.GetMalteseLocalities().Cast<ListItem>());
                    localityControl.CssClass += "country-combo";
                }
            }
            else
            {
                if (FieldLocality.Enabled)
                    localityControl =  this.AddString(FieldLocality.ID, FieldLocality.Title, FieldLocality.Required, null, null, FieldLocality.EditableInitialValue as string);
            }

            //if (FieldPostCode.Enabled) this.AddString(FieldPostCode.ID, FieldPostCode.Title, FieldPostCode.Required, null, null, FieldPostCode.EditableInitialValue as string);
            //if (FieldCountry.Enabled) this.AddString(FieldCountry.ID, FieldCountry.Title, FieldCountry.Required, null, null, FieldCountry.EditableInitialValue as string,
            //                                                                                                                            General_v3.Util.Data.GetCountriesAsListWith3LetterCodes());

            ///Locality field

            string countryCode = FieldCountry.EditableInitialValue.ToString();

            IMyFormWebControl locDropDown;
            IMyFormWebControl locTextField;

            ListItemCollection locCol = new ListItemCollection();
            locCol = CS.General_v3.Util.Data.GetMalteseLocalities();

            if (FieldLocality.Enabled)
            {
                if (localityControl == null)
                {
                    locDropDown = this.AddString(FieldLocality.ID, FieldLocality.Title, FieldLocality.Required, null,
                                                 null, FieldLocality.EditableInitialValue as string, locCol.Cast<ListItem>());
                }

                if (FieldLocalityMore.Enabled)
                {
                    locTextField = this.AddString(FieldLocalityMore.ID, FieldLocalityMore.Title, FieldLocalityMore.Required, null, null, FieldLocalityMore.EditableInitialValue as string);

                    localityControl.CssClass += "locality-dropdown";
                    locTextField.CssClass += "locality-textfield";

                    if (countryCode == "MT")
                    {
                        locTextField.CssClass += " not-show";
                    }
                    else
                    {
                        localityControl.CssClass += " not-show";
                    }
                }

            }

            ///End of locality field

            if (FieldPostCode.Enabled) this.AddString(FieldPostCode.ID, FieldPostCode.Title, FieldPostCode.Required, null, null, null);

            ///Country field
            ListItemCollection couCol = new ListItemCollection();
            couCol = CS.General_v3.Util.Data.GetCountriesAsListWith2LetterCodes();
            if (FieldCountry.Enabled)
            {
                var countryControl = this.AddString(FieldCountry.ID, FieldCountry.Title, FieldCountry.Required, null, null, FieldCountry.EditableInitialValue as string, couCol.Cast<ListItem>());
                countryControl.CssClass += "country-combo ";
            }
            ///End of country field
            
            if (FieldPassword.Enabled)
            {
                var password = this.AddString(Enums.STRING_DATA_TYPE.Password, FieldPassword.ID, FieldPassword.Title, false, null, null, null);
                var confirmPassword = this.AddString(Enums.STRING_DATA_TYPE.Password, FieldConfirmPassword.ID, FieldConfirmPassword.Title, false, null, null, null);
                confirmPassword.SubGroupParams = password.SubGroupParams = new FormFieldsValidationParams.FormFieldValidationSubGroupParams("passwordsvalidation",FormFieldsValidationParams.FIELD_SUBGROUP_TYPE.SameValues);
            }
        }

        void RegistrationFields_ClickSubmit(object sender, EventArgs e)
        {
            FormValues fv = new FormValues();
            fv.FirstName = this.GetFormValueStr(FieldFirstName.ID);
            fv.LastName = this.GetFormValueStr(FieldLastName.ID);
            fv.PhoneNumber = this.GetFormValueStr(FieldPhoneNumber.ID);
            fv.Email = this.GetFormValueStr(FieldEmail.ID);
            fv.AddressOne = this.GetFormValueStr(FieldAddressOne.ID);
            fv.AddressTwo = this.GetFormValueStr(FieldAddressTwo.ID);
            fv.Locality = this.GetFormValueStr(FieldLocality.ID);
            fv.LocalityMore = this.GetFormValueStr(FieldLocalityMore.ID);
            fv.PostCode = this.GetFormValueStr(FieldPostCode.ID);
            string countryCode = this.GetFormValueStr(FieldCountry.ID);
            fv.Country = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_FromCode(countryCode);
            fv.Password = this.GetFormValueStr(FieldPassword.ID);


            if (OnSubmit != null)
            {
                OnSubmit(fv);
            }
        }
       
        protected override void OnLoad(EventArgs e)
        {
            createFields();
            base.OnLoad(e);
        }
        

    }
}
