﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.CSS;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace CS.General_v3.Controls.WebControls.Specialized.Reporting.v1
{
    public class BaseReportTableCellDataImpl : BaseReportTableCellDataBaseImpl, IBaseReportTableCellData
    {
        #region IBaseReportTableCellData Members


        public string InnerHTML
        {
            get;
            set;
        }

        public Control Control
        {
            get;
            set;
        }
        
        public override Control GetCellContent()
        {
            
            if (!string.IsNullOrWhiteSpace(InnerHTML))
            {
                return new Literal() {Text = InnerHTML};
            }
            else if (this.Control != null)
            {
                return this.Control;
            }
            else
            {
                return null;
            }
        }

        #endregion
    }
}
