﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_20090518.JavaScript.Data;
using CS.General_20090518.JavaScript.Interfaces;
namespace CS.General_20090518.Controls.WebControls.Specialized.MediaGallery.v2
{
    public class MediaGalleryItemData : IJavaScriptObject
    {
        public int ID { get; set; }
        public bool CanDelete { get; set; }
        public bool CanCrop { get; set; }
        public bool CanReorder { get; set; }
        public bool CanEditCaption { get; set; }

        public string ThumbURL { get; set; }
        public string ItemURL { get; set; }
        public string Caption { get; set; }
        public string SectionName { get; set; }
        public int? Width { get; set; }
        public int? Height { get; set; }

        /// <summary>
        /// If item is an image and can crop in different sizes, fill this in with the respective sizes
        /// </summary>
        public List<MediaGalleryItemCropData> CropSizes { get; set; }

        public MediaGalleryItemData()
        {
            CropSizes = new List<MediaGalleryItemCropData>();
        }

   

        

        /* public bool canDelete;
        public bool canCrop;
        public bool canReorder;

        public string thumbURL;
        public string itemURL;
        public string caption;
        public string sectionName;
        public string id;
        public int width;
        public int height;

        public MediaGalleryItemCropData[] cropSizes;*/

        #region IJavaScriptObject Members

        public JSObject GetJsObject(bool forFlash = false)
        {
            JSObject obj = forFlash ? new JSObjectFlash() : new JSObject();
            obj["id"] = ID.ToString();
            obj["canDelete"] = CanDelete;
            obj["canCrop"] = CanCrop;
            obj["canReorder"] = CanReorder;
            obj["canEditCaption"] = CanEditCaption;
            obj["thumbURL"] = ThumbURL;
            obj["itemURL"] = ItemURL;
            obj["caption"] = Caption;
            obj["sectionName"] = SectionName;
            obj["width"] = Width;
            obj["height"] = Height;
            JSArray arrCrop = new JSArray();
            if (CropSizes != null)
            {
                for (int i = 0; i < CropSizes.Count; i++)
                {
                    arrCrop.AddItem(CropSizes[i].GetJsObject());
                }
            }
            obj["cropSizes"] = arrCrop;
            return obj;
        }

        #endregion
    }
}
