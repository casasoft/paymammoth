﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Util;
using CS.General_v3.Controls.WebControls.Common.General;

namespace CS.General_v3.Controls.WebControls.Specialized.OpeningHours
{
    public class MyOpeningHourItem : BaseFormWebControl
    {
        public class OpeningHoursData
        {
            public TimeSpan? Time1Start { get; set; }
            public TimeSpan? Time1End { get; set; }
            public TimeSpan? Time2Start { get; set; }
            public TimeSpan? Time2End { get; set; }
            public bool IsClosed { get; set; }
            public string Day { get; set; }
        }

        public class FUNCTIONALITY
        {
           private MyOpeningHourItem _control;

            public FUNCTIONALITY(MyOpeningHourItem control)
            {
                _control = control;
            }

            public bool Is24Hr { get; set; }
            public int TimeInterval { get; set; }
           
            public string TextTo { get; set; }
            public string TextClosed { get; set; }
            public string Day { get; set; }

            public TimeSpan GetTime1StartFormValue()
            {
                string strValue = _control._cmbTime1Start.FormValue;
                return _control.convertToTimeSpan(strValue);
            }

            public TimeSpan GetTime1EndFormValue()
            {
                string strValue = _control._cmbTime1End.FormValue;
                return _control.convertToTimeSpan(strValue);
            }

            public TimeSpan GetTime2StartFormValue()
            {
                string strValue = _control._cmbTime2Start.FormValue;
                return _control.convertToTimeSpan(strValue);
            }

            public TimeSpan GetTime2EndFormValue()
            {
                string strValue = _control._cmbTime2End.FormValue;
                return _control.convertToTimeSpan(strValue);
            }

            public bool IsClosed()
            {
                return _control._chkIsClosed.FormValue;
            }
        }

        private MyDropDownList _cmbTime1Start;
        private MyDropDownList _cmbTime1End;
        private MyDropDownList _cmbTime2Start;
        private MyDropDownList _cmbTime2End;
        private MyCheckBox _chkIsClosed;

        public FUNCTIONALITY Functionality { get; private set; }
      
        public MyOpeningHourItem()
        {
            this.Functionality = createFunctionality();
        }
        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        public void init()
        {
            initTable();
        }

        private TimeSpan convertToTimeSpan(string time)
        {
            return TimeSpan.Parse(time);
        }

        private OpeningHoursData getValue()
        {
            TimeSpan time1StartValue = this.Functionality.GetTime1StartFormValue();
            TimeSpan time1EndValue = this.Functionality.GetTime1EndFormValue();
            TimeSpan time2StartValue = this.Functionality.GetTime2StartFormValue();
            TimeSpan time2EndValue = this.Functionality.GetTime2EndFormValue();

            OpeningHoursData hoursData = new OpeningHoursData();
            hoursData.Time1Start = time1StartValue;
            hoursData.Time1End = time1EndValue;
            hoursData.Time2Start = time2StartValue;
            hoursData.Time2End = time2EndValue;
            return hoursData;
        }

        private void initTable()
        {
            MyTable table = new MyTable();
            table.CssClass = "opening-hours-item";
            
            MyTableRow tr = new MyTableRow();
            MyTableCell td1 = new MyTableCell();
            MyTableCell td2 = new MyTableCell();

            td1.Controls.Add(initTimeSets(initCmbTime1Start(), initCmbTime1End(), true, "first-hour-set"));
            td1.Controls.Add(initSpanSeparator());
            td1.Controls.Add(initTimeSets(initCmbTime2Start(), initCmbTime2End(), false, "second-hour-set"));

            td2.Controls.Add(initChkClosed("field-closed"));

            tr.Controls.Add(td1);
            tr.Controls.Add(td2);
            table.Controls.Add(tr);
            this.Controls.Add(table);
        }

        private MyDropDownList initCmbTime1Start()
        {
            _cmbTime1Start = new MyDropDownList();
            _cmbTime1Start.ID = "time1Start";
            _cmbTime1Start.AddListItems(Date.CreateListOfHours(Functionality.Is24Hr, Functionality.TimeInterval));
            return _cmbTime1Start;
        }

        private MyDropDownList initCmbTime1End()
        {
            _cmbTime1End = new MyDropDownList();
            _cmbTime1End.ID = "time1End";
            _cmbTime1End.AddListItems(Date.CreateListOfHours(Functionality.Is24Hr, Functionality.TimeInterval));
            return _cmbTime1End;
        }

        private MyDropDownList initCmbTime2Start()
        {
            _cmbTime2Start = new MyDropDownList();
            _cmbTime2Start.ID = "time2Start";
            _cmbTime2Start.AddListItems(Date.CreateListOfHours(Functionality.Is24Hr, Functionality.TimeInterval));
            return _cmbTime2Start;
        }

        private MyDropDownList initCmbTime2End()
        {
            _cmbTime2End = new MyDropDownList();
            _cmbTime2End.ID = "time2End";
            _cmbTime2End.AddListItems(Date.CreateListOfHours(Functionality.Is24Hr, Functionality.TimeInterval));
            return _cmbTime2End;
        }

        private MyDiv initTimeSets(MyDropDownList start, MyDropDownList end, bool isFirstSet, string cssClass = null)
        {
            MyDiv div = new MyDiv();

            if (!String.IsNullOrEmpty(cssClass))
            {
                div.CssClass = cssClass;
            }

            div.Controls.Add(start);
            div.Controls.Add(initSpanTo());
            div.Controls.Add(end);
            return div;
        }

        private MySpan initSpanSeparator()
        {
            MySpan spanAnd = new MySpan();
            spanAnd.InnerHtml = "&";
            spanAnd.CssClass = "opening-hour-item-separator";
            return spanAnd;
        }

        private MySpan initSpanTo()
        {
            MySpan spanTo = new MySpan();
            spanTo.InnerHtml = Functionality.TextTo;
            return spanTo;
        }

        private MyCheckBox initChkClosed(string cssClass = null)
        {
            _chkIsClosed = new MyCheckBox();
            _chkIsClosed.ID = "chkClosed";
            if(!String.IsNullOrEmpty(cssClass))
            {
                _chkIsClosed.CssClass = cssClass;
            }
            _chkIsClosed.Text = Functionality.TextClosed;
            return _chkIsClosed;
        }

        public OpeningHoursData Value
        {
            get
            {
                return getValue();
            }
            set
            {
                setValue(value);
            }
        }

        protected override object valueAsObject
        {
            get
            {
                return Value;
            }
            set
            {
                if (value == null || value is OpeningHoursData)
                {
                    this.Value = (OpeningHoursData)value;
                }
            }
        }

        private void setValue(OpeningHoursData data)
        {
            if (data != null)
            {
                this._cmbTime1Start.SelectedValue = data.Time1Start.Value.ToString();
                this._cmbTime1End.SelectedValue = data.Time2End.Value.ToString();
                this._cmbTime2Start.SelectedValue = data.Time2Start.Value.ToString();
                this._cmbTime2End.SelectedValue = data.Time2End.Value.ToString();
            }
        }
    }
}

