﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common;

using CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructureBasic;
using CS.General_v3;
namespace CS.General_v3.Controls.WebControls.Specialized.FileBrowser
{
    public class FolderPanel : TableCell
    {
        public class FUNCTIONALITY
        {
            private TreeStructureBasicClass _tree = null;
            private FolderPanel _FolderPanel = null;
            public List<ITreeItemBasic> RootFolders { get; set; }
            public FUNCTIONALITY(FolderPanel FolderPanel)
            {
                _FolderPanel = FolderPanel;
                _tree = new TreeStructureBasicClass();
                this.RootFolders = new List<ITreeItemBasic>();
            }
            public void Render()
            {
                _FolderPanel.Controls.Add(_tree);
                _tree.Functionality.ShowPriorityAfterName = false;
                _tree.Functionality.RootItems = this.RootFolders;
                _tree.Functionality.RenderTree();
            }

        }
        public FUNCTIONALITY Functionality { get; set; }
        public FolderPanel()
        {
            this.Functionality = new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            this.Functionality.Render();
            base.OnLoad(e);
        }
    }
}
