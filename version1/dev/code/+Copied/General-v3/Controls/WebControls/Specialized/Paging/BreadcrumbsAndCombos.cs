﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Controls.WebControls.Specialized.Navigation;
using CS.General_v3.Classes.SearchParams;
namespace CS.General_v3.Controls.WebControls.Specialized.Paging
{
    public class BreadcrumbsAndCombos : MyDiv
    {
        
        public class FUNCTIONALITY
        {
            private BreadcrumbsAndCombos _ui;
            public NavigationBreadcrumbs BreadCrumbs { get; private set; }

            public MyDropDownList CmbShowAmounts { get; private set; }
            public MyDropDownList CmbSortValues { get; private set; }

            private int? _selectedShowAmount;
            public int? SelectedShowAmount
            {
                get
                {
                    return _selectedShowAmount;
                }
                set
                {
                    _selectedShowAmount = value;
                }
            }
            public string SelectedSortBy { get; set; }
            /// <summary>
            /// To interface with the Querystring natively
            /// </summary>
            public IParamsInfo SearchParams { get; set; }
            /// <summary>
            /// Within the COmbos, fill in the Tag with the URL parameter to update on change
            /// </summary>
            public List<MyDropDownList> Combos { get; private set; }

            public FUNCTIONALITY(BreadcrumbsAndCombos ui)
            {
                _ui = ui;
                

                Combos = new List<MyDropDownList>();
                BreadCrumbs = new NavigationBreadcrumbs();
                CmbShowAmounts = new MyDropDownList();
                CmbSortValues = new MyDropDownList();
                CmbShowAmounts.Title = "Show";
                CmbSortValues.Title = "Sort By";

                Combos.Add(CmbSortValues);
                Combos.Add(CmbShowAmounts);

                //Parameter of querystring
                CmbShowAmounts.Tag = SearchParams_v2.PARAM_SHOWAMT;
                CmbSortValues.Tag = SearchParams_v2.PARAM_SORTBY;

            }
            private void initBreadcrumbs()
            {
                if (BreadCrumbs.Functionality.MenuItems != null && BreadCrumbs.Functionality.MenuItems.Count > 0)
                {
                    _ui.Controls.Add(BreadCrumbs);
                }
            }
            
            private void initCombos()
            {
                
                bool combosAdded = false;
                MyDiv divCombos = new MyDiv();
                MyTable tblCombos = new MyTable();
                MyTableRow tr = tblCombos.AddRow();
                divCombos.Controls.Add(tblCombos);
                divCombos.CssManager.AddClass("combos");
                for (int i = 0; i < Combos.Count; i++)
                {
                    
                    MyDropDownList cmb = Combos[i];

                    if (cmb != null && cmb.Items.Count > 0)
                    {
                        MyTableCell tdLabel = tr.AddCell();
                        MyTableCell tdCmb = tr.AddCell();
                        tdLabel.CssManager.AddClass("label");
                        tdCmb.CssManager.AddClass("combo");
                        MyLabel lbl = new MyLabel(cmb);
                        tdLabel.Controls.Add(lbl);

                        tdCmb.Controls.Add(cmb);

                        combosAdded = true;
                        
                        CS.General_v3.Classes.URL.URLClass url = new General_v3.Classes.URL.URLClass();
                        
                        string c = "VALLUUUEEE";
                        url.QS[(string)cmb.Tag] = c;
                        //cmb.OnClientChange =
                        cmb.Attributes["onchange"] = "window.location = '" + url.ToString().Replace(c, "'+this.value+'") + "';";




                    }
                }
                if (SelectedShowAmount != null)
                {
                    CmbShowAmounts.SelectedValue = SelectedShowAmount.Value.ToString();

                }
                else
                {
                    throw new Exception("Specify SelectedShowAmount");
                }
                if (SelectedSortBy != null)
                {
                    CmbSortValues.SelectedValue = SelectedSortBy;

                }
                else
                {
                    throw new Exception("Specify SelectedSortBy");
                }
                if (combosAdded)
                {
                    _ui.Controls.Add(divCombos);
                }
            }
            private void initSearchParams()
            {
                if (SearchParams != null)
                {
                    //Copy values from QS
                    SelectedSortBy = SearchParams.PagingInfo.SortByValue;
                    SelectedShowAmount = SearchParams.PagingInfo.ShowAmount;
                    //CmbShowAmounts.Tag = SearchParams.PagingInfo.SearchParams.Param;
                    //CmbSortValues.Tag = SearchParams.ParamSortBy.Param;

                }
            }
            public void Init()
            {
                initSearchParams();
                initBreadcrumbs();
                initCombos();
                _ui.Controls.Add(new MyDiv() { CssClass = "clear" });
            }
            /// <summary>
            /// Whether at least one combo box has some values
            /// </summary>
            public bool CombosHasValues
            {
                get
                {
                    for (int i = 0; i < Combos.Count; i++)
                    {
                        if (Combos[i] != null && Combos[i].Items.Count > 0) return true;
                    }
                    return false;
                }
            }

        }
        
        public FUNCTIONALITY Functionality { get; private set; }
        public MyDropDownList CmbSortBy { get; set; }
        public MyDropDownList CmbShowAmt { get; set; }

        //Use these to seperate them if you want
        public MyDiv DivResultsText { get; private set; }
        public MyDiv DivPagingBar { get; private set; }

        public BreadcrumbsAndCombos()
        {
            Functionality = new FUNCTIONALITY(this);
            

            this.CssManager.AddClass("breadcrumbs-and-combos");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchParams">Pass the search params of the website and it will interface with such querystring automatically by liasing
        /// with the search params and reading the SortByValue / SortByParam and the ShowAmountValue / Param</param>
        public BreadcrumbsAndCombos(SearchParams_v2 searchParams)
            : this()
        {
            this.Functionality.SearchParams = searchParams;
        }
       
        protected override void OnLoad(EventArgs e)
        {
            Functionality.Init();
            
            base.OnLoad(e);
        }
    }
}
