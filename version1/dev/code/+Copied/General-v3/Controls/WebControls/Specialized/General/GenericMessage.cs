﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Controls.WebControls.Specialized;
namespace CS.General_v3.Controls.WebControls.Specialized.General

{
    public class GenericMessage : MyDiv
    {
        public delegate void GenericMessageHandler(GenericMessage sender, MESSAGE_TYPE type);
        public enum MESSAGE_TYPE {
            Error,
            Success,
            Info
        }

        public class FUNCTIONALITY
        {

            public string SessionErrorID { get { return _note.ClientID + "_error"; } }
            public string SessionSuccessID { get { return _note.ClientID + "_success"; } }
            public string SessionInfoID { get { return _note.ClientID + "_info"; } }

            public event GenericMessageHandler OnShowMessage;


            private GenericMessage _note;
            public FUNCTIONALITY(GenericMessage note)
            {
                _note = note;
                _note.CssManager.AddClass("generic-message");
                _note.Visible = false;
            }
            private void checkMessage()
            {

                string msg = CS.General_v3.Util.SessionUtil.GetMessageAndRemove(SessionErrorID);
                MESSAGE_TYPE type = MESSAGE_TYPE.Error;
                if (!String.IsNullOrEmpty(msg))
                {
                    _note.Visible = true;
                    _note.InnerHtml = msg;
                    _note.CssManager.AddClass("generic-message-error");
                    type = MESSAGE_TYPE.Error;
                }

                msg = CS.General_v3.Util.SessionUtil.GetMessageAndRemove(SessionSuccessID);
                if (!String.IsNullOrEmpty(msg))
                {
                    _note.Visible = true;
                    _note.InnerHtml = msg;
                    _note.CssManager.AddClass("generic-message-success");
                    type = MESSAGE_TYPE.Success;
                }
                msg = CS.General_v3.Util.SessionUtil.GetMessageAndRemove(SessionInfoID);
                if (!String.IsNullOrEmpty(msg))
                {
                    _note.Visible = true;
                    //_note.Controls.Add(new Literal() { Text = msg });
                    _note.InnerHtml = msg;
                    _note.CssManager.AddClass("generic-message-info");
                    type = MESSAGE_TYPE.Info;
                }
                if (_note.Visible)
                {
                    if (this.OnShowMessage != null)
                    {
                        this.OnShowMessage(_note, type);
                    }
                }

            }
            private void redirect(string url, string anchorLink)
            {
                string path = url;
                if (!string.IsNullOrEmpty(anchorLink))
                {
                    path += "#" + anchorLink;
                }
                CS.General_v3.Util.PageUtil.RedirectPage(path);
                
            }
            public void Error(string errorMessage, bool redirectToSamePage, string anchorLink)
            {
                Error(errorMessage, System.Web.HttpContext.Current.Request.Url.PathAndQuery, anchorLink);
            }
            public void Error(string errorMessage, string redirectTo, string anchorLink)
            {
                _note.Page.Session[SessionErrorID] = errorMessage;
                if (!string.IsNullOrEmpty(redirectTo)) redirect(redirectTo, anchorLink);
            }
            public void Success(string successMessage, bool redirectToSamePage, string anchorLink)
            {
                Success(successMessage, System.Web.HttpContext.Current.Request.Url.PathAndQuery, anchorLink);
            }
            public void Success(string successMessage, string redirectTo, string anchorLink)
            {
                _note.Page.Session[SessionSuccessID] = successMessage;
                if (!string.IsNullOrEmpty(redirectTo)) redirect(redirectTo, anchorLink);
            }
            public void Info(string infoMessage, bool redirectToSamePage, string anchorLink)
            {
                Error(infoMessage, System.Web.HttpContext.Current.Request.Url.PathAndQuery, anchorLink);
            }
            public void Info(string infoMessage, string redirectTo, string anchorLink)
            {
                _note.Page.Session[SessionInfoID] = infoMessage;
                if (!string.IsNullOrEmpty(redirectTo)) redirect(redirectTo, anchorLink);

            }
            public void Init()
            {
                checkMessage();
            }


        }
        public FUNCTIONALITY Functionality { get; private set; }


        public string Error { get { return (string)Page.Session[Functionality.SessionErrorID]; } set { Functionality.Error(value, false, null); } }
        public string Success { get { return (string)Page.Session[Functionality.SessionSuccessID]; } set { Functionality.Success(value, false, null); } }
        public string Info { get { return (string)Page.Session[Functionality.SessionInfoID]; } set { Functionality.Info(value, false, null); } }

        public GenericMessage()
        {
            Functionality = new FUNCTIONALITY(this);

        }


        protected override void OnLoad(EventArgs e)
        {
            Functionality.Init();
            base.OnLoad(e);
        }
    }
}
