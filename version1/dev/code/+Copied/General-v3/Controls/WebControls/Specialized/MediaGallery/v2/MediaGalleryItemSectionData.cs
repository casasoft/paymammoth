﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_20090518.JavaScript.Data;
using CS.General_20090518.JavaScript.Interfaces;
namespace CS.General_20090518.Controls.WebControls.Specialized.MediaGallery.v2
{
    public class MediaGalleryItemSectionData :IJavaScriptObject
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public List<MediaGalleryItemData> Items { get; set; }
        public bool CanUpload { get; set; }

        public MediaGalleryItemSectionData()
        {
            Items = new List<MediaGalleryItemData>();
        }

        

        


        

        /* public bool canDelete;
        public bool canCrop;
        public bool canReorder;

        public string thumbURL;
        public string itemURL;
        public string caption;
        public string sectionName;
        public string id;
        public int width;
        public int height;

        public MediaGalleryItemCropData[] cropSizes;*/

        #region IJavaScriptObject Members

        public JSObject GetJsObject(bool forFlash = false)
        {
            JSObject obj = forFlash ? new JSObjectFlash() : new JSObject();
            obj["id"] = ID.ToString();
            obj["title"] = Title;
            JSArray arrItems = new JSArray();
            if (Items != null)
            {
                for (int i = 0; i < Items.Count; i++)
                {
                    arrItems.AddItem(Items[i].GetJsObject());
                }
            }
            obj["items"] = arrItems;
            obj["canUpload"] = CanUpload;
            return obj;
        }

        #endregion
    }
}
