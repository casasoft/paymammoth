﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common;

using CS.General_v3;
using CS.General_v3.Controls.WebControls.Specialized.TreeStructureClasses.TreeStructureBasic;
using System.IO;
namespace CS.General_v3.Controls.WebControls.Specialized.FileBrowser
{
    public class FileBrowser : MyDiv
    {
        public class FUNCTIONALITY
        {
            private string sessionStatusMsgText
            {
                get
                {
                    return (string) CS.General_v3.Util.PageUtil.GetSessionObject("FileBrowser_" + this._fileBrowser.ID + "_StatusMsg_Text");
                }
                set
                {
                    CS.General_v3.Util.PageUtil.SetSessionObject("FileBrowser_" + this._fileBrowser.ID + "_StatusMsg_Text",value);
                }
            }
            private bool sessionStatusMsgStatus
            {
                get
                {
                    return ((bool?)CS.General_v3.Util.PageUtil.GetSessionObject("FileBrowser_" + this._fileBrowser.ID + "_StatusMsg_Status")).GetValueOrDefault();
                }
                set
                {
                    CS.General_v3.Util.PageUtil.SetSessionObject("FileBrowser_" + this._fileBrowser.ID + "_StatusMsg_Status",value);
                }
            }
            private void initDefaultImages()
            {
                this.ImageURL_FolderSmall = "/_common/static/images/components/v1/filebrowser/icon_folder_small.gif";
                this.ImageURL_FolderLarge = "/_common/static/images/components/v1/filebrowser/icon_folder_large.png";
                this.ImageURL_File_Photo = "/_common/static/images/components/v1/filebrowser/icon_photo.png";
                this.ImageURL_File_Video = "/_common/static/images/components/v1/filebrowser/icon_video.png";
                this.ImageURL_File_Other = "/_common/static/images/components/v1/filebrowser/icon_file.png";
                this.ImageURL_Delete = "/_common/static/images/components/v1/filebrowser/delete.gif";

            }
            public FUNCTIONALITY(FileBrowser fileBrowser)
            {
                initDefaultImages();
                this.QueryStringParam_FileTypes = "filetypes";
                this.QueryStringParam_Folder = "folder";
                _fileBrowser = fileBrowser;
                this.FileBrowserUI = new FileBrowserUI();
                this.FileBrowserUI.ID = this._fileBrowser.ID + "_fileBrowserUI";
                this.FileBrowserUI.Functionality.OnFileUpload += new UploadPanel.FUNCTIONALITY.UploadEventDelegate(FileBrowserUI_OnFileUpload);
                this.FileBrowserUI.Functionality.OnDeletedItems += new FileBrowserUI.FUNCTIONALITY.OnDeletedItemsDelegate(FileBrowserUI_OnDeletedItems);
                this.FileBrowserUI.Functionality.ShowDeleteButtonForCheckboxes = true;
                this.FileBrowserUI.Functionality.ShowCheckboxes = true;
                this.ShowStatusMessage = true;
                this.DivStatusMsg = new MyDiv();
                this.FileBrowserUI.Functionality.OnCreateNewFolder += new FileBrowserUI.FUNCTIONALITY.OnCreateNewFolderDelegate(FileBrowserUI_OnCreateNewFolder);
            }

            void FileBrowserUI_OnCreateNewFolder(FileBrowserUI sender, string folderName)
            {
                string folder = this.RootFolder + "/" +  this.Folder;
                if (!folder.EndsWith("/")) folder += "/";
                folder += folderName + "/";
                folder = CS.General_v3.Util.PageUtil.MapPath(folder);
                if (System.IO.Directory.Exists(folder))
                    showStatusMessage("Folder already exists",false);
                else
                {
                    CS.General_v3.Util.IO.CreateDirectory(folder);
                    showStatusMessage("Folder created successfully",true);
                }
            }


            public delegate string GetFileLinkURLDelegate(FileInfo fileInfo);
            public event GetFileLinkURLDelegate GetFileLinkURL;


            public delegate void ShowStatusMessageDelegate(string msg, bool success);
            public event ShowStatusMessageDelegate OnShowStatusMessage;
            protected void showStatusMessage(string msg, bool success)
            {
                if (OnShowStatusMessage != null)
                    OnShowStatusMessage(msg,success);
                if (ShowStatusMessage)
                {
                    this.sessionStatusMsgText = msg;
                    this.sessionStatusMsgStatus = success;
                    CS.General_v3.Classes.URL.URLClass.RedirectToSamePage();
                }
            }
            #region Properties
            public bool ShowStatusMessage { get; set; }
            public MyDiv DivStatusMsg { get; set; }
            public string RootFolder { get; set; }
            public string ImageURL_FolderSmall { get; set; }
            public string ImageURL_FolderLarge { get; set; }
            public string ImageURL_File_Photo { get; set; }
            public string ImageURL_File_Video { get; set; }
            public string ImageURL_File_Other { get; set; }
            public string ImageURL_Delete { get; set; }
            public string BtnDelete_CssClass { get; set; }
            public string Folder
            {
                get
                {
                    string s = CS.General_v3.Util.PageUtil.GetVariableFromQuerystring(QueryStringParam_Folder);

                    return s ?? "";
                }
            }
            /// <summary>
            /// Comma/colon-separated list of file types allowed
            /// </summary>
            public string FileTypes
            {
                get
                {
                    string s = CS.General_v3.Util.PageUtil.GetVariableFromQuerystring(QueryStringParam_FileTypes);
                    if (string.IsNullOrEmpty(s))
                        s = "jpg,jpeg,bmp,tiff,tif,png,gif";
                    return s;
                }
            }
            private List<string> getFileTypesAsList()
            {
                string[] tokens = FileTypes.Split(new string[] { ",", "|", ";" }, StringSplitOptions.RemoveEmptyEntries);
                List<string> list = new List<string>();
                for (int i = 0; i < tokens.Length; i++)
                {
                    string s = tokens[i];
                    s = s.Trim();
                    list.Add(s);

                }
                return list;
            }
            #endregion

            // public string 
            public string GetRootFolder_Local()
            {
                string path = CS.General_v3.Util.PageUtil.MapPath(RootFolder);
                if (!path.EndsWith("\\")) path += "\\";
                return path;

            }
            public string GetCurrentFolder_Local()
            {
                
                string path = CS.General_v3.Util.PageUtil.MapPath(RootFolder + "/" + Folder);
                if (!path.EndsWith("\\")) path += "\\";
                return path;
            }
            public string QueryStringParam_Folder { get; set; }
            public string QueryStringParam_FileTypes { get; set; }
            public event UploadPanel.FUNCTIONALITY.UploadEventDelegate OnFileUpload;
            
            private void checkCurrentFolder()
            {
                string currentFolder = GetCurrentFolder_Local();
                if (!Directory.Exists(currentFolder))
                {
                 //   throw new InvalidOperationException("Specified directory does not exist");
                }
            }

            public FileBrowserUI FileBrowserUI { get; private set; }

            
            private FileBrowser _fileBrowser = null;
           
            private void FileBrowserUI_OnDeletedItems(FileBrowserUI sender, List<ContentPanelItem> itemsToDelete)
            {
                foreach (var item in itemsToDelete)
                {
                    if (item.Functionality.Tag is FileInfo)
                    {
                        FileInfo file = (FileInfo)item.Functionality.Tag;
                        CS.General_v3.Util.IO.DeleteFile(file.FullName);
                    }
                    else
                    {
                        try
                        {
                            DirectoryInfo dir = (DirectoryInfo)item.Functionality.Tag;
                            CS.General_v3.Util.IO.DeleteDirectory(dir.FullName);
                        }
                        catch
                        {

                        }
                    }
                    
                }
                showStatusMessage("File(s) deleted successfully",true);
            }

            private void FileBrowserUI_OnFileUpload(UploadPanel sender, string filename, Stream fileContent,
                int? widthPx, int? heightPx, bool fillBoxByCropping)
            {
                string folderPath = GetCurrentFolder_Local();
                string newFilename = filename;
                bool ok = false;
                string path;
                int fileNo = 0;
                do
                {
                    path = folderPath + newFilename;
                    if (System.IO.File.Exists(path))
                    {
                        fileNo++;
                        newFilename = CS.General_v3.Util.IO.GetFilenameOnly(newFilename) + "_" + fileNo + "." + 
                            CS.General_v3.Util.IO.GetExtension(newFilename);
                        ok = false;
                    }
                    else
                    {
                        ok = true;
                    }
                } while (!ok);
                CS.General_v3.Util.IO.SaveStreamToFile(fileContent, path);
                if (widthPx != null || heightPx != null)
                {
                    Enums.CROP_IDENTIFIER cropIdentifier = Enums.CROP_IDENTIFIER.None;
                    if (fillBoxByCropping)
                        cropIdentifier = Enums.CROP_IDENTIFIER.MiddleMiddle;
                    if (widthPx == null) widthPx = 999999;
                    if (heightPx == null) heightPx = 999999;
                    CS.General_v3.Util.Image.ResizeAndCropImage(path, widthPx.Value, heightPx.Value, path, cropIdentifier);
                }

                showStatusMessage("File '" + filename + "' uploaded successfully",true);
                    



            }
            private TreeItemBasic getFolderNode(string title, string folder)
            {
                TreeItemBasic n = new TreeItemBasic();
                n.Title = title;
                n.ImageURL = this.ImageURL_FolderSmall;
                CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass();
                url[QueryStringParam_Folder] = folder;
                n.LinkURL = url.ToString();
                return n;
            }
            private void fillNodeFromDir(string currFolder, TreeItemBasic node, DirectoryInfo dir )
            {
                DirectoryInfo[] subDirs = dir.GetDirectories();
                for (int i = 0; i < subDirs.Length; i++)
                {
                    var d = subDirs[i];
                    string folder = currFolder + d.Name + "/";
                    TreeItemBasic nodeDir = getFolderNode(d.Name, folder);
                    node.ChildItems.Add(nodeDir);
                    fillNodeFromDir(folder, nodeDir, d);
                }
            }
            private void fillTree()
            {
                string path = RootFolder;
                if (path.Contains("/")) path = CS.General_v3.Util.PageUtil.MapPath(path);
                CS.General_v3.Util.IO.CreateDirectory(path);
                DirectoryInfo rootDir = new DirectoryInfo(path);
                TreeItemBasic nodeRoot = getFolderNode("Root","/");  
                fillNodeFromDir("/",nodeRoot, rootDir);
                this.FileBrowserUI.Functionality.FolderPanel.Functionality.RootFolders.Add(nodeRoot);

            }
            private int contentIndex = 0;
            private ContentPanelItem getContentPanelItem(object tag, string filename, string imageURL, string linkURL,
                bool addDeleteBtn)
            {
                contentIndex++;
                ContentPanelItem item = new ContentPanelItem();
                item.ID = this._fileBrowser.ID + "_contentItem_" + contentIndex;
                item.Functionality.Title = filename;
                item.Functionality.LinkURL = linkURL;
                item.Functionality.ImageURL = imageURL;
                if (addDeleteBtn)
                {
                    CS.General_v3.Controls.WebControls.Classes.ExtraButton btnDelete = new CS.General_v3.Controls.WebControls.Classes.ExtraButton();
                    btnDelete.Title = "Delete";
                    btnDelete.ConfirmMessageOnClick = "Are you sure you want to delete?";
                    btnDelete.ImageUrl_Up = this.ImageURL_Delete;
                    btnDelete.CssClass = this.BtnDelete_CssClass;
                    btnDelete.Tag = item;
                    
                    btnDelete.Click += new EventHandler(btnDelete_Click);
                    item.Functionality.Buttons.Add(btnDelete);
                }
                item.Functionality.Tag = tag;
                return item;

            }

            private void btnDelete_Click(object sender, EventArgs e)
            {
                MyButton btn = (MyButton)sender;
                ContentPanelItem item = (ContentPanelItem)btn.Tag;
                List<ContentPanelItem> list = new List<ContentPanelItem>();
                list.Add(item);
                FileBrowserUI_OnDeletedItems(FileBrowserUI, list);
                
            }
            private void addContentNode_UpDir()
            {
                CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass();
                string upFolder = this.Folder;
                if (upFolder.EndsWith("/"))
                    upFolder = upFolder.Substring(0, upFolder.Length - 1);
                int lastIndex = upFolder.LastIndexOf('/');
                upFolder = upFolder.Substring(0, lastIndex + 1);
                url[QueryStringParam_Folder] = upFolder;
                ContentPanelItem itemUp = getContentPanelItem(null,"..",this.ImageURL_FolderLarge,url.ToString(),false);
                FileBrowserUI.Functionality.ContentPanelItems.Add(itemUp);

            }
            private string getImageForFilename(string localPath)
            {
                if (CS.General_v3.Util.Image.CheckImageFileExtension(localPath))
                {
                    string webPath = CS.General_v3.Util.PageUtil.MapPathFromLocalToWeb(localPath);
                    return webPath;
                }
                else if (CS.General_v3.Util.VideoUtil.CheckVideoExtension(localPath))
                    return this.ImageURL_File_Video;
                else
                    return this.ImageURL_File_Other;
            }
            private void addContent_Files()
            {
                if (Directory.Exists(GetCurrentFolder_Local()))
                {
                    DirectoryInfo dir = new DirectoryInfo(GetCurrentFolder_Local());
                    
                    
                    DirectoryInfo[] dirs = dir.GetDirectories();
                    CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass();
                    foreach (var d in dirs)
                    {
                        string currFolder = this.Folder ;
                        if (!currFolder.EndsWith("/")) currFolder += "/";
                        currFolder += d.Name + "/";
                        url[QueryStringParam_Folder] = currFolder;
                        ContentPanelItem panel = getContentPanelItem(d,d.Name, this.ImageURL_FolderLarge,url.ToString(),true);
                        FileBrowserUI.Functionality.ContentPanelItems.Add(panel);
                    }
                    List<FileInfo> files = CS.General_v3.Util.IO.GetFilesInDirByExtensions(dir, FileTypes);
                    foreach (var f in files)
                    {
                        ContentPanelItem panelFile = getContentPanelItem(f,f.Name, getImageForFilename(f.FullName),GetFileLinkURL(f),true);
                        FileBrowserUI.Functionality.ContentPanelItems.Add(panelFile);
                    }
                }


            }
            private void fillContent()
            {
                this.FileBrowserUI.Functionality.FileTypesAllowed = getFileTypesAsList();
                if (string.Compare(GetCurrentFolder_Local(), GetRootFolder_Local(), true) != 0)
                {
                    addContentNode_UpDir();

                }
                addContent_Files();
                

            }
            private void renderStatusMsg()
            {
                if (this.ShowStatusMessage)
                {
                    _fileBrowser.Controls.Add(DivStatusMsg);
                    if (!string.IsNullOrEmpty(sessionStatusMsgText))
                    {
                        DivStatusMsg.InnerHtml = sessionStatusMsgText;
                    }
                    sessionStatusMsgText = null;
                    sessionStatusMsgStatus = false;
                }
            }
            private void checkRequirements()
            {
                StringBuilder sb = new StringBuilder();
                if (this.GetFileLinkURL == null)
                    sb.AppendLine("GetFileLinkURL must be attached to");
                if (sb.Length > 0)
                {
                    throw new InvalidOperationException("FileBrowser:: " + sb.ToString());
                }
            }
            private void fillNavBreadcrumb()
            {
                string folder = this.Folder ?? "";
                string currFolder = "/";
                string[] tokens = folder.Split('/');
                CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass();
                url[QueryStringParam_Folder] = "/";
                this.FileBrowserUI.Functionality.AddNavigationBreadCrumbItem("Root", url.ToString());
                for (int i = 0; i < tokens.Length; i++)
                {
                    string s = tokens[i] ?? "";
                    s = s.Trim();

                    if (!string.IsNullOrEmpty(s))
                    {
                        if (s.StartsWith("/")) s = s.Substring(1);
                        if (s.EndsWith("/")) s = s.Substring(0,s.Length-1);
                        currFolder += s + "/";
                        url[QueryStringParam_Folder] = currFolder;
                        this.FileBrowserUI.Functionality.AddNavigationBreadCrumbItem(s, url.ToString());
                    }

                    
                }
            }
            public virtual void Render()
            {
                checkRequirements();
                renderStatusMsg();
                _fileBrowser.CssManager.AddClass("filebrowser");
                checkCurrentFolder();
                _fileBrowser.Controls.Add(this.FileBrowserUI);
                fillNavBreadcrumb();
                fillTree();
                fillContent();
                
                
            }

        }
        public FUNCTIONALITY Functionality { get; set; }
        public FileBrowser(string id)
        {
            this.ID = id;
            this.Functionality = new FUNCTIONALITY(this);

        }

        protected override void OnLoad(EventArgs e)
        {
            this.Functionality.Render();
            base.OnLoad(e);
        }

    }
}
