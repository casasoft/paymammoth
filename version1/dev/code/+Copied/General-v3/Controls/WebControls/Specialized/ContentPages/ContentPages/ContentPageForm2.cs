﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using CS.General_20090518.Controls.WebControls.Common;
using CS.General_20090518.Controls.Classes.MediaGallery;
using CS.General_20090518.Util;
namespace CS.General_20090518.Controls.WebControls.Specialized.ContentPages.ContentPages
{
    [ToolboxData("<{0}:ContentPageForm runat=server></{0}:ContentPageForm>")]    
    public class ContentPageForm2 : MenuTreeStructure.MenuItemForm
    {
        public class FUNCTIONALITY
        {
            public enum HTML_EDITOR_TYPE
            {
                FCKEditor,
                CKEditor
            }
            private ContentPageForm2 _contentPageForm = null;
            public FUNCTIONALITY(ContentPageForm2 contentPageForm)
            {
                _contentPageForm = contentPageForm;
                this.HTMLEditorType = HTML_EDITOR_TYPE.CKEditor;
            }
            public HTML_EDITOR_TYPE HTMLEditorType { get; set; }
            public string EditorCSSFile { get; set; }
        }
        public FUNCTIONALITY Functionality {get;set;}
        
        /* HOW TO USE
        * ----------
        * 
        * Parameters to fill
        * 
        *  - ItemTitle
        *  - ListingPageUrl
        *  
        * Events to Attach
        *  
        *  - LoadFolder
        *  
        *  
        * Optional
        * 
        * - QueryStringItemParam
        * - QueryStringParentFolderParam
        */
        public ContentPageForm2()
        {
            this.Functionality = new FUNCTIONALITY(this);
            this.ItemTitle = "Page";
            base.LoadFolder += new CS.General_20090518.Controls.WebControls.Specialized.MenuTreeStructure.MenuItemForm.CreateLoadFolderEvent(LinkItemForm_LoadFolder);
            
        }
        
        
        CS.General_20090518.Controls.WebControls.Specialized.MenuTreeStructure.IMenuFolder LinkItemForm_LoadFolder(int ID)
        {
            return LoadFolder(ID);
        }
        #region Constants / Statics
        

        #endregion
        #region Delegates
        public delegate IContentPage CreateLoadFolderEvent(int ID);
        #endregion
        #region Events
        public new event CreateLoadFolderEvent LoadFolder;
        #endregion
        #region Methods


        public IPageFolder GetParentFolder()
        {
            return LoadFolder(GetParentFolderIDFromQueryString());
        }
        public IPage LoadItemFromParent()
        {
            IPageFolder parent = GetParentFolder();

            IPage item = null;
            int itemID = GetItemIDFromQueryString();
            item = (IPage)parent.GetSubItems().FindElemObj(obj => ((IPage)obj).ID == itemID);

            return item;


        }
        #endregion
        #region Properties
        public IContentPage PageItem
        {
            get
            {
                
                return (IContentPage)base.MenuItem;
            }


        }

        #endregion
        

        protected override void OnInit(EventArgs e)
        {

            
            base.OnInit(e);
        }
        public CS.General_20090518.Controls.WebControls.Common.FCKEditor.FCKeditor GetFCKEditorForHTMLEditor()
        {
            return (CS.General_20090518.Controls.WebControls.Common.FCKEditor.FCKeditor)this.GetControl("txtText");
        }
        public CS.General_20090518.Controls.WebControls.Common.CKEditor GetCKEditorForHTMLEditor()
        {
            return (CS.General_20090518.Controls.WebControls.Common.CKEditor)this.GetControl("txtText");
        }
        public override void addExtraFormFields()
        {
            var pg = this.PageItem;
            
            
            if (this.Functionality.HTMLEditorType == FUNCTIONALITY.HTML_EDITOR_TYPE.FCKEditor)
            {
                var fck = this.AddFCKEditor("txtText", "Text", true, null, 600, 600, pg != null ? pg.HtmlText : "");
                fck.EditorAreaCSS = this.Functionality.EditorCSSFile;
                
            }
            else if (this.Functionality.HTMLEditorType == FUNCTIONALITY.HTML_EDITOR_TYPE.CKEditor) 
            {
                var ck = this.AddCKEditor("txtText", "Text", true, null, 800, 600, pg != null ? pg.HtmlText : "");
                if (!string.IsNullOrEmpty(this.Functionality.EditorCSSFile))
                    ck.ConfigParameters.ContentCSSFiles.Add(this.Functionality.EditorCSSFile);
            }
            
            base.addExtraFormFields();
        }

        public override void saveExtraFields()
        {
            var pg = this.PageItem;
            pg.HtmlText = this.GetFormValueStr("txtText");
            

            base.saveExtraFields();
        }



        protected override void OnLoad(EventArgs e)
        {
            
            base.OnLoad(e);
        }

        


 
    }
}
