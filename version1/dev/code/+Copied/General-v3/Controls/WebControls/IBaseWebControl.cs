﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.CSS;
using System.Web.UI.WebControls;
namespace CS.General_v3.Controls.WebControls
{
    public interface IBaseWebControl : IBaseControl
    {
        
        

        
        CSSManagerForControl CssManager { get;  }
        string CssClass { get; set; }
        new BaseWebControlFunctionality WebControlFunctionality { get; }

        string RollOverClass { get; set; }

        /// <summary>
        /// An ID to reference the WebControl with
        /// </summary>
        string StringID { get; set; }
        new WebControl Control { get; }

        string OnClientBlur { get; set; }
        string OnClientClick { get; set; }
        string OnClientChange { get; set; }
        string OnClientLoad { get; set; }
    }
}
