﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI;
using CS.General_v3.Classes.CSS;
namespace CS.General_v3.Controls.WebControls
{
    public class BaseControlFunctionality : IBaseControl
    {
        protected Control _control;
        public BaseControlFunctionality(Control control)
        {
            _control = control;
         
        }
        public string ID
        {
            get { return Control.ID; }
            set { Control.ID = value; }
        }

        #region IBaseControl Members

        public Control Control
        {
            get { return _control; }
        }

        public BaseControlFunctionality WebControlFunctionality
        {
            get { return this; }
        }

        private bool isVisibleControlEvenInParents(Control c)
        {

            if (!c.Visible)
            {
                return false;
            }
            else if (c.Parent != null)
            {
                return isVisibleControlEvenInParents(c.Parent);
            }
            else
            {
                return true;
            }
        }

        public bool IsVisibleEvenInParents()
        {
            return isVisibleControlEvenInParents(_control);
        }

        #endregion
    }
}
