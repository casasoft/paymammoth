﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI;
using CS.General_v3.Classes.CSS;
namespace CS.General_v3.Controls.WebControls
{
    public class BaseHtmlControlFunctionality : BaseControlFunctionality
    {
        public int __GlobalCtrlIdentifier = Util.Other.GetNextControlGlobalIdentifier();

        public BaseHtmlControlFunctionality(HtmlControl control) : base(control)
        {
            
            this.CssManager = new CSSManagerForControl(control);
        }



        public CSSManagerForControl CssManager { get; private set; }
        public string CssClass
        {
            get
            {
                return this.CssManager.ToString();
            }
            set
            {
                this.CssManager.Clear();
                this.CssManager.AddClass(value);
            }
        }
    }
}
