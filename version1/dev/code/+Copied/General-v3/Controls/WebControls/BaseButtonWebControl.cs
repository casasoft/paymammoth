﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.CSS;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.Web.UI.WebControls ;
using CS.General_v3.Controls.WebControls.Common.General;
namespace CS.General_v3.Controls.WebControls
{
    public abstract class BaseButtonWebControl : BaseWebControl, IMyButtonWebControl, IPostBackEventHandler
    {
        public event EventHandler Click
        {
            add
            {
                this.WebControlFunctionality.Click += value;
            }
            remove
            {
                this.WebControlFunctionality.Click -= value;
            }
        }

        public BaseButtonWebControl(string id)
            : this(HtmlTextWriterTag.Input, id)
        {
            init(id);
        }

        public BaseButtonWebControl(System.Web.UI.HtmlTextWriterTag tagName, string id)
            : base(tagName)
        {
            init(id);

        }
        public BaseButtonWebControl(string tagName, string id)
            : base(tagName)
        {
            init(id);
            
          
        }

        protected override BaseWebControlFunctionality getWebControlFunctionality()
        {
            return new MyButtonWebControlFunctionality(this);
        }

     


        public bool doNotRenderElement = false;


        private void init(string ID) {
            this.ID = ID;
            this.DisableButtonOnSubmitForm = true;
            this.ValidationDefault = true;
            this.DisableButtonOnSubmitFormText = null;
            init();
            
            
        }







        public string Text { get { return WebControlFunctionality.Text; } set { WebControlFunctionality.Text = value; } }



       /* private void attachASPPostbackJS()
        {
            //this

            string js = Page.ClientScript.GetPostBackEventReference(this, "Click");
            this.OnClientClick += js;
        }*/
       /* private void attachValidationJS()
        {
            this.OnClientClick += "js.com.cs."+CS.General_v3.Settings.Others.JAVASCRIPT_VERSION+".UI.Forms.FormsCollection.get_instance().validateCurrentForm();";
        }*/

       /* private void attachJsClickEvent()
        {
            if (this.WebControlFunctionality.HasClickEventBeenAttached)
            {
                attachASPPostbackJS();
            }
            else
            {
                attachValidationJS();
            }

        }*/
        
        protected override void OnLoad(EventArgs e)
        {
            
            base.OnLoad(e);
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        }

        
        public void RaisePostBackEvent(string eventArgument)
        {
            
            switch (eventArgument)
            {
                case "Click":
                    this.WebControlFunctionality.TriggerClickEvent();
                    break;
            }
        }
         





        private void init()
        {
            this.ValidationGroup = "main";
            this.Attributes["type"] = "button";
        }
        public string PostBackUrl { get; set; }
        public bool UseSubmitBehavior { get; set; }
        public bool DisableButtonOnSubmitForm { get; set; }
        public string DisableButtonOnSubmitFormText { get; set; }
        public string ConfirmMessage { get { return WebControlFunctionality.ConfirmMessage; } set { WebControlFunctionality.ConfirmMessage = value; } }
        public string ConfirmMessageHighlightRowClass { get { return WebControlFunctionality.ConfirmMessageHighlightRowClass; } set { WebControlFunctionality.ConfirmMessageHighlightRowClass = value; } }
        public string ConfirmMessageHighlightRowID { get { return WebControlFunctionality.ConfirmMessageHighlightRowID; } set { WebControlFunctionality.ConfirmMessageHighlightRowID = value; } }
        public bool NoValidation { get; set; }
        private StringBuilder _JSAfterControl = new StringBuilder();
        public object Tag2 { get; set; }
        public object Tag3 { get; set; }
        public bool ValidationDefault { get; set; }
        public bool ShowProgressBar { get; set; }

        public virtual string ValidationGroup { get { return WebControlFunctionality.ValidationParams.validationGroup; } set { WebControlFunctionality.ValidationParams.validationGroup = value; } }



        private void checkPostBackUrl()
        {
            if (!string.IsNullOrEmpty(this.PostBackUrl))
            {
                string JS = ";document.forms[0].action = \"" + this.PostBackUrl + "\";";
                JS += "document.forms[0].submit();";
                OnClientClick += JS;

            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (string.IsNullOrEmpty(this.ID))
            {
                throw new InvalidOperationException("A button must always have an ID set to a value");
            }

            checkPostBackUrl();
            base.Render(writer);

        }


        public object Tag { get; set; }
      

        public new WebControl Control
        {
            get { return this; }
        }


        #region IMyButtonWebControl Members


        MyButtonWebControlFunctionality IMyButtonWebControl.WebControlFunctionality
        {
            get { return (MyButtonWebControlFunctionality)base.WebControlFunctionality; }
        }

        #endregion

        #region IBaseControl Members

        Control IBaseControl.Control
        {
            get { return this; }
        }

        public new MyButtonWebControlFunctionality WebControlFunctionality
        {
            get { return (MyButtonWebControlFunctionality)base.WebControlFunctionality; }
        }

        #endregion

        #region IMyBaseFormWebControl Members

        public string HelpID
        {
            get;
            set;
        }

        public string HelpMsg
        {
            get;
            set;
        }

        #endregion

        #region IBaseControl Members


        BaseControlFunctionality IBaseControl.WebControlFunctionality
        {
            get { return (BaseControlFunctionality)this.WebControlFunctionality; }
        }

        #endregion

        #region IMyBaseFormWebControl Members

        public virtual string Title { get { return this.Text; } set { this.Text = value; } }

        #endregion

        #region IMyBaseFormWebControl Members


        public string Class_Disabled { get { return WebControlFunctionality.Class_Disabled; } set { WebControlFunctionality.Class_Disabled = value; } }
        public string Class_onFocus { get { return WebControlFunctionality.Class_Focus; } set { WebControlFunctionality.Class_Focus = value; } }
        public string Class_onError { get { return WebControlFunctionality.Class_Error; } set { WebControlFunctionality.Class_Error = value; } }
        public string Class_onOver { get { return WebControlFunctionality.Class_Over; } set { WebControlFunctionality.Class_Over = value; } }
        public string Class_ReadOnly { get { return WebControlFunctionality.Class_ReadOnly; } set { WebControlFunctionality.Class_ReadOnly = value; } }
        public string Class_Required { get { return WebControlFunctionality.Class_Required; } set { WebControlFunctionality.Class_Required = value; } }
        public bool UseDefaultCSSClasses { get { return WebControlFunctionality.UseDefaultCSSClasses; } set { WebControlFunctionality.UseDefaultCSSClasses = value; } }

        //public string OnClickHref { get { return WebControlFunctionality.OnClickHref; } set { WebControlFunctionality.OnClickHref = value; } }


        #endregion

        #region IMyBaseFormWebControl Members


        public string Value { get { return WebControlFunctionality.Value; } set { WebControlFunctionality.Value = value; } }

        #endregion
    }
}
