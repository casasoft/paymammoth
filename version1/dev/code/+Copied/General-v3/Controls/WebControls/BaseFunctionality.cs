﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
namespace CS.General_20101215.Controls.WebControls
{
    public class BaseFunctionality
    {
        protected BaseWebControl _control;
        public BaseFunctionality(BaseWebControl control)
        {
            _control = control;

        }

        public virtual void Init()
        {

        }
    }
}
