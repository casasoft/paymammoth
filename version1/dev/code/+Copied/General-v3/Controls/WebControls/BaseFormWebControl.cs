﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.CSS;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.Web.UI.WebControls ;
using CS.General_v3.Controls.WebControls.Common.General;
using CS.General_v3.Controls.WebControls.Specialized.FormFieldsClasses;
namespace CS.General_v3.Controls.WebControls
{
    public abstract class BaseFormWebControl : BaseWebControl, IMyFormWebControl
    {

        public BaseFormWebControl() : base() {
        }

        public BaseFormWebControl(System.Web.UI.HtmlTextWriterTag tagName)
            : base(tagName)
        {
        }

        public BaseFormWebControl(string tagName)
            : base(tagName)
        {
        }

        protected override BaseWebControlFunctionality getWebControlFunctionality()
        {
            return new MyFormWebControlFunctionality(this, this.ClientIDSeparator);
        }

        #region IMyFormWebControl Members

        public string HelpID
        {
            get { return WebControlFunctionality.HelpID; }
            set { WebControlFunctionality.HelpID = value; }
        }

        public string HelpMsg
        {
            get { return WebControlFunctionality.HelpMsg; }
            set { WebControlFunctionality.HelpMsg = value; }
        }

        public bool DoNotValidateOnBlur
        {
            get { return WebControlFunctionality.DoNotValidateOnBlur; }
            set { WebControlFunctionality.DoNotValidateOnBlur = value; }
        }

        public string ClientName
        {
            get { return WebControlFunctionality.ClientName; }
        }

        public bool HasValidation
        {
            get { return WebControlFunctionality.HasValidation; }
            set { WebControlFunctionality.HasValidation = value; }
        }

        public FormFieldsValidationParams.FormFieldValidationSubGroupParams SubGroupParams { get { return WebControlFunctionality.SubGroupParams; } set { WebControlFunctionality.SubGroupParams = value; } }

        public string Title
        {
            get { return WebControlFunctionality.Title; }
            set { WebControlFunctionality.Title = value; }
        }

        public virtual object FormValueObject
        {
            get { return WebControlFunctionality.FormValue; }
        }

        public virtual int? GetFormValueAsIntNullable()
        {
            return WebControlFunctionality.FormValueIntNullable;
        }

        public virtual string GetFormValueAsStr()
        {
            return WebControlFunctionality.FormValue;
        }

        public bool Required
        {
            get { return WebControlFunctionality.Required; }
            set { WebControlFunctionality.Required = value; }
        }

        public string Class_Disabled { get { return WebControlFunctionality.Class_Disabled; } set { WebControlFunctionality.Class_Disabled = value; } }
        public string Class_onFocus { get { return WebControlFunctionality.Class_Focus; } set { WebControlFunctionality.Class_Focus = value; } }
        public string Class_onError { get { return WebControlFunctionality.Class_Error; } set { WebControlFunctionality.Class_Error = value; } }
        public string Class_onOver { get { return WebControlFunctionality.Class_Over; } set { WebControlFunctionality.Class_Over = value; } }
        public string Class_ReadOnly { get { return WebControlFunctionality.Class_ReadOnly; } set { WebControlFunctionality.Class_ReadOnly = value; } }
        public string Class_Required { get { return WebControlFunctionality.Class_Required; } set { WebControlFunctionality.Class_Required = value; } }

        //public object Value { get; set; }
        
        public bool UseDefaultCSSClasses
        {
            get { return WebControlFunctionality.UseDefaultCSSClasses; }
            set { WebControlFunctionality.UseDefaultCSSClasses = value; }
        }

        public Enums.DATA_TYPE DataType
        {
            get { return WebControlFunctionality.DataType; }
        }

        public void FocusGood()
        {
            WebControlFunctionality.FocusGood();
            
        }

        public new MyFormWebControlFunctionality WebControlFunctionality
        {
            get { return (MyFormWebControlFunctionality)base.WebControlFunctionality; }
        }

        object IMyFormWebControl.Value
        {
            get
            {
                return valueAsObject;
            }
            set
            {
                valueAsObject = value;
            }
        }

        protected abstract object valueAsObject { get; set; }
        #endregion

        #region IMyFormWebControl Members


        public FormFieldsValidationParams ValidationParams
        {
            get
            {
                return this.WebControlFunctionality.ValidationParams;
            }

            set { this.WebControlFunctionality.UpdateValidationParams(value); }

        }

        public FormFieldsCSSClasses FieldCssClasses
        {
            get
            {
                return this.WebControlFunctionality.FieldCssClasses;
            }
            set
            {
                this.WebControlFunctionality.FieldCssClasses = value;
            }
        }

        #endregion

        #region IBaseControl Members

        public new Control Control
        {
            get { return this; }
        }

        BaseControlFunctionality IBaseControl.WebControlFunctionality
        {
            get { return this.WebControlFunctionality; }
        }

        #endregion

        #region IMyBaseFormWebControl Members


        public string ValidationGroup
        {
            get
            {
                return this.ValidationParams.validationGroup;
            }
            set
            {
                this.ValidationParams.validationGroup = value;
            }
        }

        #endregion
    }
}
