﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI;
using CS.General_v3.Classes.CSS;
using CS.General_v3.JavaScript.jQuery.Plugins.Tooltips.QTip;
namespace CS.General_v3.Controls.WebControls
{
    public class BaseWebControlFunctionality : BaseControlFunctionality, IBaseWebControl
    {
        public int __GlobalCtrlIdentifier = Util.Other.GetNextControlGlobalIdentifier();

        private void init()
        {
            
            this.CssManager = new CSSManagerForControl(this.Control);
        }







        public BaseWebControlFunctionality(WebControl control)
            : base(control)
        {
            this.Control.PreRender += new EventHandler(Control_PreRender);
            init();
        }

        private void checkRemoveHtmlLiteral()
        {
            if (_InnerHtmlLiteral != null &&
                ((this.Control.Controls.Count == 1 && this.Control.Controls[0] != _InnerHtmlLiteral) ||
                (this.Control.Controls.Count > 1)))
            {
                this.Control.Controls.Remove(_InnerHtmlLiteral);
            }
        }

        private void Control_PreRender(object sender, EventArgs e)
        {
            checkRemoveHtmlLiteral();

        }

        //public WebControl Control { get; set; }

        public CSSManagerForControl CssManager { get; private set; }

        public string RollOverClass { get; set; }
        private string _CssClass = null;
        public string CssClass
        {
            get
            {
                return CssManager.ToString();

            }
            set
            {
                CssManager.Clear(false);
                CssManager.AddClass(false, value);
            }
        }

        private string _InnerHtml = "";
        private Literal _InnerHtmlLiteral = null;
        private void updateHtmlLiteral(string s)
        {

            if ((this.Control.Controls.Count > 1) ||
                (_InnerHtmlLiteral == null && this.Control.Controls.Count > 0) ||
                (_InnerHtmlLiteral != null && this.Control.Controls.Count == 1 && this.Control.Controls[0] != _InnerHtmlLiteral))
            {
                this.Control.Controls.Clear();
                //throw new InvalidOperationException("In order to set InnerHtml/InnerText, you must not have any controls added to this control");
            }
            if (_InnerHtmlLiteral == null)
            {
                _InnerHtmlLiteral = new Literal();
                this.Control.Controls.Add(_InnerHtmlLiteral);
            }

            _InnerHtmlLiteral.Text = s;


        }
        public string InnerHtml
        {
            get
            {
                return _InnerHtml;
            }
            set
            {
                _InnerHtml = value;

                updateHtmlLiteral(_InnerHtml);
            }
        }
        public string InnerText
        {
            get
            {
                return System.Web.HttpUtility.HtmlDecode(InnerHtml);
            }
            set
            {
                InnerHtml = System.Web.HttpUtility.HtmlEncode(value);
            }
        }



        #region IBaseWebControl Members


        public new BaseWebControlFunctionality WebControlFunctionality { get { return this; } }

        public string StringID { get; set; }

        public string OnClientBlur { get; set; }


        public string OnClientClick
        {
            get
            {
                return this.Control.Attributes["onclick"];
            }
            set
            {
                this.Control.Attributes["onclick"] = value;
            }
        }

        public string OnClientChange { get; set; }

        public string OnClientLoad { get; set; }
        #endregion

        #region IBaseWebControl Members


        public new WebControl Control
        {
            get { return (WebControl)base.Control; }
        }

        #endregion


        private jQueryQTip _tooltip;
        public jQueryQTip Tooltip
        {
            get { return _tooltip; }
            set
            {
                _tooltip = value;
                if (_tooltip.Control == null)
                {
                    _tooltip.Control = this._control;
                }
            }
        }
    }
}
