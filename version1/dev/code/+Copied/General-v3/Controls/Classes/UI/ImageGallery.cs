﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.Classes.UI
{
    

    public class ImageGallery
    {
        
        public ImageGallery()
        {

            
        }


        private string imageToJS(CS.General_v3.Classes.PhotoData photo, string varName)
        {
            string js = "";
            if (varName != null)
            {
                js += "var " + varName + " = ";
            }

            js += "new js.com.cs." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION + ".UI.Images.MediaItem('" + photo.Small + "','" + photo.Medium + "','" + photo.Large + "','" + photo.Caption + "')";
            return js;
        }

        public string InitGallery(List<CS.General_v3.Classes.PhotoData> images, string elemImageID, List<string>elemThumbnailsIDs, bool addToDOM, int startIndex, string elemCaptionID,
                           string CSS_ThumbnailUp, string CSS_ThumbnailOver, string CSS_ThumbnailSelected, bool attachCSSToParentElement, bool useDojoForDefaultAnimations, int defaultAnimationDuration, string varName, bool addToDojoOnLoad)
        {

            string js = "var " + varName + " = new js.com.cs." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION + ".UI.Images.MediaGallery();";

            List<string> imagesForJS = new List<string>();
            for (int i = 0; i < images.Count; i++) { imagesForJS.Add(imageToJS(images[i],null)); };
            string jsImages = CS.General_v3.Util.JSUtilOld.ConvertToArray(imagesForJS, false);
            string jsThumbs = CS.General_v3.Util.JSUtilOld.ConvertToArray(elemThumbnailsIDs);

            js += varName + ".attachCSSToParent = " + attachCSSToParentElement.ToString().ToLower() + ";";
            if (CSS_ThumbnailUp != null) 
                js += varName + ".CSS_ThumbnailUp = '"+CSS_ThumbnailUp+"';";
            if (CSS_ThumbnailOver != null) 
                js += varName + ".CSS_ThumbnailOver = '" + CSS_ThumbnailOver + "';";
            if (CSS_ThumbnailSelected != null)
                js += varName + ".CSS_ThumbnailSelected = '" + CSS_ThumbnailSelected + "';";
            js += varName + ".useDojo = " + useDojoForDefaultAnimations.ToString().ToLower() + ";";
            js += varName + ".defaultDuration = " + defaultAnimationDuration + ";";

            
            if (elemCaptionID != null)
            {
                elemCaptionID = "'" + elemCaptionID + "'";
            }
            else
            {
                elemCaptionID = "null";
            }
            js += varName + ".init(" + jsImages + ", '" + elemImageID + "', " + jsThumbs + ", " + addToDOM.ToString().ToLower() + ", " + startIndex + ", " + elemCaptionID + ");";
            if (addToDojoOnLoad)
            {
                //js = CS.General_v3.Util.JSUtil.Do2jo.AddOnLoad(js); not used anymore - Karl 26/01/11
            }
            js = CS.General_v3.Util.JSUtilOld.MakeJavaScript(js, true);

            return js;

           

        }
    }
}
