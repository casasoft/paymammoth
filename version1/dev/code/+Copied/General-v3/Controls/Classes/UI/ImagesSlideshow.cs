﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.Classes.UI
{
    public class ImagesSlideshow
    {
        /// <summary>
        /// Inteface to the com.cs.ui.images.Slideshow class.  It will create a slideshow of images which rotate one after the other.
        /// </summary>
        public ImagesSlideshow()
        {
        }
        /// <summary>
        /// Inteface to the com.cs.ui.images.Slideshow class.  It will create a slideshow of images which rotate one after the other.
        /// </summary>
        /// <param name="images">The list of images to show in the slideshow</param>
        /// <param name="elemImageID">The id of the img tag where to display the images</param>
        /// <param name="addToDOM">Wheter you want to add new images to the DOM</param>
        /// <param name="startIndex">The index from the array where you want to start showing</param>
        /// <param name="elemCaptionID">The ID of the caption element.  You can leave this to null if caption is not required</param>
        /// <param name="captions">The list of captions for the images.  The length of the array must be EQUAL to the length of the images
        /// if specified.  You can leave it to null if captions are not required.</param>
        /// <param name="onImageChangedFunctionName">The function name to be called when the onImageChanged event is triggered</param>
        /// <param name="delay">The delay between images in milliseconds</param>
        /// <param name="useDojoForDefaultAnimations">Whether to use dojo for default animations should the onImageChanged event be left null</param>
        /// <param name="defaultAnimationDuration">The duration for the default fading in/out</param>
        /// <param name="varName">The var name to allocate to the slideshow in JS</param>
        /// <param name="addToDojoOnLoad">Wheter to add the initialization to the dojo.a3ddOnLoad method</param>
        /// <returns></returns>
        public string ShowImages(List<string> images, string elemImageID, bool addToDOM, int startIndex, string elemCaptionID, List<string> captions, string onImageChangedFunctionName, int delay, bool useDojoForDefaultAnimations, int defaultAnimationDuration, string varName, bool addToDojoOnLoad)
        {
            string js = "var " + varName + " = new js.com.cs." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION + ".UI.Images.SlideShow();";
            string jsImages = CS.General_v3.Util.JSUtilOld.ConvertToArray(images);
            string jsCaptions = CS.General_v3.Util.JSUtilOld.ConvertToArray(captions);

            if (captions != null && captions.Count != images.Count)
            {
                throw new Exception("The length of the captions array must be equal to the length of the images array or else left null if no captions are required");
            }

            if (captions == null) jsCaptions = "null";
            if (elemCaptionID != null)
            {
                elemCaptionID = "'" + elemCaptionID + "'";
            }
            else
            {
                elemCaptionID = "null";
            }
            if (onImageChangedFunctionName != null)
            {
                js += varName + ".onImageChanged = " + onImageChangedFunctionName + ";";
            }
            js += varName + ".useDojo = " + useDojoForDefaultAnimations.ToString().ToLower() + ";";
            js += varName + ".defaultDuration = " + defaultAnimationDuration + ";";
            js += varName + ".delay = " + delay + ";";
            js += varName + ".showImages(" + jsImages + ", '" + elemImageID + "', " + addToDOM.ToString().ToLower() + ", " + startIndex + ", " + delay + ", " + elemCaptionID + ", " + jsCaptions + ");";

            if (addToDojoOnLoad)
            {
               // js = CS.General_v3.Util.JSUtil.Doj2o.AddOnLoad(js); //not used anymore (26/01/2011) Karl
            }
            return CS.General_v3.Util.Text.MakeJavaScript(js, true);
        }  

    }
}
