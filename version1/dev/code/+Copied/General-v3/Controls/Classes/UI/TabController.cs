﻿using System;
using System.Collections.Generic;

using System.Text;

namespace CS.General_v3.Controls.Classes.UI
{
    public class TabController
    {
        private List<Tab> _tabs;
        private bool _useDefaultCSS = true;

        private CS.General_v3.EnumValues.Easing _easingShow = CS.General_v3.EnumValues.Easing.CubicEaseOut;
        private CS.General_v3.EnumValues.Easing _easingHide = CS.General_v3.EnumValues.Easing.CubicEaseOut;
        private double _durationShow = 0.5;
        private double _durationHide = 0.5;

        private string _varName;

        /// <summary>
        /// JS function handler which is called whenever a tab change is made and will
        /// have the new tab id as parameter
        /// </summary>
        public string onChange { get; set; }
        /// <summary>
        /// JS function handler which is called whenever a tab content needs to be changed. 
        /// It will have as parameters: oldID, newID, oldContent and newContent
        /// </summary>
        public string onContentChanged { get; set; }
        /// <summary>
        /// JS function handler called whenever the mouse moves over a tab.
        /// Parameter: tab object (com.cs.ui.tabs.Tab)
        /// </summary>
        public string onMouseOverTab { get; set; }
        /// <summary>
        /// JS function handler called whenever the mouse moves out from a tab.
        /// Parameter: tab object (com.cs.ui.tabs.Tab)
        /// </summary>
        public string onMouseOutTab { get; set; }
        /// <summary>
        /// JS function handler called whenever a tab is unselected.
        /// Parameter: tab object (com.cs.ui.tabs.Tab)
        /// </summary>
        public string onUnSelectTab { get; set; }
        /// <summary>
        /// JS function handler called whenever a tab is selected.
        /// Parameter: tab object (com.cs.ui.tabs.Tab)
        /// </summary>
        public string onSelectTab { get; set; }

        /// <summary>
        /// This is used in conjunction with the com.cs.ui.tabs.Tabs class to facilitate instantiation.
        /// </summary>
        /// <param name="varName">Variable name to be used in javascript</param>
        public TabController(string varName)
        {
            this._varName = varName;
            this._tabs = new List<Tab>();



        }
        /// <summary>
        /// Produce the required javascript.
        /// </summary>
        /// <param name="initTabIndexOrID">The tab index to initialize.  Can be either number e.g. 4 or else id e.g. 'Test'</param>
        /// <param name="addToDojoOnLoad"></param>
        /// <returns></returns>
        public string GetJavascript(string initTabIndexOrID, bool addToDojoOnLoad)
        {
            string js = "var "+this._varName+" = new com.cs.ui.tabs.Tabs();";

            for (int i = 0;i<this._tabs.Count;i++) {
                Tab t = this._tabs[i];
                js += this._varName+".addTab(new com.cs.ui.tabs.Tab('"+t.TabElementID+"', '"+t.Id+"', '"+t.ContentElementID+"'));";
            }

            List<string> values = new List<string>();
            values.Add(this._useDefaultCSS.ToString().ToLower());
            values.Add(CS.General_v3.EnumValues.StringValues.EasingToString(this._easingShow));
            values.Add(CS.General_v3.EnumValues.StringValues.EasingToString(this._easingHide));
            values.Add(this._durationShow.ToString());
            values.Add(this._durationHide.ToString());
            values.Add(this.onChange);
            values.Add(this.onContentChanged);
            values.Add(this.onMouseOverTab);
            values.Add(this.onMouseOutTab);
            values.Add(this.onUnSelectTab);
            values.Add(this.onSelectTab);
            

            List<string> varNames = new List<string>();
            varNames.Add("useDefaultCSS");
            varNames.Add("easingShow");
            varNames.Add("easingHide");
            varNames.Add("durationShow");
            varNames.Add("durationHide");
            varNames.Add("onChange");
            varNames.Add("onContentChanged");
            varNames.Add("onMouseOverTab");
            varNames.Add("onMouseOutTab");
            varNames.Add("onUnSelectTab");
            varNames.Add("onSelectTab");

            for (int i = 0; i < values.Count; i++)
            {
                string value = values[i];
                if (value == null) continue;
                string varName = varNames[i];
                js += this._varName + "." + varName + " = " + value + ";";
            }

           /* if (initTabID != null)
            {
                initTabID = "'" + initTabID + "'";
            }
            else
            {
                initTabID = "";
            }*/
            string initFunction = this._varName + ".init("+initTabIndexOrID+");";
            if (addToDojoOnLoad)
            {
                //js += "dojo.addO3nLoad(function() {" + initFunction + " });"; 
                //commented by karl on 26/01/11
            }
            else
            {
                js += initFunction;
            }
            return CS.General_v3.Util.Text.MakeJavaScript(js, true);
        }
        /// <summary>
        /// Add a tab to the list of tab controller
        /// </summary>
        /// <param name="tab"></param>
        public void addTab(Tab tab)
        {
            this._tabs.Add(tab);
        }
        public void addTabs(List<Tab> tabs)
        {
            for (int i = 0; i < tabs.Count; i++)
            {
                addTab(tabs[i]);
            }
        }
        /// <summary>
        /// Whether to use the default CSS classes on rollover of tab.  By default, on rollover
        /// on Tab, the className is changed to 'over', 'up', and selected.
        /// </summary>
        public bool UseDefaultCSS
        {
            get { return this._useDefaultCSS; }
            set { this._useDefaultCSS = value; }
        }

        public CS.General_v3.EnumValues.Easing EasingShow
        {
            get { return this._easingShow; }
            set { this._easingShow = value; }
        }
        public CS.General_v3.EnumValues.Easing EasingHide
        {
            get { return this._easingHide; }
            set { this._easingHide = value; }
        }
            

        public double DurationShow
        {
            get { return _durationShow; }
            set { _durationShow = value; }
        }


        public double DurationHide
        {
            get { return _durationHide; }
            set { _durationHide = value; }
        }

        public List<Tab> Tabs { get { return this._tabs; } }

    }




    public class Tab
    {
        public string TabElementID { get; set; }
        public string Id { get; set; }
        public string ContentElementID { get; set; }

        /// <summary>
        /// Create a tab element which is used by the javascript tab controller
        /// </summary>
        /// <param name="tabElementID">The element of the tab item which is used to toggle between panels</param>
        /// <param name="id">ID to reference the tab section when clicked</param>
        /// <param name="contentElementID">The element ID of the content panel linked with this tab</param>
        public Tab(string tabElementID, string id, string contentElementID) {
            this.TabElementID = tabElementID;
            this.Id = id;
            this.ContentElementID = contentElementID;
        }

    }
}
