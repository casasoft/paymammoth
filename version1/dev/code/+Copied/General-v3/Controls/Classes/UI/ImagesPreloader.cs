﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.Classes.UI
{
    

    public class ImagesPreloader
    {
        
        
        public ImagesPreloader()
        {

        }

        /// <summary>
        /// This will interface with the 
        /// </summary>
        /// <param name="images">The list of images URLs to preload</param>
        /// <param name="imgTagID">The ID of the img tag</param>
        /// <param name="addToDOM">Wheter you want to add the new images to the DOM or not.  If not, the image URL is just replaced.</param>
        /// <param name="startIndex">The index to start preloading</param>
        /// <param name="onImageLoadedFunctionName">The name of the function to be called when an image is loaded.  The function must implement the following
        /// this.onImageLoaded = function(imageIndex, imageURL, success) {}</param>
        /// <param name="varName">The variable name of the images preloader</param>
        /// <param name="addToDojoOnLoad">Wheter to add the initialization of the images preloader to dojo.a3ddOnLoad</param>
        /// <returns>The javascript code required</returns>
        public string PreLoadImages(List<string> images, string imgTagID, bool addToDOM, int startIndex, string onImageLoadedFunctionName, string varName, bool addToDojoOnLoad)
        {
            string js = "var " + varName + " = new js.com.cs." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION + ".UI.Images.ImagesPreloader();";
            if (onImageLoadedFunctionName != null)
            {
                js += varName + ".onImageLoaded = " + onImageLoadedFunctionName + ";";
            }
            string imagesJSArray = CS.General_v3.Util.JSUtilOld.ConvertToArray(images);

            js += varName + ".preloadImages(" + imagesJSArray + ", '" + imgTagID + "', " + addToDOM.ToString().ToLower() + ", " + startIndex + ");";

            if (addToDojoOnLoad)
            {
               // js = CS.General_v3.Util.JSUtil.Do2jo.AddOnLoad(js); not used anymore dojo! 26/01/2011 - by karl
            }
            return CS.General_v3.Util.Text.MakeJavaScript(js, true);
            

        }
    }
}
