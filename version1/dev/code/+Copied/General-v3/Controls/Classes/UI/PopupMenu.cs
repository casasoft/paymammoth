﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI;

namespace CS.General_v3.Controls.Classes.UI
{
    
    public class PopUpMenu
    {
        public class PopUpMenuLevel
        {

            private List<PopUpMenuLevel> _children;
            /// <summary>
            /// 
            /// </summary>
            /// <param name="title"></param>
            /// <param name="URL">Leave null if it is not clickable</param>
            public PopUpMenuLevel(string title, string URL)
            {
                this.Title = title;
                this.URL = URL;
                this._children = new List<PopUpMenuLevel>();
            }
            public void AddLevel(PopUpMenuLevel level)
            {
                level.Parent = this;
                this._children.Add(level);
            }
            public List<PopUpMenuLevel> Children { get { return this._children; } }
            public string Title { get; set; }
            public string URL { get; set; }
            public PopUpMenuLevel Parent { get; set; }

            /// <summary>
            /// Returns the [div] control for this particular menu level.  It will also contain all its sub levels
            /// </summary>
            /// <returns></returns>
            public HtmlGenericControl GetDivControl()
            {
                

                HtmlGenericControl divTitle = new HtmlGenericControl("div");
                

                if (!String.IsNullOrEmpty(this.URL))
                {
                    HtmlAnchor aMain = new HtmlAnchor();
                    aMain.HRef = this.URL;
                    aMain.Title = aMain.InnerHtml = this.Title;
                    divTitle.Controls.Add(aMain);
                }
                else
                {
                    divTitle.InnerHtml = this.Title;
                }

                divTitle.Attributes["class"] = "item";

                
                if (this.Children.Count > 0)
                {
                    //Contains sub children
                    HtmlGenericControl divContainer = new HtmlGenericControl("div");
                    
                    divContainer.Controls.Add(divTitle);
                    HtmlGenericControl divSubItemsContainer = new HtmlGenericControl("div");
                    divSubItemsContainer.Attributes["class"] = "subitems_container";
                    divContainer.Controls.Add(divSubItemsContainer);
                    for (int i = 0; i < this.Children.Count; i++)
                    {
                        PopUpMenuLevel level = Children[i];
                        HtmlGenericControl div = level.GetDivControl();
                        divSubItemsContainer.Controls.Add(div);
                    }

                    return divContainer;
                }
                else
                {
                    return divTitle;
                }
            }
        }

        private List<PopUpMenuLevel> _levels;
        public PopUpMenu(int closeTimeout, bool openLeft)
        {
            this.CloseTimeout = closeTimeout;

            this.OpenLeft = openLeft;
            this._levels = new List<PopUpMenuLevel>();
        }

        public void AddLevel(PopUpMenuLevel level)
        {
            this._levels.Add(level);
        }
        /// <summary>
        /// Output the popup menu with its sub levels to the control you pass and it will initialize the required Javascript.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="clearControls"></param>
        public void OutputToControl(Control control, string jsVarName, bool clearControls, System.Web.UI.Page page)
        {
            if (clearControls) control.Controls.Clear();
            for (int i = 0; i < this._levels.Count; i++)
            {
                control.Controls.Add(this._levels[i].GetDivControl());
            }
            string js = "";
            //string js = "dojo.addO2nLoad(function() { ";
            //js += "var "+jsVarName+" = new com.cs.ui.PopUpMenu('"+control.ClientID+"', "+this.CloseTimeout+", "+this.OpenLeft.ToString().ToLower()+");";
            js += "var " + jsVarName + " = new js.com.cs." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION + ".UI.PopUpMenu.PopUpMenuClass('" + control.ClientID + "', " + this.CloseTimeout + ", " + this.OpenLeft.ToString().ToLower() + ");";
            //js += "} );";


            CS.General_v3.Util.JSUtilOld.AddJSScriptToPage(js);
            //page.ClientScript.RegisterStartupScript(page.GetType(), jsVarName, CS.General_v3.Util.JSUtil.MakeJavaScript(js,true));
                
            


        }
        public List<PopUpMenuLevel> Levels { get { return this._levels; } }
        /// <summary>
        /// The duration (in ms) to hide the menu when the mouse goes out of the menu
        /// </summary>
        /// 
        public int CloseTimeout { get; set; }
        /// <summary>
        /// Open left or right.
        /// </summary>
        public bool OpenLeft { get; set; }
    }
}
