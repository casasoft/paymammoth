﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.Classes.Forms
{
    public class LinkedMenuComboBox {
        public LinkedMenuComboBox(string id, string disabledText) {
            this.ID = id;
            this.DisabledText = disabledText;
        }
        public string ID { get; set; }
        public string DisabledText { get; set;}
    }

    public class LinkedMenuLevel {

        private List<LinkedMenuLevel> _children;
        public LinkedMenuLevel(string title, string value, bool selected) {
            this._children = new List<LinkedMenuLevel>();
            this.Title = title;
            this.Value = value;
            this.Selected = selected;
        }
        /// <summary>
        /// Add a sub level to this linked menu level.  
        /// </summary>
        /// <param name="subLevel"></param>
        public void AddSubLevel(LinkedMenuLevel subLevel) {
            this._children.Add(subLevel);
        }
        /// <summary>
        /// Gets the Javascript code to initialize this menu level and its sub children.
        /// </summary>
        /// <param name="varName">Variable name of this menu level</param>
        /// <returns></returns>
        public string GetJS(string varName)
        {
            string js = "";
            string title = CS.General_v3.Util.JSUtilOld.GetStringOrNull(this.Title);
            string value = CS.General_v3.Util.JSUtilOld.GetStringOrNull(this.Value);

            js = "var " + varName + " = new js.com.cs."+CS.General_v3.Settings.Others.JAVASCRIPT_VERSION+".Classes.FormsOld.linkedMenu.LinkedMenuLevel(" + title + ", " + value + ", " + this.Selected.ToString().ToLower() + ");";
            for (int i = 0; i < this._children.Count; i++)
            {
                string subLevelVarName = varName + "_"+ i;
                js += this._children[i].GetJS(subLevelVarName);
                js += varName + ".addSubLevel(" + subLevelVarName + ");";
            }
            

            return js;
        }

        public string Title { get; set; }
        public string Value { get; set; }
        public bool Selected { get; set; }
    }
    public class LinkedMenu
    {
        private List<LinkedMenuComboBox> _cmbs;
        private LinkedMenuLevel _root;

        /// <summary>
        /// Create an instance of js.com.cs."+CS.General_v3.Settings.Others.JAVASCRIPT_VERSION+".Classes.Forms.linkedMenu.LinkedMenuLevel and link several combo boxes together.
        /// </summary>
        public LinkedMenu()
        {
            this._cmbs = new List<LinkedMenuComboBox>();
            this._root = new LinkedMenuLevel(null, null, false);
            this.CSSDisabled = "disabled";
        }
        /// <summary>
        /// Add a combo box to the linked menu.  The combo boxes must be added in order of linking.  The first combo
        /// box is the first linkage, then second etc...
        /// 
        /// 
        /// </summary>
        /// <param name="id">ID of combobox in DOM</param>
        /// <param name="disabledText">The disabled text when previous item in linkage got no items</param>
        public void AddComboBox(string id, string disabledText)
        {
            this._cmbs.Add(new LinkedMenuComboBox(id, disabledText));
        }
        /// <summary>
        /// Create an instance of js.com.cs."+CS.General_v3.Settings.Others.JAVASCRIPT_VERSION+".Classes.Forms.linkedMenu.LinkedMenuLevel and link several combo boxes together, and outputs the javascript
        /// directly to the page
        /// </summary>
        /// <param name="pg">The page</param>
        /// <param name="rootLevel">A LinkedMenuLevel item which has got all the structure of the menu.  The root node is not used in itself,
        /// only the children is used, thus the title and value can be left null.</param>
        /// <param name="varName">The variable name for the linked menu in JS</param>
        /// <param name="addToDojoOnLoad">Wheter to add initialization to the d2ojo.addOnLoad() method</param>
        
        /// <returns></returns>
        public void OutputJS(string varName, bool addToJQueryOnLoad, System.Web.UI.Page page)
        {
            string js = GetJS(this._cmbs, this._root, varName, addToJQueryOnLoad);

            if (addToJQueryOnLoad)
            {
                CS.General_v3.Util.JSUtilOld.AddJSScriptToPage(js, deferJSThroughJQuery: addToJQueryOnLoad);
            }
        }
        
        /// <summary>
        /// Create an instance of js.com.cs."+CS.General_v3.Settings.Others.JAVASCRIPT_VERSION+".Classes.Forms.linkedMenu.LinkedMenuLevel and link several combo boxes together.
        /// </summary>
        /// <param name="comboBoxes">The list of combo boxes
        /// <param name="rootLevel">A LinkedMenuLevel item which has got all the structure of the menu.  The root node is not used in itself,
        /// only the children is used, thus the title and value can be left null.</param>
        /// <param name="varName">The variable name for the linked menu in JS</param>
        /// <param name="addToDojoOnLoad">Wheter to add initialization to the do2jo.addOnLoad() method</param>
        /// <returns></returns>
        private string GetJS(List<LinkedMenuComboBox> comboBoxes, LinkedMenuLevel rootLevel, string varName, bool addToDojoOnLoad)
        {
            
            string js = "";

            string cssDisabled = CS.General_v3.Util.JSUtilOld.GetStringOrNull(this.CSSDisabled);


            js += "var " + varName + " = new js.com.cs." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION + ".UI.FormsOld.LinkedMenu.LinkedMenuClass();";

            js += varName + ".CSSDisabled = " + cssDisabled + ";";
            for (int i = 0; i < comboBoxes.Count; i++)
            {

                string disabledText = CS.General_v3.Util.JSUtilOld.GetStringOrNull(comboBoxes[i].DisabledText);
                js += varName + ".addComboBox('" + comboBoxes[i].ID + "'," + disabledText + ");";
            }
            js += rootLevel.GetJS(varName + "_root");
            js += varName + ".init("+varName+"_root);";
            if (addToDojoOnLoad)
            {
                
               // js = CS.General_v3.Util.JSUtil.D2ojo.AddOnLoad(js); not used anymore (26/01/2011)
            }
            return js;


        }

        public string CSSDisabled { get; set; }
        public LinkedMenuLevel Root { get { return this._root; } }
    }
}
