﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common.General;
using CS.General_v3.Controls.WebControls;
using System.Web.UI;
namespace CS.General_v3.Controls.Classes.Forms
{
    public class FormFieldItem
    {
        public IMyFormWebControl Control { get; private set; }
        public Control GeneralControl { get; private set; }
        public HtmlTableRow Row { get; private set; }
        public Enums.DATA_TYPE DataType { get; private set; }

        

        public string ID { get; set; }
        /// <summary>
        /// The item in the list of form fields
        /// </summary>
        /// <param name="Row"></param>
        /// <param name="Control"></param>
        public FormFieldItem(string ID,HtmlTableRow Row, IMyFormWebControl Control, Enums.DATA_TYPE dataType)
        {
            init(ID, Row, dataType);
            
            this.Control = Control;
        }
        /// <summary>
        /// The item in the list of form fields
        /// </summary>
        /// <param name="Row"></param>
        /// <param name="Control"></param>
        public FormFieldItem(string ID, HtmlTableRow Row, Control Control)
        {
            init(ID, Row, null);
            this.GeneralControl = Control;
        }
        private void init(string id, HtmlTableRow Row, Enums.DATA_TYPE? dataType)
        {
            this.ID = id;
            this.Row = Row;
            if (dataType.HasValue)
                this.DataType = dataType.Value;
        }
        public virtual object Value
        {
            get
            {
                return this.Control.Value;
            }
            set
            {
                this.Control.Value = value;
                
            }
        }

        public virtual object FormValue
        {
            get
            {
                object o = this.Control.FormValueObject;
                return CS.General_v3.Util.Other.GetObjectFromValueByDataType(this.DataType, o);
            }
        }

        
    }
}
