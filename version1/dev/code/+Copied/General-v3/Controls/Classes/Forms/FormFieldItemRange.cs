﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common.General;
using CS.General_v3.Controls.WebControls;
namespace CS.General_v3.Controls.Classes.Forms
{
    public class FormFieldItemRange : FormFieldItem
    {
        

        public IMyFormWebControl ControlTo { get; set; }

        public FormFieldItemRange(string ID, HtmlTableRow Row, IMyFormWebControl Control, IMyFormWebControl ControlTo, Enums.DATA_TYPE dataType):
            base(ID, Row, Control,dataType)
        {
            this.ControlTo = ControlTo;
        }

        public override object FormValue
        {
            get
            {
                return base.Value;
            }
        }
        public object FormValueTo
        {
            get
            {
                return ControlTo.FormValueObject;
            }
        }

        public object ValueTo
        {
            get
            {
                return ControlTo.Value;
            }
            set
            {
                ControlTo.Value = value;
            }
        }
        /// <summary>
        /// Since this is an item range, it must have two values.. From and To
        /// </summary>
        public override object Value
        {
            get
            {
                return base.Value;
            }
            set
            {

                if (value is System.Collections.IList)
                {
                    System.Collections.IList list = (System.Collections.IList)value;
                    object objFrom = null;
                    object objTo = null;
                    if (list.Count > 1)
                    {
                        objTo = list[1];
                    }
                    if (list.Count > 0)
                    {
                        objFrom = list[0];
                    }

                    if (objFrom != null)
                    {
                        base.Value = objFrom;
                    }
                    if (objTo != null)
                    {
                        ControlTo.Value = objTo;
                    }
                }
                else
                {
                    base.Value = value;
                }
            }
        }
    }
}
