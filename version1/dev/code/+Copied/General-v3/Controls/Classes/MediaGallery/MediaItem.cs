﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.Classes.MediaGallery
{
    public class MediaItem : CS.General_v3.Classes.MediaItems.IMediaItem
    {
        #region IMediaItem Members

        public int FileSizeKB { get; set; }
        public int Priority
        {
            get;
            set;
        }

        public string ThumbnailImageURL
        {
            get;
            set;
        }

        public string LargeImageURL
        {
            get;
            set;
        }

        public string OriginalItemURL
        {
            get;
            set;
        }

        public string Caption
        {
            get;
            set;
        }
        public void Delete()
        {
            

        }

        public void SetMediaItem(CS.General_v3.Controls.WebControls.Common.MyFileUpload txt)
        {

        }

    


        public void Save()
        {
            
        }



        public string Extension
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

       


        public void SetMediaItem(System.IO.Stream txt, string filename)
        {
            throw new NotImplementedException();
        }
        
       

        public bool IsEmpty()
        {
            return String.IsNullOrEmpty(ThumbnailImageURL) || String.IsNullOrEmpty(LargeImageURL) || String.IsNullOrEmpty(OriginalItemURL);
        }

       


        public Enums.ITEM_TYPE ItemType {get;set;}
       

        public virtual void SetAsMainItem()
        {
            throw new NotImplementedException();
        }

      

        public string Filename
        {
            get { throw new NotImplementedException(); }
        }


        public string Identifier
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        #endregion

        #region IMediaItem Members


        
        #endregion
        public virtual string ExtraValueChoice { get; set; }



        public string GetThumbnailImageUrl()
        {
            return this.ThumbnailImageURL;
        }

        public string GetLargeImageUrl()
        {
            return this.LargeImageURL;
        }

        public string GetOriginalItemUrl()
        {
            return this.OriginalItemURL;
        }

        public int GetFileSizeInKB()
        {
            return this.FileSizeKB;
        }

        
    }
}
