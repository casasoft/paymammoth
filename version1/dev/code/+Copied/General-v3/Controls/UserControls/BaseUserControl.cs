﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.IO;
using CS.General_v3.Classes.Caching;
using System.Web.Caching;
using CS.General_v3.Controls.WebControls.Classes.VisibleController;

namespace CS.General_v3.Controls.UserControls
{
    
    public abstract class BaseUserControl : _tempZ_BaseUserControlWithCustomCaching
    {
        private VisibleController __visibleController;
        private VisibleController _visibleController
        {
            get
            {
                if (__visibleController == null)
                {
                    __visibleController = new VisibleController(this);
                }
                return __visibleController;
            }
        }

            public BaseUserControl()
        {
            this.CacheParams = createCacheParameters();

            
        }
        protected virtual CacheParameters createCacheParameters()
        {
            return new CacheParameters(this);
        }

        public class CacheParameters
        {
            protected BaseUserControl _userControl;
            private bool _cacheKeySet;
            public bool DoNotPrefixCacheKeyWithClientID { get; set; }
            public string CacheKey
            {
                get;
                set;
            }
            public List<string> CacheKeySuffices { get; private set; }
            public int ExpiryInMinutes { get; set; }

            public virtual bool? CacheControl { get; set; }

            public CacheDependency CacheDependency { get; set; }

            public CacheParameters(BaseUserControl userControl)
            {
                this.CacheKeySuffices = new List<string>();
                _userControl = userControl;
                this.ExpiryInMinutes = 60;
                this.CacheDependency = CustomCacheDependencyController.Instance.GetNewCacheDependency();
            }

        }
        protected virtual bool checkIfCacheable()
        {
            
            if (this.CacheParams.CacheControl.GetValueOrDefault() && !string.IsNullOrWhiteSpace(getCacheKey()))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public CacheParameters CacheParams { get; private set; }


       
        private string getCacheKey()
        {
            return this.CacheParams.CacheKey;
        }

        //{
        //    throw new InvalidOperationException("This must be overridden in a cached control");
        //}
        protected virtual string getFinalCacheKey()
        {
            if (this.CacheParams.CacheControl.GetValueOrDefault())
            {
                string key = "CustomUserControlCaching_" + getCacheKey();
                if (this.CacheParams.CacheKeySuffices.Count > 0)
                {
                    this.CacheParams.CacheKeySuffices.Sort();
                    
                    key += "-" + CS.General_v3.Util.ListUtil.JoinList(this.CacheParams.CacheKeySuffices, "-");
                }
                if (!this.CacheParams.DoNotPrefixCacheKeyWithClientID && !string.IsNullOrEmpty(this.ClientID))
                {
                    key = this.ClientID + "-" + key;
                }
                

                return key;
            }
            else
            {
                return null;
            }
        }

        private string cachedHtml = null;
        private bool doesExistInCache()
        {
            return !string.IsNullOrEmpty(cachedHtml);
        }
        private void checkAndLoadFromCache()
        {
            cachedHtml = null;
            if (checkIfCacheable())
            {
                string key = getFinalCacheKey();
                if (!string.IsNullOrWhiteSpace(key))
                {
                    cachedHtml = (string) Context.Cache.Get(key);

                    if (string.IsNullOrEmpty(cachedHtml))
                    {
                        cachedHtml = null;
                    }
                    else
                    {
                        string s = "";
                    }
                }

            }
        }

        protected new virtual void OnLoad(EventArgs e)
        {

        }
        protected new virtual void OnInit(EventArgs e)
        {

        }
        protected new virtual void OnPreRender(EventArgs e)
        {

        }
        protected override void onBasePreRender(EventArgs e)
        {

                if (checkIfCacheable() && !doesExistInCache())
                {
                    //Try loading it from cache
                    checkAndLoadFromCache();
                }
                if (!doesExistInCache())
                {
                    //If still doesn't exist, prerender it
                    OnPreRender(e);
                }
                else
                {
                    //If exists, clear it and then will update with cache
                    this.Controls.Clear();
                }
            base.onBasePreRender(e);
        }
        protected override void onBaseInit(EventArgs e)
        {
            OnInit(e);
            base.onBaseInit(e);
        }

        protected override void onBaseLoad(EventArgs e)
        {
                if (checkIfCacheable() && !doesExistInCache())
                {
                    //Try loading it from cache
                    checkAndLoadFromCache();
                }
                if (!doesExistInCache())
                {
                    //If still doesn't exist, prerender it
                    OnLoad(e);
                }
                else
                {
                    //If exists, clear it and then will update with cache
                    this.Controls.Clear();
                }

            base.onBaseLoad(e);
        }

        private void saveInCache(string html)
        {
            string key = getFinalCacheKey();
            if (!string.IsNullOrEmpty(key))
            {
                DateTime expiryDate = CS.General_v3.Util.Date.Now.AddMinutes(this.CacheParams.ExpiryInMinutes);

                string outputHtml = html;
                
                this.Cache.Remove(key);
                this.Cache.Add(key, outputHtml, this.CacheParams.CacheDependency, expiryDate, System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Normal,null);
                
            }

        }

        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            base.Render(writer);
            
        }
        protected override void RenderChildren(HtmlTextWriter writer)
        {
            base.RenderChildren(writer);
            
        }

        private bool savingInCache = false;

        public override void RenderControl(HtmlTextWriter writer)
        {
            StringBuilder sb = new StringBuilder();

            if (!checkIfCacheable() || !doesExistInCache())
            {
                //Either not cacheable, or else not in cache
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter cacheWriter = new HtmlTextWriter(sw);
                //Render the control to cacheWriter
                base.RenderControl(cacheWriter);
                
                if (checkIfCacheable())
                {
                    //Store it
                     saveInCache(sb.ToString());
                }

            }
            else
            {
                //Cached so just append the HTML
                sb.Append(cachedHtml);
            }
            //Render it
            writer.Write(sb);
        }

        public override bool Visible
        {
            get
            {
                return base.Visible;
            }
            set
            {
                base.Visible = value;
                _visibleController.SetVisible(value);
            }
        }
    }

    public abstract class _tempZ_BaseUserControlWithCustomCaching : UserControl
    {

        protected virtual void onBaseLoad(EventArgs e)
        {

        }
        protected virtual void onBaseInit(EventArgs e)
        {

        }
        protected virtual void onBasePreRender(EventArgs e)
        {

        }
        protected override void OnPreRender(EventArgs e)
        {
            onBasePreRender(e);
            base.OnPreRender(e);
        }
        protected override void OnInit(EventArgs e)
        {
            onBaseInit(e);
            base.OnInit(e);
        }
        protected override void OnLoad(EventArgs e)
        {
            onBaseLoad(e);
            base.OnLoad(e);
        }

    }
}
