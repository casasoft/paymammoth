﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CS.General_v3.Controls.DesignerEditors
{
    public partial class NameValueUIEditor : Form
    {
        public NameValueCollection _NV = null;
        
        public NameValueUIEditor(NameValueCollection nv)
            : this()
        {
            _NV = nv;
        }

        public NameValueUIEditor()
        {
            InitializeComponent();
        }
        private void showSelected()
        {
            if (lst.SelectedIndex > -1)
            {
                txtName.Text = _NV.Keys[lst.SelectedIndex];
                txtValue.Text = _NV[lst.SelectedIndex];
            }
        }

        private void fillList()
        {
            lst.Items.Clear();
            for (int i = 0; i < _NV.Count; i++)
            {
                string s = _NV.Keys[i] + " : " + _NV[i];
                lst.Items.Add(s);
            }

        }

        private void HashTableUIEditor_Load(object sender, EventArgs e)
        {
            fillList();

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            _NV.Add("[NAME]","[VALUE]");
            fillList();
            btnDelete.Enabled = true;
            lst.SelectedIndex = lst.Items.Count - 1;
            showSelected();
            txtName.Focus();
            txtName.SelectAll();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (lst.SelectedIndex > -1)
            {
                _NV.Remove(_NV.Keys[lst.SelectedIndex]);
                _NV.Add(txtName.Text, txtValue.Text);
                fillList();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lst.SelectedIndex > -1)
            {
                int i = lst.SelectedIndex;
                _NV.Remove(_NV.Keys[i]);
                fillList();
                if (i >= lst.Items.Count)
                    i = -1;
                lst.SelectedIndex = i;


            }
            btnDelete.Enabled = (lst.Items.Count > 0);
        }

        private void lst_SelectedIndexChanged(object sender, EventArgs e)
        {
            showSelected();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
