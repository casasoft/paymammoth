﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
namespace CS.General_v3.Controls.TypeConvertors
{
    class DoubleNullableConverter : TypeConverter
    {
        private object o;

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(double) || sourceType == typeof(string))
            {
                return true;
            }
            else
                return false;
        }
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(double) || destinationType == typeof(string))
            {
                return true;
            }
            else
                return false;
        }
        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            value = null;
            if (value is double)
                o = value;
            else if (value is string)
            {
                string s = (string)value;
                if (!string.IsNullOrEmpty(s))
                {
                    double d;
                    double.TryParse(s, out d);

                }
            }
            return o;
        }
        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(double))
            {
                double? d = null;
                if (value is double)
                    d = ((double)value);
                else if (value is string)
                {
                    string s = (string)value;
                    if (!string.IsNullOrEmpty(s))
                    {
                        double tmpD;
                        double.TryParse(s, out tmpD);
                    }
                }
                return d;
            }
            else if (destinationType == typeof(string))
            {
                string s = null;
                if (value is double)
                {
                    s = ((double)value).ToString("0.00");
                }
                else if (value is string)
                {
                    s  = (string)value;
                }
                return s;
            }
            return null;
        }
        public override bool IsValid(ITypeDescriptorContext context, object value)
        {
            bool ok = true;
            if (value is string)
            {
                string s = (string)value;
                if (!string.IsNullOrEmpty(s))
                {
                    double d;
                    if (!double.TryParse(s, out d))
                        ok = false;
                }

            }
            return ok;
        }
        
        
    }
}
