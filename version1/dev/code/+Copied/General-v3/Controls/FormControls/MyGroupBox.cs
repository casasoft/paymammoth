﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using System.CodeDom;

namespace CS.General_20090518.Controls.FormControls
{
    public class MyGroupBox : GroupBox
    {
        public void SetControlsEnabledState(bool b, params Control[] exceptionList)
        {
            bool[] bExceptionListValue = null;
            if (exceptionList != null)
            {
                bExceptionListValue = new bool[exceptionList.Length];
                for (int i = 0; i < bExceptionListValue.Length; i++)
                {
                    bExceptionListValue[i] = exceptionList[i].Enabled;
                }
            }
            CS.General_20090518.WindowsForms.ChangeControlEnabled(this, b);
            if (exceptionList != null)
            {
                for (int i = 0; i < bExceptionListValue.Length; i++)
                {
                    exceptionList[i].Enabled = bExceptionListValue[i];
                }
            }
        }
        public void DisableAllControls()
        {
            SetControlsEnabledState(false);
        }
        public void DisableAllControls(params Control[] exceptionList)
        {
            SetControlsEnabledState(false, exceptionList);
        }
        public void EnableAllControls()
        {
            SetControlsEnabledState(true);
        }
        public void EnableAllControls(params Control[] exceptionList)
        {
            SetControlsEnabledState(true, exceptionList);
        }


        public MyGroupBox()
        {
            
        }
        
    }
}
