﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CS.General_20090518.Controls.FormControls
{
    public partial class MyComboBox : ComboBox

    {
        #region Accept/Cancel Buttons
        private IButtonControl _LastAcceptButton = null;
        private IButtonControl _LastCancelButton = null;
        
        private IButtonControl _AcceptButton = null;
        private IButtonControl _CancelButton = null;

        [Description("The Accept button to select, when this field has focus")]
        public IButtonControl AcceptButton
        {
            get
            {
                return _AcceptButton;
            }
            set
            {
                _AcceptButton = value;
            }
        }
        [Description("The Cancel button to select, when this field has focus")]
        public IButtonControl CancelButton
        {
            get
            {
                return _CancelButton;
            }
            set
            {
                _CancelButton = value;
            }
        }
        private void CheckAcceptCancelGotFocus()
        {
            if (this.AcceptButton != null)
            {
                _LastAcceptButton = this.FindForm().AcceptButton;
                this.FindForm().AcceptButton = this.AcceptButton;
            }
            if (this.CancelButton != null)
            {
                _LastCancelButton = this.FindForm().CancelButton;
                this.FindForm().CancelButton = this.CancelButton;
            }
        }
        private void CheckAcceptCancelLostFocus()
        {
            if (this.AcceptButton != null)
            {
                this.FindForm().AcceptButton = _LastAcceptButton;
            }
            if (this.CancelButton != null)
            {
                this.FindForm().CancelButton = _LastCancelButton;
            }
        }
        #endregion
        private List<CS.General_20090518.Classes.ListItem> _ListItems = new List<CS.General_20090518.Classes.ListItem>();

        public List<CS.General_20090518.Classes.ListItem> GetListItems()
        {
            return _ListItems;
        }
        public void SetListItems(List<CS.General_20090518.Classes.ListItem> value)
        {
            _ListItems = value;
            if (_ListItems != null)
            {
                Items.Clear();
                for (int i = 0; i < _ListItems.Count; i++)
                {
                    Items.Add(_ListItems[i].Text);
                }
            }
        }

        public void AddListItem(string text, object value)
        {
            if (_ListItems == null)
                _ListItems = new List<CS.General_20090518.Classes.ListItem>();
            _ListItems.Add(new CS.General_20090518.Classes.ListItem(text, value));
            Items.Add(text);
        }
        public void RemoveListItem(string text, string value)
        {
            for (int i = 0; i < _ListItems.Count; i++)
            {
                if (_ListItems[i].Text == text)
                {
                    _ListItems.RemoveAt(i);
                    break;
                }
            }
            Items.Remove(text);
        }
        public void RemoveListItems()
        {
            _ListItems.Clear();
            Items.Clear();

        }

        private CS.General_20090518.Classes.Validation _Validation = new CS.General_20090518.Classes.Validation();
        #region Form Methods
        private string _OriginalText = null;
        private bool _NotPartOfForm = false;

        public bool NotPartOfForm
        {
            get { return _NotPartOfForm; }
            set { _NotPartOfForm = value; }
        }
        public string OriginalText
        {
            get { return _OriginalText; }
            set { _OriginalText = value; this.Text = value; _OriginalText = this.Text; }
        }
        public bool IsModified
        {
            get
            {
                if (NotPartOfForm)
                    return false;
                else
                {
                    return this.Text != OriginalText || OriginalText == null;
                }
            }
        }
        #endregion
        #region Validation Parameters
        private bool _HasError = false;
        [Category("Validation")]
        public bool HasError
        {
            get { return _HasError; }
            set { _HasError = value; }
        }
        [Category("Validation")]
        public bool HasValidation
        {
            get { return _Validation.HasValidation; }
            set { _Validation.HasValidation = value; }
        }
        [Category("Validation")]
        public bool Required
        {
            get { return _Validation.IsRequired; }
            set { _Validation.IsRequired = value; }
        }
        [Category("Validation")]
        public bool IsNumeric
        {
            get { return _Validation.IsNumeric; }
            set { _Validation.IsNumeric = value; }
        }
        [Category("Validation")]
        [DefaultValue("")]
        [Description("Whether the field should contain an email address")]
        public bool IsEmail
        {
            get { return _Validation.IsEmail; }
            set { _Validation.IsEmail = value; }
        }
        [Category("Validation")]
        public bool PositiveNumbersOnly
        {
            get { return _Validation.PositiveNumsOnly; }
            set { _Validation.PositiveNumsOnly = value; }
        }
        [Category("Validation")]
        public bool IntegersOnly
        {
            get { return _Validation.IntegersOnly; }
            set { _Validation.IntegersOnly = value; }
        }
        [Category("Validation")]
        public double NumberFrom
        {
            get { return _Validation.NumberFrom; }
            set { _Validation.NumberFrom = value; }
        }
        [Category("Validation")]
        public double NumberTo
        {
            get { return _Validation.NumberTo; }
            set { _Validation.NumberTo = value;  }
        }
        [Category("Validation")]
        public string Title
        {
            get { return _Validation.Title; }
            set { _Validation.Title = value; }
        }
        [Category("Validation")]
        public string ValidationGroup
        {
            get { return _Validation.ValidationGroup; }
            set { _Validation.ValidationGroup = value; }
        }
        [Category("Validation")]
        public string RequiredGroup
        {
            get { return _Validation.RequiredGroup; }
            set { _Validation.RequiredGroup = value; }
        }
        private string _InitText = "";
        [Category("Validation")]
        public string InitText
        {
            get
            {
                
                return _InitText;
            }
            set
            {
                _InitText = value;
                if (this.DropDownStyle != ComboBoxStyle.DropDownList && (this.Text == ""  || this.Text == null))
                    this.Text = _InitText;
            }
        }

        
        #endregion
        private bool _SelectAllOnFocus = true;
        public bool SelectAllOnFocus
        {
            get
            {
                return _SelectAllOnFocus;
            }
            set
            {
                _SelectAllOnFocus = value;
            }
        }
        public MyComboBox()
        {
            InitializeComponent();
            if (this.DropDownStyle != ComboBoxStyle.DropDownList)
            {
                LostFocus += new EventHandler(MyComboBox_LostFocus);
                GotFocus += new EventHandler(MyComboBox_GotFocus);
            }
        }
        public void Clear()
        {
            this.SelectedIndex = -1;
            this.Text = "";
            this.OriginalText = this.Text;
            
        }
        void MyComboBox_GotFocus(object sender, EventArgs e)
        {
            if (this.DropDownStyle != ComboBoxStyle.DropDownList && this.Text == this.InitText && this.Text != "" && this.Text != null)
            {
                this.Text = "";
            }
            if (this.DropDownStyle != ComboBoxStyle.DropDownList && _SelectAllOnFocus)
            {
                this.SelectAll();
            }
            CheckAcceptCancelGotFocus();
        }

        void MyComboBox_LostFocus(object sender, EventArgs e)
        {
            if (((this.Text == "") || (this.Text == null)) && this.InitText != "")
            {
                this.Text = this.InitText;
            }
            CheckAcceptCancelLostFocus();
        }
        /// <summary>
        /// This boolean is used to fix a call-stack error, due to the text set
        /// </summary>
        private bool _changedTextFromSelectListItems = false;
        public string Text
        {
            get
            {

                return base.Text;
            }
            set
            {
                if (_changedTextFromSelectListItems)
                    base.Text = value;
                else if (!selectFromListItems(value))
                    base.Text = value;
            }
        }

        public string Validate(ErrorProvider errProvider)
        {
            string result = "";
            if (this.Enabled && this.HasValidation)
            {
                this.HasError = !_Validation.Validate(this.FindForm(), this.FormValue, out result);
                if (this.HasError)
                {
                    errProvider.SetError(this, result);
                }
            }
            return result;
        }
        public string FormValue
        {
            get
            {
                if (this.Text == this.InitText && this.Text != "")
                    return "";
                else
                {
                    if (this._ListItems.Count > 0)
                    {
                        for (int i = 0; i < this._ListItems.Count; i++)
                        {
                            if (this._ListItems[i].Text == this.Text)
                                return this._ListItems[i].Value;
                        }
                        return "";
                    }
                    else
                        return this.Text;
                }
            }
        }
        public int FormValueInt
        {
            get
            {
                if (FormValue == "" || FormValue == null)
                    return Int32.MinValue;
                else
                    return Convert.ToInt32(FormValue);
            }
        }
        public int? FormValueIntNullable
        {
            get
            {
                if (this.FormValue == "" || this.FormValue == null)
                    return null;
                else
                    return Convert.ToInt32(FormValue);
            }
        }
        
        private bool selectFromListItems(object o)
        {
            List<CS.General_20090518.Classes.ListItem> list = GetListItems();
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].Value == o.ToString())
                    {
                        _changedTextFromSelectListItems = true;
                        this.Text = list[i].Text;
                        _changedTextFromSelectListItems = false;
                        return true;
                    }
                }
            }
            _changedTextFromSelectListItems = true;
            this.Text = "";
            _changedTextFromSelectListItems = false;
                        
            return false;
        }

        public void SetText(object o)
        {
            this.SelectedIndex = -1;

            if (o != null)
            {
                if (o is double)
                {
                    double d = (double)o;
                    if (d != double.MinValue)
                    {
                        if (!this.selectFromListItems(d))
                            this.Text = d.ToString("0.00");
                    }
                }
                else if (o is int)
                {
                    int i = (int)o;
                    if (i != Int32.MinValue)
                    {
                        if (!this.selectFromListItems(i))
                            this.Text = i.ToString();
                    }

                }
                else if (o is DateTime)
                {
                    DateTime d = (DateTime)o;
                    if (d != DateTime.MinValue)
                    {
                        if (!this.selectFromListItems(d))
                        {
                            this.OriginalText = d.ToString("dd/MM/yyyy");
                            if (this._Validation.HasTime)
                                this.Text += " " + d.ToString("HH:mm");
                        }
                    }
                }
                else if (o is string)
                {
                    string s = (string)o;
                    if (!this.selectFromListItems(s))
                    {
                        if (s != null)
                            this.Text = s;
                    }
                }
                else //is enum
                {
                    int i = (int)o;
                    if (i > 0)
                    {
                        if (!this.selectFromListItems(i))
                        {
                            this.Text = i.ToString();
                        }
                    }
                    else
                    {
                        this.Clear();
                    }
                }
            }
            
        }
        public void FillData(object o)
        {
            this.SelectedIndex = -1;
            SetText(o);
            this.OriginalText = this.Text;
        }
        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }
    }
}
