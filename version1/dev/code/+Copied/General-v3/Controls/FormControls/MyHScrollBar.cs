﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using System.CodeDom;

namespace CS.General_20090518.Controls.FormControls
{
    public class MyHScrollBar : HScrollBar
    {
        public new int Value
        {
            get
            {
                int value = base.Value;
                int newValue = Convert.ToInt32(((double)value / (double)(this.Maximum - LargeChange + 1)) * (double)this.Maximum);
                return newValue;
            }
            set
            {
                base.Value = value;
            }
        }
        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }
    }
}
