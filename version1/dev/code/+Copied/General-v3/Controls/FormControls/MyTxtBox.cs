﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using System.CodeDom;

namespace CS.General_20090518.Controls.FormControls
{
    public class MyTxtBox : TextBox
    {
        private IButtonControl _LastAcceptButton = null;
        private IButtonControl _LastCancelButton = null;

        private CS.General_20090518.Classes.Validation _Validation = new CS.General_20090518.Classes.Validation();
        #region Accept/Cancel Buttons
        private IButtonControl _AcceptButton = null;
        private IButtonControl _CancelButton = null;

        [Description("The Accept button to select, when this field has focus")]
        public IButtonControl AcceptButton
        {
            get
            {
                return _AcceptButton;
            }
            set
            {
                _AcceptButton = value;
            }
        }
        [Description("The Cancel button to select, when this field has focus")]
        public IButtonControl CancelButton
        {
            get
            {
                return _CancelButton;
            }
            set
            {
                _CancelButton = value;
            }
        }
        private void CheckAcceptCancelGotFocus()
        {
            if (this.AcceptButton != null)
            {
                _LastAcceptButton = this.FindForm().AcceptButton;
                this.FindForm().AcceptButton = this.AcceptButton;
            }
            if (this.CancelButton != null)
            {
                _LastCancelButton = this.FindForm().CancelButton;
                this.FindForm().CancelButton = this.CancelButton;
            }
        }
        private void CheckAcceptCancelLostFocus()
        {
            if (this.AcceptButton != null)
            {
                this.FindForm().AcceptButton = _LastAcceptButton;
            }
            if (this.CancelButton != null)
            {
                this.FindForm().CancelButton = _LastCancelButton;
            }
        }
#endregion
        #region Validation Parameters



        private bool _HasError = false;
        
        [Category("Validation")]
        [DefaultValue("")]
        [Description("Whether it has an error or not")]
        public bool HasError
        {
            get { return _HasError; }
            set { _HasError = value; }
        }


        [Category("Validation")]
        [DefaultValue("")]
        [Description("Whether it has validation or not")]
        public bool HasValidation
        {
            get { return _Validation.HasValidation; }
            set { _Validation.HasValidation = value; }
        }
        [Category("Validation")]
        public bool Required
        {
            get { return _Validation.IsRequired; }
            set { _Validation.IsRequired = value; }
        }
        [Category("Validation")]
        [DefaultValue("")]
        [Description("Whether it is required or not")]
        public bool IsNumeric
        {
            get { return _Validation.IsNumeric; }
            set { _Validation.IsNumeric = value; }
        }
        [Category("Validation")]
        [DefaultValue("")]
        [Description("Whether the field should contain an email address")]
        public bool IsEmail
        {
            get { return _Validation.IsEmail; }
            set { _Validation.IsEmail = value; }
        }
        [Category("Validation")]
        public bool PositiveNumbersOnly
        {
            get { return _Validation.PositiveNumsOnly; }
            set { _Validation.PositiveNumsOnly = value; }
        }
        [Category("Validation")]
        public bool IntegersOnly
        {
            get { return _Validation.IntegersOnly; }
            set { _Validation.IntegersOnly = value; }
        }
        [Category("Validation")]
        public double NumberFrom
        {
            get { return _Validation.NumberFrom; }
            set { _Validation.NumberFrom = value; }
        }
        [Category("Validation")]
        public double NumberTo
        {
            get { return _Validation.NumberTo; }
            set { _Validation.NumberTo = value; }
        }
        [Category("Validation")]
        public string Title
        {
            get { return _Validation.Title; }
            set { _Validation.Title = value; }
        }
        [Category("Validation")]
        public string ValueSameAsTxtID
        {
            get { return _Validation.ValueSameAsTxtID; }
            set { _Validation.ValueSameAsTxtID = value; }
        }
        [Category("Validation")]
        public string RequiredGroup
        {
            get { return _Validation.RequiredGroup; }
            set { _Validation.RequiredGroup = value; }
        }

        [Category("Validation")]
        [DefaultValue("main")]
        public string ValidationGroup
        {
            get { return _Validation.ValidationGroup; }
            set { _Validation.ValidationGroup = value; }
        }

        private string _InitText = "";
        [Category("Validation")]
        public string InitText
        {
            get
            {
                return _InitText;
            }
            set
            {
                _InitText = value;
                if (this.Text == "" || this.Text != null)
                    this.Text = _InitText;
            }
        }
        private bool _SelectAllOnFocus = true;
        [Category("Validation")]
        public bool SelectAllOnFocus
        {
            get
            {
                return _SelectAllOnFocus;
            }
            set
            {
                _SelectAllOnFocus = value;
            }
        }

        #endregion
        #region Form Methods
        private string _OriginalText = null;
        private bool _NotPartOfForm = false;
        
        public bool NotPartOfForm
        {
            get { return _NotPartOfForm; }
            set { _NotPartOfForm = value; }
        }
        public string OriginalText
        {
            get { return _OriginalText; }
            set { this.Text = value; _OriginalText = this.Text; }
        }
        public bool IsModified
        {
            get
            {
                if (NotPartOfForm || this.OriginalText == null)
                    return false;
                else
                    return this.Text != OriginalText;
            }
        }

        #endregion
        #region Form Getters
        public void GetFormValue(ref int o)
        {
            if (!Int32.TryParse(this.FormValue, out o))
                o = Int32.MinValue;
        }
        public void GetFormValue(ref DateTime o)
        {
            if (!DateTime.TryParse(this.FormValue, out o))
                o = DateTime.MinValue;
        }
        public void GetFormValue(ref double o)
        {
            if (!double.TryParse(this.FormValue, out o))
                o = double.MinValue;
        }
        public void GetFormValue(ref int? o)
        {
            int i; o = null;
            if (Int32.TryParse(this.FormValue,out i))
                o = i;
            
        }
        public void GetFormValue(ref double? o)
        {
            double tmp; o = null;
            if (Double.TryParse(this.FormValue, out tmp))
                o = tmp;

        }
        public void GetFormValue(ref DateTime? o)
        {
            DateTime tmp; o = null;
            if (DateTime.TryParse(this.FormValue, out tmp))
                o = tmp;

        }
        public void GetFormValue(ref string o)
        {
            o = this.FormValue;
        }
        #endregion

        public MyTxtBox()
        {
            LostFocus += new EventHandler(MyTxtBox_LostFocus);
            GotFocus += new EventHandler(MyTxtBox_GotFocus);
        }
        void MyTxtBox_GotFocus(object sender, EventArgs e)
        {
            if (this.Text == this.InitText && this.Text != "" && this.Text != null)
            {
                this.Text = "";
            }
            if (_SelectAllOnFocus)
            {
                this.SelectAll();
            }
            CheckAcceptCancelGotFocus();
        }
        
        void MyTxtBox_LostFocus(object sender, EventArgs e)
        {
            if (((this.Text == "") || (this.Text == null)) && this.InitText != "")
            {
                this.Text = this.InitText;
            }
            CheckAcceptCancelLostFocus();
        }

        public new void Clear()
        {
            this.OriginalText = "";
            this.Text = "";
            base.Clear();
        }
         
        public string Validate(ErrorProvider errProvider)
        {
            string result = "";
            if (this.Enabled && this.HasValidation)
            {
                this.HasError = !_Validation.Validate(this.FindForm(), this.FormValue, out result);
                if (this.HasError)
                {
                    errProvider.SetError(this,result);
                }
            }
            return result;
        }
        public string FormValue
        {
            get
            {
                if (this.Text == this.InitText && this.Text != "")
                    return "";
                else
                    return this.Text;
            }
        }
        public int FormValueInt
        {
            get
            {
                if (this.FormValue == "" || this.FormValue == null)
                    return Int32.MinValue;
                else
                    return CS.General_20090518.Conversion.ToInt32(FormValue);
            }
        }
        public int? FormValueIntNullable
        {
            get
            {
                if (this.FormValue == "" || this.FormValue == null)
                    return null;
                else
                    return CS.General_20090518.Conversion.ToInt32(FormValue);
            }
        }
        public double? FormValueDblNullable
        {
            get
            {
                if (this.FormValue == "" || this.FormValue == null)
                    return null;
                else
                    return CS.General_20090518.Conversion.ToDouble(FormValue);
            }
        }
        public double FormValueDbl
        {
            get
            {
                if (this.FormValue == "" || this.FormValue == null)
                    return double.MinValue;
                else
                    return CS.General_20090518.Conversion.ToDouble(FormValue);
            }
        }
        /// <summary>
        /// Sets the text to the object value
        /// </summary>
        /// <param name="o"></param>
        public void SetText(object o)
        {
            this.Text = "";
            if (o != null)
            {
                if (o is double)
                {
                    double d = (double)o;
                    if (d != double.MinValue)
                        this.Text  = d.ToString("0.00");
                }
                else if (o is int)
                {
                    int i = (int)o;
                    if (i != Int32.MinValue)
                        this.Text = i.ToString();

                }
                else if (o is DateTime)
                {
                    DateTime d = (DateTime)o;
                    if (d != DateTime.MinValue)
                    {

                        this.OriginalText = d.ToString("dd/MM/yyyy");
                        if (this._Validation.HasTime)
                            this.Text += " " + d.ToString("HH:mm");
                    }
                }
                else if (o is string)
                {
                    string s = (string)o;
                    if (s != null)
                        this.Text = s;
                }
            }
        }
        /// <summary>
        /// Fills the text with the object as parameter, and sets the original text also to it
        /// </summary>
        /// <param name="o"></param>
        public void FillData(object o)
        {
            SetText(o);
            this.OriginalText = this.Text;
            
        }
        
        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }
    }
}
