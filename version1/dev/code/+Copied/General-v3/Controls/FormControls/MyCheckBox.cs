﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CS.General_20090518.Controls.FormControls
{
    public partial class MyCheckBox : CheckBox
    {
        #region Accept/Cancel Buttons
        private IButtonControl _LastAcceptButton = null;
        private IButtonControl _LastCancelButton = null;
        private IButtonControl _AcceptButton = null;
        private IButtonControl _CancelButton = null;

        [Description("The Accept button to select, when this field has focus")]
        public IButtonControl AcceptButton
        {
            get
            {
                return _AcceptButton;
            }
            set
            {
                _AcceptButton = value;
            }
        }
        [Description("The Cancel button to select, when this field has focus")]
        public IButtonControl CancelButton
        {
            get
            {
                return _CancelButton;
            }
            set
            {
                _CancelButton = value;
            }
        }
        private void CheckAcceptCancelGotFocus()
        {
            if (this.AcceptButton != null)
            {
                _LastAcceptButton = this.FindForm().AcceptButton;
                this.FindForm().AcceptButton = this.AcceptButton;
            }
            if (this.CancelButton != null)
            {
                _LastCancelButton = this.FindForm().CancelButton;
                this.FindForm().CancelButton = this.CancelButton;
            }
        }
        private void CheckAcceptCancelLostFocus()
        {
            if (this.AcceptButton != null)
            {
                this.FindForm().AcceptButton = _LastAcceptButton;
            }
            if (this.CancelButton != null)
            {
                this.FindForm().CancelButton = _LastCancelButton;
            }
        }
        #endregion
        #region Form Methods
        private bool _OriginalValue = false;
        private bool _NotPartOfForm = false;




        public bool NotPartOfForm
        {
            get { return _NotPartOfForm; }
            set { _NotPartOfForm = value; }
        }
        public bool OriginalValue
        {
            get { return _OriginalValue; }
            set { _OriginalValue = value; this.Checked = value; }
        }
        public bool IsModified
        {
            get
            {
                if (NotPartOfForm)
                    return false;
                else
                    return this.Checked != OriginalValue;
            }
        }
        public void SetChecked(object o)
        {
            this.Checked = false;
            if (o != null)
            {
                if (o is bool)
                {
                    bool b = (bool)o;
                    this.Checked = b;
                }
                else if (o is CS.Types.MyBool)
                {
                    CS.Types.MyBool b = (CS.Types.MyBool)o;
                    this.Checked = b.IsSet && b.Value;
                }
            }

        }
        public void FillData(object o)
        {
            SetChecked(o);
            this.OriginalValue = this.Checked;
        }

        #endregion
        public void Clear()
        {
            this.OriginalValue = false;
            this.Checked = false;
            

        }
        public MyCheckBox()
        {
            InitializeComponent();
            
        }


        protected override void OnGotFocus(EventArgs e)
        {
            CheckAcceptCancelGotFocus();
            base.OnGotFocus(e);
        }
        protected override void OnLostFocus(EventArgs e)
        {
            CheckAcceptCancelLostFocus();
            base.OnLostFocus(e);
        }
        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }
    }
}
