﻿namespace CS.General_20090518.Controls.FormControls
{
    partial class MyFolderInput
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtInput = new CS.General_20090518.Controls.FormControls.MyTxtBox();
            this.btnInput = new CS.General_20090518.Controls.FormControls.MyButton();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.SuspendLayout();
            // 
            // txtInput
            // 
            this.txtInput.AcceptButton = null;
            this.txtInput.CancelButton = null;
            this.txtInput.HasError = false;
            this.txtInput.HasValidation = true;
            this.txtInput.InitText = "";
            this.txtInput.IntegersOnly = false;
            this.txtInput.IsEmail = false;
            this.txtInput.IsNumeric = false;
            this.txtInput.Location = new System.Drawing.Point(0, 0);
            this.txtInput.Name = "txtInput";
            this.txtInput.NotPartOfForm = false;
            this.txtInput.NumberFrom = -1.7976931348623157E+308;
            this.txtInput.NumberTo = -1.7976931348623157E+308;
            this.txtInput.OriginalText = "";
            this.txtInput.PositiveNumbersOnly = false;
            this.txtInput.Required = false;
            this.txtInput.RequiredGroup = "";
            this.txtInput.SelectAllOnFocus = true;
            this.txtInput.Size = new System.Drawing.Size(170, 20);
            this.txtInput.TabIndex = 0;
            this.txtInput.Title = "";
            this.txtInput.ValueSameAsTxtID = "";
            // 
            // btnInput
            // 
            this.btnInput.AdjustImageLocation = new System.Drawing.Point(0, 0);
            this.btnInput.BtnShape = CS.General_20090518.Controls.FormControls.emunType.BtnShape.Rectangle;
            this.btnInput.BtnStyle = CS.General_20090518.Controls.FormControls.emunType.XPStyle.Default;
            this.btnInput.Location = new System.Drawing.Point(167, 0);
            this.btnInput.Name = "btnInput";
            this.btnInput.Size = new System.Drawing.Size(21, 23);
            this.btnInput.TabIndex = 1;
            this.btnInput.Text = "...";
            this.btnInput.UseVisualStyleBackColor = true;
            this.btnInput.Click += new System.EventHandler(this.btnInput_Click);
            // 
            // MyFolderInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnInput);
            this.Controls.Add(this.txtInput);
            this.Name = "MyFolderInput";
            this.Size = new System.Drawing.Size(213, 39);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MyTxtBox txtInput;
        private MyButton btnInput;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
    }
}
