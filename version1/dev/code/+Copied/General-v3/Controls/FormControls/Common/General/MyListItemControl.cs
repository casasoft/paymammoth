﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CS.General_v3.Controls.FormControls.Common.General
{
    public class MyListItemControl : IMyListItemControl
    {
        public MyListItemControl(ListControl ctrl)
        {
            _control = ctrl;
        }
        private ListControl _control = null; 
        private List<CS.General_v3.Classes.ListItem> _ListItems = new List<CS.General_v3.Classes.ListItem>();

        public List<CS.General_v3.Classes.ListItem> GetListItems()
        {
            return _ListItems;
        }
        public void SetListItems(List<CS.General_v3.Classes.ListItem> value)
        {
            
            _ListItems = value;
            if (_ListItems != null)
            {
                if (_control is ListBox)
                {
                    ListBox listBox = (ListBox)_control;
                    string selText = listBox.Text;
                    listBox.Items.Clear();
                    for (int i = 0; i < _ListItems.Count; i++)
                    {
                        listBox.Items.Add(_ListItems[i].Text);
                    }
                    listBox.Text = selText;
                }
                else if (_control is ComboBox)
                {
                    
                    ComboBox cmb = (ComboBox)_control;
                    string selText = cmb.Text;
                    cmb.Items.Clear();
                    for (int i = 0; i < _ListItems.Count; i++)
                    {
                        cmb.Items.Add(_ListItems[i].Text);
                    }
                    cmb.Text = selText;
                    
                }
            }
        }

        public void AddListItem(string text, object value)
        {
            text = text ?? "";

            if (_ListItems == null)
                _ListItems = new List<CS.General_v3.Classes.ListItem>();
            _ListItems.Add(new CS.General_v3.Classes.ListItem(text, value));
            if (_control is ListBox)
            {
                ListBox ctrl = (ListBox)_control;
                ctrl.Items.Add(text);
            }
            else if (_control is ComboBox)
            {
                ComboBox ctrl = (ComboBox)_control;
                ctrl.Items.Add(text);
            }
             
        }
        public void RemoveListItem(string text, string value)
        {
            for (int i = 0; i < _ListItems.Count; i++)
            {
                if (_ListItems[i].Text == text)
                {
                    _ListItems.RemoveAt(i);
                    break;
                }
            }
            if (_control is ListBox)
            {
                ListBox ctrl = (ListBox)_control;
                ctrl.Items.Remove(text);
            }
            else if (_control is ComboBox)
            {
                ComboBox ctrl = (ComboBox)_control;
                ctrl.Items.Remove(text);
            } 
           
        }
        public void RemoveListItems()
        {
            if (_ListItems != null) 
                _ListItems.Clear();
            if (_control is ListBox)
            {
                ListBox ctrl = (ListBox)_control;
                ctrl.Items.Clear();
            }
            else if (_control is ComboBox)
            {
                ComboBox ctrl = (ComboBox)_control;
                ctrl.Items.Clear();
            } 
        }

        public CS.General_v3.Classes.ListItem GetListItemByText(string txt, out int index)
        {
            index = -1;
            CS.General_v3.Classes.ListItem listItem = null;
            if (this._ListItems != null)
            {
                for (int i = 0; i < this._ListItems.Count; i++)
                {
                    if (string.Compare(this._ListItems[i].Text, txt, false) == 0)
                    {
                        index = i;
                        listItem = this._ListItems[i];
                        break;
                    }
                }
            }
            return listItem;

        }
        public CS.General_v3.Classes.ListItem GetListItemByValue(string value, out int index)
        {
            index = -1;
            CS.General_v3.Classes.ListItem listItem = null;
            if (this._ListItems != null)
            {
                for (int i = 0; i < this._ListItems.Count; i++)
                {
                    if (string.Compare(this._ListItems[i].Value, value, false) == 0)
                    {
                        listItem = this._ListItems[i];
                        index = i;
                        break;
                    }
                }
            }
            return listItem;

        }
    }
}
