﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.FormControls.Common.General
{
    interface IMyListItemControl
    {

        List<CS.General_v3.Classes.ListItem> GetListItems();

        void SetListItems(List<CS.General_v3.Classes.ListItem> value);

        void AddListItem(string text, object value);
        void RemoveListItem(string text, string value);
        void RemoveListItems();
        CS.General_v3.Classes.ListItem GetListItemByText(string txt, out int  index);
        CS.General_v3.Classes.ListItem GetListItemByValue(string value, out int index);
        
        
    }
}
