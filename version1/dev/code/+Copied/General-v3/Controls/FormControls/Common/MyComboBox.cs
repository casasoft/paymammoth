﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CS.General_v3.Controls.FormControls.Common
{
    public partial class MyComboBox : ComboBox,General.IMyFormControlRequireable, General.IMyListItemControl

    {

        protected class MoreThanFirstCharSelector
        {
            private MyComboBox _cmb;
            private Timer timer;
            private string currSel;
            public MoreThanFirstCharSelector(MyComboBox cmb)
            {
                _cmb = cmb;
                timer = new Timer();
                timer.Interval = 1000;
                timer.Tick += new EventHandler(timer_Tick);
                cmb.KeyPress += new KeyPressEventHandler(cmb_KeyPress);
                
                currSel = "";
            }

            void cmb_KeyPress(object sender, KeyPressEventArgs e)
            {
                
                timer.Stop();
                if (_cmb.AllowSelectByMoreThanFirstCharacter && _cmb.DropDownStyle == ComboBoxStyle.DropDownList)
                {
                    e.Handled = true;
                    addKeyToSelection(e);
                    timer.Start();
                }
            }

            void timer_Tick(object sender, EventArgs e)
            {
                timer.Stop();
                currSel = "";
                lastSel = -1;

                
            }
            private void addKeyToSelection(KeyPressEventArgs e)
            {
                
                currSel += e.KeyChar;
                currSel = currSel.ToLower();
                selectCurrentItem(e);
            }
            private int lastSel = -1;
            private void selectCurrentItem(KeyPressEventArgs e)
            {
                int currIndex = -1;
                
                for (int i = 0; i < _cmb.Items.Count; i++)
                {
                    string item = _cmb.Items[i].ToString().ToLower();
                    if (item.StartsWith(currSel))
                    {
                        currIndex = i;
                        break;
                    }

                }
                
                if ((currIndex == -1 || (currIndex == lastSel)) && lastSel > -1 && currSel.Length == 2 && currSel[0] == currSel[1])
                {// same character typed, removed last char, move to next if exists
                    currSel = currSel.Substring(0, 1);

                    int nextIndex = lastSel + 1;
                    if (nextIndex < _cmb.Items.Count)
                    {
                        if (_cmb.Items[lastSel].ToString().ToLower().StartsWith(currSel) &&
                            _cmb.Items[nextIndex].ToString().ToLower().StartsWith(currSel))
                        {
                            currIndex = nextIndex;
                        }

                    }
                }

                if (currIndex > -1)
                {
                   // e.Handled = true;
                    _cmb.SelectedIndex = currIndex;
                    lastSel = currIndex;

                }

            }
            
        }
        
        private General.MyFormControlRequireable _myControl = null;
        private General.MyListItemControl _myListItemControl = null;

        /// <summary>
        /// Allows selection of items in list box by more than the first character. Default TRUE
        /// </summary>
        public bool AllowSelectByMoreThanFirstCharacter { get; set; }

        public bool IsFilled
        {
            get
            {
                
                if (!string.IsNullOrEmpty(FormValue))
                {
                    return true;
                }
                else
                {
                    return false;
                }
                
            }
        }
        public MyComboBox()
        {
            this.AllowSelectByMoreThanFirstCharacter = true;
            _myControl = new CS.General_v3.Controls.FormControls.Common.General.MyFormControlRequireable(this);
            _myListItemControl = new CS.General_v3.Controls.FormControls.Common.General.MyListItemControl(this);
            //this.DropDownStyle = ComboBoxStyle.DropDownList;
            this.SelectAllOnFocus = true;
            InitializeComponent();
            if (this.DropDownStyle != ComboBoxStyle.DropDownList)
            {
                LostFocus += new EventHandler(MyComboBox_LostFocus);
                GotFocus += new EventHandler(MyComboBox_GotFocus);
            }
            firstCharSelector = new MoreThanFirstCharSelector(this);
        }

        private MoreThanFirstCharSelector firstCharSelector = null;

        public new ComboBoxStyle DropDownStyle
        {
            get
            {
                return base.DropDownStyle;
            }
            set
            {
                if (this.Name.Contains("cmbDataFieldEnumName") || (string.Compare(this.Name,"cmbDataFieldEnumName",true) == 0))
                {
                    int k = 5;
                }
                base.DropDownStyle = value;
            }
        }
        public new string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                int tmpI;
                if (Int32.TryParse(value,out tmpI))
                {
                    SetValue(tmpI,false);
                }
                else
                {
                    SetValue(value,false);
                }
            }
        }
        public void Clear()
        {
            this.SelectedIndex = -1;
            this.Text = "";
            this.OriginalValue = "";
            
        }
        private string _InitValue = null;
        public string InitValue
        {
            get
            {
                return _InitValue;
            }
            set
            {
                _InitValue = value;
                if (string.IsNullOrEmpty(this.Text))
                    this.Text = _InitValue;
            }
        }
        void MyComboBox_GotFocus(object sender, EventArgs e)
        {
            if (this.DropDownStyle != ComboBoxStyle.DropDownList && this.Text == this.InitValue && this.Text != "" && this.Text != null)
            {
                this.Text = "";
            }
            if (this.DropDownStyle != ComboBoxStyle.DropDownList && SelectAllOnFocus)
            {
                this.SelectAll();
            }
            
        }

        void MyComboBox_LostFocus(object sender, EventArgs e)
        {
            if (this.DropDownStyle != ComboBoxStyle.DropDownList && ((this.Text == "") || (this.Text == null)) && this.InitValue != "")
            {
                this.Text = this.InitValue;
            }
            
        }
        public bool SelectAllOnFocus { get; set; }
        #region IMyFormControl Members

        public IButtonControl AcceptButton
        {
            get
            {
                return _myControl.AcceptButton;
            }
            set
            {
                _myControl.AcceptButton = value;
            }
        }

        public IButtonControl CancelButton
        {
            get
            {
                return _myControl.CancelButton;
            }
            set
            {
                _myControl.CancelButton = value;
            }
        }

        public bool NotPartOfForm
        {
            get
            {
                return _myControl.NotPartOfForm;
            }
            set
            {
                _myControl.NotPartOfForm = value;
            }
        }

        public bool IsModified
        {
            get 
            {
                
                string s = this.FormValue;
                string origValue = (string)this.OriginalValue ?? "";

                return (string.Compare(origValue, s, true) != 0);
                
            }
        }

        [Browsable(false)]
        public object FormValueObject
        {
            get 
            {
                return FormValue;
            }
        }

        public string Validate(ErrorProvider errProvider)
        {
            return _myControl.Validate(errProvider);
        }

        public bool HasValidation
        {
            get
            {
                return _myControl.HasValidation;
            }
            set
            {
                _myControl.HasValidation = value;
            }
        }

        public string ValidationGroup
        {
            get
            {
                return _myControl.ValidationGroup;
            }
            set
            {
                _myControl.ValidationGroup = value;
            }
        }

        public bool HasError
        {
            get
            {
                return _myControl.HasError;
            }
            set
            {
                _myControl.HasError = value;
            }
        }


        public string Title
        {
            get
            {
                return _myControl.Title;
            }
            set
            {
                _myControl.Title = value;
            }
        }

        #endregion

    
        #region IMyListItemControl Members

            public List<CS.General_v3.Classes.ListItem>  GetListItems()
            {
 	            return _myListItemControl.GetListItems();
            }

            public void  SetListItems(List<CS.General_v3.Classes.ListItem> value)
            {
 	            _myListItemControl.SetListItems(value);
            }
            public void AddListItem(CS.General_v3.Classes.ListItem listItem)
            {
                this.AddListItem(listItem.Text, listItem.Value);
            }
            public void  AddListItem(string text, object value)
            {
 	            _myListItemControl.AddListItem(text,value);
            }

            public void  RemoveListItem(string text, string value)
            {
 	            _myListItemControl.RemoveListItem(text,value);
            }

            public void  RemoveListItems()
            {
 	            _myListItemControl.RemoveListItems();
                this.Items.Clear();
            }

            public CS.General_v3.Classes.ListItem GetListItemByText(string txt, out int index)
            {
                return _myListItemControl.GetListItemByText(txt, out index);
            }
            public CS.General_v3.Classes.ListItem GetListItemByValue(string txt, out int index)
            {
                return _myListItemControl.GetListItemByValue(txt, out index);
            }
#endregion


        private string _OriginalValue = null;
        public string OriginalValue
        {
            get { return _OriginalValue; }
            set 
            {
                _OriginalValue = value; 
                SetValue(_OriginalValue);
            }
        }
        [Browsable(false)]
        public object OriginalValueObject
        {
            get { return OriginalValue; }
            set
            {
                if (value != null)
                {
                    SetValue(value,true);
                    
                }
                
            }
        }
        public void SetOriginalValues(string s)
        {
            SetValue(s, true);
        }
        
        public void SetOriginalValues(int i)
        {
            SetValue(i, true);
        }
        

        public void SetValue(object o)
        {
            SetValue(o, true);
        }
        private string selectListItem(int id)
        {
            bool ok = false;
            int index;
            CS.General_v3.Classes.ListItem listItem = this.GetListItemByValue(id.ToString(), out index);
            if (listItem == null)
            {
                return selectListItem(id.ToString());
            }
            else
            {
                return selectListItem(listItem.Text);
            }
        }
        private string selectListItem(string txt)
        {
            int index = -1;
            var li = this._myListItemControl.GetListItemByValue(txt, out index);
            if (li == null)
                li = this._myListItemControl.GetListItemByText(txt, out index);
            if (li == null)
            {
                for (int i = 0; i < this.Items.Count; i++)
                {

                    string s = this.Items[i].ToString();
                    if (string.Compare(s, txt, true) == 0)
                    {
                        this.SelectedIndex = i;
                        return txt;
                        break;
                    }
                }
            }
            else
            {
                this.SelectedIndex = index;
                return li.Value;
            }
            this.SelectedIndex = -1;
            return null;
        }
        public void SetValue(object o, bool SetAsOriginalValue)
        {
            int selIndex = -1;
            //this.SelectedIndex = -1;
            string selValue = null;
            if (o is Enum)
                o = (int)o;
            if (o is string)
            {
                selValue = selectListItem((string)o);
            }
            else if (o is int)
            {
                selValue = selectListItem((int)o);
            }

            if (this.DropDownStyle != ComboBoxStyle.DropDownList && selValue == null && o != null)
                selValue = o.ToString();
            if (SetAsOriginalValue)
            {
                this._OriginalValue = selValue;

            }
            else
            {
                
                base.Text = selValue;
                
            }
        }


        [Browsable(false)]
        public string FormValue
        {
            get
            {
                int index;
                CS.General_v3.Classes.ListItem l = GetListItemByText(this.Text, out index);
                if (l == null)
                {
                    l = new CS.General_v3.Classes.ListItem(this.Text, this.Text);
                }
                return l.Value;
            }
        }
        public int? FormValueAsInt
        {
            get
            {
                int? integer = null;
                string s = this.FormValue;
                if (!string.IsNullOrEmpty(FormValue))
                {
                    integer = Util.Conversion.ToInt32(s);
                }
                return integer;
            }
        }
        #region IMyFormControlRequireable Members

        public bool Required
        {
            get
            {
                return _myControl.Required;
            }
            set
            {
                _myControl.Required = value;
            }
        }

        public string RequiredGroup
        {
            get
            {
                return _myControl.RequiredGroup;
            }
            set
            {
                _myControl.RequiredGroup = value;
            }
        }

        #endregion
        public string GetFormValueAsString()
        {
            string s = "";
            return FormValue;
        }


    }
}
