﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace CS.General_v3.Controls.FormControls.Common.General
{
    public class MyFormControl : MyControl, IMyFormControl
    {
        private void initHandlers()
        {
            _control.GotFocus += new EventHandler(_control_GotFocus);
            _control.LostFocus += new EventHandler(_control_LostFocus);
            var form = _control.FindForm();
            if (form != null)
            {
                form.Load += new EventHandler(form_Activated);
                
            }
            
        }

        void form_Activated(object sender, EventArgs e)
        {
            if (!formLoaded)
            {
                formLoaded = true;
                updateTooltipText();
            }
            
        }
        private System.Windows.Forms.ToolTip _tooltip = null;
        private void createTooltip()
        {
            _tooltip = new ToolTip();
            var t = _tooltip;
            t.InitialDelay = 400;
            t.IsBalloon = true;
            t.ReshowDelay = 300;
            t.AutoPopDelay = 15000;
            t.AutomaticDelay = 300;
            //t.AutoPopDelay = 100;
            t.Active = true;
            t.UseAnimation = true;
            t.UseFading = true;

        }
        private void updateTooltipText()
        {
            if (formLoaded)
            {
                if (_tooltip != null)
                {
                    _tooltip.RemoveAll();
                }
                if (!string.IsNullOrEmpty(this.TooltipText))
                {
                    if (_tooltip == null)
                    {
                        createTooltip();
                    }
                    _tooltip.SetToolTip(_control, this.TooltipText);
                }
            }
        }
        private bool formLoaded = false;
        

        void _control_LostFocus(object sender, EventArgs e)
        {
            CheckAcceptCancelLostFocus();
        }

        void _control_GotFocus(object sender, EventArgs e)
        {
            CheckAcceptCancelGotFocus();
        }
        public MyFormControl(Control ctrl)
            : base(ctrl)
        {
            Validation.Control = ctrl;
            initHandlers();
        }
        public CS.General_v3.Classes.Validation Validation = new CS.General_v3.Classes.Validation();
        
        private IButtonControl _LastAcceptButton = null;
        private IButtonControl _LastCancelButton = null;

        private void CheckAcceptCancelGotFocus()
        {
            
            if (this.AcceptButton != null)
            {
                _LastAcceptButton = _control.FindForm().AcceptButton;
                _control.FindForm().AcceptButton = this.AcceptButton;
            }
            if (this.CancelButton != null)
            {
                _LastCancelButton = _control.FindForm().CancelButton;
                _control.FindForm().CancelButton = this.CancelButton;
            }
        }
        private void CheckAcceptCancelLostFocus()
        {
            if (this.AcceptButton != null)
            {
                _control.FindForm().AcceptButton = _LastAcceptButton;
            }
            if (this.CancelButton != null)
            {
                _control.FindForm().CancelButton = _LastCancelButton;
            }
        }

        private string _TooltipText = null;

        public string TooltipText
        {
            get { return _TooltipText; }
            set 
            {
                _TooltipText = value;
                updateTooltipText();
            }
        }
        

        #region Validation Params
        public bool HasValidation
        {
            get
            {
                return Validation.HasValidation;
            }
            set
            {
                Validation.HasValidation = value;
            }


        }
        public bool HasError
        {
            get
            {
                return Validation.HasError;

            }
            set
            {
                Validation.HasError = value;
            }
        }
        public string Title 
        {
            get { return Validation.Title;}
            set { Validation.Title = value; }

        }

        public string ValidationGroup
        {
            get { return Validation.ValidationGroup; }
            set { Validation.ValidationGroup = value; }
        }
        #endregion

        #region IMyFormControl Members

        public IButtonControl AcceptButton {get;set;}
        public IButtonControl CancelButton {get;set;}

        public bool NotPartOfForm {get;set;}

        public bool IsModified 
        {
            get 
            {
                
                throw new NotImplementedException(); 
            }
        }

        public object FormValueObject
        {
            get 
            {
                throw new NotImplementedException();
            }
        }

        public virtual void Clear()
        {
            throw new NotImplementedException();
        }

        public string Validate(ErrorProvider errProvider)
        {
            string result = "";
            if (_control.Enabled && this.HasValidation)
            {
                General.IMyFormControl formControl = (General.IMyFormControl)_control;
                this.HasError = !Validation.Validate(_control.FindForm(), formControl.GetFormValueAsString(), out result);
                if (this.HasError && errProvider != null)
                {
                    errProvider.SetError(_control, result);
                }
            }
            return result;
        }

        public virtual void SetValue(object o)
        {
            throw new NotImplementedException();
        }

        public virtual void SetValue(object o, bool SetAsOriginalValue)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IMyFormControl Members


        public object OriginalValueObject {get;set;}

        public bool IsFilled
        {
            get
            {
                throw new NotImplementedException();
            }
        }
        public string GetFormValueAsString()
        {
            return FormValueObject.ToString();
        }
        #endregion
    }
}
