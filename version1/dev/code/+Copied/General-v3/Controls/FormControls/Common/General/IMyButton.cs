﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.FormControls.Common.General
{
    interface IMyButton : IMyControl
    {
        void PerformClick();
        string ValidationGroup { get; set; }
        bool DoValidation { get; set; }
        event EventHandler ClickWithValidation;
    }
}
