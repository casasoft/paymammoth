﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CS.General_v3.Controls.FormControls.Common
{
    public partial class MyLabel : Label, General.IMyControl
    {
        protected General.MyControl _myControl = null;

        public MyLabel()
        {
            _myControl = new CS.General_v3.Controls.FormControls.Common.General.MyControl(this);
            InitializeComponent();
            Click += new EventHandler(MyLabel_Click);
            
        }
        private Control _CtrlTabIndex = null;
        private Control findControlOfNextTabIndex()
        {
            long leastTabIndex = long.MaxValue;
            long thisTabIndex = CS.General_v3.Util.WindowsForms.CalculateRelativeTabIndex(this);
            Control leastControl = null;

            List<Control> list = new List<Control>();
            list.Add(this.FindForm());
            int i = 0;
            while (i < list.Count)
            {
                long currTabIndex = CS.General_v3.Util.WindowsForms.CalculateRelativeTabIndex(list[i]);
                if (currTabIndex > thisTabIndex && currTabIndex < leastTabIndex)
                {
                    leastTabIndex = currTabIndex;
                    leastControl = list[i];
                }
                for (int j = 0; j < list[i].Controls.Count; j++)
                {
                    list.Add(list[i].Controls[j]);
                }
                i++;
            }
            return leastControl;

        }
       
        void MyLabel_Click(object sender, EventArgs e)
        {
            
            if (_CtrlTabIndex == null)
                _CtrlTabIndex = findControlOfNextTabIndex();
            if (_CtrlTabIndex != null)
            {
                _CtrlTabIndex.Select();
            }
            //SelectNextControl(this, true, true, true, false);
            
        }

    }
}
