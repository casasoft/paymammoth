﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Controls.FormControls.Common.General
{
    interface IMyTxtBox : IMyFormControlRequireable
    {
        
        bool SelectAllOnFocus { get; set; }
    }
}
