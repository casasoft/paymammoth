﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using System.CodeDom;

namespace CS.General_20090518.Controls.FormControls.Common
{
    public class MyTxtBoxNumericDouble : MyTxtBoxNumeric
    {
        public MyTxtBoxNumericDouble()
            : base()
        {
            
        }
        
        #region Validation Parameters
        
        private double? _InitValue = null;
        [Category("Validation")]
        public new double? InitValue
        {
            get
            {
                return _InitValue;
            }
            set
            {
                _InitValue = value;
                if (string.IsNullOrEmpty(this.Text))
                {
                    if (_InitValue != null)
                        this.Text = _InitValue.Value.ToString("0.00");
                    else
                        this.Text = "";
                }
            }
        }
        
        

        #endregion
        
        
        
        
    }
}
