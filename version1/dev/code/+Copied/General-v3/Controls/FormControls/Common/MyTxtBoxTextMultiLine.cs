﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using System.CodeDom;

namespace CS.General_20090518.Controls.FormControls.Common
{
    public class MyTxtBoxTextMultiLine : MyTxtBoxText
    {
        public new bool Multiline
        {
            get
            {
                return true;
            }
            
        }
        
        public MyTxtBoxTextMultiLine() : base()
        {
            base.Multiline = true;
            _myControl.Validation.IsEmail = true;
        }
        
      
        
    }
}
