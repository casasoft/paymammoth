﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace CS.General_v3.Controls.FormControls.Common.General
{
    public class MyFormControlRequireable : MyFormControl, IMyFormControlRequireable
    {
        public MyFormControlRequireable(Control ctrl)
            : base(ctrl)
        {

        }
        #region IMyFormControlRequireable Members

        public bool Required
        {
            get
            {
                return Validation.IsRequired;
            }
            set
            {
                Validation.IsRequired = value;
            }
        }

        public string RequiredGroup
        {
            get
            {
                return Validation.RequiredGroup;
            }
            set
            {
                Validation.RequiredGroup = value;
            }
        }

        #endregion
    }
}
