﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace CS.General_v3.Controls.FormControls.Common.General
{
    public class MyMaskedTxtBox : MyFormControlRequireable
    {
        public CS.General_v3.Controls.FormControls.Common.MyMaskedTextBox controlTxt
        {
            get
            {
                
                return (CS.General_v3.Controls.FormControls.Common.MyMaskedTextBox)_control;

            }
        }
        public MyMaskedTxtBox(Control ctrl)
            : base(ctrl)
        {
            
            ctrl.GotFocus += new EventHandler(MyTxtBox_GotFocus);
            ctrl.LostFocus += new EventHandler(MyTxtBox_LostFocus);
        }
        private object __OriginalValue = null;
        private object _OriginalValue
        {
            get
            {
                return __OriginalValue;
            }
            set
            {
                __OriginalValue = value;
                
            }
        }
        public object OriginalValueObject
        {
            get
            {
                return _OriginalValue;
            }
            set
            {
                OriginalValue = (string)value;

            }
        }
        public object OriginalValue
        {
            get
            {
                return _OriginalValue;
            }
            set
            {
                //_control.Text = value;
                _OriginalValue = value;
            }
        }
        public virtual object FormValueObject
        {
            get
            {
                if (_control.Text == this.InitValue && _control.Text != "")
                    return "";
                else
                    return _control.Text;
            }
        }
        public string FormValue
        {
            get
            {
                return (string)FormValueObject;
            }
        }

        private string _InitValue = "";
        public string InitValue
        {
            get
            {
                return _InitValue;
            }
            set
            {
                _InitValue = value;
                if (_control.Text == "" || _control.Text != null)
                    _control.Text = _InitValue;
            }
        }
        void MyTxtBox_GotFocus(object sender, EventArgs e)
        {
            if (_control.Text == this.InitValue && !string.IsNullOrEmpty(_control.Text))
            {
                _control.Text = "";
            }
            if (_SelectAllOnFocus)
            {
                controlTxt.SelectAll();
            }
            
        }
        void MyTxtBox_LostFocus(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(_control.Text) && !string.IsNullOrEmpty(this.InitValue))
            {
                _control.Text = this.InitValue;
            }
            
        }

        
        /// <summary>
        /// Sets the text to the object value
        /// </summary>
        /// <param name="o"></param>
        public void SetValue(object o)
        {
            SetValue(o, true);
        }
        /// <summary>
        /// Sets the text to the object value
        /// </summary>
        /// <param name="o"></param>
        public void SetValue(object o, bool setAsOriginalValue)
        {
            string s = "";
            if (o != null)
            {
                if (o is double)
                {
                    double d = (double)o;
                    if (d != double.MinValue)
                        s = d.ToString("0.00");
                }
                else if (o is int)
                {
                    int i = (int)o;
                    if (i != Int32.MinValue)
                        s = i.ToString();

                }
                else if (o is DateTime)
                {
                    DateTime d = (DateTime)o;
                    if (d != DateTime.MinValue)
                    {
                        s = d.ToString("dd/MM/yyyy HH:mm");
                        //if (this.Validation.TimeRequired )
                          //  s += " " + d.ToString("HH:mm");                    
                    }
                }
                else if (o is string)
                {
                    string str = (string)o;
                    if (str != null)
                        s = str;
                }
            }
            if (setAsOriginalValue)
                this.OriginalValue = o;
           
                _control.Text = s;
        }

        private bool _SelectAllOnFocus = true;
        public bool SelectAllOnFocus
        {
            get
            {
                return _SelectAllOnFocus;
            }
            set
            {
                _SelectAllOnFocus = value;
            }
        }

        #region Validation Params
        public bool IsFilled
        {
            get
            {
                return !string.IsNullOrEmpty(_control.Text);
            }
        }
        public bool Required
        {
            get { return Validation.IsRequired; }
            set { Validation.IsRequired = value; }
        }

        #endregion
        
    }
}
