﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using System.CodeDom;

namespace CS.General_v3.Controls.FormControls.Common
{
    public class MyTxtBox : TextBox, General.IMyTxtBox
    {
        protected General.MyTxtBox _myControl = null;

        #region Accept/Cancel Buttons
       
#endregion
        #region Validation Parameters



        private bool _HasError = false;
        
        [Category("Validation")]
        [DefaultValue("")]
        [Description("Whether it has an error or not")]
        public bool HasError
        {
            get { return _myControl.HasError; }
            set { _myControl.HasError = value; }
        }


        [Category("Validation")]
        [DefaultValue("")]
        [Description("Whether it has validation or not")]
        public bool HasValidation
        {
            get { return _myControl.HasValidation; }
            set { _myControl.HasValidation = value; }
        }
        [Category("Validation")]
        public bool Required
        {
            get { return _myControl.Required; }
            set { _myControl.Required = value; }
        }
        
        [Category("Validation")]
        public string Title
        {
            get { return _myControl.Title; }
            set { _myControl.Title = value; }
        }
        [Category("Validation")]
        public CS.General_v3.Controls.FormControls.Common.MyTxtBox ValueSameAsTxt
        {
            get { return _myControl.Validation.ValueSameAsTxt; }
            set { _myControl.Validation.ValueSameAsTxt = value; }
        }
        [Category("Validation")]
        public string RequiredGroup
        {
            get { return _myControl.Validation.RequiredGroup; }
            set { _myControl.Validation.RequiredGroup = value; }
        }

        [Category("Validation")]
        [DefaultValue("main")]
        public string ValidationGroup
        {
            get { return _myControl.ValidationGroup; }
            set { _myControl.ValidationGroup = value; }
        }

        private string _InitValue = "";
        [Category("Validation")]
        public string InitValue
        {
            get
            {
                return _myControl.InitValue;
                
            }
            set
            {
                _myControl.InitValue = value;
            }
        }
        private bool _SelectAllOnFocus = true;
        [Category("Validation")]
        public bool SelectAllOnFocus
        {
            get
            {
                return _myControl.SelectAllOnFocus;
            }
            set
            {
                _myControl.SelectAllOnFocus  = value;
            }
        }

        #endregion
        #region Form Methods
        private string _OriginalText = null;
        
        public object OriginalValue
        {
            get { return _myControl.OriginalValue; }
            set { _myControl.OriginalValue = value; }
        }
        [Browsable(false)]
        public virtual object OriginalValueObject
        {
            get { return _myControl.OriginalValueObject; }
            set { _myControl.OriginalValueObject = value; }
        }
        public virtual bool IsModified
        {
            get
            {
                if (NotPartOfForm)
                    return false;
                else
                {

                    string origValue = this.OriginalValue != null ? this.OriginalValue.ToString() : "";
                    return string.Compare(this.FormValue, origValue, true) != 0;
                }
            }
        }

        #endregion

        public MyTxtBox()
        {
            _myControl = new CS.General_v3.Controls.FormControls.Common.General.MyTxtBox(this);
            TextBox t;
            
        }
        public new void Focus()
        {
            this.Focus(true);
        }
        public void Focus(bool selectAll)
        {
            _myControl.Focus(selectAll);
        }
        public new void Clear()
        {
            OriginalValueObject = null;
            base.Clear();
        }
         
        public string Validate(ErrorProvider errProvider)
        {
            return _myControl.Validate(errProvider);
        }
        [Browsable(false)]
        public string FormValue
        {
            get
            {
                return _myControl.FormValue;
            }
        }
        [Browsable(false)]
        public virtual object FormValueObject
        {
            get
            {
                return _myControl.FormValueObject;
            }
            

        }
        public string TooltipText { get { return _myControl.TooltipText; } set { _myControl.TooltipText = value; } }
        /// <summary>
        /// Sets the text to the object value
        /// </summary>
        /// <param name="o"></param>
        public void SetValue(object o)
        {
            _myControl.SetValue(o);
            
        }
        /// <summary>
        /// Sets the text to the object value
        /// </summary>
        /// <param name="o"></param>
        public void SetValue(object o, bool setAsOriginalValue)
        {
            _myControl.SetValue(o,setAsOriginalValue);

        }



        public string LastValue { get;private set; }
        protected override void OnEnter(EventArgs e)
        {
            this.LastValue = this.Text;
            base.OnEnter(e);
        }







        #region IMyFormControl Members
        public bool IsFilled
        {
            get
            {
                return _myControl.IsFilled;
            }
        }
        public IButtonControl AcceptButton
        {
            get
            {
                return _myControl.AcceptButton;
                
            }
            set
            {
                _myControl.AcceptButton = value;
                
            }
        }

        public IButtonControl CancelButton
        {
            get
            {
                return _myControl.CancelButton;
                
            }
            set
            {
                _myControl.CancelButton = value;
                
            }
        }
        [Category("Validation")]
        
        public bool NotPartOfForm
        {
            get
            {
                return _myControl.NotPartOfForm;
                
            }
            set
            {
                _myControl.NotPartOfForm = value;
                
            }
        }

        #endregion
        public virtual string GetFormValueAsString()
        {
            return FormValue;
        }
    }
}
