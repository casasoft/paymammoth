﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using System.CodeDom;

namespace CS.General_v3.Controls.FormControls.Common
{
    public class MyListView : ListView,General.IMyControl
    {
        public bool AllowSortingOfColumnsBasedOnContent {get;set;}

        public IList<object> GetAllTagsAsList()
        {
            return this.GetAllTagsAsList<object>((item => item));
        }
        public IList<T> GetAllTagsAsList<T>(Converter<object, T> converter)
        {
            List<object> list = new List<object>();
            foreach (ListViewItem item in this.Items)
            {
                list.Add(item.Tag);
            }
            return list.ConvertAll<T>(converter);
            
        }
        public IList<ListViewItem> GetNewListOfCheckedItems()
        {
            List<ListViewItem> list = new List<ListViewItem>();
            for (int i = 0; i < this.CheckedItems.Count; i++)
            {
                list.Add(this.CheckedItems[i]);
            }
            return list;

        }

        public void MoveSelectedUp()
        {
            List<ListViewItem> list = new List<ListViewItem>();
            foreach (ListViewItem lv in this.Items)
            {
                list.Add(lv);
            }
            for (int i = 1; i < list.Count; i++)
            {
                var li = list[i];
                int moveToIndex = i - 1;
                if (li.Selected)
                {
                    var tmp = list[moveToIndex];
                    list[moveToIndex] = li;
                    list[i] = tmp;
                }
            }
        }
        public void MoveSelectedDown()
        {
            List<ListViewItem> list = new List<ListViewItem>();
            foreach (ListViewItem lv in this.Items)
            {
                list.Add(lv);
            }
            for (int i = list.Count -2 ; i >=0; i--)
            {
                var li = list[i];
                int moveToIndex = i + 1;
                if (li.Selected)
                {
                    var tmp = list[moveToIndex];
                    list[moveToIndex] = li;
                    list[i] = tmp;
                }
            }
        }

        private void initHandlers()
        {
            base.ItemCheck += new ItemCheckEventHandler(MyListView_ItemCheck);
            base.ItemChecked += new ItemCheckedEventHandler(MyListView_ItemChecked);
            base.ItemSelectionChanged += new ListViewItemSelectionChangedEventHandler(MyListView_ItemSelectionChanged);
            base.ColumnClick += new ColumnClickEventHandler(MyListView_ColumnClick);
        }

        private CS.General_v3.Enums.SORT_TYPE sortType = Enums.SORT_TYPE.Ascending;

        private int lastSortedColumn = -1;

        void MyListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (AllowSortingOfColumnsBasedOnContent)
            {
                if (lastSortedColumn == e.Column)
                {
                    if (sortType == Enums.SORT_TYPE.Ascending)
                        sortType = Enums.SORT_TYPE.Descending;
                    else
                        sortType = Enums.SORT_TYPE.Ascending;
                }
                else
                {
                    sortType = Enums.SORT_TYPE.Ascending;

                }

                sortByColumn(e.Column);
            }
            
        }
        private void sortByColumn(int index)
        {
            lastSortedColumn = index;
            List<ListViewItem> list = new List<ListViewItem>();
            for (int i = 0; i < this.Items.Count; i++)
            {
                list.Add(this.Items[i]);
            }
            int multiplier = 1;
            if (sortType == Enums.SORT_TYPE.Descending) multiplier = -1;
            list.Sort((item1, item2) =>
                {
                    return item1.SubItems[index].Text.CompareTo(item2.SubItems[index].Text) * multiplier;
                });

            this.Items.Clear();
            this.Items.AddRange(list.ToArray());
        }
        public bool CurrentlyAdding { get; set; }
        public new void BeginUpdate()
        {
            this.CurrentlyAdding = true;
            base.BeginUpdate();
        }
        public new void EndUpdate()
        {
            this.CurrentlyAdding = false;


            base.EndUpdate();
        }
        public new event ListViewItemSelectionChangedEventHandler ItemSelectionChanged;
        public new event ItemCheckEventHandler ItemCheck;
        public new event ItemCheckedEventHandler ItemChecked;
        void MyListView_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (!this.CurrentlyAdding && (ItemSelectionChanged != null))
                ItemSelectionChanged(sender, e);
        }

        void MyListView_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (!this.CurrentlyAdding && (ItemChecked != null))
                ItemChecked(sender, e);
        }

        void MyListView_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (!this.CurrentlyAdding && (ItemCheck != null))
                ItemCheck(sender, e);
        }

        public void SelectAllCheckBoxes()
        {

            if (this.CheckBoxes)
            {
                for (int i = 0; i < this.Items.Count; i++)
                {
                    this.Items[i].Checked = true;
                }
            }
        }
        public void UnSelectAllItems()
        {
            for (int i = 0; i < this.SelectedItems.Count; i++)
            {

                this.SelectedItems[i].Selected = false;
            }
        }
        public void UnSelectAllCheckBoxes()
        {
            if (this.CheckBoxes)
            {
                for (int i = 0; i < this.Items.Count; i++)
                {
                    this.Items[i].Checked = false;
                }
            }
        }
        public void InvertCheckBoxes()
        {
            if (this.CheckBoxes)
            {
                for (int i = 0; i < this.Items.Count; i++)
                {
                    this.Items[i].Checked = !this.Items[i].Checked;
                }
            }
        }

        public MyListView()
        {
            
            initHandlers();
        }
        
    }
}
