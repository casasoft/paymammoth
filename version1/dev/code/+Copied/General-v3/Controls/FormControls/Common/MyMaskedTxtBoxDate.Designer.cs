﻿namespace CS.General_v3.Controls.FormControls.Common
{
    partial class MyMaskedTextBoxDate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MyMaskedTextBoxDate));
            this.CalendarMonthCalendar = new System.Windows.Forms.MonthCalendar();
            this.CalendarButton = new CS.General_v3.Controls.FormControls.Common.MyButton();
            this.SuspendLayout();
            // 
            // CalendarMonthCalendar
            // 
            this.CalendarMonthCalendar.FirstDayOfWeek = System.Windows.Forms.Day.Monday;
            this.CalendarMonthCalendar.Location = new System.Drawing.Point(0, 0);
            this.CalendarMonthCalendar.MaxSelectionCount = 1;
            this.CalendarMonthCalendar.Name = "CalendarMonthCalendar";
            this.CalendarMonthCalendar.TabIndex = 0;
            this.CalendarMonthCalendar.TabStop = false;
            this.CalendarMonthCalendar.Visible = false;
            this.CalendarMonthCalendar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.CalendarMonthCalendar_MouseUp);
            // 
            // CalendarButton
            // 
            this.CalendarButton.AdjustImageLocation = new System.Drawing.Point(0, 0);
            this.CalendarButton.BtnShape = CS.General_v3.Controls.FormControls.Common.emunType.BtnShape.Rectangle;
            this.CalendarButton.BtnStyle = CS.General_v3.Controls.FormControls.Common.emunType.XPStyle.Default;
            this.CalendarButton.DoValidation = false;
            this.CalendarButton.Image = ((System.Drawing.Image)(resources.GetObject("CalendarButton.Image")));
            this.CalendarButton.Location = new System.Drawing.Point(0, 0);
            this.CalendarButton.Name = "CalendarButton";
            this.CalendarButton.Size = new System.Drawing.Size(75, 23);
            this.CalendarButton.TabIndex = 0;
            this.CalendarButton.TabStop = false;
            this.CalendarButton.UseVisualStyleBackColor = true;
            this.ResumeLayout(false);

        }

        #endregion

        private MyButton CalendarButton;
        private System.Windows.Forms.MonthCalendar CalendarMonthCalendar;

    }
}
