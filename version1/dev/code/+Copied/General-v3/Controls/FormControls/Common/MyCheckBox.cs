﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CS.General_v3.Controls.FormControls.Common
{
    public partial class MyCheckBox : CheckBox, General.IMyFormControl
    {
        private General.MyFormControl _myControl = null;
        
        public void Clear()
        {
            this.OriginalValue = false;
            this.Checked = false;
            

        }
        public bool IsFilled
        {
            get
            {
                return this.Checked;
            }
        }
        public MyCheckBox()
        {
            _myControl = new CS.General_v3.Controls.FormControls.Common.General.MyFormControl(this);
            InitializeComponent();
            
        }

        #region IMyFormControl Members

        public IButtonControl AcceptButton
        {
            get
            {
                return _myControl.AcceptButton;
            }
            set
            {
                _myControl.AcceptButton = value;
            }
        }

        public IButtonControl CancelButton
        {
            get
            {
                return _myControl.CancelButton;
            }
            set
            {
                _myControl.CancelButton = value;
            }
        }

        public bool NotPartOfForm
        {
            get
            {
                return _myControl.NotPartOfForm;
            }
            set
            {
                _myControl.NotPartOfForm = value;
            }
        }

        public bool IsModified
        {
            get
            {
                return this.FormValue != this.OriginalValue;
            }
        }

        [Browsable(false)]
        public object FormValueObject
        {
            get
            {
                return FormValue;
            }
        }

        public string Validate(ErrorProvider errProvider)
        {
            return _myControl.Validate(errProvider);
        }

        public bool HasValidation
        {
            get
            {
                return _myControl.HasValidation;
            }
            set
            {
                _myControl.HasValidation = value;
            }
        }

        public string ValidationGroup
        {
            get
            {
                return _myControl.ValidationGroup;
            }
            set
            {
                _myControl.ValidationGroup = value;
            }
        }

        public bool HasError
        {
            get
            {
                return _myControl.HasError;
            }
            set
            {
                _myControl.HasError = value;
            }
        }


        public string Title
        {
            get
            {
                return _myControl.Title;
            }
            set
            {
                _myControl.Title = value;
            }
        }

        #endregion

        private bool _OriginalValue = false;
        public bool OriginalValue
        {
            get { return _OriginalValue; }
            set { _OriginalValue = value; this.Checked = value; }
        }
        public object OriginalValueObject
        {
            get { return OriginalValue; }
            set
            {
                if (value != null)
                {
                    if (!(value is bool))
                    {
                        throw new InvalidOperationException("Only booleans can be set as the original value for a checkbox");
                    }
                    OriginalValue = (bool)value;
                }
            }
        }
        public void SetValue(object o)
        {
            SetValue(o, true);
        }

        public void SetValue(object o, bool SetAsOriginalValue)
        {
            bool value = false;
            if (o is string)
            {
                string s = (string)o;
                s = s.ToLower();
                if (s == "1" || s == "yes" || s == "on" || s == "true")
                    value = true;
            }
            else if (o is bool)
            {
                value = (bool)o;
            }
            if (SetAsOriginalValue)
                this.OriginalValue = value;
            else
                this.Checked = value;

        }


        [Browsable(false)]
        public bool FormValue
        {
            get
            {
                return this.Checked;
            }
        }

        public string GetFormValueAsString()
        {
            string s = "0";
            if (FormValue)
                s = "1";
            return s;
        }
        public string TooltipText { get { return _myControl.TooltipText; } set { _myControl.TooltipText = value; } }
    }
}
