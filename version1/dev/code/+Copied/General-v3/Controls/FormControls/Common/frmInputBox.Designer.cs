﻿namespace CS.General_v3.Controls.FormControls.Common
{
    partial class frmInputBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtValue = new MyTxtBoxTextSingleMultiLine();
            this.lblTitle = new MyLabel();
            this.btnOK = new MyButton();
            this.btnCancel = new MyButton();
            this.SuspendLayout();
            // 
            // txtValue
            // 
            this.txtValue.AcceptButton = null;
            this.txtValue.CancelButton = null;
            this.txtValue.HasError = false;
            this.txtValue.HasValidation = true;
            this.txtValue.InitValue = "";
            this.txtValue.Location = new System.Drawing.Point(9, 54);
            this.txtValue.Name = "txtValue";
            this.txtValue.NotPartOfForm = false;
            this.txtValue.Required = false;
            this.txtValue.RequiredGroup = "";
            this.txtValue.SelectAllOnFocus = true;
            this.txtValue.Size = new System.Drawing.Size(355, 20);
            this.txtValue.TabIndex = 0;
            this.txtValue.Title = "";
            // 
            // lblTitle
            // 
            this.lblTitle.Location = new System.Drawing.Point(6, 9);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(355, 42);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "myLabel1";
            // 
            // btnOK
            // 
            this.btnOK.AdjustImageLocation = new System.Drawing.Point(0, 0);
            this.btnOK.BtnShape = CS.General_v3.Controls.FormControls.Common.emunType.BtnShape.Rectangle;
            this.btnOK.BtnStyle = CS.General_v3.Controls.FormControls.Common.emunType.XPStyle.Default;
            this.btnOK.Location = new System.Drawing.Point(108, 80);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.AdjustImageLocation = new System.Drawing.Point(0, 0);
            this.btnCancel.BtnShape = CS.General_v3.Controls.FormControls.Common.emunType.BtnShape.Rectangle;
            this.btnCancel.BtnStyle = CS.General_v3.Controls.FormControls.Common.emunType.XPStyle.Default;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(189, 80);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // frmInputBox
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(373, 110);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.txtValue);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmInputBox";
            this.Text = "frmInputBox";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmInputBox_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MyTxtBox txtValue;
        private MyLabel lblTitle;
        private MyButton btnOK;
        private MyButton btnCancel;
    }
}