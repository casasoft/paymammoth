﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CS.General_v3.Controls.FormControls.Common
{
    public partial class MyMaskedTextBoxDate : MyMaskedTextBox
    {
        public bool AutoFormatDateOnLostFocus { get; set; }
        public override bool IsModified
        {
            get
            {
                DateTime? d1 = this.OriginalValue;
                DateTime? d2 = this.FormValue;
                if (d1.HasValue && d1 == DateTime.MinValue)
                    d1 = null;
                if (d2.HasValue && d2 == DateTime.MinValue)
                    d2 = null;
                if ((d1 == null && d2 != null) || (d1 != null && d2 == null))
                    return true;
                else if (d1 != null && d2 != null)
                {
                    return DateTime.Compare(d1.Value, d2.Value) != 0;
                }
                else
                    return false;

            }
        }
        public enum Mask_Mode
        {
            Date = 10,
            DateTime = 20,
            Custom = 30
        }
        public enum ALIGNMENT
        {
            TopLeft,TopRight,BottomLeft,BottomRight
        }
        private DateTime? _OriginalValue = null;
        public new DateTime? OriginalValue
        {
            get { return (DateTime?)OriginalValueObject; }
            set { base.OriginalValueObject = value;

             }
        }
        public override object OriginalValueObject
        {
            get
            {
                return base.OriginalValueObject;
            }
            set
            {
                base.OriginalValueObject = value;
            }
        }
        [Browsable(false)]
        public override object FormValueObject
        {
            get
            {
                string s = base.FormValue; 
                DateTime? date = null;
                if (!string.IsNullOrEmpty(s))
                {
                    DateTime tmpDate;
                    string sDate = s;
                    if (!DateTime.TryParse(s, out tmpDate))
                    {
                        int spacePos = sDate.IndexOf(' ');
                        if (spacePos == -1)
                            spacePos = sDate.Length;
                        sDate = sDate.Substring(0, spacePos);
                        if (DateTime.TryParse(sDate, out tmpDate))
                        {
                            date = tmpDate;
                        }
                    }
                    else
                    {
                        date = tmpDate;
                    }



                }
                return date;
            }
        }
        [Browsable(false)]
        public new DateTime? FormValue
        {
            get
            {
                
                return (DateTime?)(FormValueObject);
            }
        }
        #region Validation Parameters
        [Category("Validation")]
        public bool TimeRequired
        {
            get
            {
                return _myControl.Validation.TimeRequired;
            }
            set
            {
                _myControl.Validation.TimeRequired= value;
            }
        }
        
        [Category("Validation")]
        public DateTime DateFrom
        {
            get { return _myControl.Validation.DateFrom; }
            set { _myControl.Validation.DateFrom = value; }
        }
        [Category("Validation")]
        public DateTime DateTo
        {
            get { return _myControl.Validation.DateTo; }
            set { _myControl.Validation.DateTo = value; }
        }
        [Category("Validation")]
        public CS.General_v3.Controls.FormControls.Common.MyMaskedTextBox DateAfterTxt
        {
            get { return _myControl.Validation.DateAfterTxt; }
            set { _myControl.Validation.DateAfterTxt = value; }

        }
        [Category("Validation")]
        public CS.General_v3.Controls.FormControls.Common.MyMaskedTextBox DateBeforeTxt
        {
            get { return _myControl.Validation.DateBeforeTxt; }
            set { _myControl.Validation.DateBeforeTxt = value; }

        }

        
        #endregion
        #region Date Calendar
        private bool _HasCalendar = true;
        private bool _CheckedCalendar = false;
        
        
        //private MyButton _CalendarButton = null;
        //private MonthCalendar CalendarMonthCalendar = null;
        private ALIGNMENT _CalendarShowLocation = ALIGNMENT.TopRight;
        private MouseEventHandler closeMouseClickEvent = null;
        [Description("Whether the date is accompanied by a calendar button")]
        [Category("Calendar")]
        public bool HasCalendar
        {
            get { return _HasCalendar; }
            set { _HasCalendar = value; }
        }
        private void ShowCalendar()
        {
            if (string.Compare(this.Name, "txtdeparturedate", true) == 0)
            {
                int k = 5;
            }
            int padding = 0;
            Form form = this.FindForm();
            CalendarMonthCalendar.TabStop = false;
            CalendarButton.TabStop = false;
            //CalendarMonthCalendar.TabIndex = this.TabIndex + 1;
            if (CalendarMonthCalendar.Parent == null)
            {
                //CalendarMonthCalendar = new MonthCalendar();
                this.FindForm().Controls.Add(CalendarMonthCalendar);
                CalendarMonthCalendar.BringToFront();
                CalendarMonthCalendar.DateChanged += new DateRangeEventHandler(CalendarMonthCalendar_DateChanged);
            }
            DateTime d;
            if (!DateTime.TryParse(this.Text, out d))
            {
                d = CS.General_v3.Util.Date.Now;
            }
            try
            {
                CalendarMonthCalendar.SelectionStart = CalendarMonthCalendar.SelectionEnd = d;
            }
            catch
            {
                d = CS.General_v3.Util.Date.Now;
                CalendarMonthCalendar.SelectionStart = CalendarMonthCalendar.SelectionEnd = d;
            }

            CS.General_v3.Util.WindowsForms.Events.AttachMouseDownListener(this.FindForm(), closeMouseClickEvent);
            CalendarMonthCalendar.MouseDown -= closeMouseClickEvent;
            CalendarButton.MouseDown -= closeMouseClickEvent;
            int x,y;
            CS.General_v3.Util.WindowsForms.Mouse.GetGlobalXY(CalendarButton, out x, out y);
            bool invalidLoc = false;
            if ((_CalendarShowLocation == ALIGNMENT.TopLeft && (x - CalendarMonthCalendar.Width < 0 || y - CalendarMonthCalendar.Height < 0)) ||
                (_CalendarShowLocation == ALIGNMENT.TopRight && (x + CalendarMonthCalendar.Width > form.Width || y - CalendarMonthCalendar.Height < 0)) ||
                (_CalendarShowLocation == ALIGNMENT.BottomLeft && (x - CalendarMonthCalendar.Width < 0 || y + CalendarMonthCalendar.Height + CalendarButton.Height > form.Height)) ||
                (_CalendarShowLocation == ALIGNMENT.BottomRight && (x + CalendarMonthCalendar.Width > form.Width || y + CalendarMonthCalendar.Height + CalendarButton.Height > form.Height)))
            {
                if (x + CalendarMonthCalendar.Width < form.Width && y - CalendarMonthCalendar.Height > 0)
                {
                    _CalendarShowLocation = ALIGNMENT.TopRight;
                }
                else if (x - CalendarMonthCalendar.Width > 0 && y - CalendarMonthCalendar.Height > 0)
                {
                    _CalendarShowLocation = ALIGNMENT.TopLeft;
                }
                else if (x - CalendarMonthCalendar.Width > 0 && y + CalendarMonthCalendar.Height + CalendarButton.Height < form.Height)
                {
                    _CalendarShowLocation = ALIGNMENT.BottomLeft;
                }
                else 
                {
                    _CalendarShowLocation = ALIGNMENT.BottomRight;
                }
            }
            switch (_CalendarShowLocation)
            {
                case ALIGNMENT.BottomLeft:
                    {
                        x = x - CalendarMonthCalendar.Width + CalendarButton.Width;
                        y = y + CalendarButton.Height + padding; 
                    }
                    break;
                case ALIGNMENT.BottomRight:
                    {
                        x = x;
                        y = y + CalendarButton.Height + padding;
                    }
                    break;
                case ALIGNMENT.TopLeft:
                    {
                        x = x - CalendarMonthCalendar.Width + CalendarButton.Width;
                        y = y - CalendarMonthCalendar.Height - padding;
                    }
                    break;
                case ALIGNMENT.TopRight:
                    {
                        x = x;
                        y = y - CalendarMonthCalendar.Height - padding;
                    }
                    break;
            }
            CalendarMonthCalendar.Left = x;
            CalendarMonthCalendar.Top = y;
            CalendarMonthCalendar.Visible = true;
            CalendarButton.TabStop = false;
        }

        void CalendarMonthCalendar_DateChanged(object sender, DateRangeEventArgs e)
        {
            DateTime dOld;
            int hour = 0, minute = 0, second = 0;
            if (DateTime.TryParse(this.Text, out dOld))
            {
                hour = dOld.Hour;
                minute = dOld.Minute;
                second = dOld.Second;
            }

            DateTime d = CalendarMonthCalendar.SelectionStart;
            d = new DateTime(d.Year,d.Month,d.Day,hour,minute,second);
            if (this.MaskMode == Mask_Mode.Date)
                this.Text = d.ToString("dd/MM/yyyy");
            else if (this.MaskMode == Mask_Mode.DateTime)
            {
                this.Text = d.ToString("dd/MM/yyyy HH:mm");
            }
            //HideCalendar();

        }
        private void HideCalendar()
        {
            if (CalendarMonthCalendar != null && CalendarMonthCalendar.Visible)
            {
                CS.General_v3.Util.WindowsForms.Events.DetachMouseDownListener(this.FindForm(), closeMouseClickEvent);
                CalendarMonthCalendar.Visible = false;
            }
        }
        
        protected override void OnVisibleChanged(EventArgs e)
        {
            base.OnVisibleChanged(e);
            checkCalendarControls();
        }
        protected override void OnParentBindingContextChanged(EventArgs e)
        {
            base.OnParentBindingContextChanged(e);
            checkCalendarControls();
        }
        protected override void OnParentChanged(EventArgs e)
        {
            base.OnParentChanged(e);
            checkCalendarControls();
        }
        protected override void OnLayout(LayoutEventArgs levent)
        {
            
            base.OnLayout(levent);
            checkCalendarControls();
        }
        protected override void OnLocationChanged(EventArgs e)
        {
            base.OnLocationChanged(e);
            checkCalendarControls();
        }
        protected override void OnMarginChanged(EventArgs e)
        {
            base.OnMarginChanged(e);
            checkCalendarControls();
        }
        private void checkCalendarControls()
        {
            if (!_CheckedCalendar)
            {
                if (Site == null || !Site.DesignMode)
                {
                    if (_HasCalendar && this.MaskMode != Mask_Mode.Custom)
                    {
                        if (CalendarButton.Parent == null)
                        {
                            if (this.Parent != null)
                            {
                                this.Parent.Controls.Add(CalendarButton);
                                _CheckedCalendar = true;

                                CalendarButton.Click += new EventHandler(CalendarButton_Click);




                                closeMouseClickEvent = new MouseEventHandler(MouseClickClose_MouseClick);
                            }

                        }

                    }
                }
                
            }
            if (CalendarButton != null)
                CalendarButton.TabStop = false;
            CalendarButton.Left = this.Left + this.Width;
            CalendarButton.Top = this.Top;
            CalendarButton.Height = this.Height;
            CalendarButton.Width = 24;
                                
        }

        void MouseClickClose_MouseClick(object sender, EventArgs e)
        {
            HideCalendar();
        }

        void CalendarButton_Click(object sender, EventArgs e)
        {
            if (CalendarMonthCalendar == null || !CalendarMonthCalendar.Visible)
                ShowCalendar();
            else
                HideCalendar();
        }
        protected override void OnResize(EventArgs e)
        {
            checkCalendarControls();
            base.OnResize(e);
        }
        
        
        
        #endregion
       
        private void checkMaskMode()
        {
            if (_MaskMode == Mask_Mode.Date)
            {
                base.Mask = "00/00/0000";
                _myControl.Validation.IsDate = true;

            }
            else if (_MaskMode == Mask_Mode.DateTime)
            {
                base.Mask = "00/00/0000 00:00";
                _myControl.Validation.IsDate = true;

                
            }
            else if (_MaskMode == Mask_Mode.Custom)
            {
                _myControl.Validation.IsDate = false;
            }
        }
        private Mask_Mode _MaskMode = Mask_Mode.Date;
        public Mask_Mode MaskMode
        {
            get
            {
                return _MaskMode;
            }
            set
            {
                _MaskMode = value;
                checkMaskMode();
            }
        }
        public new string Mask
        {
            get
            {
                return base.Mask;
            }
            set
            {
                if (MaskMode == Mask_Mode.Custom)
                    base.Mask = value;
            }
        }
        
        public MyMaskedTextBoxDate()
        {
            _myControl.Validation.IsDate = true;
            InitializeComponent();
            this.AutoFormatDateOnLostFocus = true;
            checkMaskMode();
        }


        protected override void OnGotFocus(EventArgs e)
        {
            
            base.OnGotFocus(e);
        }
        protected override void OnLostFocus(EventArgs e)
        {
            checkDateFormat();
            base.OnLostFocus(e);
        }

        private void checkDateFormat()
        {
            if (AutoFormatDateOnLostFocus && (this.MaskMode == Mask_Mode.Date || this.MaskMode == Mask_Mode.DateTime))
            {
                
                if (this.FormValue != null)
                {
                    string s = this.FormValue.Value.ToString("dd/MM/yyyy");
                    if (this.MaskMode == Mask_Mode.DateTime)
                        s += " " + this.FormValue.Value.ToString("HH:mm");
                    this.Text = s;
                }
            }
            
        }
        
        private DateTime _lastMouseDownTime = DateTime.MinValue;
        private DateTime _lastChosenDate = DateTime.MinValue;
        private int _lastMouseDownClicks = 0;
        private void CalendarMonthCalendar_MouseUp(object sender, MouseEventArgs e)
        {
            DateTime selDate = CalendarMonthCalendar.SelectionStart;
            
            DateTime d = CS.General_v3.Util.Date.Now;
            int tickInterval = 8000000;
            long diff = d.Ticks - _lastMouseDownTime.Ticks;
            if (((diff <= tickInterval)) && selDate == _lastChosenDate)
            {
                _lastMouseDownClicks++;
            }
            else
            {
                _lastMouseDownClicks = 1;
            }
            if (_lastMouseDownClicks >= 2)
            {
                _lastMouseDownClicks = 0;
                HideCalendar();
            }
            _lastMouseDownTime = d;
            _lastChosenDate = CalendarMonthCalendar.SelectionStart;
            
        }
    }
}
