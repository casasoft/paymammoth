﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using System.CodeDom;

namespace CS.General_v3.Controls.FormControls.Common
{
    public class MyGroupBox : GroupBox, General.IMyControl
    {
        protected General.MyControl _myControl = null;
        public void SetControlsEnabledState(bool b, params Control[] exceptionList)
        {
            bool[] bExceptionListValue = null;
            if (exceptionList != null)
            {
                bExceptionListValue = new bool[exceptionList.Length];
                for (int i = 0; i < bExceptionListValue.Length; i++)
                {
                    bExceptionListValue[i] = exceptionList[i].Enabled;
                }
            }
            CS.General_v3.Util.WindowsForms.ChangeControlEnabled(this, b);
            if (exceptionList != null)
            {
                for (int i = 0; i < bExceptionListValue.Length; i++)
                {
                    exceptionList[i].Enabled = bExceptionListValue[i];
                }
            }
        }
        public void DisableAllControls()
        {
            SetControlsEnabledState(false);
        }
        public void DisableAllControls(params Control[] exceptionList)
        {
            SetControlsEnabledState(false, exceptionList);
        }
        public void EnableAllControls()
        {
            SetControlsEnabledState(true);
        }
        public void EnableAllControls(params Control[] exceptionList)
        {
            SetControlsEnabledState(true, exceptionList);
        }


        public MyGroupBox()
        {
            _myControl = new CS.General_v3.Controls.FormControls.Common.General.MyControl(this);
            
        }
        
    }
}
