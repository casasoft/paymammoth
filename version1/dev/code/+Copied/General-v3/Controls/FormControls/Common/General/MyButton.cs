﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CS.General_v3.Controls.FormControls.Common.General
{
    public class MyButton : IMyButton
    {
        protected Control _control = null;
        #region IMyButton Members

        private ErrorProvider _errProvider = null;
        private ErrorProvider errProvider
        {
            get
            {
                if (_errProvider == null)
                    getErrorProvider();
                return _errProvider;
            }
        }
        private void getErrorProvider()
        {
            _errProvider = CS.General_v3.Util.WindowsForms.GetErrorProviderForControl(_control);
            
            
        }

        private void initHandlers()
        {

            if (_control is Button)
            {
                Button btn = (Button)_control;
                btn.Click += new EventHandler(btn_Click);
            }
            else if (_control is LinkLabel)
            {
                LinkLabel btn = (LinkLabel)_control;
                btn.Click += new EventHandler(btn_Click);
            }
            
        }
        /// <summary>
        /// Checks for validation
        /// </summary>
        /// <returns></returns>
        public bool CheckValidation()
        {
            return CS.General_v3.Util.WindowsForms.Validation.CheckValidation(_control.FindForm(), this.ValidationGroup, errProvider);
        }
        void btn_Click(object sender, EventArgs e)
        {
            bool ok = true;
            if (DoValidation)
            {
                
                if (errProvider != null)
                {
                    ok = CheckValidation();
                }
                else
                {
                    throw new InvalidOperationException("No error provider defined for this form, although button is set to require validation!");
                }

            }
            if (ok && ClickWithValidation != null)
                ClickWithValidation(sender, e);
        }
        public MyButton(Control ctrl)
        {
            this.ValidationGroup = "main";
            this.DoValidation = false;
            _control = ctrl;
            initHandlers();
        }
        public void PerformClick()
        {
            

            IButtonControl btn = (IButtonControl)_control;
            btn.PerformClick();
            btn_Click(this, null);
            
        }
        public event EventHandler ClickWithValidation;
        public string ValidationGroup {get;set;}
        public bool DoValidation { get; set; }
        public object Tag {get;set;}
        #endregion
    }
}
