﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using System.CodeDom;

namespace CS.General_v3.Controls.FormControls.Common
{
    public class MyTxtBoxTextPassword : MyTxtBoxText
    {
        public new char PasswordChar
        {
            get
            {
                return base.PasswordChar;
            }
            set
            {
                char c = value;
                if (c == null)
                    c = '*';
                base.PasswordChar = value;
            }
        }
        public MyTxtBoxTextPassword() : base()
        {
            this.PasswordChar = '*';
            
        }

        
        

        
    }
}
