﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Reflection;
using System.Runtime.InteropServices;
using System.ComponentModel;
namespace CS.General_v3.Controls.FormControls.Common
{
    public class MyForm : Form
    {
        /// <summary>
        /// Returns the 'real' height.  The normal 'height' property does not give you the total pixels in screen space, thus when you
        /// are moving an item, and doing frm.height - item.height, it will not be set to the bottom-most part, but off screen due to the height difference.
        /// </summary>
        public new int Height
        {
            get
            {
                //return base.Height - 37;
                return base.Height;
            }
            set
            {
                
                base.Height = value;
            }
        }
        public int Height_Base
        {
            get
            {
                return base.Height;
            }
            set
            {
                base.Height = value;
            }
        }
        /// <summary>
        /// Returns the 'real' width.  The normal 'Width' property does not give you the total pixels in screen space, thus when you
        /// are moving an item, and doing frm.width - item.width, it will not be set to the rightmost part, but off screen due to the width difference.
        /// </summary>
        public new int Width
        {
            get
            {
                //return base.Width - 15;
                return base.Width;
            }
            set
            {
                base.Width = value;
            }
        }
        public int Width_Base
        {
            get
            {
                return base.Width;
            }
            set
            {
                base.Width = value;
            }
        }
        private ErrorProvider _ErrorProvider = null;
        public ErrorProvider ErrorProvider
        {
            get
            {
                
                if (_ErrorProvider == null)
                {
                    //_ErrorProvider = findErrorProvider();
                }
                return _ErrorProvider;
            }
            set
            {
                _ErrorProvider = value;
            }
        }

    }
}
