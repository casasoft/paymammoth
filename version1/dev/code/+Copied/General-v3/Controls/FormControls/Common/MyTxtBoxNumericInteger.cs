﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using System.CodeDom;

namespace CS.General_20090518.Controls.FormControls.Common
{
    public class MyTxtBoxNumericInteger : MyTxtBoxNumeric
    {
        public MyTxtBoxNumericInteger()
            : base()
        {
            _myControl.Validation.IntegersOnly = true;
            
        }
        
        #region Validation Parameters
        
        [Category("Validation")]
        public int? NumberFrom
        {
            get { return (int)_myControl.Validation.NumberFrom; }
            set { _myControl.Validation.NumberFrom = (double)value; }
        }
        [Category("Validation")]
        public int? NumberTo
        {
            get { return (int)_myControl.Validation.NumberTo; }
            set { _myControl.Validation.NumberTo = (double)value; }
        }




        #endregion
        #region Form Methods
        private int? _OriginalValue = null;
        public int? OriginalValue
        {
            get 
            {
                return _OriginalValue;
            }
            set { _OriginalValue = value; SetValue(_OriginalValue, false); }
        }
        [Browsable(false)]
        public object OriginalValueObject
        {
            get { return _myControl.OriginalValueObject; }
            set 
            {
                if (value != null && (!(value is double) && !(value is int)))
                {
                    throw new InvalidOperationException("Only ints or null can be set to a numeric integer field");
                }
                OriginalValue = (int?)value;
            }
        }

        #endregion

        [Browsable(false)]
        public override object FormValueObject
        {
            get
            {
                double? d = (double?)base.FormValueObject;
                int? integer = null;
                if (d != null)
                    integer = (int)d.Value;
                
                return integer;
            }
        }
         [Browsable(false)]
         public new int? FormValue
        {
            get
            {
                return (int?)(FormValueObject);
            }
        }
        public override string GetFormValueAsString()
        {
            string s = "";
            if (FormValue != null)
                s = FormValue.ToString();
            return s;
        }
        protected override void OnTextChanged(EventArgs e)
        {
            base.checkText(true,PositiveNumbersOnly);
            base._lastText= this.Text;
            //base.OnTextChanged(e);
        }
    }
}
