﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CS.General_v3.Controls.FormControls.Common
{
    [DefaultEvent("ClickWithValidation")]
	
    public partial class MyLinkLabel : LinkLabel, General.IMyButton
    {
        protected General.MyButton _myControl = null;

        private void initHandlers()
        {
            _myControl.ClickWithValidation += new EventHandler(_myControl_ClickWithValidation);
        }

        void _myControl_ClickWithValidation(object sender, EventArgs e)
        {
            if (ClickWithValidation != null)
                ClickWithValidation(sender, e);
        }
        public MyLinkLabel()
        {
            _myControl = new CS.General_v3.Controls.FormControls.Common.General.MyButton(this);
            initHandlers();
            InitializeComponent();
        }


        #region IMyButton Members

        public void PerformClick()
        {
            _myControl.PerformClick();
            
        }

        public string ValidationGroup
        {
            get
            {
                return _myControl.ValidationGroup;
                
            }
            set
            {
                _myControl.ValidationGroup = value;
            }
        }

        #endregion

        #region IMyButton Members


        public bool DoValidation
        {
            get
            {
                return _myControl.DoValidation;
            }
            set
            {
                _myControl.DoValidation = value;
            }
        }

        public event EventHandler ClickWithValidation;

        #endregion
    }
}
