﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CS.General_v3.Controls.FormControls.Common
{
    public partial class frmInputBox : Form
    {
        private DialogResult _Result = DialogResult.Cancel;

        public DialogResult Result
        {
            get { return _Result; }
            set { _Result = value; }
        }

        private bool _IsRequired = false;

        public bool IsRequired
        {
            get { return _IsRequired; }
            set { _IsRequired = value; }
        }
        private string _Title = null;

        public string Title
        {
            get { return _Title; }
            set { _Title = value; base.Text = _Title; }
        }
        private string _Message = null;

        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        private string _InitText = null;

        public string InitText
        {
            get { return _InitText; }
            set { _InitText = value; }
        }

        public string ShowDialog()
        {
            return ShowDialog(null, this.Title, this.Message, this.InitText);
        }
        public string ShowDialog(IWin32Window owner)
        {
            return ShowDialog(owner, this.Title, this.Message, this.InitText);
        }
        public string ShowDialog(string title, string message, string initText)
        {
            return ShowDialog(null, title, message, initText);
        }
        public string ShowDialog(IWin32Window owner, string title, string message, string initText)
        {
            this.Message = message;
            this.InitText = initText;
            txtValue.Text = initText;
            this.Title = title;
            lblTitle.Text = message;
            base.ShowDialog(owner);
            if (Result == DialogResult.OK)
            {
                return txtValue.FormValue;
            }
            else
            {
                return null;
            }
        }


        public frmInputBox()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Result = DialogResult.Cancel;
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Result = DialogResult.OK;
            _IsClosing = true;
            this.Close();
        }
        private bool _IsClosing = false;
        private void frmInputBox_FormClosed(object sender, FormClosedEventArgs e)
        {
            
            if (!_IsClosing && e.CloseReason == CloseReason.UserClosing)
            {
                Result = DialogResult.Cancel;
            }
            _IsClosing = true;
        }
        
        public frmInputBox(string title, string message, string initText)
        {
            this.Title = title;
            this.Message = message;
            this.InitText = initText;
        }
    }
}
