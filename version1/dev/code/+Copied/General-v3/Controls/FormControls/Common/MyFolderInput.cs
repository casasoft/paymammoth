﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CS.General_v3.Controls.FormControls.Common
{
    public partial class MyFolderInput : UserControl, General.IMyFormControlRequireable
    {
        protected General.MyFormControlRequireable _myControl = null;
        public MyFolderInput()
        {
            _myControl = new CS.General_v3.Controls.FormControls.Common.General.MyFormControlRequireable(this);
            InitializeComponent();
            this.Height = 20;
        }
        
        #region Properties
        public Environment.SpecialFolder RootFolder { get; set; }
        public string FormValue
        {
            get
            {
                return txtInput.Text;
            }
        }
        public string Text
        {
            get
            {
                return this.txtInput.Text;
            }
            set
            {
                this.txtInput.Text = value;
            }
        }
        #endregion

        private void setDialogProperties()
        {
            folderBrowserDialog.RootFolder = RootFolder;

        }

        private void positionUI()
        {
            txtInput.Left = 0;
            txtInput.Top = 0;
            txtInput.Width = this.Width - btnInput.Width+1;
            txtInput.Height = this.Height;

            btnInput.Height = this.Height;
            btnInput.Left = txtInput.Left + txtInput.Width-1;
            
        }

        protected override void OnResize(EventArgs e)
        {
            positionUI();
            base.OnResize(e);
        }
        
        private void btnInput_Click(object sender, EventArgs e)
        {
            setDialogProperties();
            DialogResult result = folderBrowserDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtInput.Text = folderBrowserDialog.SelectedPath;
            }
             
        }

        #region IMyFormControl Members

        public IButtonControl AcceptButton
        {
            get
            {
                return _myControl.AcceptButton;
            }
            set
            {
                _myControl.AcceptButton = value;
            }
        }

        public IButtonControl CancelButton
        {
            get
            {
                return _myControl.CancelButton;
            }
            set
            {
                _myControl.CancelButton = value;
            }
        }

        public bool NotPartOfForm
        {
            get
            {
                return _myControl.NotPartOfForm;
            }
            set
            {
                _myControl.NotPartOfForm = value;
            }
        }

        public bool IsModified
        {
            get
            {
                return this.FormValue != this.OriginalValue;
            }
        }

        public object FormValueObject
        {
            get
            {
                return FormValue;
            }
        }

        public string Validate(ErrorProvider errProvider)
        {
            return _myControl.Validate(errProvider);
        }

        public bool HasValidation
        {
            get
            {
                return _myControl.HasValidation;
            }
            set
            {
                _myControl.HasValidation = value;
            }
        }

        public string ValidationGroup
        {
            get
            {
                return _myControl.ValidationGroup;
            }
            set
            {
                _myControl.ValidationGroup = value;
                this.txtInput.ValidationGroup = this.ValidationGroup;
            }
        }

        public bool HasError
        {
            get
            {
                return _myControl.HasError;
            }
            set
            {
                _myControl.HasError = value;
            }
        }


        public string Title
        {
            get
            {
                return _myControl.Title;
            }
            set
            {
                txtInput.Title = _myControl.Title = value;
            }
        }

        #endregion


        #region IMyFormControl Members
        public bool IsFilled
        {
            get
            {
                return !string.IsNullOrEmpty(txtInput.FormValue);
            }
        }

        public void Clear()
        {
            txtInput.Clear();
        }
        private string _OriginalText = null;

        public string OriginalValue
        {
            get { return _OriginalText; }
            set { _OriginalText = value; SetValue(value,false); }
        }
        public object OriginalValueObject
        {
            get
            {
                return OriginalValue;
            }
            set
            {
                OriginalValue = (string)value;
            }
        }

        public void SetValue(object o)
        {
            SetValue(o, true);

        }

        public void SetValue(object o, bool SetAsOriginalValue)
        {
            if (o == null)
                o = "";
            string s = o.ToString();
            if (SetAsOriginalValue)
                OriginalValue = s;
            else
                this.Text = s;
            
        }

        #endregion
        #region IMyFormControlRequireable Members

        public bool Required
        {
            get
            {
                return _myControl.Required;
            }
            set
            {
                _myControl.Required = value;
                this.txtInput.Required = this.Required;
            }
        }

        public string RequiredGroup
        {
            get
            {
                return _myControl.RequiredGroup;
            }
            set
            {
                _myControl.RequiredGroup = value;
                this.txtInput.RequiredGroup = this.RequiredGroup;
            }
        }

        #endregion
        public string GetFormValueAsString()
        {
            return FormValue;
        }
    }
}
