﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using System.CodeDom;

namespace CS.General_v3.Controls.FormControls.Common
{
    public class MyNumericUpDown : NumericUpDown, General.IMyFormControl
    {
        protected General.MyFormControl _myControl = null;



        public MyNumericUpDown()
        {
            _myControl = new CS.General_v3.Controls.FormControls.Common.General.MyFormControl(this);
        }
        
        public new void Clear()
        {
            this.Value = 0;
        }
         
        public string Validate(ErrorProvider errProvider)
        {
            return _myControl.Validate(errProvider);
        }
        [Browsable(false)]
        public int FormValue
        {
            get
            {
                return Convert.ToInt32(this.Value);
            }
        }
        [Browsable(false)]
        public virtual object FormValueObject
        {
            get
            {
                return FormValue;
            }
            

        }
        
        /// <summary>
        /// Sets the text to the object value
        /// </summary>
        /// <param name="o"></param>
        public void SetValue(object o)
        {
            int num = 0;
            num = Convert.ToInt32(o);
            this.Value = num;
            
            
        }
        /// <summary>
        /// Sets the text to the object value
        /// </summary>
        /// <param name="o"></param>
        public void SetValue(object o, bool setAsOriginalValue)
        {
            _myControl.SetValue(o,setAsOriginalValue);

        }











        #region IMyFormControl Members
        public bool IsFilled
        {
            get
            {
                return true;
            }
        }
        public IButtonControl AcceptButton
        {
            get
            {
                return _myControl.AcceptButton;
                
            }
            set
            {
                _myControl.AcceptButton = value;
                
            }
        }

        public IButtonControl CancelButton
        {
            get
            {
                return _myControl.CancelButton;
                
            }
            set
            {
                _myControl.CancelButton = value;
                
            }
        }
        [Category("Validation")]
        
        public bool NotPartOfForm
        {
            get
            {
                return _myControl.NotPartOfForm;
                
            }
            set
            {
                _myControl.NotPartOfForm = value;
                
            }
        }

        #endregion
        public virtual string GetFormValueAsString()
        {
            return FormValue.ToString();
        }

        #region IMyFormControl Members


        public bool IsModified
        {
            get {

                if (NotPartOfForm)
                    return false;
                else
                {
                    return OriginalValue != this.Value;
                    
                }
            
            }
        }
        public int OriginalValue
        {
            get
            {
                return ((int?)_myControl.OriginalValueObject).GetValueOrDefault();
            }
            set
            {
                _myControl.OriginalValueObject = value;
            }
        }
        public object OriginalValueObject
        {
            get
            {
                return _myControl.OriginalValueObject;
                
            }
            set
            {
                _myControl.OriginalValueObject = value;
                
            }
        }

        public bool HasValidation
        {
            get
            {
                return _myControl.HasValidation;
                
            }
            set
            {
                _myControl.HasValidation = value;
                
            }
        }

        public string ValidationGroup
        {
            get
            {
                return _myControl.ValidationGroup;
            }
            set

            {
                _myControl.ValidationGroup = value;
                
            }
        }

        public bool HasError
        {
            get
            {
                return _myControl.HasError ;
                
            }
            set
            {
                _myControl.HasError = value;
            }
        }

        public string Title
        {
            get
            {
                return _myControl.Title;
            }
            set
            {
                _myControl.Title = value;
                
            }
        }

        #endregion
    }
}
