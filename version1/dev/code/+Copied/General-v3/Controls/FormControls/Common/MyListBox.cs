﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using System.CodeDom;

namespace CS.General_v3.Controls.FormControls.Common
{
    public class MyListBox : ListBox, General.IMyFormControl, General.IMyListItemControl
    {
        public bool IsFilled
        {
            get
            {
                return this.FormValue.Count > 0;
            }
        }
        private General.MyListItemControl _myListItemControl = null;
        private General.MyFormControl _myControl = null;
        public MyListBox()
        {
            _myListItemControl = new CS.General_v3.Controls.FormControls.Common.General.MyListItemControl(this);
            _myControl = new CS.General_v3.Controls.FormControls.Common.General.MyFormControl(this);
        }
        #region IMyFormControl Members

        public IButtonControl AcceptButton
        {
            get
            {
                return _myControl.AcceptButton;
            }
            set
            {
                _myControl.AcceptButton = value;
            }
        }

        public IButtonControl CancelButton
        {
            get
            {
                return _myControl.CancelButton;
            }
            set
            {
                _myControl.CancelButton = value;
            }
        }

        public bool NotPartOfForm
        {
            get
            {
                return _myControl.NotPartOfForm;
            }
            set
            {
                _myControl.NotPartOfForm = value;
            }
        }

        public bool IsModified
        {
            get
            {
                bool modified = false;
                if (this.FormValue.Count != this.OriginalValue.Count)
                    modified = true;
                else
                {
                    for (int i = 0; i < this.FormValue.Count; i++)
                    {
                        CS.General_v3.Classes.ListItem listItem = this.FormValue[i];
                        bool found = false;
                        for (int j = 0; j < this.OriginalValue.Count; j++)
                        {
                            if (string.Compare(listItem.Text, this.OriginalValue[j], true) == 0)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            modified = true;
                            break;
                        }
                    }

                }
                return modified;
            }
        }

        [Browsable(false)]
        public object FormValueObject
        {
            get
            {
                return FormValue;
            }
        }

        public string Validate(ErrorProvider errProvider)
        {
            return _myControl.Validate(errProvider);
        }

        public bool HasValidation
        {
            get
            {
                return _myControl.HasValidation;
            }
            set
            {
                _myControl.HasValidation = value;
            }
        }

        public string ValidationGroup
        {
            get
            {
                return _myControl.ValidationGroup;
            }
            set
            {
                _myControl.ValidationGroup = value;
            }
        }

        public bool HasError
        {
            get
            {
                return _myControl.HasError;
            }
            set
            {
                _myControl.HasError = value;
            }
        }


        public string Title
        {
            get
            {
                return _myControl.Title;
            }
            set
            {
                _myControl.Title = value;
            }
        }

        #endregion


        private List<string> _OriginalValue = null;
        public List<string> OriginalValue
        {
            get { return _OriginalValue; }
            set 
            {
                _OriginalValue = value; 
                SetValue(_OriginalValue);
            }
        }
        public object OriginalValueObject
        {
            get { return OriginalValue; }
            set
            {
                if (value != null)
                {
                    SetValue(value);
                    
                }
                
            }
        }
        public void SetOriginalValues(string s)
        {
            SetValue(s, true);
        }
        public void SetOriginalValues(List<string> list)
        {
            SetValue(list, true);
        }
        public void SetOriginalValues(int i)
        {
            SetValue(i, true);
        }
        public void SetOriginalValues(List<int> list)
        {
            SetValue(list, true);
        }
        public void SetValue(object o)
        {
            SetValue(o, true);
        }
        private string selectListItem(int id)
        {
            bool ok = false;
            int index;
            CS.General_v3.Classes.ListItem listItem = this.GetListItemByText(id.ToString(),out index);
            if (listItem == null)
            {
                return selectListItem(id.ToString());
            }
            else
            {
                return selectListItem(listItem.Text);
            }
        }
        private string selectListItem(string txt)
        {
            for (int i = 0; i < this.Items.Count; i++)
            {
                string s = this.Items[i].ToString();
                if (string.Compare(s, txt, true) == 0)
                {
                    if (!this.SelectedIndices.Contains(i))
                        this.SelectedIndices.Add(i);
                    return txt;
                    break;
                }
            }
            return null;
        }
        public void SetValue(object o, bool SetAsOriginalValue)
        {
            this.SelectedIndices.Clear();
            List<string> selectedValues = new List<string>();
            if (o is string)
            {
                string s = selectListItem((string)o);
                if (s != null)
                    selectedValues.Add(s);

            }
            else if (o is int)
            {
                string s = selectListItem((int)o);
                if (s != null)
                    selectedValues.Add(s);
            }
            else if (o is List<object> || o is List<int> || o is List<string>)
            {
                List<object> list = new List<object>();
                if (o is List<object>)
                    list = (List<object>)o;
                else if (o is List<string>)
                {
                    List<string> listS = (List<string>)o;
                    for (int i = 0; i < listS.Count; i++)
                    {
                        list.Add(listS[i]);
                    }
                }
                else if (o is List<int>)
                {
                    List<int> listI = (List<int>)o;
                    for (int i = 0; i < listI.Count; i++)
                    {
                        list.Add(listI[i]);
                    }
                    
                }
                for (int i = 0; i < list.Count; i++)
                {
                    string s = null;
                    if (list[i] is string)
                        s = selectListItem((string)list[i]);
                    else
                        s = selectListItem((int)list[i]);
                    selectedValues.Add(s);
                }
            }
            if (SetAsOriginalValue)
            {
                this._OriginalValue = selectedValues;
            }
        }

        public void Clear()
        {
            this.OriginalValue = null;
        }
        [Browsable(false)]
        public List<CS.General_v3.Classes.ListItem> FormValue
        {
            get
            {
                List<CS.General_v3.Classes.ListItem> list = new List<CS.General_v3.Classes.ListItem>();
                for (int i = 0; i < this.SelectedIndices.Count; i++)
                {
                    int selIndex = this.SelectedIndices[i];
                    int index;
                    CS.General_v3.Classes.ListItem l = GetListItemByText((string)this.Items[selIndex], out index);
                    if (l == null)
                    {
                        l = new CS.General_v3.Classes.ListItem(this.Items[selIndex].ToString(), this.Items[selIndex]);
                    }
                    list.Add(l);
                }
                return list;
            }
        }



        #region IMyListItemControl Members

        public List<CS.General_v3.Classes.ListItem> GetListItems()
        {
            return _myListItemControl.GetListItems();
            
        }

        public void SetListItems(List<CS.General_v3.Classes.ListItem> value)
        {
            _myListItemControl.SetListItems(value);
            
        }

        public void AddListItem(string text, object value)
        {
            _myListItemControl.AddListItem(text, value);
            
        }

        public void RemoveListItem(string text, string value)
        {
            _myListItemControl.RemoveListItem(text, value);
            
        }

        public void RemoveListItems()
        {
            _myListItemControl.RemoveListItems();
            
        }

        public CS.General_v3.Classes.ListItem GetListItemByText(string txt,out int index)
        {
            return _myListItemControl.GetListItemByText(txt,out index);
        }
        public CS.General_v3.Classes.ListItem GetListItemByValue(string txt, out int index)
        {
            return _myListItemControl.GetListItemByValue(txt, out index);
        }

        #endregion
        public string GetFormValueAsString()
        {
            string s = "";
            List<CS.General_v3.Classes.ListItem> list = FormValue;
            if (list != null)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    if (i > 0)
                        s += ", ";
                    s += list[i].Value;
                }
            }
            return s;
        }
    }
}
