﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CS.General_v3.Controls.FormControls.Common
{
    public partial class MyMaskedTextBox : MaskedTextBox, General.IMyTxtBox
    {

        protected General.MyMaskedTxtBox _myControl = null;
        public bool IsFilled
        {
            get
            {


                return !string.IsNullOrEmpty(this.FormValue);
            }
        }
        
        public MyMaskedTextBox()
        {
            _myControl = new CS.General_v3.Controls.FormControls.Common.General.MyMaskedTxtBox(this);
            InitializeComponent();
            
        }

        #region IMyFormControl Members

        public IButtonControl AcceptButton
        {
            get
            {
                return _myControl.AcceptButton;
            }
            set
            {
                _myControl.AcceptButton = value;
            }
        }

        public IButtonControl CancelButton
        {
            get
            {
                return _myControl.CancelButton;
            }
            set
            {
                _myControl.CancelButton = value;
            }
        }

        public bool NotPartOfForm
        {
            get
            {
                return _myControl.NotPartOfForm;
            }
            set
            {
                _myControl.NotPartOfForm = value;
            }
        }
        
        public virtual bool IsModified
        {
            get
            {
                string origValue = (string)this.OriginalValue ?? "";
                return (string.Compare(origValue, this.FormValue, true) != 0);
                
            }
        }
        [Browsable(false)]
        public string FormValue
        {
            get
            {
                var maskFormat = this.TextMaskFormat;
                this.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
                string s = this.Text;
                this.TextMaskFormat = maskFormat;
                if (!string.IsNullOrEmpty(s))
                    s = this.Text;
                return s;
            }
        }
        [Browsable(false)]
        public virtual object FormValueObject
        {
            get
            {
                return FormValue;
            }
        }

        public string Validate(ErrorProvider errProvider)
        {
            return _myControl.Validate(errProvider);
        }

        public bool HasValidation
        {
            get
            {
                return _myControl.HasValidation;
            }
            set
            {
                _myControl.HasValidation = value;
            }
        }

        public string ValidationGroup
        {
            get
            {
                return _myControl.ValidationGroup;
            }
            set
            {
                _myControl.ValidationGroup = value;
            }
        }

        public bool HasError
        {
            get
            {
                return _myControl.HasError;
            }
            set
            {
                _myControl.HasError = value;
            }
        }


        public string Title
        {
            get
            {
                return _myControl.Title;
            }
            set
            {
                _myControl.Title = value;
            }
        }

        #endregion

        #region IMyTxtBox Members


        public bool SelectAllOnFocus
        {
            get
            {
                return _myControl.SelectAllOnFocus;
                
            }
            set
            {
                _myControl.SelectAllOnFocus = value;
                
            }
        }

        #endregion

        #region IMyFormControl Members


        //private object _OriginalValue = null;
        public object OriginalValue
        {
            get { return _myControl.OriginalValue; }
            set
            {
                _myControl.OriginalValue = value; 
                
            }
        }
        public virtual object OriginalValueObject
        {
            get { return OriginalValue; }
            set
            {
               
                   
                    OriginalValue = value;
                    
            }
        }
        public void SetValue(object o)
        {
            _myControl.SetValue(o);
            
        }

        public void SetValue(object o, bool SetAsOriginalValue)
        {
            _myControl.SetValue(o, SetAsOriginalValue);
            
        }

        #endregion
        #region IMyFormControlRequireable Members
        [Category("Validation")]
        public bool Required
        {
            get
            {
                return _myControl.Required;
            }
            set
            {
                _myControl.Required = value;
            }
        }

        public string RequiredGroup
        {
            get
            {
                return _myControl.RequiredGroup;
            }
            set
            {
                _myControl.RequiredGroup = value;
            }
        }
        public string GetFormValueAsString()
        {
            return FormValue;
        }
        #endregion
    }
}
