﻿namespace CS.General_v3.Controls.FormControls.Common
{
    partial class MyFileInput
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.btnInput = new CS.General_v3.Controls.FormControls.Common.MyButton();
            this.txtInput = new CS.General_v3.Controls.FormControls.Common.MyTxtBoxTextSingleMultiLine();
            this.SuspendLayout();
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // btnInput
            // 
            this.btnInput.AdjustImageLocation = new System.Drawing.Point(0, 0);
            this.btnInput.BtnShape = CS.General_v3.Controls.FormControls.Common.emunType.BtnShape.Rectangle;
            this.btnInput.BtnStyle = CS.General_v3.Controls.FormControls.Common.emunType.XPStyle.Default;
            this.btnInput.Location = new System.Drawing.Point(364, 87);
            this.btnInput.Name = "btnInput";
            this.btnInput.Size = new System.Drawing.Size(21, 23);
            this.btnInput.TabIndex = 1;
            this.btnInput.Text = "...";
            this.btnInput.UseVisualStyleBackColor = true;
            this.btnInput.Click += new System.EventHandler(this.btnInput_Click);
            // 
            // txtInput
            // 
            this.txtInput.AcceptButton = null;
            this.txtInput.CancelButton = null;
            this.txtInput.HasError = false;
            this.txtInput.HasValidation = true;
            this.txtInput.Location = new System.Drawing.Point(26, 15);
            this.txtInput.Name = "txtInput";
            this.txtInput.NotPartOfForm = false;
            this.txtInput.Required = false;
            this.txtInput.RequiredGroup = "";
            this.txtInput.SelectAllOnFocus = true;
            this.txtInput.Size = new System.Drawing.Size(100, 20);
            this.txtInput.TabIndex = 0;
            this.txtInput.Title = "";
            // 
            // MyFileInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnInput);
            this.Controls.Add(this.txtInput);
            this.Name = "MyFileInput";
            this.Size = new System.Drawing.Size(538, 185);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MyTxtBoxTextSingleMultiLine txtInput;
        private MyButton btnInput;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
    }
}
