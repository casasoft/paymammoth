﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using System.CodeDom;

namespace CS.General_v3.Controls.FormControls.Common
{
    public class MyHScrollBar : HScrollBar,General.IMyControl
    {
        protected General.MyControl _myControl = null;
        public new int Value
        {
            get
            {
                int value = base.Value;
                int newValue = Convert.ToInt32(((double)value / (double)(this.Maximum - LargeChange + 1)) * (double)this.Maximum);
                return newValue;
            }
            set
            {
                base.Value = value;
            }
        }
        public MyHScrollBar()
        {
            _myControl = new CS.General_v3.Controls.FormControls.Common.General.MyControl(this);
        }
        
    }
}
