﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace CS.General_v3.Controls.FormControls.Common.General
{
    public class MyControl : IMyControl
    {
        public MyControl(Control ctrl)
        {
            _control = ctrl;
        }
        protected Control _control = null;


        #region IMyControl Members

        public object Tag
        {
            get
            {
                return _control.Tag;
            }
            set
            {
                _control.Tag = value;
            }
        }

        #endregion
    }
}
