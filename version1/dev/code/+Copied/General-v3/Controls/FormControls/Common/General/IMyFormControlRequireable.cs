﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CS.General_v3.Controls.FormControls.Common.General
{
    interface IMyFormControlRequireable : IMyFormControl
    {
        bool Required { get; set; }
        string RequiredGroup { get; set; }
    }
}
