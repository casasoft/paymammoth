﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CS.General_v3.Controls.FormControls.Common
{
    public partial class MyFileInput : UserControl, General.IMyFormControlRequireable
    {
        private General.MyFormControlRequireable _myControl = null;
        public MyFileInput()
        {
            _myControl = new CS.General_v3.Controls.FormControls.Common.General.MyFormControlRequireable(this);
            InitializeComponent();
            this.InputType = INPUT_TYPE.OpenDialog;
            this.Height = 20;
            initHandlers();
        }
        private void initHandlers()
        {
            txtInput.TextChanged += new EventHandler(txtInput_TextChanged);
        }
        public event EventHandler TextChanged;
        void txtInput_TextChanged(object sender, EventArgs e)
        {
            if (TextChanged != null)
                TextChanged(this, e);
            
        }
        public enum INPUT_TYPE
        {
            SaveDialog,
            OpenDialog
        }
        #region Properties
        public INPUT_TYPE InputType { get; set; }
        public string Filter { get; set; }
        public bool CheckPathExists { get; set; }
        public bool CheckFileExists { get; set; }
        public bool CreatePrompt { get; set; }
        public string DefaultExt { get; set; }
        public bool OverwritePrompt { get; set; }
        public string InitialDirectory { get; set; }
        public bool AddExtension { get; set; }
        public string TitleOfDialog { get; set; }
        public string LastValue
        {
            get
            {
                return this.txtInput.LastValue;
                
            }

        }
        public string Text
        {
            get
            {
                return this.txtInput.Text;
            }
            set
            {
                this.txtInput.Text = value;
            }
        }
        public string FormValue
        {
            get
            {
                return txtInput.Text;
            }
        }
        #endregion

        private void setDialogProperties()
        {
            openFileDialog.Title = saveFileDialog.Title = TitleOfDialog;
            openFileDialog.Multiselect = false;
            
            openFileDialog.AddExtension = saveFileDialog.AddExtension = AddExtension;
            openFileDialog.Filter = saveFileDialog.Filter = Filter;
            openFileDialog.CheckPathExists = saveFileDialog.CheckPathExists = CheckPathExists;
            openFileDialog.CheckFileExists = saveFileDialog.CheckFileExists = CheckFileExists;
            saveFileDialog.CreatePrompt = CreatePrompt;
            openFileDialog.DefaultExt = saveFileDialog.DefaultExt = DefaultExt;
            openFileDialog.InitialDirectory = saveFileDialog.InitialDirectory = InitialDirectory;
            openFileDialog.AddExtension = saveFileDialog.AddExtension = AddExtension;

        }

        private void positionUI()
        {
            this.Height = 20;
            txtInput.Left = 0;
            txtInput.Top = 0;
            txtInput.Width = this.Width - btnInput.Width + 1;
            txtInput.Height = this.Height;

            btnInput.Height = this.Height;
            btnInput.Top = 0;
            btnInput.Left = txtInput.Left + txtInput.Width-1;
            
        }
        protected override void OnResize(EventArgs e)
        {
            positionUI();
            base.OnResize(e);
        }
       

        private void btnInput_Click(object sender, EventArgs e)
        {
            setDialogProperties();
            FileDialog dialog = null;
            if (InputType == INPUT_TYPE.SaveDialog)
            {
                dialog = saveFileDialog;
            }
            else
            {
                dialog = openFileDialog;
            }
            if (System.IO.Directory.Exists(CS.General_v3.Util.IO.GetDirName(txtInput.FormValue)))
                dialog.InitialDirectory = CS.General_v3.Util.IO.GetDirName(txtInput.FormValue);
            dialog.FileName = CS.General_v3.Util.IO.GetFilenameAndExtension(txtInput.FormValue);
            DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtInput.Text = dialog.FileName;
            }
             
        }

        #region IMyFormControl Members

        public IButtonControl AcceptButton
        {
            get
            {
                return _myControl.AcceptButton;
            }
            set
            {
                _myControl.AcceptButton = value;
            }
        }

        public IButtonControl CancelButton
        {
            get
            {
                return _myControl.CancelButton;
            }
            set
            {
                _myControl.CancelButton = value;
            }
        }

        public bool NotPartOfForm
        {
            get
            {
                return _myControl.NotPartOfForm;
            }
            set
            {
                _myControl.NotPartOfForm = value;
            }
        }

        public bool IsModified
        {
            get
            {
                return this.FormValue != this.OriginalValue;
            }
        }

        public object FormValueObject
        {
            get
            {
                return FormValue;
            }
        }

        public string Validate(ErrorProvider errProvider)
        {
            return _myControl.Validate(errProvider);
        }

        public bool HasValidation
        {
            get
            {
                return _myControl.HasValidation;
            }
            set
            {
                _myControl.HasValidation = value;
            }
        }

        public string ValidationGroup
        {
            get
            {
                return _myControl.ValidationGroup;
            }
            set
            {
                _myControl.ValidationGroup = value;
            }
        }

        public bool HasError
        {
            get
            {
                return _myControl.HasError;
            }
            set
            {
                _myControl.HasError = value;
            }
        }


        public string Title
        {
            get
            {
                return _myControl.Title;
            }
            set
            {
                this.txtInput.Title = _myControl.Title = value;
                
            }
        }

        #endregion


        #region IMyFormControl Members
        public bool IsFilled
        {
            get
            {
                return !string.IsNullOrEmpty(txtInput.Text);
            }
        }

        public void Clear()
        {
            txtInput.Text = "";
        }
        private string _OriginalText = null;

        public string OriginalValue
        {
            get { return _OriginalText; }
            set { _OriginalText = value; SetValue(value, false); }
        }
        public object OriginalValueObject
        {
            get
            {
                return OriginalValue;
            }
            set
            {
                OriginalValue = (string)value;
            }
        }

        public void SetValue(object o)
        {
            SetValue(o, true);
        }

        public void SetValue(object o, bool SetAsOriginalValue)
        {
            if (o == null) o = "";
            string s = o.ToString();
            if (SetAsOriginalValue)
                OriginalValue = s;
            else
                this.Text = s;
            
        }

        #endregion

        #region IMyFormControlRequireable Members

        public bool Required
        {
            get
            {
                return _myControl.Required;
            }
            set
            {
                _myControl.Required = value;
            }
        }

        public string RequiredGroup
        {
            get
            {
                return _myControl.RequiredGroup;
            }
            set
            {
                _myControl.RequiredGroup = value;
            }
        }

        #endregion
        public string GetFormValueAsString()
        {
            return FormValue;
        }
    }
}
