﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using System.CodeDom;

namespace CS.General_v3.Controls.FormControls.Common
{
    public class MyTxtBoxNumeric : MyTxtBox
    {
        public override bool IsModified
        {
            get
            {
                if (NotPartOfForm)
                    return false;
                else
                {
                    double? d1 = this.FormValue;
                    double? d2 = this.OriginalValue;
                    if (d1.HasValue && ((int)d1 == Int32.MinValue || d1 == double.MinValue)) d1 = null;
                    if (d2.HasValue && ((int)d2 == Int32.MinValue || d2 == double.MinValue)) d2 = null;
                    if ((d1 == null && d2 == null))
                        return false;
                    else if ((d1 != null && d2 == null) || (d1 == null && d2 != null))
                        return true;
                    else
                    {
                        return d1.Value.CompareTo(d2.Value) != 0;
                    }
                    
                }
            }
        }
        private bool checkingText = false;
        protected void checkText(bool integersOnly,bool positiveOnly)
        {
            if (!checkingText)
            {
                checkingText = true;

                int lastSel = this.SelectionStart;

                bool oneCharChanged = false;
                if ((_lastText.Length + 1 == this.Text.Length) ||
                    (_lastText.Length - 1 == this.Text.Length))
                {
                    oneCharChanged = true;
                }
                bool foundDot = false;
                bool removed = false;
                string s = this.Text;
                for (int i = 0; i < s.Length; i++)
                {
                    char c = s[i];

                    if ((c >= '0' && c <= '9') || (i == 0 && c == '-' && !positiveOnly) || (i == 0 && c == '+') || (c == '.' && !foundDot && !integersOnly))
                    {
                        if (c == '.')
                            foundDot = true;
                    }
                    else
                    {
                        removed = true;
                        s = s.Remove(i, 1);
                        i--;
                    }

                }
                this.Text = s;
                if (removed)
                {
                    if (oneCharChanged && lastSel > 0)
                        lastSel--;

                    this.SelectionStart = lastSel;
                    this.SelectionLength = 0;
                }
                checkingText = false;
            }


        }
        protected string _lastText = "";
        protected override void OnTextChanged(EventArgs e)
        {
            
            checkText(false, this.PositiveNumbersOnly);
            _lastText = this.Text;
            base.OnTextChanged(e);
        }

        public MyTxtBoxNumeric() : base()
        {
            _myControl.Validation.IsNumeric = true;
        }
        
        #region Validation Parameters
        
        [Category("Validation")]
        public bool PositiveNumbersOnly
        {
            get { return _myControl.Validation.PositiveNumsOnly; }
            set { _myControl.Validation.PositiveNumsOnly = value; }
        }

        /// <summary>
        /// ///////////////////
        /// </summary>



        #endregion
        #region Form Methods
        private double? _InitValue = null;
        [Category("Validation")]
        public new double? InitValue
        {
            get
            {
                return _InitValue;
            }
            set
            {
                _InitValue = value;
                if (string.IsNullOrEmpty(this.Text))
                {
                    if (_InitValue != null)
                        this.Text = _InitValue.Value.ToString();
                    else
                        this.Text = "";
                }
            }
        }
        private double? _OriginalValue = null;
        public double? OriginalValue
        {
            get 
            {
                if (_myControl.OriginalValueObject == null)
                    return null;
                else
                    return Convert.ToDouble(_myControl.OriginalValueObject);
            }
            set {

                _OriginalValue = value;
                SetValue(_OriginalValue, false);
                 }
        }
        
        [Browsable(false)]
        public override object OriginalValueObject
        {
            get { return _myControl.OriginalValueObject; }
            set 
            {
                if (value != null && (!(value is double) && !(value is int)))
                {
                    throw new InvalidOperationException("Only doubles/ints or null can be set to a numeric field");
                }
                _myControl.OriginalValueObject = value;
                if (_myControl.OriginalValueObject is int)
                    _myControl.OriginalValueObject = Convert.ToDouble(_myControl.OriginalValueObject);
            }
        }

        #endregion

        [Browsable(false)]
        public new double? FormValue
        {
            get
            {
                return (double?)FormValueObject;
            }
        }
        public bool IntegersOnly
        {
            get
            {
                return _myControl.Validation.IntegersOnly;
            }
            set
            {
                _myControl.Validation.IntegersOnly = value;
            }
            
        }
        [Browsable(false)]
        public int? FormValueInt
        {
            get
            {
                if (FormValueObject == null)
                    return null;
                else
                    return Convert.ToInt32(FormValueObject);
                
            }
        }
        [Browsable(false)]
        public override object FormValueObject
        {
            get
            {
                string s = (string)base.FormValueObject;
                double? d = null;
                if (s == "-" || s == "+")
                    s = "";
                else if (s.EndsWith("."))
                    s = s.Substring(0, s.Length - 1);
                if (!string.IsNullOrEmpty(s))
                    d = Util.Conversion.ToDouble(s);
                return d;
            }
            
        }


        
        /// <summary>
        /// Sets the text to the object value
        /// </summary>
        /// <param name="o"></param>
        public void SetValue(object o)
        {
            _myControl.SetValue(o);
            
        }
        /// <summary>
        /// Sets the text to the object value
        /// </summary>
        /// <param name="o"></param>
        public void SetValue(object o, bool setAsOriginalValue)
        {
            _myControl.SetValue(o,setAsOriginalValue);

        }
        public override string GetFormValueAsString()
        {
            string s = "";
            if (FormValue != null)
            {
                if (!IntegersOnly)
                    s = FormValue.Value.ToString("0.0000");
                else
                    s = FormValueInt.Value.ToString();
            }
            return s;
        }
        [Category("Validation")]
        [DefaultValue(Int32.MinValue)]
        public double NumberFrom
        {
            get { return _myControl.Validation.NumberFrom; }
            set { _myControl.Validation.NumberFrom = value; }
        }
        [Category("Validation")]
        [DefaultValue(Int32.MaxValue)]
        public double NumberTo
        {
            get
            {

                return _myControl.Validation.NumberTo;
            }
            set { _myControl.Validation.NumberTo = value; }
        }
    }
}
