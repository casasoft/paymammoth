﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CS.General_v3.Controls.FormControls.Common.General
{
    interface IMyFormControl : IMyControl
    {
        bool IsFilled { get;  }
        IButtonControl AcceptButton { get; set; }
        IButtonControl CancelButton { get; set; }
        bool NotPartOfForm { get; set; }
        bool IsModified { get; }
        object FormValueObject { get; }
        void Clear();
        string GetFormValueAsString();
        object OriginalValueObject { get; set; }
        string Validate(ErrorProvider errProvider);
        bool HasValidation { get; set; }
        string ValidationGroup { get; set; }
        bool HasError { get; set; }
        void SetValue(object o);
        void SetValue(object o, bool SetAsOriginalValue);
        string Title { get; set; }
    }
}
