﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using System.CodeDom;

namespace CS.General_v3.Controls.FormControls.Common
{
    public class MyPanel : Panel
    {
        #region MouseMove
        private MouseEventHandler lastMouseMoveHandler;
        public void AttachMouseMove(MouseEventHandler eventHandler)
        {
            CS.General_v3.Util.WindowsForms.Events.AttachMouseMoveListener(this, eventHandler);
            lastMouseMoveHandler = eventHandler;
        }
        public void DetachLastMouseMove()
        {
            if (lastMouseMoveHandler != null)
            {
                DetachMouseMove(lastMouseMoveHandler);
            }
        }
        public void DetachMouseMove(MouseEventHandler eventHandler)
        {
            CS.General_v3.Util.WindowsForms.Events.DetachMouseMoveListener(this, eventHandler);
        }
        #endregion
        #region MouseEnter
        private EventHandler lastMouseEnterHandler;
        public void AttachMouseEnter(EventHandler eventHandler)
        {
            CS.General_v3.Util.WindowsForms.Events.AttachMouseEnterListener(this, eventHandler);
            lastMouseEnterHandler = eventHandler;
        }
        public void DetachLastMouseEnter()
        {
            if (lastMouseEnterHandler != null)
            {
                DetachMouseEnter(lastMouseEnterHandler);
            }
        }
        public void DetachMouseEnter(EventHandler eventHandler)
        {
            CS.General_v3.Util.WindowsForms.Events.DetachMouseEnterListener(this, eventHandler);
        }
        #endregion
        #region MouseLeave
        private EventHandler lastMouseLeaveHandler;
        public void AttachMouseLeave(EventHandler eventHandler)
        {
            CS.General_v3.Util.WindowsForms.Events.AttachMouseLeaveListener(this, eventHandler);
            lastMouseLeaveHandler = eventHandler;
        }
        public void DetachLastMouseLeave()
        {
            if (lastMouseLeaveHandler != null)
            {
                DetachMouseLeave(lastMouseLeaveHandler);
            }
        }
        public void DetachMouseLeave(EventHandler eventHandler)
        {

            CS.General_v3.Util.WindowsForms.Events.DetachMouseLeaveListener(this, eventHandler);
        }
        #endregion
        public void SetEnabled(bool b)
        {
            CS.General_v3.Util.WindowsForms.ChangeControlEnabled(this, b);

        }
        public new bool Enabled
        {
            get
            {
                return base.Enabled;
            }
            set
            {
                base.Enabled = value;
                SetEnabled(base.Enabled);
            }

        }

    }
}
