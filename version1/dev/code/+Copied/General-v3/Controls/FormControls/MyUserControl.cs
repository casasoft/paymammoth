﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Reflection;
using System.Runtime.InteropServices;
using System.ComponentModel;
namespace CS.General_v3.Controls.FormControls.Common
{
    public class MyUserControl : UserControl
    {

        public new MyForm ParentForm
        {
            get
            {
                return (MyForm)base.ParentForm;
            }
        }

        public ErrorProvider ErrorProvider { get; set; }

    }
}
