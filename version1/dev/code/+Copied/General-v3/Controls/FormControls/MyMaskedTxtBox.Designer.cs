﻿namespace CS.General_20090518.Controls.FormControls
{
    partial class MyMaskedTextBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MyMaskedTextBox));
            this.CalendarMonthCalendar = new System.Windows.Forms.MonthCalendar();
            this.CalendarButton = new CS.General_20090518.Controls.FormControls.MyButton();
            this.SuspendLayout();
            // 
            // CalendarMonthCalendar
            // 
            this.CalendarMonthCalendar.FirstDayOfWeek = System.Windows.Forms.Day.Monday;
            this.CalendarMonthCalendar.Location = new System.Drawing.Point(0, 0);
            this.CalendarMonthCalendar.MaxSelectionCount = 1;
            this.CalendarMonthCalendar.Name = "CalendarMonthCalendar";
            this.CalendarMonthCalendar.TabIndex = 0;
            this.CalendarMonthCalendar.Visible = false;
            this.CalendarMonthCalendar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.CalendarMonthCalendar_MouseUp);
            // 
            // CalendarButton
            // 
            this.CalendarButton.AdjustImageLocation = new System.Drawing.Point(0, 0);
            this.CalendarButton.BtnShape = CS.General_20090518.Controls.FormControls.emunType.BtnShape.Rectangle;
            this.CalendarButton.BtnStyle = CS.General_20090518.Controls.FormControls.emunType.XPStyle.Default;
            this.CalendarButton.Image = ((System.Drawing.Image)(resources.GetObject("CalendarButton.Image")));
            this.CalendarButton.Location = new System.Drawing.Point(0, 0);
            this.CalendarButton.Name = "CalendarButton";
            this.CalendarButton.Size = new System.Drawing.Size(75, 23);
            this.CalendarButton.TabIndex = 0;
            this.CalendarButton.UseVisualStyleBackColor = true;
            this.ResumeLayout(false);

        }

        #endregion

        private MyButton CalendarButton;
        private System.Windows.Forms.MonthCalendar CalendarMonthCalendar;

    }
}
