﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using System.CodeDom;

namespace CS.General_20090518.Controls.FormControls
{
    public class MyListView : ListView
    {
        public void SelectAllCheckBoxes()
        {
            if (this.CheckBoxes)
            {
                for (int i = 0; i < this.Items.Count; i++)
                {
                    this.Items[i].Checked = true;
                }
            }
        }
        public void InvertCheckBoxes()
        {
            if (this.CheckBoxes)
            {
                for (int i = 0; i < this.Items.Count; i++)
                {
                    this.Items[i].Checked = !this.Items[i].Checked;
                }
            }
        }
        
        public MyListView()
        {
            
        }
       
    }
}
