﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CS.General_20090518.Controls.FormControls
{
    public partial class MyFolderInput : UserControl
    {
        public MyFolderInput()
        {
            InitializeComponent();
            this.Height = 20;
        }
        
        #region Properties
        public Environment.SpecialFolder RootFolder { get; set; }
        public string FormValue
        {
            get
            {
                return txtInput.Text;
            }
        }
        public string Text
        {
            get
            {
                return this.txtInput.Text;
            }
            set
            {
                this.txtInput.Text = value;
            }
        }
        #endregion

        private void setDialogProperties()
        {
            folderBrowserDialog.RootFolder = RootFolder;

        }

        private void positionUI()
        {
            txtInput.Left = 0;
            txtInput.Top = 0;
            txtInput.Width = this.Width - btnInput.Width+1;
            txtInput.Height = this.Height;

            btnInput.Height = this.Height;
            btnInput.Left = txtInput.Left + txtInput.Width-1;
            
        }

        protected override void OnResize(EventArgs e)
        {
            positionUI();
            base.OnResize(e);
        }
        
        private void btnInput_Click(object sender, EventArgs e)
        {
            setDialogProperties();
            DialogResult result = folderBrowserDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtInput.Text = folderBrowserDialog.SelectedPath;
            }
             
        }
    }
}
