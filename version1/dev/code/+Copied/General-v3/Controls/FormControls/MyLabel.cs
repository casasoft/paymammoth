﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CS.General_20090518.Controls.FormControls
{
    public partial class MyLabel : Label
    {
        private CS.General_20090518.Classes.Validation _Validation = new CS.General_20090518.Classes.Validation();


        public MyLabel()
        {
            InitializeComponent();
            Click += new EventHandler(MyLabel_Click);
            
        }
        private Control _CtrlTabIndex = null;
        private Control findControlOfNextTabIndex()
        {
            long leastTabIndex = long.MaxValue;
            long thisTabIndex = CS.General_20090518.WindowsForms.CalculateRelativeTabIndex(this);
            Control leastControl = null;

            List<Control> list = new List<Control>();
            list.Add(this.FindForm());
            int i = 0;
            while (i < list.Count)
            {
                long currTabIndex = CS.General_20090518.WindowsForms.CalculateRelativeTabIndex(list[i]);
                if (currTabIndex > thisTabIndex && currTabIndex < leastTabIndex)
                {
                    leastTabIndex = currTabIndex;
                    leastControl = list[i];
                }
                for (int j = 0; j < list[i].Controls.Count; j++)
                {
                    list.Add(list[i].Controls[j]);
                }
                i++;
            }
            return leastControl;

        }
       
        void MyLabel_Click(object sender, EventArgs e)
        {
            
            if (_CtrlTabIndex == null)
                _CtrlTabIndex = findControlOfNextTabIndex();
            if (_CtrlTabIndex != null)
            {
                _CtrlTabIndex.Select();
            }
            //SelectNextControl(this, true, true, true, false);
            
        }

    }
}
