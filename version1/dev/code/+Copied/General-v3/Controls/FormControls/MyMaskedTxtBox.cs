﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CS.General_20090518.Controls.FormControls
{
    public partial class MyMaskedTextBox : MaskedTextBox
    {
        #region Accept/Cancel Buttons
        private IButtonControl _AcceptButton = null;
        private IButtonControl _CancelButton = null;
        private IButtonControl _LastAcceptButton = null;
        private IButtonControl _LastCancelButton = null;
        
        [Description("The Accept button to select, when this field has focus")]
        public IButtonControl AcceptButton
        {
            get
            {
                return _AcceptButton;
            }
            set
            {
                _AcceptButton = value;
            }
        }
        [Description("The Cancel button to select, when this field has focus")]
        public IButtonControl CancelButton
        {
            get
            {
                return _CancelButton;
            }
            set
            {
                _CancelButton = value;
            }
        }
        private void CheckAcceptCancelGotFocus()
        {
            if (this.AcceptButton != null)
            {
                _LastAcceptButton = this.FindForm().AcceptButton;
                this.FindForm().AcceptButton = this.AcceptButton;
            }
            if (this.CancelButton != null)
            {
                _LastCancelButton = this.FindForm().CancelButton;
                this.FindForm().CancelButton = this.CancelButton;
            }
        }
        private void CheckAcceptCancelLostFocus()
        {
            if (this.AcceptButton != null)
            {
                this.FindForm().AcceptButton = _LastAcceptButton;
            }
            if (this.CancelButton != null)
            {
                this.FindForm().CancelButton = _LastCancelButton;
            }
        }
        #endregion
        public enum Mask_Mode
        {
            Date = 10,
            DateTime = 20,
            Custom = 30
        }
        public enum ALIGNMENT
        {
            TopLeft,TopRight,BottomLeft,BottomRight
        }
        private CS.General_20090518.Classes.Validation _Validation = new CS.General_20090518.Classes.Validation();
        #region Form Methods
        private string _OriginalText = null;
        private bool _NotPartOfForm = false;

        public bool NotPartOfForm
        {
            get { return _NotPartOfForm; }
            set { _NotPartOfForm = value; }
        }
        public string OriginalText
        {
            get { return _OriginalText; }
            set { this.Text = value; _OriginalText = this.Text; }
        }
        public bool IsModified
        {
            get
            {
                if (NotPartOfForm)
                    return false;
                else
                    return this.Text != OriginalText;
            }
        }
        #endregion
        #region Validation Parameters
        private bool _HasError = false;
        [Category("Validation")]
        
        public bool HasError
        {
            get { return _HasError; }
            set { _HasError = value; }
        }
        [Category("Validation")]
        public bool HasValidation
        {
            get { return _Validation.HasValidation; }
            set { _Validation.HasValidation = value; }
        }
        [Category("Validation")]
        public bool Required
        {
            get { return _Validation.IsRequired; }
            set { _Validation.IsRequired = value; }
        }
        [Category("Validation")]
        public bool IsNumeric
        {
            get { return _Validation.IsNumeric; }
            set { _Validation.IsNumeric = value; }
        }
        [Category("Validation")]
        public bool PositiveNumbersOnly
        {
            get { return _Validation.PositiveNumsOnly; }
            set { _Validation.PositiveNumsOnly = value; }
        }
        [Category("Validation")]
        public bool IntegersOnly
        {
            get { return _Validation.IntegersOnly; }
            set { _Validation.IntegersOnly = value; }
        }
        [Category("Validation")]
        public bool IsDate
        {
            get { return _Validation.IsDate; }
            set { _Validation.IsDate = value; }
        }
        [Category("Validation")]
        public double NumberFrom
        {
            get { return _Validation.NumberFrom; }
            set { _Validation.NumberFrom = value; }
        }
        [Category("Validation")]
        public double NumberTo
        {
            get { return _Validation.NumberTo; }
            set { _Validation.NumberTo = value;  }
        }
        [Category("Validation")]
        public DateTime DateFrom
        {
            get { return _Validation.DateFrom; }
            set { _Validation.DateFrom = value; }
        }
        [Category("Validation")]
        public DateTime DateTo
        {
            get { return _Validation.DateTo; }
            set { _Validation.DateTo = value; }
        }
        [Category("Validation")]
        public string DateAfterTxtID
        {
            get { return _Validation.DateAfterTxtID; }
            set { _Validation.DateAfterTxtID = value; }

        }
        [Category("Validation")]
        public string DateBeforeTxtID
        {
            get { return _Validation.DateBeforeTxtID; }
            set { _Validation.DateBeforeTxtID = value; }

        }
        [Category("Validation")]
        public string Title
        {
            get { return _Validation.Title; }
            set { _Validation.Title = value; }
        }
        [Category("Validation")]
        public string ValidationGroup
        {
            get { return _Validation.ValidationGroup; }
            set { _Validation.ValidationGroup = value; }
        }
        [Category("Validation")]
        public string RequiredGroup
        {
            get { return _Validation.RequiredGroup; }
            set { _Validation.RequiredGroup = value; }
        }
        
        #endregion
        #region Date Calendar
        private bool _HasCalendar = true;
        private bool _CheckedCalendar = false;
        
        
        //private MyButton _CalendarButton = null;
        //private MonthCalendar CalendarMonthCalendar = null;
        private ALIGNMENT _CalendarShowLocation = ALIGNMENT.TopRight;
        private MouseEventHandler closeMouseClickEvent = null;
        [Description("Whether the date is accompanied by a calendar button")]
        [Category("Calendar")]
        public bool HasCalendar
        {
            get { return _HasCalendar; }
            set { _HasCalendar = value; }
        }
        private void ShowCalendar()
        {

            int padding = 0;
            Form form = this.FindForm();
            
            if (CalendarMonthCalendar.Parent == null)
            {
                //CalendarMonthCalendar = new MonthCalendar();
                this.FindForm().Controls.Add(CalendarMonthCalendar);
                CalendarMonthCalendar.BringToFront();
                CalendarMonthCalendar.DateChanged += new DateRangeEventHandler(CalendarMonthCalendar_DateChanged);
            }
            DateTime d;
            if (!DateTime.TryParse(this.Text, out d))
            {
                d = CS.General_20090518.Date.Now();
            }
            try
            {
                CalendarMonthCalendar.SelectionStart = CalendarMonthCalendar.SelectionEnd = d;
            }
            catch
            {
                d = CS.General_20090518.Date.Now();
                CalendarMonthCalendar.SelectionStart = CalendarMonthCalendar.SelectionEnd = d;
            }

            CS.General_20090518.WindowsForms.Events.AttachMouseDownListener(this.FindForm(), closeMouseClickEvent);
            CalendarMonthCalendar.MouseDown -= closeMouseClickEvent;
            CalendarButton.MouseDown -= closeMouseClickEvent;
            int x,y;
            CS.General_20090518.WindowsForms.Mouse.GetGlobalXY(CalendarButton, out x, out y);
            bool invalidLoc = false;
            if ((_CalendarShowLocation == ALIGNMENT.TopLeft && (x - CalendarMonthCalendar.Width < 0 || y - CalendarMonthCalendar.Height < 0)) ||
                (_CalendarShowLocation == ALIGNMENT.TopRight && (x + CalendarMonthCalendar.Width > form.Width || y - CalendarMonthCalendar.Height < 0)) ||
                (_CalendarShowLocation == ALIGNMENT.BottomLeft && (x - CalendarMonthCalendar.Width < 0 || y + CalendarMonthCalendar.Height + CalendarButton.Height > form.Height)) ||
                (_CalendarShowLocation == ALIGNMENT.BottomRight && (x + CalendarMonthCalendar.Width > form.Width || y + CalendarMonthCalendar.Height + CalendarButton.Height > form.Height)))
            {
                if (x + CalendarMonthCalendar.Width < form.Width && y - CalendarMonthCalendar.Height > 0)
                {
                    _CalendarShowLocation = ALIGNMENT.TopRight;
                }
                else if (x - CalendarMonthCalendar.Width > 0 && y - CalendarMonthCalendar.Height > 0)
                {
                    _CalendarShowLocation = ALIGNMENT.TopLeft;
                }
                else if (x - CalendarMonthCalendar.Width > 0 && y + CalendarMonthCalendar.Height + CalendarButton.Height < form.Height)
                {
                    _CalendarShowLocation = ALIGNMENT.BottomLeft;
                }
                else 
                {
                    _CalendarShowLocation = ALIGNMENT.BottomRight;
                }
            }
            switch (_CalendarShowLocation)
            {
                case ALIGNMENT.BottomLeft:
                    {
                        x = x - CalendarMonthCalendar.Width + CalendarButton.Width;
                        y = y + CalendarButton.Height + padding; 
                    }
                    break;
                case ALIGNMENT.BottomRight:
                    {
                        x = x;
                        y = y + CalendarButton.Height + padding;
                    }
                    break;
                case ALIGNMENT.TopLeft:
                    {
                        x = x - CalendarMonthCalendar.Width + CalendarButton.Width;
                        y = y - CalendarMonthCalendar.Height - padding;
                    }
                    break;
                case ALIGNMENT.TopRight:
                    {
                        x = x;
                        y = y - CalendarMonthCalendar.Height - padding;
                    }
                    break;
            }
            CalendarMonthCalendar.Left = x;
            CalendarMonthCalendar.Top = y;
            CalendarMonthCalendar.Visible = true;
        }

        void CalendarMonthCalendar_DateChanged(object sender, DateRangeEventArgs e)
        {
            DateTime dOld;
            int hour = 0, minute = 0, second = 0;
            if (DateTime.TryParse(this.Text, out dOld))
            {
                hour = dOld.Hour;
                minute = dOld.Minute;
                second = dOld.Second;
            }

            DateTime d = CalendarMonthCalendar.SelectionStart;
            d = new DateTime(d.Year,d.Month,d.Day,hour,minute,second);
            if (this.MaskMode == Mask_Mode.Date)
                this.Text = d.ToString("dd/MM/yyyy");
            else if (this.MaskMode == Mask_Mode.DateTime)
            {
                this.Text = d.ToString("dd/MM/yyyy HH:mm");
            }
            //HideCalendar();

        }
        private void HideCalendar()
        {
            if (CalendarMonthCalendar != null && CalendarMonthCalendar.Visible)
            {
                CS.General_20090518.WindowsForms.Events.DetachMouseDownListener(this.FindForm(), closeMouseClickEvent);
                CalendarMonthCalendar.Visible = false;
            }
        }
        private void checkCalendarControls()
        {
            if (Site == null || !Site.DesignMode)
            {
                if (_HasCalendar && this.MaskMode != Mask_Mode.Custom)
                {
                    if (CalendarButton.Parent == null)
                    {

                        this.Parent.Controls.Add(CalendarButton);
                        CalendarButton.Left = this.Left + this.Width;
                        CalendarButton.Top = this.Top;
                        CalendarButton.Height = this.Height;
                        CalendarButton.Width = 24;
                        CalendarButton.Click += new EventHandler(CalendarButton_Click);

                        


                        closeMouseClickEvent = new MouseEventHandler(MouseClickClose_MouseClick);
                        
                        
                    }
                    
                }
            }
            _CheckedCalendar = true;
        }

        void MouseClickClose_MouseClick(object sender, EventArgs e)
        {
            HideCalendar();
        }

        void CalendarButton_Click(object sender, EventArgs e)
        {
            if (CalendarMonthCalendar == null || !CalendarMonthCalendar.Visible)
                ShowCalendar();
            else
                HideCalendar();
        }
        protected override void OnResize(EventArgs e)
        {
            if (!_CheckedCalendar)
                checkCalendarControls();
            base.OnResize(e);
        }
        
        
        
        #endregion
        private bool _SelectAllOnFocus = true;
        public bool SelectAllOnFocus
        {
            get
            {
                return _SelectAllOnFocus;
            }
            set
            {
                _SelectAllOnFocus = value;
            }
        }
        private void checkMaskMode()
        {
            if (_MaskMode == Mask_Mode.Date)
            {
                base.Mask = "00/00/0000";
                _Validation.IsDate = true;

            }
            else if (_MaskMode == Mask_Mode.DateTime)
            {
                base.Mask = "00/00/0000 00:00";
                _Validation.IsDate = true;
                _Validation.HasTime = true;
            }
            else if (_MaskMode == Mask_Mode.Custom)
            {
                _Validation.IsDate = false;
            }
        }
        private Mask_Mode _MaskMode = Mask_Mode.Date;
        public Mask_Mode MaskMode
        {
            get
            {
                return _MaskMode;
            }
            set
            {
                _MaskMode = value;
                checkMaskMode();
            }
        }
        public new string Mask
        {
            get
            {
                return base.Mask;
            }
            set
            {
                if (MaskMode == Mask_Mode.Custom)
                    base.Mask = value;
            }
        }
        

        public new void Clear()
        {

            base.Clear();
            this.OriginalText = this.Text;
            

        }
        public MyMaskedTextBox()
        {
            InitializeComponent();
            checkMaskMode();
        }


        public string Validate(ErrorProvider errProvider)
        {
            string result = "";
            if (this.Enabled && this.HasValidation)
            {
                this.HasError = !_Validation.Validate(this.FindForm(), this.FormValue, out result);
                if (this.HasError)
                {
                    errProvider.SetError(this, result);
                }
            }
            return result;
        }
        public string FormValue
        {
            get
            {
                string s = this.Text;
                string oldText = s;
                this.Text = "";
                if (s == this.Text)
                    s = "";
                this.Text = oldText;
                return s;
            }
        }
        public double FormValueDbl
        {
            get
            {
                if (this.FormValue != "")
                    return Convert.ToDouble(this.FormValue);
                else
                    return double.MinValue;
            }
        }
        public DateTime FormValueDate
        {
            get
            {
                if (this.FormValue != "")
                    return Convert.ToDateTime(this.FormValue);
                else
                    return DateTime.MinValue;
            }
        }
        public DateTime? FormValueDateNullable
        {
            get
            {
                if (this.FormValue == "" || this.FormValue == null)
                    return null;
                else
                    return Convert.ToDateTime(FormValue);
            }
        }
        public double? FormValueDblNullable
        {
            get
            {
                if (this.FormValue == "" || this.FormValue == null)
                    return null;
                else
                    return Convert.ToDouble(FormValue);
            }
        }
        public void SetText(object o)
        {
            this.Text = "";
            if (o != null)
            {
                if (o is double)
                {
                    double d = (double)o;
                    if (d != double.MinValue)
                        this.Text = d.ToString("0.00");
                }
                else if (o is int)
                {
                    int i = (int)o;
                    if (i != Int32.MinValue)
                        this.Text = i.ToString();

                }
                else if (o is DateTime)
                {
                    DateTime d = (DateTime)o;
                    if (d != DateTime.MinValue)
                    {
                        string s = d.ToString("dd/MM/yyyy");
                        if (this._Validation.HasTime)
                            s += " " + d.ToString("HH:mm");
                        this.Text = s;
                    }
                }
                else if (o is string)
                {
                    string s = (string)o;
                    if (s != null)
                        this.Text = s;
                }
            }
            

        }
        /// <summary>
        /// Fills the text, and sets it as the original text of the text box
        /// </summary>
        /// <param name="o"></param>
        public void FillData(object o)
        {
            SetText(o);
            this.OriginalText = this.Text;

        }
        protected override void OnGotFocus(EventArgs e)
        {
            CheckAcceptCancelGotFocus();
            base.OnGotFocus(e);
        }
        protected override void OnLostFocus(EventArgs e)
        {
            checkDateFormat();
            CheckAcceptCancelLostFocus();
            base.OnLostFocus(e);
        }

        private void checkDateFormat()
        {
            if (this.MaskMode == Mask_Mode.Date || this.MaskMode == Mask_Mode.DateTime)
            {
                DateTime d;
                if (DateTime.TryParse(this.Text, out d))
                {
                    string s = d.ToString("dd/MM/yyyy");
                    if (this.MaskMode == Mask_Mode.DateTime)
                        s += " " + d.ToString("HH:mm");
                    this.Text = s;
                }
            }
            
        }
        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }
        private DateTime _lastMouseDownTime = DateTime.MinValue;
        private DateTime _lastChosenDate = DateTime.MinValue;
        private int _lastMouseDownClicks = 0;
        private void CalendarMonthCalendar_MouseUp(object sender, MouseEventArgs e)
        {
            DateTime selDate = CalendarMonthCalendar.SelectionStart;
            
            DateTime d = CS.General_20090518.Date.Now();
            int tickInterval = 8000000;
            long diff = d.Ticks - _lastMouseDownTime.Ticks;
            if (((diff <= tickInterval)) && selDate == _lastChosenDate)
            {
                _lastMouseDownClicks++;
            }
            else
            {
                _lastMouseDownClicks = 1;
            }
            if (_lastMouseDownClicks >= 2)
            {
                _lastMouseDownClicks = 0;
                HideCalendar();
            }
            _lastMouseDownTime = d;
            _lastChosenDate = CalendarMonthCalendar.SelectionStart;
            
        }
    }
}
