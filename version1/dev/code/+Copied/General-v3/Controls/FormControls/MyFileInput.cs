﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CS.General_20090518.Controls.FormControls
{
    public partial class MyFileInput : UserControl
    {
        public MyFileInput()
        {
            InitializeComponent();
            this.InputType = INPUT_TYPE.OpenDialog;
            this.Height = 20;
        }
        public enum INPUT_TYPE
        {
            SaveDialog,
            OpenDialog
        }
        #region Properties
        public INPUT_TYPE InputType { get; set; }
        public string Filter { get; set; }
        public bool CheckPathExists { get; set; }
        public bool CheckFileExists { get; set; }
        public bool CreatePrompt { get; set; }
        public string DefaultExt { get; set; }
        public bool OverwritePrompt { get; set; }
        public string InitialDirectory { get; set; }
        public bool AddExtension { get; set; }
        public string TitleOfDialog { get; set; }
        public string Text
        {
            get
            {
                return this.txtInput.Text;
            }
            set
            {
                this.txtInput.Text = value;
            }
        }
        public string FormValue
        {
            get
            {
                return txtInput.Text;
            }
        }
        #endregion

        private void setDialogProperties()
        {
            openFileDialog.Title = saveFileDialog.Title = TitleOfDialog;
            openFileDialog.Multiselect = false;
            
            openFileDialog.AddExtension = saveFileDialog.AddExtension = AddExtension;
            openFileDialog.Filter = saveFileDialog.Filter = Filter;
            openFileDialog.CheckPathExists = saveFileDialog.CheckPathExists = CheckPathExists;
            openFileDialog.CheckFileExists = saveFileDialog.CheckFileExists = CheckFileExists;
            saveFileDialog.CreatePrompt = CreatePrompt;
            openFileDialog.DefaultExt = saveFileDialog.DefaultExt = DefaultExt;
            openFileDialog.InitialDirectory = saveFileDialog.InitialDirectory = InitialDirectory;
            openFileDialog.AddExtension = saveFileDialog.AddExtension = AddExtension;

        }

        private void positionUI()
        {
            this.Height = 20;
            txtInput.Left = 0;
            txtInput.Top = 0;
            txtInput.Width = this.Width - btnInput.Width + 1;
            txtInput.Height = this.Height;

            btnInput.Height = this.Height;
            btnInput.Top = 0;
            btnInput.Left = txtInput.Left + txtInput.Width-1;
            
        }
        protected override void OnResize(EventArgs e)
        {
            positionUI();
            base.OnResize(e);
        }
       

        private void btnInput_Click(object sender, EventArgs e)
        {
            setDialogProperties();
            FileDialog dialog = null;
            if (InputType == INPUT_TYPE.SaveDialog)
            {
                dialog = saveFileDialog;
            }
            else
            {
                dialog = openFileDialog;
            }
            DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtInput.Text = dialog.FileName;
            }
             
        }
    }
}
