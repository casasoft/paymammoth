﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using CS.General_v3.JavaScript.Data;
using System.Web.SessionState;
using System.Collections;
using System.Collections.Specialized;
using CS.General_v3.Util;

namespace CS.General_v3.HTTPHandlers
{
    public abstract class BaseJSONHandler : BaseHandler, IRequiresSessionState
    {
        private Dictionary<string, object> _dictionary = new Dictionary<string,object>();


        public void AddProperty(string key, object value)
        {
            _dictionary[key] = value;
        }

        public object this[string key]
        {
            get {
                return _dictionary[key];
            }
            set { _dictionary[key] = value; }
        }



        protected override void processRequest(HttpContext context)
        {
            string js = JSONUtil.Serialize(_dictionary);

            context.Response.Write(js);
        }

    }
}
