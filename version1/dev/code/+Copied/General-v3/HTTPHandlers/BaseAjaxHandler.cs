﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using CS.General_v3.JavaScript.Data;
using System.Web.SessionState;
using System.Collections;
using CS.General_v3.JavaScript.Interfaces;
using System.Collections.Specialized;
namespace CS.General_v3.HTTPHandlers
{
    using JavaScript.Data;

    public abstract class BaseAjaxHandler : BaseHandler, IRequiresSessionState
    {
        private JSObject _jsObject;

        public bool EncodeBase64 { get; set; }


        public BaseAjaxHandler(bool forFlash = false)
        {
            _jsObject = forFlash ? new JSObjectFlash() : new JSObject();
        }

        public void AddProperty(string id, DateTime value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            _jsObject.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, DateTime? value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            _jsObject.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, TimeSpan value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            _jsObject.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, TimeSpan? value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            _jsObject.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, bool value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            _jsObject.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, string value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            _jsObject.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, bool? value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            _jsObject.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IJavaScriptSerializable value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            _jsObject.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        
        public void AddProperty(string id, double value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            _jsObject.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, double? value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            _jsObject.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, int value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            _jsObject.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, int? value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            _jsObject.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, float value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            _jsObject.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, long value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            _jsObject.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IJavaScriptObject value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            _jsObject.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }


        public void AddProperty(string id, IEnumerable<int> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            _jsObject.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<string> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            _jsObject.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<long> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            _jsObject.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<double> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            _jsObject.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<DateTime> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            _jsObject.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<IJavaScriptObject> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            _jsObject.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        
        public void AddProperty(string id, IEnumerable<IJavaScriptSerializable> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            _jsObject.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<NameValueCollection> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            _jsObject.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<Int16> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            _jsObject.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<int?> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            _jsObject.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<double?> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            _jsObject.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, JSONObject value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            _jsObject.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }




        protected override void processRequest(HttpContext context)
        {
            string js = _jsObject.GetJS();
            if (EncodeBase64)
            {
                js = CS.General_v3.Util.Text.Base64Encode(js);
            }
            context.Response.Write(js);
        }
       
        public void UpdateJsObject(JSObject obj)
        {
            _jsObject = obj;
        }
    }
}
