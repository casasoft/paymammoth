﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using CS.General_v3.JavaScript.Data;
using System.Web.SessionState;
using System.Collections;
using CS.General_v3.JavaScript.Interfaces;
using System.Collections.Specialized;
namespace CS.General_v3.HTTPHandlers
{
    /// <summary>
    /// Ovverride the Validate method and return an error string message.  Return NULL if OK!
    /// </summary>
    public abstract class BaseAjaxValidationHandler : BaseAjaxHandler, IRequiresSessionState
    {
        

        protected override void processRequest(HttpContext context)
        {
            string data = context.Request.QueryString["data"];
            this.AddProperty("error", Validate(data));
            base.processRequest(context);
        }

        /// <summary>
        /// Validate the user request
        /// </summary>
        /// <param name="data">Data to validate</param>
        /// <returns>Error message (null if ok)</returns>
        protected abstract string Validate(string data);


    }
}
