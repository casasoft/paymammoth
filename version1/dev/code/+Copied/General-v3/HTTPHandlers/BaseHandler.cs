﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.SessionState;

namespace CS.General_v3.HTTPHandlers
{
    public abstract class BaseHandler : IHttpHandler, IRequiresSessionState
    {
        #region IHttpHandler Members
        public bool RequiresSsl { get; set; }
        public bool RequiresSslEvenOnTestDevelopment { get; set; }
        public bool IsReusable { get { return false; } }
        protected abstract void processRequest(HttpContext ctx);
        public BaseHandler()
        {
            int k = 5;
        }

        

        private void ProcessRequest(HttpContext context)
        {
            bool ok = true;
            CS.General_v3.Util.PageUtil.checkFromHandlerWhetherBrowserSupportsCookies();
            if (RequiresSsl && 
                (!context.Request.IsSecureConnection))
            {
                if (!CS.General_v3.Util.Other.IsLocalTestingMachine ||
                    (CS.General_v3.Util.Other.IsLocalTestingMachine && RequiresSslEvenOnTestDevelopment))
                {

                    ok = false;
                }
            }
            if (ok)
            {
                processRequest(context);
            }
            //non SSL requests are not processed if requires ssl
        }

        #endregion

        #region IHttpHandler Members


        void IHttpHandler.ProcessRequest(HttpContext context)
        {
            this.ProcessRequest(context);

        }

        #endregion
    }
}
