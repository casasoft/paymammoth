using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using MySql.Data.MySqlClient;
using MySql.Data;

using System.Data;
using CS.General_20090518;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CS
{
    
    /// <summary>
    /// Database class, old, and new functions are added to CS.DB instead of CS.DataBase
    /// </summary>
    public static class Database
    {
        public static MySqlConnection mySQLconn = null;

        public static void CheckOpenConnection(MySqlConnection conn)
        {
            if (conn.State != ConnectionState.Open)
            {
                if (conn.State != ConnectionState.Closed)
                    conn.Close();

                bool retry = false;
                int times = 0;
                do
                {
                    try
                    {
                        conn.Open();
                        retry = false;
                    }
                    catch (MySqlException ex)
                    {
                        if (ex.Message.Trim().ToLower() == "too many connections")
                        {
                            retry = (times < 10);
                            times++;
                            if (!retry)
                            {
                                throw ex;
                            }
                            else
                            {
                                System.Threading.Thread.Sleep(500); //wait half a second
                            }

                        }
                        else
                        {
                            throw ex;
                        }
                    }
                } while (retry);

                
            }
        }
        

        /// <summary>
        /// Creates a new database connection adapter to a MySQL database,
        /// according to the connection details specified in settings.
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        public static MySqlConnection createConn()
        {
            
            mySQLconn = createConn(CS.Settings.DBHost, CS.Settings.DBName, CS.Settings.DBUser,
                                                CS.Settings.DBPass, CS.Settings.DBCharacterSet);

            return mySQLconn;

        }

        /// <summary>
        /// Creates a new database connection adapter to a MySQL database.
        /// </summary>
        /// <param name="connectionString">Connection string</param>
        public static MySqlConnection createConn(string connectionString)
        {
            string connstr = connectionString;
            MySqlConnection conn = null;
            bool retry = false;
            int times = 0;
            do
            {
                try
                {
                    conn = new MySqlConnection(connstr);
                    retry = false;
                }
                catch (MySqlException ex)
                {
                    if (ex.Message.Trim().ToLower() == "too many connections")
                    {
                        retry = (times < 10);
                        times++;
                        if (!retry)
                        {
                            throw ex;
                        }
                        else
                        {
                            System.Threading.Thread.Sleep(500); //wait half a second
                        }

                    }
                    else
                    {
                        throw ex;
                    }
                }
            } while (retry);
            //conn.Open();
            return conn;
        }

        /// <summary>
        /// Creates a new database connection adapter to a MySQL database.
        /// </summary>
        /// <param name="host">Server</param>
        /// <param name="database">Database to connect to</param>
        /// <param name="user">Username</param>
        /// <param name="pass">Password</param>
        public static MySqlConnection createConn(string host, string database, string user, string pass, string characterset)
        {
            //string connstr = "Server=" + host + ";Database="
                        //+ database   + ";Uid=" + user
                                       //+ ";Pwd=" + pass + ";";
            string charSet = "";
            
            if (characterset != "" && characterset != null)
            {
                charSet = "charset=" + characterset + ";";
            }
            string connstr = "Data Source=" + host + ";Database="
                        + database + ";User Id=" + user
                                       + ";Password=" + pass + ";" + charSet + ";Allow Zero Datetime=Yes;";
            
            return createConn(connstr);
            
        }
        public static void ExecuteSQL(StringBuilder SQL)
        {
            ExecuteSQL(SQL.ToString());
        }
        public static void ExecuteSQL(string SQL)
        {
            if (!string.IsNullOrEmpty(SQL))
            {
                MySqlConnection myConnection = null;
                string myQuery = SQL;

                MySqlCommand myCommand = null;
                bool ok = false;
                int retries = 0;
                while (!ok)
                {

                    try
                    {
                        if (myConnection == null)
                        {
                            myConnection = createConn();
                        }
                        CheckOpenConnection(myConnection);
                        myCommand = new MySqlCommand(myQuery);
                        myCommand.Connection = myConnection;
                        myCommand.ExecuteNonQuery();
                        myCommand.Connection.Close();
                        myCommand.Dispose();
                        myConnection.Dispose();
                        myCommand = null;
                        myConnection = null;
                        ok = true;
                    }

                    catch (Exception ex)
                    {
                        if (myConnection != null && myConnection.State != ConnectionState.Closed)
                        {
                            myConnection.Close();
                            myConnection.Dispose();
                            myConnection = null;
                        }
                        if (myCommand != null)
                        {
                            myCommand.Dispose();
                            myCommand = null;
                        }
                        retries++;
                        if (retries > 3)
                        {
                            string msg = "";

                            msg += " Error: " + ex.Message + "\n\n";
                            msg += "   SQL Statement\n";
                            msg += "-----------------------------------------------------------------------------------\n\n";
                            msg += Text.limitText(SQL, 500) + "\n\n";
                            msg += "===============================================";


                            Exception newEx = new Exception(msg, ex.InnerException);
                            newEx.Source = ex.Source;
                            throw newEx;
                        }
                    }
                    finally
                    {
                        if (myConnection != null)
                        {
                            myConnection.Close();
                            myConnection.Dispose();
                        }
                        if (myCommand != null)
                        {
                            myCommand.Dispose();
                            myCommand = null;
                        }

                    }
                }
            }   
        }
        public static void ExecuteSQL(string SQL, MySqlConnection conn)
        {
            if (conn == null)
                ExecuteSQL(SQL);
            else
            {
                bool returnsdata = false;
                string tmp = SQL.Replace("(", "");
                tmp = tmp.Trim().ToUpper();


                MySqlCommand cmd = new MySqlCommand(SQL, conn);
                returnsdata = (tmp.StartsWith("SELECT"));
                
                try
                {
                    CheckOpenConnection(conn);
                    
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    
                }
                catch (MySqlException e)
                {
                    string msg = "";

                    msg += " Error: " + e.Message + "\n\n";
                    msg += "   SQL Statement\n";
                    msg += "-----------------------------------------------------------------------------------\n\n";
                    msg += Text.limitText(SQL, 500) + "\n\n";
                    msg += "===============================================";
                    ArgumentException err = new ArgumentException(msg);
                    throw err;
                }

            }

        }
       
        /// <summary>
        /// Retrieves a data table from the default database.
        /// </summary>
        /// <param name="SQL">The query to execute</param>
        /// <returns>The DataSet, filled with data from DB</returns>
        public static DataTable getDataTablefromDB(string SQL)
        {
            MySqlConnection conn = createConn();
            DataTable tb =getDataTablefromDB(SQL, conn);
            
            conn.Close();
            conn.Dispose();
            conn = null;
            return tb;

        }

        /// <summary>
        /// Retrieves a data table from the default database.
        /// </summary>
        /// <param name="SQL">The query to execute</param>
        /// <returns>The DataSet, filled with data from DB</returns>
        public static DataTable getDataTablefromDB(string SQL, MySqlConnection connection)
        {
            bool ok = false;
            int retries = 0;
            MySqlConnection conn = connection;
            
            MySqlDataAdapter dataAdapter = null;
            DataSet ds = null;
            while (!ok)
            {
                try
                {
                    CheckOpenConnection(conn);

                    dataAdapter = new MySqlDataAdapter(SQL, conn);
                    ds = new DataSet();
                    dataAdapter.Fill(ds);
                        
                    ok = true;
                }
                
                catch (Exception ex)
                {
                    retries++;
                    if (conn.State != ConnectionState.Closed)
                    {
                        conn.Close();
                    }
                    if (ds != null)
                    {
                        ds.Dispose();
                        ds = null;
                    }


                    if (dataAdapter != null)
                    {

                        dataAdapter.Dispose();
                        dataAdapter = null;
                    }
                    if (retries > 3)
                    {
                        string msg = "";

                        msg += " Error: " + ex.Message + "\n\n";
                        msg += "   SQL Statement\n";
                        msg += "-----------------------------------------------------------------------------------\n\n";
                        msg += Text.limitText(SQL, 500) + "\n\n";
                        msg += "===============================================";
                        

                        Exception newEx = new Exception(msg, ex.InnerException);
                        newEx.Source = ex.Source;
                        throw newEx;

                    }
                }
                finally
                {
                    if (conn.State != ConnectionState.Closed)
                    {
                        conn.Close();
                    }
                    
                    if (dataAdapter != null)
                    {
                        
                        dataAdapter.Dispose();
                        dataAdapter = null;
                    }
                    
                }

            }
            if (dataAdapter != null)
            {
                dataAdapter.Dispose();
            }

            
            return ds.Tables[0];
        }


        public static int getNextValueFromTable(string table, string field, MySqlConnection conn)
        {
            bool ok = false;
            int retries = 0;
            string SQL = "SELECT COALESCE(MAX(" + field + "),0) + 1 '" + field +
                         "' FROM " + table + ";";
            DataRow row = null;
            int result = 0;
            while (!ok)
            {
                try
                {
                    row = getDataRowfromDB(SQL,conn);
                    result = Convert.ToInt32(row["id"]);
                    ok = true;
                }
                catch (Exception e)
                {
                    retries++;
                    if (true || retries > 3)
                    {
                        ok = false;
                        throw e;
                    }

                }


            }
            if (row != null)
            {
                row = null;
            }
            return result;


            
        }


        public static int getNextValueFromTable(string table, string field)
        {
            MySqlConnection conn = createConn();
            int res = getNextValueFromTable(table, field, conn);
            conn.Close();
            conn.Dispose();
            return res;

        }




       

        /// <summary>
        /// Returns the first value from an SQL statment
        /// </summary>
        /// <param name="SQL">SQL to execute</param>
        /// <returns>The value of the first field</returns>
        public static bool getBoolFromSQL(string SQL)
        {
            bool ret = false;
            DataRow row = getDataRowfromDB(SQL);
            if (row != null)
            {
                ret = Convert.ToBoolean(row[row.Table.Columns[0].ColumnName]);
            }
            
            return ret;

        }

        /// <summary>
        /// Returns the first value from an SQL statment
        /// </summary>
        /// <param name="SQL">SQL to execute</param>
        /// <param name="field">Field to return</param>
        /// <returns>The value of the field</returns>
        public static bool getBoolFromSQL(string SQL, string field)
        {
            bool ret = false;
            DataRow row = getDataRowfromDB(SQL);
            if (row != null)
            {
                ret = Convert.ToBoolean(row[field]);
            }
            return ret;

        }

        /// <summary>
        /// Returns the first value from an SQL statment
        /// </summary>
        /// <param name="SQL">SQL to execute</param>
        /// <param name="field">Field to return</param>
        /// <returns>The value of the field</returns>
        public static int getIntFromSQL(string SQL, string field)
        {
            int ret = 0;
            DataRow row = getDataRowfromDB(SQL);
            if (row != null)
            {
                ret = Convert.ToInt32(row[field]);
            }
            return ret;

        }

        /// <summary>
        /// Returns the first value from an SQL statment
        /// </summary>
        /// <param name="SQL">SQL to execute</param>
        /// <returns>The value of the field</returns>
        public static int getIntFromSQL(string SQL)
        {
            int ret = 0;
            DataRow row = getDataRowfromDB(SQL);
            if (row != null)
            {
                ret = Convert.ToInt32(row[row.Table.Columns[0].ColumnName]);
            }
            return ret;

        }


        /// <summary>
        /// Returns the first value from an SQL statment
        /// </summary>
        /// <param name="SQL">SQL to execute</param>
        /// <param name="field">Field to return</param>
        /// <returns>The value of the field</returns>
        public static double getDoubleFromSQL(string SQL, string field)
        {
            double ret = 0;
            DataRow row = getDataRowfromDB(SQL);
            if (row != null)
            {
                ret = Convert.ToDouble(row[field]);
            } 
            return ret;

        }


        /// <summary>
        /// Returns the first value from an SQL statment
        /// </summary>
        /// <param name="SQL">SQL to execute</param>
        /// <returns>The value of the field</returns>
        public static string getStringFromSQL(string SQL)
        {
            return getStringFromSQL(SQL, "");

        }

        /// <summary>
        /// Returns the FIELD  value from an SQL statment
        /// </summary>
        /// <param name="SQL">SQL to execute</param>
        /// <param name="field">Field to return</param>
        /// <returns>The value of the field</returns>
        public static string getStringFromSQL(string SQL, string field)
        {
            string ret = null;
            DataRow row = getDataRowfromDB(SQL);
            if (row != null)
            {
                if (field != "" && field != null)
                    ret = Convert.ToString(row[field]);
                else
                    ret = Convert.ToString(row[row.Table.Columns[0].ColumnName]);
            }
            return ret;

        }

        /// <summary>
        /// Returns the first value from an SQL statment
        /// </summary>
        /// <param name="SQL">SQL to execute</param>
        /// <returns>The value of the field, as an object </returns>
        public static object getValueFromSQL(string SQL)
        {
            return getValueFromSQL(SQL, "");

        }


        /// <summary>
        /// Returns the FIELD  value from an SQL statment
        /// </summary>
        /// <param name="SQL">SQL to execute</param>
        /// <param name="field">Field to return</param>
        /// <returns>The value of the field as object</returns>
        public static object getValueFromSQL(string SQL, string field)
        {
            DataRow row = getDataRowfromDB(SQL);
            object ret = null;
            if (row != null)
            {
                ret = row[field];
            }
            return ret;

        }



        public static bool recordsExistInSQL(string SQL)
        {

            DataRow row = getDataRowfromDB(SQL);
            bool res = (row != null);
            return res;
        }

        /// <summary>
        /// Retrieves a data row from the default database.
        /// </summary>
        /// <param name="SQL">The query to execute</param>
        /// <returns>The DataRow, filled with data from DB</returns>
        public static DataRowCollection getDataRowsfromDB(string SQL,MySqlConnection conn)
        {

            DataTable dt = getDataTablefromDB(SQL,conn);
            return dt.Rows;

        }
        /// <summary>
        /// Retrieves a data row from the default database.
        /// </summary>
        /// <param name="SQL">The query to execute</param>
        /// <returns>The DataRow, filled with data from DB</returns>
        public static DataRowCollection getDataRowsfromDB(string SQL)
        {
            MySqlConnection conn = createConn();

            DataRowCollection rows = getDataRowsfromDB(SQL, conn);
            if (conn != null)
                conn.Dispose();
            return rows;

        }

        /// <summary>
        /// Retrieves a data row from the default database.
        /// </summary>
        /// <param name="SQL">The query to execute</param>
        /// <returns>The DataRow, filled with data from DB</returns>
        public static DataRow getDataRowfromDB(string SQL)
        {
            MySqlConnection conn = createConn();
            
            DataRow row = getDataRowfromDB(SQL, conn);
            conn.Close();
            conn.Dispose();
            return row;
        }
        /// <summary>
        /// Retrieves a data row from the default database.
        /// </summary>
        /// <param name="SQL">The query to execute</param>
        /// <returns>The DataRow, filled with data from DB</returns>
        public static DataRow getDataRowfromDB(string SQL, MySqlConnection conn)
        {

            DataTable dt = getDataTablefromDB(SQL,conn);
            if (dt.Rows.Count > 0)
                return dt.Rows[0];
            else
            {
                dt.Clear();
                return null;
            }
        }

        public static void clearForm(HtmlForm frm)
        {
            for (int i = 0; i < frm.Controls.Count; i++)
            {
                Type t = frm.Controls[i].GetType();

                switch (t.Name)
                {
                    case "MyTxtBox":
                        CS.General_20090518.Controls.WebControls.Common.MyTxtBox txt = (CS.General_20090518.Controls.WebControls.Common.MyTxtBox)frm.Controls[i];
                        txt.Text = "";

                        break;
                    case "MyCheckBox":

                        CS.General_20090518.Controls.WebControls.Common.MyCheckBox chk = (CS.General_20090518.Controls.WebControls.Common.MyCheckBox)frm.Controls[i];
                        chk.Checked = false;
                        break;
                    case "MyDropDownList":

                        CS.General_20090518.Controls.WebControls.Common.MyDropDownList ddl = (CS.General_20090518.Controls.WebControls.Common.MyDropDownList)frm.Controls[i];
                        ddl.SelectedIndex = 0;
                        break;
                    case "MyRadioButtonList":
                        CS.General_20090518.Controls.WebControls.Common.MyRadioButtonList rbl = (CS.General_20090518.Controls.WebControls.Common.MyRadioButtonList)frm.Controls[i];
                        rbl.SelectedIndex = 0;
                        break;

                }
            }

        }


    }
}
