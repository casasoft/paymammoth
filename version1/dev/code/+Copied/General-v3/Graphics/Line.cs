﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Drawing;
using System.Windows.Forms;


namespace CS.General_v3.Graphics
{
    public class LineCollection : ICollection<Line>
    {

        private List<Line> lines = new List<Line>();

        public Line this[int index]
        {
            get
            {
                return lines[index];
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            for (int i = 0; i < lines.Count; i++)
            {
                lines[i].Dispose();
            }
        }

        #endregion
            
        #region ICollection<Line> Members

        public void  Add(Line item)
        {
            lines.Add(item);
        }

        public void  Clear()
        {
            lines.Clear();
        }

        public bool  Contains(Line item)
        {
            return lines.Contains(item);
        }

        public void  CopyTo(Line[] array, int arrayIndex)
        {
            lines.CopyTo(array, arrayIndex);
        }

        public bool  IsReadOnly
        {
            get
            {
                return false ;
            }
        }

        public bool  Remove(Line item)
        {
            return lines.Remove(item);
        }

        #endregion

        #region IEnumerable<Line> Members

        public IEnumerator<Line>  GetEnumerator()
        {
            return lines.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator  System.Collections.IEnumerable.GetEnumerator()
        {
            return lines.GetEnumerator();
        }

        #endregion

        #region ICollection<Line> Members


        public int Count
        {
            get { return lines.Count; }
        }

        #endregion
    }
    public class Line : IDisposable
    {
        private CS.General_v3.Graphics.GraphicsUtil g;
        private Control parent = null;
        public Point From { get; set; }
        public Point To { get; set; }
        public Color Color { get; set; }
        public int Size { get; set; }
        private PaintEventHandler paintEventHandler;
        public Line(Control parent, int x1, int y1, int x2, int y2)
            : this(parent,x1,y1,x2,y2,Color.Black,1)
        {
        }
        public Line(Control parent, int x1, int y1, int x2, int y2, Color color, int Size)
        {
            this.parent = parent;
            From = new Point(x1, y1);
            To = new Point(x2, y2);
            Color = color;
            this.Size = Size;
            init();
        }
        private void init()
        {
            paintEventHandler = new PaintEventHandler(parent_Paint); ;
            g = new GraphicsUtil(parent);
            parent.FindForm().Paint += paintEventHandler;
            //parent.Paint += paintEventHandler;
        }

        void parent_Paint(object sender, PaintEventArgs e)
        {
            g.DrawLine(Color, From.X, From.Y, To.X, To.Y,this.Size);
        }

        #region IDisposable Members

        public void Dispose()
        {
            //parent.Paint -= paintEventHandler;
            parent.FindForm().Paint -= paintEventHandler;
        }

        #endregion
    }
}
