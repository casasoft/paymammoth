﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Drawing;

namespace CS.General_v3.Graphics
{
    public class LineHitTest
    {

        public Point Start { get; set; }
        public Point End { get; set; }
        public double Size
        {
            get;
            set;
        }
        private double gradient;

        private bool isInBox(double x, double y)
        {
            double halfSize = this.Size / 2;
            double sX, sY, eX, eY;
            sX = (double)Start.X;
            sY = (double)Start.Y;
            eX = (double)End.X;
            eY = (double)End.Y;
            if (sX <= eX)
            {
                sX -= halfSize;
                eX += halfSize;
            }
            else
            {
                sX += halfSize;
                eX -= halfSize;
            }
            if (sY <= eY)
            {
                sY -= halfSize;
                eY += halfSize;
            }
            else
            {
                sY += halfSize;
                eY -= halfSize;
            }



            return isInBox(sX, sY, eX, eY, x, y);
        }
            
        private bool isInBox(double startX, double startY, double endX, double endY, double x, double y)
        {
            if (((x >= startX && x <= endX) || (x >= endX && x <= startX)) &&
                ((y >= startY && y <= endY) || (y >= endY && y <= startY)))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public bool HitText(float x, float y)
        {
            return HitTest((double)x, (double)y);
        }
        public bool HitText(int x, int y)
        {
            return HitTest((double)x, (double)y);
        }
        public bool HitTest(double x, double y)
        {
            bool result = false;
            if (isInBox(x, y))
            {
                int startX;
                int startY;
                if (Start.X <= x)
                {
                    startX = Start.X;
                    startY = Start.Y;
                }
                else
                {
                    startX = End.X;
                    startY = End.Y;
                }
                double halfSize = Size / (double)2;
                double expectedY = startY + ((x - startX) * gradient);
                if (expectedY - (halfSize * Math.Abs(gradient)) <= y && expectedY + (halfSize * Math.Abs(gradient)) >= y)
                {
                    result = true;
                }

            }
            return result;

        }
        public LineHitTest(double x1,double y1, double x2, double y2)
        {
            SetLine(new Point((int)x1, (int)y1), new Point((int)x2, (int)y2), 1);
        }
        public LineHitTest(int x1, int y1, int x2, int y2)
        {
            SetLine(new Point((int)x1, (int)y1), new Point((int)x2, (int)y2), 1);
        }
        public LineHitTest(double x1, double y1, double x2, double y2, double LineSize)
        {
            SetLine(new Point((int)x1, (int)y1), new Point((int)x2, (int)y2), (int)LineSize);
        }
        public LineHitTest(int x1, int y1, int x2, int y2, int LineSize)
        {
            SetLine(new Point((int)x1, (int)y1), new Point((int)x2, (int)y2), LineSize);
        }
        public LineHitTest(Point start, Point end)
            : this(start, end, 1)
        {
        }
        public LineHitTest(Point start, Point end, double LineSize)
        {
            SetLine(start, end, LineSize);
        }
        public LineHitTest()
        {

        }
        public void SetLine(Point start, Point end, double LineSize)
        {
            this.Start = start;
            this.End = end;
            this.Size = LineSize;
            this.gradient = ((double)((double)this.Start.Y - (double)this.End.Y) / (double)((double)this.Start.X - (double)this.End.X));

        }
    }

}
