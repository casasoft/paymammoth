﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace CS.General_v3.Graphics
{
    public class GraphicsUtil
    {
        public enum Alignment
        {
            Left, Center, Right
        }

        #region Default Properties

        private Color _CurrentColor = Color.Black;

        public Color CurrentColor
        {
            get { return _CurrentColor; }
            set { _CurrentColor = value; }
        }
        private Font _CurrentFont = new Font("Times New Roman", 10);

        public Font CurrentFont
        {
            get { return _CurrentFont; }
            set { _CurrentFont = value; }
        }

        public void SetCurrentFont(string fontName, int fontSize)
        {
            SetCurrentFont(fontName, fontSize, FontStyle.Regular);
        }
        public void SetCurrentFont(string fontName, int fontSize, FontStyle fontStyle)
        {
            CurrentFont = new Font(fontName, fontSize, fontStyle);
        }

        #endregion

        public System.Drawing.Graphics GetGraphics()
        {
            return g;
        }
        private System.Drawing.Graphics g = null;
        public GraphicsUtil(Control c)
        {
            g = c.CreateGraphics();
        }
        public GraphicsUtil(System.Drawing.Graphics graphics)
        {
            g = graphics;
        }
        #region Text Methods
        #region Plain Strings
        public SizeF DrawString(string text, float x, float y)
        {
            return DrawString(text, CurrentColor, CurrentFont, x, y);
        }
        public SizeF DrawString(string text, string fontName, int fontSize, float x, float y)
        {
            return DrawString(text, CurrentColor, fontName, fontSize, FontStyle.Regular, x, y);
        }
        public SizeF DrawString(string text, Font font, float x, float y)
        {
            return DrawString(text, Color.Black, font, x, y);
        }
        public SizeF DrawString(string text, Color textColor, string fontName, int fontSize, float x, float y)
        {
            return DrawString(text, textColor, fontName, fontSize, FontStyle.Regular, x, y);
        }

        public SizeF DrawString(string text, Color textColor, string fontName, int fontSize, FontStyle fontStyle, float x, float y)
        {
            Font font = new Font(fontName, fontSize, fontStyle);
            return DrawString(text, textColor, font, x, y);
        }
        public SizeF DrawString(string text, Color textColor, Font font, float x, float y)
        {
            SolidBrush brush = new SolidBrush(textColor);
            SizeF size = g.MeasureString(text, font);
            g.DrawString(text, font, brush, x, y);
            return size;
        }
        #endregion
        #region String Measurement
        public SizeF MeasureString(string text)
        {
            return g.MeasureString(text, CurrentFont);
        }

        public SizeF MeasureString(string text, string fontName,int fontSize)
        {
            return MeasureString(text, fontName,fontSize, FontStyle.Regular);
        }
        public SizeF MeasureString(string text, string fontName, int fontSize, FontStyle fontStyle)
        {
            return MeasureString(text, new Font(fontName, fontSize, fontStyle));
        }
        public SizeF MeasureString(string text, Font font)
        {
            return g.MeasureString(text, font);
        }
        #endregion
        #region Strings with Alignment
        public SizeF DrawString(string text, float x, float y, float boxWidth, Alignment alignment)
        {
            return DrawString(text, CurrentFont, x, y, boxWidth,  alignment);
        }
        public SizeF DrawString(string text, Font font, float x, float y, float boxWidth, Alignment alignment)
        {
            return DrawString(text, CurrentColor, font, x, y, boxWidth, CurrentFont.Size + 6, alignment);
        }
        public SizeF DrawString(string text, float x, float y, float boxWidth, float boxHeight, Alignment alignment)
        {
            return DrawString(text, CurrentColor, CurrentFont, x, y, boxWidth, boxHeight, alignment);
        }
        public SizeF DrawString(string text, Color textColor, Font font, float x, float y, float boxWidth, float boxHeight, Alignment alignment)
        {
            StringFormat stringFormat = new StringFormat();
            switch (alignment)
            {
                case Alignment.Left: stringFormat.Alignment = StringAlignment.Near; break;
                case Alignment.Right: stringFormat.Alignment = StringAlignment.Far; break;
                case Alignment.Center: stringFormat.Alignment = StringAlignment.Center; break;
            }
            RectangleF rectangleF = new RectangleF(x, y, boxWidth, boxHeight);
            SolidBrush brush = new SolidBrush(textColor);

            SizeF layoutArea = new SizeF(boxWidth, boxHeight);

            g.DrawString(text, font, brush, rectangleF, stringFormat);
            return g.MeasureString(text, font, layoutArea, stringFormat);
        }
        #endregion
        #endregion
        #region Line
        public void DrawLine(float x1, float y1, float x2, float y2)
        {
            DrawLine(CurrentColor, x1, y1, x2, y2,1);
        }
        public void DrawLine(Color color, float x1, float y1, float x2, float y2)
        {
            DrawLine(color, x1, y1, x2, y2, 1);
        }
        public void DrawLine(Color color, float x1, float y1, float x2, float y2, int Size)
        {
            Pen pen = new Pen(color,Size);
            g.DrawLine(pen, x1, y1, x2, y2);
        }
        #endregion
        #region Rectangles


        public void DrawDoubleRectangle(float x, float y, float width, float height)
        {
            DrawDoubleRectangle(CurrentColor, x, y, width, height);
        }
        public void DrawDoubleRectangle(float x, float y, float width, float height, int padSize)
        {
            DrawDoubleRectangle(CurrentColor, CurrentColor, x, y, width, height, padSize);
        }
        public void DrawDoubleRectangle(Color color, float x, float y, float width, float height)
        {
            DrawDoubleRectangle(color, color, x, y, width, height, 2);
        }
        public void DrawDoubleRectangle(Color innerColor, Color outerColor, float x, float y, float width, float height, int padSize)
        {
            g.DrawRectangle(new Pen(innerColor), x, y, width, height);
            g.DrawRectangle(new Pen(outerColor), x - padSize, y - padSize, width + (padSize * 2), height + (padSize * 2));
        }


        public void DrawRectangle(float x, float y, float width, float height)
        {
            DrawRectangle(CurrentColor, x, y, width, height);
        }
        public void DrawRectangle(Color color, float x, float y, float width, float height)
        {
            g.DrawRectangle(new Pen(color), x, y, width, height);
        }

        #endregion


    }
}
