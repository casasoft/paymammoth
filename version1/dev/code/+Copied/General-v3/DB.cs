﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using MySql.Data.MySqlClient;
using MySql.Data;

using System.Data;
using CS.General_20090518;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CS
{
    /// <summary>
    /// Database static class, created on 28/03/2008.
    /// </summary>
    public static class DB
    {
        #region SetRowValue
        private const string DATE_VALUE_NULL = "0001-01-01 00:00:00";
        public static void SetRowValue(ref DataRow row, string colName, string value)
        {
            if (value != null)
                row[colName] = value;
            else
                row[colName] =  DBNull.Value;
        }
        public static void SetRowValue(ref DataRow row, string colName, DateTime? value)
        {
            MySql.Data.Types.MySqlDateTime mysqlDate;

            if (value != null)
            {
                DateTime d = value.Value;
                mysqlDate = new MySql.Data.Types.MySqlDateTime(d.Year, d.Month, d.Day, d.Hour, d.Minute, d.Second);
            }
            else
            {
                mysqlDate = new MySql.Data.Types.MySqlDateTime(1, 1, 1, 0, 0, 0);
            }
            row[colName] = mysqlDate;
        }
        public static void SetRowValue(ref DataRow row, string colName, int? value)
        {
            if (value != null)
                row[colName] = value;
            else
                row[colName] = DBNull.Value;
        }
        public static void SetRowValue(ref DataRow row, string colName, bool? value)
        {
            if (value != null)
                row[colName] = value;
            else
                row[colName] = DBNull.Value;
        }
        public static void SetRowValue(ref DataRow row, string colName, double? value)
        {
            if (value != null)
                row[colName] = value;
            else
                row[colName] = DBNull.Value;
        }
        public static void SetRowValue(ref DataRow row, string colName, int value)
        {
            if (value != Int32.MinValue)
                row[colName] = value;
            else
                row[colName] = DBNull.Value;
        }
        public static void SetRowValue(ref DataRow row, string colName, double value)
        {
            if (value != double.MinValue)
                row[colName] = value;
            else
                row[colName] = DBNull.Value;
        }
        public static void SetRowValue(ref DataRow row, string colName, MySql.Data.Types.MySqlDateTime value)
        {
            SetRowValue(ref row, colName, value.GetDateTime());
        }
        public static void SetRowValue(ref DataRow row, string colName, DateTime value)
        {
            if (value != DateTime.MinValue)
                row[colName] = value;
            else
                row[colName] = Convert.ToDateTime(DATE_VALUE_NULL);
        }
        public static void SetRowValue(ref DataRow row, string colName, bool value)
        {
            row[colName] = value;
        }
        public static void SetRowValue(ref DataRow row, string colName, CS.Types.MyBool value)
        {
            if (value != null && value.IsSet)
                row[colName] = value.Value;
            else
                row[colName] = DBNull.Value;
        }
        #endregion
        /// <summary>
        /// Creates a new data row with the table schema of the table provided
        /// </summary>
        /// <param name="TableName">Table Name</param>
        /// <returns>Data Row</returns>
        public static DataRow CreateDataRowFromTable(string TableName)
        {
            DataTable tb = CS.Database.getDataTablefromDB("SELECT * FROM " + TableName + " LIMIT 0");
            return tb.NewRow();
        }

        /// <summary>
        /// Returns the data in the row, resolved to one of the main data types (int,bool,datetime,string,double).  Note that int, double and datetime are all set to nullable types - +?.
        /// </summary>
        /// <param name="row">Data Row</param>
        /// <param name="colName">Column Name</param>
        /// <returns>Object</returns>
        public static object GetRowData(DataRow row, string colName)
        {
            colName = colName.ToLower();
            DataColumn col = null;
            for (int i = 0; i < row.Table.Columns.Count; i++)
            {
                if (row.Table.Columns[i].ColumnName.ToLower() == colName)
                {
                    col = row.Table.Columns[i];
                    break;
                }
            }
            if (col == null)
                throw new InvalidOperationException("Column specified '" + colName + "' does not exist!");
            return GetRowData(row, col);

        }
        /// <summary>
        /// Returns the data in the row, resolved to one of the main data types (int,bool,datetime,string,double).  Note that int, double and datetime are all set to nullable types - +?.
        /// </summary>
        /// <param name="row">Data Row</param>
        /// <param name="col">Data Column </param>
        /// <returns>Object</returns>
        public static object GetRowData(DataRow row, DataColumn col)
        {
            object o = row[col.ColumnName];
            System.Type type = col.DataType;
            if (type == typeof(System.UInt16) || type == typeof(System.UInt32) ||
                type == typeof(System.Int16) || type == typeof(System.Int32) ||
                type == typeof(int) || type == typeof(uint) || type == typeof(short) ||
                type == typeof(byte) || type == typeof(Byte))
            {
                if (!(o is System.DBNull))
                {
                    o = Convert.ToInt32(o);
                }
                else
                {
                    o = 0;
                }
            }
            else if (type == typeof(System.UInt64) || type == typeof(System.Int64) || type == typeof(long))
            {
                if (!(o is System.DBNull))
                {
                    o = (long)Convert.ToInt64(o);
                }
                else
                {
                    o = 0;
                }
            }
            else if (type == typeof(string) || type == typeof(String) || type == typeof(Char) || type == typeof(char))
            {
                o = (string)o;
            }
            else if (type == typeof(MySql.Data.Types.MySqlDateTime))
            {
                MySql.Data.Types.MySqlDateTime mysqlData = (MySql.Data.Types.MySqlDateTime)o;
                DateTime d = DateTime.MinValue;
                try
                {
                    if (mysqlData.IsValidDateTime)
                    {
                        d = mysqlData.GetDateTime();
                    }
                }
                catch
                {
                    d = DateTime.MinValue;
                }
                o = d;

            }
            else if (type == typeof(DateTime))
            {
                if (!(o is System.DBNull))
                {
                    o = (DateTime)Convert.ToDateTime(o);
                }
                else
                {
                    o = DateTime.MinValue;
                }
            }
            else if (type == typeof(double) || type == typeof(Double) || type == typeof(float))
            {
                if (!(o is System.DBNull))
                {
                    o = Convert.ToDouble(o);
                }
                else
                {
                    o = 0;
                }

            }
            else if (type == typeof(bool) || type == typeof(Boolean) || type == typeof(sbyte) || type == typeof(SByte))
            {
                if (!(o is System.DBNull))
                {
                    o = Convert.ToBoolean(o);
                }
                else
                {
                    o = false;
                }
            }
            return o;
        }

        /// <summary>
        /// Returns the row, as a comma-seperated list of columns according to the schema
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        
        public static string GetRowInsertColumns(DataRow row)
        {

            StringBuilder sb = new StringBuilder();
            sb.Append("(");
            for (int i = 0; i < row.Table.Columns.Count; i++)
            {
                DataColumn col = row.Table.Columns[i];
                if (i > 0)
                    sb.Append(", ");
                sb.Append(col.ColumnName);
            }
            sb.Append(")");
            return sb.ToString();
        }
        /// <summary>
        /// Returns the row, as a comma-seperated list of values, as used in the INSERT SQL statement
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static string GetRowInsertValues(DataRow row)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("(");
            for (int i = 0; i < row.Table.Columns.Count; i++)
            {
                DataColumn col = row.Table.Columns[i];
                object o = GetRowData(row, col);
                if (i > 0)
                    sb.Append(", ");
                sb.Append(CS.General_20090518.MySQL.forMySql(o));
            }
            sb.Append(")");
            return sb.ToString();
        }
        /// <summary>
        /// Generates an sql query for inserting the specified rows in the table.
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="TableName"></param>
        /// <returns></returns>
        public static string GenerateInsertSQLForRows(DataRowCollection coll, string TableName)
        {
            List<DataRow> list = new List<DataRow>();
            for (int i = 0; i < coll.Count; i++)
            {
                list.Add(coll[i]);
            }
            return GenerateInsertSQLForRows(list, TableName);
        }
        /// <summary>
        /// Generates an sql query for inserting the specified rows in the table.
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="TableName"></param>
        /// <returns></returns>
        public static string GenerateInsertSQLForRows(List<DataRow> rows, string TableName)
        {
            StringBuilder sql = new StringBuilder();

            if (rows != null && rows.Count > 0)
            {
                sql.Append("INSERT INTO " + TableName);
                sql.Append(GetRowInsertColumns(rows[0]));
                sql.Append(" VALUES ");
                for (int i = 0; i < rows.Count; i++)
                {
                    if (i > 0)
                        sql.Append(", ");
                    sql.Append(GetRowInsertValues(rows[i]));
                }
                sql.Append(";");
            }
            return sql.ToString();
        }

        /// <summary>
        /// Saves a row into table
        /// </summary>
        /// <param name="row">Row</param>
        /// <param name="TableName">Table Name</param>
        public static void SaveRow(DataRow row, string TableName)
        {
            StringBuilder sqlInsertCols = new StringBuilder();
            StringBuilder sqlInsertValues = new StringBuilder();
            StringBuilder sqlUpdate = new StringBuilder();
            StringBuilder sqlInsert = new StringBuilder();
            StringBuilder sql = new StringBuilder();

            sql.Append("INSERT INTO " + TableName + " (");
            bool addedOne = false;
            for (int i = 0; i < row.Table.Columns.Count; i++)
            {
                DataColumn col = row.Table.Columns[i];
                object o = GetRowData(row, col);
                if (o != null)
                {

                    if (addedOne)
                    {
                        sqlInsertCols.Append(", ");
                        sqlUpdate.Append(", ");
                        sqlInsertValues.Append(", ");
                    }

                    addedOne = true;
                    sqlInsertCols.Append(col.ColumnName);
                    sqlInsertValues.Append(CS.General_20090518.MySQL.forMySql(o));
                    sqlUpdate.Append(col.ColumnName + " = " + CS.General_20090518.MySQL.forMySql(o));
                }
            }
            sql.Append(sqlInsertCols.ToString() + ") VALUES (" + sqlInsertValues + ")");
            sql.Append(" ON DUPLICATE KEY UPDATE " + sqlUpdate.ToString());
            sql.Append(";");
            CS.Database.ExecuteSQL(sql.ToString());

        }
        public static void ExecuteSQL(StringBuilder SQL)
        {
            CS.Database.ExecuteSQL(SQL);
        }
        public static void ExecuteSQL(string SQL)
        {
            CS.Database.ExecuteSQL(SQL);
        }
        
        /// <summary>
        /// Gets the next ID from a table, of column ID
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public static int GetNextIDFromTable(string table)
        {
            return GetNextIDFromTable(table, "ID");
        }
        
        /// <summary>
        /// Gets an ID from database and saves it, if no ID is specified ( = 0)
        /// </summary>
        public static int GetNextIDFromTable(string table, string colName)
        {
            int cnt = 0;
            int ID = 0;
            bool ok = false;
            while (!ok)
            {
                try
                {
                    ID = CS.Database.getNextValueFromTable(table, colName);
                    string SQL = "INSERT INTO " + table + " (" + colName + ") VALUES (" + ID.ToString() + ");";
                    CS.Database.ExecuteSQL(SQL);
                    ok = true;
                }
                catch (Exception ex)
                {
                    if (cnt > 50)
                    {
                        throw ex;
                    }
                    cnt++;
                    ok = false;
                }
            }
            return ID;
        }
    }
}
