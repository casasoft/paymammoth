using System;
using System.Collections;
using System.Text;
using MySql.Data.MySqlClient;
namespace CS
{
    public class DBFieldList
    {
        private MySqlConnection conn = null;
        public void setConnection(MySqlConnection conn)
        {
            this.conn = conn;
        }
        public DBFieldList()
        {
            _fields = new ArrayList();
        }
        ~DBFieldList()
        {
            _fields.Clear();
            _fields = null;
        }
        
        public class FIELD
        {
            public string name = "";
            public string value = "";
            public CS.General_20090518.Encoding.ENCODING_TYPE EncodingType = CS.General_20090518.Encoding.ENCODING_TYPE.Normal;

        }
        private ArrayList _fields = new ArrayList();

        /// <summary>
        /// Returns the whole list, as required for an SQL INSERT, e.g (VAL1,VAL2) VALUES ('test',5)
        /// </summary>
        /// <returns>The whole list, as required for an SQL INSERT, e.g (VAL1,VAL2) VALUES ('test',5), etc.</returns>
        public string forInsert()
        {
            string all,names = "",values = "";
            for (int i = 0; i < _fields.Count; i++)
            {
                FIELD field =  (FIELD)_fields[i];
                if (names != "")
                {
                    names += ", ";
                    values += ", ";
                }
                names += field.name;
                values += field.value;
            }
            all = "(" + names + ") VALUES (" + values + ")";
            return all;
        }

        /// <summary>
        /// Returns the whole list, as required for an SQL Update, e.g Val1 = 'test', Val2 = 5, etc.
        /// </summary>
        /// <returns>The whole list, as required for an SQL Update, e.g Val1 = 'test', Val2 = 5, etc.</returns>
        public string forUpdate()
        {
            string all = "";
            for (int i = 0; i < _fields.Count; i++)
            {
                FIELD field = (FIELD)_fields[i];
                if (all != "")
                {
                    all += ", ";
                }
                all += field.name + " = " + field.value;
            }
            return all;
        }

        public void Add(string name, object value)
        {
            Add(name, value, CS.General_20090518.Encoding.ENCODING_TYPE.Normal);
        }
        public void Add(string name, object value, CS.General_20090518.Encoding.ENCODING_TYPE encodingType)
        {
            bool ok = true;
            if (value == null)
                ok = false;
            //this was removed on 14/03/2008, because I think that where it can, values are mapped to DB.  However, as in the case for doubles
            //the min value for C# is not allowed in MySQL, so do not even set if it is left as minvalue.

            /*if (ok && ((value is double && (double)value == Double.MinValue) ||
                 (value is int && (int)value == Int32.MinValue) ||
                 (value is CS.Types.MyBool && !((CS.Types.MyBool)value).IsSet) ||
                 (value is DateTime && (DateTime)value == DateTime.MinValue)))
            {
                ok = false;
                ok = true;
            }*/
            
            if (ok && ((value is double && (double)value == Double.MinValue)))
            {
                ok = false;
                
            }
            if (ok)
            {
                
                FIELD field = new FIELD();
                field.name = name;
                field.EncodingType = encodingType;
                object data = value;
                if (encodingType == CS.General_20090518.Encoding.ENCODING_TYPE.UTF8)
                {
                    data = CS.General_20090518.Encoding.UTF8.Encode((string)data);
                }
                field.value = CS.General_20090518.MySQL.forMySql(data);
                _fields.Add(field);
            }
        }
        
        public void Remove(string name)
        {
            _fields.Remove(this[name]);
        }

        public void Clear()
        {
            _fields.Clear();
        }



        public int Count
        {
            get
            {
                return _fields.Count;
            }
        }

        /// <summary>
        /// Returns the field
        /// </summary>
        /// <param name="name">String containing Name of field</param>
        /// <returns></returns>
        public FIELD this[string name]
        {
            get
            {
                for (int i = 0; i < _fields.Count; i++)
                {
                    FIELD field = (FIELD)_fields[i];
                    if (field.name.ToUpper() == name.ToUpper())
                    {
                        return field;
                    }
                }
                return null;
            }
            set
            {
                for (int i = 0; i < _fields.Count; i++)
                {
                    FIELD field = (FIELD)_fields[i];
                    if (field.name.ToUpper() == name.ToUpper())
                    {
                        _fields[i] = value;
                        break;
                    }
                }
            }

        }

        /// <summary>
        /// Returns the field
        /// </summary>
        /// <param name="index">Integer of the index</param>
        /// <returns></returns>
        public FIELD this[int index]
        {
            get
            {
                return (FIELD)_fields[index];
            }
            set
            {
                _fields[index] = value;
            }
        }

        /// <summary>
        /// Performs an insert or update on the specified table
        /// </summary>
        /// <param name="table">Table</param>
        public void executeAsSQL(string table)
        {
            
            string SQL = "INSERT INTO " + table + " " + forInsert() + " ON DUPLICATE KEY UPDATE " +
                    forUpdate() + ";";
            Database.ExecuteSQL(SQL,conn);
        }
    }
}
