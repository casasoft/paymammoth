﻿using System;
using System.Collections.Generic;

using System.Text;
using System.IO;

namespace CS.General_v3.FFMpeg
{
    public class Converter
    {
        public class PARAMS
        {
            public int Width { get; set; }
            public int Height { get; set; }
            public PARAMS()
            {
                Width = 0;
                Height = 0;
            }
            public override string ToString()
            {
                StringBuilder sb = new StringBuilder();
                if (Width > 0 || Height > 0)
                {
                    sb.Append(" -s " + Width + "x" + Height);
                }
                sb.Append(" ");
                return sb.ToString();
            }
        }
        private PARAMS _Params = new PARAMS();
        public PARAMS Params { get { return _Params; } }
        public static Converter CreateConverter(string inputVideo, bool runAsynch)
        {
            Converter c = new Converter(Enums.SETTINGS_ENUM.Others_FFMpeg_Path.GetSettingFromDatabase<string>(), inputVideo, runAsynch);
            return c;
        }
        public static Converter CreateConverter(string inputVideo)
        {
            return CreateConverter(inputVideo, false);
        }
        public bool RunAsynchronously { get; set; }
        public Converter(string ffmpegLocation, string inputVideo, bool runAsynch)
        {
            this.ffMpegLocation = ffmpegLocation;
            if (!File.Exists(ffmpegLocation))
                throw new InvalidDataException("FFMpeg does not exist at [" + ffmpegLocation + "]");
            this.RunAsynchronously = runAsynch;
            _InputVideo = inputVideo;
            _p = new CS.General_v3.Util.Process.MyProcess(ffmpegLocation);
        }
        public Converter(string ffmpegLocation, string inputVideo)
            : this(ffmpegLocation, inputVideo, false)
        {

        }


        private CS.General_v3.Util.Process.MyProcess _p = null;
        private CS.General_v3.Util.Process.MyProcess p
        {
            get
            {
                return _p;
            }
        }
        public string ffMpegLocation { get; set; }
        private string _InputVideo;
        public string InputVideo { get { return _InputVideo; } set { _InputVideo = value; } }

        public List<bool> GenerateThumbnails(List<string> paths, int width, int height, Enums.CROP_IDENTIFIER cropIdentifier)
        {
            return GenerateThumbnails(paths, 0, GetInputVideoDuration(), width, height, cropIdentifier);
        }
        public List<bool> GenerateThumbnails(List<string> paths, int startTime, int endTime, int width, int height, Enums.CROP_IDENTIFIER cropIdentifier)
        {
            int length = endTime - startTime;
            double curr = startTime;
            double interval = (double)length / (double)(paths.Count - 1);
            List<bool> result = new List<bool>();
            for (int i = 0; i < paths.Count; i++)
            {
                int currSecond = System.Convert.ToInt32(Math.Round(curr));
                bool b = GenerateThumbnail(currSecond, paths[i]);
                CS.General_v3.Classes.Attributes.SettingsInfoAttribute defaultValues = new Classes.Attributes.SettingsInfoAttribute();
                if (b)
                {
                    int w = width;
                    int h = height;

                    if (cropIdentifier != Enums.CROP_IDENTIFIER.None)
                        CS.General_v3.Util.Image.ResizeAndCropImage(paths[i], w, h, paths[i], Enums.CROP_IDENTIFIER.MiddleMiddle);
                    else
                        CS.General_v3.Util.Image.ResizeImage(paths[i], w, h, paths[i]);
                }
                result.Add(b);
                curr += interval;
            }
            return result;
        }
        public List<bool> GenerateThumbnails(string path, int totalThumbnails, string IDToken, int width, int height, Enums.CROP_IDENTIFIER cropIdentifier)
        {
            return GenerateThumbnails(path, totalThumbnails, IDToken, 0, GetInputVideoDuration(), width, height, cropIdentifier);
        }
        public List<bool> GenerateThumbnails(string path, int totalThumbnails, string IDToken, int startTime, int endTime, int width, int height, Enums.CROP_IDENTIFIER cropIdentifier)
        {
            string currPath;
            List<string> paths = new List<string>();
            for (int i = 1; i <= totalThumbnails; i++)
            {
                currPath = path;
                currPath = currPath.Replace(IDToken, i.ToString("00"));
                paths.Add(currPath);
            }
            return GenerateThumbnails(paths, startTime, endTime, width, height, cropIdentifier);
        }
        public List<bool> GenerateThumbnails(string path, int totalThumbnails, int width, int height, Enums.CROP_IDENTIFIER cropIdentifier)
        {
            return GenerateThumbnails(path, totalThumbnails, "{ID}", width, height, cropIdentifier);
        }
        public List<bool> GenerateThumbnails(string path, int totalThumbnails, int startTime, int endTime, int width, int height, Enums.CROP_IDENTIFIER cropIdentifier)
        {
            return GenerateThumbnails(path, totalThumbnails, "{ID}", startTime, endTime, width, height, cropIdentifier);
        }

        public bool GenerateThumbnail(int secondInterval, string path)
        {
            if (File.Exists(path))
                File.Delete(path);
            _p.Arguments = "-i \"" + InputVideo + "\" -deinterlace -an -ss " + secondInterval + " -t 00:00:01 -r 1 -y -vcodec mjpeg -f mjpeg \"" + path + "\"";
            _p.Start(this.RunAsynchronously);
            return File.Exists(path);
        }

        public int GetInputVideoDuration()
        {
            _p.Arguments = "-i \"" + InputVideo + "\"";
            _p.Start();
            string s = _p.GetErrorText();
            int durationLoc = s.IndexOf("Duration:") + 10;
            int hours = System.Convert.ToInt32(s.Substring(durationLoc, 2)); durationLoc += 3;
            int minutes = System.Convert.ToInt32(s.Substring(durationLoc, 2)); durationLoc += 3;
            int seconds = System.Convert.ToInt32(s.Substring(durationLoc, 2)); durationLoc += 2;
            int commaPos = s.IndexOf(",", durationLoc);
            int milleSecs = System.Convert.ToInt32(Math.Round((double)1000 * System.Convert.ToDouble("0" + s.Substring(durationLoc, commaPos - durationLoc))));

            int totSeconds = seconds + (minutes * 60) + (hours * 60 * 60);
            return totSeconds;

        }
        public bool Convert(string outputVideo)
        {
            if (File.Exists(outputVideo))
                File.Delete(outputVideo);
            _p.Arguments = "-i \"" + InputVideo + "\" \"" + outputVideo + "\"" + Params.ToString();
            _p.Start();
            bool ok = false;
            if (File.Exists(outputVideo))
            {
                FileStream fs = new FileStream(outputVideo, FileMode.Open);
                ok = (fs.Length > 0);
                fs.Close();
                fs.Dispose();
                if (!ok)
                    File.Delete(outputVideo);
            }
            return ok;


        }


    }
        
}
