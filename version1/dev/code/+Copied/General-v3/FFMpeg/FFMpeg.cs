﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;


namespace CS.General_v3.FFMpeg
{
    public class FFMpeg
    {
        #region Statics
        /// <summary>
        /// Returns the video duration, in seconds
        /// </summary>
        /// <param name="videoFile"></param>
        /// <returns></returns>
        public static int GetMediaDuration(string videoFile)
        {
            int totSeconds = 0;
            if (File.Exists(videoFile))
            {
                FFMpeg ffmpeg = new FFMpeg();
                ffmpeg.GeneralInfo.InputFile = videoFile;
                ffmpeg.RunFFMpeg(false,"-y");

                string s = ffmpeg.ProcessInfo.ErrorText;

                var match = Regex.Match(s, "Duration: ([0-9:.]+),");
                if (match.Success)
                {
                    string sDuration = match.Groups[1].Value;
                    int hours = System.Convert.ToInt32(sDuration.Substring(0, 2));
                    int minutes = System.Convert.ToInt32(sDuration.Substring(3, 2));
                    int seconds = System.Convert.ToInt32(sDuration.Substring(6, 2));
                    int dotPos = sDuration.IndexOf('.');
                    int millisecs = 0;
                    if (dotPos > -1)
                    {
                        millisecs = Convert.ToInt32(sDuration.Substring(dotPos + 1));
                    }


                    totSeconds = seconds + (minutes * 60) + (hours * 60 * 60);
                   // if (millisecs >= 50)
                    //    totSeconds++;

                }
                if (totSeconds == 0)
                {
                    totSeconds = getVideoDurationByBruteForce(videoFile);
                }
                
            }
            return totSeconds;

        }

        private static int getVideoDurationByBruteForce(string videoFile)
        {
            string tmpPath = videoFile + "_" + CS.General_v3.Util.Random.GetString(10, 10) + ".jpg";
            int duration = 0;
            int lastDuration = 0;
            
            int lastOKDuration = 0;
            int currInterval = 180; //seconds;
            do
            {
                CS.General_v3.FFMpeg.FFMpeg.GenerateThumbnail(videoFile, tmpPath, duration, null, null, Enums.CROP_IDENTIFIER.None);
                int fileSize = (int)CS.General_v3.Util.IO.GetFileSizeInBytes(tmpPath);
                if (fileSize > 0)
                {
                    lastOKDuration = duration;
                    duration += currInterval;
                    //image exists;
                }
                else
                {
                    if (currInterval == 1 && (duration == lastOKDuration + 1))
                    {
                        duration = lastOKDuration;
                        break;
                    }
                    else
                    {
                        currInterval = currInterval / 2;
                        if (currInterval < 5) //if interval is smaller than 5 seconds set to 1 second
                            currInterval = 1;
                        duration -= currInterval;
                        
                    }
                }
                

                lastDuration = duration;
            } while (true && duration > 0);
            if (duration < 0)
                duration = 0;
            if (File.Exists(tmpPath))
                File.Delete(tmpPath);
            return duration;

        }
        /// <summary>
        /// Generates thumbnails, at the specified paths.  The thumbnails are split throughout the video, 
        /// so if 3 paths are specified, 3 thumbnails are taken, at the start, at the middle, at the end.
        /// </summary>
        /// <param name="paths"></param>
        /// <returns></returns>
        public static List<bool> GenerateThumbnails(string videoPath, List<string> outputPaths, int? width, int? height, Enums.CROP_IDENTIFIER cropIdentifier)
        {
            return GenerateThumbnails(videoPath, outputPaths, 0, GetMediaDuration(videoPath), width, height, cropIdentifier);
        }
        /// <summary>
        /// Generates thumbnails, at the specified paths.  The thumbnails are split throughout the video from the specified time, 
        /// so if 3 paths are specified, 3 thumbnails are taken, at the start, at the middle, at the end.
        /// </summary>
        /// <param name="paths"></param>
        /// <param name="startTime">Start time (Seconds)</param>
        /// <param name="endTime">end time (Seconds)</param>
        /// <returns></returns>
        public static List<bool> GenerateThumbnails(string videoPath, List<string> outputPaths, int startTime, int endTime,
            int? width, int? height, Enums.CROP_IDENTIFIER cropIdentifier)
        {
            int length = endTime - startTime;
            double curr = startTime;
            double interval = (double)(length-1) / (double)(outputPaths.Count - 1);
            List<bool> result = new List<bool>();
            if (outputPaths.Count == 1)
            {
                curr = startTime + (length / 2);

            }
            for (int i = 0; i < outputPaths.Count; i++)
            {
                int currSecond = System.Convert.ToInt32(Math.Round(curr));
                bool b = GenerateThumbnail(videoPath, outputPaths[i], currSecond, width, height, cropIdentifier);
                
                result.Add(b);
                curr += interval;
            }
            return result;
        }
        /// <summary>
        /// Generates a 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="totalThumbnails"></param>
        /// <param name="IDToken"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="cropIdentifier"></param>
        /// <returns></returns>
        public static List<bool> GenerateThumbnails(string videoPath, string outputPath, int totalThumbnails, string IDToken,
            int? width, int? height, Enums.CROP_IDENTIFIER cropIdentifier)
        {
            return GenerateThumbnails(videoPath,outputPath, totalThumbnails, IDToken, 0, GetMediaDuration(videoPath),width,height,cropIdentifier);
        }
        /// <summary>
        /// Generates thumbnails, to the output path.  The output path must contain an ID TOKEN, which is specified in ID TOKEN, and is replaced
        /// with 01,02,03,etc for the numbers of the thumbnails.
        /// </summary>
        /// <param name="videoPath">Video Path</param>
        /// <param name="outputPath">Output Path. Must contain an ID Token</param>
        /// <param name="totalThumbnails">Total thumbnails to create, split evenly between start and end time</param>
        /// <param name="IDToken">ID Token.  This is replaced with the thumbnail number</param>
        /// <param name="startTime">Start Time. In seconds</param>
        /// <param name="endTime">End Time. In seconds</param>
        /// <param name="width">Width. Optional</param>
        /// <param name="height">Height. Optional</param>
        /// <param name="cropIdentifier">Crop Identifier</param>
        /// <returns></returns>
        public static List<bool> GenerateThumbnails(string videoPath, string outputPath,  int totalThumbnails,
            string IDToken, int startTime, int endTime, int? width, int? height, Enums.CROP_IDENTIFIER cropIdentifier)
        {
            string currPath;

            List<string> paths = new List<string>();
            for (int i = 1; i <= totalThumbnails; i++)
            {
                currPath = outputPath;
                currPath = currPath.Replace(IDToken, i.ToString("00"));
                paths.Add(currPath);
            }
            return GenerateThumbnails(videoPath,paths, startTime, endTime, width, height, cropIdentifier);
        }
        /// <summary>
        /// Generates thumbnails, to the output path.  The output path must contain an {ID} string, that is replaced with the number of the thumbnail
        /// </summary>
        /// <param name="videoPath"></param>
        /// <param name="outputPath"></param>
        /// <param name="totalThumbnails"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="cropIdentifier"></param>
        /// <returns></returns>
        public static List<bool> GenerateThumbnails(string videoPath, string outputPath, int totalThumbnails,
            int width, int height, Enums.CROP_IDENTIFIER cropIdentifier)
        {
            return GenerateThumbnails(videoPath,outputPath, totalThumbnails, "{ID}", width, height, cropIdentifier);
        }
        /// <summary>
        /// Generates thumbnails, to the output path.  The output path must contain an {ID} string, that is replaced with the number of the thumbnail
        /// </summary>
        /// <param name="videoPath"></param>
        /// <param name="outputPath"></param>
        /// <param name="totalThumbnails"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="cropIdentifier"></param>
        /// <returns></returns>
        public static List<bool> GenerateThumbnails(string videoPath, string outputPath, int totalThumbnails, int startTime,
            int endTime, int width, int height, Enums.CROP_IDENTIFIER cropIdentifier)
        {
            return GenerateThumbnails(videoPath, outputPath, totalThumbnails, "{ID}", startTime, endTime, width, height, cropIdentifier);
        }

        /// <summary>
        /// Generates a thumbnail from a video, at the specified time
        /// </summary>
        /// <param name="videoPath">Video Path</param>
        /// <param name="secondInterval">Time interval, in seconds</param>
        /// <param name="outputPath">Output path for thumbnail</param>
        /// <param name="width">Width (Optional)</param>
        /// <param name="height">Height (Optional)</param>
        /// <param name="cropIdentifier">Crop Identifier.  Optional</param>
        /// <returns></returns>
        public static bool GenerateThumbnail(string videoPath, string outputPath, int secondInterval,
            int? width, int? height, Enums.CROP_IDENTIFIER cropIdentifier)
        {
            if (File.Exists(outputPath))
                File.Delete(outputPath);
            FFMpeg ffmpeg = new FFMpeg();
            ffmpeg.ClearParameters(false);
            ffmpeg.GeneralInfo.OverwriteExisting = true;
            ffmpeg.GeneralInfo.InputFile = videoPath;
            ffmpeg.GeneralInfo.StartFrom = secondInterval;
            ffmpeg.GeneralInfo.OutputFile = outputPath;
            ffmpeg.GeneralInfo.Width = width;
            ffmpeg.GeneralInfo.Height = height;
            ffmpeg.GeneralInfo.SetFormat(VIDEO_FORMAT.MJPEG);
            ffmpeg.VideoOptions.VideoCodec = "mjpeg";
            ffmpeg.VideoOptions.NumberOfFramesToRecord = 1;
            ffmpeg.AudioOptions.DisableAudio = true;

                
            ffmpeg.RunFFMpeg(false);
            if (File.Exists(outputPath))
            {
                if (width != null && height != null && cropIdentifier != Enums.CROP_IDENTIFIER.None)
                {
                    CS.General_v3.Util.Image.CropImage(outputPath, width.Value, height.Value, cropIdentifier);
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Generates a thumbnail from a video, at the specified time
        /// </summary>
        /// <param name="videoPath">Video Path</param>
        /// <param name="secondInterval">Time interval, in seconds</param>
        /// <param name="outputPath">Output path for thumbnail</param>
        /// <param name="width">Width (Optional)</param>
        /// <param name="height">Height (Optional)</param>
        /// <param name="cropIdentifier">Crop Identifier.  Optional</param>
        /// <returns></returns>
        public static bool GenerateThumbnailAtCenter(string videoPath, string outputPath,
            int? width, int? height, Enums.CROP_IDENTIFIER cropIdentifier)
        {
            int duration = GetMediaDuration(videoPath);
            int sec = duration / 2;
            return GenerateThumbnail(videoPath,outputPath,sec,width,height,cropIdentifier);
        }


        
        #endregion


        public FFMpeg()
        {
            ClearParameters(true);
        }
        public void ClearParameters(bool initDefaultValues)

        {
            this.Information = new INFORMATION();
            this.AudioOptions = new AUDIO_OPTIONS();
            this.VideoOptions = new VIDEO_OPTIONS();
            this.GeneralInfo = new GENERAL_INFO(this);
            this.ProcessInfo = new PROCESS_INFO();
            if (initDefaultValues)
            {
                this.AudioOptions.InitDefaultValues();
                this.VideoOptions.InitDefaultOptions();
                this.GeneralInfo.InitDefaultValues();
            }
            
        }
        public class GENERAL_INFO
        {
            public GENERAL_INFO(FFMpeg ffmpeg)
            {
                _ffmpeg = ffmpeg;
            }
            public void InitDefaultValues()
            {
                this.OverwriteExisting = true;

            }
            private FFMpeg _ffmpeg = null;
            public string InputFile { get; set; }
            public string OutputFile { get; set; }
            public bool OverwriteExisting {get;set;}
            /// <summary>
            /// Sets the video format.  Can use the 'SetFormat' to use an enumeration
            /// </summary>
            public string ForceFormat {get;set;}
            /// <summary>
            /// Must be a multiple of 2
            /// </summary>
            public int? Width { get; set; }
            /// <summary>
            /// Must be a multiple of 2
            /// </summary>
            public int? Height { get; set; }
            public void SetFormat(VIDEO_FORMAT format)
            {
                this.ForceFormat = ConvertVideoFormatToCode(format);
                if( format == VIDEO_FORMAT.FLV)
                {
                    _ffmpeg.VideoOptions.VideoCodec = "flv";
                }
            }
            /// <summary>
            /// Restrict the transcoded/captured video sequence to the duration specified in seconds. hh:mm:ss[.xxx] syntax is also supported. 
            /// </summary>
            public int? DurationSeconds { get; set; }
            /// <summary>
            /// Sets the start position, in seconds
            /// </summary>
            public int? StartFrom { get; set; }
            /// <summary>
            /// Set the input time offset in seconds. [-]hh:mm:ss[.xxx] syntax is also supported. This option affects all the input files that follow it. The offset is added to the timestamps of the input files. Specifying a positive offset means that the corresponding streams are delayed by 'offset' seconds. 
            /// </summary>
            public int? InputTimeOffset { get; set; }
            /// <summary>
            /// Set the file size limit. 
            /// </summary>
            public int? MaxFileSize {get;set;}
            public override string  ToString()
            {
                if (this.Width.HasValue && this.Width.Value % 2 != 0)
                    this.Width++;
                if (this.Height.HasValue && this.Height.Value % 2 != 0)
                    this.Height++;

 	            string s = "";
                if (this.OverwriteExisting) s += " -y";
                if (!string.IsNullOrEmpty(ForceFormat)) s += " -f " + this.ForceFormat.ToLower() + "";
                if (StartFrom.HasValue) s+= " -ss " + this.StartFrom.Value + "";
                if (InputTimeOffset.HasValue) s+= " -itsoffset " + this.InputTimeOffset.Value + "";
                if (DurationSeconds.HasValue) s += " -t " + this.DurationSeconds.Value + "";
                if (MaxFileSize.HasValue) s+= " -fs  " + this.MaxFileSize.Value + "";
                if (this.Width != null && this.Height != null) s += " -s " + this.Width.Value + "x" + this.Height.Value;
                
                return s;
                            
            }

        }
        public class INFORMATION
        {
            public string Title { get; set; }
            public string Author { get; set; }
            public string Copyright { get; set; }
            public string Comment { get; set; }
            public string Album { get; set; }
            public int? TrackNumber { get; set; }
            public int? Year {get;set;}
        
            public override string  ToString()
            {
                string s = "";
                
                if (!string.IsNullOrEmpty(Title)) s+= " -title \"" + Title + "\"";
                if (!string.IsNullOrEmpty(Author)) s+= " -author \"" + Author + "\"";
                if (!string.IsNullOrEmpty(Copyright)) s+= " -copyright \"" + Copyright + "\"";
                if (!string.IsNullOrEmpty(Comment)) s+= " -comment \"" + Comment + "\"";
                if (!string.IsNullOrEmpty(Album)) s+= " -album \"" + Album + "\"";
                if (TrackNumber != null) s+= " -track " + TrackNumber.Value.ToString();
                if (Year != null) s+= " -year " + Year.Value.ToString();
                return s;
            }
        }
        public class AUDIO_OPTIONS
        {
            
            public AUDIO_OPTIONS()
            {

            }
            public void InitDefaultValues()
            {
                this.AudioCodec = "1";
                this.AudioSamplingFrequency = 44100;

            }
            public bool DisableAudio {get;set;}
            /// <summary>
            /// In Hertz. Set the audio sampling frequency (default = 44100 Hz). 
            /// </summary>
            public int? AudioSamplingFrequency {get;set;}
            /// <summary>
            /// In KB. Set the audio bitrate in bit/s (default = 64k). 
            /// </summary>
            public int? AudioBitRate {get;set;}
            /// <summary>
            /// Force audio codec to codec. 
            /// </summary>
            public string AudioCodec {get;set;}
            public override string  ToString()
            {
                string s = "";
                if (this.AudioSamplingFrequency != null) s+= " -ar " + this.AudioSamplingFrequency.Value;
                if (this.AudioBitRate!= null) s+= " -ab " + this.AudioSamplingFrequency.Value;
                if (!string.IsNullOrEmpty(this.AudioCodec)) s += " -ac " + this.AudioCodec;
                if (DisableAudio) s += " -an";
                return s;
            }
        }
        public class VIDEO_OPTIONS
        {
            public VIDEO_OPTIONS()
            {
                
            }
            public void InitDefaultOptions()
            {
                this.DeInterlace = true;
                this.Trellis = 1;
                this.GroupOfPicturesSize = 160;
                this.CMP = "dct";
                this.SUBCMP = "dct";
                this.MBD = 2;
            }
            public int? MBD {get;set;}
            /// <summary>
            /// Specify target file type ("vcd", "svcd", "dvd", "dv", "dv50", "pal-vcd", "ntsc-svcd", ... ). All the format options (bitrate, codecs, buffer sizes) are then set automatically. You can just type: 
            /// </summary>
            public TARGET_TYPE? TargetType { get; set; }
            /// <summary>
            /// Bitrate, in kilobytes
            /// </summary>
            public int? BitRate { get; set; }
            /// <summary>
            /// Frames to record
            /// </summary>
            public int? NumberOfFramesToRecord { get; set; }
            public int? FramesPerSecond { get; set; }
            
            public string CMP {get;set;}
            public string SUBCMP {get;set;}
            
            public ASPECT_RATIO? AspectRatio {get;set;}
            /// <summary>
            /// in pixels
            /// </summary>
            public int? CropFromTop {get;set;}
            /// <summary>
            /// in pixels
            /// </summary>
            public int? CropFromLeft {get;set;}
/// <summary>
            /// in pixels
            /// </summary>
            public int? CropFromRight {get;set;}
/// <summary>
            /// in pixels
            /// </summary>
            public int? CropFromBottom {get;set;}
            /// <summary>
            /// In 'KB'. Default is 4000. Set video bitrate tolerance (in bits, default 4000k). Has a minimum value of: (target_bitrate/target_framerate). In 1-pass mode, bitrate tolerance specifies how far ratecontrol is willing to deviate from the target average bitrate value. This is not related to min/max bitrate. Lowering tolerance too much has an adverse effect on quality. 
            /// </summary>
            public int? BitRateTolerance {get;set;}
            private int? _MaxBitRate = null;
            /// <summary>
            /// In KB. Set max video bitrate. Requires -bufsize to be set. 
            /// </summary>
            public int? MaxBitRate
            {
                get
                {
                    return _MaxBitRate;
                }
                set
                {
                    _MaxBitRate = value;
                    if (_MaxBitRate != null &&  BufferSize == null)
                        BufferSize = 64;
                }
            }
            /// <summary>
            /// In KB. Set min video bitrate. Most useful in setting up a CBR encode: 
            /// </summary>
            public int? MinBitRate {get;set;}
            /// <summary>
            /// Set video buffer verifier buffer size. In Kb.
            /// </summary>
            public int? BufferSize {get;set;}
            public int? Trellis {get;set;}
            
            public int? GroupOfPicturesSize {get;set;}
            public bool DeInterlace { get; set; }
            public bool TwoPass {get;set;}
            /// <summary>
            /// Force video codec to codec. Use the copy special value to tell that the raw codec data must be copied as is. 
            /// </summary>
            public string VideoCodec {get;set;}
            public override string  ToString()
            {
                string s = "";
                if (this.TargetType != null) s += " -target " + Enum.GetName(typeof(TARGET_TYPE),this.TargetType).Replace("_","-");
                if (this.BitRate != null) s += " -b " + this.BitRate.Value + "kb";
                if (this.NumberOfFramesToRecord != null) s += " -vframes " + this.NumberOfFramesToRecord.Value;
                if (this.FramesPerSecond !=null) s+= " -r " + this.FramesPerSecond.Value;
                if (this.AspectRatio != null)
                {
                    switch (this.AspectRatio.Value)
                    {
                        case ASPECT_RATIO.Normal_4to3: s += " -aspect 4:3";break;
                        case ASPECT_RATIO.Widescreen_16to9: s += " -aspect 16:9"; break;
                    }
                }
                if (this.CMP!= null) s+= " -cmp " + this.MBD.Value;
                if (this.SUBCMP!= null) s+= " -subcmp " + this.MBD.Value;
                if (this.MBD != null) s+= " -mbd " + this.MBD.Value;
                if (this.GroupOfPicturesSize != null) s+= " -g " + this.GroupOfPicturesSize.Value;
                if (this.Trellis != null) s+= " -trellis " + this.Trellis.Value;
                if (this.CropFromTop != null) s+= " -croptop " + this.CropFromTop.Value;
                if (this.CropFromLeft != null) s+= " -cropright " + this.CropFromLeft.Value;
                if (this.CropFromRight != null) s+= " -cropleft " + this.CropFromRight.Value;
                if (this.CropFromBottom != null) s+= " -cropbottom " + this.CropFromBottom.Value;
                if (this.BitRateTolerance != null) s+= " -bt " + this.BitRateTolerance.Value + "k";
                if (this.MaxBitRate != null ) s+= " -maxrate " + this.MaxBitRate.Value + "kb";
                if (this.MinBitRate != null ) s+= " -minrate " + this.MinBitRate.Value + "kb";
                if (this.BufferSize!= null ) s+= " -bufsize " + this.BufferSize.Value + "kb";
                if (this.DeInterlace) s += " -deinterlace";
                if (!string.IsNullOrEmpty(this.VideoCodec)) s+= " -vcodec " + this.VideoCodec;
                if (this.TwoPass)
                    s+=" -pass 2";
                
                return s;

                 
            }

        }
       
        public class PROCESS_INFO
        {
            public string ErrorText { get; set; }
            public string OutputText {get;set;}
        }
        public enum ASPECT_RATIO
        {
            Normal_4to3,
            Widescreen_16to9
        }
        public enum TARGET_TYPE
        {
            VCD,SVCD,DVD,DV,DV50,PAL_VCD,NTSC_SVCD

        }
        public enum VIDEO_FORMAT
        {
            FLV,
            MPEG,
            RawVideo,
            ThreeGP,
            AAC,
            AC3,
            AVI,
            FLAC,
            GIF,
            MJPEG,
            MOV,
            MP4,
            M4A,
            MP3,
            OGG,
            WAV,
            WMV1,
            WMV2,
            WMV3,
            WMAV1,
            WMAV2,



        }
        public object Tag { get; set; }
        public object Tag2 { get; set; }
        public static string ConvertVideoFormatToCode(VIDEO_FORMAT format)
        {
            string s = "";
            switch ( format)
            {
                case VIDEO_FORMAT.ThreeGP: s = "3GP"; break;
                default:
                    s = Enum.GetName(typeof(VIDEO_FORMAT),format);
                    break;
            }
            return s;

        }
       
        public static string GetFFMpegPath()
        {
            string path = null;
            if (!string.IsNullOrWhiteSpace(CustomFFMpegPath))
                path = CustomFFMpegPath;
            else
            {
                path = CS.General_v3.Settings.GetSettingFromDatabase(Enums.SETTINGS_ENUM.Others_FFMpeg_Path);
                if (string.IsNullOrWhiteSpace(path))
                {
                    List<string> defaultPaths = new List<string>();
                    defaultPaths.Add("/_common/static/files/ffmpeg_20110806_git-6c4e9ca.exe");
                    defaultPaths.Add("/_common/static/files/ffmpeg_r25512.exe");
                    for (int i = 0; i < defaultPaths.Count; i++)
                    {
                        string p = defaultPaths[i];
                        if (File.Exists(p))
                        {
                            path = p;
                            break;
                        }
                    }
                }
            }
            return path;
        }

        public static string CustomFFMpegPath { get; set; }

        public INFORMATION Information { get; set; }
        public AUDIO_OPTIONS AudioOptions { get; set; }
        public VIDEO_OPTIONS VideoOptions { get; set; }
        public GENERAL_INFO GeneralInfo { get; set; }
        public PROCESS_INFO ProcessInfo { get; set; }
        public event EventHandler Finished;
        private void checkParams(bool asynch)
        {
            StringBuilder sb = new StringBuilder();
            if (string.IsNullOrEmpty(GetFFMpegPath()))
            {
                sb.AppendLine("FFMpegPath must be specified to use FFMpeg");
            }
            if (string.IsNullOrEmpty(this.GeneralInfo.InputFile ))
            {
                sb.AppendLine("input file must be specified");
            }
            if (sb.Length>0)
            {
                string s = "CS.General_v3.FFMpeg.FFMpeg:: " + sb.ToString();
                throw new InvalidOperationException(s);
            }
            
        }
        public void RunFFMpeg(bool asynchronous, string specificArgumentString = null)
        {
            checkParams(asynchronous);
            string parameters = "";
            if (specificArgumentString == null)
            {
                parameters = this.AudioOptions.ToString() + this.Information.ToString() + this.VideoOptions.ToString() + GeneralInfo.ToString();
                parameters = parameters.Trim();
            }
            else
            {
                parameters = specificArgumentString;
            }
            parameters = "-i \"" + this.GeneralInfo.InputFile + "\" " + parameters + " \"" + this.GeneralInfo.OutputFile + "\"";
            string ffMpegPath = FFMpeg.GetFFMpegPath();
            if (ffMpegPath.Contains("/"))
                ffMpegPath = CS.General_v3.Util.PageUtil.MapPath(ffMpegPath);

            Util.Process.MyProcess p = new Util.Process.MyProcess(ffMpegPath, parameters);
            if (asynchronous)
            {
                p.ProcessFinished +=new EventHandler(p_ProcessFinished);
            }
            p.Start(asynchronous);
            if (!asynchronous)
            {
                this.ProcessInfo.ErrorText = p.GetErrorText();
                this.ProcessInfo.OutputText = p.GetOutputText();
            }
            

        }

        private void  p_ProcessFinished(object sender, EventArgs e)
        {
            if (Finished!=null)
 	            Finished(this,null);
            Util.Process.MyProcess p = (Util.Process.MyProcess)sender;
            this.ProcessInfo.ErrorText = p.GetErrorText();
        }
    }
}
