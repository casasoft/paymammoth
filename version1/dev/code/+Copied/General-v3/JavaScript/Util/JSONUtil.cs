﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_20101215.JavaScript.Util
{
    public static class JSONUtil
    {
        /// <summary>
        /// Encodes a URL to JSON format since line breaks can't be included
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string Encode(string str)
        {
            str = str.Replace("\r", "#R#");
            str = str.Replace("\n", "#N#");
            str = str.Replace("\t", "#T#");
            return str;
        }
    }
}
