﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Web.Script.Serialization;


namespace CS.General_v3.JavaScript.Data
{
    public enum JSON_OBJECT_ENCODING
    {
        None,
        HtmlEncode,
        URLEncode
    }
    public class JSONObject 
    {
        private JSON_OBJECT_ENCODING _performEncoding = JSON_OBJECT_ENCODING.URLEncode;
        private JSON_OBJECT_ENCODING _encodedPerformed = JSON_OBJECT_ENCODING.None;

        private object urlEncodeValue(object obj, MemberInfo property, object propertyValue)
        {
            if (propertyValue is String)
            {
                return CS.General_v3.Util.PageUtil.UrlEncode((string)propertyValue);
            }
            return propertyValue;
        }

        public string GetJSON()
        {
            /*if (_encodedPerformed != _performEncoding)
            {
                _encodedPerformed = _performEncoding;
                ReflectionUtil.UpdateAllPropertiesOfObject(this, true, false, urlEncodeValue);

            }
            */

            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(this);
        }
    }
}
