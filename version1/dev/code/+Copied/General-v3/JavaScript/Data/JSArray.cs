﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using CS.General_v3.JavaScript.Interfaces;
namespace CS.General_v3.JavaScript.Data
{
    public class JSArray : IJavaScriptSerializable, IJavaScriptVar
    {
        private List<string> _values;
        public string QuotationMark { get; set; }
        public int Count { get { return _values.Count; } }
        /// <summary>
        /// Creates a Javascript array
        /// </summary>
        /// <param name="varName">The variable name in JS.  If left null, the array is an array literal</param>
        public JSArray(string varName)
        {
            this.QuotationMark = "\"";
            this._values = new List<string>();
            this.VarName = varName;
        }
        public JSArray():this(null)
        {
        }
        public string VarName { get; set; }

        public void AddRange(IEnumerable list)
        {
            bool addQuotationMarks = false;
            if (list is List<string> || list is string[])
                addQuotationMarks = true;
            AddRange(list, addQuotationMarks);
        }
        public void AddRange(IEnumerable list, bool addQuotationMarks)
        {
            IEnumerator enumerator = list.GetEnumerator();
            while (enumerator.MoveNext())
            {
                this.AddItem(CS.General_v3.Util.Text.forJS(enumerator.Current, this is JSArrayFlash),addQuotationMarks);
            }
        }
        public void AddItems(params object[] values)
        {
            for (int i = 0; i < values.Length; i++)
            {
                AddItem(values[i]);
            }
        }
        public void AddItem(object value)
        {
            if (value is IEnumerable && !(value is string))
            {
                JSArray arr = new JSArray();
                arr.AddRange((IEnumerable)value);
                this.AddItem(arr);

            }
            else
            {
                bool addQuotationMarks = false;
                string s = null;
                if (value is string)
                    addQuotationMarks = true;
                if (value != null)
                {
                    s = CS.General_v3.Util.Text.forJS(value, this is JSArrayFlash);
                }

                if (s == null) s = "null";
                AddItem(s, addQuotationMarks);
            }
        }
        public void InsertItem(int index, object value)
        {
            bool addQuotationMarks = false;
            if (value is string)
                addQuotationMarks = true;
            InsertItem(index, value.ToString(), addQuotationMarks);
        }
        public void InsertItem(int index, string value, bool addQuotationMarks)
        {
            if (addQuotationMarks)
            {
                string quotMark = QuotationMark;
                value = quotMark + value + quotMark;
            } 
            this._values.Insert(index, value);
        }
        
        public void AddItem(string value, bool addQuotationMarks)
        {


            if (addQuotationMarks)
            {
                string quotMark = QuotationMark;
                value = quotMark + value + quotMark;
            }
            this._values.Add(value);

        }

        public string GetJS(bool addCarriageReturn)
        {
            StringBuilder sb = new StringBuilder();


            if (!String.IsNullOrEmpty(VarName))
            {
                sb.Append( "var "+VarName+" = ");
            }
             sb.Append("[");

            for (int i = 0; i < _values.Count; i++)
            {
                if (i > 0)
                {
                     sb.Append(", ");
                    if (addCarriageReturn)
                    {
                         sb.Append("\r\n");
                    }
                }
                 sb.Append(_values[i]);
            }

             sb.Append("]");
            if (!string.IsNullOrEmpty(VarName))
            {
                 sb.Append( ";");
            }
            return sb.ToString();
        }
        public string GetJS()
        {
            return GetJS(false);
        }
        public override string ToString()
        {
            return GetJS();
        }
        public void Randomize()
        {
            _values.Sort((item1,item2) => (item1 == item2) ? 0 : (CS.General_v3.Util.Random.GetBool() ? -1 : 1));
        }
        public static JSArray GetFromList<T>(List<T> strings)
        {
            JSArray arr = new JSArray();
            for (int i = 0; i < strings.Count; i++)
            {
                arr.AddItem(strings[i]);
            }
            return arr;
        }
    }
}
