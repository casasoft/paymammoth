﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using CS.General_v3.JavaScript.Interfaces;
using System.Text.RegularExpressions;
namespace CS.General_v3.JavaScript.Data
{
    public class JSRegex : IJavaScriptSerializable
    {
        private Regex _regex;
        public JSRegex(Regex regex)
        {
            _regex = regex;
        }
        


        public string GetJS()
        {
            if (_regex != null)
            {
                string options = "";
                
                if ((_regex.Options & RegexOptions.IgnoreCase) == RegexOptions.IgnoreCase)
                {
                    options += "i";
                }
                if ((_regex.Options & RegexOptions.Multiline) == RegexOptions.Multiline)
                {
                    options += "m";
                }
                if ((_regex.Options & RegexOptions.Singleline) == RegexOptions.Singleline)
                {
                    options += "s";
                }
                if (string.IsNullOrEmpty(options))
                {
                    options = null;
                }
                string js = "new RegExp('" + _regex.ToString() + "'";
                if (!string.IsNullOrEmpty(options))
                {
                    js += ",'"+options+"'";
                }
                js += ")";
                return js;
            }
            else
            {
                return null;
            }
            
        }
        
    }
}
