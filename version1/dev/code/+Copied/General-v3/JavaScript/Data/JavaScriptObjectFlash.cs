﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.JavaScript.Data
{
    public class JavaScriptObjectFlash : JavaScriptObject
    {
        public JavaScriptObjectFlash()
        {
        }
        public Interfaces.IJavaScriptSerializable GetJsObject()
        {
            return base.GetJsObject(true);
        }
        public override Interfaces.IJavaScriptSerializable GetJsObject(bool forFlash = false)
        {
            return base.GetJsObject(true);
        }
    }
}
