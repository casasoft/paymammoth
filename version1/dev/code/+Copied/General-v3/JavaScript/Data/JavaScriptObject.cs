﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Interfaces;
using CS.General_v3.Util;

namespace CS.General_v3.JavaScript.Data
{
    public class JavaScriptObject : IJavaScriptObject
    {
        protected bool _allPropertiesStartWithLowerCase { get; set; }

        public JavaScriptObject()
        {
        }

        #region IJavaScriptObject Members

        public virtual IJavaScriptSerializable GetJsObject(bool forFlash = false)
        {
            
            return JSUtilOld.GetJSObjectFromTypeWithReflection(this, forFlash, allPropertiesStartWithLowerCase:_allPropertiesStartWithLowerCase);
        }
        public JSObject GetJsObjectAsJSObject(bool forFlash = false)
        {

            return JSUtilOld.GetJSObjectFromTypeWithReflection(this, forFlash, allPropertiesStartWithLowerCase: _allPropertiesStartWithLowerCase);
        }

        #endregion
    }
}
