﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Interfaces;

namespace CS.General_v3.JavaScript.Data
{
    public class JSObjectList : List<JSObject>
    {
        public JSArray GetJSArray()
        {
            JSArray arr = new JSArray();
            for (int i = 0; i < Count; i++)
            {
                arr.AddItem(this[i]);
            }
            return arr;
        }
    }

}
