﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Interfaces;
using System.Text.RegularExpressions;

namespace CS.General_v3.JavaScript.Data
{
    public class JSObjectFlash : JSObject
    {
        public JSObjectFlash()
        {
            this.QuotationMark = "'";
        }

        public override string GetDateForJS(DateTime dt, out bool addQuotationMarks)
        {
            //For flash, we have to write different date format
            //jsValue = dt.ToString("ddd MMM dd hh:mm:ss 
            StringBuilder sb = new StringBuilder();
            sb.Append(dt.ToString("ddd MMM dd HH:mm:ss"));
            sb.Append(" ");
            sb.Append(dt.ToString("UTCzzz").Replace(":", ""));
            sb.Append(" ");
            sb.Append(dt.ToString("yyyy"));
            
            addQuotationMarks = true;
            return sb.ToString();
        }
        public override bool AddQuotationForVariableNames
        {
            get
            {
                return true;
            }
        }
        public override void AddProperty(string id, string value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            if (value != null)
            {
                value = CS.General_v3.Util.Text.ReplaceAllHtmlTagAttributeQuotes(value, '"');
            }
            base.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        /*public override void AddProperty(string id, object value, bool addQuotationMarks, bool dontAddIfNull, bool escapeJSCharacters)
        {
           /* if (value is System.Collections.IEnumerable && !(value is string) && !(value is JSObject) && !(value is JSArray))
            {
                JSArrayFlash jsArr = new JSArrayFlash();
                jsArr.AddRange((System.Collections.IEnumerable)value);
                value = jsArr;
            }
            base.AddProperty(id, value,addQuotationMarks,dontAddIfNull,escapeJSCharacters);
        }*/
    }

}
