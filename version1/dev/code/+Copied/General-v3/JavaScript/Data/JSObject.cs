﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Interfaces;
using System.Collections;
using System.Text.RegularExpressions;

namespace CS.General_v3.JavaScript.Data
{
    public class JSObject : IJavaScriptSerializable, IJavaScriptVar
    {
        public class JSObjectPropertyParams
        {
            
            public object Value { get; set;}
            public bool? AddQuotationMarksAroundValue { get; set;}
            public bool DontAddIfNull { get; set;}
            public bool EscapeJSCharacters {get; set;}
        

        }

        public Dictionary<string, JSObjectPropertyParams> DictionaryProperties { get; private set; }
        

        public string QuotationMark { get; set; }

        /// <summary>
        /// Create a javascript object
        /// </summary>
        /// <param name="varName">The variable name of the object.  If left null, object is an object literal</param>
        public JSObject(string varName)
        {
            
            this.VarName = varName;
            init();
        }
        public JSObject()
        {
            this.VarName = null; 
            init();
            
        }

        private void init()
        {
            QuotationMark = "\""; // Do not change this cause else there is some error
            this.DictionaryProperties = new Dictionary<string, JSObjectPropertyParams>();
            AddQuotationForVariableNames = true;
        }

        public virtual bool AddQuotationForVariableNames
        {
            get;
            set;
        }

        public string VarName { get; set; }
        public virtual string GetDateForJS(TimeSpan timeSpan, out bool addQuotationMarks)
        {
            string jsValue = new JSDate(timeSpan).GetJS();
            addQuotationMarks = false;
            return jsValue;
        }
        public virtual string GetRegexForJS(Regex r, out bool addQuotationMarks)
        {
            string jsValue = new JSRegex(r).GetJS();
            addQuotationMarks = false;
            return jsValue;
        }
        public virtual string GetDateForJS(DateTime dt, out bool addQuotationMarks)
        {
            string jsValue = new JSDate(dt).GetJS();
            addQuotationMarks = false;
            return jsValue;
        }
        /// <summary>
        /// Add a property to the JS object.
        /// </summary>
        /// <param name="id">ID of the property</param>
        /// <param name="value">Value of the property</param>
        /// <param name="addQuotationMarks">Whether to add quotation marks or not around the value if it is NOT a numeric value.</param>
        private void storeProperty(string id, object value, bool? addQuotationMarksAroundValue = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            JSObjectPropertyParams param = new JSObjectPropertyParams()
            {
                AddQuotationMarksAroundValue = addQuotationMarksAroundValue,
                DontAddIfNull = dontAddIfNull,
                EscapeJSCharacters = escapeJSCharacters,
                Value = value
            };
            DictionaryProperties[id] = param;

        }
        private string getValueJS(JSObjectPropertyParams property)
        {
            bool addQuotationMarks = false;

            object value = property.Value;
            bool? addQuotationMarksAroundValue = property.AddQuotationMarksAroundValue;
            bool dontAddIfNull = property.DontAddIfNull;
            bool escapeJSCharacters = property.EscapeJSCharacters;



            //Default value for addQuotationMarks
            if (!addQuotationMarksAroundValue.HasValue)
            {
                addQuotationMarks = value is string;
            }
            else
            {
                addQuotationMarks = addQuotationMarksAroundValue.Value;
            }
            
            if (value is IJavaScriptObject)
            {
                value = ((IJavaScriptObject)value).GetJsObject(this is JSObjectFlash);
            }
            else if (value is NameValueCollection)
            {
                //This will be mapped with a JSObject;
                NameValueCollection nvc = (NameValueCollection)value;
                JSObject obj = this is JSObjectFlash ? new JSObjectFlash() : new JSObject();
                for (int i = 0; i < nvc.Keys.Count; i++)
                {
                    obj.AddProperty(nvc.Keys[i], nvc[i]);
                    //obj[nvc.Keys[i]] = nvc[i];
                }
                value = obj;
            }
            else if (value is IEnumerable && !(value is string) && !(value is JSObject) && !(value is JSArray))
            {  //If it is a list but it is not a string, JSObject or JSArray

                JSArray jsArr = this is JSObjectFlash ? new JSArrayFlash() : new JSArray();


                IEnumerable list = (IEnumerable)value;

                foreach (var item in list)
                {
                    jsArr.AddItem(item);
                }

                //jsArr.AddRange((System.Collections.IEnumerable)value);
                value = jsArr;
            }

            if (value is IJavaScriptSerializable)
            {

                addQuotationMarks = false;
                escapeJSCharacters = false;
                value = ((IJavaScriptSerializable)value).GetJS();

            }
            if (dontAddIfNull && (value == null))
            {
                return null; //omit this
            }
            string jsValue = null;
            if (value != null)
            {
                jsValue = value.ToString();
            }
            else
            {
                jsValue = "null";
                addQuotationMarks = false;
            }

            if (escapeJSCharacters)
            {

                jsValue = CS.General_v3.Util.Text.forJS(jsValue, QuotationMark);
            }

           // this._properties.Remove(id); // Just in case

            if (value is bool) jsValue = jsValue.ToLower(); // true/false not True/False
            if (value is DateTime || value is TimeSpan)
            {
                if (value is DateTime)
                {
                    jsValue = GetDateForJS((DateTime)value, out addQuotationMarks);
                }
                else
                {
                    jsValue = GetDateForJS((TimeSpan)value, out addQuotationMarks);
                }
            }
            if (value is Regex)
            {
                jsValue = GetRegexForJS((Regex)value, out addQuotationMarks);
            }
            double dbl;
            if (value is double || value is int || value is float || value is long || value is Int32 || value is Int64 || value is Int16)
            {
                //Numeric
                jsValue = value.ToString();
            }
            else if (value is JSONObject)
            {
                jsValue = ((JSONObject)value).GetJSON();
            }
            else if (addQuotationMarks)
            {
                string quotMark = (this.AddQuotationForVariableNames) ? QuotationMark : "";
                quotMark = this.QuotationMark;
                //jsValue = quotMark + jsValue.Replace(quotMark, "\\"+quotMark) + quotMark;

                if (this is JSObjectFlash)
                {
                    jsValue = CS.General_v3.Util.PageUtil.UrlEncode(jsValue);
                    //jsValue = CS.General_v3.Util.PageUtil.GetCurrentPage().Server.UrlEncode(jsValue);
                }

                jsValue = quotMark + jsValue + quotMark;
            }
            if (value == null) jsValue = "null";


            return jsValue;
            //this._properties.Add(id, jsValue);
        }
        internal void addPropertyFromObject(string id, object value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            if (value is DateTime)
                this.AddProperty(id, (DateTime)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
            else if (value is DateTime?)
                this.AddProperty(id, (DateTime?)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
            else if (value is TimeSpan)
                this.AddProperty(id, (TimeSpan)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
            else if (value is TimeSpan?)
                this.AddProperty(id, (TimeSpan?)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
            else if (value is bool)
                this.AddProperty(id, (bool)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
            else if (value is bool?)

                this.AddProperty(id, (bool?)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
            else if (value is string)
                this.AddProperty(id, (string)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
            else if (value is IJavaScriptSerializable)
                this.AddProperty(id, (IJavaScriptSerializable)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
            else if (value is IEnumerable<uint>)
                this.AddProperty(id, (IEnumerable<uint>)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
            else if (value is IEnumerable<int>)
                this.AddProperty(id, (IEnumerable<int>)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
            else if (value is IEnumerable<long>)
                this.AddProperty(id, (IEnumerable<long>)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
            else if (value is IEnumerable<string>)
                this.AddProperty(id, (IEnumerable<string>)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
            else if (value is IEnumerable<DateTime>)
                this.AddProperty(id, (IEnumerable<DateTime>)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
            else if (value is IEnumerable<IJavaScriptObject>)
                this.AddProperty(id, (IEnumerable<IJavaScriptObject>)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
            else if (value is IEnumerable<IJavaScriptSerializable>)
                this.AddProperty(id, (IEnumerable<IJavaScriptSerializable>)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
            else if (value is IEnumerable<double>)
                this.AddProperty(id, (IEnumerable<double>)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
            else if (value is IEnumerable)
                this.AddProperty(id, (IEnumerable)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
            else if (value is double)
                this.AddProperty(id, (double)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
            else if (value is double?)
                this.AddProperty(id, (double?)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
            else if (value is uint)
                this.AddProperty(id, (uint)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
            else if (value is int)
                this.AddProperty(id, (int)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
            else if (value is int?)
                this.AddProperty(id, (int?)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
            else if (value is float)
                this.AddProperty(id, (float)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
            else if (value is long)
                this.AddProperty(id, (long)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
            else if (value is Int16)
                this.AddProperty(id, (Int16)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
           
            else if (value is IJavaScriptObject)
                this.AddProperty(id, (IJavaScriptObject)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
            else if (value is NameValueCollection)
                this.AddProperty(id, (NameValueCollection)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
            else if (value is Enum)
                this.AddProperty(id, (int)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
            else if (value is Regex)
                this.AddProperty(id, (Regex)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
            else if (value == null)
                this.AddProperty(id, (string)value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);

            else 
            {
                throw new InvalidOperationException("Not supported for this datatype <" + value.GetType().ToString());
            }
            
        }
        public void AddPropertyAsObject(string id, object value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, DateTime value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, DateTime? value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, TimeSpan value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, TimeSpan? value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, bool value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, bool? value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IJavaScriptSerializable value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, double value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, double? value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, int value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, int? value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, float value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, long value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
       /* public void AddProperty(string id, Int32 value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            addProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, Int64 value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            addProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }*/
        public void AddProperty(string id, Int16 value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, Regex value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public virtual void AddProperty(string id, string value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IJavaScriptObject value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, NameValueCollection value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<uint> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<int> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<string> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<long> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<double> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<DateTime> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<IJavaScriptObject> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
       
        public void AddProperty(string id, IEnumerable<IJavaScriptSerializable> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<NameValueCollection> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<Int16> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<int?> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<double?> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, JSONObject value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            storeProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }

        public object GetPropertyValue(string id)
        {
            if (DictionaryProperties.ContainsKey(id))
            {
                return DictionaryProperties[id].Value;
            }
            else
            {
                return null;
            }
        }
        public string GetPropertyValueAsString(string id)
        {
            var val = GetPropertyValue(id);
            if (val != null)
            {
                return val.ToString();
            }
            else
            {
                return null;
            }
        }
        public bool GetPropertyValueAsBool(string id)
        {
            var val = GetPropertyValue(id);
            if (val != null)
            {
                string s = val.ToString();
                s = s.ToLower();
                return (s == "true" || s == "1");
            }
            else
            {
                return false;
            }
            
        }

       /* private string EscapeQuotationMark123123(string str)
        {
            return str.Replace(QuotationMark, "\\" + QuotationMark);
        }*/
        /// <summary>
        /// Add a property to the JS object.
        /// </summary>
        /// <param name="id">ID of the property</param>
        /// <param name="value">Value of the property</param>
        /// <param name="addQuotationMarks">Whether to add quotation marks or not around the value if it is NOT a numeric value.</param>
        /// <param name="dontAddIfNull">If the value is null, and this is true, it won't be added to the object</param>
       /* public virtual void AddProperty(string id, object value, bool addQuotationMarks, bool dontAddIfNull)
        {
            
            AddProperty(id, value, addQuotationMarks, dontAddIfNull, true);
            
        }*/
        /*public virtual void AddProperty(string id, object value)
        {
            bool addQuotationMarks = false;
            if (value is string)
                addQuotationMarks = true;
            
            AddProperty(id, value, addQuotationMarks);
        
        }*/
       /* public virtual new void Add(string title, object value)
        {
            addProperty(title, value);
        }*/
       /* public virtual void AddProperty(string id, object value, bool addQuotationMarks)
        {
            

            addProperty(id, value, addQuotationMarks, false, true);

        }*/

        /// <summary>
        /// Get JS Object in javascript format var VAR_NAME = {cikku:'test',...};
        /// </summary>
        /// <returns></returns>
        public string GetJS(bool addCarriageReturn)
        {
            StringBuilder sb = new StringBuilder();
            
            if (!String.IsNullOrEmpty(VarName))
            {
                sb.Append( "var "+VarName+" = ");
            }
            sb.Append( "{");
            int i = 0;
            foreach (KeyValuePair<string, JSObjectPropertyParams> entry in this.DictionaryProperties)
            {
                // do something with entry.Value or entry.Key
                string id = entry.Key;
                JSObjectPropertyParams property = entry.Value;
                string value = getValueJS(property);
                if (value == "0,5")
                {
                    int k = 5;
                }
                if (i > 0)
                {
                    sb.Append(", ");
                    if (addCarriageReturn)
                    {
                        sb.Append("\r\n");
                    }

                }
                ///This is what flash requires for properties
                if (this.AddQuotationForVariableNames)
                {
                    sb.Append(this.QuotationMark + id + this.QuotationMark + ":" + value);
                }
                else
                {
                    sb.Append(id + ":" + value);
                }



                i++;
            }

            sb.Append( "}");
            if (!string.IsNullOrEmpty(VarName))
            {
                sb.Append( ";");
            }
            return sb.ToString();
        }
        public string GetJS()
        {
            //Only in testing
            bool addCarriageReturn = CS.General_v3.Settings.Others.IsInLocalTesting;
            addCarriageReturn = false;
            return GetJS(addCarriageReturn);
        }
        public override string ToString()
        {
            return GetJS();
        }
        /*
        public new object this[string key]
        {
            get
            {
                return null;
                //return _properties[key];
            }
            set
            {
               // AddProperty(key, value);
            }
        }*/
    }

}
