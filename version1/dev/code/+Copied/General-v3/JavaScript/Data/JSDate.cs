﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using CS.General_v3.JavaScript.Interfaces;
namespace CS.General_v3.JavaScript.Data
{
    public class JSDate : IJavaScriptSerializable
    {
        private TimeSpan? _timeSpan;
        private DateTime _date;
        public JSDate(TimeSpan timeSpan)
        {
            _timeSpan = timeSpan;
        }
        public JSDate(DateTime date)
        {
            _date = date;
        }


        public string GetJS()
        {
            if (_timeSpan != null)
            {
                return "new Date(" + _timeSpan.Value.TotalMilliseconds + ")";
            }
            else
            {
                return "new Date(" + _date.Year + "," + (_date.Month - 1) + "," + _date.Day + "," + _date.Hour + "," + _date.Minute + "," + _date.Second + "," + _date.Millisecond + ")";
            }
            
        }
        
    }
}
