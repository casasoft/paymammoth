﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.JavaScript.jQuery.Plugins.Tooltips.QTip
{
    using System.Web.UI;
    using CS.General_v3.Controls.WebControls.Common;
    using System.Web.UI.WebControls;

    public class jQueryQTip
    {

        private Control _control;
        public Control Control
        {
            get { return _control; }
            set
            {
                if (_control != null)
                {
                    _control.PreRender -= _control_PreRender;
                }
                _control = value;
                if (_control != null)
                {
                    _control.PreRender += new EventHandler(_control_PreRender);
                }
            }
        }
         private _jQueryQTipOptions _options = new _jQueryQTipOptions();

        public _jQueryQTipOptions Options
        {
            get { return _options; }
            set { _options = value; }
        }
        public jQueryQTip()
        {

        }

        public jQueryQTip(Control control)
        {
            this.Control = control;
        }

        private string getSelectorSuffix()
        {
            if (_control is MyImage)
            {
                return "img";
            }
            else
            {
                return "";
            }
        }
        private void updateImageDefaultOptions()
        {
            if (_options.content == null)
            {
                _options.content = new _jQueryQTipOptionsContent();
                
            }
            if (string.IsNullOrEmpty(_options.content.text))
            {
                _options.content.attr = "alt";
            }
        }
        private void updateDefaultOptionsAccordingToControl()
        {
            if (_options.position == null)
            {
                _options.position = new _jQueryQTipOptionsPosition();
            }
            if (_options.position.adjust == null)
            {
                _options.position.adjust = new _jQueryQTipOptionsAdjust();
                _options.position.adjust.x = 15;
                _options.position.adjust.y = 15;
                _options.position.target = "mouse";
            }

            if (_control is MyImage || _control is Image)
            {
                updateImageDefaultOptions();
            }
        }

        private void initJS()
        {
            updateDefaultOptionsAccordingToControl();

            string js = "jQuery(function() { jQuery('#"+_control.ClientID+" "+getSelectorSuffix()+"').qtip("+Options.GetJsObject().GetJS()+"); });";
            js += "\r\n";
            CS.General_v3.Util.PageUtil.GetCurrentPage().ClientScript.RegisterStartupScript(this.GetType(), "tooltip_" + _control.ClientID, js, true);
        }

        void _control_PreRender(object sender, EventArgs e)
        {
            initJS();   
        }

        public string Title
        {
            get
            {
                if (_options.content != null)
                {
                    if (_options.content.title != null)
                    {
                        return _options.content.title.text;
                    }
                }
                return null;
            }
            set
            {
                if (_options.content == null)
                {
                    _options.content = new _jQueryQTipOptionsContent();
                }
                if (_options.content.title == null)
                {
                    _options.content.title = new _jQueryQTipOptionsTitle();
                }
                _options.content.title.text = value;
            }
        }








        }
}
