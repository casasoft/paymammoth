﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.JavaScript.jQuery.Plugins.Highlight
{
    using System.Web.UI;

    public static class Highlight
    {
        /// <summary>
        /// This makes use of the jQuery plugin found in \_common\js\jQuery\plugins\highlight\
        /// </summary>
        /// <param name="keywords"></param>
        /// <param name="cssSelector"></param>
        public static void HighlightKeywords(string keywords, string cssSelector, Page pg, bool splitWordsAsSeparate = true)
        {
            string[] words = null;
            if (splitWordsAsSeparate)
            {
                words = keywords.Split(' ');
            }
            else
            {
                words = new string[1] { keywords };
            }
            string js = "";

            for (int i = 0; i < words.Length; i++)
            {
                string word = words[i].Trim();
                js += "jQuery('" + cssSelector + "').highlight('"+word+"');\r\n";
            }
            pg.ClientScript.RegisterStartupScript(pg.GetType(), "highlight" + keywords, js, true);

        }
    }
}
