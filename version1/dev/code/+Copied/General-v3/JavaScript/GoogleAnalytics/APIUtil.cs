﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;

namespace CS.General_v3.JavaScript.GoogleAnalytics
{
    using Data;

    public static class APIUtil
    {
        public static string GetTrackEventJS_Sync(string category, string action, string label = null, string value = null, string trackerVarName = "ga")
        {
            string s = trackerVarName + "._trackEvent('" + CS.General_v3.Util.Text.forJS(category) + "', '" + CS.General_v3.Util.Text.forJS(action) + "'" +
                    ", '" + CS.General_v3.Util.Text.forJS(label) + "', '" + CS.General_v3.Util.Text.forJS(value) + "');\r\n";
            return s;
        }
        public static string GetTrackEventJS_ASync(string category, string action, string label = null, string value = null, string trackerVarName = "_gaq")
        {

            JSArray arr = new JSArray();
            arr.QuotationMark = "'";
            arr.AddItem("_trackEvent");
            arr.AddItem(category);
            arr.AddItem(action);
            if (!string.IsNullOrEmpty(label)) arr.AddItem(label);
            if (!string.IsNullOrEmpty(value)) arr.AddItem(value);

            string s = trackerVarName + ".push("+arr.GetJS()+");";
            return s;
        }
        
    }
}
