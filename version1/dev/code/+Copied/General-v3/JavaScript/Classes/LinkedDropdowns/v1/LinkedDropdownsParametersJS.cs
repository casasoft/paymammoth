﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;
using System.Web.UI;

namespace CS.General_v3.JavaScript.Classes.LinkedDropdowns.v1
{
    using Data;

    public class LinkedDropdownsParametersJS : JavaScriptObject
    {
        public List<LinkedDropdownSelectDataJS> selectElementsData;
        public LinkedDropdownItemDataJS data;

        public LinkedDropdownsParametersJS()
        {
            this.selectElementsData = new List<LinkedDropdownSelectDataJS>();
        }



        public static void InitJS(LinkedDropdownsParametersJS parameters, string key, Page pg)
        {
            string js = "new js.com.cs." + CS.General_v3.Settings.Others.JAVASCRIPT_VERSION + ".UI.LinkedDropdowns.v1.LinkedDropdowns(" + parameters.GetJsObject().GetJS() + ");\r\n";
            CS.General_v3.Util.JSUtilOld.AddJSScriptToPage(js);
            //pg.ClientScript.RegisterStartupScript(pg.GetType(), key, js, true);
        }
    }
}
