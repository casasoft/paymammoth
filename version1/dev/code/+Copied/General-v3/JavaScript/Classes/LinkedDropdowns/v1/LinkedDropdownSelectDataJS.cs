﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;

namespace CS.General_v3.JavaScript.Classes.LinkedDropdowns.v1
{
    using Data;

    public class LinkedDropdownSelectDataJS : JavaScriptObject
    {
        public string id;
        public string emptyText;
        public string initialSelectedValue;
    }
}
