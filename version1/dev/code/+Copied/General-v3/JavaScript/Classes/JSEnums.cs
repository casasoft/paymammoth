﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.JavaScript.Classes
{
    public class JSEnums
    {
        public enum POSITION
        {
            Top = 0,
            TopRight = 10,
            Right = 20,
            BottomRight  =30,
            Bottom = 40,
            BottomLeft = 50,
            Left = 60,
            TopLeft = 70,
            Center = 80
        }
    }
}
