﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;
using System.Web.UI;

namespace CS.General_v3.JavaScript.Classes.DropDownMenu.v1
{
    using Data;

    public enum DROP_DOWN_MENU_INITIAL_OPEN_POS
    {
        TopHorizAlignLeft = 0,
        TopHorizAlignCenter = 4,
        TopHorizAlignRight = 8,
        BottomHorizAlignLeft = 10,
        BottomHorizAlignCenter = 14,
        BottomHorizAlignRight = 18,
        Right = 20,
        Left = 30
    }
    public enum DROP_DOWN_MENU_CONTINUATION_OPEN_POS
    {
        Left = 0,
        Right = 10,
    }
    public enum DROP_DOWN_MENU_SHOW_HIDE_ANIMATION_TYPE
    {
        None = 10,
        OpacityFade = 20,
        Slide = 30
    }
    public class DropDownMenuParametersJS : JavaScriptObject
    {
        public string ulElementID;

        public DROP_DOWN_MENU_INITIAL_OPEN_POS initialOpenPos = DROP_DOWN_MENU_INITIAL_OPEN_POS.BottomHorizAlignCenter;
        public DROP_DOWN_MENU_CONTINUATION_OPEN_POS continuationOpenPos = DROP_DOWN_MENU_CONTINUATION_OPEN_POS.Right;
        public DROP_DOWN_MENU_SHOW_HIDE_ANIMATION_TYPE animationType = DROP_DOWN_MENU_SHOW_HIDE_ANIMATION_TYPE.OpacityFade;
        public int animationDurationMS = 200;

        public int hideDelayMS = 500;
        public bool enableCufonRefresh = false;
        public string menuButtonClassNameForCufonRefresh;

        public static void InitJS(DropDownMenuParametersJS parameters, string key, Page pg)
        {
            string js = "new js.com.cs."+CS.General_v3.Settings.Others.JAVASCRIPT_VERSION+".UI.DropDownMenu.v1.DropDownMenu("+parameters.GetJsObject().GetJS()+");\r\n";
            CS.General_v3.Util.JSUtilOld.AddJSScriptToPage(js);
            //pg.ClientScript.RegisterStartupScript(pg.GetType(), key, js, true);
        }
    }
}
