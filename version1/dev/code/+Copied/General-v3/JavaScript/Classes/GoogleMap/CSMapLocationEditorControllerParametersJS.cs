﻿namespace CS.General_v3.JavaScript.Classes.GoogleMap
{
    using CS.General_v3.JavaScript.Data;
    using CS.General_v3.Util;
    using Data;

    public class CSMapLocationEditorControllerParametersJS : JSONObject
    {
        public string txtHiddenElemOrID;

        public double? initialPositionLat;
        public double? initialPositionLng;
        public int initialZoom = 14;
        public string markerTitle;
        
    }
}
