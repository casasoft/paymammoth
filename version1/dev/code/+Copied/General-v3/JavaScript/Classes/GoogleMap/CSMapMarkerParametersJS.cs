﻿namespace CS.General_v3.JavaScript.Classes.GoogleMap
{
    using CS.General_v3.JavaScript.Data;
    using CS.General_v3.Util;
    using Data;

    public class CSMapMarkerParametersJS : JSONObject
    {
        /// <summary>
        /// Marker position. Required.
        /// </summary>
        public double? lat;
        public double? lng;
        /// <summary>
        /// Rollover text
        /// </summary>
        public string title;

        /// <summary>
        /// Mouse cursor to show on hover
        /// </summary>
        public string cursor;
        /// <summary>
        /// If true, the marker can be clicked
        /// </summary>
        public bool clickable = false;
        /// <summary>
        /// If true, the marker is visible
        /// </summary>
        public bool visible = true;
        /// <summary>
        /// If true, the marker shadow will not be displayed.
        /// </summary>
        public bool flat = false;
        /// <summary>
        /// The zIndex of this marker. If not set, markers are displayed in the order in which they were added to the map.
        /// </summary>
        public int? zIndex;
        public bool draggable = false;
    }
}
