﻿using CS.General_v3.JavaScript.Data;

namespace CS.General_v3.JavaScript.Classes.GoogleMap
{
    using System.Collections.Generic;
    using CS.General_v3.JavaScript.Data;
    using CS.General_v3.Util;
    using Data;

    public enum MAP_TYPE
    {
        Roadmap = 0,
        Satellite = 10,
        Hybrid = 20,
        Terrain = 30
    }
    public class CSMapControllerJS : JSONObject
    { 
        public object mapDivOrID;
        public double? centerLat;
        public double? centerLng;
        public int zoomLevel = 14;
        public MAP_TYPE mapType = MAP_TYPE.Roadmap;
        public bool confirmVisible = true;
        public List<CSMapMarkerParametersJS> markers = new List<CSMapMarkerParametersJS>();

        /// <summary>
        /// Fill this if you want the map to be editable
        /// </summary>
        public CSMapLocationEditorControllerParametersJS editableParams;

        public static void InitJS(CSMapControllerJS parameters)
        {
            string js = "new js.com.cs.GoogleMapsV3.v3.Classes.Controllers.CSMap.CSMapController("+parameters.GetJSON()+");\r\n";
            Util.JSUtilOld.AddJSScriptToPage(js);
        }
    }
}
