﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;

namespace CS.General_v3.JavaScript.Classes.SliderNavigation.v2
{
    using Data;

    public class SliderNavigationJSDetails : JavaScriptObject
    {
        public int slideDurationMS = 500;
        public string cssClassIconExpand = "navigation-expand";
        public string cssClassIconCollapse = "navigation-collapse";
        public string cssClassLiSectionExpanded = "navigation-section-expanded";
        public string cssClassLiSectionCollapsed = "navigation-section-collapsed";
        public string cssClassLinkExpanded = "navigation-link-expanded";
        public string cssClassLinkCollapsed = "navigation-link-collapsed";
        public bool clickOnNonLeafNodesOpenSection = false;
        public string initialSelectedClass = "selected";
        public bool autoOpenSelectedHierarchy = true;
        public bool doNotAllowMultipleSiblingsOpen = true;

        public string rootULElementOrID;

    }
}
