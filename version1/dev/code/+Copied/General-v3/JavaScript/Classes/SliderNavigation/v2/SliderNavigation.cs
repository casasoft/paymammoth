﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.JavaScript.Classes.SliderNavigation.v2
{
    public class SliderNavigation
    {
        public string RootULClientID { get; set; }
        public SliderNavigationJSDetails Data { get; private set; }

        public SliderNavigation(string RootULClientID, SliderNavigationJSDetails Data, bool autoOutputJS = true)
        {
            this.RootULClientID = RootULClientID;
            this.Data = Data;
            if (autoOutputJS)
            {
                OutputJSToPage();
            }
        }

        public void OutputJSToPage()
        {
            if (string.IsNullOrEmpty(RootULClientID))
            {
                throw new Exception("Please specify root UL client ID");
            }
            string js = "new js.com.cs."+CS.General_v3.Settings.Others.JAVASCRIPT_VERSION+".UI.NavigationMenu.SliderNavigation.v2.SliderNavigation(jQuery('#"+RootULClientID+"'), "+Data.GetJsObject().GetJS()+");";
            CS.General_v3.Util.JSUtilOld.AddJSScriptToPage(js, true, key: this.RootULClientID + "jsSliderNavMenu");
            //js = CS.General_v3.Util.JSUtil.MakeJavaScript(js, true);

            //CS.General_v3.Util.PageUtil.GetCurrentPage().ClientScript.RegisterStartupScript(this.GetType(), this.RootULClientID + "jsSliderNavMenu", js, false);

        }
    }
}
