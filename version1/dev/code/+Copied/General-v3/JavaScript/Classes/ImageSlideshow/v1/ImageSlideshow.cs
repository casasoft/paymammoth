﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common;
namespace CS.General_v3.JavaScript.Classes.ImageSlideshow.v1
{
    /// <summary>
    /// Create a slideshow of images.  You must first specify the base image and then they will
    /// create more images in the parent of the base image.
    /// 
    /// The parent will be make position:relative and all images are position:absolute.
    /// </summary>
    public class ImageSlideshow
    {
        
        public ImageSlideshow(List<ImageSlideshowData> Images, MyImage img, int fadeDurationMS, bool randomizeImages)
        {
            if (Images.Count > 1)
            {
                CS.General_v3.Util.Random.RandomizeArray(Images);
                img.ImageUrl = Images[0].Image;
                img.AlternateText = Images[0].Title;

                string js = "new js.com.cs."+CS.General_v3.Settings.Others.JAVASCRIPT_VERSION+".UI.Images.v1.ImageSlideshow(" + ImageSlideshowData.GetArray(Images).GetJS() + ",'" + img.ClientID + "',"+fadeDurationMS+", false);";
                CS.General_v3.Util.JSUtilOld.AddJSScriptToPage(js);
                //js = CS.General_v3.Util.JSUtil.MakeJavaScript(js, true);
                //CS.General_v3.Util.PageUtil.GetCurrentPage().ClientScript.RegisterStartupScript(this.GetType(), img.ClientID + "-imageSlideshow", js);
            }

        }
    }
}
