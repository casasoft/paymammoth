﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using CS.General_v3.JavaScript.Data;
namespace CS.General_v3.JavaScript.Classes.ImageSlideshow.v1
{
    using Data;

    public class ImageSlideshowData
    {
        public string Title { get; set; }
        public int DurationMS { get; set; }
        public string Image { get; set; }

        public JSObject GetJsObject()
        {
            JSObject obj = new JSObject();
            obj.AddProperty("title",Title);
            obj.AddProperty("durationMS", DurationMS);
            obj.AddProperty("image", Image);
            return obj;
        }

        public static JSArray GetArray(List<ImageSlideshowData> images)
        {
            JSArray arr = new JSArray();
            for (int i = 0; i < images.Count; i++)
            {
                arr.AddItem(images[i].GetJsObject());
            }
            return arr;
        }
    }
}
