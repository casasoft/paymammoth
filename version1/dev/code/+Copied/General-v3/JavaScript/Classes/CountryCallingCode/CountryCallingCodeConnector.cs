﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;

namespace CS.General_v3.JavaScript.Classes.CountryCallingCode
{
    using Data;

    /// <summary>
    /// Connects two combo boxes, one of country and one of calling code together
    /// so that when one changes, the other changes as well
    /// </summary>
    public class CountryCallingCodeConnector : JavaScriptObject
    {

        public CountryCallingCodeConnector(CountryCallingCodeConnectorParams parameters, string key)
        {
            string js = "new js.com.cs."+CS.General_v3.Settings.Others.JAVASCRIPT_VERSION+".UI.CountryCallingCode.CountryCallingCodeConnector("+parameters.GetJsObject().GetJS()+");\r\n";
            CS.General_v3.Util.JSUtilOld.AddJSScriptToPage(js);
            //CS.General_v3.Util.PageUtil.GetCurrentPage().ClientScript.RegisterStartupScript(this.GetType(), key, js, true);
        }
    }
}
