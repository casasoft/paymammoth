﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;

namespace CS.General_v3.JavaScript.Classes.CountryCallingCode
{
    using Data;

    public class CountryCallingCodeConnectorOptionParams : JavaScriptObject
    {
        public string country;
        public string callingCode;
    }
}
