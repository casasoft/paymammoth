﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;
using CS.General_v3.Util;

namespace CS.General_v3.JavaScript.Classes.CountryCallingCode
{
    using Data;

    public class CountryCallingCodeConnectorParams : JavaScriptObject
    {
        public enum COUNTRY_CODE_DEFAULT {
            None,
            CountryCode2Letters,
            CountryCode3Letters,
            CountryFullName,

        }
        private static List<CountryCallingCodeConnectorOptionParams> _VALUES_2LETTERS;
        private static List<CountryCallingCodeConnectorOptionParams> _VALUES_3LETTERS;
        private static List<CountryCallingCodeConnectorOptionParams> _VALUES_FULL_NAME;

        public List<CountryCallingCodeConnectorOptionParams> options;
        public string cmbCountryID;
        public string cmbCallingCodeID;

        public CountryCallingCodeConnectorParams(COUNTRY_CODE_DEFAULT defaultCodes)
        {
            initDefaultCountryCodes(defaultCodes);    
        }

        private void initDefaultCountryCodes(COUNTRY_CODE_DEFAULT defaultCodes)
        {
            if (defaultCodes != COUNTRY_CODE_DEFAULT.None)
            {

                if (defaultCodes == COUNTRY_CODE_DEFAULT.CountryCode2Letters && _VALUES_2LETTERS != null)
                {
                    this.options = _VALUES_2LETTERS;
                }
                else if (defaultCodes == COUNTRY_CODE_DEFAULT.CountryCode3Letters && _VALUES_3LETTERS != null)
                {
                    this.options = _VALUES_3LETTERS;
                }
                else if (defaultCodes == COUNTRY_CODE_DEFAULT.CountryFullName && _VALUES_FULL_NAME != null)
                {
                    this.options = _VALUES_FULL_NAME;
                }
                else
                {
                    this.options = new List<CountryCallingCodeConnectorOptionParams>();
                    IList<Enums.ISO_ENUMS.Country_ISO3166> values = CS.General_v3.Util.EnumUtils.GetListOfEnumValues<CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166>();
                    foreach (Enums.ISO_ENUMS.Country_ISO3166 country in values)
                    {
                        string countryValue = null;
                        switch (defaultCodes)
                        {
                            case COUNTRY_CODE_DEFAULT.CountryCode2Letters: countryValue = country.GetCountry2LetterCode(); break;
                            case COUNTRY_CODE_DEFAULT.CountryCode3Letters: countryValue = country.GetCountry3LetterCode(); break;
                            case COUNTRY_CODE_DEFAULT.CountryFullName: countryValue = country.GetCountryFullName(); break;
                        }
                        int callingCode = country.GetCountryCallingCode();
                        if (callingCode != -1)
                        {
                            //If -1 means that they do not have calling code
                            options.Add(new CountryCallingCodeConnectorOptionParams()
                            {
                                callingCode = callingCode.ToString(),
                                country = countryValue
                            });
                        }
                    }

                    if (defaultCodes == COUNTRY_CODE_DEFAULT.CountryCode2Letters && _VALUES_2LETTERS != null)
                    {
                        _VALUES_2LETTERS = this.options;
                    }
                    else if (defaultCodes == COUNTRY_CODE_DEFAULT.CountryCode3Letters && _VALUES_3LETTERS != null)
                    {
                        _VALUES_3LETTERS = this.options;
                    }
                    else if (defaultCodes == COUNTRY_CODE_DEFAULT.CountryFullName && _VALUES_FULL_NAME != null)
                    {
                        _VALUES_FULL_NAME = this.options;
                    }

                }
            }
        }
    }
}
