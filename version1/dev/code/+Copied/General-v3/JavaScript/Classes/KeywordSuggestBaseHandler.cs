﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace CS.General_v3.JavaScript.Classes
{
    public abstract class KeywordSuggestBaseHandler : CS.General_v3.HTTPHandlers.BaseAjaxHandler
    {
        //public AjaxMediaGalleryItemData UploadedItemData { get; set; }
        protected string _paramResponse = "data";
        protected string _paramKeywords = "k";
        
        protected override void processRequest(System.Web.HttpContext context)
        {




            string userKeywords = context.Request[_paramKeywords];
            List<string> keywordSuggestions = GetKeywordSuggest(context, userKeywords);
            this.AddProperty(_paramResponse, keywordSuggestions);

            

            base.processRequest(context);
            
        }

        /// <summary>
        /// The request to handle the upload of the item.
        /// 
        /// You must specify other parameters with the 'scriptData' parameter of the AjaxMediaGalleryItemSectionData so that
        /// they are appended to the form variables.
        /// </summary>
        /// <param name="fileContent"></param>
        /// <returns></returns>
        protected abstract List<string> GetKeywordSuggest(HttpContext context, string userKeywords);
    }
}
