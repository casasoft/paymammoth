﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;
using CS.General_v3.Util;

namespace CS.General_v3.JavaScript.Classes.TimeRanges
{
    using Data;

    public class TimeRangesUIJS : JSONObject
    {
        public string divContainerIDOrElem;
        public string initialValue;
        public string txtHiddenIDOrElem;
        public int animationMS = 500;

        public List<string> dayNamesTexts = new List<string>() { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
        public string addHoursText = "Add Hours";
        public string applyToAllDaysText = "Apply to all days";
        public string addText = "Add";
        public string cancelText = "Cancel";
        public string noItemsText = "No opening hours specified.";

        public static void InitJS(TimeRangesUIJS p)
        {
            string js = "new js.com.cs.v4.UI.TimeRanges.TimeRangesUI("+p.GetJSON()+");\r\n";
            JSUtilOld.AddJSScriptToPage(js);
        }
    }
}
