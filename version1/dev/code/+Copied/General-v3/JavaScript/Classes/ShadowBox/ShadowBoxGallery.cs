﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using CS.General_v3.Controls.WebControls.Common;
using System.Web.UI.HtmlControls;

namespace CS.General_v3.JavaScript.Classes.ShadowBox
{
    public class ShadowBoxGallery
    {
        private string _galleryID;

        
        public ShadowBoxGallery(string galleryID) {
            _galleryID = galleryID;
            ShadowBoxUtil.InitShadowBox();
        }

        public static string GetRelValue(string title = null, string gallery = null, int? width = null, int? height = null)
        {
            string relValue = "shadowbox";
            if (!string.IsNullOrEmpty(gallery)) {
                relValue += "[" + gallery + "]";
            }
            if (!string.IsNullOrEmpty(title))
            {
                relValue += ";title=" + title;
            }
            if (width.HasValue)
            {
                relValue += ";width=" + width.Value;
            }
            if (height.HasValue)
            {
                relValue += ";height=" + height.Value;
            }
            return relValue;
        }
        public void AddItem(MyImage control, string title = null, int? width = null, int? height = null)
        {
            control.Rel = GetRelValue(title, _galleryID, width, height);

        }
        public void AddItem(HtmlAnchor control, string title = null, int? width = null, int? height = null)
        {
             control.Attributes["rel"] = GetRelValue(title, _galleryID, width, height);
           
        }


    }
}
