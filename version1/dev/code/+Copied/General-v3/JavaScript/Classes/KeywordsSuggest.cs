﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
namespace CS.General_v3.JavaScript.Classes
{
    public class KeywordsSuggest
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="txt"></param>
        /// <param name="keywordSuggestURL">This url must return a javascript object with name 'data' which will contain a string of keywords returned.
        /// e.g. {data:['keyword1','keyword2'...]}
        /// 
        /// Preferably you can extend a handler from KeywordSuggestBaseHandler to make life easier
        /// </param>
        /// <param name="delayMS"></param>
        /// <param name="keywordsParam"></param>
        public KeywordsSuggest(TextBox txt, string keywordSuggestURL, int delayMS, string keywordsParam)
        {
            string js = "new js.com.cs."+CS.General_v3.Settings.Others.JAVASCRIPT_VERSION+".UI.KeywordSuggest.KeywordSuggest('" + CS.General_v3.Util.Text.forJS(txt.ClientID) + "','" + CS.General_v3.Util.Text.forJS(keywordSuggestURL) + "', "+delayMS+", '"+keywordsParam+"');";
            CS.General_v3.Util.JSUtilOld.AddJSScriptToPage(js);
            //js = CS.General_v3.Util.JSUtil.MakeJavaScript(js, true);
            //CS.General_v3.Util.PageUtil.GetCurrentPage().ClientScript.RegisterStartupScript(this.GetType(), txt.ClientID + "-suggest", js);

        }
    }
}
