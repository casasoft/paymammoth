﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CS.General_v3.JavaScript.Interfaces
{
    public interface IJavaScriptVar
    {
        string VarName { get; set; }
    }
}
