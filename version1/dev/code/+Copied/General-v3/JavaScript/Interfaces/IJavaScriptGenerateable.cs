﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.JavaScript.Interfaces
{
    public interface IJavaScriptGenerateable
    {
        void Generate(System.Web.UI.Page page);
    }
}
