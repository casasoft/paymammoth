﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;

namespace CS.General_v3.JavaScript.Interfaces
{
    public interface IJavaScriptObject
    {
        IJavaScriptSerializable GetJsObject(bool forFlash = false);

    }
}
