﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

namespace CS.General_20090518
{
    public class DBSingletons
    {
        public const int EXPIRY_INTERVAL = 90; //in minutes
        private Hashtable _HashTable = null;
        private DateTime _LastChecked = DateTime.MinValue;
        private Hashtable hashTable
        {
            get 
            {
                if (_HashTable == null)
                    _HashTable = new Hashtable();
                return _HashTable; 
            }
            
        }
        private void checkExpired()
        {
            if (_LastChecked.AddMinutes(EXPIRY_INTERVAL) <= CS.General_20090518.Date.Now())
            {
                _LastChecked = CS.General_20090518.Date.Now();
                object[] keys = new object[_HashTable.Count];
                _HashTable.Keys.CopyTo(keys,0);
                for (int i = 0; i < keys.Length; i++)
                {

                    object currKey = keys[i];
                    SingletonInfo info = (SingletonInfo) _HashTable[currKey];
                    if (info.IsExpired)
                        _HashTable.Remove(currKey);
                }
            }
        }
        public DateTime GetLastUpdated(int ID)
        {
            if (hashTable[ID] == null)
            {
                return CS.General_20090518.Date.Now();
            }
            else
            {
                SingletonInfo info = (SingletonInfo)hashTable[ID];
                return info.LastUpdated;
            }
        }
        public void Clear()
        {
            if (hashTable != null)
                hashTable.Clear();
        }
        public void UpdateLastUpdated(int ID)
        {
            if (hashTable[ID] != null)
            {
                SingletonInfo info = (SingletonInfo)hashTable[ID];
                info.LastUpdated = CS.General_20090518.Date.Now(); ;
            }
        }
        public T GetInstance<T>(int ID, T nullValue)
        {
            T value;
            if (hashTable[ID] == null)
                value =nullValue;
            else
            {
                SingletonInfo info = (SingletonInfo)hashTable[ID];
                value = (T)info.Object;
            }
            checkExpired();
            return value;
            
        }
        public void SetInstance(int ID, object o)
        {
            SingletonInfo info = new SingletonInfo(o);
            if (o != null)
                hashTable[ID] = info;
            else
                hashTable.Remove(ID);
            checkExpired();
        }

        public class SingletonInfo
        {
            private DateTime _LastUpdated = DateTime.MinValue;

            public DateTime LastUpdated
            {
                get { return _LastUpdated; }
                set { _LastUpdated = value; }
            }

            private DateTime _LastAccessed = DateTime.MinValue;

            public DateTime LastAccessed
            {
                get { return _LastAccessed; }
                set { _LastAccessed = value; }
            }
            private object _Object = null;

            public object Object
            {
                get { return _Object; LastAccessed = CS.General_20090518.Date.Now(); }
                set { _Object = value; LastAccessed = CS.General_20090518.Date.Now(); }
            }
            public SingletonInfo(Object o)
            {
                this.Object = o;
                this.LastUpdated = this.LastAccessed = CS.General_20090518.Date.Now();
                
            }
            public bool IsExpired
            {
                get
                {
                    return CS.General_20090518.Date.Now().CompareTo(LastAccessed.AddMinutes(EXPIRY_INTERVAL)) > 0;
                }
            }


        }
    }
}
