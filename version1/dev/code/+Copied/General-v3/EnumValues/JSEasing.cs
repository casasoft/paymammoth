﻿using System;
using System.Collections.Generic;

using System.Text;

namespace CS.General_v3.EnumValues
{
    public class StringValues {
        public static string EasingToString(Easing easing)
        {
            switch (easing)
            {
                /*
                 * case Easing.BackEaseIn: return "Tween.backEaseIn";
                case Easing.BackEaseOut: return "Tween.backEaseOut";
                case Easing.BackEaseInOut: return "Tween.backEaseInOut";
                case Easing.ElasticEaseIn: return "Tween.elasticEaseIn";
                case Easing.ElasticEaseOut: return "Tween.elasticEaseOut";
                case Easing.ElasticEaseInOut: return "Tween.elasticEaseInOut";
                case Easing.BounceEaseOut: return "Tween.bounceEaseOut";
                case Easing.BounceEaseIn: return "Tween.bounceEaseIn";
                case Easing.BounceEaseInOut: return "Tween.bounceEaseInOut";
                case Easing.RegularEaseIn: return "Tween.regularEaseIn";
                case Easing.RegularEaseOut: return "Tween.regularEaseOut";
                case Easing.RegularEaseInOut: return "Tween.regularEaseInOut";
                case Easing.StrongEaseIn: return "Tween.strongEaseIn";
                case Easing.StrongEaseOut: return "Tween.strongEaseOut";
                case Easing.StrongEaseInOut: return "Tween.strongEaseInOut";
                case Easing.Linear: return "null";
                 */
                case Easing.BackEaseIn: return "dojox.fx.easing.backIn";
                case Easing.BackEaseOut: return "dojox.fx.easing.backOut";
                case Easing.BackEaseInOut: return "dojox.fx.easing.backInOut";
                case Easing.BounceEaseIn: return "dojox.fx.easing.bounceIn";
                case Easing.BounceEaseOut: return "dojox.fx.easing.bounceOut";
                case Easing.BounceEaseInOut: return "dojox.fx.easing.bounceInOut";
                case Easing.CircEaseIn: return "dojox.fx.easing.circIn";
                case Easing.CircEaseOut: return "dojox.fx.easing.cirnOut";
                case Easing.CircEaseInOut: return "dojox.fx.easing.circInOut";
                case Easing.CubicEaseIn: return "dojox.fx.easing.cubicIn";
                case Easing.CubicEaseOut: return "dojox.fx.easing.cubicOut";
                case Easing.CubicEaseInOut: return "dojox.fx.easing.cubicInOut";
                case Easing.ElasticEaseIn: return "dojox.fx.easing.elasticIn";
                case Easing.ElasticEaseOut: return "dojox.fx.easing.elasticOut";
                case Easing.ElasticEaseInOut: return "dojox.fx.easing.elasticInOut";
                
                case Easing.ExpoEaseIn: return "dojox.fx.easing.expoIn";
                case Easing.ExpoEaseOut: return "dojox.fx.easing.expoOut";
                case Easing.ExpoEaseInOut: return "dojox.fx.easing.expoInOut";
                
                case Easing.QuadEaseIn: return "dojox.fx.easing.quadIn";
                case Easing.QuadEaseOut: return "dojox.fx.easing.quadOut";
                case Easing.QuadEaseInOut: return "dojox.fx.easing.quadInOut";
                
                case Easing.QuartEaseIn: return "dojox.fx.easing.quartIn";
                case Easing.QuartEaseOut: return "dojox.fx.easing.quartOut";
                case Easing.QuartEaseInOut: return "dojox.fx.easing.quartInOut";
                
                
                case Easing.QuintEaseIn: return "dojox.fx.easing.quintIn";
                case Easing.QuintEaseOut: return "dojox.fx.easing.quintOut";
                case Easing.QuintEaseInOut: return "dojox.fx.easing.quintInOut";

                case Easing.SineEaseIn: return "dojox.fx.easing.sineIn";
                case Easing.SineEaseOut: return "dojox.fx.easing.sineOut";
                case Easing.SineEaseInOut: return "dojox.fx.easing.sineInOut";
                
                case Easing.Linear: return "dojox.fx.easing.linear";





            }
            return null;
        }
    }
    /// <summary>
    /// Javascript Easing functions
    /// </summary>
    public enum Easing
    {
        BackEaseIn,
        BackEaseOut,
        BackEaseInOut,
        BounceEaseIn,
        BounceEaseOut,
        BounceEaseInOut,
        CircEaseIn,
        CircEaseOut,
        CircEaseInOut,
        CubicEaseIn,
        CubicEaseOut,
        CubicEaseInOut,
        ElasticEaseIn,
        ElasticEaseOut,
        ElasticEaseInOut,
        ExpoEaseIn,
        ExpoEaseOut,
        ExpoEaseInOut,
        QuadEaseIn,
        QuadEaseOut,
        QuadEaseInOut,
        QuartEaseIn,
        QuartEaseOut,
        QuartEaseInOut,
        QuintEaseIn,
        QuintEaseOut,
        QuintEaseInOut,
        SineEaseIn,
        SineEaseOut,
        SineEaseInOut,
        Linear
    }

    
}
