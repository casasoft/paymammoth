using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using System.Reflection;
using System.ComponentModel;
using CS.General_v3.Util;
using CS.General_v3.Classes.Attributes;
namespace CS.General_v3
{
    public static partial class Enums
    {
        public enum ELEMENT_TEXT_DIRECTION
        {
            [Description("ltr")]
            LeftToRight,
            [Description("rtl")]
            RightToLeft,
            [Description("auto")]
            Auto
        }
        public enum SOCIAL_NETWORK_TYPES
        {
            [DescriptionAttribute("Facebook")]
            Facebook,
            [DescriptionAttribute("Twitter")]
            Twitter,
            [DescriptionAttribute("LinkedIn")]
            LinkedIn,
            [DescriptionAttribute("Google Plus")]
            GooglePlus,
            [DescriptionAttribute("Orkut")]
            Orkut,
            [DescriptionAttribute("Hi5")]
            Hi5,
            [DescriptionAttribute("MySpace")]
            MySpace,
            [DescriptionAttribute("Other / Direct Link / Email")]
            Other
        }


        public enum IMAGE_FORMAT
        {
            [Description("jpg")]
            JPEG,
            PNG,
            GIF,
            [Description("tif")]
            TIFF,
            BMP,
            None
        }
        public static string ImageFormatToExtension(IMAGE_FORMAT format)
        {
            return CS.General_v3.Util.EnumUtils.StringValueOf(format);
        }
        public static IMAGE_FORMAT? FilenameToImageFormat(string path)
        {
            IMAGE_FORMAT? format = null;
            if (!string.IsNullOrWhiteSpace(path))
            {
                string ext = CS.General_v3.Util.IO.GetExtension(path).ToLower();
                //if (ext == "jpeg") ext = "jpg";
                format = CS.General_v3.Util.EnumUtils.EnumValueNullableOf<IMAGE_FORMAT>(ext);

            }

            return format;
        }

        public enum METATAG_ROBOTS_REL_VALUES
        {
            NONE,
            NOFOLLOW,
            NOINDEX,
            INDEX,
            FOLLOW
        }
        public enum ANCHOR_REL_VALUE
        {
            alternate,
            stylesheet,
            start,
            next,
            prev,
            contents,
            index,
            glossary,
            copyright,
            chapter,
            section,
            subsection,
            appendix,
            help,
            bookmark,
            nofollow,
            license,
            tag,
            friend
        }
        public enum MEMBER_BLOCKED_REASON
        {
            None,
            TooManyInvalidLoginAttempts,
            BlockedByAdministration,
            FraudulentUser,
            NeedsVerification,
            
        }
        public enum PASSWORD_ENCRYPTION_TYPE
        {
            PlainText =0,
            PBKDF2 = 100

        }
        public enum LOG4NET_MSG_TYPE
        {
            Info,
            Warn,
            Debug,
            Fatal,
            Error
        }

        public enum ENUM_SORT_BY
        {
            PriorityAttributeValue = 0,
            NameAscending = 100,
            NameDescending = 200,
            Value = 300
        }

        public enum XMLFILE_SETTINGS_ENUM
        {
            [SettingsInfo]
            Database_Host,
            [SettingsInfo]
            Database_HostLocalhost,
            [SettingsInfo(DefaultValue = 3306)]
            Database_Port,
            [SettingsInfo]
            Database_PortLocalhost,
            [SettingsInfo]
            Database_Name,
            [SettingsInfo]
            Database_NameLocalhost,
            [SettingsInfo]
            Database_User,
            [SettingsInfo]
            Database_UserLocalhost,
            [SettingsInfo]
            Database_Pass,
            [SettingsInfo]
            Database_PassLocalhost,
            //[SettingsInfo]
            //Database_EncryptedPass,
            [SettingsInfo]
            Database_CharacterSet,
            [SettingsInfo(DefaultValue = CS.General_v3.Settings.Database.DATABASE_TYPE.MySQL)]
            Database_DatabaseType,
            [SettingsInfo()]
            UseNHibernateProfiler,
            [SettingsInfo(DefaultValue = "sqllite.testdb")]
            Database_Path,
            [SettingsInfo(DefaultValue = true)]
            Database_CheckMappingsAndOthers,
            [SettingsInfo(DefaultValue = false)]
            Database_CheckSchema,
            [SettingsInfo(DefaultValue = true)]
            Database_CheckSchemaBasedOnFileExists,
            [SettingsInfo(DefaultValue = true)]
            Database_GenerateNHibernateXmlMappingFiles,
            [SettingsInfo(DefaultValue = false)]
            Database_CheckDefaultsInRealDatabase,
            [SettingsInfo(DefaultValue = true)]
            Database_UseRealDatabase,
            [SettingsInfo(DefaultValue = true)]
            EnableDbTempItemChecker,
            [SettingsInfo(DefaultValue = 0)]
            ServerTimeGmt,

            [SettingsInfo(DefaultValue = false)]
            Other_DontThrowThreadStateExceptionsInLocalhost,


        }

        public enum COMMENT_POST_RESULT
        {
            SubmittedAndApproved,
            SubmittedAwaitingModeration,
            Failure
        }

        //The alternate value is only used for 'old' settings, when the proper identifier does not exist
        public enum SETTINGS_ENUM
        {
            #region Company Information
            #region Social Networking

            //Company Information

            //Company Information 

            [SettingsInfo(Priority = 1000)]
            CompanyInfo_SocialNetworking_Facebook,
            [SettingsInfo(Priority = 004000, CmsAccessRequired = CMS_ACCESS_TYPE.Owner, DefaultValue = true)]
            CompanyInfo_SocialNetworking_MySpace,
            [SettingsInfo(Priority = 2000)]
            CompanyInfo_SocialNetworking_FacebookGroup,
            [SettingsInfo(Priority = 004000, CmsAccessRequired = CMS_ACCESS_TYPE.Owner, DefaultValue = true)]
            CompanyInfo_SocialNetworking_YouTube,
            [SettingsInfo(Priority = 3000)]
            CompanyInfo_SocialNetworking_Twitter,
            [SettingsInfo(Priority = 3000)]
            CompanyInfo_SocialNetworking_LinkedIn,
            [SettingsInfo(Priority = 3000)]
            CompanyInfo_SocialNetworking_GooglePlus,
            [SettingsInfo(Priority = 4000)]
            CompanyInfo_SocialNetworking_Facebook_Admins,
            [SettingsInfo(Priority = 4000)]
            CompanyInfo_SocialNetworking_Facebook_AppID,
            [SettingsInfo(Priority = 4000)]
            CompanyInfo_SocialNetworking_Facebook_PageID,
            #endregion
            #region Contact Details
            [SettingsInfo(Priority = 4000)]
            CompanyInfo_ContactDetails_CompanyName,

            [SettingsInfo(Priority = 5000)]
            CompanyInfo_ContactDetails_Website,

            [SettingsInfo(Priority = 6000)]
            CompanyInfo_ContactDetails_Telephone,

            [SettingsInfo(Priority = 6000)]
            CompanyInfo_ContactDetails_TelCountryCode,

            [SettingsInfo(Priority = 7000)]
            CompanyInfo_ContactDetails_Fax,

            [SettingsInfo(Priority = 8000)]
            CompanyInfo_ContactDetails_Skype,

            [SettingsInfo(Priority = 8500)]
            CompanyInfo_ContactDetails_SkypeName,

            [SettingsInfo(Priority = 9000)]
            CompanyInfo_ContactDetails_DirectorName,

            [SettingsInfo(Priority = 10000)]
            CompanyInfo_ContactDetails_Mobile,

            [SettingsInfo(Priority = 11000)]
            CompanyInfo_ContactDetails_Address1,

            [SettingsInfo(Priority = 12000)]
            CompanyInfo_ContactDetails_Address2,

            [SettingsInfo(Priority = 13000)]
            CompanyInfo_ContactDetails_Address3,

            [SettingsInfo(Priority = 14000)]
            CompanyInfo_ContactDetails_Locality,

            [SettingsInfo(Priority = 15000)]
            CompanyInfo_ContactDetails_PostCode,

            [SettingsInfo(Priority = 16000)]
            CompanyInfo_ContactDetails_Country,
            [SettingsInfo(Priority = 16000)]
            CompanyInfo_ContactDetails_Country3LetterCode,

            [SettingsInfo(Priority = 17000)]
            CompanyInfo_ContactDetails_Email,


            [SettingsInfo(Priority = 4000, DefaultValue = "09:00 - 12:00 & 16:00 - 19:00")]
            CompanyInfo_ContactDetails_OpeningHoursMonday,
            [SettingsInfo(Priority = 4100)]
            CompanyInfo_ContactDetails_OpeningHoursTuesday,
            [SettingsInfo(Priority = 4200)]
            CompanyInfo_ContactDetails_OpeningHoursWednesday,
            [SettingsInfo(Priority = 4300)]
            CompanyInfo_ContactDetails_OpeningHoursThursday,
            [SettingsInfo(Priority = 4400)]
            CompanyInfo_ContactDetails_OpeningHoursFriday,
            [SettingsInfo(Priority = 4500)]
            CompanyInfo_ContactDetails_OpeningHoursSaturday,
            [SettingsInfo(Priority = 4600)]
            CompanyInfo_ContactDetails_OpeningHoursSunday,





            #endregion
            #region Other

            [SettingsInfo(Priority = 18000)]
            CompanyInfo_Other_VATNumber,

            [SettingsInfo(Priority = 19000)]
            CompanyInfo_Other_PaymentGatewayTransactionID,
            #endregion
            #region Google Maps

            [SettingsInfo(Priority = 20000)]
            CompanyInfo_GoogleMaps_Latitude,

            [SettingsInfo(Priority = 21000)]
            CompanyInfo_GoogleMaps_Longtitude,

            [SettingsInfo(Priority = 22000)]
            CompanyInfo_GoogleMaps_ZoomLevel,

            [SettingsInfo(Priority = 23000)]
            CompanyInfo_GoogleMaps_EmbedURL,

            #endregion
            #endregion
            #region Culture Information
            [SettingsInfo(Priority = 1000, DefaultValue = "en", CmsAccessRequired = CMS_ACCESS_TYPE.CasaSoft)]
            Culture_DefaultCultureCode,
            #endregion
            #region Email info
            #region SMTP
            [SettingsInfo(CmsAccessRequired = CMS_ACCESS_TYPE.CasaSoft, DefaultValue = "localhost", DefaultLocalhostValue = "server")]
            Email_SMTP_Host,


            [SettingsInfo(CmsAccessRequired = CMS_ACCESS_TYPE.CasaSoft, DefaultValue = "automailer@casasoft-automailer", DefaultLocalhostValue = "automailer@office.casasoft")]
            Email_SMTP_User,



            [SettingsInfo(CmsAccessRequired = CMS_ACCESS_TYPE.CasaSoft, DefaultValue = "XkZlmQCMGNUwt5Cbum4u", DefaultLocalhostValue = "wuI47dblnSp71ogtZvpK")]
            Email_SMTP_Pass,



            [SettingsInfo(Priority = 009000, DefaultValue = false)]
            Email_SMTP_UseSecureAuthentication,


            [SettingsInfo(Priority = 009000, DefaultValue = 25, DefaultLocalhostValue = 2525)]
            Email_SMTP_Port,


            [SettingsInfo(Priority = 11000, DefaultValue = "dd/MM/yyyy")]
            Website_ShortDateFormat,
            [SettingsInfo(Priority = 11000, DefaultValue = "dddd, MMMM dd yyyy HH:mm")]
            Website_LongDateTimeFormat,
            [SettingsInfo(Priority = 11000, DefaultValue = "dd/MM/yyyy HH:mm")]
            Website_ShortDateTimeFormat,
            #endregion
            #region Other

            [SettingsInfo(CmsAccessRequired = CMS_ACCESS_TYPE.CasaSoft)]
            Email_SignatureTag,

            #endregion
            #region Email Addresses

            [SettingsInfo(Priority = 001000, DefaultValue = "email.test@casasoft.com.mt")]
            Email_EmailAddresses_ContactForm_FromEmail,


            [SettingsInfo(Priority = 003000, DefaultValue = "{From Name}")]
            Email_EmailAddresses_ContactForm_FromName,


            [SettingsInfo(Priority = 005000, DefaultValue = "{From Name}")]
            Email_EmailAddresses_General_FromName,

            [SettingsInfo(Priority = 006000, DefaultValue = "email.test@casasoft.com.mt")]
            Email_EmailAddresses_General_FromEmail,







            [SettingsInfo(CmsAccessRequired = CMS_ACCESS_TYPE.CasaSoft, DefaultValue = "errors@casasoft.com.mt", Priority = 010000)]
            Email_EmailAddresses_ErrorNotification_Email,
            #endregion
            #region POP
            [SettingsInfo(CmsAccessRequired = CMS_ACCESS_TYPE.CasaSoft)]
            Email_POP_Host,

            [SettingsInfo(CmsAccessRequired = CMS_ACCESS_TYPE.CasaSoft)]
            Email_POP_Username,

            [SettingsInfo(CmsAccessRequired = CMS_ACCESS_TYPE.CasaSoft)]
            Email_POP_Password,

            [SettingsInfo(CmsAccessRequired = CMS_ACCESS_TYPE.CasaSoft)]
            Email_POP_Port,
            #endregion
            #endregion
            [SettingsInfo(CmsAccessRequired = CMS_ACCESS_TYPE.CasaSoft)]
            ImagesInfo,
            #region Others

            [SettingsInfo(Priority = 002000, DefaultValue = true)]
            AdvertsShowTopRightAdvertTag,

            [SettingsInfo(Priority = 001000)]
            Others_BlogLink,
            [SettingsInfo(Priority = 002000, CmsAccessRequired = CMS_ACCESS_TYPE.CasaSoft, DefaultValue = "/App_data/logs/")]
            Others_LogFilesFolder,
            [SettingsInfo(Priority = 002000, DefaultValue = "{Website Name}")]
            Others_WebsiteName,

            [SettingsInfo(Priority = 003000, DefaultValue = true, DefaultLocalhostValue = false)]
            Others_PermanentRedirectionUrlsToWWW,

            [SettingsInfo(Priority = 003000, DefaultValue = true)]
            Others_RemoveDefaultPageFromWWWRedirect,

            [SettingsInfo(Priority = 004000, DefaultValue = "/_common/static/files/ffmpeg_20110806_git-6c4e9ca.exe")]
            Others_FFMpeg_Path,

            [SettingsInfo(Priority = 005000, DefaultValue = "/_common/geoip/maxmind.dat")]
            Others_MaxMind_GeoIP_DBLocation,

            [SettingsInfo(Priority = 000000)]
            Others_Error_Identifier,

            [SettingsInfo(Priority = 006000)]
            Others_HttpCombiner_UseCaching,



            [SettingsInfo(Title = "Add This Identifier", DefaultValue = "[ADD_THIS_ID_HERE]")]
            Others_AddThis_PublisherId,
            [SettingsInfo(Title = "CasaSoft Website URL", DefaultValue = "http://www.casasoft.com.mt")]
            Others_CasaSoftWebsiteURL,
            [SettingsInfo(Priority = 009000, DefaultValue = (double)18)]
            Others_VatRate,

            [SettingsInfo(Priority = 010000)]
            Others_WebsiteBaseUrl,
            [SettingsInfo(Priority = 011000)]
            Others_MinimiseJavascript,
            [SettingsInfo(Priority = 004000, CmsAccessRequired = CMS_ACCESS_TYPE.Owner, DefaultValue = 5)]
            Others_SponsorsDelayDurationSec,
            #endregion
            #region Online Payment
            [SettingsInfo(Priority = 001000, DefaultValue = false, DefaultLocalhostValue = true)]
            OnlinePayment_PaymentInTestingMode,


            [SettingsInfo(Priority = 003000, DefaultValue = true, DefaultLocalhostValue = false)]
            OnlinePayment_PaymentPageUsesSSL,


            #endregion
            [SettingsInfo(Priority = 004000, CmsAccessRequired = CMS_ACCESS_TYPE.CasaSoft, DefaultValue = true)]
            CMS_AutoLoginFromDevMachines,
            [SettingsInfo(Priority = 004000, CmsAccessRequired = CMS_ACCESS_TYPE.Owner, DefaultValue = "")]
            CMS_WhiteListedIPs,
            #region Importer settings
            [SettingsInfo(DefaultValue = "/uploads/DataImport/")]
            ProductsBulkImportUploadPath,
            #endregion
            [SettingsInfo(Priority = 004000, CmsAccessRequired = CMS_ACCESS_TYPE.Owner, DefaultValue = "")]
            OpenGraph_DefaultImage,

            [SettingsInfo(Priority = 004000, CmsAccessRequired = CMS_ACCESS_TYPE.Owner, DefaultValue = "")]
            Email_EmailAddresses_General_ReplyToEmail,
            [SettingsInfo(Priority = 004000, CmsAccessRequired = CMS_ACCESS_TYPE.Owner, DefaultValue = "")]
            Email_EmailAddresses_General_ReplyToName,
            [SettingsInfo(Priority = 004000, CmsAccessRequired = CMS_ACCESS_TYPE.Owner, DefaultValue = "")]
            Email_EmailAddresses_ContactForm_ToEmail,
            [SettingsInfo(Priority = 004000, CmsAccessRequired = CMS_ACCESS_TYPE.Owner, DefaultValue = "")]
            Email_EmailAddresses_ContactForm_ToName,
            [SettingsInfo(Priority = 004000, CmsAccessRequired = CMS_ACCESS_TYPE.Owner, DefaultValue = "")]
            CompanyInfo_GoogleAnalyticsUserAccountID,
            [SettingsInfo(CmsAccessRequired = CMS_ACCESS_TYPE.Owner, DefaultLocalhostValue = "email.test@casasoft.com.mt")]
            Email_EmailAddresses_Notification_Email,
            [SettingsInfo(CmsAccessRequired = CMS_ACCESS_TYPE.CasaSoft, DefaultValue = "/images/logo.png")]
            CompanyInfo_InvoiceLogoURL,
            [SettingsInfo(AlternateIdentifier = "PayMammoth/SecretWord", Priority = 004000, DefaultValue = "123456")]
            PayMammoth_SecretWord,
            [SettingsInfo(AlternateIdentifier = "PayMammoth/WebsiteAccountCode", Priority = 004000, DefaultValue = "TEST")]
            PayMammoth_WebsiteAccountCode,
            [SettingsInfo(Priority = 004000, CmsAccessRequired = CMS_ACCESS_TYPE.CasaSoft, DefaultValue = true)]
            MembersArea_Invoice_ShowQuantity,
            [SettingsInfo(Priority = 004000, CmsAccessRequired = CMS_ACCESS_TYPE.CasaSoft, DefaultValue = true)]
            MembersArea_Invoice_ShowUnitPrice,
            [SettingsInfo(Priority = 004000, CmsAccessRequired = CMS_ACCESS_TYPE.CasaSoft, DefaultValue = true)]
            MembersArea_Invoice_ShowTax,
            MembersArea_Invoice_ShowDiscount,
            MembersArea_Invoice_ShowShipping,
            [SettingsInfo(CmsAccessRequired = CMS_ACCESS_TYPE.Owner, DefaultValue = "SingleDomain")]
            CompanyInfo_GoogleAnalyticsDomainType,
            BaseWebsiteDomain,
            [SettingsInfo(Priority = 1000)]
            CompanyInfo_SocialNetworking_Linkedin,
            [SettingsInfo(CmsAccessRequired = CMS_ACCESS_TYPE.Owner, DefaultValue = "/images/default-listing-image.jpg")]
            Listings_Default_Image,
            [SettingsInfo(CmsAccessRequired = CMS_ACCESS_TYPE.Owner, DefaultValue = "true")]
            MembersArea_Register_IncludeTermsAndConditions,
            [SettingsInfo(CmsAccessRequired = CMS_ACCESS_TYPE.Administrator, DefaultValue = "email.test@casasoft.com.mt")]
            Email_EmailAddresses_AdminNotificationToEmail,
            [SettingsInfo(CmsAccessRequired = CMS_ACCESS_TYPE.Administrator, DefaultValue = "Admin Istrator")]
            Email_EmailAddresses_AdminNotificationToName,
            [SettingsInfo(CmsAccessRequired = CMS_ACCESS_TYPE.CasaSoft, DefaultValue = "DefaultStyleSmall")]
            Others_AddThis_Style,
            [SettingsInfo(CmsAccessRequired = CMS_ACCESS_TYPE.CasaSoft, DefaultValue = "/images/flags/")]
            BaseRelativeFlagLocationDirectoryPath,
            [SettingsInfo(CmsAccessRequired = CMS_ACCESS_TYPE.CasaSoft, DefaultValue = ".png")]
            FlagImagesExtension,
            [SettingsInfo(CmsAccessRequired = CMS_ACCESS_TYPE.CasaSoft, DefaultValue = "GenericListingItem")]
            General_Listing_ListingControlType,
            [SettingsInfo(CmsAccessRequired = CMS_ACCESS_TYPE.CasaSoft, DefaultValue = "3")]
            General_Listing_MaximumItems,
            [SettingsInfo(CmsAccessRequired = CMS_ACCESS_TYPE.CasaSoft, DefaultValue = false)]
            MakeSystemOfflineCompatible,
            [SettingsInfo(Priority = 4000, Description = "This is used to fake the current user's IP address.  If you are testing online, you must be logged in to CMS")]
            UserInfo_FakeIPV4Address,
            [SettingsInfo(Priority = 4000, Description = "This is used to fake the current user's IP address.  If you are testing online, you must be logged in to CMS")]
            UserInfo_FakeIPV6Address,
            [SettingsInfo(CmsAccessRequired = CMS_ACCESS_TYPE.CasaSoft, DefaultValue = 10)]
            LatestNews_LimitAmt,
            [SettingsInfo(CmsAccessRequired = CMS_ACCESS_TYPE.CasaSoft, DefaultValue = true)]
            LatestNews_ShowViewMoreAnchor,
            [SettingsInfo(CmsAccessRequired = CMS_ACCESS_TYPE.CasaSoft, DefaultValue = "dd")]
            LatestNews_Day_DateFormat,
            [SettingsInfo(CmsAccessRequired = CMS_ACCESS_TYPE.CasaSoft, DefaultValue = "MMM")]
            LatestNews_Month_DateFormat,
            [SettingsInfo(CmsAccessRequired = CMS_ACCESS_TYPE.CasaSoft, DefaultValue = "yyyy")]
            LatestNews_Year_DateFormat
        }

        public enum GOOGLE_ANALYTICS_TYPE
        {
            SingleDomain = 0,
            WithSubDomains = 25,
            MultipleTLD = 50
        }


        //THe 'worse' the error, the higher the number
        public enum STATUS_MSG_TYPE
        {
            Success = 0,
            Warning = 25,
            Information = 50,
            Error = 100
            
        }

        public enum LISTING_CONTROl_TYPE
        {
            [Description("GenericListingItem")]
            GenericListingItem,
            [Description("GenericListingFloatedItem")]
            GenericListingFloatedItem
        }

        public enum CMS_ACCESS_TYPE
        {
            Everyone =1,
            RestrictedUser = 25,
            Normal = 50,
            Administrator = 100,
            SuperAdministrator = 200,
            Owner = 300,
            [Description("CasaSoft (Dev Team)")]
            CasaSoftDevTeam = 950,
            [Description("CasaSoft")]
            CasaSoft = 1000,
            NoOne = 99999

        }
        public static bool CheckAdminUserAccess(CMS_ACCESS_TYPE currAccessType, CMS_ACCESS_TYPE requiredAccessType)
        {
            int iCurr = (int)currAccessType;
            int iReq = (int)requiredAccessType;
            return (iCurr >= iReq);
        }

        
        public enum HREF_TARGET
        {
            [Description("_blank")]
            Blank = 1,
            [Description("_self")]
            Self = 2,
            [Description("_parent")]            
            Parent = 3
        }

        public enum LENGTH_UNITS
        {
            [Description("Millimetre(mm)"), Unit(0.000001,"mm")]
            Millimetre = 0,
            [Description("Centimetre(cm)"), Unit(0.00001, "cm")]
            Centimetre = 10,
            [Description("Inches (in)"), Unit(0.0000254, "in")]
            Inches = 15,
            [Description("Feet (ft)"), Unit(0.0003048, "ft")]
            Feet = 17,
            [Description("Metre (m)"), Unit(0.001,"m")]
            Metre = 20,
            [Description("Kilometre (km)"), Unit(1, "km", true)]
            Kilometre = 30,
            [Description("Mile (mi)"), Unit(1.609344, "mi")]
            Mile = 40,
            
            
        }

        public enum WEIGHT_UNITS
        {
            [Description("Ounce (oz)"), Priority(30), Unit(0.0283495231, "oz")]            
            Ounce,
            [Description("Pound (lb)"), Priority(20), Unit(0.45359237, "lb")]            
            Pound,
            [Description("Tonnes (t)"), Priority(0), Unit(1000, "t")]            
            Tonne,
            [Description("Kilograms (kg)"), Priority(10), Unit(1, "kg", true)]            
            Kilograms,
            [Description("Grams (g)"), Priority(40), Unit(0.001, "g")]            
            Grams,
        }

        public static VOLUME_UNITS MapLengthUnitsToVolumeUnits(LENGTH_UNITS unit)
        {
            switch (unit)
            {
                case LENGTH_UNITS.Centimetre:
                    return VOLUME_UNITS.CubicCentimetres;
                    break;
                case LENGTH_UNITS.Kilometre:
                    return VOLUME_UNITS.CubicKilometres;
                    break;
                case LENGTH_UNITS.Metre:
                    return VOLUME_UNITS.CubicMetres;
                    break;
                case LENGTH_UNITS.Mile:
                    return VOLUME_UNITS.CubicMiles;
                    break;
                case LENGTH_UNITS.Inches:
                    return VOLUME_UNITS.CubicInches;
                    break;
                case LENGTH_UNITS.Feet:
                    return VOLUME_UNITS.CubicFeet;
                    break;
                case LENGTH_UNITS.Millimetre:
                    return VOLUME_UNITS.CubicMillimitres;
                default:
                    throw new InvalidOperationException("Unit <" + unit + "> not mapped!");
            }
        }

        public enum VOLUME_UNITS
        {
            [Description("Cubic Millimetre (mm�)"), Unit(0.000000001, "mm�", true)]
            CubicMillimitres,
            [Description("Cubic Centimetres (cm�)"), Unit(0.000001, "cm�")]
            CubicCentimetres,
            [Description("Cubic Inches (in�)"), Unit(0.000016387064, "in�", true)]
            CubicInches,
            [Description("Cubic Feet (ft�)"), Unit(0.0283168466, "ft�", true)]
            CubicFeet,
            [Description("Cubic Metres (m�)"), Unit(1, "m�", true)]
            CubicMetres,
            [Description("Cubic Kilometres (km�)"), Unit(1000000000, "km�", true)]
            CubicKilometres,
            [Description("Cubic Miles (mi�)"), Unit(4168181830,"mi�")]            
            CubicMiles
        }
        public static LENGTH_UNITS GetCorrespondingLengthUnitFromVolumeUnit(VOLUME_UNITS unit)
        {
            switch (unit)
            {
                case VOLUME_UNITS.CubicCentimetres: return LENGTH_UNITS.Centimetre;
                case VOLUME_UNITS.CubicKilometres: return LENGTH_UNITS.Kilometre;
                case VOLUME_UNITS.CubicMetres: return LENGTH_UNITS.Metre;
                case VOLUME_UNITS.CubicInches: return LENGTH_UNITS.Inches;
                case VOLUME_UNITS.CubicFeet: return LENGTH_UNITS.Feet;
                case VOLUME_UNITS.CubicMiles: return LENGTH_UNITS.Mile;
                case VOLUME_UNITS.CubicMillimitres: return LENGTH_UNITS.Millimetre;
            }
            throw new NotImplementedException("This type is not converted");
        }
        public static VOLUME_UNITS GetCorrespondingVolumeUnitFromLengthUnit(LENGTH_UNITS unit)
        {
            switch (unit)
            {
                case LENGTH_UNITS.Centimetre: return VOLUME_UNITS.CubicCentimetres;
                case LENGTH_UNITS.Kilometre: return VOLUME_UNITS.CubicKilometres;
                case LENGTH_UNITS.Metre: return VOLUME_UNITS.CubicMetres;
                case LENGTH_UNITS.Feet: return VOLUME_UNITS.CubicInches;
                case LENGTH_UNITS.Inches: return VOLUME_UNITS.CubicFeet;
                case LENGTH_UNITS.Mile: return VOLUME_UNITS.CubicMiles;
                case LENGTH_UNITS.Millimetre: return VOLUME_UNITS.CubicMillimitres;
            }
            throw new NotImplementedException("This type is not converted");
        }
        public enum TEMPERATURE_UNITS
        {
            [Description("Fahrenheit (F)"), Unit(1, "�F", true)]
            Fahrenheit,
            [Description("Celcius"), Unit(1, "�C")]
            Celcius
        }


        public enum ADDTHIS_STYLE
        {
            [Description("DefaultStyleSmall")]            
            DefaultStyleSmall,
            [Description("DefaultStyleLarge")]            
            DefaultStyleLarge,
            [Description("ExtraCountStyle")]            
            ExtraCountStyle
        }

        public enum FACEBOOK_COLOUR_SCHEME
        {
            [Description("light")]
            Light,
            [Description("dark")]
            Dark,
        }

        public enum FACEBOOK_FONT
        {
            [Description("arial")]
            Arial,
            [Description("lucida grande")]
            LucidaGrande,
            [Description("segoe ui")]
            SegoeUi,
            [Description("tahoma")]
            Tahoma,
            [Description("trebuchet ms")]
            TrebuchetMs,
            [Description("verdana")]
            Verdana,
        }
        public enum PROFILE_FIELDS_TYPE
        {
            Registration,
            Profile
        }
        public enum NIVO_SLIDER_EFFECTS
        {
            sliceDown,
            sliceDownLeft,
            sliceUp,
            sliceUpLeft,
            sliceUpDown,
            sliceUpDownLeft,
            fold,
            fade,
            random,
            slideInRight,
            slideInLeft,
            boxRandom,
            boxRain,
            boxRainReverse,
            boxRainGrow,
            boxRainGrowReverse
        }
        public enum FACEBOOK_VERB
        {
            [Description("like")]
            Like,
            [Description("recommend")]
            Recommend,
        }

        public enum FACEBOOK_LINK_TARGET
        {
            [Description("_blank")]
            Blank,
            [Description("_top")]
            Top,
            [Description("_parent")]
            Parent
        }

        public enum FACEBOOK_LAYOUT_STYLE
        {
            [Description("standard")]
            Standard,
            [Description("button_count")]
            ButtonCount,
            [Description("box_count")]
            BoxCount
        }

        public enum GOOGLE_PLUS_BUTTON_STYLE
        {
            [Description("tall")]
            None,
            [Description("bubble")]
            Bubble,
            [Description("inline")]
            Inline
        }

        public enum GOOGLE_PLUS_BUTTON_SIZE
        {
            [Description("small")]
            Small,
            [Description("standard")]
            Standard,
            [Description("medium")]
            Medium,
            [Description("tall")]
            Tall
        }

        public enum FACEBOOK_FBML_NAMESPACE
        {
            [Description("fb:admins")]
            Admins,
            [Description("fb:app_id")]
            AppID,
            [Description("fb:page_id")]
            PageID
        }

        public enum OPEN_GRAPH_TYPE
        {
            [Description("activity")]
            Activity,
            [Description("sport")]
            Sport,
            [Description("bar")]
            Bar,
            [Description("company")]
            Company,
            [Description("cafe")]
            Cafe,
            [Description("hotel")]
            Hotel,
            [Description("restaurant")]
            Restaurant,
            [Description("cause")]
            Cause,
            [Description("sports_league")]
            SportsLeague,
            [Description("sports_team")]
            SportsTeam,
            [Description("band")]
            Band,
            [Description("government")]
            Government,
            [Description("non_profit")]
            NonProfit,
            [Description("school")]
            School,
            [Description("university")]
            University,
            [Description("actor")]
            Actor,
            [Description("athlete")]
            Athlete,
            [Description("author")]
            Author,
            [Description("director")]
            Director,
            [Description("musician")]
            Musician,
            [Description("politician")]
            Politician,
            [Description("profile")]
            Profile,
            [Description("public_figure")]
            PublicFigure,
            [Description("city")]
            City,
            [Description("country")]
            Country,
            [Description("landmark")]
            Landmark,
            [Description("state_province")]
            StateProvince,
            [Description("album")]
            Album,
            [Description("book")]
            Book,
            [Description("drink")]
            Drink,
            [Description("food")]
            Food,
            [Description("game")]
            Game,
            [Description("movie")]
            Movie,
            [Description("product")]
            Product,
            [Description("song")]
            Song,
            [Description("tv_show")]
            TVShow,
            [Description("article")]
            Article,
            [Description("blog")]
            Blog,
            [Description("website")]
            Website
        }

        public enum OPEN_GRAPH
        {
            [Description("og:title")]
            Title,
            [Description("og:type")]
            Type,
            [Description("og:image")]
            Image,
            [Description("og:url")]
            URL,
            [Description("og:site_name")]
            SiteName,
            [Description("og:description")]
            Description,
            [Description("og:latidude")]
            Latidude,
            [Description("og:longitude")]
            Longitude,
            [Description("og:street-address")]
            StreetAddress,
            [Description("og:locality")]
            Locality,
            [Description("og:region")]
            Region,
            [Description("og:postal-code")]
            PostalCode,
            [Description("og:country-name")]
            CountryName,
            [Description("og:email")]
            Email,
            [Description("og:phone_number")]
            PhoneNumber,
            [Description("og:fax_number")]
            FaxNumber,
            [Description("og:video")]
            Video,
            [Description("og:video:height")]
            VideoHeight,
            [Description("og:video:width")]
            VideoWidth,
            [Description("og:video:type")]
            VideoType,
            [Description("og:audio")]
            Audio,
            [Description("og:audio:title")]
            AudioTitle,
            [Description("og:audio:artist")]
            AudioArtist,
            [Description("og:audio:album")]
            AudioAlbum,
            [Description("og:audio:type")]
            AudioType,
            [Description("og:language")]
            Language
        }



        public enum JSCRIPT_FRAMEWORK
        {
           // Dojo,
            JQuery,
            NotSpecified
        }
       

        public enum HTTP_METHOD
        {
            POST,
            GET
        }

        public enum ORDER_RETURN_STATUS
        {
            [DescriptionAttribute("None")]
            None = 10,
            [DescriptionAttribute("To Return")]
            ToReturn = 30,
            [DescriptionAttribute("Returned")]
            Returned = 60,
        }
        public enum SORT_TYPE
        {

            None = 0, Ascending = 1, Descending = 2
        }
        public enum ORDER_STATUS
        {
            [DescriptionAttribute("Cancelled")]
            Cancelled = 10,
            [DescriptionAttribute("Processing")]
            Paid = 40,
            [DescriptionAttribute("Pending Payment")]
            PendingPayment = 45,
            [DescriptionAttribute("Shipped")]
            Shipped = 50,
            [DescriptionAttribute("Delivered")]
            Delivered = 70,
            [DescriptionAttribute("Marked For Return")]
            ToReturn = 80,
            [DescriptionAttribute("Returned")]
            Returned = 60
        }
        public static SORT_TYPE InvertSortType(SORT_TYPE sortType)
        {
            if (sortType == SORT_TYPE.Ascending)
                return SORT_TYPE.Descending;
            else
                return SORT_TYPE.Ascending;
        }
        public static string GoogleMapTypeToString(GOOGLE_MAP_TYPE mapType)
        {
            switch (mapType)
            {
                case GOOGLE_MAP_TYPE.Hybrid: return "google.maps.MapTypeId.HYBRID";
                case GOOGLE_MAP_TYPE.Roadmap: return "google.maps.MapTypeId.ROADMAP";
                case GOOGLE_MAP_TYPE.Satellite: return "google.maps.MapTypeId.SATELLITE";
                case GOOGLE_MAP_TYPE.Terrain: return "google.maps.MapTypeId.TERRAIN";                    
            }
            return null;
        }
        public enum GENDER
        {
            [Priority(0)]
            Male = 100,
            [Priority(10)]
            Female = 50
        }
        public enum GOOGLE_MAP_TYPE
        {
            Hybrid,
            Roadmap,
            Satellite,
            Terrain,
            None
        }
        public static ITEM_TYPE ItemTypeFromFilename(string filename)
        {
            ITEM_TYPE itemType = ITEM_TYPE.Other;
            filename = filename ?? "";
            filename = filename.ToLower();
            string ext = CS.General_v3.Util.IO.GetExtension(filename);
            switch (ext)
            {
                case "pdf": itemType = ITEM_TYPE.PDF; break;
                case "mp3": itemType = ITEM_TYPE.MP3; break;
                case "swf" : itemType = ITEM_TYPE.SWF; break;
                default:
                    {
                        if (CS.General_v3.Util.Image.CheckImageFileExtension(filename))
                            itemType = ITEM_TYPE.Image;
                        else if (CS.General_v3.Util.VideoUtil.CheckVideoExtension(filename))
                            itemType = ITEM_TYPE.Video;
                        break;
                    }
            }
            return itemType;
        }
        public enum ITEM_TYPE
        {
            File = 25,
            Image = 50,
            MP3 = 75,
            Other = 100,
            PDF = 125,
            SWF = 150,
            Video = 175
            
        }
        public enum GALLERY_IMAGE_TYPE
        {
            Images = 5000,
            Videos = 5010
            
        }
        public enum CROP_IDENTIFIER
        {
            None = 0,
            TopLeft = 1, TopMiddle = 2, TopRight = 3,
            MiddleLeft = 4, MiddleMiddle = 5, MiddleRight = 6,
            BottomLeft = 7, BottomMiddle = 8, BottomRight = 9
        }
        
        public static CROP_IDENTIFIER CropIdentifierFromText(string s)
        {
            return CropIdentifierFromText(s, CROP_IDENTIFIER.None);
        }
        public static CROP_IDENTIFIER CropIdentifierFromText(string s, CROP_IDENTIFIER defaultCropIdentifier)
        {
            s = s ?? "";
            s = s.ToLower();
            switch (s)
            {
                case "topleft": return CROP_IDENTIFIER.TopLeft;
                case "topmiddle": return CROP_IDENTIFIER.TopMiddle;
                case "topright": return CROP_IDENTIFIER.TopRight;
                case "middleleft": return CROP_IDENTIFIER.MiddleLeft;
                case "middlemiddle": return CROP_IDENTIFIER.MiddleMiddle;
                case "middleright": return CROP_IDENTIFIER.MiddleRight;
                case "bottomleft": return CROP_IDENTIFIER.BottomLeft;
                case "bottommiddle": return CROP_IDENTIFIER.BottomMiddle;
                case "bottomright": return CROP_IDENTIFIER.BottomRight;



            }
            return defaultCropIdentifier;

        }
        public enum Direction {
            Top,
            Right,
            Bottom,
            Left,
            TopRight,
            BottomLeft,
            BottomRight,
            TopLeft
            }

        
        
        public enum LOGIN_STATUS
        {
            InvalidUser,
            InvalidPass,
            NotActivated,
            TerminatedAccount,
            NotAccepted,
            Ok,
            Blocked,
            SelfExcluded
        }
        public enum MEDIA_ITEM_TYPE
        {
            Document,
            Excel,
            Powerpoint,
            Image,
            Video,
            PDF,
            Music,
            General
        }
        public enum DATA_TYPE
        {
            None,
            Bool = 50,
            BoolNullable = 100,
            DateTime = 150,
            DateTimeNullable = 200,
            
            Double = 250,
            DoubleNullable = 300,
            Integer = 350,
            IntegerMultipleChoice = 400,
            IntegerNullable = 450,
            String = 500,
            FileUpload = 550,
            FileUploadWithMediaItem = 600,
            
            LongNullable = 650,
            Long = 700,
            Enumeration = 750,
            Other = 800

        }

        public static DATA_TYPE GetDataTypeFromObject(object o, DATA_TYPE defaultType)
        {
            if (o != null)
                return GetDataTypeFromType(o.GetType());
            else
                return defaultType;

        }
        public static DATA_TYPE GetDataTypeFromType(Type t)
        {
            if (t.IsAssignableFrom(typeof(bool?)))
                return DATA_TYPE.BoolNullable;
            else if (t.IsAssignableFrom(typeof(bool)))
                return DATA_TYPE.Bool;
            else if (t.IsEnum)
                return DATA_TYPE.Enumeration;
            else if (t.IsAssignableFrom(typeof(long?)))
                return DATA_TYPE.LongNullable;
            else if (t.IsAssignableFrom(typeof(long)))
                return DATA_TYPE.Long;
            else if (t.IsAssignableFrom(typeof(int?)))
                return DATA_TYPE.IntegerNullable;
            else if (t.IsAssignableFrom(typeof(int)))
                return DATA_TYPE.Integer;
            else if (t.IsAssignableFrom(typeof(double?)))
                return DATA_TYPE.DoubleNullable;
            else if (t.IsAssignableFrom(typeof(double)))
                return DATA_TYPE.Double;
            else if (t.IsAssignableFrom(typeof(DateTime?)))
                return DATA_TYPE.DateTimeNullable;
            else if (t.IsAssignableFrom(typeof(DateTime)))
                return DATA_TYPE.DateTime;
            else if (t.IsAssignableFrom(typeof(string)))
                return DATA_TYPE.String;
            else
                return DATA_TYPE.Other;


        }
        public static DATA_TYPE GetDataTypeFromPropertyInfo(PropertyInfo pInfo)
        {
            return GetDataTypeFromType(pInfo.PropertyType);

        }
        public enum STRING_DATA_TYPE
        {
            SingleLine,
            MultiLine,
            Email,
            Password,
            Website,
            Html
        }
        
        public enum CONTENT_TEXT_DATA_TYPE
        {
            Html = 100,
            PlainText = 0,
            Image = 200
        }

        public enum PERSONAL_ACCOUNT_TYPE
        {
            [DescriptionAttribute("Personal Account")]
            PersonalAccount,
            [DescriptionAttribute("Business Entity")]
            BusinessEntity
        }
    }
}
