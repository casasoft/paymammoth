using System;
using System.Collections.Generic;
using System.Text;

namespace CS.General_v3
{
    /// <summary>
    /// Tokenizer class to retrieve tokens of data from a piece of string, seperated 
    /// by "tokens"
    /// </summary>
    public class Tokenizer
    {
        private string _data = "";
        private string _token = "";
        private bool _getEmpty = true;

        /// <summary>
        /// Default constructor
        /// </summary>
        public Tokenizer()
        { }

        /// <summary>
        /// Constructor that can be set the data/token for this Tokenizer
        /// </summary>
        /// <param name="data">The data string</param>
        /// <param name="token">The token itself</param>
        public Tokenizer(string data, string token)
        {
            _data = data;
            _token = token;
        }

        /// <summary>
        /// The data string
        /// </summary>
        public string Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value;

            }
        }

        /// <summary>
        /// Returns whether tokenizer is empty
        /// </summary>
        public bool isEmpty
        {
            get
            {
                if (_getEmpty)
                    return (_data == "");
                else
                {
                    return (getToken(false) == "");
                }
            }
        }

        /// <summary>
        /// Whether the tokenizer should retrieve empty tokens.7
        /// </summary>
        public bool getEmptyTokens
        {
            get
            {
                return _getEmpty;
            }
            set
            {
                _getEmpty = value;
            }
        }

        /// <summary>
        /// The token that seperates the information
        /// </summary>
        public string Token
        {
            get
            {
                return _token;
            }
            set
            {
                _token = value;

            }
        }

        /// <summary>
        /// Retrieves the next token, and removes it from the data string
        /// </summary>
        /// <returns>The token, if any</returns>
        public string getToken()
        {
            return getToken(true);
        }

        /// <summary>
        /// Retrieves the next token.
        /// </summary>
        /// <param name="remove">Whether to remove the token or not from the data string</param>
        /// <returns>The token</returns>
        public string getToken(bool remove)
        {
            string token;
            int lastPos = 0, fromPos = 0;
            do
            {
                fromPos = 0;
                //if (remove)
                fromPos = lastPos;
                int pos = _data.IndexOf(_token, fromPos);
                if (pos == -1) pos = _data.Length;

                token = _data.Substring(lastPos, pos - lastPos);
                if (remove)
                {
                    if (pos < _data.Length)
                        _data = _data.Substring(pos + _token.Length);
                    else
                        _data = "";
                }
                else
                {
                    lastPos = pos + _token.Length;
                }
            } while ((token == "" && getEmptyTokens == false) && _data != "" && lastPos < _data.Length);
            return token;
        }

        /// <summary>
        /// Retrieves a token by index.  Indexing starts from 0.
        /// </summary>
        /// <param name="index">The location of the token in the string</param>
        /// <returns>The token</returns>
        public string getTokenByIndex(int index)
        {
            string token;
            string tmp = _data;
            int left = index;
            int pos = -1;
            for (int i = 0; i < index; i++)
            {
                pos = tmp.IndexOf(_token);
                if (pos == -1) pos = tmp.Length;
                tmp = tmp.Substring(pos + _token.Length);
            }
            pos = tmp.IndexOf(_token);
            if (pos == -1) pos = tmp.Length;
            token = tmp.Substring(0, pos);
            return token;

        }


    }
}
