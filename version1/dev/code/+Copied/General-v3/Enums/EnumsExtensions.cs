using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using System.ComponentModel;
using CS.General_v3.Util;
using CS.General_v3.Classes.Attributes;
namespace CS.General_v3
{
    public static class EnumsExtensionsClass
    {

        public static string GetSettingFromDatabase(this Enums.SETTINGS_ENUM enumValue)
        {
            return GetSettingFromDatabase<string>(enumValue);
        }

        public static T GetSettingFromDatabase<T>(this Enums.SETTINGS_ENUM enumValue)
        {
            return CS.General_v3.Settings.GetSettingFromDatabase<T>(enumValue);
        }


    }
}
