﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace CS.General_v3
{
    public static partial class Enums
    {
        public static partial class ISO_ENUMS
        {
            public enum LANGUAGE_ISO639
            {
                [DescriptionAttribute("Abkhaz")]
                Abkhaz,
                [DescriptionAttribute("Afar")]
                Afar,
                [DescriptionAttribute("Afrikaans")]
                Afrikaans,
                [DescriptionAttribute("Akan")]
                Akan,
                [DescriptionAttribute("Albanian")]
                Albanian,
                [DescriptionAttribute("Amharic")]
                Amharic,
                [DescriptionAttribute("Arabic")]
                Arabic,
                [DescriptionAttribute("Aragonese")]
                Aragonese,
                [DescriptionAttribute("Armenian")]
                Armenian,
                [DescriptionAttribute("Assamese")]
                Assamese,
                [DescriptionAttribute("Avaric")]
                Avaric,
                [DescriptionAttribute("Avestan")]
                Avestan,
                [DescriptionAttribute("Aymara")]
                Aymara,
                [DescriptionAttribute("Azerbaijani")]
                Azerbaijani,
                [DescriptionAttribute("Bambara")]
                Bambara,
                [DescriptionAttribute("Bashkir")]
                Bashkir,
                [DescriptionAttribute("Basque")]
                Basque,
                [DescriptionAttribute("Belarusian")]
                Belarusian,
                [DescriptionAttribute("Bengali")]
                Bengali,
                [DescriptionAttribute("Bihari")]
                Bihari,
                [DescriptionAttribute("Bislama")]
                Bislama,
                [DescriptionAttribute("Bosnian")]
                Bosnian,
                [DescriptionAttribute("Breton")]
                Breton,
                [DescriptionAttribute("Bulgarian")]
                Bulgarian,
                [DescriptionAttribute("Burmese")]
                Burmese,
                [DescriptionAttribute("Catalan")]
                Catalan,
                [DescriptionAttribute("Chamorro")]
                Chamorro,
                [DescriptionAttribute("Chechen")]
                Chechen,
                [DescriptionAttribute("Chichewa")]
                Chichewa,
                [DescriptionAttribute("Chinese")]
                Chinese,
                [DescriptionAttribute("Chinese (Simplified)")]
                ChineseSimplified,
                [DescriptionAttribute("Chuvash")]
                Chuvash,
                [DescriptionAttribute("Cornish")]
                Cornish,
                [DescriptionAttribute("Corsican")]
                Corsican,
                [DescriptionAttribute("Cree")]
                Cree,
                [DescriptionAttribute("Croatian")]
                Croatian,
                [DescriptionAttribute("Czech")]
                Czech,
                [DescriptionAttribute("Danish")]
                Danish,
                [DescriptionAttribute("Divehi")]
                Divehi,
                [DescriptionAttribute("Dutch")]
                Dutch,
                [DescriptionAttribute("Dzongkha")]
                Dzongkha,
                [DescriptionAttribute("English")]
                English,
                [DescriptionAttribute("Esperanto")]
                Esperanto,
                [DescriptionAttribute("Estonian")]
                Estonian,
                [DescriptionAttribute("Ewe")]
                Ewe,
                [DescriptionAttribute("Faroese")]
                Faroese,
                [DescriptionAttribute("Fijian")]
                Fijian,
                [DescriptionAttribute("Finnish")]
                Finnish,
                [DescriptionAttribute("French")]
                French,
                [DescriptionAttribute("Fula")]
                Fula,
                [DescriptionAttribute("Galician")]
                Galician,
                [DescriptionAttribute("Georgian")]
                Georgian,
                [DescriptionAttribute("German")]
                German,
                [DescriptionAttribute("Greek")]
                Greek,
                [DescriptionAttribute("Guaraní")]
                Guaraní,
                [DescriptionAttribute("Gujarati")]
                Gujarati,
                [DescriptionAttribute("Haitian")]
                Haitian,
                [DescriptionAttribute("Hausa")]
                Hausa,
                [DescriptionAttribute("Hebrew")]
                Hebrew,
                [DescriptionAttribute("Herero")]
                Herero,
                [DescriptionAttribute("Hindi")]
                Hindi,
                [DescriptionAttribute("Hiri Motu")]
                HiriMotu,
                [DescriptionAttribute("Hungarian")]
                Hungarian,
                [DescriptionAttribute("Interlingua")]
                Interlingua,
                [DescriptionAttribute("Indonesian")]
                Indonesian,
                [DescriptionAttribute("Interlingue")]
                Interlingue,
                [DescriptionAttribute("Irish")]
                Irish,
                [DescriptionAttribute("Igbo")]
                Igbo,
                [DescriptionAttribute("Inupiaq")]
                Inupiaq,
                [DescriptionAttribute("Ido")]
                Ido,
                [DescriptionAttribute("Icelandic")]
                Icelandic,
                [DescriptionAttribute("Italian")]
                Italian,
                [DescriptionAttribute("Inuktitut")]
                Inuktitut,
                [DescriptionAttribute("Japanese")]
                Japanese,
                [DescriptionAttribute("Javanese")]
                Javanese,
                [DescriptionAttribute("Kalaallisut")]
                Kalaallisut,
                [DescriptionAttribute("Kannada")]
                Kannada,
                [DescriptionAttribute("Kanuri")]
                Kanuri,
                [DescriptionAttribute("Kashmiri")]
                Kashmiri,
                [DescriptionAttribute("Kazakh")]
                Kazakh,
                [DescriptionAttribute("Khmer")]
                Khmer,
                [DescriptionAttribute("Kikuyu")]
                Kikuyu,
                [DescriptionAttribute("Kinyarwanda")]
                Kinyarwanda,
                [DescriptionAttribute("Kirghiz")]
                Kirghiz,
                [DescriptionAttribute("Komi")]
                Komi,
                [DescriptionAttribute("Kongo")]
                Kongo,
                [DescriptionAttribute("Korean")]
                Korean,
                [DescriptionAttribute("Kurdish")]
                Kurdish,
                [DescriptionAttribute("Kwanyama")]
                Kwanyama,
                [DescriptionAttribute("Latin")]
                Latin,
                [DescriptionAttribute("Luxembourgish")]
                Luxembourgish,
                [DescriptionAttribute("Luganda")]
                Luganda,
                [DescriptionAttribute("Limburgish")]
                Limburgish,
                [DescriptionAttribute("Lingala")]
                Lingala,
                [DescriptionAttribute("Lao")]
                Lao,
                [DescriptionAttribute("Lithuanian")]
                Lithuanian,
                [DescriptionAttribute("Luba-Katanga")]
                LubaKatanga,
                [DescriptionAttribute("Latvian")]
                Latvian,
                [DescriptionAttribute("Manx")]
                Manx,
                [DescriptionAttribute("Macedonian")]
                Macedonian,
                [DescriptionAttribute("Malagasy")]
                Malagasy,
                [DescriptionAttribute("Malay")]
                Malay,
                [DescriptionAttribute("Malayalam")]
                Malayalam,
                [DescriptionAttribute("Maltese")]
                Maltese,
                [DescriptionAttribute("Māori")]
                Māori,
                [DescriptionAttribute("Marathi (Marāṭhī)")]
                Marathi,
                [DescriptionAttribute("Marshallese")]
                Marshallese,
                [DescriptionAttribute("Mongolian")]
                Mongolian,
                [DescriptionAttribute("Nauru")]
                Nauru,
                [DescriptionAttribute("Navajo")]
                Navajo,
                [DescriptionAttribute("Norwegian Bokmål")]
                NorwegianBokmål,
                [DescriptionAttribute("North Ndebele")]
                NorthNdebele,
                [DescriptionAttribute("Nepali")]
                Nepali,
                [DescriptionAttribute("Ndonga")]
                Ndonga,
                [DescriptionAttribute("Norwegian Nynorsk")]
                NorwegianNynorsk,
                [DescriptionAttribute("Norwegian")]
                Norwegian,
                [DescriptionAttribute("Nuosu")]
                Nuosu,
                [DescriptionAttribute("South Ndebele")]
                SouthNdebele,
                [DescriptionAttribute("Occitan")]
                Occitan,
                [DescriptionAttribute("Ojibwe")]
                Ojibwe,
                [DescriptionAttribute("Old Church Slavonic")]
                OldChurchSlavonic,
                [DescriptionAttribute("Oromo")]
                Oromo,
                [DescriptionAttribute("Oriya")]
                Oriya,
                [DescriptionAttribute("Ossetian")]
                Ossetian,
                [DescriptionAttribute("Panjabi")]
                Panjabi,
                [DescriptionAttribute("Pāli")]
                Pāli,
                [DescriptionAttribute("Persian")]
                Persian,
                [DescriptionAttribute("Polish")]
                Polish,
                [DescriptionAttribute("Pashto")]
                Pashto,
                [DescriptionAttribute("Portuguese")]
                Portuguese,
                [DescriptionAttribute("Quechua")]
                Quechua,
                [DescriptionAttribute("Romansh")]
                Romansh,
                [DescriptionAttribute("Kirundi")]
                Kirundi,
                [DescriptionAttribute("Romanian")]
                Romanian,
                [DescriptionAttribute("Russian")]
                Russian,
                [DescriptionAttribute("Sanskrit (Saṁskṛta)")]
                Sanskrit,
                [DescriptionAttribute("Sardinian")]
                Sardinian,
                [DescriptionAttribute("Sindhi")]
                Sindhi,
                [DescriptionAttribute("Northern Sami")]
                NorthernSami,
                [DescriptionAttribute("Samoan")]
                Samoan,
                [DescriptionAttribute("Sango")]
                Sango,
                [DescriptionAttribute("Serbian")]
                Serbian,
                [DescriptionAttribute("Scottish Gaelic")]
                ScottishGaelic,
                [DescriptionAttribute("Shona")]
                Shona,
                [DescriptionAttribute("Sinhala")]
                Sinhala,
                [DescriptionAttribute("Slovak")]
                Slovak,
                [DescriptionAttribute("Slovene")]
                Slovene,
                [DescriptionAttribute("Somali")]
                Somali,
                [DescriptionAttribute("Southern Sotho")]
                SouthernSotho,
                [DescriptionAttribute("Spanish")]
                Spanish,
                [DescriptionAttribute("Sundanese")]
                Sundanese,
                [DescriptionAttribute("Swahili")]
                Swahili,
                [DescriptionAttribute("Swati")]
                Swati,
                [DescriptionAttribute("Swedish")]
                Swedish,
                [DescriptionAttribute("Tamil")]
                Tamil,
                [DescriptionAttribute("Telugu")]
                Telugu,
                [DescriptionAttribute("Tajik")]
                Tajik,
                [DescriptionAttribute("Thai")]
                Thai,
                [DescriptionAttribute("Tigrinya")]
                Tigrinya,
                [DescriptionAttribute("Tibetan Standard")]
                TibetanStandard,
                [DescriptionAttribute("Turkmen")]
                Turkmen,
                [DescriptionAttribute("Tagalog")]
                Tagalog,
                [DescriptionAttribute("Tswana")]
                Tswana,
                [DescriptionAttribute("Tonga")]
                Tonga,
                [DescriptionAttribute("Turkish")]
                Turkish,
                [DescriptionAttribute("Tsonga")]
                Tsonga,
                [DescriptionAttribute("Tatar")]
                Tatar,
                [DescriptionAttribute("Twi")]
                Twi,
                [DescriptionAttribute("Tahitian")]
                Tahitian,
                [DescriptionAttribute("Uighur")]
                Uighur,
                [DescriptionAttribute("Ukrainian")]
                Ukrainian,
                [DescriptionAttribute("Urdu")]
                Urdu,
                [DescriptionAttribute("Uzbek")]
                Uzbek,
                [DescriptionAttribute("Venda")]
                Venda,
                [DescriptionAttribute("Vietnamese")]
                Vietnamese,
                [DescriptionAttribute("Volapük")]
                Volapük,
                [DescriptionAttribute("Walloon")]
                Walloon,
                [DescriptionAttribute("Welsh")]
                Welsh,
                [DescriptionAttribute("Wolof")]
                Wolof,
                [DescriptionAttribute("Western Frisian")]
                WesternFrisian,
                [DescriptionAttribute("Xhosa")]
                Xhosa,
                [DescriptionAttribute("Yiddish")]
                Yiddish,
                [DescriptionAttribute("Yoruba")]
                Yoruba,
                [DescriptionAttribute("Zhuang")]
                Zhuang,
                [DescriptionAttribute("Zulu")]
                Zulu

            }
            /// <summary>
            /// Returns the code as ISO639-1 (2-letter)
            /// </summary>
            /// <param name="code"></param>
            /// <returns></returns>
            public static string Language_ISO639_2letter_ToCode(LANGUAGE_ISO639 code)
            {
                switch (code)
                {
                    case LANGUAGE_ISO639.Abkhaz: return "ab";
                    case LANGUAGE_ISO639.Afar: return "aa";
                    case LANGUAGE_ISO639.Afrikaans: return "af";
                    case LANGUAGE_ISO639.Akan: return "ak";
                    case LANGUAGE_ISO639.Albanian: return "sq";
                    case LANGUAGE_ISO639.Amharic: return "am";
                    case LANGUAGE_ISO639.Arabic: return "ar";
                    case LANGUAGE_ISO639.Aragonese: return "an";
                    case LANGUAGE_ISO639.Armenian: return "hy";
                    case LANGUAGE_ISO639.Assamese: return "as";
                    case LANGUAGE_ISO639.Avaric: return "av";
                    case LANGUAGE_ISO639.Avestan: return "ae";
                    case LANGUAGE_ISO639.Aymara: return "ay";
                    case LANGUAGE_ISO639.Azerbaijani: return "az";
                    case LANGUAGE_ISO639.Bambara: return "bm";
                    case LANGUAGE_ISO639.Bashkir: return "ba";
                    case LANGUAGE_ISO639.Basque: return "eu";
                    case LANGUAGE_ISO639.Belarusian: return "be";
                    case LANGUAGE_ISO639.Bengali: return "bn";
                    case LANGUAGE_ISO639.Bihari: return "bh";
                    case LANGUAGE_ISO639.Bislama: return "bi";
                    case LANGUAGE_ISO639.Bosnian: return "bs";
                    case LANGUAGE_ISO639.Breton: return "br";
                    case LANGUAGE_ISO639.Bulgarian: return "bg";
                    case LANGUAGE_ISO639.Burmese: return "my";
                    case LANGUAGE_ISO639.Catalan: return "ca";
                    case LANGUAGE_ISO639.Chamorro: return "ch";
                    case LANGUAGE_ISO639.Chechen: return "ce";
                    case LANGUAGE_ISO639.Chichewa: return "ny";
                    case LANGUAGE_ISO639.Chinese: return "sp";
                    case LANGUAGE_ISO639.Chuvash: return "cv";
                    case LANGUAGE_ISO639.Cornish: return "kw";
                    case LANGUAGE_ISO639.Corsican: return "co";
                    case LANGUAGE_ISO639.Cree: return "cr";
                    case LANGUAGE_ISO639.Croatian: return "hr";
                    case LANGUAGE_ISO639.Czech: return "cs";
                    case LANGUAGE_ISO639.Danish: return "da";
                    case LANGUAGE_ISO639.Divehi: return "dv";
                    case LANGUAGE_ISO639.Dutch: return "nl";
                    case LANGUAGE_ISO639.Dzongkha: return "dz";
                    case LANGUAGE_ISO639.English: return "en";
                    case LANGUAGE_ISO639.Esperanto: return "eo";
                    case LANGUAGE_ISO639.Estonian: return "et";
                    case LANGUAGE_ISO639.Ewe: return "ee";
                    case LANGUAGE_ISO639.Faroese: return "fo";
                    case LANGUAGE_ISO639.Fijian: return "fj";
                    case LANGUAGE_ISO639.Finnish: return "fi";
                    case LANGUAGE_ISO639.French: return "fr";
                    case LANGUAGE_ISO639.Fula: return "ff";
                    case LANGUAGE_ISO639.Galician: return "gl";
                    case LANGUAGE_ISO639.Georgian: return "ka";
                    case LANGUAGE_ISO639.German: return "de";
                    case LANGUAGE_ISO639.Greek: return "el";
                    case LANGUAGE_ISO639.Guaraní: return "gn";
                    case LANGUAGE_ISO639.Gujarati: return "gu";
                    case LANGUAGE_ISO639.Haitian: return "ht";
                    case LANGUAGE_ISO639.Hausa: return "ha";
                    case LANGUAGE_ISO639.Hebrew: return "he";
                    case LANGUAGE_ISO639.Herero: return "hz";
                    case LANGUAGE_ISO639.Hindi: return "hi";
                    case LANGUAGE_ISO639.HiriMotu: return "ho";
                    case LANGUAGE_ISO639.Hungarian: return "hu";
                    case LANGUAGE_ISO639.Interlingua: return "ia";
                    case LANGUAGE_ISO639.Indonesian: return "id";
                    case LANGUAGE_ISO639.Interlingue: return "ie";
                    case LANGUAGE_ISO639.Irish: return "ga";
                    case LANGUAGE_ISO639.Igbo: return "ig";
                    case LANGUAGE_ISO639.Inupiaq: return "ik";
                    case LANGUAGE_ISO639.Ido: return "io";
                    case LANGUAGE_ISO639.Icelandic: return "is";
                    case LANGUAGE_ISO639.Italian: return "it";
                    case LANGUAGE_ISO639.Inuktitut: return "iu";
                    case LANGUAGE_ISO639.Japanese: return "ja";
                    case LANGUAGE_ISO639.Javanese: return "jv";
                    case LANGUAGE_ISO639.Kalaallisut: return "kl";
                    case LANGUAGE_ISO639.Kannada: return "kn";
                    case LANGUAGE_ISO639.Kanuri: return "kr";
                    case LANGUAGE_ISO639.Kashmiri: return "ks";
                    case LANGUAGE_ISO639.Kazakh: return "kk";
                    case LANGUAGE_ISO639.Khmer: return "km";
                    case LANGUAGE_ISO639.Kikuyu: return "ki";
                    case LANGUAGE_ISO639.Kinyarwanda: return "rw";
                    case LANGUAGE_ISO639.Kirghiz: return "ky";
                    case LANGUAGE_ISO639.Komi: return "kv";
                    case LANGUAGE_ISO639.Kongo: return "kg";
                    case LANGUAGE_ISO639.Korean: return "ko";
                    case LANGUAGE_ISO639.Kurdish: return "ku";
                    case LANGUAGE_ISO639.Kwanyama: return "kj";
                    case LANGUAGE_ISO639.Latin: return "la";
                    case LANGUAGE_ISO639.Luxembourgish: return "lb";
                    case LANGUAGE_ISO639.Luganda: return "lg";
                    case LANGUAGE_ISO639.Limburgish: return "li";
                    case LANGUAGE_ISO639.Lingala: return "ln";
                    case LANGUAGE_ISO639.Lao: return "lo";
                    case LANGUAGE_ISO639.Lithuanian: return "lt";
                    case LANGUAGE_ISO639.LubaKatanga: return "td";
                    case LANGUAGE_ISO639.Latvian: return "lv";
                    case LANGUAGE_ISO639.Manx: return "gv";
                    case LANGUAGE_ISO639.Macedonian: return "mk";
                    case LANGUAGE_ISO639.Malagasy: return "mg";
                    case LANGUAGE_ISO639.Malay: return "ms";
                    case LANGUAGE_ISO639.Malayalam: return "ml";
                    case LANGUAGE_ISO639.Maltese: return "mt";
                    case LANGUAGE_ISO639.Māori: return "mi";
                    case LANGUAGE_ISO639.Marathi: return "mr";
                    case LANGUAGE_ISO639.Marshallese: return "mh";
                    case LANGUAGE_ISO639.Mongolian: return "mn";
                    case LANGUAGE_ISO639.Nauru: return "na";
                    case LANGUAGE_ISO639.Navajo: return "nv";
                    case LANGUAGE_ISO639.NorwegianBokmål: return "nb";
                    case LANGUAGE_ISO639.NorthNdebele: return "nd";
                    case LANGUAGE_ISO639.Nepali: return "ne";
                    case LANGUAGE_ISO639.Ndonga: return "ng";
                    case LANGUAGE_ISO639.NorwegianNynorsk: return "nn";
                    case LANGUAGE_ISO639.Norwegian: return "no";
                    case LANGUAGE_ISO639.Nuosu: return "ii";
                    case LANGUAGE_ISO639.SouthNdebele: return "nr";
                    case LANGUAGE_ISO639.Occitan: return "oc";
                    case LANGUAGE_ISO639.Ojibwe: return "oj";
                    case LANGUAGE_ISO639.OldChurchSlavonic: return "cu";
                    case LANGUAGE_ISO639.Oromo: return "om";
                    case LANGUAGE_ISO639.Oriya: return "or";
                    case LANGUAGE_ISO639.Ossetian: return "os";
                    case LANGUAGE_ISO639.Panjabi: return "pa";
                    case LANGUAGE_ISO639.Pāli: return "pi";
                    case LANGUAGE_ISO639.Persian: return "fa";
                    case LANGUAGE_ISO639.Polish: return "pl";
                    case LANGUAGE_ISO639.Pashto: return "ps";
                    case LANGUAGE_ISO639.Portuguese: return "pt";
                    case LANGUAGE_ISO639.Quechua: return "qu";
                    case LANGUAGE_ISO639.Romansh: return "rm";
                    case LANGUAGE_ISO639.Kirundi: return "rn";
                    case LANGUAGE_ISO639.Romanian: return "ro";
                    case LANGUAGE_ISO639.Russian: return "ru";
                    case LANGUAGE_ISO639.Sanskrit: return "sa";
                    case LANGUAGE_ISO639.Sardinian: return "sc";
                    case LANGUAGE_ISO639.Sindhi: return "sd";
                    case LANGUAGE_ISO639.NorthernSami: return "se";
                    case LANGUAGE_ISO639.Samoan: return "sm";
                    case LANGUAGE_ISO639.Sango: return "sg";
                    case LANGUAGE_ISO639.Serbian: return "sr";
                    case LANGUAGE_ISO639.ScottishGaelic: return "gd";
                    case LANGUAGE_ISO639.Shona: return "sn";
                    case LANGUAGE_ISO639.Sinhala: return "si";
                    case LANGUAGE_ISO639.Slovak: return "sk";
                    case LANGUAGE_ISO639.Slovene: return "sl";
                    case LANGUAGE_ISO639.Somali: return "so";
                    case LANGUAGE_ISO639.SouthernSotho: return "st";
                    case LANGUAGE_ISO639.Spanish: return "es";
                    case LANGUAGE_ISO639.Sundanese: return "su";
                    case LANGUAGE_ISO639.Swahili: return "sw";
                    case LANGUAGE_ISO639.Swati: return "ss";
                    case LANGUAGE_ISO639.Swedish: return "sv";
                    case LANGUAGE_ISO639.Tamil: return "ta";
                    case LANGUAGE_ISO639.Telugu: return "te";
                    case LANGUAGE_ISO639.Tajik: return "tg";
                    case LANGUAGE_ISO639.Thai: return "th";
                    case LANGUAGE_ISO639.Tigrinya: return "ti";
                    case LANGUAGE_ISO639.TibetanStandard: return "bo";
                    case LANGUAGE_ISO639.Turkmen: return "tk";
                    case LANGUAGE_ISO639.Tagalog: return "tl";
                    case LANGUAGE_ISO639.Tswana: return "tn";
                    case LANGUAGE_ISO639.Tonga: return "to";
                    case LANGUAGE_ISO639.Turkish: return "tr";
                    case LANGUAGE_ISO639.Tsonga: return "ts";
                    case LANGUAGE_ISO639.Tatar: return "tt";
                    case LANGUAGE_ISO639.Twi: return "tw";
                    case LANGUAGE_ISO639.Tahitian: return "ty";
                    case LANGUAGE_ISO639.Uighur: return "ug";
                    case LANGUAGE_ISO639.Ukrainian: return "uk";
                    case LANGUAGE_ISO639.Urdu: return "ur";
                    case LANGUAGE_ISO639.Uzbek: return "uz";
                    case LANGUAGE_ISO639.Venda: return "ve";
                    case LANGUAGE_ISO639.Vietnamese: return "vi";
                    case LANGUAGE_ISO639.Volapük: return "vo";
                    case LANGUAGE_ISO639.Walloon: return "wa";
                    case LANGUAGE_ISO639.Welsh: return "cy";
                    case LANGUAGE_ISO639.Wolof: return "wo";
                    case LANGUAGE_ISO639.WesternFrisian: return "fy";
                    case LANGUAGE_ISO639.Xhosa: return "xh";
                    case LANGUAGE_ISO639.Yiddish: return "yi";
                    case LANGUAGE_ISO639.Yoruba: return "yo";
                    case LANGUAGE_ISO639.Zhuang: return "za";
                    case LANGUAGE_ISO639.Zulu: return "zu";


                }
                return "";
            }

/// <summary>
            /// Returns the code as ISO639-2 (3-letter)
            /// </summary>
            /// <param name="code"></param>
            /// <returns></returns>
            public static string Language_ISO639_3letter_ToCode(LANGUAGE_ISO639 code)
            {
                switch (code)
                {
                    case LANGUAGE_ISO639.Abkhaz: return "abk";
                    case LANGUAGE_ISO639.Afar: return "aar";
                    case LANGUAGE_ISO639.Afrikaans: return "afr";
                    case LANGUAGE_ISO639.Akan: return "aka";
                    case LANGUAGE_ISO639.Albanian: return "sqi";
                    case LANGUAGE_ISO639.Amharic: return "amh";
                    case LANGUAGE_ISO639.Arabic: return "ara";
                    case LANGUAGE_ISO639.Aragonese: return "arg";
                    case LANGUAGE_ISO639.Armenian: return "hye";
                    case LANGUAGE_ISO639.Assamese: return "asm";
                    case LANGUAGE_ISO639.Avaric: return "ava";
                    case LANGUAGE_ISO639.Avestan: return "ave";
                    case LANGUAGE_ISO639.Aymara: return "aym";
                    case LANGUAGE_ISO639.Azerbaijani: return "aze";
                    case LANGUAGE_ISO639.Bambara: return "bam";
                    case LANGUAGE_ISO639.Bashkir: return "bak";
                    case LANGUAGE_ISO639.Basque: return "eus";
                    case LANGUAGE_ISO639.Belarusian: return "bel";
                    case LANGUAGE_ISO639.Bengali: return "ben";
                    case LANGUAGE_ISO639.Bihari: return "bih";
                    case LANGUAGE_ISO639.Bislama: return "bis";
                    case LANGUAGE_ISO639.Bosnian: return "bos";
                    case LANGUAGE_ISO639.Breton: return "bre";
                    case LANGUAGE_ISO639.Bulgarian: return "bul";
                    case LANGUAGE_ISO639.Burmese: return "mya";
                    case LANGUAGE_ISO639.Catalan: return "cat";
                    case LANGUAGE_ISO639.Chamorro: return "cha";
                    case LANGUAGE_ISO639.Chechen: return "che";
                    case LANGUAGE_ISO639.Chichewa: return "nya";
                    case LANGUAGE_ISO639.Chinese: return "zho";
                    case LANGUAGE_ISO639.Chuvash: return "chv";
                    case LANGUAGE_ISO639.Cornish: return "cor";
                    case LANGUAGE_ISO639.Corsican: return "cos";
                    case LANGUAGE_ISO639.Cree: return "cre";
                    case LANGUAGE_ISO639.Croatian: return "hrv";
                    case LANGUAGE_ISO639.Czech: return "ces";
                    case LANGUAGE_ISO639.Danish: return "dan";
                    case LANGUAGE_ISO639.Divehi: return "div";
                    case LANGUAGE_ISO639.Dutch: return "nld";
                    case LANGUAGE_ISO639.Dzongkha: return "dzo";
                    case LANGUAGE_ISO639.English: return "eng";
                    case LANGUAGE_ISO639.Esperanto: return "epo";
                    case LANGUAGE_ISO639.Estonian: return "est";
                    case LANGUAGE_ISO639.Ewe: return "ewe";
                    case LANGUAGE_ISO639.Faroese: return "fao";
                    case LANGUAGE_ISO639.Fijian: return "fij";
                    case LANGUAGE_ISO639.Finnish: return "fin";
                    case LANGUAGE_ISO639.French: return "fra";
                    case LANGUAGE_ISO639.Fula: return "ful";
                    case LANGUAGE_ISO639.Galician: return "glg";
                    case LANGUAGE_ISO639.Georgian: return "kat";
                    case LANGUAGE_ISO639.German: return "deu";
                    case LANGUAGE_ISO639.Greek: return "ell";
                    case LANGUAGE_ISO639.Guaraní: return "grn";
                    case LANGUAGE_ISO639.Gujarati: return "guj";
                    case LANGUAGE_ISO639.Haitian: return "hat";
                    case LANGUAGE_ISO639.Hausa: return "hau";
                    case LANGUAGE_ISO639.Hebrew: return "heb";
                    case LANGUAGE_ISO639.Herero: return "her";
                    case LANGUAGE_ISO639.Hindi: return "hin";
                    case LANGUAGE_ISO639.HiriMotu: return "hmo";
                    case LANGUAGE_ISO639.Hungarian: return "hun";
                    case LANGUAGE_ISO639.Interlingua: return "ina";
                    case LANGUAGE_ISO639.Indonesian: return "ind";
                    case LANGUAGE_ISO639.Interlingue: return "ile";
                    case LANGUAGE_ISO639.Irish: return "gle";
                    case LANGUAGE_ISO639.Igbo: return "ibo";
                    case LANGUAGE_ISO639.Inupiaq: return "ipk";
                    case LANGUAGE_ISO639.Ido: return "ido";
                    case LANGUAGE_ISO639.Icelandic: return "isl";
                    case LANGUAGE_ISO639.Italian: return "ita";
                    case LANGUAGE_ISO639.Inuktitut: return "iku";
                    case LANGUAGE_ISO639.Japanese: return "jpn";
                    case LANGUAGE_ISO639.Javanese: return "jav";
                    case LANGUAGE_ISO639.Kalaallisut: return "kal";
                    case LANGUAGE_ISO639.Kannada: return "kan";
                    case LANGUAGE_ISO639.Kanuri: return "kau";
                    case LANGUAGE_ISO639.Kashmiri: return "kas";
                    case LANGUAGE_ISO639.Kazakh: return "kaz";
                    case LANGUAGE_ISO639.Khmer: return "khm";
                    case LANGUAGE_ISO639.Kikuyu: return "kik";
                    case LANGUAGE_ISO639.Kinyarwanda: return "kin";
                    case LANGUAGE_ISO639.Kirghiz: return "kir";
                    case LANGUAGE_ISO639.Komi: return "kom";
                    case LANGUAGE_ISO639.Kongo: return "kon";
                    case LANGUAGE_ISO639.Korean: return "kor";
                    case LANGUAGE_ISO639.Kurdish: return "kur";
                    case LANGUAGE_ISO639.Kwanyama: return "kua";
                    case LANGUAGE_ISO639.Latin: return "lat";
                    case LANGUAGE_ISO639.Luxembourgish: return "ltz";
                    case LANGUAGE_ISO639.Luganda: return "lug";
                    case LANGUAGE_ISO639.Limburgish: return "lim";
                    case LANGUAGE_ISO639.Lingala: return "lin";
                    case LANGUAGE_ISO639.Lao: return "lao";
                    case LANGUAGE_ISO639.Lithuanian: return "lit";
                    case LANGUAGE_ISO639.LubaKatanga: return "lub";
                    case LANGUAGE_ISO639.Latvian: return "lav";
                    case LANGUAGE_ISO639.Manx: return "glv";
                    case LANGUAGE_ISO639.Macedonian: return "mkd";
                    case LANGUAGE_ISO639.Malagasy: return "mlg";
                    case LANGUAGE_ISO639.Malay: return "msa";
                    case LANGUAGE_ISO639.Malayalam: return "mal";
                    case LANGUAGE_ISO639.Maltese: return "mlt";
                    case LANGUAGE_ISO639.Māori: return "mri";
                    case LANGUAGE_ISO639.Marathi: return "mar";
                    case LANGUAGE_ISO639.Marshallese: return "mah";
                    case LANGUAGE_ISO639.Mongolian: return "mon";
                    case LANGUAGE_ISO639.Nauru: return "nau";
                    case LANGUAGE_ISO639.Navajo: return "nav";
                    case LANGUAGE_ISO639.NorwegianBokmål: return "nob";
                    case LANGUAGE_ISO639.NorthNdebele: return "nde";
                    case LANGUAGE_ISO639.Nepali: return "nep";
                    case LANGUAGE_ISO639.Ndonga: return "ndo";
                    case LANGUAGE_ISO639.NorwegianNynorsk: return "nno";
                    case LANGUAGE_ISO639.Norwegian: return "nor";
                    case LANGUAGE_ISO639.Nuosu: return "iii";
                    case LANGUAGE_ISO639.SouthNdebele: return "nbl";
                    case LANGUAGE_ISO639.Occitan: return "oci";
                    case LANGUAGE_ISO639.Ojibwe: return "oji";
                    case LANGUAGE_ISO639.OldChurchSlavonic: return "chu";
                    case LANGUAGE_ISO639.Oromo: return "orm";
                    case LANGUAGE_ISO639.Oriya: return "ori";
                    case LANGUAGE_ISO639.Ossetian: return "oss";
                    case LANGUAGE_ISO639.Panjabi: return "pan";
                    case LANGUAGE_ISO639.Pāli: return "pli";
                    case LANGUAGE_ISO639.Persian: return "fas";
                    case LANGUAGE_ISO639.Polish: return "pol";
                    case LANGUAGE_ISO639.Pashto: return "pus";
                    case LANGUAGE_ISO639.Portuguese: return "por";
                    case LANGUAGE_ISO639.Quechua: return "que";
                    case LANGUAGE_ISO639.Romansh: return "roh";
                    case LANGUAGE_ISO639.Kirundi: return "run";
                    case LANGUAGE_ISO639.Romanian: return "ron";
                    case LANGUAGE_ISO639.Russian: return "rus";
                    case LANGUAGE_ISO639.Sanskrit: return "san";
                    case LANGUAGE_ISO639.Sardinian: return "srd";
                    case LANGUAGE_ISO639.Sindhi: return "snd";
                    case LANGUAGE_ISO639.NorthernSami: return "sme";
                    case LANGUAGE_ISO639.Samoan: return "smo";
                    case LANGUAGE_ISO639.Sango: return "sag";
                    case LANGUAGE_ISO639.Serbian: return "srp";
                    case LANGUAGE_ISO639.ScottishGaelic: return "gla";
                    case LANGUAGE_ISO639.Shona: return "sna";
                    case LANGUAGE_ISO639.Sinhala: return "sin";
                    case LANGUAGE_ISO639.Slovak: return "slk";
                    case LANGUAGE_ISO639.Slovene: return "slv";
                    case LANGUAGE_ISO639.Somali: return "som";
                    case LANGUAGE_ISO639.SouthernSotho: return "sot";
                    case LANGUAGE_ISO639.Spanish: return "spa";
                    case LANGUAGE_ISO639.Sundanese: return "sun";
                    case LANGUAGE_ISO639.Swahili: return "swa";
                    case LANGUAGE_ISO639.Swati: return "ssw";
                    case LANGUAGE_ISO639.Swedish: return "swe";
                    case LANGUAGE_ISO639.Tamil: return "tam";
                    case LANGUAGE_ISO639.Telugu: return "tel";
                    case LANGUAGE_ISO639.Tajik: return "tgk";
                    case LANGUAGE_ISO639.Thai: return "tha";
                    case LANGUAGE_ISO639.Tigrinya: return "tir";
                    case LANGUAGE_ISO639.TibetanStandard: return "bod";
                    case LANGUAGE_ISO639.Turkmen: return "tuk";
                    case LANGUAGE_ISO639.Tagalog: return "tgl";
                    case LANGUAGE_ISO639.Tswana: return "tsn";
                    case LANGUAGE_ISO639.Tonga: return "ton";
                    case LANGUAGE_ISO639.Turkish: return "tur";
                    case LANGUAGE_ISO639.Tsonga: return "tso";
                    case LANGUAGE_ISO639.Tatar: return "tat";
                    case LANGUAGE_ISO639.Twi: return "twi";
                    case LANGUAGE_ISO639.Tahitian: return "tah";
                    case LANGUAGE_ISO639.Uighur: return "uig";
                    case LANGUAGE_ISO639.Ukrainian: return "ukr";
                    case LANGUAGE_ISO639.Urdu: return "urd";
                    case LANGUAGE_ISO639.Uzbek: return "uzb";
                    case LANGUAGE_ISO639.Venda: return "ven";
                    case LANGUAGE_ISO639.Vietnamese: return "vie";
                    case LANGUAGE_ISO639.Volapük: return "vol";
                    case LANGUAGE_ISO639.Walloon: return "wln";
                    case LANGUAGE_ISO639.Welsh: return "cym";
                    case LANGUAGE_ISO639.Wolof: return "wol";
                    case LANGUAGE_ISO639.WesternFrisian: return "fry";
                    case LANGUAGE_ISO639.Xhosa: return "xho";
                    case LANGUAGE_ISO639.Yiddish: return "yid";
                    case LANGUAGE_ISO639.Yoruba: return "yor";
                    case LANGUAGE_ISO639.Zhuang: return "zha";
                    case LANGUAGE_ISO639.Zulu: return "zul";

                }
                return "";
            }
            public static LANGUAGE_ISO639? LANGUAGE_ISO639FromCode(string code, bool throwErrorIfCodeNotValid =true)
            {
                LANGUAGE_ISO639? result = null;
                code = code ?? "";
                if (code.Length == 2)
                    result = LANGUAGE_ISO639From2letter_Code(code, throwErrorIfCodeNotValid);
                else if (code.Length == 3)
                    result = LANGUAGE_ISO639From3letter_Code(code, throwErrorIfCodeNotValid);
                else
                {
                    if (throwErrorIfCodeNotValid)
                        throw new InvalidOperationException("Code not valid.  It must either a 2-letter or 3-letter code");
                }
                return result;
            }
            
            private static LANGUAGE_ISO639? LANGUAGE_ISO639From3letter_Code(string code, bool throwErrorIfCodeNotValid = true)
            {
                if (!string.IsNullOrEmpty(code) && code.Length != 3)
                    throw new InvalidOperationException("Code must be a 3-letter word");
                if (code == null)
                    code = "";
                code = code.ToLower();
                switch (code)
                {
                    case "abk": return LANGUAGE_ISO639.Abkhaz;
                    case "aar": return LANGUAGE_ISO639.Afar;
                    case "afr": return LANGUAGE_ISO639.Afrikaans;
                    case "aka": return LANGUAGE_ISO639.Akan;
                    case "sqi": return LANGUAGE_ISO639.Albanian;
                    case "amh": return LANGUAGE_ISO639.Amharic;
                    case "ara": return LANGUAGE_ISO639.Arabic;
                    case "arg": return LANGUAGE_ISO639.Aragonese;
                    case "hye": return LANGUAGE_ISO639.Armenian;
                    case "asm": return LANGUAGE_ISO639.Assamese;
                    case "ava": return LANGUAGE_ISO639.Avaric;
                    case "ave": return LANGUAGE_ISO639.Avestan;
                    case "aym": return LANGUAGE_ISO639.Aymara;
                    case "aze": return LANGUAGE_ISO639.Azerbaijani;
                    case "bam": return LANGUAGE_ISO639.Bambara;
                    case "bak": return LANGUAGE_ISO639.Bashkir;
                    case "eus": return LANGUAGE_ISO639.Basque;
                    case "bel": return LANGUAGE_ISO639.Belarusian;
                    case "ben": return LANGUAGE_ISO639.Bengali;
                    case "bih": return LANGUAGE_ISO639.Bihari;
                    case "bis": return LANGUAGE_ISO639.Bislama;
                    case "bos": return LANGUAGE_ISO639.Bosnian;
                    case "bre": return LANGUAGE_ISO639.Breton;
                    case "bul": return LANGUAGE_ISO639.Bulgarian;
                    case "mya": return LANGUAGE_ISO639.Burmese;
                    case "cat": return LANGUAGE_ISO639.Catalan;
                    case "cha": return LANGUAGE_ISO639.Chamorro;
                    case "che": return LANGUAGE_ISO639.Chechen;
                    case "nya": return LANGUAGE_ISO639.Chichewa;
                    case "zho": return LANGUAGE_ISO639.Chinese;
                    case "chv": return LANGUAGE_ISO639.Chuvash;
                    case "cor": return LANGUAGE_ISO639.Cornish;
                    case "cos": return LANGUAGE_ISO639.Corsican;
                    case "cre": return LANGUAGE_ISO639.Cree;
                    case "hrv": return LANGUAGE_ISO639.Croatian;
                    case "ces": return LANGUAGE_ISO639.Czech;
                    case "dan": return LANGUAGE_ISO639.Danish;
                    case "div": return LANGUAGE_ISO639.Divehi;
                    case "nld": return LANGUAGE_ISO639.Dutch;
                    case "dzo": return LANGUAGE_ISO639.Dzongkha;
                    case "eng": return LANGUAGE_ISO639.English;
                    case "epo": return LANGUAGE_ISO639.Esperanto;
                    case "est": return LANGUAGE_ISO639.Estonian;
                    case "ewe": return LANGUAGE_ISO639.Ewe;
                    case "fao": return LANGUAGE_ISO639.Faroese;
                    case "fij": return LANGUAGE_ISO639.Fijian;
                    case "fin": return LANGUAGE_ISO639.Finnish;
                    case "fra": return LANGUAGE_ISO639.French;
                    case "ful": return LANGUAGE_ISO639.Fula;
                    case "glg": return LANGUAGE_ISO639.Galician;
                    case "kat": return LANGUAGE_ISO639.Georgian;
                    case "deu": return LANGUAGE_ISO639.German;
                    case "ell": return LANGUAGE_ISO639.Greek;
                    case "grn": return LANGUAGE_ISO639.Guaraní;
                    case "guj": return LANGUAGE_ISO639.Gujarati;
                    case "hat": return LANGUAGE_ISO639.Haitian;
                    case "hau": return LANGUAGE_ISO639.Hausa;
                    case "heb": return LANGUAGE_ISO639.Hebrew;
                    case "her": return LANGUAGE_ISO639.Herero;
                    case "hin": return LANGUAGE_ISO639.Hindi;
                    case "hmo": return LANGUAGE_ISO639.HiriMotu;
                    case "hun": return LANGUAGE_ISO639.Hungarian;
                    case "ina": return LANGUAGE_ISO639.Interlingua;
                    case "ind": return LANGUAGE_ISO639.Indonesian;
                    case "ile": return LANGUAGE_ISO639.Interlingue;
                    case "gle": return LANGUAGE_ISO639.Irish;
                    case "ibo": return LANGUAGE_ISO639.Igbo;
                    case "ipk": return LANGUAGE_ISO639.Inupiaq;
                    case "ido": return LANGUAGE_ISO639.Ido;
                    case "isl": return LANGUAGE_ISO639.Icelandic;
                    case "ita": return LANGUAGE_ISO639.Italian;
                    case "iku": return LANGUAGE_ISO639.Inuktitut;
                    case "jpn": return LANGUAGE_ISO639.Japanese;
                    case "jav": return LANGUAGE_ISO639.Javanese;
                    case "kal": return LANGUAGE_ISO639.Kalaallisut;
                    case "kan": return LANGUAGE_ISO639.Kannada;
                    case "kau": return LANGUAGE_ISO639.Kanuri;
                    case "kas": return LANGUAGE_ISO639.Kashmiri;
                    case "kaz": return LANGUAGE_ISO639.Kazakh;
                    case "khm": return LANGUAGE_ISO639.Khmer;
                    case "kik": return LANGUAGE_ISO639.Kikuyu;
                    case "kin": return LANGUAGE_ISO639.Kinyarwanda;
                    case "kir": return LANGUAGE_ISO639.Kirghiz;
                    case "kom": return LANGUAGE_ISO639.Komi;
                    case "kon": return LANGUAGE_ISO639.Kongo;
                    case "kor": return LANGUAGE_ISO639.Korean;
                    case "kur": return LANGUAGE_ISO639.Kurdish;
                    case "kua": return LANGUAGE_ISO639.Kwanyama;
                    case "lat": return LANGUAGE_ISO639.Latin;
                    case "ltz": return LANGUAGE_ISO639.Luxembourgish;
                    case "lug": return LANGUAGE_ISO639.Luganda;
                    case "lim": return LANGUAGE_ISO639.Limburgish;
                    case "lin": return LANGUAGE_ISO639.Lingala;
                    case "lao": return LANGUAGE_ISO639.Lao;
                    case "lit": return LANGUAGE_ISO639.Lithuanian;
                    case "lub": return LANGUAGE_ISO639.LubaKatanga;
                    case "lav": return LANGUAGE_ISO639.Latvian;
                    case "glv": return LANGUAGE_ISO639.Manx;
                    case "mkd": return LANGUAGE_ISO639.Macedonian;
                    case "mlg": return LANGUAGE_ISO639.Malagasy;
                    case "msa": return LANGUAGE_ISO639.Malay;
                    case "mal": return LANGUAGE_ISO639.Malayalam;
                    case "mlt": return LANGUAGE_ISO639.Maltese;
                    case "mri": return LANGUAGE_ISO639.Māori;
                    case "mar": return LANGUAGE_ISO639.Marathi;
                    case "mah": return LANGUAGE_ISO639.Marshallese;
                    case "mon": return LANGUAGE_ISO639.Mongolian;
                    case "nau": return LANGUAGE_ISO639.Nauru;
                    case "nav": return LANGUAGE_ISO639.Navajo;
                    case "nob": return LANGUAGE_ISO639.NorwegianBokmål;
                    case "nde": return LANGUAGE_ISO639.NorthNdebele;
                    case "nep": return LANGUAGE_ISO639.Nepali;
                    case "ndo": return LANGUAGE_ISO639.Ndonga;
                    case "nno": return LANGUAGE_ISO639.NorwegianNynorsk;
                    case "nor": return LANGUAGE_ISO639.Norwegian;
                    case "iii": return LANGUAGE_ISO639.Nuosu;
                    case "nbl": return LANGUAGE_ISO639.SouthNdebele;
                    case "oci": return LANGUAGE_ISO639.Occitan;
                    case "oji": return LANGUAGE_ISO639.Ojibwe;
                    case "chu": return LANGUAGE_ISO639.OldChurchSlavonic;
                    case "orm": return LANGUAGE_ISO639.Oromo;
                    case "ori": return LANGUAGE_ISO639.Oriya;
                    case "oss": return LANGUAGE_ISO639.Ossetian;
                    case "pan": return LANGUAGE_ISO639.Panjabi;
                    case "pli": return LANGUAGE_ISO639.Pāli;
                    case "fas": return LANGUAGE_ISO639.Persian;
                    case "pol": return LANGUAGE_ISO639.Polish;
                    case "pus": return LANGUAGE_ISO639.Pashto;
                    case "por": return LANGUAGE_ISO639.Portuguese;
                    case "que": return LANGUAGE_ISO639.Quechua;
                    case "roh": return LANGUAGE_ISO639.Romansh;
                    case "run": return LANGUAGE_ISO639.Kirundi;
                    case "ron": return LANGUAGE_ISO639.Romanian;
                    case "rus": return LANGUAGE_ISO639.Russian;
                    case "san": return LANGUAGE_ISO639.Sanskrit;
                    case "srd": return LANGUAGE_ISO639.Sardinian;
                    case "snd": return LANGUAGE_ISO639.Sindhi;
                    case "sme": return LANGUAGE_ISO639.NorthernSami;
                    case "smo": return LANGUAGE_ISO639.Samoan;
                    case "sag": return LANGUAGE_ISO639.Sango;
                    case "srp": return LANGUAGE_ISO639.Serbian;
                    case "gla": return LANGUAGE_ISO639.ScottishGaelic;
                    case "sna": return LANGUAGE_ISO639.Shona;
                    case "sin": return LANGUAGE_ISO639.Sinhala;
                    case "slk": return LANGUAGE_ISO639.Slovak;
                    case "slv": return LANGUAGE_ISO639.Slovene;
                    case "som": return LANGUAGE_ISO639.Somali;
                    case "sot": return LANGUAGE_ISO639.SouthernSotho;
                    case "spa": return LANGUAGE_ISO639.Spanish;
                    case "sun": return LANGUAGE_ISO639.Sundanese;
                    case "swa": return LANGUAGE_ISO639.Swahili;
                    case "ssw": return LANGUAGE_ISO639.Swati;
                    case "swe": return LANGUAGE_ISO639.Swedish;
                    case "tam": return LANGUAGE_ISO639.Tamil;
                    case "tel": return LANGUAGE_ISO639.Telugu;
                    case "tgk": return LANGUAGE_ISO639.Tajik;
                    case "tha": return LANGUAGE_ISO639.Thai;
                    case "tir": return LANGUAGE_ISO639.Tigrinya;
                    case "bod": return LANGUAGE_ISO639.TibetanStandard;
                    case "tuk": return LANGUAGE_ISO639.Turkmen;
                    case "tgl": return LANGUAGE_ISO639.Tagalog;
                    case "tsn": return LANGUAGE_ISO639.Tswana;
                    case "ton": return LANGUAGE_ISO639.Tonga;
                    case "tur": return LANGUAGE_ISO639.Turkish;
                    case "tso": return LANGUAGE_ISO639.Tsonga;
                    case "tat": return LANGUAGE_ISO639.Tatar;
                    case "twi": return LANGUAGE_ISO639.Twi;
                    case "tah": return LANGUAGE_ISO639.Tahitian;
                    case "uig": return LANGUAGE_ISO639.Uighur;
                    case "ukr": return LANGUAGE_ISO639.Ukrainian;
                    case "urd": return LANGUAGE_ISO639.Urdu;
                    case "uzb": return LANGUAGE_ISO639.Uzbek;
                    case "ven": return LANGUAGE_ISO639.Venda;
                    case "vie": return LANGUAGE_ISO639.Vietnamese;
                    case "vol": return LANGUAGE_ISO639.Volapük;
                    case "wln": return LANGUAGE_ISO639.Walloon;
                    case "cym": return LANGUAGE_ISO639.Welsh;
                    case "wol": return LANGUAGE_ISO639.Wolof;
                    case "fry": return LANGUAGE_ISO639.WesternFrisian;
                    case "xho": return LANGUAGE_ISO639.Xhosa;
                    case "yid": return LANGUAGE_ISO639.Yiddish;
                    case "yor": return LANGUAGE_ISO639.Yoruba;
                    case "zha": return LANGUAGE_ISO639.Zhuang;
                    case "zul": return LANGUAGE_ISO639.Zulu;


                }
                if (throwErrorIfCodeNotValid)
                    throw new InvalidOperationException("invalid code!");
                else
                    return null;
            }

            private static LANGUAGE_ISO639? LANGUAGE_ISO639From2letter_Code(string code, bool throwErrorIfCodeNotValid = true)
            {
                if (!string.IsNullOrEmpty(code) && code.Length != 2)
                    throw new InvalidOperationException("Code must be a 2-letter word");
                if (code == null)
                    code = "";
                code = code.ToLower();
                switch (code)
                {
                    case "ab": return LANGUAGE_ISO639.Abkhaz;
                    case "aa": return LANGUAGE_ISO639.Afar;
                    case "af": return LANGUAGE_ISO639.Afrikaans;
                    case "ak": return LANGUAGE_ISO639.Akan;
                    case "sq": return LANGUAGE_ISO639.Albanian;
                    case "am": return LANGUAGE_ISO639.Amharic;
                    case "ar": return LANGUAGE_ISO639.Arabic;
                    case "an": return LANGUAGE_ISO639.Aragonese;
                    case "hy": return LANGUAGE_ISO639.Armenian;
                    case "as": return LANGUAGE_ISO639.Assamese;
                    case "av": return LANGUAGE_ISO639.Avaric;
                    case "ae": return LANGUAGE_ISO639.Avestan;
                    case "ay": return LANGUAGE_ISO639.Aymara;
                    case "az": return LANGUAGE_ISO639.Azerbaijani;
                    case "bm": return LANGUAGE_ISO639.Bambara;
                    case "ba": return LANGUAGE_ISO639.Bashkir;
                    case "eu": return LANGUAGE_ISO639.Basque;
                    case "be": return LANGUAGE_ISO639.Belarusian;
                    case "bn": return LANGUAGE_ISO639.Bengali;
                    case "bh": return LANGUAGE_ISO639.Bihari;
                    case "bi": return LANGUAGE_ISO639.Bislama;
                    case "bs": return LANGUAGE_ISO639.Bosnian;
                    case "br": return LANGUAGE_ISO639.Breton;
                    case "bg": return LANGUAGE_ISO639.Bulgarian;
                    case "my": return LANGUAGE_ISO639.Burmese;
                    case "ca": return LANGUAGE_ISO639.Catalan;
                    case "ch": return LANGUAGE_ISO639.Chamorro;
                    case "ce": return LANGUAGE_ISO639.Chechen;
                    case "ny": return LANGUAGE_ISO639.Chichewa;
                    case "sp": return LANGUAGE_ISO639.Chinese;
                    case "cv": return LANGUAGE_ISO639.Chuvash;
                    case "kw": return LANGUAGE_ISO639.Cornish;
                    case "co": return LANGUAGE_ISO639.Corsican;
                    case "cr": return LANGUAGE_ISO639.Cree;
                    case "hr": return LANGUAGE_ISO639.Croatian;
                    case "cs": return LANGUAGE_ISO639.Czech;
                    case "da": return LANGUAGE_ISO639.Danish;
                    case "dv": return LANGUAGE_ISO639.Divehi;
                    case "nl": return LANGUAGE_ISO639.Dutch;
                    case "dz": return LANGUAGE_ISO639.Dzongkha;
                    case "en": return LANGUAGE_ISO639.English;
                    case "eo": return LANGUAGE_ISO639.Esperanto;
                    case "et": return LANGUAGE_ISO639.Estonian;
                    case "ee": return LANGUAGE_ISO639.Ewe;
                    case "fo": return LANGUAGE_ISO639.Faroese;
                    case "fj": return LANGUAGE_ISO639.Fijian;
                    case "fi": return LANGUAGE_ISO639.Finnish;
                    case "fr": return LANGUAGE_ISO639.French;
                    case "ff": return LANGUAGE_ISO639.Fula;
                    case "gl": return LANGUAGE_ISO639.Galician;
                    case "ka": return LANGUAGE_ISO639.Georgian;
                    case "de": return LANGUAGE_ISO639.German;
                    case "el": return LANGUAGE_ISO639.Greek;
                    case "gn": return LANGUAGE_ISO639.Guaraní;
                    case "gu": return LANGUAGE_ISO639.Gujarati;
                    case "ht": return LANGUAGE_ISO639.Haitian;
                    case "ha": return LANGUAGE_ISO639.Hausa;
                    case "he": return LANGUAGE_ISO639.Hebrew;
                    case "hz": return LANGUAGE_ISO639.Herero;
                    case "hi": return LANGUAGE_ISO639.Hindi;
                    case "ho": return LANGUAGE_ISO639.HiriMotu;
                    case "hu": return LANGUAGE_ISO639.Hungarian;
                    case "ia": return LANGUAGE_ISO639.Interlingua;
                    case "id": return LANGUAGE_ISO639.Indonesian;
                    case "ie": return LANGUAGE_ISO639.Interlingue;
                    case "ga": return LANGUAGE_ISO639.Irish;
                    case "ig": return LANGUAGE_ISO639.Igbo;
                    case "ik": return LANGUAGE_ISO639.Inupiaq;
                    case "io": return LANGUAGE_ISO639.Ido;
                    case "is": return LANGUAGE_ISO639.Icelandic;
                    case "it": return LANGUAGE_ISO639.Italian;
                    case "iu": return LANGUAGE_ISO639.Inuktitut;
                    case "ja": return LANGUAGE_ISO639.Japanese;
                    case "jv":
                    case "jw":
                        return LANGUAGE_ISO639.Javanese;
                    case "kl": return LANGUAGE_ISO639.Kalaallisut;
                    case "kn": return LANGUAGE_ISO639.Kannada;
                    case "kr": return LANGUAGE_ISO639.Kanuri;
                    case "ks": return LANGUAGE_ISO639.Kashmiri;
                    case "kk": return LANGUAGE_ISO639.Kazakh;
                    case "km": return LANGUAGE_ISO639.Khmer;
                    case "ki": return LANGUAGE_ISO639.Kikuyu;
                    case "rw": return LANGUAGE_ISO639.Kinyarwanda;
                    case "ky": return LANGUAGE_ISO639.Kirghiz;
                    case "kv": return LANGUAGE_ISO639.Komi;
                    case "kg": return LANGUAGE_ISO639.Kongo;
                    case "ko": return LANGUAGE_ISO639.Korean;
                    case "ku": return LANGUAGE_ISO639.Kurdish;
                    case "kj": return LANGUAGE_ISO639.Kwanyama;
                    case "la": return LANGUAGE_ISO639.Latin;
                    case "lb": return LANGUAGE_ISO639.Luxembourgish;
                    case "lg": return LANGUAGE_ISO639.Luganda;
                    case "li": return LANGUAGE_ISO639.Limburgish;
                    case "ln": return LANGUAGE_ISO639.Lingala;
                    case "lo": return LANGUAGE_ISO639.Lao;
                    case "lt": return LANGUAGE_ISO639.Lithuanian;
                    case "td": return LANGUAGE_ISO639.LubaKatanga;
                    case "lv": return LANGUAGE_ISO639.Latvian;
                    case "gv": return LANGUAGE_ISO639.Manx;
                    case "mk": return LANGUAGE_ISO639.Macedonian;
                    case "mg": return LANGUAGE_ISO639.Malagasy;
                    case "ms": return LANGUAGE_ISO639.Malay;
                    case "ml": return LANGUAGE_ISO639.Malayalam;
                    case "mt": return LANGUAGE_ISO639.Maltese;
                    case "mi": return LANGUAGE_ISO639.Māori;
                    case "mr": return LANGUAGE_ISO639.Marathi;
                    case "mh": return LANGUAGE_ISO639.Marshallese;
                    case "mn": return LANGUAGE_ISO639.Mongolian;
                    case "na": return LANGUAGE_ISO639.Nauru;
                    case "nv": return LANGUAGE_ISO639.Navajo;
                    case "nb": return LANGUAGE_ISO639.NorwegianBokmål;
                    case "nd": return LANGUAGE_ISO639.NorthNdebele;
                    case "ne": return LANGUAGE_ISO639.Nepali;
                    case "ng": return LANGUAGE_ISO639.Ndonga;
                    case "nn": return LANGUAGE_ISO639.NorwegianNynorsk;
                    case "no": return LANGUAGE_ISO639.Norwegian;
                    case "ii": return LANGUAGE_ISO639.Nuosu;
                    case "nr": return LANGUAGE_ISO639.SouthNdebele;
                    case "oc": return LANGUAGE_ISO639.Occitan;
                    case "oj": return LANGUAGE_ISO639.Ojibwe;
                    case "cu": return LANGUAGE_ISO639.OldChurchSlavonic;
                    case "om": return LANGUAGE_ISO639.Oromo;
                    case "or": return LANGUAGE_ISO639.Oriya;
                    case "os": return LANGUAGE_ISO639.Ossetian;
                    case "pa": return LANGUAGE_ISO639.Panjabi;
                    case "pi": return LANGUAGE_ISO639.Pāli;
                    case "fa": return LANGUAGE_ISO639.Persian;
                    case "pl": return LANGUAGE_ISO639.Polish;
                    case "ps": return LANGUAGE_ISO639.Pashto;
                    case "pt": return LANGUAGE_ISO639.Portuguese;
                    case "qu": return LANGUAGE_ISO639.Quechua;
                    case "rm": return LANGUAGE_ISO639.Romansh;
                    case "rn": return LANGUAGE_ISO639.Kirundi;
                    case "ro":
                    case "mo":
                        return LANGUAGE_ISO639.Romanian;
                    case "ru": return LANGUAGE_ISO639.Russian;
                    case "sa": return LANGUAGE_ISO639.Sanskrit;
                    case "sc": return LANGUAGE_ISO639.Sardinian;
                    case "sd": return LANGUAGE_ISO639.Sindhi;
                    case "se": return LANGUAGE_ISO639.NorthernSami;
                    case "sm": return LANGUAGE_ISO639.Samoan;
                    case "sg": return LANGUAGE_ISO639.Sango;
                    case "sr": return LANGUAGE_ISO639.Serbian;
                    case "gd": return LANGUAGE_ISO639.ScottishGaelic;
                    case "sn": return LANGUAGE_ISO639.Shona;
                    case "si": return LANGUAGE_ISO639.Sinhala;
                    case "sk": return LANGUAGE_ISO639.Slovak;
                    case "sl": return LANGUAGE_ISO639.Slovene;
                    case "so": return LANGUAGE_ISO639.Somali;
                    case "st": return LANGUAGE_ISO639.SouthernSotho;
                    case "es": return LANGUAGE_ISO639.Spanish;
                    case "su": return LANGUAGE_ISO639.Sundanese;
                    case "sw": return LANGUAGE_ISO639.Swahili;
                    case "ss": return LANGUAGE_ISO639.Swati;
                    case "sv": return LANGUAGE_ISO639.Swedish;
                    case "ta": return LANGUAGE_ISO639.Tamil;
                    case "te": return LANGUAGE_ISO639.Telugu;
                    case "tg": return LANGUAGE_ISO639.Tajik;
                    case "th": return LANGUAGE_ISO639.Thai;
                    case "ti": return LANGUAGE_ISO639.Tigrinya;
                    case "bo": return LANGUAGE_ISO639.TibetanStandard;
                    case "tk": return LANGUAGE_ISO639.Turkmen;
                    case "tl": return LANGUAGE_ISO639.Tagalog;
                    case "tn": return LANGUAGE_ISO639.Tswana;
                    case "to": return LANGUAGE_ISO639.Tonga;
                    case "tr": return LANGUAGE_ISO639.Turkish;
                    case "ts": return LANGUAGE_ISO639.Tsonga;
                    case "tt": return LANGUAGE_ISO639.Tatar;
                    case "tw": return LANGUAGE_ISO639.Twi;
                    case "ty": return LANGUAGE_ISO639.Tahitian;
                    case "ug": return LANGUAGE_ISO639.Uighur;
                    case "uk": return LANGUAGE_ISO639.Ukrainian;
                    case "ur": return LANGUAGE_ISO639.Urdu;
                    case "uz": return LANGUAGE_ISO639.Uzbek;
                    case "ve": return LANGUAGE_ISO639.Venda;
                    case "vi": return LANGUAGE_ISO639.Vietnamese;
                    case "vo": return LANGUAGE_ISO639.Volapük;
                    case "wa": return LANGUAGE_ISO639.Walloon;
                    case "cy": return LANGUAGE_ISO639.Welsh;
                    case "wo": return LANGUAGE_ISO639.Wolof;
                    case "fy": return LANGUAGE_ISO639.WesternFrisian;
                    case "xh": return LANGUAGE_ISO639.Xhosa;
                    case "yi": return LANGUAGE_ISO639.Yiddish;
                    case "yo": return LANGUAGE_ISO639.Yoruba;
                    case "za": return LANGUAGE_ISO639.Zhuang;
                    case "zu": return LANGUAGE_ISO639.Zulu;
                    case "zh": return LANGUAGE_ISO639.ChineseSimplified;



                }
                if (throwErrorIfCodeNotValid)
                    throw new InvalidOperationException("invalid code!");
                else
                    return null;
            }
        }
    }
}
