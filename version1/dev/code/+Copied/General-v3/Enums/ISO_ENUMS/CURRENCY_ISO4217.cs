﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace CS.General_v3
{
    public static partial class Enums
    {
        public static partial class ISO_ENUMS
        {
            public enum CURRENCY_ISO4217
            {
                UnitedArabEmiratesDirhams, AfghanistanAfghanis, AlbaniaLeke, ArmeniaDrams, NetherlandsAntillesGuildersAlsoCalledFlorins, AngolaKwanza, ArgentinaPesos, AustraliaDollars, ArubaGuildersAlsoCalledFlorins, AzerbaijanNewManats, BosniaAndHerzegovinaConvertibleMarka, BarbadosDollars, BangladeshTaka, BulgariaLeva, BahrainDinars, BurundiFrancs, BermudaDollars, BruneiDarussalamDollars, BoliviaBolivianos, BrazilReais, BahamasDollars, BhutanNgultrum, BotswanaPulas, BelarusRubles, BelizeDollars, CanadaDollars, CongoKinshasaCongoleseFrancs, SwitzerlandFrancs, ChilePesos, ChinaYuanRenminbi, ColombiaPesos, CostaRicaColones, CubaPesos, CapeVerdeEscudos, CyprusPoundsExpiresJan, CzechRepublicKoruny, DjiboutiFrancs, DenmarkKroner, DominicanRepublicPesos, AlgeriaAlgeriaDinars, EstoniaKrooni, EgyptPounds, EritreaNakfa, EthiopiaBirr, EuroMemberCountriesEuro, FijiDollars, FalklandIslandsMalvinasPounds, UnitedKingdomPounds, GeorgiaLari, GuernseyPounds, GhanaCedis, GibraltarPounds, GambiaDalasi, GuineaFrancs, GuatemalaQuetzales, GuyanaDollars, HongKongDollars, HondurasLempiras, CroatiaKuna, HaitiGourdes, HungaryForint, IndonesiaRupiahs, IsraelNewShekels, IsleOfManPounds, IndiaRupees, IraqDinars, IranRials, IcelandKronur, JerseyPounds, JamaicaDollars, JordanDinars, JapanYen, KenyaShillings, KyrgyzstanSoms, CambodiaRiels, ComorosFrancs, KoreaNorthWon, KoreaSouthWon, KuwaitDinars, CaymanIslandsDollars, KazakhstanTenge, LaosKips, LebanonPounds, SriLankaRupees, LiberiaDollars, LesothoMaloti, LithuaniaLitai, LatviaLati, LibyaDinars, MoroccoDirhams, MoldovaLei, MadagascarAriary, MacedoniaDenars, MyanmarBurmaKyats, MongoliaTugriks, MacauPatacas, MauritaniaOuguiyas, MaltaLiriExpiresJan, MauritiusRupees, MaldivesMaldiveIslandsRufiyaa, MalawiKwachas, MexicoPesos, MalaysiaRinggits, MozambiqueMeticais, NamibiaDollars, NigeriaNairas, NicaraguaCordobas, NorwayKrone, NepalRupees, NewZealandDollars, OmanRials, PanamaBalboa, PeruNuevosSoles, PapuaNewGuineaKina, PhilippinesPesos, PakistanRupees, PolandZlotych, ParaguayGuarani, QatarRials, RomaniaNewLei, SerbiaDinars, RussiaRubles, RwandaRwandaFrancs, SaudiArabiaRiyals, SolomonIslandsDollars, SeychellesRupees, SudanPounds, SwedenKronor, SingaporeDollars, SaintHelenaPounds, SlovakiaKoruny, SierraLeoneLeones, SomaliaShillings, SeborgaLuigini, SurinameDollars, SOTomeAndPrincipeDobras, ElSalvadorColones, SyriaPounds, SwazilandEmalangeni, ThailandBaht, TajikistanSomoni, TurkmenistanManats, TunisiaDinars, TongaPaAnga, TurkeyLiras, TrinidadAndTobagoDollars, TuvaluDollars, TaiwanNewDollars, TanzaniaShillings, UkraineHryvnia, UgandaShillings, UnitedStatesOfAmericaDollars, UruguayPesos, UzbekistanSums, VenezuelaBolivaresExpiresJun, VenezuelaBolivaresFuertes, VietNamDong, VanuatuVatu, SamoaTala, CommunautFinanciReAfricaineBeacFrancs, SilverOunces, GoldOunces, EastCaribbeanDollars, InternationalMonetaryFundImfSpecialDrawingRights, CommunautFinanciReAfricaineBceaoFrancs, PalladiumOunces, ComptoirsFranAisDuPacifiqueFrancs, PlatinumOunces, YemenRials, SouthAfricaRand, ZambiaKwacha, ZimbabweZimbabweDollars
            }
            public static string Currency_ISO4217ToCode(Enum code)
            {
                if (code is CURRENCY_ISO4217)
                {
                    return Currency_ISO4217ToCode((CURRENCY_ISO4217)code);
                }
                else
                {
                    throw new InvalidOperationException("General.Enums:: code must be of type CURRENCY_ISO4217");
                }

            }
            public static string Currency_ISO4217ToCode(CURRENCY_ISO4217 code)
            {
                switch (code)
                {
                    case CURRENCY_ISO4217.UnitedArabEmiratesDirhams: return "AED";
                    case CURRENCY_ISO4217.AfghanistanAfghanis: return "AFN";
                    case CURRENCY_ISO4217.AlbaniaLeke: return "ALL";
                    case CURRENCY_ISO4217.ArmeniaDrams: return "AMD";
                    case CURRENCY_ISO4217.NetherlandsAntillesGuildersAlsoCalledFlorins: return "ANG";
                    case CURRENCY_ISO4217.AngolaKwanza: return "AOA";
                    case CURRENCY_ISO4217.ArgentinaPesos: return "ARS";
                    case CURRENCY_ISO4217.AustraliaDollars: return "AUD";
                    case CURRENCY_ISO4217.ArubaGuildersAlsoCalledFlorins: return "AWG";
                    case CURRENCY_ISO4217.AzerbaijanNewManats: return "AZN";
                    case CURRENCY_ISO4217.BosniaAndHerzegovinaConvertibleMarka: return "BAM";
                    case CURRENCY_ISO4217.BarbadosDollars: return "BBD";
                    case CURRENCY_ISO4217.BangladeshTaka: return "BDT";
                    case CURRENCY_ISO4217.BulgariaLeva: return "BGN";
                    case CURRENCY_ISO4217.BahrainDinars: return "BHD";
                    case CURRENCY_ISO4217.BurundiFrancs: return "BIF";
                    case CURRENCY_ISO4217.BermudaDollars: return "BMD";
                    case CURRENCY_ISO4217.BruneiDarussalamDollars: return "BND";
                    case CURRENCY_ISO4217.BoliviaBolivianos: return "BOB";
                    case CURRENCY_ISO4217.BrazilReais: return "BRL";
                    case CURRENCY_ISO4217.BahamasDollars: return "BSD";
                    case CURRENCY_ISO4217.BhutanNgultrum: return "BTN";
                    case CURRENCY_ISO4217.BotswanaPulas: return "BWP";
                    case CURRENCY_ISO4217.BelarusRubles: return "BYR";
                    case CURRENCY_ISO4217.BelizeDollars: return "BZD";
                    case CURRENCY_ISO4217.CanadaDollars: return "CAD";
                    case CURRENCY_ISO4217.CongoKinshasaCongoleseFrancs: return "CDF";
                    case CURRENCY_ISO4217.SwitzerlandFrancs: return "CHF";
                    case CURRENCY_ISO4217.ChilePesos: return "CLP";
                    case CURRENCY_ISO4217.ChinaYuanRenminbi: return "CNY";
                    case CURRENCY_ISO4217.ColombiaPesos: return "COP";
                    case CURRENCY_ISO4217.CostaRicaColones: return "CRC";
                    case CURRENCY_ISO4217.CubaPesos: return "CUP";
                    case CURRENCY_ISO4217.CapeVerdeEscudos: return "CVE";
                    case CURRENCY_ISO4217.CyprusPoundsExpiresJan: return "CYP";
                    case CURRENCY_ISO4217.CzechRepublicKoruny: return "CZK";
                    case CURRENCY_ISO4217.DjiboutiFrancs: return "DJF";
                    case CURRENCY_ISO4217.DenmarkKroner: return "DKK";
                    case CURRENCY_ISO4217.DominicanRepublicPesos: return "DOP";
                    case CURRENCY_ISO4217.AlgeriaAlgeriaDinars: return "DZD";
                    case CURRENCY_ISO4217.EstoniaKrooni: return "EEK";
                    case CURRENCY_ISO4217.EgyptPounds: return "EGP";
                    case CURRENCY_ISO4217.EritreaNakfa: return "ERN";
                    case CURRENCY_ISO4217.EthiopiaBirr: return "ETB";
                    case CURRENCY_ISO4217.EuroMemberCountriesEuro: return "EUR";
                    case CURRENCY_ISO4217.FijiDollars: return "FJD";
                    case CURRENCY_ISO4217.FalklandIslandsMalvinasPounds: return "FKP";
                    case CURRENCY_ISO4217.UnitedKingdomPounds: return "GBP";
                    case CURRENCY_ISO4217.GeorgiaLari: return "GEL";
                    case CURRENCY_ISO4217.GuernseyPounds: return "GGP";
                    case CURRENCY_ISO4217.GhanaCedis: return "GHS";
                    case CURRENCY_ISO4217.GibraltarPounds: return "GIP";
                    case CURRENCY_ISO4217.GambiaDalasi: return "GMD";
                    case CURRENCY_ISO4217.GuineaFrancs: return "GNF";
                    case CURRENCY_ISO4217.GuatemalaQuetzales: return "GTQ";
                    case CURRENCY_ISO4217.GuyanaDollars: return "GYD";
                    case CURRENCY_ISO4217.HongKongDollars: return "HKD";
                    case CURRENCY_ISO4217.HondurasLempiras: return "HNL";
                    case CURRENCY_ISO4217.CroatiaKuna: return "HRK";
                    case CURRENCY_ISO4217.HaitiGourdes: return "HTG";
                    case CURRENCY_ISO4217.HungaryForint: return "HUF";
                    case CURRENCY_ISO4217.IndonesiaRupiahs: return "IDR";
                    case CURRENCY_ISO4217.IsraelNewShekels: return "ILS";
                    case CURRENCY_ISO4217.IsleOfManPounds: return "IMP";
                    case CURRENCY_ISO4217.IndiaRupees: return "INR";
                    case CURRENCY_ISO4217.IraqDinars: return "IQD";
                    case CURRENCY_ISO4217.IranRials: return "IRR";
                    case CURRENCY_ISO4217.IcelandKronur: return "ISK";
                    case CURRENCY_ISO4217.JerseyPounds: return "JEP";
                    case CURRENCY_ISO4217.JamaicaDollars: return "JMD";
                    case CURRENCY_ISO4217.JordanDinars: return "JOD";
                    case CURRENCY_ISO4217.JapanYen: return "JPY";
                    case CURRENCY_ISO4217.KenyaShillings: return "KES";
                    case CURRENCY_ISO4217.KyrgyzstanSoms: return "KGS";
                    case CURRENCY_ISO4217.CambodiaRiels: return "KHR";
                    case CURRENCY_ISO4217.ComorosFrancs: return "KMF";
                    case CURRENCY_ISO4217.KoreaNorthWon: return "KPW";
                    case CURRENCY_ISO4217.KoreaSouthWon: return "KRW";
                    case CURRENCY_ISO4217.KuwaitDinars: return "KWD";
                    case CURRENCY_ISO4217.CaymanIslandsDollars: return "KYD";
                    case CURRENCY_ISO4217.KazakhstanTenge: return "KZT";
                    case CURRENCY_ISO4217.LaosKips: return "LAK";
                    case CURRENCY_ISO4217.LebanonPounds: return "LBP";
                    case CURRENCY_ISO4217.SriLankaRupees: return "LKR";
                    case CURRENCY_ISO4217.LiberiaDollars: return "LRD";
                    case CURRENCY_ISO4217.LesothoMaloti: return "LSL";
                    case CURRENCY_ISO4217.LithuaniaLitai: return "LTL";
                    case CURRENCY_ISO4217.LatviaLati: return "LVL";
                    case CURRENCY_ISO4217.LibyaDinars: return "LYD";
                    case CURRENCY_ISO4217.MoroccoDirhams: return "MAD";
                    case CURRENCY_ISO4217.MoldovaLei: return "MDL";
                    case CURRENCY_ISO4217.MadagascarAriary: return "MGA";
                    case CURRENCY_ISO4217.MacedoniaDenars: return "MKD";
                    case CURRENCY_ISO4217.MyanmarBurmaKyats: return "MMK";
                    case CURRENCY_ISO4217.MongoliaTugriks: return "MNT";
                    case CURRENCY_ISO4217.MacauPatacas: return "MOP";
                    case CURRENCY_ISO4217.MauritaniaOuguiyas: return "MRO";
                    case CURRENCY_ISO4217.MaltaLiriExpiresJan: return "MTL";
                    case CURRENCY_ISO4217.MauritiusRupees: return "MUR";
                    case CURRENCY_ISO4217.MaldivesMaldiveIslandsRufiyaa: return "MVR";
                    case CURRENCY_ISO4217.MalawiKwachas: return "MWK";
                    case CURRENCY_ISO4217.MexicoPesos: return "MXN";
                    case CURRENCY_ISO4217.MalaysiaRinggits: return "MYR";
                    case CURRENCY_ISO4217.MozambiqueMeticais: return "MZN";
                    case CURRENCY_ISO4217.NamibiaDollars: return "NAD";
                    case CURRENCY_ISO4217.NigeriaNairas: return "NGN";
                    case CURRENCY_ISO4217.NicaraguaCordobas: return "NIO";
                    case CURRENCY_ISO4217.NorwayKrone: return "NOK";
                    case CURRENCY_ISO4217.NepalRupees: return "NPR";
                    case CURRENCY_ISO4217.NewZealandDollars: return "NZD";
                    case CURRENCY_ISO4217.OmanRials: return "OMR";
                    case CURRENCY_ISO4217.PanamaBalboa: return "PAB";
                    case CURRENCY_ISO4217.PeruNuevosSoles: return "PEN";
                    case CURRENCY_ISO4217.PapuaNewGuineaKina: return "PGK";
                    case CURRENCY_ISO4217.PhilippinesPesos: return "PHP";
                    case CURRENCY_ISO4217.PakistanRupees: return "PKR";
                    case CURRENCY_ISO4217.PolandZlotych: return "PLN";
                    case CURRENCY_ISO4217.ParaguayGuarani: return "PYG";
                    case CURRENCY_ISO4217.QatarRials: return "QAR";
                    case CURRENCY_ISO4217.RomaniaNewLei: return "RON";
                    case CURRENCY_ISO4217.SerbiaDinars: return "RSD";
                    case CURRENCY_ISO4217.RussiaRubles: return "RUB";
                    case CURRENCY_ISO4217.RwandaRwandaFrancs: return "RWF";
                    case CURRENCY_ISO4217.SaudiArabiaRiyals: return "SAR";
                    case CURRENCY_ISO4217.SolomonIslandsDollars: return "SBD";
                    case CURRENCY_ISO4217.SeychellesRupees: return "SCR";
                    case CURRENCY_ISO4217.SudanPounds: return "SDG";
                    case CURRENCY_ISO4217.SwedenKronor: return "SEK";
                    case CURRENCY_ISO4217.SingaporeDollars: return "SGD";
                    case CURRENCY_ISO4217.SaintHelenaPounds: return "SHP";
                    case CURRENCY_ISO4217.SlovakiaKoruny: return "SKK";
                    case CURRENCY_ISO4217.SierraLeoneLeones: return "SLL";
                    case CURRENCY_ISO4217.SomaliaShillings: return "SOS";
                    case CURRENCY_ISO4217.SeborgaLuigini: return "SPL";
                    case CURRENCY_ISO4217.SurinameDollars: return "SRD";
                    case CURRENCY_ISO4217.SOTomeAndPrincipeDobras: return "STD";
                    case CURRENCY_ISO4217.ElSalvadorColones: return "SVC";
                    case CURRENCY_ISO4217.SyriaPounds: return "SYP";
                    case CURRENCY_ISO4217.SwazilandEmalangeni: return "SZL";
                    case CURRENCY_ISO4217.ThailandBaht: return "THB";
                    case CURRENCY_ISO4217.TajikistanSomoni: return "TJS";
                    case CURRENCY_ISO4217.TurkmenistanManats: return "TMM";
                    case CURRENCY_ISO4217.TurkeyLiras: return "TRY";
                    case CURRENCY_ISO4217.TunisiaDinars: return "TND";
                    case CURRENCY_ISO4217.TongaPaAnga: return "TOP";
                    case CURRENCY_ISO4217.TrinidadAndTobagoDollars: return "TTD";
                    case CURRENCY_ISO4217.TuvaluDollars: return "TVD";
                    case CURRENCY_ISO4217.TaiwanNewDollars: return "TWD";
                    case CURRENCY_ISO4217.TanzaniaShillings: return "TZS";
                    case CURRENCY_ISO4217.UkraineHryvnia: return "UAH";
                    case CURRENCY_ISO4217.UgandaShillings: return "UGX";
                    case CURRENCY_ISO4217.UnitedStatesOfAmericaDollars: return "USD";
                    case CURRENCY_ISO4217.UruguayPesos: return "UYU";
                    case CURRENCY_ISO4217.UzbekistanSums: return "UZS";
                    case CURRENCY_ISO4217.VenezuelaBolivaresExpiresJun: return "VEB";
                    case CURRENCY_ISO4217.VenezuelaBolivaresFuertes: return "VEF";
                    case CURRENCY_ISO4217.VietNamDong: return "VND";
                    case CURRENCY_ISO4217.VanuatuVatu: return "VUV";
                    case CURRENCY_ISO4217.SamoaTala: return "WST";
                    case CURRENCY_ISO4217.CommunautFinanciReAfricaineBeacFrancs: return "XAF";
                    case CURRENCY_ISO4217.SilverOunces: return "XAG";
                    case CURRENCY_ISO4217.GoldOunces: return "XAU";
                    case CURRENCY_ISO4217.EastCaribbeanDollars: return "XCD";
                    case CURRENCY_ISO4217.InternationalMonetaryFundImfSpecialDrawingRights: return "XDR";
                    case CURRENCY_ISO4217.CommunautFinanciReAfricaineBceaoFrancs: return "XOF";
                    case CURRENCY_ISO4217.PalladiumOunces: return "XPD";
                    case CURRENCY_ISO4217.ComptoirsFranAisDuPacifiqueFrancs: return "XPF";
                    case CURRENCY_ISO4217.PlatinumOunces: return "XPT";
                    case CURRENCY_ISO4217.YemenRials: return "YER";
                    case CURRENCY_ISO4217.SouthAfricaRand: return "ZAR";
                    case CURRENCY_ISO4217.ZambiaKwacha: return "ZMK";
                    case CURRENCY_ISO4217.ZimbabweZimbabweDollars: return "ZWD";

                }
                return "";
            }
            public static CURRENCY_ISO4217? Currency_ISO4217FromCode(string code)
            {
                if (code == null)
                    code = "";
                code = code.ToUpper();
                switch (code)
                {
                    case "AED": return CURRENCY_ISO4217.UnitedArabEmiratesDirhams;
                    case "AFN": return CURRENCY_ISO4217.AfghanistanAfghanis;
                    case "ALL": return CURRENCY_ISO4217.AlbaniaLeke;
                    case "AMD": return CURRENCY_ISO4217.ArmeniaDrams;
                    case "ANG": return CURRENCY_ISO4217.NetherlandsAntillesGuildersAlsoCalledFlorins;
                    case "AOA": return CURRENCY_ISO4217.AngolaKwanza;
                    case "ARS": return CURRENCY_ISO4217.ArgentinaPesos;
                    case "AUD": return CURRENCY_ISO4217.AustraliaDollars;
                    case "AWG": return CURRENCY_ISO4217.ArubaGuildersAlsoCalledFlorins;
                    case "AZN": return CURRENCY_ISO4217.AzerbaijanNewManats;
                    case "BAM": return CURRENCY_ISO4217.BosniaAndHerzegovinaConvertibleMarka;
                    case "BBD": return CURRENCY_ISO4217.BarbadosDollars;
                    case "BDT": return CURRENCY_ISO4217.BangladeshTaka;
                    case "BGN": return CURRENCY_ISO4217.BulgariaLeva;
                    case "BHD": return CURRENCY_ISO4217.BahrainDinars;
                    case "BIF": return CURRENCY_ISO4217.BurundiFrancs;
                    case "BMD": return CURRENCY_ISO4217.BermudaDollars;
                    case "BND": return CURRENCY_ISO4217.BruneiDarussalamDollars;
                    case "BOB": return CURRENCY_ISO4217.BoliviaBolivianos;
                    case "BRL": return CURRENCY_ISO4217.BrazilReais;
                    case "BSD": return CURRENCY_ISO4217.BahamasDollars;
                    case "BTN": return CURRENCY_ISO4217.BhutanNgultrum;
                    case "BWP": return CURRENCY_ISO4217.BotswanaPulas;
                    case "BYR": return CURRENCY_ISO4217.BelarusRubles;
                    case "BZD": return CURRENCY_ISO4217.BelizeDollars;
                    case "CAD": return CURRENCY_ISO4217.CanadaDollars;
                    case "CDF": return CURRENCY_ISO4217.CongoKinshasaCongoleseFrancs;
                    case "CHF": return CURRENCY_ISO4217.SwitzerlandFrancs;
                    case "CLP": return CURRENCY_ISO4217.ChilePesos;
                    case "CNY": return CURRENCY_ISO4217.ChinaYuanRenminbi;
                    case "COP": return CURRENCY_ISO4217.ColombiaPesos;
                    case "CRC": return CURRENCY_ISO4217.CostaRicaColones;
                    case "CUP": return CURRENCY_ISO4217.CubaPesos;
                    case "CVE": return CURRENCY_ISO4217.CapeVerdeEscudos;
                    case "CYP": return CURRENCY_ISO4217.CyprusPoundsExpiresJan;
                    case "CZK": return CURRENCY_ISO4217.CzechRepublicKoruny;
                    case "DJF": return CURRENCY_ISO4217.DjiboutiFrancs;
                    case "DKK": return CURRENCY_ISO4217.DenmarkKroner;
                    case "DOP": return CURRENCY_ISO4217.DominicanRepublicPesos;
                    case "DZD": return CURRENCY_ISO4217.AlgeriaAlgeriaDinars;
                    case "EEK": return CURRENCY_ISO4217.EstoniaKrooni;
                    case "EGP": return CURRENCY_ISO4217.EgyptPounds;
                    case "ERN": return CURRENCY_ISO4217.EritreaNakfa;
                    case "ETB": return CURRENCY_ISO4217.EthiopiaBirr;
                    case "EUR": return CURRENCY_ISO4217.EuroMemberCountriesEuro;
                    case "FJD": return CURRENCY_ISO4217.FijiDollars;
                    case "FKP": return CURRENCY_ISO4217.FalklandIslandsMalvinasPounds;
                    case "GBP": return CURRENCY_ISO4217.UnitedKingdomPounds;
                    case "GEL": return CURRENCY_ISO4217.GeorgiaLari;
                    case "GGP": return CURRENCY_ISO4217.GuernseyPounds;
                    case "GHS": return CURRENCY_ISO4217.GhanaCedis;
                    case "GIP": return CURRENCY_ISO4217.GibraltarPounds;
                    case "GMD": return CURRENCY_ISO4217.GambiaDalasi;
                    case "GNF": return CURRENCY_ISO4217.GuineaFrancs;
                    case "GTQ": return CURRENCY_ISO4217.GuatemalaQuetzales;
                    case "GYD": return CURRENCY_ISO4217.GuyanaDollars;
                    case "HKD": return CURRENCY_ISO4217.HongKongDollars;
                    case "HNL": return CURRENCY_ISO4217.HondurasLempiras;
                    case "HRK": return CURRENCY_ISO4217.CroatiaKuna;
                    case "HTG": return CURRENCY_ISO4217.HaitiGourdes;
                    case "HUF": return CURRENCY_ISO4217.HungaryForint;
                    case "IDR": return CURRENCY_ISO4217.IndonesiaRupiahs;
                    case "ILS": return CURRENCY_ISO4217.IsraelNewShekels;
                    case "IMP": return CURRENCY_ISO4217.IsleOfManPounds;
                    case "INR": return CURRENCY_ISO4217.IndiaRupees;
                    case "IQD": return CURRENCY_ISO4217.IraqDinars;
                    case "IRR": return CURRENCY_ISO4217.IranRials;
                    case "ISK": return CURRENCY_ISO4217.IcelandKronur;
                    case "JEP": return CURRENCY_ISO4217.JerseyPounds;
                    case "JMD": return CURRENCY_ISO4217.JamaicaDollars;
                    case "JOD": return CURRENCY_ISO4217.JordanDinars;
                    case "JPY": return CURRENCY_ISO4217.JapanYen;
                    case "KES": return CURRENCY_ISO4217.KenyaShillings;
                    case "KGS": return CURRENCY_ISO4217.KyrgyzstanSoms;
                    case "KHR": return CURRENCY_ISO4217.CambodiaRiels;
                    case "KMF": return CURRENCY_ISO4217.ComorosFrancs;
                    case "KPW": return CURRENCY_ISO4217.KoreaNorthWon;
                    case "KRW": return CURRENCY_ISO4217.KoreaSouthWon;
                    case "KWD": return CURRENCY_ISO4217.KuwaitDinars;
                    case "KYD": return CURRENCY_ISO4217.CaymanIslandsDollars;
                    case "KZT": return CURRENCY_ISO4217.KazakhstanTenge;
                    case "LAK": return CURRENCY_ISO4217.LaosKips;
                    case "LBP": return CURRENCY_ISO4217.LebanonPounds;
                    case "LKR": return CURRENCY_ISO4217.SriLankaRupees;
                    case "LRD": return CURRENCY_ISO4217.LiberiaDollars;
                    case "LSL": return CURRENCY_ISO4217.LesothoMaloti;
                    case "LTL": return CURRENCY_ISO4217.LithuaniaLitai;
                    case "LVL": return CURRENCY_ISO4217.LatviaLati;
                    case "LYD": return CURRENCY_ISO4217.LibyaDinars;
                    case "MAD": return CURRENCY_ISO4217.MoroccoDirhams;
                    case "MDL": return CURRENCY_ISO4217.MoldovaLei;
                    case "MGA": return CURRENCY_ISO4217.MadagascarAriary;
                    case "MKD": return CURRENCY_ISO4217.MacedoniaDenars;
                    case "MMK": return CURRENCY_ISO4217.MyanmarBurmaKyats;
                    case "MNT": return CURRENCY_ISO4217.MongoliaTugriks;
                    case "MOP": return CURRENCY_ISO4217.MacauPatacas;
                    case "MRO": return CURRENCY_ISO4217.MauritaniaOuguiyas;
                    case "MTL": return CURRENCY_ISO4217.MaltaLiriExpiresJan;
                    case "MUR": return CURRENCY_ISO4217.MauritiusRupees;
                    case "MVR": return CURRENCY_ISO4217.MaldivesMaldiveIslandsRufiyaa;
                    case "MWK": return CURRENCY_ISO4217.MalawiKwachas;
                    case "MXN": return CURRENCY_ISO4217.MexicoPesos;
                    case "MYR": return CURRENCY_ISO4217.MalaysiaRinggits;
                    case "MZN": return CURRENCY_ISO4217.MozambiqueMeticais;
                    case "NAD": return CURRENCY_ISO4217.NamibiaDollars;
                    case "NGN": return CURRENCY_ISO4217.NigeriaNairas;
                    case "NIO": return CURRENCY_ISO4217.NicaraguaCordobas;
                    case "NOK": return CURRENCY_ISO4217.NorwayKrone;
                    case "NPR": return CURRENCY_ISO4217.NepalRupees;
                    case "NZD": return CURRENCY_ISO4217.NewZealandDollars;
                    case "OMR": return CURRENCY_ISO4217.OmanRials;
                    case "PAB": return CURRENCY_ISO4217.PanamaBalboa;
                    case "PEN": return CURRENCY_ISO4217.PeruNuevosSoles;
                    case "PGK": return CURRENCY_ISO4217.PapuaNewGuineaKina;
                    case "PHP": return CURRENCY_ISO4217.PhilippinesPesos;
                    case "PKR": return CURRENCY_ISO4217.PakistanRupees;
                    case "PLN": return CURRENCY_ISO4217.PolandZlotych;
                    case "PYG": return CURRENCY_ISO4217.ParaguayGuarani;
                    case "QAR": return CURRENCY_ISO4217.QatarRials;
                    case "RON": return CURRENCY_ISO4217.RomaniaNewLei;
                    case "RSD": return CURRENCY_ISO4217.SerbiaDinars;
                    case "RUB": return CURRENCY_ISO4217.RussiaRubles;
                    case "RWF": return CURRENCY_ISO4217.RwandaRwandaFrancs;
                    case "SAR": return CURRENCY_ISO4217.SaudiArabiaRiyals;
                    case "SBD": return CURRENCY_ISO4217.SolomonIslandsDollars;
                    case "SCR": return CURRENCY_ISO4217.SeychellesRupees;
                    case "SDG": return CURRENCY_ISO4217.SudanPounds;
                    case "SEK": return CURRENCY_ISO4217.SwedenKronor;
                    case "SGD": return CURRENCY_ISO4217.SingaporeDollars;
                    case "SHP": return CURRENCY_ISO4217.SaintHelenaPounds;
                    case "SKK": return CURRENCY_ISO4217.SlovakiaKoruny;
                    case "SLL": return CURRENCY_ISO4217.SierraLeoneLeones;
                    case "SOS": return CURRENCY_ISO4217.SomaliaShillings;
                    case "SPL": return CURRENCY_ISO4217.SeborgaLuigini;
                    case "SRD": return CURRENCY_ISO4217.SurinameDollars;
                    case "STD": return CURRENCY_ISO4217.SOTomeAndPrincipeDobras;
                    case "SVC": return CURRENCY_ISO4217.ElSalvadorColones;
                    case "SYP": return CURRENCY_ISO4217.SyriaPounds;
                    case "SZL": return CURRENCY_ISO4217.SwazilandEmalangeni;
                    case "THB": return CURRENCY_ISO4217.ThailandBaht;
                    case "TJS": return CURRENCY_ISO4217.TajikistanSomoni;
                    case "TMM": return CURRENCY_ISO4217.TurkmenistanManats;
                    case "TND": return CURRENCY_ISO4217.TunisiaDinars;
                    case "TOP": return CURRENCY_ISO4217.TongaPaAnga;
                    case "TRL": return CURRENCY_ISO4217.TurkeyLiras;
                    case "TTD": return CURRENCY_ISO4217.TrinidadAndTobagoDollars;
                    case "TVD": return CURRENCY_ISO4217.TuvaluDollars;
                    case "TWD": return CURRENCY_ISO4217.TaiwanNewDollars;
                    case "TZS": return CURRENCY_ISO4217.TanzaniaShillings;
                    case "UAH": return CURRENCY_ISO4217.UkraineHryvnia;
                    case "UGX": return CURRENCY_ISO4217.UgandaShillings;
                    case "USD": return CURRENCY_ISO4217.UnitedStatesOfAmericaDollars;
                    case "UYU": return CURRENCY_ISO4217.UruguayPesos;
                    case "UZS": return CURRENCY_ISO4217.UzbekistanSums;
                    case "VEB": return CURRENCY_ISO4217.VenezuelaBolivaresExpiresJun;
                    case "VEF": return CURRENCY_ISO4217.VenezuelaBolivaresFuertes;
                    case "VND": return CURRENCY_ISO4217.VietNamDong;
                    case "VUV": return CURRENCY_ISO4217.VanuatuVatu;
                    case "WST": return CURRENCY_ISO4217.SamoaTala;
                    case "XAF": return CURRENCY_ISO4217.CommunautFinanciReAfricaineBeacFrancs;
                    case "XAG": return CURRENCY_ISO4217.SilverOunces;
                    case "XAU": return CURRENCY_ISO4217.GoldOunces;
                    case "XCD": return CURRENCY_ISO4217.EastCaribbeanDollars;
                    case "XDR": return CURRENCY_ISO4217.InternationalMonetaryFundImfSpecialDrawingRights;
                    case "XOF": return CURRENCY_ISO4217.CommunautFinanciReAfricaineBceaoFrancs;
                    case "XPD": return CURRENCY_ISO4217.PalladiumOunces;
                    case "XPF": return CURRENCY_ISO4217.ComptoirsFranAisDuPacifiqueFrancs;
                    case "XPT": return CURRENCY_ISO4217.PlatinumOunces;
                    case "YER": return CURRENCY_ISO4217.YemenRials;
                    case "ZAR": return CURRENCY_ISO4217.SouthAfricaRand;
                    case "ZMK": return CURRENCY_ISO4217.ZambiaKwacha;
                    case "ZWD": return CURRENCY_ISO4217.ZimbabweZimbabweDollars;

                }
                return null;
            }
            public static string GetCurrencySymbol(CURRENCY_ISO4217 currency)
            {
                switch (currency)
                {
                    case CURRENCY_ISO4217.AlbaniaLeke: return "Lek";
                    case CURRENCY_ISO4217.AlgeriaAlgeriaDinars: return "دج";
                    case CURRENCY_ISO4217.AngolaKwanza: return "Kz";
                    case CURRENCY_ISO4217.UnitedStatesOfAmericaDollars:
                    case CURRENCY_ISO4217.AustraliaDollars:
                    case CURRENCY_ISO4217.ArgentinaPesos:
                    case CURRENCY_ISO4217.BahamasDollars:
                    case CURRENCY_ISO4217.BarbadosDollars:
                    case CURRENCY_ISO4217.BermudaDollars:
                    case CURRENCY_ISO4217.BruneiDarussalamDollars:
                    case CURRENCY_ISO4217.CanadaDollars:
                    case CURRENCY_ISO4217.CaymanIslandsDollars:
                    case CURRENCY_ISO4217.ChilePesos:
                    case CURRENCY_ISO4217.ColombiaPesos:
                    case CURRENCY_ISO4217.EastCaribbeanDollars:
                    case CURRENCY_ISO4217.ElSalvadorColones:
                    case CURRENCY_ISO4217.FijiDollars:
                    case CURRENCY_ISO4217.GuyanaDollars:
                    case CURRENCY_ISO4217.HongKongDollars:
                    case CURRENCY_ISO4217.LiberiaDollars:
                    case CURRENCY_ISO4217.MexicoPesos:
                    case CURRENCY_ISO4217.NamibiaDollars:
                    case CURRENCY_ISO4217.NewZealandDollars:
                    case CURRENCY_ISO4217.SingaporeDollars:
                    case CURRENCY_ISO4217.SolomonIslandsDollars:
                    case CURRENCY_ISO4217.SurinameDollars:
                    case CURRENCY_ISO4217.TuvaluDollars:
                        return "$";
                    case CURRENCY_ISO4217.MaltaLiriExpiresJan: return "Lm";
                    case CURRENCY_ISO4217.UnitedKingdomPounds:
                    case CURRENCY_ISO4217.EgyptPounds:
                    case CURRENCY_ISO4217.FalklandIslandsMalvinasPounds:
                    case CURRENCY_ISO4217.GibraltarPounds:
                    case CURRENCY_ISO4217.GuernseyPounds:
                    case CURRENCY_ISO4217.IsleOfManPounds:
                    case CURRENCY_ISO4217.JerseyPounds:
                    case CURRENCY_ISO4217.LebanonPounds:
                    case CURRENCY_ISO4217.SaintHelenaPounds:
                        return "£";
                    case CURRENCY_ISO4217.EuroMemberCountriesEuro:
                        return "€";
                    case CURRENCY_ISO4217.IcelandKronur:
                    case CURRENCY_ISO4217.DenmarkKroner:
                    case CURRENCY_ISO4217.SwedenKronor:
                    case CURRENCY_ISO4217.NorwayKrone:
                        return "kr";
                    case CURRENCY_ISO4217.AfghanistanAfghanis: return "؋";
                    case CURRENCY_ISO4217.ArubaGuildersAlsoCalledFlorins: return "ƒ";
                    case CURRENCY_ISO4217.AzerbaijanNewManats: return "ман";
                    case CURRENCY_ISO4217.BelarusRubles: return "p.";
                    case CURRENCY_ISO4217.BelizeDollars: return "BZ$";
                    case CURRENCY_ISO4217.BoliviaBolivianos: return "$b";
                    case CURRENCY_ISO4217.BosniaAndHerzegovinaConvertibleMarka: return "KM";
                    case CURRENCY_ISO4217.BotswanaPulas: return "P";
                    case CURRENCY_ISO4217.BulgariaLeva: return "лв";
                    case CURRENCY_ISO4217.BrazilReais: return "R$";
                    case CURRENCY_ISO4217.CambodiaRiels: return "៛";
                    case CURRENCY_ISO4217.ChinaYuanRenminbi: return "¥";
                    case CURRENCY_ISO4217.CostaRicaColones: return "₡";
                    case CURRENCY_ISO4217.CroatiaKuna: return "kn";
                    case CURRENCY_ISO4217.CubaPesos: return "₱";
                    case CURRENCY_ISO4217.CzechRepublicKoruny: return "Kč";
                    case CURRENCY_ISO4217.DominicanRepublicPesos: return "RD$";
                    case CURRENCY_ISO4217.GhanaCedis: return "¢";
                    case CURRENCY_ISO4217.GuatemalaQuetzales: return "Q";
                    case CURRENCY_ISO4217.HondurasLempiras: return "L";
                    case CURRENCY_ISO4217.HungaryForint: return "Ft";
                    case CURRENCY_ISO4217.IndiaRupees: return null; //Not available - Introduced in July 2010
                    case CURRENCY_ISO4217.IndonesiaRupiahs: return "Rp";
                    case CURRENCY_ISO4217.IranRials: return "﷼";
                    case CURRENCY_ISO4217.IsraelNewShekels: return "₪";
                    case CURRENCY_ISO4217.JamaicaDollars: return "J$";
                    case CURRENCY_ISO4217.JapanYen: return "¥";
                    case CURRENCY_ISO4217.KyrgyzstanSoms:
                    case CURRENCY_ISO4217.KazakhstanTenge:
                        return "лв";
                    case CURRENCY_ISO4217.KoreaNorthWon:
                    case CURRENCY_ISO4217.KoreaSouthWon:
                        return "₩";
                    case CURRENCY_ISO4217.LaosKips: return "₭";
                    case CURRENCY_ISO4217.LatviaLati: return "Ls";
                    case CURRENCY_ISO4217.SwitzerlandFrancs: return "CHF";
                    case CURRENCY_ISO4217.LithuaniaLitai: return "Lt";
                    case CURRENCY_ISO4217.MacedoniaDenars: return "ден";
                    case CURRENCY_ISO4217.MalaysiaRinggits: return "RM";
                    case CURRENCY_ISO4217.MauritiusRupees:
                    case CURRENCY_ISO4217.PakistanRupees:
                    case CURRENCY_ISO4217.SeychellesRupees:
                    case CURRENCY_ISO4217.SriLankaRupees:
                        return "₨";
                    case CURRENCY_ISO4217.MongoliaTugriks: return "₮";
                    case CURRENCY_ISO4217.MozambiqueMeticais: return "MT";
                    case CURRENCY_ISO4217.NepalRupees: return "₨";
                    case CURRENCY_ISO4217.NetherlandsAntillesGuildersAlsoCalledFlorins: return "ƒ";
                    case CURRENCY_ISO4217.NicaraguaCordobas: return "C$";
                    case CURRENCY_ISO4217.NigeriaNairas: return "₦";
                    case CURRENCY_ISO4217.OmanRials: return "﷼";
                    case CURRENCY_ISO4217.PanamaBalboa: return "B/.";
                    case CURRENCY_ISO4217.ParaguayGuarani: return "Gs";
                    case CURRENCY_ISO4217.PeruNuevosSoles: return "S/.";
                    case CURRENCY_ISO4217.PhilippinesPesos: return "Php";
                    case CURRENCY_ISO4217.PolandZlotych: return "zł";
                    case CURRENCY_ISO4217.QatarRials: return "﷼";
                    case CURRENCY_ISO4217.RomaniaNewLei: return "lei";
                    case CURRENCY_ISO4217.RussiaRubles: return "руб";
                    case CURRENCY_ISO4217.SaudiArabiaRiyals: return "﷼";
                    case CURRENCY_ISO4217.SerbiaDinars: return "Дин.";
                    case CURRENCY_ISO4217.SomaliaShillings: return "S";
                    case CURRENCY_ISO4217.SouthAfricaRand: return "R";
                    case CURRENCY_ISO4217.TaiwanNewDollars: return "NT$";
                    case CURRENCY_ISO4217.ThailandBaht: return "฿";
                    case CURRENCY_ISO4217.TrinidadAndTobagoDollars: return "TT$";
                    case CURRENCY_ISO4217.TurkeyLiras: return "₤";
                    case CURRENCY_ISO4217.UkraineHryvnia: return "₴";
                    case CURRENCY_ISO4217.UruguayPesos: return "$U";
                    case CURRENCY_ISO4217.UzbekistanSums: return "лв";
                    case CURRENCY_ISO4217.VenezuelaBolivaresFuertes: return "Bs";
                    case CURRENCY_ISO4217.VietNamDong: return "₫";
                    case CURRENCY_ISO4217.YemenRials: return "﷼";
                    case CURRENCY_ISO4217.ZimbabweZimbabweDollars: return "Z$";

                    default:
                        return null;
                }
            }

            public static string GetCurrencySymbol(CURRENCY_ISO4217 currency, bool htmlEncode)
            {
                string currCode = GetCurrencySymbol(currency);

                if (htmlEncode)
                {
                    CS.General_v3.Util.PageUtil.HtmlEncode(currCode);
                }
                return currCode;
            }
            private static ListItemCollection _CURRENCIES_LIST_ITEM_COLL;
            public static ListItemCollection GetCurrenciesAsListItemCollection(IEnumerable<CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217> selectedCurrencies = null)
            {
                if (true || _CURRENCIES_LIST_ITEM_COLL == null)
                {
                    var currencies = CS.General_v3.Util.EnumUtils.GetListOfEnumValues<CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217>();
                    List<ListItem> items = new List<ListItem>();
                    foreach (var currency in currencies)
                    {
                        //EUR (€)
                        string currencySymbol = GetCurrencySymbol(currency);
                        string currCode = Currency_ISO4217ToCode(currency);

                        string text = currCode;
                        if (!string.IsNullOrEmpty(currencySymbol))
                        {
                            text += " (" + currencySymbol + ")";
                        }
                        int val = (int)currency;
                        bool isSelected = selectedCurrencies != null && selectedCurrencies.Contains(currency);
                        items.Add(new ListItem() { Text = text, Value = val.ToString(), Selected = isSelected });
                    }
                    items.Sort((a, b) => a.Text.CompareTo(b.Text));
                    _CURRENCIES_LIST_ITEM_COLL = new ListItemCollection();
                    _CURRENCIES_LIST_ITEM_COLL.AddRange(items.ToArray());
                }
                return _CURRENCIES_LIST_ITEM_COLL;
            }

        }
    }
}
