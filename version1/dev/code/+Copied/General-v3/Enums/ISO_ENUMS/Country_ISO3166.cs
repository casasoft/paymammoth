﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using CS.General_v3.Util;
//CS.General_v3.Enums.ISO_ENUMS
namespace CS.General_v3
{
    public static partial class Enums
    {
        public static partial class ISO_ENUMS
        {

            

            public enum Country_ISO3166
            {
                
                [DescriptionAttribute("Andorra")]
                Andorra,
                [DescriptionAttribute("United Arab Emirates")]
                UnitedArabEmirates,
                [DescriptionAttribute("Afghanistan")]
                Afghanistan,
                [DescriptionAttribute("Antigua and Barbuda")]
                AntiguaandBarbuda,
                [DescriptionAttribute("Anguilla")]
                Anguilla,
                [DescriptionAttribute("Albania")]
                Albania,
                [DescriptionAttribute("Armenia")]
                Armenia,
                [DescriptionAttribute("Angola")]
                Angola,
                [DescriptionAttribute("Antarctica")]
                Antarctica,
                [DescriptionAttribute("Argentina")]
                Argentina,
                [DescriptionAttribute("American Samoa")]
                AmericanSamoa,
                [DescriptionAttribute("Austria")]
                Austria,
                [DescriptionAttribute("Australia")]
                Australia,
                [DescriptionAttribute("Aruba")]
                Aruba,
                [DescriptionAttribute("Aland Islands")]
                AlandIslands,
                [DescriptionAttribute("Azerbaijan")]
                Azerbaijan,
                [DescriptionAttribute("Bosnia and Herzegovina")]
                BosniaandHerzegovina,
                [DescriptionAttribute("Barbados")]
                Barbados,
                [DescriptionAttribute("Bangladesh")]
                Bangladesh,
                [DescriptionAttribute("Belgium")]
                Belgium,
                [DescriptionAttribute("Burkina Faso")]
                BurkinaFaso,
                [DescriptionAttribute("Bulgaria")]
                Bulgaria,
                [DescriptionAttribute("Bahrain")]
                Bahrain,
                [DescriptionAttribute("Burundi")]
                Burundi,
                [DescriptionAttribute("Benin")]
                Benin,
                [DescriptionAttribute("Saint Barth")]
                SaintBarth,
                [DescriptionAttribute("Bermuda")]
                Bermuda,
                [DescriptionAttribute("Brunei Darussalam")]
                BruneiDarussalam,
                [DescriptionAttribute("Bolivia")]
                Bolivia,
                [DescriptionAttribute("Bonaire")]
                Bonaire,
                [DescriptionAttribute("Brazil")]
                Brazil,
                [DescriptionAttribute("Bahamas")]
                Bahamas,
                [DescriptionAttribute("Bhutan")]
                Bhutan,
                [DescriptionAttribute("Bouvet Island")]
                BouvetIsland,
                [DescriptionAttribute("Botswana")]
                Botswana,
                [DescriptionAttribute("Belarus")]
                Belarus,
                [DescriptionAttribute("Belize")]
                Belize,
                [DescriptionAttribute("Canada")]
                Canada,
                [DescriptionAttribute("Cocos")]
                Cocos,
                [DescriptionAttribute("Congo, the Democratic Republic of the")]
                CongoTheDemocraticRepublicOfThe,
                [DescriptionAttribute("Central African Republic")]
                CentralAfricanRepublic,
                [DescriptionAttribute("Congo")]
                Congo,
                [DescriptionAttribute("Switzerland")]
                Switzerland,
                [DescriptionAttribute("Ivory Coast")]
                CoteDIvoireIvoryCoast,
                [DescriptionAttribute("Cook Islands")]
                CookIslands,
                [DescriptionAttribute("Chile")]
                Chile,
                [DescriptionAttribute("Cameroon")]
                Cameroon,
                [DescriptionAttribute("China")]
                China,
                [DescriptionAttribute("Colombia")]
                Colombia,
                [DescriptionAttribute("Costa Rica")]
                CostaRica,
                [DescriptionAttribute("Cuba")]
                Cuba,
                [DescriptionAttribute("Cape Verde")]
                CapeVerde,
                [DescriptionAttribute("Cura")]
                Cura,
                [DescriptionAttribute("Christmas Island")]
                ChristmasIsland,
                [DescriptionAttribute("Cyprus")]
                Cyprus,
                [DescriptionAttribute("Czech Republic")]
                CzechRepublic,
                [DescriptionAttribute("Germany")]
                Germany,
                [DescriptionAttribute("Djibouti")]
                Djibouti,
                [DescriptionAttribute("Denmark")]
                Denmark,
                [DescriptionAttribute("Dominica")]
                Dominica,
                [DescriptionAttribute("Dominican Republic")]
                DominicanRepublic,
                [DescriptionAttribute("Algeria")]
                Algeria,
                [DescriptionAttribute("Ecuador")]
                Ecuador,
                [DescriptionAttribute("Estonia")]
                Estonia,
                [DescriptionAttribute("Egypt")]
                Egypt,
                [DescriptionAttribute("Western Sahara")]
                WesternSahara,
                [DescriptionAttribute("Eritrea")]
                Eritrea,
                [DescriptionAttribute("Spain")]
                Spain,
                [DescriptionAttribute("Ethiopia")]
                Ethiopia,
                [DescriptionAttribute("Finland")]
                Finland,
                [DescriptionAttribute("Fiji")]
                Fiji,
                [DescriptionAttribute("Falkland Islands")]
                FalklandIslands,
                [DescriptionAttribute("Micronesia")]
                Micronesia,
                [DescriptionAttribute("Faroe Islands")]
                FaroeIslands,
                [DescriptionAttribute("France")]
                France,
                [DescriptionAttribute("Gabon")]
                Gabon,
                [DescriptionAttribute("United Kingdom")]
                UnitedKingdom,
                [DescriptionAttribute("Grenada")]
                Grenada,
                [DescriptionAttribute("Georgia")]
                Georgia,
                [DescriptionAttribute("French Guiana")]
                FrenchGuiana,
                [DescriptionAttribute("Guernsey")]
                Guernsey,
                [DescriptionAttribute("Ghana")]
                Ghana,
                [DescriptionAttribute("Gibraltar")]
                Gibraltar,
                [DescriptionAttribute("Greenland")]
                Greenland,
                [DescriptionAttribute("Gambia")]
                Gambia,
                [DescriptionAttribute("Guinea")]
                Guinea,
                [DescriptionAttribute("Guadeloupe")]
                Guadeloupe,
                [DescriptionAttribute("Equatorial Guinea")]
                EquatorialGuinea,
                [DescriptionAttribute("Greece")]
                Greece,
                [DescriptionAttribute("South Georgia and the South Sandwich Islands")]
                SouthGeorgiaandtheSouthSandwichIslands,
                [DescriptionAttribute("Guatemala")]
                Guatemala,
                [DescriptionAttribute("Guam")]
                Guam,
                [DescriptionAttribute("Guinea-Bissau")]
                GuineaBissau,
                [DescriptionAttribute("Guyana")]
                Guyana,
                [DescriptionAttribute("Hong Kong")]
                HongKong,
                [DescriptionAttribute("Heard Island and McDonald Islands")]
                HeardIslandandMcDonaldIslands,
                [DescriptionAttribute("Honduras")]
                Honduras,
                [DescriptionAttribute("Croatia")]
                Croatia,
                [DescriptionAttribute("Haiti")]
                Haiti,
                [DescriptionAttribute("Hungary")]
                Hungary,
                [DescriptionAttribute("Indonesia")]
                Indonesia,
                [DescriptionAttribute("Ireland")]
                Ireland,
                [DescriptionAttribute("Israel")]
                Israel,
                [DescriptionAttribute("Isle of Man")]
                IsleofMan,
                [DescriptionAttribute("India")]
                India,
                [DescriptionAttribute("British Indian Ocean Territory")]
                BritishIndianOceanTerritory,
                [DescriptionAttribute("Iraq")]
                Iraq,
                [DescriptionAttribute("Iran")]
                Iran,
                [DescriptionAttribute("Iceland")]
                Iceland,
                [DescriptionAttribute("Italy")]
                Italy,
                [DescriptionAttribute("Jersey")]
                Jersey,
                [DescriptionAttribute("Jamaica")]
                Jamaica,
                [DescriptionAttribute("Jordan")]
                Jordan,
                [DescriptionAttribute("Japan")]
                Japan,
                [DescriptionAttribute("Kenya")]
                Kenya,
                [DescriptionAttribute("Kyrgyzstan")]
                Kyrgyzstan,
                [DescriptionAttribute("Cambodia")]
                Cambodia,
                [DescriptionAttribute("Kiribati")]
                Kiribati,
                [DescriptionAttribute("Comoros")]
                Comoros,
                [DescriptionAttribute("Saint Kitts and Nevis")]
                SaintKittsandNevis,
                [DescriptionAttribute("Korea, Democratic People's Republic Of")]
                KoreaDemocraticPeoplesRepublicOf_NorthKorea, //North Korea
                [DescriptionAttribute("Korea, Republic Of")]
                KoreaRepublicOf_SouthKorea,
                [DescriptionAttribute("Kuwait")]
                Kuwait,
                [DescriptionAttribute("Cayman Islands")]
                CaymanIslands,
                [DescriptionAttribute("Kazakhstan")]
                Kazakhstan,
                [DescriptionAttribute("Laos")]
                Laos,
                [DescriptionAttribute("Lebanon")]
                Lebanon,
                [DescriptionAttribute("Saint Lucia")]
                SaintLucia,
                [DescriptionAttribute("Liechtenstein")]
                Liechtenstein,
                [DescriptionAttribute("Sri Lanka")]
                SriLanka,
                [DescriptionAttribute("Liberia")]
                Liberia,
                [DescriptionAttribute("Lesotho")]
                Lesotho,
                [DescriptionAttribute("Lithuania")]
                Lithuania,
                [DescriptionAttribute("Luxembourg")]
                Luxembourg,
                [DescriptionAttribute("Latvia")]
                Latvia,
                [DescriptionAttribute("Libyan Arab Jamahiriya")]
                LibyanArabJamahiriya,
                [DescriptionAttribute("Morocco")]
                Morocco,
                [DescriptionAttribute("Monaco")]
                Monaco,
                [DescriptionAttribute("Moldova")]
                Moldova,
                [DescriptionAttribute("Montenegro")]
                Montenegro,
                [DescriptionAttribute("Saint Martin")]
                SaintMartin,
                [DescriptionAttribute("Madagascar")]
                Madagascar,
                [DescriptionAttribute("Marshall Islands")]
                MarshallIslands,
                [DescriptionAttribute("Macedonia")]
                Macedonia,
                [DescriptionAttribute("Mali")]
                Mali,
                [DescriptionAttribute("Myanmar")]
                Myanmar,
                [DescriptionAttribute("Mongolia")]
                Mongolia,
                [DescriptionAttribute("Macao")]
                Macao,
                [DescriptionAttribute("Northern Mariana Islands")]
                NorthernMarianaIslands,
                [DescriptionAttribute("Martinique")]
                Martinique,
                [DescriptionAttribute("Mauritania")]
                Mauritania,
                [DescriptionAttribute("Montserrat")]
                Montserrat,
                [DescriptionAttribute("Malta")]
                Malta,
                [DescriptionAttribute("Mauritius")]
                Mauritius,
                [DescriptionAttribute("Maldives")]
                Maldives,
                [DescriptionAttribute("Malawi")]
                Malawi,
                [DescriptionAttribute("Mexico")]
                Mexico,
                [DescriptionAttribute("Malaysia")]
                Malaysia,
                [DescriptionAttribute("Mozambique")]
                Mozambique,
                [DescriptionAttribute("Namibia")]
                Namibia,
                [DescriptionAttribute("New Caledonia")]
                NewCaledonia,
                [DescriptionAttribute("Niger")]
                Niger,
                [DescriptionAttribute("Norfolk Island")]
                NorfolkIsland,
                [DescriptionAttribute("Nigeria")]
                Nigeria,
                [DescriptionAttribute("Nicaragua")]
                Nicaragua,
                [DescriptionAttribute("Netherlands")]
                Netherlands,
                [DescriptionAttribute("Norway")]
                Norway,
                [DescriptionAttribute("Nepal")]
                Nepal,
                [DescriptionAttribute("Nauru")]
                Nauru,
                [DescriptionAttribute("Niue")]
                Niue,
                [DescriptionAttribute("New Zealand")]
                NewZealand,
                [DescriptionAttribute("Oman")]
                Oman,
                [DescriptionAttribute("Panama")]
                Panama,
                [DescriptionAttribute("Peru")]
                Peru,
                [DescriptionAttribute("French Polynesia")]
                FrenchPolynesia,
                [DescriptionAttribute("Papua New Guinea")]
                PapuaNewGuinea,
                [DescriptionAttribute("Philippines")]
                Philippines,
                [DescriptionAttribute("Pakistan")]
                Pakistan,
                [DescriptionAttribute("Poland")]
                Poland,
                [DescriptionAttribute("Saint Pierre and Miquelon")]
                SaintPierreandMiquelon,
                [DescriptionAttribute("Pitcairn")]
                Pitcairn,
                [DescriptionAttribute("Puerto Rico")]
                PuertoRico,
                [DescriptionAttribute("Palestinian Territory")]
                PalestinianTerritory,
                [DescriptionAttribute("Portugal")]
                Portugal,
                [DescriptionAttribute("Palau")]
                Palau,
                [DescriptionAttribute("Paraguay")]
                Paraguay,
                [DescriptionAttribute("Qatar")]
                Qatar,
                [DescriptionAttribute("Reunion")]
                Reunion,
                [DescriptionAttribute("Romania")]
                Romania,
                [DescriptionAttribute("Serbia")]
                Serbia,
                [DescriptionAttribute("Russian Federation")]
                RussianFederation,
                [DescriptionAttribute("Rwanda")]
                Rwanda,
                [DescriptionAttribute("Saudi Arabia")]
                SaudiArabia,
                [DescriptionAttribute("Solomon Islands")]
                SolomonIslands,
                [DescriptionAttribute("Seychelles")]
                Seychelles,
                [DescriptionAttribute("Sudan")]
                Sudan,
                [DescriptionAttribute("Sweden")]
                Sweden,
                [DescriptionAttribute("Singapore")]
                Singapore,
                [DescriptionAttribute("Saint Helena")]
                SaintHelena,
                [DescriptionAttribute("Slovenia")]
                Slovenia,
                [DescriptionAttribute("Svalbard and Jan Mayen")]
                SvalbardandJanMayen,
                [DescriptionAttribute("Slovakia")]
                Slovakia,
                [DescriptionAttribute("Sierra Leone")]
                SierraLeone,
                [DescriptionAttribute("San Marino")]
                SanMarino,
                [DescriptionAttribute("Senegal")]
                Senegal,
                [DescriptionAttribute("Somalia")]
                Somalia,
                [DescriptionAttribute("Suriname")]
                Suriname,
                [DescriptionAttribute("Sao Tome and Principe")]
                SaoTomeandPrincipe,
                [DescriptionAttribute("El Salvador")]
                ElSalvador,
                [DescriptionAttribute("Sint Maarten")]
                SintMaarten,
                [DescriptionAttribute("Syrian Arab Republic")]
                SyrianArabRepublic,
                [DescriptionAttribute("Swaziland")]
                Swaziland,
                [DescriptionAttribute("Turks and Caicos Islands")]
                TurksandCaicosIslands,
                [DescriptionAttribute("Chad")]
                Chad,
                [DescriptionAttribute("French Southern Territories")]
                FrenchSouthernTerritories,
                [DescriptionAttribute("Togo")]
                Togo,
                [DescriptionAttribute("Thailand")]
                Thailand,
                [DescriptionAttribute("Tajikistan")]
                Tajikistan,
                [DescriptionAttribute("Tokelau")]
                Tokelau,
                [DescriptionAttribute("Timor-Leste")]
                TimorLeste,
                [DescriptionAttribute("Turkmenistan")]
                Turkmenistan,
                [DescriptionAttribute("Tunisia")]
                Tunisia,
                [DescriptionAttribute("Tonga")]
                Tonga,
                [DescriptionAttribute("Turkey")]
                Turkey,
                [DescriptionAttribute("Trinidad and Tobago")]
                TrinidadandTobago,
                [DescriptionAttribute("Tuvalu")]
                Tuvalu,
                [DescriptionAttribute("Taiwan")]
                Taiwan,
                [DescriptionAttribute("Tanzania")]
                Tanzania,
                [DescriptionAttribute("Ukraine")]
                Ukraine,
                [DescriptionAttribute("Uganda")]
                Uganda,
                [DescriptionAttribute("United States Minor Outlying Islands")]
                UnitedStatesMinorOutlyingIslands,
                [DescriptionAttribute("United States")]
                UnitedStates,
                [DescriptionAttribute("Uruguay")]
                Uruguay,
                [DescriptionAttribute("Uzbekistan")]
                Uzbekistan,
                [DescriptionAttribute("Vatican City")]
                VaticanCityHolySee,
                [DescriptionAttribute("Saint Vincent and the Grenadines")]
                SaintVincentandtheGrenadines,
                [DescriptionAttribute("Venezuela")]
                Venezuela,
                [DescriptionAttribute("Virgin Islands, British")]
                VirginIslandsBritish,
                [DescriptionAttribute("Virgin Islands, US")]
                VirginIslandsUS,
                [DescriptionAttribute("Vietnam")]
                Vietnam,
                [DescriptionAttribute("Vanuatu")]
                Vanuatu,
                [DescriptionAttribute("Wallis and Futuna")]
                WallisandFutuna,
                [DescriptionAttribute("Samoa")]
                Samoa,
                [DescriptionAttribute("Yemen")]
                Yemen,
                [DescriptionAttribute("Mayotte")]
                Mayotte,
                [DescriptionAttribute("South Africa")]
                SouthAfrica,
                [DescriptionAttribute("Zambia")]
                Zambia,
                [DescriptionAttribute("Zimbabwe")]
                Zimbabwe

            }

            [Obsolete("use 3 letter code where necessary")]
            public static string Country_ISO3166_To2LetterCode(Enum code)
            {
                if (code == null)
                {
                    return null;
                }
                else
                {
                    if (code is Country_ISO3166)
                        return Country_ISO3166_To2LetterCode((Country_ISO3166) code);
                    else
                        throw new InvalidOperationException("General.Enums:: code must be of type 'Country_ISO3166'");
                }
            }
            [Obsolete("use 3 letter code where necessary")]
            public static string Country_ISO3166_To2LetterCode(Country_ISO3166 code)
            {
                switch (code)
                {
                    case Country_ISO3166.Andorra: return "AD";
                    case Country_ISO3166.UnitedArabEmirates: return "AE";
                    case Country_ISO3166.Afghanistan: return "AF";
                    case Country_ISO3166.AntiguaandBarbuda: return "AG";
                    case Country_ISO3166.Anguilla: return "AI";
                    case Country_ISO3166.Albania: return "AL";
                    case Country_ISO3166.Armenia: return "AM";
                    case Country_ISO3166.Angola: return "AO";
                    case Country_ISO3166.Antarctica: return "AQ";
                    case Country_ISO3166.Argentina: return "AR";
                    case Country_ISO3166.AmericanSamoa: return "AS";
                    case Country_ISO3166.Austria: return "AT";
                    case Country_ISO3166.Australia: return "AU";
                    case Country_ISO3166.Aruba: return "AW";
                    case Country_ISO3166.AlandIslands: return "AX";
                    case Country_ISO3166.Azerbaijan: return "AZ";
                    case Country_ISO3166.BosniaandHerzegovina: return "BA";
                    case Country_ISO3166.Barbados: return "BB";
                    case Country_ISO3166.Bangladesh: return "BD";
                    case Country_ISO3166.Belgium: return "BE";
                    case Country_ISO3166.BurkinaFaso: return "BF";
                    case Country_ISO3166.Bulgaria: return "BG";
                    case Country_ISO3166.Bahrain: return "BH";
                    case Country_ISO3166.Burundi: return "BI";
                    case Country_ISO3166.Benin: return "BJ";
                    case Country_ISO3166.SaintBarth: return "BL";
                    case Country_ISO3166.Bermuda: return "BM";
                    case Country_ISO3166.BruneiDarussalam: return "BN";
                    case Country_ISO3166.Bolivia: return "BO";
                    case Country_ISO3166.Bonaire: return "BQ";
                    case Country_ISO3166.Brazil: return "BR";
                    case Country_ISO3166.Bahamas: return "BS";
                    case Country_ISO3166.Bhutan: return "BT";
                    case Country_ISO3166.BouvetIsland: return "BV";
                    case Country_ISO3166.Botswana: return "BW";
                    case Country_ISO3166.Belarus: return "BY";
                    case Country_ISO3166.Belize: return "BZ";
                    case Country_ISO3166.Canada: return "CA";
                    case Country_ISO3166.Cocos: return "CC";
                    case Country_ISO3166.CongoTheDemocraticRepublicOfThe: return "CD";
                    case Country_ISO3166.CentralAfricanRepublic: return "CF";
                    case Country_ISO3166.Congo: return "CG";
                    case Country_ISO3166.Switzerland: return "CH";
                    case Country_ISO3166.CoteDIvoireIvoryCoast: return "CI";
                    case Country_ISO3166.CookIslands: return "CK";
                    case Country_ISO3166.Chile: return "CL";
                    case Country_ISO3166.Cameroon: return "CM";
                    case Country_ISO3166.China: return "CN";
                    case Country_ISO3166.Colombia: return "CO";
                    case Country_ISO3166.CostaRica: return "CR";
                    case Country_ISO3166.Cuba: return "CU";
                    case Country_ISO3166.CapeVerde: return "CV";
                    case Country_ISO3166.Cura: return "CW";
                    case Country_ISO3166.ChristmasIsland: return "CX";
                    case Country_ISO3166.Cyprus: return "CY";
                    case Country_ISO3166.CzechRepublic: return "CZ";
                    case Country_ISO3166.Germany: return "DE";
                    case Country_ISO3166.Djibouti: return "DJ";
                    case Country_ISO3166.Denmark: return "DK";
                    case Country_ISO3166.Dominica: return "DM";
                    case Country_ISO3166.DominicanRepublic: return "DO";
                    case Country_ISO3166.Algeria: return "DZ";
                    case Country_ISO3166.Ecuador: return "EC";
                    case Country_ISO3166.Estonia: return "EE";
                    case Country_ISO3166.Egypt: return "EG";
                    case Country_ISO3166.WesternSahara: return "EH";
                    case Country_ISO3166.Eritrea: return "ER";
                    case Country_ISO3166.Spain: return "ES";
                    case Country_ISO3166.Ethiopia: return "ET";
                    case Country_ISO3166.Finland: return "FI";
                    case Country_ISO3166.Fiji: return "FJ";
                    case Country_ISO3166.FalklandIslands: return "FK";
                    case Country_ISO3166.Micronesia: return "FM";
                    case Country_ISO3166.FaroeIslands: return "FO";
                    case Country_ISO3166.France: return "FR";
                    case Country_ISO3166.Gabon: return "GA";
                    case Country_ISO3166.UnitedKingdom: return "GB";
                    case Country_ISO3166.Grenada: return "GD";
                    case Country_ISO3166.Georgia: return "GE";
                    case Country_ISO3166.FrenchGuiana: return "GF";
                    case Country_ISO3166.Guernsey: return "GG";
                    case Country_ISO3166.Ghana: return "GH";
                    case Country_ISO3166.Gibraltar: return "GI";
                    case Country_ISO3166.Greenland: return "GL";
                    case Country_ISO3166.Gambia: return "GM";
                    case Country_ISO3166.Guinea: return "GN";
                    case Country_ISO3166.Guadeloupe: return "GP";
                    case Country_ISO3166.EquatorialGuinea: return "GQ";
                    case Country_ISO3166.Greece: return "GR";
                    case Country_ISO3166.SouthGeorgiaandtheSouthSandwichIslands: return "GS";
                    case Country_ISO3166.Guatemala: return "GT";
                    case Country_ISO3166.Guam: return "GU";
                    case Country_ISO3166.GuineaBissau: return "GW";
                    case Country_ISO3166.Guyana: return "GY";
                    case Country_ISO3166.HongKong: return "HK";
                    case Country_ISO3166.HeardIslandandMcDonaldIslands: return "HM";
                    case Country_ISO3166.Honduras: return "HN";
                    case Country_ISO3166.Croatia: return "HR";
                    case Country_ISO3166.Haiti: return "HT";
                    case Country_ISO3166.Hungary: return "HU";
                    case Country_ISO3166.Indonesia: return "ID";
                    case Country_ISO3166.Ireland: return "IE";
                    case Country_ISO3166.Israel: return "IL";
                    case Country_ISO3166.IsleofMan: return "IM";
                    case Country_ISO3166.India: return "IN";
                    case Country_ISO3166.BritishIndianOceanTerritory: return "IO";
                    case Country_ISO3166.Iraq: return "IQ";
                    case Country_ISO3166.Iran: return "IR";
                    case Country_ISO3166.Iceland: return "IS";
                    case Country_ISO3166.Italy: return "IT";
                    case Country_ISO3166.Jersey: return "JE";
                    case Country_ISO3166.Jamaica: return "JM";
                    case Country_ISO3166.Jordan: return "JO";
                    case Country_ISO3166.Japan: return "JP";
                    case Country_ISO3166.Kenya: return "KE";
                    case Country_ISO3166.Kyrgyzstan: return "KG";
                    case Country_ISO3166.Cambodia: return "KH";
                    case Country_ISO3166.Kiribati: return "KI";
                    case Country_ISO3166.Comoros: return "KM";
                    case Country_ISO3166.SaintKittsandNevis: return "KN";
                    case Country_ISO3166.KoreaDemocraticPeoplesRepublicOf_NorthKorea: return "KP";
                    case Country_ISO3166.KoreaRepublicOf_SouthKorea: return "KR";
                    case Country_ISO3166.Kuwait: return "KW";
                    case Country_ISO3166.CaymanIslands: return "KY";
                    case Country_ISO3166.Kazakhstan: return "KZ";
                    case Country_ISO3166.Laos: return "LA";
                    case Country_ISO3166.Lebanon: return "LB";
                    case Country_ISO3166.SaintLucia: return "LC";
                    case Country_ISO3166.Liechtenstein: return "LI";
                    case Country_ISO3166.SriLanka: return "LK";
                    case Country_ISO3166.Liberia: return "LR";
                    case Country_ISO3166.Lesotho: return "LS";
                    case Country_ISO3166.Lithuania: return "LT";
                    case Country_ISO3166.Luxembourg: return "LU";
                    case Country_ISO3166.Latvia: return "LV";
                    case Country_ISO3166.LibyanArabJamahiriya: return "LY";
                    case Country_ISO3166.Morocco: return "MA";
                    case Country_ISO3166.Monaco: return "MC";
                    case Country_ISO3166.Moldova: return "MD";
                    case Country_ISO3166.Montenegro: return "ME";
                    case Country_ISO3166.SaintMartin: return "MF";
                    case Country_ISO3166.Madagascar: return "MG";
                    case Country_ISO3166.MarshallIslands: return "MH";
                    case Country_ISO3166.Macedonia: return "MK";
                    case Country_ISO3166.Mali: return "ML";
                    case Country_ISO3166.Myanmar: return "MM";
                    case Country_ISO3166.Mongolia: return "MN";
                    case Country_ISO3166.Macao: return "MO";
                    case Country_ISO3166.NorthernMarianaIslands: return "MP";
                    case Country_ISO3166.Martinique: return "MQ";
                    case Country_ISO3166.Mauritania: return "MR";
                    case Country_ISO3166.Montserrat: return "MS";
                    case Country_ISO3166.Malta: return "MT";
                    case Country_ISO3166.Mauritius: return "MU";
                    case Country_ISO3166.Maldives: return "MV";
                    case Country_ISO3166.Malawi: return "MW";
                    case Country_ISO3166.Mexico: return "MX";
                    case Country_ISO3166.Malaysia: return "MY";
                    case Country_ISO3166.Mozambique: return "MZ";
                    case Country_ISO3166.Namibia: return "NA";
                    case Country_ISO3166.NewCaledonia: return "NC";
                    case Country_ISO3166.Niger: return "NE";
                    case Country_ISO3166.NorfolkIsland: return "NF";
                    case Country_ISO3166.Nigeria: return "NG";
                    case Country_ISO3166.Nicaragua: return "NI";
                    case Country_ISO3166.Netherlands: return "NL";
                    case Country_ISO3166.Norway: return "NO";
                    case Country_ISO3166.Nepal: return "NP";
                    case Country_ISO3166.Nauru: return "NR";
                    case Country_ISO3166.Niue: return "NU";
                    case Country_ISO3166.NewZealand: return "NZ";
                    case Country_ISO3166.Oman: return "OM";
                    case Country_ISO3166.Panama: return "PA";
                    case Country_ISO3166.Peru: return "PE";
                    case Country_ISO3166.FrenchPolynesia: return "PF";
                    case Country_ISO3166.PapuaNewGuinea: return "PG";
                    case Country_ISO3166.Philippines: return "PH";
                    case Country_ISO3166.Pakistan: return "PK";
                    case Country_ISO3166.Poland: return "PL";
                    case Country_ISO3166.SaintPierreandMiquelon: return "PM";
                    case Country_ISO3166.Pitcairn: return "PN";
                    case Country_ISO3166.PuertoRico: return "PR";
                    case Country_ISO3166.PalestinianTerritory: return "PS";
                    case Country_ISO3166.Portugal: return "PT";
                    case Country_ISO3166.Palau: return "PW";
                    case Country_ISO3166.Paraguay: return "PY";
                    case Country_ISO3166.Qatar: return "QA";
                    case Country_ISO3166.Reunion: return "RE";
                    case Country_ISO3166.Romania: return "RO";
                    case Country_ISO3166.Serbia: return "RS";
                    case Country_ISO3166.RussianFederation: return "RU";
                    case Country_ISO3166.Rwanda: return "RW";
                    case Country_ISO3166.SaudiArabia: return "SA";
                    case Country_ISO3166.SolomonIslands: return "SB";
                    case Country_ISO3166.Seychelles: return "SC";
                    case Country_ISO3166.Sudan: return "SD";
                    case Country_ISO3166.Sweden: return "SE";
                    case Country_ISO3166.Singapore: return "SG";
                    case Country_ISO3166.SaintHelena: return "SH";
                    case Country_ISO3166.Slovenia: return "SI";
                    case Country_ISO3166.SvalbardandJanMayen: return "SJ";
                    case Country_ISO3166.Slovakia: return "SK";
                    case Country_ISO3166.SierraLeone: return "SL";
                    case Country_ISO3166.SanMarino: return "SM";
                    case Country_ISO3166.Senegal: return "SN";
                    case Country_ISO3166.Somalia: return "SO";
                    case Country_ISO3166.Suriname: return "SR";
                    case Country_ISO3166.SaoTomeandPrincipe: return "ST";
                    case Country_ISO3166.ElSalvador: return "SV";
                    case Country_ISO3166.SintMaarten: return "SX";
                    case Country_ISO3166.SyrianArabRepublic: return "SY";
                    case Country_ISO3166.Swaziland: return "SZ";
                    case Country_ISO3166.TurksandCaicosIslands: return "TC";
                    case Country_ISO3166.Chad: return "TD";
                    case Country_ISO3166.FrenchSouthernTerritories: return "TF";
                    case Country_ISO3166.Togo: return "TG";
                    case Country_ISO3166.Thailand: return "TH";
                    case Country_ISO3166.Tajikistan: return "TJ";
                    case Country_ISO3166.Tokelau: return "TK";
                    case Country_ISO3166.TimorLeste: return "TL";
                    case Country_ISO3166.Turkmenistan: return "TM";
                    case Country_ISO3166.Tunisia: return "TN";
                    case Country_ISO3166.Tonga: return "TO";
                    case Country_ISO3166.Turkey: return "TR";
                    case Country_ISO3166.TrinidadandTobago: return "TT";
                    case Country_ISO3166.Tuvalu: return "TV";
                    case Country_ISO3166.Taiwan: return "TW";
                    case Country_ISO3166.Tanzania: return "TZ";
                    case Country_ISO3166.Ukraine: return "UA";
                    case Country_ISO3166.Uganda: return "UG";
                    case Country_ISO3166.UnitedStatesMinorOutlyingIslands: return "UM";
                    case Country_ISO3166.UnitedStates: return "US";
                    case Country_ISO3166.Uruguay: return "UY";
                    case Country_ISO3166.Uzbekistan: return "UZ";
                    case Country_ISO3166.VaticanCityHolySee: return "VA";
                    case Country_ISO3166.SaintVincentandtheGrenadines: return "VC";
                    case Country_ISO3166.Venezuela: return "VE";
                    case Country_ISO3166.VirginIslandsBritish: return "VG";
                    case Country_ISO3166.VirginIslandsUS: return "VI";
                    case Country_ISO3166.Vietnam: return "VN";
                    case Country_ISO3166.Vanuatu: return "VU";
                    case Country_ISO3166.WallisandFutuna: return "WF";
                    case Country_ISO3166.Samoa: return "WS";
                    case Country_ISO3166.Yemen: return "YE";
                    case Country_ISO3166.Mayotte: return "YT";
                    case Country_ISO3166.SouthAfrica: return "ZA";
                    case Country_ISO3166.Zambia: return "ZM";
                    case Country_ISO3166.Zimbabwe: return "ZW";

                }
                return "";
            }
            
            private static Country_ISO3166? _country_ISO3166_From2LetterCode(string code)
            {
                if (code == null) code = "";
                code = code.ToUpper();
                switch (code)
                {
                    case "AD": return Country_ISO3166.Andorra;
                    case "AE": return Country_ISO3166.UnitedArabEmirates;
                    case "AF": return Country_ISO3166.Afghanistan;
                    case "AG": return Country_ISO3166.AntiguaandBarbuda;
                    case "AI": return Country_ISO3166.Anguilla;
                    case "AL": return Country_ISO3166.Albania;
                    case "AM": return Country_ISO3166.Armenia;
                    case "AO": return Country_ISO3166.Angola;
                    case "AQ": return Country_ISO3166.Antarctica;
                    case "AR": return Country_ISO3166.Argentina;
                    case "AS": return Country_ISO3166.AmericanSamoa;
                    case "AT": return Country_ISO3166.Austria;
                    case "AU": return Country_ISO3166.Australia;
                    case "AW": return Country_ISO3166.Aruba;
                    case "AX": return Country_ISO3166.AlandIslands;
                    case "AZ": return Country_ISO3166.Azerbaijan;
                    case "BA": return Country_ISO3166.BosniaandHerzegovina;
                    case "BB": return Country_ISO3166.Barbados;
                    case "BD": return Country_ISO3166.Bangladesh;
                    case "BE": return Country_ISO3166.Belgium;
                    case "BF": return Country_ISO3166.BurkinaFaso;
                    case "BG": return Country_ISO3166.Bulgaria;
                    case "BH": return Country_ISO3166.Bahrain;
                    case "BI": return Country_ISO3166.Burundi;
                    case "BJ": return Country_ISO3166.Benin;
                    case "BL": return Country_ISO3166.SaintBarth;
                    case "BM": return Country_ISO3166.Bermuda;
                    case "BN": return Country_ISO3166.BruneiDarussalam;
                    case "BO": return Country_ISO3166.Bolivia;
                    case "BQ": return Country_ISO3166.Bonaire;
                    case "BR": return Country_ISO3166.Brazil;
                    case "BS": return Country_ISO3166.Bahamas;
                    case "BT": return Country_ISO3166.Bhutan;
                    case "BV": return Country_ISO3166.BouvetIsland;
                    case "BW": return Country_ISO3166.Botswana;
                    case "BY": return Country_ISO3166.Belarus;
                    case "BZ": return Country_ISO3166.Belize;
                    case "CA": return Country_ISO3166.Canada;
                    case "CC": return Country_ISO3166.Cocos;
                    case "CD": return Country_ISO3166.CongoTheDemocraticRepublicOfThe;
                    case "CF": return Country_ISO3166.CentralAfricanRepublic;
                    case "CG": return Country_ISO3166.Congo;
                    case "CH": return Country_ISO3166.Switzerland;
                    case "CI": return Country_ISO3166.CoteDIvoireIvoryCoast;
                    case "CK": return Country_ISO3166.CookIslands;
                    case "CL": return Country_ISO3166.Chile;
                    case "CM": return Country_ISO3166.Cameroon;
                    case "CN": return Country_ISO3166.China;
                    case "CO": return Country_ISO3166.Colombia;
                    case "CR": return Country_ISO3166.CostaRica;
                    case "CU": return Country_ISO3166.Cuba;
                    case "CV": return Country_ISO3166.CapeVerde;
                    case "CW": return Country_ISO3166.Cura;
                    case "CX": return Country_ISO3166.ChristmasIsland;
                    case "CY": return Country_ISO3166.Cyprus;
                    case "CZ": return Country_ISO3166.CzechRepublic;
                    case "DE": return Country_ISO3166.Germany;
                    case "DJ": return Country_ISO3166.Djibouti;
                    case "DK": return Country_ISO3166.Denmark;
                    case "DM": return Country_ISO3166.Dominica;
                    case "DO": return Country_ISO3166.DominicanRepublic;
                    case "DZ": return Country_ISO3166.Algeria;
                    case "EC": return Country_ISO3166.Ecuador;
                    case "EE": return Country_ISO3166.Estonia;
                    case "EG": return Country_ISO3166.Egypt;
                    case "EH": return Country_ISO3166.WesternSahara;
                    case "ER": return Country_ISO3166.Eritrea;
                    case "ES": return Country_ISO3166.Spain;
                    case "ET": return Country_ISO3166.Ethiopia;
                    case "FI": return Country_ISO3166.Finland;
                    case "FJ": return Country_ISO3166.Fiji;
                    case "FK": return Country_ISO3166.FalklandIslands;
                    case "FM": return Country_ISO3166.Micronesia;
                    case "FO": return Country_ISO3166.FaroeIslands;
                    case "FR": return Country_ISO3166.France;
                    case "GA": return Country_ISO3166.Gabon;
                    case "GB": return Country_ISO3166.UnitedKingdom;
                    case "GD": return Country_ISO3166.Grenada;
                    case "GE": return Country_ISO3166.Georgia;
                    case "GF": return Country_ISO3166.FrenchGuiana;
                    case "GG": return Country_ISO3166.Guernsey;
                    case "GH": return Country_ISO3166.Ghana;
                    case "GI": return Country_ISO3166.Gibraltar;
                    case "GL": return Country_ISO3166.Greenland;
                    case "GM": return Country_ISO3166.Gambia;
                    case "GN": return Country_ISO3166.Guinea;
                    case "GP": return Country_ISO3166.Guadeloupe;
                    case "GQ": return Country_ISO3166.EquatorialGuinea;
                    case "GR": return Country_ISO3166.Greece;
                    case "GS": return Country_ISO3166.SouthGeorgiaandtheSouthSandwichIslands;
                    case "GT": return Country_ISO3166.Guatemala;
                    case "GU": return Country_ISO3166.Guam;
                    case "GW": return Country_ISO3166.GuineaBissau;
                    case "GY": return Country_ISO3166.Guyana;
                    case "HK": return Country_ISO3166.HongKong;
                    case "HM": return Country_ISO3166.HeardIslandandMcDonaldIslands;
                    case "HN": return Country_ISO3166.Honduras;
                    case "HR": return Country_ISO3166.Croatia;
                    case "HT": return Country_ISO3166.Haiti;
                    case "HU": return Country_ISO3166.Hungary;
                    case "ID": return Country_ISO3166.Indonesia;
                    case "IE": return Country_ISO3166.Ireland;
                    case "IL": return Country_ISO3166.Israel;
                    case "IM": return Country_ISO3166.IsleofMan;
                    case "IN": return Country_ISO3166.India;
                    case "IO": return Country_ISO3166.BritishIndianOceanTerritory;
                    case "IQ": return Country_ISO3166.Iraq;
                    case "IR": return Country_ISO3166.Iran;
                    case "IS": return Country_ISO3166.Iceland;
                    case "IT": return Country_ISO3166.Italy;
                    case "JE": return Country_ISO3166.Jersey;
                    case "JM": return Country_ISO3166.Jamaica;
                    case "JO": return Country_ISO3166.Jordan;
                    case "JP": return Country_ISO3166.Japan;
                    case "KE": return Country_ISO3166.Kenya;
                    case "KG": return Country_ISO3166.Kyrgyzstan;
                    case "KH": return Country_ISO3166.Cambodia;
                    case "KI": return Country_ISO3166.Kiribati;
                    case "KM": return Country_ISO3166.Comoros;
                    case "KN": return Country_ISO3166.SaintKittsandNevis;
                    case "KP": return Country_ISO3166.KoreaDemocraticPeoplesRepublicOf_NorthKorea;
                    case "KR": return Country_ISO3166.KoreaRepublicOf_SouthKorea;
                    case "KW": return Country_ISO3166.Kuwait;
                    case "KY": return Country_ISO3166.CaymanIslands;
                    case "KZ": return Country_ISO3166.Kazakhstan;
                    case "LA": return Country_ISO3166.Laos;
                    case "LB": return Country_ISO3166.Lebanon;
                    case "LC": return Country_ISO3166.SaintLucia;
                    case "LI": return Country_ISO3166.Liechtenstein;
                    case "LK": return Country_ISO3166.SriLanka;
                    case "LR": return Country_ISO3166.Liberia;
                    case "LS": return Country_ISO3166.Lesotho;
                    case "LT": return Country_ISO3166.Lithuania;
                    case "LU": return Country_ISO3166.Luxembourg;
                    case "LV": return Country_ISO3166.Latvia;
                    case "LY": return Country_ISO3166.LibyanArabJamahiriya;
                    case "MA": return Country_ISO3166.Morocco;
                    case "MC": return Country_ISO3166.Monaco;
                    case "MD": return Country_ISO3166.Moldova;
                    case "ME": return Country_ISO3166.Montenegro;
                    case "MF": return Country_ISO3166.SaintMartin;
                    case "MG": return Country_ISO3166.Madagascar;
                    case "MH": return Country_ISO3166.MarshallIslands;
                    case "MK": return Country_ISO3166.Macedonia;
                    case "ML": return Country_ISO3166.Mali;
                    case "MM": return Country_ISO3166.Myanmar;
                    case "MN": return Country_ISO3166.Mongolia;
                    case "MO": return Country_ISO3166.Macao;
                    case "MP": return Country_ISO3166.NorthernMarianaIslands;
                    case "MQ": return Country_ISO3166.Martinique;
                    case "MR": return Country_ISO3166.Mauritania;
                    case "MS": return Country_ISO3166.Montserrat;
                    case "MT": return Country_ISO3166.Malta;
                    case "MU": return Country_ISO3166.Mauritius;
                    case "MV": return Country_ISO3166.Maldives;
                    case "MW": return Country_ISO3166.Malawi;
                    case "MX": return Country_ISO3166.Mexico;
                    case "MY": return Country_ISO3166.Malaysia;
                    case "MZ": return Country_ISO3166.Mozambique;
                    case "NA": return Country_ISO3166.Namibia;
                    case "NC": return Country_ISO3166.NewCaledonia;
                    case "NE": return Country_ISO3166.Niger;
                    case "NF": return Country_ISO3166.NorfolkIsland;
                    case "NG": return Country_ISO3166.Nigeria;
                    case "NI": return Country_ISO3166.Nicaragua;
                    case "NL": return Country_ISO3166.Netherlands;
                    case "NO": return Country_ISO3166.Norway;
                    case "NP": return Country_ISO3166.Nepal;
                    case "NR": return Country_ISO3166.Nauru;
                    case "NU": return Country_ISO3166.Niue;
                    case "NZ": return Country_ISO3166.NewZealand;
                    case "OM": return Country_ISO3166.Oman;
                    case "PA": return Country_ISO3166.Panama;
                    case "PE": return Country_ISO3166.Peru;
                    case "PF": return Country_ISO3166.FrenchPolynesia;
                    case "PG": return Country_ISO3166.PapuaNewGuinea;
                    case "PH": return Country_ISO3166.Philippines;
                    case "PK": return Country_ISO3166.Pakistan;
                    case "PL": return Country_ISO3166.Poland;
                    case "PM": return Country_ISO3166.SaintPierreandMiquelon;
                    case "PN": return Country_ISO3166.Pitcairn;
                    case "PR": return Country_ISO3166.PuertoRico;
                    case "PS": return Country_ISO3166.PalestinianTerritory;
                    case "PT": return Country_ISO3166.Portugal;
                    case "PW": return Country_ISO3166.Palau;
                    case "PY": return Country_ISO3166.Paraguay;
                    case "QA": return Country_ISO3166.Qatar;
                    case "RE": return Country_ISO3166.Reunion;
                    case "RO": return Country_ISO3166.Romania;
                    case "RS": return Country_ISO3166.Serbia;
                    case "RU": return Country_ISO3166.RussianFederation;
                    case "RW": return Country_ISO3166.Rwanda;
                    case "SA": return Country_ISO3166.SaudiArabia;
                    case "SB": return Country_ISO3166.SolomonIslands;
                    case "SC": return Country_ISO3166.Seychelles;
                    case "SD": return Country_ISO3166.Sudan;
                    case "SE": return Country_ISO3166.Sweden;
                    case "SG": return Country_ISO3166.Singapore;
                    case "SH": return Country_ISO3166.SaintHelena;
                    case "SI": return Country_ISO3166.Slovenia;
                    case "SJ": return Country_ISO3166.SvalbardandJanMayen;
                    case "SK": return Country_ISO3166.Slovakia;
                    case "SL": return Country_ISO3166.SierraLeone;
                    case "SM": return Country_ISO3166.SanMarino;
                    case "SN": return Country_ISO3166.Senegal;
                    case "SO": return Country_ISO3166.Somalia;
                    case "SR": return Country_ISO3166.Suriname;
                    case "ST": return Country_ISO3166.SaoTomeandPrincipe;
                    case "SV": return Country_ISO3166.ElSalvador;
                    case "SX": return Country_ISO3166.SintMaarten;
                    case "SY": return Country_ISO3166.SyrianArabRepublic;
                    case "SZ": return Country_ISO3166.Swaziland;
                    case "TC": return Country_ISO3166.TurksandCaicosIslands;
                    case "TD": return Country_ISO3166.Chad;
                    case "TF": return Country_ISO3166.FrenchSouthernTerritories;
                    case "TG": return Country_ISO3166.Togo;
                    case "TH": return Country_ISO3166.Thailand;
                    case "TJ": return Country_ISO3166.Tajikistan;
                    case "TK": return Country_ISO3166.Tokelau;
                    case "TL": return Country_ISO3166.TimorLeste;
                    case "TM": return Country_ISO3166.Turkmenistan;
                    case "TN": return Country_ISO3166.Tunisia;
                    case "TO": return Country_ISO3166.Tonga;
                    case "TR": return Country_ISO3166.Turkey;
                    case "TT": return Country_ISO3166.TrinidadandTobago;
                    case "TV": return Country_ISO3166.Tuvalu;
                    case "TW": return Country_ISO3166.Taiwan;
                    case "TZ": return Country_ISO3166.Tanzania;
                    case "UA": return Country_ISO3166.Ukraine;
                    case "UG": return Country_ISO3166.Uganda;
                    case "UM": return Country_ISO3166.UnitedStatesMinorOutlyingIslands;
                    case "US": return Country_ISO3166.UnitedStates;
                    case "UY": return Country_ISO3166.Uruguay;
                    case "UZ": return Country_ISO3166.Uzbekistan;
                    case "VA": return Country_ISO3166.VaticanCityHolySee;
                    case "VC": return Country_ISO3166.SaintVincentandtheGrenadines;
                    case "VE": return Country_ISO3166.Venezuela;
                    case "VG": return Country_ISO3166.VirginIslandsBritish;
                    case "VI": return Country_ISO3166.VirginIslandsUS;
                    case "VN": return Country_ISO3166.Vietnam;
                    case "VU": return Country_ISO3166.Vanuatu;
                    case "WF": return Country_ISO3166.WallisandFutuna;
                    case "WS": return Country_ISO3166.Samoa;
                    case "YE": return Country_ISO3166.Yemen;
                    case "YT": return Country_ISO3166.Mayotte;
                    case "ZA": return Country_ISO3166.SouthAfrica;
                    case "ZM": return Country_ISO3166.Zambia;
                    case "ZW": return Country_ISO3166.Zimbabwe;

                }
                return null;
            }
            //public static string Country_ISO3166_To3LetterCode(Enum code)
            //{
            //    if (code is Country_ISO3166)
            //    {
            //        return Country_ISO3166_To3LetterCode((Country_ISO3166)code);
            //    }
            //    else
            //    {
            //        throw new InvalidOperationException("invalid value");
            //    }
            //}
            public static string Country_ISO3166_To3LetterCode(Country_ISO3166? code)
            {
                if (code.HasValue)
                {
                    switch (code)
                    {
                        case Country_ISO3166.UnitedArabEmirates:
                            return "ARE";
                        case Country_ISO3166.Spain:
                            return "ESP";
                        case Country_ISO3166.Afghanistan:
                            return "AFG";
                        case Country_ISO3166.AntiguaandBarbuda:
                            return "ATG";
                        case Country_ISO3166.Anguilla:
                            return "AIA";
                        case Country_ISO3166.Albania:
                            return "ALB";
                        case Country_ISO3166.Armenia:
                            return "ARM";
                        case Country_ISO3166.Angola:
                            return "AGO";
                        case Country_ISO3166.Argentina:
                            return "ARG";
                        case Country_ISO3166.Austria:
                            return "AUT";
                        case Country_ISO3166.Australia:
                            return "AUS";
                        case Country_ISO3166.Azerbaijan:
                            return "AZE";
                        case Country_ISO3166.BosniaandHerzegovina:
                            return "BIH";
                        case Country_ISO3166.Barbados:
                            return "BRB";
                        case Country_ISO3166.Bangladesh:
                            return "BGD";
                        case Country_ISO3166.Belgium:
                            return "BEL";
                        case Country_ISO3166.BurkinaFaso:
                            return "BFA";
                        case Country_ISO3166.Bulgaria:
                            return "BGR";
                        case Country_ISO3166.Bahrain:
                            return "BHR";
                        case Country_ISO3166.Burundi:
                            return "BDI";
                        case Country_ISO3166.Benin:
                            return "BEN";
                        case Country_ISO3166.Bermuda:
                            return "BMU";
                        case Country_ISO3166.BruneiDarussalam:
                            return "BRN";
                        case Country_ISO3166.Bolivia:
                            return "BOL";
                        case Country_ISO3166.Brazil:
                            return "BRA";
                        case Country_ISO3166.Bahamas:
                            return "BHS";
                        case Country_ISO3166.Bhutan:
                            return "BTN";
                        case Country_ISO3166.Botswana:
                            return "BWA";
                        case Country_ISO3166.Belarus:
                            return "BLR";
                        case Country_ISO3166.Belize:
                            return "BLZ";
                        case Country_ISO3166.Canada:
                            return "CAN";
                        case Country_ISO3166.CongoTheDemocraticRepublicOfThe:
                            return "COD";
                        case Country_ISO3166.CentralAfricanRepublic:
                            return "CAF";
                        case Country_ISO3166.Congo:
                            return "COG";
                        case Country_ISO3166.Switzerland:
                            return "CHE";
                        case Country_ISO3166.CoteDIvoireIvoryCoast:
                            return "CIV";
                        case Country_ISO3166.CookIslands:
                            return "COK";
                        case Country_ISO3166.Chile:
                            return "CHL";
                        case Country_ISO3166.Cameroon:
                            return "CMR";
                        case Country_ISO3166.China:
                            return "CHN";
                        case Country_ISO3166.Colombia:
                            return "COL";
                        case Country_ISO3166.CostaRica:
                            return "CRI";
                        case Country_ISO3166.Cuba:
                            return "CUB";
                        case Country_ISO3166.CapeVerde:
                            return "CPV";
                        case Country_ISO3166.Cyprus:
                            return "CYP";
                        case Country_ISO3166.CzechRepublic:
                            return "CZE";
                        case Country_ISO3166.Germany:
                            return "DEU";
                        case Country_ISO3166.Djibouti:
                            return "DJI";
                        case Country_ISO3166.Denmark:
                            return "DNK";
                        case Country_ISO3166.Dominica:
                            return "DMA";
                        case Country_ISO3166.DominicanRepublic:
                            return "DOM";
                        case Country_ISO3166.Algeria:
                            return "DZA";
                        case Country_ISO3166.Ecuador:
                            return "ECU";
                        case Country_ISO3166.Estonia:
                            return "EST";
                        case Country_ISO3166.Egypt:
                            return "EGY";
                        case Country_ISO3166.Eritrea:
                            return "ERI";
                        case Country_ISO3166.Ethiopia:
                            return "ETH";
                        case Country_ISO3166.Finland:
                            return "FIN";
                        case Country_ISO3166.Fiji:
                            return "FJI";
                        case Country_ISO3166.FalklandIslands:
                            return "FLK";
                        case Country_ISO3166.Micronesia:
                            return "FSM";
                        case Country_ISO3166.France:
                            return "FRA";
                        case Country_ISO3166.Gabon:
                            return "GAB";
                        case Country_ISO3166.UnitedKingdom:
                            return "GBR";
                        case Country_ISO3166.Grenada:
                            return "GRD";
                        case Country_ISO3166.Georgia:
                            return "GEO";
                        case Country_ISO3166.FrenchGuiana:
                            return "GUF";
                        case Country_ISO3166.Ghana:
                            return "GHA";
                        case Country_ISO3166.Greenland:
                            return "GRL";
                        case Country_ISO3166.Gambia:
                            return "GMB";
                        case Country_ISO3166.Guinea:
                            return "GIN";
                        case Country_ISO3166.EquatorialGuinea:
                            return "GNQ";
                        case Country_ISO3166.Greece:
                            return "GRC";
                        case Country_ISO3166.Guatemala:
                            return "GTM";
                        case Country_ISO3166.GuineaBissau:
                            return "GNB";
                        case Country_ISO3166.Guyana:
                            return "GUY";
                        case Country_ISO3166.HongKong:
                            return "HKG";
                        case Country_ISO3166.Honduras:
                            return "HND";
                        case Country_ISO3166.Croatia:
                            return "HRV";
                        case Country_ISO3166.Haiti:
                            return "HTI";
                        case Country_ISO3166.Hungary:
                            return "HUN";
                        case Country_ISO3166.Indonesia:
                            return "IDN";
                        case Country_ISO3166.Ireland:
                            return "IRL";
                        case Country_ISO3166.Israel:
                            return "ISR";
                        case Country_ISO3166.India:
                            return "IND";
                        case Country_ISO3166.BritishIndianOceanTerritory:
                            return "IOT";
                        case Country_ISO3166.Iraq:
                            return "IRQ";
                        case Country_ISO3166.Iran:
                            return "IRN";
                        case Country_ISO3166.Iceland:
                            return "ISL";
                        case Country_ISO3166.Italy:
                            return "ITA";
                        case Country_ISO3166.Jamaica:
                            return "JAM";
                        case Country_ISO3166.Jordan:
                            return "JOR";
                        case Country_ISO3166.Japan:
                            return "JPN";
                        case Country_ISO3166.Kenya:
                            return "KEN";
                        case Country_ISO3166.Cambodia:
                            return "KHM";
                        case Country_ISO3166.Kiribati:
                            return "KIR";
                        case Country_ISO3166.SaintKittsandNevis:
                            return "KNA";
                        case Country_ISO3166.KoreaDemocraticPeoplesRepublicOf_NorthKorea:
                            return "PRK";
                        case Country_ISO3166.KoreaRepublicOf_SouthKorea:
                            return "KOR";
                        case Country_ISO3166.Kuwait:
                            return "KWT";
                        case Country_ISO3166.CaymanIslands:
                            return "CYM";
                        case Country_ISO3166.Kazakhstan:
                            return "KAZ";
                        case Country_ISO3166.Laos:
                            return "LAO";
                        case Country_ISO3166.SaintLucia:
                            return "LCA";
                        case Country_ISO3166.SriLanka:
                            return "LKA";
                        case Country_ISO3166.Lebanon:
                            return "LBN";
                        case Country_ISO3166.Liberia:
                            return "LBR";
                        case Country_ISO3166.Lesotho:
                            return "LSO";
                        case Country_ISO3166.Lithuania:
                            return "LTU";
                        case Country_ISO3166.Luxembourg:
                            return "LUX";
                        case Country_ISO3166.Latvia:
                            return "LVA";
                        case Country_ISO3166.LibyanArabJamahiriya:
                            return "LBY";
                        case Country_ISO3166.Morocco:
                            return "MAR";
                        case Country_ISO3166.Moldova:
                            return "MDA";
                        case Country_ISO3166.Madagascar:
                            return "MDG";
                        case Country_ISO3166.MarshallIslands:
                            return "MHL";
                        case Country_ISO3166.Macedonia:
                            return "MKD";
                        case Country_ISO3166.Mali:
                            return "MLI";
                        case Country_ISO3166.Myanmar:
                            return "MMR";
                        case Country_ISO3166.Mongolia:
                            return "MNG";
                        case Country_ISO3166.Macao:
                            return "MAC";
                        case Country_ISO3166.Mauritania:
                            return "MRT";
                        case Country_ISO3166.Montserrat:
                            return "MSR";
                        case Country_ISO3166.Malta:
                            return "MLT";
                        case Country_ISO3166.Mauritius:
                            return "MUS";
                        case Country_ISO3166.Maldives:
                            return "MDV";
                        case Country_ISO3166.Malawi:
                            return "MWI";
                        case Country_ISO3166.Mexico:
                            return "MEX";
                        case Country_ISO3166.Malaysia:
                            return "MYS";
                        case Country_ISO3166.Mozambique:
                            return "MOZ";
                        case Country_ISO3166.Namibia:
                            return "NAM";
                        case Country_ISO3166.NewCaledonia:
                            return "NCL";
                        case Country_ISO3166.Niger:
                            return "NER";
                        case Country_ISO3166.Nigeria:
                            return "NGA";
                        case Country_ISO3166.Nicaragua:
                            return "NIC";
                        case Country_ISO3166.Netherlands:
                            return "NLD";
                        case Country_ISO3166.Norway:
                            return "NOR";
                        case Country_ISO3166.Nepal:
                            return "NPL";
                        case Country_ISO3166.Nauru:
                            return "NRU";
                        case Country_ISO3166.Niue:
                            return "NIU";
                        case Country_ISO3166.NewZealand:
                            return "NZL";
                        case Country_ISO3166.Oman:
                            return "OMN";
                        case Country_ISO3166.Panama:
                            return "PAN";
                        case Country_ISO3166.Peru:
                            return "PER";
                        case Country_ISO3166.FrenchPolynesia:
                            return "PYF";
                        case Country_ISO3166.PapuaNewGuinea:
                            return "PNG";
                        case Country_ISO3166.Philippines:
                            return "PHL";
                        case Country_ISO3166.Pakistan:
                            return "PAK";
                        case Country_ISO3166.Poland:
                            return "POL";
                        case Country_ISO3166.PuertoRico:
                            return "PRI";
                        case Country_ISO3166.Portugal:
                            return "PRT";
                        case Country_ISO3166.Paraguay:
                            return "PRY";
                        case Country_ISO3166.Qatar:
                            return "QAT";
                        case Country_ISO3166.Romania:
                            return "ROM";
                        case Country_ISO3166.RussianFederation:
                            return "RUS";
                        case Country_ISO3166.Rwanda:
                            return "RWA";
                        case Country_ISO3166.SaudiArabia:
                            return "SAU";
                        case Country_ISO3166.SolomonIslands:
                            return "SLB";
                        case Country_ISO3166.Seychelles:
                            return "SYC";
                        case Country_ISO3166.Sudan:
                            return "SDN";
                        case Country_ISO3166.Sweden:
                            return "SWE";
                        case Country_ISO3166.Singapore:
                            return "SGP";
                        case Country_ISO3166.Slovenia:
                            return "SVN";
                        case Country_ISO3166.Slovakia:
                            return "SVK";
                        case Country_ISO3166.SierraLeone:
                            return "SLE";
                        case Country_ISO3166.Senegal:
                            return "SEN";
                        case Country_ISO3166.Somalia:
                            return "SOM";
                        case Country_ISO3166.Suriname:
                            return "SUR";
                        case Country_ISO3166.SaoTomeandPrincipe:
                            return "STP";
                        case Country_ISO3166.ElSalvador:
                            return "SLV";
                        case Country_ISO3166.SyrianArabRepublic:
                            return "SYR";
                        case Country_ISO3166.Swaziland:
                            return "SWZ";
                        case Country_ISO3166.TurksandCaicosIslands:
                            return "TCA";
                        case Country_ISO3166.Chad:
                            return "TCD";
                        case Country_ISO3166.Togo:
                            return "TGO";
                        case Country_ISO3166.Thailand:
                            return "THA";
                        case Country_ISO3166.TimorLeste:
                            return "TLS";
                        case Country_ISO3166.Tunisia:
                            return "TUN";
                        case Country_ISO3166.Turkey:
                            return "TUR";
                        case Country_ISO3166.TrinidadandTobago:
                            return "TTO";
                        case Country_ISO3166.Taiwan:
                            return "TWN";
                        case Country_ISO3166.Tanzania:
                            return "TZA";
                        case Country_ISO3166.Ukraine:
                            return "UKR";
                        case Country_ISO3166.Uganda:
                            return "UGA";
                        case Country_ISO3166.UnitedStatesMinorOutlyingIslands:
                            return "UMI";
                        case Country_ISO3166.UnitedStates:
                            return "USA";
                        case Country_ISO3166.Uruguay:
                            return "URY";
                        case Country_ISO3166.Uzbekistan:
                            return "UZB";
                        case Country_ISO3166.SaintVincentandtheGrenadines:
                            return "VCT";
                        case Country_ISO3166.Venezuela:
                            return "VEN";
                        case Country_ISO3166.VirginIslandsBritish:
                            return "VGB";
                        case Country_ISO3166.VirginIslandsUS:
                            return "VIR";
                        case Country_ISO3166.Vietnam:
                            return "VNM";
                        case Country_ISO3166.Vanuatu:
                            return "VUT";
                        case Country_ISO3166.WallisandFutuna:
                            return "WLF";
                        case Country_ISO3166.Samoa:
                            return "WSM";
                        case Country_ISO3166.Yemen:
                            return "YEM";
                        case Country_ISO3166.SouthAfrica:
                            return "ZAF";
                        case Country_ISO3166.Zambia:
                            return "ZMB";
                        case Country_ISO3166.Zimbabwe:
                            return "ZWE";

                    }
                }
                return "";
            }
            
            public static Country_ISO3166? Country_ISO3166_FromCode(string code)
            {
                Country_ISO3166? result = null;
                code = code ?? "";
                code = code.Trim();
                if (code.Length == 2)
                    result = _country_ISO3166_From2LetterCode(code);
                else
                    result = _country_ISO3166_From3LetterCode(code);
                if (result == null)
                {
                    result = (Country_ISO3166?) CS.General_v3.Util.EnumUtils.EnumValueOf(typeof(Country_ISO3166), code);
                }
                return result;
            }
            
            private static Country_ISO3166? _country_ISO3166_From3LetterCode(string code)
            {
                if (code == null)
                    code = "";
                code = code.ToUpper();
                switch (code)
                {
                    case "ESP": return Country_ISO3166.Spain;
                    case "ARE": return Country_ISO3166.UnitedArabEmirates;
                    case "AFG": return Country_ISO3166.Afghanistan;
                    case "ATG": return Country_ISO3166.AntiguaandBarbuda;
                    case "AIA": return Country_ISO3166.Anguilla;
                    case "ALB": return Country_ISO3166.Albania;
                    case "ARM": return Country_ISO3166.Armenia;
                    case "AGO": return Country_ISO3166.Angola;
                    case "ARG": return Country_ISO3166.Argentina;
                    case "AUT": return Country_ISO3166.Austria;
                    case "AUS": return Country_ISO3166.Australia;
                    case "AZE": return Country_ISO3166.Azerbaijan;
                    case "BIH": return Country_ISO3166.BosniaandHerzegovina;
                    case "BRB": return Country_ISO3166.Barbados;
                    case "BGD": return Country_ISO3166.Bangladesh;
                    case "BEL": return Country_ISO3166.Belgium;
                    case "BFA": return Country_ISO3166.BurkinaFaso;
                    case "BGR": return Country_ISO3166.Bulgaria;
                    case "BHR": return Country_ISO3166.Bahrain;
                    case "BDI": return Country_ISO3166.Burundi;
                    case "BEN": return Country_ISO3166.Benin;
                    case "BMU": return Country_ISO3166.Bermuda;
                    case "BRN": return Country_ISO3166.BruneiDarussalam;
                    case "BOL": return Country_ISO3166.Bolivia;
                    case "BRA": return Country_ISO3166.Brazil;
                    case "BHS": return Country_ISO3166.Bahamas;
                    case "BTN": return Country_ISO3166.Bhutan;
                    case "BWA": return Country_ISO3166.Botswana;
                    case "BLR": return Country_ISO3166.Belarus;
                    case "BLZ": return Country_ISO3166.Belize;
                    case "CAN": return Country_ISO3166.Canada;
                    case "COD": return Country_ISO3166.CongoTheDemocraticRepublicOfThe;
                    case "CAF": return Country_ISO3166.CentralAfricanRepublic;
                    case "COG": return Country_ISO3166.Congo;
                    case "CHE": return Country_ISO3166.Switzerland;
                    case "CIV": return Country_ISO3166.CoteDIvoireIvoryCoast;
                    case "COK": return Country_ISO3166.CookIslands;
                    case "CHL": return Country_ISO3166.Chile;
                    case "CMR": return Country_ISO3166.Cameroon;
                    case "CHN": return Country_ISO3166.China;
                    case "COL": return Country_ISO3166.Colombia;
                    case "CRI": return Country_ISO3166.CostaRica;
                    case "CUB": return Country_ISO3166.Cuba;
                    case "CPV": return Country_ISO3166.CapeVerde;
                    case "CYP": return Country_ISO3166.Cyprus;
                    case "CZE": return Country_ISO3166.CzechRepublic;
                    case "DEU": return Country_ISO3166.Germany;
                    case "DJI": return Country_ISO3166.Djibouti;
                    case "DNK": return Country_ISO3166.Denmark;
                    case "DMA": return Country_ISO3166.Dominica;
                    case "DOM": return Country_ISO3166.DominicanRepublic;
                    case "DZA": return Country_ISO3166.Algeria;
                    case "ECU": return Country_ISO3166.Ecuador;
                    case "EST": return Country_ISO3166.Estonia;
                    case "EGY": return Country_ISO3166.Egypt;
                    case "ERI": return Country_ISO3166.Eritrea;
                    case "ETH": return Country_ISO3166.Ethiopia;
                    case "FIN": return Country_ISO3166.Finland;
                    case "FJI": return Country_ISO3166.Fiji;
                    case "FLK": return Country_ISO3166.FalklandIslands;
                    case "FSM": return Country_ISO3166.Micronesia;
                    case "FRA": return Country_ISO3166.France;
                    case "GAB": return Country_ISO3166.Gabon;
                    case "GBR": return Country_ISO3166.UnitedKingdom;
                    case "GRD": return Country_ISO3166.Grenada;
                    case "GEO": return Country_ISO3166.Georgia;
                    case "GUF": return Country_ISO3166.FrenchGuiana;
                    case "GHA": return Country_ISO3166.Ghana;
                    case "GRL": return Country_ISO3166.Greenland;
                    case "GMB": return Country_ISO3166.Gambia;
                    case "GIN": return Country_ISO3166.Guinea;
                    case "GNQ": return Country_ISO3166.EquatorialGuinea;
                    case "GRC": return Country_ISO3166.Greece;
                    case "GTM": return Country_ISO3166.Guatemala;
                    case "GNB": return Country_ISO3166.GuineaBissau;
                    case "GUY": return Country_ISO3166.Guyana;
                    case "HKG": return Country_ISO3166.HongKong;
                    case "HND": return Country_ISO3166.Honduras;
                    case "HRV": return Country_ISO3166.Croatia;
                    case "HTI": return Country_ISO3166.Haiti;
                    case "HUN": return Country_ISO3166.Hungary;
                    case "IDN": return Country_ISO3166.Indonesia;
                    case "IRL": return Country_ISO3166.Ireland;
                    case "ISR": return Country_ISO3166.Israel;
                    case "IND": return Country_ISO3166.India;
                    case "IOT": return Country_ISO3166.BritishIndianOceanTerritory;
                    case "IRQ": return Country_ISO3166.Iraq;
                    case "IRN": return Country_ISO3166.Iran;
                    case "ISL": return Country_ISO3166.Iceland;
                    case "ITA": return Country_ISO3166.Italy;
                    case "JAM": return Country_ISO3166.Jamaica;
                    case "JOR": return Country_ISO3166.Jordan;
                    case "JPN": return Country_ISO3166.Japan;
                    case "KEN": return Country_ISO3166.Kenya;
                    case "KHM": return Country_ISO3166.Cambodia;
                    case "KIR": return Country_ISO3166.Kiribati;
                    case "KNA": return Country_ISO3166.SaintKittsandNevis;
                    case "PRK": return Country_ISO3166.KoreaDemocraticPeoplesRepublicOf_NorthKorea;
                    case "KOR": return Country_ISO3166.KoreaRepublicOf_SouthKorea;
                    case "KWT": return Country_ISO3166.Kuwait;
                    case "CYM": return Country_ISO3166.CaymanIslands;
                    case "KAZ": return Country_ISO3166.Kazakhstan;
                    case "LAO": return Country_ISO3166.Laos;
                    case "LCA": return Country_ISO3166.SaintLucia;
                    case "LKA": return Country_ISO3166.SriLanka;
                    case "LBN": return Country_ISO3166.Lebanon;
                    case "LBR": return Country_ISO3166.Liberia;
                    case "LSO": return Country_ISO3166.Lesotho;
                    case "LTU": return Country_ISO3166.Lithuania;
                    case "LUX": return Country_ISO3166.Luxembourg;
                    case "LVA": return Country_ISO3166.Latvia;
                    case "LBY": return Country_ISO3166.LibyanArabJamahiriya;
                    case "MAR": return Country_ISO3166.Morocco;
                    case "MDA": return Country_ISO3166.Moldova;
                    case "MDG": return Country_ISO3166.Madagascar;
                    case "MHL": return Country_ISO3166.MarshallIslands;
                    case "MKD": return Country_ISO3166.Macedonia;
                    case "MLI": return Country_ISO3166.Mali;
                    case "MMR": return Country_ISO3166.Myanmar;
                    case "MNG": return Country_ISO3166.Mongolia;
                    case "MAC": return Country_ISO3166.Macao;
                    case "MRT": return Country_ISO3166.Mauritania;
                    case "MSR": return Country_ISO3166.Montserrat;
                    case "MLT": return Country_ISO3166.Malta;
                    case "MUS": return Country_ISO3166.Mauritius;
                    case "MDV": return Country_ISO3166.Maldives;
                    case "MWI": return Country_ISO3166.Malawi;
                    case "MEX": return Country_ISO3166.Mexico;
                    case "MYS": return Country_ISO3166.Malaysia;
                    case "MOZ": return Country_ISO3166.Mozambique;
                    case "NAM": return Country_ISO3166.Namibia;
                    case "NCL": return Country_ISO3166.NewCaledonia;
                    case "NER": return Country_ISO3166.Niger;
                    case "NGA": return Country_ISO3166.Nigeria;
                    case "NIC": return Country_ISO3166.Nicaragua;
                    case "NLD": return Country_ISO3166.Netherlands;
                    case "NOR": return Country_ISO3166.Norway;
                    case "NPL": return Country_ISO3166.Nepal;
                    case "NRU": return Country_ISO3166.Nauru;
                    case "NIU": return Country_ISO3166.Niue;
                    case "NZL": return Country_ISO3166.NewZealand;
                    case "OMN": return Country_ISO3166.Oman;
                    case "PAN": return Country_ISO3166.Panama;
                    case "PER": return Country_ISO3166.Peru;
                    case "PYF": return Country_ISO3166.FrenchPolynesia;
                    case "PNG": return Country_ISO3166.PapuaNewGuinea;
                    case "PHL": return Country_ISO3166.Philippines;
                    case "PAK": return Country_ISO3166.Pakistan;
                    case "POL": return Country_ISO3166.Poland;
                    case "PRI": return Country_ISO3166.PuertoRico;
                    case "PRT": return Country_ISO3166.Portugal;
                    case "PRY": return Country_ISO3166.Paraguay;
                    case "QAT": return Country_ISO3166.Qatar;
                    case "ROM": return Country_ISO3166.Romania;
                    case "RUS": return Country_ISO3166.RussianFederation;
                    case "RWA": return Country_ISO3166.Rwanda;
                    case "SAU": return Country_ISO3166.SaudiArabia;
                    case "SLB": return Country_ISO3166.SolomonIslands;
                    case "SYC": return Country_ISO3166.Seychelles;
                    case "SDN": return Country_ISO3166.Sudan;
                    case "SWE": return Country_ISO3166.Sweden;
                    case "SGP": return Country_ISO3166.Singapore;
                    case "SVN": return Country_ISO3166.Slovenia;
                    case "SVK": return Country_ISO3166.Slovakia;
                    case "SLE": return Country_ISO3166.SierraLeone;
                    case "SEN": return Country_ISO3166.Senegal;
                    case "SOM": return Country_ISO3166.Somalia;
                    case "SUR": return Country_ISO3166.Suriname;
                    case "STP": return Country_ISO3166.SaoTomeandPrincipe;
                    case "SLV": return Country_ISO3166.ElSalvador;
                    case "SYR": return Country_ISO3166.SyrianArabRepublic;
                    case "SWZ": return Country_ISO3166.Swaziland;
                    case "TCA": return Country_ISO3166.TurksandCaicosIslands;
                    case "TCD": return Country_ISO3166.Chad;
                    case "TGO": return Country_ISO3166.Togo;
                    case "THA": return Country_ISO3166.Thailand;
                    case "TLS": return Country_ISO3166.TimorLeste;
                    case "TUN": return Country_ISO3166.Tunisia;
                    case "TUR": return Country_ISO3166.Turkey;
                    case "TTO": return Country_ISO3166.TrinidadandTobago;
                    case "TWN": return Country_ISO3166.Taiwan;
                    case "TZA": return Country_ISO3166.Tanzania;
                    case "UKR": return Country_ISO3166.Ukraine;
                    case "UGA": return Country_ISO3166.Uganda;
                    case "UMI": return Country_ISO3166.UnitedStatesMinorOutlyingIslands;
                    case "USA": return Country_ISO3166.UnitedStates;
                    case "URY": return Country_ISO3166.Uruguay;
                    case "UZB": return Country_ISO3166.Uzbekistan;
                    case "VCT": return Country_ISO3166.SaintVincentandtheGrenadines;
                    case "VEN": return Country_ISO3166.Venezuela;
                    case "VGB": return Country_ISO3166.VirginIslandsBritish;
                    case "VIR": return Country_ISO3166.VirginIslandsUS;
                    case "VNM": return Country_ISO3166.Vietnam;
                    case "VUT": return Country_ISO3166.Vanuatu;
                    case "WLF": return Country_ISO3166.WallisandFutuna;
                    case "WSM": return Country_ISO3166.Samoa;
                    case "YEM": return Country_ISO3166.Yemen;
                    case "ZAF": return Country_ISO3166.SouthAfrica;
                    case "ZMB": return Country_ISO3166.Zambia;
                    case "ZWE": return Country_ISO3166.Zimbabwe;

                }
                return null;
            }



            public static IEnumerable<Country_ISO3166> GetListOfCountriesFromCommaSeperatedString(string s)
            {
                s = s ?? "";
                string[] sTokens = s.Split(',');
                List<Country_ISO3166> list = new List<Country_ISO3166>();
                for (int i = 0; i < sTokens.Length; i++)
                {
                    string token = sTokens[i] ?? "";
                    token = token.Trim();
                    if (!string.IsNullOrWhiteSpace(token))
                    {
                        Country_ISO3166? value = Country_ISO3166_FromCode(token);
                        if (value.HasValue)
                        {
                            list.Add(value.Value);
                        }
                    }

                }
                return list;
                
            }
        }
    }
}
