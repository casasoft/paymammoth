﻿using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.URL
{
    
    public class QueryString : NameValueCollection
    {
        /// <summary>
        /// Add a list of items for flash querystring.  Items will be the prefix0/1/2/3
        /// </summary>
        /// <param name="items"></param>
        /// <param name="prefix"></param>
        public void AddListForFlash<T>(List<T> items, string prefix)
        {
            if (items != null)
            {
                for (int i = 0; i < items.Count; i++)
                {
                    string label = prefix + i;
                    this[label] = items[i].ToString();
                }
            }
        }
        public QueryString():
            base()
        {
            
        }

        public QueryString(string urlVariables):
            base()
        {
            ParseFromString(urlVariables);
        }
        public QueryString(NameValueCollection vars) :
            base()
        {
            ParseFromNameValueCollection(vars);
        }
        public void ParseFromString(string urlVariables)
        {
            this.Clear();
            if (urlVariables != null)
            {
                string[] nameValues = urlVariables.Split('&');
                for (int i = 0; i < nameValues.Length; i++)
                {
                    string[] data = nameValues[i].Split('=');
                    if (data.Length >= 2)
                    {
                        string name = CS.General_v3.Util.Text.UrlDecode(data[0]);
                        string value = CS.General_v3.Util.Text.UrlDecode(data[1]);
                        this.Add(name, value);
                    }
                }
            }
        }
        public void ParseFromNameValueCollection(NameValueCollection vars)
        {
            this.Clear();
            for (int i = 0; i < vars.Count; i++)
            {
                this.Add(vars.Keys[i], vars[i]);
            }
            

        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            
            for (int i = 0; i < Count; i++)
            {
                string name = GetKey(i);
                string value = this[i];
                
                if (value != null) //17.03.2009 Changed by Mark from != null, in case any errors are encountered
                {
                    if (sb.Length > 0)
                    {
                        sb.Append("&");
                        
                    }
                    value = CS.General_v3.Util.Text.UrlEncode(value);
                    

                    sb.Append(name + "=" + value);
                }
            }
            return sb.ToString();
        }

        public void AddByProperty<T, TValue>(T item, System.Linq.Expressions.Expression<Func<T, TValue>> property)
        {
            var propertyInfo = CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(property);


            string name = propertyInfo.Name;

            string sValue = null;
            {

                object value = propertyInfo.GetValue(item, null);
                if (value != null)
                    sValue = value.ToString();
            }
            this.Add(name, sValue);

        }
        public void AddByProperty<T>(System.Linq.Expressions.Expression<Func<T, string>> property, string value)
        {

            string name = CS.General_v3.Util.ReflectionUtil<T>.GetPropertyName(property);
            


            this.Add(name, value);

        }

        public override void Set(string name, string value)
        {
            base.Set(name, value);
        }
        public override void Add(string name, string value)
        {
            //base[name] = value;
            base.Add(name, value);
        }
        public override string Get(int index)
        {
            return base.Get(index);
        }
        public override string Get(string name)
        {
            return base.Get(name);
        }
        public override void Remove(string name)
        {
            
            base.Remove(name);
        }
        public override void Clear()
        {
            base.Clear();
            
        }
        public new string this[string id]
        {
            get
            {
                id = System.Web.HttpContext.Current.Server.UrlEncode(id);
                return base[id];
            }
            set
            {
                id = System.Web.HttpContext.Current.Server.UrlEncode(id);
                base[id] = value;
            }
        }
        public new string this[int id]
        {
            get
            {
                return base[id];
            }
        }
    }
}
