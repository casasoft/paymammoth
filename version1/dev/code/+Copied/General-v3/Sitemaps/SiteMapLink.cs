﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace CS.General_v3.Sitemaps
{
    public class SiteMapLink : Generator.SiteInfo
    {
        public List<SiteMapLink> FlattenTreeNode()
        {
            List<SiteMapLink> list = new List<SiteMapLink>();
            this.flattenTreeNode(list);
            return list;
        }
        protected void flattenTreeNode(List<SiteMapLink> list)
        {
            list.Add(this);
            foreach (SiteMapLink link in this.Children)
            {
                link.flattenTreeNode(list);
            }
        }

        public virtual SiteMapLink Clone()
        {
            SiteMapLink l = new SiteMapLink(this.Title, this.LinkTitle, this.URL, this.ID,
                this.Priority, this.ChangeFrequency, this.LastUpdated);
            return l;
        }
        private class SiteMapBrancher
        {
            private SiteMapLink _root = null;
            private SiteMapLink newRoot = null;
            private SiteMapLink currLink = null;
            private int pageNo = 0;
            private int pageSize = 0;
            private int fromIndex = 0;
            private int toIndex = 0;
            public SiteMapBrancher(SiteMapLink root,int PageNo, int PageSize)
            {
                _root = root;
                this.pageNo = PageNo;
                this.pageSize = PageSize;
                fromIndex = ((pageNo-1) * pageSize);
                toIndex = (pageNo * pageSize)-1;
            }
            private int currIndex = 0;

            /// <summary>
            /// Traverses the sitemap, to find out the only 'eligible' nodes.
            /// </summary>
            /// <param name="link"></param>
            /// <param name="curr"></param>
            private void Traverse(SiteMapLink link, SiteMapLink curr )
            {
                SiteMapLink thisLink = curr;
                thisLink = link.Clone();
                thisLink.isLeafNode = false;
                if (curr == null)
                {
                    newRoot = thisLink;
                }
                else
                {
                    curr.AddSubLink(thisLink);
                }
                if (currIndex <= toIndex)
                {
                    if (link.Children.Count == 0 )
                    {
                        if (currIndex >=fromIndex)
                            thisLink.isLeafNode = true;
                        currIndex++;
                    }
                    else
                    {
                        for (int i = 0; i < link.Children.Count; i++)
                        {
                            Traverse(link.Children[i], thisLink);
                            if (currIndex > toIndex)
                                break;
                        }
                    }
                }
                
            }
            

            public SiteMapLink GetBranch()
            {
                currIndex = 0;
                fromIndex = ((pageNo - 1) * pageSize);
                toIndex = (pageNo * pageSize) - 1;
                newRoot = null;
                Traverse(_root, null);
                newRoot.removeBranchesWithNoLeafNodes();
                return newRoot;
                
            }


        }


        private int _totalChildNodes = 0;
        private int _subLevels = 0;
        

        public SiteMapLink(XmlNode node)
            
        {
            this.LoadFromFullXML(node);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="title">Title to display in site map</param>
        /// <param name="linkTitle">Title to display in 'title' attribute of anchor</param>
        /// <param name="URL">URL</param>
        public SiteMapLink(string title, string linkTitle, string URL, int id)
            : this(title,linkTitle,URL,id,null,null,null)
        {

        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="title">Title to display in site map</param>
        /// <param name="linkTitle">Title to display in 'title' attribute of anchor</param>
        /// <param name="URL">URL</param>
        /// <param name="id"></param>
        /// <param name="priority">Priority of updating (double between 0 and 1)</param>
        /// <param name="changeFrequency">how frequently it is updated</param>
        /// <param name="lastUpdated">when it was last updated</param>
        public SiteMapLink(string title, string linkTitle, string URL, int id, double? priority, 
            CS.General_v3.Sitemaps.Generator.CHANGE_FREQUENCY? changeFrequency, DateTime? lastUpdated)
            : base(URL,priority,changeFrequency,lastUpdated)
        {
            this.LinkTitle = LinkTitle;
            if (string.IsNullOrEmpty(this.LinkTitle))
                this.LinkTitle = title;
            this.ID = id;
            this.Title = title;
            this.Children = new List<SiteMapLink>();
        }
        public void AddSubLink(SiteMapLink link)
        {
            this.Children.Add(link);
            link.Parent = this;

            /*if (_subLevels < link.SubLevels + 1)
            {
                SubLevels = link.SubLevels + 1;
            }
            */

            updateSubLevels();
            incrementChildNodes((link.TotalChildNodes+1));
        }
        public void RemoveSubLink(SiteMapLink link)
        {
            this.Children.Remove(link);
            this.updateSubLevels();
           

            incrementChildNodes(-(link.TotalChildNodes+1));
        }
        private void incrementChildNodes(int increment) {
            if (Math.Abs(increment) > 0)
            {
                _totalChildNodes += increment;
                if (Parent != null)
                {
                    Parent.incrementChildNodes(increment);

                }
            }
        }
        private void incrementSubLevels()
        {
            _subLevels++;

            if (Parent != null)
            {
                if (Parent.SubLevels < _subLevels)
                {
                    Parent.incrementSubLevels();
                }
            }
        }
        public virtual int ID { get; set; }
        protected bool isLeafNode { get; set; }
        public virtual string Title { get; set; }
        public virtual string LinkTitle { get; set; }
        public virtual SiteMapLink Parent { get; set; }
        public List<SiteMapLink> Children { get; private set; }
        public int TotalChildNodes { get { return _totalChildNodes; } }
        protected bool hasChildrenLeafNodes()
        {
            if (this.isLeafNode)
                return true;
            else
            {
                for (int i = 0; i < this.Children.Count; i++)
                {
                    if (this.Children[i].hasChildrenLeafNodes())
                        return true;
                }
            }
            return false;
        }
        protected void removeBranchesWithNoLeafNodes()
        {
            for (int i =0; i < this.Children.Count; i++)
            {
                this.Children[i].removeBranchesWithNoLeafNodes();
            }
            for (int i = 0; i < this.Children.Count; i++)
            {
                if (!this.Children[i].hasChildrenLeafNodes())
                {
                    this.RemoveSubLink(this.Children[i]);
                    i--;
                }
            }
        }
        protected void updateSubLevels()
        {
            int maxSubLevels = 0;
            for (int i = 0; i < Children.Count; i++)
            {
                maxSubLevels = Math.Max(Children[i].SubLevels, maxSubLevels);
            }
            this._subLevels = maxSubLevels;
            if (this.Parent != null)
            {
                this.Parent.updateSubLevels();
            }
        }
        public int SubLevels
        {
            get
            {
                return _subLevels;
            }
            set
            {
                /*if (Parent != null && Parent.SubLevels < value+1)
                {
                    Parent.SubLevels = value+1;
                }*/
                _subLevels = value;
            }
        }

        /// <summary>
        /// Branch the tree and remove any other nodes.  
        /// </summary>
        /// <param name="pageNumber">Number of page to get (index starts from 0)</param>
        /// <param name="pageSize">The maximum amount of elements to retrieve</param>
        /// <returns>Amount of TOTAL pages in this node</returns>
        public SiteMapLink BranchLinks(int pageNumber, int pageSize)
        {
            SiteMapBrancher brancher = new SiteMapBrancher(this, pageNumber, pageSize);
            SiteMapLink branchLink=  brancher.GetBranch();
            return branchLink;
        }
        public int GetTotalLeafNodes()
        {
            int i = 0;
            if (this.Children.Count == 0)
                i++;

            foreach (SiteMapLink link in this.Children)
            {
                i += link.GetTotalLeafNodes();
            }
            return i;
        }
        public int GetTotalPages(int pageSize)
        {
            int totalLeafNodes = this.GetTotalLeafNodes();

            totalLeafNodes += (pageSize - 1);
            int totalPages = totalLeafNodes / pageSize;
            return totalPages;

        }
        public SiteMapLink GetNodeById(int id)
        {
            List<SiteMapLink> links = FlattenTreeNode();
            for (int i = 0; i < links.Count; i++)
            {
                if (links[i].ID == id) return links[i];
            }
            return null;
        }

        private void removeExtraSubLinks(SiteMapLink link, int level, int maxLevel)
        
        {
            
            for (int i = 0; i < link.Children.Count; i++)
            {
                if (level+1 == maxLevel)
                {
                    link.RemoveSubLink(link.Children[i]);
                    i--;
                }
                else
                {
                    removeExtraSubLinks(link.Children[i], level + 1, maxLevel);
                }
            }
            
        }
        /// <summary>
        /// Remove all the extra sub links beyound this level
        /// </summary>
        /// <param name="maxSubLevels">Maximum number of levels allowed</param>
        public SiteMapLink RemoveExtraSubLevels(int maxSubLevels)
        {
            SiteMapLink clone = this.Duplicate();
            removeExtraSubLinks(clone, 0, maxSubLevels);
            return clone;
        }

        /// <summary>
        /// Duplicate this sitemap link and create new unique pointers.
        /// </summary>
        /// <returns></returns>
        public SiteMapLink Duplicate()
        {
            SiteMapLink clone = this.Clone();
            SiteMapLink node = this;
            SiteMapLink cloneNode = clone;
            while (node.Parent != null)
            {
                cloneNode.Parent = node.Parent.Clone();
                cloneNode = cloneNode.Parent;
                node = node.Parent;
            }
            for (int i = 0; i < Children.Count; i++)
            {
                clone.AddSubLink(Children[i].Duplicate());
            }
            return clone;
        }

        public void SaveAsFullXML(string path)
        {
            if (path.IndexOf("/") >-1)
                path = CS.General_v3.Util.PageUtil.MapPath(path);
            XmlDocument doc = new XmlDocument();
            XmlNode node = SaveAsFullXML(doc);
            doc.AppendChild(node);
            doc.Save(path);
        }

        /// <summary>
        /// This saves the sitemap to an XML file that contains a full information, including parent-child relationships
        /// </summary>
        /// <param name="node"></param>
        public XmlNode SaveAsFullXML(XmlDocument doc)
        {
            XmlNode node = doc.CreateElement("Link");
            XmlNode n = doc.CreateElement("Title"); n.InnerText = this.Title; node.AppendChild(n);
            if (this.ChangeFrequency != null)
            {
                n = doc.CreateElement("ChangeFrequency"); n.InnerText = Generator.ChangeFrequencyToText(this.ChangeFrequency.Value); node.AppendChild(n);
            }
            n = doc.CreateElement("ID"); n.InnerText = this.ID.ToString(); node.AppendChild(n);
            n = doc.CreateElement("IsLeafNode"); n.InnerText = CS.General_v3.Util.Other.BoolToStr(this.isLeafNode); node.AppendChild(n);
            if (this.LastUpdated != null)
            {
                n = doc.CreateElement("LastUpdated"); n.InnerText = this.LastUpdated.Value.ToString("yyyy-MM-dd"); node.AppendChild(n);
            }
            n = doc.CreateElement("LinkTitle"); n.InnerText = this.LinkTitle; node.AppendChild(n);
            if (this.Priority != null)
            {
                n = doc.CreateElement("Priority"); n.InnerText = this.Priority.Value.ToString("0.0"); node.AppendChild(n);
            }
            n = doc.CreateElement("TotalChildNodes"); n.InnerText = this.TotalChildNodes.ToString(); node.AppendChild(n);
            n = doc.CreateElement("SubLevels"); n.InnerText = this.SubLevels.ToString(); node.AppendChild(n);
            n = doc.CreateElement("URL"); n.InnerText = this.URL; node.AppendChild(n);
            XmlNode childNodes = doc.CreateElement("Children");
            for (int i =0; i < this.Children.Count; i++)
            {
                childNodes.AppendChild(this.Children[i].SaveAsFullXML(doc));
            }
            node.AppendChild(childNodes);
            return node;
        }
        /// <summary>
        /// This loads the sitemap from an XML file that contains a full information, including parent-child relationships
        /// </summary>
        /// <param name="node"></param>
        public void LoadFromFullXML(XmlNode node)
        {
            this.ChangeFrequency = null;
            this.LastUpdated = null;
            this.Priority = null;
            this.Parent = null;
            this.Title = node.SelectSingleNode("Title").InnerText;
            if (node.SelectSingleNode("ChangeFrequency") != null)
                this.ChangeFrequency = Generator.ChangeFrequencyFromText(node.SelectSingleNode("ChangeFrequency").InnerText);
            this.ID = Convert.ToInt32(node.SelectSingleNode("ID").InnerText);
            this.isLeafNode = (node.SelectSingleNode("IsLeafNode").InnerText == "1");
            if (node.SelectSingleNode("LastUpdated") != null)
            {
                string s = node.SelectSingleNode("LastUpdated").InnerText;
                int year = Convert.ToInt32(s.Substring(0, 4));
                int month = Convert.ToInt32(s.Substring(5, 2));
                int day = Convert.ToInt32(s.Substring(8, 2));
                this.LastUpdated = new DateTime(year, month, day);
            }
            this.LinkTitle = node.SelectSingleNode("LinkTitle").InnerText;
            if (node.SelectSingleNode("Priority") != null)
                this.Priority = Convert.ToDouble(node.SelectSingleNode("Priority").InnerText);

            //this.TotalChildNodes = Convert.ToInt32(node.SelectSingleNode("TotalChildNodes").InnerText);
            this.SubLevels = Convert.ToInt32(node.SelectSingleNode("SubLevels").InnerText);
            this.URL = node.SelectSingleNode("URL").InnerText;

            XmlNode childrenNode = node.SelectSingleNode("Children");
            this.Children = new List<SiteMapLink>();
            for (int i = 0; i < childrenNode.ChildNodes.Count; i++)
            {
                SiteMapLink childLink = new SiteMapLink(childrenNode.ChildNodes[i]);
                childLink.Parent = this;
                this.AddSubLink(childLink);
                
            }
            
        }
        public override string ToString()
        {
            return this.Title + " (" + this.URL + ")";
            
        }
    }
}
