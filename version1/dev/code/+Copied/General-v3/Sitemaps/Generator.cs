﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CS.General_v3.Sitemaps
{
    public class Generator
    {
        public Generator()
        {
            this.Sites = new List<SiteInfo>();
        }
        public enum CHANGE_FREQUENCY
        {
            Always,Hourly,Daily,Weekly,Monthly,Yearly,Never
        }
        public static string ChangeFrequencyToText(CHANGE_FREQUENCY value)
        {
            switch (value)
            {
                case CHANGE_FREQUENCY.Always: return "Always";
                case CHANGE_FREQUENCY.Hourly: return "Hourly";
                case CHANGE_FREQUENCY.Daily: return "Daily";
                case CHANGE_FREQUENCY.Weekly: return "Weekly";
                case CHANGE_FREQUENCY.Monthly: return "Monthly";
                case CHANGE_FREQUENCY.Yearly: return "Yearly";
                case CHANGE_FREQUENCY.Never: return "Never";
            }
            return "";
        }
        public static CHANGE_FREQUENCY ChangeFrequencyFromText(string s)
        {
            s = s.ToLower().Trim();
            switch (s)
            {
                case "always": return CHANGE_FREQUENCY.Always;
                case "hourly": return CHANGE_FREQUENCY.Hourly;
                case "daily": return CHANGE_FREQUENCY.Daily;
                case "weekly": return CHANGE_FREQUENCY.Weekly;
                case "monthly": return CHANGE_FREQUENCY.Monthly;
                case "yearly": return CHANGE_FREQUENCY.Yearly;
                case "never": return CHANGE_FREQUENCY.Never;
                
            }
            return CHANGE_FREQUENCY.Weekly;
        }
        public class SiteInfo
        {
            public SiteInfo()
            {
                
            }
            public static string encodeTxtForSitemapXML(string txt)
            {
                txt = CS.General_v3.Util.Text.TxtForHTML(txt);
                return txt;
            }
            public SiteInfo(string url)
                : this (url,null,null,null)
            {

            }
            public SiteInfo(string url, double? priority, CHANGE_FREQUENCY? changeFrequency, DateTime? lastUpdated)
            {
                this.URL = url;
                this.Priority = priority;
                this.ChangeFrequency = changeFrequency;
                this.LastUpdated = lastUpdated;
            }
            public virtual string URL { get; set; }
            public virtual double? Priority { get; set; }
            public virtual CHANGE_FREQUENCY? ChangeFrequency { get; set; }
            public virtual DateTime? LastUpdated { get; set; }
            public void AddToSiteMap(XmlDocument doc, XmlNode node)
            {
                if (!string.IsNullOrEmpty(URL))
                {
                    XmlNode url = doc.CreateElement("url", NAMESPACE_URI);
                    node.AppendChild(url);
                    XmlNode n;
                    n = doc.CreateElement("loc", NAMESPACE_URI); n.InnerText = URL; url.AppendChild(n);
                    if (Priority != null)
                    {
                        n = doc.CreateElement("priority", NAMESPACE_URI); n.InnerText = Priority.Value.ToString("0.0"); url.AppendChild(n);
                    }
                    if (ChangeFrequency != null)
                    {
                        n = doc.CreateElement("changefreq", NAMESPACE_URI); n.InnerText = ChangeFrequencyToText(this.ChangeFrequency.Value); url.AppendChild(n);
                    }
                    if (LastUpdated != null && LastUpdated.Value.Year > 1990)
                    {
                        n = doc.CreateElement("lastmod", NAMESPACE_URI); n.InnerText = LastUpdated.Value.ToString("yyyy-MM-dd"); url.AppendChild(n);
                    }
                }

            }
            public void LoadFromSitemapNode(XmlNode node)
            {
                LastUpdated = null;
                ChangeFrequency = null;
                Priority = null;
                if (node.SelectSingleNode("lastmod") != null)
                {
                    string s = node.SelectSingleNode("lastmod").InnerText;
                    int year = Convert.ToInt32(s.Substring(0, 4));
                    int month = Convert.ToInt32(s.Substring(5, 2));
                    int day = Convert.ToInt32(s.Substring(8, 2));
                    this.LastUpdated = new DateTime(year, month, day);
                }
                if (node.SelectSingleNode("changefreq") != null)
                {
                    string s = node.SelectSingleNode("changefreq").InnerText;
                    this.ChangeFrequency = ChangeFrequencyFromText(s);
                }
                if (node.SelectSingleNode("priority") != null)
                {
                    string s = node.SelectSingleNode("priority").InnerText;
                    this.Priority = Convert.ToDouble(s);
                }
                this.URL = node.SelectSingleNode("loc").InnerText;
            }
        }
        private XmlDocument doc = null;
        private XmlNode nodeURLSet = null;
        public List<SiteInfo> Sites = null;
        private const string NAMESPACE_URI = "http://www.sitemaps.org/schemas/sitemap/0.9";
        /// <summary>
        /// Generates a sitemap
        /// </summary>
        /// <param name="path"></param>
        public void GenerateSitemap(string path)
        {
            if (path.Contains("/")) path = CS.General_v3.Util.PageUtil.MapPath(path);
            
            doc = new XmlDocument();

            doc.AppendChild(doc.CreateXmlDeclaration("1.0", "UTF-8", null));

            System.Xml.XmlNamespaceManager xmlnsManager = new System.Xml.XmlNamespaceManager(doc.NameTable);



            xmlnsManager.AddNamespace(String.Empty, NAMESPACE_URI);
            //doc.NamespaceURI = namespaceURI;
            nodeURLSet = doc.CreateElement("urlset", NAMESPACE_URI);
            //nodeURLSet.AppendChild(doc.CreateAttribute("xmlns", namespaceURI, namespaceURI));
            doc.AppendChild(nodeURLSet);
            foreach (SiteInfo s in Sites)
            {
                s.AddToSiteMap(doc, nodeURLSet);
            }
            doc.Save(path);
        }
        
    }
}
