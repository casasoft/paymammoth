﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using CS.General_v3.Classes.Email;

namespace CS.General_v3.Error
{
    public class ErrorDetails
    {
        public string TypeName { get; set; }
        public string ErrorTitle { get; set; }
        private StringBuilder sb;
        public ErrorDetails()
        {
            sb = new StringBuilder();
        }
        public void SkipLine(int amt)
        {
            for (int i = 0; i < amt; i++)
            {
                sb.AppendLine();
            }
        }
        public void AddHeader(string s)
        {
            s = s ?? "";
            sb.AppendLine("");
            sb.AppendLine(s);
            sb.AppendLine(CS.General_v3.Util.Text.RepeatText("-", s.Length));
            sb.AppendLine();
        }
        public void AddParam(string name, string value)
        {
            AddParam(name, value, false);

        }
        public void AddParam(string name, string value, bool multiline)
        {
            sb.Append(CS.General_v3.Util.Text.PadText(name, Util.Text.ALIGN.Right, 25) + ": ");
            if (multiline)
            {
                string tmp = CS.General_v3.Util.Text.PadLines(value, false, 32);
                sb.AppendLine();
                sb.Append(tmp);
                sb.AppendLine();
                sb.AppendLine();
            }
            else
            {
                sb.AppendLine(value);
            }
            // sb.AppendLine();


        }
        private void addKeysAndValues(string name, IEnumerable<string> keys, IEnumerable<object> values)
        {
            sb.Append(CS.General_v3.Util.Text.PadText(name, CS.General_v3.Util.Text.ALIGN.Right, 30) + ": ");
            var keysList = keys.ToList();
            var valuesList = values.ToList();
            for (int i = 0; i < keysList.Count; i++)
            {
                string key = null;
                object value = null;
                try
                {
                    key = keysList[i];
                    value = valuesList[i];

                    string s = "";
                    if (i > 0)
                        s += CS.General_v3.Util.Text.RepeatText(" ", 32);

                    string sValue = "[NULL]";
                    if (value != null)
                        sValue = value.ToString();

                    s += key + " = " + sValue;

                    sb.AppendLine(s);
                }
                catch (Exception ex)
                {
                    sb.AppendLine("Error trying to log key <" + key + ">, with value <" + value + ">");
                    AddExceptionDetails(ex);
                }


            }
        }

        public void AddNameValueCollection(string name, NameValueCollection nv)
        {

            

            var keyList = nv.AllKeys;
            List<string> valueList = new List<string>();
            foreach(var key in keyList) {
                valueList.Add(nv[key]);
            }

            addKeysAndValues(name, keyList, valueList);
            

            //System.Collections.Generic.List<object>
            //addKeysAndValues(name, nv.AllKeys, nv.GetEnumerator());
            //sb.Append(CS.General_v3.Util.Text.PadText(name, Util.Text.ALIGN.Right, 30) + ": ");
            //for (int i = 0; i < nv.AllKeys.Length; i++)
            //{
            //    string s = "";
            //    if (i > 0)
            //        s += CS.General_v3.Util.Text.RepeatText(" ", 32);

            //    string key = nv.AllKeys[i];
            //    string value = nv[i];
            //    s += key + " = " + value;

            //    sb.AppendLine(s);
            //}
            //sb.AppendLine();
        }
        public override string ToString()
        {
            return GetAsString();

        }
        public string GetAsString()
        {
            return sb.ToString();
        }
        public void SendEmail()
        {
            string identifier = Enums.SETTINGS_ENUM.Others_Error_Identifier.GetSettingFromDatabase();
            if (string.IsNullOrWhiteSpace(identifier))
                identifier = Enums.SETTINGS_ENUM.Others_WebsiteName.GetSettingFromDatabase();

            string subject = "[ERROR] {" + identifier + "} - " + TypeName + ": " + ErrorTitle;
            EmailMessage email = new EmailMessage();

            email.SetFromEmail(Enums.SETTINGS_ENUM.Email_EmailAddresses_ErrorNotification_Email.GetSettingFromDatabase(),  "Error Reporter");
            email.AddToEmails( Enums.SETTINGS_ENUM.Email_EmailAddresses_ErrorNotification_Email.GetSettingFromDatabase(), Enums.SETTINGS_ENUM.Email_EmailAddresses_General_FromName.GetSettingFromDatabase());
            email.IsErrorNotification = true;
            email.Subject = subject;
            email.BodyPlain = sb.ToString();
            email.SendSynchronously();
        }
        public void AddLine(string s)
        {
            sb.AppendLine(s);
        }
        public void AddFooter()
        {
            sb.AppendLine();
            sb.AppendLine("=============================================================================================");
            sb.AppendLine("#############################################################################################");
            sb.AppendLine("=============================================================================================");
            sb.AppendLine();
        }

        public void AddContextInfo(HttpContext context)
        {
            var details = this;

            HttpContext ctx = context;
            int ctxLine = -1;
            try
            {
                if (ctx != null && ctx.Request != null)
                {
                    ctxLine = 0;
                    details.AddHeader("Context Info");
                    try
                    {
                        ctxLine = 5;
                        details.AddParam("Cookies Enabled", CS.General_v3.Util.PageUtil.CheckIfCookiesAreEnabled() ? "Yes" : "No");

                    }
                    catch (Exception ex)
                    {
                        details.AddLine("error in cookies enabled");
                        details.AddExceptionDetails(ex);
                    }

                    if (ctx.Request.Url != null)
                        
                    {
                        ctxLine = 10; details.AddParam("Request.URL", ctx.Request.Url.ToString()); 
                        ctxLine = 20; details.AddParam("Request.AbsolutePath", ctx.Request.Url.AbsolutePath);
                        ctxLine = 30; details.AddParam("Request.Url.Host", ctx.Request.Url.Host);
                    }

                    if (ctx.Request.Browser != null)
                    {
                        ctxLine = 40; details.AddParam("Request.Browser.Browser", ctx.Request.Browser.Browser);
                    }
                    ctxLine = 50; details.AddParam("Request.FilePath", ctx.Request.FilePath);
                    ctxLine = 60; details.AddParam("Request.Path", ctx.Request.Path);
                    ctxLine = 65; details.AddParam("Timestamp", ctx.Timestamp.ToString("dd/MMM/yyyy hh:mmtt"));
                    ctxLine = 70; details.AddParam("Request.ContentType", ctx.Request.ContentType);
                
                    ctxLine = 80; details.AddParam("Request.ApplicationPath", ctx.Request.ApplicationPath);
                    ctxLine = 90; details.AddParam("Request.PhysicalApplicationPath", ctx.Request.PhysicalApplicationPath);
                    ctxLine = 100; details.AddParam("Request.PhysicalPath", ctx.Request.PhysicalPath);

                    if (ctx.Request.QueryString != null)
                    {
                        ctxLine = 110; details.AddParam("Request.QueryString", ctx.Request.QueryString.ToString());
                    }
                    ctxLine = 120; details.AddParam("Request.RawUrl", ctx.Request.RawUrl);
                    ctxLine = 130; details.AddParam("Request.RequestType", ctx.Request.RequestType);
                    if (ctx.Request.UrlReferrer != null)
                    {
                        ctxLine = 140; details.AddParam("Request.UrlReferrer.PathAndQuery", ctx.Request.UrlReferrer.PathAndQuery);
                    }
                    ctxLine = 150; details.AddParam("Request.UserAgent", ctx.Request.UserAgent);
                
                
                    try
                    {
                        ctxLine = 160; details.AddParam("Request.UserHostAddress:", ctx.Request.UserHostAddress);
                        ctxLine = 170; details.AddParam("Request.UserHostName:", ctx.Request.UserHostName);
                    }
                    catch (NullReferenceException ex)
                    {

                        details.AddLine("No context available");

                    }
                    catch (Exception ex)
                    {
                        details.AddParam("Error in context, line #:", ctxLine.ToString());
                    }
                    details.SkipLine(2);
                    if (ctx.Request.Form != null)
                    {
                        ctxLine = 180; details.AddNameValueCollection("Request.Form", ctx.Request.Form);
                        details.SkipLine(2);
                    }
                    if (ctx.Request.Headers != null)
                    {
                        ctxLine = 190; details.AddNameValueCollection("Request.Headers", ctx.Request.Headers);
                    }
                    if (ctx.Session != null)
                    {
                        try
                        {
                            var keyList = ctx.Session.Keys.GetEnumerator().GetListFromEnumerator<string>();
                            List<object> valuesList = new List<object>();
                            foreach (var key in keyList)
                            {
                                object value = null;
                                try
                                {
                                    value = ctx.Session[key];
                                        
                                }
                                catch (Exception ex)
                                {
                                    value = "{[Error obtaining value for key <" + key + ">, Msg: " + ex.Message + "]}";

                                }
                                valuesList.Add(ctx.Session[key]);
                            }
                            details.addKeysAndValues("Session", keyList, valuesList);
                           

                        }
                        catch (Exception ex)
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.AppendLine("Could not add session info due to error - "+ ex.Message);
                            sb.AppendLine("Stack Trace: " + ex.StackTrace);
                            details.AddParam("Session:", sb.ToString());
                            AddExceptionDetails(ex);


                        }
                    }
                }
            }
            catch (Exception ex)
            {
                details.AddParam("Error in context, line #:", ctxLine.ToString());
            }

        }
        public void AddGeneralInfo()
        {
            var details = this;
            details.AddHeader("General Info");
            details.AddParam("Date", CS.General_v3.Util.Date.Now.ToLongDateString() + " " + CS.General_v3.Util.Date.Now.ToLongTimeString());

        }
        public void AddExceptionDetails(Exception ex)
        {
            try
            {
                int level = 0;
                Exception currentException = null;

                if (ex != null)
                {
                    var titleEx = ex;
                    if (ex.InnerException != null)
                    {
                        titleEx = ex.InnerException;
                    }
                    this.ErrorTitle = titleEx.Message;
                    this.TypeName = titleEx.GetType().FullName;
                }

                this.AddHeader("Error: " + ex.Message);


                for (currentException = (Exception) ex; currentException != null; currentException = currentException.InnerException)
                {
                    _addExceptionDetails(level, currentException);

                    level++;
                }
            }
            catch (Exception errEx2)
            {
                this.AddLine("Exception occurred during parsing exception details: " + errEx2.GetType().FullName + " (" + errEx2.Message + ") \r\n\r\nStack Trace: " + errEx2.StackTrace);
            }
        }

        private void _addExceptionDetails(int exceptionLevel, Exception exception)
        {
            var ex = exception;

            try
            {
                int level = 0;
                
                string sPadding = CS.General_v3.Util.Text.RepeatText(" ", 5);
                this.AddHeader(sPadding+ "Error: " + ex.Message);

                
                try
                {
                    this.AddParam(sPadding + "Exception Type: ", exception.GetType().FullName);
                    this.AddParam(sPadding + "Message: ", exception.Message, true);
                    this.AddParam(sPadding + "StackTrace: ", exception.StackTrace, true);
                    this.AddParam(sPadding + "Source: ", exception.Source, true);
                }
                catch (Exception errEx1)
                {
                    this.AddLine("_addExceptionDetails (ErrEx1) Exception occurred during parsing exception details: " + errEx1.GetType().FullName + " (" + errEx1.Message + ")\r\n\r\nStackTrace: " + errEx1.StackTrace);
                }

                
            }
            catch (Exception errEx2)
            {
                this.AddLine("_addExceptionDetails (ErrEx2) Exception occurred during parsing exception details: " + errEx2.GetType().FullName + " (" + errEx2.Message + ")\r\n\r\nStackTrace: " + errEx2.StackTrace);
            }



            return;
            
          

        }


    }
}
