﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using log4net;

using System.Threading;
using CS.General_v3.Classes.NHibernateClasses;

namespace CS.General_v3.Error
{
    public class ErrorToReport
    {
        public ErrorToReport(Exception ex, bool sendAlways)
        {
            this.ErrorException = ex;
            this.SendAlways = sendAlways;
        }

        //public ErrorToReport(string ex, bool sendAlways)
        //{
        //    this.ErrorMsgStr = ex;
        //    this.SendAlways = sendAlways;
        //}
        private static ILog _log = log4net.LogManager.GetLogger(typeof(ErrorToReport));

        public string ErrorMsgStr { get; set; }
        public Exception ErrorException { get; set; }
        public bool SendAlways { get; set; }

        public string ContextUrl { get; set; }
        private HttpContext _context = null;
        private HttpContext getHttpContext()
        {
            HttpContext c = System.Web.HttpContext.Current;
            if (c == null)
                c = _context;
            return c;
        }
        public void CopyDetailsFromContextIfExists()
        {
            
            HttpContext ctx = HttpContext.Current;
            if (ctx != null)
            {
                if (ctx.Request != null)
                {
                    if (ctx.Request.Url != null) this.ContextUrl = ctx.Request.Url.ToString();
                }
            }
        }

        private static readonly object _lock = new object(); 

        private void sendErrorWithException()
        {

            lock (_lock) //only one thread can be sending an error at the same time
            {
                Reporter.ErrorDetails details = new Reporter.ErrorDetails();
                bool ok = true;
                string url = "";
                HttpContext ctx = getHttpContext();
                bool sendEmail = true;


                if (!CS.General_v3.Settings.Emails.SendEmailsOnError)
                {
                    sendEmail = false;
                }
                if (ok &&
                    (CS.General_v3.Util.Other.IsLocalTestingMachine
                     && !Reporter.SEND_ERRORS_FOR_LOCALHOST))
                {
                    sendEmail = false;

                }
                if (SendAlways)
                {
                    sendEmail = true;
                }
                if (ok)
                {
                    try
                    {
                        ok = true;
                        int exLevel = 0;
                        Exception exception = this.ErrorException;
                        details.AddContextInfo(ctx);
                        do
                        {
                            //details.AddHeader(currentIdentifier);
                            ok = Reporter.validateErrorMessage(exception);

                            bool? reporterCheckException = Reporter._checkException(exception);
                            if (ok && reporterCheckException.HasValue)
                            {
                                ok = reporterCheckException.Value;
                            }
                            if (ok)
                            {
                                details.AddExceptionDetails(exLevel, exception);
                            }
                            exception = exception.InnerException;
                            exLevel++;
                        } while (exception != null);

                        //exception = this.ErrorException;
                        //currentIdentifier = "MainException";
                        //do
                        //{
                        //    currentIdentifier += ".BaseException";
                        //    if (exception.GetBaseException() != exception)
                        //        exception = exception.GetBaseException();
                        //    else
                        //        exception = null;
                        //    if (exception != null)
                        //    {
                        //        bool? reporterCheckException = Reporter._checkException(exception);
                        //        if (ok && reporterCheckException.HasValue)
                        //        {
                        //            ok = reporterCheckException.Value;
                        //        }
                        //        if (ok)
                        //        {
                        //            details.AddExceptionDetails(exception, ctx);
                        //        }
                        //    }

                        //} while (exception != null);
                    }
                    catch (Exception errorEx)
                    {
                        details.SkipLine(3);
                        details.AddParam("Error Occured", "while capturing error info!");
                        details.AddParam("Message", errorEx.Message);
                        details.AddParam("Stack Trace: ", errorEx.StackTrace);
                    }
                }
                details.AddFooter();
                details.LogToFile(this.ErrorException);
                if (ok)
                {
                    if (sendEmail)
                    {
                        if (!CS.General_v3.Util.Other.IsLocalTestingMachine)
                        {
                            details.SendEmail();
                        }
                    }

                }
                //if (ex.GetBaseException() != null && ex.GetBaseException() != ex)
                //details.ReportError(ex.GetBaseException(), sendAlways: sendAlways);
            }
        }
        private void _sendError()
        {

            if (this.ErrorException != null)
                sendErrorWithException();
            //else
                //sendErrorAsString();
        }
        private void _sendErrorOnSeperateThread()
        {

            if (CS.General_v3.Settings.Database.UseRealDatabase)
            {
                NHManager.Instance.CreateNewSessionForContext();
            }
            //TODO: 2011-04-25 commented by Mark due to error on start
            //Karl: if this error happens again let me know immediately

            _sendError();

            if (CS.General_v3.Settings.Database.UseRealDatabase)
            {
                NHManager.Instance.DisposeCurrentSessionInContext();
            }
        }
        public void SendErrorOnSeperateThread()
        {
            this._context = HttpContext.Current;

            Thread thread = new Thread(_sendErrorOnSeperateThread);
            thread.Priority = ThreadPriority.Lowest;
            thread.Start();


        }

    }
}
