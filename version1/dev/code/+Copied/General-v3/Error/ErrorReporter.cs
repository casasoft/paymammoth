﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using log4net;

using System.Threading;
using CS.General_v3.Extensions;

namespace CS.General_v3.Error
{
    public abstract class ErrorReporter
    {
        public static ErrorReporter CreateInstance()
        {
            return CS.General_v3.Util.ReflectionUtil.CreateObjectWithPrivateConstructorForLastType<ErrorReporter>();
        }


    
        protected ErrorReporter()
        {

            details = new ErrorDetails();
            this.ErrorLog4NetType = Enums.LOG4NET_MSG_TYPE.Warn;
        }

        protected ErrorDetails details = null;


        //public ErrorToReport(string ex, bool sendAlways)
        //{
        //    this.ErrorMsgStr = ex;
        //    this.SendAlways = sendAlways;
        //}
        protected static ILog _log = log4net.LogManager.GetLogger(typeof(ErrorReporter));
        public bool ReportFileDoesNotExistErrors { get; set; }
        public string ErrorMsgStr { get; set; }
        public Exception ErrorException { get; set; }
        public Enums.LOG4NET_MSG_TYPE ErrorLog4NetType { get; set; }
        public bool SendAlways { get; set; }

        public string ContextUrl { get; set; }
        private HttpContext _context = null;
        protected HttpContext getHttpContext()
        {
            HttpContext c = null;
            try
            {
                c = System.Web.HttpContext.Current;
                if (c == null)
                    c = _context;
            }
            catch (Exception ex)
            {
                details.AddLine("Exception when calling 'getHttpContext()': " + ex.GetType().FullName + " (" + ex.Message + ")\r\n\r\nStackTrace: " + ex.StackTrace);
            }
            return c;
        }
        public void CopyDetailsFromContextIfExists()
        {
            try
            {
                HttpContext ctx = HttpContext.Current;
                if (ctx != null)
                {
                    if (ctx.Request != null)
                    {
                        if (ctx.Request.Url != null) this.ContextUrl = ctx.Request.Url.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                details.AddLine("Exception when calling 'CopyDetailsFromContextIfExists()': " + ex.GetType().FullName + " (" + ex.Message + ")\r\n\r\nStackTrace: " + ex.StackTrace);
            }
        }

        protected static readonly object _lock = new object();


        private StringBuilder errMsg = new StringBuilder();
        protected void addContextInfoIfAvailable(HttpContext ctx)
        {
            try
            {
                details.AddContextInfo(ctx);
            }
            catch (Exception ex)
            {
                details.AddLine("General error in addContextInfoIfAvailable: " + ex.GetType().FullName + " (" + ex.Message + ")\r\n\r\nStackTrace: " + ex.StackTrace);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        /// <returns>True means to report the error</returns>
        private bool __m_checkIfExceptionShouldBeReported(Exception ex)
        {
            bool ok = true;
            if (!ReportFileDoesNotExistErrors && ex is HttpException && ex.Message.ContainsCaseInsensitive("does not exist"))
            {
                ok = false;
            }
            return ok;
        }

        protected bool checkIfExceptionShouldBeReported(Exception ex)
        {
            bool ok = __m_checkIfExceptionShouldBeReported(ex);

            if (ok && ex.InnerException != null)
                ok = checkIfExceptionShouldBeReported(ex.InnerException);
            return ok;

        }
        protected void writeToLog()
        {
            CS.General_v3.Util.Log4NetUtil.LogMsg(_log, this.ErrorLog4NetType, details.GetAsString(), this.ErrorException);
        }

        protected abstract void sendErrorWithException();
        private void _sendError()
        {

            if (this.ErrorException != null)
                sendErrorWithException();
            //else
                //sendErrorAsString();
        }
        private void _sendErrorOnSeperateThread()
        {

           
           
           
            _sendError();

            
        }

        public void SendErrorSynchronously(Exception ex, bool sendAlways)
        {
            _log.Error("Uncaptured error has been thrown", ex);
            this.ErrorException = ex;
            this.SendAlways = SendAlways;
            this._context = HttpContext.Current;
            _sendErrorOnSeperateThread();

        }


        public void SendError(Exception ex, bool sendAlways, bool sendAsynchronously = true)
        {
            _log.Error("Uncaptured error has been thrown", ex);

            this.ErrorException = ex;
            this.SendAlways = SendAlways;
            this._context = HttpContext.Current;
            if (checkIfExceptionShouldBeReported(ex))
            {
                if (sendAsynchronously)
                {
                    CS.General_v3.Util.ThreadUtil.CallMethodOnSeperateThread(_sendErrorOnSeperateThread, "ErrorReporter-SendError");

                }
                else
                {
                    _sendError();
                }
            }
            else
            {
                int k = 5;
            }



        }

    }
}
