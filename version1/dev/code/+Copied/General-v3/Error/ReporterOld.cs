﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Web;
using System.Text.RegularExpressions;
using CS.General_v3.Util;
using log4net;
using System.Threading;

namespace CS.General_v3.Error
{
    public class ReporterOld : IHttpModule
    {
        private static ILog _log = log4net.LogManager.GetLogger(typeof(ReporterOld));
        public static bool SEND_ERRORS_FOR_LOCALHOST = false;
        public delegate bool CheckExceptionToReport(Exception ex);
        public static event CheckExceptionToReport CheckException;
        internal static bool? _checkException(Exception ex)
        {
            bool? b = null;
            if (ex != null)
            {
                if (CheckException != null)
                    b = CheckException(ex);
            }
            return b;
        }
        /*
         * To attach this HTTP module, add this to the 'Web.Config' file
         * <add name="ErrorReporter" type="CS.General_v3.Error.Reporter"/>
         * 
         * This should go under <system.web><httpModules>[HERE]</httpModules>
         * */
        #region IHttpModule Members

        public void Dispose()
        {
            
        }

        public void Init(HttpApplication context)
        {
            context.Error += new EventHandler(context_Error);
        }

        public class ErrorDetails
        {
            public string TypeName { get; set; }
            public string ErrorTitle { get; set; }
            private StringBuilder sb;
            public ErrorDetails()
            {
                sb = new StringBuilder();
            }
            public void SkipLine(int amt)
            {
                for (int i = 0; i < amt; i++)
                {
                    sb.AppendLine();
                }
            }
            public void AddHeader(string s)
            {
                s = s ?? "";
                sb.AppendLine("");
                sb.AppendLine(s);
                sb.AppendLine(CS.General_v3.Util.Text.RepeatText("-", s.Length));
                sb.AppendLine();
            }
            public void AddParam(string name, string value)
            {
                AddParam(name, value, false);

            }
            public void AddParam(string name, string value, bool multiline)
            {
                sb.Append(CS.General_v3.Util.Text.PadText(name, Util.Text.ALIGN.Right, 25) + ": ");
                if (multiline)
                {
                    string tmp = CS.General_v3.Util.Text.PadLines(value, false, 32);
                    sb.AppendLine();
                    sb.Append(tmp);
                    sb.AppendLine();
                    sb.AppendLine();
                }
                else
                {
                    sb.AppendLine(value);
                }
               // sb.AppendLine();
                            

            }
            public void AddNameValueCollection(string name, NameValueCollection nv)
            {
                sb.Append(CS.General_v3.Util.Text.PadText(name, Util.Text.ALIGN.Right, 30) + ": ");
                for (int i = 0; i < nv.AllKeys.Length; i++)
                {
                    string s = "";
                    if (i >0)
                        s += CS.General_v3.Util.Text.RepeatText(" ", 32);
                    
                    string key = nv.AllKeys[i];
                    string value = nv[i];
                    s += key + " = " + value;
                    
                    sb.AppendLine(s);
                }
                sb.AppendLine();
            }
            public override string ToString()
            {
                return GetAsString();
                
            }
            public string GetAsString()
            {
                return sb.ToString();
            }
            public void SendEmail()
            {
                string identifier = Enums.SETTINGS_ENUM.Others_Error_Identifier.GetSettingFromDatabase();
                if (string.IsNullOrWhiteSpace(identifier))
                    identifier = Enums.SETTINGS_ENUM.Others_WebsiteName.GetSettingFromDatabase();

                string subject = "[ERROR] {" + identifier + "} - " + TypeName + ": " + ErrorTitle;
                EmailUtil.SendEmailSync(
                    Enums.SETTINGS_ENUM.Email_EmailAddresses_ErrorNotification_Email.GetSettingFromDatabase(), "Error Reporter",
                    Enums.SETTINGS_ENUM.Email_EmailAddresses_ErrorNotification_Email.GetSettingFromDatabase(), "Error Reporter", null, null, subject, sb.ToString(), null, null);
            }
            public void AddLine(string s)
            {
                sb.AppendLine(s);
            }
            public void AddFooter()
            {
                sb.AppendLine();
                sb.AppendLine("=============================================================================================");
                sb.AppendLine("#############################################################################################");
                sb.AppendLine("=============================================================================================");
                sb.AppendLine();
            }

            public void AddContextInfo(HttpContext context)
            {
                var details = this;
                
                HttpContext ctx = context;
                if (ctx != null && ctx.Request != null)
                {
                    int ctxLine =0;
                    details.AddHeader("Context Info");
                    try
                    {
                        if (ctx.Request.Url != null)
                        {
                            ctxLine = 1; details.AddParam("URL", ctx.Request.Url.ToString());
                            ctxLine = 2; details.AddParam("AbsolutePath", ctx.Request.Url.AbsolutePath);
                            ctxLine = 3; details.AddParam("Host", ctx.Request.Url.Host);


                        }
                        if (ctx.Request.Browser != null)
                        {
                            ctxLine =04; details.AddParam("Browser", ctx.Request.Browser.Browser);
                        }
                        ctxLine = 5; details.AddParam("FilePath", ctx.Request.FilePath);
                        ctxLine = 6; details.AddParam("Path", ctx.Request.Path);
                        ctxLine = 7; details.AddParam("ContentType", ctx.Request.ContentType);
                        ctxLine = 8; details.AddParam("ApplicationPath", ctx.Request.ApplicationPath);
                        ctxLine = 9; details.AddParam("PhysicalApplicationPath", ctx.Request.PhysicalApplicationPath);
                        ctxLine = 10; details.AddParam("PhysicalPath", ctx.Request.PhysicalPath);
                        if (ctx.Request.QueryString != null)
                        {
                            ctxLine = 11; details.AddParam("QueryString", ctx.Request.QueryString.ToString());
                        }
                        ctxLine = 12; details.AddParam("RawUrl", ctx.Request.RawUrl);
                        ctxLine = 13; details.AddParam("RequestType", ctx.Request.RequestType);
                        if (ctx.Request.UrlReferrer != null)
                        {
                            ctxLine = 14; details.AddParam("URL Referrer", ctx.Request.UrlReferrer.PathAndQuery);
                        }
                        ctxLine = 15; details.AddParam("User Agent", ctx.Request.UserAgent);
                        try
                        {
                            ctxLine = 16; details.AddParam("User Host Address:", ctx.Request.UserHostAddress);
                            ctxLine = 17; details.AddParam("User Host Name:", ctx.Request.UserHostName);
                        }
                        catch (Exception ex)
                        {
                            details.AddParam("Error in context, line #:", ctxLine.ToString());
                        }
                        details.SkipLine(2);
                        if (ctx.Request.Form != null)
                        {
                            ctxLine = 18; details.AddNameValueCollection("Form", ctx.Request.Form);
                            details.SkipLine(2);
                        }
                        if (ctx.Request.Headers != null)
                        {
                            ctxLine = 19; details.AddNameValueCollection("Headers", ctx.Request.Headers);
                        }
                    }
                    catch (Exception ex)
                    {
                        details.AddParam("Error in context, line #:", ctxLine.ToString());
                    }
                }
                        
            }
            public void AddGeneralInfo()
            {
                var details = this;
                details.AddHeader("General Info");
                details.AddParam("Date", CS.General_v3.Util.Date.Now.ToLongDateString() + " " + CS.General_v3.Util.Date.Now.ToLongTimeString());

            }

            public void AddExceptionDetails(int exceptionLevel, Exception exception)
            {
                
                var details = this;
                string padding = CS.General_v3.Util.Text.RepeatText(" ", exceptionLevel * 4);
                details.AddHeader(padding + exception.GetType().Name + ": " + exception.Message);
                details.TypeName = exception.GetType().ToString();
                details.ErrorTitle = exception.Message;
                details.AddParam("Type", exception.GetType().ToString());
                details.AddParam("Message", exception.Message);
                if (exception.TargetSite != null)
                {
                    details.AddParam("Method Name:", exception.TargetSite.Name);
                    if (exception.TargetSite.Module != null)
                        details.AddParam("Module:", exception.TargetSite.Module.Name);
                }

                details.SkipLine(2);
                

                details.AddParam("StackTrace", exception.StackTrace);
                details.AddParam("Source", exception.Source);
                   
            }

            public void LogToFile(Exception mainEx)
            {
                if (_log.IsFatalEnabled)
                {
                    _log.Fatal(this.GetAsString(), mainEx);
                }
                
            }
        }
        internal static bool validateErrorMessage(Exception ex)
        {
            bool ok = true;
            if (ex != null)
            {
                string msg = ex.Message;
                if (ex is HttpException)
                {
                    HttpException httpEx = (HttpException)ex;
                    if (Regex.IsMatch(msg, "The file .*? does not exist", RegexOptions.IgnoreCase))
                    {
                        ok = false;
                    }
                }
            }
            return ok;
        }

        //public static void ReportError(string errorContents, bool sendAlways = false)
        //{
        //    _ReportErrorInLogFile(new Exception(errorContents));
        //    ErrorToReport error = new ErrorToReport(errorContents, sendAlways);
        //    error.SendErrorOnSeperateThread();
        //    return;

        //    //ErrorDetails details = new ErrorDetails();
        //    //bool ok = true;
        //    //string url = "";
        //    //bool sendEmail = true;

            
        //    //if (!Enums.SETTINGS_ENUM.Others_SendEmailsOnError.GetSettingValue<bool>())
        //    //{
        //    //    sendEmail = false;
        //    //}
        //    //if (ok &&
        //    //    (CS.General_v3.Util.Other.IsLocalTestingMachine
        //    //        && !SEND_ERRORS_FOR_LOCALHOST))
        //    //{
        //    //    sendEmail = false;

        //    //}
        //    //if (sendAlways)
        //    //{
        //    //    sendEmail = true;
        //    //}
        //    //if (ok && sendEmail)
        //    //{
        //    //    try
        //    //    {
        //    //        ok = true;
        //    //        details.AddHeader("Error Details");
        //    //        details.AddLine(errorContents);
        //    //        details.AddParam("Stack Trace", Environment.StackTrace);
        //    //    }
        //    //    catch (Exception errorEx)
        //    //    {
        //    //        details.SkipLine(3);
        //    //        details.AddParam("Error Occured", "while capturing error info!");
        //    //        details.AddParam("Message", errorEx.Message);
        //    //        ok = false;
        //    //    }
        //    //}
        //    //if (ok && sendEmail)
        //    //{
        //    //    details.AddFooter();
        //    //    details.SendEmail();
        //    //}
        
        //}
        private static void _ReportErrorInLogFile(Exception ex)
        {
            if (_log.IsFatalEnabled)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("Fatal Error");
                if (System.Web.HttpContext.Current != null)
                {
                    if (System.Web.HttpContext.Current.Request != null)
                    {
                        if (System.Web.HttpContext.Current.Request.Url != null)
                        {
                            sb.Append(" - Url: " + System.Web.HttpContext.Current.Request.Url.AbsolutePath);
                        }
                    }
                }

                _log.Fatal(sb.ToString(), ex);
                //_log.Fatal("Fatal Error: " + details.GetAsString(), this.ErrorException);
                //CS.General_v3.Util.ErrorsUtil.WriteExceptionToErrorLog(this.ErrorException);
            }
        }
        public static void ReportError(string s, bool sendAlways = false)
        {
            Exception ex = new Exception(s);
            ReportError(ex, sendAlways);
        }
        public static void ReportError(Exception ex, bool sendAlways = false)
        {
            //_ReportErrorInLogFile(ex);
          //  ErrorToReport error = new ErrorToReport(ex, sendAlways);
          //  error.SendErrorOnSeperateThread();
            return;
            //ErrorDetails details = new ErrorDetails();
            //bool ok = true;
            //string url = "";
            //HttpContext ctx = HttpContext.Current;
            //bool sendEmail = true;
            
            //if (ctx != null && ctx.Request != null && ctx.Request.Url != null)
            //{
            //    url = ctx.Request.Url.ToString().ToLower();
            //}
            //if (!Enums.SETTINGS_ENUM.Others_SendEmailsOnError.GetSettingValue<bool>())
            //{
            //    sendEmail = false;
            //}
            //if (ok &&
            //    (CS.General_v3.Util.Other.IsLocalTestingMachine
            //        && !SEND_ERRORS_FOR_LOCALHOST))
            //{
            //    sendEmail = false;

            //}
            //if (sendAlways)
            //{
            //    sendEmail = true;
            //}
            //if (ok)
            //{
            //    try
            //    {
            //        ok = true;

            //        Exception exception = ex;
            //        string currentIdentifier = "MainException";
            //        do
            //        {
            //            details.AddHeader(currentIdentifier);
            //            ok = validateErrorMessage(exception);


            //            if (ok && CheckException != null)
            //            {
            //                ok = CheckException(exception);
            //            }
            //            if (ok)
            //            {
            //                details.AddExceptionDetails(exception);
            //            }
            //            exception = exception.InnerException;
            //            if (exception != null)
            //            {
            //                currentIdentifier += ".InnerException";
            //            }
            //        } while (exception != null);
            //        exception = ex;
            //        currentIdentifier = "MainException";
            //        do
            //        {
            //            currentIdentifier += ".BaseException";
            //            if (exception.GetBaseException() != exception)
            //                exception = exception.GetBaseException();
            //            else
            //                exception = null;
            //            if (exception != null)
            //            {
            //                if (ok && CheckException != null)
            //                {
            //                    ok = CheckException(exception);
            //                }
            //                if (ok)
            //                {
            //                    details.AddExceptionDetails(exception);
            //                }
            //            }

            //        } while (exception != null);
            //    }
            //    catch (Exception errorEx)
            //    {
            //        details.SkipLine(3);
            //        details.AddParam("Error Occured", "while capturing error info!");
            //        details.AddParam("Message", errorEx.Message);
            //    }
            //}
            //if (ok)
            //{
            //    details.AddFooter();
            //    if (sendEmail)
            //    {
            //        details.SendEmail();
            //    }
            //    if (_log.IsFatalEnabled)
            //    {
            //        _log.Fatal("Fatal Error - Send Email: <" + CS.General_v3.Util.Other.BoolToText(sendEmail) + ">");
            //        _log.Fatal("Fatal Error: " + details.GetAsString(), ex);
            //        CS.General_v3.Util.ErrorsUtil.WriteExceptionToErrorLog(ex);
            //    }
            //}
            ////if (ex.GetBaseException() != null && ex.GetBaseException() != ex)
            //    //details.ReportError(ex.GetBaseException(), sendAlways: sendAlways);

        }
        void context_Error(object sender, EventArgs e)
        {
            HttpContext ctx = HttpContext.Current;
            Exception exception = ctx.Server.GetLastError();
            ReportError(exception);
        }

        #endregion
    }
}
