﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Web;
using System.Text.RegularExpressions;
using log4net;
using System.Threading;

namespace CS.General_v3.Error
{
    public class Reporter : IHttpModule
    {
        private static ILog _log = log4net.LogManager.GetLogger(typeof(Reporter));
         
 
        static int _unhandledExceptionCount = 0;
 
        static string _sourceName = null;
        static object _initLock = new object();
        static bool _initialized = false;
 
        public void Init(HttpApplication app)
        {
           // _log.Debug("Initializing Error-Reporter [Start]");
            // Do this one time for each AppDomain.
            if (!_initialized)
            {
                lock (_initLock)
                {
                    if (!_initialized)
                    {
 
                        //string webenginePath = Path.Combine(RuntimeEnvironment.GetRuntimeDirectory(), "webengine.dll");
 
                        //if (!File.Exists(webenginePath))
                        //{
                        //    throw new Exception(String.Format(CultureInfo.InvariantCulture,
                        //        "Failed to locate webengine.dll at '{0}'.  This module requires .NET Framework 2.0.",
                        //        webenginePath));
                        //}
 
                        //FileVersionInfo ver = FileVersionInfo.GetVersionInfo(webenginePath);
                        //_sourceName = string.Format(CultureInfo.InvariantCulture, "ASP.NET {0}.{1}.{2}.0",
                        //    ver.FileMajorPart, ver.FileMinorPart, ver.FileBuildPart);
 
                        //if (!EventLog.SourceExists(_sourceName))
                        //{
                        //    throw new Exception(String.Format(CultureInfo.InvariantCulture,
                        //        "There is no EventLog source named '{0}'. This module requires .NET Framework 2.0.",
                        //        _sourceName));
                        //}
 
                        AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(OnUnhandledException);
                       
                       
                      //  app.BeginRequest += new EventHandler(app_BeginRequest);
                        _initialized = true;
                    }
                }
            }
            app.Error += new EventHandler(app_Error);
           // _log.Debug("Initializing Error-Reporter [Finish]");
        }

        //void app_BeginRequest(object sender, EventArgs e)
        //{
        //    if (HttpContext.Current != null && HttpContext.Current.ApplicationInstance != null)
        //    {
        //      //  HttpContext.Current.ApplicationInstance.Error += new EventHandler(app_Error);
        //    }
        //}
        
        void app_Error(object sender, EventArgs e)
        {
            try
            {
                HttpContext ctx = HttpContext.Current;
                Exception exception = ctx.Server.GetLastError();
                ReportError(Enums.LOG4NET_MSG_TYPE.Warn, exception);
                
            }
            catch (Exception ex)
            {
                _log.Warn("Error while in app_Error - Type: " + ex.GetType().FullName + " | Msg: " + ex.Message + " | StackTrace: " + ex.StackTrace);
            }
        }
 
        public void Dispose()
        {
        }


        public static void ReportError(Enums.LOG4NET_MSG_TYPE msgType, string msg, Exception innerEx = null, bool sendAsync = true)
        {
            var ex = new InvalidOperationException(msg, innerEx);
            ReportError(msgType, ex, sendAsynch: sendAsync);

        }
        public static void ReportError(Enums.LOG4NET_MSG_TYPE msgType, Exception exception, bool sendAsynch = true)
        {
            try
            {

               
                // Let this occur one time for each AppDomain.
                //if (Interlocked.Exchange(ref _unhandledExceptionCount, 1) != 0)
                //    return;

                ErrorReporter rep = ErrorReporter.CreateInstance();
                rep.ErrorLog4NetType = msgType;
                
                Exception ex = (Exception)exception;
                rep.SendError(ex, false, sendAsynch);
            }
            catch (Exception ex)
            {
                _log.Warn("Error while in ReportError - Type: " + ex.GetType().FullName + " | Msg: " + ex.Message + " | StackTrace: " + ex.StackTrace);
            }
        }

        void OnUnhandledException(object o, UnhandledExceptionEventArgs e)
        {
            try
            {
                ReportError(Enums.LOG4NET_MSG_TYPE.Warn, (Exception)e.ExceptionObject);
            }
            catch (Exception ex)
            {
                _log.Warn("Error while in OnUnhandledException - Type: " + ex.GetType().FullName + " | Msg: " + ex.Message + " | StackTrace: " + ex.StackTrace);
            }

        }
 
    }
}