﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.MediaItems.ImageSizes
{
    public class ImageSizeInfoBase
    {

        public ImageSizeInfoBase()
        {

        }
        public ImageSizeInfoBase(string title, string folder = null, int width = 0, int height = 0,
            Enums.CROP_IDENTIFIER cropIdentifier = Enums.CROP_IDENTIFIER.None)
        {
            init(title, null, folder, width, height, cropIdentifier);

        }
        public ImageSizeInfoBase(Enum title, string folder = null, int width = 0, int height = 0,
            Enums.CROP_IDENTIFIER cropIdentifier = Enums.CROP_IDENTIFIER.None)
        {
            init(null, title, folder, width, height, cropIdentifier);

        }

        private void init(string titleStr = null, Enum titleEnum = null, string folder = null, int width = 0, int height = 0,
            Enums.CROP_IDENTIFIER cropIdentifier = Enums.CROP_IDENTIFIER.None)
        {
            string sTitle = titleStr;
            if (string.IsNullOrEmpty(sTitle) && titleEnum != null)
            {
                sTitle = CS.General_v3.Util.EnumUtils.StringValueOf(titleEnum);
            }

            this.Title = sTitle;
            this.Folder = folder;
            this.Width = width;
            this.Height = height;
            this.CropIdentifier = cropIdentifier;
            this.Extension = "jpg";

            
        }

        public void CopyFromOtherImageSize(ImageSizeInfoBase imageSize, bool copyEmptyValues = false)
        {
            if (!string.IsNullOrWhiteSpace(imageSize.Folder) || copyEmptyValues) this.Folder = imageSize.Folder;
            if (!string.IsNullOrWhiteSpace(imageSize.Title) || copyEmptyValues) this.Title = imageSize.Title;
            this.Width = imageSize.Width;
            this.Height = imageSize.Height;
            this.CropIdentifier = imageSize.CropIdentifier;
            
        }
        public string Title {get;set;}
    
        private string _Folder = null;
        /// <summary>
        /// Folder where to store the original photo.  Should NOT start with a /, and SHOULD end with a /.  E.g uploads/products/
        /// </summary>
        public string Folder
        {
            get
            {
                return _Folder;
            }
            set
            {
                _Folder = value;
                if (_Folder != null && _Folder.Length > 0)
                {
                    if (_Folder[_Folder.Length - 1] != '/')
                        _Folder += "/";
                    if (_Folder.Length > 0 && _Folder[0] == '/')
                        _Folder = _Folder.Remove(0, 1);

                }
            }
        }
        public string Extension { get; set; }
        public int Width { get; set; }
        public int Height {get;set;}
        public bool FillBox
        {
            get
            {
                return (this.CropIdentifier != Enums.CROP_IDENTIFIER.None);
            }
        }

        public CS.General_v3.Enums.CROP_IDENTIFIER CropIdentifier = CS.General_v3.Enums.CROP_IDENTIFIER.MiddleMiddle;
        
    }
}
