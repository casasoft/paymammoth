﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.MediaItems.ImageSizes
{
    public interface IImageSizing
    {
        int OriginalMaxWidth { get;  }
        int OriginalMaxHeight { get;  }
        string Identifier { get; }
        string BaseUploadFolder { get; }
        List<ISpecificImageSize> SpecificImageSizes {get;}
    }
}
