﻿using System;

namespace CS.General_v3.Classes.MediaItems.ImageSizes
{
    public class ImageDefaultSizeInfoAttribute : Attribute
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public CS.General_v3.Enums.CROP_IDENTIFIER CropType { get; set; }
        public ImageDefaultSizeInfoAttribute()
        {
            this.CropType = CS.General_v3.Enums.CROP_IDENTIFIER.None;

        }
        
        public string GetAsStringValue()
        {
            return Width + "x" + Height + "-" + CS.General_v3.Util.EnumUtils.StringValueOf(CropType);
        }
        public override string ToString()
        {
            return GetAsStringValue();
        }

    }
}
