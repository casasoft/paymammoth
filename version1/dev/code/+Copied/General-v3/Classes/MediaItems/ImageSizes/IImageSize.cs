﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.MediaItems.ImageSizes
{
    public interface ISpecificImageSize
    {
        int Width { get;  }
        int Height { get;  }
        Enums.CROP_IDENTIFIER CropIdentifier { get;  }
        string Extension { get; }
        string Folder { get; }
        string Title { get; }

    }
}
