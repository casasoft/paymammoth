﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.HelperClasses;
using System.IO;

namespace CS.General_v3.Classes.MediaItems.ImageSizes
{
    public class ImageSizeInfo : ImageSizeInfoBase
    {
        public static ImageSizeInfo LoadImageSizeInfoFromSettings(MediaItemImageBase image, string baseFolder, string title, string settingName)
        {
            int width, height;
            string folder;
            Enums.CROP_IDENTIFIER cropIdentifier;
            CS.General_v3.Settings.ImageInfo.GetImageInfoFromSettings(settingName, "1600x1200-None", out width, out height, out cropIdentifier);
            

            folder = baseFolder + title + "/";

            ImageSizeInfo sizeInfo = new ImageSizeInfo(image, title, folder, width, height, cropIdentifier);
            return sizeInfo;
        }


        public delegate ImageSizeInfoBase ImageSizeInfoDelegate();

        public ImageSizeInfoDelegate ImageSizeInfoToLoadFrom = null;

        private bool checkLoadFromDelegate()
        {
            bool ok = false;
            if (ImageSizeInfoToLoadFrom != null)
            {

                var size = ImageSizeInfoToLoadFrom();
                this.CopyFromOtherImageSize(size);
                ok = true;
            }
            return ok;
        }

        private MediaItemImageBase _image = null;
        
        public ImageSizeInfo(MediaItemImageBase image, ImageSizeInfoDelegate loadFromDelegate)
        {
            init(image);
            this.ImageSizeInfoToLoadFrom = loadFromDelegate;
            checkLoadFromDelegate();
        }
        private void init(MediaItemImageBase image)
        {
            this._image = image;
            this.Extension = "jpg";

        }
        public ImageSizeInfo(MediaItemImageBase image, string title, string folder, int w, int h, CS.General_v3.Enums.CROP_IDENTIFIER cropIdentifier)
            : base(title,folder,w,h,cropIdentifier)
        {
            System.Diagnostics.Contracts.Contract.Requires(!string.IsNullOrWhiteSpace(title), "Title cannot be null or empty");
            init(image);

            // this.Extension = extension;
            this.Folder = folder;
            this.Title = title;
            this.Width = w;
            this.Height = h;
            
            this.CropIdentifier = cropIdentifier;
        }
        public void CheckFile(bool overwriteExisting)
        {
            
            
            checkLoadFromDelegate();
            string localPath = _image.GetOriginalLocalPath();
            string newLocalPath = _image.getLocalPath(this.Title, false);
          
            if (System.IO.File.Exists(localPath))
            {

                if (overwriteExisting || !System.IO.File.Exists(newLocalPath))
                {
                    if (this._image.checkIfExtensionCanBeResized())
                    {
                        if (this.FillBox)
                        {
                            CS.General_v3.Util.Image.ResizeAndCropImage(localPath, this.Width, this.Height, newLocalPath, this.CropIdentifier);
                        }
                        else
                        {
                            CS.General_v3.Util.Image.ResizeImage(localPath, this.Width, this.Height, newLocalPath);
                        }
                    }
                    else
                    {
                        CS.General_v3.Util.IO.CopyFile(localPath, newLocalPath);
                    }
                }
            }
        }
        public OperationResult RemoveFile()
        {
            OperationResult result = new OperationResult();
            
            string path = _image.getLocalPath(this.Title, false);
            if (File.Exists(path))
            {
                CS.General_v3.Util.IO.DeleteFile(path);
            }
            else
            {
                result.StatusMessages.Add(new OperationResultMsg( Enums.STATUS_MSG_TYPE.Warning, "File for size " + this.Title + " could not be deleted as it does not exist"));

            }
            return result;
            
        }
    }
}
