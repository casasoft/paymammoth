﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace CS.General_v3.Classes.MediaItems
{
    /// <summary>
    /// To use the image, you need to define the Image Sizes.  You can use either
    /// 
    ///   - AddImageSize
    ///   - AddImageSizeFromSettings
    ///   
    /// Also, you need to load the Original Image Sizes, which can be done by LoadOriginalImageSizesFromSettings()
    ///   
    /// </summary>
    public abstract class ImageWithDBObject : ImageBase
    {
        protected IMediaItemDB _mediaItemInfo = null;
        protected ImageWithDBObject(IMediaItemDB mediaItem)
        {
            initImageWithDBObject(mediaItem);
        }

        
        public ImageWithDBObject(IMediaItemDB mediaItem, string baseUploadFolder,
            List<ImageSizeInfo> customSizes = null, int? originalImageMaxWidth = null, int? originalImageMaxHeight = null,
            List<ImageSizeInfo.ImageSizeInfoDelegate> loadFromDelegates = null, IEnumerable<string> sizesToLoadFromSettings = null)
            : base(baseUploadFolder, customSizes,originalImageMaxWidth,originalImageMaxHeight,loadFromDelegates, sizesToLoadFromSettings)
        {

            initImageWithDBObject(mediaItem);
        }
        protected void initImageWithDBObject(IMediaItemDB mediaItem)
        {
            _mediaItemInfo = mediaItem;
        }
        

        public override int Priority
        {
            get
            {
                return _mediaItemInfo.Priority;
            }
            set
            {
                _mediaItemInfo.Priority = value;
            }
        }

        public override string Caption
        {
            get
            {
                return _mediaItemInfo.Caption;
            }
            set
            {
                _mediaItemInfo.Caption = value;
            }
        }

        public override void Save()
        {
            _mediaItemInfo.Save();
        }


        public override string ExtraValueChoice
        {
            get
            {
                return _mediaItemInfo.ExtraValueChoice;
            }
            set
            {
                _mediaItemInfo.ExtraValueChoice = value;
            }
        }
        public override void SetMediaItem(Stream stream, string filename)
        {
            
            base.SetMediaItem(stream, filename);

            
        }
        public override void Delete()
        {
            _mediaItemInfo.OnDeletedItem();
            
            base.Delete();
        }

        protected override void generateIdentifier(string uploadedFilename)
        {
            _mediaItemInfo.Identifier = _mediaItemInfo.GenerateFilename(CS.General_v3.Util.IO.GetFilenameOnly(uploadedFilename), CS.General_v3.Util.IO.GetExtension(uploadedFilename));


        }
        public override void UploadFile(Stream s, string fileName)
        {
            base.UploadFile(s, fileName);
            _mediaItemInfo.Save();
        }
        
    }
}
