﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using CS.General_v3.Classes.HelperClasses;

namespace CS.General_v3.Classes.MediaItems
{
    public class MediaItemTemp : IMediaItem
    {
        #region IMediaItem Members

        public int Priority { get; set; }

        public Enums.ITEM_TYPE ItemType { get; set; }

        public string ThumbnailImageUrl { get; set; }

        public string LargeImageUrl { get; set; }

        public string OriginalItemUrl { get; set; }

        public string Caption { get; set; }

        public string Identifier { get; set; }
        public string GetIdentifier()
        {
            return this.Identifier;
        }
        public string Extension { get; set; }
        public int FileSizeKB { get; set; }

        public OperationResult Remove()
        {
            return new OperationResult();
        }

        public OperationResult SetMediaItem(CS.General_v3.Controls.WebControls.Common.MyFileUpload txt)
        {
            return new OperationResult();
        }

        public OperationResult SetMediaItem(Stream fileContents, string filename)
        {
            return new OperationResult();
        }

        public OperationResult Save()
        {
            return new OperationResult();
        }

        public OperationResult SetMainItem()
        {
            return new OperationResult();
        }

        public bool IsEmpty()
        {
            return String.IsNullOrEmpty(GetThumbnailImageUrl()) && String.IsNullOrEmpty(GetLargeImageUrl()) && String.IsNullOrEmpty(GetMediaItemUrl());
        }

        #endregion

        #region IMediaItem Members


        public void RemoveFromDB()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IMediaItem Members

        public virtual string ExtraValueChoice { get; set; }
        

        #endregion

        #region IMediaItem Members


        public int Width
        {
            get;
            set;
        }

        public int Height
        {
            get;
            set;
        }

        #endregion

        public string GetThumbnailImageUrl()
        {
            return this.ThumbnailImageUrl;

        }
        public string GetLargeImageUrl()
        {
            return this.LargeImageUrl;
        }


        public string GetMediaItemUrl()
        {
            return this.OriginalItemUrl;
        }
        public int FileSizeInKB = 0;
        public int GetFileSizeInKB()
        {
            return this.FileSizeInKB;
        }

        public OperationResult SetAsMainItem()
        {
            return new OperationResult();
        }


        public OperationResult Delete()
        {
            return new OperationResult();
        }

        #region IMediaItem Members


        public IEnumerable<string> GetAllowedFileExtensions()
        {
            return null;
            
        }

        #endregion
    }
}
