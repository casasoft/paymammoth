﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace CS.General_v3.Classes.MediaItems
{
    /// <summary>
    /// To use the image, you need to define the Image Sizes.  You can use either
    /// 
    ///   - AddImageSize
    ///   - AddImageSizeFromSettings
    ///   
    /// Also, you need to load the Original Image Sizes, which can be done by LoadOriginalImageSizesFromSettings()
    ///   
    /// </summary>
    public class ImageWithDBObjectAndEnum<TEnum> : ImageWithDBObject
        where TEnum : struct
    {

        protected ImageWithDBObjectAndEnum(IMediaItemDB mediaItem) : base(mediaItem)
        {
            

            
        }

        protected string baseEnumSetting = null;
        public ImageWithDBObjectAndEnum(IMediaItemDB mediaItem, string baseUploadFolder,
            string baseEnumSetting,
            List<ImageSizeInfo> customSizes = null, int? originalImageMaxWidth = null, int? originalImageMaxHeight = null,
            List<ImageSizeInfo.ImageSizeInfoDelegate> loadFromDelegates = null, IEnumerable<string> sizesToLoadFromSettings = null)
            : base(mediaItem, baseUploadFolder, customSizes, originalImageMaxWidth, originalImageMaxHeight, loadFromDelegates, sizesToLoadFromSettings)
        {

            initImageWithDBObjectAndEnum(baseEnumSetting);
        }

        

        protected void initImageWithDBObjectAndEnum(string baseEnumSetting)
        {
            this.baseEnumSetting = baseEnumSetting;
            var enumValues = CS.General_v3.Util.EnumUtils.GetListOfEnumValues<TEnum>();
            foreach (var enumValue in enumValues)
            {
                string enumTitle = CS.General_v3.Util.EnumUtils.StringValueOf<TEnum>(enumValue);
                var sizeInfo = this.getImageSize(enumTitle);
                if (sizeInfo == null)
                {
                    string folder = this.OriginalImageFolder + CS.General_v3.Util.IO.ConvertStringToFilename(enumTitle) + "/";
                    bool result = AddImageSizeFromSettings(enumTitle, baseEnumSetting);
                    if (!result)
                    {
                        //todo: 30/12/2010 test this out
                        throw new InvalidOperationException("initImageWithDBObjectAndEnum: Enum with title <" + enumTitle + "> does not have a corresponding size info!");
                    }
                }
                

            }
        }

        /// <summary>
        /// Returns the path from root, WITHOUT the start '/'
        /// </summary>
        /// <param name="prefix">A prefix, before the identifier</param>
        /// <param name="suffix">A suffix, after the identifier</param>
        public string GetWebPathFromRoot(TEnum enumValue)
        {
            //todo: 30/12/10 test this out GetWebPathFromRoot()
            string sValue = CS.General_v3.Util.EnumUtils.StringValueOf<TEnum>(enumValue);
            return base.getWebPathFromRoot(sValue);

            
        }


        public override void SetAsMainItem()
        {
            _mediaItemInfo.SetAsMainItem();
        }

        public override string Identifier
        {
            get
            {
                return _mediaItemInfo.Identifier;
            }
            set { _mediaItemInfo.Identifier = value; }
        }

    }
}
