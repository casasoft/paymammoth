﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.MediaItems.ImageSizes;
using CS.General_v3.Classes.DbObjects;

namespace CS.General_v3.Classes.MediaItems
{
    /// <summary>
    /// To use the image, you need to define the Image Sizes.  You can use either
    /// 
    ///   - AddImageSize
    ///   - AddImageSizeFromSettings
    ///   
    /// Also, you need to load the Original Image Sizes, which can be done by LoadOriginalImageSizesFromSettings()
    ///   
    /// </summary>
    public abstract class MediaItemImageWithDBObject : MediaItemImageBase, IMediaItemImageDB
    {
        protected IMediaItemImageDB _mediaItemInfo = null;

        public new virtual IBaseDbObject Item
        {
            get { return (IBaseDbObject) base.Item; }
            set { base.Item = value; }
        }
        
        public MediaItemImageWithDBObject(IMediaItemImageDB mediaItem)
            : base(mediaItem.ImageSizing)
        {
            
            initImageWithDBObject(mediaItem);
        }
        protected void initImageWithDBObject(IMediaItemImageDB mediaItem)
        {
            _mediaItemInfo = mediaItem;
        }
        

        public override int Priority
        {
            get
            {
                return _mediaItemInfo.Priority;
            }
            set
            {
                _mediaItemInfo.Priority = value;
            }
        }

        public override string Caption
        {
            get
            {
                return _mediaItemInfo.Caption;
            }
            set
            {
                _mediaItemInfo.Caption = value;
            }
        }

        public override OperationResult Save()
        {
            
            return _mediaItemInfo.Save();
        }


        public override string ExtraValueChoice
        {
            get
            {
                return _mediaItemInfo.ExtraValueChoice;
            }
            set
            {
                _mediaItemInfo.ExtraValueChoice = value;
            }
        }
        public override OperationResult SetMediaItem(Stream stream, string filename)
        {
            
            return base.SetMediaItem(stream, filename);

            
        }
        public override OperationResult Delete()
        {
            _mediaItemInfo.OnDeletedItem();
            
            return base.Delete();
        }

        protected override void generateIdentifier(string uploadedFilename)
        {
            var identifier = _mediaItemInfo.GenerateFilename(CS.General_v3.Util.IO.GetFilenameOnly(uploadedFilename), CS.General_v3.Util.IO.GetExtension(uploadedFilename));
            _mediaItemInfo.Identifier = identifier;


        }
        public virtual OperationResult UploadFileFromPhysicalPath(string path)
        {
            try
            {
                string localPath = path;
                FileStream fs = new FileStream(localPath, FileMode.Open, FileAccess.Read);
                var result = this.UploadFile(fs, CS.General_v3.Util.IO.GetFilenameAndExtension(localPath));
                fs.Dispose();
                return result;
            }
            catch (FileNotFoundException ex)
            {
                OperationResult result = new OperationResult();
                result.AddException(ex);
                return result;
            }
            catch (FileLoadException ex)
            {
                OperationResult result = new OperationResult();
                result .AddException(ex);
                return result;

            }
            
        }
        public override OperationResult UploadFile(Stream s, string fileName, bool autoSave = true)
        {
            var result = base.UploadFile(s, fileName);
            if (result.Status == Enums.STATUS_MSG_TYPE.Success)
            {
                if (autoSave)
                    result.AddFromOperationResult(_mediaItemInfo.Save());
            }
            return result;
        }


        #region IMediaItemDB Members


        public virtual string GenerateFilename(string uploadedFilenameOnly, string uploadedFilenameExtension)
        {
            return uploadedFilenameOnly + "." + uploadedFilenameExtension;
        }

        public virtual void OnDeletedItem()
        {
            
        }

        #endregion
    }
}
