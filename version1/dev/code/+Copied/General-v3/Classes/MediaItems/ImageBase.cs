﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace CS.General_v3.Classes.MediaItems
{
    /// <summary>
    /// To use the image, you need to define the Image Sizes.  You can use either
    /// 
    ///   - AddImageSize
    ///   - AddImageSizeFromSettings
    ///   
    /// Also, you need to load the Original Image Sizes, which can be done by LoadOriginalImageSizesFromSettings()
    ///   
    /// </summary>
    public abstract class ImageBase : MediaItem, IMediaItem
    {
        protected ImageBase()
        {

        }
        
        public ImageBase(string baseUploadFolder, List<ImageSizeInfo> customSizes= null, int? originalImageMaxWidth = null, int? originalImageMaxHeight= null, 
            List<ImageSizeInfo.ImageSizeInfoDelegate> loadFromDelegates = null, IEnumerable<string> sizesToLoadFromSettings = null)
        {

            initImageBase(baseUploadFolder, customSizes, originalImageMaxWidth, originalImageMaxHeight, loadFromDelegates, sizesToLoadFromSettings);
        }
        protected void initImageBase(string baseUploadFolder, List<ImageSizeInfo> customSizes, int? originalImageMaxWidth, int? originalImageMaxHeight, List<ImageSizeInfo.ImageSizeInfoDelegate> loadFromDelegates, IEnumerable<string> sizesToLoadFromSettings)
        {
            this.OriginalImageFolder = baseUploadFolder;

            this.imageSizes = new List<ImageSizeInfo>();
            if (customSizes != null)
                this.imageSizes.AddRange(customSizes);

            
            if (originalImageMaxWidth == null) originalImageMaxWidth = 1600;
            if (originalImageMaxHeight == null) originalImageMaxHeight = 1600;
            this.OriginalImageMaxWidth = originalImageMaxWidth.Value;
            this.OriginalImageMaxHeight = originalImageMaxHeight.Value;
            this.OriginalImageExtension = "jpg";

            if (sizesToLoadFromSettings != null)
            {
                foreach (var sSize in sizesToLoadFromSettings)
                {

                    AddImageSizeFromSettings(sSize);
                }
            }
            if (loadFromDelegates != null)
            {
                for (int i = 0; i < loadFromDelegates.Count; i++)
                {
                    var loadFromDel = loadFromDelegates[i];
                    ImageSizeInfo size = new ImageSizeInfo(this ,loadFromDel);
                    this.imageSizes.Add(size);
                }
            }
            

        }
        public override CS.General_v3.Enums.ITEM_TYPE ItemType { get { return CS.General_v3.Enums.ITEM_TYPE.Image; } set { } }
        public override string Extension
        {
            get
            {
                if (string.IsNullOrEmpty(base.Extension))
                    base.Extension = "jpg";

                return base.Extension;
            }
            set
            {
                base.Extension = value;
            }
        }
        private string _OriginalImageFolder = null;
        /// <summary>
        /// Folder where to store the original photo.  Should NOT start with a /, and SHOULD end with a /.  E.g uploads/products/
        /// </summary>
        public string OriginalImageFolder
        {
            get
            {
                return _OriginalImageFolder;
            }
            set
            {
                _OriginalImageFolder = value;
                if (_OriginalImageFolder != null && _OriginalImageFolder.Length > 0)
                {
                    if (_OriginalImageFolder[_OriginalImageFolder.Length - 1] != '/')
                        _OriginalImageFolder += "/";
                    if (_OriginalImageFolder.Length > 0 && _OriginalImageFolder[0] == '/')
                        _OriginalImageFolder = _OriginalImageFolder.Remove(0, 1);

                }
            }
        }
        /// <summary>
        /// The extension for the original image, without the '.'
        /// </summary>
        public string OriginalImageExtension {get;set;}
        
        /*
        public override void SetIdentifier(string identifier)
        {
            if (!string.IsNullOrEmpty(identifier))
            {
                if (identifier == null)
                    identifier = "";
                string oldIdentifier = this.Identifier;
                if (oldIdentifier == null) oldIdentifier = "";
                if (identifier.CompareTo(oldIdentifier) != 0 && !string.IsNullOrEmpty(oldIdentifier))
                {
                    string oldPath = GetOriginalLocalPath();
                    base.SetIdentifier(identifier);
                    string newPath = GetOriginalLocalPath();
                    base.SetIdentifier(oldIdentifier);
                    if (System.IO.File.Exists(newPath))
                        System.IO.File.Delete(newPath);
                    if (System.IO.File.Exists(oldPath))
                        System.IO.File.Copy(oldPath, newPath);
                    //Remove();

                    CheckImageSizes(true);
                }
            }
            base.SetIdentifier(identifier);
        }*/
        /// <summary>
        /// Original Image maximum width.  Default is 1600
        /// </summary>
        public int OriginalImageMaxWidth { get; set; }
        /// <summary>
        /// Original Image maximum height. Default is 1200
        /// </summary>
        public int OriginalImageMaxHeight { get; set; }
        

     
        protected List<ImageSizeInfo> imageSizes = null;
        /// <summary>
        /// Adds an image size from the settings file.  Settings are 'Folder','W','H','FillsBox','CropIdentifier'.
        /// </summary>
        /// <param name="title">Title of image size</param>
        /// <param name="baseSettingName">Base setting node.  Must end with a '/' (Added if not)</param>
        public bool AddImageSizeFromSettings(string title, string baseSettingName, bool throwErrorIfSettingNotFound = true)
        {
            if (!baseSettingName.EndsWith("/"))
                baseSettingName += "/";
            if (baseSettingName.StartsWith("/"))
                baseSettingName = baseSettingName.Remove(0, 1);

            int width, height;
            
            CS.General_v3.Enums.CROP_IDENTIFIER cropIdentifier;
            bool result = CS.General_v3.Settings.ImageInfo.GetImageInfoFromSettings(baseSettingName + title, out width, out height,  out cropIdentifier);
            string folder = this.OriginalImageFolder + CS.General_v3.Util.IO.ConvertStringToFilename(title) + "/";
            if (result)
                this.AddImageSize(title, folder, width, height,  cropIdentifier);
            else
            {
                if (throwErrorIfSettingNotFound)
                    throw new InvalidOperationException("AddImageSizeFromSettings: Could not find setting <" + baseSettingName + title + ">");

            }

            return result;
        }
        /// <summary>
        /// Adds an image size from the settings file.  Settings are 'Folder','W','H','FillsBox','CropIdentifier'.
        /// </summary>
        /// <param name="title">Title of image size</param>
        /// <param name="baseSettingName">Base setting node.  Must end with a '/' (Added if not)</param>
        public bool AddImageSizeFromSettings(string settingName,  bool throwErrorIfSettingNotFound = true)
        {
            int indexOf = settingName.LastIndexOf("/");
            if (indexOf == -1)
                indexOf = settingName.LastIndexOf("_");
            string title = settingName.Substring(indexOf + 1);
            string baseSetting = settingName.Substring(0, indexOf);
            
            return AddImageSizeFromSettings(title, baseSetting, throwErrorIfSettingNotFound);
        }
        /// <summary>
        /// Adds an image size to the list of images
        /// </summary>
        /// <param name="title">Title</param>
        /// <param name="folder">Folder name must not start with a '/' and must end in a '/'.  Will be converted as needed</param>
        /// <param name="width">Width</param>
        /// <param name="height">Height</param>
        /// <param name="FillBox">Whether to fill the box or not (by cropping)</param>
        /// <param name="cropIdentifier">Crop Identifier</param>
        public void AddImageSize(string title, string folder,
            int width, int height, CS.General_v3.Enums.CROP_IDENTIFIER cropIdentifier)
        {
            this.imageSizes.Add(new ImageSizeInfo(this,title,folder, width, height, cropIdentifier));
        }
        public void RemoveImageSize(string title)
        {
            ImageSizeInfo size = getImageSize(title);
            this.imageSizes.Remove(size);
        }

        /// <summary>
        /// Returns an IMAGE_SIZE_OF with the specified title
        /// </summary>
        /// <param name="title">Title</param>
        /// <returns>The IMAGE_SIZE_OF item</returns>
        protected ImageSizeInfo getImageSize(string title)
        {
            title = title.ToLower();
            for (int i = 0; i < imageSizes.Count; i++)
            {
                if (imageSizes[i].Title.ToLower() == title)
                    return imageSizes[i];
            }
            return null;
        }
        public void RemoveAllImageSizes()
        {
            this.imageSizes.Clear();
        }

        /// <summary>
        /// Loads values from the settings file.  The 'node' appended is named 'Original'
        /// </summary>
        /// <param name="baseSetting">Base setting node. </param>
        protected void loadOriginalImageValuesFromSettings(string baseSetting)
        {

            if (!baseSetting.EndsWith("/"))
                baseSetting += "/";
            if (baseSetting.StartsWith("/"))
                baseSetting = baseSetting.Remove(0, 1);
            string nodeName = "Original";
            int w, h;
            CS.General_v3.Settings.ImageInfo.GetImageInfoFromSettings(baseSetting + nodeName, out w, out h);

            this.OriginalImageMaxWidth = w;
            this.OriginalImageMaxHeight = h;
            //this.OriginalImageExtension = extension;


        }
        /// <summary>
        /// gets the smallest image path
        /// </summary>
        /// <returns></returns>
        public virtual string GetWebPathFromRootForSmallest()
        {
            string s = "";
            int leastArea = Int32.MaxValue;
            foreach (var size in this.imageSizes)
            {
                int area = size.Width * size.Height;
                if (area < leastArea)
                {
                    leastArea = area;
                    s = size.Title;
                }
            }
            return getWebPathFromRoot(s);

        }
        /// <summary>
        /// gets the smallest image path
        /// </summary>
        /// <returns></returns>
        public virtual string GetWebPathFromRootForLargest()
        {
            string s = "";
            int maxArea = 0;
            foreach (var size in this.imageSizes)
            {
                int area = size.Width * size.Height;
                if (area > maxArea)
                {
                    maxArea = area;
                    s = size.Title;
                }
            }
            return getWebPathFromRoot(s);

        }
        /// <summary>
        /// Get web path (from root, excluding start '/'), for image of type 'imageSize'
        /// </summary>
        /// <param name="imageSize"></param>
        /// <returns></returns>
        protected virtual string getWebPathFromRoot(string imageSize)
        {
            CheckImageSizes(false);
            ImageSizeInfo sizeInfo = getImageSize(imageSize);
            string s = base.getWebPathFromRoot(sizeInfo.Folder, null, "-"+sizeInfo.Title,this.Extension);
            return s;
        }

        
        internal virtual string getLocalPath(string imageSize, bool checkImageSizes)
        {
            if (checkImageSizes)
                CheckImageSizes(false);
            ImageSizeInfo sizeInfo = getImageSize(imageSize);
            string s = base.getLocalPath(sizeInfo.Folder, null, "-" + sizeInfo.Title, this.Extension);
            return s;
        }

        /// <summary>
        /// Returns the original image local path
        /// </summary>
        /// <returns></returns>
        public virtual string GetOriginalLocalPath()
        {
            string s = base.getLocalPath(this.OriginalImageFolder, null, null, this.OriginalImageExtension);
            return s;
        }
        /// <summary>
        /// Returns the original image web path (from root, excluding start '/')
        /// </summary>
        /// <returns></returns>
        public virtual string GetOriginalWebPathFromRoot()
        {
            string s = base.getWebPathFromRoot(this.OriginalImageFolder, null, null, this.OriginalImageExtension);
            return s;
        }
        /// <summary>
        /// Uploads an image, and resizes based on the existing sizes
        /// </summary>
        /// <param name="txt"></param>
        public void UploadFile(CS.General_v3.Controls.WebControls.Common.MyFileUpload txt)
        {
            this.UploadFile(txt.FileContent, txt.FileName);
        }

        /// <summary>
        /// Uploads an image, and resizes based on the existing sizes
        /// </summary>
        /// <param name="s"></param>
        public virtual void UploadFile(System.IO.Stream s, string fileName)
        {
            if (string.IsNullOrEmpty(this.OriginalImageFolder))
                throw new InvalidOperationException("Please specify 'OriginalImageFolder'");
            
            base.uploadFile(this.OriginalImageFolder, null, null, this.OriginalImageExtension,s, fileName);
            resizeOriginal();
            CheckImageSizes(true);

        }

        private void resizeOriginal()
        {
            string localPath = GetOriginalLocalPath();
            CS.General_v3.Util.Image.ResizeImage(localPath, this.OriginalImageMaxWidth, this.OriginalImageMaxHeight, localPath);

        }
        /// <summary>
        /// Checks the image sizes, and creates any ones that do not exist (or are to be overwritten)
        /// </summary>
        /// <param name="overwriteExisting">Overwrite existing files</param>
        public void CheckImageSizes(bool overwriteExisting)
        {
            string localPath = GetOriginalLocalPath();
            if (System.IO.File.Exists(localPath))
            {
                foreach (var size in this.imageSizes)
                {
                    size.CheckFile(overwriteExisting);
                }
            }
        }



        #region IMediaItem Members

        //public abstract int Priority {get;set;}
        private List<ImageSizeInfo> getImageSizesSortedBySize()
        {
            List<ImageSizeInfo> list = new List<ImageSizeInfo>();
            list.AddRange(this.imageSizes);
            list.Sort((size1, size2) => (size1.Width * size1.Height).CompareTo((size2.Width * size2.Height)));
            return list;

        }
        public override string GetThumbnailImageUrl()
        {
            List<ImageSizeInfo> list = getImageSizesSortedBySize();
            
            if (list.Count > 0)
            {
                return  getWebPathFromRoot(list[0].Title);
            }
            else
            {
                return null;
            }
        }

        public override string GetLargeImageUrl()
        {
            List<ImageSizeInfo> list = getImageSizesSortedBySize();
            int index = 0;
            if (list.Count > 0)
            {

                index = list.Count - 1;
                if (list.Count > 2)
                    index--;
                return getWebPathFromRoot(list[index].Title);
            }
            else
            {
                return null;
            }
        }

        public override string GetOriginalItemUrl()
        {
            List<ImageSizeInfo> list = getImageSizesSortedBySize();
            int index = 0;
            index = list.Count-1;
                
            if (list.Count > 0)
            {
                return getWebPathFromRoot(list[index].Title);
            }
            else
            {
                return null;
            }
        }

        //public abstract string Caption {get;set;}

        public override int GetFileSizeInKB()
        {
            return (int)CS.General_v3.Util.IO.GetFileSizeInKB(GetOriginalLocalPath()); 
        }

        public override void Delete()
        {
            string path = this.getLocalPath(this.OriginalImageFolder, null, null, this.OriginalImageExtension);
            CS.General_v3.Util.IO.DeleteFile(path);
            foreach (var size in this.imageSizes)
            {
                size.RemoveFile();
            }
            
            //this.Save();
        }

        
        /// <summary>
        /// An identical copy of 'UploadFile'.  
        /// </summary>
        /// <param name="txt"></param>
        public override void SetMediaItem(System.IO.Stream stream, string filename)
        {
            
            if (string.IsNullOrEmpty(this.Extension))
            {
                throw new InvalidOperationException("CS.General_v3.Classes.MediaItems.Image:: 'Extension' cannot be empty.  Please fill by overriding 'SetMediaItem'");
            }
                
            this.UploadFile(stream, filename);
            
        }

        #endregion

        #region IMediaItem Members


       // public abstract void Save();

        #endregion

        #region IMediaItem Members


        
        #endregion

        #region IMediaItem Members



        #endregion
        
    }
}
