﻿using System;
namespace CS.General_v3.Classes.MediaItems
{
    public interface IVideoSettings
    {
        int? AudioBitRate { get; set; }
        int? AudioSampleRate { get; set; }
        bool? DisableAudio { get; set; }
        CS.General_v3.Enums.CROP_IDENTIFIER? ThumbnailCropIdentifier { get; set; }
        int? ThumbnailHeight { get; set; }
        int? ThumbnailWidth { get; set; }
        int? VideoBitRate { get; set; }
        int? VideoFPS { get; set; }
        int? VideoHeight { get; set; }
        int? VideoWidth { get; set; }
        int? TotalThumbnails { get; set; }
        string VideoExtension { get; set; }
        string ThumbnailExtension { get; set; }
    }
}
