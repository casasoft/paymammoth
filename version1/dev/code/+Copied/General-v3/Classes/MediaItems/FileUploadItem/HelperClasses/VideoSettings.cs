﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.MediaItems.FileUploadItem.HelperClasses
{
    public class VideoSettings : CS.General_v3.Classes.MediaItems.FileUploadItem.HelperClasses.IVideoSettings
    {
        public VideoSettings()
        {
            
        }
        public int? TotalThumbnails { get; set; }
        public bool? DisableAudio {get;set;}
        public int? AudioSampleRate {get;set;}
        public int? AudioBitRate {get;set;}
        public int? VideoFPS {get;set;}
        public int? VideoBitRate {get;set;}
        public int? ThumbnailWidth {get;set;}
        public int? ThumbnailHeight {get;set;}
        public Enums.CROP_IDENTIFIER? ThumbnailCropIdentifier {get;set;}
        public int? VideoHeight {get;set;}
        public int? VideoWidth {get;set;}
        public string VideoExtension { get; set; }
        public string ThumbnailExtension { get; set; }

    }
}
