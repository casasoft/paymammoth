﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace CS.General_v3.Classes.MediaItems
{
    
        public interface IMediaType
        {
            void Remove();
            void UploadFile(string filenameOnly, string extension, Stream s);
            
        }
        
    
}
