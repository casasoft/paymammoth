﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using CS.General_v3.Classes.MediaItems.FileUploadItem.HelperClasses;

namespace CS.General_v3.Classes.MediaItems
{
    
        public class IMAGE : IMediaType
        {
            public string WatermarkImagePath { get; set; }
            public int WatermarkPaddingFromBorder { get; set; }
            /// <summary>
            /// Watermark Transarent Value, from 0 to 1 (percentage - 1 is not transparent, 0 is fully transparent)
            /// </summary>
            public float WatermarkTransparencyValue { get; set; }
            public List<uint> WatermarkBackgroundColors { get; set; }
            public CS.General_v3.Enums.CROP_IDENTIFIER WatermarkPosition { get; set; }
            public int GetOriginalImageWidth()
            {
                int w,h;
                CS.General_v3.Util.Image.getImageSize(GetOriginalLocalPath(), out w, out h);
                return w;
            }
            public int GetOriginalImageHeight()
            {
                int w, h;
                CS.General_v3.Util.Image.getImageSize(GetOriginalLocalPath(), out w, out h);
                return h;
            }
            protected WebFileUploadItem _webFileUploadItem = null;
            public IMAGE(WebFileUploadItem item)
            {
                _webFileUploadItem = item;
                //this.WatermarkBackgroundColors = new List<uint>();
                this.imageSizes = new List<IMAGE_SIZE_INFO>();
                this.OriginalImageMaxWidth = 1600;
                this.OriginalImageMaxHeight = 1200;
                this.WatermarkTransparencyValue = 0.5F;
                this.WatermarkPaddingFromBorder = 0;
                this.WatermarkPosition = Enums.CROP_IDENTIFIER.MiddleMiddle;
               
                
            }
            public List<string> GetAllFilesLocalPaths()
            {
                List<string> list = new List<string>();
                foreach (var size in this.imageSizes)
                {
                    list.Add(GetLocalPathForImage(size.Title));
                }
                return list;
            }
            public void UploadFile(string filenameOnly, string extension, Stream s)
            {
                
                //_webFileUploadItem.Extension = _webFileUploadItem.ImageExtension; //01/03/2010 - commented by Karl, to allow any extension for images
                _webFileUploadItem.ImageExtension = extension;
                _webFileUploadItem.Extension = extension;
                string localPath = _webFileUploadItem.GetLocalFilePath() + "." + extension;
                CS.General_v3.Util.IO.SaveStreamToFile(s, localPath);
                resizeOriginal(localPath);

                CS.General_v3.Util.IO.DeleteFile(localPath);
                CheckImageSizes(true);
                

            }
            private void resizeOriginal(string path)
            {
                string localPath = GetOriginalLocalPath();
                CS.General_v3.Util.Image.ResizeImage(path, this.OriginalImageMaxWidth, this.OriginalImageMaxHeight, localPath);

            }
            public class IMAGE_SIZE_INFO
            {
                public override string ToString()
                {
                    return this.Title + " (" + this.Width + "x" + this.Height + ") - Crop: " + CS.General_v3.Enums.GetEnumName(typeof(Enums.CROP_IDENTIFIER), CropIdentifier);
                    
                }
                private IMAGE _image = null;
                public string Title = "";
                
                public int Width = 0;
                public int Height = 0;
                public CS.General_v3.Enums.CROP_IDENTIFIER CropIdentifier = CS.General_v3.Enums.CROP_IDENTIFIER.MiddleMiddle;
                public IMAGE_SIZE_INFO(IMAGE image, string title, int w, int h, CS.General_v3.Enums.CROP_IDENTIFIER cropIdentifier)
                {
                    this._image = image;
                    this.Title = title;
                    this.Width = w;
                    this.Height = h;
                    this.CropIdentifier = cropIdentifier;
                }

                private void checkFileVersion_v1()
                {
                    string localPath_old = _image.getLocalPath_v1(this.Title, false);
                    string localPath_new = _image.getLocalPath_v2(this.Title, false);
                    if (System.IO.File.Exists(localPath_old))
                    {
                        if (!System.IO.File.Exists(localPath_new))
                        {
                            CS.General_v3.Util.IO.MoveFile(localPath_old, localPath_new);
                        }
                        CS.General_v3.Util.IO.DeleteFile(localPath_old);
                    }
                }
                private void checkFileVersion_v2()
                {
                    string localPath_old = _image.getLocalPath_v2(this.Title, false);
                    string localPath_new = _image.getLocalPath_v3(this.Title, false);
                    if (System.IO.File.Exists(localPath_old))
                    {
                        if (!System.IO.File.Exists(localPath_new))
                        {
                            CS.General_v3.Util.IO.MoveFile(localPath_old, localPath_new);
                        }
                        CS.General_v3.Util.IO.DeleteFile(localPath_old);
                    }
                }
                public void CheckFile(bool overwriteExisting)
                {
                    checkFileVersion_v1();
                    checkFileVersion_v2();
                    string localPath = _image.GetOriginalLocalPath();
                    string newLocalPath = _image.getLocalPath_v3(this.Title, false);
                    //string newLocalPathOld = _image.getLocalPathOld(this.Title, false);
                    if (System.IO.File.Exists(localPath))
                    {
                        /*if (System.IO.File.Exists(newLocalPathOld))
                        {
                            if (!System.IO.File.Exists(newLocalPathNew))
                            {
                                CS.General_v3.Util.IO.MoveFile(newLocalPathOld, newLocalPathNew);
                            }
                            CS.General_v3.Util.IO.DeleteFile(newLocalPathOld);
                        }
                        */


                        if (overwriteExisting || !System.IO.File.Exists(newLocalPath))
                        {
                            if (this.CropIdentifier != CS.General_v3.Enums.CROP_IDENTIFIER.None)
                            {
                                CS.General_v3.Util.Image.ResizeAndCropImage(localPath, this.Width, this.Height, newLocalPath, this.CropIdentifier);
                            }
                            else
                            {
                                CS.General_v3.Util.Image.ResizeImage(localPath, this.Width, this.Height, newLocalPath);
                            }
                            if (!string.IsNullOrEmpty(_image.WatermarkImagePath))
                            {
                                string waterMarkLocalPath = _image.WatermarkImagePath;
                                if (waterMarkLocalPath.Contains("/"))
                                    waterMarkLocalPath = CS.General_v3.Util.PageUtil.MapPath(_image.WatermarkImagePath);
                                CS.General_v3.Util.Image.AddWatermark(newLocalPath, waterMarkLocalPath,
                                    _image.WatermarkBackgroundColors, 
                                    (_image.WatermarkBackgroundColors == null || _image.WatermarkBackgroundColors.Count == 0), _image.WatermarkTransparencyValue,
                                    _image.WatermarkPosition,_image.WatermarkPaddingFromBorder);
                            }
                        }
                    }
                }
                public void RemoveFile()
                {
                    
                    _image.removeImage(this.Title);
                }
            }
            private List<IMAGE_SIZE_INFO> imageSizes = null;
            public IMAGE_SIZE_INFO GetImageSize_Smallest()
            {
                IMAGE_SIZE_INFO smallestSize = null;
                int leastArea = Int32.MaxValue;
                foreach (var size in this.imageSizes)
                {
                    int area = size.Width * size.Height;
                    if (area < leastArea)
                    {
                        leastArea = area;
                        smallestSize  = size;
                    }
                }
                return smallestSize;
            }
            public IMAGE_SIZE_INFO GetImageSize_Largest()
            {
                IMAGE_SIZE_INFO largestSize = null;
                int mostArea = -1;
                foreach (var size in this.imageSizes)
                {
                    int area = size.Width * size.Height;
                    if (area > mostArea)
                    {
                        mostArea = area;
                        largestSize = size;
                    }
                }
                return largestSize;
            }
            /// <summary>
            /// Adds an image size from the settings file.  Settings are 'Folder','W','H','FillsBox','CropIdentifier'.
            /// </summary>
            /// <param name="title">Title of image size</param>
            /// <param name="baseSettingName">Base setting node.  Must end with a '/' (Added if not)</param>
            public void AddImageSizeFromSettings(string title, string baseSettingName)
            {
                if (!baseSettingName.EndsWith("/"))
                    baseSettingName += "/";
                if (baseSettingName.StartsWith("/"))
                    baseSettingName = baseSettingName.Remove(0, 1);
                int width, height;
                

                CS.General_v3.Enums.CROP_IDENTIFIER cropIdentifier = CS.General_v3.Enums.CROP_IDENTIFIER.MiddleMiddle;
                CS.General_v3.Settings.ImageInfo.GetImageInfoFromSettings(baseSettingName + title, out width, out height,  out cropIdentifier);
                
               
                if (width <= 0 || height <= 0)
                {
                    throw new InvalidOperationException("The settings for the item '" + baseSettingName + title + "' are not filled");
                }

                this.AddImageSize(title,  width, height, cropIdentifier);
            }
            /// <summary>
            /// Adds an image size to the list of images
            /// </summary>
            /// <param name="title">Title</param>
            /// <param name="width">Width</param>
            /// <param name="height">Height</param>
            /// <param name="FillBox">Whether to fill the box or not (by cropping)</param>
            /// <param name="cropIdentifier">Crop Identifier</param>
            public void AddImageSize(string title,  int width, int height, CS.General_v3.Enums.CROP_IDENTIFIER cropIdentifier)
            {
                this.imageSizes.Add(new IMAGE_SIZE_INFO(this, title,  width, height, cropIdentifier));
            }
            public void AddImageSize(string title, IImageSize size)
            {
                AddImageSize(title, size.Width, size.Height, size.CropIdentifier);
            }
            
            public void RemoveImageSize(string title)
            {
                IMAGE_SIZE_INFO size = getImageSize(title);
                this.imageSizes.Remove(size);
            }
            /// <summary>
            /// Original Image maximum width.  Default is 1600
            /// </summary>
            public int OriginalImageMaxWidth { get; set; }
            /// <summary>
            /// Original Image maximum height. Default is 1200
            /// </summary>
            public int OriginalImageMaxHeight { get; set; }


            /// <summary>
            /// Returns an IMAGE_SIZE_OF with the specified title
            /// </summary>
            /// <param name="title">Title</param>
            /// <returns>The IMAGE_SIZE_OF item</returns>
            protected IMAGE_SIZE_INFO getImageSize(string title)
            {
                title = title.ToLower();
                for (int i = 0; i < imageSizes.Count; i++)
                {
                    if (string.Compare(imageSizes[i].Title,title,true) == 0)
                        return imageSizes[i];
                }
                return null;
            }
            public void RemoveAllImageSizes()
            {
                this.imageSizes.Clear();
            }
            public void Remove()
            {
                string path = _webFileUploadItem.GetLocalFilePath();
                CS.General_v3.Util.IO.DeleteFile(path);
                
                for (int i = 0; i < this.imageSizes.Count; i++)
                {
                    path = getLocalPath_v1(this.imageSizes[i].Title);
                    CS.General_v3.Util.IO.DeleteFile(path);
                    path = getLocalPath_v2(this.imageSizes[i].Title);
                    CS.General_v3.Util.IO.DeleteFile(path);
                    path = getLocalPath_v3(this.imageSizes[i].Title);
                    CS.General_v3.Util.IO.DeleteFile(path);

                }
            }

            public void LoadOriginalImageValues(IImageSize size)
            {
                LoadOriginalImageValues(size.Width, size.Height, size.CropIdentifier);
            }
            public void LoadOriginalImageValues(int width, int height, Enums.CROP_IDENTIFIER cropIdentifier)
            {
                this.OriginalImageMaxWidth = width;
                this.OriginalImageMaxHeight = height;
            }

            /// <summary>
            /// Loads values from the settings file.  The 'node' appended is named 'Original'
            /// </summary>
            /// <param name="baseSetting">Base setting node. </param>
            public void LoadOriginalImageValuesFromSettings(string baseSetting)
            {

                if (!baseSetting.EndsWith("/"))
                    baseSetting += "/";
                if (baseSetting.StartsWith("/"))
                    baseSetting = baseSetting.Remove(0, 1);
                int w, h;
               
                CS.General_v3.Settings.ImageInfo.GetImageInfoFromSettings(baseSetting + "Original", out w, out h);
                LoadOriginalImageValues(w, h, Enums.CROP_IDENTIFIER.None);

                //this.OriginalImageFolder = folder;


            }
            /// <summary>
            /// gets the smallest image path
            /// </summary>
            /// <returns></returns>
            public virtual string GetWebPathFromRootForSmallest()
            {
                var size = GetImageSize_Smallest();
                string title = (size != null ? size.Title : "");
                return GetWebPathFromRoot(title);

            }
            /// <summary>
            /// gets the smallest image path
            /// </summary>
            /// <returns></returns>
            public virtual string GetWebPathFromRootForLargest()
            {
                string s = "";
                int maxArea = -1;
                foreach (var size in this.imageSizes)
                {
                    int area = size.Width * size.Height;
                    if (area > maxArea)
                    {
                        maxArea = area;
                        s = size.Title;
                    }
                }
                return GetWebPathFromRoot(s);
                
            }
            /// <summary>
            /// gets the smallest image path
            /// </summary>
            /// <returns></returns>
            public virtual string GetWebPathFromRootForMiddleSize()
            {
                int index = 0;
                index = (this.imageSizes.Count / 2) + (this.imageSizes.Count % 2);
                if (index >= this.imageSizes.Count)
                    index = this.imageSizes.Count - 1;
                
                return GetWebPathFromRoot(imageSizes[index].Title);
                
            }
            /// <summary>
            /// Checks the image sizes, and creates any ones that do not exist (or are to be overwritten)
            /// </summary>
            /// <param name="overwriteExisting">Overwrite existing files</param>
            public void CheckImageSizes(bool overwriteExisting)
            {
                string localPath = GetOriginalLocalPath();
                if (System.IO.File.Exists(localPath))
                {
                    foreach (var size in this.imageSizes)
                    {
                        size.CheckFile(overwriteExisting);
                    }
                }
            }

            /// <summary>
            /// Get web path (from root, excluding start '/'), for image of type 'imageSize'
            /// </summary>
            /// <param name="imageSize"></param>
            /// <returns></returns>
            public virtual string GetLocalPathForImage(string imageSize)
            {
                string s = GetWebPathFromRoot(imageSize);
                return CS.General_v3.Util.PageUtil.MapPath(s);
            }


            /// <summary>
            /// Get web path (from root, excluding start '/'), for image of type 'imageSize'
            /// </summary>
            /// <param name="imageSize"></param>
            /// <returns></returns>
            public virtual string GetWebPathFromRoot(string imageSize)
            {
                if (!string.IsNullOrEmpty(imageSize))
                {

                    CheckImageSizes(false);

                    IMAGE_SIZE_INFO sizeInfo = getImageSize(imageSize);
                    string sizeTitle = "";
                    if (sizeInfo != null) sizeTitle = sizeInfo.Title;
                    string s = _webFileUploadItem.getWebPathFromRoot(imageSize, null,null, _webFileUploadItem.ImageExtension).Replace("\\","/");
                    return s;
                }
                else
                {
                    return GetOriginalWebPathFromRoot();
                }
            }
            /// <summary>
            /// Get local path for image of type 'imageSize'
            /// </summary>
            /// <param name="imageSize"></param>
            /// <returns></returns>
            protected virtual string getLocalPath_v3(string imageSize)
            {
                return getLocalPath_v3(imageSize, true);
            }
            /// <summary>
            /// Get local path for image of type 'imageSize'
            /// </summary>
            /// <param name="imageSize"></param>
            /// <returns></returns>
            protected virtual string getLocalPath_v2(string imageSize)
            {
                return getLocalPath_v2(imageSize, true);
            }
            /// <summary>
            /// Get local path for image of type 'imageSize'
            /// </summary>
            /// <param name="imageSize"></param>
            /// <returns></returns>
            protected virtual string getLocalPath_v1(string imageSize)
            {
                return getLocalPath_v1(imageSize, true);
            }
            protected virtual void removeImage(string imageSize)
            {
                string path = "";
                path = getLocalPath_v1(imageSize);
                CS.General_v3.Util.IO.DeleteFile(path);
                path = getLocalPath_v2(imageSize);
                CS.General_v3.Util.IO.DeleteFile(path);
                path = getLocalPath_v3(imageSize);
                CS.General_v3.Util.IO.DeleteFile(path);
            }
            /// <summary>
            /// This is the new function which returns the image size at the end of th efilename
            /// </summary>
            /// <param name="imageSize"></param>
            /// <param name="checkImageSizes"></param>
            /// <returns></returns>
            protected string getLocalPath_v3(string imageSize, bool checkImageSizes)
            {
                if (checkImageSizes)
                    CheckImageSizes(false);
                IMAGE_SIZE_INFO sizeInfo = getImageSize(imageSize);
                string s = _webFileUploadItem.getLocalFilePath(sizeInfo.Title, null, null, _webFileUploadItem.ImageExtension);
                return s;
            }
            /// <summary>
            /// This is the new function which returns the image size at the end of th efilename
            /// </summary>
            /// <param name="imageSize"></param>
            /// <param name="checkImageSizes"></param>
            /// <returns></returns>
            protected string getLocalPath_v2(string imageSize, bool checkImageSizes)
            {
                if (checkImageSizes)
                    CheckImageSizes(false);
                IMAGE_SIZE_INFO sizeInfo = getImageSize(imageSize);
                string s = _webFileUploadItem.getLocalFilePath(null, null, "-" + sizeInfo.Title, _webFileUploadItem.ImageExtension);
                return s;
            }
            /// <summary>
            /// This is the old function, which returns the image size at the start of the filename
            /// </summary>
            /// <param name="imageSize"></param>
            /// <param name="checkImageSizes"></param>
            /// <returns></returns>
            protected string getLocalPath_v1(string imageSize, bool checkImageSizes)
            {
                if (checkImageSizes)
                    CheckImageSizes(false);
                IMAGE_SIZE_INFO sizeInfo = getImageSize(imageSize);
                string s = _webFileUploadItem.getLocalFilePath(null, sizeInfo.Title + "_", null, _webFileUploadItem.ImageExtension);
                return s;
            }
            /// <summary>
            /// Returns the original image local path
            /// </summary>
            /// <returns></returns>
            public virtual string GetOriginalLocalPath()
            {
                string s = _webFileUploadItem.getLocalFilePath(null, null, null, _webFileUploadItem.ImageExtension);
                return s;
            }
            /// <summary>
            /// Returns the original image web path (from root, excluding start '/')
            /// </summary>
            /// <returns></returns>
            public virtual string GetOriginalWebPathFromRoot()
            {
                string s = _webFileUploadItem.getWebPathFromRoot(null, null, null, _webFileUploadItem.ImageExtension);
                return s;
            }
        
        }
    
}
