﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace CS.General_v3.Classes.MediaItems
{
    public  abstract partial class WebFileUploadItem 
    {
        public class IMEDIA_ITEM : CS.General_v3.Classes.MediaItems.IMediaItem
        {
            public WebFileUploadItem _item;
            public IMEDIA_ITEM(WebFileUploadItem item)
            {
                _item = item;
            }




            #region IMediaItem Members

            public int Priority
            {
                get
                {

                    return _item.Priority;
                }
                set
                {
                    _item.Priority = value;
                }
            }

            public Enums.ITEM_TYPE ItemType
            {
                get
                {
                    return _item.ItemType;
                    
                }
                
            }

            public string GetThumbnailImageUrl()
            {
                return _item.ItemThumbnailImage; 
            }

            public string GetLargeImageUrl()
            {
                return _item.ItemNormalImage; 
            }

            public string GetOriginalItemUrl()
            {
                return _item.GetWebPathFromRoot(); 
            }

            public string Caption
            {
                get
                {
                    return _item.Caption;
                }
                set
                {
                    _item.Caption = value;
                }
            }

            public string Identifier
            {
                get
                {
                    return _item.Identifier;
                }
                set
                {
                    _item.Identifier = value;
                }
            }

            public string Extension
            {
                get { return _item.Extension; }
            }

            public int GetFileSizeInKB()
            {
                 return CS.General_v3.Util.IO.GetFileSizeInKB(_item.GetLocalFilePath()); 
            }

            public void Delete()
            {
                _item.Remove();
            }

            public void SetMediaItem(CS.General_v3.Controls.WebControls.Common.MyFileUpload txt)
            {
                
                SetMediaItem(txt.FileContent, txt.FileName);
                
            }

            public void SetMediaItem(Stream txt, string filename)
            {
                string identifier = this.Caption + "_" + CS.General_v3.Util.Random.GetAlpha(10, 10);
                identifier = CS.General_v3.Util.IO.ConvertStringToFilename(identifier);
                _item.UploadFile(identifier, filename, txt);
                
            }

            public void Save()
            {
                _item.Save();
            }

            public void SetAsMainItem()
            {
                this.Priority = 0;
                Save();
            }

            public bool IsEmpty()
            {
                return string.IsNullOrEmpty(this.Identifier);
            }

            #endregion

            #region IMediaItem Members



            #endregion

            #region IMediaItem Members

            public virtual string ExtraValueChoice { get { return _item.ExtraValueChoice; } set { _item.ExtraValueChoice = value; } }
            

            #endregion
        }
        private Enums.ITEM_TYPE _ItemType = Enums.ITEM_TYPE.Other;

        public Enums.ITEM_TYPE ItemType
        {
            get
            { 
                if (string.IsNullOrEmpty(this.Identifier)) 
                    updateIdentifierAndExtensionFromFilename();
                return _ItemType; 
            }
            set { _ItemType = value; }
        }
        public IMEDIA_ITEM GetAsIMediaItem()
        {
            return new IMEDIA_ITEM(this);
        }
        public virtual string ExtraValueChoice { get; set; }
        private void checkExtension(string filename)
        {
            string ext = CS.General_v3.Util.IO.GetExtension(filename);
            if (CS.General_v3.Util.Image.CheckImageFileExtension(filename))
                this.ItemType = Enums.ITEM_TYPE.Image;
            else if (CS.General_v3.Util.VideoUtil.CheckVideoExtension(filename))
                this.ItemType = Enums.ITEM_TYPE.Video;
            else
            {
                switch (ext)
                {
                    case "pdf": this.ItemType = Enums.ITEM_TYPE.PDF; break;
                    case "swf": this.ItemType = Enums.ITEM_TYPE.SWF; break;
                    default:
                        this.ItemType = Enums.ITEM_TYPE.Other; break;
                }

            }

            
        }
        public WebFileUploadItem()
        {
            this.ImageExtension = "jpg";
            this.VideoExtension = "flv";
            //this.VideoThumbnailExtension = "jpg";
            this.Image = new IMAGE(this);
            this.SWF = new SWFFile(this);
            this.PDF = new PDFFile(this);
            this.GeneralFile = new GeneralFile(this);
            this.Video = new VIDEO(this);
            
        }
        public abstract string Caption { get; set; }
        private string _Identifier = "";
        public virtual bool HasContent()
        {
            return !string.IsNullOrEmpty(this.Identifier);
        }
        public virtual string Identifier 
        {
            get {
                if (string.IsNullOrEmpty(_Identifier) && !string.IsNullOrEmpty(Filename))
                {
                    updateIdentifierAndExtensionFromFilename();
                }
                return _Identifier;
            }
            protected set { _Identifier = value;}
        }
         
        public virtual string ItemThumbnailImage
        {
            get
            {
                switch (ItemType)
                {
                    case Enums.ITEM_TYPE.Image: return Image.GetWebPathFromRootForSmallest();
                    case Enums.ITEM_TYPE.Video: return Video.ThumbnailImageURL;
                    case Enums.ITEM_TYPE.PDF: return PDF.GetThumbnailImage();
                    case Enums.ITEM_TYPE.SWF: return SWF.GetThumbnailImage();
                         
                           

                }
                return "";

            }
        }
        public virtual string ItemNormalImage
        {
            get
            {
                switch (ItemType)
                {
                    case Enums.ITEM_TYPE.Image: return Image.GetWebPathFromRootForMiddleSize();
                    case Enums.ITEM_TYPE.Video: return Video.LargeImageURL;
                    case Enums.ITEM_TYPE.PDF: return PDF.GetNormalImage();
                    case Enums.ITEM_TYPE.SWF: return SWF.GetNormalImage();
                }
                return "";
            }
        }
        public abstract int Priority { get; set; }
        private string _ImageExtension = null;
        public virtual string ImageExtension
        {
            get { return _ImageExtension; }
            set { _ImageExtension = value; }
        }
        public virtual string VideoExtension { get; set; }
        private string _VideoThumbnailExtension = null;

        public virtual string VideoThumbnailExtension
        {
            get 
            {
                if (_VideoThumbnailExtension == null)
                    _VideoThumbnailExtension = "jpg";
                return _VideoThumbnailExtension; 
            }
            set 
            {
                _VideoThumbnailExtension = value;
            

            }
        }
        
        public abstract int VideoDuration { get; set; }
        public abstract void Save();
        public abstract string Filename { get; set; }
        protected void updateIdentifierAndExtensionFromFilename()
        {
            this.Identifier = CS.General_v3.Util.IO.ConvertStringToFilename(CS.General_v3.Util.IO.GetFilenameOnly(Filename));
            this.Extension = CS.General_v3.Util.IO.GetExtension(Filename);

            if (!string.IsNullOrEmpty(this.Identifier) && this.ItemType == Enums.ITEM_TYPE.Image)
            {
                this.ImageExtension = this.Extension;
            }
            checkExtension(this.Filename);
        }
        public virtual void SetFilename(string filename)
        {
            this.Filename = filename;
            
            updateIdentifierAndExtensionFromFilename();
        }
       
        private string _Extension = null;
        public string Extension
        {
            get
            {
                if (string.IsNullOrEmpty(_Extension) && !string.IsNullOrEmpty(Filename))
                {
                    updateIdentifierAndExtensionFromFilename();
                }
                return _Extension;

            }
             internal set
            {
                _Extension = value;
                checkExtension(_Extension);
            }
        }
        private string _Folder = null;
        /// <summary>
        /// Folder of file upload, from root
        /// </summary>
        public virtual string Folder 
        {
            get { return _Folder;}
            set { 
                string s = value;
                s = s ?? "";
                if (!s.StartsWith("/")) s= "/" + s;
                if (!s.EndsWith("/")) s += "/";
                _Folder= s;
            }
        
        }
        

        private string getFilename(string subFolder, string prefix, string suffix, string extension)
        {
            if (subFolder != null)
            {
                subFolder = subFolder.Replace("/", "\\");
                if (subFolder.EndsWith("\\")) subFolder = subFolder.Substring(0, subFolder.Length -1);
            }
           // if (!string.IsNullOrEmpty(subFolder) && !subFolder.EndsWith("\\")) subFolder += "\\";

            //2010-04-26 :  the call to 'convert string to filename' was removed as it was taking very long !
            //string path = CS.General_v3.Util.IO.ConvertStringToFilename(prefix + Identifier + suffix + "." + extension);
            string path = (prefix + Identifier + suffix + "." + extension);
            if (!string.IsNullOrEmpty(subFolder))
            {
                //path = CS.General_v3.Util.IO.ConvertStringToFilename(subFolder) + "\\" + path;
                //2010-04-26 :  the call to 'convert string to filename' was removed as it was taking very long !
                path = (subFolder) + "\\" + path;
            }
            return path;
        }
        internal virtual string getLocalFilePath(string subFolder, string prefix, string suffix, string extension)
        {
            string currFolder = this.Folder;
            
            string path = CS.General_v3.Util.PageUtil.MapPath(Folder );
            path += getFilename(subFolder, prefix,suffix,extension);
            
            return path;
        }

        public virtual List<string> GetAllLocalFilePaths()
        {
            List<string> list = new List<string>();
            list.Add(GetLocalFilePath());
            if (this.ItemType == Enums.ITEM_TYPE.Image)
            {
                list.AddRange(this.Image.GetAllFilesLocalPaths());

            }
            else if (this.ItemType == Enums.ITEM_TYPE.Video)
            {
                for (int i=0; i < this.Video.TotalThumbnails;i++)
                {
                    list.Add(this.Video.GetLocalThumbnailPath(i));
                }
            }
            return list;
        }

        public virtual string GetLocalFilePath()
        {
            return getLocalFilePath(null,"", "",Extension);
        }
        internal virtual string getWebPathFromRoot(string subFolder, string prefix, string suffix, string extension)
        {
            return Folder + getFilename(subFolder, prefix,suffix,extension);
        }
        public virtual string GetWebPathFromRoot()
        {
            return getWebPathFromRoot(null, "", "", Extension);
        }
        public virtual void Remove()
        {
            if (!string.IsNullOrEmpty(this.Identifier))
            {
                
                var mediaType = getMediaItem();
                mediaType.Remove();
                this.Identifier = "";
            }
            
        }

        
        private IMediaType getMediaItem()
        {
            switch (this.ItemType)
            {
                case Enums.ITEM_TYPE.Image: return this.Image;
                case Enums.ITEM_TYPE.Video: return this.Video;
                case Enums.ITEM_TYPE.PDF: return this.PDF;
                case Enums.ITEM_TYPE.SWF: return this.SWF;
                default: return this.GeneralFile;
                
            }
            
         
        }
        /// <summary>
        /// Downloads the file to the user, with the specified filename.  The Content Type is automatically set based on the extension.
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="suffix"></param>
        /// <param name="contentType">If left null, it is automatically set </param>
        /// <param name="fileName">The filename.  If left null, it is set as the prefix + identifier + suffix + '.' + extension </param>
        public virtual void DownloadFile(string contentType, string fileName)
        {
            string path = GetLocalFilePath();

            if (string.IsNullOrEmpty(contentType))
            {
                contentType = CS.General_v3.Util.Data.getMIMEContentType(Extension);
            }
            CS.General_v3.Util.Web.ForceDownload(path, contentType, fileName);
        }
        private void checkRequiredVars()
        {
            StringBuilder sb = new StringBuilder();
            if (string.IsNullOrEmpty(this.Folder))
            {
                sb.AppendLine("'Folder' is required");
            }
            if (string.IsNullOrEmpty(this.Identifier))
            {
                sb.AppendLine("'Identifier' is required");

            }

            if (sb.Length > 0)
            {
                string msg = "CS.General_v3.Classes.MediaItem.WebFileUploadItem: " + this.GetType().ToString() + "\r\n\r\n" + sb.ToString();
                throw new InvalidOperationException(msg);

            }
        }
        public virtual void UploadFile(string newIdentifier, CS.General_v3.Controls.WebControls.Common.MyFileUpload txt)
        {
            UploadFile(newIdentifier, txt.FileName, txt.FileContent);
        }
        
        /// <summary>
        /// Uploads a file
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="suffix"></param>
        /// <param name="s"></param>
        public virtual void UploadFile(string newIdentifier, string uploadedfilename, System.IO.Stream s)
        {
            
            Remove();
            
            
            string filenameOnly = CS.General_v3.Util.IO.GetFilenameOnly(uploadedfilename);
            string extension = CS.General_v3.Util.IO.GetExtension(uploadedfilename);
            checkExtension(uploadedfilename);
            this.Identifier = CS.General_v3.Util.IO.ConvertStringToFilename(newIdentifier);
            checkRequiredVars();
            createBaseDirectory();
            var mediaType = getMediaItem();
            mediaType.UploadFile(filenameOnly, extension, s);
            SetFilename( this.Identifier + "." + this.Extension);
            
            if (FileUpload != null)
                FileUpload(filenameOnly,extension, s);
            Save();
            //else
              //  throw new InvalidOperationException("Please attach to 'FileUpload' event for uploading files");

           
            
        }
        public delegate void FileUploadHandler(string filenameOnly, string extension, System.IO.Stream s);

        public event FileUploadHandler FileUpload;
        //public event EventHandler UploadFileReady;

        
        private void createBaseDirectory()
        {
            string path = CS.General_v3.Util.PageUtil.MapPath(Folder);
            path = path.Replace("/", "\\");
            path = path.Replace(@"\\", @"\");
            CS.General_v3.Util.IO.CreateDirectory(path);

        }
        internal void parseSizeFromString(string s, out int w, out int h)
        {
            s = s.Replace(" ", "");
            s = s.ToLower();
            s = s.Replace("*", "x");
            string[] tokens = s.Split(new string[] { "x" }, StringSplitOptions.RemoveEmptyEntries);
            w = Convert.ToInt32(tokens[0]);
            h = Convert.ToInt32(tokens[1]);

        }

        public VIDEO Video { get; set; }
        public IMAGE Image { get; set; }
        public PDFFile PDF { get; set; }
        public GeneralFile GeneralFile { get; set; }       
        public SWFFile SWF { get; set; }

    }
}

