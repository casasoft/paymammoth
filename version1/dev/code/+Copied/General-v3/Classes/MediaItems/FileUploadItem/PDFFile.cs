﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace CS.General_v3.Classes.MediaItems
{
    
        public class PDFFile : IMediaType
        {
            public string ThumbnailImage { get; set; }
            public string NormalImage { get; set; }
            public string GetThumbnailImage()
            {
                return ThumbnailImage;
            }
            public string GetNormalImage()
            {
                return NormalImage;
            }
            
            protected WebFileUploadItem _webFileUploadItem = null;
            
            public PDFFile(WebFileUploadItem item)
            {
                this._webFileUploadItem = item;
                this.ThumbnailImage = "/_common/images/icons/v1/pdf_small.jpg";
                this.NormalImage = "/_common/images/icons/v1/pdf_large.jpg";
               
            }
            
            public void UploadFile(string filenameOnly, string extension, Stream s)
            {
                if (string.IsNullOrEmpty(ThumbnailImage) || string.IsNullOrEmpty(NormalImage))
                {
                    throw new InvalidOperationException("Please fill in both 'ThumbnailImage' and 'NormalImage' when using PDF files");
                } 
                _webFileUploadItem.Extension = extension;
                string localPath = _webFileUploadItem.GetLocalFilePath();
                CS.General_v3.Util.IO.SaveStreamToFile(s, localPath);
                

                
            }
            public void Remove()
            {
                string path = _webFileUploadItem.GetLocalFilePath();
                CS.General_v3.Util.IO.DeleteFile(path);
            }
            
            
        }
        
    
}
