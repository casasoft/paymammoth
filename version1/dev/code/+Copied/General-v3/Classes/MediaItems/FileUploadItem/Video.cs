﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using CS.General_v3.Classes.MediaItems.FileUploadItem.HelperClasses;

namespace CS.General_v3.Classes.MediaItems
{
    
        public class VIDEO : IMediaType
        {
            protected WebFileUploadItem _webFileUploadItem = null;
            
            public int TotalThumbnails { get; set; }
            public int? ThumbnailWidth { get; set; }
            public int? ThumbnailHeight { get; set; }
            public int? VideoWidth { get; set; }
            public int? VideoHeight { get; set; }
            public int VideoBitRate { get; set; }
            public int? VideoFPS { get; set; }
            public bool DisableAudio { get; set; }
            public int? AudioSampleRate { get; set; }
            public int? AudioBitRate { get; set; }
            public string VideoThumbnailExtension
            {
                get { return _webFileUploadItem.VideoThumbnailExtension; }
                set { _webFileUploadItem.VideoThumbnailExtension = value; }
            }
            public int Duration
            {
                get { return _webFileUploadItem.VideoDuration; }
                set { _webFileUploadItem.VideoDuration = value; }
            }
            public CS.General_v3.Enums.CROP_IDENTIFIER ThumbnailCropIdentifier { get; set; }
            public VIDEO(WebFileUploadItem item)
            {
                this._webFileUploadItem = item;
                this.TotalThumbnails = 1;
                this.VideoBitRate = 440;
                this.AudioSampleRate = 44100;
                this.AudioBitRate = 64;
                this.DisableAudio = false;
                
            }
            private string originalFilePath = "";
            public void UploadFile(string filenameOnly, string extension, Stream s)
            {
                _webFileUploadItem.Extension = _webFileUploadItem.VideoExtension;
                string localPath = _webFileUploadItem.GetLocalFilePath() + "." + extension;
                originalFilePath = localPath;
                CS.General_v3.Util.IO.SaveStreamToFile(s, localPath);
                string path = _webFileUploadItem.GetLocalFilePath();
                    
                if ((string.Compare(_webFileUploadItem.VideoExtension, "flv",true) == 0) &&
                    (string.Compare(extension,"flv",true) != 0))
                {
                
                    CS.General_v3.FFMpeg.FFMpeg ffmpeg = new CS.General_v3.FFMpeg.FFMpeg();
                    this.Duration = CS.General_v3.FFMpeg.FFMpeg.GetVideoDuration(localPath);
                    ffmpeg.GeneralInfo.InputFile = localPath;
                    ffmpeg.GeneralInfo.OutputFile = path;
                    ffmpeg.VideoOptions.BitRate = VideoBitRate;
                    ffmpeg.GeneralInfo.Width = VideoWidth;
                    ffmpeg.GeneralInfo.Height = VideoHeight;
                    ffmpeg.Tag = this;
                    ffmpeg.Finished += new EventHandler(ffmpeg_Finished);
                    ffmpeg.RunFFMpeg(true);
                    //ffmpeg_Finished(ffmpeg, null);
                }
                else
                {
                    CS.General_v3.Util.IO.MoveFile(localPath, path);
                    generateThumbnails();
                }
                

                
            }

            private void checkThumbnails(bool checkVariable)
            {
                bool ok = true;
                if (!checkVariable || (checkVariable && !_checkedThumbnails))
                {
                    if (checkVariable)
                        _checkedThumbnails = true;
                    for (int i = 1; i <= this.TotalThumbnails; i++)
                    {
                        string path = GetLocalThumbnailPath(i);
                        if (!System.IO.File.Exists(path))
                        {
                            ok = false;
                            break;
                        }
                    }
                    if (!ok)
                    {
                        generateThumbnails();
                    }
                }
            }

            private void generateThumbnails()
            {
                string path = _webFileUploadItem.GetLocalFilePath();
                List<string> imgPaths = new List<string>();
                for (int i = 1; i <= this.TotalThumbnails; i++)
                {
                    string imgPath = this.GetLocalThumbnailPath(i);
                    imgPaths.Add(imgPath);
                }
                if (this.Duration == 0)
                {
                    this.Duration = CS.General_v3.FFMpeg.FFMpeg.GetVideoDuration(_webFileUploadItem.GetLocalFilePath());
                }

                CS.General_v3.FFMpeg.FFMpeg.GenerateThumbnails(path, imgPaths, 0, this.Duration, this.ThumbnailWidth, this.ThumbnailHeight, this.ThumbnailCropIdentifier);
            }
            void ffmpeg_Finished(object sender, EventArgs e)
            {

                CS.General_v3.FFMpeg.FFMpeg ffmpeg = (CS.General_v3.FFMpeg.FFMpeg)sender;
                VIDEO video = (VIDEO)ffmpeg.Tag;
                string path = originalFilePath;
                
                if (System.IO.File.Exists(path))
                    System.IO.File.Delete(path);
                generateThumbnails();


            }
            
            public void LoadSettings(IVideoSettings settings)
            {

                if (settings.DisableAudio != null) this.DisableAudio = settings.DisableAudio.Value;
                this.AudioSampleRate = settings.AudioSampleRate;
                this.AudioBitRate = settings.AudioBitRate;
                this.VideoFPS = settings.VideoFPS;
                this.ThumbnailWidth = settings.ThumbnailWidth;
                this.ThumbnailHeight = settings.ThumbnailHeight;

                if (settings.ThumbnailCropIdentifier != null) this.ThumbnailCropIdentifier = settings.ThumbnailCropIdentifier.Value;
                if (settings.VideoExtension != null) _webFileUploadItem.VideoExtension = settings.VideoExtension;
                if (settings.VideoBitRate != null) this.VideoBitRate = settings.VideoBitRate.Value;
                this.VideoWidth = settings.VideoWidth;
                this.VideoHeight = settings.VideoHeight;


                _webFileUploadItem.VideoThumbnailExtension = settings.ThumbnailExtension;
                if (settings.TotalThumbnails != null) this.TotalThumbnails = settings.TotalThumbnails.Value;

                
                if (this.VideoFPS == 0) this.VideoFPS = null;
                if (this.VideoBitRate == 0)
                    throw new InvalidOperationException("Video bitrate is required!");
                if (this.ThumbnailWidth == 0) this.ThumbnailWidth = null;
                if (this.ThumbnailHeight == 0) this.ThumbnailHeight = null;
                if (this.VideoWidth == 0) this.VideoWidth = null;
                if (this.VideoHeight == 0) this.VideoHeight = null;
                if (this.TotalThumbnails == 0) TotalThumbnails = 1;

            }
            public void LoadSettingsFromFile(string BaseSetting)
            {
                VideoSettings settings = new VideoSettings();
                if (!BaseSetting.EndsWith("/"))
                    BaseSetting += "/";
                if (BaseSetting.StartsWith("/"))
                    BaseSetting = BaseSetting.Remove(0, 1);

                settings.DisableAudio = CS.General_v3.Settings.GetSettingBool(BaseSetting + "DisableAudio", false);
                settings.AudioSampleRate = CS.General_v3.Settings.GetSettingInt(BaseSetting + "AudioSampleRate", 44100);
                settings.AudioBitRate = CS.General_v3.Settings.GetSettingInt(BaseSetting + "AudioBitRate", 64);
                settings.VideoFPS = CS.General_v3.Settings.GetSettingInt(BaseSetting + "VideoFPS", 0);
                settings.VideoExtension = CS.General_v3.Settings.GetSetting(BaseSetting + "VideoExt", "flv");
                settings.VideoBitRate = CS.General_v3.Settings.GetSettingInt(BaseSetting + "VideoBitRate", 440);
                string thumbSize = CS.General_v3.Settings.GetSetting(BaseSetting + "ThumbSize", "");
                if (string.IsNullOrEmpty(thumbSize))
                {
                    settings.ThumbnailWidth = CS.General_v3.Settings.GetSettingInt(BaseSetting + "ThumbWidth", 80);
                    settings.ThumbnailHeight = CS.General_v3.Settings.GetSettingInt(BaseSetting + "ThumbHeight", 80);
                    settings.ThumbnailCropIdentifier = CS.General_v3.Enums.CropIdentifierFromText(CS.General_v3.Settings.GetSetting(BaseSetting + "ThumbCropIdentifier", "none"));
                }
                else
                {
                    int w,h;
                    Enums.CROP_IDENTIFIER cropIdentifier;
                    CS.General_v3.Settings.ImageInfo.ParseImageSettingsIntoValues(thumbSize, out w, out h, out cropIdentifier);
                    settings.ThumbnailWidth = w;
                    settings.ThumbnailHeight = h;
                    settings.ThumbnailCropIdentifier = cropIdentifier;
                }
                int width = 0, height = 0;
                string videoSize = CS.General_v3.Settings.GetSetting(BaseSetting + "VideoSize", "");
                if (!string.IsNullOrEmpty(videoSize))
                {
                    _webFileUploadItem.parseSizeFromString(videoSize, out width, out height);
                }
                else
                {
                    width = CS.General_v3.Settings.GetSettingInt(BaseSetting + "VideoWidth", 0);
                    height = CS.General_v3.Settings.GetSettingInt(BaseSetting + "VideoHeight", 0);
                
                }
                settings.VideoWidth = width;
                settings.VideoHeight = height;
                _webFileUploadItem.VideoThumbnailExtension = CS.General_v3.Settings.GetSetting(BaseSetting + "ThumbExt", "jpg");
                settings.TotalThumbnails = CS.General_v3.Settings.GetSettingInt(BaseSetting + "TotalThumbnails", 1);

                if (string.IsNullOrEmpty(_webFileUploadItem.VideoThumbnailExtension)) settings.VideoExtension = "jpg";
                LoadSettings(settings);
                //this.Folder = CS.General_v3.Settings.GetSetting(BaseSetting + "Folder");
                //if (string.IsNullOrEmpty(this.Folder))
                  //  throw new InvalidOperationException("Video Folder must be specified!");
            }
            public void Remove()
            {
                string path= _webFileUploadItem.GetLocalFilePath();
                CS.General_v3.Util.IO.DeleteFile(path);
                for (int i = 1; i <= this.TotalThumbnails;i++ )
                {
                    path = GetLocalThumbnailPath(i);
                    CS.General_v3.Util.IO.DeleteFile(path);
                }
            }
            public string GetLocalThumbnailPath()
            {
                return GetLocalThumbnailPath(1);
            }
            public string GetLocalThumbnailPath(int Index)
            {
                checkThumbnails(true);
                string path = GetImageWebPath(Index);
                return CS.General_v3.Util.PageUtil.MapPath(path);
                
            }
            public string GetImageWebPath()
            {
                return GetImageWebPath(1);
            }
            private bool _checkedThumbnails = false;
            public string GetImageWebPath(int Index)
            {
                checkThumbnails(true);
                string oldWebPath = _webFileUploadItem.getWebPathFromRoot(null, null, "_" + Index.ToString("000"), this.VideoThumbnailExtension);
                string newWebPath = _webFileUploadItem.getWebPathFromRoot(null, null, "_Thumbnail_" + Index.ToString("000"), this.VideoThumbnailExtension);
                string localOldPath = CS.General_v3.Util.PageUtil.MapPath(oldWebPath);
                string localNewPath = CS.General_v3.Util.PageUtil.MapPath(oldWebPath);
                if (System.IO.File.Exists(localOldPath))
                    return oldWebPath;
                else
                    return newWebPath;
            }
            public int GetCenterThumbnailIndex()
            {
                double d = Math.Ceiling((double)TotalThumbnails / (double)2);
                return Util.Conversion.ToInt32(d);
            }
            public string ThumbnailImageURL
            {
                get
                {
                    return GetImageWebPath(GetCenterThumbnailIndex());
                }
            }
            public string LargeImageURL
            {
                get
                {
                    return ThumbnailImageURL;
                }
            }

            public string ItemURL
            {
                get
                {
                    return _webFileUploadItem.GetWebPathFromRoot();
                }
            }
            
        }
        
    
}
