﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.MediaItems.FileUploadItem.HelperClasses
{
    public interface IImageSize
    {
        int Width { get;  }
        int Height { get;  }
        Enums.CROP_IDENTIFIER CropIdentifier { get;  }
    }
}
