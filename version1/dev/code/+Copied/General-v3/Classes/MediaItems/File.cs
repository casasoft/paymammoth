﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace CS.General_v3.Classes.MediaItems
{
    public abstract class File : BaseFile, IMediaItem
    {
        private string _Folder = null;
        /// <summary>
        /// Folder where to store the photo.  Should NOT start with a /, and SHOULD end with a /.  E.g uploads/products/
        /// </summary>
        public string Folder
        {
            get
            {
                return _Folder;
            }
            set
            {
                _Folder = value;
                if (_Folder != null && _Folder.Length > 0)
                {
                    if (_Folder[_Folder.Length - 1] != '/')
                        _Folder += "/";
                    if (_Folder.Length > 0 && _Folder[0] == '/')
                        _Folder = _Folder.Remove(0, 1);

                }
            }
        }
        public override Enums.ITEM_TYPE ItemType { get; set; }
        private string _Extension = null;
        /// <summary>
        /// The extension, without the '.'
        /// </summary>
        public override string Extension
        {
            get
            {
                return _Extension;
            }
            set
            {
                _Extension = value;
                if (_Extension == null)
                    _Extension = "";
                if (_Extension.Length >= 1)
                {
                    if (_Extension[0] == '.')
                        _Extension = _Extension.Remove(0, 1);
                }
                

            }
        }
        

        public virtual string GetWebPathFromRoot()
        {
            return base.getWebPathFromRoot(this.Folder, "", "",this.Extension);
        }
        public virtual string GetLocalPath()
        {
            return base.getLocalPath(this.Folder, "", "", this.Extension);
        }

        protected void checkRequiredVars()
        {
            StringBuilder sb = new StringBuilder();
            if (string.IsNullOrEmpty(this.Folder))
            {
                sb.AppendLine("'Folder' is required");
            }
            if (string.IsNullOrEmpty(Extension))
            {
                sb.AppendLine("'Extension' is required");
            }
            if (sb.Length > 0)
            {
                string msg = "CS.General_v3.Classes.MediaItem.File: " + this.GetType().ToString() + "\r\n\r\n" + sb.ToString();
                throw new InvalidOperationException(msg);

            }
            
        }

        public virtual void UploadFile(Stream s, string filename)
        {
            checkRequiredVars();
            base.uploadFile(Folder,"", "",this.Extension, s, filename);
        }
        public virtual void UploadFile(CS.General_v3.Controls.WebControls.Common.MyFileUpload txt)
        {
            base.uploadFile(Folder, "", "", Extension, txt);
        }
        public virtual void DownloadFile(string filename)
        {
            this.DownloadFile(null, filename);
        }
        public virtual void DownloadFile(string contentType, string filename)
        {
            base.downloadFile(Folder, "", "", Extension, contentType, filename);
        }


        

        #region IMediaItem Members

        public override int Priority { get; set; }

        public override string GetThumbnailImageUrl()
        {
            return GetWebPathFromRoot(); 
        }

        public override string GetLargeImageUrl()
        {
            return GetWebPathFromRoot();; 
        }

        public override string GetOriginalItemUrl()
        {
             return GetWebPathFromRoot(); 
        }

        //public abstract string Caption {get;set;}
        public override int GetFileSizeInKB()
        {
                return (int)CS.General_v3.Util.IO.GetFileSizeInKB(GetLocalPath());
        }

        /// <summary>
        /// Identical to 'DeleteFile'
        /// </summary>
        public override void Delete()
        {
            string path = this.getLocalPath(Folder, "", "", Extension);
            CS.General_v3.Util.IO.DeleteFile(path);
            
        }

        public override void SetMediaItem(System.IO.Stream stream, string filename)
        {
            UploadFile(stream, filename);

        }

        #endregion

        #region IMediaItem Members


        //public abstract void Save();

        #endregion

        #region IMediaItem Members



        #endregion

        #region IMediaItem Members


        

        #endregion

        
    }
}
