﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.MediaItems.ImageSizes;

namespace CS.General_v3.Classes.MediaItems
{
    public interface IMediaItemDB : IMediaItemInfo
    {
        new string Identifier { get; set; }
        /// <summary>
        /// This should generate the filename, update the field and Save in DB
        /// </summary>
        /// <param name="uploadedFilename"></param>
        /// <returns></returns>
        string GenerateFilename(string uploadedFilenameOnly, string uploadedFilenameExtension);
        
        /// <summary>
        /// Sets the item as the main item (priority).  
        /// </summary>
        OperationResult SetAsMainItem();
        /// <summary>
        /// Called whenever the image is deleted.  Files, etc will be automatically deleted
        /// </summary>
        void OnDeletedItem();
       // IImageSizing ImageSizing { get; }
    }
}
