﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.HelperClasses;

namespace CS.General_v3.Classes.MediaItems
{
    public interface IMediaItemInfo
    {
        string Identifier { get;  }
        int Priority { get; set; }
        string Caption { get; set; }
       // string Extension { get;  }
        OperationResult Delete();
        OperationResult Save();
        string ExtraValueChoice {get;set;}
    }
}
