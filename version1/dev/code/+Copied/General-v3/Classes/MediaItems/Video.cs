﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace CS.General_v3.Classes.MediaItems
{
    public abstract class Video : MediaItemFile
    {
        public int TotalThumbnails { get; set; }
        public string ThumbExtension { get; set; }
        public int? ThumbnailWidth { get; set; }
        public int? ThumbnailHeight { get; set; }
        public int? VideoWidth { get; set; }
        public int? VideoHeight { get; set; }
        public int VideoBitRate { get; set; }
        public int? VideoFPS { get; set; }
        public bool DisableAudio { get; set; }
        public int? AudioSampleRate { get; set; }
        public int? AudioBitRate { get; set; } 
        public abstract int Duration { get; set; }
        public CS.General_v3.Enums.CROP_IDENTIFIER ThumbnailCropIdentifier { get; set; }
        public Video()
        {
            this.TotalThumbnails = 1;
            this.ThumbExtension = "jpg";
            this.VideoBitRate = 440;
            this.AudioSampleRate = 44100;
            this.AudioBitRate = 64;
            this.DisableAudio = false;
        }

        protected void loadSettingsFromFile(string BaseSetting)
        {
            /*
              <add key="Videos_Gallery_Folder" value="/uploads/videos/gallery/"/>
              <add key="Videos_Gallery_TotalThumbnails" value="1"/>
              <add key="Videos_Gallery_VideoFPS" value=""/>
              <add key="Videos_Gallery_VideoBitRate" value="440"/>
              <add key="Videos_Gallery_ThumbWidth" value="320"/>
              <add key="Videos_Gallery_ThumbHeight" value="240"/>
              <add key="Videos_Gallery_ThumbCropIdentifier" value="MiddleMiddle"/>
             * */
            if (!BaseSetting.EndsWith("/"))
                BaseSetting += "/";
            if (BaseSetting.StartsWith("/"))
                BaseSetting = BaseSetting.Remove(0, 1);

            this.DisableAudio = CS.General_v3.Settings.GetSettingBool(BaseSetting + "DisableAudio", false);
            this.AudioSampleRate= CS.General_v3.Settings.GetSettingInt(BaseSetting + "AudioSampleRate", 44100);
            this.AudioBitRate= CS.General_v3.Settings.GetSettingInt(BaseSetting + "AudioBitRate", 64);
            this.VideoFPS = CS.General_v3.Settings.GetSettingInt(BaseSetting + "VideoFPS", 0);
            this.Extension = CS.General_v3.Settings.GetSetting(BaseSetting + "VideoExt", "flv");
            this.VideoBitRate = CS.General_v3.Settings.GetSettingInt(BaseSetting + "VideoBitRate", 440);
            this.ThumbnailWidth= CS.General_v3.Settings.GetSettingInt(BaseSetting + "ThumbWidth",0);
            this.ThumbnailHeight= CS.General_v3.Settings.GetSettingInt(BaseSetting + "ThumbHeight",0);
            this.VideoWidth= CS.General_v3.Settings.GetSettingInt(BaseSetting + "VideoWidth", 0);
            this.VideoHeight= CS.General_v3.Settings.GetSettingInt(BaseSetting + "VideoHeight", 0);
            this.ThumbExtension = CS.General_v3.Settings.GetSetting(BaseSetting + "ThumbExt", "jpg");
            this.ThumbnailCropIdentifier = CS.General_v3.Enums.CropIdentifierFromText(CS.General_v3.Settings.GetSetting(BaseSetting + "ThumbCropIdentifier", "none"));
            this.TotalThumbnails = CS.General_v3.Settings.GetSettingInt(BaseSetting + "TotalThumbnails",1);

            if (string.IsNullOrEmpty(this.ThumbExtension)) this.ThumbExtension = "jpg";
            if (TotalThumbnails == 0) TotalThumbnails = 1;

            if (this.VideoFPS == 0) this.VideoFPS = null;
            if (this.VideoBitRate == 0)
                throw new InvalidOperationException("Video bitrate is required!");
            if (this.ThumbnailWidth == 0) this.ThumbnailWidth = null;
            if (this.ThumbnailHeight== 0) this.ThumbnailHeight= null;
            if (this.VideoWidth == 0) this.VideoWidth= null;
            if (this.VideoHeight == 0) this.VideoHeight= null;
            this.Folder = CS.General_v3.Settings.GetSetting(BaseSetting + "Folder");
            if (string.IsNullOrEmpty(this.Folder))
                throw new InvalidOperationException("Video Folder must be specified!");
        }

        public string GetLocalThumbnailPath()
        {
            return GetLocalThumbnailPath(1);
        }
        public string GetLocalThumbnailPath(int Index)
        {
            return base.getLocalPath(this.Folder, null, "_" + Index.ToString("000"), this.ThumbExtension);
        }
        public string GetImageWebPath()
        {
            return GetImageWebPath(1);
        }
        public string GetImageWebPath(int Index)
        {
            return base.getWebPathFromRoot(this.Folder, null, "_" + Index.ToString("000"), this.ThumbExtension);
        }
        public int GetCenterThumbnailIndex()
        {
            double d = Math.Ceiling((double)TotalThumbnails / (double)2);
            return Util.Conversion.ToInt32(d);
        }



        public override string GetThumbnailImageUrl()
        {
            return GetImageWebPath(GetCenterThumbnailIndex());
        }
        public override string GetLargeImageUrl()
        {
            return GetThumbnailImageUrl();
        }

        

        public override string GetMediaItemUrl()
        {
                return GetWebPathFromRoot();
        }
    }
}
