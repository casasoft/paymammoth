﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using CS.General_v3.Classes.HelperClasses;

namespace CS.General_v3.Classes.MediaItems
{
    public abstract class MediaItemFile : MediaItemBaseFile, IMediaItem
    {
        
        public Enums.ITEM_TYPE _ItemType;
        public override Enums.ITEM_TYPE ItemType
        {
            get
            {
                return _ItemType;
            }
        }
       /* private string _Extension = null;
        /// <summary>
        /// The extension, without the '.'
        /// </summary>
        public override string Extension
        {
            get
            {
                return _Extension;
            }
            set
            {
                _Extension = value;
                if (_Extension == null)
                    _Extension = "";
                if (_Extension.Length >= 1)
                {
                    if (_Extension[0] == '.')
                        _Extension = _Extension.Remove(0, 1);
                }
                

            }
        }
        */

        

        protected void checkRequiredVars()
        {
            StringBuilder sb = new StringBuilder();
            if (string.IsNullOrEmpty(this.Folder))
            {
                sb.AppendLine("'Folder' is required");
            }
            /*if (string.IsNullOrEmpty(Extension))
            {
                sb.AppendLine("'Extension' is required");
            }*/
            if (sb.Length > 0)
            {
                string msg = "CS.General_v3.Classes.MediaItem.File: " + this.GetType().ToString() + "\r\n\r\n" + sb.ToString();
                throw new InvalidOperationException(msg);

            }
            
        }

        public override OperationResult UploadFile(Stream s, string filename, bool autoSave = true)
        {
            checkRequiredVars();
            return base.uploadFile(Folder,"", "",null, s, filename);
        }
        public virtual OperationResult UploadFile(CS.General_v3.Controls.WebControls.Common.MyFileUpload txt)
        {
            return base.uploadFile(Folder, "", "", null, txt);
        }
        public virtual void DownloadFile(string filename)
        {
            this.DownloadFile(null, filename);
        }
        public virtual void DownloadFile(string contentType, string filename)
        {
            base.downloadFile(Folder, "", "", null, contentType, filename);
        }


        

        #region IMediaItem Members

        public override int Priority { get; set; }

        public override string GetThumbnailImageUrl()
        {
            return GetWebUrl(); 
        }

        public override string GetLargeImageUrl()
        {
            return GetWebUrl(); ; 
        }

        public override string GetMediaItemUrl()
        {
            return GetWebUrl(); 
        }

        //public abstract string Caption {get;set;}
        

        /// <summary>
        /// Identical to 'DeleteFile'
        /// </summary>
        public override OperationResult Delete()
        {
            OperationResult result = new OperationResult();


            string path = this.getLocalPath(Folder, "", "", null);
            if (File.Exists(path))
            {
                CS.General_v3.Util.IO.DeleteFile(path);
                result.SetStatusInfo(Enums.STATUS_MSG_TYPE.Success, "File deleted successfully");

            }
            else
            {
                result.SetStatusInfo(Enums.STATUS_MSG_TYPE.Warning, "File not found");
            }
            return result;
            
        }

        public override OperationResult SetMediaItem(System.IO.Stream stream, string filename)
        {
            return UploadFile(stream, filename);

        }

        #endregion



        
        
    }
}
