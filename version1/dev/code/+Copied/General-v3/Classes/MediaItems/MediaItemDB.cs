﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace CS.General_v3.Classes.MediaItems
{
    /// <summary>
    /// To use the image, you need to define the Image Sizes.  You can use either
    /// 
    ///   - AddImageSize
    ///   - AddImageSizeFromSettings
    ///   
    /// </summary>
    public class MediaItemDB : MediaItemBaseFile
    {

        private IMediaItemDB _itemDB = null;
        public override string Identifier
        {
            get { return _itemDB.Identifier; }
        }

        protected override void generateIdentifier(string uploadedFilename)
        {
             _itemDB.Identifier = _itemDB.GenerateFilename(CS.General_v3.Util.IO.GetFilenameOnly(uploadedFilename), CS.General_v3.Util.IO.GetExtension(uploadedFilename));

        }

        public override int Priority
        {
            get
            {
                return _itemDB.Priority;
            }
            set
            {
                _itemDB.Priority = value;
                
            }
        }

        public override Enums.ITEM_TYPE ItemType
        {
            get
            {
                return CS.General_v3.Enums.ItemTypeFromFilename(this.Identifier);
            }
        }

        public override string GetThumbnailImageUrl()
        {
            return "#";
        }

        public override string GetLargeImageUrl()
        {
            return "#";
        }

        public override string GetOriginalItemUrl()
        {
            //r//eturn base.getWebPathFromRoot(
            throw new NotImplementedException();
        }

        public override string Caption
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override int GetFileSizeInKB()
        {
            throw new NotImplementedException();
        }

        public override void Delete()
        {
            throw new NotImplementedException();
        }

        public override void SetMediaItem(Stream txt, string filename)
        {
            throw new NotImplementedException();
        }

        public override void Save()
        {
            throw new NotImplementedException();
        }

        public override void SetAsMainItem()
        {
            throw new NotImplementedException();
        }

        public override string ExtraValueChoice
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}
