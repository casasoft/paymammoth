﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using CS.General_v3.Classes.HelperClasses;
namespace CS.General_v3.Classes.MediaItems
{
    /// <summary>
    /// An abstract class used for uploading files.  Required parameters are 'Folder', 'Identifier', 'Extension'.
    /// </summary>
    public abstract class MediaItemBaseFile : CS.General_v3.Classes.MediaItems.IMediaItem
    {
        private object _Item;
        public virtual object Item
        {
            get { return _Item; }
            set { _Item = value; }
        }


        public MediaItemBaseFile()
        {
            
        }
        private bool identifierUpdatedFirstTime = false;
        public abstract string Identifier { get; set; }
        
        
        public virtual bool IsEmpty()
        {
            
            return (string.IsNullOrEmpty(this.Identifier));
            
        }

        private string parseFolder(string folderFromRoot)
        {
            folderFromRoot = folderFromRoot ?? "";
            if (!folderFromRoot.StartsWith("/")) folderFromRoot = "/" + folderFromRoot;
            return folderFromRoot;

            
        }

        private string getFilePath(string folderFromRoot, string prefix, string suffix, string extension)
        {
            folderFromRoot = parseFolder(folderFromRoot);
            if (!identifierUpdatedFirstTime)
            {
                identifierUpdatedFirstTime = true;
            }
            if (string.IsNullOrWhiteSpace(extension))
                extension = CS.General_v3.Util.IO.GetExtension(this.Identifier);

            return folderFromRoot + CS.General_v3.Util.IO.ConvertStringToFilename(prefix + CS.General_v3.Util.IO.GetFilenameOnly( this.Identifier) + suffix) + "." + extension;
        }
        /// <summary>
        /// Returns the path from root, WITHOUT the start '/'
        /// </summary>
        /// <param name="prefix">A prefix, before the identifier</param>
        /// <param name="suffix">A suffix, after the identifier</param>
        protected string getWebUrl(string folderFromRoot, string prefix, string suffix, string extension)
        {
            string path = "";
            if (!IsEmpty())
            {

                folderFromRoot = parseFolder(folderFromRoot);
                path = getFilePath(folderFromRoot, prefix, suffix, extension);
                path = path.Replace("\\", "/");
                //path = path.Replace("/", "/");
            }
            return path;
        }
        /// <summary>
        /// Returns the local path for this file
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="suffix"></param>
        protected string getLocalPath(string folderFromRoot, string prefix, string suffix, string extension)
        {
            string path = "";
            if (!this.IsEmpty())
            {
                folderFromRoot = parseFolder(folderFromRoot);
                path = CS.General_v3.Settings.Others.LocalRootFolder;
                if (path.EndsWith("\\"))
                    path = path.Substring(0, path.Length - 1);
                path += getFilePath(folderFromRoot, prefix, suffix, extension);
                path = path.Replace("/", "\\");
            }
            return path;
        }

        /*internal void deleteFile(string folderFromRoot, string prefix, string suffix, string extension)
        {
            folderFromRoot = parseFolder(folderFromRoot);
            string localPath = getLocalPath(folderFromRoot, prefix, suffix, extension);
            if (System.IO.File.Exists(localPath))
            {
                System.IO.File.Delete(localPath);
            }

        }*/

        /// <summary>
        /// Downloads the file to the user, with the specified filename.  The Content Type is automatically set based on the extension.
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="suffix"></param>
        /// <param name="contentType">If left null, it is automatically set </param>
        /// <param name="fileName">The filename.  If left null, it is set as the prefix + identifier + suffix + '.' + extension </param>
        internal void downloadFile(string folderFromRoot, string prefix, string suffix, string extension, string contentType, string fileName)
        {
            folderFromRoot = parseFolder(folderFromRoot);
            if (string.IsNullOrEmpty(fileName))
            {
                fileName = prefix + this.Identifier + suffix + "." + extension;
            }
            string localPath = getLocalPath(folderFromRoot, prefix, suffix,extension);
            if (string.IsNullOrEmpty(contentType))
            {
                contentType = CS.General_v3.Util.Data.GetMIMEContentType(extension);
            }
            CS.General_v3.Util.Web.ForceDownload(localPath, contentType,fileName);
        }
        private void checkRequiredVars()
        {
            StringBuilder sb = new StringBuilder();

            if (string.IsNullOrEmpty(this.Identifier))
            {
                sb.AppendLine("'Identifier' is required");

            }
           
            if (sb.Length > 0)
            {
                string msg = "CS.General_v3.Classes.MediaItem.BaseFile: " + this.GetType().ToString() + "\r\n\r\n" + sb.ToString();
                throw new InvalidOperationException(msg);

            }
        }
        /// <summary>
        /// Is called only when an update of the identifier is needed
        /// </summary>
        protected abstract void generateIdentifier(string uploadedFilename);
        
        private void createBaseDirectory(string folderFromRoot)
        {
            string path = CS.General_v3.Settings.Others.LocalRootFolder + folderFromRoot;
            path = path.Replace("/", "\\");
            CS.General_v3.Util.IO.CreateDirectory(path);
                
        }
        /// <summary>
        /// Uploads a file
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="suffix"></param>
        /// <param name="txt"></param>
        protected OperationResult uploadFile(string folderFromRoot, string prefix, string suffix, string extension, CS.General_v3.Controls.WebControls.Common.MyFileUpload txt)
        {

            return uploadFile(folderFromRoot, prefix, suffix,extension, txt.FileContent, txt.FileName);
        }
        /// <summary>
        /// Uploads a file
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="suffix"></param>
        /// <param name="s"></param>
        protected virtual OperationResult uploadFile(string folderFromRoot, string prefix, string suffix, string extension, System.IO.Stream s, string filename)
        {
            OperationResult result = new OperationResult();
            if (s.Length > 0)
            {

                string oldPath = getLocalPath(folderFromRoot, prefix, suffix, extension);

                generateIdentifier(filename);

                checkRequiredVars();
                createBaseDirectory(folderFromRoot);

                bool uploadedFileAlreadyExistsAtSameLocation = false;
                string newPath = getLocalPath(folderFromRoot, prefix, suffix, extension);
                if (s is FileStream)
                {
                    FileStream fs = (FileStream)s;
                    if (string.Compare(fs.Name, newPath, true) == 0)
                    {
                        uploadedFileAlreadyExistsAtSameLocation = true;
                    }
                }
                if (!uploadedFileAlreadyExistsAtSameLocation || (uploadedFileAlreadyExistsAtSameLocation && string.Compare(oldPath, newPath, true) != 0 && !string.IsNullOrWhiteSpace(oldPath)))
                {
                    CS.General_v3.Util.IO.DeleteFile(oldPath);
                }
                if (!uploadedFileAlreadyExistsAtSameLocation)
                {
                    CS.General_v3.Util.IO.SaveStreamToFile(s, newPath);
                }
            }
            else
            {
                result.StatusMessages.Add(new OperationResultMsg(Enums.STATUS_MSG_TYPE.Warning, "File could not be uploaded as there was an error in the file contents. Please try again"));
            }
            return result;


        }

        public abstract OperationResult UploadFile(Stream fileConents, string fileName, bool autoSave = true);


        #region IMediaItem Members

        public abstract int Priority {get;set;}
        public abstract Enums.ITEM_TYPE ItemType { get; }

        public abstract string GetThumbnailImageUrl();
        public abstract string GetLargeImageUrl();

        public abstract string GetMediaItemUrl();

        public abstract string Caption { get; set; }

        //public abstract string Extension { get;  }


        public virtual int GetFileSizeInKB()
        {

            return (int)CS.General_v3.Util.IO.GetFileSizeInKB(GetLocalPath());
        }
        
        public abstract OperationResult Delete();
        public OperationResult SetMediaItemFromLocalFile(FileInfo fileInfo)
        {
            return SetMediaItemFromLocalFile(fileInfo.FullName);
        }
        public OperationResult SetMediaItemFromLocalFile(string filePath)
        {
            string fileName = CS.General_v3.Util.IO.GetFilenameAndExtension(filePath);
            FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            var result = SetMediaItem(fs, fileName);
            fs.Dispose();
            return result;
        }

        public OperationResult SetMediaItem(CS.General_v3.Controls.WebControls.Common.MyFileUpload txt)
        {
            return this.SetMediaItem(txt.FileContent, txt.FileName);
        }

        public abstract OperationResult SetMediaItem(Stream txt, string filename);

        public abstract OperationResult Save();

        public abstract OperationResult SetAsMainItem();

        public virtual List<string> AllowedFileExtensions { get; private set; }
        IEnumerable<string> IMediaItem.GetAllowedFileExtensions()
        {
            return this.AllowedFileExtensions;
        }
        #endregion

        #region IMediaItem Members


        #endregion

        #region IMediaItem Members

        public abstract string ExtraValueChoice { get; set; }
        private string _Folder = null;
        /// <summary>
        /// Folder where to store the photo.  Should NOT start with a /, and SHOULD end with a /.  E.g uploads/products/
        /// </summary>
        public virtual string Folder
        {
            get
            {
                return _Folder;
            }
            set
            {
                _Folder = value;
                if (_Folder != null && _Folder.Length > 0)
                {
                    if (_Folder[_Folder.Length - 1] != '/')
                        _Folder += "/";
                    if (_Folder.Length > 0 && _Folder[0] == '/')
                        _Folder = _Folder.Remove(0, 1);

                }
            }
        }
        public virtual string GetWebUrl()
        {
            return this.getWebUrl(this.Folder, "", "", null);
        }
        public virtual string GetLocalPath()
        {
            return this.getLocalPath(this.Folder, "", "", null);
        }


        #endregion

        #region IMediaItem Members


        

        #endregion
    }
}
