﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.MediaItems.ImageSizes;

namespace CS.General_v3.Classes.MediaItems
{
    /// <summary>
    /// To use the image, you need to define the Image Sizes.  You can use either
    /// 
    ///   - AddImageSize
    ///   - AddImageSizeFromSettings
    ///   
    /// Also, you need to load the Original Image Sizes, which can be done by LoadOriginalImageSizesFromSettings()
    ///   
    /// </summary>
    public class MediaItemImageWithDBObjectAndEnum<TEnum> : MediaItemImageWithDBObject
        where TEnum : struct
    {

        

       // protected string baseEnumSetting = null;
        public MediaItemImageWithDBObjectAndEnum(IMediaItemImageDB mediaItem)
            : base(mediaItem)
        {

           // initImageWithDBObjectAndEnum(baseEnumSetting);
        }

        

        //protected void initImageWithDBObjectAndEnum(string baseEnumSetting)
        //{
        //    this.baseEnumSetting = baseEnumSetting;
        //    var enumValues = CS.General_v3.Util.EnumUtils.GetListOfEnumValues<TEnum>();
        //    foreach (var enumValue in enumValues)
        //    {

        //        ImageDefaultSizeInfoAttribute imageDefaultSizeAttrib = (ImageDefaultSizeInfoAttribute)CS.General_v3.Util.EnumUtils.GetAttributeFromEnumValue((Enum)(object)enumValue, typeof(ImageDefaultSizeInfoAttribute));
        //        string defValue = "1600x1200-None";
        //        if (imageDefaultSizeAttrib != null)
        //            defValue = imageDefaultSizeAttrib.GetAsStringValue();
                    
        //        string enumTitle = CS.General_v3.Util.EnumUtils.StringValueOf<TEnum>(enumValue);
        //        var sizeInfo = this.getImageSize(enumTitle);
        //        if (sizeInfo == null)
        //        {
        //            string folder = this.OriginalImageFolder + CS.General_v3.Util.IO.ConvertStringToFilename(enumTitle) + "/";
        //            bool result = AddImageSizeFromSettings(enumTitle, baseEnumSetting, defValue, throwErrorIfSettingNotFound: false);
        //            if (!result)
        //            {
                        
        //                throw new InvalidOperationException("initImageWithDBObjectAndEnum: Enum with title <" + enumTitle + "> does not have a corresponding size info!");
        //            }
        //        }
                

        //    }
        //}

        /// <summary>
        /// Returns the path from root, WITHOUT the start '/'
        /// </summary>
        /// <param name="prefix">A prefix, before the identifier</param>
        /// <param name="suffix">A suffix, after the identifier</param>
        public string GetWebUrl(TEnum enumValue)
        {
            
            string sValue = CS.General_v3.Util.EnumUtils.StringValueOf<TEnum>(enumValue);
            var imageSize= getImageSize(sValue);
            return base.getWebUrl(imageSize);

            
        }


        public override OperationResult SetAsMainItem()
        {
            return _mediaItemInfo.SetAsMainItem();
        }

        public override string Identifier
        {
            get
            {
                return _mediaItemInfo.Identifier;
            }
            set
            {
                _mediaItemInfo.Identifier = value;
            }
            
        }

    }
}
