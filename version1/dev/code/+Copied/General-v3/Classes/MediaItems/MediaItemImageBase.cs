﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.MediaItems.ImageSizes;

namespace CS.General_v3.Classes.MediaItems
{
    /// <summary>
    /// To use the image, you need to define the Image Sizes.  You can use either
    /// 
    ///   - AddImageSize
    ///   - AddImageSizeFromSettings
    ///   
    /// Also, you need to load the Original Image Sizes, which can be done by LoadOriginalImageSizesFromSettings()
    ///   
    /// </summary>
    public abstract class MediaItemImageBase : MediaItemBaseFile, IMediaItem
    {
        protected MediaItemImageBase()
        {

        }
        
        public MediaItemImageBase(IImageSizing imageSizing)
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(imageSizing);
            this.ImageSizing = imageSizing;
           
        }
        public IImageSizing ImageSizing { get; private set; }
       

        public override CS.General_v3.Enums.ITEM_TYPE ItemType { get { return CS.General_v3.Enums.ITEM_TYPE.Image; }  }
       

        private string _OriginalImageFolder = null;
        /// <summary>
        /// Folder where to store the original photo.  Should NOT start with a /, and SHOULD end with a /.  E.g uploads/products/
        /// </summary>
        public string OriginalImageFolder
        {
            get
            {
                return base.Folder;
                
            }
            set
            {
                base.Folder = value;
               

            }
        }
        

      



        /// <summary>
        /// Returns an IMAGE_SIZE_OF with the specified title
        /// </summary>
        /// <param name="title">Title</param>
        /// <returns>The IMAGE_SIZE_OF item</returns>
        protected ISpecificImageSize getImageSize(string title)
        {
            return this.ImageSizing.SpecificImageSizes.Where(item => string.Compare(item.Title, title, true) == 0).FirstOrDefault();

        }
        


        /// <summary>
        /// gets the smallest image path
        /// </summary>
        /// <returns></returns>
        public virtual string GetWebUrlForSmallest()
        {
            ISpecificImageSize leastSize = null;
            int leastArea = Int32.MaxValue;
            foreach (var size in this.ImageSizing.SpecificImageSizes)
            {
                int area = size.Width * size.Height;
                if (area < leastArea)
                {
                    leastArea = area;
                    leastSize = size;
                }
            }
            return getWebUrl(leastSize);

        }
        /// <summary>
        /// gets the smallest image path
        /// </summary>
        /// <returns></returns>
        public virtual string GetWebUrlForLargest()
        {
            ISpecificImageSize largestSize = null;
            int maxArea = 0;
            foreach (var size in this.ImageSizing.SpecificImageSizes)
            {
                int area = size.Width * size.Height;
                if (area > maxArea)
                {
                    maxArea = area;
                    largestSize = size;
                }
            }
            return getWebUrl(largestSize);

        }
        /// <summary>
        /// Get web path (from root, excluding start '/'), for image of type 'imageSize'
        /// </summary>
        /// <param name="imageSize"></param>
        /// <returns></returns>
        protected virtual string getWebUrl(ISpecificImageSize sizeInfo)
        {
            
            if (checkIfExtensionCanBeResized())
            {
                CheckImageSizes(false);
                
                string s = base.getWebUrl(sizeInfo.Folder, null, "-" + sizeInfo.Title, sizeInfo.Extension);

                return s;
            }
            else
            {
                return this.GetOriginalImageWebUrl();
            }
        }
        


        internal virtual string getLocalPath(string imageSize, bool checkImageSizes)
        {
            
            ISpecificImageSize sizeInfo = getImageSize(imageSize);
            return getLocalPath(sizeInfo, checkImageSizes);
        }
        internal virtual string getLocalPath(ISpecificImageSize sizeInfo, bool checkImageSizes)
        {
            if (checkImageSizes)
                CheckImageSizes(false);
            
            string s = base.getLocalPath(sizeInfo.Folder, null, "-" + sizeInfo.Title, sizeInfo.Extension);
            return s;
        }
        /// <summary>
        /// Returns the original image local path
        /// </summary>
        /// <returns></returns>
        public virtual string GetOriginalImageLocalPath()
        {
            string s = base.getLocalPath(this.OriginalImageFolder, null, null, null);
            return s;
        }
        /// <summary>
        /// Returns the original image web path (from root, excluding start '/')
        /// </summary>
        /// <returns></returns>
        public virtual string GetOriginalImageWebUrl()
        {
            string s = base.getWebUrl(this.OriginalImageFolder, null, null, null);
            return s;
        }
        /// <summary>
        /// Uploads an image, and resizes based on the existing sizes
        /// </summary>
        /// <param name="txt"></param>
        public void UploadFile(CS.General_v3.Controls.WebControls.Common.MyFileUpload txt)
        {
            this.UploadFile(txt.FileContent, txt.FileName);
        }
        /// <summary>
        /// Uploads an image, and resizes based on the existing sizes
        /// </summary>
        /// <param name="txt"></param>
        public void UploadFile(FileInfo file)
        {
            var fs = file.OpenRead();
            this.UploadFile(fs, file.Name);
            fs.Dispose();
        }
        /// <summary>
        /// Uploads an image, and resizes based on the existing sizes
        /// </summary>
        /// <param name="s"></param>
        public override OperationResult UploadFile(Stream fileConents, string fileName, bool autoSave = true)
        {
            if (string.IsNullOrEmpty(this.OriginalImageFolder))
                throw new InvalidOperationException("Please specify 'OriginalImageFolder'");
            
            var result = base.uploadFile(this.OriginalImageFolder, null, null, null,fileConents, fileName);
            if (result.Status == Enums.STATUS_MSG_TYPE.Success)
            {
                resizeOriginal();
                CheckImageSizes(true);
            }
            return result;

        }
        public bool ResizeGIFFiles { get; set; }
        internal bool checkIfExtensionCanBeResized()
        {
            bool ok = true;
            string filename = CS.General_v3.Util.IO.GetFilenameOnly(this.Identifier);
            string ext = CS.General_v3.Util.IO.GetExtension(this.Identifier).ToLower();
            switch (ext)
            {
                case "gif":
                    {
                        if (!ResizeGIFFiles)
                        {
                            ok = false;
                            
                        }
                        break;
                    }
            }
            if (ok && !CS.General_v3.Util.Image.CheckImageFileExtension(this.Identifier))
                ok = false;
            return ok;
        }
        private void resizeOriginal()
        {
            
            string localPath = GetOriginalImageLocalPath();
            if (checkIfExtensionCanBeResized())
            {
                CS.General_v3.Util.Image.ResizeImage(localPath, this.ImageSizing.OriginalMaxWidth, this.ImageSizing.OriginalMaxHeight, localPath);
            }

        }
        /// <summary>
        /// Checks the image sizes, and creates any ones that do not exist (or are to be overwritten)
        /// </summary>
        /// <param name="overwriteExisting">Overwrite existing files</param>
        public void CheckImageSizes(bool overwriteExisting)
        {
            if (checkIfExtensionCanBeResized())
            {
                string localPath = GetOriginalImageLocalPath();
                if (System.IO.File.Exists(localPath))
                {
                    foreach (var size in this.ImageSizing.SpecificImageSizes)
                    {
                        checkSpecificImageFiles(size, overwriteExisting);
                        
                    }
                }
            }
        }


        private void checkSpecificImageFiles(ISpecificImageSize size, bool overwriteExisting)
        {

            
            string localPath = this.GetOriginalImageLocalPath();
            string newLocalPath = this.getLocalPath(size, false);

            if (System.IO.File.Exists(localPath))
            {

                if (overwriteExisting || !System.IO.File.Exists(newLocalPath))
                {
                    if (this.checkIfExtensionCanBeResized())
                    {
                        if (size.CropIdentifier != Enums.CROP_IDENTIFIER.None)
                        {
                            CS.General_v3.Util.Image.ResizeAndCropImage(localPath, size.Width, size.Height, newLocalPath, size.CropIdentifier);
                        }
                        else
                        {
                            CS.General_v3.Util.Image.ResizeImage(localPath, size.Width, size.Height, newLocalPath);
                        }
                    }
                    else
                    {
                        CS.General_v3.Util.IO.CopyFile(localPath, newLocalPath);
                    }
                }
            }
        }



        #region IMediaItem Members

        //public abstract int Priority {get;set;}
        private List<ISpecificImageSize> getImageSizesSortedBySize()
        {
            List<ISpecificImageSize> list = new List<ISpecificImageSize>();
            list.AddRange(this.ImageSizing.SpecificImageSizes);
            list.Sort((size1, size2) => (size1.Width * size1.Height).CompareTo((size2.Width * size2.Height)));
            return list;

        }
        public override string GetThumbnailImageUrl()
        {
            List<ISpecificImageSize> list = getImageSizesSortedBySize();
            
            if (list.Count > 0)
            {
                return getWebUrl(list[0]);
            }
            else
            {
                return null;
            }
        }

        public override string GetLargeImageUrl()
        {
            List<ISpecificImageSize> list = getImageSizesSortedBySize();
            int index = 0;
            if (list.Count > 0)
            {

                index = list.Count - 1;
                if (list.Count > 2)
                    index--;
                return getWebUrl(list[index]);
            }
            else
            {
                return null;
            }
        }

        public override string GetMediaItemUrl()
        {
            string url = null;
            if (!this.IsEmpty())
            {
                List<ISpecificImageSize> list = getImageSizesSortedBySize();
                int index = 0;
                index = list.Count - 1;

                if (list.Count > 0)
                {
                    url = getWebUrl(list[index]);
                }
            }
            return url;
        }

        //public abstract string Caption {get;set;}

        //public override int GetFileSizeInKB()
        //{
        //    return (int)CS.General_v3.Util.IO.GetFileSizeInKB(GetOriginalLocalPath()); 
        //}

        public override OperationResult Delete()
        {
            OperationResult result = new OperationResult();
            string path = this.getLocalPath(this.OriginalImageFolder, null, null, null);
            CS.General_v3.Util.IO.DeleteFile(path);
            if (!File.Exists(path))
            {
                result.StatusMessages.Add(new OperationResultMsg(Enums.STATUS_MSG_TYPE.Warning, "Original file could not be deleted as it does not exist"));
            }

            foreach (var size in this.ImageSizing.SpecificImageSizes)
            {
                result.AddFromOperationResult(removeSpecificImage(size));
            }
            return result;
            //this.Save();
        }


        protected OperationResult removeSpecificImage(ISpecificImageSize imageSize)
        {
            OperationResult result = new OperationResult();

            string path = this.getLocalPath(imageSize, false);
            if (File.Exists(path))
            {
                CS.General_v3.Util.IO.DeleteFile(path);
            }
            else
            {
                result.StatusMessages.Add(new OperationResultMsg(Enums.STATUS_MSG_TYPE.Warning, "File for size " + imageSize.Title + " could not be deleted as it does not exist"));

            }
            return result;
        }


        /// <summary>
        /// An identical copy of 'UploadFile'.  
        /// </summary>
        /// <param name="txt"></param>
        public override OperationResult SetMediaItem(Stream fileContents, string filename)
        {
           
                
            return this.UploadFile(fileContents, filename);
            
        }

        #endregion


        
    }
}
