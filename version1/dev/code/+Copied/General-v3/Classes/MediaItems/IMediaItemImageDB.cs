﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.MediaItems.ImageSizes;

namespace CS.General_v3.Classes.MediaItems
{
    public interface IMediaItemImageDB : IMediaItemDB
    {
        
        IImageSizing ImageSizing { get; }
    }
}
