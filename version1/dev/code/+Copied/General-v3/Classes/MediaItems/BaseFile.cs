﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
namespace CS.General_v3.Classes.MediaItems
{
    /// <summary>
    /// An abstract class used for uploading files.  Required parameters are 'Folder', 'Identifier', 'Extension'.
    /// </summary>
    public abstract class BaseFile : CS.General_v3.Classes.MediaItems.IMediaItem
    {
        private object _Item;
        public virtual object Item
        {
            get { return _Item; }
            set { _Item = value; }
        }


        public BaseFile()
        {
            
        }
        private bool identifierUpdatedFirstTime = false;
        public abstract string Identifier { get; set;  }
        public virtual string Extension { get; set; }
        
        public virtual bool IsEmpty()
        {
            
            return (string.IsNullOrEmpty(this.Identifier));
            
        }

        private string parseFolder(string folderFromRoot)
        {
            folderFromRoot = folderFromRoot ?? "";
            if (!folderFromRoot.StartsWith("/")) folderFromRoot = "/" + folderFromRoot;
            return folderFromRoot;

            
        }

        private string getFilePath(string folderFromRoot, string prefix, string suffix, string extension)
        {
            folderFromRoot = parseFolder(folderFromRoot);
            if (!identifierUpdatedFirstTime)
            {
                identifierUpdatedFirstTime = true;
            }
            return folderFromRoot + CS.General_v3.Util.IO.ConvertStringToFilename(prefix + CS.General_v3.Util.IO.GetFilenameOnly( this.Identifier) + suffix) + "." + extension;
        }
        /// <summary>
        /// Returns the path from root, WITHOUT the start '/'
        /// </summary>
        /// <param name="prefix">A prefix, before the identifier</param>
        /// <param name="suffix">A suffix, after the identifier</param>
        protected string getWebPathFromRoot(string folderFromRoot, string prefix, string suffix, string extension)
        {
            folderFromRoot = parseFolder(folderFromRoot);
            string path = "";
            path = getFilePath(folderFromRoot, prefix, suffix,extension);
            path = path.Replace("\\", "/");
            //path = path.Replace("/", "/");
            return path;
        }
        /// <summary>
        /// Returns the local path for this file
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="suffix"></param>
        protected string getLocalPath(string folderFromRoot, string prefix, string suffix, string extension)
        {
            folderFromRoot = parseFolder(folderFromRoot);
            string path = CS.General_v3.Settings.Others.LocalRootFolder;
            if (path.EndsWith("\\"))
                path = path.Substring(0, path.Length - 1);
            path += getFilePath(folderFromRoot, prefix, suffix, extension);
            path = path.Replace("/", "\\");
            return path;
        }

        /*internal void deleteFile(string folderFromRoot, string prefix, string suffix, string extension)
        {
            folderFromRoot = parseFolder(folderFromRoot);
            string localPath = getLocalPath(folderFromRoot, prefix, suffix, extension);
            if (System.IO.File.Exists(localPath))
            {
                System.IO.File.Delete(localPath);
            }

        }*/

        /// <summary>
        /// Downloads the file to the user, with the specified filename.  The Content Type is automatically set based on the extension.
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="suffix"></param>
        /// <param name="contentType">If left null, it is automatically set </param>
        /// <param name="fileName">The filename.  If left null, it is set as the prefix + identifier + suffix + '.' + extension </param>
        internal void downloadFile(string folderFromRoot, string prefix, string suffix, string extension, string contentType, string fileName)
        {
            folderFromRoot = parseFolder(folderFromRoot);
            if (string.IsNullOrEmpty(fileName))
            {
                fileName = prefix + this.Identifier + suffix + "." + extension;
            }
            string localPath = getLocalPath(folderFromRoot, prefix, suffix,extension);
            if (string.IsNullOrEmpty(contentType))
            {
                contentType = CS.General_v3.Util.Data.getMIMEContentType(extension);
            }
            CS.General_v3.Util.Web.ForceDownload(localPath, contentType,fileName);
        }
        private void checkRequiredVars()
        {
            StringBuilder sb = new StringBuilder();

            if (string.IsNullOrEmpty(this.Identifier))
            {
                sb.AppendLine("'Identifier' is required");

            }
           
            if (sb.Length > 0)
            {
                string msg = "CS.General_v3.Classes.MediaItem.BaseFile: " + this.GetType().ToString() + "\r\n\r\n" + sb.ToString();
                throw new InvalidOperationException(msg);

            }
        }
        /// <summary>
        /// Is called only when an update of the identifier is needed
        /// </summary>
        protected abstract void generateIdentifier(string uploadedFilename);
        
        private void createBaseDirectory(string folderFromRoot)
        {
            string path = CS.General_v3.Settings.Others.LocalRootFolder + folderFromRoot;
            path = path.Replace("/", "\\");
            CS.General_v3.Util.IO.CreateDirectory(path);
                
        }
        /// <summary>
        /// Uploads a file
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="suffix"></param>
        /// <param name="txt"></param>
        protected void uploadFile(string folderFromRoot, string prefix, string suffix, string extension, CS.General_v3.Controls.WebControls.Common.MyFileUpload txt)
        {

            uploadFile(folderFromRoot, prefix, suffix,extension, txt.FileContent, txt.FileName);
        }
        /// <summary>
        /// Uploads a file
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="suffix"></param>
        /// <param name="s"></param>
        protected virtual void uploadFile(string folderFromRoot, string prefix, string suffix, string extension, System.IO.Stream s, string filename)
        {
            string oldPath = getLocalPath(folderFromRoot, prefix, suffix, extension);

            generateIdentifier(filename);
            
            checkRequiredVars();
            createBaseDirectory(folderFromRoot);

            bool uploadedFileAlreadyExists = false;
            string newPath = getLocalPath(folderFromRoot, prefix,suffix,extension);
            if (s is FileStream)
            {
                FileStream fs = (FileStream)s;
                if (string.Compare(fs.Name, newPath, true) == 0)
                {
                    uploadedFileAlreadyExists = true;
                }
            }
            if (!uploadedFileAlreadyExists || (uploadedFileAlreadyExists && string.Compare(oldPath, newPath, true) != 0))
            {
                CS.General_v3.Util.IO.DeleteFile(oldPath);
            }
            if (!uploadedFileAlreadyExists)
            {
                CS.General_v3.Util.IO.SaveStreamToFile(s, newPath);
            }
            


        }



        #region IMediaItem Members

        public abstract int Priority {get;set;}
        public abstract Enums.ITEM_TYPE ItemType { get; set; }

        public abstract string GetThumbnailImageUrl();
        public abstract string GetLargeImageUrl();

        public abstract string GetOriginalItemUrl();

        public abstract string Caption { get; set; }

        //public abstract string Extension { get;  }


        public abstract int GetFileSizeInKB();
        public abstract void Delete();

        public void SetMediaItem(CS.General_v3.Controls.WebControls.Common.MyFileUpload txt)
        {
            this.SetMediaItem(txt.FileContent, txt.FileName);
        }

        public abstract void SetMediaItem(Stream txt, string filename);

        public abstract void Save();

        public abstract void SetAsMainItem();

        #endregion

        #region IMediaItem Members


        #endregion

        #region IMediaItem Members

        public abstract string ExtraValueChoice { get; set; }
        
        

        #endregion

        #region IMediaItem Members


        

        #endregion
    }
}
