﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using CS.General_v3.Classes.HelperClasses;

namespace CS.General_v3.Classes.MediaItems
{
    public interface IMediaItem : IMediaItemInfo
    {
        Enums.ITEM_TYPE ItemType { get;  }
        string GetThumbnailImageUrl();
        string GetLargeImageUrl();
        string GetMediaItemUrl();
        int GetFileSizeInKB();
        IEnumerable<string> GetAllowedFileExtensions();
        OperationResult SetMediaItem(Stream fileContents, string filename);

        OperationResult SetAsMainItem();
        bool IsEmpty();
    }
}
