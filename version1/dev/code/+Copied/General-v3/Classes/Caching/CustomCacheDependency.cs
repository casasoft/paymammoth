﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Caching;

namespace CS.General_v3.Classes.Caching
{
    public class CustomCacheDependency : CacheDependency
    {
        internal CustomCacheDependency()
        {
            
        }
        public void Invalidate()
        {
            base.NotifyDependencyChanged(this, EventArgs.Empty);
            
        }
        


    }
}
