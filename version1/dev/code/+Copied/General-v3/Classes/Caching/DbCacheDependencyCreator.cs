﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.DbObjects;
using CS.General_v3.Classes.NHibernateClasses;
using Iesi.Collections.Generic;
using System.Web.Caching;

namespace CS.General_v3.Classes.Caching
{
    public class DbCacheDependencyCreator
    {
        public DbCacheDependencyCreator()
        {
            this.DbObjects = new HashedSet<IBaseDbObject>();
            this.DbFactories = new HashedSet<IBaseDbFactory>();
        }

        public HashedSet<IBaseDbObject> DbObjects { get; private set; }
        public HashedSet<IBaseDbFactory> DbFactories { get; private set; }
        public void AddDbObjects(IEnumerable<IBaseDbObject> dbObjects)
        {
            this.DbObjects.AddAll(DbObjects);
        }

        public void AddDbObjects(params IBaseDbObject[] dbObjects)
        {
            this.DbObjects.AddAll(dbObjects);
        }
        public void AddDbObjects(params IBaseDbFactory[] dbFactories)
        {
            this.DbFactories.AddAll(dbFactories);
        }

        public CacheDependency GetCacheDependency()
        {
            List<string> list = new List<string>();
            foreach (var item in DbObjects)
            {
                list.Add(item.GetOutputCacheDependencyKey());
            }
            foreach (var item in DbFactories)
            {
                list.Add(item.GetOutputCacheDependencyKey());
            }
            return CS.General_v3.Util.CachingUtil.GetCacheDependency(list.ToArray());
        }


    }
}
