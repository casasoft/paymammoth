﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Iesi.Collections.Generic;

namespace CS.General_v3.Classes.Caching
{
    using System.Web.Caching;

    public class CacheInvalidator
    {
        public static CacheInvalidator Instance
        {
            get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<CacheInvalidator>(); }
        }
        private CacheInvalidator()
        {
            
        }


      //  private HashedSet<string> stillToInvalidate = new HashedSet<string>();

        private Cache Cache
        {
            get { return HttpRuntime.Cache; }
        }

        private void invalidateCacheDependencies(IEnumerable<string> keys)
        {
            var list = keys.ToList();
            foreach (var k in list)
            {
                if (k != null)
                {
                    Cache[k] = DateTime.Now;
                }
            }
        }

        /*public void CheckCacheToInvalidate()
        {
            
           // if (stillToInvalidate.Count > 0)
          //  {
           /     invalidateCacheDependencies(stillToInvalidate);
           //     stillToInvalidate.Clear();
           // }
        }*/
        public void InvalidateCacheDependencies(params string[] keys)
        {
            InvalidateCacheDependencies(keys.ToList());
        }

        public void InvalidateCacheDependencies(IEnumerable<string> keys)
        {
            //if (Context != null)
            //{
                invalidateCacheDependencies(keys);
            /*}
            else
            {
                stillToInvalidate.AddAll(keys.ToList());
            }*/
        }
    }
}
