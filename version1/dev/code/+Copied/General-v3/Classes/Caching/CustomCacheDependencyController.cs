﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Caching
{
    public class CustomCacheDependencyController
    {
        public static CustomCacheDependencyController Instance
        {
            get { return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<CustomCacheDependencyController>(null); }
        }
        private List<CustomCacheDependency> dependencies = null;
        private CustomCacheDependencyController()
        {
            dependencies = new List<CustomCacheDependency>();

        }
        public CustomCacheDependency GetNewCacheDependency()
        {
            CustomCacheDependency dep = new CustomCacheDependency();
            this.dependencies.Add(dep);
            return dep;
        }
        public void InvalidateAll()
        {
            for (int i = 0; i < dependencies.Count; i++)
            {
                var d = dependencies[i];
                if (d != null)
                {
                    d.Invalidate();
                }
            }

        }

    }
}
