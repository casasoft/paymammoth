﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.ConversionUnits
{
    public class UnitConvertorNonLinear<TUnitEnumType> : UnitConvertor<TUnitEnumType>
    {
        public delegate double UnitConvertorNonLinearHandler(TUnitEnumType type, double value);


        private UnitConvertorNonLinearHandler _convertValueToBase;
        private UnitConvertorNonLinearHandler _convertBaseToValue;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="convertValueToBase">Convert value to Base, e.g. if base is oC and you need them in oF, convert from oC to oF</param>
        /// <param name="convertBaseToValue"></param>
        public UnitConvertorNonLinear(UnitConvertorNonLinearHandler convertValueToBase, UnitConvertorNonLinearHandler convertBaseToValue)
            : base()
        {
            _convertValueToBase = convertValueToBase;
            _convertBaseToValue = convertBaseToValue;
        }




        protected override double convertValueToBase(TUnitEnumType type, double value)
        {
            return _convertValueToBase(type, value);
        }

        protected override double convertBaseToValue(TUnitEnumType type, double value)
        {
            return _convertBaseToValue(type, value);
        }
    }
}
