﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.ConversionUnits
{
    /// <summary>
    /// Return the value to its base value
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public abstract class UnitConvertor<TUnitEnumType>
    {
        public class UnitConvertorItem
        {
            public TUnitEnumType UnitType { get; set; }
            public double Value { get; set; }
        }

        public delegate double UnitConvertorToBaseHandler(double value, TUnitEnumType type);

        public UnitConvertor()
        {
        }


        protected abstract double convertValueToBase(TUnitEnumType type, double value);
        protected abstract double convertBaseToValue(TUnitEnumType type, double value);


        public double ConvertValueToUnit(double valueFrom, TUnitEnumType fromType, TUnitEnumType toType)
        {
            double value = convertValueToBase(fromType, valueFrom);
            value = convertBaseToValue(toType, value);
            return value;
        }
        public double SumValuesToCommonUnit(List<UnitConvertorItem> values, TUnitEnumType toType)
        {
            double result = 0;
            foreach (UnitConvertorItem value in values)
            {
                result += ConvertValueToUnit(value.Value, value.UnitType, toType);
            }
            return result;
        }
        public string SumValuesToCommonUnitAsText(List<UnitConvertorItem> values, TUnitEnumType toType, int decimalPlaces = 2, string thousandsDelimiter = ",")
        {
            double result = SumValuesToCommonUnit(values, toType);
            string s = CS.General_v3.Util.NumberUtil.FormatNumber(result, thousandsDelimiter, decimalPlaces) + CS.General_v3.Util.EnumUtils.GetUnitTextSuffix(toType);
            return s;
        }
        /// <summary>
        /// Sum all the values, to the largest unit.  E.g. if you have grams, kgs, tonnes, they will all be converted to tons.
        /// 
        /// If you have mm, cm and km, they will all be converted to km.
        /// </summary>
        /// <param name="values"></param>
        /// <param name="typeConvertedTo"></param>
        /// <returns></returns>
        public double SumValuesToLargestUnit(List<UnitConvertorItem> values, out TUnitEnumType typeConvertedTo)
        {
            double largestRatio = double.MinValue;
            typeConvertedTo = default(TUnitEnumType);
            foreach (var value in values)
            {
                double ratio = convertValueToBase(value.UnitType, 1);
                if (ratio > largestRatio)
                {
                    typeConvertedTo = value.UnitType;
                    largestRatio = ratio;
                }
            }
            double result = SumValuesToCommonUnit(values, typeConvertedTo);
            return result;
        }
        public string SumValuesToLargestUnitAsText(List<UnitConvertorItem> values, int decimalPlaces = 2, string thousandsDelimiter = ",")
        {
            TUnitEnumType type;
            return SumValuesToLargestUnitAsText(values, out type, decimalPlaces, thousandsDelimiter);
        }

        public string SumValuesToLargestUnitAsText(List<UnitConvertorItem> values, out TUnitEnumType typeConvertedTo, int decimalPlaces = 2, string thousandsDelimiter = ",")
        {
            double value = SumValuesToLargestUnit(values, out typeConvertedTo);
            string s = CS.General_v3.Util.NumberUtil.FormatNumber(value, thousandsDelimiter, decimalPlaces) + CS.General_v3.Util.EnumUtils.GetUnitTextSuffix(typeConvertedTo);
            return s;
        }
        public bool IsUnitLargerThanUnit(TUnitEnumType type1, TUnitEnumType type2)
        {
            double val = ConvertValueToUnit(1, type1, type2);
            return val > 1;
        }
        
    }
}
