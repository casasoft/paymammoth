﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.ConversionUnits
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TUnitEnumType">The enumeration type of this unit.  They must have the UnitAttribute to specify the unit conversion</typeparam>
    public class UnitConvertorLinear<TUnitEnumType> : UnitConvertor<TUnitEnumType>
    {
        public UnitConvertorLinear()
            : base()
        {

        }




        protected override double convertValueToBase(TUnitEnumType type, double value)
        {
            double? ratio = CS.General_v3.Util.EnumUtils.UnitRatioOfNullable<TUnitEnumType>(type);
            if (ratio.HasValue)
            {
                return value * ratio.Value;
            }
            else
            {
                throw new ArgumentException("Unit attribute must be defined with enumeration to use this conversion");
            }
        }

        protected override double convertBaseToValue(TUnitEnumType type, double value)
        {
            double integralRatio = convertValueToBase(type, 1);
            return value / integralRatio;
        }
        
    }
}
