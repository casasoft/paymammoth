﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CS.General_v3.Classes.Factories;
using CS.General_v3.Classes.Locking;

namespace CS.General_v3.Classes.Locking
{
    public class LockManager
    {
        public static LockManager Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<LockManager>(); } }

        private Dictionary<string, ILock> _locks = null;
        private readonly object managerLock = new object();
        

        private LockManager()
        {
            _locks = new Dictionary<string, ILock>();
        }

        private string getKey(string key)
        {
            if (key != null)
                return key.ToLower();
            else
                return null;

                
        }
        /// <summary>
        /// Returns a lock for an object
        /// </summary>
        /// <param name="dbObject">Database object, e.g Member</param>
        /// <param name="lockType">Lock Type, e.g 'AccountBalanceUpdate'.  This allows you to specify distinct locks per same db object</param>
        /// <returns>The lock</returns>
        public ILock GetLock(IBaseObject dbObject, string lockType = null)
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(dbObject, "dbObject cannot be null");
            string key = dbObject.GetType().FullName + "-" + dbObject.ID + "-" + lockType;
            return GetLock(key);
        }
       

        public ILock GetLock(string key)
        {
            string lockKey = getKey(key);
            ILock resultLock = null;
            if (!_locks.TryGetValue(lockKey, out resultLock)) //if lock does not exist
            {
                lock (managerLock) //lock, so that you make sure that no other thread is trying to create the lock as well
                {
                    if (!_locks.TryGetValue(lockKey, out resultLock)) //check that it has not been created while waiting to get the 'managerLock'
                    {
                        resultLock = new LockImpl(this); //create the lock
                        _locks[lockKey] = resultLock;
                    }
                }
            }
            return resultLock;
            
        }


    }
}
