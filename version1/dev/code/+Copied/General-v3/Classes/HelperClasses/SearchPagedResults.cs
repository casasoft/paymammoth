﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.HelperClasses
{
    public class SearchPagedResults<T> : IEnumerable<T>
    {
        private List<T> _list = null;
        public SearchPagedResults(IEnumerable<T> list, int totalResults)
        {
            this.TotalResults = totalResults;
            _list = new List<T>();
            if (list != null)
                _list.AddRange(list);
        }

        public int TotalResults { get; private set; }
        public IEnumerable<T> GetResults()
        {
            return _list.ToList();
        }

        #region IEnumerable<T> Members

        public IEnumerator<T> GetEnumerator()
        {
            return _list.GetEnumerator();

        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _list.GetEnumerator();

        }

        #endregion
    }
}
