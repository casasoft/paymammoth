﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.HelperClasses
{
    public class CompareTwoListsResults<TItemToBeAdded, TItemThatCouldBeRemoved>
    {
        public List<TItemToBeAdded> ItemsToBeAdded { get; private set; }
        public List<TItemThatCouldBeRemoved> ItemsToBeRemoved { get; private set; }
        public CompareTwoListsResults(List<TItemToBeAdded> itemsToAdd, List<TItemThatCouldBeRemoved> itemsToRemove)
        {
            this.ItemsToBeAdded = itemsToAdd;
            this.ItemsToBeRemoved = itemsToRemove;
        }
    }
}
