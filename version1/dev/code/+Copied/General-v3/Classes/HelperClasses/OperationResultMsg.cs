﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using System.Web.UI.WebControls;

namespace CS.General_v3.Classes.HelperClasses
{
    [Serializable]
    public class OperationResultMsg
    {
        public string AdditionalInformation { get; set; }
        public string Message { get; set; }
        public CS.General_v3.Enums.STATUS_MSG_TYPE Status { get; set; }
        //public bool CanBeShownInCms {get;set;}
        public List<OperationResultMsg> SubMessages { get; private set; }
        public OperationResultMsg()
        {
            this.Status = Enums.STATUS_MSG_TYPE.Success;
        }
        public OperationResultMsg(CS.General_v3.Enums.STATUS_MSG_TYPE status, string message, string additionalInformation = null)
        {
            this.AdditionalInformation = additionalInformation;
            
            this.Status = status;
            this.Message = message;
            this.SubMessages = new List<OperationResultMsg>();
        }
        public override string ToString()
        {
            return this.AdditionalInformation + " - " + this.Message;
        }
        public string GetMessageAsString(int padLevel, bool showAdditionalInfo = false)
        {
            int padAmount = padLevel *5;
            string padding = CS.General_v3.Util.Text.RepeatText(" ", padAmount);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine(padding + "Message: " + this.Message);
            if (showAdditionalInfo && !string.IsNullOrWhiteSpace(this.AdditionalInformation))
            {
                sb.AppendLine();
                sb.AppendLine(padding+"Details:");
                sb.AppendLine(padding+"-----------");
                sb.AppendLine(CS.General_v3.Util.Text.PadAllLines(this.AdditionalInformation, padAmount));

            }

            {
                bool oneOK = false;
                foreach (var subMsg in SubMessages)
                {
                    if (subMsg.Status != Enums.STATUS_MSG_TYPE.Success ||
                        (subMsg.Status == Enums.STATUS_MSG_TYPE.Success && this.Status == Enums.STATUS_MSG_TYPE.Success))
                    {
                        oneOK = true;
                        sb.AppendLine(subMsg.GetMessageAsString(padLevel+1));
                    }
                }
            }

            return sb.ToString();
        }

        public string GetMessageAsHtml(bool showAdditionalInfo = false)
        {
            MyDiv divMsg = new MyDiv();
            divMsg.CssManager.AddClass("additional-message");
            divMsg.CssManager.AddClass(CS.General_v3.Util.EnumUtils.StringValueOf(Status).ToLower());
                
            {
                MySpan spanTitle = new MySpan();
                spanTitle.CssManager.AddClass("additional-message-title");
                spanTitle.CssManager.AddClass(CS.General_v3.Util.EnumUtils.StringValueOf(Status).ToLower());
                spanTitle.InnerHtml = CS.General_v3.Util.Text.HtmlEncode(this.Message);
                divMsg.Controls.Add(spanTitle);
            }
            {
                if (showAdditionalInfo&& !string.IsNullOrWhiteSpace(this.AdditionalInformation))
                {
                    MySpan spanMessage = new MySpan();
                    spanMessage.CssManager.AddClass("additional-message-details");
                    spanMessage.CssManager.AddClass(CS.General_v3.Util.EnumUtils.StringValueOf(Status).ToLower());
                    spanMessage.InnerHtml = CS.General_v3.Util.Text.HtmlEncode(this.AdditionalInformation);
                    divMsg.Controls.Add(spanMessage);
                }
            }
            {
                if (SubMessages != null && SubMessages.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("<ul class=\"sub-message\">");
                    bool oneOK = false;
                    foreach (var subMsg in SubMessages)
                    {
                        if (subMsg.Status != Enums.STATUS_MSG_TYPE.Success ||
                            (subMsg.Status == Enums.STATUS_MSG_TYPE.Success && this.Status == Enums.STATUS_MSG_TYPE.Success))
                        {
                            oneOK = true;
                            sb.AppendLine("<li>");
                            sb.AppendLine(subMsg.GetMessageAsHtml());
                            sb.AppendLine("</li>");
                        }
                    }
                    sb.AppendLine("</ul>");
                    if (oneOK)
                    {
                        Literal lit = new Literal();
                        lit.Text = sb.ToString();
                        divMsg.Controls.Add(lit);
                    }
                }
               
                

                
            }

            return CS.General_v3.Util.ControlUtil.RenderControl(divMsg);

        }

        public void FillFromException(Exception ex, CS.General_v3.Enums.STATUS_MSG_TYPE status = Enums.STATUS_MSG_TYPE.Error)
        {
            this.AdditionalInformation = ex.StackTrace;
            this.Message = ex.Message;
            if (ex.InnerException != null)
            {
                OperationResultMsg subMsg = new OperationResultMsg();
                subMsg.FillFromException(ex.InnerException, status);
                this.SubMessages.Add(subMsg);
            }
        }


    }
}
