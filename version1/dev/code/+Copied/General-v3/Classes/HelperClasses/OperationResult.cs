﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Common;
using CS.General_v3.Classes.Exceptions;

namespace CS.General_v3.Classes.HelperClasses
{
    [Serializable]
    public class OperationResult
    {
        public object Tag { get; set; }

        public Enums.STATUS_MSG_TYPE Status
        {
            get
            {
                int curr = (int)Enums.STATUS_MSG_TYPE.Success;
                foreach (var item in this.StatusMessages)
                {
                    int itemStatusValue = (int)item.Status;
                    if (itemStatusValue > curr)
                        curr = itemStatusValue;
                }
                return (Enums.STATUS_MSG_TYPE)curr;
            }
        }
        
        public void SetStatusInfo(Enums.STATUS_MSG_TYPE status, string message , string additionalInformation = null, bool clearExistingMsgs= false)
        {
            if (clearExistingMsgs)
                this.StatusMessages.Clear();
            OperationResultMsg msg = new OperationResultMsg(status, message, additionalInformation: additionalInformation);
            msg.SubMessages.AddRange(this.StatusMessages);
            this.StatusMessages.Clear();
            this.StatusMessages.Add(msg);
        }
        
        public List<OperationResultMsg> StatusMessages { get; private set; }

        

        public void AddException(Exception ex, Enums.STATUS_MSG_TYPE status = Enums.STATUS_MSG_TYPE.Error, string prefix = null)
        {
            if (ex is OperationException)
            {
                OperationException opEx = (OperationException)ex;
                if (!string.IsNullOrEmpty(prefix))
                {
                    foreach (var item in opEx.ResultInfo.StatusMessages)
                    {
                        if (!string.IsNullOrEmpty(item.Message))
                        {
                            item.Message = prefix + ": " + item.Message;
                        }
                    }
                }
                this.AddFromOperationResult(opEx.ResultInfo);
            }
            else
            {
                Exception curr = ex;
                while (curr != null)
                {
                    string msg = curr.Message;
                    if (!string.IsNullOrEmpty(prefix)) msg = prefix + ": " + msg;
                    this.StatusMessages.Add(new OperationResultMsg(status, msg, curr.StackTrace));
                    curr = curr.InnerException;
                }
            }
            
        }

        public OperationResult()
        {
            this.StatusMessages = new List<OperationResultMsg>();
        }

        private List<OperationResultMsg> getListOfAllNonSuccessStatusMsgs()
        {
            List<OperationResultMsg> applicableMsgs = new List<OperationResultMsg>();
            applicableMsgs.AddRange(this.StatusMessages);
            if (this.Status == Enums.STATUS_MSG_TYPE.Error)
            {
                for (int i = 0; i < applicableMsgs.Count; i++)
                {
                    var item = applicableMsgs[i];
                    if (item.Status == Enums.STATUS_MSG_TYPE.Success)
                    {
                        applicableMsgs.RemoveAt(i);
                        i--;
                    }
                }
            }
            return applicableMsgs;
        }

        public string GetMessageAsString()
        {
            StringBuilder sb = new StringBuilder();
            List<OperationResultMsg> applicableMsgs = getListOfAllNonSuccessStatusMsgs();

            if (applicableMsgs.Count > 0)
            {
                

                foreach (var msg in applicableMsgs)
                {
                    sb.AppendLine();
                    sb.AppendLine(msg.GetMessageAsString(0));
                    sb.AppendLine();
                    sb.AppendLine("----------------------------------------------");
                    

                }
            }
            return sb.ToString();
        }

        public string GetMessageAsHtml()
        {

            StringBuilder sb = new StringBuilder();
            List<OperationResultMsg> applicableMsgs = getListOfAllNonSuccessStatusMsgs();
            if (applicableMsgs.Count > 0)
            {
                sb.AppendLine("<ul class=\"status-messages\">");


                foreach (var msg in applicableMsgs)
                {
                    sb.AppendLine("<li>");
                    sb.AppendLine((msg.GetMessageAsHtml()));
                    sb.AppendLine("</li>");
                    
                }
                sb.AppendLine("</ul>");
            }
            return sb.ToString();
            
        }

        public OperationResult(Enums.STATUS_MSG_TYPE status, string msg = null) : this()
        {
            this.StatusMessages.Add( new OperationResultMsg(status, msg));
            
        }
        /// <summary>
        /// Returns whether the operation completed.  This also returns as true if operation only has warnings
        /// </summary>
        public bool IsSuccessful
        {
            get
            {
                return this.Status != Enums.STATUS_MSG_TYPE.Error;
            }
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var msg in this.StatusMessages)
            {
                sb.AppendLine(msg.Message + " - " + msg.AdditionalInformation);
            }

            return this.Status + " <" + sb.ToString() + ">";
            
        }

        public OperationResultMsg GetWorstStatusMsg()
        {
            OperationResultMsg msg = null;
            foreach (var m in this.StatusMessages)
            {
                if (msg == null || ((int)msg.Status < (int)m.Status))
                {
                    msg = m;
                }
            }
            return msg;
        }

        public void AddStatusMsg(Enums.STATUS_MSG_TYPE statusMsgType, string message, string title = null)
        {
            this.StatusMessages.Add( new OperationResultMsg(statusMsgType,message,title));
        }

        public void AddFromOperationResult(OperationResult otherResult)
        {
            if (otherResult != null)
            {
                this.StatusMessages.AddRange(otherResult.StatusMessages);
            }
            
        }
    }
}
