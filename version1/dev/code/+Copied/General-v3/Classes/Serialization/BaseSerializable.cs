﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Serialization
{
    public class BaseSerializable<T> : BaseSerializable
    {
        public void LoadFromQuerystring()
        {
            base.loadFromQuerystring<T>();
        }
        
        public void LoadFromNameValueCollection(NameValueCollection nv)
        {
            base.loadFromNameValueCollection<T>(nv);
        }
        public void SerializeToNameValueCollection(NameValueCollection nv)
        {
            base.serializeToNameValueCollection<T>(nv);
            
        }
    }
    public class BaseSerializable
    {

        protected void loadFromQuerystring<T>()
        {
            loadFromNameValueCollection<T>(CS.General_v3.Util.PageUtil.GetRequestQueryString());
        }
        protected void loadFromNameValueCollection<T>(NameValueCollection nv)
        {
            CS.General_v3.Util.ReflectionUtil.DeserializeObjectFromNameValueCollection(nv, typeof (T), this);
                  
          }
          protected void serializeToNameValueCollection<T>(NameValueCollection nv)
          {
              CS.General_v3.Util.ReflectionUtil.SerializeObjectToNameValueCollection(nv, typeof (T), this);
          }
    }
}
