﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Singletons
{
    public class SingletonException :Exception
    {
        public SingletonException()
            : base()
        {

        }
        public SingletonException(string msg)
            : base(msg)
        {

        }
        public SingletonException(Exception ex)
            : base(ex.Message,ex)
        {

        }
    }
}
