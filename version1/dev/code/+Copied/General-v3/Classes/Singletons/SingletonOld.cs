﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace CS.General_v3.Classes.Singletons
{
    public static class SingletonOld<T>
       where T : class
    {
        static volatile T _instance;
        static object _lock = new object();

        static SingletonOld()
        {
        }

        public static T Instance
        {
            get
            {
                
                if (_instance == null)
                {
                    lock (_lock)
                    {
                        if (_instance == null)
                        {
                            _instance = Util.ReflectionUtil.CreateObjectWithPrivateConstructor<T>();
                            /*
                            ConstructorInfo constructor = null;

                            try
                            {
                                // Binding flags exclude public constructors.
                                constructor = typeof(T).GetConstructor(BindingFlags.Instance |
                                              BindingFlags.NonPublic, null, new Type[0], null);
                            }
                            catch (Exception exception)
                            {
                                throw;
                            }

                            if (constructor == null || constructor.IsAssembly)
                                // Also exclude internal constructors.
                                throw new SingletonException(string.Format("A private or " +
                                      "protected constructor is missing for '{0}'.", typeof(T).Name));

                            _instance = (T)constructor.Invoke(null);*/
                        }
                    }
                }
                return _instance;
            }
        }
    }
}
