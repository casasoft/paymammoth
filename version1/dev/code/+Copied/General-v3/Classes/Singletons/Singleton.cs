﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using CS.General_v3.Classes.Reflection;

namespace CS.General_v3.Classes.Singletons
{
    public static class Singleton
       
    {
        static volatile ConcurrentDictionary<Type, object> _instanceHashTable;
        //static volatile Dictionary<Type, object> _typeLocks;
        static object _lock = new object();

        static Singleton()
        {
            _instanceHashTable = new ConcurrentDictionary<Type, object>();
           // _typeLocks = new Dictionary<Type, object>();
        }

        public static object GetInstance(Type t)
        {
            object result = null;
            if (!_instanceHashTable.TryGetValue(t, out result))
            {
                lock (_lock)
                {
                    if (!_instanceHashTable.ContainsKey(t))
                    {
                        _instanceHashTable[t]  = Util.ReflectionUtil.CreateObjectWithPrivateConstructor(t);
                       
                    }
                }
                result = _instanceHashTable[t];
            }
            return result;
            
        }
        public static T GetInstance<T>()
        {
            return (T)GetInstance(typeof(T));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetInstanceForLastTypeInInheritanceChain<T>(IEnumerable<Assembly> assembliesToCheck)
        {
            Type typeToCreate = typeof(T);
            //Type nonAbstractType = CS.General_v3.Util.ReflectionUtil.GetTypeWhichExtendsFromAndIsNotAbstract(assemblyToCheck, typeof(T));
            Type nonAbstractType = CS.General_v3.Util.ReflectionUtil.GetLastTypeInInheritanceChain(typeToCreate, assembliesToCheck);
            var obj = GetInstance(nonAbstractType);
            T item = default(T);
            try
            {
                item = (T)obj;
            }
            catch (InvalidCastException ex)
            {
                //log error
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Could not cast matched type <" + nonAbstractType  + "> to required type <"  + typeToCreate + ">.  This should never happen! See inner exception");
                InvalidOperationException newEx = new InvalidOperationException(sb.ToString(), ex);
                Error.Reporter.ReportError(Enums.LOG4NET_MSG_TYPE.Error, newEx);

                //if for some reason this happens, reset the cache and retry again
                LastInheritanceChainTypeFinder.Instance.ResetCacheForType(typeToCreate);
                nonAbstractType = CS.General_v3.Util.ReflectionUtil.GetLastTypeInInheritanceChain(typeToCreate, assembliesToCheck);
                obj = GetInstance(nonAbstractType);
                item = (T)obj;
            }

            return item;
            
        }
    }
}
