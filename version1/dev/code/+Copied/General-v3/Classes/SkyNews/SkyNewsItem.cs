﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.SkyNews
{
    public class SkyNewsItem
    {
        public string Title;
        public string Href;
        public string Description;
        public string Image;

    }
}
