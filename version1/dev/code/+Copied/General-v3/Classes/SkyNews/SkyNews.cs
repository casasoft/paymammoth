﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
namespace CS.General_v3.Classes.SkyNews
{
    public class SkyNews
    {
        public List<SkyNewsItem> SkyNewsItems { get; private set; }
        public SkyNews(string feedURL)
        {
            SkyNewsItems = new List<SkyNewsItem>();
            XmlDocument doc = new XmlDocument();
            doc.Load(feedURL);
            XmlNodeList list = doc.SelectNodes("rss/channel/item");
            for (int i = 0; i < list.Count; i++)
            {
                var itemNode = list[i];
                if (itemNode != null)
                {


                    string title = CS.General_v3.Util.XML.GetNodeInnerText(itemNode.SelectSingleNode("title"));
                    string link = CS.General_v3.Util.XML.GetNodeInnerText(itemNode.SelectSingleNode("link"));
                    string description = CS.General_v3.Util.XML.GetNodeInnerText(itemNode.SelectSingleNode("description"));
                    XmlNode xmlEnclosure = itemNode.SelectSingleNode("enclosure");
                    string image = null;
                    if (xmlEnclosure != null)
                    {
                        image = CS.General_v3.Util.XML.GetAttribute(xmlEnclosure, "url");
                    }

                    SkyNewsItems.Add(new SkyNewsItem() { Title = title, Href = link, Description = description, Image = image });
                }
            }


        }

    }
}
