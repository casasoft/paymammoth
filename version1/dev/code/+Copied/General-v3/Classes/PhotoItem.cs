﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes
{
    public class PhotoData
    {
        public string Small { get; set; }
        public string Medium { get; set; }
        public string Large { get; set; }
        public string Caption { get; set; }
        /// <summary>
        /// A general class to hold filenames and caption of a photo
        /// </summary>
        /// <param name="small"></param>
        /// <param name="medium"></param>
        /// <param name="large"></param>
        /// <param name="caption"></param>
        public PhotoData(string small, string medium, string large, string caption)
        {
            this.Small = small;
            this.Medium = medium;
            this.Large = large;
            this.Caption = caption;
        }
    }
}
