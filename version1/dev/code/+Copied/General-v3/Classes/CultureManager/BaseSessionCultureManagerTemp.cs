﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using CS.General_v3.Classes.PorterStemmer;

namespace CS.General_v3.Classes.CultureManager
{
    public class BaseSessionCultureManagerTemp : BaseSessionCultureManager
    {


        public override CultureInfo GetCurrentCultureFormatProvider()
        {
            return System.Threading.Thread.CurrentThread.CurrentCulture;
        }

        public override NumberFormatInfo GetCurrentCurrencyNumberFormatProviderForHTML()
        {
            return GetCurrentCultureFormatProvider().NumberFormat;
        }

        public override NumberFormatInfo GetCurrentCurrencyNumberFormatProviderForUnicode()
        {
            return GetCurrentCultureFormatProvider().NumberFormat;
        }

        public override StemmerInterface GetStemmerForCurrentCulture()
        {
            return new PorterStemmerEnglish();
            
        }

    }
}
