﻿namespace CS.General_v3.Classes.Interfaces.Hierarchy
{
    using System.Collections.Generic;

    public static class IHierarchyExtensions
    {
        

        /// <summary>
        /// Returns whether this category is a child (anywhere in the tree) of parent. A tree of Root > Store > Product, Product is a child of Root + Store.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="matchItselfAsWell">If the parent is the same as this, it matches as well although essentially it is not a child</param>
        /// <returns></returns>
        public static bool IsChildOf(this IHierarchy category, IHierarchy parent, bool matchItselfAsWell = true)
        {
            bool isChild = false;
            if (matchItselfAsWell && parent.ID == category.ID)
            {
                isChild = true;

            }
            if (!isChild)
            {
                var subCategories = parent.GetChildren();
                foreach (var child in subCategories) //first check if this is a child of its direct children
                {
                    if (child.ID == category.ID)
                    {
                        isChild = true;
                        break;
                    }

                }
                if (!isChild)
                {
                    foreach (var child in subCategories) //propagate to its children
                    {
                        isChild = IsChildOf(category, child);
                        if (isChild)
                            break;

                    }
                }
            }
            return isChild;
        }
        /// <summary>
        /// Returns whether this category contains another category as it's child. A tree of Root > Store > Produc, Store would have a child 'Product'.
        /// </summary>
        /// <param name="child"></param>
        /// <returns></returns>
        public static bool ContainsChildCategory(this IHierarchy category, IHierarchy child)
        {
            return IsChildOf(child,category);


        }
        /// <summary>
        /// Returns whether this category is a child (anywhere in the tree) of parent. A tree of Root > Store > Product, Product is a child of Root + Store.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="matchItselfAsWell">If the parent is the same as this, it matches as well although essentially it is not a child</param>
        /// <returns></returns>
        public static bool IsParentOf(this IHierarchy category, IHierarchy childToCheck, bool matchItselfAsWell = true)
        { 
            return IsChildOf(childToCheck, category, matchItselfAsWell);


        }

    }
    public interface IHierarchy
    {
        long ID { get; }
        string Title { get;  }
        IEnumerable<IHierarchy> GetChildren();
        IEnumerable<IHierarchy> GetParents();
        IHierarchy GetMainParent();
        bool Visible { get; }
    }
}
