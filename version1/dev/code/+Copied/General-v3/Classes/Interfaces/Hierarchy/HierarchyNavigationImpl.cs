﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Interfaces.Hierarchy
{
    public class HierarchyNavigationImpl : IHierarchyNavigation
    {
        public HierarchyNavigationImpl()
        {
            this.Children = new List<IHierarchyNavigation>();
            this.Parents = new List<IHierarchyNavigation>();
            this.Visible = true;
        }

        public void AddChild(HierarchyNavigationImpl child)
        {
            this.Children.Add(child);
        }

        public string Href
        {
            get;
            set;
        }

        public Enums.HREF_TARGET HrefTarget
        {
            get;
            set;
        }

        public bool Selected
        {
            get;
            set;
        }

        public long ID
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public List<IHierarchyNavigation> Children { get; set; }
        public List<IHierarchyNavigation> Parents { get; set; }

        public List<IHierarchyNavigation> GetChildren()
        {
            return this.Children;
        }

        public List<IHierarchyNavigation> GetParents()
        {
            return this.Parents;
           
        }

        public bool Visible
        {
            get;
            set;
        }


        IEnumerable<IHierarchy> IHierarchy.GetChildren()
        {
            return this.GetChildren();
        }

        IEnumerable<IHierarchy> IHierarchy.GetParents()
        {
            return this.GetParents();
        }

        IEnumerable<IHierarchyNavigation> IHierarchyNavigation.GetChildren()
        {
            return this.GetChildren();
        }

        IEnumerable<IHierarchyNavigation> IHierarchyNavigation.GetParents()
        {
            return this.GetParents();
        }


        IHierarchyNavigation IHierarchyNavigation.GetMainParent()
        {
            return GetParents().FirstOrDefault();
        }


        IHierarchy IHierarchy.GetMainParent()
        {
            return GetParents().FirstOrDefault();
        }


        public bool OmitChildrenInNavigation { get; set; }

        public bool ConsiderAsRootNode { get; set; }
    }
}
