﻿namespace CS.General_v3.Classes.Interfaces.Hierarchy
{
    using System.Collections.Generic;
    using System.Linq;
    using CS.General_v3.Classes.Factories;
    using CS.General_v3.Classes.HelperClasses;

    public static class IHierarchyEditableExtensions
    {

        public static void CheckParents(this  IHierarchyEditable hierarchy, OperationResult result)
        {


            var parentLinks = hierarchy.GetParents().ToList();
            for (int i = 0; i < parentLinks.Count; i++)
            {

                var parent = parentLinks[i];
                
                
                if (parent != null && hierarchy.IsParentOf(parent))
                {
                    //something is wrong, a parent can never be also a child!
                    result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Warning, "Cannot add <" + parent.ToString() + "> as a parent when this category is it's parent as well!");
                    hierarchy.RemoveParent(parent);
                }
            }
        }
        
        public static void CheckChildren(this  IHierarchyEditable hierarchy, OperationResult result)
        {



            var childLinks = hierarchy.GetChildren().ToList();
            for (int i = 0; i < childLinks.Count; i++)
            {

                var child = childLinks[i];
                
                //var childFrontend = CategoryBaseFrontend.LoadFrontendFromDataItem(childLink.ChildCategory);

                if (child != null && hierarchy.IsChildOf(child))
                {
                    //something is wrong, a parent can never be also a child!
                    result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Warning, "Cannot add <" + child.ToString() + "> as a child when this category is it's child as well!");
                    hierarchy.RemoveChild(child);

                }
            }
        }
    }
    public interface IHierarchyEditable : IHierarchy
    {
        void AddChild(IHierarchy hierarchy);
        void AddParent(IHierarchy hierarchy);
        void RemoveChild(IHierarchy child);
        void RemoveParent(IHierarchy parent);
        new IEnumerable<IHierarchyEditable> GetChildren();
        new IEnumerable<IHierarchyEditable> GetParents();
    }
}
