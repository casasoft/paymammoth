﻿namespace CS.General_v3.Classes.Interfaces.Hierarchy
{
    using System.Collections.Generic;
    using CS.General_v3;

    public interface IHierarchyNavigation : IHierarchy
    {
        string Href { get;  }
        Enums.HREF_TARGET HrefTarget { get; }
        bool Selected { get; }
        new IEnumerable<IHierarchyNavigation> GetChildren();
        new IEnumerable<IHierarchyNavigation> GetParents();
        new IHierarchyNavigation GetMainParent();
        bool OmitChildrenInNavigation { get; }
        bool ConsiderAsRootNode { get; }
    }
}
