﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Interfaces.BlogPosts
{
    public interface ICommentData
    {
        string FullName { get; set; }
        string Email { get; set; }
        string Comment { get; set; }
        DateTime PostedOn { get; set; }
        string PostedByIP { get; set; }
    }
}
