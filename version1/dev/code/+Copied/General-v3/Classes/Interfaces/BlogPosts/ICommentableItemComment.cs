﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Interfaces.BlogPosts
{
    public interface ICommentableItemComment
    {
        string Author { get; }
        DateTime PostedOn { get; }
        string Comment { get; }

        IEnumerable<ICommentableItemComment> GetReplies();
        Enums.COMMENT_POST_RESULT PostReply(ICommentData data, bool autoSave = true);
    }
}
