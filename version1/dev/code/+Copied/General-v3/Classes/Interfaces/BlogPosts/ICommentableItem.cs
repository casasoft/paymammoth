﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Interfaces.BlogPosts
{
    public interface ICommentableItem
    {
        CS.General_v3.Enums.COMMENT_POST_RESULT PostComment(ICommentData data, bool autoSave = true);
    }
}
