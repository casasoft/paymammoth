﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Interfaces.Gallery
{
    public interface IMediaItemData
    {
        string ThumbnailImageUrl { get; }
        string NormalImageUrl { get; }
        string LargeImageUrl { get; }
        string Caption { get; }
        string VideoLink { get; }
    }
}
