﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Interfaces.ImageItem
{
    public class ImageItemImpl : IImageItem
    {
        public string LargeImageURL { get; set; }
        public string NormalImageURL { get; set; }
        public string ThumbnailImageURL { get; set; }
        public string GetLargeImageURL()
        {
            return LargeImageURL;
        }

        public string GetNormalImageURL()
        {
            return NormalImageURL;
        }

        public string GetThumbnailImageURL()
        {
            return ThumbnailImageURL;
        }
        public string GetTitle()
        {
            return Title;
        }
        public string GetCaption()
        {
            return Caption;
        }
        public string Title
        {
            get;
            set;
        }

        public string Caption
        {
            get;
            set;
        }


        public string GetAlternateText()
        {
            return Title + " - " + Caption;
        }
    }
}
