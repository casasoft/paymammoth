﻿namespace CS.General_v3.Classes.Interfaces.ImageItem
{
    public interface IImageItem
    {
        string GetLargeImageURL();
        string GetNormalImageURL();
        string GetThumbnailImageURL();
        string GetTitle();
        string GetCaption();
        string GetAlternateText();
    }
}
