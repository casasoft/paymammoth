﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.Routing;
using CS.General_v3.Extensions;

namespace CS.General_v3.Classes.URL.URLParser.v1
{
    public class URLParser : URLParser_Base
    {

        public static string PARAM_PAGENO = "pg";
        public static string PARAM_SHOWAMT = "show";
        public static string PARAM_SORTBY = "sort";
        public ListingParams ListingParameters { get; set; }
        private string _customURL = null;

        public URLParser(string id, string customURL = null)
            : base(id)
        {
            _customURL = customURL; 
            ListingParameters = new ListingParams();
            this.AddParameterRouteData(SearchRoute.ROUTE_PARAM_KEYWORDS, null, true);
            this.AddParameterRouteData(SearchRoute.ROUTE_PARAM_CATEGORIES, null, true);
            this.AddParameterRouteData(SearchRoute.ROUTE_PARAM_BRANDS, null, true);           
            this.AddParameterRouteData(SearchRoute.ROUTE_PARAM_CATEGORY_IDS, null, true);
            this.AddParameterRouteData(SearchRoute.ROUTE_PARAM_BRAND_IDS, null, true);
            initValues();
            //this.AddParameterQueryString(PARAM_PAGENO, "1", false, false);
            //this.AddParameterQueryString(PARAM_SHOWAMT, null, true, true);
            //this.AddParameterQueryString(PARAM_SORTBY, null, true, true);
            //this.AddParameterQueryString(PARAM_KEYWORDS, null, false, false);
            //this.AddParameterQueryString(PARAM_CATEGORYID, null, false, false);
           

            this.PagingInfo = new URLParser_PagingInfo(this);
        }
        private void initValues()
        {
            //this.ListingParameters.Keywords = this.GetParameter(SearchRoute.ROUTE_PARAM_KEYWORDS).GetValue();
            //this.ListingParameters.Brands = this.GetParameter(SearchRoute.ROUTE_PARAM_BRANDS).GetValueAsListOfString();
            //this.ListingParameters.Categories = this.GetParameter(SearchRoute.ROUTE_PARAM_CATEGORIES).GetValueAsListOfString();
            //this.ListingParameters.CategoryIDs = this.GetParameter(SearchRoute.ROUTE_PARAM_CATEGORY_IDS).GetValueAsListOfInteger();
            //this.ListingParameters.BrandIDs = this.GetParameter(SearchRoute.ROUTE_PARAM_BRAND_IDS).GetValueAsListOfInteger();
        }

        //public ListingParams GetURLParserValues()
        //{
        //    updateParameterValues();
        //    return ListingParameters;
        //}

        protected override void updateURLParameterValues()
        {
            //if (ListingParameters.Keywords.IsNotNullOrEmpty())
            //{
            //    this.GetParameter(SearchRoute.ROUTE_PARAM_KEYWORDS).SetValue(ListingParameters.Keywords);
            //}
            //if (ListingParameters.BrandIDs != null && ListingParameters.BrandIDs.Count() > 0)
            //{
            //    this.GetParameter(SearchRoute.ROUTE_PARAM_BRAND_IDS).SetValue(ListingParameters.BrandIDs);
            //}
            //if (ListingParameters.CategoryIDs != null && ListingParameters.CategoryIDs.Count() > 0)
            //{
            //    this.GetParameter(SearchRoute.ROUTE_PARAM_CATEGORY_IDS).SetValue(ListingParameters.CategoryIDs);
            //}
            //if (ListingParameters.Categories.IsNotNullOrEmpty())
            //{
            //    this.GetParameter(SearchRoute.ROUTE_PARAM_CATEGORIES).SetValue(ListingParameters.Categories);
            //}
            //if (ListingParameters.Brands.IsNotNullOrEmpty())
            //{
            //    this.GetParameter(SearchRoute.ROUTE_PARAM_BRANDS).SetValue(ListingParameters.Brands);
            //}
        }

        public List<string> GetKeywordsAsList()
        {
            List<string> list = new List<string>();
            string s = this.ListingParameters.Keywords ?? "";
            if (!string.IsNullOrEmpty(s))
            {
                list.AddRange(s.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries));
            }
            return list;
        }


        protected override string getRouteName()
        {
            //if (this.ListingParameters.BrandIDs.IsNotNullOrEmpty() && this.ListingParameters.CategoryIDs.IsNotNullOrEmpty())
            //{
            //    return SearchRoute.ROUTE_NAME_BRANDS_AND_CATEGORIES;
            //}
            //else if (this.ListingParameters.BrandIDs.IsNotNullOrEmpty() && this.ListingParameters.Keywords.IsNotNullOrEmpty())
            //{
            //    return SearchRoute.ROUTE_NAME_KEYWORDS_AND_BRANDS;
            //}
            //else if (this.ListingParameters.CategoryIDs.IsNotNullOrEmpty() && this.ListingParameters.Keywords.IsNotNullOrEmpty())
            //{
            //    return SearchRoute.ROUTE_NAME_KEYWORDS_AND_CATEGORIES;
            //}
            //else if (this.ListingParameters.Keywords.IsNotNullOrEmpty())
            //{
            //    return SearchRoute.ROUTE_NAME_KEYWORDS;
            //}
            //else if (this.ListingParameters.BrandIDs.IsNotNullOrEmpty())
            //{
            //    return SearchRoute.ROUTE_NAME_BRANDS;
            //}
            //else if (this.ListingParameters.CategoryIDs.IsNotNullOrEmpty())
            //{
            //    return SearchRoute.ROUTE_NAME_CATEGORIES;
            //}
            return null;
        }

        protected override string getPageURL()
        {
            if (_customURL.IsNotNullOrEmpty())
            {
                return _customURL;
            }
            else
            {
                return SearchRoute.SEARCH_PAGE_URL;
            }
        }

        public URLParser_PagingInfo PagingInfo { get; private set; }

    }
}
