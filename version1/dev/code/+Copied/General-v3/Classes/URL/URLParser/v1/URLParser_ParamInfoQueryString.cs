﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.URL.URLParser.v1
{
    public class URLParser_ParamInfoQueryString : URLParserParamInfoBase
    {
         public bool UsesSession { get; set; }
        public bool UsesCookies { get; set; }

        private string _value;
        private string _key;
        private string _sessionKey;
        public URLParser_ParamInfoQueryString(URLParser_Base searchParams, string identifier, bool usesSession = false, bool usesCookies = false)
            : base(searchParams, identifier)
        {
            this.UsesCookies = usesCookies;
            this.UsesSession = usesSession;
            _key = _searchParams.getKey(this.Identifier);
            _sessionKey = _searchParams.getSessionCookieKey(this.Identifier);
         //   initValue();
        }
        private string getKey()
        {
            if (_key == null)
            {
                _key = _searchParams.getKey(this.Identifier);
            }
            return _key;
        }
        private bool _valueInit = false;
        private void initValue(/*bool loadFromQS*/)
        {
            _valueInit = true;
            string s = null;
            //if (loadFromQS)
            //{
                s = (string)_searchParams.Url[_key];
                //To store in session / cookies if need be
                if (s != null)
                {
                    //Update value since it is in QS
                    this.setValue(s);
                }
           // }
            if (string.IsNullOrEmpty(s) && UsesSession)
            {
                s = (string)CS.General_v3.Util.PageUtil.GetSessionObject(_sessionKey);
            }
            if (UsesCookies && string.IsNullOrEmpty(s))
            {
                s = CS.General_v3.Util.PageUtil.GetCookie(_sessionKey);
            }
            _value = s;
        }
      

        protected override string getValue(string defaultValue = null)
        {
            if (!_valueInit)
            {
                initValue();
            }

            return _value;
           /* string key = _searchParams.getKey(this.Identifier);
            string s = (string)_searchParams.Url[key];
            string sessionKey = _searchParams.getSessionCookieKey(this.Identifier);

            if (string.IsNullOrEmpty(s) && UsesSession)
            {
                s = (string)CS.General_v3.Util.PageUtil.GetSessionObject(sessionKey);
            }

            if (UsesCookies && string.IsNullOrEmpty(s))
            {
                s = CS.General_v3.Util.PageUtil.GetCookie(sessionKey);
            }
            return s;*/
        }

        protected override void setValue(string value)
        {
            _value = value;
            /*string key = _searchParams.getKey(this.Identifier);
            string sessionKey = _searchParams.getSessionCookieKey(this.Identifier);*/
            _searchParams.Url[_key] = value;

            if (UsesSession)
            {
                CS.General_v3.Util.PageUtil.SetSessionObject(_sessionKey, value);
            }
            if (UsesCookies)
            {
                CS.General_v3.Util.PageUtil.SetCookie(_sessionKey, value);
            }
        }
    }
}
