﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.URL;
using CS.General_v3.Controls.WebControls.Specialized.Paging;

namespace CS.General_v3.Classes.URL.URLParser.v1
{
    public abstract class URLParser_Base
    {
        public static string PARAM_PAGENO = "pg";
        public static string PARAM_SHOWAMT = "show";
        public static string PARAM_SORTBY = "sort";

        private Dictionary<string, URLParserParamInfoBase> _paramInfos = null;

        public string DefaultRouteName { get; set; }
        public URLClass Url { get; private set; }
        public string ID { get;set; }

        private bool _loadCurrentURLIfNull;
       
        internal protected string getKey(string identifier)
        {
            
            string key = identifier.ToLower();
            if (!string.IsNullOrEmpty(this.ID))
                key += "_" + this.ID.ToLower();
            return key;
        }
        internal protected string getSessionCookieKey(string identifier)
        {
            string key = getKey(identifier);
            key = "searchParams_" + key;
            return key;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">An identifier which will be used to get the session keys</param>
        /// <param name="urlToLoad"></param>
        /// <param name="loadCurrentUrlIfNull"></param>
        public URLParser_Base(string id, string urlToLoad = null, bool loadCurrentUrlIfNull = true)
        {
            _loadCurrentURLIfNull = loadCurrentUrlIfNull;
            this.ID = id ?? "";
            if (urlToLoad == null && loadCurrentUrlIfNull)
            {
                urlToLoad = CS.General_v3.Util.PageUtil.GetCurrentUrlLocation().GetURL(fullyQualified:false);
            }

            this.Url = new CS.General_v3.Classes.URL.URLClass(urlToLoad, _loadCurrentURLIfNull);
            this._paramInfos = new Dictionary<string, URLParserParamInfoBase>();

        }
        private bool hasGotRoutingParameters()
        {
            foreach(var key in _paramInfos.Keys)
            {
                if (_paramInfos[key] is URLParser_ParamInfoRouteData)
                {
                    return true;
                }
            }
            return false;
        }
        private void addParameter(URLParserParamInfoBase paramInfo, string identifier)
        {
            string key = getKey(identifier);
            if (_paramInfos.ContainsKey(key))
                _paramInfos.Remove(key);
            _paramInfos.Add(key, paramInfo);
        }
        public URLParser_ParamInfoQueryString AddParameterQueryString(string identifier, string defaultValue, bool usesSession = false, bool usesCookies = false)
        {
            URLParser_ParamInfoQueryString paramInfo = new URLParser_ParamInfoQueryString(this, identifier, usesSession, usesCookies);
            paramInfo.DefaultValue = defaultValue;
            addParameter(paramInfo, identifier);
            return paramInfo;
        }
        public void RemoveParameter(string identifier)
        {
            string key = getKey(identifier);
            if (_paramInfos.ContainsKey(key))
                _paramInfos.Remove(key);
        }
        public URLParser_ParamInfoRouteData AddParameterRouteData(string identifier, string defaultValue, bool decodeValuesFromRouteData = false)
        {
            URLParser_ParamInfoRouteData paramInfo = new URLParser_ParamInfoRouteData(this, identifier, decodeValuesFromRouteData);
            paramInfo.DefaultValue = defaultValue;
            addParameter(paramInfo, identifier);
            return paramInfo;
        }
        public URLParserParamInfoBase GetParameter(string identifier)
        {
            URLParserParamInfoBase result = null;
            string key = getKey(identifier);
            if (_paramInfos.ContainsKey(key))
                result =_paramInfos[key];
            else
                throw new InvalidOperationException("URLParser_Base:: Parameter with identifier <" + identifier + "> does not exist");
            return result;
        }

        public virtual string GetQuerystring()
        {
            return Url.GetQueryString();
        }

        protected abstract string getRouteName();
        protected abstract string getPageURL();

        /// <summary>
        /// Override this for custom RouteURL params
        /// </summary>
        /// <param name="fullyQualifiedUrl"></param>
        /// <returns></returns>
        protected string getRouteURL(bool fullyQualifiedUrl = true)
        {
            Routing.MyRouteValueDictionary values = new Routing.MyRouteValueDictionary();
            foreach( string key in _paramInfos.Keys) {
                URLParserParamInfoBase param = _paramInfos[key];
                if (param is URLParser_ParamInfoRouteData) {
                    string value = param.GetValue();
                    values.Add(param.Identifier, value);
                }
            }

            string url = CS.General_v3.Util.RoutingUtil.GetRouteUrl(getRouteName(), values, fullyQualifiedUrl);
            return url;
        }

        /// <summary>
        /// Update the values of the parameters with the ones within the parameters as a class
        /// </summary>
        protected abstract void updateURLParameterValues();


        public virtual string GetUrl(bool appendQS = true)
        {
            updateURLParameterValues();
            string baseURL = null;
            if (hasGotRoutingParameters())
            {
                baseURL = getRouteURL();
            }
            else
            {
                baseURL = getPageURL();
            }
            Url.ParseURL(baseURL, parseQuerystringVariables: false);
            return Url.GetURL(fullyQualified: true, appendQueryString: appendQS);
        }

        //public virtual string GetUrl(string customURL = null, bool fullyQualifiedUrl = true)
        //{

        //    if (hasGotRoutingParameters())
        //    {
        //        if (!string.IsNullOrEmpty(customURL))
        //        {
        //            throw new Exception("If this SearchParams is using Routing (" + DefaultRouteName + "), then you can't specify customURL (" + customURL + ")");
        //        }
        //        customURL = getRouteURL(fullyQualifiedUrl);
        //    }
        //    return Url.GetURL(fullyQualified: fullyQualifiedUrl, url: customURL);
        //}

        public virtual void Redirect(bool appendQS = true)
        {
            string url = GetUrl(appendQS);
            CS.General_v3.Util.PageUtil.RedirectPage(url);

        }


    }
}
