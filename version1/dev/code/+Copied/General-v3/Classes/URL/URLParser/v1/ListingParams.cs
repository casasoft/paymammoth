﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.URL.URLParser.v1
{
    public class ListingParams
    {
        public string Keywords { get; set; }
        public List<string> Brands { get; set; }
        public List<string> Categories { get; set; }
    }
}
