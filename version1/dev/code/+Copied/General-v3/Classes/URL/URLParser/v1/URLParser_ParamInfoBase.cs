﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.URL.URLParser.v1
{
    public abstract class URLParserParamInfoBase
    {   
        public string Identifier { get; set; }
        protected URLParser_Base _searchParams = null;

        public object DefaultValue { get; set; }

        public URLParserParamInfoBase(URLParser_Base searchParams, string identifier)
        {
            this._searchParams = searchParams;
            this.Identifier = identifier;

        }

        protected abstract string getValue(string defaultValue = null);
        public string GetValue()
        {
            return getValue(this.DefaultValue == null ? null : CS.General_v3.Util.Other.ConvertBasicDataTypeToString(this.DefaultValue));
        }
       
        public List<string> GetValueAsListOfString(string delimeter = ",")
        {
            List<string> list = new List<string>();
            string s = GetValue() ?? "";
            string[] tokens = s.Split(new string[] { delimeter },  StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < tokens.Length; i++)
            {
                string t = tokens[i];
                if (!string.IsNullOrEmpty(t))
                {
                    list.Add(t);
                }

            }
            return list;
        }
        public List<long> GetValueAsListOfLong(string delimeter = ",")
        {
            List<long> list = new List<long>();
            IList<string> strList = GetValueAsListOfString(delimeter);
            for (int i = 0; i < strList.Count; i++)
            {
                string s = strList[i];
                long num = 0;
                if (Int64.TryParse(s, out num))
                {
                    list.Add(num);
                }

            }
            return list;
        }
        public List<int> GetValueAsListOfInteger(string delimeter = ",")
        {
            return GetValueAsListOfLong(delimeter).ConvertAll<int>(item => (int)item);
        }
        public List<EnumType> GetValueAsListOfEnums<EnumType>(string delimeter = ",", bool convertEnumValuesForRouteData = false)

            where EnumType : struct, IConvertible
        {
            
            List<EnumType> list = new List<EnumType>();
            var strList = GetValueAsListOfString(delimeter);
            for (int i = 0; i < strList.Count; i++)
            {
                string sVal = strList[i];
                EnumType? enumValue = CS.General_v3.Util.EnumUtils.EnumValueNullableOf<EnumType>(sVal, convertEnumValuesForRouteData);
                if (enumValue.HasValue)
                {
                    list.Add(enumValue.Value);
                }
                
            }
            return list;
        }

        public double? GetValueAsDouble()
        {
            double? num = null;
            double tmp =0;
            string s = GetValue();
            if (double.TryParse(s, out tmp))
                num = tmp;
            return num;
        }
        public int? GetValueAsInt()
        {
            double? d = GetValueAsDouble();
            return (d.HasValue ? (int?)d.Value : null);
            
        }
        public long? GetValueAsLong()
        {
            double? d = GetValueAsDouble();
            return (d.HasValue ? (long?)d.Value : null);

        }
        public bool? GetValueAsBool()
        {
            string s =GetValue();
            bool? b = CS.General_v3.Util.Other.TextToBoolNullable(s);
            return b;
        }
        public DateTime? GetValueAsDateTimeNullable()
        {
            string s = GetValue();
            return CS.General_v3.Util.Other.ConvertStringToBasicDataType<DateTime?>(s);

        }
        protected abstract void setValue(string value);
        /*{
            string key = _searchParams.getKey(this.Identifier);
            string sessionKey = _searchParams.getSessionCookieKey(this.Identifier);
            _searchParams.Url[key] = value;
            
            if (UsesSession)
            {
                CS.General_v3.Util.PageUtil.SetSessionObject(sessionKey, value);
            }
            if (UsesCookies)
            {
                CS.General_v3.Util.PageUtil.SetCookie(sessionKey, value);
            }
        }*/
        public void SetValue(string value)
        {
            this.setValue( value);
        }
        public void SetValue(int? value)
        {
            this.setValue( (value != null ? value.Value.ToString() : null));
        }
        public void SetValue(Enum value)
        {
            int? iVal = null;
            if (value != null)
                iVal = (int) (object)value;

            this.SetValue(iVal);
        }
        public void SetValue(long? value)
        {
            this.setValue((value != null ? CS.General_v3.Util.Other.ConvertBasicDataTypeToString(value.Value) : null));
        }
        public void SetValue(double? value)
        {
            this.setValue((value != null ? CS.General_v3.Util.Other.ConvertBasicDataTypeToString(value.Value) : null));
        }
        public void SetValue(DateTime? value)
        {
            
            this.setValue((value != null ? CS.General_v3.Util.Other.ConvertBasicDataTypeToString(value.Value) : null));
        }
        public void SetValue(bool? value)
        {
            this.setValue((value != null ? CS.General_v3.Util.Other.ConvertBasicDataTypeToString(value.Value) : null));
        }
        public void SetValue(IEnumerable<string> value, string delimeter = ",")
        {
            this.setValue(value, delimeter);
        }
        public void SetValue(IEnumerable<int?> value, string delimeter = ",")
        {
            this.setValue(value, delimeter);
        }
        public void SetValue(IEnumerable<int> value, string delimeter = ",")
        {
            this.setValue(value, delimeter);
        }
        public void SetValue(IEnumerable<long> value, string delimeter = ",")
        {
            this.setValue(value, delimeter);
        }
        public void SetValue(IEnumerable<double?> value, string delimeter = ",")
        {
            this.setValue(value, delimeter);
        }
        public void SetValue(IEnumerable<DateTime?> value, string delimeter = ",")
        {
            this.setValue(value, delimeter);
        }
        public void SetValue(IEnumerable<bool?> value, string delimeter = ",")
        {
            this.setValue(value, delimeter);
        }
        public void SetValueFromListOfEnumForRouteData<EnumType>(IEnumerable<EnumType> value, string delimeter = ",")
            where EnumType : struct
        {

            this.setValue(value, delimeter, true, true, true);
        }
        public void SetValueFromListOfEnum<EnumType>(IEnumerable<EnumType> value, string delimeter = ",", bool useStringValue = false, bool toLowerCase = false) 
            where EnumType : struct
        {

            this.setValue(value, delimeter);
        }
        public void SetValue(IEnumerable<Enum> value, string delimeter = ",", bool useStringValue = false, bool toLowerCase = false)
        {
            
            this.setValue(value, delimeter);
        }
        private void setValue(IEnumerable value, string delimeter = ",", bool useStringValue = false, bool toLowerCase = false, bool forRouteData = false)
        {
            StringBuilder sb = new StringBuilder();
            var enumerator = value.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (sb.Length >0) sb.Append(delimeter);
                var curr = enumerator.Current;
                if (curr != null)
                {
                    string s = "";

                    if (curr is Enum)
                    {
                        if (forRouteData)
                        {
                            s = CS.General_v3.Util.EnumUtils.StringValueOf((Enum)curr);
                            s = CS.General_v3.Util.RoutingUtil.ConvertStringForRouteUrl(s);
                        }
                        else
                        {
                            s = ((int) curr).ToString();
                        }
                    }
                    else
                        s = CS.General_v3.Util.Other.ConvertBasicDataTypeToString(curr);
                    if (toLowerCase && s != null)
                    {
                        s = s.ToLower();
                    }
                    sb.Append(s);
                }
            }
            

            this.setValue(sb.ToString());
        }

    }
}
