﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using CS.General_v3.Util;
using System.Text.RegularExpressions;

namespace CS.General_v3.Classes.URL
{

    public class URLClass
    {
        public void AddQuerystringParamsFromNameValueColl(NameValueCollection nv)
        {
            if (nv != null)
            {

                for (int i = 0; i < nv.AllKeys.Length; i++)
                {
                    string key = nv.AllKeys[i];
                    this[key] = nv[key];
                }
            }

        }
        /// <summary>
        /// scheme://username:password@domain:port/path?query_string#fragment_id
        /// </summary>
        public int? Port { get; set; }
        /// <summary>
        /// scheme://username:password@domain:port/path?query_string#fragment_id
        /// </summary>
        public bool UseSsl { get; set; }
       
        /// <summary>
        /// scheme://username:password@domain:port/path?query_string#fragment_id
        /// </summary>
        public string Domain { get; set; }
        private string _path;
        /// <summary>
        /// scheme://username:password@domain:port/path?query_string#fragment_id
        /// </summary>
        public string Path
        {
            get
            {
                return _path;
            }
            set {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    URLClass url = new URLClass(value);
                    this._path = url.Path;
                }
                else
                {
                    _path = value;
                }
            }
        }

            /// <summary>
        /// scheme://username:password@domain:port/path?query_string#fragment_id
        /// </summary>
        public string FragmentID { get; set; }

        public void Remove(params string[] keys)
        {
            if (keys != null)
            {
                
                for (int i = 0; i < keys.Length; i++)
                {
                    string key = keys[i];
                    QS.Remove(key);
                    
                }
            }

        }
        /// <summary>
        /// Transfers the processing to the same page
        /// </summary>
        public static void TransferToSamePage()
        {
            URLClass url = new URLClass();
            url.TransferTo();
        }
        /// <summary>
        /// Redirects the processing to the same page
        /// </summary>
        public static void RedirectToSamePage()
        {
            URLClass url = new URLClass();
            url.RedirectTo();
        }
        public CS.General_v3.URL.QueryString QS { get; set; }

        /*private string _pageURL = "";


        public string PageURL
        {
            get
            {
                return _pageURL;
            }
            set
            {
                _pageURL = value;
                int qIndex = _pageURL.IndexOf("?");
                if (qIndex != -1)
                {
                    _pageURL = _pageURL.Substring(0, qIndex);
                }
                _pageURL = UrlUtil.RemovePortFromUrl(_pageURL);

                
            }
        }
        */
        private void init()
        {

            this.QS = new General_v3.URL.QueryString();
        }

        /// <summary>
        /// Creates URL from current Page URL
        /// </summary>
        public URLClass()
        {
            init();
            initFromCurrentURLQueryString();
        }

       

        /// <summary>
        /// Initializes the QS from the current URL querystring
        /// </summary>
        private void initFromCurrentURLQueryString()
        {
            string url = CS.General_v3.Util.PageUtil.GetUrlFromCurrentRequest();
            this.ParseURL(url);
            /*QS = new CS.General_v3.URL.QueryString();
            var reqQS = CS.General_v3.Util.PageUtil.GetRequestQueryString();
            for (int i = 0; i < reqQS.Count; i++)
            {
                QS.Add(reqQS.Keys[i], reqQS[i]);
            }
            if (System.Web.HttpContext.Current != null)
            {
                _pageURL = System.Web.HttpContext.Current.Request.Path;
            }*/
        }

        private void parseQuerystring(string querystring)
        {
            //if (!string.IsNullOrWhiteSpace(qs))
           // {
           //     int QMarkIndex = qs.IndexOf("?");
              //  string querystring = qs.Substring(QMarkIndex+1);

                
                
                //Contains queryString
                string[] keyValues = querystring.Split('&');
                for (int i = 0; i < keyValues.Length; i++)
                {
                    string[] data = keyValues[i].Split('=');
                    if (data.Length >= 2)
                    {
                        string key = data[0];

                        string value = CS.General_v3.Util.PageUtil.UrlDecode(data[1]);

                        QS.Add(key, value);
                    }
                }
                
            
          //  }
        }





        /// <summary>
        /// Updates the url, without changing any querystring variables
        /// </summary>
        /// <param name="url"></param>
        public void SetUrl(string url)
        {

            this.ParseURL(url, parseQuerystringVariables: false);
        }
        public void ParseURL(string url, bool loadCurrentURLIfNull = false, bool parseQuerystringVariables = true)
        {
            if (!string.IsNullOrWhiteSpace(url))
            {
                if (url == null && loadCurrentURLIfNull)
                {
                    url = PageUtil.GetCurrentUrlLocation().GetURL(fullyQualified: false);
                }

                Regex r = new Regex(@"(((.*?)://)?(.*?)(:(.*?))?)/(.*?)\?(.*?)#(.*?)$", RegexOptions.Singleline | RegexOptions.Multiline | RegexOptions.IgnoreCase);
                if (url.Contains("//") || url.Contains(":"))
                {
                    //Contains Scheme or port
                    url = url.Replace("//", "$$");
                    if (!url.Contains("/"))
                    {
                        //NO slash, so add it at end
                        url += "/";
                    }
                    url = url.Replace("$$", "//");
                }
                else if (!url.Contains("/"))
                {
                    //Not even one /, e.g. text.aspx
                    url = "/" + url;
                }

                if (url.IndexOf("?") == -1) url += "?";
                if (url.IndexOf("#") == -1) url += "#";


                Match match = r.Match(url);
                if (match.Success)
                {
                    string port, queryString, scheme;
                    scheme = match.Groups[3].Value;

                    //domain = mathc.
                    this.Domain = match.Groups[4].Value;
                    //po
                    port = match.Groups[6].Value;
                    int nPort = 0;
                    if (int.TryParse(port, out nPort))
                    {
                        this.Port = nPort;
                    }
                    _path = match.Groups[7].Value;
                    this.UseSsl = scheme != null && scheme.ToLower().Contains("https");
                    queryString = match.Groups[8].Value;
                    this.FragmentID = match.Groups[9].Value;

                    parseQuerystring(queryString);
                }
            }

        }
        /*public void ParseURL3(string Url, bool loadCurrentURLIfNull = false, bool parseQuerystringVariables = true)
        {
            if (Url == null && loadCurrentURLIfNull)
            {
                Url = PageUtil.GetCurrentUrlLocation().GetURL(fullyQualified: false);
            }
            
            if (!String.IsNullOrEmpty(Url))
            {
                
                int QMarkIndex = Url.IndexOf("?");
                if (QMarkIndex != -1)
                {
                    PageURL = Url.Substring(0, QMarkIndex);
                    if (parseQuerystringVariables)
                    {
                        QS.Clear();
                        //QS = new CS.General_v3.URL.QueryString();
                        parseQuerystring(Url);
                    }

                }
                else
                {
                    PageURL = Url;
                }
                
            }
            else
            {
                //_pageURL = System.Web.HttpContext.Current.Request.Path;
            }
            this.Port = CS.General_v3.Util.UrlUtil.ParsePortFromUrl(Url);
            this.UseSsl = CS.General_v3.Util.UrlUtil.CheckWhetherUrlIsSecure(Url);
        }
        */
        /// <summary>
        /// Creates URL from passed URL.  If left empty or null, an empty Querystring is created with your current page URL
        /// </summary>
        /// <param name="URL"></param>
        public URLClass(string URL, bool loadCurrentURLIfNull = true)
        {
            init();
            ParseURL(URL, loadCurrentURLIfNull);
        }

        public void ClearQueryString()
        {
            QS.Clear();

        }

        public object this[string id]
        {
            get
            {
                return QS[id];
            }
            set
            {
                if (value != null)
                {
                    string s = CS.General_v3.Util.Other.ConvertBasicDataTypeToString(value);
                    QS.Set(id, s);

                }
                else
                {
                    QS.Set(id, "");

                }
            }
        }
        public object this[int id]
        {
            get
            {
                return QS[id];
            }
        }

        public virtual string GetQueryString()
        {
            string qs = null;
            if (QS != null)
            {
                qs = QS.ToString();

            }

            return qs;
        }
        
        /// <summary>
        /// Returns a url, formatted based on the parameters
        /// </summary>
        /// <param name="fullyQualified">Whether to return the FQDN, i.e including http://www.maltacarsonline.com/ etc</param>
        /// <param name="appendQueryString">Whether to append the querystring to the url</param>
        /// <param name="includeScheme">Whether to included the scheme (http/s).  If not fully qualified, this parameter is ignored.</param>
        /// <param name="includePath">Whether to include the 'relative' portion of the url.  I.e without the relative part, http://www.maltacarsonline.com/test would become http://www.maltacarsonline.com</param>
        /// <returns>The URL</returns>
        public virtual string GetURL(bool fullyQualified = false, bool appendQueryString = true, bool includeScheme = true, bool includePath = true, bool appendFragmentID = true)
        {
            string url = "";
            if (fullyQualified)
            {
                
                if (!string.IsNullOrWhiteSpace(Domain))
                {
                    url += Domain;
                }
                else
                {
                    url = CS.General_v3.Util.UrlUtil.GetBaseUrl(CS.General_v3.Util.PageUtil.GetUrlFromCurrentRequest(), includeScheme: false);
                }
                if (includeScheme && !string.IsNullOrEmpty(url))
                {
                    url = (this.UseSsl ? "https://" : "http://") + url;
                }
            }

            if (this.Port.HasValue && fullyQualified)
            {
                var match = Regex.Match(url, @"(.*?)\:[0-9]{1,5}(.*?)", RegexOptions.IgnoreCase);
                if (match.Success)
                {
                    url = match.Groups[1].Value + match.Groups[2].Value;
                }
                url += ":" + this.Port.Value;
            }
            if (includePath && !string.IsNullOrWhiteSpace(this.Path))
            {
                if (!this.Path.StartsWith("/"))
                {
                    url += "/";
                }
                url += this.Path;
            }
            else {
                url += "/";
            }

            if (appendQueryString)
            {
                string qs = GetQueryString();
                if (!String.IsNullOrEmpty(url) && !String.IsNullOrEmpty(qs))
                {
                    url += "?" + qs;
                }
            } 
            if (appendFragmentID)
            {
                if (!String.IsNullOrEmpty(url) && !String.IsNullOrEmpty(FragmentID))
                {
                    url += "#" + FragmentID;
                }
            }
            
            return url;
            
        }
        /*public virtual string GetURL2(bool fullyQualified = false, bool appendQueryString = true, bool includeProtocol = true, bool includeRelativeUrl = true)
        {
            if (fullyQualified)
            {

            }

            string url = "";
            if (fullyQualified)
            {
                url = CS.General_v3.Util.UrlUtil.GetBaseUrl(this.PageURL, includeScheme: false);
                if (includeProtocol && !string.IsNullOrEmpty(url))
                {
                    if (!this.UseSsl)
                        url = "http://" + url;
                    else
                        url = "https://" + url;
                }
            }
            if (this.Port.HasValue && fullyQualified)
                url += ":" + this.Port.Value;
            if (includeRelativeUrl && !string.IsNullOrEmpty(this.PageURL))
                url += UrlUtil.GetRelativeUrl(this.PageURL);
            else
            {
                //if (string.IsNullOrWhiteSpace(url))
                //    url = "/";
            }

            if (appendQueryString)
            {

                string qs = GetQueryString();
                if (!String.IsNullOrEmpty(url) && !String.IsNullOrEmpty(qs))
                    url += "?" + qs;


            }
            return url;

        }*/
         

        /// <summary>
        /// Returns the non-fully-qualified url, including querystring.  E.g /test/testing?param1=true
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.GetURL(fullyQualified: true);
            


        }
        
        /// <summary>
        /// Redirects to the same page, with the new querystring
        /// </summary>
        public void RedirectTo()
        {
            PageUtil.RedirectPage(this.GetURL(fullyQualified:true));
        }
        public void TransferTo()
        {
            TransferTo(this.ToString());
        }
        /// <summary>
        /// The path, relative to the location
        /// </summary>
        /// <param name="path">Path name, e.g .http://css-tricks.com/examples/AnythingSlider/.http://css-tricks.com/examples/AnythingSlider/index.aspx</param>
        public void RedirectTo(string path)
        {
            //PageURL = path;
            CS.General_v3.Util.PageUtil.RedirectPage(this.ToString());
        }
        /// <summary>
        /// The path, relative to the location
        /// </summary>
        /// <param name="path">Path name, e.g .http://css-tricks.com/examples/AnythingSlider/.http://css-tricks.com/examples/AnythingSlider/index.aspx</param>
        public void TransferTo(string path)
        {
            //PageURL = path;
            CS.General_v3.Util.PageUtil.ServerTransfer(this.ToString());
        }
        public void LoadFromNameValueCollection(NameValueCollection nv)
        {
            foreach (var key in nv.AllKeys)
            {
                this[key] = nv[key];
            }
        }

        public void LoadFromCurrentQuerystring()
        {
            if (CS.General_v3.Util.PageUtil.CheckRequestExistsAtContext())
            {
                var qs = CS.General_v3.Util.PageUtil.GetRequestQueryString();
                LoadFromNameValueCollection(qs);
            }

        }
    }

}

        //public string PageURL
        //{
        //    get
        //    {
        //        return _pageURL;
        //    }
        //    set
        //    {
        //        _pageURL = value;
        //        int qIndex = _pageURL.IndexOf("?");
        //        if (qIndex != -1)
        //        {
        //            _pageURL = _pageURL.Substring(0, qIndex);
        //        }
        //        _pageURL = UrlUtil.RemovePortFromUrl(_pageURL);

                
        //    }
        //}
