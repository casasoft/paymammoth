﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using CS.General_v3.Util;
using System.Text.RegularExpressions;

namespace CS.General_v3.Classes.URL
{

    public class URLClassOld
    {
        public void AddQuerystringParamsFromNameValueColl(NameValueCollection nv)
        {
            if (nv != null)
            {

                for (int i = 0; i < nv.AllKeys.Length; i++)
                {
                    string key = nv.AllKeys[i];
                    this[key] = nv[key];
                }
            }

        }
        public int? Port { get; set; }
        public bool UseSsl { get; set; }
        public string Scheme { get; set; }
        public string Domain { get; set; }

        public void Remove(params string[] keys)
        {
            if (keys != null)
            {
                
                for (int i = 0; i < keys.Length; i++)
                {
                    string key = keys[i];
                    QS.Remove(key);
                    
                }
            }

        }
        /// <summary>
        /// Transfers the processing to the same page
        /// </summary>
        public static void TransferToSamePage()
        {
            URLClassOld url = new URLClassOld();
            url.TransferTo();
        }
        /// <summary>
        /// Redirects the processing to the same page
        /// </summary>
        public static void RedirectToSamePage()
        {
            URLClassOld url = new URLClassOld();
            url.RedirectTo();
        }
        public CS.General_v3.URL.QueryString QS { get; set; }

        private string _pageURL = "";


        public string PageURL
        {
            get
            {
                return _pageURL;
            }
            set
            {
                _pageURL = value;
                int qIndex = _pageURL.IndexOf("?");
                if (qIndex != -1)
                {
                    _pageURL = _pageURL.Substring(0, qIndex);
                }
                _pageURL = UrlUtil.RemovePortFromUrl(_pageURL);

                
            }
        }

        private void init()
        {

            this.QS = new General_v3.URL.QueryString();
        }

        /// <summary>
        /// Creates URL from current Page URL
        /// </summary>
        public URLClassOld()
        {
            init();
            initFromCurrentURLQueryString();
        }

        private void parseURL()
        {

        }

        /// <summary>
        /// Initializes the QS from the current URL querystring
        /// </summary>
        private void initFromCurrentURLQueryString()
        {
            QS = new CS.General_v3.URL.QueryString();
            var reqQS = CS.General_v3.Util.PageUtil.GetRequestQueryString();
            for (int i = 0; i < reqQS.Count; i++)
            {
                QS.Add(reqQS.Keys[i], reqQS[i]);
            }
            _pageURL = System.Web.HttpContext.Current.Request.Path;
        }

        private void parseQuerystring(string qs)
        {
            if (!string.IsNullOrWhiteSpace(qs))
            {
                int QMarkIndex = qs.IndexOf("?");
                string querystring = qs.Substring(QMarkIndex+1);

                
                
                //Contains queryString
                string[] keyValues = querystring.Split('&');
                for (int i = 0; i < keyValues.Length; i++)
                {
                    string[] data = keyValues[i].Split('=');
                    if (data.Length >= 2)
                    {
                        string key = data[0];

                        string value = CS.General_v3.Util.PageUtil.UrlDecode(data[1]);

                        QS.Add(key, value);
                    }
                }
                
            
            }
        }





        /// <summary>
        /// Updates the url, without changing any querystring variables
        /// </summary>
        /// <param name="url"></param>
        public void SetUrl(string url)
        {

            this.ParseURL(url, parseQuerystringVariables: false);
        }
        public void ParseURL2(string Url, bool loadCurrentURLIfNull = false, bool parseQuerystringVariables = true)
        {
            Regex r = new Regex(@"(((.*?)://)(.*?)(:(.*?))?/)?(.*?)\?(.*?)#(.*?)$", RegexOptions.Singleline | RegexOptions.Multiline | RegexOptions.IgnoreCase);

            //http://www.cikku.com:8080/test.aspx
            ///test.aspx




            Match match = r.Match(Url);

        }
        public void ParseURL(string Url, bool loadCurrentURLIfNull = false, bool parseQuerystringVariables = true)
        {
            if (Url == null && loadCurrentURLIfNull)
            {
                Url = PageUtil.GetCurrentUrlLocation().GetURL(fullyQualified: false);
            }
            
            if (!String.IsNullOrEmpty(Url))
            {
                
                int QMarkIndex = Url.IndexOf("?");
                if (QMarkIndex != -1)
                {
                    PageURL = Url.Substring(0, QMarkIndex);
                    if (parseQuerystringVariables)
                    {
                        QS.Clear();
                        //QS = new CS.General_v3.URL.QueryString();
                        parseQuerystring(Url);
                    }

                }
                else
                {
                    PageURL = Url;
                }
                
            }
            else
            {
                //_pageURL = System.Web.HttpContext.Current.Request.Path;
            }
            this.Port = CS.General_v3.Util.UrlUtil.ParsePortFromUrl(Url);
            this.UseSsl = CS.General_v3.Util.UrlUtil.CheckWhetherUrlIsSecure(Url);
        }

        /// <summary>
        /// Creates URL from passed URL.  If left empty or null, an empty Querystring is created with your current page URL
        /// </summary>
        /// <param name="URL"></param>
        public URLClassOld(string URL, bool loadCurrentURLIfNull = true)
        {
            init();
            ParseURL(URL, loadCurrentURLIfNull);
        }

        public void ClearQueryString()
        {
            QS.Clear();

        }

        public object this[string id]
        {
            get
            {
                return QS[id];
            }
            set
            {
                if (value != null)
                {
                    string s = CS.General_v3.Util.Other.ConvertBasicDataTypeToString(value);
                    QS.Set(id, s);

                }
                else
                {
                    QS.Set(id, "");

                }
            }
        }
        public object this[int id]
        {
            get
            {
                return QS[id];
            }
        }

        public virtual string GetQueryString()
        {
            string qs = null;
            if (QS != null)
            {
                qs = QS.ToString();

            }

            return qs;
        }
        
        /// <summary>
        /// Returns a url, formatted based on the parameters
        /// </summary>
        /// <param name="fullyQualified">Whether to return the FQDN, i.e including http://www.maltacarsonline.com/ etc</param>
        /// <param name="appendQueryString">Whether to append the querystring to the url</param>
        /// <param name="includeScheme">Whether to included the protocol.  If not fully qualified, this parameter is ignored.</param>
        /// <param name="includePath">Whether to include the 'relative' portion of the url.  I.e without the relative part, http://www.maltacarsonline.com/test would become http://www.maltacarsonline.com</param>
        /// <returns>The URL</returns>
        public virtual string GetURL(bool fullyQualified = false, bool appendQueryString = true, bool includeScheme = true, bool includePath = true)
        {


            string url = "";
            if (fullyQualified )
            {
                url = CS.General_v3.Util.UrlUtil.GetBaseUrl(this.PageURL, includeScheme: false);
                if (includeScheme && !string.IsNullOrEmpty(url))
                {
                    if (!this.UseSsl)
                        url = "http://" + url;
                    else
                        url = "https://" + url;
                }
            }
            if (this.Port.HasValue && fullyQualified)
                url += ":" + this.Port.Value;
            if (includePath && !string.IsNullOrEmpty(this.PageURL))
                url += UrlUtil.GetRelativeUrl(this.PageURL);
            else
            {
                //if (string.IsNullOrWhiteSpace(url))
                //    url = "/";
            }

            if (appendQueryString)
            {
                
                string qs = GetQueryString();
                if (!String.IsNullOrEmpty(url) && !String.IsNullOrEmpty(qs))
                    url +="?" + qs;


            }
            return url;
            
        }
        /*
         private static void _test_getUrl()
        {
            string customUrl = null;
            customUrl = "http://www.maltacarsonline.com/testing/test.html?hello=1";
            URLClass url = new URLClass(customUrl);

            string result;
            

            

            result = url.GetURL(fullyQualified: true, appendQueryString: false, includeProtocol: false, includeRelativeUrl: false);
            CS.General_v3.Util.ContractsUtil.Requires(result == "www.maltacarsonline.com");

            result = url.GetURL(fullyQualified: true, appendQueryString: true, includeProtocol: false, includeRelativeUrl: false);
            CS.General_v3.Util.ContractsUtil.Requires(result == "www.maltacarsonline.com?hello=1");

            result = url.GetURL(fullyQualified: true, appendQueryString: true, includeProtocol: false, includeRelativeUrl: true);
            CS.General_v3.Util.ContractsUtil.Requires(result == "www.maltacarsonline.com/testing/test.html?hello=1");

            result = url.GetURL(fullyQualified: true, appendQueryString: true, includeProtocol: true, includeRelativeUrl: true);
            CS.General_v3.Util.ContractsUtil.Requires(result == "http://www.maltacarsonline.com/testing/test.html?hello=1");


            result = url.GetURL(fullyQualified: false, appendQueryString: true, includeProtocol: false, includeRelativeUrl: true);
            CS.General_v3.Util.ContractsUtil.Requires(result == "/testing/test.html?hello=1");

            result = url.GetURL(fullyQualified: false, appendQueryString: true, includeProtocol: true, includeRelativeUrl: true);
            CS.General_v3.Util.ContractsUtil.Requires(result == "/testing/test.html?hello=1");

            result = url.GetURL(fullyQualified: false, appendQueryString: true, includeProtocol: true, includeRelativeUrl: false);
            CS.General_v3.Util.ContractsUtil.Requires(result == "/?hello=1");

         }
        */
        /// <summary>
        /// Returns the non-fully-qualified url, including querystring.  E.g /test/testing?param1=true
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.GetURL(fullyQualified: true);
            


        }
        
        /// <summary>
        /// Redirects to the same page, with the new querystring
        /// </summary>
        public void RedirectTo()
        {
            PageUtil.RedirectPage(this.GetURL(fullyQualified:true));
        }
        public void TransferTo()
        {
            TransferTo(this.ToString());
        }
        /// <summary>
        /// The path, relative to the location
        /// </summary>
        /// <param name="path">Path name, e.g .http://css-tricks.com/examples/AnythingSlider/.http://css-tricks.com/examples/AnythingSlider/index.aspx</param>
        public void RedirectTo(string path)
        {
            PageURL = path;
            CS.General_v3.Util.PageUtil.RedirectPage(this.ToString());
        }
        /// <summary>
        /// The path, relative to the location
        /// </summary>
        /// <param name="path">Path name, e.g .http://css-tricks.com/examples/AnythingSlider/.http://css-tricks.com/examples/AnythingSlider/index.aspx</param>
        public void TransferTo(string path)
        {
            PageURL = path;
            CS.General_v3.Util.PageUtil.ServerTransfer(this.ToString());
        }
        public void LoadFromNameValueCollection(NameValueCollection nv)
        {
            foreach (var key in nv.AllKeys)
            {
                this[key] = nv[key];
            }
        }

        public void LoadFromCurrentQuerystring()
        {
            if (CS.General_v3.Util.PageUtil.CheckRequestExistsAtContext())
            {
                var qs = CS.General_v3.Util.PageUtil.GetRequestQueryString();
                LoadFromNameValueCollection(qs);
            }

        }
    }

}
