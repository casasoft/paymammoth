﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
namespace CS.General_v3.Classes.Weather
{
    public class YahooWeatherUnits
    {
        public enum DISTANCE
        {
            Miles,
            Kilometers
        }
        public enum PRESSURE
        {
            Pounds,
            Millibars
        }
        public enum SPEED
        {
            MPH,
            KPH
        }
        public enum TEMPERATURE
        {
            Fahrenheit,
            Celsius
        }
        public YahooWeatherUnits(TEMPERATURE t, DISTANCE d, PRESSURE p, SPEED s)
        {
            this.Temperature = t;
            this.Distance = d;
            this.Pressure = p;
            this.Speed = s;
        }
        /// <summary>
        /// Get the units from the XML node in the RSS Feed
        /// </summary>
        /// <param name="nodeUnits"></param>
        public YahooWeatherUnits(XmlNode nodeUnits)
        {
            string t = nodeUnits.Attributes["temperature"].Value;
            string d = nodeUnits.Attributes["distance"].Value;
            string p = nodeUnits.Attributes["pressure"].Value;
            string s = nodeUnits.Attributes["speed"].Value;
            switch (t)
            {
                case "f": this.Temperature = TEMPERATURE.Fahrenheit; break;
                case "c": this.Temperature = TEMPERATURE.Celsius; break;
            }
            switch (d)
            {
                case "mi": this.Distance = DISTANCE.Miles; break;
                case "km": this.Distance = DISTANCE.Kilometers; break;

            }
            switch (p)
            {
                case "in": this.Pressure = PRESSURE.Pounds; break;
                case "mb": this.Pressure = PRESSURE.Millibars; break;
            }
            switch (s)
            {
                case "mph": this.Speed = SPEED.MPH; break;
                case "kph": this.Speed = SPEED.KPH; break;
            }
        }
        public TEMPERATURE Temperature { get; set; }
        public DISTANCE Distance { get; set; }
        public PRESSURE Pressure { get; set; }
        public SPEED Speed { get; set; }
    }
}
