﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;
using System.Xml.XPath;

namespace CS.General_v3.Classes.Weather
{

    public class YahooWeatherInfo
    {
        public YahooWeatherInfo(DateTime dateRetrieved, YahooWeatherChannel channel)
        {
            this.DateRetrieved = dateRetrieved;
            this.Channel = channel;
        }
        public YahooWeatherInfo(string XMLFeedURL)
        {
            XmlDocument xml = new XmlDocument();
            xml.Load(XMLFeedURL);


            // Create navigator
            XPathNavigator navigator = xml.CreateNavigator();

            // Set up namespace manager for XPath
            XmlNamespaceManager ns = new XmlNamespaceManager(navigator.NameTable);
            ns.AddNamespace("yweather", "http://xml.weather.yahoo.com/ns/rss/1.0");



            YahooWeatherChannel channel = new YahooWeatherChannel(xml.SelectSingleNode("rss/channel"), ns);
            this.Channel = channel;
            this.DateRetrieved = CS.General_v3.Util.Date.Now;
        }
        public DateTime DateRetrieved { get; private set; }
        public YahooWeatherChannel Channel { get; private set; }


        /// <summary>
        /// Groups the huge set of weather status into smaller groups
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public static WEATHER_STATUS_GROUP GetWeatherStatus(YAHOO_WEATHER_STATUS status)
        {
            switch (status)
            {
                case YAHOO_WEATHER_STATUS.Tornado:
                case YAHOO_WEATHER_STATUS.Thunderstorms:
                case YAHOO_WEATHER_STATUS.TropicalStorm:
                case YAHOO_WEATHER_STATUS.Hurricane:
                case YAHOO_WEATHER_STATUS.SevereThunderstorms:
                case YAHOO_WEATHER_STATUS.Thundershowers:
                case YAHOO_WEATHER_STATUS.IsolatedThunderstorms:
                case YAHOO_WEATHER_STATUS.ScatteredThunderstorms:
                case YAHOO_WEATHER_STATUS.ScatteredThunderstorms2:
                case YAHOO_WEATHER_STATUS.SnowShowers:
                case YAHOO_WEATHER_STATUS.IsolatedThundershowers:
                    return WEATHER_STATUS_GROUP.Stormy;

                case YAHOO_WEATHER_STATUS.MixedRainAndSnow:
                case YAHOO_WEATHER_STATUS.MixedSnowAndSleet:
                case YAHOO_WEATHER_STATUS.SnowFlurries:
                case YAHOO_WEATHER_STATUS.LightSnowShowers:
                case YAHOO_WEATHER_STATUS.BlowingSnow:
                case YAHOO_WEATHER_STATUS.Snow:
                case YAHOO_WEATHER_STATUS.HeavySnow:
                case YAHOO_WEATHER_STATUS.HeavySnow2:
                    return WEATHER_STATUS_GROUP.Snowy;

                case YAHOO_WEATHER_STATUS.MixedRainAndSleet:
                case YAHOO_WEATHER_STATUS.FreezingDrizzle:
                case YAHOO_WEATHER_STATUS.Drizzle:
                case YAHOO_WEATHER_STATUS.FreezingRain:
                case YAHOO_WEATHER_STATUS.Showers:
                case YAHOO_WEATHER_STATUS.Showers2:
                case YAHOO_WEATHER_STATUS.Hail:
                case YAHOO_WEATHER_STATUS.Sleet:
                case YAHOO_WEATHER_STATUS.MixedRainAndHail:

                    return WEATHER_STATUS_GROUP.Showers;

                case YAHOO_WEATHER_STATUS.Foggy:
                case YAHOO_WEATHER_STATUS.Haze:
                case YAHOO_WEATHER_STATUS.Cloudy:
                case YAHOO_WEATHER_STATUS.MostlyCloudyDay:
                    return WEATHER_STATUS_GROUP.Cloudy;

                case YAHOO_WEATHER_STATUS.Dust:
                case YAHOO_WEATHER_STATUS.Sunny:
                case YAHOO_WEATHER_STATUS.FairDay:
                case YAHOO_WEATHER_STATUS.Hot:
                    return WEATHER_STATUS_GROUP.Clear;

                case YAHOO_WEATHER_STATUS.Windy:
                case YAHOO_WEATHER_STATUS.Smoky:
                case YAHOO_WEATHER_STATUS.Blustery:

                    return WEATHER_STATUS_GROUP.Windy;

                case YAHOO_WEATHER_STATUS.Cold:
                    return WEATHER_STATUS_GROUP.Cold;

                case YAHOO_WEATHER_STATUS.MostlyCloudyNight:
                case YAHOO_WEATHER_STATUS.PartlyCloudyNight:

                    return WEATHER_STATUS_GROUP.CloudyNight;

                case YAHOO_WEATHER_STATUS.PartlyCloudyDay:
                case YAHOO_WEATHER_STATUS.PartlyCloudy:

                    return WEATHER_STATUS_GROUP.PartlyCloudy;

                case YAHOO_WEATHER_STATUS.ClearNight:
                case YAHOO_WEATHER_STATUS.FairNight:

                    return WEATHER_STATUS_GROUP.ClearNight;

                case YAHOO_WEATHER_STATUS.ScatteredShowers:
                    return WEATHER_STATUS_GROUP.ShowersScattered;


            }
            return WEATHER_STATUS_GROUP.Null;
        }
    }
}
