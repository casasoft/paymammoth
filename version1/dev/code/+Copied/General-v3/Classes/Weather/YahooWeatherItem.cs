﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;
using System.Xml.XPath;

namespace CS.General_v3.Classes.Weather
{

    public class YahooWeatherItem
    {

        private YahooWeatherChannel _channel;

        public YahooWeatherChannel Channel
        {
            get { return _channel; }
        }

        public YahooWeatherItem(YahooWeatherChannel channel, XmlNode itemNode, XmlNamespaceManager ns)
        {
            _channel = channel;
            this.Title = itemNode.SelectSingleNode("title").InnerText;
            this.Description = itemNode.SelectSingleNode("description").InnerText;
            this.GUID = itemNode.SelectSingleNode("guid").InnerText;
            this.PublicationDate = CS.General_v3.Util.Date.GetDateFromRFC822(itemNode.SelectSingleNode("pubDate").InnerText);
            this.Condition = new YahooWeatherCondition(this, itemNode.SelectSingleNode("yweather:condition", ns));
            this.Forecast = new YahooWeatherForecastList(this, itemNode.SelectNodes("yweather:forecast", ns));
            parseImageIcon(Description);
            
        }



        private void parseImageIcon(string desc)
        {
            string findToken = "src=\"";
            int srcIndex = desc.IndexOf(findToken);
            if (srcIndex != -1)
            {
                srcIndex += findToken.Length;
                int endIndex = desc.IndexOf("\"", srcIndex);
                this.ImageIconURL = desc.Substring(srcIndex, endIndex - srcIndex);
            }
        }
        public string ImageIconURL { get; set; }
        public string Title { get; set; }
        public string URL { get; set; }
        public string GUID { get; set; }
        public string Description { get; set; }
        public DateTime PublicationDate { get; set; }
        public YahooWeatherCondition Condition { get; set; }
        public YahooWeatherForecastList Forecast { get; set; }

    }
   
}
