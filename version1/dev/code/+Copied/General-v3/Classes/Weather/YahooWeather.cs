﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;
using System.Xml.XPath;

namespace CS.General_v3.Classes.Weather
{

    public enum YAHOO_WEATHER_STATUS
    {
        Tornado = 0,
        TropicalStorm = 1,
        Hurricane = 2,
        SevereThunderstorms = 3,
        Thunderstorms = 4,
        MixedRainAndSnow = 5,
        MixedRainAndSleet = 6,
        MixedSnowAndSleet = 7,
        FreezingDrizzle = 8,
        Drizzle = 9,
        FreezingRain = 10,
        Showers = 11,
        Showers2 = 12,
        SnowFlurries = 13,
        LightSnowShowers = 14,
        BlowingSnow = 15,
        Snow = 16,
        Hail = 17,
        Sleet = 18,
        Dust = 19,
        Foggy = 20,
        Haze = 21,
        Smoky = 22,
        Blustery = 23,
        Windy = 24,
        Cold = 25,
        Cloudy = 26,
        MostlyCloudyNight = 27,
        MostlyCloudyDay = 28,
        PartlyCloudyNight = 29,
        PartlyCloudyDay = 30,
        ClearNight = 31,
        Sunny = 32,
        FairNight = 33,
        FairDay = 34,
        MixedRainAndHail = 35,
        Hot = 36,
        IsolatedThunderstorms = 37,
        ScatteredThunderstorms = 38,
        ScatteredThunderstorms2 = 39,
        ScatteredShowers = 40,
        HeavySnow = 41,
        ScatteredSnowShowers = 42,
        HeavySnow2 = 43,
        PartlyCloudy = 44,
        Thundershowers = 45,
        SnowShowers = 46,
        IsolatedThundershowers = 47,
        NotAvailable = 3200
    }

    public enum WEATHER_STATUS_GROUP
    {
        Cloudy,
        PartlyCloudy,
        CloudyNight,
        Showers,
        ShowersScattered,
        Clear,
        ClearNight,
        Stormy,
        Snowy,
        Windy,
        Cold,
        Null
    }




    public class YahooWeather
    {
        

        private YahooWeatherInfo _info;

        private YahooWeatherUnits.TEMPERATURE _temperatureUnit;

        private static Dictionary<string, YahooWeatherInfo> _CACHED_WEATHER = new Dictionary<string, YahooWeatherInfo>();


        /// <summary>
        /// e.g. http://weather.yahooapis.com/forecastrss?p=MTXX0001&u=f
        /// </summary>
        /// <param name="rssFeedUrl"></param>
        public YahooWeather(string rssFeedUrl = "http://weather.yahooapis.com/forecastrss?p=MTXX0001&u=f", int cacheSeconds = 1800)
        {
            _info = getFromCache(rssFeedUrl, cacheSeconds);
            if (_info == null)
            {
                try
                {
                    _info = new YahooWeatherInfo(rssFeedUrl);
                    _CACHED_WEATHER[rssFeedUrl] = _info;
                }
                catch (Exception ex)
                {

                }
            }
           
        }
        private YahooWeatherInfo getFromCache(string rssFeedURL, int cacheSeconds)
        {
            if (_CACHED_WEATHER.ContainsKey(rssFeedURL))
            {
                YahooWeatherInfo weather = _CACHED_WEATHER[rssFeedURL];
                if (weather != null)
                {
                    TimeSpan now = new TimeSpan(DateTime.Now.Ticks);
                    TimeSpan weatherTime = new TimeSpan(weather.DateRetrieved.Ticks);
                    TimeSpan diff = now - weatherTime;
                    if (diff.TotalSeconds > cacheSeconds)
                    {
                        //need a new one
                        return null;
                    }
                    else
                    {
                        return weather;
                    }

                }
            }
            
            return null;
        }
        public string Text
        {
            get
            {
                return _info.Channel.Item.Condition.Description;
            }
        }
        public string ImageIconUrl
        {
            get
            {
                return _info.Channel.Item.ImageIconURL;
            }
        }

        public double TemperatureCelsius
        {
            get
            {
                double temp = _info.Channel.Item.Condition.Temperature;
                if (_info.Channel.Units.Temperature == YahooWeatherUnits.TEMPERATURE.Celsius)
                {
                    return temp;
                }
                else
                {
                    return CS.General_v3.Util.Conversion.FahrenheitToCelsius(temp);
                }
            }
        }

        public double TemperatureFahrenheit
        {
            get
            {
                double temp = _info.Channel.Item.Condition.Temperature;
                if (_info.Channel.Units.Temperature == YahooWeatherUnits.TEMPERATURE.Fahrenheit)
                {
                    return temp;
                }
                else
                {
                    return CS.General_v3.Util.Conversion.CelsiusToFahrenheit(temp);
                }
            }
        }

        public YahooWeatherForecastList Forecast
        {
            get
            {
                return _info.Channel.Item.Forecast;
            }
        }

    }
}
