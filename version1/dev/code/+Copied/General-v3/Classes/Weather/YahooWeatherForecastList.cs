﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;
using System.Xml.XPath;

namespace CS.General_v3.Classes.Weather
{

    public class YahooWeatherForecastList : List<YahooWeatherForecast>
    {
        private YahooWeatherItem _item;

        public YahooWeatherItem Item
        {
            get { return _item; }
        }

        public YahooWeatherForecastList(YahooWeatherItem item, XmlNodeList forecastNodes)
        {
            _item = item;
            for (int i = 0; i < forecastNodes.Count; i++)
            {
                Add(new YahooWeatherForecast(item, forecastNodes[i]));
                
            }
        }
    }
}
