﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;
using System.Xml.XPath;
using System.Globalization;
namespace CS.General_v3.Classes.Weather
{

    public class YahooWeatherForecast
    {
        public string Day { get; private set; }
        public DateTime Date { get; private set; }
        public double LowestTemperature { get; private set; }
        public double HighestTemperature { get; private set; }
        public string Text { get; private set; }
        public int Code { get; private set; }

        private YahooWeatherItem _item;

        private XmlNode _node;
        public YahooWeatherForecast(YahooWeatherItem item, XmlNode forecastNode)
        {
            _item = item;
            _node = forecastNode;
            Day = forecastNode.Attributes["day"].Value;
            
            Date = DateTime.Parse(forecastNode.Attributes["date"].Value);
            LowestTemperature = Double.Parse(forecastNode.Attributes["low"].Value);
            HighestTemperature = Double.Parse(forecastNode.Attributes["high"].Value);
            Text = forecastNode.Attributes["text"].Value;
            Code = Int32.Parse(forecastNode.Attributes["code"].Value);
        }

        public double LowestTemperatureCelsius
        {
            get
            {
                if (_item.Channel.Units.Temperature == YahooWeatherUnits.TEMPERATURE.Celsius)
                {
                    return LowestTemperature;
                }
                else
                {
                    return CS.General_v3.Util.Conversion.FahrenheitToCelsius(LowestTemperature);
                }
            }
        }
        public double LowestTemperatureFahrenheit
        {
            get
            {
                if (_item.Channel.Units.Temperature == YahooWeatherUnits.TEMPERATURE.Fahrenheit)
                {
                    return LowestTemperature;
                }
                else
                {
                    return CS.General_v3.Util.Conversion.CelsiusToFahrenheit(LowestTemperature);
                }
            }
        }

        public double HighestTemperatureCelsius
        {
            get
            {
                if (_item.Channel.Units.Temperature == YahooWeatherUnits.TEMPERATURE.Celsius)
                {
                    return HighestTemperature;
                }
                else
                {
                    return CS.General_v3.Util.Conversion.FahrenheitToCelsius(HighestTemperature);
                }
            }
        }
        public double HighestTemperatureFahrenheit
        {
            get
            {
                if (_item.Channel.Units.Temperature == YahooWeatherUnits.TEMPERATURE.Fahrenheit)
                {
                    return HighestTemperature;
                }
                else
                {
                    return CS.General_v3.Util.Conversion.CelsiusToFahrenheit(HighestTemperature);
                }
            }
        }
        /*<yweather:forecast day="Thu" date="30 Jul 2009" low="72" high="92" text="Sunny" code="32" />
*/
    }
}
