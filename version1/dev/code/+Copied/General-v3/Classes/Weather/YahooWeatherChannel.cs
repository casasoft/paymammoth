﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;
using System.Xml.XPath;

namespace CS.General_v3.Classes.Weather
{
    public class YahooWeatherChannel
    {
        /// <summary>
        ///  Units for various aspects of the forecast.
        /// </summary>
      

        /// <summary>
        /// 
        /// </summary>
        /// <param name="channelNode"></param>
        /// <param name="ns">Namespace manager for XML due to yweather</param>
        public YahooWeatherChannel(XmlNode channelNode, XmlNamespaceManager ns)
        {

            this.Title = channelNode.SelectSingleNode("title").InnerText;
            this.URL = channelNode.SelectSingleNode("link").InnerText;
            this.Language = channelNode.SelectSingleNode("language").InnerText;
            this.Description = channelNode.SelectSingleNode("description").InnerText;
            this.LastUpdated = CS.General_v3.Util.Date.GetDateFromRFC822(channelNode.SelectSingleNode("lastBuildDate").InnerText);
            this.TimeToLive = Convert.ToInt32(channelNode.SelectSingleNode("ttl").InnerText);
            //string title = channelNode.SelectSingleNode("title").InnerText;
            string lastBuildDateStr = channelNode.SelectSingleNode("lastBuildDate").InnerText;
            lastBuildDateStr = lastBuildDateStr.Substring(0, lastBuildDateStr.IndexOf("2008") + 13);







            this.Units = new YahooWeatherUnits(channelNode.SelectSingleNode("yweather:units", ns));
            this.Item = new YahooWeatherItem(this, channelNode.SelectSingleNode("item"), ns);

        }
        /// <summary>
        /// The title of the feed, which includes the location city. For example "Yahoo! Weather - Sunnyvale, CA"
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// The URL for the Yahoo! Weather page of the forecast for this location. For example http://us.rd.yahoo.com/dailynews/rss/weather/ Sunnyvale__CA/ *http://weather.yahoo.com/ forecast/94089_f.html
        /// </summary>
        public string URL { get; set; }
        /// <summary>
        /// The language of the weather forecast, for example, en-us for US English.
        /// </summary>
        public string Language { get; set; }
        /// <summary>
        /// The overall description of the feed including the location, for example "Yahoo! Weather for Sunnyvale, CA"
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// The last time the feed was updated. The format is in the date format defined by RFC822 Section 5, for example Mon, 256 Sep 17:25:18 -0700.
        /// </summary>
        public DateTime LastUpdated { get; set; }
        /// <summary>
        /// Time to Live; how long in minutes this feed should be cached.
        /// </summary>
        public int TimeToLive { get; set; }
        public string Location { get { throw new NotImplementedException(); } }
        /// <summary>
        /// Units for various aspects of the forecast.
        /// </summary>
        public YahooWeatherUnits Units { get; set; }
        /// <summary>
        /// The local weather conditions and forecast for a specific location.
        /// </summary>
        public YahooWeatherItem Item { get; set; }

    }
}
