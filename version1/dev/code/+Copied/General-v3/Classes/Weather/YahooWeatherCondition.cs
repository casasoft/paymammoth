﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CS.General_v3.Classes.Weather
{
    public class YahooWeatherCondition
    {

        private YahooWeatherItem _item;
        public YahooWeatherCondition(YahooWeatherItem item, XmlNode nodeCondition)
        {
            _item = item;
            this.Description = nodeCondition.Attributes["text"].Value;
            this.Status = (YAHOO_WEATHER_STATUS)(Convert.ToInt32(nodeCondition.Attributes["code"].Value));
            this.Temperature = Convert.ToDouble(nodeCondition.Attributes["temp"].Value);
            this.Date = CS.General_v3.Util.Date.GetDateFromRFC822(nodeCondition.Attributes["date"].Value);

        }

        public string Description { get; set; }
        public YAHOO_WEATHER_STATUS Status { get; set; }
        public WEATHER_STATUS_GROUP StatusGroup
        {
            get
            {
                return YahooWeatherInfo.GetWeatherStatus(this.Status);
            }
        }
        public double Temperature { get; set; }

        public double TemperatureCelsius
        {
            get
            {
                if (_item.Channel.Units.Temperature == YahooWeatherUnits.TEMPERATURE.Celsius)
                {
                    return Temperature;
                }
                else
                {
                    return CS.General_v3.Util.Conversion.FahrenheitToCelsius(Temperature);
                }
            }
        }

        public double TemperatureFahrenheit
        {
            get
            {
                if (_item.Channel.Units.Temperature == YahooWeatherUnits.TEMPERATURE.Fahrenheit)
                {
                    return Temperature;
                }
                else
                {
                    return CS.General_v3.Util.Conversion.CelsiusToFahrenheit(Temperature);
                }
            }
        }
        public DateTime Date { get; set; }

    }
    
}
