﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace CS.General_v3.Classes.Processes
{
    public class ProcessDumpOutputToFile : IDisposable
    {

        public string ExecutableFilePath { get; set; }
        public string Arguments { get; set; }

        private string getExecutableFilePath()
        {

            return CS.General_v3.Util.IO.AddCurrentDirectoryIfPathDoesNotIncludeADir(this.ExecutableFilePath);
        }

        private bool _outputFileTemporary = false;

        /// <summary>
        /// If left empty, a temporary one is created.
        /// </summary>
        public string OutputPath { get; set; }

        private string generateBatchFileContent()
        {
            _outputFileTemporary = false;
            if (string.IsNullOrWhiteSpace(OutputPath))
            {
                OutputPath = CS.General_v3.Util.IO.GetTempFilename(".out");
                _outputFileTemporary = true;
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("\"" + getExecutableFilePath() + "\"");
            if (!string.IsNullOrWhiteSpace(Arguments)) sb.Append(" " + Arguments);
            sb.Append(" > \"" + CS.General_v3.Util.IO.AddCurrentDirectoryIfPathDoesNotIncludeADir(this.OutputPath) + "\"");

            sb.AppendLine();
            return sb.ToString();

        }

        private ProcessStartInfo _pStartInfo = null;

        private string _batchFilePath = null;
        private void generateBatchFile()
        {
            string batchFileContents = generateBatchFileContent();
            _batchFilePath = CS.General_v3.Util.IO.GetTempFilename("bat");
            CS.General_v3.Util.IO.SaveToFile(_batchFilePath, batchFileContents);
        }

        private void verifyRequirements()
        {
            StringBuilder sb = new StringBuilder();
            if (string.IsNullOrWhiteSpace(ExecutableFilePath)) sb.AppendLine("Fill in 'ExecutableFilePath'");
            
            if (sb.Length > 0) throw new InvalidOperationException(sb.ToString());
        }
        private Process _process = null;
        public bool ExecuteProcess2()
        {
            verifyRequirements();
            generateBatchFile();
            _pStartInfo = new ProcessStartInfo(ExecutableFilePath, this.Arguments);
            _pStartInfo.CreateNoWindow = true;
            _pStartInfo.UseShellExecute = false;

            _process = new Process();
            _process.StartInfo = this._pStartInfo;
            _process.OutputDataReceived += new DataReceivedEventHandler(_process_OutputDataReceived);
            _process.Start();
            _process.BeginOutputReadLine();

            _process.WaitForExit();
            _process.Dispose();
            return (_process.ExitCode == 0);

        }
        public bool ExecuteProcess()
        {
            verifyRequirements();
            generateBatchFile();
            _pStartInfo = new ProcessStartInfo(_batchFilePath, this.Arguments);
            _pStartInfo.CreateNoWindow = true;
            _pStartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            _pStartInfo.UseShellExecute = false;
            _process = new Process();
            _process.StartInfo = _pStartInfo;
            //_process.EnableRaisingEvents = true;
            //_process.Exited += new EventHandler(_process_Exited);
            _process.Start();
            //_process.OutputDataReceived +=new DataReceivedEventHandler(_process_OutputDataReceived);
            _process.WaitForExit();
            int exitCode = _process.ExitCode;
            _process.Dispose();
            CS.General_v3.Util.IO.DeleteFile(_batchFilePath);
            return (exitCode == 0);

        }

        void _process_Exited(object sender, EventArgs e)
        {
            int k = 5;
        }
        void _process_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            int k = 5;
        }




        #region IDisposable Members

        public void Dispose()
        {

            //if (_outputFileTemporary && !string.IsNullOrWhiteSpace(this.OutputPath))
            //    CS.General_v3.Util.IO.DeleteFile(this.OutputPath);


        }

        #endregion
    }
}
