﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Hosting;
using log4net;

namespace CS.General_v3.Classes.Settings.v1
{
    public abstract class SettingsMain
    {
        private static ILog _log = log4net.LogManager.GetLogger(typeof(SettingsMain));
        protected static SettingsMain _Instance;
        public static SettingsMain Instance
        {
            get
            { 
                
                
                if (_Instance == null)
                {
                    throw new InvalidOperationException("Please make sure that you define the 'BusinessLogic.v1.Classes.Settings.Instance' value to an implemented value");
                }
                return _Instance;
            }
            set
            {
                _Instance = value;
                //CS.General_v3.Util.Page.SetSessionObject("BusinessLogic_v1_SettingsMain", value);
            }
        }
        public static bool CheckIfInstanceIsFilled()
        {
            return _Instance != null;
        }
        /*private static bool _ApplicationStart_Done = false;
        public static void ApplicationStart()
        {
            if (!_ApplicationStart_Done)
            {
                //do code
            }
        }*/
        private bool _onApplicationStart_Called = false;
        protected readonly object _lock = new object();
        private void printSettingsToLog()
        {
            if (_log.IsInfoEnabled)
            {
                _log.Info("IsInLocalTesting: " + CS.General_v3.Util.Other.BoolToText(CS.General_v3.Settings.IsInLocalTesting));
                _log.Info("IsOnProductionServer: " + CS.General_v3.Util.Other.BoolToText(CS.General_v3.Settings.IsOnProductionServer));
                _log.Info("AdminName: " + CS.General_v3.Settings.AdminName);
                _log.Info("AdminEmail: " + CS.General_v3.Settings.AdminEmail);
            }
        }
        protected virtual void onApplicationStart()
        {
            
            //this is meant to be overriden.  The other method provides uniqueness and locking
        }
        public void OnApplicationStart()
        {
            lock (_lock)
            {
                if (!_onApplicationStart_Called)
                {
                    if (_log.IsInfoEnabled) _log.Info("OnApplicationStart - CalledFirstTime");
                    CS.General_v3.Util.ApplicationUtil.MonitorFatalExceptions();
                    this.onApplicationStart();
                    _onApplicationStart_Called = true;
                }
            }
        }
        private bool _onFirstSessionStart_Called = false;
        protected virtual void onFirstSessionStart()
        {

        }
        private void onFirstSessionStartCheck()
        {
            lock (_lock)
            {
                if (!_onFirstSessionStart_Called)
                {
                    onFirstSessionStart();
                    _onFirstSessionStart_Called = true;
                    printSettingsToLog();
                }
            }
        }

        public virtual void OnSessionStart()
        {
            OnApplicationStart();
            onFirstSessionStartCheck();
        }
        public virtual SettingsFrontend Frontend { get; set; }
        public virtual SettingsOther Other { get; set; }
        public virtual SettingsCMS CMS { get; set; }
        protected void initDefaultSettingsClasses()
        {
            this.Frontend = new SettingsFrontend();
            this.Other = new SettingsOther();
            this.CMS = new SettingsCMS();
        }
        protected virtual void initSettingsClasses()
        {
            initDefaultSettingsClasses();
        }
        public SettingsMain()
        {
            initSettingsClasses();
        }
        public static string GetSettingFromDB_WithDefaultValueIfNotAttached(string identifier, string defaultValue)
        {
            string s = defaultValue;
            if (CheckIfInstanceIsFilled())
            {
                s = Instance.GetSetting(identifier,defaultValue);
            }
            return s;
        }

        public abstract string GetSetting(string settingName, string defaultValue = null);
        public double GetSettingDbl(string settingName, double defaultValue)
        {
            string s = GetSetting(settingName,null);
            double d = 0;
            if (string.IsNullOrEmpty(s) || !double.TryParse(s, out d))
            {
                d = defaultValue;
            }
            return d;

        }
        public  int GetSettingInt(string settingName, int defaultValue)
        {
            double d = GetSettingDbl(settingName, (double)defaultValue);
            int num = (int)d;
            return num;

        }
        public bool GetSettingBool(string name, bool defaultValue)
        {
            bool? b = GetSettingBoolNullable(name);
            if (b == null)
                b = defaultValue;
            return b.Value;
        }
        public bool? GetSettingBoolNullable(string name)
        {
            string s = GetSetting(name);
            bool? val = (CS.General_v3.Util.Other.TextToBoolNullable(s)); 
            return val;
        }

        protected virtual string setCurrentLanguageToDefault()
        {
            string defaultLang = "en";
            CurrentDefaultLanguageCode = defaultLang;
            return defaultLang;
        }

        public string CurrentDefaultLanguageCode
        {
            get
            {
                string s = CS.General_v3.Util.Page.GetSession("__CurrentDefaultLanguageCode");
                if (string.IsNullOrEmpty(s))
                    s = setCurrentLanguageToDefault();

                return s;
            }
            set
            {
                CS.General_v3.Util.Page.SetSessionObject("__CurrentDefaultLanguageCode", value);
            }
        }

    }
}
