﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Settings.v1
{
    public class SettingsFrontend
    {
        public virtual string QuerystringParam_ReferralID {get{ return "rid";}}
        public virtual string QuerystringParam_ReferralCode {get{ return "rc";}}
        //the base URLs were added on 15/09/2010 by Karl C.
        public virtual string GetMembersActivationLink(int memberID, string activationCode)
        {
            return CS.General_v3.Util.Page.GetBaseURL() + "activate.aspx?id=" + memberID + "&code=" + activationCode;
        }
        public virtual string GetMembersForgottenPassLink(int memberID, string resetCode)
        {
            return CS.General_v3.Util.Page.GetBaseURL() + "forgotPass.aspx?id=" + memberID + "&code=" + resetCode;
        }
        public virtual string GetContactUsLink()
        {
            return CS.General_v3.Util.Page.GetBaseURL() + "contactus.aspx";
        }
        public virtual string GetMembersArea_WelcomeURL()
        {
            return CS.General_v3.Util.Page.GetBaseURL() + "members/welcome.aspx";
        }
        public virtual string GetMembersReferralLink(int referralID, string referralCode)
        {
            return CS.General_v3.Util.Page.GetBaseURL() + "?" + QuerystringParam_ReferralID + "=" + referralID + "&" + QuerystringParam_ReferralCode + "=" + referralCode;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="invoiceID"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public virtual string GetViewInvoiceLink(int invoiceID, string guid)
        {
            return CS.General_v3.Util.Page.GetBaseURL() + "view_invoice.aspx?id=" + invoiceID + "&guid=" + guid;
        }
        public virtual string GetMemberLoginURL(int MemberID, bool fullyQualifiedUrl = true)
        {
            string s ="members/";
            if (fullyQualifiedUrl)
                s = CS.General_v3.Util.Page.GetBaseURL() + s;
            else
                s = "/" + s;
            return s;

        }

    }
}
