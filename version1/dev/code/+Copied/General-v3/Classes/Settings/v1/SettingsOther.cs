﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Settings.v1
{
    public class SettingsOther 
    {


        internal static string getEmailSignature_Static(bool asHTML)
        {
            string signature = "";
            if (CS.General_v3.Settings.EmailSignatureFile != "")
            {
                string path = CS.General_v3.Settings.LocalRootFolder + CS.General_v3.Settings.EmailSignatureFile.Replace("/", "\\");

                signature = CS.General_v3.Util.IO.LoadFile(path);
                if (asHTML)
                {
                    signature = CS.General_v3.Util.Text.ReplaceTexts(signature, "<br />", "\r\n", "\r", "\n");
                }
            }
            return signature;

        }
        public virtual string GetEmailSignature(bool asHTML)
        {
            return getEmailSignature_Static(asHTML);

        }
        /*
        internal static string getAdminEmailAddress_Static()
        {
            return (CS.General_v3.Util.Other.IsLocalTestingMachine ? CS.General_v3.Settings.AdminEmail_Localhost : CS.General_v3.Settings.AdminEmail_Normal);
            //return CS.General_v3.Settings.AdminEmail;
            /*if (CS.General_v3.Settings.Emails.LoadEmailsFromDatabase)
            {

            }*-/
        }*/
      /*  internal static string getNotificationEmailAddress_Static()
        {
            return (CS.General_v3.Util.Other.IsLocalTestingMachine ? CS.General_v3.Settings.Emails.NotificationEmail_Localhost : CS.General_v3.Settings.Emails.NotificationEmail_Normal);
            //return CS.General_v3.Settings.AdminEmail;
            /*if (CS.General_v3.Settings.Emails.LoadEmailsFromDatabase)
            {

            }*-/
        }*/

        
        /*
        public virtual string GetAdminEmailAddress()
        {
            return getAdminEmailAddress_Static();
        }

        public virtual string GetNotificationEmailAddress()
        {
            return getNotificationEmailAddress_Static();
        }

        internal static string getAdminEmailName_Static()
        {
            return CS.General_v3.Settings.GetSetting("Email/Name");
        }
        public virtual string GetAdminEmailName()
        {
            return getAdminEmailName_Static();
        }*/
        public virtual string GetHtmlEmailTemplate(string htmlTemplatePath)
        {
            string template = CS.General_v3.Util.IO.LoadFile(htmlTemplatePath);
            return template;
        }
        public virtual void InitCKEditor(CS.General_v3.Controls.WebControls.Common.CKEditor ckEditor)
        {
            //ckEditor.ConfigParameters.ContentCSSFiles.Add("/css/Neriku.css");
  //          ckEditor.ConfigParameters.BodyClass = "testing123";
            ckEditor.ConfigParameters.BodyClass = "panel-ckeditor";

            //ckEditor.ConfigParameters.StylesCombo_StyleSet = "/css/test.css";
            ckEditor.ConfigParameters.StylesCombo_StyleSet = "default:/_common/js/ckeditor/default.js";

            ckEditor.ConfigParameters.Font_Names.Clear();
            ckEditor.ConfigParameters.Font_Names.Add("Tahoma");
            ckEditor.ConfigParameters.Font_Names.Add("Arial");
            ckEditor.ConfigParameters.Font_Names.Add("Helvetica");
            ckEditor.ConfigParameters.Font_Names.Add("Verdana");
            ckEditor.ConfigParameters.Font_Names.Add("Sans-Serif/sans-serif");


            ckEditor.ConfigParameters.FontSize_Sizes.Clear();
            ckEditor.ConfigParameters.FontSize_Sizes.Add("Small", "10px");
            ckEditor.ConfigParameters.FontSize_Sizes.Add("Normal", "11px");
            ckEditor.ConfigParameters.FontSize_Sizes.Add("Large", "14px");
            ckEditor.ConfigParameters.FontSize_Sizes.Add("Very Large", "18px");

        }
        public virtual double GetVATRate()
        {
            return SettingsMain.Instance.GetSettingDbl("VATRate", 18);
        }

    }
}
