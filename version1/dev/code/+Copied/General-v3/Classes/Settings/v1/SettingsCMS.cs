﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Settings.v1
{
    public class SettingsCMS
    {
        public virtual string Root
        {
            get
            {
                return "/cms/";
            }
        }
        public virtual string WelcomeURL
        {
            get
            {
                return Root + "welcome.aspx";
            }
        }

        public virtual string GetMemberEditPageURL(int memberID)
        {
            return "/cms/Member/member.aspx?id=" + memberID;
        }


    }
}
