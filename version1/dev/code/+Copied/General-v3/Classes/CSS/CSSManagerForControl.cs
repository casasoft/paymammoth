﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI;
namespace CS.General_v3.Classes.CSS
{
    public class CSSManagerForControl : CSSManager
    {
        private WebControl _webControl;
        private HtmlControl _htmlControl;
        public CSSManagerForControl(WebControl control)
        {
            _webControl= control;
            init();
        }
        public CSSManagerForControl(HtmlControl control)
        {
            _htmlControl = control;
            init();
        }
        private string controlCssClass
        {
            get
            {
                if (_webControl != null)
                    return _webControl.CssClass;
                else if (_htmlControl != null)
                {
                    return _htmlControl.Attributes["class"];
                }
                return null;
            }
            set
            {
                if (_webControl != null)
                    _webControl.CssClass = value;
                else if (_htmlControl != null)
                {
                    _htmlControl.Attributes["class"] = value;
                }
            }
        }


        private void init()
        {
            {
                string css = controlCssClass ?? "" ;
                
                AddClass(false, css);
                
            }
        }

        private List<string> _cssClasses = new List<string>();

        public override void AddClass(params string[] cssClasses)
        {
            if (this._webControl is CS.General_v3.Controls.WebControls.Common.MyLinkButton)
            {
                string s = "";
            }
            this.AddClass(true, cssClasses);
        }

        public void AddClass(bool updateControlCSS, params string[] cssClasses)
        {
            if (this._webControl is CS.General_v3.Controls.WebControls.Common.MyLinkButton)
            {
                string s = "";
            }
            base.AddClass(cssClasses);

            if (updateControlCSS)
            {
                controlCssClass = this.ToString();
            }
        }

        public override void RemoveClass(params string[] cssClasses)
        {
            this.RemoveClass(true, cssClasses);
            
        }

        public void RemoveClass(bool updateControlCSS, params string[] cssClasses)
        {
            base.RemoveClass(cssClasses);

            if (updateControlCSS)
            {
                controlCssClass = this.ToString();
            }
        }
        public override void Clear()
        {
            this.Clear(true);
        }

        public void Clear(bool updateControlCSS)
        {
            base.Clear();
            if (updateControlCSS)
            {
                controlCssClass = "";
            }
        }
        
    }
}
