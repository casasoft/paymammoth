﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace CS.General_v3.Classes.Reflection.HelperClasses
{
    public class PropertyAttributeInfo
    {
        public PropertyInfo Property { get; set; }
        public List<Attribute> Attributes { get; set; }
        public PropertyAttributeInfo()
        {
            this.Attributes = new List<Attribute>();
        }
    }
}
