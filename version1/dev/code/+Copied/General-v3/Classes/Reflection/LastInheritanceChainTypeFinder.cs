﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace CS.General_v3.Classes.Reflection
{
    public class LastInheritanceChainTypeFinder
    {
        
						
		
        private static readonly LastInheritanceChainTypeFinder _instance = new LastInheritanceChainTypeFinder();

        public static LastInheritanceChainTypeFinder Instance
        {
            get { return _instance;  }
        }
        
        



        private Iesi.Collections.Generic.HashedSet<Assembly> _assemblies =
            new Iesi.Collections.Generic.HashedSet<Assembly>();
        /// <summary>
        /// For example, if A extends from B, B would be the Key and A the value
        /// </summary>
        //public Dictionary<Type, Type> isExtendedBy = null;
        public ConcurrentDictionary<Type, Type> lastTypeFor = null;
        private LastInheritanceChainTypeFinder()
        {
            
            checkloadFromFile();
            
            //if (isExtendedBy == null)
            //{
            //    isExtendedBy = new Dictionary<Type, Type>();
            //}
            if (lastTypeFor == null)
            {
                lastTypeFor = new ConcurrentDictionary<Type, Type>();
            }

        }
        public void AddAssembliesToCheck(IEnumerable<Assembly> assembliesToCheck)
        {
            _assemblies.AddAll(assembliesToCheck.ToList());
        }
        public void AddAssembliesToCheck(Assembly assembly)
        {
            _assemblies.Add(assembly);
        }

        private Type findTypeFromAssembliesThatInheritsFrom(Type t)
        {
            foreach (var assembly in _assemblies)
            {
                Type[] assemblyTypes = null;
                try
                {
                    assemblyTypes = assembly.GetTypes();
                }
                catch (ReflectionTypeLoadException ex)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine(ex.Message);
                    sb.AppendLine("----------------");
                    sb.AppendLine("Loader Exceptions");
                    sb.AppendLine("----------------");
                    if (ex.LoaderExceptions != null)
                    {
                        for (int i = 0; i < ex.LoaderExceptions.Length; i++)
                        {
                            var loaderEx = ex.LoaderExceptions[i];
                            sb.AppendLine("Loader Exception #" + (i+1) + ": " + loaderEx.Message);
                        }
                    }
                    throw new InvalidOperationException(sb.ToString(), ex);
                }
                for (int i=0; i < assemblyTypes.Length; i++)
                {
                    var assemblyType = assemblyTypes[i];

                    if (assemblyType.IsSubclassOf(t))
                    {
                        return assemblyType;
                    }



                    //03/jul/2012 [karl] this was changed to the above

                    //if (assemblyType.BaseType != null)
                    //{
                    //    //isExtendedBy[assemblyType.BaseType] = assemblyType; //store it so that for future types you dont need to check all types again
                    //    if ( assemblyType.BaseType == t)
                    //    {
                    //        return assemblyType;
                    //    }
                    //}

                }

            }
            return null;

        }

        private Type _getLastTypeFor(Type t)
        {
            
            Type lastOne = t;
            Type typeThatExtendsFrom = null;
            //if (isExtendedBy.ContainsKey(t)) //if the dictionary already knows directly a type that extends from , set that
            //{
            //    typeThatExtendsFrom = isExtendedBy[t];
            //}
            //else
            { //try to find from whole assemblies

                typeThatExtendsFrom = findTypeFromAssembliesThatInheritsFrom(t);
            }

            if (typeThatExtendsFrom != null) //if a type exists that exists from this, then try to find another that does
            {
                lastOne = _getLastTypeFor(typeThatExtendsFrom);
            }
            return lastOne;

        }

        public Type GetLastTypeFor(Type t)
        {
            Type returnType = null;
            if (!lastTypeFor.TryGetValue(t, out returnType))
            {
                Type lastType = _getLastTypeFor(t);

                lastTypeFor[t] = lastType;
                returnType = lastType;
            }
            return returnType;
            
        }

        public void ResetCacheForType(Type t)
        {
            Type deletedItem = null;
            
            lastTypeFor.TryRemove(t, out deletedItem);
        }

        public Type GetLastTypeFor<T>()
        {
            return _getLastTypeFor(typeof(T));
        }

        private const string CONFIG_SAVE_PATH = "/App_Data/nhSavedMappings/inheritanceChain.bin";
        public void SaveToFile()
        {
            string localPath = CS.General_v3.Util.PageUtil.MapPath(CONFIG_SAVE_PATH);
            //this was disabled as I dont think you can reload this from a file (13/10/2011, Karl)
            //CS.General_v3.Util.IO.SerializeObjectToFile(localPath, lastTypeFor);
            
        }
        private void checkloadFromFile()
        {
            string localPath = CS.General_v3.Util.PageUtil.MapPath(CONFIG_SAVE_PATH);
            if (File.Exists(localPath) && ! CS.General_v3.Settings.Database.CheckMappingsAndOthers)
            {
                //this was disabled as I dont think you can reload this from a file (13/10/2011, Karl)
                //lastTypeFor = CS.General_v3.Util.IO.DeserializeObjectFromFile(localPath) as Dictionary<Type, Type>;
                
            }

        }

    }
}
