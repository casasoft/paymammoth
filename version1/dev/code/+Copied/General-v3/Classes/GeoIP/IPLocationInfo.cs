﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace CS.General_v3.Classes.GeoIP
{
    [Serializable]
    public class IPLocationInfo 
    {
        public string IP { get; set; }
        public string CountryCode { get; set; }
        public CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? CountryCodeEnum { get; set; }

        public string CountryName {get;set;}
        public string City { get; set; }
        public double? Longtitude {get;set;}
        public double? Latitude {get;set;}

     
    }
}
