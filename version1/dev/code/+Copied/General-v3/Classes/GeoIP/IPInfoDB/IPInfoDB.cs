﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Net;
using System.Xml;
namespace CS.General_v3.Classes.GeoIP.IPInfoDB
{
    public static class IPInfoDB
    {
        private static Dictionary<string, IPInfoDBData> _storedRequests = new Dictionary<string, IPInfoDBData>();

        private static IPInfoDBData getCachedRequest(string ipAddress)
        {
            if (_storedRequests.ContainsKey(ipAddress))
            {
                return _storedRequests[ipAddress];
            }
            else
            {
                return null;
            }
            
        }
        private static IPInfoDBData getResponse_old(string IP, bool countryPrecision)
        {
            IPInfoDBData result = getCachedRequest(IP);
            if (result == null || (!countryPrecision && result.CountryPrecision))
            {
                //New Request
                string urlStr = countryPrecision ? "http://ipinfodb.com/ip_query_country.php" : "http://ipinfodb.com/ip_query.php";
                CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass(urlStr);
                url["ip"] = IP;

                System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                xmlDoc.Load(url.ToString());
                XmlNode xmlNode = xmlDoc.SelectSingleNode("Response");
                result = ParseXmlNode(xmlNode);
                _storedRequests.Remove(IP); // just in case
                _storedRequests[IP] = result;
                result.CountryPrecision = countryPrecision;


                if (String.Compare(result.CountryCode, "rd", true) == 0)
                {
                    //Default
                    result.CountryCode = "MT";
                    result.City = "Luqa";
                    result.CountryName = "Malta";
                }
            }


            return result;
        }
        private static string IPINFODB_KEY = "59ae0574250a16f4db0ecd823749100ac69622adb9a7bf9d3d9ad92aab4f6a7c";
        private static IPInfoDBData getResponse(string IP = null, bool countryPrecision = true)
        {
            IPInfoDBData result = getCachedRequest(IP);
            if (result == null || (!countryPrecision && result.CountryPrecision))
            {
                //New Request
                string urlStr = countryPrecision ? "http://api.ipinfodb.com/v2/ip_query_country.php" : "http://api.ipinfodb.com/v2/ip_query.php";
                CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass(urlStr);
                url["ip"] = IP;
                url["key"] = IPINFODB_KEY;

                System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                xmlDoc.Load(url.ToString());
                XmlNode xmlNode = xmlDoc.SelectSingleNode("Response");
                result = ParseXmlNode(xmlNode);
                _storedRequests.Remove(IP); // just in case
                _storedRequests[IP] = result;
                result.CountryPrecision = countryPrecision;


                if (String.Compare(result.CountryCode, "rd", true) == 0)
                {
                    //Default
                    result.CountryCode = "MT";
                    result.City = "Luqa";
                    result.CountryName = "Malta";
                }
            }


            return result;
        }

        public class IPInfoDBData
        {
            public bool CountryPrecision { get; set; }
            public string IP { get; set; }
            public string Status { get; set; }
            public string CountryCode { get; set; }
            public string CountryName { get; set; }
            public string RegionCode { get; set; }
            public string RegionName { get; set; }
            public string City { get; set; }
            public string ZipPostalCode { get; set; }
            public double? Latitude { get; set; }
            public double? Longitude { get; set; }
            public int? Timezone { get; set; }
            public int? Gmtoffset { get; set; }
            public int? Dstoffset { get; set; }

        }
        public static IPInfoDBData ParseXmlNode(XmlNode node)
        {
            IPInfoDBData data = new IPInfoDBData();
            try
            {
                
                    data.IP = CS.General_v3.Util.XML.GetNodeValue(node.SelectSingleNode("Ip"));
                    data.Status = CS.General_v3.Util.XML.GetNodeValue(node.SelectSingleNode("Status"));
                    data.CountryCode = CS.General_v3.Util.XML.GetNodeValue(node.SelectSingleNode("CountryCode"));
                    data.CountryName = CS.General_v3.Util.XML.GetNodeValue(node.SelectSingleNode("CountryName"));
                    data.RegionCode = CS.General_v3.Util.XML.GetNodeValue(node.SelectSingleNode("RegionCode"));
                    data.RegionName = CS.General_v3.Util.XML.GetNodeValue(node.SelectSingleNode("RegionName"));
                    data.City = CS.General_v3.Util.XML.GetNodeValue(node.SelectSingleNode("City"));
                    data.ZipPostalCode = CS.General_v3.Util.XML.GetNodeValue(node.SelectSingleNode("ZipPostalCode"));
                    data.Latitude = CS.General_v3.Util.XML.GetNodeValueDoubleNullable(node.SelectSingleNode("Latitude"), null);
                    data.Longitude = CS.General_v3.Util.XML.GetNodeValueDoubleNullable(node.SelectSingleNode("Longitude"), null);
                    data.Timezone = CS.General_v3.Util.XML.GetNodeValueIntNullable(node.SelectSingleNode("Timezone"), null);
                    data.Gmtoffset = CS.General_v3.Util.XML.GetNodeValueIntNullable(node.SelectSingleNode("Gmtoffset"), null);
                    data.Dstoffset = CS.General_v3.Util.XML.GetNodeValueIntNullable(node.SelectSingleNode("Dstoffset"), null);
                
            }
            catch (Exception ex)
            {
                Error.Reporter.ReportError(Enums.LOG4NET_MSG_TYPE.Warn, ex);
            }
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="IP"></param>
        /// <param name="countryPrecision">Faster.  It will load only country data and not city specific data</param>
        /// <returns></returns>
        public static string GetCountryCode(string IP, bool countryPrecision)
        {
            IPInfoDBData result = getResponse(IP, countryPrecision);
            return result.CountryCode;
        }
    }
}
