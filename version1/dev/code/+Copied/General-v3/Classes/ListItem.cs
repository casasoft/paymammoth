﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CS.General_v3.Classes
{
    [Serializable]
    public class ListItem
    {
        
        private string _Text;

        public string Text
        {
          get { return _Text; }
          set { _Text = value; }
        }
        private string _Value;

        public string Value
        {
          get { return _Value; }
          set { _Value = value; }
        }
        public ListItem()
        {
            _Text = "";
            _Value = "";
        }
        public ListItem(string title)
        {
            _Text = _Value = title;
        }
        public ListItem(string title, object value)
        {
            _Text = title;
            
            if (value is Enum)
                _Value = ((int)value).ToString();
            else
                
            {
                if (value != null)
                    _Value = value.ToString();
                else
                    _Value = "";
            }
            
        }
    }
}
