﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.MediaItems;

namespace CS.General_v3.Classes.NewsItems
{
    public interface INewsItem
    {
        int ID { get; set; }
        string Title { get; set; }
        string ShortDescription { get; set; }
        string FullDescription { get; set; }
        /// <summary>
        /// A boolean that shows if the news is archived or not
        /// </summary>
        bool Archive { get; set; }
        DateTime DatePosted { get; set; }
        IMediaItem Image { get; }
        void Save();
        void Remove();

    }
}
