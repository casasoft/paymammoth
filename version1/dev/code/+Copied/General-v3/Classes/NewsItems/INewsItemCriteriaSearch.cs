﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.NewsItems
{
    public interface INewsItemCriteriaSearch
    {
        string Title { get; set; }
        DateTime? DatePostedFrom { get; set; }
        DateTime? DatePostedTo { get; set; }
        void SetSortBy(string sortBy);
        bool? Archive { get; set; }

    }
}
