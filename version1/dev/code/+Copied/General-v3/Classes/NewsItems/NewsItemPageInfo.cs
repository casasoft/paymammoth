﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.NewsItems
{
    public class NewsItemPageInfo
    {
        public string Title_SingleLowercase = "news item";
        public string Title_SingleUppercase = "News Item";
        public string Title_PluralLowercase = "news items";
        public string Title_PluralUppercase = "News Items";
        public string EditPageUrl = "news.aspx";
        public string Folder = "news";
        public NewsItemPageInfo()
        { }

    }
}
