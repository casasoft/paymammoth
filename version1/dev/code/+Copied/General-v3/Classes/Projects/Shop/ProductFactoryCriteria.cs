﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_20090518.DB.SQL;
namespace CS.General_20090518.Classes.Projects.Shop
{
    public class ProductFactoryCriteria
    {
        
        public class CATEGORY_FEATURE_PARAM
        {
            public int CategoryFeatureID { get; set; }
            public List<int> CategoryFeatureValueIDs { get; set; }
            public string GetTableJoinName()
            {
                return Settings.DB.ProductFeatureValues.TABLE_NAME + "_" + CategoryFeatureID;
            }
            public LeftJoin GetAsLeftJoin()
            {
                string tbName = GetTableJoinName();
                LeftJoin leftJoin = new LeftJoin(Settings.DB.ProductFeatureValues.TABLE_NAME, tbName);
                leftJoin.Conditions.Add(Condition.CreateColumnCondition(Settings.DB.Product.TABLE_NAME + "." + Settings.DB.Product.Columns.ID,
                    tbName + "." + Settings.DB.ProductFeatureValues.Columns.ProductID));
                leftJoin.Conditions.Add(Condition.CreateCondition(tbName + "." + Settings.DB.ProductFeatureValues.Columns.CategoryFeatureID,
                    this.CategoryFeatureID));

                return leftJoin;
            }
            public Condition GetCondition()
            {

                string tbName = GetTableJoinName();
                Condition c = null;
                if (this.CategoryFeatureValueIDs != null && this.CategoryFeatureValueIDs.Count > 0)
                {
                    c = Condition.CreateCondition(tbName + "." + Settings.DB.ProductFeatureValues.Columns.CategoryFeatureValueID,
                        this.CategoryFeatureValueIDs, Condition.OPERATION_TYPE.In);
                }
                return c;
            }
            
        }
        //Fill any extra Search properties here

        public virtual string Keywords { get; set; }
        public virtual List<CATEGORY_FEATURE_PARAM> CategoryFeatureValues { get; set; }
        public virtual void FillForFrontend()
        {

        }
    }
}
