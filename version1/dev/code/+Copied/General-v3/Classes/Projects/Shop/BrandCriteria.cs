﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_20090518.Classes.Projects.Shop
{
    public partial class Brand
    {
        public virtual string Title { get; set; }
        public override string ToString()
        {
            return this.Title;

        }

        
        private void checkIfCanDelete()
        {
            Product.Criteria crit = new BaseProduct.Criteria();
            crit.Search.BrandID = this.ID;
            var list = Product.GetList(crit);
            if (list.Count > 0)
                _allowRemove = false;
        }
        
        private void updateSearchFields()
        {
            this.Title_Search = CS.General_20090518.Util.Text.ConvertStringForSearch(this.Title);

        }
        /*
        public override void Save(bool saveProducts)
        {
            updateSearchFields();
            base.Save(saveProducts);
        }*/
        
    }
}
