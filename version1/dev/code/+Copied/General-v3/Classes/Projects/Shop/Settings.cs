﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_20090518.Classes.Projects.Shop
{
    public static class Settings
    {
        public static class DB
        {
            public static class Product
            {
                public static string TABLE_NAME = "Products";
                public static class Columns
                {
                    public static string ID = "ID";

                }
            }
            public static class ProductFeatureValues
            {
                public static string TABLE_NAME = "ProductFeatureValues";
                public static class Columns
                {
                    public static string ID = "ID";
                    public static string ProductID = "ProductID";
                    public static string CategoryFeatureID = "CategoryFeatureID";
                    public static string CategoryFeatureValueID = "CategoryFeatureValueID";

                }
            }
            

        }
    }
}
