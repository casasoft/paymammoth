﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.SearchParams
{
    public class SearchParamsProductFeatureList : List<SearchParamsProductFeature>
    {

        public override string ToString()
        {
            string s = "";
            for (int i = 0; i < Count; i++)
            {
                if (i > 0)
                {
                    s += ";";
                }
                s += this[i].ToString();
            }
            return s;
        }

        public static SearchParamsProductFeatureList FromString(string s)
        {
            SearchParamsProductFeatureList featList = new SearchParamsProductFeatureList();
            if (!string.IsNullOrEmpty(s))
            {
                var data = s.Split(';');
                foreach (var item in data) { featList.Add(SearchParamsProductFeature.FromString(item)); }
            }
            return featList;
        }

    }
}
