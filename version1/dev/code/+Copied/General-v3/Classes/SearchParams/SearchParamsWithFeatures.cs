﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PG = CS.General_v3.Util.PageUtil;
using CS.General_v3.Classes.URL;

namespace CS.General_v3.Classes.SearchParams
{
    public class SearchParamsWithFeatures : SearchParams_v2
    {
        private const string PARAM_FILTERS = "filters";

        public SearchParamsWithFeatures()
            : this(null, null)
        {

        }
        public SearchParamsWithFeatures(string id, string url)
            : base(id, url)
        {
            init();
        }
        private void init()
        {
            parseQS();

        }
        public virtual void parseQS()
        {
            this.AddParameterQueryString(PARAM_FILTERS,null, false, false);
        }

        public SearchParamsProductFeatureList ProductFeatures
        {
            get
            {
                string strValue = this.GetParameter("filters").GetValue();
                return SearchParamsProductFeatureList.FromString(strValue);
            }
            set
            {
                if (value != null)
                {
                    this.GetParameter("filters").SetValue(value.ToString());
                    
                }
                else
                {
                    this.GetParameter("filters").SetValue( (string)null);
                }
           }
        }
        public override string GetUrl(string customURL = null, bool fullyQualifiedUrl = true)
        {
            this.Url["filters"] = this.ProductFeatures.ToString();
            var url = base.GetUrl(customURL, fullyQualifiedUrl);
            return url;
        }
        

    }
}
