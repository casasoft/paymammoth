﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.SearchParams
{
    public class SearchParamsProductFeature
    {
        public string ID { get; set; } 
        public List<string> Values { get; set; }

        public SearchParamsProductFeature()
        {
            Values = new List<string>();
        }
        
        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            str.Append(ID + ",");
            foreach (var val in Values)
            {
                if (val.Equals(Values.LastOrDefault()))
                {
                    str.Append(val);
                }
                else
                {
                    str.Append(val + ",");
                }
            }
            return str.ToString();
        }

        public static SearchParamsProductFeature FromString(string s)
        {
            SearchParamsProductFeature pFeature = new SearchParamsProductFeature();
            var splittedString = s.Split(',');
            pFeature.ID = splittedString[0].ToString();
            for (int i = 1; i < splittedString.Length; i++)
            {
                pFeature.Values.Add(splittedString[i]);
            }
            return pFeature;
        }
    }
}
