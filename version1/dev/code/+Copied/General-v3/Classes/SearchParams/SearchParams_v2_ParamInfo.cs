﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace CS.General_v3.Classes.SearchParams
{
    public class SearchParams_v2_ParamInfo
    {
        public string Identifier { get; set; }
        public bool UsesSession { get; set; }
        public bool UsesCookies { get; set; }
        private SearchParams_v2_Base searchParams = null;
        public SearchParams_v2_ParamInfo(SearchParams_v2_Base searchParams, string identifier, bool usesSession = false, bool usesCookies = false)
        {
            this.searchParams = searchParams;
            this.Identifier = identifier;
            this.UsesCookies = usesCookies;
            this.UsesSession = usesSession;

        }
        

        public string GetValue(string defaultValue = null)
        {
            string key = searchParams.getKey(this.Identifier);
            string s = (string)searchParams.Url[key];
            string sessionKey = searchParams.getSessionCookieKey(this.Identifier);
                
            if (string.IsNullOrEmpty(s) && UsesSession)
            {
                s = (string)CS.General_v3.Util.PageUtil.GetSessionObject(sessionKey);
            }
                
            if (UsesCookies && string.IsNullOrEmpty(s))
            {
                s = CS.General_v3.Util.PageUtil.GetCookie(sessionKey);
            }
            return s;
        }
        public IList<string> GetValueAsListOfString(string delimeter = ",")
        {
            List<string> list = new List<string>();
            string s = GetValue() ?? "";
            string[] tokens = s.Split(new string[] { delimeter },  StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < tokens.Length; i++)
            {
                string t = tokens[i];
                if (!string.IsNullOrEmpty(t))
                {
                    list.Add(t);
                }

            }
            return list;
        }
        public List<long> GetValueAsListOfLong(string delimeter = ",")
        {
            List<long> list = new List<long>();
            IList<string> strList = GetValueAsListOfString(delimeter);
            for (int i = 0; i < strList.Count; i++)
            {
                string s = strList[i];
                long num = 0;
                if (Int64.TryParse(s, out num))
                {
                    list.Add(num);
                }

            }
            return list;
        }
        public IList<int> GetValueAsListOfInteger(string delimeter = ",")
        {
            return GetValueAsListOfLong(delimeter).ConvertAll<int>(item => (int)item);
        }
        public IList<EnumType> GetValueAsListOfEnums<EnumType>(string delimeter = ",")
            
            where EnumType : struct
        {
            
            List<EnumType> list = new List<EnumType>();
            var strList = GetValueAsListOfString(delimeter);
            for (int i = 0; i < strList.Count; i++)
            {
                string sVal = strList[i];
                EnumType val ;
                if (Enum.TryParse<EnumType>(sVal,out val))
                    list.Add(val);
            }
            return list;
        }

        public double? GetValueAsDouble(double? defaultValue = null)
        {
            double? num = null;
            double tmp =0;
            string s = GetValue((defaultValue != null ? defaultValue.Value.ToString("0.00000") : null));
            if (double.TryParse(s, out tmp))
                num = tmp;
            return num;
        }
        public int? GetValueAsInt(int? defaultValue = null)
        {
            double? d = GetValueAsDouble((defaultValue.HasValue ? (double?)defaultValue.Value : null));
            return (d.HasValue ? (int?)d.Value : null);
            
        }
        public long? GetValueAsLong(int? defaultValue = null)
        {
            double? d = GetValueAsDouble((defaultValue.HasValue ? (double?)defaultValue.Value : null));
            return (d.HasValue ? (long?)d.Value : null);

        }
        public bool? GetValueAsBool(bool? defaultValue = null)
        {
            string s =GetValue((defaultValue.HasValue ? CS.General_v3.Util.Other.BoolToStr(defaultValue.Value) : null));
            bool? b = CS.General_v3.Util.Other.TextToBoolNullable(s);
            return b;
        }
        private void setValue(string value)
        {
            string key = searchParams.getKey(this.Identifier);
            string sessionKey = searchParams.getSessionCookieKey(this.Identifier);
            searchParams.Url[key] = value;
            
            if (UsesSession)
            {
                CS.General_v3.Util.PageUtil.SetSessionObject(sessionKey, value);
            }
            if (UsesCookies)
            {
                CS.General_v3.Util.PageUtil.SetCookie(sessionKey, value);
            }
        }
        public void SetValue(string value)
        {
            this.setValue( value);
        }
        public void SetValue(int? value)
        {
            this.setValue( (value != null ? value.Value.ToString() : null));
        }
        public void SetValue(Enum value)
        {
            int? iVal = null;
            if (value != null)
                iVal = (int) (object)value;

            this.SetValue(iVal);
        }
        public void SetValue(long? value)
        {
            this.setValue((value != null ? value.Value.ToString() : null));
        }
        public void SetValue(double? value)
        {
            this.setValue( (value != null ? value.Value.ToString("0.0000") : null));
        }
        public void SetValue(DateTime? value)
        {
            this.setValue( (value != null ? value.Value.ToString("yyyyMMddhhmmss") : null));
        }
        public void SetValue(bool? value)
        {
            this.setValue( (value != null ? CS.General_v3.Util.Other.BoolToStr(value.Value) : null));
        }
        public void SetValue(IEnumerable<string> value, string delimeter = ",")
        {
            this.setValue(value, delimeter);
        }
        public void SetValue(IEnumerable<int?> value, string delimeter = ",")
        {
            this.setValue(value, delimeter);
        }
        public void SetValue(IEnumerable<int> value, string delimeter = ",")
        {
            this.setValue(value, delimeter);
        }
        public void SetValue(IEnumerable<long> value, string delimeter = ",")
        {
            this.setValue(value, delimeter);
        }
        public void SetValue(IEnumerable<double?> value, string delimeter = ",")
        {
            this.setValue(value, delimeter);
        }
        public void SetValue(IEnumerable<DateTime?> value, string delimeter = ",")
        {
            this.setValue(value, delimeter);
        }
        public void SetValue(IEnumerable<bool?> value, string delimeter = ",")
        {
            this.setValue(value, delimeter);
        }
        public void SetValueFromListOfEnum<EnumType>(IEnumerable<EnumType> value, string delimeter = ",") 
            where EnumType : struct
        {

            this.setValue(value, delimeter);
        }
        public void SetValue(IEnumerable<Enum> value, string delimeter = ",")
        {
            
            this.setValue(value, delimeter);
        }
        public void setValue(IEnumerable value, string delimeter = ",")
        {
            StringBuilder sb = new StringBuilder();
            var enumerator = value.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (sb.Length >0) sb.Append(delimeter);
                var curr = enumerator.Current;
                if (curr != null)
                {
                    string s = "";
                    if (curr is Enum)
                        s = ((int)curr).ToString();
                    else
                        s = curr.ToString();

                    sb.Append(s);
                }
            }
            

            this.setValue(sb.ToString());
        }

    }
}
