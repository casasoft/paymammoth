﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Specialized.Paging;

namespace CS.General_v3.Classes.SearchParams
{
    public class SearchParamsPagingInfo_v2 : IPagingInfo
    {
        private SearchParams_v2_Base _searchParams = null;

        public SearchParams_v2_Base SearchParams
        {
            get { return _searchParams; }
            set { _searchParams = value; }
        }
        
        public string StrPrepend { get; set; }
        public string StrAppend { get; set; }



        /// <summary>
        /// Initialises a search params.
        /// </summary>
        /// <param name="ValuePrepend">Value Prepend is a string to prepend the value, e.g pageNo would be "prepend_[value]", etc</param>
        public SearchParamsPagingInfo_v2(SearchParams_v2_Base searchParams, string strPrepend = null, string strAppend = null, bool clearValues = false)
        {
            this._searchParams = searchParams;
            this.StrPrepend = strPrepend;
            this.StrAppend = strAppend;
            this.DefaultShowAmtValue = 10;
            //this.DefaultSortByValue = "";

            searchParams.AddParameterQueryString(getKey(SearchParams_v2.PARAM_PAGENO),"1", false, false);
            searchParams.AddParameterQueryString(getKey(SearchParams_v2.PARAM_SHOWAMT), null, true, true);
            searchParams.AddParameterQueryString(getKey(SearchParams_v2.PARAM_SORTBY), null, true, true);

            if (clearValues)
            {
                //this.SortByValue = null;
                //this.ShowAmount = this.DefaultShowAmtValue;
                //this.PageNo = 1;
            }
        }

        private string getKey(string key)
        {
            return this.StrPrepend + key + this.StrAppend;
        }

        public string GetPageNo_QuerystringVar()
        {
            return getKey(SearchParams_v2.PARAM_PAGENO);
        }

        private SearchParams_v2_ParamInfoBase getPageNoParamInfo()
        {
            return this._searchParams.GetParameter(GetPageNoKey());
        }
        public int PageNo 
        {
            get
            {
                int? pgNo = getPageNoParamInfo().GetValueAsInt();
                if (pgNo.GetValueOrDefault() < 1)
                    pgNo = 1;
                return pgNo.Value;
            }
            set
            {
                int val = value;
                if (val < 1) val = 1;
                getPageNoParamInfo().SetValue(val);
            }
        }
        public SearchParams_v2_ParamInfoBase GetShowAmountParamInfo()
        {
            return this._searchParams.GetParameter(GetShowAmtKey());
        }
        public int ShowAmount 
        {
            get
            {
                int? num = GetShowAmountParamInfo().GetValueAsInt();
                if (num == null)
                {
                    if (this.DefaultShowAmtValue == null)
                    {

                        num = 10;
//                        throw new Exception("Please fill in DefaultShowAmtValue");
                    }
                    else
                    {
                        num = this.DefaultShowAmtValue;
                    }
                }
                return num.Value;
            }
            set
            {
                GetShowAmountParamInfo().SetValue(value);
            }
        }
        private SearchParams_v2_ParamInfoBase getSortByParamInfo()
        {
            return this._searchParams.GetParameter(GetSortByKey());
        }
        public string GetPageNoKey()
        {
            return getKey(SearchParams_v2.PARAM_PAGENO);
        }
        public string GetSortByKey()
        {
            return getKey(SearchParams_v2.PARAM_SORTBY);
        }
        public string GetShowAmtKey()
        {
            return getKey(SearchParams_v2.PARAM_SHOWAMT);
        }
        public SearchParams_v2_ParamInfoQueryString GetSortByParameter()
        {
            return (SearchParams_v2_ParamInfoQueryString)getSortByParamInfo();
        }

        public string SortByValue 
        {
            get
            {
                string s=getSortByParamInfo().GetValue();
                if (string.IsNullOrEmpty(s))
                {
                    if (this.DefaultSortByValue == null)
                    {
                        //throw new Exception("Please fill in DefaultSortByValue");
                    }
                    s = this.DefaultSortByValue;
                }
                return s;
            }
            set
            {
                getSortByParamInfo().SetValue(value);
            }
        }

       public void SetSortByValueFromEnum<TEnum>(TEnum value) where TEnum: struct
       {
           
           this.SortByValue = Convert.ToInt32(value).ToString();

       }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEnum">Make sure this is an enum</typeparam>
        /// <returns></returns>
       public TEnum GetSortByValueAsEnum<TEnum>(TEnum defaultValue ) where TEnum: struct
       {
           string value = SortByValue;


           return (TEnum)(object)CS.General_v3.Util.EnumUtils.GetEnumByStringValue(typeof(TEnum), value, (Enum)(object)defaultValue);
           /*if (!string.IsNullOrEmpty(value))
           {
               int nValue = 0;
               if (int.TryParse(value, out nValue))
               {
                   return 
               }
               else
               {
                   return default(T);
               }
           }
           else
           {
               return default(T);
           }*/
       }

       public int DefaultShowAmtValue { get; set; }
       public string DefaultSortByValue { get; set; }



      
       #region IPagingInfo Members


       int? IPagingInfo.PageNo
       {
           get
           {
               return PageNo;
           }
           set
           {
               PageNo = PageNo = value.HasValue ? value.Value : 1;
           }
       }

       int? IPagingInfo.ShowAmount
       {
           get
           {
               return ShowAmount;
           }
           set
           {
               ShowAmount = value.HasValue ? value.Value : 1;
           }
       }

       #endregion
    }
}
