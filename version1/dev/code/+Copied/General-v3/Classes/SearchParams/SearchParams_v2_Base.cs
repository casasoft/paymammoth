﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.URL;
using CS.General_v3.Controls.WebControls.Specialized.Paging;

namespace CS.General_v3.Classes.SearchParams
{
    [Obsolete("Use BusinessLogic.Classes.Url.ListingUrlParser")]
    public abstract class SearchParams_v2_Base
    {
        private Dictionary<string, SearchParams_v2_ParamInfoBase> _paramInfos = null;

        public string DefaultRouteName { get; set; }
        public URLClass Url { get; private set; }
        public string ID { get;set; }

        private bool _loadCurrentURLIfNull;
       
        internal protected string getKey(string identifier)
        {
            string key = identifier.ToLower();
            if (!string.IsNullOrEmpty(this.ID))
                key += "_" + this.ID.ToLower();
            return key;
        }
        internal protected string getSessionCookieKey(string identifier)
        {
            string key = getKey(identifier);
            key = "searchParams_" + key;
            return key;
        }
        public SearchParams_v2_Base(string id, string urlToLoad = null, bool loadCurrentUrlIfNull = true)
        {
            _loadCurrentURLIfNull = loadCurrentUrlIfNull;
            this.ID = id ?? "";
            if (urlToLoad == null && loadCurrentUrlIfNull)
            {
                urlToLoad = CS.General_v3.Util.PageUtil.GetCurrentUrlLocation().GetURL(fullyQualified:false);
            }

            this.Url = new CS.General_v3.Classes.URL.URLClass(urlToLoad, _loadCurrentURLIfNull);
            this._paramInfos = new Dictionary<string, SearchParams_v2_ParamInfoBase>();

        }
        private bool hasGotRoutingParameters()
        {
            foreach(var key in _paramInfos.Keys)
            {
                if (_paramInfos[key] is SearchParams_v2_ParamInfoRouteData)
                {
                    return true;
                }
            }
            return false;
        }
        private void addParameter(SearchParams_v2_ParamInfoBase paramInfo, string identifier)
        {
            string key = getKey(identifier);
            if (_paramInfos.ContainsKey(key))
                _paramInfos.Remove(key);
            _paramInfos.Add(key, paramInfo);
        }
        public SearchParams_v2_ParamInfoQueryString AddParameterQueryString(string identifier, string defaultValue, bool usesSession = false, bool usesCookies = false)
        {
            SearchParams_v2_ParamInfoQueryString paramInfo = new SearchParams_v2_ParamInfoQueryString(this, identifier, usesSession, usesCookies);
            paramInfo.DefaultValue = defaultValue;
            addParameter(paramInfo, identifier);
            return paramInfo;
        }
        public void RemoveParameter(string identifier)
        {
            string key = getKey(identifier);
            if (_paramInfos.ContainsKey(key))
                _paramInfos.Remove(key);
        }
        public SearchParams_v2_ParamInfoRouteData AddParameterRouteData(string identifier, string defaultValue, bool decodeValuesFromRouteData = false)
        {
            SearchParams_v2_ParamInfoRouteData paramInfo = new SearchParams_v2_ParamInfoRouteData(this, identifier, decodeValuesFromRouteData);
            paramInfo.DefaultValue = defaultValue;
            addParameter(paramInfo, identifier);
            return paramInfo;
        }
        public SearchParams_v2_ParamInfoBase GetParameter(string identifier)
        {
            SearchParams_v2_ParamInfoBase result = null;
            string key = getKey(identifier);
            if (_paramInfos.ContainsKey(key))
                result =_paramInfos[key];
            else
                throw new InvalidOperationException("SearchParams_v2:: Parameter with identifier <" + identifier + "> does not exist");
            return result;
        }

        public virtual string GetQuerystring()
        {
            return Url.GetQueryString();
        }

        /// <summary>
        /// Override this if routename is dependant on parameters
        /// </summary>
        /// <returns></returns>
        protected virtual string getRouteName()
        {
            return DefaultRouteName;
        }

        /// <summary>
        /// Override this for custom RouteURL params
        /// </summary>
        /// <param name="fullyQualifiedUrl"></param>
        /// <returns></returns>
        protected string getRouteURL(bool fullyQualifiedUrl = true)
        {
            Routing.MyRouteValueDictionary values = new Routing.MyRouteValueDictionary();
            foreach( string key in _paramInfos.Keys) {
                SearchParams_v2_ParamInfoBase param = _paramInfos[key];
                if (param is SearchParams_v2_ParamInfoRouteData) {
                    string value = param.GetValue();
                    values.Add(param.Identifier, CS.General_v3.Util.RoutingUtil.ConvertStringForRouteUrl(value));
                }
            }

            string url = CS.General_v3.Util.RoutingUtil.GetRouteUrl(getRouteName(), values, fullyQualifiedUrl);
            return url;
        }
        
        public virtual string GetUrl(string customURL = null, bool fullyQualifiedUrl = true)
        {
            
            if (hasGotRoutingParameters())
            {
                if (!string.IsNullOrEmpty(customURL))
                {
                    throw new Exception("If this SearchParams is using Routing ("+DefaultRouteName+"), then you can't specify customURL ("+customURL+")");
                }
                customURL = getRouteURL(fullyQualifiedUrl);
            }
            Url.ParseURL(customURL, parseQuerystringVariables: false);
            
            return Url.GetURL( fullyQualified: fullyQualifiedUrl);
        }

        public virtual void Redirect(string customURL = null, bool fullyQualifiedUrl = true)
        {
            string url = GetUrl(customURL: customURL, fullyQualifiedUrl: fullyQualifiedUrl);
            CS.General_v3.Util.PageUtil.RedirectPage(url);

        }


    }
}
