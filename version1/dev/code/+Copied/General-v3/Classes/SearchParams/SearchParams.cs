﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Collections;
using System.Web;
namespace CS.General_v3.Classes.SearchParams
{
    /// <summary>
    /// Used to store search params / settings in session and also update them from Querystring.
    /// 
    /// Basically, if you want to specify new parameters, add them to the Parameters list and specify whether the parameter
    /// is stored in session or just taken from QS
    /// 
    /// Ex. Show amount is stored in Session, also Sort By.
    /// Category ID is not, and even page is not.
    /// 
    /// You can also specify DefaultValues for parameters.  What you should do is, extend this class with your website specific
    /// SearchParameters and then set the default value for parameters like ParamShowAmt.DefaultValue = '500' etc...
    /// This way, then, the values of the parameters will result in not null values, but the default values.
    /// 
    /// You then use this searchParams to interface with common components, such as PagingBar and PagingBarAndBreadcrumbs ...
    /// </summary>
    [Obsolete("Use BusinessLogic.Classes.Url.ListingUrlParser")]
    public abstract class SearchParams
    {
        private CS.General_v3.Classes.URL.URLClass _currURL;
        public static string PARAM_CATEGORY = "category";
        public static string PARAM_SORTBY = SearchParams_v2.PARAM_SORTBY;
        public static string PARAM_SHOW = SearchParams_v2.PARAM_SHOWAMT;
        public static string PARAM_KEYWORDS = "keywords";
        public static string PARAM_PG = "pg";

        /// <summary>
        /// This class is used to specify whether this parameter is just taken from QS, or else it is stored 
        /// in session whenever found different in QS
        /// </summary>
        public class SearchParamDetails
        {
            private SearchParams _searchParams = null;
            public string Param { get; set; }
            /// <summary>
            /// If this paramter is disabled, it will not appear in querystrings
            /// </summary>
            public bool Disabled { get; set; }
            public bool StoreInSession { get; set; }
            /// <summary>
            /// Sets the default value of the item should it be empty
            /// </summary>
            public string DefaultValue { get; set; }
            private string _Value = null;
            public int? GetValueAsInt()
            {
                int? val = null;
                if (!string.IsNullOrEmpty(Value))
                {
                    int num = 0;
                    if (Int32.TryParse(Value, out num))
                        val = num;

                }
                return val;
            }
            /// <summary>
            /// Returns either the value or the default value of the item if value is empty
            /// </summary>
            public string Value
            {
                get
                {

                    return string.IsNullOrEmpty(_Value) ? DefaultValue : _Value;
                }
                set
                {

                    _Value = value;
                    if (this.StoreInSession && !string.IsNullOrEmpty(value))
                        _searchParams.setSessionValue(this.Param, this.Value);


                }
            }
            public SearchParamDetails(SearchParams searchParams, string ParamName, bool storeInSession, bool loadFromQS)
            {
                this._searchParams = searchParams;
                this.Param = ParamName;
                this.StoreInSession = storeInSession;
                if (loadFromQS)
                {
                    this.Value = _searchParams.getQueryStringValue(this.Param);

                }
                if (string.IsNullOrEmpty(this.Value) && storeInSession)
                {
                    this.Value = _searchParams.getSessionValue(this.Param);
                }
                this.Disabled = false;

            }

        }

        //private NameValueCollection _parameters = new NameValueCollection();

        public SearchParamDetails ParamPage { get { return this[PARAM_PG]; } }
        public SearchParamDetails ParamSortBy { get { return this[PARAM_SORTBY]; } }
        public SearchParamDetails ParamShowAmt { get { return this[PARAM_SHOW]; } }
        public SearchParamDetails ParamKeywords { get { return this[PARAM_KEYWORDS]; } }
        public SearchParamDetails ParamCategory { get { return this[PARAM_CATEGORY]; } }

        private Hashtable parameters { get; set; }
        private string _id;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">An id to prefix session vars</param>
        /// <param name="url">The URL to copy.  If left null, the default URL is taken</param>
        public SearchParams(string id, string url)
        {
            this.Path = url;
            this._currURL = new CS.General_v3.Classes.URL.URLClass(url);
            this.parameters = new Hashtable();
            _id = id;
            AddParameter(PARAM_PG, false, true, "1");
            AddParameter(PARAM_SORTBY, true, true, null);
            AddParameter(PARAM_SHOW, true, true, null);
            AddParameter(PARAM_KEYWORDS, false, true, null);
            AddParameter(PARAM_CATEGORY, false, true, null);
            this.Path = _currURL.Path;


        }
        public SearchParamDetails AddParameter(string ParamName, bool storeInSession, bool loadFromQS)
        {
            return AddParameter(ParamName, storeInSession, loadFromQS, null);
        }
        public SearchParamDetails AddParameter(string ParamName, bool storeInSession, bool loadFromQS, string defaultValue)
        {
            var param = new SearchParamDetails(this, ParamName, storeInSession, loadFromQS);
            param.DefaultValue = defaultValue;
            parameters[ParamName] = param;
            return param;
        }

        public SearchParams()
            : this("search", null)
        {

        }
       
        protected string getQueryStringValue(string param)
        {
            return (string)_currURL[param];
        }
        protected void setSessionValue(string param, string value)
        {
            string sID = getSessionId(param);
            HttpContext.Current.Session[sID] = value;
        }
        protected string getSessionValue(string param)
        {
            string sID = getSessionId(param);
            return (string)HttpContext.Current.Session[sID];
        }
        private string getSessionId(string paramName)
        {
            paramName = paramName ?? "";
            paramName = paramName.ToLower();
            return _id + "_" + paramName;
        }


        public void SetValue(string paramName, object value, bool? saveInSession)
        {
            string sVal = null;
            if (value != null)
                sVal = value.ToString();
            var param = this[paramName];

            if (param == null)
            {
                param = this.AddParameter(paramName, saveInSession.GetValueOrDefault(false), true);
            }
            if (saveInSession.HasValue)
                param.StoreInSession = saveInSession.Value;
            param.Value = sVal;


        }
        public int? GetValueAsInt(string param)
        {
            string s = GetValue(param);
            int? ret = null;
            if (!string.IsNullOrEmpty(s))
            {
                int num = 0;
                if (Int32.TryParse(s, out num))
                    ret = num;
            }
            return ret;
        }

        public bool? GetValueAsBool(string param)
        {
            string s = GetValue(param);
            bool? ret = null;
            if (!string.IsNullOrEmpty(s))
            {
                bool data = false;
                if (Boolean.TryParse(s, out data))
                    ret = data;
            }
            return ret;
        }

        public string GetValue(string param)
        {
            var p = this[param];
            string s = null;
            if (p != null)
                s = p.Value;
            return s;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="paramName"></param>
        /// <param name="updateSession">Whether to update the Session variables or just take them from QueryString.  E.g. pg / category ID are taken from QS,
        /// while Sort by / show amt are stored in session</param>
        /// <returns></returns>
        protected string getValue(string paramName)
        {
            var p = this[paramName];
            string s = "";
            if (p != null)
                s = p.Value;
            return s;

        }


        public int Page
        {
            get
            {

                return this.ParamPage.GetValueAsInt().GetValueOrDefault(1);
            }
            set
            {
                this.ParamPage.Value = value.ToString();
            }
        }
        public int? ShowAmt
        {
            get
            {
                return this.ParamShowAmt.GetValueAsInt();
            }
            set
            {
                this.ParamShowAmt.Value = value.ToString();
            }
        }
        public string Keywords
        {
            get
            {
                return this.ParamKeywords.Value;
            }
            set
            {
                this.ParamKeywords.Value = value.ToString();
            }
        }

        public string SortBy
        {
            get
            {
                return this.ParamSortBy.Value;
            }
            set
            {
                this.ParamSortBy.Value = value.ToString();
            }
        }
        public virtual int? CategoryID
        {
            get
            {
                return this.ParamCategory.GetValueAsInt();
            }
            set
            {
                this.ParamCategory.Value = value.ToString();
            }
        }

        private SearchParamDetails getFromHashTable(string key)
        {
            key = key ?? "";
            //key = key.ToLower();
            return (SearchParamDetails)this.parameters[key];
        }
        private void setFromHashTable(string key, SearchParamDetails param)
        {
            key = key ?? "";
            key = key.ToLower();
            if (param != null)
                this.parameters[key] = param;
            else
                this.parameters.Remove(key);
        }
        public SearchParamDetails this[string key]
        {
            get
            {

                return getFromHashTable(key);
            }
            set
            {

                setFromHashTable(key, value);
            }

        }
        public SearchParamDetails this[int index]
        {
            get
            {
                List<SearchParamDetails> list = CS.General_v3.Util.ListUtil.EnumerateHashTableValues<SearchParamDetails>(this.parameters);
                return list[index];
            }
        }
        public int Count
        {
            get
            {
                List<SearchParamDetails> list = CS.General_v3.Util.ListUtil.EnumerateHashTableValues<SearchParamDetails>(this.parameters);
                return list.Count;
            }
        }
        public virtual string Path { get; set; }
       // public string Url { get; set; }


        protected virtual void initialiseExtraParameters()
        {

        }
        public virtual CS.General_v3.Classes.URL.URLClass GetURL()
        {
            initialiseExtraParameters();


            CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass(_currURL.GetURL());
            url.ClearQueryString();

            url.Path = Path;
            List<SearchParamDetails> list = CS.General_v3.Util.ListUtil.EnumerateHashTableValues<SearchParamDetails>(this.parameters);
            for (int i = 0; i < list.Count; i++)
            {
                var p = list[i];
                if (!p.Disabled)
                {
                    url[p.Param] = p.Value;
                }
            }
            
            return url;
        }
        public virtual string GetURLAsString()
        {
            var url = GetURL();
            return url.ToString();
        }
        public override string ToString()
        {
            return GetURLAsString();
        }
        public void Redirect()
        {
            string url = GetURLAsString();
            CS.General_v3.Util.PageUtil.RedirectPage(url);
            
        }

        public void RedirectTo(string urlLocation)
        {
            if (string.IsNullOrEmpty(urlLocation))
            {
                Redirect();
            }
            else
            {
                CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass(urlLocation);
                List<SearchParamDetails> list = CS.General_v3.Util.ListUtil.EnumerateHashTableValues<SearchParamDetails>(this.parameters);
                for (int i = 0; i < list.Count; i++)
                {
                    var p = list[i];
                    if (!p.Disabled)
                    {
                        url[p.Param] = p.Value;
                    }
                }
                url.RedirectTo();
            }
        }

    }
}
