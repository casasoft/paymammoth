﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Controls.WebControls.Specialized.Paging;

namespace CS.General_v3.Classes.SearchParams
{
    [Obsolete("Use BusinessLogic.Classes.Url.ListingUrlParser")]
    public class SearchParams_v2 : SearchParams_v2_Base, IParamsInfo
    {

        public static string PARAM_PAGENO = "pg";
        public static string PARAM_SHOWAMT = "show";
        public static string PARAM_SORTBY = "sort";
        public static string PARAM_KEYWORDS = "k";
        public static string PARAM_CATEGORYID = "catID";




        public SearchParams_v2(string ID = null, string urlToLoad = null, bool loadCurrentUrlIfNull = true)
            : base(ID, urlToLoad, loadCurrentUrlIfNull)
        {
            //this.AddParameterQueryString(PARAM_PAGENO, "1", false, false);
            //this.AddParameterQueryString(PARAM_SHOWAMT, null, true, true);
            //this.AddParameterQueryString(PARAM_SORTBY, null, true, true);
            this.AddParameterQueryString(PARAM_KEYWORDS, null, false, false);
            this.AddParameterQueryString(PARAM_CATEGORYID, null, false, false);
            this.PagingInfo = new SearchParamsPagingInfo_v2(this, null, null, urlToLoad == null && !loadCurrentUrlIfNull);
        }

        public List<string> GetKeywordsAsList()
        {
            List<string> list = new List<string>();
            string s = this.Keywords ?? "";
            if (!string.IsNullOrEmpty(s))
            {
                list.AddRange(s.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries));
            }
            return list;
        }

        public virtual string Keywords
        {
            get
            {
                return this.GetParameter(PARAM_KEYWORDS).GetValue();
            }
            set
            {
                this.GetParameter(PARAM_KEYWORDS).SetValue(value);
            }
        }
        public virtual long? CategoryID
        {
            get
            {
                return this.GetParameter(PARAM_CATEGORYID).GetValueAsInt();
            }
            set
            {
                this.GetParameter(PARAM_CATEGORYID).SetValue(value);
            }
        }
       
        public SearchParamsPagingInfo_v2 PagingInfo { get; private set; }




        #region IParamsInfo Members

        IPagingInfo IParamsInfo.PagingInfo
        {
            get
            {
                return (IPagingInfo)PagingInfo;
            }
            set
            {
                PagingInfo = (SearchParamsPagingInfo_v2)value;
            }
        }

        #endregion
    }
}
