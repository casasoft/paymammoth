﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace CS.General_v3.Classes.SearchParams
{
    public class SearchParams_v2_ParamInfoRouteData : SearchParams_v2_ParamInfoBase
    {
        private string _value = null;

        private bool _valueNeverAccessed = true;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchParams"></param>
        /// <param name="identifier"></param>
        /// <param name="decodeValuesFromRouteData">Wheter you would need to decode from route data such as 'test-boy' to 'test boy' </param>
        public SearchParams_v2_ParamInfoRouteData(SearchParams_v2_Base searchParams, string identifier, bool encodeValuesForRouteData = true)
            : base(searchParams, identifier)
        {

            this.EncodeValuesForRouteData = encodeValuesForRouteData;
        }

        public bool EncodeValuesForRouteData { get; set; }
        
        private string parseInitialValueFromRouteData(string defaultValue)
        {
            return CS.General_v3.Util.RoutingUtil.GetRouteValue<string>(this.Identifier, defaultValue);

        }

        protected override string getValue(string defaultValue = null)
        {
            if (_valueNeverAccessed)
            {
                _value = parseInitialValueFromRouteData(defaultValue);
                if (this.EncodeValuesForRouteData)
                {
                    _value = CS.General_v3.Util.RoutingUtil.ConvertRouteValueToString(_value);
                }
                _valueNeverAccessed = false;
            }
            return string.IsNullOrWhiteSpace(_value) ? defaultValue : _value;
        }

        protected override void setValue(string value)
        {
            if (this.EncodeValuesForRouteData)
            {
                value = CS.General_v3.Util.RoutingUtil.ConvertStringForRouteUrl(value);
            }
            this._value = value;

            _valueNeverAccessed = false;
        }
    }
}
