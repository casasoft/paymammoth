﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.SearchParams
{
    [Obsolete("Use BusinessLogic.Classes.Url.ListingUrlParser")]
    public class SearchParamsImplemented : SearchParams
    {

        public SearchParamsImplemented(string id, string url) : base(id, url)
        {
        }

    }
}
