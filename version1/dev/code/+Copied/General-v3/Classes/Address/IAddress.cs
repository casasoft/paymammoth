﻿using System;
namespace CS.General_v3.Classes.Address
{
    public interface IAddress
    {
        string Address1 { get; set; }
        string Address2 { get; set; }
        string City { get; set; }
        string CompanyName { get; set; }
        CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166 Country { get; set; }
        string GetAsOneLine(string delimiter = ", ");
        string StateProvinceRegion { get; set; }
        string ZIPPostCode { get; set; }
    }
}
