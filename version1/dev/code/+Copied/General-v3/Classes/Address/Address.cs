﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Util;

namespace CS.General_v3.Classes.Address
{
    public class Address : CS.General_v3.Classes.Address.IAddress
    {
        public string CompanyName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string StateProvinceRegion { get; set; }
        public string ZIPPostCode { get; set; }

        public Enums.ISO_ENUMS.Country_ISO3166 Country { get; set; }

        public string GetAsOneLine(string delimiter = ", ")
        {
            return IAddressExtensions.GetAsOneLine(this, delimiter);
        }
    }
}
