﻿using System;
using CS.General_v3.Util;

namespace CS.General_v3.Classes.Address
{
    public static class IAddressExtensions
    {
        public static string GetAsOneLine(this IAddress address, string delimiter = ", ")
        {
            return CS.General_v3.Util.Text.JoinList(delimiter, false, address.CompanyName, address.Address1, address.Address2, address.City, address.StateProvinceRegion, address.ZIPPostCode,
                address.Country.GetCountryFullName());

        }
    }
}
