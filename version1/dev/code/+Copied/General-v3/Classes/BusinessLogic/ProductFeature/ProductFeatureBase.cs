﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_20090518.Classes.BusinessLogic.ProductFeature
{
    public abstract class ProductFeatureBase
    {
        private static IProductFeatureFactory _Factory = new ProductFeatureFactoryTemp();

        public static IProductFeatureFactory Factory
        {
            get { return ProductFeatureBase._Factory; }
            set { ProductFeatureBase._Factory = value; }
        }
        


    }
}
