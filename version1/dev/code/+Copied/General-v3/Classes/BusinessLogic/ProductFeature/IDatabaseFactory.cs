﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_20090518.Classes.BusinessLogic.ProductFeature
{
    public interface IDatabaseFactory<T>
    {
        T LoadFromDB(int ID);
    }
}
