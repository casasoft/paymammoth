﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.Routing;

namespace CS.General_v3.Classes.Routing
{
    public abstract class BaseRoute
    {
        public string Name { get; private set; }
        public string PageUrl { get; private set; }
        public string RouteAppendStr { get; private set; }
        public List<RouteParam> Params { get; private set; }
        public BaseRoute(string name, string pageUrl, string appendStr = null, params RouteParam[] Params)
        {
            this.RouteAppendStr = appendStr;
            this.PageUrl = pageUrl;
            this.Name = name;
            this.Params = new List<RouteParam>();
            if (Params != null)
                this.Params.AddRange(Params);
        }

        public void AddParam(string name, string restriction = null)
        {
            //if (!name.StartsWith("{")) name = "{" + name + "}";
            

            this.Params.Add(new RouteParam(name, restriction));
        }

        protected void initRoute()
        {
            StringBuilder route = new StringBuilder();
            route.Append("/" + this.Name + "/");
            MyRouteValueDictionary dict = new MyRouteValueDictionary();
            foreach (var p in Params)
            {
                route.Append("{" + p.Name + "}/");
                if (!string.IsNullOrEmpty(p.Value))
                {
                    dict[p.Name] = p.Value;
                }
            }
            route.Append(RouteAppendStr);

            CS.General_v3.Util.RoutingUtil.MapPageRoute(Name, route.ToString(), PageUrl, null, dict);
        }
        protected string getUrl(params RouteParam[] routeParams)
        {
            return getUrl((IEnumerable<RouteParam>)routeParams);

        }
        protected string getUrl(IEnumerable<RouteParam> routeParams)
        {
            MyRouteValueDictionary dict = new MyRouteValueDictionary();
            foreach (var p in routeParams)
            {
                dict[p.Name] = p.Value;

            }
            return CS.General_v3.Util.RoutingUtil.GetRouteUrl(this.Name, dict);
        }



    }
}
