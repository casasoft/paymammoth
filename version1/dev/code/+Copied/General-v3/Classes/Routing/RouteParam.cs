﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Routing
{
    public class RouteParam
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public RouteParam(string Name, object Value = null)
        {
            this.Name = Name;
            if (Value != null)
            {
                this.Value = Value.ToString();
            }
        }

    }
}
