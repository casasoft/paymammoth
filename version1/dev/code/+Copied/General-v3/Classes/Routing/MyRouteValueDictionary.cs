﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Routing;

namespace CS.General_v3.Classes.Routing
{
    public class MyRouteValueDictionary : RouteValueDictionary, ICloneable
    {
        public void FillFromNameValuePairString(params string[] nameValuePairs)
        {
            if (nameValuePairs.Length % 2 != 0)
                throw new InvalidOperationException("Constructor accepts only name-value pairs");
            for (int i = 0; i < nameValuePairs.Length; i += 2)
            {
                if (nameValuePairs[i] is string)
                {
                    string key = (string)nameValuePairs[i];
                    

                    string value = nameValuePairs[i + 1] ??"" ;
                    
                    this[key] = value;
                }
                else
                {
                    throw new InvalidOperationException("First 'name' of each name-value pair must ALWAYS be a string");
                }
            }

        }
        public MyRouteValueDictionary(params string[] nameValuePairs)
        {
            FillFromNameValuePairString(nameValuePairs);
        }

        public void RemoveNonUsedKeysFromRouteURL(string routeURL)
        {
            var matches = Regex.Matches(routeURL, @"\{(.*?)\}", RegexOptions.Singleline);
            bool[] foundMatches = new bool[matches.Count];
            var enumerator = this.Keys.GetEnumerator();
            List<string> keyList = this.GetKeysAsList();
            for (int i=0; i < keyList.Count;i++)
            {
                string key = keyList[i];
                string value = (string)this[key];
                bool ok = false;
                for (int j=0; j< matches.Count;j++)
                {
                    string matchValue = matches[j].Groups[1].Value;
                    if (string.Compare(matchValue,key,true) == 0)
                    {
                        ok = true;
                        break;
                    }
                }
                if (!ok)
                {
                    this.Remove(key);
                }
            }
        }

        public List<string> GetKeysAsList()
        {
            List<string> list = new List<string>();
            var enumerator = this.Keys.GetEnumerator();
            while (enumerator.MoveNext())
            {
                list.Add(enumerator.Current);
            }
            return list;
        }

        public object Clone()
        {
            MyRouteValueDictionary newDict = new MyRouteValueDictionary();
            var keyList = this.GetKeysAsList();
            for (int i = 0; i < keyList.Count; i++)
            {
                string key = keyList[i];
                object value = this[key];
                newDict[key] = value;

            }
            return newDict;
        }

        /// <summary>
        /// Fills a route with the tokens in the dictionary
        /// </summary>
        /// <param name="route"></param>
        /// <returns></returns>
        public string GetUrlFromRoute(Route route)
        {
            
            
            var keys = GetKeysAsList();
            string url = route.Url;
            foreach (var key in keys)
            {
                if (this[key] != null)
                {
                    string value = this[key].ToString();
                    
                    if (!string.IsNullOrEmpty(value))
                    {
                        value = CS.General_v3.Util.RoutingUtil.ConvertStringForRouteUrl(value);
                        url = Regex.Replace(url, @"\{" + key + @"\}", value, RegexOptions.IgnoreCase);
                    }
                }
            }
            return url; 
        }

    }

}
