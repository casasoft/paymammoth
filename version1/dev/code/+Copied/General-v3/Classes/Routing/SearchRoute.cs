﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Routing
{
    public class SearchRoute
    {

        private const string REGEX_IDS = "[0-9]+(,[0-9]+)*";
        private const string REGEX_IDS_WITH_PIPELINE = "[0-9]+(,[0-9]+)*|[0-9]+(,[0-9]+)*";

        public static string SEARCH_PAGE_URL = "search.aspx";

        #region Route names
        public static string ROUTE_NAME_KEYWORDS = "route_keywords";
        public static string ROUTE_NAME_BRANDS = "route_brands";
        public static string ROUTE_NAME_CATEGORIES = "route_categories";
        public static string ROUTE_NAME_BRANDS_AND_CATEGORIES = "route_brands_and_categories";
        public static string ROUTE_NAME_KEYWORDS_AND_BRANDS = "route_keywords_and_brands";
        public static string ROUTE_NAME_KEYWORDS_AND_CATEGORIES = "route_keywords_and_categories";
        
        #endregion

        #region Route parameters
        public static string ROUTE_PARAM_KEYWORDS = "Keywords";
        public static string ROUTE_PARAM_BRANDS = "Brands";
        public static string ROUTE_PARAM_BRAND_IDS = "BrandIDs";
        public static string ROUTE_PARAM_CATEGORIES = "Categories";
        public static string ROUTE_PARAM_CATEGORY_IDS = "CategoryIDs";
        public static string ROUTE_PARAM_BRANDS_CATEGORIES_IDS = "BrandCategoriesIDs";
        #endregion

        public static void MapRoutes()
        {
            initBrandsAndCategoriesRoute();
            initKeywordsAndBrandsRoute();
            initKeywordsAndCategoriesRoute();
            initKeywordsRoute();
            initBrandsRoute();
            initCategoriesRoute();
        }

        private static void initKeywordsAndBrandsRoute()
        {
            Util.RoutingUtil.MapPageRoute(
              routeName: ROUTE_NAME_KEYWORDS_AND_BRANDS,
              routeUrl: "/search/keywords/{" + ROUTE_PARAM_KEYWORDS + "}/brands/{" + ROUTE_PARAM_BRANDS + "}/{" + ROUTE_PARAM_BRAND_IDS + "}/",
              physicalFile: SEARCH_PAGE_URL,
              defaultValues: null,
              constraints: new MyRouteValueDictionary(ROUTE_PARAM_BRAND_IDS, REGEX_IDS));
        }


        private static void initKeywordsAndCategoriesRoute()
        {
            Util.RoutingUtil.MapPageRoute(
              routeName: ROUTE_NAME_KEYWORDS_AND_CATEGORIES,
              routeUrl: "/search/keywords/{" + ROUTE_PARAM_KEYWORDS + "}/categories/{" + ROUTE_PARAM_CATEGORIES + "}/{" + ROUTE_PARAM_CATEGORY_IDS + "}/",
              physicalFile: SEARCH_PAGE_URL,
              defaultValues: null,
              constraints: new MyRouteValueDictionary(ROUTE_PARAM_CATEGORY_IDS, REGEX_IDS));
        }

                
        private static void initBrandsAndCategoriesRoute()
        {
            Util.RoutingUtil.MapPageRoute(
              routeName: ROUTE_NAME_BRANDS_AND_CATEGORIES,
              routeUrl: "/search/brands/{" + ROUTE_PARAM_BRANDS + "}/{" + ROUTE_PARAM_CATEGORIES + "}/{" + ROUTE_PARAM_BRANDS_CATEGORIES_IDS + "}/",
              physicalFile: SEARCH_PAGE_URL,
              defaultValues: null,
              constraints: new MyRouteValueDictionary(ROUTE_PARAM_BRANDS_CATEGORIES_IDS, REGEX_IDS_WITH_PIPELINE));
        }

        private static void initCategoriesRoute()
        {
            Util.RoutingUtil.MapPageRoute(
            routeName: ROUTE_NAME_CATEGORIES,
            routeUrl: "/search/categories/{" + ROUTE_PARAM_CATEGORIES + "}/{" + ROUTE_PARAM_CATEGORY_IDS + "}/",
            physicalFile: SEARCH_PAGE_URL,
            defaultValues: null,
            constraints: new MyRouteValueDictionary(ROUTE_PARAM_CATEGORY_IDS, REGEX_IDS));
        }
      
        private static void initKeywordsRoute()
        {
            Util.RoutingUtil.MapPageRoute(
                routeName: ROUTE_NAME_KEYWORDS,
                routeUrl: "/search/keywords/{" + ROUTE_PARAM_KEYWORDS + "}/",
                physicalFile: SEARCH_PAGE_URL,
                defaultValues: null,
                constraints: null);
        }

        private static void initBrandsRoute()
        {
            Util.RoutingUtil.MapPageRoute(
              routeName: ROUTE_NAME_BRANDS,
              routeUrl: "/search/brands/{" + ROUTE_PARAM_BRANDS + "}/{" + ROUTE_PARAM_BRAND_IDS + "}/",
              physicalFile: SEARCH_PAGE_URL,
              defaultValues: null,
              constraints: new MyRouteValueDictionary(ROUTE_PARAM_BRAND_IDS, REGEX_IDS));
        }



    }
}
