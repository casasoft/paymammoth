﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using CS.General_v3.Util;
using log4net;

namespace CS.General_v3.Classes.Email
{
    public class EmailHtmlParser
    {
        private static ILog _log = log4net.LogManager.GetLogger(typeof(EmailHtmlParser));
       
        public string InputHTML { get; set; }
        public string OutputHTML { get; set; }
        public bool ConvertToLinkedResources { get; set; }
        public bool ConvertURLsToFull { get; set; }
        public string BaseURL { get; set; }
        public List<System.Net.Mail.LinkedResource> LinkedResources { get; private set; }
        public EmailHtmlParser()
        {

        }
        public string ParseHTML(string html, bool convertImagesToLinkedResources, bool convertURLsToFull)
        {
            return ParseHTML(html, convertImagesToLinkedResources, convertURLsToFull, null);
        }
        public string ParseHTML(string html, bool convertImagesToLinkedResources, bool convertURLsToFull, string baseURL)
        {
            if (this.LinkedResources != null)
            {
                foreach (var r in this.LinkedResources)
                {
                    r.Dispose();
                }
                this.LinkedResources.Clear();
            }
            this.LinkedResources = new List<LinkedResource>();
            if (string.IsNullOrEmpty(baseURL)) baseURL = CS.General_v3.Util.PageUtil.GetApplicationBaseUrl().GetURL(appendQueryString: false, fullyQualified: true);
            if (!baseURL.EndsWith("/")) baseURL += "/";
            if (!baseURL.ToLower().StartsWith("http")) baseURL = "http://" + baseURL;

            this.BaseURL = baseURL;


            this.InputHTML = html;

            this.ConvertToLinkedResources = convertImagesToLinkedResources;
            this.ConvertURLsToFull = convertURLsToFull;
            this.OutputHTML = html;
            this.LinkedResources = new List<LinkedResource>();
            parseImages();
            parseUrlLinks();
            parseBackgroundImages();
            //                parseInlineBackgroundImages();

            return this.OutputHTML;


        }

        private void parseUrlLinks()
        {
            if (ConvertURLsToFull)
            {

                this.OutputHTML = Regex.Replace(this.OutputHTML, @"url\(\/", "url(" + this.BaseURL, RegexOptions.IgnoreCase); //converts any url(/fdasfsad) tags
                this.OutputHTML = Regex.Replace(this.OutputHTML, @"href=(""|')/", @"href=\1" + this.BaseURL); //converts any a hrefs =/'
                //this.OutputHTML = Regex.Replace(this.OutputHTML, @"src=(""|')/", @"href=\1" + this.BaseURL);

            }

        }

        private string addImageToLinkedResource(string img)
        {
            if (img.ToLower().StartsWith(this.BaseURL.ToLower()))
            {
                img = img.Substring(this.BaseURL.Length - 1);

            }
            //string contentID = this.PrependContentID + "_" + (this.LinkedResources.Count + 1);
            Stream stream = null;
            string contentType = CS.General_v3.Util.Data.GetMIMEContentType(img);

            if (img.StartsWith("/"))
            {
                //get off file system
                string path = CS.General_v3.Util.PageUtil.MapPath(img);
                if (File.Exists(path))
                {
                    byte[] by = CS.General_v3.Util.IO.LoadFileAsByteArray(path);
                    stream = new MemoryStream(by);
                }
            }
            else
            {
                //download from web
                try
                {
                    stream = CS.General_v3.Util.Web.DownloadDataAsStream(img);
                }
                catch
                {
                    stream = null;
                }

            }
            if (stream != null)
            {
                LinkedResource linkedRes = new LinkedResource(stream, contentType);

                this.LinkedResources.Add(linkedRes);
                return linkedRes.ContentId;
            }
            else
            {
                return "";
            }

        }

        private void parseImages()
        {
            string pattern = "<img(.*?)src=(\"|')(.*?)(\"|')";
            MatchCollection matches = Regex.Matches(this.OutputHTML, pattern, RegexOptions.Singleline);
            int lastEndPos = -1;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < matches.Count; i++)
            {
                var match = matches[i];
                sb.Append(OutputHTML.SubStr(lastEndPos + 1, match.Groups[0].Index, false));
                lastEndPos = match.Groups[0].Index + match.Groups[0].Length - 1;
                string img = match.Groups[3].Value;
                string imgSrc = getImgSrc(img);

                sb.Append("<img" + match.Groups[1].Value + "src=" + match.Groups[2].Value + imgSrc + match.Groups[2].Value);
            }
            sb.Append(this.OutputHTML.Substring(lastEndPos + 1));
            this.OutputHTML = sb.ToString();
        }
        private string getImgSrc(string img)
        {
            string imgSrc = img;
            if (ConvertToLinkedResources)
            {
                imgSrc = "cid:" + addImageToLinkedResource(img);
            }
            else if (ConvertURLsToFull && !imgSrc.ToLower().StartsWith(this.BaseURL.ToLower()))
            {
                if (imgSrc.StartsWith("/")) imgSrc = img.Substring(1);

                imgSrc = this.BaseURL + imgSrc;
            }
            return imgSrc;
        }
        private void parseBackgroundImages()
        {
            string pattern = "background=(\"|')(.*?)(\"|')";
            MatchCollection matches = Regex.Matches(this.OutputHTML, pattern, RegexOptions.Singleline);
            int lastEndPos = -1;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < matches.Count; i++)
            {
                var match = matches[i];
                sb.Append(OutputHTML.SubStr(lastEndPos + 1, match.Groups[0].Index, false));
                lastEndPos = match.Groups[0].Index + match.Groups[0].Length - 1;
                string img = match.Groups[2].Value;
                string imgSrc = getImgSrc(img);
                sb.Append("background=" + match.Groups[1].Value + imgSrc + match.Groups[1].Value);
            }
            sb.Append(this.OutputHTML.Substring(lastEndPos + 1));
            this.OutputHTML = sb.ToString();
        }
        /*
        private void parseInlineBackgroundImages()
        {
            string pattern = @"(background-image|background)(.*?):(.*?)url\((.*?)\)";
            MatchCollection matches = Regex.Matches(this.OutputHTML, pattern, RegexOptions.Singleline);
            int lastEndPos = -1;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < matches.Count; i++)
            {
                var match = matches[i];
                sb.Append(OutputHTML.SubStr(lastEndPos + 1, match.Groups[0].Index, false));
                lastEndPos = match.Groups[0].Index + match.Groups[0].Length - 1;
                string img = match.Groups[4].Value;
                string imgSrc = getImgSrc(img);
                sb.Append(match.Groups[1] + ": url(" + imgSrc + ")");
            }
            sb.Append(this.OutputHTML.Substring(lastEndPos + 1));
            this.OutputHTML = sb.ToString();
        }
        */

    }
}
