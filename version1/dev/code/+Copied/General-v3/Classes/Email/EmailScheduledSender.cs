﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace CS.General_v3.Classes.Email
{
    public class EmailScheduledSender
    {
        private static readonly object _instanceLock = new object();
        private static EmailScheduledSender _InstanceApp {get;set;}
        public static EmailScheduledSender InstanceApp
        {
            get
            {
                lock (_instanceLock)
                {
                    if (_InstanceApp == null)
                    {
                        _InstanceApp = new EmailScheduledSender();
                        
                    }
                }
                return _InstanceApp;
            }
        }

        /// <summary>
        /// Emails to send each btach
        /// </summary>
        public int EmailsPerBatch { get; set; }
        /// <summary>
        /// Interval in seconds
        /// </summary>
        public int BatchInterval { get; set; }
        
        public EmailScheduledSender()
        {
            this.EmailsPerBatch = 25;
            this.BatchInterval = 60 * 3;// 3 minutes
            _tmr = new Timer();
            _tmr.Elapsed += new ElapsedEventHandler(_tmr_Elapsed);
            this.EmailsToSend = new List<Email.EmailMessage>();
        }

        private Timer _tmr = null;
        private bool isStarted = false;
        public void Start()
        {
            Stop();
            _tmr.Interval = this.BatchInterval * 1000;
            //sendNextBatch();
            _tmr.Start();
            isStarted = true;
            
        }
        public void Stop()
        {
            
            _tmr.Stop();
            isStarted = false;
        }

        void _tmr_Elapsed(object sender, ElapsedEventArgs e)
        {
            Stop();
            sendNextBatch();
            Start();
        }


        public List<Email.EmailMessage> EmailsToSend { get; private set; }
        private readonly object _lock = new object();
        public void EnqueueEmail(Email.EmailMessage msg, bool autoStart = false)
        {
            lock (_lock)
            {
                this.EmailsToSend.Add(msg);
                if (autoStart && !isStarted)
                    this.Start();
            }

        }




        private void sendNextBatch()
        {
            lock (_lock)
            {
                for (int i = 0; i < EmailsPerBatch; i++)
                {
                    if (EmailsToSend.Count == 0) break;
                    var currMsg = this.EmailsToSend[0];
                    this.EmailsToSend.RemoveAt(0);
                    currMsg.SendSynchronously();
                }
            }
        }



    }
}
