﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using CS.General_v3.Classes.HelperClasses;

using CS.General_v3.Util;
using log4net;
using CS.General_v3.Classes.Text;

namespace CS.General_v3.Classes.Email
{
    public class EmailMessage
    {
        public static event Action<EmailMessage> OnEmailSent;

        /// <summary>
        /// If this is set to true, all emails are sent synchronously, irrespecive.  Useful for unit testing, if you want to check that an email is sent.
        /// </summary>
        public static bool SendAllEmailsSynchronouslyIrrespective { get; set; }

        private static readonly ILog _log = LogManager.GetLogger(typeof(EmailMessage));

        public bool IsErrorNotification { get; set; }
        /// <summary>
        /// Token Replacer.  If this is filled, the subject & body are parsed before sending.
        /// </summary>
        public ITokenReplacer TokenReplacer { get; set; }
        public EmailMessage()
        {
            
            this.Attachments = new List<Attachment>();
            this.Resources = new List<LinkedResource>();
            this.To = new List<MailAddress>();
            this.Bcc = new List<MailAddress>();
            initDefaultSettings();
        }
        private void initDefaultSettings()
        {
            this.SendRetryTimes = 3;
            this.SmtpTimeoutSeconds = 3 * 60;// 3 minutes
            this.SmtpUseSecureAuthentication = CS.General_v3.Settings.GetSettingFromDatabase<bool>(Enums.SETTINGS_ENUM.Email_SMTP_UseSecureAuthentication);
            {
                string replyToEmail = CS.General_v3.Settings.GetSettingFromDatabase(Enums.SETTINGS_ENUM.Email_EmailAddresses_General_ReplyToEmail);
                string replyToName = CS.General_v3.Settings.GetSettingFromDatabase(Enums.SETTINGS_ENUM.Email_EmailAddresses_General_ReplyToName);
                this.SetReplyToEmail(replyToEmail, replyToName);
            }
            CheckDefaultSettings();
        }

        public void CheckDefaultSettings()
        {

            if (string.IsNullOrWhiteSpace(this.SmtpUsername)) this.SmtpUsername = CS.General_v3.Settings.GetSettingFromDatabase(Enums.SETTINGS_ENUM.Email_SMTP_User);
            if (string.IsNullOrWhiteSpace(this.SmtpHost)) this.SmtpHost = CS.General_v3.Settings.GetSettingFromDatabase(Enums.SETTINGS_ENUM.Email_SMTP_Host);
            if (string.IsNullOrWhiteSpace(this.SmtpPass)) this.SmtpPass = CS.General_v3.Settings.GetSettingFromDatabase(Enums.SETTINGS_ENUM.Email_SMTP_Pass);
            if (this.SmtpPort <= 0) this.SmtpPort = CS.General_v3.Settings.GetSettingFromDatabase<int?>(Enums.SETTINGS_ENUM.Email_SMTP_Port).GetValueOrDefault(25);
            if (this.From == null)
            {
                string fromEmail = CS.General_v3.Settings.GetSettingFromDatabase(Enums.SETTINGS_ENUM.Email_EmailAddresses_General_FromEmail);
                string fromName = CS.General_v3.Settings.GetSettingFromDatabase(Enums.SETTINGS_ENUM.Email_EmailAddresses_General_FromName);
                this.SetFromEmail(fromEmail, fromName);

                
            }
            if (!this.SmtpUseSecureAuthentication.HasValue) this.SmtpUseSecureAuthentication = CS.General_v3.Settings.GetSettingFromDatabase<bool>(Enums.SETTINGS_ENUM.Email_SMTP_UseSecureAuthentication);

            //if (!string.IsNullOrWhiteSpace(this.ToEmail)) this.ToEmail = CS.General_v3.Settings.GetSettingFromDatabase(Enums.SETTINGS_ENUM.Email_EmailAddresses_General_FromEmail);
            //if (!string.IsNullOrWhiteSpace(this.ToName)) this.ToName = CS.General_v3.Settings.GetSettingFromDatabase(Enums.SETTINGS_ENUM.Email_EmailAddresses_General_FromName);

        }

        public void SetFromEmail(string email, string name)
        {
            if (!string.IsNullOrWhiteSpace(email))
            {
                this.From = new MailAddress(email, name);
            }
            else
            {
                this.From = null;
            }
        }
        public void SetReplyToEmail(string email, string name)
        {
            if (!string.IsNullOrWhiteSpace(email))
            {
                this.ReplyTo = new MailAddress(email, name);
            }
            else
            {
                this.ReplyTo = null;
            }
        }
        /// <summary>
        /// Adds a list of recipients
        /// </summary>
        /// <param name="emailsToAdd">This can include multiple emails, seperated by one of [, | ;]</param>
        /// <param name="name"></param>
        public void AddToEmails(string emailsToAdd, string name)
        {
            parseEmailsToCollection(emailsToAdd, name, this.To);
            
        }
        /// <summary>
        /// Adds a list of recipients
        /// </summary>
        /// <param name="emailsToAdd">This can include multiple emails, seperated by one of [, | ;]</param>
        /// <param name="name"></param>
        public void AddBccEmails(string emailsToAdd, string name)
        {
            parseEmailsToCollection(emailsToAdd, name, this.Bcc);

        }
        /// <summary>
        /// Adds a list of recipients
        /// </summary>
        /// <param name="emailsToAdd">This can include multiple emails, seperated by one of [, | ;]</param>
        /// <param name="name"></param>
        private void parseEmailsToCollection(string emailsToAdd, string name, ICollection<MailAddress> list)
        {
            if (!string.IsNullOrWhiteSpace(emailsToAdd))
            {

                var emailList = CS.General_v3.Util.Text.Split(emailsToAdd, ",", "|", ";");
                foreach (var emailToAdd in emailList)
                {
                    string email = emailToAdd.Trim();
                    bool found = false;

                    foreach (var toEmail in this.To)
                    {
                        if (string.Compare(toEmail.Address, email, true) == 0)
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        list.Add(new MailAddress(email, name));

                    }
                }


            }

        }
        public int SendRetryTimes { get; set; }
        public MailAddress From { get; set; }
        /// <summary>
        /// The first email will be the main email, any other addresses are sent as BCC
        /// </summary>
        public ICollection<MailAddress> To { get; set; }
        public ICollection<MailAddress> Bcc { get; set; }
        public MailAddress ReplyTo { get; set; }
        public string Subject { get; set; }
        public string BodyPlain { get; set; }
        public string BodyHtml { get; set; }
        public ICollection<Attachment> Attachments { get; set; }
        public ICollection<LinkedResource> Resources { get; set; }
        public bool? SmtpUseSecureAuthentication { get; set; }
        public int SmtpPort { get; set; }
        public int SmtpTimeoutSeconds {get;set;}
        public string SmtpUsername { get; set; }
        public string SmtpHost { get; set; }
        public string SmtpPass { get; set; }
        public OperationResult LastOperationResult { get; internal set; }


        /// <summary>
        /// This will add the notification email as recipient
        /// </summary>
        public void AddToSendToNotificationEmail()
        {
            string email = CS.General_v3.Settings.GetSettingFromDatabase(Enums.SETTINGS_ENUM.Email_EmailAddresses_Notification_Email);
            string name = CS.General_v3.Settings.GetSettingFromDatabase(Enums.SETTINGS_ENUM.Email_EmailAddresses_General_FromName);
            if (!string.IsNullOrWhiteSpace(email))
            {
                this.To.Add(new MailAddress(email, name));
            }
        }






        internal string getPlainText()
        {
            if (!string.IsNullOrWhiteSpace(this.BodyPlain))
                return this.BodyPlain;
            else
                return CS.General_v3.Util.Text.ConvertHTMLToPlainText(this.BodyHtml, true);

        }

        public void SendAsynchronously(System.Threading.ThreadPriority threadPriority = System.Threading.ThreadPriority.BelowNormal)
        {
            if (this.To == null || this.To.Count == 0)
            {
                throw new InvalidOperationException("You cannot send an email without specifying at least one 'To'");
            }

            if (!SendAllEmailsSynchronouslyIrrespective)
            {
                string threadName = "EmailMessage-SendAsync [" + this.Subject + "]";

                CS.General_v3.Util.ThreadUtil.CallMethodOnSeperateThread(_sendAsynch, threadName, threadPriority);
            }
            else
            {
                SendSynchronously(throwErrorOnException: true);
            }




        }
        private void _sendAsynch()
        {
            
            this.SendSynchronously();
            
        }
        public OperationResult SendSynchronously(bool throwErrorOnException = false)
        {
            var result = EmailMsgSender.Instance.SendSynchronously(this, throwErrorOnException);
            if (result.IsSuccessful)
            {
                if (OnEmailSent != null) OnEmailSent(this);
            }

            return result;
        }

        public void VerifyParams()
        {
            StringBuilder sb = new StringBuilder();
            if (this.To == null)
                sb.AppendLine("Please fill in Recipient - 'To'");
            if (sb.Length > 0)
                throw new InvalidOperationException("Cannot send email\r\n\r\n" + sb.ToString());
        }


        internal void checkForTokenReplacer()
        {
            if (this.TokenReplacer != null)
            {
                 this.Subject = this.TokenReplacer.ReplaceString(this.Subject);
                 this.BodyHtml = this.TokenReplacer.ReplaceString(this.BodyHtml);
                 this.BodyPlain = this.TokenReplacer.ReplaceString(this.BodyPlain);
            }
        }

        

        public string GetWholeRecipientEmailListCommaDelimeted()
        {
            return CS.General_v3.Util.Text.AppendStrings(", ", this.To.ConvertListToString(x => x.Address));
            
        }
        public string GetWholeRecipientNameListCommaDelimeted()
        {
            return CS.General_v3.Util.Text.AppendStrings(", ", this.To.ConvertListToString(x => x.DisplayName));

        }
    }
}
