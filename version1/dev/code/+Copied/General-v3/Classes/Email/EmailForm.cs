using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.Data;
using System.Data.Common;
using System.Web.UI.HtmlControls;
using CS.General_v3.Controls.WebControls.Common;
using System.Web.UI;
using System.IO;
using CS.General_v3.Util;

namespace CS.General_v3.Classes.Email
{
    /// <summary>
    /// Generates an email form.  It will take a subject and body, and you can add sections & fields etc.  
    /// The default content token is [CONTENT]
    /// </summary>
    public class EmailForm
    {
        public class Section
        {
            public string SectionName { get; set; }
            public FieldList Fields { get; set; }
            public void GenerateHtmlContent(MyPlaceHolder ph)
            {
                MyHeading h2 = new MyHeading(2, this.SectionName); ph.Controls.Add(h2);

                MyTable tb = new MyTable(); ph.Controls.Add(tb);
                tb.Style["padding"] = "2px";
                tb.CellSpacing = 2;
                tb.CellPadding = 2;
                tb.Style["border-spacing"] = "2px";
                Fields.GenerateHtmlContent(tb);

            }
            public string GeneratePlainTextContent()
            {
                StringBuilder sb = new StringBuilder();
                if (!string.IsNullOrEmpty(SectionName))
                {
                    sb.AppendLine("" + SectionName.ToUpper());
                    sb.AppendLine("" + CS.General_v3.Util.Text.RepeatText("-", 60));
                    
                }
                sb.AppendLine(Fields.GeneratePlainTextContent());
                return sb.ToString();
            }
            public Section()
                : this(null)
            {

            }
            public Section(string sectionName)
            {
                this.SectionName = sectionName;
                this.Fields = new FieldList();
            }
            public void AddField()
            {
                AddField(null, null);
                
            }
            public void AddField(string fieldName, object value)
            {
                AddField(fieldName, value, false, 0);
            }
            public void AddField(string fieldName, object value,bool showValueUnderneath, int leftPadding)
            {
                Field f = new Field(fieldName, value, showValueUnderneath, leftPadding);
                this.Fields.Add(f);
            }
        }
        public class FieldList : List<Field>
        {
            public int GetLongestFieldSize()
            {
                int curr = 0;
                for (int i = 0; i < this.Count; i++)
                {
                    if (this[i].Title != null && this[i].Title.Length > curr)
                    {
                        curr = this[i].Title.Length;
                    }
                }
                return curr;
            }

            public void GenerateHtmlContent(MyTable tb)
            {
                
                for (int i = 0; i < this.Count; i++)
                {
                    MyTableRow tr = new MyTableRow(); tb.Rows.Add(tr);
                    this[i].GenerateHtmlContent(tr);
                }
            }
            public string GeneratePlainTextContent()
            {
                int longestField = GetLongestFieldSize();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < this.Count; i++)
                {
                    sb.AppendLine(this[i].GeneratePlainTextContent(longestField));
                }
                return sb.ToString();

            }
        }
        public class Field
        {
            private void init()
            {
                this.MaxCharsPerLine = 80;
                this.SpacePadding = 4;
            }
            public Field(string title, object value)
                : this(title, value, false, 0)
            {

            }
            public void SetValue(object val)
            {
                string s = "";
                if (val == null) val = "";
                if (val is bool)
                {
                    s = CS.General_v3.Util.Other.BoolToText((bool)val);
                }
                else if (val is DateTime)
                {
                    s = ((DateTime)val).ToString("dd/MM/yyyy HH:mm:ss");
                }
                else if (val is double)
                {
                    s = ((double)val).ToString("0.00");
                }
                else
                {
                    s = val.ToString();
                }
                this.Value = s;
            }
            public Field(string title, object value, bool showValueUnderneath, int spacePadding)
            {
                this.Title = title;
                SetValue(value);
                
                this.ShowValueUnderneath = showValueUnderneath;
                this.SpacePadding = spacePadding;
                this.MaxCharsPerLine = 60;
            }
            public string Title { get; set; }
            public string Value { get; set; }
            public bool ShowValueUnderneath { get; set; }
            public int SpacePadding { get; set; }
            public int MaxCharsPerLine { get; set; }
            public void GenerateHtmlContent(MyTableRow tr)
            {


                MyTableCell td = null;
                td = tr.AddCell(null, "<strong>" + CS.General_v3.Util.Text.TxtForHTML(this.Title) + ":</strong>");
                td.Style["vertical-align"] = "top";
                td.Style["text-align"] = "right";
                string htmlVal = CS.General_v3.Util.Text.TxtForHTML(this.Value);
                if (string.IsNullOrEmpty(htmlVal)) htmlVal = "&nbsp;";
                td = tr.AddCell(null, CS.General_v3.Util.Text.TxtForHTML(this.Value));

            }
            public string GeneratePlainTextContent(int fieldSize)
            {
                StringBuilder sb =new StringBuilder();
                if (!string.IsNullOrEmpty(this.Title))
                {
                    sb.Append("" + CS.General_v3.Util.Text.PadText(this.Title, Util.Text.ALIGN.Left, fieldSize) + " :");
                }
                if (!ShowValueUnderneath)
                {
                    sb.Append(" " + this.Value);
                }
                else
                {
                    if (!string.IsNullOrEmpty(this.Title))
                    {
                        sb.AppendLine();
                        sb.AppendLine();
                    }
                    sb.Append(CS.General_v3.Util.Text.PadLines(this.Value, true, SpacePadding, MaxCharsPerLine));

                }
                return sb.ToString();
            }
        }
        public class SectionList : List<Section>
        {
            public string GenerateHTMLContent()
            {
                MyPlaceHolder ph = new MyPlaceHolder();
                for (int i = 0; i < this.Count; i++)
                {
                    this[i].GenerateHtmlContent(ph);

                }
                return CS.General_v3.Util.ControlUtil.RenderControl(ph);
                
            }
            public string GeneratePlainTextContent()
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < this.Count; i++)
                {
                    sb.Append(this[i].GeneratePlainTextContent());
                }
                return sb.ToString();
                
            }
        }
        public SectionList Sections { get; set; }
        private Section currentSection = null;
        public void AddSection(string section)
        {
            Section sect = new Section(section);
            AddSection(sect);
            
        }
        
        public void AddSection(Section section)
        {
            Sections.Add(section);
            currentSection = section;
        }
        public void AddField(string name, object value)
        {
            Field field = new Field(name, value);
            AddField(field);
        }
        public void AddField(string name, object value, bool showValueUndernath, int spacePadding)
        {
            Field field = new Field(name, value,showValueUndernath,spacePadding);
            AddField(field);
        }
        public void AddField(Field field)
        {
            if (currentSection == null)
                throw new InvalidOperationException("You cannot call this method if you are not using 'AddSection'");

            currentSection.Fields.Add(field);
        }

        public string Body { get; set; }
        public string Subject { get; set; }
        public string ContentToken { get; set; }
        public bool IsHTML { get; set; }
        public ICollection<LinkedResource> LinkedResources { get; set; }
       


        public EmailForm(string subject, string body) : this (subject, body, false, null)
        {

        }
        public EmailForm(string subject, string body, bool isHTML, ICollection<LinkedResource> linkedResources)
        {
            this.LinkedResources = linkedResources;
            this.Sections = new SectionList();
            this.Subject = subject;
            this.Body = body;
            this.IsHTML = isHTML;
            this.ContentToken = "[CONTENT]";
        }
        
        public string GenerateContent()
        {
            if (!this.IsHTML)
                return Sections.GeneratePlainTextContent();
            else
                return Sections.GenerateHTMLContent();
        }
        /// <summary>
        /// Generates the body of the email based on fields passed
        /// </summary>
        /// <returns></returns>
        public string GetBody()
        {
            string content = GenerateContent();
            string body = Body;
            
            body = System.Text.RegularExpressions.Regex.Replace(body,CS.General_v3.Util.Text.ForRegExp(ContentToken),content, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                
            return body;
        }
        
        /// <summary>
        /// Sends the email asynchronously
        /// </summary>
        /// <param name="fromEmail"></param>
        /// <param name="fromName"></param>
        /// <param name="toEmail"></param>
        /// <param name="toName"></param>
        public void SendEmailAsync(string fromEmail, string fromName,
            string toEmail, string toName, string replyToEmail, string replyToName)
        {
            sendEmail(fromEmail, fromName, toEmail, toName, replyToEmail, replyToName, true);
            
        }
        private List<Attachment> getAttachements()
        {
            List<Attachment> list = null;
            if (!string.IsNullOrEmpty(this.AttachmentPath))
            {
                list = new List<Attachment>();
                string path = this.AttachmentPath;
                if (path.Contains("/")) path = CS.General_v3.Util.PageUtil.MapPath(path);
                list.Add(new Attachment(path));
            }
            return list;
        }

        private void sendEmail(string fromEmail, string fromName,
            string toEmail, string toName, string replyToEmail, string replyToName, bool async)
        {
            string body = GetBody();
            string plainText = null;
            string htmlText = null;

            if (IsHTML)
            {
                htmlText = body;
            }
            else
            {
                plainText = body;
            }
            EmailMessage email = new EmailMessage();
            email.SetFromEmail(fromEmail, fromName);
            email.AddToEmails(toEmail, toName);
            email.SetReplyToEmail(replyToEmail, replyToName);
            email.Subject = Subject;
            email.BodyPlain = plainText;
            email.BodyHtml = htmlText;
            email.Attachments = getAttachements();
            email.Resources = this.LinkedResources.ToList();
            if (async)
                email.SendAsynchronously();
            else
                email.SendSynchronously();
        }

        /// <summary>
        /// Sends the email synchronusly. Blocking
        /// </summary>
        /// <param name="fromEmail"></param>
        /// <param name="fromName"></param>
        /// <param name="toEmail"></param>
        /// <param name="toName"></param>
        public void SendEmailSync(string fromEmail, string fromName,
            string toEmail, string toName, string replyToEmail, string replyToName)
        {
            sendEmail(fromEmail, fromName, toEmail, toName, replyToEmail, replyToName, false);
            
        }

        public string AttachmentPath { get; set; }

    }
}
