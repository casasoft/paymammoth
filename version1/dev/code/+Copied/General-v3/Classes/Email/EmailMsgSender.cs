﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using CS.General_v3.Classes.HelperClasses;
using log4net;

namespace CS.General_v3.Classes.Email
{
    public class EmailMsgSender
    {
        public static EmailMsgSender Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<EmailMsgSender>(null); } }
        private static readonly ILog _log = LogManager.GetLogger(typeof(EmailMessage));



        public int CurrentSendingCount { get; private set; }

        private readonly object _lock = new object();

        private void incrementSendCount()
        {
            lock (_lock)
            {

                CurrentSendingCount++;
            }
        }
        private void decrementSendCount()
        {
            lock (_lock)
            {
                CurrentSendingCount--;
            }
        }

        /// <summary>
        /// Sleeps the current thread until all emails are sent. 
        /// </summary>
        /// <param name="maxTimeToWait">Maximum time to wait in milleseconds.  Default is 60 seconds.</param>
        public void WaitUntilAllEmailsAreSent(int maxTimeToWait = 60000)
        {
            int totalWaitTime = 0;
            int interval = 500;
            while (CurrentSendingCount > 0 && totalWaitTime < maxTimeToWait)
            {
                System.Threading.Thread.Sleep(interval);
                totalWaitTime += interval;
            }
        }


        private void addBccRecipientsToEmail(EmailMessage emailToSend, MailMessage msg)
        {
            {//add bcc recipients
                List<MailAddress> bccList = new List<MailAddress>();
                bccList.AddRange(emailToSend.To.Skip(1));//add all except first
                bccList.AddRange(emailToSend.Bcc);
                for (int i = 0; i < bccList.Count; i++)
                {
                    //add any other emails as Bcc (blind-carbon-copy)
                    var email = bccList[i];
                    if (email != null)
                    {

                        msg.Bcc.Add(email);
                    }
                }
            }
        }
        private void writeInitSendingLogEntry(EmailMessage emailMsg, MailAddress mainToEmail)
        {
            if (_log.IsDebugEnabled)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("MAIL_SENDER: Sending Email - '" + emailMsg.From.DisplayName + "' <" + emailMsg.From.Address + ">, Recipient - '" + mainToEmail.DisplayName + "' <" + mainToEmail.Address + ">");
                sb.AppendLine("Subject: " + emailMsg.Subject);
                sb.AppendLine("Host: " + emailMsg.SmtpHost);
                sb.AppendLine("Port: " + emailMsg.SmtpPort);
                sb.AppendLine("User: " + emailMsg.SmtpUsername);

                string hiddenPass = "";
                if (emailMsg.SmtpPass.Length > 0)
                    hiddenPass += emailMsg.SmtpPass.Substring(0, 1);
                hiddenPass += CS.General_v3.Util.Text.RepeatText("*", emailMsg.SmtpPass.Length - 2);
                if (emailMsg.SmtpPass.Length >= 2)
                    hiddenPass += emailMsg.SmtpPass.Substring(emailMsg.SmtpPass.Length - 1, 1);

                sb.AppendLine("Pass: " + hiddenPass);

                _log.Debug(sb.ToString());

            }
        }
        private void sendActualEmail(EmailMessage emailMsg, MailMessage msg, OperationResult result, bool throwErrorOnException)
        {
            /*(var client = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new NetworkCredential("website@maltafreightexchange.com", "xWFTpLghgfwn"),
                EnableSsl = true
            };
            client.Send("website@maltafreightexchange.com", "mark@casasoft.com.mt", "Testing", "body text is here");
            //Console.WriteLine("Sent");
            //Console.ReadLine();
            (*/

            int retryTimes = emailMsg.SendRetryTimes;
            bool ok = false;

            for (int i = 0; i < retryTimes; i++)
            {
                SmtpClient smtp = new SmtpClient(emailMsg.SmtpHost, emailMsg.SmtpPort);
                try
                {
                    //smtp.Host = emailMsg.SmtpHost;
                    //smtp.Port = emailMsg.SmtpPort;
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    // smtp.Host = "192.168.5.50";



                    smtp.Timeout = emailMsg.SmtpTimeoutSeconds * 1000;
                    smtp.UseDefaultCredentials = false;
                    smtp.EnableSsl = emailMsg.SmtpUseSecureAuthentication.GetValueOrDefault();
                    if (!string.IsNullOrEmpty(emailMsg.SmtpUsername))
                    {
                        smtp.UseDefaultCredentials = true;
                        NetworkCredential userInfo = new NetworkCredential(emailMsg.SmtpUsername, emailMsg.SmtpPass);

                        smtp.Credentials = userInfo;
                    }



                    smtp.Send(msg);
                    if (_log.IsInfoEnabled) _log.Debug("MAIL_SENDER: Email sent successfully");
                    ok = true;

                }
                catch (Exception ex)
                {
                    result.AddException(ex);
                    _log.Error("MAIL_SENDER: Error trying to send email - retry #" + retryTimes, ex);
                    if (throwErrorOnException)
                    {
                        retryTimes = 0;
                        throw;
                    }
                    else
                    {
                        ok = false;
                        retryTimes--;
                    }
                }
                if (smtp != null)
                    smtp.Dispose();
                smtp = null;
                if (ok || retryTimes < 0)
                {
                    if (!ok)
                    {
                        result.AddStatusMsg(Enums.STATUS_MSG_TYPE.Error, "Email coult not be sent - stopped trying");
                        _log.Error("MAIL_SENDER: Email could not be sent, stopped trying");
                    }
                    break;
                }
            }

        }

        public virtual OperationResult SendSynchronously(EmailMessage emailMsg, bool throwErrorOnException = false)
        {

            incrementSendCount();
            emailMsg.CheckDefaultSettings();
            emailMsg.VerifyParams();
            OperationResult result = new OperationResult();
            emailMsg.LastOperationResult = result;
            
            List<string> attachmentsStrList = new List<string>();
            List<string> resourcesStrList = new List<string>();

            //bool allOK = false;
            if (emailMsg.To != null && emailMsg.To.Count > 0)
            {
                emailMsg.checkForTokenReplacer();
                try
                {



                    
                    var mainToEmail = emailMsg.To.FirstOrDefault();
                    writeInitSendingLogEntry(emailMsg, mainToEmail);
                    MailMessage msg = new MailMessage(emailMsg.From, mainToEmail);
                    addBccRecipientsToEmail(emailMsg, msg);
                    
                    string plainText = emailMsg.getPlainText();
                    if (!string.IsNullOrEmpty(plainText))
                    {
                        AlternateView viewPlain = AlternateView.CreateAlternateViewFromString(plainText, null, "text/plain");
                        msg.AlternateViews.Add(viewPlain);
                    }
                    if (!string.IsNullOrEmpty(emailMsg.BodyHtml))
                    {
                        AlternateView viewHTML = AlternateView.CreateAlternateViewFromString(emailMsg.BodyHtml, null, "text/html");
                        if (emailMsg.Resources != null)
                        {
                            foreach (var r in emailMsg.Resources)
                            {
                                long len = 0;
                                if (r.ContentStream != null)
                                    len = r.ContentStream.Length;
                                resourcesStrList.Add("Content Id: " + r.ContentId + ", Content Type: " + r.ContentType + ", Size: " + (len / 1024).ToString("#,0.00") + " KB");

                                viewHTML.LinkedResources.Add(r);
                            }
                        }
                        msg.AlternateViews.Add(viewHTML);

                    }
                    if (emailMsg.Attachments != null)
                    {
                        foreach (var a in emailMsg.Attachments)
                        {
                            long len = 0;
                            if (a.ContentStream != null)
                                len = a.ContentStream.Length;
                            attachmentsStrList.Add(a.Name + " (Content Type: " + a.ContentType + ", Content Id: " + a.ContentId + ", Size: " + (len / 1024).ToString("#,0.00") + " KB)");
                            msg.Attachments.Add(a);
                        }

                    }

                    msg.Subject = emailMsg.Subject;
                    if (emailMsg.ReplyTo != null)
                    {
                        msg.ReplyToList.Add(emailMsg.ReplyTo);
                    }

                    sendActualEmail(emailMsg, msg, result, throwErrorOnException);
                    

                    msg.Attachments.Clear();
                    msg.Dispose();




                }
                catch (Exception ex)
                {
                    result.AddException(new InvalidOperationException("Could not send email due to a general error", ex));
                }
            }
            else
            {
                if (CS.General_v3.Util.Other.IsLocalTestingMachine)
                {
                    decrementSendCount();
                    throw new Exceptions.EmailSendException("Cannot send email <" + emailMsg.Subject + "> without specifying at least one 'To' address");
                }
            }
            //clear up
            if (emailMsg.Attachments != null)
            {
                foreach (var a in emailMsg.Attachments)
                {
                    a.Dispose();
                }
            }
            if (emailMsg.Resources != null)
            {
                foreach (var r in emailMsg.Resources)
                {
                    r.Dispose();
                }
            }



            decrementSendCount();
            return result;
        }

    }
}
