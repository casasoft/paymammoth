﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.DataImport
{
    public class ImportResults
    {
        public enum RESULT_TYPE
        {
            OK,
            Error,
            Warning
        }
        public List<ImportResultLine> Lines { get; set; }
        public List<string> ColumnNames { get; set; }
        public string CSVFilename { get; set; }
        public string ZIPFilename { get; set; }
        public bool Success
        {
            get
            {
                return this.ErrorMessages.Count == 0;
            }

        }
        public List<string> ErrorMessages { get; set; }
        private int currLineIndex = 0;
        public ImportResultLine AddNewResultLine()
        {
            ImportResultLine line = new ImportResultLine(currLineIndex, ColumnNames.Count);
            currLineIndex++;
            this.Lines.Add(line);
            return line;
        }
        /*public void AddLine(RESULT_TYPE resultType, params string[] columnValues)
        {
            ImportResultLine line = new ImportResultLine(resultType, columnValues);
            AddLine(line);
        }*/
        public ImportResults(int StartLineIndex )
        {
            //this.Success = true;
            this.Lines = new List<ImportResultLine>();
            this.ColumnNames = new List<string>();
            this.currLineIndex = StartLineIndex;
            this.ErrorMessages = new List<string>();
        }
        public ImportResults(int StartLineIndex, IEnumerable<string> resultTableColumnNames)
            : this(StartLineIndex)
        {
            this.ColumnNames.AddRange(resultTableColumnNames);
        }
        public ImportResults(int StartLineIndex,params string[] resultTableColumnNames) : this (StartLineIndex)
        {
            this.ColumnNames.AddRange(resultTableColumnNames);
        }

    }
}
