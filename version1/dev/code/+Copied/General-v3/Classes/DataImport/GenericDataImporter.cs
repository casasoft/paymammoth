﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Util.Encoding.CSV;
using System.IO;

namespace CS.General_v3.Classes.DataImport
{
    public abstract class GenericDataImporter : GenericDataImporterBase
    {

        protected abstract List<ColumnInfo> getColumnInfos();
        private void parseColumns()
        {
            for(int i=0; i< this.Columns.Count;i++)
            {
                var col = this.Columns[i];
                col.ColumnIndex = i;
            }
        }
        public List<ColumnInfo> Columns { get; private set; }
        public GenericDataImporter()
        {
            this.Columns = getColumnInfos();
            parseColumns();


        }

        /// <summary>
        /// Adds patsh to look for
        /// </summary>
        /// <param name="paths">Can be comma seperatred or pipe-line</param>
        public void AddPathsToLookForFiles( string paths)
        {
            if (!string.IsNullOrEmpty(paths))
            {
                string[] tokens = paths.Split(new string[] { ",", "|" }, StringSplitOptions.RemoveEmptyEntries); ;
                foreach (var t in tokens)
                {
                    string path = t.Trim();
                    if (Directory.Exists(path))
                    {
                        this.FilePathsToSearchForFiles.Add(path.ToLower());
                    }
                }
            }
        }

        protected override IEnumerable<string> getCSVFileHeaders()
        {
            List<string> list = new List<string>();
            foreach (var c in Columns)
            {
                list.Add(c.Title);
            }

            return list;
            
        }
        protected override bool preProcessLine(CSVLine csvLine, ImportResultLine resultsLine)
        {
            bool ok = true;
            for (int i=0; i< Columns.Count;i++)
            {
                string s = csvLine[i];
                var col = this.Columns[i];
                if (!col.ParseString(s, resultsLine))
                {
                    ok = false;
                    break;
                }
            }
            return ok;
            
        }

    }
}
