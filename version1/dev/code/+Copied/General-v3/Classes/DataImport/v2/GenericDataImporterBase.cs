﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Util.Encoding.CSV;
using System.IO;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;

namespace CS.General_v3.Classes.DataImport.v2
{
    public abstract class GenericDataImporterBase_v2
    {
        public int FlushSessionEveryNumOfLines { get; set; }
        public GenericDataImporterBase_v2()
        {
            this.FilePathsToSearchForFiles = new Iesi.Collections.Generic.HashedSet<string>();
            this.HeaderIndex = 0;
            this.TotalLinesToskipAfterHeader = 0;
            FlushSessionEveryNumOfLines = 100;
        }
        public ImportResults Results {get;set;}
        protected CSVFile csvFile = null;
        protected bool getBoolFromString(string s)
        {
            return getBoolNullableFromString(s).GetValueOrDefault();
        }

        /// <summary>
        /// Index where header is located (starts at 0);
        /// </summary>
        public int HeaderIndex { get; set; }
        public int TotalLinesToskipAfterHeader{ get; set; }

        protected abstract IEnumerable<string> getCSVFileHeaders();

        private int getStartLineIndex()
        {
            return this.HeaderIndex + TotalLinesToskipAfterHeader + 1;
        }
        private bool checkHeadings()
        {
            var headings = getCSVFileHeaders();
            bool result = true;
            if (headings != null && headings.Count() > 0)
            {
                int i = 0;
                var line = csvFile[HeaderIndex];
                foreach (var h in headings)
                {
                    if (string.Compare(line[i], h, true) != 0)
                    {
                        result = false;
                        Results.ErrorMessages.Add("First line does not contain correct headings");
                        break;
                    }
                    i++;
                }
            }
            return result;

        }


        protected bool? getBoolNullableFromString(string s)
        {
            s = s ?? "";
            s = s.ToLower();
            if (string.IsNullOrEmpty(s))
                return null;
            else
                return s == "1" || s == "y" || s == "yes" || s == "true";
        }
        private string processStringForNumbers(string s)
        {
            s = CS.General_v3.Util.Text.ReplaceTexts(s, "", ",", "€", " ", "$").Trim();
            return s;
        }

        protected int? getIntFromString(string s)
        {
            s = processStringForNumbers(s);
            double? d = getDoubleFromString(s);
            if (d.HasValue)
                return (int)Math.Round(d.Value);
            else
                return null;

        }
        
        protected double? getDoubleFromString(string s)
        {
            s = processStringForNumbers(s);
            if (string.IsNullOrEmpty(s))
                return null;
            else
                return Convert.ToDouble(s);
        }
        /// <summary>
        /// Preprocess line.  If result is false, it wont be processed
        /// </summary>
        /// <param name="csvLine"></param>
        /// <returns></returns>
        protected virtual bool preProcessLine(CSVLine csvLine, ImportResultLine resultsLine)
        {
            return true;
        }
        protected abstract void processLine(CSVLine csvLine, ImportResultLine resultsLine);
        protected virtual bool preProcessFile(CSVFile csvFile)
        {
            return true;
        }
        protected virtual void postProcessFile()
        {

        }
        public ImportResults ProcessFile(CSVFile csvFile)
        {
            this.csvFile = csvFile;
            this.Results = new ImportResults(this.getStartLineIndex(), getCSVFileHeaders());
            int flushCount = 0;
            if (checkHeadings() && preProcessFile(csvFile))
            {
                for (int i = getStartLineIndex(); i < csvFile.Count; i++ )
                {
                    var csvLine = csvFile[i];

                    if (csvLine.CheckIfAtLeastOneFilled())
                    {
                        ImportResultLine resultsLine = this.Results.AddNewResultLine(csvLine);
                        if (preProcessLine(csvLine, resultsLine))
                        {
                            processLine(csvLine, resultsLine);
                            flushCount++;
                            if (flushCount >= FlushSessionEveryNumOfLines)
                            {
                                NHManager.Instance.FlushCurrentSessionInContext();
                                flushCount = 0;
                            }
                        }
                    }
                }
            }
            postProcessFile();
            removeExtractedFiles();
            
            return this.Results;
        }

        public ImportResults ProcessFile(string sCSVFile)
        {
            CSVFile csvFile = new CSVFile(sCSVFile);
            return this.ProcessFile(csvFile);
        }

        private string tmpImagesDir = null;
        
        /// <summary>
        /// Returns an image from the extracted files
        /// </summary>
        /// <param name="imageFilename"></param>
        /// <param name="filenameIncludesRegExp"></param>
        /// <returns></returns>
        private System.IO.FileInfo _getFileFromFileSystem(string filename, bool filenameIncludesRegExp = false)
        {
            FileInfo f = null;
            List<string> pathsToSearch = new List<string>();
            
            if (!string.IsNullOrEmpty(tmpImagesDir))
            {
                pathsToSearch.Add(tmpImagesDir);
            }
            foreach (var path in FilePathsToSearchForFiles)
            {
                var files = CS.General_v3.Util.IO.FindFilesInFolder(path, filename, filenameIncludesRegExp, true);
                if (files.Count > 0)
                {
                    f = files[0];
                    break;
                }
            }
            return f;
        }

        public Iesi.Collections.Generic.HashedSet<string> FilePathsToSearchForFiles { get; private set; }

        protected FileInfo getImageFromFileSystem(string imgName)
        {
            string s = CS.General_v3.Util.IO.GetFilenameOnly(imgName);
            string regex = s + ".(" + CS.General_v3.Util.Image.GetImageFilesExtensions().ConvertListToString(",") + ")";
            return getFileFromFileSystem(regex, true);

        }

        protected FileInfo getFileFromFileSystem(string filename, bool filenameIncludesRegExp=false)
        {
            var fs = getFileFromFileSystem(filename, filenameIncludesRegExp);

            if (fs != null)
            {
                return fs;
                
                
            }
            else
            {
                return null;
            }
        }

        public void ExtractZIPFile(string zipPath)
        {
            if (!string.IsNullOrEmpty(zipPath))
            {
                string localPath = "";
                if (zipPath.Contains("/"))
                    localPath = CS.General_v3.Util.PageUtil.MapPath(zipPath);
                else
                    localPath = zipPath;
                if (System.IO.File.Exists(localPath))
                {
                    FileStream fs = new FileStream(localPath, FileMode.Open, FileAccess.Read);
                    ExtractZIPFile(fs);
                    fs.Dispose();
                }
            }
        }

        public void ExtractZIPFile(System.IO.Stream zipFile)
        {
            if (zipFile != null && zipFile.Length > 0)
            {
                string tmpZipPath = CS.General_v3.Util.IO.GetTempFilename("zip");
                CS.General_v3.Util.IO.SaveStreamToFile(zipFile, tmpZipPath);
                tmpImagesDir = CS.General_v3.Util.IO.CreateTempDir();
                CS.General_v3.Util.ZIPUtil.ExtractZIP(tmpZipPath, tmpImagesDir);
                CS.General_v3.Util.IO.DeleteFile(tmpZipPath);

            }
        }
        /// <summary>
        /// Removes extract files. No need to call, as it is automatically called.
        /// </summary>
        protected void removeExtractedFiles()
        {
            if (!string.IsNullOrEmpty(tmpImagesDir))
            {
                CS.General_v3.Util.IO.DeleteDirectory(tmpImagesDir);
            }
        }
    }
}
