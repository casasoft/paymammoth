﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Util.Encoding.CSV;
using System.IO;

namespace CS.General_v3.Classes.DataImport.v2
{
    public abstract class GenericDataImporter_v2 : GenericDataImporterBase_v2
    {
        public bool AllowColumnsNotToBeExactlyInSameOrder { get; set; }

        protected abstract List<ColumnInfo> getColumnInfos();
        private void parseColumns()
        {
            for(int i=0; i< this.Columns.Count;i++)
            {
                var col = this.Columns[i];
                col.ColumnIndex = i;
            }
        }
        public List<ColumnInfo> Columns { get; private set; }
        public GenericDataImporter_v2()
        {
            this.AllowColumnsNotToBeExactlyInSameOrder = true;
            this.Columns = getColumnInfos();
            parseColumns();


        }




        protected override IEnumerable<string> getCSVFileHeaders()
        {
            List<string> list = new List<string>();
            foreach (var c in Columns)
            {
                list.Add(c.GetMainTitle());
            }

            return list;
            
        }
        protected override bool preProcessLine(CSVLine csvLine, int lineIndex, ImportResultLine resultsLine)
        {
            bool ok = true;
            for (int i=0; i< Columns.Count;i++)
            {
                string s = csvLine[i];
                var col = this.Columns[i];
                if (!col.ParseString(s, resultsLine))
                {
                    ok = false;
                    break;
                }
            }
            return ok;
            
        }


        #region IGenericDataImporter_v2 Members


       
        #endregion
    }
}
