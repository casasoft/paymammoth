﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Util.Encoding.CSV;

namespace CS.General_v3.Classes.DataImport.v2
{
    public class ImportResultLine
    {

        
        //p/ublic List<string> ErrorMessages { get; set; }
      //  public List<string> WarningMessages { get; set; }
   //     public List<string> ColumnValues { get; private set; }
        
        public OperationResult Result { get; private set; }
        public int LineIndex { get; set; }

        public CSVLine CsvLine { get; private set; }
        public ImportResultLine(int lineIndex, CSVLine line) 
        {
            
        //    this.ColumnValues = new List<string>();
            this.LineIndex = lineIndex;
            this.Result = new OperationResult();
            this.CsvLine = line;

        }

        //public void SetColumnValues(params string[] colValues)
        //{
        //    for (int i = 0; i < colValues.Length; i++)
        //    {
        //        var colVal = colValues[i];
        //        ColumnValues.Add(colVal);
        //    }
        //}


        /*   public void AddErrorMessage(string msg)
        {
            this.ErrorMessages.Add(msg);
            this.ResultType = ImportResults.RESULT_TYPE.Error;
        }
        public void AddWarningMessage(string msg)
        {
            this.WarningMessages.Add(msg);
            if (this.ResultType == ImportResults.RESULT_TYPE.OK)
                this.ResultType = ImportResults.RESULT_TYPE.Warning;    
        }*/

    }
}
