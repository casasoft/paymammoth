using System;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Util.Encoding.CSV;

namespace CS.General_v3.Classes.DataImport.v2
{
    public interface IGenericDataImporter_v2
    {
        /// <summary>
        /// Adds patsh to look for
        /// </summary>
        /// <param name="paths">Can be comma seperatred or pipe-line</param>
        void AddPathsToLookForFiles( string paths);

        int FlushSessionEveryNumOfLines { get; set; }
        ImportResults Results { get; set; }
        bool TerminateImportJob();
        int GetTotalLineCount();
        string CurrentCsvFilename { get; set; }
        string CurrentZipFilename { get; set; }
        OperationResult ProcessFile(CSVFile csvFile);
        void ExtractZIPFile(string zipPath);
        void ExtractZIPFile(System.IO.Stream zipFile);
        int CurrentLineIndex { get; }
        bool IsFinished();
        DateTime? StartedOn { get; }
        DateTime? FinishedOn { get; }

        bool IsRunning();

        bool RemoveAnyExistingImages { get; set; }
    }
}