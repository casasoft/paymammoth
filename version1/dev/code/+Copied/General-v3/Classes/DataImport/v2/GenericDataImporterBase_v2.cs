﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.NHManager;
using CS.General_v3.Util.Encoding.CSV;
using System.IO;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using log4net;
using CS.General_v3.Classes.ZIP;
using CS.General_v3.Classes.HelperClasses;

namespace CS.General_v3.Classes.DataImport.v2
{
    public abstract class GenericDataImporterBase_v2: IGenericDataImporter_v2
    {
        public int FlushSessionEveryNumOfLines { get; set; }
        public GenericDataImporterBase_v2()
        {
            _log = LogManager.GetLogger(this.GetType().Name);
            this.FilePathsToSearchForFiles = new Iesi.Collections.Generic.HashedSet<string>();
            
            this.TotalLinesToskipAfterHeader = 0;
            FlushSessionEveryNumOfLines = 100;
        }
        public ImportResults Results {get;set;}
        protected CSVFile csvFile = null;
        protected bool getBoolFromString(string s)
        {
            return getBoolNullableFromString(s).GetValueOrDefault();
        }
        private bool checkForHeaderIndex()
        {
            headerIndex = null;
            int result = getHeaderIndex();
            if (result == -1)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Could not find line with correct headings. Format should be:");
                sb.AppendLine("");
                var headings = getCSVFileHeaders().ToList();
                for (int i = 0; i < headings.Count; i++)
                {
                    var h = headings[i];
                    if (i > 0)
                        sb.Append(" | ");
                    sb.Append(h);
                }

                //esults.GeneralImportResult.AddStatusMsg(Enums.STATUS_MSG_TYPE.Error, "Column #" + (i + 1) + ": Title <" + colName + "> does not match the required heading <" + hColName + ">");
                Results.GeneralImportResult.AddStatusMsg( Enums.STATUS_MSG_TYPE.Error,sb.ToString());
            }
            return headerIndex.HasValue;
        }

        protected bool _terminate = false;
        private readonly object _padlock = new object();

        protected virtual object getPadlock()
        {
            return _padlock;

        }

        public bool TerminateImportJob()
        {
            bool ok = false;
            if (_started)
            {
                this.Results.GeneralImportResult.AddStatusMsg(Enums.STATUS_MSG_TYPE.Warning, "Data import job terminated");
                _terminate = true;
                ok = true;
            }
            return ok;
        }
        private int? headerIndex = null;
        /// <summary>
        /// Index where header is located (starts at 0);
        /// </summary>
        protected int getHeaderIndex()
        {
            if (!headerIndex.HasValue)
            {
                for (int i = 0; i < csvFile.Count; i++)
                {

                    if (checkHeadings(i))
                    {
                        headerIndex = i;
                        break;
                    }
                }
            }
            if (headerIndex.HasValue)
                return headerIndex.Value;
            else
                return -1;

        }

        public int TotalLinesToskipAfterHeader{ get; set; }

        protected abstract IEnumerable<string> getCSVFileHeaders();

        private int getStartLineIndex()
        {
            return this.getHeaderIndex() + TotalLinesToskipAfterHeader + 1;
        }
        private bool checkHeadings(int lineIndex)
        {
            var headings = getCSVFileHeaders();
            bool result = true;
            if (headings != null && headings.Count() > 0)
            {
                int i = 0;
                var line = csvFile[lineIndex];
                

                foreach (var h in headings)
                {
                    string colName = line[i].Trim();
                    string hColName = h.Trim();
                    if (string.Compare(colName, hColName, true) != 0)
                    {
                        result = false;
                        //Results.GeneralImportResult.AddStatusMsg( Enums.STATUS_MSG_TYPE.Error, "Column #" + (i + 1) + ": Title <" + colName + "> does not match the required heading <" + hColName  + ">");
                        
                    }
                    i++;
                }
            }
            return result;

        }


        protected bool? getBoolNullableFromString(string s)
        {
            s = s ?? "";
            s = s.ToLower();
            if (string.IsNullOrEmpty(s))
                return null;
            else
                return s == "1" || s == "y" || s == "yes" || s == "true";
        }
        private string processStringForNumbers(string s)
        {
            s = CS.General_v3.Util.Text.ReplaceTexts(s, "", ",", "€", " ", "$").Trim();
            return s;
        }

        protected int? getIntFromString(string s)
        {
            s = processStringForNumbers(s);
            double? d = getDoubleFromString(s);
            if (d.HasValue)
                return (int)Math.Round(d.Value);
            else
                return null;

        }
        
        protected double? getDoubleFromString(string s)
        {
            s = processStringForNumbers(s);
            if (string.IsNullOrEmpty(s))
                return null;
            else
                return Convert.ToDouble(s);
        }
        /// <summary>
        /// Preprocess line.  If result is false, it wont be processed
        /// </summary>
        /// <param name="csvLine"></param>
        /// <returns></returns>
        protected virtual bool preProcessLine(CSVLine csvLine, int lineIndex, ImportResultLine resultsLine)
        {
            return true;
        }
        protected abstract void processLine(CSVLine csvLine, int lineIndex, ImportResultLine resultsLine);
        protected virtual bool preProcessFile(CSVFile csvFile)
        {
            return true;
        }
        protected virtual void postProcessFile()
        {

        }
        protected ImportResultLine currentResultLine { get; private set; }
        protected CSVLine currentCsvLine { get; private set; }
        protected ILog _log { get; private set; }
        
        protected bool _started { get; private set; }

        public int CurrentLineIndex { get; private set; }

        public int GetTotalLineCount()
        {
            int cnt = 0;
            if (csvFile != null)
                cnt = csvFile.Count;
            return cnt;
            
        }
        protected virtual void onPreStart()
        {

        }

        private void start()
        {
            this.FinishedOn = null;
            NHClasses.NhManager.CreateNewSessionForContext();
            int flushCount = 0;
            onPreStart();
            for (int i = getStartLineIndex(); i < csvFile.Count; i++)
            {
                this.CurrentLineIndex = i;
                var csvLine = csvFile[i];

                _log.Debug("Processing line #" + i);
                if (!csvLine.IsLineBlank())
                {
                    ImportResultLine resultsLine = this.Results.AddNewResultLine(csvLine);
                    this.currentResultLine = resultsLine;
                    this.currentCsvLine = csvLine;
                    if (preProcessLine(csvLine, i, resultsLine))
                    {
                        processLine(csvLine, i, resultsLine);
                        flushCount++;
                        if (flushCount >= FlushSessionEveryNumOfLines)
                        {
                            var session = NHClasses.NhManager.GetCurrentSessionFromContext();
                            session.Flush();
                            //session.Clear();

                            flushCount = 0;
                        }
                    }
                }
                if (_terminate)
                {
                    break;
                }
            }
            _log.Debug("Finished importing all lines, disposing session");
            NHClasses.NhManager.DisposeCurrentSessionInContext();
            _log.Debug("Disposed session successfully");
            onJobEnd();
            _log.Debug("Import done successfully");
        }

        private void onJobEnd()
        {
            postProcessFile();
            removeExtractedFiles();
            _terminate = false;
            _started = false;
            this.FinishedOn = CS.General_v3.Util.Date.Now;
        }
        public DateTime? StartedOn { get; private set; }
        public DateTime? FinishedOn { get; private set; }

        protected virtual void onJobStarted()
        {

        }

        protected virtual void startProcessingFile(CSVFile csvFile)
        {
            
            lock (getPadlock())
            {
                this.StartedOn = CS.General_v3.Util.Date.Now;
                _terminate = false;
                _started = true;
                this.csvFile = csvFile;
                this.Results = new ImportResults(this.getStartLineIndex(), getCSVFileHeaders());
                bool ok = checkForHeaderIndex();
                if (ok && preProcessFile(csvFile))
                {
                    onJobStarted();
                    CS.General_v3.Util.ThreadUtil.CallMethodOnSeperateThread(start, System.Threading.ThreadPriority.Lowest);
                }
                else
                {
                    onJobEnd();
                }

            }
        }

        private OperationResult _operationResult = null;
        public virtual OperationResult ProcessFile(CSVFile csvFile)
        {
            OperationResult result = new OperationResult();

             if (_started)
            {
                result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, "Another import process is already running. Please wait for it to finish or terminate it in order to start a new one");
                
            }
            else
            {
                startProcessingFile(csvFile);
            }
             return result;
            
            
        }

        public OperationResult ProcessFile(string sCSVFile)
        {
            CSVFile csvFile = new CSVFile(sCSVFile);
            return this.ProcessFile(csvFile);
        }

        private string tmpImagesDir = null;
        
        /// <summary>
        /// Returns an image from the extracted files
        /// </summary>
        /// <param name="imageFilename"></param>
        /// <param name="filenameIncludesRegExp"></param>
        /// <returns></returns>
        private IEnumerable< System.IO.FileInfo> _getFilesFromFileSystem(string filename, bool filenameIncludesRegExp = false)
        {
            List<FileInfo> fileResults = new List<FileInfo>();
            
            List<string> pathsToSearch = new List<string>();
            
            if (!string.IsNullOrEmpty(tmpImagesDir))
            {
                pathsToSearch.Add(tmpImagesDir);
            }
            foreach (var path in FilePathsToSearchForFiles)
            {
                string sPath = path;
                if (sPath.Contains("/"))
                    sPath = CS.General_v3.Util.PageUtil.MapPath(sPath);
                var files = CS.General_v3.Util.IO.FindFilesInFolder(sPath, filename, filenameIncludesRegExp, true);
                if (files.Count > 0)
                {
                    fileResults.AddRange(files);
                }
            }
            return fileResults;
        }

        /// <summary>
        /// Adds patsh to look for
        /// </summary>
        /// <param name="paths">Can be comma seperatred or pipe-line</param>
        public void AddPathsToLookForFiles(string paths)
        {
            if (!string.IsNullOrWhiteSpace(paths))
            {
                string[] tokens = paths.Split(new string[] { ",", "|" }, StringSplitOptions.RemoveEmptyEntries); ;
                foreach (var t in tokens)
                {
                    string path = t.Trim();
                    if (Directory.Exists(path))
                    {
                        this.FilePathsToSearchForFiles.Add(path.ToLower());
                    }
                }
            }
        }
        public Iesi.Collections.Generic.HashedSet<string> FilePathsToSearchForFiles { get; private set; }

        protected IEnumerable<FileInfo> getImagesFromFileSystem(string imgNameRegexp)
        {
            //string s = CS.General_v3.Util.IO.GetFilenameOnly(imgNameRegexp);
            //string regex = s + ".(" + CS.General_v3.Util.Image.GetImageFilesExtensions().ConvertListToString("|") + ")";
            var files = getFilesFromFileSystem(imgNameRegexp, true).ToList();
            var imgExtensions = CS.General_v3.Util.Image.GetImageFilesExtensions().ConvertAll<string>(item=>item.ToLower());
            for (int i = 0; i < files.Count; i++)
            {
                var f = files[i];
                string ext = CS.General_v3.Util.IO.GetExtension(f.Name);

                if (!imgExtensions.Contains(ext.ToLower()))
                {
                    files.Remove(f);
                    i--;
                }
            }
            return files;

        }

        protected IEnumerable<FileInfo> getFilesFromFileSystem(string filename, bool filenameIncludesRegExp = false)
        {
            var fs = _getFilesFromFileSystem(filename, filenameIncludesRegExp);

            if (fs != null)
            {
                return fs;
                
                
            }
            else
            {
                return null;
            }
        }

        public void ExtractZIPFile(string zipPath)
        {
            if (!string.IsNullOrEmpty(zipPath))
            {
                string localPath = "";
                if (zipPath.Contains("/"))
                    localPath = CS.General_v3.Util.PageUtil.MapPath(zipPath);
                else
                    localPath = zipPath;
                if (System.IO.File.Exists(localPath))
                {
                    FileStream fs = new FileStream(localPath, FileMode.Open, FileAccess.Read);
                    ExtractZIPFile(fs);
                    fs.Dispose();
                }
            }
        }

        public void ExtractZIPFile(System.IO.Stream zipFile)
        {
            if (zipFile != null && zipFile.Length > 0)
            {
                string tmpZipPath = CS.General_v3.Util.IO.GetTempFilename("zip");
                CS.General_v3.Util.IO.SaveStreamToFile(zipFile, tmpZipPath);
                tmpImagesDir = CS.General_v3.Util.IO.CreateTempDir();
                ZipExtractor zipExtractor = new ZipExtractor(EnumsZip.ZIP_COMPONENT_TYPE.IonicZip);
                zipExtractor.ExtractZIP(tmpZipPath, tmpImagesDir);
                CS.General_v3.Util.IO.DeleteFile(tmpZipPath);

            }
        }
        /// <summary>
        /// Removes extract files. No need to call, as it is automatically called.
        /// </summary>
        protected void removeExtractedFiles()
        {
            if (!string.IsNullOrEmpty(tmpImagesDir))
            {
                CS.General_v3.Util.IO.DeleteDirectory(tmpImagesDir);
            }
        }

        #region IGenericDataImporter_v2 Members


        public string CurrentCsvFilename { get; set; }

        public string CurrentZipFilename { get; set; }

        public bool IsFinished()
        {
            return !_started;

        }


        #endregion

        #region IGenericDataImporter_v2 Members


        public bool IsRunning()
        {
            return _started;
            
        }

        #endregion

        #region IGenericDataImporter_v2 Members


        public bool RemoveAnyExistingImages { get; set; }

        #endregion
    }
}
