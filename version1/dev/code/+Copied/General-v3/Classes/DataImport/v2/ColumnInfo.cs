﻿using System;
using System.Linq;
using Iesi.Collections.Generic;

namespace CS.General_v3.Classes.DataImport.v2
{
    public class ColumnInfo
    {
        public int ColumnIndex { get; set; }
        public HashedSet<string> Titles { get; private set; }

        public bool Required { get; set; }
        public bool WarningOnEmpty { get; set; }
        public bool WarningOnInvalidDataType { get; set; }
        public Enums.DATA_TYPE DataType { get; set; }
        public void AddAlternateTitle(Enum columnName)
        {
            string title = CS.General_v3.Util.EnumUtils.StringValueOf(columnName);
            AddAlternateTitle(title);
            
        }
        public void AddAlternateTitle(string columnName)
        {


            this.Titles.Add(columnName);
        }

        public ColumnInfo(Enum columnName, bool required = true, bool warningOnEmpty = false, bool warningOnInvalidDataType = false, Enums.DATA_TYPE dataType = Enums.DATA_TYPE.String)
        {
            string title = CS.General_v3.Util.EnumUtils.StringValueOf(columnName);
            init(title, required, warningOnEmpty, warningOnInvalidDataType, dataType);
        }
        private void init(string Title, bool required = true, bool warningOnEmpty = false, bool warningOnInvalidDataType = false, Enums.DATA_TYPE dataType = Enums.DATA_TYPE.String)
        {
            this.Titles = new HashedSet<string>();
            AddAlternateTitle(Title);
            
            this.Required = required;
            this.WarningOnInvalidDataType = warningOnInvalidDataType;
            this.DataType = dataType;
            this.WarningOnEmpty = warningOnEmpty;
        }
        public string GetMainTitle()
        {
            return this.Titles.ToList()[0];
        }

        public ColumnInfo(string Title, bool required = true,bool warningOnEmpty = false, bool warningOnInvalidDataType = false, Enums.DATA_TYPE dataType = Enums.DATA_TYPE.String)
        {
            init(Title, required, warningOnEmpty, warningOnInvalidDataType, dataType);

        }

        public bool ParseString(string s, ImportResultLine lineResult)
        {
            bool ok = true;
            if (ok && string.IsNullOrWhiteSpace(s))
            {

                if (Required)
                {
                    ok = false;
                    lineResult.Result.AddStatusMsg(Enums.STATUS_MSG_TYPE.Error, "Column <" + GetMainTitle() + "> is required");
                }
                else if (WarningOnEmpty)
                {
                    lineResult.Result.AddStatusMsg(Enums.STATUS_MSG_TYPE.Warning, "Column <" + GetMainTitle() + "> is empty");
                }
                
            }
            {
                string dataTypeErrMsg = null;
                switch (DataType)
                {
                    case Enums.DATA_TYPE.Double:
                    case Enums.DATA_TYPE.DoubleNullable:
                    case Enums.DATA_TYPE.Integer:
                    case Enums.DATA_TYPE.IntegerMultipleChoice:
                    case Enums.DATA_TYPE.IntegerNullable:
                        {
                            double d = 0;
                            if (!double.TryParse(s, out d))
                            {
                                ok = false;
                                dataTypeErrMsg = "Column <" + GetMainTitle() + "> value of '" + s + "' could not be parsed into a number";
                            }
                        }
                        break;
                    case Enums.DATA_TYPE.Bool:
                    case Enums.DATA_TYPE.BoolNullable:
                        {
                            bool? b = CS.General_v3.Util.Other.TextToBoolNullable(s);
                            if (b == null)
                            {
                                ok = false;
                                dataTypeErrMsg = "Column <" + GetMainTitle() + "> value of '" + s + "' could not be parsed into Yes/No (boolean) value";
                            }
                        }
                        break;
                }
                if (!string.IsNullOrEmpty(dataTypeErrMsg))
                {
                    if (WarningOnInvalidDataType)
                        lineResult.Result.AddStatusMsg(Enums.STATUS_MSG_TYPE.Warning,  dataTypeErrMsg);
                    else
                        lineResult.Result.AddStatusMsg(Enums.STATUS_MSG_TYPE.Error, dataTypeErrMsg);
                }
            }
            return ok;
        }


    }
}
