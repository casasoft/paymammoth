﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Util.Encoding.CSV;

namespace CS.General_v3.Classes.DataImport
{
    public class ColumnInfo
    {
        public int ColumnIndex { get; set; }
        public string Title { get; set; }
        public bool Required { get; set; }
        public bool WarningOnEmpty { get; set; }
        public bool WarningOnInvalidDataType { get; set; }
        public Enums.DATA_TYPE DataType { get; set; }

        public ColumnInfo(string Title, bool required = true,bool warningOnEmpty = false, bool warningOnInvalidDataType = false, Enums.DATA_TYPE dataType = Enums.DATA_TYPE.String)
        {
           this.Title = Title;
            this.Required =required;
            this.WarningOnInvalidDataType = warningOnInvalidDataType;
            this.DataType = dataType;
            this.WarningOnEmpty = warningOnEmpty;

        }

        public bool ParseString(string s, ImportResultLine lineResult)
        {
            bool ok = true;
            if (ok && string.IsNullOrWhiteSpace(s))
            {

                if (Required)
                {
                    ok = false;
                    lineResult.AddValueAsError(ColumnIndex, "Column <" + Title + "> is required");
                }
                else if (WarningOnEmpty)
                {
                    lineResult.AddValueAsWarning(ColumnIndex, "Column <" + Title + "> is empty");
                }
                
            }
            {
                string dataTypeErrMsg = null;
                switch (DataType)
                {
                    case Enums.DATA_TYPE.Double:
                    case Enums.DATA_TYPE.DoubleNullable:
                    case Enums.DATA_TYPE.Integer:
                    case Enums.DATA_TYPE.IntegerMultipleChoice:
                    case Enums.DATA_TYPE.IntegerNullable:
                        {
                            double d = 0;
                            if (!double.TryParse(s, out d))
                            {
                                ok = false;
                                dataTypeErrMsg = "Column <" + Title + "> value of '" + s + "' could not be parsed into a number";
                            }
                        }
                        break;
                    case Enums.DATA_TYPE.Bool:
                    case Enums.DATA_TYPE.BoolNullable:
                        {
                            bool? b = CS.General_v3.Util.Other.TextToBoolNullable(s);
                            if (b == null)
                            {
                                ok = false;
                                dataTypeErrMsg = "Column <" + Title + "> value of '" + s + "' could not be parsed into Yes/No (boolean) value";
                            }
                        }
                        break;
                }
                if (!string.IsNullOrEmpty(dataTypeErrMsg))
                {
                    if (WarningOnInvalidDataType)
                        lineResult.AddValueAsWarning(ColumnIndex, dataTypeErrMsg);
                    else
                        lineResult.AddValueAsError(ColumnIndex, dataTypeErrMsg);
                }
            }
            return ok;
        }


    }
}
