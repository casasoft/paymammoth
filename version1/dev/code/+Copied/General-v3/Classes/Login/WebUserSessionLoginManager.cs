﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CS.General_v3.Util;
using log4net;
using Brickred.SocialAuth.NET.Core.BusinessObjects;

namespace CS.General_v3.Classes.Login
{
    /// <summary>
    /// Login user factory.
    /// 
    /// Please fill 'LoginURL' and 'WelcomeUrl';
    /// </summary>
    public abstract class WebUserSessionLoginManager<TUser> : UserSessionLoginManager<TUser>, IWebUserSessionLoginManager<TUser>
        where TUser : class, IUser
    {
        private ILog _log = null;
        public WebUserSessionLoginManager(string welcomeURL, string loginID = "WebUserLogin")
            : base(loginID)
        {
            _log = LogManager.GetLogger(this.GetType());

            this.ID = loginID;
            this.WelcomeUrl = welcomeURL;
            this.HoursToRememberLoggedUser = 7 * 24; // 7 days;
            this.HoursToRememberTemporaryUser = 14 * 24; // 14 days;

        }



        private System.Web.UI.Page pg = null;

        public override bool CheckAuthentication()
        {
            UpdateLastUrl();
            return base.CheckAuthentication();
        }
        public string ID { get; private set; }
        public string WelcomeUrl { get; set; }
        public string lastUrlForCurrentSession
        {
            get
            {
                return CS.General_v3.Util.PageUtil.GetSessionObject<string>(getSessionKey("lastUrlForCurrentSession"));
            }
            set
            {
                CS.General_v3.Util.PageUtil.SetSessionObject(getSessionKey("lastUrlForCurrentSession"), value);

            }
        }
        public void UpdateLastUrl()
        {
            lastUrlForCurrentSession = CS.General_v3.Util.PageUtil.GetCurrentUrlLocation().GetURL(fullyQualified: false);

        }
        public string LoginUrl { get; set; }
        protected override void loginUser(TUser user, bool fromAdmin)
        {
            base.loginUser(user, fromAdmin);
            onLoggedInUser(user, fromAdmin, false);
        }
        private void doAutoRedirection()
        {
            if (string.IsNullOrEmpty(WelcomeUrl))
                throw new InvalidOperationException("Please fill in 'WelcomeUrl'");
            if (!string.IsNullOrEmpty(lastUrlForCurrentSession))
            {
                CS.General_v3.Util.PageUtil.RedirectPage(lastUrlForCurrentSession);
            }
            else
            {
                CS.General_v3.Util.PageUtil.RedirectPage(WelcomeUrl);
            }
        }
        protected virtual void onLoggedInUser(TUser user, bool fromAdmin, bool autoRedirect)
        {
            if (autoRedirect)
                doAutoRedirection();

        }
        /* public override Enums.LOGIN_STATUS Login(string username, string password, bool fromAdmin)
         {
             return Login(username, password, fromAdmin, autoRedirect: false, rememberMe: false);
         }*/
        protected string getCookieName()
        {
            return this.ID + "_user_rm";
        }
        public override bool IsLoggedIn
        {
            get { return IsLoggedInAlsoFromRememberMe; }
        }

        public bool IsLoggedInAlsoFromRememberMe
        {
            get
            {
                return GetLoggedInUserAlsoFromRememberMe() != null;
            }
        }
        public TUser GetLoggedInUserAlsoFromRememberMe()
        {
            TUser user = checkRememberedUser();
            if (user == null)
            {
                user = getUserFromSocialLogin(autoLogin: true);
                checkRememberMeFromSocial(user);
            }
            return user;
        }

        private void checkRememberMeFromSocial(TUser user)
        {
            if (user != null)
            {
                bool? rememberMe =
                    CS.General_v3.Util.SessionUtil.GetObject<bool?>(
                        CS.General_v3.Constants.SESSION_SOCIAL_LOGIN_REMEMBERME);
                if (rememberMe.HasValue && rememberMe.Value)
                {
                    cookie_RememberMeID = user.ID;
                    cookie_RememberMeGUID = user.SessionGUID;
                }
                CS.General_v3.Util.SessionUtil.RemoveObject(CS.General_v3.Constants.SESSION_SOCIAL_LOGIN_REMEMBERME);
            }
        }
        private void rememberCurrentUser()
        {
            var user = this.GetLoggedInUser();
            cookie_RememberMeID = user.ID;
            cookie_RememberMeGUID = user.SessionGUID;

        }
        public int HoursToRememberLoggedUser { get; set; }
        public int HoursToRememberTemporaryUser { get; set; }
        private long? cookie_RememberMeID
        {
            get
            {
                return CS.General_v3.Util.PageUtil.GetCookieLongNullable(getCookieName(), "RememberMeID");
            }
            set
            {
                CS.General_v3.Util.PageUtil.SetCookie(getCookieName(), "RememberMeID", (value != null ? value.ToString() : ""), CS.General_v3.Util.Date.Now.AddHours(HoursToRememberLoggedUser));

            }

        }

        private string cookie_RememberMeGUID
        {
            get
            {
                return CS.General_v3.Util.PageUtil.GetCookie(getCookieName(), "RememberMeGUID");
            }
            set
            {
                CS.General_v3.Util.PageUtil.SetCookie(getCookieName(), "RememberMeGUID", value, CS.General_v3.Util.Date.Now.AddHours(HoursToRememberLoggedUser));

            }

        }


        public override void Logout()
        {

            base.Logout();
            this.cookie_RememberMeGUID = null;
            this.cookie_RememberMeID = null;
        }
        //protected abstract TUser createNewTemporaryUser();
        //protected void clearTemporaryUser()
        //{
        //    this.cookie_TempMemberGUID = null;
        //    this.cookie_TempMemberID = null;
        //}
        //protected virtual TUser _getTemporaryUser()
        //{
        //    TUser user = default(TUser);

        //    if (CS.General_v3.Util.PageUtil.CheckIfCookiesAreEnabled())
        //    {
        //        long? memberID = cookie_TempMemberID;
        //        if (memberID.HasValue)
        //        {
        //            user = GetUserByID(memberID.Value);
        //        }
        //        if (user != null)
        //        {
        //            if (!user.CheckSessionGUID(cookie_TempMemberGUID))
        //                user = default(TUser);
        //        }
        //        if (user == null)
        //        {
        //            user = createNewTemporaryUser();
        //            user.GenerateSessionGUIDAndSave();
        //        }
        //        cookie_TempMemberID = user.ID;
        //        cookie_TempMemberGUID = user.SessionGUID;

        //    }
        //    else
        //    {
        //        if (_log.IsInfoEnabled)
        //        {
        //            if (false)
        //            {
        //                StringBuilder sb = new StringBuilder();
        //                sb.AppendLine("Tried to create temporary user, without COOKIES = DISABLED");
        //                var r = CS.General_v3.Util.PageUtil.GetCurrentRequest();
        //                if (r != null)
        //                {
        //                    sb.AppendLine("UserAgent: " + r.UserAgent);
        //                    sb.AppendLine("Url: " + r.Url);
        //                }
        //                sb.AppendLine("StackTrace: " + Environment.StackTrace);
        //                _log.Debug(sb.ToString());
        //            }
        //        }
        //    }
        //    return user;
        //}
        //public TUser GetTemporaryUser()
        //{
        //    return _getTemporaryUser();

        //}


        private TUser checkRememberedUser()
        {
            TUser user = default(TUser);
            var LoggedInUser = GetLoggedInUser();
            if (LoggedInUser == null)
            {
                string sGUID = this.cookie_RememberMeGUID;

                if (cookie_RememberMeID.GetValueOrDefault() > 0)
                {
                    user = this.GetUserByID(cookie_RememberMeID.Value);
                }
                if (user != null)
                {
                    if (string.Compare(user.SessionGUID, sGUID, true) == 0)
                    {

                        this.loginUser(user, false);
                        this.cookie_RememberMeID = user.ID;
                        this.cookie_RememberMeGUID = user.SessionGUID;
                        //this.Login(user.Username, user.Password, false, false, true);

                    }
                }
            }
            else
            {
                user = LoggedInUser;
            }
            return user;
        }
        public ILoginResult<TUser> Login(IUser user, bool fromAdmin, bool autoRedirectToLastURL = false, bool rememberMe = false)
        {
            var status = base.Login((TUser)user, fromAdmin);
            if (status.LoginStatus == Enums.LOGIN_STATUS.Ok)
            {
                afterLoginCheck(autoRedirect: autoRedirectToLastURL, rememberMe: rememberMe);
            }
            return status;
        }

        public ILoginResult<TUser> Login(string username, string password, bool fromAdmin, bool autoRedirectToLastURL = false, bool rememberMe = false)
        {
            var result = base.Login(username, password, fromAdmin);

            if (result.LoginStatus == Enums.LOGIN_STATUS.Ok)
            {
                afterLoginCheck(autoRedirect: autoRedirectToLastURL, rememberMe: rememberMe);
            }
            return result;
        }

        public ILoginResult<TUser> LoginAndReturnLoggedUser(string username, string password, bool fromAdmin, bool autoRedirectToLastURL = false, bool rememberMe = false)
        {
            var result = base.Login(username, password, fromAdmin);

            if (result.LoginStatus == Enums.LOGIN_STATUS.Ok)
            {
                afterLoginCheck(autoRedirect: autoRedirectToLastURL, rememberMe: rememberMe);
            }
            return result;
        }

        private void afterLoginCheck(bool autoRedirect = false, bool rememberMe = false)
        {

            if (rememberMe)
            {
                rememberCurrentUser();
            }
            else
            {
                cookie_RememberMeID = null;
                cookie_RememberMeGUID = null;
            }
            if (autoRedirect)
            {
                doAutoRedirection();
            }
        }


        public TUser GetCurrentLoggedUser()
        {
            TUser user = this.GetLoggedInUserAlsoFromRememberMe();
            //if (user == null)
            //    user = GetTemporaryUser();
            return user;

        }

        protected abstract TUser getUserFromSocialLogin(bool autoLogin);

    }
}
