﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Login
{
    public class LoginResult<TUser> : CS.General_v3.Classes.Login.ILoginResult<TUser> where TUser: IUser
    {
        public CS.General_v3.Enums.LOGIN_STATUS LoginStatus { get; private set; }
        public TUser LoggedInUser { get; private set; }
        public LoginResult()
        {
            this.LoginStatus = Enums.LOGIN_STATUS.InvalidUser;
        }
        public LoginResult(CS.General_v3.Enums.LOGIN_STATUS loginStatus, TUser user = default(TUser))
        {
            this.LoginStatus = loginStatus;
            this.LoggedInUser = user;
        }
        public void SetStatus(CS.General_v3.Enums.LOGIN_STATUS loginStatus)
        {
            this.LoginStatus = loginStatus;
        }
        public void SetUser(TUser user)
        {
            this.LoggedInUser = user;
        }

        public void FillFromMember(IUser user)
        {
            if (user.IsBlocked())
            {
                this.BlockedReason = user.BlockedReason;
                this.BlockedUntil = user.BlockedUntil;
            }
            else if (user.IsSelfExcluded())
            {
                this.BlockedUntil = user.SelfExcludedUntil;
            }
        }


        #region ILoginResult<TUser> Members


        public Enums.MEMBER_BLOCKED_REASON? BlockedReason { get; set; }

        public DateTime? BlockedUntil { get; set; }
        #endregion
    }
}
