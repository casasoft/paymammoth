﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Login
{
    public interface IWebUserSessionLoginManager<out TItem>  : IUserSessionLoginManager<TItem>
        where TItem : IUser
    {

        ///// <summary>
        ///// This returns NULL if the user has NO cookies
        ///// </summary>
        ///// <returns></returns>
        //TItem GetTemporaryUser();
        /// <summary>
        /// This returns NULL if the user has NO cookies, and is not logged in.
        /// </summary>
        /// <returns> This returns NULL if the user has NO cookies</returns>
        TItem GetCurrentLoggedUser();
        ILoginResult<TItem> Login(IUser user, bool fromAdmin, bool autoRedirectToLastURL = false, bool rememberMe = false);

    }
}
