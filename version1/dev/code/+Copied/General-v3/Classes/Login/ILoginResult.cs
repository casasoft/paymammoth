﻿using System;
namespace CS.General_v3.Classes.Login
{
    public interface ILoginResult<out TUser>
     where TUser : IUser
    {
        TUser LoggedInUser { get; }
        CS.General_v3.Enums.LOGIN_STATUS LoginStatus { get; }
        CS.General_v3.Enums.MEMBER_BLOCKED_REASON? BlockedReason { get; }
        DateTime? BlockedUntil { get; }
        void SetStatus(CS.General_v3.Enums.LOGIN_STATUS status);
    }
}
