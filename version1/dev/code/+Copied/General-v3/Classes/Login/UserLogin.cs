﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CS.General_v3.Util;
namespace CS.General_v3.Classes.Login
{
    /// <summary>
    /// Login user factory.
    /// 
    /// Please fill 'LoginURL' and 'WelcomeUrl';
    /// </summary>
    public abstract class UserSessionLogin : IUserSessionLogin
    {
        public UserSessionLogin()
        {
            
        }

        public bool IsLoggedIn
        {
            get { return LoggedInUser != null; }
        }
        /// <summary>
        /// This is called when a user did a successful login
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        protected virtual Enums.LOGIN_STATUS checkUserBeforeLogin(CS.General_v3.Classes.Login.IUser user)
        {
            return Enums.LOGIN_STATUS.Ok;
        }
        public virtual bool CheckAuthentication()
        {
            if (!IsLoggedIn)
            {
                return false;
            }
            return true;

        }

        #region ILoginUser Members

        public abstract IEnumerable<IUser> GetUsersList(string username);
        public abstract IUser GetUserByID(long id);
        protected virtual bool checkPassword(IUser user, string password)
        {
            return (user.Password == password);
        }
        public virtual Enums.LOGIN_STATUS Login(IUser user, bool fromAdmin)
        {
            Enums.LOGIN_STATUS status = Enums.LOGIN_STATUS.InvalidUser;
            if (user != null)
            {
                if (!user.Activated)
                {
                    status = Enums.LOGIN_STATUS.NotActivated;
                }
                else if (user.AccountTerminated)
                {
                    status = Enums.LOGIN_STATUS.TerminatedAccount;
                }
                else if (!user.Accepted)
                {
                    status = Enums.LOGIN_STATUS.NotAccepted;
                }
                else
                {
                    status = Enums.LOGIN_STATUS.Ok;
                }
                if (status == Enums.LOGIN_STATUS.Ok)
                {
                    status = checkUserBeforeLogin(user);
                    if (fromAdmin && (status == Enums.LOGIN_STATUS.NotActivated || status == Enums.LOGIN_STATUS.NotAccepted || status == Enums.LOGIN_STATUS.TerminatedAccount))
                        status = Enums.LOGIN_STATUS.Ok;
                    if (status == Enums.LOGIN_STATUS.Ok)
                    {
                        loginUser(user, fromAdmin);
                    }
                }

            }
            return status;
        }
        public virtual Enums.LOGIN_STATUS Login(string username, string password, bool fromAdmin)
        {
            List<IUser> list = CS.General_v3.Util.ListUtil.GetListFromEnumerator(GetUsersList(username));
            IUser user = list.FindElem<IUser>(item => string.Compare(item.Username, username, true) == 0);
            Enums.LOGIN_STATUS status = Enums.LOGIN_STATUS.InvalidUser;
            if (user != null)
            {
                if (checkPassword(user,password))
                {
                    status = this.Login(user, fromAdmin);
                }
                else
                {
                    status = Enums.LOGIN_STATUS.InvalidPass;
                }

            }
            return status;
        }
        public event EventHandler OnLoginSuccessful;
        protected virtual void loginUser(IUser user, bool fromAdmin)
        {

            this.LoggedInUser = user;
            if (!fromAdmin)
            {
                user.LoggedIn();
                user.LastLoggedIn = user._LastLoggedIn_New;
                user._LastLoggedIn_New = CS.General_v3.Util.Date.Now;
            }
            else
            {
                user.LoggedInFromAdmin();
            }
            if (OnLoginSuccessful != null)
                OnLoginSuccessful(this, null);
            
            
        }

        public virtual void Logout()
        {
            IUser user = this.LoggedInUser;
            if (user != null)
            {
                user.LastLoggedIn = user._LastLoggedIn_New = CS.General_v3.Util.Date.Now;
                this.LoggedInUser = null;
                user.LoggedOut();
            }
        }
        public virtual IUser LoggedInUser {get;set;}

        #endregion
    }
}
