﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CS.General_v3.Util;
using Brickred.SocialAuth.NET.Core.BusinessObjects;
namespace CS.General_v3.Classes.Login
{
    /// <summary>
    /// Login user factory.
    /// 
    /// Please fill 'LoginURL' and 'WelcomeUrl';
    /// </summary>
    public abstract class UserSessionLoginManager<TUser> : IUserSessionLoginManager<TUser>
        where TUser : class, IUser
    {
        protected string sessionIdentifier = null;
        public UserSessionLoginManager(string sessionIdentifier = "userSessionLogin")
        {
            this.sessionIdentifier = sessionIdentifier;
        }
        
        public virtual bool IsLoggedIn
        {
            get { return CurrentSessionLoggedInUserID > 0; }
        }
        

        public virtual bool DoNotAllowConcurrentLogins { get; set; }

        /// <summary>
        /// This is called when a user did a successful login
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        protected virtual Enums.LOGIN_STATUS checkUserBeforeLogin(TUser user)
        {
            return user.OnPreLogin();

        }
        public virtual bool CheckAuthentication()
        {
            if (!IsLoggedIn)
            {
                return false;
            }
            return true;

        }

        #region ILoginUser Members

        public abstract IEnumerable<TUser> GetUsersList(string username);
        public abstract TUser GetUserByID(long id);
        protected virtual bool checkPassword(TUser user, string password)
        {
            return user.CheckPassword(password);

        }

        protected Enums.LOGIN_STATUS verifyUserStatus(TUser user)
        {
            Enums.LOGIN_STATUS status = Enums.LOGIN_STATUS.InvalidUser;
            if (user != null)
            {
                if (!user.Activated)
                {
                    status = Enums.LOGIN_STATUS.NotActivated;
                }
                else if (user.AccountTerminated)
                {
                    status = Enums.LOGIN_STATUS.TerminatedAccount;
                }
                else if (!user.Accepted)
                {
                    status = Enums.LOGIN_STATUS.NotAccepted;
                }
                
                else if (user.IsSelfExcluded())
                {
                    status = Enums.LOGIN_STATUS.SelfExcluded;
                }
                else if (user.IsBlocked())
                {
                    status = Enums.LOGIN_STATUS.Blocked;
                }
                else
                {
                    status = Enums.LOGIN_STATUS.Ok;
                }
            }
            return status;
        }

        public LoginResult<TUser> Login(TUser user, bool fromAdmin)
        {
            LoginResult<TUser> result = new LoginResult<TUser>();

            Enums.LOGIN_STATUS status = verifyUserStatus(user);
            if (user != null)
            {
                
                if (status == Enums.LOGIN_STATUS.Ok)
                {
                    status = checkUserBeforeLogin(user);
                    if (fromAdmin && (status == Enums.LOGIN_STATUS.NotActivated || status == Enums.LOGIN_STATUS.NotAccepted || status == Enums.LOGIN_STATUS.TerminatedAccount))
                        status = Enums.LOGIN_STATUS.Ok;
                    if (status == Enums.LOGIN_STATUS.Ok)
                    {
                        loginUser(user, fromAdmin);
                        result.SetUser(user);
                    }
                }
            }
            result.SetStatus(status);
            return result;

        }
        public ILoginResult<TUser> Login(string username, string password, bool fromAdmin)
        {
            LoginResult<TUser> result = new LoginResult<TUser>();
            List<TUser> list = CS.General_v3.Util.ListUtil.GetListFromEnumerator(GetUsersList(username));
            //IUser user = list.FindElem<IUser>(item => string.Compare(item.Username, username, true) == 0);
            //   Enums.LOGIN_STATUS status = Enums.LOGIN_STATUS.InvalidUser;
            //loggedUser = default(TUser);
            foreach (var user in list)
            {
                Enums.LOGIN_STATUS status = Enums.LOGIN_STATUS.Ok;
                bool userFound = false;
                if (checkPassword(user, password))
                {
                    //pass is OK
                    status = verifyUserStatus(user); //it can be blocked
                    
                    if (status == Enums.LOGIN_STATUS.Ok)
                    {
                        result = this.Login(user, fromAdmin);
                    }

                    userFound = true;
                }
                else
                {
                    status = Enums.LOGIN_STATUS.InvalidPass;
                }

                if (status != Enums.LOGIN_STATUS.Ok)
                {
                    if (status == Enums.LOGIN_STATUS.InvalidPass)
                    {
                        //2012-04-10 This has been updated by Mark as an invalid login is only counted when password is invalid.
                        user.OnInvalidLoginAttempt(status, username, password);
                    }
                    result.SetStatus(status);
                    result.FillFromMember(user);
                }

                if (userFound)
                {
                    break;
                }
            }
            // result.SetStatus(status);
            return result;

        }


        public event EventHandler OnLoginSuccessful;
        protected virtual void loginUser(TUser user, bool fromAdmin)
        {
            if (user != null)
            {

                bool triggerUserLoggedIn = false;
                if (!fromAdmin)
                {
                    user.OnLoggedIn();
                    user.LastLoggedIn = user._LastLoggedIn_New;
                    user._LastLoggedIn_New = CS.General_v3.Util.Date.Now;
                }
                else
                {
                    user.OnLoggedInFromAdmin();
                }
                this.CurrentSessionLoggedInUserID = user.ID;
                this.CurrentLoggedInUserSessionId = user.SessionGUID;
                if (OnLoginSuccessful != null)
                    OnLoginSuccessful(this, null);

            }
        }

        public virtual void Logout()
        {
            TUser user = this._getLoggedInUser();
            if (user != null)
            {
                user.LastLoggedIn = user._LastLoggedIn_New = CS.General_v3.Util.Date.Now;
                this.CurrentSessionLoggedInUserID = 0;
                this.CurrentLoggedInUserSessionId = null;
                logoutFromSocial();
                user.OnLoggedOut();
            }
        }

        private void logoutFromSocial()
        {
            if(SocialAuthUser.IsLoggedIn())
            {
                SocialAuthUser loggedUser = SocialAuthUser.GetCurrentUser();
                loggedUser.Logout("/");
            }
        }
        /*
        protected TItem getFromSession<TItem>(string key)
        {
            return Util.PageUtil.GetSessionObject<TItem>(getSessionKey(key));
        }*/
        protected string getSessionKey(string key)
        {
            return sessionIdentifier + key;
        }

        //protected void setInSession(string key, object value)
        //{
        //    Util.PageUtil.SetSessionObject(sessionIdentifier + key, value);

        //}

        [ThreadStatic]
        private static long _unitTestingCurrentSessionLoggedInUserID = 0;

        public long CurrentSessionLoggedInUserID
        {
            get
            {
                long id = 0;
                if (Util.PageUtil.CheckSessionExistsAtContext())
                {

                    id = (CS.General_v3.Util.PageUtil.GetSessionObject<long?>(getSessionKey("UserID"))).GetValueOrDefault();
                }
                else if (CS.General_v3.Util.Other.IsInUnitTestingEnvironment)
                {
                    //this is only to be able to login etc in unit-testing environment
                    id = _unitTestingCurrentSessionLoggedInUserID;
                }
                return id;
            }
            set
            {
                if (Util.PageUtil.CheckSessionExistsAtContext())
                {
                    CS.General_v3.Util.PageUtil.SetSessionObject(getSessionKey("UserID"), value);
                }
                else if (CS.General_v3.Util.Other.IsInUnitTestingEnvironment)
                { //this is only to be able to login etc in unit-testing environment
                    _unitTestingCurrentSessionLoggedInUserID = value;
                }

            }
        }

        [ThreadStatic]
        private static string _unitTestingCurrentLoggedInUserSessionId = null;

        public string CurrentLoggedInUserSessionId
        {
            get
            {
                string s = null;
                if (Util.PageUtil.CheckSessionExistsAtContext())
                {

                    s = (CS.General_v3.Util.PageUtil.GetSessionObject<string>(getSessionKey("UserSessionGuid")));
                }
                else if (CS.General_v3.Util.Other.IsInUnitTestingEnvironment)
                {
                    //this is only to be able to login etc in unit-testing environment
                    s = _unitTestingCurrentLoggedInUserSessionId;
                }
                return s;
            }
            set
            {
                if (Util.PageUtil.CheckSessionExistsAtContext())
                {
                    CS.General_v3.Util.PageUtil.SetSessionObject(getSessionKey("UserSessionGuid"), value);
                }
                else if (CS.General_v3.Util.Other.IsInUnitTestingEnvironment)
                { //this is only to be able to login etc in unit-testing environment
                    _unitTestingCurrentLoggedInUserSessionId = value;
                }
            }
        }

        private TUser _getLoggedInUser()
        {
            long userID = CurrentSessionLoggedInUserID;
            TUser user = default(TUser);
            if (userID > 0)
                user = GetUserByID(userID);

            return user;
        }

        public virtual TUser GetLoggedInUser()
        {


            TUser user = _getLoggedInUser();
            if (user != null && this.DoNotAllowConcurrentLogins)
            {
                if (!checkForConcurrentLogin(user))
                    user = default(TUser); ;
            }
            return user;
            
        }

        private bool checkForConcurrentLogin(TUser user)
        {
            bool allow = true;
            if (user != null && user.SessionGUID != this.CurrentLoggedInUserSessionId)
            {
                allow = false;
                this.Logout();
            }
            return allow;
        }



        #endregion

        #region IUserSessionLoginManager<TUser> Members

        ILoginResult<IUser> IUserSessionLoginManager<TUser>.Login(string username, string password, bool fromAdmin)
        {
            return (ILoginResult<IUser>)this.Login(username, password, fromAdmin);

        }

        #endregion
    }
}
