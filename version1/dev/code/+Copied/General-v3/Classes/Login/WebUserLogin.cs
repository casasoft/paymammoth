﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CS.General_v3.Util;
namespace CS.General_v3.Classes.Login
{
    /// <summary>
    /// Login user factory.
    /// 
    /// Please fill 'LoginURL' and 'WelcomeUrl';
    /// </summary>
    public abstract class WebUserSessionLogin : UserSessionLogin
    {
        public WebUserSessionLogin(string welcomeURL, string loginID = "WebUserLogin")
        {
            this.WelcomeUrl = welcomeURL;
            this.ID = loginID;
            
            checkRememberedUser();
        }

        
            
        
        private System.Web.UI.Page pg = null;

        public override bool CheckAuthentication()
        {
            lastUrl = CS.General_v3.Util.PageUtil.GetURLLocationAndQuerystring();
            return base.CheckAuthentication();
        }
        public string ID { get; private set; }
        public string WelcomeUrl { get; set; }
        public string lastUrl { get; set; }
        public string LoginUrl { get; set; }
        protected override void loginUser(IUser user, bool fromAdmin)
        {
            base.loginUser(user, fromAdmin);
            loginUser(user, fromAdmin, false);
        }
        private void doAutoRedirection()
        {
            if (string.IsNullOrEmpty(WelcomeUrl))
                throw new InvalidOperationException("Please fill in 'WelcomeUrl'");
            if (!string.IsNullOrEmpty(lastUrl))
            {
                CS.General_v3.Util.PageUtil.RedirectPage(lastUrl);
            }
            else
            {
                CS.General_v3.Util.PageUtil.RedirectPage(WelcomeUrl);
            }
        }
        protected virtual void loginUser(IUser user, bool fromAdmin, bool autoRedirect)
        {
            if (autoRedirect)
                doAutoRedirection();

        }
        public override Enums.LOGIN_STATUS Login(string username, string password, bool fromAdmin)
        {
            return Login(username, password, fromAdmin, autoRedirect: false, rememberMe: false);
        }
        private string getRememberMeCookieName()
        {
            return this.ID + "_user_rm";
        }
        private void rememberCurrentUser()
        {

            cookie_RememberMeID = this.LoggedInUser.ID.ToString();
            cookie_RememberMeGUID = this.LoggedInUser.SessionGUID;

        }

        private string cookie_RememberMeID
        {
            get
            {
                return CS.General_v3.Util.PageUtil.GetCookie(getRememberMeCookieName(), "id");
            }
            set
            {
                CS.General_v3.Util.PageUtil.SetCookie(getRememberMeCookieName(), "id", value);
                
            }

        }
        private string cookie_RememberMeGUID
        {
            get
            {
                return CS.General_v3.Util.PageUtil.GetCookie(getRememberMeCookieName(), "guid");
            }
            set
            {
                CS.General_v3.Util.PageUtil.SetCookie(getRememberMeCookieName(), "guid", value);

            }

        }

        public override void Logout()
        {

            base.Logout();
            this.cookie_RememberMeGUID = null;
            this.cookie_RememberMeID = null;
        }
        private void checkRememberedUser()
        {
            if (LoggedInUser == null)
            {
                string sID = this.cookie_RememberMeID;
                string sGUID = this.cookie_RememberMeGUID;
                int id = 0;
                if (!Int32.TryParse(sID, out id))
                    id = 0;
                IUser user = null;
                if (id > 0)
                {
                    user = this.GetUserByID(id);
                }
                if (user != null)
                {
                    if (string.Compare(user.SessionGUID, sGUID, true) == 0)
                    {
                        
                        this.loginUser(user, false);
                        this.cookie_RememberMeID = user.ID.ToString();
                        this.cookie_RememberMeGUID = user.SessionGUID;
                        //this.Login(user.Username, user.Password, false, false, true);
                        
                    }
                }
            }
        }
        public virtual Enums.LOGIN_STATUS Login(IUser user, bool fromAdmin, bool autoRedirect = false, bool rememberMe = false)
        
        {
            var status = base.Login(user, fromAdmin);
            if (status == Enums.LOGIN_STATUS.Ok)
            {
                afterLoginCheck(autoRedirect: autoRedirect, rememberMe: rememberMe);
            }
            return status;

        }
        
        public virtual Enums.LOGIN_STATUS Login(string username, string password, bool fromAdmin, bool autoRedirect = false, bool rememberMe = false)
        {
            var result = base.Login(username, password, fromAdmin);

            if (result == Enums.LOGIN_STATUS.Ok)
            {
                afterLoginCheck(autoRedirect: autoRedirect, rememberMe: rememberMe);
            }
            return result;
        }

        private void afterLoginCheck(bool autoRedirect = false, bool rememberMe = false)
        {

            if (rememberMe)
            {
                rememberCurrentUser();
            }
            else
            {
                cookie_RememberMeID = null;
                cookie_RememberMeGUID = null;
            }
            if (autoRedirect)
            {
                doAutoRedirection();
            }
        }
    }
}
