﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Login
{
    public interface IUserSessionLoginManager<out TItem> 
        where TItem : IUser
    {
        ILoginResult<IUser> Login(string username, string password,bool fromAdmin);
        void Logout();
        long  CurrentSessionLoggedInUserID { get; }
        string CurrentLoggedInUserSessionId { get; }
        TItem GetLoggedInUser();
        bool IsLoggedIn { get; }
        


    }
}
