﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_20090518.Classes.Login
{
    public abstract class AdminUserLogin : CS.General_20090518.Classes.Login.WebUserLogin
    {
        public AdminUserLogin(string welcomeURL)
            : base(welcomeURL)
        {

        }
        public new IAdminUser LoggedInUser
        {
            get
            {
                return (IAdminUser)base.LoggedInUser;
            }
            set
            {
                base.LoggedInUser = value;
            }
        }
        public  bool CheckAuthentication(Enums.ADMINUSER_ACCESSTYPE AccessTypeRequired)
        {
            bool b = base.CheckAuthentication();
            if (b)
            {
                b = Enums.CheckAdminUserAccess(this.LoggedInUser.AccessType,AccessTypeRequired);
            }
            return b;
        }


        public override bool CheckAuthentication()
        {
            return CheckAuthentication( Enums.ADMINUSER_ACCESSTYPE.Normal);
            
        }
        
    }
}
