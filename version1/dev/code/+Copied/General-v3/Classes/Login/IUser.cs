﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.Factories;

namespace CS.General_v3.Classes.Login
{
    public interface IUser : IBaseObject
    {
        //long ID { get; }

        string UsernameHashed { get; }


        string PasswordHashed { get;  }
        
        void SetUsername(string usernameUnhashed);
        /// <summary>
        /// This must update the password, with the HASHED PASSWORD. Importantly, this MUST NOT RE-HASH
        /// </summary>
        /// <param name="hashedPassword"></param>
        void UpdatePasswordWithHashedValue(string hashedPassword, string salt, int totalIterations, Enums.PASSWORD_ENCRYPTION_TYPE encryptionType);
        /// <summary>
        /// This must update the password, with the HASHED PASSWORD. Importantly, this MUST NOT RE-HASH
        /// </summary>
        /// <param name="hashedUsername"></param>
        void UpdateUsernameWithHashedValue(string hashedUsername, string salt, int totalIterations, Enums.PASSWORD_ENCRYPTION_TYPE encryptionType);
        void OnInvalidLoginAttempt(Enums.LOGIN_STATUS status, string username, string password);
        /// <summary>
        /// This is used for remember me
        /// </summary>
        string SessionGUID { get; set; }

        bool Activated { get; set; }
        
        bool Accepted { get; }
        
        bool AccountTerminated { get; }
        Enums.MEMBER_BLOCKED_REASON BlockedReason { get; }
        DateTime? BlockedUntil { get; }
        DateTime? SelfExcludedUntil { get; }
        DateTime? LastLoggedIn { get; set; }
        
        DateTime? _LastLoggedIn_New { get; set; }
        
        string Name { get; set; }
        
        string Surname { get; set; }
        
        string FullName { get; }

        Enums.LOGIN_STATUS OnPreLogin();
        /// <summary>
        /// Gets called when user logs in
        /// </summary>
        void OnLoggedIn();
        
        /// <summary>
        /// Gets called when user logs in, but by an 'Administrator'
        /// </summary>
        void OnLoggedInFromAdmin();

        /// <summary>
        /// Called when the user loggs out
        /// </summary>
        void OnLoggedOut();
        

        bool CheckSessionGUID(string guid);
        
        void GenerateSessionGUIDAndSave();
        
        bool CheckPassword(string password);


        void SetPassword(string passwordUnhashed);
        string PasswordSalt { get;  }
        int PasswordIterations { get;  }
        Enums.PASSWORD_ENCRYPTION_TYPE PasswordEncryptionType { get;  }
        void Save();
        
        bool IsSelfExcluded();
        bool IsBlocked();
        
        
    }
}
