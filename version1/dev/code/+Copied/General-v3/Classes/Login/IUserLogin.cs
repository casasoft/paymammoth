﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Login
{
    public interface IUserSessionLogin
    {
        Enums.LOGIN_STATUS Login(string username, string password,bool fromAdmin);
        void Logout();
        IUser LoggedInUser { get; set; }
        bool IsLoggedIn { get; }
        


    }
}
