﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace CS.General_v3.Classes.MediaFiles
{
    public interface IMediaFileInfo
    {
        string Filename { get; set; }
       // void SetFile(Stream s, string filename);
       ////// string GetLocalPathForOriginal();
       // string GetWebPathForOriginal();
        string Folder { get; set; }
         CS.General_v3.Classes.MediaFiles.MediaFileInfo.FilenameUpdateHandler FilenameUpdater {get;}

    }
}
