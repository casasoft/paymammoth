﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.MediaFiles
{
    public class MediaFileInfo : IMediaFileInfo
    {
        public delegate string FilenameUpdateHandler(string uploadedFilename);

        public FilenameUpdateHandler FilenameUpdater { get; set; }
        public string Filename {get;set;}
        public string Folder { get; set; }
        
    }
}
