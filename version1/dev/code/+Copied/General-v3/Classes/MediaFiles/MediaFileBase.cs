﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace CS.General_v3.Classes.MediaFiles
{
    public abstract class MediaFileBase
    {
        public MediaFileInfo FileInfo { get; private set; }

        public MediaFileBase(MediaFileInfo fileInfo)
        {
            System.Diagnostics.Contracts.Contract.Requires(fileInfo != null);
            this.FileInfo = fileInfo;
        }


        public CS.General_v3.Enums.ITEM_TYPE GetItemType()
        {
            string ext = CS.General_v3.Util.IO.GetExtension(this.FileInfo.Filename).ToLower();
            if (CS.General_v3.Util.Image.CheckImageFileExtension(ext))
                return Enums.ITEM_TYPE.Image;
            else if (CS.General_v3.Util.VideoUtil.CheckVideoExtension(ext))
                return Enums.ITEM_TYPE.Video;
            else
            {
                switch (ext)
                {
                    case "pdf": return Enums.ITEM_TYPE.PDF;
                    case "swf": return Enums.ITEM_TYPE.SWF;
                    case "mp3": return Enums.ITEM_TYPE.MP3;
                }
            }
            return Enums.ITEM_TYPE.Other;

        }

        private void updateFilename(string uploadedFilename)
        {
            if (FileInfo.FilenameUpdater != null)
                FileInfo.FilenameUpdater(uploadedFilename);
        }

        
        public virtual void SetFile(System.IO.Stream s, string filename)
        {
            System.Diagnostics.Contracts.Contract.Requires(s != null);
            System.Diagnostics.Contracts.Contract.Requires(!string.IsNullOrWhiteSpace(filename));

            string oldPath = GetLocalPathForOriginal();

            CS.General_v3.Util.IO.DeleteFile(oldPath);

            updateFilename(filename);
            string newPath = GetLocalPathForOriginal();
            CS.General_v3.Util.IO.SaveStreamToFile(s, newPath);

        }

        protected string getFilename(string prepend = null, string append = null, string extension = null)
        {
            string filenameOnly = CS.General_v3.Util.IO.GetFilenameOnly(this.FileInfo.Filename);
            string extOnly = CS.General_v3.Util.IO.GetExtension(this.FileInfo.Filename);
            if (extension == null)
                extension = extOnly;
            if (extension.StartsWith(".")) extension = extension.Substring(1);

            string result = prepend + filenameOnly + append + "." + extension;
            return result;


        }



        public string GetLocalPathForOriginal()
        {
            string path = CS.General_v3.Util.PageUtil.MapPath(this.FileInfo.Folder) + getFilename();
            return path;
        }

        public string GetWebPathForOriginal()
        {
            string path = this.FileInfo.Folder + "/" + getFilename();
            path = CS.General_v3.Util.IO.RemoveDuplicateSlashesFromPath(path);
            return path;
            
        }
    }
}
