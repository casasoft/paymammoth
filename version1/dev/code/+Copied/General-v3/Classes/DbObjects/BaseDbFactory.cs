﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.NHManager;
using CS.General_v3.Classes.NHibernateClasses.Session;
using Iesi.Collections;
using Iesi.Collections.Generic;
using CS.General_v3.Classes.Factories;
using log4net;
using NHibernate;
using NHibernate.Criterion;
using CS.General_v3.Extensions;
using NHibernate.Exceptions;
using General = CS.General_v3.Util.General;
using CS.General_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Classes.NHibernateClasses.Transactions;

namespace CS.General_v3.Classes.DbObjects
{
 

    

    public abstract class BaseDbFactory<TItem> : BaseFactory, IBaseDbFactory<TItem>, IBaseDbFactory
        where TItem : class,IBaseDbObject
       
        
       
    {

        public static bool UsedInProject { get; set; }

        protected MyTransaction beginTransaction()
        {
            return getCurrentSessionInContext().BeginTransaction();
        }

        protected override IEnumerable<object> __getAllItemsInRepository()
        {
            
            return this.FindAll();
            
        }

        public  string GetOutputCacheDependencyKey()
        {
            string key ="Factory-" + this.GetType().Name;
            return key;
        }
        
        public void InvalidateOutputCacheDependency()
        {
            string key = GetOutputCacheDependencyKey();
            CS.General_v3.Util.CachingUtil.InvalidateCacheDependency(key);
            

        }



        protected void flushCurrentSession()
        {
            NHClasses.NhManager.FlushCurrentSessionInContext();
        }
        
        public event General.OnItemUpdateHandler OnItemUpdate;

       

        //public readonly object factoryLock = new object();


        public BaseDbFactory() : base()
        {
            _log = LogManager.GetLogger(this.GetType());
            this.FactoryPriority = Int32.MaxValue;
            addTypeCreatedByFactory(typeof(TItem));
        }

        //public static int callCount = 0;


       






        
        #region IBaseDbFactory Members


        void IBaseDbFactory.OnItemUpdated(IBaseDbObject obj, Enums.UPDATE_TYPE updateType)
        {
            if (OnItemUpdate != null)
                OnItemUpdate(obj, updateType);


            
        }



        /// <summary>
        /// Should return the total number of hours, before removing a temporary item from the database
        /// </summary>
        /// <returns></returns>
        protected virtual int getTotalHoursToRemoveTemporary()
        {
            return 7 * 24; // 7 days;
        }

        int IBaseDbFactory.GetTotalHoursToRemoveTemporary()
        {
            return getTotalHoursToRemoveTemporary();
        }







        #endregion

         private ILog _log = null;
        private string getContextKey(string key)
        {
            return this.GetType().FullName + key;
        }

        protected T getFromContext<T>(string key)
        {
            return CS.General_v3.Util.PageUtil.GetContextObject<T>(getContextKey(key), throwErrorIfContextDoesNotExist: false);
        }
        protected void setInContext(string key, object value)
        {
            CS.General_v3.Util.PageUtil.SetContextObject(getContextKey(key), value, throwErrorIfContextDoesNotExist: false);
        }





            #region IBaseFactory<TItem,TCriteria,TSet,TDBInfo,TFactory> Members

        protected override object _createNewItem()
        {
            var item = (IBaseDbObject)base._createNewItem();
            item.InitPropertiesForNewItem();
            item.resetDirtyFlag();
            return item;
        }

        public new TItem CreateNewItem()
        {
            return (TItem)base.CreateNewItem();
            
        }






        #endregion


        private Type _lastType = null;

        ///// <summary>
        ///// Returns the 'final' type of TItem
        ///// </summary>
        ///// <returns></returns>
        //protected Type getLastType()
        //{
        //    if (_lastType == null)
        //    {
        //        _lastType = CS.General_v3.Util.ReflectionUtil.GetLastTypeInInheritanceChain(typeof(TItem),
        //                                                                                   CS.General_v3.Classes.Application.AppInstance.Instance.GetProjectAssemblies());

        //    }
        //    return _lastType;
        //}


        //private void addType()
        //{
        //    //Type t = CS.General_v3.Util.ReflectionUtil.GetTypeWhichExtendsFromAndIsNotAbstract(DatabaseConstants.ProjectSpecificAssembly, typeof(TItem));
        //    Type t = getLastType();

        //    //var item = CreateInstance();
        //    //Type t = item.GetType();
        //    addTypeCreatedByFactory(t);
        //    // item.Delete();

        //}

        public override void OnPostInitialisation()
        {
            
            base.OnPostInitialisation();
            //   _checkForTemporaryItems();

        }

        public abstract void CheckCultureInfos();







        protected void postGetQueryOver(IQueryOver q, GetQueryParams qParams)
        {
            if (qParams.LoadOnlyItemsForFrontend)
                this.FillCriteriaWithConditionsToShowOnlyFrontendItems(q.RootCriteria);
        }

        public IEnumerable<TItem> FindAll(MyNHSessionBase session = null)
        {
            GetQueryParams qParams = new GetQueryParams(session: session);
            var c = GetCriteria(qParams);
            
            return FindAll(c);
        }


        public IEnumerable<TItem> FindAll(IQuery query)
        {
            
            IEnumerable<TItem> dbList = null;
            try
            {
                dbList = query.List<TItem>();
            }
            catch (GenericADOException ex)
            {
                throw;
            }
            return dbList;

        }

        public IEnumerable<TItem> FindAll(IQueryOver query, MyNHSessionBase session = null)
        {
            return FindAll(query.RootCriteria);

        }

        protected MyNHSessionBase getCurrentSessionInContext()
        {
            var session = NHClasses.NhManager.GetCurrentSessionFromContext();
            return session;
        }

        public IEnumerable<TItem> FindAll(IDetachedQuery query)
        {
            var session = NHClasses.NhManager.GetCurrentSessionFromContext();
            var q = query.GetExecutableQuery(session.GetAsISession());
            return FindAll(q);
        }



       
        public IEnumerable<TItem> FindAll(ICriteria crit, MyNHSessionBase session = null)
        {

            Iesi.Collections.ISet dbList = null;
            dbList = (ISet)CS.General_v3.Util.ReflectionUtil.CreateGenericType(typeof(OrderedSet<>), GetMainTypeCreatedByFactory());
        
            try
            {
                
                var results = crit.List<TItem>();
            
                if (_log.IsDebugEnabled)
                {
                    if (results.Count >= 500)
                    {
                        StringBuilder sb = new StringBuilder();
                        string msg = "FindAll<" + typeof(TItem).FullName +
                                        ">() returned a very large list, which might slow down greatly performance - Total count: " +
                                        results.Count;
                        sb.AppendLine(msg);
                        StringBuilder errMsg = new StringBuilder();
                        errMsg.AppendLine(msg);
                        errMsg.AppendLine("Criteria: " + crit.ToString());
                        errMsg.AppendLine("Stack Trace: " + Environment.StackTrace);

                        Exception ex = new Exception(errMsg.ToString());


                        _log.Debug(sb.ToString(), ex);
                    }
                }

                dbList.AddAll(results.ToList());
            
            }
            catch (GenericADOException ex)
            {
                int k = 5;
                throw;
            }
            catch (HibernateException ex)
            {
                int k = 5;
                throw;
            }
            return (IEnumerable<TItem>) dbList;
        


        }
        public IEnumerable<TItem> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return FindAll(query.RootCriteria);

        }

        public IEnumerable<TItem> FindAll(DetachedCriteria crit)
        {
            return FindAll(crit.GetExecutableCriteria(getCurrentSessionInContext().GetAsISession()));

        }

        private TItem getItemFromList(IEnumerable<TItem> list)
        {
            return list.FirstOrDefault();

        }

        public TItem FindItem(QueryOver query)
        {
            var list = FindAll(query);
            return getItemFromList(list);
        }

        public TItem FindItem(IQueryOver query)
        {
            var list = FindAll(query);
            return getItemFromList(list);
        }



        private void checkSession(ref MyNHSessionBase session)
        {
            if (session == null)
                session = getCurrentSessionInContext();

        }






        #region IBaseDbFactory<TItem> Members

        public TItem ReloadItemFromDatabase(TItem item)
        {
            TItem newItem = null;
            if (item != null)
            {
                long id = item.ID;
                var session = CS.General_v3.Classes.NHibernateClasses.NHClasses.NhManager.CreateNewSession();
                session.FlushMode = FlushMode.Never;
                
                newItem = this.GetByPrimaryKey(id, session);
                session.Dispose();
            }
            return newItem;
        }

        public TItem GetByPrimaryKey(long id, MyNHSessionBase session = null)
        {
            checkSession(ref session);

            TItem item = default(TItem);
            if (id > 0)
            {
                try
                {
                    Type lastType = GetMainTypeCreatedByFactory();
                    string name = lastType.FullName;
                    object itemBase = session.Get(name, id);
                    item = (TItem) itemBase;
                    if (item != null)
                    {
                        item.resetDirtyFlag();
                    }
                }
                catch (GenericADOException ex)
                {
                    int k = 5;
                    throw;
                }
            }
            return item;
        }

        #endregion


        #region IBaseDbFactory Members


        IBaseDbObject IBaseDbFactory.GetByPrimaryKey(long id, MyNHSessionBase session = null)
        {
            return GetByPrimaryKey(id, session);
            
        }

        IEnumerable<IBaseDbObject> IBaseDbFactory.FindAll(IQueryOver query, MyNHSessionBase session = null)
        {
            return FindAll(query).Cast<IBaseDbObject>();
            
        }

        IEnumerable<IBaseDbObject> IBaseDbFactory.FindAll(ICriteria query, MyNHSessionBase session = null)
        {
            return FindAll(query).Cast<IBaseDbObject>();
        }

        IEnumerable<IBaseDbObject> IBaseDbFactory.FindAll(MyNHSessionBase session = null)
        {

            return FindAll().Cast<IBaseDbObject>();
        }

        public DetachedCriteria GetDetachedCriteria(GetQueryParams qParams = null)
        {
            if (qParams == null) qParams = new GetQueryParams();
            
            var crit = DetachedCriteria.For( GetMainTypeCreatedByFactory());
            qParams.FillDetachedCriteria(crit);
            
            
            return crit;
            
            
        }

        public virtual void FillCriteriaWithConditionsToShowOnlyFrontendItems(ICriteria crit)
        {

        }

        public virtual ICriteria GetQueryToRemoveTemporaryItems(int pageSize, int pageNum, GetQueryParams qParams = null)
        {
            DateTime date = DateTime.Now.AddHours(-getTotalHoursToRemoveTemporary());
            var crit = GetCriteria(qParams);
            crit.Add(Restrictions.Eq(CS.General_v3.Util.ReflectionUtil<IBaseDbObject>.GetPropertyName(item=>item._Temporary_Flag), true));
            crit.Add(Restrictions.Lt(CS.General_v3.Util.ReflectionUtil<IBaseDbObject>.GetPropertyName(item=>item._Temporary_LastUpdOn), date));
            
            CS.General_v3.Util.nHibernateUtil.LimitCriteriaByPrimaryKeys(crit,

                pageNum,pageSize);
            return crit;
            
        }

        #endregion

        #region IBaseDbFactory Members


        protected ICriteria getRawCriteria(MyNHSessionBase session)
        {
            if (session == null)
                session = CS.General_v3.Classes.NHibernateClasses.NHClasses.NhManager.GetCurrentSessionFromContext();
            var crit = session.CreateCriteria(GetMainTypeCreatedByFactory());
            return crit;
        }


        public ICriteria GetCriteria(GetQueryParams qParams = null )
        {
            if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();

            var crit = getRawCriteria(session);
            qParams.FillCriteria(crit);
            if (qParams.LoadOnlyItemsForFrontend)
                FillCriteriaWithConditionsToShowOnlyFrontendItems(crit);
            return crit;
            
        }

        #endregion

        #region IBaseDbFactory Members


        IBaseDbObject IBaseDbFactory.CreateNewItem()
        {
            return this.CreateNewItem();
            
        }

        #endregion

        #region IBaseDbFactory<TItem> Members


        public IEnumerable<TItem> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null)
        {
            List<TItem> list = new List<TItem>();
            if (loadAsSeperateQueries)
            {
                foreach (var id in ids)
                {
                    var item = GetByPrimaryKey(id, session);
                    if (item != null)
                        list.Add(item);
                }
                return list;
            }
            else
            {
                
                GetQueryParams qParams = new GetQueryParams(session: session);
                var crit = GetCriteria(qParams);
                crit.Add(Restrictions.InG<long>("ID", ids));
                return FindAll(crit);
                    
                
            }
            
        }

        #endregion


        #region IBaseDbFactory<TItem> Members


        public TItem FindItem(IQueryOver query, MyNHSessionBase session = null)
        {
            return this.FindAll(query, session).FirstOrDefault();
            
        }

        public TItem FindItem(ICriteria query, MyNHSessionBase session = null)
        {
            return this.FindAll(query, session).FirstOrDefault();
        }

        public TItem FindItem(MyNHSessionBase session = null)
        {
            return this.FindAll(session).FirstOrDefault();
        }

        #endregion

        #region IBaseDbFactory Members


        IBaseDbObject IBaseDbFactory.FindItem(IQueryOver query, MyNHSessionBase session = null)
        {
            return this.FindAll(query, session).FirstOrDefault();
            
        }

        IBaseDbObject IBaseDbFactory.FindItem(ICriteria query, MyNHSessionBase session = null)
        {
            return this.FindAll(query, session).FirstOrDefault();
            
        }

        IBaseDbObject IBaseDbFactory.FindItem(MyNHSessionBase session = null)
        {
            return this.FindAll(session).FirstOrDefault();
        }

        #endregion



        #region IBaseDbFactory Members


        IEnumerable<IBaseDbObject> IBaseDbFactory.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null)
        {
            return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
            
        }

        #endregion

        #region IBaseDbFactory Members


        public void RemoveAnyUnitTestingItems()
        {
            var session = CS.General_v3.Util.nHibernateUtil.GetCurrentSessionFromContext();
            CS.General_v3.Util.MySQL.SetForeignKeyCheck(false, session);
            
            
            var crit = getRawCriteria(null);
            crit.Add(NHibernate.Criterion.Expression.Where<IBaseDbObject>(x => x._TestItemSessionGuid != null && x._TestItemSessionGuid != ""));
            var result= FindAll(crit).ToList();
            using (var t = beginTransaction())
            {
                for (int i = 0; i < result.Count; i++)
                {
                    var item = result[i];
                    DeleteParams delParams = new DeleteParams(delPermanently: true);
                    item.Delete(delParams);
                }
                t.CommitIfActiveElseFlush();
            }
            CS.General_v3.Util.MySQL.SetForeignKeyCheck(true, session);
            
        }

        #endregion
    }
}
