﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Caching;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.Session;
using NHibernate;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;
namespace CS.General_v3.Classes.DbObjects
{
    public interface IBaseDbFactory<TItem> : IBaseDbFactory
        where TItem : IBaseDbObject
    {
        new TItem CreateNewItem();
        new TItem GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<TItem> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<TItem> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<TItem> FindAll(MyNHSessionBase session = null);
        new TItem FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new TItem FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new TItem FindItem(MyNHSessionBase session = null);
        new IEnumerable<TItem> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
    public interface IBaseDbFactory : CS.General_v3.Classes.Factories.IBaseFactory
    {
        event CS.General_v3.Classes.DbObjects.General.OnItemUpdateHandler OnItemUpdate;
        new IBaseDbObject CreateNewItem();
        IBaseDbObject GetByPrimaryKey(long id, MyNHSessionBase session = null);
        IEnumerable<IBaseDbObject> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);

        

        IEnumerable<IBaseDbObject> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        IEnumerable<IBaseDbObject> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        IEnumerable<IBaseDbObject> FindAll(MyNHSessionBase session = null);
        IBaseDbObject FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        IBaseDbObject FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        IBaseDbObject FindItem(MyNHSessionBase session = null);
        void OnItemUpdated(IBaseDbObject obj, Enums.UPDATE_TYPE updateType);

        ICriteria GetCriteria(GetQueryParams qParams = null);
        DetachedCriteria GetDetachedCriteria(GetQueryParams qParams = null);
        ICriteria GetQueryToRemoveTemporaryItems(int pageSize, int pageNum, GetQueryParams qParams = null);
        
        
        int GetTotalHoursToRemoveTemporary();
        
        string GetOutputCacheDependencyKey();
        void InvalidateOutputCacheDependency();


        void RemoveAnyUnitTestingItems();
    }
}

