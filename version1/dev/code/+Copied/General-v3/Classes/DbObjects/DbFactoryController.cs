﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.Factories;
using Iesi.Collections.Generic;
using CS.General_v3.Classes.NHibernateClasses;

namespace CS.General_v3.Classes.DbObjects
{
    public class DbFactoryController : FactoryController<IBaseDbFactory>
        
    {
        private static readonly DbFactoryController _Instance = new DbFactoryController();
        public static DbFactoryController Instance
        {
            get
            {
                return _Instance;
            }
        }
        
    }
}
