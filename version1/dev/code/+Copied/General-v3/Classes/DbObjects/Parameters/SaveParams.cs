﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.Session;

namespace CS.General_v3.Classes.DbObjects.Parameters
{
    public class SaveParams
    {
        public bool SavePermanently {get;set;}
        public bool UpdateFormulaProperties {get;set;}  
        public MyNHSessionBase Session {get;set;}
        public SaveParams(bool savePermanently = true, bool updateFormulaProperties = true, 
            MyNHSessionBase session = null)
        {
            this.SavePermanently = savePermanently;
            this.UpdateFormulaProperties = updateFormulaProperties;
            this.Session = session;
        }




        
        

    }
}
