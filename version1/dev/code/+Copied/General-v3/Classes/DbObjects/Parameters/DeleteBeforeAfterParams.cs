﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.HelperClasses;

namespace CS.General_v3.Classes.DbObjects.Parameters
{
    public delegate void DeleteBeforeAfterHandler(OperationResult result, DeleteBeforeAfterParams delParams);
    public class DeleteBeforeAfterParams : DeleteParams
    {
        public DeleteBeforeAfterParams(bool delPermanently = true, MyNHSessionBase session = null) :
            base (delPermanently: delPermanently, 
            session: session)
        {
            
        }




        
        

    }
}
