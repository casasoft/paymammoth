﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.HelperClasses;

namespace CS.General_v3.Classes.DbObjects.Parameters
{
    public class DeleteParams
    {
        public bool DeletePermanently {get;set;}
        
        public MyNHSessionBase Session {get;set;}
        public DeleteParams(bool delPermanently = false, MyNHSessionBase session = null)
        {
            
            this.DeletePermanently = delPermanently;
            this.Session = session;
        }




        
        

    }
}
