﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.HelperClasses;

namespace CS.General_v3.Classes.DbObjects.Parameters
{
    public delegate void SaveBeforeAfterHandler(OperationResult result, SaveBeforeAfterParams saveParams);
    public class SaveBeforeAfterParams : SaveParams
    {
        public bool IsNew {get;set;}
       
       
        public SaveBeforeAfterParams(bool isNew = false, bool savePermanently = true, 
            bool performUpdateFormulaProperties = true, MyNHSessionBase session = null) : base(savePermanently: savePermanently,
            updateFormulaProperties: performUpdateFormulaProperties,
            session: session)
        {
            this.IsNew = isNew;
            
        }

        
        

    }
}
