﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.NHManager;
using CS.General_v3.Classes.NHibernateClasses.Session;
using log4net;
using NHibernate;
using CS.General_v3.Classes.Factories;
using System.Collections;
using CS.General_v3.Classes.HelperClasses;
using System.Diagnostics.Contracts;
using NHibernate.Exceptions;
using CS.General_v3.Classes.Login;
using CS.General_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Classes.NHibernateClasses.Transactions;
using CS.General_v3.Classes.DbObjects.Persistors;
using CS.General_v3.Classes.NHibernateClasses.Persistance;

namespace CS.General_v3.Classes.DbObjects
{
    public abstract class BaseDbObject : IBaseDbObject
    {
        protected ILog _log = null;

        private IPersistor _persistor;
        protected IPersistor persistor
        {
            get 
            {
                if (_persistor == null)
                {
                    _persistor = new NhPersistor();
                }
                return _persistor; 
            }
            set 
            {
                _persistor = value;
                if (_persistor == null)
                {
                    int k = 5;
                }
            }
        }


        public BaseDbObject()
        {
            _log = log4net.LogManager.GetLogger(this.GetType());
            this.OnSaveBefore += this.Save_Before;
            this.OnSaveAfter += this.Save_After;
            this.OnDeleteBefore += this.Delete_Before;
            this.OnDeleteAfter += this.Delete_After;
            
        }


        protected object getItemLock()
        {
            return this.GetFactory().GetItemLock(this);
        }


        public bool ReferenceEquals(BaseDbObject obj)
        {
            return Object.ReferenceEquals(this, obj);
        }

        protected MyTransaction beginTransaction()
        {

            return getCurrentSession().BeginTransaction();
        }

        public static bool operator ==(BaseDbObject obj1, BaseDbObject obj2)
        {
            return Object.Equals(obj1, obj2);
        }
        public static bool operator !=(BaseDbObject obj1, BaseDbObject obj2)
        {
            return !Object.Equals(obj1, obj2);
        }

      

        public virtual bool IsAvailableToFrontend()
        {
            return this.Published && !this.Deleted && !this._Temporary_Flag;
        }

        protected MyNHSessionBase _currentSession = null;

        private bool isDirty { get; set; }
        protected void markAsDirty()
        {
            this.MarkAsDirty();
        }
        public virtual void MarkAsDirty()
        {
            isDirty = true;
        }

        protected virtual void checkPropertyUpdateAndUpdateDirtyFlag(object p1, object p2)
        {
            if (
                (p1 != null && p2 != null && p1.GetHashCode() != p2.GetHashCode()) ||
                (p1 == null && p2 != null) ||
                (p1 != null && p2 == null)
                )
            {
                if (!(p1 is DateTime) || !(p2 is DateTime))
                {
                    markAsDirty();
                }
                else
                {
                    DateTime d1 = (DateTime)p1;
                    DateTime d2 = (DateTime)p2;
                    if ((d1.Year != d2.Year) ||
                        (d1.Month != d2.Month) ||
                        (d1.Day != d2.Day) ||
                        (d1.Hour != d2.Hour) ||
                        (d1.Minute != d2.Minute))
                    {
                        markAsDirty();
                    }
                }
                
            }

        }

        protected virtual bool _alwaysDeletePermanently { get; set; }

        private bool checkIfItemIsNew()
        {
            bool isNew = IsTransient() || _Temporary_Flag;

            return isNew;
        }
        private void checkSavePermanentlyVariable(bool savePermanently)
        {
            if (savePermanently) MarkAsNotTemporary();

        }


        //++++++++++++++++++++++++











        private IBaseDbFactory _factory = null;
        public IBaseDbFactory GetFactory()
        {
            if (_factory == null)
                _factory = DbFactoryController.Instance.GetFactoryForObject(this);
            return _factory;
        }
        protected MyNHSessionBase getCurrentSession()
        {
            MyNHSessionBase s = null;
            if (_currentSession == null)
                s = NHClasses.NhManager.GetCurrentSessionFromContext();
            else
                s = _currentSession;
            return s;
        }

        public virtual bool HasTemporaryFields()
        {
            return true;
        }




         protected internal virtual void updateLastEdited()
         {
             this.LastEditedOn = CS.General_v3.Util.Date.Now;
         }

        /// <summary>
        /// Reloads the item from the database
        /// </summary>
        /// <param name="session"></param>
        public void ReloadFromDb(MyNHSessionBase session = null)
        {
            if (session == null) session = getCurrentSession();
            session.Refresh(this);
            
            
        }
        


        public virtual bool ComparePKey(BaseDbObject itemToCompareWith)
        {
            if (itemToCompareWith != null && itemToCompareWith.ID == this.ID)
            {

                return true;
            }
            else
            {
                return false;
            }
        }

        
        public virtual void MarkAsNotTemporary()
        {
            _Temporary_Flag = false;
            

        }
        public virtual void MarkAsTemporary()
        {
            _Temporary_Flag= true;
            _Temporary_LastUpdOn = DateTime.Now;

        }


        
        protected long? _currentCultureInfoID = null;
        public abstract long GetCurrentCultureDBId();

        public virtual void SetCurrentCultureInfoDBId(long? cultureInfoID)
        {
            _currentCultureInfoID = cultureInfoID;

        }


        



        /// <summary>
        /// Returns whether this object is yet saved in DB or not
        /// </summary>
        public virtual bool IsSavedInDB()
        {
            return (this.ID > 0);
        }



        public override sealed bool Equals(object obj)
        {
            BaseDbObject compareTo = obj as BaseDbObject;

            
          

            return (compareTo != null) &&
                   (hasSameNonDefaultIdAs(compareTo) ||
                // Since the IDs aren't the same, either of them must be transient to 
                // compare business value signatures
                    (((IsTransient()) || compareTo.IsTransient()) &&
                     HasSameBusinessSignatureAs(compareTo)));
        }

        /// <summary>
        /// Transient objects are not associated with an item already in storage.  For instance,
        /// a domain object instance is transient if its ID is 0.
        /// </summary>
        public virtual bool IsTransient()
        {
            return Equals(ID, default(long));
        }
        public virtual bool IsTemporary()
        {
            return IsTransient() || _Temporary_Flag;
        }


        /// <summary>
        /// Must be provided to properly compare two objects.
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            
            return IsTransient() ? base.GetHashCode() : (base.GetHashCode() * 31) + ID.GetHashCode();

            //if (!IsTransient())
            //    return this.ID.GetHashCode();
            //else
            //    return base.GetHashCode();
        }

        private bool HasSameBusinessSignatureAs(BaseDbObject compareTo)
        {

            Contract.Requires(compareTo != null, "compareTo may not be null");

            return GetHashCode().Equals(compareTo.GetHashCode());
        }

        /// <summary>
        /// Returns true if self and the provided persistent object have the same ID values 
        /// and the IDs are not of the default ID value
        /// </summary>
        private bool hasSameNonDefaultIdAs(BaseDbObject compareTo)
        {
            Contract.Requires(compareTo != null, "compareTo may not be null");

            return (!ID.Equals(default(long))) &&
                   (!compareTo.ID.Equals(default(long))) && ID.Equals(compareTo.ID);
        }


        #region CRUD operations

        public event DeleteBeforeAfterHandler OnDeleteBefore;
        public event DeleteBeforeAfterHandler OnDeleteAfter;

        protected virtual void Delete_Before(OperationResult result, DeleteBeforeAfterParams delParams)
        {
           
        }

        protected virtual void Delete_After(OperationResult result, DeleteBeforeAfterParams delParams)
        {
            invalidateOutputCaching();
            if (!delParams.DeletePermanently)
            { //if not deleted permanently, update the computed deleted value
                UpdateComputedDeletedValue(autoSave: true);
            }
        }

        private void invalidateOutputCaching()
        {
            this.GetFactory().InvalidateOutputCacheDependency();
            this.InvalidateOutputCacheDependency();
        }

        protected virtual void updateFormulaPropertiesBeforeSaving()
        {

        }
        protected virtual void updateFormulaPropertiesAfterSaving()
        {

        }
        protected void _save_Before(OperationResult result, SaveBeforeAfterParams sParams)
        {
            if (sParams.UpdateFormulaProperties)
                updateFormulaPropertiesBeforeSaving();
            if (OnSaveBefore != null) OnSaveBefore(result, sParams);
        }

        public event SaveBeforeAfterHandler OnSaveBefore;
        public event SaveBeforeAfterHandler OnSaveAfter;

        protected virtual void Save_Before(OperationResult result, SaveBeforeAfterParams sParams)
        {
            if (!sParams.SavePermanently && _Temporary_Flag)
            {
                _Temporary_LastUpdOn = CS.General_v3.Util.Date.Now;
            }
            if (isDirty)
            {
                updateLastEdited();
            }

            

        }
        protected void _save_After(OperationResult result, SaveBeforeAfterParams sParams)
        {
            if (sParams.UpdateFormulaProperties)
                updateFormulaPropertiesAfterSaving();
            if (OnSaveAfter != null) OnSaveAfter(result, sParams);
        }
        protected virtual void Save_After(OperationResult result, SaveBeforeAfterParams sParams)
        {
            invalidateOutputCaching();
            isDirty = false;
        }
        public void InvalidateOutputCacheDependency()
        {
            string key = GetOutputCacheDependencyKey();
            CS.General_v3.Util.CachingUtil.InvalidateCacheDependency(key);
            
        }

        /// <summary>
        /// This can be used to re-attach detached objects for lazy loading
        /// </summary>
        /// <param name="session"></param>
        public void Lock(MyNHSessionBase session = null)
        {
            Lock(LockMode.None, session);

        }

        /// <summary>
        /// This can be used to re-attach detached objects for lazy loading
        /// </summary>
        /// <param name="lockMode"></param>
        /// <param name="session"></param>
        public void Lock(LockMode lockMode= null,MyNHSessionBase session = null)
        {
            
            if (session == null) session = getCurrentSession();
            session.Lock(this, lockMode);
            
        }


        public virtual void Merge(MyNHSessionBase session = null)
        {
            if (session == null) session = getCurrentSession();
            session.Merge(this);

            
        }

        public void Evict(MyNHSessionBase session = null)
        {
            if (session == null) session = getCurrentSession();
            try
            {
                session.Evict(this);
            }
            catch (Exception ex)
            {
                int k = 5;
                throw;
            }
        }

        #endregion

        
        #region IBaseDbObject Members
        

        protected virtual void updateDeletedFields()
        {
            Deleted = true;
            DeletedOn = CS.General_v3.Util.Date.Now;
            

        }

        private OperationResult _delete(MyNHSessionBase session , bool deletePermanently, bool autoCommit)
        {
            if (!deletePermanently && _alwaysDeletePermanently) deletePermanently = true;
            if (session == null) session = getCurrentSession();
            OperationResult result = new OperationResult();
            if (OnDeleteBefore != null) OnDeleteBefore(result, new DeleteBeforeAfterParams(deletePermanently, session)); 
            if (result.IsSuccessful)
            {
                if (!deletePermanently)
                {
                    updateDeletedFields();
                    persistor.Update(this, result, session, autoCommit, true, true);
                    //if (autoCommit)
                    //    UpdateAndCommit();
                    //else
                    //    Update();
                }
                else
                {
                    persistor.Delete(this, result, session, deletePermanently, autoCommit);

                }
                persistor.DeleteAfter(this, result, session, deletePermanently, autoCommit);
               
                
                if (OnDeleteAfter != null) OnDeleteAfter(result, new DeleteBeforeAfterParams(deletePermanently, session));
            }
           
            return result;
        }
        private OperationResult _update(MyNHSessionBase session, bool autoCommit, bool savePermanently, bool updateFormulaProperties)
        {
            if (IsTransient())
            {
                throw new InvalidOperationException("You cannot call UPDATE if the object is still transient (not yet saved in database)");
            }

            OperationResult result = new OperationResult();
            if (session == null) session = getCurrentSession();
            checkSavePermanentlyVariable(savePermanently);
            _save_Before(result, new SaveBeforeAfterParams(isNew: false, savePermanently: savePermanently, performUpdateFormulaProperties: updateFormulaProperties, session: session));
            if (result.IsSuccessful)
            {
                persistor.Update(this, result, session, autoCommit, savePermanently, updateFormulaProperties);

                
                _save_After(result, new SaveBeforeAfterParams(isNew: false, savePermanently: savePermanently, performUpdateFormulaProperties: updateFormulaProperties, session: session));
                persistor.SaveAfter(this, result, session, autoCommit, savePermanently, updateFormulaProperties, false);
            }
           
            return result;
        }
        private OperationResult _save(MyNHSessionBase session, bool autoCommit, bool savePermanently , bool updateFormulaProperties )
        {

            OperationResult result = new OperationResult();
            if (session == null) session = getCurrentSession();
            
            checkSavePermanentlyVariable(savePermanently);
            bool isNew = checkIfItemIsNew();
            _save_Before(result, new SaveBeforeAfterParams(isNew: false, savePermanently: savePermanently, performUpdateFormulaProperties: updateFormulaProperties, session: session));
            if (result.IsSuccessful)
            {
                persistor.Save(this, result, session, autoCommit, savePermanently, updateFormulaProperties);

                _save_After(result, new SaveBeforeAfterParams(isNew: false, savePermanently: savePermanently, performUpdateFormulaProperties: updateFormulaProperties, session: session));
                persistor.SaveAfter(this, result, session, autoCommit, savePermanently, updateFormulaProperties, isNew);
                
                
            }
           
            return result;
        }
        
        private OperationResult _saveCopy(MyNHSessionBase session, bool autoCommit, bool savePermanently, bool updateFormulaProperties )
        {
            OperationResult result = new OperationResult();
            if (session == null) session = getCurrentSession();

            checkSavePermanentlyVariable(savePermanently);
            bool isNew = checkIfItemIsNew();
            _save_Before(result, new SaveBeforeAfterParams(isNew: false, savePermanently: savePermanently, performUpdateFormulaProperties: updateFormulaProperties, session: session));
            if (result.IsSuccessful)
            {
                persistor.SaveOrUpdateCopy(this, result, session, autoCommit, savePermanently, updateFormulaProperties);
                _save_After(result, new SaveBeforeAfterParams(isNew: false, savePermanently: savePermanently, performUpdateFormulaProperties: updateFormulaProperties, session: session));
                persistor.SaveAfter(this, result, session, autoCommit, savePermanently, updateFormulaProperties, isNew);
                
               
            }
            
            
            return result;
        }
        private OperationResult _create(MyNHSessionBase session, bool autoCommit, bool savePermanently, bool updateFormulaProperties )
        {

            OperationResult result = new OperationResult();
            if (session == null) session = getCurrentSession();
            checkSavePermanentlyVariable(savePermanently);
            bool isNew = checkIfItemIsNew();
            _save_Before(result, new SaveBeforeAfterParams(isNew: false, savePermanently: savePermanently, performUpdateFormulaProperties: updateFormulaProperties, session: session));
            if (result.IsSuccessful)
            {
                persistor.Save(this, result, session, autoCommit, savePermanently, updateFormulaProperties);

                
                _save_After(result, new SaveBeforeAfterParams(isNew: false, savePermanently: savePermanently, performUpdateFormulaProperties: updateFormulaProperties, session: session));
                persistor.SaveAfter(this, result, session, autoCommit, savePermanently, updateFormulaProperties, isNew);
            }
            
            return result;

        }
        public OperationResult Create(SaveParams sParams = null)
        {
            if (sParams == null) sParams = new SaveParams();
            return _create(session: sParams.Session, autoCommit: false, savePermanently: sParams.SavePermanently, updateFormulaProperties: sParams.UpdateFormulaProperties);
        }

        public OperationResult CreateAndCommit(SaveParams sParams = null)
        {
            if (sParams == null) sParams = new SaveParams();
            return _create(session: sParams.Session, autoCommit: true, savePermanently: sParams.SavePermanently, updateFormulaProperties: sParams.UpdateFormulaProperties);
        }

        public OperationResult SaveAndCommit(SaveParams sParams = null)
        {
            if (sParams == null) sParams = new SaveParams();
            return _save(session: sParams.Session, autoCommit: true, savePermanently: sParams.SavePermanently, updateFormulaProperties: sParams.UpdateFormulaProperties);
        }



        public OperationResult UpdateAndCommit(SaveParams sParams = null)
        {
            if (sParams == null) sParams = new SaveParams();
            return _update(session: sParams.Session, autoCommit: true, savePermanently: sParams.SavePermanently, updateFormulaProperties: sParams.UpdateFormulaProperties);
            
        }

        public OperationResult Save(SaveParams sParams = null)
        {
            if (sParams == null) sParams = new SaveParams();
            return _save(session: sParams.Session, autoCommit: false, savePermanently: sParams.SavePermanently, updateFormulaProperties: sParams.UpdateFormulaProperties);
        }

        public OperationResult Update(SaveParams sParams = null)
        {

            if (sParams == null) sParams = new SaveParams();
            return _update(session: sParams.Session, autoCommit: false, savePermanently: sParams.SavePermanently, updateFormulaProperties: sParams.UpdateFormulaProperties);

        }
        public OperationResult Delete(DeleteParams delParams = null)
        {
            if (delParams == null) delParams = new DeleteParams();
            return _delete(session: delParams.Session, autoCommit: false, deletePermanently: delParams.DeletePermanently);
           
        }
        public OperationResult DeleteAndCommit(DeleteParams delParams = null)
        {
            if (delParams == null) delParams = new DeleteParams();
            return _delete(session: delParams.Session, autoCommit: true, deletePermanently: delParams.DeletePermanently);
           
        }






        #endregion

        

       
     

    

        public virtual void DeleteItemPermanentlyIncludingLinked()
        {
            this.Delete(new DeleteParams(delPermanently: true));
        }

        public virtual void DeleteTemporaryItemAsTimeElapsed()
        {
            this.DeleteItemPermanentlyIncludingLinked();
            
        }

        protected virtual void resetDirtyFlag()
        {
            this.isDirty = false;
            
        }


        void IBaseDbObject.resetDirtyFlag()
        {
            resetDirtyFlag();
        }

        protected virtual void initPropertiesForNewItem()
        {
            this.Published = true;
            this.LastEditedOn = CS.General_v3.Util.Date.Now;
            if (CS.General_v3.Util.Other.IsInUnitTestingEnvironment)
            {
                this._TestItemSessionGuid = CS.General_v3.Util.Other.UnitTestingEnvironment_SessionGuid;
            }
        }


        void IBaseDbObject.InitPropertiesForNewItem()
        {
            this.initPropertiesForNewItem();
        }


        private bool _deleted;
        public virtual bool Deleted
        {
            get { return _deleted; }
            set { _deleted = value; }
        }
        private bool __computeddeletedvalue;
        public virtual bool _ComputedDeletedValue
        {
            get { return __computeddeletedvalue; }
            set { __computeddeletedvalue = value; }
        }
        private bool _published;
        public virtual bool Published
        {
            get { return _published; }
            set { _published = value; }
        }

        public event UpdateComputedDelValueHandler OnUpdateComputedDeletedValue;

        public void UpdateComputedDeletedValue(bool autoSave = false)
        {
            UpdateComputedDelValueArgs args = new UpdateComputedDelValueArgs();
                
            if (OnUpdateComputedDeletedValue != null)
            {
                
                OnUpdateComputedDeletedValue(args);
            }

            bool lastValue = this._ComputedDeletedValue;
            if (args.ComputedDeletedValue.HasValue)
            {
                this._ComputedDeletedValue = args.ComputedDeletedValue.Value;
            }
            else
            {
                this._ComputedDeletedValue = this.Deleted;
            }
            if (autoSave && lastValue != this._ComputedDeletedValue)
                 this.Save();
            

            
            

        }

        private DateTime __temporary_lastupdon;
        public virtual DateTime _Temporary_LastUpdOn
        {
            get { return __temporary_lastupdon; }
            set { __temporary_lastupdon = value; }
        }

        private bool __temporary_flag;
        public virtual bool _Temporary_Flag
        {
            get { return __temporary_flag; }
            set { __temporary_flag = value; }
        }

        private DateTime? _deletedon;
        public virtual DateTime? DeletedOn
        {
            get { return _deletedon; }
            set { _deletedon = value; }
        }

        private DateTime? _lastEditedOn;
        public virtual DateTime? LastEditedOn
        {
            get { return _lastEditedOn; }
            set { _lastEditedOn = value; }
        }

        private IUser _LastEditedBy;
        public virtual IUser LastEditedBy
        {
            get { return _LastEditedBy; }
            set { _LastEditedBy = value; }
        }

      

        private IUser _deletedby;
        public virtual IUser DeletedBy
        {
            get { return _deletedby; }
            set { _deletedby = value; }
        }

      

        private int _priority;
        public virtual int Priority
        {
            get { return _priority; }
            set { _priority = value; }
        }

        private long _id;
        public virtual long ID
        {
            get { return _id; }
           // private set { _id = value; }

        }



        public string GetOutputCacheDependencyKey()
        {
            return this.GetType().FullName + "_ID_" + this.ID;
        }

        public override string ToString()
        {
            return this.GetType().Name + " [" + this.ID + "]";
            
        }
        




        #region IBaseDbObject Members


     
        #endregion

        #region IBaseDbObject Members

        private string _testitemsessionguid;
        public string _TestItemSessionGuid
        {
            get { return _testitemsessionguid; }
            set { _testitemsessionguid = value; }
        }

        #endregion

        /// <summary>
        /// Resets the test item session guid, and saves immediatey if needed.
        /// </summary>
        public void ResetTestItemSessionGuid()
        {
            if (!string.IsNullOrWhiteSpace(_TestItemSessionGuid))
            {
                using (var t = beginTransaction())
                {
                    _TestItemSessionGuid = null;
                    if (!this.IsTransient())
                    {
                        this.Save();
                        t.CommitIfActiveElseFlush();
                    }
                }
            }
        }

        #region IBaseDbObject Members


        


        #endregion
    }
}
