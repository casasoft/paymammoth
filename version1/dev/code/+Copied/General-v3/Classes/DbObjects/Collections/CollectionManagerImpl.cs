﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.DbObjects.Collections
{
    public class CollectionManagerImpl<T> : List<T>, ICollectionManager<T>
    {
        public T CreateNewItem()
        {
            throw new NotImplementedException();
        }

        public void Remove(T item, bool removeToOtherSideAsWell = true)
        {
            base.Remove(item);
        }

        public void Add(T item, bool addToOtherSideAsWell = true)
        {
            base.Add(item);
        }

        public void SetItems(IEnumerable<T> list)
        {
            base.Clear();
            base.AddRange(list);
        }

        public List<T> ToList()
        {
            return this;
        }

        public List<T> GetItemsOnlyAvailableToFrontend()
        {
            return this;
        }
    }
}
