﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.DbObjects.Collections
{
    public interface IBaseDbCollectionInfo<TItem, TCollectionItem>
    {
        IEnumerable<TCollectionItem> Collection { get; }
        
    }

}
