﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Util;

namespace CS.General_v3.Classes.DbObjects.Collections
{
    public class DbCollectionManyToManyManager<TItem, TCollectionItem, TCollectionItemInterface> : BaseDbCollectionManager<TItem, TCollectionItem>, ICollectionManager<TCollectionItemInterface>
              where TItem : IBaseDbObject
        where TCollectionItem : TCollectionItemInterface, IBaseDbObject
    {

        protected IDbCollectionManyToManyInfo<TItem, TCollectionItem> collectionInfo { get { return (IDbCollectionManyToManyInfo<TItem, TCollectionItem>)base.collectionInfo; } }
        public DbCollectionManyToManyManager(TItem dbItem)
            : base(dbItem)
        {
            
        }

        protected override void onPreAdd(TCollectionItem item, bool addToOtherSideAsWell )
        {
            if (addToOtherSideAsWell)
            {
                var linkedColl = collectionInfo.GetLinkedCollectionForItem(item);

                linkedColl.Add(this.dbItem, addToOtherSideAsWell: false);
            }

            base.onPreAdd(item, addToOtherSideAsWell);
        }

        protected override void onPreRemove(TCollectionItem item, bool removeToOtherSideAsWell )
        {
            if (removeToOtherSideAsWell)
            {
                var linkedColl = collectionInfo.GetLinkedCollectionForItem(item);
                linkedColl.Remove(this.dbItem, removeToOtherSideAsWell: false);
            }
            base.onPreRemove(item, removeToOtherSideAsWell);
        }

        #region ICollectionManager<TInterface> Members

        TCollectionItemInterface ICollectionManager<TCollectionItemInterface>.CreateNewItem()
        {
            return this.CreateNewItem();

        }

        void ICollectionManager<TCollectionItemInterface>.AddRange(IEnumerable<TCollectionItemInterface> list)
        {

            this.AddRange((IEnumerable<TCollectionItem>)list);

        }

        void ICollectionManager<TCollectionItemInterface>.Remove(TCollectionItemInterface item, bool removeToOtherSideAsWell = true)
        {
            this.Remove((TCollectionItem)item, removeToOtherSideAsWell);

        }

        void ICollectionManager<TCollectionItemInterface>.Add(TCollectionItemInterface item, bool addToOtherSideAsWell = true)
        {
            this.Add((TCollectionItem)item, addToOtherSideAsWell);

        }

        void ICollectionManager<TCollectionItemInterface>.SetItems(IEnumerable<TCollectionItemInterface> list)
        {
            this.SetItems((IEnumerable<TCollectionItem>)list);

        }

        #endregion

        #region IEnumerable<TInterface> Members

        IEnumerator<TCollectionItemInterface> IEnumerable<TCollectionItemInterface>.GetEnumerator()
        {
            return ((IEnumerable<TCollectionItemInterface>)this.collectionInfo.Collection).GetEnumerator();
            //List<TCollectionItemInterface> list = new List<TCollectionItemInterface>();
            //list.AddRange(this);
            //return list.GetEnumerator();

        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();

        }

        #endregion


        #region ICollectionManager<TCollectionItemInterface> Members

        List<TCollectionItemInterface> ICollectionManager<TCollectionItemInterface>.ToList()
        {
            return this.castedCollection.Cast<TCollectionItemInterface>().ToList();
            
        }

        #endregion



        #region ICollectionManager<TCollectionItemInterface> Members


        public new List<TCollectionItemInterface> GetItemsOnlyAvailableToFrontend()
        {
            return base.GetItemsOnlyAvailableToFrontend().Cast<TCollectionItemInterface>().ToList();
            
        }

        #endregion
    }

}
