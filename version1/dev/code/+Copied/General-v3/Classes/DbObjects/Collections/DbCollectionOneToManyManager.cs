﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Util;

namespace CS.General_v3.Classes.DbObjects.Collections
{
    public class DbCollectionOneToManyManager<TItem, TCollectionItem, TCollectionItemInterface> : BaseDbCollectionManager<TItem, TCollectionItem>, ICollectionManager<TCollectionItemInterface>
              where TItem : IBaseDbObject

        where TCollectionItem : TCollectionItemInterface, IBaseDbObject

    {

        protected new IDbCollectionOneToManyInfo<TItem, TCollectionItem> collectionInfo { get { return (IDbCollectionOneToManyInfo<TItem, TCollectionItem>) base.collectionInfo; } }
        
        public DbCollectionOneToManyManager(TItem dbItem) : base(dbItem)
        {
            
        }

        protected override void onPreAdd(TCollectionItem item, bool addLinkToOtherSide)
        {
            if (addLinkToOtherSide)
                collectionInfo.SetLinkOnItem(item, this.dbItem);
                
            base.onPreAdd(item, addLinkToOtherSide);
        }

        protected override void onPreRemove(TCollectionItem item, bool removeLinkFromOtherSide)
        {
            if (removeLinkFromOtherSide)
                collectionInfo.SetLinkOnItem(item, default(TItem));
                
            base.onPreRemove(item, removeLinkFromOtherSide);
        }

        #region ICollectionManager<TInterface> Members

        TCollectionItemInterface ICollectionManager<TCollectionItemInterface>.CreateNewItem()
        {
            return this.CreateNewItem();
            
        }

        void ICollectionManager<TCollectionItemInterface>.AddRange(IEnumerable<TCollectionItemInterface> list)
        {
            
            this.AddRange((IEnumerable<TCollectionItem>) list);
            
        }

        void ICollectionManager<TCollectionItemInterface>.Remove(TCollectionItemInterface item, bool removeToOtherSideAsWell = true)
        {
            this.Remove((TCollectionItem) item, removeToOtherSideAsWell);
            
        }

        void ICollectionManager<TCollectionItemInterface>.Add(TCollectionItemInterface item, bool addToOtherSideAsWell = true)
        {
            this.Add((TCollectionItem) item, addToOtherSideAsWell);
            
        }

        void ICollectionManager<TCollectionItemInterface>.SetItems(IEnumerable<TCollectionItemInterface> list)
        {
            this.SetItems((IEnumerable<TCollectionItem>) list);
            
        }

        #endregion

        #region IEnumerable<TInterface> Members

        IEnumerator<TCollectionItemInterface> IEnumerable<TCollectionItemInterface>.GetEnumerator()
        {
            return (IEnumerator<TCollectionItemInterface>)(this.castedCollection.GetEnumerator());

            List<TCollectionItemInterface> list = new List<TCollectionItemInterface>();
            list.AddRange(this);
            return list.GetEnumerator();
            
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
            
        }

        #endregion


        public List<TCollectionItem> ToList()
        {
            
            return this.castedCollection.ToList();
        }

        #region ICollectionManager<TCollectionItemInterface> Members

        List<TCollectionItemInterface> ICollectionManager<TCollectionItemInterface>.ToList()
        {
            return this.castedCollection.Cast<TCollectionItemInterface>().ToList();
            
        }

        #endregion



        #region ICollectionManager<TCollectionItemInterface> Members


        public new List<TCollectionItemInterface> GetItemsOnlyAvailableToFrontend()
        {
            return base.GetItemsOnlyAvailableToFrontend().Cast<TCollectionItemInterface>().ToList();
            
        }

        #endregion
    }

}
