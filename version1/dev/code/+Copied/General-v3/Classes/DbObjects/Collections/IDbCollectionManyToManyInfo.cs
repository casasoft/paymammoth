﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.DbObjects.Collections
{
    public interface IDbCollectionManyToManyInfo<TItem, TCollectionItem> : IBaseDbCollectionInfo<TItem, TCollectionItem>
    {
        ICollectionManager<TItem> GetLinkedCollectionForItem(TCollectionItem item);
        
    }

}
