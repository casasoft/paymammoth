﻿using System;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.NHibernateClasses.Session;
using NHibernate;
using CS.General_v3.Classes.Login;
using CS.General_v3.Classes.DbObjects.Parameters;
namespace CS.General_v3.Classes.DbObjects
{
    public interface IBaseDbObject
    {
        IBaseDbFactory GetFactory();
        bool Published { get; set; }
        bool Deleted { get; set; }
        bool _ComputedDeletedValue { get; set; }
        DateTime _Temporary_LastUpdOn { get; set; }
        bool _Temporary_Flag { get; set; }
        DateTime? DeletedOn { get; set; }
        DateTime? LastEditedOn { get; set; }
        bool IsAvailableToFrontend();
        IUser DeletedBy { get; set; }
        IUser LastEditedBy { get; set; }
        int Priority { get; set; }
        string _TestItemSessionGuid { get; set; }
        bool IsSavedInDB();
        void UpdateComputedDeletedValue(bool autoSave = false);
        bool IsTransient();
        void MarkAsDirty();
        void MarkAsNotTemporary();
        void MarkAsTemporary();
        void InitPropertiesForNewItem();
       
        long ID { get; }

        void resetDirtyFlag();
        void Evict(MyNHSessionBase session = null);
        void ReloadFromDb(MyNHSessionBase  session = null);

        OperationResult Create(SaveParams saveParams = null);
        OperationResult CreateAndCommit(SaveParams saveParams = null);
        OperationResult Save(SaveParams saveParams = null);
        OperationResult SaveAndCommit(SaveParams saveParams = null);
        OperationResult Update(SaveParams saveParams = null);
        OperationResult UpdateAndCommit(SaveParams saveParams = null);
        OperationResult Delete(DeleteParams delParams = null);
        OperationResult DeleteAndCommit(DeleteParams delParams = null);

        void Merge(MyNHSessionBase session = null);
        string GetOutputCacheDependencyKey();

        void DeleteTemporaryItemAsTimeElapsed();
        event UpdateComputedDelValueHandler OnUpdateComputedDeletedValue;
        event SaveBeforeAfterHandler OnSaveBefore;
        event SaveBeforeAfterHandler OnSaveAfter;
        event DeleteBeforeAfterHandler OnDeleteBefore;
        event DeleteBeforeAfterHandler OnDeleteAfter;
    }
}
