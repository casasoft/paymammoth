﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.NHibernateClasses.Session;

namespace CS.General_v3.Classes.DbObjects.Persistors
{
    public interface IPersistor
    {

        void Save(IBaseDbObject itemToPersist, OperationResult result, MyNHSessionBase session, bool autoCommit, bool savePermanently, bool updateFormulaProperties);
        void SaveOrUpdateCopy(IBaseDbObject itemToPersist, OperationResult result, MyNHSessionBase session, bool autoCommit, bool savePermanently, bool updateFormulaProperties);
        void SaveAfter(IBaseDbObject itemToPersist, OperationResult result, MyNHSessionBase session, bool autoCommit, bool savePermanently, bool updateFormulaProperties, bool isNew);
        void Update(IBaseDbObject itemToPersist, OperationResult result, MyNHSessionBase session, bool autoCommit, bool savePermanently, bool updateFormulaProperties);
        void Delete(IBaseDbObject itemToPersist, OperationResult result, MyNHSessionBase session, bool deletePermanently, bool autoCommit);
        void DeleteAfter(IBaseDbObject itemToPersist, OperationResult result, MyNHSessionBase session, bool deletePermanently, bool autoCommit);
        //void SaveAfter(OperationResult result, MyNHSessionBase session, bool autoCommit, bool savePermanently, bool updateFormulaProperties, bool isNew);
       
    }
}
