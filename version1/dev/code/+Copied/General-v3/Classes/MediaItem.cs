﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace CS.General_20090518.Classes
{

    public abstract class MediaItem
    {
        /*
         * Using this class:
         * 
         * The property 'Identifier' must be set, which will be the string to identify the photo in the folder.  (Normally this is the ID as string). Take care
         * to update this property say should the ID change.
         * 
         * 'Folder' must be set to the folder (From root)
         * 'Extension' must be set to the extension for this photo
         * 'OriginalMaxWidth' must be set to the size for the original photo
         * 'OriginalMaxHeight' must be set to the size for the original photo
         * 'OriginalFolder' must be set for the folder for the original photos.  If left empty, is same as folder
         * 'OriginalExtension' must be set for the extension of the original photo. If let empty, is same as extension
         * 
         * An initialise function should be created, that will call 'AddImageSize()' according to the number of sizes needed for this photo.  Eg. 3 times, one for thumbnail, normal and large
         * 
         * Add the respective 'getRelXXXXXX(), getRootXXXXXXx()' etc functions to map them to the 'AddImageSize' calls.
         */

        public class IMAGE_SIZE_INFO
        {
            public string Title = "";
            public string NoImagePath = "";
            public int Width = 0;
            public int Height = 0;
            public bool FillBox = false;
            public CS.General_20090518.Image.CROP_IDENTIFIER CropIdentifier = Image.CROP_IDENTIFIER.MiddleMiddle;
            public IMAGE_SIZE_INFO(string title, int w, int h, bool fillBox, CS.General_20090518.Image.CROP_IDENTIFIER cropIdentifier)
            {
                this.Title = title;
                this.Width = w;
                this.Height = h;
                this.FillBox = fillBox;
                this.CropIdentifier = cropIdentifier;
            }
        }

        private List<IMAGE_SIZE_INFO> _sizes = null;
        //from root, e.g uploads/photos/
        private string _BaseLocalFolder = null;

        private string _Folder = null;
        private string _OriginalFolder = null;
        private string _Extension = "jpg";
        private string _OriginalExtension = "jpg";
        private string _Identifier = null;
        private int _OriginalMaxWidth = Int32.MinValue;
        private int _OriginalMaxHeight = Int32.MinValue;

        #region Publics
        /// <summary>
        /// List of Sizes of Photo
        /// </summary>
        public List<IMAGE_SIZE_INFO> Sizes
        {
            get
            {
                if (_sizes == null)
                    _sizes = new List<IMAGE_SIZE_INFO>();
                return _sizes;
            }
            set
            {
                _sizes = value;
            }
        }
        public string OriginalFolder
        {
            get
            {
                if (string.IsNullOrEmpty(_OriginalFolder))
                    return _Folder;
                else
                    return _OriginalFolder;
            }
            set
            {
                _OriginalFolder = value;
            }

        }
        public string OriginalExtension
        {
            get
            {
                if (string.IsNullOrEmpty(_OriginalExtension))
                    return _Extension;
                else
                    return _OriginalExtension;
            }
            set
            {
                _OriginalExtension = value;
            }

        }
        /// <summary>
        /// Local folder, mapped either with Server.MapPath or local folder, according to situation.  Always ends in '\'
        /// </summary>
        public string BaseLocalFolder
        {
            get
            {
                if (!string.IsNullOrEmpty(_BaseLocalFolder))
                    return _BaseLocalFolder;
                else
                {
                    return CS.Settings.LocalRootFolder;
                }

            }
            set
            {
                _BaseLocalFolder = value;
                if (_BaseLocalFolder != null && _BaseLocalFolder.Length > 0 && _BaseLocalFolder[_BaseLocalFolder.Length - 1] != '\\')
                {
                    _BaseLocalFolder += "\\";
                }
            }
        }
        /// <summary>
        /// Folder where to store the photo.  Should NOT start with a /, and SHOULD end with a /.  E.g uploads/products/
        /// </summary>
        public string Folder
        {
            get
            {
                return _Folder;
            }
            set
            {
                _Folder = value;
                if (_Folder != null && _Folder.Length > 0)
                {
                    if (_Folder[_Folder.Length - 1] != '/')
                        _Folder += "//";
                    if (_Folder.Length > 0 && _Folder[0] == '/')
                        _Folder = _Folder.Remove(0, 1);

                }
            }
        }
        /// <summary>
        /// Extension of image
        /// </summary>
        public string Extension
        {
            get
            {
                return _Extension;

            }
            set
            {
                _Extension = value;
            }
        }
        protected void SetIdentifier(object o)
        {
            _Identifier = Convert.ToString(o);
            checkImages();
        }

        /// <summary>
        /// Identifier to identify the image
        /// </summary>
        public string Identifier
        {
            get
            {
                return _Identifier;

            }
            set
            {
                _Identifier = value;
            }
        }
        /// <summary>
        /// Original photo maximum width
        /// </summary>
        public int OriginalMaxWidth
        {
            get { return _OriginalMaxWidth; }
            set { _OriginalMaxWidth = value; }
        }
        /// <summary>
        /// Original photo maximum height
        /// </summary>
        public int OriginalMaxHeight
        {
            get { return _OriginalMaxHeight; }
            set { _OriginalMaxHeight = value; }
        }
        //######################
        /// <summary>
        /// Returns an IMAGE_SIZE_OF with the specified title
        /// </summary>
        /// <param name="title">Title</param>
        /// <returns>The IMAGE_SIZE_OF item</returns>
        public IMAGE_SIZE_INFO getImageSize(string title)
        {
            title = title.ToLower();
            for (int i = 0; i < Sizes.Count; i++)
            {
                if (Sizes[i].Title.ToLower() == title)
                    return Sizes[i];
            }
            return null;
        }
        /// <summary>
        /// Adds an image size to the list of images
        /// </summary>
        /// <param name="title">Title</param>
        /// <param name="width">Width</param>
        /// <param name="height">Height</param>
        /// <param name="FillBox">Whether to fill the box or not (by cropping)</param>
        /// <param name="cropIdentifier">Crop Identifier</param>
        public void AddImageSize(string title, int width, int height, bool FillBox, CS.General_20090518.Image.CROP_IDENTIFIER cropIdentifier)
        {
            this.Sizes.Add(new IMAGE_SIZE_INFO(title, width, height, FillBox, cropIdentifier));
        }
        protected void RemoveImageSize(string title)
        {
            IMAGE_SIZE_INFO size = getImageSize(title);
            this.Sizes.Remove(size);
        }
        protected void RemoveAllImageSizes()
        {
            this.Sizes.Clear();
        }
        protected string getOriginalFilename()
        {
            return Identifier + "." + OriginalExtension;
        }
        /// <summary>
        /// Returns the filename of the image
        /// </summary>
        protected string getFilename(IMAGE_SIZE_INFO imageSize)
        {
            return imageSize.Title + "_" + Identifier + "." + Extension;
        }
        /// <summary>
        /// Returns the filename of the image
        /// </summary>
        /// <param name="title">Title</param>
        /// <returns></returns>
        protected string getFilename(string title)
        {
            IMAGE_SIZE_INFO size = getImageSize(title);
            return getFilename(size);
        }
        /// <summary>
        /// Returns the relative path of the image
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        protected string getRelFilePath(string title)
        {
            try
            {
                IMAGE_SIZE_INFO size = getImageSize(title);
                if (size == null)
                    throw new InvalidOperationException("Photo:: Image Size with title '" + title + "' does not exist");
                return getRelImagePath(size);
            }
            catch (Exception ex)
            {
                
            }
            return null;
            
        }
        /// <summary>
        /// Returns the relative path of the image
        /// </summary>
        /// <param name="imageSize"></param>
        /// <returns></returns>
        protected string getRelFilePath(IMAGE_SIZE_INFO imageSize)
        {
            string folder = this.Folder;
            folder = folder.Replace("\\", "/");
            string path = Folder;
            path = CS.General_20090518.Page.getRoot() + folder + getFilename(imageSize);
            //throw new InvalidCastException("Path: " + path + "\r\nRoot: " + CS.General_20090518.Page.getRoot() + "\r\nFolder: " + Folder);
            return path;
        }

        /// <summary>
        /// Returns the path from the root, WITHOUT  a start '/'
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        protected string getRootFilePath(string title)
        {
            IMAGE_SIZE_INFO size = getImageSize(title);
            return getRootImagePath(size);
        }
        /// <summary>
        /// Returns the path from the root, with regard to web
        /// </summary>
        /// <param name="imageSize"></param>
        /// <returns></returns>
        protected string getRootFilePath(IMAGE_SIZE_INFO imageSize)
        {
            string folder = this.Folder;
            folder = folder.Replace("\\", "/");
            return folder + getFilename(imageSize);
        }

        /// <summary>
        /// Returns the local path
        /// </summary>
        /// <param name="imageSize"></param>
        /// <returns></returns>
        protected string getLocalFilePath(IMAGE_SIZE_INFO imageSize)
        {
            return BaseLocalFolder + Folder.Replace("/", "\\") + getFilename(imageSize);

        }
        /// <summary>
        /// Returns the local path
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        protected string getLocalFilePath(string title)
        {
            return getLocalImagePath(getImageSize(title));
        }
        /// <summary>
        /// Returns the local path of the original photo
        /// </summary>
        /// <returns></returns>
        protected string getLocalOriginalFilePath()
        {
            return BaseLocalFolder + OriginalFolder.Replace("/", "\\") + getOriginalFilename();
        }
        /// <summary>
        /// Returns the root path of the original photo
        /// </summary>
        /// <returns></returns>
        protected string getRootOriginalFilePath()
        {
            string folder = this.OriginalFolder;
            folder = folder.Replace("\\", "/");
            return folder + getOriginalFilename();
        }
        /// <summary>
        /// Returns the relative path of the original photo
        /// </summary>
        /// <returns></returns>
        protected string getRelOriginalFilePath()
        {
            string folder = this.OriginalFolder;
            folder = folder.Replace("\\", "/");
            string path = folder;
            path = CS.General_20090518.Page.getRoot() + folder + getOriginalFilename();
            return path;

        }
        private void checkRequiredVars()
        {
            StringBuilder sb = new StringBuilder();
            if (string.IsNullOrEmpty(this.Folder))
            {
                sb.AppendLine("CS.General_20090518.Classes.MediaItem:: Please initialise 'Folder' variable");

            }
            if (string.IsNullOrEmpty(this.Identifier))
            {
                sb.AppendLine("CS.General_20090518.Classes.MediaItem:: Please initialise 'Identifier' variable");
            }
            string s = sb.ToString();
            if (!string.IsNullOrEmpty(s))
            {
                throw new InvalidOperationException(s);
            }


        }
        /// <summary>
        /// Sets the image of this photo
        /// </summary>
        /// <param name="txt"></param>
        public virtual void SetMediaItem(Stream s, bool originalAlreadyExists)
        {
            checkRequiredVars();
            UpdateIdentifier();
            if (s != null && s.Length > 0)
            {
                RemoveFiles();
                CS.General_20090518.IO.CreateDirectory(getLocalOriginalPath());
                if (!originalAlreadyExists)
                {
                    FileStream fs = new FileStream(getLocalOriginalPath(), FileMode.Create, FileAccess.Write);
                    BinaryReader br = new BinaryReader(s);
                    BinaryWriter bw = new BinaryWriter(fs);
                    s.Position = 0;

                    byte[] buffer = new byte[32768];
                    int cnt = 0;
                    int index = 0;

                    do
                    {
                        cnt = br.Read(buffer, index, buffer.Length);
                        if (cnt > 0)
                        {
                            bw.Write(buffer, 0, cnt);
                        }
                    } while (cnt == buffer.Length);

                    bw.Close(); br.Close(); fs.Close();
                    fs = null; br = null; bw = null;
                    
                    
                }
                s.Close();
                checkImages();
                
            }
        }
        /// <summary>
        /// Sets the image of this photo
        /// </summary>
        /// <param name="txt"></param>
        public virtual void SetMediaItem(Stream s)
        {
            SetMediaItem(s, false);
        }

        /// <summary>
        /// Sets the image of this photo
        /// </summary>
        /// <param name="txt"></param>
        public virtual void SetMediaItem(CS.General_20090518.Controls.WebControls.Common.MyFileUpload txt, bool originalAlreadyExists)
        {
            if (txt != null)
            {
                //txt.SaveAs(@"D:\Work\Clients\preferredparties.com\deploy\uploads\photos\test.jpg");
                SetMediaItem(txt.FileContent, originalAlreadyExists);

            }
        }
        /// <summary>
        /// Sets the image of this photo
        /// </summary>
        /// <param name="txt"></param>
        public virtual void SetMediaItem(CS.General_20090518.Controls.WebControls.Common.MyFileUpload txt)
        {
            SetMediaItem(txt, false);
        }
        #endregion




        private System.Web.SessionState.HttpSessionState Session
        {
            get
            {
                return System.Web.HttpContext.Current.Session;
            }
        }
        private System.Web.HttpRequest Request
        {
            get
            {
                return System.Web.HttpContext.Current.Request;
            }
        }


        public void checkImages()
        {

            CS.General_20090518.IO.CreateDirectory(getLocalOriginalPath());
            
            string origPath = getLocalOriginalPath();
            if (Identifier != "" && Identifier != null && Identifier != Int32.MinValue.ToString())
            {

                if (File.Exists(origPath))
                {
                    for (int i = 0; i < Sizes.Count; i++)
                    {
                        IMAGE_SIZE_INFO size = Sizes[i];


                        string path = getLocalImagePath(size);
                        CS.General_20090518.IO.CreateDirectory(path);
                        if (!File.Exists(path))
                        {
                            if (!size.FillBox)
                            {
                                CS.General_20090518.Image.ResizeImage(origPath, size.Width, size.Height, path);
                            }
                            else if (size.FillBox && size.CropIdentifier == Image.CROP_IDENTIFIER.None)
                            {
                                CS.General_20090518.Image.resizeImageBiggest(origPath, size.Width, size.Height, path);
                            }
                            else
                            {
                                CS.General_20090518.Image.ResizeAndCropImage(origPath, size.Width, size.Height, path, size.CropIdentifier);
                            }
                        }
                        ;
                    }
                }
            }
        }


        private void CheckFolder()
        {
            string path = CS.General_20090518.Page.MapPath(CS.General_20090518.Page.getRoot() + Folder);
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
        }

        protected void RemoveFiles()
        {
            for (int i = 0; i < Sizes.Count; i++)
            {
                IMAGE_SIZE_INFO size = Sizes[i];
                string path = getLocalImagePath(size);
                try
                {
                    if (File.Exists(path))
                    {
                        File.Delete(path);
                    }
                }
                catch
                {

                }
            }
            string origPath = getLocalOriginalPath();
            if (File.Exists(origPath))
            {
                try
                {
                    File.Delete(origPath);
                }
                catch
                { }

            }
        }
        public MediaItem()
        {

        }
        public MediaItem(string folder, string identifier, string extension)
        {
            this.Folder = folder;
            this.Identifier = identifier;
            this.Extension = extension;
        }
        public MediaItem(string folder, string identifier, string extension, Stream s, int origMaxWidth, int origMaxHeight)
        {
            this.Folder = folder;
            this.Extension = extension;
            this.Identifier = identifier;
            this.OriginalMaxHeight = origMaxHeight;
            this.OriginalMaxWidth = origMaxWidth;
            this.SetMediaItem(s);
        }
        public MediaItem(string folder, string identifier, string extension, CS.General_20090518.Controls.WebControls.Common.MyFileUpload txt, int origMaxWidth, int origMaxHeight)
            : this(folder, identifier, extension, txt.FileContent, origMaxWidth, origMaxHeight)
        {

        }
        public MediaItem(string folder, string identifier, string extension, CS.General_20090518.Controls.WebControls.Common.MyFileUpload txt)
            : this(folder, identifier, extension, txt, Int32.MinValue, Int32.MinValue)
        {

        }

        public virtual void Remove()
        {
            RemoveFiles();
        }
        public virtual void UpdateIdentifier()
        {
            
        }
        
    }
}
