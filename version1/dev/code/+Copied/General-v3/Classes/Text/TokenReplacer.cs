﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Reflection;

namespace CS.General_v3.Classes.Text
{
    public class TokenReplacer : ITokenReplacer
    {

        public class TokenToRemove
        {
            public string Token { get; set; }
            public bool RemoveContent { get; set; }
            public TokenToRemove(string token, bool removeContent)
            {
                this.Token = token;
                this.RemoveContent = removeContent;
            }
        }


        protected void fillDefaultTags(string preTag = null)
        {
            this["[" + preTag + "WebsiteName]"] = Enums.SETTINGS_ENUM.Others_WebsiteName.GetSettingFromDatabase(); ;
           // this["[" + preTag + "Facebook]"] = Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Facebook.GetSettingFromDatabase();
            this["[" + preTag + "BaseWebsiteUrl]"] = Enums.SETTINGS_ENUM.Others_WebsiteBaseUrl.GetSettingFromDatabase();
            
        }

        
        public virtual string GetToken(string key)
        {
            return nvReplace[key];
            
        }


        public static TokenReplacer CreateDefault(string preTag = null)
        {

            TokenReplacer tr = null;
            
            if (tr == null)
                tr = TokenReplacer.CreateBlank();
            tr.fillDefaultTags(preTag);
            return tr;
            
            
        }
        public static TokenReplacer CreateBlank()
        {
            return new TokenReplacer();
            
        }


        protected NameValueCollection nvReplace = null;
        private Dictionary<string,TokenToRemove> nvTagsToRemove = null;


        public virtual void SetToken(string key, string value)
        {
            nvReplace[key] = value;
            
        }


        public void Remove(params string[] keys)
        {
            for (int i = 0; i < keys.Length; i++)
            {
                nvReplace.Remove(keys[i]);
            }
        }

        public string this[string key]
        {
            get
            {
                return GetToken(key);
            }
            set
            {
                nvReplace[key] = value;        
                
                
            }

        }
       



        public string this[int key]
        {
            get { return nvReplace[key]; }

        }


        protected TokenReplacer()
        {
            nvReplace = new NameValueCollection();
            this.nvTagsToRemove = new Dictionary<string, TokenToRemove>();
        }
        public void AddTagToRemove(string tagName, bool removeContent)
        {
            if (tagName.StartsWith("{")) tagName = tagName.Substring(1);
            if (tagName.EndsWith("}")) tagName = CS.General_v3.Util.Text.RemoveLastNumberOfCharactersFromString(tagName,1);
            this.nvTagsToRemove[tagName.ToLower()] = new TokenToRemove(tagName, removeContent);


        }

        public string ReplaceString(string s)
        {
            return replaceInString(s);
        }

        public void ReplaceInString(ref string str1)
        {
            string str2 = "";
            ReplaceInString(false, ref str1, ref str2);
        }
        public void ReplaceInString(ref string str1, ref string str2)
        {
            string str3 = "";
            ReplaceInString(false, ref str1, ref str2, ref str3);
        }
        public void ReplaceInString(ref string str1, ref string str2, ref string str3)
        {
            string str4 = "";
            ReplaceInString(false, ref str1, ref str2, ref str3, ref str4);
        }
        public void ReplaceInString(ref string str1, ref string str2, ref string str3, ref string str4)
        {
            ReplaceInString(false, ref str1, ref str2, ref str3, ref str4);

        }
        /// <summary>
        /// /////////
        /// </summary>
        /// <param name="str1"></param>

        public void ReplaceInString(bool replaceForHTML, ref string str1)
        {
            string str2 = "";
            ReplaceInString(replaceForHTML, ref str1, ref str2);
        }
        public void ReplaceInString(bool replaceForHTML, ref string str1, ref string str2)
        {
            string str3 = "";
            ReplaceInString(replaceForHTML, ref str1, ref str2, ref str3);
        }
        public void ReplaceInString(bool replaceForHTML, ref string str1, ref string str2, ref string str3)
        {
            string str4 = "";
            ReplaceInString(replaceForHTML, ref str1, ref str2, ref str3, ref str4);
        }
        public void ReplaceInString(bool replaceForHTML, ref string str1, ref string str2, ref string str3, ref string str4)
        {

            str1 = replaceInString(str1, replaceForHTML);
            str2 = replaceInString(str2, replaceForHTML);
            str3 = replaceInString(str3, replaceForHTML);
            str4 = replaceInString(str4, replaceForHTML);

        }
        protected virtual void preReplaceToken(string token)
        {

        }

        private string replaceInString(string s, bool ReplaceForHTML = false)
        {
            if (s != null)
            {
                for (int i = 0; i < nvReplace.Count; i++)
                {
                    string key = nvReplace.AllKeys[i];
                    string val = nvReplace[i] ?? "";
                    string tag = CS.General_v3.Util.Text.ForRegExp(key);
                    preReplaceToken(key);
                    if (ReplaceForHTML)
                        val = CS.General_v3.Util.Text.ConvertPlainTextToHTML(val);
                    s = Regex.Replace(s, tag, val, RegexOptions.IgnoreCase);
                    
                }

                foreach (var value in nvTagsToRemove.Values)
                {
                    
                    s = CS.General_v3.Util.Text.RemoveTagsFromContent(s, "{" + value.Token + "}","{/" + value.Token+ "}", value.RemoveContent);
                }
            }
            return s;
        }


        #region ITokenReplacer Members


        object ITokenReplacer.this[string key]
        {
            get
            {
                return this[key];
                
            }
            set
            {
                this[key] = (value != null ? value.ToString() : "");
                
            }
        }

        #endregion
    }
}
