namespace CS.General_v3.Classes.Text
{
    public interface ITokenReplacer
    {
        string ReplaceString(string s);
        object this[string key] { get; set; }

        //void ReplaceInString(ref string str1);
        //void ReplaceInString(ref string str1, ref string str2);
        //void ReplaceInString(ref string str1, ref string str2, ref string str3);
        //void ReplaceInString(ref string str1, ref string str2, ref string str3, ref string str4);

        
    }
}