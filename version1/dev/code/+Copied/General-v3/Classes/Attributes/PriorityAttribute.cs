﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Attributes
{
    public class PriorityAttribute : Attribute
    {

        public PriorityAttribute(int priority)
        {
            this.Priority = priority;
        }

        public int Priority { get; set; }


    }
}
