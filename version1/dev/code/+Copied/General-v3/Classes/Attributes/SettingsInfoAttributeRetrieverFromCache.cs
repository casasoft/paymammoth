﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Attributes
{
    using System.Collections.Concurrent;

    public class SettingsInfoAttributeRetrieverFromCache
    {
        private ConcurrentDictionary<string, SettingsInfoAttribute> cache = null;

        private SettingsInfoAttributeRetrieverFromCache()
        {
            cache = new ConcurrentDictionary<string, SettingsInfoAttribute>();
        }
        public static SettingsInfoAttributeRetrieverFromCache Instance
        {
            get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<SettingsInfoAttributeRetrieverFromCache>(); }

        }
        public SettingsInfoAttribute GetSettingsInfo(Enum value)
        {
            SettingsInfoAttribute attrib = null;
            string sID = CS.General_v3.Util.EnumUtils.StringValueOf(value);
            if (!cache.ContainsKey(sID))
            {
                attrib = SettingsInfoAttribute._getForEnum(value);
                cache[sID] = attrib;
            }
            return cache[sID];
        }
    }
}
