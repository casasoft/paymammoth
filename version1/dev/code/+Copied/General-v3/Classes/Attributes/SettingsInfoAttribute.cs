﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Attributes
{
    public class SettingsInfoAttribute : Attribute
    {
        internal static SettingsInfoAttribute _getForEnum(Enum identifier)
        {
            return CS.General_v3.Util.EnumUtils.GetAttributeFromEnumValue<SettingsInfoAttribute>(identifier);
        }
        public static SettingsInfoAttribute GetForEnum(Enum identifier)
        {
            return SettingsInfoAttributeRetrieverFromCache.Instance.GetSettingsInfo(identifier);
            /*
            SettingsInfoAttribute a = null;
            lock (_lock)
            {
                CS.General_v3.Util.Stopwatch.StartTimer(100);
                a = CS.General_v3.Util.EnumUtils.GetAttributeFromEnumValue<SettingsInfoAttribute>(identifier);
                CS.General_v3.Util.Stopwatch.EndTimer(100);
            }
            return a;*/
        }
        private static readonly object _lock = new object();

        public string AlternateIdentifier { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Enums.CMS_ACCESS_TYPE CmsAccessRequired { get; set; }
        public Enum LocalhostIdentifier { get; set; }
        public object DefaultValue { get; set; }
        public object DefaultLocalhostValue { get; set; }
        public int Priority { get; set; }
        public Enums.DATA_TYPE? DataType { get; set; }
        public Type EnumType { get; set; }
        public SettingsInfoAttribute()
        {
            CmsAccessRequired = Enums.CMS_ACCESS_TYPE.Normal;
        }
        public SettingsInfoAttribute(Enums.CMS_ACCESS_TYPE cmsAccessRequired = Enums.CMS_ACCESS_TYPE.CasaSoft , object defaultValue = null)
        {
            this.CmsAccessRequired = cmsAccessRequired;
            this.DefaultValue = DefaultValue;
        }

        public string GetEnumTypeIfApplicable()
        {
            if (this.EnumType != null)
                return this.EnumType.AssemblyQualifiedName;
            else
            {
                string s = null;
                if (GetDataType() == Enums.DATA_TYPE.Enumeration)
                {
                    s = this.DefaultValue.GetType().AssemblyQualifiedName;
                }
                return s;
            }

        }

        public Enums.DATA_TYPE GetDataType()
        {
            
            if (DataType.HasValue)
                return DataType.Value;
            else
                return CS.General_v3.Enums.GetDataTypeFromObject(this.DefaultValue, Enums.DATA_TYPE.String);
        }
        public string GetDefaultValueAsString()
        {
            return CS.General_v3.Util.Other.ConvertBasicDataTypeToString(DefaultValue, ifEnumConvertToIntValue: false);
        }
        public string GetDefaultLocalhostValueAsString()
        {
            return CS.General_v3.Util.Other.ConvertBasicDataTypeToString(DefaultLocalhostValue);
        }

        public string GetTitle(Enum identifier)
        {
            string s = this.Title;
            if (string.IsNullOrEmpty(s))
                s = CS.General_v3.Util.EnumUtils.StringValueOf(identifier, true);
            return s;
            
        }

    }
}
