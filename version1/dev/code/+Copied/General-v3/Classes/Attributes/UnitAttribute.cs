﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Attributes
{

    public delegate double ConversionToBaseValueHandler(double value);

    public class UnitAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="isBaseValue">Whether item is the base value, i.e. ratio = 1</param>
        /// <param name="ratioToConvertValueToBase">Ratio to conver to base, e.g. if base is in KG, then 1g = 0.001kg</param>
        /// <param name="customConversionFunction">Certain conversions are not simple so you need a custom function.  Specify the function to convert from a value of this item to its base value</param>
        public UnitAttribute(double ratioToConvertValueToBase, string unitText, bool isBaseValue = false)
        {
            if (this.Ratio == 1) isBaseValue = true;

            this.IsBaseValue = isBaseValue;
            this.Ratio = IsBaseValue ? 1 : ratioToConvertValueToBase;
            this.UnitText = unitText;
        }
        public string UnitText { get; set; }
        public bool IsBaseValue { get; set; }

        public double Ratio { get; set; }



    }
}
