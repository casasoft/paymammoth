﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace CS.General_v3.Classes.MyList
{
    public interface IMyEnumerable<T> : IEnumerable<T>
    {
        T FindElem(Predicate<T> p);
        void RandomizeList();
        object FindElemObj(Predicate<object> p);
        List<T> GetXRandomElements(int totalElements);
        List<T> BinarySearchAll(T itemToFind, Comparison<T> p);
        int BinarySearch(T itemToFind, Comparison<T> p);
        void RemoveNullObjectsFromList();
        MyList<T> FindAll(Predicate<T> predicate);
        List<T> GetAsStandardList();
        List<T> GetAsStandardList(bool createANewList);
        bool DoNotAllowDuplicates { get; set; }
        event MyList<T>.OnChangeItemHandler OnAddItem_After;
        event MyList<T>.OnChangeItemHandlerWithAcknowledgment OnAddItem_Before;
        event MyList<T>.OnChangeItemHandlerWithAcknowledgment OnRemoveItem_Before;
        event MyList<T>.OnChangeItemHandler OnRemoveItem_After;

    }
}
