﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace CS.General_v3.Classes.MyList
{
    public class MyList<T> : IMyList<T>
    {
        

        public override string ToString()
        {
            return "Count: " + this.Count;

            
        }
        /// <summary>
        /// Creates a new list
        /// </summary>
        /// <param name="doNotAllowDuplicates">Whether to allow duplicates or not.  TRUE means no duplicates, FALSE vice-versa</param>
        public MyList(bool doNotAllowDuplicates)
        {
            this._sourceList = new List<T>();
            this.DoNotAllowDuplicates = doNotAllowDuplicates;
        }
        /// <summary>
        /// Creates a new list
        /// </summary>
        public MyList() : this (false)
        {
            
        }
        protected IList<T> _sourceList = null;
        
        /// <summary>
        /// A handler triggered whenever an item is added to a list.  
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="item"></param>
        /// <returns>Whether to proceed with the change or not</returns>
        public delegate bool OnChangeItemHandlerWithAcknowledgment(MyList<T> sender, T item);
        public delegate void OnChangeItemHandler(MyList<T> sender, T item);
        /// <summary>
        /// Called before adding any item.  If returns 'false', the item is not added
        /// </summary>
        public event OnChangeItemHandlerWithAcknowledgment OnAddItem_Before;
        /// <summary>
        /// Called before removeing any item.  If returns 'false', item is not removed
        /// </summary>
        public event OnChangeItemHandlerWithAcknowledgment OnRemoveItem_Before;

        private event OnChangeItemHandler _OnAddItem_After;
        /// <summary>
        /// 
        /// Called after adding an item
        /// </summary>
        public event OnChangeItemHandler OnAddItem_After
        {
            add
            {
                _OnAddItem_After += value;
            }
            remove
            {
                _OnAddItem_After -= value;
            }
        }
        private event OnChangeItemHandler _OnRemoveItem_After;
        /// <summary>
        /// Called after removeing an item
        /// </summary>
        public event OnChangeItemHandler OnRemoveItem_After
        {
            add
            {
                _OnRemoveItem_After += value;
            }
            remove
            {
                _OnRemoveItem_After -= value;
            }
        }
        
        private bool checkAddItem(T item)
        {
            
            bool ok = true;
            if (DoNotAllowDuplicates && item != null)
            {
                ok = !this.Contains(item);
                
            }
            if (ok && item != null && OnAddItem_Before != null)
                ok = OnAddItem_Before(this, item);
            return ok;
        }
        private void callAddItem_After(T item)
        {

            if (item != null && _OnAddItem_After != null)
                _OnAddItem_After(this, item);
            
        }
        private bool checkRemoveItem(T item)
        {
            bool ok = true;
            if (item != null && OnRemoveItem_Before!= null)
                ok = OnRemoveItem_Before(this, item);
            return ok;
        }
        private void callRemoveItem_After(T item)
        {
            if (item != null && _OnRemoveItem_After != null)
                _OnRemoveItem_After(this, item);
        }
        public  virtual void AddRange(IEnumerable<T> collection)
        {
            var enumerator = collection.GetEnumerator();
            while (enumerator.MoveNext())
            {
                var item = enumerator.Current;
                _Add(item);
            };

        }


        public virtual bool Exists(Predicate<T> item)
        {
            for (int i=0; i < _sourceList.Count;i++)
            {
                if (item(_sourceList[i]))
                    return true;
            }
            return false;
        }
        

        /// <summary>
        /// Sorts the elements in the entire System.Collections.Generic.List<T> using  the default comparer.
        /// </summary>
        public virtual void Sort()
        {
                //Comparison<T> cmp = new Comparison<T>((x,y)=>((IComparable<T>)x).CompareTo(y));
            this.Sort(((x,y)=>((IComparable<T>)x).CompareTo(y)));
            
            
        }
        
        /// <summary>
        /// Sorts the elements in the entire System.Collections.Generic.List<T> using  the specified System.Comparison<T>.
        /// </summary>
        /// <param name="comparer">The System.Comparison<T> to use when comparing elements.</param>
        public virtual void Sort(Comparison<T> comparer, int index = 0, int? count = null)
        {
           
            CS.General_v3.Util.SortingUtil.QuickSort(this._sourceList, comparer, index: index, count: count);
            //this._sourceList.Sort(comparer);
        }

        
        /*
        public new virtual void Add(T item)
        {
            bool ok = true;
            if (OnAddItem != null)
                ok = OnAddItem(this, item);
            if (ok)
                base.Add(item);
        }
        
        public new virtual void Remove(T item)
        {
            bool ok = true;
            if (OnRemoveItem != null)
                ok = OnRemoveItem(this, item);
            if (ok)
            {
                base.Remove(item);
            }
        }
        public new virtual void RemoveAt(int index)
        {
            var item = this[index];
            Remove(item);
        }
        */
        

        #region IList<T> Members

        public virtual int IndexOf(T item)
        {
            
            return _sourceList.IndexOf(item);
        }
        private void _Insert(int index, T item)
        {
            bool ok = checkAddItem(item);
            if (ok)
            {
                _sourceList.Insert(index, item);
                callAddItem_After(item);
            }
        }
        public virtual void Insert(int index, T item)
        {
            _Insert(index, item);
            
        }

        public T this[int index]
        {
            get
            {
                return _sourceList[index];
            }
            set
            {
                _sourceList[index] = value;
            }
        }

        #endregion

        #region ICollection<T> Members


        public virtual void Clear()
        {
            
            if (OnRemoveItem_Before != null || _OnRemoveItem_After != null)
            {
                int index = 0;
                while (index < this._sourceList.Count)
                {
                    var item = this._sourceList[index];
                    bool ok = checkRemoveItem(item);
                    if (ok)
                    {
                        this._sourceList.RemoveAt(index);
                        callRemoveItem_After(item);
                        //if ok, dont increment index as the list will have been removed, thus index will be the next item
                    }
                    else
                    {
                        index++;
                    }

                }
            }
            else
            {
                _sourceList.Clear();
            }
        }

        public virtual bool Contains(T item)
        {
            return _sourceList.Contains(item);
        }

        public virtual void CopyTo(T[] array, int arrayIndex)
        {
            _sourceList.CopyTo(array, arrayIndex);
        }

        public virtual int Count
        {
            get { return _sourceList.Count; }
        }

        public virtual bool IsReadOnly
        {
            get { return false; }
        }
        private bool _Remove(T item)
        {
            bool result = false;
            bool ok = checkRemoveItem(item);
            if (ok)
            {
                result = _sourceList.Remove(item);
                if (item != null)
                    callRemoveItem_After(item);
            }
            return result;
        }
        bool ICollection<T>.Remove(T item)
        {
            return _Remove(item);
        }

        #endregion

        #region IEnumerable<T> Members

        public virtual IEnumerator<T> GetEnumerator()
        {
            return _sourceList.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _sourceList.GetEnumerator();
        }

        #endregion

        #region IList<T> Members


        public virtual void RemoveAt(int index)
        {
           this.Remove(_sourceList[index]);
            
        }

        #endregion

        #region ICollection<T> Members

        private int _Add(T item)
        {
            int result = -1;
            bool ok = checkAddItem(item);
            if (ok)
            {

                result = ((IList)_sourceList).Add(item);
                if (item != null)
                    callAddItem_After(item);
            }
            return result;
        }
        public virtual int Add(T item)
        {
            return _Add(item);   
        }

        #endregion

        #region IList Members

        int IList.Add(object value)
        {
           
            return this.Add((T)value);   
        }

        bool IList.Contains(object value)
        {
            return this.Contains((T)value);
        }

        int IList.IndexOf(object value)
        {
            return this.IndexOf((T)value);
        }

        void IList.Insert(int index, object value)
        {
            this.Insert(index, (T)value);
        }

        public virtual bool IsFixedSize
        {
            get { return false; }
        }

        public virtual bool Remove(T value)
        {
            return _Remove(value);
        }
        void IList.Remove(object value)
        {
            _Remove((T)value);
        }

        object IList.this[int index]
        {
            get
            {
                return this._sourceList[index];
            }
            set
            {
                this._sourceList[index] = (T)value;
            }
        }

        #endregion

        #region ICollection Members

        public virtual void CopyTo(Array array, int index)
        {
            _sourceList.ToArray().CopyTo(array, index);
            
        }

        public virtual bool IsSynchronized
        {
            get { return false; }
        }

        public virtual object SyncRoot
        {
            get { return null;  }
        }

        #endregion
        

        /// <summary>
        /// Removes the all the elements that match the conditions defined by the specified predicate.
        /// </summary>
        /// <param name="match"> The System.Predicate<T> delegate that defines the conditions of the elements to remove.</param>
        public void RemoveAll(Predicate<T> match)
        {
            int index = 0;
            while (index < this.Count)
            {
                var item = this[index];
                if (match(item))
                {
                    bool ok = checkRemoveItem(item);
                    if (ok)
                    {
                        
                        _sourceList.Remove(item);
                        callRemoveItem_After(item);
                        index--;
                    }
                }
                index++;
            }
        }

        

        
        /*
        public void RemoveDuplicates<T>(Comparison<T> p)
        {
            var list = this.list;

            this.Sort(p);
            for (int i = 0; i < list.Count - 1; i++)
            {
                T item1 = this.list[i];
                T item2 = this.list[i + 1];
                if (p(item1, item2) == 0)
                {
                    list.RemoveAt(i + 1);
                    i--;
                }
            }
        }

        */

        public MyList<TOutput> ConvertAll<TOutput>(Converter<T,TOutput> converter)
        {
            List<object> l = new List<object>();
            

            MyList<TOutput> convList = new MyList<TOutput>();

            List<TOutput> convBaseList = CS.General_v3.Util.ListUtil.ConvertAll<T,TOutput>(_sourceList, converter);
            convList.AddRange(convBaseList);
            return convList;

        }

        


        
        /*
        public virtual void AddRange(IEnumerable<T> list)
        {
            var enumerator = list.GetEnumerator();
            while (enumerator.MoveNext())
            {
                T item = (T)enumerator.Current;
                _Add(item);
            }
        }*/
        
        /// <summary>
        /// Removes a range of elements from the System.Collections.Generic.List<T>.
        /// </summary>
        /// <param name="index">The zero-based starting index of the range of elements to remove.</param>
        /// <param name="count">The number of elements to remove.</param>
        public void RemoveRange(int index, int count)
        {
            int i = 0;
            while (i < count)
            {
                this._sourceList.RemoveAt(index);
                i++;
            }
            //this._sourceList.RemoveRange(index, count);
        }

        public T GetRandomElement()
        {
            T rndElem = default(T);
            if (this.Count > 0)
            {
                int rndIndex = CS.General_v3.Util.Random.GetInt(0, this.Count - 1);
                rndElem = this[rndIndex];
            }
            return rndElem;
        }

        #region MyList specific
        public List<T> BinarySearchAll(T itemToFind, Comparison<T> p)
        {
            var list = this;
            int index = list.BinarySearch(itemToFind, p);
            int startIndex = index, endIndex = index;
            List<T> result = new List<T>();
            if (index > -1)
            {
                for (int i = index; i >= 0; i--)
                {
                    if (p(itemToFind, list[i]) == 0)
                    {
                        result.Insert(0, list[i]);
                    }
                }
                for (int i = index + 1; i < list.Count; i++)
                {
                    if (p(itemToFind, list[i]) == 0)
                    {
                        result.Add(list[i]);
                    }
                }
            }
            return result;
        }
        public T FindElem(Predicate<T> p)
        {
            var list = this;
            T result = default(T);
            if (list != null)
            {
                IEnumerator<T> enumerator = (IEnumerator<T>)list.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    if (p(enumerator.Current))
                    {
                        result = enumerator.Current;
                        break;
                    }
                }
            }
            return result;
        }
        public void RandomizeList()
        {
            var list = this;
            List<T> tmpList = new List<T>();
            tmpList.AddRange((IEnumerable<T>)list);
            list.Clear();
            while (tmpList.Count > 0)
            {
                int rndIndex = CS.General_v3.Util.Random.GetInt(0, tmpList.Count - 1);
                list.Add(tmpList[rndIndex]);
                tmpList.RemoveAt(rndIndex);
            }



        }
        public object FindElemObj(Predicate<object> p)
        {

            var list = this;
            
            IEnumerator enumerator = list.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (p(enumerator.Current))
                {
                    return enumerator.Current;
                }
            }
            return null;
        }
        /// <summary>
        /// Gets X random elements from the list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="totalElements"></param>
        /// <returns></returns>
        public List<T> GetXRandomElements(int totalElements)
        {
            var list = this;
            List<T> currList = new List<T>();
            currList.AddRange((IEnumerable<T>)list);
            List<T> result = new List<T>();
            int cnt = 0;
            while (cnt < totalElements && currList.Count > 0)
            {
                int rndIndex = CS.General_v3.Util.Random.GetInt(0, currList.Count - 1);
                result.Add(currList[rndIndex]);
                currList.RemoveAt(rndIndex);
                cnt++;
            }
            return result;
        }
        private class __comparer<TItem> : IComparer<TItem>
        {
            private Comparison<TItem> comparer = null;
            public __comparer(Comparison<TItem> p)
            {
                this.comparer = p;
            }
            public int Compare(TItem x, TItem y)
            {
                return comparer(x, y);
            }
        }

        public int BinarySearch(T itemToFind, Comparison<T> p)
        {
            var list = this;
            //T result = default(T);
            return  CS.General_v3.Util.ListUtil.BinarySearch<T>(_sourceList, itemToFind, p);
            //int index = this._sourceList.BinarySearch(itemToFind, new __comparer<T>(p));
            //return index;

        }
        public virtual void RemoveNullObjectsFromList()
        {

            for (int i = 0; i < this.Count; i++)
            {
                if (this[i] == null)
                {
                    this.RemoveAt(i);
                    i--;
                }
            }
        }
        public virtual MyList<T> FindAll(Predicate<T> predicate)
        {
            
            MyList<T> list = new MyList<T>();
            list.AddRange(CS.General_v3.Util.ListUtil.FindAll<T>(this, predicate));
            return list;
        }
        public List<T> GetAsStandardList() 
        {
            return this.GetAsStandardList(false);
        }
        public List<T> GetAsStandardList(bool createANewList)
        {
            List<T> list = null;
            if (!createANewList && this._sourceList is List<T>)
                list = (List<T>)this._sourceList;
            if (list == null)
            {
                list = new List<T>();
                list.AddRange(this._sourceList);
            }
            return list;
        }
        public bool DoNotAllowDuplicates { get; set; }
        public void __setSourceList(IList<T> list)
        {
            _sourceList = list;
        }
        public IList<T> __getSourceList()
        {
            return _sourceList;
        }
        #endregion

        void ICollection<T>.Add(T item)
        {
            this.Add(item);
            
        }
    }
}
