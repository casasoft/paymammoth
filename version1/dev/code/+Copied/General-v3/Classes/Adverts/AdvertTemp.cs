﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Adverts
{
    public class AdvertTemp : Advert
    {

        public override int ID
        {
            get;
            set;
        }

        public override string Title
        {
            get;
            set;
        }

        public override string Link
        {
            get;
            set;
        }
        public override string ItemURL {get;set;}

        public override int AdvertShownPerRound
        {
            get;
            set;
        }

        public override double AdvertRatio
        {
            get;
            set;
        }

        public override void Save()
        {
            
        }

        public override int RoundCounter
        {
            get;
            set;
        }
        public override int Width {get;set;}

        public override int Height{get;set;}
    }
}
