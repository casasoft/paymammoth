﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;

namespace CS.General_v3.Classes.Adverts
{
    public abstract class Advert : IAdvert2
    {
         
         
         public virtual bool IsRoundReady()
         {
             return this.RoundCounter == this.AdvertShownPerRound;
         }
         public virtual void ResetCounter(bool autoSave)
         {
             this.RoundCounter = 0;
             if (autoSave)
                 this.Save();
             

         }
         public virtual void AddHit(bool autoSave)
         {
             this.RoundCounter++;
             if (autoSave)
                 this.Save();
         }

         #region IAdvert Members

         public abstract int ID { get; set; }
         public abstract int Width { get; set; }
         public abstract int Height { get; set; }
         public abstract string Title { get; set; }
         public abstract string Link { get; set; }
         public abstract string ItemURL { get; set; }
         public abstract int AdvertShownPerRound { get; set; }
         public abstract double AdvertRatio { get; set; }
         public abstract void Save();

         public abstract int RoundCounter { get; set; }

         #endregion
         public virtual string GetGoogleAnalyticsCodeForImpression(string trackerVarName, string Category, bool synchronousCode = true)
         {
             Category = CS.General_v3.Util.Text.forJS(Category);
             string Title = CS.General_v3.Util.Text.forJS(this.Title);
             /*string s = trackerVarName + "._trackEvent('" +  + "', '" + + " [" + this.ID + "]" + "'" +
                 ", 'Impression');\r\n";*/

             if (synchronousCode)
                 return CS.General_v3.JavaScript.GoogleAnalytics.APIUtil.GetTrackEventJS_Sync(Category, Title + " [" + this.ID + "]", "Impression", null, trackerVarName);
             else
                 return CS.General_v3.JavaScript.GoogleAnalytics.APIUtil.GetTrackEventJS_ASync(Category, Title + " [" + this.ID + "]", "Impression", null, trackerVarName);

             //return s;
         }
         public virtual string GetGoogleAnalyticsCodeForHit(string trackerVarName, string Category, bool synchronousCode = true)
         {
             //return "alert(_gaq);";
             Category = CS.General_v3.Util.Text.forJS(Category);
             string Title = CS.General_v3.Util.Text.forJS(this.Title);
             /*string s = trackerVarName + "._trackEvent('" +  + "', '" + + " [" + this.ID + "]" + "'" +
                 ", 'Impression');\r\n";*/

             if (synchronousCode)
                 return CS.General_v3.JavaScript.GoogleAnalytics.APIUtil.GetTrackEventJS_Sync(Category, Title + " [" + this.ID + "]", "Hit", null, trackerVarName);
             else
                 return CS.General_v3.JavaScript.GoogleAnalytics.APIUtil.GetTrackEventJS_ASync(Category, Title + " [" + this.ID + "]", "Hit", null, trackerVarName);

             /*
             string s = trackerVarName + "._trackEvent('" + CS.General_v3.Util.Text.forJS(Category) + "', '" + CS.General_v3.Util.Text.forJS(this.Title) + " [" + this.ID + "]"+ "'" +
                 ", 'Hit');\r\n";
             return s;*/
         }
         public virtual void AttachGoogleAnalyticsCodeForImpressionToPage(bool synchronous, string trackerVarName, string Category,System.Web.UI.Page pg)
         {
             string js = GetGoogleAnalyticsCodeForImpression(trackerVarName, Category, synchronous);
             pg.ClientScript.RegisterStartupScript(this.GetType(), "advert_page_" + this.ID, js, true);
         }
         public virtual void AttachGoogleAnalyticsCodeToControl(bool synchronous, string trackerVarName, string Category, System.Web.UI.WebControls.WebControl ctrl)
         {
             ctrl.Attributes["onclick"] += GetGoogleAnalyticsCodeForHit(trackerVarName, Category, synchronous);
             
         }

         public JSObject GetJsObject(bool forFlash = false)
         {
             JSObject obj = forFlash ? new JSObjectFlash() : new JSObject();
             obj.AddProperty("id", ID);
             obj.AddProperty("title", Title);
             obj.AddProperty("link", Link);
             obj.AddProperty("itemURL", ItemURL);
             obj.AddProperty("width", Width);
             obj.AddProperty("height",Height);
             return obj;
         }
    }
}
