﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Adverts
{
    public abstract class AdvertSlot : IAdvertSlot
    {
        public virtual void UpdateAdvertRatios()
        {
            var adverts = this.GetAdvertsListToShow();
            //double totalRatio = 0;
            //get the total ratio count
            double[] tmpAdRatio = new double[adverts.Count];
            for (int i = 0; i < adverts.Count; i++)
            {
                var adv = adverts[i];
                
                //totalRatio += adv.AdvertRatio;
                tmpAdRatio[i] = adv.AdvertRatio;
            }
            
            //find the actual ratio per advert
            /*
            for (int i = 0; i < tmpAdRatio.Length; i++)
            {
                var adv = this.Adverts[i];
                tmpAdRatio[i] = adv.AdvertRatio / totalRatio;
            }/
             * */
            bool ok = true;
            //multiply ratios until all values are an integer number
            int counter = 0;
            for (int j = 0; j < 4; j++)
            {
                for (int i = 0; i < tmpAdRatio.Length; i++)
                {
                    if (!CS.General_v3.Util.Number.IsInteger(tmpAdRatio[i]))
                    {
                        tmpAdRatio[i] *= 10;
                        ok = false;
                    }
                }
                if (ok) break;
            }
            long total = 0;
            int gcd = CS.General_v3.Util.Number.FindGreatestCommonDivisorForAll(tmpAdRatio);
            for (int i = 0; i < tmpAdRatio.Length; i++)
            {
                tmpAdRatio[i] /= gcd;
            }


            //Set the total shown times per round for each ad
            for (int i = 0; i < tmpAdRatio.Length; i++)
            {
                tmpAdRatio[i] = Math.Truncate(tmpAdRatio[i]);
                adverts[i].AdvertShownPerRound = (int)tmpAdRatio[i];
                adverts[i].ResetCounter(false);
                adverts[i].Save();

            }
            
            this.RoundNo = 0;

            this.Save();

        }


        public virtual Advert GetNextAdvertToShow()
        {
            
            var adverts = this.GetAdvertsListToShow();
            if (adverts != null && adverts.Count > 0)
            {
                bool oneNotZero = false;

                bool allShown = true;
                //check if all have been shown
                foreach (var ad in adverts)
                {
                    if (ad.AdvertShownPerRound != 0)
                        oneNotZero = true;
                    if (!ad.IsRoundReady())
                        allShown = false;

                }
                if (allShown && oneNotZero)
                {
                    //increment round number and reset all adverts
                    RoundNo++;
                    foreach (var ad in adverts)
                    {
                        ad.ResetCounter(false);
                        ad.Save();
                    }
                    
                    
                    return GetNextAdvertToShow();
                }
                else if (!oneNotZero)
                {
                    return null;
                }
                else
                {
                    List<Advert> eligibleAds = new List<Advert>();
                    int totalFrom = 0;
                    //get list of eligible ads
                    foreach (var ad in adverts)
                    {
                        if (!ad.IsRoundReady())
                        {
                            eligibleAds.Add(ad);
                            totalFrom += ad.AdvertShownPerRound;
                        }
                    }
                    //get random number between 1 and the total times an advert needs to be shown per round
                    int rndNum = CS.General_v3.Util.Random.GetInt(1, totalFrom);
                    int curr = 0;
                    //check the random number under which advert falls
                    foreach (var ad in eligibleAds)
                    {
                        curr += ad.AdvertShownPerRound;
                        if (rndNum <= curr)
                        {
                            ad.AddHit(true);
                            return ad;
                        }
                    }

                    return null;


                }
            }
            else
            {
                return null;
            }


        }

        #region IAdvertSlot Members

        
        public abstract void Save();

        public abstract List<Advert> GetAdvertsListToShow();

        public virtual int RoundNo {get;set;}

        #endregion
    }
}
