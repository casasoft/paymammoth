﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Adverts
{
    public interface IAdvertSlot
    {
         void Save();
         List<Advert> GetAdvertsListToShow();
         int RoundNo { get; set; }
         
    }
}
