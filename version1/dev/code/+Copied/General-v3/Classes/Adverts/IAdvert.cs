﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Data;

namespace CS.General_v3.Classes.Adverts
{
    public interface IAdvert3
    {
         int ID { get; }
         int AdvertShownPerRound { get; set; }
         string Title { get; set; }
         string Link { get; set;  }
         string ItemURL { get;  }
         double AdvertRatio { get; set; }
         void Save();
         void ResetCounter(bool autoSave);
         int RoundCounter { get; set; }
         bool IsRoundReady();
         void AddHit(bool autoSave);
         int Width { get; }
         int Height { get; }
         JSObject GetJsObject(bool forFlash = true);
    }
}
