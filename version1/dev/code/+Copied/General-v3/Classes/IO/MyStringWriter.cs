﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace CS.General_v3.Classes.IO
{
    public class MyStringWriter : StringWriter
    {
        public delegate void OnWriteHandler(string s);
        public event OnWriteHandler OnWrite;

        private void _write(char[] buffer, int index, int count)
        {
            if (OnWrite != null)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(buffer,index,count);
                OnWrite(sb.ToString());
            }
        }

        public override void Write(bool value)
        {
            base.Write(value);
        }
        public override void Write(char value)
        {
            base.Write(value);
        }
        public override void Write(char[] buffer, int index, int count)
        {
            base.Write(buffer, index, count);
            _write(buffer,index,count);
            
        }
        public override void Write(char[] buffer)
        {
            base.Write(buffer);
        }
        public override void Write(decimal value)
        {
            base.Write(value);
        }
        public override void Write(double value)
        {
            base.Write(value);
        }
        public override void Write(float value)
        {
            base.Write(value);
        }
        public override void Write(int value)
        {
            base.Write(value);
        }
        public override void Write(long value)
        {
            base.Write(value);
        }
        public override void Write(object value)
        {
            base.Write(value);
        }
        public override void Write(string format, object arg0)
        {
            
            base.Write(format, arg0);
        }
        public override void Write(string format, object arg0, object arg1)
        {
            base.Write(format, arg0, arg1);
        }
        public override void Write(string format, object arg0, object arg1, object arg2)
        {
            base.Write(format, arg0, arg1, arg2);
        }
        public override void Write(string format, params object[] arg)
        {
            base.Write(format, arg);
        }
        public override void Write(string value)
        {
            base.Write(value);
        }
        public override void Write(uint value)
        {
            base.Write(value);
        }
        public override void Write(ulong value)
        {
            base.Write(value);
        }
        public override void WriteLine()
        {
            base.WriteLine();
        }
        public override void WriteLine(bool value)
        {
            base.WriteLine(value);
        }
        public override void WriteLine(char value)
        {
            base.WriteLine(value);
        }
        public override void WriteLine(char[] buffer)
        {
            base.WriteLine(buffer);
        }
        public override void WriteLine(char[] buffer, int index, int count)
        {
            base.WriteLine(buffer, index, count);
        }
        public override void WriteLine(double value)
        {
            base.WriteLine(value);
        }
        public override void WriteLine(object value)
        {
            base.WriteLine(value);
        }
        public override void WriteLine(string format, object arg0)
        {
            base.WriteLine(format, arg0);
        }
        public override void WriteLine(string format, object arg0, object arg1)
        {
            base.WriteLine(format, arg0, arg1);
        }
        public override void WriteLine(decimal value)
        {
            base.WriteLine(value);
        }
        public override void WriteLine(float value)
        {
            base.WriteLine(value);
        }
        public override void WriteLine(long value)
        {
            base.WriteLine(value);
        }
        public override void WriteLine(int value)
        {
            base.WriteLine(value);
        }
        public override void WriteLine(string format, object arg0, object arg1, object arg2)
        {
            base.WriteLine(format, arg0, arg1, arg2);
        }
        public override void WriteLine(string format, params object[] arg)
        {
            base.WriteLine(format, arg);
        }
        public override void WriteLine(string value)
        {
            base.WriteLine(value);
        }
        public override void WriteLine(uint value)
        {
            base.WriteLine(value);
        }
        public override void WriteLine(ulong value)
        {
            base.WriteLine(value);
        }
    }
}
