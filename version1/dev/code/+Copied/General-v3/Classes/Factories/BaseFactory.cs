﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Iesi.Collections.Generic;

namespace CS.General_v3.Classes.Factories
{
    /// <summary>
    /// The TSpecificFactoryInstance must be a type that can be used to retrieve the last type from the inheritance chain, e.g an AdvertBase.  It must NEVER be a generic base class,
    /// e.g BaseDataFactory
    /// </summary>
    /// <typeparam name="TSpecificFactoryInstance"></typeparam>
    public abstract class BaseFactory : IBaseFactory
      
    {
        
        static BaseFactory()
        {
            
        }


        

        public virtual object GetItemLock(IBaseObject item)
        {
            return Classes.Locking.LockManager.Instance.GetLock(item);

        }

        private void init()
        {
            this.FactoryPriority = 1000000;

            this.TypesCreatedByFactory = new HashedSet<Type>();
            
        }
         public BaseFactory()
        {
            init();
            
        }
        public BaseFactory(Type typeCreated)
        {
            init();
           
            
        }
        public HashedSet<Type> TypesCreatedByFactory { get; private set; }
        public bool CheckIfFactoryIsForObject(object o)
        {
            return CheckIfFactoryIsForType(o.GetType());
        }
        public bool CheckIfFactoryIsForType(Type objType)
        {
            foreach (var t in TypesCreatedByFactory)
            {
                if (t.IsAssignableFrom(objType) || objType.IsAssignableFrom(t))
                {
                    return true;
                }
            }
            return false;

        }

        public readonly object factoryLock = new object();

        protected void addTypeCreatedByFactory(IEnumerable<Type> types)
        {
            foreach (var t in types)
            {
                this.TypesCreatedByFactory.Add(t);
            }
        }
        protected void addTypeCreatedByFactory(params Type[] types)
        {
            addTypeCreatedByFactory((IEnumerable<Type>)types);
        }


        protected bool onPostInitialisationCalled { get; private set; }
        /// <summary>
        /// This will be called after all factories have been initialised
        /// </summary>
        public virtual void OnPostInitialisation()
        {
            onPostInitialisationCalled = true;
            if (PostInitialisation != null)
                PostInitialisation(this);
        }

        public delegate void PostInitialisationDelegate (BaseFactory sender);
        public event PostInitialisationDelegate PostInitialisation;

        public BaseFactory(params Type[] typesCreated)
        {
            init();
            addTypeCreatedByFactory(typesCreated);
            
        }
        public int FactoryPriority { get; protected set; }



        #region IBaseFactory Members

        IEnumerable<Type> IBaseFactory.GetTypesCreatedByFactory()
        {
            return this.TypesCreatedByFactory;

        }

        #endregion

        #region IBaseFactory Members



        #endregion

        #region IBaseFactory Members

        protected abstract IEnumerable<object> __getAllItemsInRepository();

        IEnumerable<object> IBaseFactory.GetAllItemsInRepository()
        {
            return __getAllItemsInRepository();

        }

        #endregion


        #region IBaseFactory Members

        protected Type _mainTypeCreated = null;
        public Type GetMainTypeCreatedByFactory()
        {
            return _mainTypeCreated;
        }
        public void SetMainTypeCreatedByFactory(Type t)
        {
            _mainTypeCreated = t;
        }

        #endregion

        #region IBaseFactory Members

        protected virtual object _createNewItem()
        {
            Type t = GetMainTypeCreatedByFactory();
            if (t == null)
                throw new InvalidOperationException("Please define the main type created by factory");
            object item = CS.General_v3.Util.ReflectionUtil.CreateObjectWithPrivateConstructor(t);
            

            return item;
        }

        public object CreateNewItem()
        {

            return _createNewItem();
            


        }

        #endregion
    }
}
