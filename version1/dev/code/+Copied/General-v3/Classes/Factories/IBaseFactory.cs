﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Iesi.Collections.Generic;

namespace CS.General_v3.Classes.Factories
{
    public interface IBaseFactory
    {
        //bool UsedInProject { get; set; }
        object GetItemLock(IBaseObject item);
        int FactoryPriority { get; }
        Type GetMainTypeCreatedByFactory();
        void SetMainTypeCreatedByFactory(Type t);
        IEnumerable<Type> GetTypesCreatedByFactory();
        bool CheckIfFactoryIsForType(Type objType);
        object CreateNewItem();
        IEnumerable<object> GetAllItemsInRepository();
        /// <summary>
        /// This will be called after all factories have been initialised
        /// </summary>
        void OnPostInitialisation();

    }
}
