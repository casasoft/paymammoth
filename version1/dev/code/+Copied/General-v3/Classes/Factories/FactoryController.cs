﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Iesi.Collections.Generic;

namespace CS.General_v3.Classes.Factories
{
    public class FactoryController<T> 
        where T : IBaseFactory
    {
        protected HashedSet<T> _factories = new HashedSet<T>();

        

        protected Dictionary<Type, T> _FactoriesForTypes = new Dictionary<Type, T>();
        public virtual void AddFactory(T f, bool removeFactoryIfAnotherOneForSameTypeExists = true)
        {
            if (removeFactoryIfAnotherOneForSameTypeExists)
                RemoveFactoryForType(f.GetTypesCreatedByFactory());
            if (_Finalised)
            {
                //throw new InvalidOperationException("Factory controller is marked as finalised - You cannot add more factories");
            }
            _factories.Add(f);
            foreach (var t in f.GetTypesCreatedByFactory())
            {
                _FactoriesForTypes[t] = f;
            }
            if (_Finalised)
            {
                f.OnPostInitialisation();
            }
        }
        public bool CheckIfFactoryAlreadyExistsForTheSameTypes(IBaseFactory factory)
        {
            foreach (var t in factory.GetTypesCreatedByFactory())
            {
                if (GetFactoryForType(t) != null)
                    return true;
            }
            return false;
        }
        public T GetFactoryForObject(object o)
        {
            return GetFactoryForType(o.GetType());
        }
        protected T getFactoryForTypes(IEnumerable<Type> types)
        {
            foreach (var t in types)
            {
                var f = GetFactoryForType(t);
                if (f != null)
                    return f;
            }
            return default(T);
        }
        private Dictionary<Type, T> _factoriesByType = new Dictionary<Type, T>();
        /// <summary>
        /// Returns the factory, which implements the type passed
        /// </summary>
        /// <typeparam name="TFactory"></typeparam>
        /// <returns></returns>
        public TFactory GetFactoryWhichImplements<TFactory>() where TFactory: IBaseFactory
        {

            TFactory result = default(TFactory);
            {
                T f = default(T);
                if (_factoriesByType.TryGetValue(typeof (TFactory), out f))
                {
                    result = (TFactory)(object)f;
                }
            }
            if (result == null)
            {

                Type typeT = typeof (TFactory);
                foreach (var f in this.GetAllFactories())
                {
                    Type tFactory = f.GetType();
                    if (typeT.IsAssignableFrom(tFactory))
                    {
                        result =(TFactory) (object) f;
                        break;
                    }

                }
                
                _factoriesByType[typeof(TFactory)] = (T)(object)result;

            }
            return result;

        }

        

        public T GetFactoryForType(Type t)
        {
            T factory = default(T);
            
            if (_FactoriesForTypes.ContainsKey(t))
                factory = _FactoriesForTypes[t];
            else
            
            {
                var factories = GetAllFactories();
                foreach (var f in factories)
                {
                    if (f.CheckIfFactoryIsForType(t))
                    {
                        factory = f;
                        _FactoriesForTypes[t] = factory;
                        break;
                    }
                }
            }
            return factory;
            /*

            foreach (var f in _factories)
            {
                if (t.IsAssignableFrom(f.TypeCreatedByFactory))
                    return f;
            }
            return default(T);*/
            /*
            if (_FactoriesForTypes.ContainsKey(t))
                return _FactoriesForTypes[t];
            else
                return default(T);*/
        }


        private bool _Finalised = false;
        /// <summary>
        /// This should be called when all factories have been added
        /// </summary>
        public virtual void FinishedInitialisingFactories()
        {
            _Finalised = true;

            var factories = GetAllFactories().ToList();
            factories.Sort((f1, f2) => (f1.FactoryPriority.CompareTo(f2.FactoryPriority)));



            for (int i = 0; i < factories.Count; i++)
            {
                var item = factories[i];

                item.OnPostInitialisation();
                
            }
        }

        public IEnumerable<T> GetAllFactories()
        
        {
            return _factories;

        }
        public bool RemoveFactoryForType(Type types)
        {
            var typeList = CS.General_v3.Util.ListUtil.GetListFromSingleItem(types);
            return RemoveFactoryForType(typeList);
                
        }

        public bool RemoveFactory(T factory)
        {
            bool ok = false;
            var keys = _FactoriesForTypes.Keys.ToList();
            var values = _FactoriesForTypes.Values.ToList();
            for (int i = 0; i < values.Count; i++)
            {
                T value = values[i];
                if (value.Equals(factory))
                {
                    _FactoriesForTypes.Remove(keys[i]);
                    ok = true;
                }

            }
            _factories.Remove(factory);
            return ok;
        }

        public TItem CreateItemWhichImplementsType<TItem>()
        {
            var f = GetFactoryForType(typeof(TItem));
            if (f == null)
                throw new InvalidOperationException("No factory exists that can create type <" + typeof( TItem).FullName + ">");
            return (TItem)f.CreateNewItem();

        }



        public bool RemoveFactoryForType(IEnumerable<Type> types)
        {
            bool ok = false;
            var f = getFactoryForTypes(types);
            if (f != null)
            {
                
                /*foreach (var typeCreated in f.GetTypesCreatedByFactory())
                {
                    _FactoriesForTypes.Remove(typeCreated);
                }*/
                ok = RemoveFactory(f);
                //foreach (var t in types)
                //{
                //    _FactoriesForTypes.Remove(types);
                //}
                //_factories.Remove(f);
            }
            return ok;
        }


    }
}
