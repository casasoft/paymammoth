﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Factories
{
    public interface IBaseObject
    {
        long ID { get; }
    }
}
