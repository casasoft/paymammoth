﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_20090518.Controls.WebControls.Specialized;
using CS.General_20090518.Classes.GeneralDB.v1;

namespace CS.General_20090518.Classes.CMS.Pages
{
    public abstract class BaseListingPage<TBase, TCriteria> : BasePage, ICMSListingPage<TBase, TCriteria, BasePageInfo>
      where TBase : BaseObject
        where TCriteria : BaseCriteriaBase
    {

        protected abstract BasePageInfo _pageInfo { get; set; }
        public BasePageInfo PageInfo
        {
            get
            {
                return _pageInfo;
            }
            set
            {
                _pageInfo = value;
            }
        }
        protected abstract TCriteria _criteria { get; set; }
        public TCriteria Criteria
        {
            get
            {
                return _criteria;
            }
            set
            {
                _criteria = value;
            }
        }

        protected CS.General_20090518.Controls.WebControls.Specialized.ListingWithPaging _listing = null;
        
        public static string LISTING_QUERYSTRING_SORTBY = "sortby";
        public static string LISTING_QUERYSTRING_SORTTYPE = "sorttype";
        public static string LISTING_QUERYSTRING_PAGENO = "pgno";
        public static string LISTING_QUERYSTRING_PAGESIZE = "pgsize";

        public string SortBy
        {
            get
            {
                InitParameters();
                return _listing.SortBy;
                
            }
        }
        public CS.General_20090518.Enums.SORT_TYPE? SortType
        {
            get
            {
                InitParameters();
                return _listing.SortType;
                
            }
        }
        public int PageNo
        {
            get
            {
                InitParameters();
                return _listing.SelectedPage;

            }
        }
        public int PageSize
        {
            get
            {
                InitParameters();
                return _listing.SelectedShowAmt;
            }
        }
        public BaseListingPage()
        {
            

        }
        public virtual void InitParameters()
        {

        }

        protected override void OnInit(EventArgs e)
        {
            InitParameters();
            _listing.QueryStringVarNamePage = LISTING_QUERYSTRING_PAGENO;
            _listing.QueryStringSortByKey = LISTING_QUERYSTRING_SORTBY;
            _listing.QueryStringSortTypeKey = LISTING_QUERYSTRING_SORTTYPE;
            _listing.QueryStringVarNameShowAmt = LISTING_QUERYSTRING_PAGESIZE;

            base.OnInit(e);
        }



        protected virtual void constructorAfter() { /*do nothing */ }
        protected virtual void constructorBefore() { /*do nothing */ }
        protected virtual void initButtonsAfter() { /*do nothing */ }
        protected virtual void loadDataBeforeGetList() { /*do nothing */ }
        protected virtual void loadDataBeforeBinding() { /*do nothing */ }
        protected virtual void loadDataAfterBinding() { /*do nothing */ }
        protected virtual void fillCriteria() { /*do nothing */ }
        protected virtual void initColumns_Before() { /*do nothing */ }
        protected virtual void initColumns_After() { /*do nothing */ }
        protected virtual void listing_ItemDataBound_Before(Listing.ListingItemRow row) { /*do nothing */ }
        protected virtual void listing_ItemDataBound_After(Listing.ListingItemRow row) { /*do nothing */ }
        protected virtual void initPageTitle_Before() { /*do nothing */ }
        protected virtual void initPageTitle_After() { /*do nothing */ }
        protected virtual void onLoadBefore() { /*do nothing */ }
        protected virtual void onLoadAfter() { /*do nothing */ }
        
        
    }
}