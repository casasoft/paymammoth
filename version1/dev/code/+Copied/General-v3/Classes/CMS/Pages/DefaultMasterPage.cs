﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CS.CMS.UserControls;
using System.Configuration;

using CS.General_20090518.Controls.WebControls.Common;
using System.Web.UI;
using CS.CMS.Classes;

namespace CS.General_20090518.Classes.CMS.Pages
{
    /// <summary>
    /// This is the master page that is directly inherited by the CMS.  This, in turn has 'BaseMasterPage' as its master
    /// </summary>
    public abstract class DefaultMasterPage : MasterPage
    {

        protected CS.CMS.Collections.MainMenuItem addDefaultMenuItem(IObjectPageInfo pageInfo, CS.CMS.Enums.IMAGE_ICON icon)
        {
            string folderRoot = CS.General_20090518.Classes.Settings.v1.SettingsMain.Instance.CMS.Root + pageInfo.Folder + "/";
            string indexPageName = pageInfo.IndexPageName;
            return this.addDefaultMenuItem(folderRoot, pageInfo.Title_SingleUppercase, pageInfo.Title_PluralUppercase, icon, indexPageName);
        }

        /// <summary>
        /// Does not add the 'edit' links
        /// </summary>
        /// <param name="folder"></param>
        /// <param name="titleSingular"></param>
        /// <param name="titlePlural"></param>
        /// <param name="icon"></param>
        /// <param name="indexPageName"></param>
        /// <returns></returns>
        protected CS.CMS.Collections.MainMenuItem addDefaultMenuItem(string folder, string titleSingular, string titlePlural,
            CS.CMS.Enums.IMAGE_ICON icon, string indexPageName)
        {
            return addDefaultMenuItem(folder, titleSingular, titlePlural, icon, indexPageName, null);
        }
        protected CS.CMS.Collections.MainMenuItem addDefaultMenuItem(string folder, string titleSingular, string titlePlural,
           CS.CMS.Enums.IMAGE_ICON icon, string indexPageName, string editPageName)
        {


            string rootPageURL = folder;
            if (!rootPageURL.EndsWith("/")) rootPageURL += "/";
            if (string.IsNullOrEmpty(editPageName))
                rootPageURL += indexPageName;
            CS.CMS.Collections.MainMenuItem menuItem = new CS.CMS.Collections.MainMenuItem(titlePlural, rootPageURL , icon);

           /* if (!string.IsNullOrEmpty(editPageName))
            {
                menuItem.AddChild(new CS.CMS.Collections.MainMenuItem("Manage " + titlePlural,
                     folder + "/" + indexPageName, CS.CMS.Enums.IMAGE_ICON.Listing));
                menuItem.AddChild(new CS.CMS.Collections.MainMenuItem("Add new " + titleSingular,
                    folder + "/" + editPageName + "?id=0", CS.CMS.Enums.IMAGE_ICON.Add));

            }*/
            Master.Functionality.MainMenu.AddItem(menuItem);
            return menuItem;
        }


        public new BaseMasterPage Master
        {
            get
            {
                return (BaseMasterPage)base.Master;
            }
        }


        
    }
}
