﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_20090518.Classes.GeneralDB.v1;
using CS.CMS.Pages;

namespace CS.General_20090518.Classes.CMS.Pages
{
    public interface ICMSListingPage<TBase, TCriteria, TPageInfo> 
        where TBase : BaseObject
        where TCriteria : BaseCriteriaBase
        where TPageInfo : BasePageInfo
    {
        TCriteria Criteria { get; set; }
        TPageInfo PageInfo { get; set; }
    }
}
