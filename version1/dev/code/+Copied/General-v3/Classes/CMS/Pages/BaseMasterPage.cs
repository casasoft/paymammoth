﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CS.CMS.UserControls;
using System.Configuration;
using System.Collections;

using CS.General_20090518.Controls.WebControls.Common;

namespace CS.General_20090518.Classes.CMS.Pages
{

    public abstract class BaseMasterPage : CS.General_20090518.Pages.BaseMasterPage
    {

        

        public string Title
        {
            get
            {
                return CS.General_20090518.Settings.GetSetting("Title");
            }


        }
            
        public class FUNCTIONALITY
        {
            public Hashtable SessionDataResettable
            {
                get
                {
                    Hashtable nv = (Hashtable)Session["__SessionDataResettable"];
                    if (nv == null)
                    {
                        nv = new Hashtable();

                        SessionDataResettable = nv;
                    }
                    
                    return nv;
                }
                private set
                {
                    Session["__SessionDataResettable"] = value;
                }
            }

            private BaseMasterPage _baseMasterPage = null;
            public System.Web.SessionState.HttpSessionState Session
            {
                get { return _baseMasterPage.Session; }
            }
            public FUNCTIONALITY(BaseMasterPage pg)
            {
                _baseMasterPage = pg;
                ExtraCSSFile = "/css/cms.css";
                this.GoBackText = "Go Back";
                
            }
            public string DojoLocalURL { get; set; }
            public string ExtraCSSFile { get; set; }
            public bool ShowLanguageSelect { get; set; }
            public Classes.AdminUserLogin MemberLogin
            {
                get
                {
                    return (Classes.AdminUserLogin)Session["__AdminCMS_MemberLogin"];
                }
                set
                {
                    Session["__AdminCMS_MemberLogin"] = value;
                }
            }
            public string GoBackText { get; set; }
            public string GoBackURL { get; set; }
            public IEnumerable<CS.CMS.BusinessLogic.Languages.Language_Base> Languages { get; set; }
            public MyImage Logo
            {
                get
                {
                    _baseMasterPage.initControls();
                    return _baseMasterPage._logo;


                }
            }

            public TopButtons TopButtons
            {
                get
                {
                    _baseMasterPage.initControls();
                    return _baseMasterPage._topButtons;
                }
            }
            public MainMenu MainMenu
            {
                get
                {
                    _baseMasterPage.initControls();
                    return _baseMasterPage._mainMenu;
                }
            }



            /// <summary>
            /// Set the page title of the CMS
            /// </summary>
            /// <param name="titles">A list of sections.  Only pass the sections, e.g. Manage Tracks, Add Track</param>
            public void SetPageTitle(List<Classes.PageTitle> list)
            {
                var arr = list.ToArray();
                SetPageTitle(arr);
            }
            
            
            
            /// <summary>
            /// Set the page title of the CMS
            /// </summary>
            /// <param name="titles">A list of sections.  Only pass the sections, e.g. Manage Tracks, Add Track</param>
            public void SetPageTitle(params Classes.PageTitle[] titles)
            {
                _baseMasterPage.initControls();
                StringBuilder title = new StringBuilder();
                title.Append(this._baseMasterPage.Title + " CMS");
                StringBuilder listingTitle = new StringBuilder();
                
                for (int i = 0; i < titles.Length; i++)
                {
                    if (i > 0)
                    {
                        listingTitle.Append(" | ");
                    }

                    title.Insert(0,titles[i].Title + " | ");
                    listingTitle.Append(titles[i].GetHTML());
                    
                    
                }
                _baseMasterPage.Page.Master.Page.Title = _baseMasterPage.Page.Title = title.ToString();
                //_ltlCurrentTitle.Text = titles[titles.Length-1];
                _baseMasterPage._ltlCurrentTitle.Text = listingTitle.ToString();
            }


            public void ShowStatusMessageAndRedirectToSamePage(string message, STATUS_MSG_TYPE type)
            {
                ShowStatusMessage(message, type);
                CS.General_20090518.Util.Page.URL.RedirectToSamePage();

            }
            public void ShowStatusMessageAndTransferToSamePage(string message, STATUS_MSG_TYPE type)
            {
                ShowStatusMessage(message, type);
                CS.General_20090518.Util.Page.URL.TransferToSamePage();

            }
            public void ShowStatusMessage(string message, STATUS_MSG_TYPE type)
            {
                Session[SESSION_STATUS_MESSAGE] = message;
                Session[SESSION_STATUS_MESSAGE_TYPE] = type;
            }
            /*
            /// <summary>
            /// 
            /// </summary>
            /// <param name="username"></param>
            /// <param name="redirectURL">The redirecting URL IF there is no previously stored last logged in URL</param>
            public void Login(CS.General_20090518.Classes.Login.IUser user, string redirectURL)
            {
                LoggedMember = user;
                if (!String.IsNullOrEmpty(_lastLoggedInURL))
                {
                    redirectURL = _lastLoggedInURL;
                }
                if (string.Compare(CS.General_20090518.Util.Page.getURLLocation(), redirectURL, true) != 0)
                {
                    Response.Redirect(redirectURL);
                }
            }
            */
            public bool NoAuthenticationRequired { get; set; }
            internal void resetSessionDataResettable()
            {
                SessionDataResettable = null;
            }
        }
        public FUNCTIONALITY Functionality { get; set; }
        public BaseMasterPage()
        {
            
            this.Functionality = new FUNCTIONALITY(this);
            
            
            
        }
        private const string SESSION_STATUS_MESSAGE = "cs-cms-status-msg";
        private const string SESSION_STATUS_MESSAGE_TYPE = "cs-cms-status-msg-type";

        public enum STATUS_MSG_TYPE
        {
            Success = 0,
            Warning = 25,
            Error = 50
        }
        public abstract CS.CMS.WebControls.ButtonsBar ButtonsBar { get; }
        protected MainMenu _mainMenu;
        protected TopButtons _topButtons;
        protected Literal _ltlTitle;
        protected Literal _ltlCurrentTitle;
        protected MyImage _logo;
        protected HtmlLink _linkExtraCSS;
        protected HtmlGenericControl _divGoBack;
        protected MyLink _aGoBack;
        protected HtmlGenericControl _divDate;
        protected HtmlGenericControl _divStatusMessage;
        private string _lastLoggedInURL
        {
            get
            {
                return (string)Session["_lastLoggedInURL"];
            }
            set
            {
                Session["_lastLoggedInURL"] = value;
            }
        }
        

        private const string SESSION_LOGGED_MEMBER = "logged_member";
        
        private void initDate()
        {
            initControls();
            _divDate.InnerHtml = DateTime.Now.ToString("ddd dd MMMM yyyy");
        }
        public new BasePage Page
        {
            get
            {
                return (BasePage)base.Page;
            }
        }
        private void checkAuthentication()
        {
            
            if (!Functionality.NoAuthenticationRequired && !Functionality.MemberLogin.CheckAuthentication(Page.Functionality.AccessTypeRequired))
            {
                //Authentication is required and no member is logged in
                _lastLoggedInURL = Request.Url.ToString();
                Functionality.ShowStatusMessage("You do not have access to visit this section.  Please login.", STATUS_MSG_TYPE.Error);
                Response.Redirect(CS.General_20090518.Settings.Others.CMS_Root);
            }
        }
        
        protected override void OnLoad(EventArgs e)
        {
            this.Page.PreRenderComplete += new EventHandler(Page_PreRenderComplete);
            initDate();
            checkAuthentication();

            showDefaultTopButtons();
            base.OnLoad(e);
        }
        private void renderControls()
        {
            _ltlTitle.Text = this.Title;
        }
        private void checkExtraCSS()
        {
            _linkExtraCSS.Visible = false;
            if (!string.IsNullOrEmpty(Functionality.ExtraCSSFile))
            {
                _linkExtraCSS.Visible = true;
                _linkExtraCSS.Href = Functionality.ExtraCSSFile;
            }
        }
        protected void checkGoBackButton()
        {
            this._divGoBack.Visible = (!string.IsNullOrEmpty(Functionality.GoBackURL));
            this._aGoBack.Text = Functionality.GoBackText;
            this._aGoBack.ResolveClientHRef = false;
            this._aGoBack.HRef = Functionality.GoBackURL;
        }
        private void checkButtonsBar()
        {
            ButtonsBar.Visible = (ButtonsBar.HasButtons());

        }
        protected override void OnPreRender(EventArgs e)
        {
            checkExtraCSS();
            initStatusMessage();
            renderControls();
            if (Functionality.MemberLogin.IsLoggedIn)
            {
                Functionality.MainMenu.RenderMenu(Functionality.MemberLogin.LoggedInUser.AccessType);
            }
            checkGoBackButton();
            checkButtonsBar();
            base.OnPreRender(e);
            
        }

        void Page_PreRenderComplete(object sender, EventArgs e)
        {
            this.Functionality.resetSessionDataResettable();
        }

        private void initStatusMessage()
        {
            _divStatusMessage.Visible = false;
            if (Session[SESSION_STATUS_MESSAGE] != null)
            {
                string msg = (string)Session[SESSION_STATUS_MESSAGE];
                Session.Remove(SESSION_STATUS_MESSAGE);
                //Literal litMsg = new Literal();
                //litMsg.Text = msg;
                //litMsg.Text = litMsg.Text.Insert(50, "<ul><li>test1</li></ul>");

                _divStatusMessage.InnerHtml = msg;
                //_pStatusMessage.Controls.Add(litMsg);
                //.InnerHtml = msg;

                if (Session[SESSION_STATUS_MESSAGE_TYPE] != null)
                {
                    STATUS_MSG_TYPE type = (STATUS_MSG_TYPE)Session[SESSION_STATUS_MESSAGE_TYPE];
                    Session.Remove(SESSION_STATUS_MESSAGE_TYPE);
                    switch (type)
                    {
                        case STATUS_MSG_TYPE.Error: _divStatusMessage.Attributes["class"] = "error"; break;
                        case STATUS_MSG_TYPE.Success: _divStatusMessage.Attributes["class"] = "success"; break;
                        case STATUS_MSG_TYPE.Warning: _divStatusMessage.Attributes["class"] = "warning"; break;
                    }
                    _divStatusMessage.Visible = true;
                }
            }
        }

        private void showDefaultTopButtons()
        {
            initControls(); //Just in case
            if (Functionality.MemberLogin.IsLoggedIn)
            {

                Functionality.TopButtons.AddButton(Functionality.MemberLogin.LoggedInUser.FullName + " (" + Functionality.MemberLogin.LoggedInUser.Username + ")", Enums.IMAGE_ICON.User, MyImageButtonWithText.BUTTON_MODE.None);
                MyImageButtonWithText btnLogout = Functionality.TopButtons.AddButton("Logout", Enums.IMAGE_ICON.Logout, MyImageButtonWithText.BUTTON_MODE.Button);

                btnLogout.Click += new EventHandler(btnLogout_Click);
                if (Functionality.ShowLanguageSelect)
                {
                    UserControls.LanguageSelect langSelect = (UserControls.LanguageSelect)LoadControl("../usercontrols/LanguageSelect.ascx");
                  //  langSelect.Languages = this.Functionality.Languages;
                    Functionality.TopButtons.AddControl(langSelect);
                }
            }
            
        }

        void btnLogout_Click(object sender, EventArgs e)
        {

            Session.Remove(SESSION_LOGGED_MEMBER);
            this.Functionality.MemberLogin.Logout();
            Functionality.ShowStatusMessage("You have been logged out successfully!", STATUS_MSG_TYPE.Success);
            Response.Redirect(CS.General_20090518.Settings.Others.CMS_Root);
        }
        
    }
}
