﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_20090518.Classes.CMS.Pages
{
    public abstract class BasePage : CS.General_20090518.Pages.BasePage
    {
        
        public new BaseMasterPage Master
        {
            get
            {
                if (base.Master != null)
                {
                    if (base.Master is BaseMasterPage)
                    {
                        return (BaseMasterPage)base.Master;
                    }
                    else if (base.Master.Master is BaseMasterPage)
                    {
                        return (BaseMasterPage)base.Master.Master;
                    }
                }
                return null;
            }
        }
        public class FUNCTIONALITY : CS.General_20090518.Pages.BasePage.FUNCTIONALITY
        {
            private BasePage _page = null;
            public Enums.ADMINUSER_ACCESSTYPE AccessTypeRequired { get; set; }
            public FUNCTIONALITY(BasePage page) : base(page)
            {
                _page = page;
            }
            
        }
        public new FUNCTIONALITY Functionality
        {
            get
            {
                return (FUNCTIONALITY)base.Functionality;
            }
            set
            {
                base.Functionality = value;
            }
        }
        protected override void setJavaScriptOverrideValues()
        {
            base.setJavaScriptOverrideValues();
            CS.General_20090518.Settings.JScriptFramework_Override = General_20090518.Settings.JSCRIPT_FRAMEWORK.JQuery;
            CS.General_20090518.Settings.Others.JavaScript_Com_Version_Override = "v2";
        }

        public BasePage()
        {
            this.Functionality = new FUNCTIONALITY(this);
            this.Functionality.AccessTypeRequired = Enums.ADMINUSER_ACCESSTYPE.Normal;
            
        }

    }
}