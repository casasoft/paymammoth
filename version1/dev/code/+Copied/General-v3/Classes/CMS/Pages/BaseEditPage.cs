﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_20090518.Classes.GeneralDB.v1;


namespace CS.General_20090518.Classes.CMS.Pages
{
    public abstract class BaseEditPage<TBase> : BasePage, ICMSEditPage<TBase, BasePageInfo>
        where TBase : BaseObject
    {

        protected abstract BasePageInfo _pageInfo { get; set; }
        public BasePageInfo PageInfo
        {
            get
            {
                return _pageInfo;
            }
            set
            {
                _pageInfo = value;
            }
        }

        protected CS.General_20090518.Controls.WebControls.Specialized.FormFields _formFields = null;
        private bool _initParameters = false;
        private void initParamters()
        {
            if (!_initParameters)
            {
                _initParameters = true;
                InitParameters();
            }
        }
        public override void InitParameters()
        {
            base.InitParameters();
        }
        

        public int ID
        {
            get
            {

                string s = CS.General_20090518.Util.Page.RQ("id");
                int id = 0;
                if (!string.IsNullOrEmpty(s))
                {
                    Int32.TryParse(s, out id);
                }
                return id;
            }
        }

        public virtual bool IsEdit
        {
            get { return ID> 0;}
        }
        protected override void OnLoad(EventArgs e)
        {
            InitParameters();

            if (IsEdit)
            {
                _formFields.ButtonSubmitText = "Edit";
            }
            else
            {
                _formFields.ButtonCancelText = "Add";
            }
            base.OnLoad(e);
        }
        protected override void OnPreRender(EventArgs e)
        {
            
            base.OnPreRender(e);
        }
        public BaseEditPage()
            : base()
        {
            

        }

        protected abstract TBase _Item {get;set;}
        public TBase Item
        {
            get
            {
                return _Item;
            }
            set
            {
                _Item = value;
            }
        }

        protected virtual List<PageTitle> pageTitles { get; set; }

        protected virtual void initPageTitle_Before() { /* do nothing */ }
        protected virtual void initPageTitle_After() { /* do nothing */ }
        
        protected virtual void constructor_After() { /* do nothing */ }
        protected virtual void constructor_Before(){ /* do nothing */ }
        protected virtual void initParametersBefore(){ /* do nothing */ }
        protected virtual void initParametersAfter(){ /* do nothing */ }
        protected virtual void initFormBefore(){ /* do nothing */ }
        protected virtual void initFormAfter(){ /* do nothing */ }
        protected virtual void saveFields_BeforeSaving(List<string> errorMsgs){ /* do nothing */ }
        protected virtual void saveFields_AfterSaving(List<string> errorMsgs){ /* do nothing */ }
        protected virtual void saveFieldsBefore(List<string> errorMsgs){ /* do nothing */ }
        protected virtual void onLoadAfter(){ /* do nothing */ }
        protected virtual void onLoadBefore(){ /* do nothing */ }


    }
}