﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_20090518.Classes.GeneralDB.v1;


namespace CS.General_20090518.Classes.CMS.Pages
{
    public interface ICMSEditPage<TBase, TPageInfo> 
        where TBase : BaseObject
        where TPageInfo : BasePageInfo
    {
        TBase Item { get; set; }
        TPageInfo PageInfo { get; set; }
    }
}
