﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using CS.General_20090518.Classes.GeneralDB.v1;


namespace CS.General_20090518.Classes.CMS.Pages
{
    public abstract class BasePageInfo : IObjectPageInfo
    {
        public abstract List<PageTitle> GetLinkedWithPageTitles();
        public abstract string Title_SingleLowercase { get; set; }
        public abstract string Title_SingleUppercase { get; set; }
        public abstract string Title_PluralLowercase { get; set; }
        public abstract string Title_PluralUppercase { get; set; }
        public abstract string EditPageName { get; set; }
        public abstract string IndexPageName { get; set; }
        public abstract string Folder { get; set; }
        public abstract string GetItemLink(int primaryKeyValue);
        public abstract string GetItemLink(int primaryKeyValue, NameValueCollection extraQSParams);
    }
}
