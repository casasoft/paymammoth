﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_20090518.Classes.CMS
{
    public class Enums 
    {
        public enum ADMINUSER_ACCESSTYPE
        {
            Normal = 0,
            Administrator = 50,
            SuperAdministrator = 100
        }
        public static bool CheckAdminUserAccess(ADMINUSER_ACCESSTYPE currAccessType, ADMINUSER_ACCESSTYPE requiredAccessType)
        {
            int iCurr = (int)currAccessType;
            int iReq = (int)requiredAccessType;
            return (iCurr >= iReq);
        }
        

        public enum IMAGE_ICON
        {
            Add,
            Edit,
            Brand,
            GoBack,
            Delete,
            Track,
            Logout,
            User,
            ReOrder,
            Award,
            Download,
            Search,
            Success,
            Error,
            Member,
            Member2,
            Email,
            Calendar,
            Category,
            MailForward,
            Settings,
            PageEdit,
            Listing,
            Language,
            Music,
            News,
            Import,
            Export,
            Product,
            PDF,
            UnorderedList,
            EuroSign,
            Update,
            Speaker,
            DeeJay,
            Music2,
            News2,
            Photos,
            Link,
            Generate,
            Question

        }
        





        /// <summary>
        /// Returns the icon image URL according to image Folder
        /// </summary>
        /// <param name="image"></param>
        /// <param name="imageFolder">Specify image folder path in CMS e.g. ../images/</param>
        /// <returns></returns>
        public static string GetImageFilePath(IMAGE_ICON image)
        {
            string imgFileName = "";
            switch (image)
            {
                case IMAGE_ICON.Brand: imgFileName = "icon_brand.png"; break;
                case IMAGE_ICON.Add: imgFileName = "icon_add_up.png"; break;
                case IMAGE_ICON.Language: imgFileName = "icon_language.png"; break;
                case IMAGE_ICON.Edit: imgFileName = "icon_edit_up.png"; break;
                case IMAGE_ICON.GoBack: imgFileName = "icon_edit_up.png"; break;
                case IMAGE_ICON.Delete: imgFileName = "icon_delete_up.png"; break;
                case IMAGE_ICON.Track: imgFileName = "icon_track_up.png"; break;
                case IMAGE_ICON.Logout: imgFileName = "icon_logout_up.png"; break;
                case IMAGE_ICON.Calendar: imgFileName = "icon_calendar.png"; break;
                case IMAGE_ICON.User: imgFileName = "icon_user_up.png"; break;
                case IMAGE_ICON.ReOrder: imgFileName = "icon_reorder_up.png"; break;
                case IMAGE_ICON.Search: imgFileName = "icon_search_up.png"; break;
                case IMAGE_ICON.Award: imgFileName = "icon_award.png"; break;
                case IMAGE_ICON.Download: imgFileName = "icon_download.png"; break;
                case IMAGE_ICON.Success: imgFileName = "icon_success_up.png"; break;
                case IMAGE_ICON.Error: imgFileName = "icon_error_up.png"; break;
                case IMAGE_ICON.Product: imgFileName = "icon_product.gif"; break;
                case IMAGE_ICON.Category: imgFileName = "icon_category.gif"; break;
                case IMAGE_ICON.Question: imgFileName = "icon_question.png"; break;
                case IMAGE_ICON.Settings: imgFileName = "icon_settings.png"; break;
                case IMAGE_ICON.EuroSign: imgFileName = "icon_euro.png"; break;
                case IMAGE_ICON.UnorderedList: imgFileName = "icon_unorderedlist.gif"; break;
                case IMAGE_ICON.PDF: imgFileName = "icon_pdf.gif"; break;

                case IMAGE_ICON.Member: imgFileName = "icon_member_up.png"; break;
                case IMAGE_ICON.Member2: imgFileName = "icon_member2_up.png"; break;
                case IMAGE_ICON.Email: imgFileName = "icon_email_up.png"; break;
                case IMAGE_ICON.MailForward: imgFileName = "icon_mailforward_up.png"; break;
                case IMAGE_ICON.PageEdit: imgFileName = "icon_pageedit_up.gif"; break;
                case IMAGE_ICON.Listing: imgFileName = "icon_listing_up.gif"; break;
                case IMAGE_ICON.Music: imgFileName = "icon_music_up.png"; break;
                case IMAGE_ICON.News: imgFileName = "icon_news_up.png"; break;
                case IMAGE_ICON.Import: imgFileName = "icon_import_up.gif"; break;
                case IMAGE_ICON.Export: imgFileName = "icon_export_up.gif"; break;

                case IMAGE_ICON.Update: imgFileName = "icon_update_up.gif"; break;
                case IMAGE_ICON.Speaker: imgFileName = "icon_speaker_up.png"; break;
                case IMAGE_ICON.DeeJay: imgFileName = "icon_deejay_up.png"; break;
                case IMAGE_ICON.Music2: imgFileName = "icon_music2_up.png"; break;
                case IMAGE_ICON.News2 : imgFileName = "icon_news2_up.png"; break;
                case IMAGE_ICON.Link: imgFileName = "icon_link_up.gif"; break;
                case IMAGE_ICON.Photos: imgFileName = "icon_photos_up.png"; break;
                case IMAGE_ICON.Generate: imgFileName = "icon_generate_up.gif"; break;
            }


            string rootFolder = Settings.Images.GetImageRootFolder();
            //rootFolder += "_common/images/";
            string imageURL = rootFolder + imgFileName;
            
            
            return imageURL;
        }


        public static string GetImageFilePath(IMAGE_ICON image, out string rolloverImageURL)
        {
            string imgFileName = GetImageFilePath(image);
            rolloverImageURL = imgFileName.Replace("up.png", "over.png");
            return imgFileName;
        }
    }
    





    
}
