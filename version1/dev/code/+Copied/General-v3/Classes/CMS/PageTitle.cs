﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_20090518.Classes.CMS
{
    public class PageTitle
    {
        public string Title { get; set; }
        public string URL { get; set; }
        public PageTitle(string title) : this(title,"")
        {

        }
        public PageTitle(string title, string url)
        {
            this.Title = title;
            this.URL = url;
            if (string.IsNullOrEmpty(this.URL))
                this.URL = CS.General_20090518.Util.Page.GetURLLocationAndQuerystring();
        }
        public string GetHTML()
        {
            string s = "";
            if (!string.IsNullOrEmpty(URL))
            {
                
                s += "<a class='page_title' href=\"" + URL + "\" title=\"" + Title.Replace("\"","\\\"") + "\">";
            }
            s += this.Title;
            if (!string.IsNullOrEmpty(URL))
            {
                s += "</a>";
            }
            return s;

        }
        public override string ToString()
        {
            return this.Title + " (" + this.URL + ")";
        }
    }
}
