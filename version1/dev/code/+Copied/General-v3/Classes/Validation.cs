﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using CS.General_v3.Controls.WebControls.Common;
namespace CS.General_v3.Classes
{
    
    public class Validation
    {
        private const int NUM_THRESHOLD = 999999;
        


        public static void AddToRequiredGroup(ref Hashtable ht, Control ctrl, string group)
        {
            if (group != "")
            {
                List<Control> list = (List<Control>)ht[group];
                if (list == null)
                {
                    list = new List<Control>();
                    ht[group] = list;
                }
                list.Add(ctrl);
            }
        }
        

        private static string CheckRequiredGroups(Hashtable requiredGroups, ErrorProvider errProvider)
        {
            string msg = "";
            StringBuilder sb = new StringBuilder();
            #region Required Groups
            foreach (object o in requiredGroups.Keys)
            {
                string group = (string)o;
                List<Control> list = (List<Control>)requiredGroups[group];
                string titles = "";
                bool groupOK = false;
                for (int j = 0; j < list.Count; j++)
                {
                    Control c = list[j];
                    if (j < list.Count - 1)
                        titles += ", ";
                    else
                        titles += " or ";
                    
                }
                if (titles.Length > 0)
                    titles = titles.Substring(2);
                if (!groupOK)
                {
                    msg = "One of " + titles + " must be filled in";
                    for (int j = 0; j < list.Count; j++)
                    {
                        Control c = list[j];
                        errProvider.SetError(c, msg);
                       
                        
                    }
                    sb.Append(" - " + msg + "\r\n");
                }

            }
            return sb.ToString();
            #endregion
        }

        public static string ValidateForm(Control checkCtrl, string validationGroup, ErrorProvider errProvider, out Control leastTabIndexCtrl)
        {
            Hashtable requiredGroups = new Hashtable();
            List<Control> controlList = new List<Control>();
            GetValidationControlCollection(checkCtrl.Controls, validationGroup, ref controlList, ref requiredGroups);
            string errMsg = ValidateControlList(controlList, errProvider);
            errMsg += CheckRequiredGroups(requiredGroups, errProvider);
            leastTabIndexCtrl = null;
            if (errMsg != "")
            {
                for (int i = 0; i < controlList.Count; i++)
                {
                    Control ctrl = controlList[i];
                    
                }
            }

            return errMsg;
        }
        private static void GetValidationControlCollection(Control.ControlCollection coll, string validationGroup, ref List<Control> controlList, ref Hashtable HTRequiredGroups)
        {
            for (int i = 0; i < coll.Count; i++)
            {
                

            }
            for (int i = 0; i < coll.Count; i++)
            {
                if (coll[i].Controls != null)
                {
                    GetValidationControlCollection(coll[i].Controls, validationGroup, ref controlList, ref HTRequiredGroups);
                    
                }
            }
            
        }
        
        private static string ValidateControlList(List<Control> controlList, ErrorProvider errProvider)
        {

            
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < controlList.Count; i++)
            {
                Control c = controlList[i];
                
                
            }
            return sb.ToString();
        }
        public string Title = "";
        public Control Control = null;
        public bool HasValidation = true;
        public bool HasError = false;
        public bool IsNumeric = false;
        public bool IsRequired = false;
        public bool IsEmail = false;
        public bool IsWebsite = false;
        public double NumberFrom = Int32.MinValue;
        public double NumberTo = Int32.MaxValue;
        public bool IsDate = false;
        public bool IsTime = false;
        public bool TimeRequired = false;
        public DateTime DateFrom = DateTime.MinValue;
        public DateTime DateTo = DateTime.MinValue;
        public int MaxLength = Int32.MinValue;
        public int MinLength = Int32.MinValue;
        public bool IntegersOnly = false;
        public bool PositiveNumsOnly = false;
        public List<string> ValueNotIn = null;
        public MyTxtBox ValueSameAsTxt = null;
        public string ValidationGroup = "main";
        public string RequiredGroup = "";
        
        private string formatErrMsg(string msg)
        {
            return msg;
        }


        public bool Validate(Form form, string s, out string errorMessage)
        {
            if (this.HasValidation)
            {
                bool err = false;
                bool ok = false;
                HasError = false;
                if (s == null)
                    s = "";
                string controlID = null;
                if (this.Control != null)
                    controlID = this.Control.Name;
                if (string.IsNullOrEmpty(Title) && this.Control != null)
                    Title = this.Control.Name;
                if (string.IsNullOrEmpty(Title))
                    throw new InvalidOperationException("Please fill in title first for '" + controlID + "'");

                StringBuilder errMsg = new StringBuilder();
                if (IsRequired && s == "")
                {
                    errMsg.Append(formatErrMsg(Title + " is required"));
                    HasError = true;
                }
                if (IsDate && s != "")
                {
                    DateTime tmp;
                    string sDate = s;
                    ok = DateTime.TryParse(sDate, out tmp);
                    if (!ok)
                    {
                        int spacePos = sDate.IndexOf(' ');
                        if (spacePos > -1)
                        {
                            sDate = sDate.Substring(0, spacePos);
                            ok = DateTime.TryParse(sDate, out tmp);
                        }
                    }
                    if (ok && sDate.Length < 12 && TimeRequired)
                    {
                        errMsg.Append(formatErrMsg(Title + " must contain a time, in 24-hour format (hh:mm)"));
                        err = true;
                        HasError = true;
                    }
                    if (!ok)
                    {
                        if (!TimeRequired)
                            errMsg.Append(formatErrMsg(Title + " must contain a valid date (dd/mm/yyyy)"));
                        else
                            errMsg.Append(formatErrMsg(Title + " must contain a valid date (dd/mm/yyyy) and time in 24-hour clock (hh:mm)"));
                        HasError = true;
                    }
                }
                if ((DateFrom > DateTime.MinValue && DateFrom < DateTime.MaxValue) || (DateTo > DateTime.MinValue && DateTo < DateTime.MaxValue) && s != "")
                {
                    DateTime tmp;
                    ok = DateTime.TryParse(s, out tmp);
                    
                    if (ok)
                    {

                        if ((DateFrom > DateTime.MinValue && tmp.Date < DateFrom.Date) ||
                            (DateTo > DateTime.MinValue && tmp.Date > DateTo.Date))
                        {
                            if (DateFrom > DateTime.MinValue && DateTo > DateTime.MinValue)
                                errMsg.Append(formatErrMsg(Title + " must contain a valid date from " + DateFrom.ToString("dd/MM/yyyy") + " and " + DateTo.ToString("dd/MM/yyyy")));
                            else if (DateFrom > DateTime.MinValue)
                                errMsg.Append(formatErrMsg(Title + " must contain a valid date from " + DateFrom.ToString("dd/MM/yyyy") + " onwards"));
                            else
                                errMsg.Append(formatErrMsg(Title + " must contain a valid date up to " + DateTo.ToString("dd/MM/yyyy")));
                            HasError = true;
                        }
                        //errMsg.Append(formatErrMsg(Title + " must contain a number"));
                        HasError = true;
                    }
                }
                if (ValueSameAsTxt != null && s != "")
                {

                    MyTxtBox txt = ValueSameAsTxt;
                    ok = (txt.FormValue == s);
                    if (!ok)
                    {
                        HasError = true;
                        errMsg.Append(formatErrMsg(Title + " must contain the same value as " + txt.Title));
                        
                    }
                }
                if (s != "" && IsEmail)
                {
                    int atCounts = 0;
                    int lastAtPos = -1;
                    do
                    {
                        lastAtPos = s.IndexOf('@', lastAtPos + 1);
                        if (lastAtPos > -1)
                            atCounts++;
                    } while (lastAtPos > -1);
                    if (atCounts > 1)
                    {
                        HasError = true;
                        errMsg.Append(formatErrMsg(Title + " must contain an email address, which must have only one '@' sign"));
                    }
                    else if (atCounts == 0)
                    {
                        HasError = true;
                        errMsg.Append(formatErrMsg(Title + " must contain an email address, which must contain an '@' sign"));
                    }
                    if (!HasError && s.IndexOf(' ') > -1)
                    {
                        HasError = true;
                        errMsg.Append(formatErrMsg(Title + " must contain an email address, which cannot contain any spaces ' '"));
                    }
                    if (!HasError)
                    {
                        lastAtPos = s.IndexOf('@');
                        if (lastAtPos == 0)
                        {
                            HasError = true;
                            errMsg.Append(formatErrMsg(Title + " must contain an email address, which must contain some text before the '@' sign"));
                        }
                        else if (lastAtPos == s.Length - 1)
                        {
                            HasError = true;
                            errMsg.Append(formatErrMsg(Title + " must contain an email address, which must contain some text after the '@' sign"));
                        }
                    }
                    

                }
                if (!string.IsNullOrEmpty(s) && IsWebsite)
                {
                    string regex = @"^(https?://)?[A-Za-z0-9.]+\.[A-Za-z]{2,4}$";
                    if (!Regex.IsMatch(s, regex))
                    {
                        errMsg.Append(formatErrMsg(Title + " must contain a website"));
                    }


                }
                
                if (IsNumeric && s != "")
                {
                    double tmp;
                    ok = Double.TryParse(s, out tmp);
                    if (!ok)
                    {
                        errMsg.Append(formatErrMsg(Title + " must contain a number"));
                        HasError = true;
                    }
                }
                if (PositiveNumsOnly && s != "")
                {
                    double tmp;
                    ok = Double.TryParse(s, out tmp);
                    if (ok && tmp < 0)
                    {
                        errMsg.Append(formatErrMsg(Title + " must contain a positive number"));
                        HasError = true;
                    }
                }
                if (IntegersOnly && s != "")
                {
                    double tmp;
                    ok = Double.TryParse(s, out tmp);

                    if (ok)
                    {
                        double tmp2 = Convert.ToDouble((int)tmp);
                        if (tmp2 != tmp)
                        {
                            errMsg.Append(formatErrMsg(Title + " must contain only integers"));
                            HasError = true;
                        }
                    }
                }
                if ((NumberFrom > -NUM_THRESHOLD || NumberTo < NUM_THRESHOLD) && s != "")
                {
                    double tmp;
                    ok = Double.TryParse(s, out tmp);
                    if (ok)
                    {
                        if (((NumberFrom > -NUM_THRESHOLD && tmp < NumberFrom) || ((NumberTo < NUM_THRESHOLD) && tmp > NumberTo)))
                        {
                            string fromNum = NumberFrom.ToString("0.00");
                            string toNum = NumberTo.ToString("0.00");
                            if (!Util.NumberUtil.HasDecimalPoint(NumberFrom))
                                fromNum = NumberFrom.ToString("0");
                            if (!Util.NumberUtil.HasDecimalPoint(NumberTo))
                                toNum = NumberTo.ToString("0");


                            if ((NumberFrom > -NUM_THRESHOLD) && (NumberTo < NUM_THRESHOLD))
                                errMsg.Append(formatErrMsg(Title + " must contain a number between " + fromNum + " and " + toNum));
                            else if (NumberFrom > -NUM_THRESHOLD)
                                errMsg.Append(formatErrMsg(Title + " must contain a number larger than or equal to " + fromNum));
                            else
                                errMsg.Append(formatErrMsg(Title + " must contain a number smaller than or equal to " + toNum));
                            HasError = true;
                        }
                    }
                }
                if (MaxLength < NUM_THRESHOLD && MaxLength > 0 && s != "" && s.Length > MaxLength)
                {
                    errMsg.Append(formatErrMsg(Title + " must contain no more than " + MaxLength+ " characters"));
                    HasError = true;
                }
                if (MinLength > 0 && s != "" && s.Length < MinLength)
                {
                    errMsg.Append(formatErrMsg(Title + " must contain at least " + MinLength + " characters"));
                    HasError = true;
                }
                if (ValueNotIn != null && s != "")
                {
                    err = false;
                    string sL = s.ToLower();
                    for (int i = 0; i < ValueNotIn.Count; i++)
                    {
                        if (ValueNotIn[i].ToLower() == sL)
                        {
                            err = true;
                            break;
                        }
                    }
                    if (err)
                    {
                        errMsg.Append(formatErrMsg(Title + " already exists"));
                        HasError = true;
                    }
                }
                errorMessage = errMsg.ToString();
                return !HasError;
            }
            else
            {
                errorMessage = "";
                return true;
            }

        }


    }
}
