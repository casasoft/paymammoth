﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Newsletter
{
    public class NewsletterPageInfo
    {
        public string Title_SingleLowercase = "newsletter member";
        public string Title_SingleUppercase = "Newsletter Member";
        public string Title_PluralLowercase = "newsletter members";
        public string Title_PluralUppercase = "Newsletter Members";
        public string EditPageUrl = "member.aspx";
        public string Folder = "newsletter";
        public NewsletterPageInfo()
        { }

    }
}
