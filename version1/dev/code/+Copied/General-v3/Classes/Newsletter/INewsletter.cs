﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Newsletter
{
    public interface INewsletterMember
    {
        int ID { get; set; }
        string Name { get; set; }
        string Email { get; set; }
        DateTime DateRegistered { get; set; }
        void Save();
        void Remove();

    }
}
