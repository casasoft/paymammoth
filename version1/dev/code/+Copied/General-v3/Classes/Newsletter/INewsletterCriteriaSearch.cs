﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Newsletter
{
    public interface INewsletterCriteriaSearch
    {
        string Name { get; set; }
        string Email { get; set; }
        DateTime? DateRegisteredFrom { get; set; }
        DateTime? DateRegisteredTo { get; set; }
        void SetSortBy(string sortBy);
        

    }
}
