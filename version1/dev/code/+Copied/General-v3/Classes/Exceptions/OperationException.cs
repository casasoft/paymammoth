﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.HelperClasses;

namespace CS.General_v3.Classes.Exceptions
{
    public class OperationException : Exception
    {
        public virtual OperationResult ResultInfo { get; set; }


        public override string Message
        {
            get
            {
                return this.ResultInfo.ToString();

                
            }
        }


        public OperationException(string msg = null, CS.General_v3.Enums.STATUS_MSG_TYPE status = CS.General_v3.Enums.STATUS_MSG_TYPE.Error) 
        {
            this.ResultInfo = new OperationResult();
            this.ResultInfo.SetStatusInfo(status, msg, null, false);
        }

        public OperationException(OperationResult result)
        {
            this.ResultInfo = result;
        }
    }
}
