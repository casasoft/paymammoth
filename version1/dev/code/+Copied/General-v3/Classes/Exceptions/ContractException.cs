﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Exceptions
{
    public class ContractException :Exception
    {
        public ContractException(string errorMsg) : base(errorMsg)
        {
            
        }
    }
}
