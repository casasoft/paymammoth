﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Exceptions
{
    public class DuplicateCodeException :Exception
    {
        public DuplicateCodeException(string errorMsg)
            : base(errorMsg)
        {
            
        }
    }
}
