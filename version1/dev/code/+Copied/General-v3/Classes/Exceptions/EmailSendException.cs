﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.Exceptions
{
    public class EmailSendException : Exception
    {
        public EmailSendException() : base()
        {

        }
            public EmailSendException(string msg) : base(msg)
        {

        }
    }
}
