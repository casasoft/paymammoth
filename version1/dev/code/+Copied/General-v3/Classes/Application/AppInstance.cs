﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CS.General_v3.Classes.Application
{
    public abstract class AppInstance
    {
        public static AppInstance Instance { get; protected set; }
        public abstract IEnumerable<Assembly> GetProjectAssemblies();
        public abstract string GetSettingFromDatabase(string setting, Attributes.SettingsInfoAttribute settingInfo);
        public abstract bool IsFirstSessionStartCalled();
        public abstract bool IsCurrentUserLoggedInCms();
    }
}
