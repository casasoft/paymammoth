﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Hosting;
using log4net;
using NHibernate;
using System.Reflection;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using FluentNHibernate.Automapping;

using CS.General_v3.Classes.SettingsClasses.Other;

namespace CS.General_v3.Classes.SettingsClasses
{
    public class SettingsMainTemp : SettingsMain 
    {
        protected override T _getSettingByIdentifier<T>(string setting, Attributes.SettingsInfoAttribute defaultValues, bool createIfNotExists)
        {
            if (defaultValues == null) defaultValues = new Attributes.SettingsInfoAttribute();
            return CS.General_v3.Settings._getSettingFromFileSystem<T>(setting, defaultValues.GetDefaultValueAsString());
            
        }
        /*
        protected override long setCurrentCultureToDefault()
        {
            return 1;
            
        }*/
        /*
        public override long GetCurrentDefaultCultureDBID()
        {
            return 0;
            
        }
        */
        protected override T _getSettingByIdentifier<T>(Enum setting, bool createIfNotExists)
        {
            string id = CS.General_v3.Util.EnumUtils.StringValueOf(setting);
            return _getSettingByIdentifier<T>(id, null, createIfNotExists);
            
        }
    }
}
