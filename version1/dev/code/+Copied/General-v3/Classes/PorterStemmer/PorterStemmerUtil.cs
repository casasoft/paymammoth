﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.PorterStemmer
{
    public static class PorterStemmerUtil
    {
        public static StemmerInterface GetPorterStemmerByLanguage(string langCode)
        {
            CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 language = CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639FromCode(langCode).Value;
            return GetPorterStemmerByLanguage(language);
        }
        public static StemmerInterface GetPorterStemmerByLanguage(CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 language)
        {
            switch (language)
            {
                case Enums.ISO_ENUMS.LANGUAGE_ISO639.English:
                    return new PorterStemmerEnglish();

            }
            return null;
        }
    }
}
