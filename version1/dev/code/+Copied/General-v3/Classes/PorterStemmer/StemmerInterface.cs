﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.PorterStemmer
{
    public interface StemmerInterface
    {
        string stemTerm(string s);
    }
}
