﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CS.General_v3.JavaScript.Data;
namespace CS.General_v3.Classes.Pages
{
    public abstract class BaseAjaxFlashPage : BaseAjaxPage
    {
        public new class FUNCTIONALITY : BaseAjaxPage.FUNCTIONALITY
        {
            public FUNCTIONALITY(BaseAjaxFlashPage page)
                : base(page)
            {
                Data = new JSObjectFlash();
            }
            
        }
        public new FUNCTIONALITY Functionality { get { return (FUNCTIONALITY)base.Functionality; } }

        public BaseAjaxFlashPage()
        {
        }
        protected override BasePage.FUNCTIONALITY getFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
    }
}
