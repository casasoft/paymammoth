﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Web.UI.HtmlControls;
using System.Web.UI;
namespace CS.General_v3.Classes.Pages
{
    public abstract class BaseErrorGenericPage : BasePage
    {
        public new class FUNCTIONALITY : BasePage.FUNCTIONALITY
        {
            public FUNCTIONALITY(BaseErrorGenericPage pg):base(pg)
            {

            }

            public Control ErrorContentContainer { get; set; }
            public string ErrorURL { get; set; }
            public string ContactURL { get; set; }

            public new void Init()
            {
                if (String.IsNullOrEmpty(ErrorURL))
                {
                    ErrorURL = CS.General_v3.Util.PageUtil.GetVariableFromQuerystring("aspxerrorpath");
                }
                CS.General_v3.Controls.WebControls.Specialized.Errors.GenericErrorContent err = new CS.General_v3.Controls.WebControls.Specialized.Errors.GenericErrorContent(ErrorURL, ContactURL);
                ErrorContentContainer.Controls.Add(err);
            }
        }
        public new FUNCTIONALITY Functionality { get { return (FUNCTIONALITY)base.Functionality; } }



        public BaseErrorGenericPage()
        {
        }
        protected override BasePage.FUNCTIONALITY getFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            
            base.OnLoad(e);
            Functionality.Init();
        }
        
    }
}
