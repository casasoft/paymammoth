﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.IO;
using System.Text.RegularExpressions;
using log4net;
namespace CS.General_v3.Classes.Pages
{
    public class BasePageContextData
    {
        

        public string GenericMessage { get; set; }
        public Enums.STATUS_MSG_TYPE GenericMessageType { get; set; }

        public string LastGenericMessage { get; set; }
        public Enums.STATUS_MSG_TYPE LastGenericMessageType { get; set; }


    }
}
