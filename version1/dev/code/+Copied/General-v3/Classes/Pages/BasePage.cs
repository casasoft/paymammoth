﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.IO;
using System.Text.RegularExpressions;
using log4net;

using CS.General_v3.Util;
using CS.General_v3.Classes.Caching;
namespace CS.General_v3.Classes.Pages
{
    
    public abstract class BasePage : System.Web.UI.Page
    {
        
        private ILog _log = log4net.LogManager.GetLogger(typeof(BasePage));
        public delegate void MessageHandler(string msg, Enums.STATUS_MSG_TYPE messageType);
        public bool PerformCookieCheck { get; set; }
       
        private HtmlMeta _metaSkype;
        //<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
        private HtmlMeta _metaLanguage;

        private HtmlMeta _metaGeoRegion;
        private HtmlMeta _metaGeoPosition;
        private HtmlMeta _metaGeoPlaceName;
        private HtmlMeta _metaGeoICBM;

        private HtmlMeta _metaKeywords;
        private HtmlMeta _metaDescription;

        public class FUNCTIONALITY
        {
            /// <summary>
            /// To override any specific title setting (for recent sites)
            /// </summary>
            public bool? AlwaysDoNotIncludeWebsiteTitleInMetaTitle { get; set; }

            public BasePageContextData PageContextData
            {
                get
                {
                    if (CS.General_v3.Util.PageUtil.GetContextObject(PAGE_CONTEXT_DATA_ID) == null)
                    {
                        CS.General_v3.Util.PageUtil.SetContextObject(PAGE_CONTEXT_DATA_ID, new BasePageContextData());
                    }
                    return (BasePageContextData)CS.General_v3.Util.PageUtil.GetContextObject(PAGE_CONTEXT_DATA_ID);
                }
            }
          
            public bool HasErrorInContext
            {
                get
                {
                    

                     return PageContextData.GenericMessageType == Enums.STATUS_MSG_TYPE.Error && !string.IsNullOrEmpty(PageContextData.GenericMessage);
                    
                }
            }

            public const string PAGE_CONTEXT_DATA_ID = "pageContextDataID";
            
            public bool DisableServerCachingForAll { get; set; }
            public bool DisableServerCachingForLocalhost { get; set; }
            public event MessageHandler DisplayGenericMessage;
            //public event MessageHandler DisplaySuccessMessage;
            //public event MessageHandler DisplayInformationMessage;
            public bool? PermanentRedirectRootToWWW { get; set; }
            //private const string BASE_PAGE_ERROR = "BasePageError";
            //private const string BASE_PAGE_SUCCESS = "BasePageSuccess";
            //private const string BASE_PAGE_INFORMATION = "BasePageInformation";

            public bool SkypeChangeNumbersInapplicable { get; set; }
            
            

            
           
            
            
            public HtmlMeta MetaGeoPlacename
            {
                get
                {
                    if (_basePage._metaGeoPlaceName == null)
                    {
                        _basePage._metaGeoPlaceName = _basePage.getMetaTagFromHeader("geo.placename");
                    }
                    return _basePage._metaGeoPlaceName;
                }
            }
            public HtmlMeta MetaGeoICBM
            {
                get
                {
                    if (_basePage._metaGeoICBM == null)
                    {
                        _basePage._metaGeoICBM = _basePage.getMetaTagFromHeader("ICBM");
                    }
                    return _basePage._metaGeoICBM;
                }
            }
            public HtmlMeta MetaGeoPosition
            {
                get
                {
                    if (_basePage._metaGeoPosition == null)
                    {
                        _basePage._metaGeoPosition = _basePage.getMetaTagFromHeader("geo.position");
                    }
                    return _basePage._metaGeoPosition;
                }
            }
            public HtmlMeta MetaGeoRegion
            {
                get
                {
                    if (_basePage._metaGeoRegion == null)
                    {
                        _basePage._metaGeoRegion = _basePage.getMetaTagFromHeader("geo.region");
                    }
                    return _basePage._metaGeoRegion;
                }
            }
            public HtmlMeta MetaLanguage
            {
                get
                {
                    if (_basePage._metaLanguage == null)
                    {
                        _basePage._metaLanguage = _basePage.getMetaTagFromHeaderWithHttpEquiv("content-language");
                    }
                    return _basePage._metaLanguage;
                }
            }
            public string MetaKeywords
            {
                get
                {
                    if (_basePage._metaKeywords != null)
                    {
                        return _basePage._metaKeywords.Content;
                    }
                    return null;
                }
                set
                {
                    if (string.IsNullOrWhiteSpace(value))
                    {
                        if (_basePage._metaKeywords != null)
                        {
                            _basePage.Header.Controls.Remove(_basePage._metaKeywords);
                        }
                        _basePage._metaKeywords = null;
                    }
                    else
                    {
                        if (_basePage._metaKeywords == null)
                        {
                            _basePage._metaKeywords = _basePage.getMetaTagFromHeader("keywords");
                        }
                        _basePage._metaKeywords.Content = value;
                    }
                }
            }
            public string MetaDesc
            {
                get
                {
                    if (_basePage._metaDescription != null)
                    {
                        return _basePage._metaDescription.Content;
                    }
                    return null;
                }
                set
                {
                    if (string.IsNullOrWhiteSpace(value))
                    {
                        if (_basePage._metaDescription != null)
                        {
                            _basePage.Header.Controls.Remove(_basePage._metaDescription);
                        }
                        _basePage._metaDescription = null;
                    }
                    else
                    {
                        if (_basePage._metaDescription == null)
                        {
                            _basePage._metaDescription = _basePage.getMetaTagFromHeader("description");
                        }
                        _basePage._metaDescription.Content = value;
                    }
                }
            }

           /*
            * Commented by Mark 2010-11-25
            * public HtmlMeta MetaKeywords
            {
                get
                {
                    if (_metaKeywords == null)
                    {
                        _metaKeywords = getMetaTagFromHeader("keywords");
                        
                    }
                    return _metaKeywords;
                }
            }
            public HtmlMeta MetaDesc
            {
                get
                {
                    if (_metaDescriptions == null)
                    {
                        _metaDescriptions = getMetaTagFromHeader("description");

                    }
                    return _metaDescriptions;
                }
            }*/


            public virtual void UpdateMetaDesc(string desc, bool useOpenGraph = false) { UpdateMetaDesc(desc, true, useOpenGraph); }
            public virtual void UpdateMetaDesc(string desc, bool removeHTML, bool useOpenGraph = false)
            {
                if (removeHTML)
                {
                    desc = CS.General_v3.Util.Text.ConvertHTMLToPlainText(desc, false);
                }
                MetaDesc = desc;
                if (useOpenGraph)
                {
                    this.UpdateOpenGraphTag(Enums.OPEN_GRAPH.Description, desc);
                }
            }

            public void UpdateOpenGraphTag(CS.General_v3.Enums.OPEN_GRAPH tag, string value, bool overwrite = true)
            {
                HtmlMeta metaPropertyOG = _basePage.getOpenGraphMetaTagFromHeader(tag);

                if (metaPropertyOG != null)
                {
                    if (!string.IsNullOrWhiteSpace(value))
                    {
                        /*if (!_usingOpenGraphProtocol)
                        {
                            updateHtmlOpenGraphXMLNS();
                        }*/
                        if (overwrite || string.IsNullOrWhiteSpace(metaPropertyOG.Content))
                        {
                            metaPropertyOG.Content = value;
                        }
                    }
                    else
                    {
                        _basePage.Header.Controls.Remove(metaPropertyOG);
                    }
                }
            }
            public void UpdateOpenGraphPageType(CS.General_v3.Enums.OPEN_GRAPH_TYPE type, bool overwrite = true)
            {
                UpdateOpenGraphTag(Enums.OPEN_GRAPH.Type, EnumUtils.StringValueOf(type), overwrite);

            }
            public void UpdateFacebookTag(CS.General_v3.Enums.FACEBOOK_FBML_NAMESPACE tag, string value)
            {
                HtmlMeta metaPropertyFB = _basePage.getOpenGraphMetaTagFromHeader(tag);
                if (!string.IsNullOrWhiteSpace(value))
                {
                   /* if (!_usingFacebookProtocol)
                    {
                        updateHtmlFacebookXMLNS();
                    }*/
                    metaPropertyFB.Content = value;
                }
                else
                {
                    _basePage.Header.Controls.Remove(metaPropertyFB);
                }
            }
           
            public virtual void UpdateMetaTags(string metaKeywords, string metaDesc, bool useOpenGraph = true)
            {
                
                MetaKeywords = metaKeywords;
                MetaDesc = metaDesc;
                if (useOpenGraph)
                {
                    this.UpdateOpenGraphTag(Enums.OPEN_GRAPH.Description, metaDesc);
                }
                //MetaKeywords.Visible = !string.IsNullOrEmpty(MetaKeywords.Content);
                //MetaDesc.Visible = !string.IsNullOrEmpty(MetaDesc.Content);
            }
            public virtual void UpdateTitleAndMetaTags(string title, string metaKeywords, string metaDesc, bool includeWebsiteTitle, string websiteTitle = null, bool useOpenGraph = true)
            {
                UpdateTitle(title, includeWebsiteTitle, websiteTitle, useOpenGraph);
                UpdateMetaTags(metaKeywords, metaDesc);
                if (useOpenGraph)
                {
                    UpdateMetaDesc(metaDesc, useOpenGraph);
                }
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="pageTitle"></param>
            /// <param name="includeWebsiteTitle"></param>
            /// <param name="websiteTitle">If left null, it is taken from settings</param>
            public virtual void UpdateTitle(string pageTitle, bool includeWebsiteTitle, string websiteTitle = null, bool useOpenGraph = true)
            {
                if (AlwaysDoNotIncludeWebsiteTitleInMetaTitle.HasValue && AlwaysDoNotIncludeWebsiteTitleInMetaTitle.Value)
                {
                    includeWebsiteTitle = false;
                }
                if (websiteTitle == null)
                {
                    websiteTitle = Enums.SETTINGS_ENUM.Others_WebsiteName.GetSettingFromDatabase();;
                }
                if (useOpenGraph)
                {
                    UpdateOpenGraphTag(Enums.OPEN_GRAPH.Title, pageTitle);
                    UpdateOpenGraphTag(Enums.OPEN_GRAPH.SiteName, websiteTitle);
                }
                if (includeWebsiteTitle && !String.IsNullOrEmpty(websiteTitle))
                {
                    if (!string.IsNullOrWhiteSpace(pageTitle))
                    {
                        pageTitle += " - ";
                    }
                    pageTitle += websiteTitle;
                }                
                _basePage.updateBaseTitle(pageTitle);
               // _basePage.Title = pageTitle;
            }

            public System.Web.HttpRequest Request
            {
                get
                {
                    return System.Web.HttpContext.Current.Request;
                }
            }
            public System.Web.HttpResponse Response

            {
                get
                {
                    return System.Web.HttpContext.Current.Response;
                }
            }
            protected BasePage _basePage = null;
            public FUNCTIONALITY(BasePage basePage)
            {
                this.AlwaysDoNotIncludeWebsiteTitleInMetaTitle = true;
                
                this.DisableServerCachingForLocalhost = true;
                _basePage = basePage;
                SkypeChangeNumbersInapplicable = true;
             //   this.PageSessionData = new BasePageSessionData(basePage);

            }

            public bool RequiresSSL { get; set; }
            public void CheckSSL()
            {
                if (this.RequiresSSL && !Request.IsSecureConnection)
                {
                    var urlClass =CS.General_v3.Util.PageUtil.GetCurrentUrlLocation();
                    urlClass.UseSsl = true;
                    CS.General_v3.Util.PageUtil.PermanentRedirect(urlClass.GetURL(fullyQualified: true));
                    //CS.General_v3.Util.PageUtil.RedirectPage(CS.General_v3.Util.PageUtil.GetCurrentFullURL(true));
                }
                else if (!this.RequiresSSL && Request.IsSecureConnection)
                {
                    var urlClass =CS.General_v3.Util.PageUtil.GetCurrentUrlLocation();
                    urlClass.UseSsl = false;
                    CS.General_v3.Util.PageUtil.PermanentRedirect(urlClass.GetURL(fullyQualified: true));
                    //CS.General_v3.Util.PageUtil.RedirectPage(CS.General_v3.Util.PageUtil.GetCurrentFullURL(false));
                }
            }
            private void initSkype()
            {
                _basePage._metaSkype = _basePage.getMetaTagFromHeader("SKYPE_TOOLBAR");
                if (_basePage._metaSkype != null)
                {
                    _basePage._metaSkype.Content = "SKYPE_TOOLBAR_PARSER_COMPATIBLE";
                    _basePage._metaSkype.Visible = SkypeChangeNumbersInapplicable;
                }
            }

            internal void checkForBasePageMessage()
            {
               
                if (!string.IsNullOrEmpty(PageContextData.GenericMessage))
                {
                    if (DisplayGenericMessage != null)
                    {
                        DisplayGenericMessage(PageContextData.GenericMessage, PageContextData.GenericMessageType);
                    }
                    PageContextData.LastGenericMessage = PageContextData.GenericMessage;
                    PageContextData.LastGenericMessageType = PageContextData.GenericMessageType;
                    PageContextData.GenericMessage = null;
                    PageContextData.GenericMessageType = CS.General_v3.Enums.STATUS_MSG_TYPE.Success;
                }
                else
                {
                    PageContextData.LastGenericMessage = null;
                    PageContextData.LastGenericMessageType = CS.General_v3.Enums.STATUS_MSG_TYPE.Success;
                }
                //Session is used from different page to page
                if (!string.IsNullOrEmpty(BasePageSessionData.GenericMessage))
                {
                    if (DisplayGenericMessage != null)
                    {
                        DisplayGenericMessage(BasePageSessionData.GenericMessage, BasePageSessionData.GenericMessageType);
                    }
                    BasePageSessionData.LastGenericMessage = BasePageSessionData.GenericMessage;
                    BasePageSessionData.LastGenericMessageType = BasePageSessionData.GenericMessageType;
                    BasePageSessionData.GenericMessage = null;
                    BasePageSessionData.GenericMessageType = CS.General_v3.Enums.STATUS_MSG_TYPE.Success;

                }
                else
                {
                    BasePageSessionData.LastGenericMessage = null;
                    BasePageSessionData.LastGenericMessageType = CS.General_v3.Enums.STATUS_MSG_TYPE.Success;
                }
            }

            public void Init()
            {
                CheckSSL();
                ///MetaDesc.Visible = !string.IsNullOrEmpty(MetaDesc.Content);
                //MetaKeywords.Visible = !string.IsNullOrEmpty(MetaKeywords.Content);
                if (!string.IsNullOrEmpty(MetaKeywords))
                {
                    MetaKeywords = MetaKeywords.ToLower();
                }
                initSkype();
                //updateHtmlOpenGraphXMLNS();
            }

            
            public void ShowErrorMessage(string msg) {
                ShowGenericMessage(msg, Enums.STATUS_MSG_TYPE.Error);
            }
            public void ShowGenericMessage(string msg, CS.General_v3.Enums.STATUS_MSG_TYPE msgType)
            {
                showGenericMessage(msg, msgType, false, null);

            }
            private void showGenericMessage(string msg, CS.General_v3.Enums.STATUS_MSG_TYPE msgType, bool storeInSession, string redirectURL)
            {
                if (storeInSession)
                {
                    
                    StoreGenericMessageInSession(msg, msgType);
                    if (!string.IsNullOrEmpty(redirectURL))
                    {
                        CS.General_v3.Util.PageUtil.RedirectPage(redirectURL);
                    }

                    
                }
                else
                {
                    if (this.PageContextData.LastGenericMessage != msg || this.PageContextData.LastGenericMessageType != msgType)
                    {
                        this.PageContextData.GenericMessage = msg;
                        this.PageContextData.GenericMessageType = msgType;
                    }
                }
            }
            public static void StoreGenericMessageInSession(string msg, CS.General_v3.Enums.STATUS_MSG_TYPE msgType)
            {
                BasePageSessionData.GenericMessage = msg;
                BasePageSessionData.GenericMessageType = msgType;
            }
            public void ShowGenericMessageAndRedirectToPage(string msg, CS.General_v3.Enums.STATUS_MSG_TYPE msgType, string redirectURL)
            {
                showGenericMessage(msg, msgType, true, redirectURL);
                
            }
            /// <summary>
            /// This will NOT redirect to page but will wait for next redirect by someone else, used e.g. when need to change window.top
            /// </summary>
            /// <param name="msg"></param>
            public void ShowErrorMessageForNextRedirectToPage(string msg)
            {
                showGenericMessage(msg, Enums.STATUS_MSG_TYPE.Error, true, null);
            }
            public void ShowSuccessMessageForNextRedirectToPage(string msg)
            {
                showGenericMessage(msg, Enums.STATUS_MSG_TYPE.Success, true, null);
            }
            public void ShowInformationMessageForNextRedirectToPage(string msg)
            {
                showGenericMessage(msg, Enums.STATUS_MSG_TYPE.Information, true, null);
            }
            public void ShowErrorMessageAndRedirectToPage(string msg, string redirectURL = null)
            {
                if (redirectURL == null) redirectURL = CS.General_v3.Util.PageUtil.GetCurrentUrlLocation().GetURL(fullyQualified: false);
                showGenericMessage(msg, Enums.STATUS_MSG_TYPE.Error, true, redirectURL);
            }
            public void ShowSuccessMessageAndRedirectToPage(string msg, string redirectURL = null)
            {
                if (redirectURL == null) redirectURL = CS.General_v3.Util.PageUtil.GetCurrentUrlLocation().GetURL(fullyQualified: false);
                showGenericMessage(msg, Enums.STATUS_MSG_TYPE.Success, true, redirectURL);
            }
            public void ShowInformationMessageAndRedirectToPage(string msg, string redirectURL = null)
            {
                if (redirectURL == null) redirectURL = CS.General_v3.Util.PageUtil.GetCurrentUrlLocation().GetURL(fullyQualified: false);
                showGenericMessage(msg, Enums.STATUS_MSG_TYPE.Information, true, redirectURL);
            }
           /* public void ShowErrorMessage(string msg, string pageRedirect) {
                ShowGenericMessage(msg, CS.General_v3.Enums.STATUS_MSG_TYPE.Error, pageRedirect);
            }*/

            public void ShowSuccessMessage(string msg) {
                ShowGenericMessage(msg, Enums.STATUS_MSG_TYPE.Success);
            }
            
           /* public void ShowSuccessMessage(string msg, string pageRedirect) {
                ShowGenericMessage(msg, CS.General_v3.Enums.STATUS_MSG_TYPE.Success, pageRedirect);
                
            }*/

            public void ShowInformationMessage(string msg) {
                ShowGenericMessage(msg, Enums.STATUS_MSG_TYPE.Information);
            }

          /*  public void ShowInformationMessage(string msg, string pageRedirect) {
                ShowGenericMessage(msg, CS.General_v3.Enums.STATUS_MSG_TYPE.Information, pageRedirect);
            }*/

            

            /*
            private Classes.MultiLingual.Language _CurrentLanguage = null;
            public virtual Classes.MultiLingual.Language CurrentLanguage
            {
                get
                {


                    if (_CurrentLanguage == null)
                    {
                        string code = CS.General_v3.Util.PageUtil.GetCookie(currentLanguageCookie);
                        if (!string.IsNullOrEmpty(code))
                        {
                            _CurrentLanguage = General.Classes.MultiLingual.LanguageList.Instance.GetLanguageByCode(code);
                        }
                        if (_CurrentLanguage == null)
                            _CurrentLanguage = General.Classes.MultiLingual.LanguageList.Instance.GetDefaultLanguage();
                    }
                    return _CurrentLanguage;
                }
                set
                {
                    _CurrentLanguage = value;
                    CS.General_v3.Util.PageUtil.SetCookie(currentLanguageCookie, (_CurrentLanguage != null ? _CurrentLanguage.Code : ""));
                }
            }
            public void SetLanguage(string code)
            {
                CurrentLanguage = LanguageList.Instance.GetLanguageByCode(code);
            }*/
        }
        public FUNCTIONALITY Functionality { get; private set; }
        public virtual void InitParameters()
        {
            

        }
        
        
        public bool ProcessFormAutomatically { get; set; }
        
        protected virtual FUNCTIONALITY getFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        public BasePage()
        {
           // this.AutoTransferToItselfIfPostback = true;
            this.PerformCookieCheck = true;

            //if (_log.IsInfoEnabled) _log.Debug("ConstructorStart");
            this.Functionality = getFunctionality();
            ProcessFormAutomatically = true;
            CS.General_v3.Util.PageUtil.SetCurrentPage(this);
            this.Init += new EventHandler(BasePage_Init);
            //if (_log.IsInfoEnabled) _log.Debug("ConstructorEnd");
            this.PreRenderComplete += new EventHandler(BasePage_PreRenderComplete);
        }

        void BasePage_PreRenderComplete(object sender, EventArgs e)
        {
            int k = 5;
        }
        public override void RenderControl(HtmlTextWriter writer)
        {
            base.RenderControl(writer);
        }
        protected override void RenderChildren(HtmlTextWriter writer)
        {
            base.RenderChildren(writer);
            //checkPostback();
        }
        
        protected override void OnUnload(EventArgs e)
        {
            base.OnUnload(e);
            
        }
        

        void BasePage_Init(object sender, EventArgs e)
        {
            this.EnableViewState = false;
            
        }

        protected override void OnPreInit(EventArgs e)
        {
            if (PerformCookieCheck)
                PageUtil.CheckFromPageWhetherBrowserSupportsCookies();
            
            CS.General_v3.Util.PageUtil.WriteCookieToCheckIfCookiesAreEnabled();

            CacheInvalidator.Instance.CheckCacheToInvalidate();
            base.OnPreInit(e);
        }
        protected override void OnPreLoad(EventArgs e)
        {
            //setJavaScriptOverrideValues();
            
            base.OnPreLoad(e);
        }
        private static void CheckUrlWhetherItRequiresRedirect_Test()
        {
            bool redirect = CheckUrlWhetherItRequiresRedirect("http://www.karlcassar.com");
            redirect = CheckUrlWhetherItRequiresRedirect("http://karlcassar.com");
            redirect = CheckUrlWhetherItRequiresRedirect("karlcassar.com");
            redirect = CheckUrlWhetherItRequiresRedirect("localhost");
            redirect = CheckUrlWhetherItRequiresRedirect("fadsfasd");
            redirect = CheckUrlWhetherItRequiresRedirect("127.0.0.1");
            redirect = CheckUrlWhetherItRequiresRedirect("http://localhost");
            redirect = CheckUrlWhetherItRequiresRedirect("http://localhost/");


        }

        public static bool CheckUrlWhetherItRequiresRedirect(string url)
        {
            
            Match match = Regex.Match(url, "(https?://)(.*)");
            string urlOnly = url;
            if (match.Success)
                urlOnly = match.Groups[2].Value;

            //here use regexsd
            string regExpPattern = @"^((http|https)://)?((www[0-9]?|test|new|dev|old|staging|[0-9]{1,3})\.|localhost|127\.0\.0\.1)";
            bool redirect=  false;
            if (!Regex.IsMatch(urlOnly, regExpPattern, RegexOptions.IgnoreCase))
            {//redirect required
                redirect  = true;
            }
            return redirect;
        }
        private void checkForRootToWWWPermanentRedirect()
        {
            bool redirectToWWW = false;
            if (this.Functionality.PermanentRedirectRootToWWW.HasValue)
            {
                redirectToWWW = this.Functionality.PermanentRedirectRootToWWW.Value;
            }
            else
            {
                //Load from settings
                redirectToWWW = CS.General_v3.Settings.GetSettingFromDatabase<bool>(CS.General_v3.Enums.SETTINGS_ENUM.Others_PermanentRedirectionUrlsToWWW);
            }

            if (redirectToWWW && !CS.General_v3.Util.Other.IsLocalTestingMachine)
            {
                
                var urlClass = CS.General_v3.Util.PageUtil.GetCurrentUrlLocation();
                string url = urlClass.GetURL(fullyQualified: true);

                //Changed by Mark on 2011-08-11 as GetCurrentFullURL uses GetBaseURL which interfaces with settings which is not good for this case
                //string url = CS.General_v3.Util.PageUtil.GetCurrentRequest().Url.AbsoluteUri;
                Match match = Regex.Match(url, "(https?://)(.*)");
                string urlOnly = url;
                if (match.Success)
                    urlOnly = match.Groups[2].Value;
                bool redirect = CheckUrlWhetherItRequiresRedirect(urlOnly);

                bool removeDefaultAspx = CS.General_v3.Settings.GetSettingFromDatabase<bool>(CS.General_v3.Enums.SETTINGS_ENUM.Others_RemoveDefaultPageFromWWWRedirect);

                string newURL = match.Groups[1].Value + "www." + urlOnly;
                string defaultAspxFileName = "default.aspx";
                if (removeDefaultAspx)
                {
                    if (newURL.EndsWith(defaultAspxFileName))
                    {
                        newURL = newURL.SubStr(0, (newURL.Length - defaultAspxFileName.Length) - 1);
                    }
                }

                if (redirect)
                {
                    CS.General_v3.Util.PageUtil.PermanentRedirect(newURL);
                }
            }
        }
        private void checkCaching()
        {
            if (this.Functionality.DisableServerCachingForAll || (CS.General_v3.Util.Other.IsLocalTestingMachine && this.Functionality.DisableServerCachingForLocalhost))
            {
                Response.Cache.SetNoServerCaching();
            }
        }
        private void updateHtmlOpenGraphXMLNS()
        {
            /*if (!_usingOpenGraphProtocol)
            {
            */
            if (HtmlTag != null)
            {//[2011/Oct/28 - Karl added the null check
                HtmlTag.Attributes.Add("xmlns:og", "http://ogp.me/ns#");
            }
           // }
        }

        private void updateHtmlFacebookXMLNS()
        {
           /* if (!_usingFacebookProtocol)
            {*/
            if (HtmlTag != null)
            { //[2011/Oct/28 - Karl added the null check
                HtmlTag.Attributes.Add("xmlns:fb", "http://www.facebook.com/2008/fbml");
            }
            //}
        }
        private void updateHtmlTagNamespaces()
        {
            updateHtmlFacebookXMLNS();
            updateHtmlOpenGraphXMLNS();
        }

        protected override void OnLoad(EventArgs e)
        {
            checkCaching();
            checkForRootToWWWPermanentRedirect();
            updateHtmlTagNamespaces();
            checkForErrorGenerationForTest();
            InitParameters();
            initDefaultFacebookTags();
            initDefaultOpenGraphTags();
            initDefaultGeoLocationTags();
            this.Functionality.Init();

            base.OnLoad(e);
        }
        
        private void parseHTMLForSSL(HtmlTextWriter writer)
        {
            StringWriter sw = new StringWriter();
            HtmlTextWriter htmlTW = new HtmlTextWriter(sw);
            base.Render(htmlTW);
            string htmlOutput = sw.GetStringBuilder().ToString();

            //E.g. http://www.bet-at.eu";
            var domain = CS.General_v3.Util.PageUtil.GetApplicationBaseUrl().GetURL(fullyQualified:true);
            //E.g. www.bet-at.eu
            domain = CS.General_v3.Util.PageUtil.GetDomainFromUrl(domain);
            //Replace links which do not start with the domain
            System.Text.RegularExpressions.Regex rExternalLinks = new System.Text.RegularExpressions.Regex("<a(.*?)href=(['\"])(http://)(?!"+domain+")(.*?)(['\"])", System.Text.RegularExpressions.RegexOptions.Singleline | System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            //Temporarily with this
            string replaceTempWith = "!!#_##_##_##!!";            
            htmlOutput = rExternalLinks.Replace(htmlOutput, "<a$1href=$2" + replaceTempWith + "$4$5");
            //Replace all http to https
            htmlOutput = Regex.Replace(htmlOutput, "http://", "https://", RegexOptions.Singleline | RegexOptions.IgnoreCase);
            //Replace external links back
            htmlOutput = htmlOutput.Replace(replaceTempWith, "http://"); // back to where they were

            writer.Write(htmlOutput);

        }
        protected override void Render(HtmlTextWriter writer)
        {
            if (this.Functionality.RequiresSSL)
            {
                parseHTMLForSSL(writer);
            }
            else
            {
                base.Render(writer);
            }
        }

        private void updateMetaTagsPosition()
        {

            IEnumerable<Control> metaTags = CS.General_v3.Util.ControlUtil.GetControlsFromHtmlTagName<Control>(this.Header, "meta");
            //Place them all at top
            foreach (var metaTag in metaTags)
            {
                this.Header.Controls.Remove(metaTag);
                this.Header.Controls.AddAt(0, metaTag);
            }

            //Override description on top
            if (_metaDescription != null)
            {
                this.Header.Controls.Remove(_metaDescription);
                this.Header.Controls.AddAt(0, _metaDescription);
            }

            //Override keywords on top
            if (_metaKeywords != null)
            {
                this.Header.Controls.Remove(_metaKeywords);
                this.Header.Controls.AddAt(0, _metaKeywords);
            }

            //Override title on top
            Control titleTag = CS.General_v3.Util.ControlUtil.GetFirstControlFromHtmlTagName<Control>(this.Header, "title");
            if (titleTag != null)
            {
                this.Header.Controls.Remove(titleTag);
                this.Header.Controls.AddAt(0, titleTag);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            this.Functionality.checkForBasePageMessage();
            if (this.Form != null)
                CS.General_v3.Util.Forms.ProcessForm(this.Form, true);
            updateMetaTagsPosition();
            base.OnPreRender(e);
        }
        private void initDefaultGeoLocationTags()
        {

            double? lat = CS.General_v3.Settings.GetSettingFromDatabase<double?>(CS.General_v3.Enums.SETTINGS_ENUM.CompanyInfo_GoogleMaps_Latitude);
            double? lng = CS.General_v3.Settings.GetSettingFromDatabase<double?>(CS.General_v3.Enums.SETTINGS_ENUM.CompanyInfo_GoogleMaps_Longtitude);
            if (lat.HasValue && lng.HasValue)
            {
                if (string.IsNullOrWhiteSpace(this.Functionality.MetaGeoICBM.Content) && string.IsNullOrWhiteSpace(this.Functionality.MetaGeoPosition.Content))
                {
                    this.Functionality.MetaGeoICBM.Content = this.Functionality.MetaGeoPosition.Content = lat.Value + ", " + lng.Value;
                }
            }
            if (this.Functionality.MetaGeoRegion != null && string.IsNullOrWhiteSpace(this.Functionality.MetaGeoRegion.Content))
            {
                string countryCode3Letter = CS.General_v3.Settings.GetSettingFromDatabase<string>(CS.General_v3.Enums.SETTINGS_ENUM.CompanyInfo_ContactDetails_Country3LetterCode);
                var countryCode = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_FromCode(countryCode3Letter);
                if (countryCode.HasValue)
                {
                    this.Functionality.MetaGeoRegion.Content = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_To2LetterCode(countryCode.Value);
                    this.Functionality.MetaGeoPlacename.Content = CS.General_v3.Util.EnumUtils.StringValueOf(countryCode.Value);
                }
            }
        }
        private void initDefaultOpenGraphTags()
        {
            string url = CS.General_v3.Util.PageUtil.GetCurrentUrlLocation().GetURL(fullyQualified: true);
            //og:url
            this.Functionality.UpdateOpenGraphTag(Enums.OPEN_GRAPH.URL, url);
            var openGraphMetaTag = getOpenGraphMetaTagFromHeader(Enums.OPEN_GRAPH.URL);
            if (!string.IsNullOrEmpty(url) && openGraphMetaTag != null && string.IsNullOrEmpty(openGraphMetaTag.Content)) this.Functionality.UpdateOpenGraphTag( Enums.OPEN_GRAPH.URL, url);

            //og:title
            if (openGraphMetaTag != null && string.IsNullOrEmpty(openGraphMetaTag.Content))
            {
                HtmlTitle titleTag = CS.General_v3.Util.ControlUtil.GetFirstControlFromHtmlTagName<HtmlTitle>(this.Header, "title");
                if (titleTag != null)
                {
                    this.Functionality.UpdateOpenGraphTag(Enums.OPEN_GRAPH.Title, titleTag.Text);
                }
            }
        }

        private void initDefaultFacebookTags()
        {
            string pageID = CS.General_v3.Settings.GetSettingFromDatabase<string>(CS.General_v3.Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Facebook_PageID);
            string adminsID = CS.General_v3.Settings.GetSettingFromDatabase<string>(CS.General_v3.Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Facebook_Admins);
            string appID = CS.General_v3.Settings.GetSettingFromDatabase<string>(CS.General_v3.Enums.SETTINGS_ENUM.CompanyInfo_SocialNetworking_Facebook_AppID);
            if (!string.IsNullOrEmpty(pageID) && string.IsNullOrEmpty(getOpenGraphMetaTagFromHeader(Enums.FACEBOOK_FBML_NAMESPACE.PageID).Content)) this.Functionality.UpdateFacebookTag(Enums.FACEBOOK_FBML_NAMESPACE.PageID, pageID);
            if (!string.IsNullOrEmpty(adminsID) && string.IsNullOrEmpty(getOpenGraphMetaTagFromHeader(Enums.FACEBOOK_FBML_NAMESPACE.Admins).Content)) this.Functionality.UpdateFacebookTag(Enums.FACEBOOK_FBML_NAMESPACE.Admins, adminsID);
            if (!string.IsNullOrEmpty(appID) && string.IsNullOrEmpty(getOpenGraphMetaTagFromHeader(Enums.FACEBOOK_FBML_NAMESPACE.AppID).Content)) this.Functionality.UpdateFacebookTag(Enums.FACEBOOK_FBML_NAMESPACE.AppID, appID);
            this.Functionality.UpdateOpenGraphPageType(Enums.OPEN_GRAPH_TYPE.Website, false);
        }

        private void checkForErrorGenerationForTest()
        {
            if (CS.General_v3.Util.PageUtil.GetVariableFromQuerystring<bool>("GenerateError"))
            {
                throw new InvalidOperationException("Test Error Generated at " + CS.General_v3.Util.Date.Now.ToString("dd/MM/yyyy hh:mm tt"));
            }

           
        }


        protected HtmlMeta getMetaTagFromHeader(string name)
        {
            string origName = name;
            name = name.ToLower();
            if (Header != null)
            {
                for (int i = 0; i < Header.Controls.Count; i++)
                {
                    if (Header.Controls[i] is HtmlMeta)
                    {
                        HtmlMeta metaControl = (HtmlMeta)Header.Controls[i];

                        if (metaControl.Name.ToLower() == name)
                        {
                            return metaControl;
                        }
                    }
                }
                //Still not found so add them
                HtmlMeta meta = new HtmlMeta();
                meta.Name = origName;
                Header.Controls.Add(meta);
                return meta;
            }
            else
            {
                return null;
            }
            

        }
        private HtmlMeta getOpenGraphMetaTagFromHeader(Enums.FACEBOOK_FBML_NAMESPACE tag)
        {
            return getOpenGraphMetaTagFromHeader(CS.General_v3.Util.EnumUtils.StringValueOf(tag));
        }
        private HtmlMeta getOpenGraphMetaTagFromHeader(string tag)
        {
            HtmlMeta metaPropertyOG = getMetaTagFromHeaderFromAttribute("property", tag);
            return metaPropertyOG;
        }
        private HtmlMeta getOpenGraphMetaTagFromHeader(Enums.OPEN_GRAPH tag)
        {
            return getOpenGraphMetaTagFromHeader(CS.General_v3.Util.EnumUtils.StringValueOf(tag));
        }

        private HtmlMeta getMetaTagFromHeaderFromAttribute(string attribute, string value)
        {
            attribute = attribute.ToLower();
            HtmlMeta metaTag = null;
            if (Header != null)
            {
                for (int i = 0; i < Header.Controls.Count; i++)
                {
                    if (Header.Controls[i] is HtmlMeta)
                    {
                        HtmlMeta metaControl = (HtmlMeta) Header.Controls[i];
                        if (metaControl.Attributes[attribute] == value)
                        {
                            metaTag = metaControl;
                        }
                    }
                }
                if (metaTag == null)
                {
                    //Still not found so add them
                    metaTag = new HtmlMeta();
                    metaTag.Attributes[attribute] = value;
                    //meta.Content = value;
                    Header.Controls.AddAt(1, metaTag);
                }
            }
            return metaTag;
        }
        private HtmlMeta getMetaTagFromHeaderWithHttpEquiv(string name)
        {
            string origName = name;
            name = name.ToLower();
            for (int i = 0; i < Header.Controls.Count; i++)
            {
                if (Header.Controls[i] is HtmlMeta)
                {
                    HtmlMeta metaControl = (HtmlMeta)Header.Controls[i];

                    if (metaControl.HttpEquiv.ToLower() == name)
                    {
                        return metaControl;
                    }
                }
            }

            //Still not found so add them
            HtmlMeta meta = new HtmlMeta();
            meta.HttpEquiv = origName;

            Header.Controls.Add(meta);
            return meta;

        }
        private void updateBaseTitle(string title)
        {
            base.Title = title;

            //if (base.Title != title && CS.General_v3.Util.Other.IsLocalTestingMachine)
            //{
            //    throw new NotSupportedException("You cannot change title of page at this state, e.g. with OnPreRender");
            //}

        }

        public abstract HtmlGenericControl HtmlTag { get; }

        public new BaseMasterPage Master { get { return (BaseMasterPage)base.Master; } }

        /// <summary>
        /// To update title, use Functionality.UpdateTitle
        /// </summary>
        public new string Title
        {
            get { return base.Title; }
            set
            {
                base.Title = value;
                //this.Functionality.UpdateTitle(value, false);

            }
        }    

       
        

    }
}
