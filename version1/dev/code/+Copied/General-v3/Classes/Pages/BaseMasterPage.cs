﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;

namespace CS.General_v3.Classes.Pages 
{
    public abstract class BaseMasterPage : System.Web.UI.MasterPage
    {
        public class FUNCTIONALITY
        {
            protected BaseMasterPage _masterPage;
            public FUNCTIONALITY(BaseMasterPage masterPage)
            {
                _masterPage = masterPage;
            }
            //public string LastPageURL
            //{
            //    get
            //    {
            //        return (string)CS.General_v3.Util.SessionUtil.GetObject("_LastPageURL");
            //    }
            //    set
            //    {
            //        CS.General_v3.Util.SessionUtil.SetObject("_LastPageURL", value);
            //    }

            //}

        }

        protected virtual FUNCTIONALITY createFunctionality()
        {
            return new FUNCTIONALITY(this);
        }

        public FUNCTIONALITY Functionality {get;private set;}
        
        protected virtual void initControls()
        {
            
        }

        public BaseMasterPage()
        {
            this.Functionality = createFunctionality();
            
        }

        protected override void OnInit(EventArgs e)
        {
            //this.Functionality.LastPageURL = CS.General_v3.Util.PageUtil.GetUrlLocationAndQuerystring();
            base.OnInit(e);
        }


        public abstract HtmlGenericControl HtmlTag { get; }

        public new BasePage Page
        {
            get
            {
                return
                    (BasePage)base.Page;
            }
        }
    }
}
