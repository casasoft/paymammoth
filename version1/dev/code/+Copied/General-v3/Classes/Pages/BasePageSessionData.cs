﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.IO;
using System.Text.RegularExpressions;
using log4net;
namespace CS.General_v3.Classes.Pages
{
    public static class BasePageSessionData
    {
        private static string getSessionID(string s)
        {
            return "BasePageSessionData_" + s;
        }

        public static string GenericMessage
        {
            get { return CS.General_v3.Util.PageUtil.GetSessionObject<string>(getSessionID("GenericMessage")); }
            set { CS.General_v3.Util.PageUtil.SetSessionObject(getSessionID("GenericMessage"), value); }
        }
        public static Enums.STATUS_MSG_TYPE GenericMessageType
        {
            get { return CS.General_v3.Util.PageUtil.GetSessionObject<Enums.STATUS_MSG_TYPE>(getSessionID("GenericMessageType")); }
            set { CS.General_v3.Util.PageUtil.SetSessionObject(getSessionID("GenericMessageType"), value); }
        }

        public static string LastGenericMessage 
        {
            get { return CS.General_v3.Util.PageUtil.GetSessionObject<string>(getSessionID("LastGenericMessage")); }
            set { CS.General_v3.Util.PageUtil.SetSessionObject(getSessionID("LastGenericMessage"), value); }
        }
        public static Enums.STATUS_MSG_TYPE LastGenericMessageType
        {
            get { return CS.General_v3.Util.PageUtil.GetSessionObject<Enums.STATUS_MSG_TYPE>(getSessionID("LastGenericMessageType")); }
            set { CS.General_v3.Util.PageUtil.SetSessionObject(getSessionID("LastGenericMessageType"), value); }
        }

        public static string LastNoAccessURL
        {
            get { return CS.General_v3.Util.PageUtil.GetSessionObject<string>(getSessionID("LastNoAccessURL")); }
            set { CS.General_v3.Util.PageUtil.SetSessionObject(getSessionID("LastNoAccessURL"), value); }
        }
        
    }
}
