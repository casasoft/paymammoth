﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CS.General_v3.JavaScript.Data;
using CS.General_v3.JavaScript.Interfaces;
using System.Collections.Specialized;
using System.Collections;
namespace CS.General_v3.Classes.Pages
{
    public abstract class BaseAjaxPage : BasePage
    {
        public new class FUNCTIONALITY : BasePage.FUNCTIONALITY
        {
            /// <summary>
            /// The data which is output
            /// </summary>
            public JSObject Data { get; set; }
            /// <summary>
            /// If you set this string, then it will output only this string, nothing more.
            /// </summary>
            public string DataString { get; set; }
            public FUNCTIONALITY(BaseAjaxPage page)
                : base(page)
            {
                Data = new JSObject();
                Data.AddQuotationForVariableNames = true;
            }

            public new void Init()
            {
                _basePage.Response.Clear();
                if (Data != null || !string.IsNullOrEmpty(DataString))
                {
                    if (!string.IsNullOrEmpty(DataString))
                    {
                        _basePage.Response.Write(DataString);
                    }
                    else
                    {
                        _basePage.Response.Write(Data.GetJS());
                    }
                }
                _basePage.Response.Flush();
                _basePage.Response.End();
            }
            
        }
        public new FUNCTIONALITY Functionality { get { return (FUNCTIONALITY)base.Functionality; } }

        public BaseAjaxPage()
        {
        }

       
        protected override BasePage.FUNCTIONALITY getFunctionality()
        {
            return new FUNCTIONALITY(this);
        }
        protected override void OnLoad(EventArgs e)
        {
            
            base.OnLoad(e);
            Functionality.Init();
            
        }

        public void AddProperty(string id, DateTime value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            
            this.Functionality.Data.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, DateTime? value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            this.Functionality.Data.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, TimeSpan value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            this.Functionality.Data.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, TimeSpan? value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            this.Functionality.Data.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, bool value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            this.Functionality.Data.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, bool? value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            this.Functionality.Data.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IJavaScriptSerializable value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            this.Functionality.Data.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        
        public void AddProperty(string id, double value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            this.Functionality.Data.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, double? value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            this.Functionality.Data.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, int value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            this.Functionality.Data.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, int? value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            this.Functionality.Data.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, float value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            this.Functionality.Data.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, long value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            this.Functionality.Data.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        /* public void AddProperty(string id, Int32 value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
         {
             addProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
         }
         public void AddProperty(string id, Int64 value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
         {
             addProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
         }*/
        public void AddProperty(string id, Int16 value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            this.Functionality.Data.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, string value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            this.Functionality.Data.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IJavaScriptObject value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            this.Functionality.Data.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, NameValueCollection value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            this.Functionality.Data.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }




        public void AddProperty(string id, IEnumerable<int> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            this.Functionality.Data.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<string> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            this.Functionality.Data.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<long> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            this.Functionality.Data.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<double> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            this.Functionality.Data.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<DateTime> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            this.Functionality.Data.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<IJavaScriptObject> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            this.Functionality.Data.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<IJavaScriptSerializable> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            this.Functionality.Data.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<NameValueCollection> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            this.Functionality.Data.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<Int16> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            this.Functionality.Data.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<int?> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            this.Functionality.Data.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
        public void AddProperty(string id, IEnumerable<double?> value, bool? addQuotationMarks = null, bool dontAddIfNull = false, bool escapeJSCharacters = true)
        {
            this.Functionality.Data.AddProperty(id, value, addQuotationMarks, dontAddIfNull, escapeJSCharacters);
        }
       

        /*
        public object this[string index]
        {
            get
            {
                return this.Functionality.Data[index];
            }
            set
            {
                this.Functionality.Data[index] = value;
            }
        }*/
        
    }
}
