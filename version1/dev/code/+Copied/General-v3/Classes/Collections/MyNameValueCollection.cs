﻿using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.JavaScript.Interfaces;
using CS.General_v3.JavaScript.Data;

namespace CS.General_v3.Classes.Collections
{
    using JavaScript.Data;

    public class MyNameValueCollection : NameValueCollection, IJavaScriptObject
    {
        public static MyNameValueCollection GetQueryString()
        {
            return new MyNameValueCollection(CS.General_v3.Util.PageUtil.GetRequestQueryString());
        }
        public static MyNameValueCollection GetForm()
        {
            return new MyNameValueCollection(CS.General_v3.Util.PageUtil.GetRequestForm());
        }
        public MyNameValueCollection()
        {

        }
        public MyNameValueCollection(NameValueCollection nv)
        {
            for (int i = 0; i < nv.Count; i++)
            {
                this[nv.AllKeys[i]] = nv[i];
            }
        }

        public int GetInt(string key)
        {
            return GetInt(key, 0);
        }
        public int GetInt(string key, int defaultNum)
        {
            string s = this[key];
            int num;
            if (!Int32.TryParse(s, out num))
                num = defaultNum;
            return num;
        }
        public double GetDouble(string key)
        {
            return GetDouble(key, 0);
        }
        public double GetDouble(string key, double defaultNum)
        {
            string s = this[key];
            double num;
            if (!Double.TryParse(s, out num))
                num = defaultNum;
            return num;
        }
        public bool GetBool(string key, bool defaultNum)
        {
            string s = this[key];
            bool val = CS.General_v3.Util.Other.TextToBool(s);
            return val;
        }

        #region IJavaScriptObject Members

        public IJavaScriptSerializable GetJsObject(bool forFlash = false)
        {
            JSArray arr = forFlash ? new JSArrayFlash() : new JSArray();
            for (int i = 0; i < this.Keys.Count; i++)
            {
                JSObject objNameValue = forFlash ? new JSObjectFlash() : new JSObject();
                objNameValue.AddProperty("name", this.Keys[i]);
                objNameValue.AddProperty("value", this[i]);
                arr.AddItem(objNameValue);
                

            }
            return arr;
        }

        #endregion
    }
}
