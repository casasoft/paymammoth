﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace CS.General_v3.Classes.RSS.v2
{
    public class RSSFeedChannelImage
    {
        /// <summary>
        /// This is the URL of a GIF, JPEG or PNG image that represents the channel.
        /// </summary>
        public string URL { get; set; }
       
        /// <summary>
        /// This describes the image, it's used in the ALT attribute of the HTML <img> tag when the channel is rendered in HTML.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// This is the URL of the site, when the channel is rendered, the image is a link to the site. 
        /// (Note, in practice the image <title> and <link> should have the same value as the channel's <title> and <link>
        /// </summary>
        public string Link { get; set; }

        /// <summary>
        /// Width in pixels
        /// </summary>
        public string Width { get; set; }

        /// <summary>
        /// Height in pixels
        /// </summary>
        public string Height { get; set; }

        /// <summary>
        /// This is the text that is included in the TITLE attribute of the link formed around the image in the HTML rendering.
        /// </summary>
        public string Description { get; set; }

        public RSSFeedChannelImage(string url, string title, string link)
        {
            this.URL = url;
            this.Title = title;
            this.Link = link;
        }

        public XElement GetAsXElement()
        {
            XElement elem = new XElement("image");
            elem.Add(new XElement("url", URL));
            elem.Add(new XElement("title", Title));
            elem.Add(new XElement("link", Link));

            initOptionalElements(elem);
            return elem;
        }

        private void initOptionalElements(XElement elem)
        {
            if (!String.IsNullOrEmpty(Width))
            {
                elem.Add(new XElement("width", Width));
            }
            if (!String.IsNullOrEmpty(Height))
            {
                elem.Add(new XElement("height", Height));
            }
            if (!String.IsNullOrEmpty(Description)) ;
            {
                elem.Add(new XElement("description", Description));
            }
        }
    }
}
