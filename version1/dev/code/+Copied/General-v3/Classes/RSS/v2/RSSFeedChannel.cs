﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace CS.General_v3.Classes.RSS.v2
{
    public class RSSFeedChannel
    {
        /// <summary>
        /// The name of the channel. It's how people refer to your service. If you have an HTML website that contains the same information
        /// as your RSS file, the title of your channel should be the same as the title of your website.
        /// </summary>
        public string Title { get; set; }
        
        /// <summary>
        /// The URL to the HTML website corresponding to the channel.
        /// </summary>
        public string Link { get; set; }
        
        /// <summary>
        /// Phrase or sentence describing the channel.
        /// </summary>
        public string Description { get; set; }
      
        /// <summary>
        /// The language the channel is written in. This allows aggregators to group all Italian language sites, for example, on a single page. 
        /// A list of allowable values for this element, as provided by Netscape, is here. You may also use values defined by the W3C.
        /// </summary>
        public string Language { get; set; }

        /// <summary>
        /// Copyright notice for content in the channel.
        /// </summary>
        public string Copyright { get; set; }

        /// <summary>
        /// Email address for person responsible for editorial content.
        /// </summary>
        public string ManagingEditor { get; set; }

        /// <summary>
        /// Email address for person responsible for technical issues relating to channel.
        /// </summary>
        public string WebMaster { get; set; }

        /// <summary>
        /// The publication date for the content in the channel. For example, the New York Times publishes on a daily basis,
        /// the publication date flips once every 24 hours. That's when the pubDate of the channel changes. All date-times 
        /// in RSS conform to the Date and Time Specification of RFC 822, with the exception that the year may be expressed 
        /// with two characters or four characters (four preferred).
        /// </summary>
        public DateTime? PubDate { get; set; }

        /// <summary>
        /// The last time the content of the channel changed.
        /// </summary>
        public DateTime? LastBuildDate { get; set; }

        /// <summary>
        /// Specify one or more categories that the channel belongs to. Follows the same rules as the <item>-level category element. 
        /// </summary>
        public string Category { get; set; }
        public List<RSSFeedChannelItem> ChannelItems { get; set; }

        public RSSFeedChannelImage Image { get; set; }
        public RSSFeedChannel(string title, string link, string description)
        {
            this.Title = title;
            this.Link = link;
            this.Description = description;
            this.ChannelItems = new List<RSSFeedChannelItem>();
        }

        public XElement GetAsXElement()
        {



            XElement rssElement = new XElement("rss", new XAttribute("version", "2.0"));

            XElement elem = new XElement("channel");
            elem.Add(new XElement("title", Title));
            elem.Add(new XElement("link", Link));
            elem.Add(new XElement("description", Description));
            if (this.Image != null)
            {
                elem.Add(this.Image.GetAsXElement());
            }
            initOptionalElements(elem);
            addChildItems(elem);
            rssElement.Add(elem);
            return rssElement;
        }
        private void addChildItems(XElement elem)
        {
            if (this.ChannelItems != null)
            {
                foreach (var channelItem in this.ChannelItems)
                {
                    elem.Add(channelItem.GetAsXElement());
                }
            }
        }

        private void initOptionalElements(XElement elem)
        {
            if (!String.IsNullOrEmpty(Language))
            {
                elem.Add(new XElement("language", Language));
            }
            if (!String.IsNullOrEmpty(Copyright))
            {
                elem.Add(new XElement("copyright", Copyright));
            }
            if (!String.IsNullOrEmpty(ManagingEditor))
            {
                elem.Add(new XElement("managingeditor", ManagingEditor));
            }
            if (!String.IsNullOrEmpty(WebMaster))
            {
                elem.Add(new XElement("webmaster", WebMaster));
            }
            if (PubDate != null)
            {
                elem.Add(new XElement("pubDate", CS.General_v3.Util.Date.GetDateAsRFC822(PubDate.Value)));
            }
            if (LastBuildDate != null)
            {
                elem.Add(new XElement("lastBuildDate", LastBuildDate));
            }
            if (!String.IsNullOrEmpty(Category))
            {
                elem.Add(new XElement("category", Category));
            }
        }
    }
}
