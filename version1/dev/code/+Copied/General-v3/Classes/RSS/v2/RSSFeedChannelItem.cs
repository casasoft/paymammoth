﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace CS.General_v3.Classes.RSS.v2
{
    public class RSSFeedChannelItem
    {
        /// <summary>
        /// The name of the channel. It's how people refer to your service. If you have an HTML website that contains the same information
        /// as your RSS file, the title of your channel should be the same as the title of your website.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// The URL to the HTML website corresponding to the channel.
        /// </summary>
        public string Link { get; set; }

        /// <summary>
        /// Phrase or sentence describing the channel.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Information or email address of the author of the item.
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// Includes the item in one or more categories.
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// URL of a page for comments relating to the item.
        /// </summary>
        public string Comments { get; set; }
        public string MediaItemURL { get; set; }
        /// <summary>
        /// Indicates when the item was published.
        /// </summary>
        public DateTime? PubDate { get; set; }
        

        public RSSFeedChannelItem(string title, string link, string description)
        {
            this.Title = title;
            this.Link = link;
            this.Description = description;
        }

        private void initOptionalElements(XElement elem)
        {
            if (!String.IsNullOrEmpty(Author))
            {
                elem.Add(new XElement("author", Author));
            }
            if (!String.IsNullOrEmpty(Category))
            {
                elem.Add(new XElement("category", Category));
            }
            if (!String.IsNullOrEmpty(Comments))
            {
                elem.Add(new XElement("comments", Comments));
            }
            if (!String.IsNullOrEmpty(MediaItemURL))
            {
                string url = CS.General_v3.Util.PageUtil.ConvertRelativeUrlToAbsoluteUrl(MediaItemURL);
                string mimeType = CS.General_v3.Util.Data.GetMIMEContentType(url);
                var elemEnclosure = new XElement("enclosure", new XAttribute("type", mimeType), new XAttribute("url", url));

                elem.Add(elemEnclosure);
            }
            if (PubDate != null)
            {
                elem.Add(new XElement("pubDate", CS.General_v3.Util.Date.GetDateAsRFC822(PubDate.Value)));
            }
        }

        public XElement GetAsXElement()
        {
            XElement elem = new XElement("item");
            elem.Add(new XElement("title", Title));
            elem.Add(new XElement("link", Link));
            elem.Add(new XElement("description", Description));

            initOptionalElements(elem);
            return elem;
        }
    }
}
