﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace CS.General_v3.Classes.RSS.v2
{
    public class RSSFeedWriter
    {
        public List<RSSFeedChannel> Channels { get; private set; }
        public List<RSSFeedChannelImage> ChannelImages { get; private set; }
        public RSSFeedWriter()
        {
            this.Channels = new List<RSSFeedChannel>();
        }

        public XDocument GetAsXDocument()
        {
            XDocument doc = new XDocument();
            doc.Declaration = new XDeclaration("1.0", "utf-8", "yes");
            foreach (var channel in Channels)
            {
                doc.Add(channel.GetAsXElement());
            }
            return doc;
        }
    }
}
