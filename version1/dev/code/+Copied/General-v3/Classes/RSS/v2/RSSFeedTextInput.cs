﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace CS.General_v3.Classes.RSS.v2
{
    public class RSSFeedTextInput
    {
        public string Title { get; set; }
        public string Decsription { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }


        public RSSFeedTextInput(string title, string description, string name, string link)
        {
            this.Title = title;
            this.Decsription = description;
            this.Name = name;
            this.Link = link;
        }

        public XElement GetAsXElement()
        {
            XElement elem = new XElement("textInput");
            elem.Add(new XElement("title", Title));
            elem.Add(new XElement("description", Decsription));
            elem.Add(new XElement("name", Name));
            elem.Add(new XElement("link", Link));
            return elem;
        }
    }
}
