﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.DbObjects.Persistors;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.NHibernateClasses.Transactions;
using NHibernate;
using NHibernate.Exceptions;
using CS.General_v3.Classes.DbObjects;

namespace CS.General_v3.Classes.NHibernateClasses.Persistance
{
    public class TempPersistor : IPersistor
    {


        #region IPersistor Members

        public void Save(IBaseDbObject itemToPersist, OperationResult result, MyNHSessionBase session, bool autoCommit, bool savePermanently, bool updateFormulaProperties)
        {
            
        }

        public void SaveOrUpdateCopy(IBaseDbObject itemToPersist, OperationResult result, MyNHSessionBase session, bool autoCommit, bool savePermanently, bool updateFormulaProperties)
        {
        
        }

        public void SaveAfter(IBaseDbObject itemToPersist, OperationResult result, MyNHSessionBase session, bool autoCommit, bool savePermanently, bool updateFormulaProperties, bool isNew)
        {
        }

        public void Update(IBaseDbObject itemToPersist, OperationResult result, MyNHSessionBase session, bool autoCommit, bool savePermanently, bool updateFormulaProperties)
        {
        }

        public void Delete(IBaseDbObject itemToPersist, OperationResult result, MyNHSessionBase session, bool deletePermanently, bool autoCommit)
        {
        }

        public void DeleteAfter(IBaseDbObject itemToPersist, OperationResult result, MyNHSessionBase session, bool deletePermanently, bool autoCommit)
        {
        }

        #endregion
    }
}
