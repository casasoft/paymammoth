﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.DbObjects.Persistors;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.NHibernateClasses.Transactions;
using NHibernate;
using NHibernate.Exceptions;
using CS.General_v3.Classes.DbObjects;

namespace CS.General_v3.Classes.NHibernateClasses.Persistance
{
    public class NhPersistor : IPersistor
    {
        
        public NhPersistor()
        {
            //CS.General_v3.Util.ContractsUtil.RequiresNotNullable(_dbItem, "db item required");
        }

        #region IPersistor Members

        public void Save(IBaseDbObject itemToPersist, OperationResult result, MyNHSessionBase session, bool autoCommit, bool savePermanently, bool updateFormulaProperties)
        {
            if (session == null) session = CS.General_v3.Util.nHibernateUtil.GetCurrentSessionFromContext();
            MyTransaction t = null;
            if (autoCommit) t = session.BeginTransaction();
            try
            {
                session.SaveOrUpdate(itemToPersist);
            }
            catch (GenericADOException ex)
            {
                int k = 5;
                throw;

            }
            if (autoCommit) t.Commit();
            
        }

        public void SaveAfter(IBaseDbObject itemToPersist, OperationResult result, MyNHSessionBase session, bool autoCommit, bool savePermanently, bool updateFormulaProperties, bool isNew)
        {
            if (session == null) session = CS.General_v3.Util.nHibernateUtil.GetCurrentSessionFromContext();
            var f = itemToPersist.GetFactory();
            f.OnItemUpdated(itemToPersist, (isNew ? Enums.UPDATE_TYPE.CreateNew : Enums.UPDATE_TYPE.Update));
            
        }

        #endregion

        #region IPersistor Members


        public void Update(IBaseDbObject itemToPersist, OperationResult result, MyNHSessionBase session, bool autoCommit, bool savePermanently, bool updateFormulaProperties)
        {
            if (session == null) session = CS.General_v3.Util.nHibernateUtil.GetCurrentSessionFromContext();
            MyTransaction t = null;
            if (autoCommit) t = session.BeginTransaction();
            try
            {
                session.Update(itemToPersist);
            }
            catch (GenericADOException ex)
            {
                int k = 5;
                throw;
            }
        }

        #endregion

        #region IPersistor Members


        public void Delete(IBaseDbObject itemToPersist, OperationResult result, MyNHSessionBase session, bool deletePermanently, bool autoCommit)
        {
            if (session == null) session = CS.General_v3.Util.nHibernateUtil.GetCurrentSessionFromContext();

            ITransaction t = null;
            if (autoCommit) t = session.BeginTransaction();
            try
            {

                session.Delete(itemToPersist);
                
            }
            catch (GenericADOException ex)
            {
                int k = 5;
                throw;
            }
            if (autoCommit) t.Commit();
        }

        public void DeleteAfter(IBaseDbObject itemToPersist, OperationResult result, MyNHSessionBase session, bool deletePermanently, bool autoCommit)
        {
            if (session == null) session = CS.General_v3.Util.nHibernateUtil.GetCurrentSessionFromContext();
            var f = itemToPersist.GetFactory();
            f.OnItemUpdated(itemToPersist, Enums.UPDATE_TYPE.Delete);
        }

        #endregion

        #region IPersistor Members


        public void SaveOrUpdateCopy(IBaseDbObject itemToPersist, OperationResult result, MyNHSessionBase session, bool autoCommit, bool savePermanently, bool updateFormulaProperties)
        {
            if (session == null) session = CS.General_v3.Util.nHibernateUtil.GetCurrentSessionFromContext();
            MyTransaction t = null;
            if (autoCommit) t = session.BeginTransaction();
            try
            {
                session.SaveOrUpdateCopy(itemToPersist);
            }
            catch (GenericADOException ex)
            {
                int k = 5;
                throw;

            }
            if (autoCommit) t.Commit();
        }

        #endregion
    }
}
