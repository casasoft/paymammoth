﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using NHibernate.Context;

namespace CS.General_v3.Classes.NHibernateClasses
{
    public class WebContextModule : IHttpModule
    {
        #region IHttpModule Members
        public void Init(HttpApplication context)
        {

            context.BeginRequest += context_BeginRequest;

            context.EndRequest += context_EndRequest;

        }
        public void Dispose()
        {

        }


        private void context_BeginRequest(object sender, EventArgs e)
        {

            var application = (HttpApplication)sender;
            
            var context = application.Context;

            

           // BindSession(context);

        }


        private bool verifyExtensionOfCurrentUrlRequiresSession(HttpContext context)
        {
            string ext = "";
            string filename = "";
            if (context != null && context.Request.Url != null)
            {
                string s = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
                ext = CS.General_v3.Util.IO.GetExtension(s).ToLower();
                filename = CS.General_v3.Util.IO.GetFilenameAndExtension(s).ToLower();
            }
            bool ok = true;
            switch (ext)
            {
                case "jpg":
                case "jpeg":
                case "css":
                case "bmp":
                case "tif":
                case "js":
                case "html":
                case "htm":
                case "gif":
                case "png":
                    ok = false;
                    break;

            }
            if (ok)
            {
                switch (filename)
                {
                    case "renewsession.aspx":
                    case "combiner.ashx":
                        ok = false;
                        break;
                }
            }
            return ok;


        }

        

        private void context_EndRequest(object sender, EventArgs e)
        {

            var application = (HttpApplication)sender;

            var context = application.Context;



           // UnbindSession(context);

        }




        private void BindSession(HttpContext context)
        {

            //var sessionBuilder = SessionBuilderFactory.CurrentBuilder;

          //  NHManager.Instance.CreateNewSessionInCurrentContext();

          //  var session = NHManager.Instance.CreateNewSession();


            if (verifyExtensionOfCurrentUrlRequiresSession(context))
            {

                // Tell NH session context to use it
                // NHManager.Instance.BindSessionToCurrentContext(session);
                NHManager.Instance.CreateNewSessionForContext();
                //SessionContextManager.Instance.CreateNewSessionForContext2(context);


                //WebSessionContext.Bind(session);
            }

        }

        private void UnbindSession(HttpContext context)
        {
            NHManager.Instance.DisposeCurrentSessionInContext();
            //SessionContextManager.Instance.DisposeCurrentSessionInContext2(context);
            

            //NHManager.Instance.UnBindSessionFromCurrentContext(true);
            

        }
        #endregion
    }
}
