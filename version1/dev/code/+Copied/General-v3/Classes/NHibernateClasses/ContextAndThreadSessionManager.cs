﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Context;
using NHibernate;
using System.Threading;
using System.Collections;

namespace CS.General_v3.Classes.NHibernateClasses
{
  
    public class ContextAndThreadSessionManager 
    {
        

        #region ICurrentSessionContext Members

        public ContextAndThreadSessionManager()
        {
            int k = 5;
        }
        private static readonly Dictionary<Thread, Stack<NHibernate.Impl.SessionImpl>> _threadSessions = new Dictionary<Thread, Stack<NHibernate.Impl.SessionImpl>>();
        private static readonly object _lock = new object();

        public static NHibernate.Impl.SessionImpl GetCurrentSession()
        {
            var stack = getStackForCurrentContext();
            if (stack.Count > 0)
            {
                NHibernate.Impl.SessionImpl session = stack.Peek();
                return session;
            }
            else
            {
                throw new Exceptions.NoSessionException("No session assigned to the current context");
            }
        }


        private static NHibernate.Impl.SessionImpl contextSession
        {
            get
            {
                return (NHibernate.Impl.SessionImpl )(System.Web.HttpContext.Current.Items["NHSessionContext"]);
            }
            set
            {
                System.Web.HttpContext.Current.Items["NHSessionContext"] = value;
            }


}


        public static Stack<NHibernate.Impl.SessionImpl> getStackForCurrentContext()
        {
            Stack<NHibernate.Impl.SessionImpl> stack = null;
            if (false && System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Session != null)
            {
                stack = (Stack<NHibernate.Impl.SessionImpl>)(System.Web.HttpContext.Current.Session["NHSessionContext"]);
                if (stack == null)
                {
                    stack = new Stack<NHibernate.Impl.SessionImpl>();
                    System.Web.HttpContext.Current.Session["NHSessionContext"] = stack;
                }

                //session = stack.Peek();

                //session = (ISession)(System.Web.HttpContext.Current.Items["nhibernate_session_context"]);
            }
            else
            {
                var t = System.Threading.Thread.CurrentThread;
                
                if (!_threadSessions.TryGetValue(t, out stack))
                {
                    stack = new Stack<NHibernate.Impl.SessionImpl>();
                    _threadSessions[t] = stack;
                }


            }
            return stack;
        }



       
        public static void BindSession(ISession session)
        {
            var stack = getStackForCurrentContext();
            //stack.Add((NHibernate.Impl.SessionImpl)session);

            stack.Push((NHibernate.Impl.SessionImpl)session);

            

        }

        public static NHibernate.Impl.SessionImpl UnBindSession(ISessionFactory factory)
        {
            return null;
            var stack = getStackForCurrentContext();
            NHibernate.Impl.SessionImpl session = stack.Pop();
            //NHibernate.Impl.SessionImpl session = stack[stack.Count - 1];
            //stack.Remove(session);

            return session;
        }


        #endregion

        /*protected override ISession Session
        {
            get
            {
                return _currentSession;
                
            }
            set
            {
                _currentSession = value;
                
            }
        }*/

    }
}
