﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.Session;
using NHibernate;
using NHibernate.Engine;

namespace CS.General_v3.Classes.NHibernateClasses.Transactions
{
    public class MyTransaction_old : ITransaction
    {
        private ITransaction _t;
        public bool AutoBeginTransactionOnCommit { get; set; }
        private MyNHSessionBase _session = null;
        internal MyTransaction_old(MyNHSessionBase session, ITransaction t)
        {
            _t = t;
            _session = session;
            
        }

        #region ITransaction Members

        public void Begin(System.Data.IsolationLevel isolationLevel)
        {
            _t.Begin(isolationLevel);
            
        }

        public void Begin()
        {
            _t.Begin();
            
        }

        public void Commit()
        {
            _t.Commit();
            if (AutoBeginTransactionOnCommit)
            {
                _t.Begin();
            }

        }
        public bool CommitIfActive()
        {
            bool b = false;
            if (this.IsActive)
            {
                b = true;
                this.Commit();
            }
            return b;
        }

        public void Enlist(System.Data.IDbCommand command)
        {
            _t.Enlist(command);
            
        }

        public bool IsActive
        {
            get { return _t.IsActive; }
        }

        public void RegisterSynchronization(NHibernate.Transaction.ISynchronization synchronization)
        {
            _t.RegisterSynchronization(synchronization);
            
        }

        public void Rollback()
        {
            _t.Rollback();
            if (AutoBeginTransactionOnCommit)
            {
                _t.Begin();
            }
        }

        public bool WasCommitted
        {
            get { return _t.WasCommitted; }
        }

        public bool WasRolledBack
        {
            get { return _t.WasRolledBack;}
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            _t.Dispose();
            
        }

        #endregion

        public bool CommitIfActiveElseFlush()
        {
            bool commitOk = this.CommitIfActive();
            if (!commitOk)
            {
                _session.Flush();
                
            }
            return commitOk;
            
        }
    }
}
