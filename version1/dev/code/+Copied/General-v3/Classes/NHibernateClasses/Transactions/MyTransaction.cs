﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.Session;
using NHibernate;
using NHibernate.Engine;

namespace CS.General_v3.Classes.NHibernateClasses.Transactions
{
    public class MyTransaction : ITransaction
    {

        bool _transactionOwner = false;
        bool _transactionActive = false;
        public bool IsActive
        {
            get
            {
                return _transactionActive;
            }
        }
        private MyNHSessionBase _session = null;
        private ITransaction _t = null;
        public MyTransaction(MyNHSessionBase session)
        {
            this._session = session;
        }

        protected ITransaction getTransactionFromSession(System.Data.IsolationLevel? isolationLevel)
        {
            if (isolationLevel.HasValue)
                return _session._beginTransaction(isolationLevel.Value);
            else
                return _session._beginTransaction();
        }

        public void Begin(System.Data.IsolationLevel? isolationLevel = null)
        {
            if (_transactionActive) return; //return to bad programer's code

            try
            {


                _transactionOwner = _session.Transaction == null || !_session.Transaction.IsActive;
                if (_transactionOwner)
                    _t = getTransactionFromSession(isolationLevel);
                else
                    _t = _session.Transaction;
                _transactionActive = true;
            }
            catch
            {
                _transactionOwner = false;
                _transactionActive = false;
                throw;
            }
        }
        


        protected bool sessionHasOpenTransaction()
        {
            return _session.Transaction != null && _session.Transaction.IsActive;
        }

        public void Commit()
        {
            if (!_transactionActive || !sessionHasOpenTransaction())
            {
                _transactionActive = false;
                return;
            }

            try
            {
                if (_transactionOwner)
                    _t.Commit();
                    
            }
            catch //when you don't know what you are committing to.
            {
                _t.Rollback();
                
                throw;
            }
            finally
            {
                _transactionActive = false;
            }
        }

        public void Rollback()
        {
            _transactionActive = false;

            if (!sessionHasOpenTransaction())
                return;
            else
                _t.Rollback();
                
        }

        #region ITransaction Members


        public void Enlist(System.Data.IDbCommand command)
        {
            _t.Enlist(command);
            
        }

        public void RegisterSynchronization(NHibernate.Transaction.ISynchronization synchronization)
        {
            _t.RegisterSynchronization(synchronization);
            
        }

        public bool WasCommitted
        {
            get { return _t.WasCommitted;}
        }

        public bool WasRolledBack
        {
            get { return _t.WasRolledBack; }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            if (_t !=null)
                _t.Dispose();
            
        }

        #endregion

        [Obsolete("Use Commit()")]
        public void CommitIfActiveElseFlush()
        {
            this.Commit();
            
        }

        #region ITransaction Members

        void ITransaction.Begin(System.Data.IsolationLevel isolationLevel)
        {
            this.Begin(isolationLevel);
            
        }

        void ITransaction.Begin()
        {
            this.Begin(null);
            
        }

        #endregion
    }
}
