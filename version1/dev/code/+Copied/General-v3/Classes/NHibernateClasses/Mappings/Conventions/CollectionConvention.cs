﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using FluentNHibernate.Conventions.Inspections;
using FluentNHibernate.Conventions.Instances;

namespace CS.General_v3.Classes.NHibernateClasses.FluentNH.Conventions
{
    public class CollectionConvention : ICollectionConvention
    {


        #region IConvention<ICollectionInspector,ICollectionInstance> Members

        public void Apply(ICollectionInstance instance)
        {
            instance.Access.LowerCaseField(LowerCasePrefix.Underscore);

            if (!(instance.Relationship is FluentNHibernate.Conventions.Instances.ManyToManyInstance))
            {

                instance.Where("((`_Deleted` = 0 or `_deleted` is null) and (`_Temporary_Flag` = 0 or `_Temporary_Flag` is null))");
            }
        }

        #endregion
    }
}
