﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.Mappings.Cascades;

namespace CS.General_v3.Classes.NHibernateClasses.Mappings
{
    public interface ICollectionMapInfo
    {
        Type CustomType { get; set; }
        Enums.NHIBERNATE_COLLECTION_TYPE CollectionType { get; set; }
        Enums.NHIBERNATE_COLLECTION_RELATIONSHIP RelationshipType { get;  }
        string ManyToManyTableName { get; set; }
        string ParentKey { get; set; }
        string ChildKey { get; set; }
        NHibernate.Mapping.ByCode.CollectionLazy? LazyMode { get; set; } 
        List<OrderByValue> OrderByValuesSql { get; set; }
        bool IsIndexed { get; set; }
        CollectionCascadeInfo CascadeType { get; }
        Enums.NHIBERNATE_COLLECTION_FETCHMODE FetchType { get; set; }
        Enums.NHIBERNATE_CACHE_TYPE CacheType { get; set; }
        bool? Inverse { get; set; }
        int? BatchSize { get; set; }

        /// <summary>
        /// This only applies to a ManyToOne relationship
        /// </summary>
        string ColumnName { get; set; }

        PropertyInfo Property { get; }

        OrderByValue AddOrderBy(PropertyInfo pInfo, CS.General_v3.Enums.SORT_TYPE sortType = Enums.SORT_TYPE.Ascending);
        void MapCollection();
    }
}
