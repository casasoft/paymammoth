﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Search.Mapping;

namespace CS.General_v3.Classes.NHibernateClasses.Mappings.Lucene
{
    public class LuceneMappingClassInfo
    {
        public Type ClassType { get; set; }
        public List<ILucenePropertyInfo> PropertyInfos { get; private set; }
        public LuceneMappingClassInfo(Type classType)
        {
            this.PropertyInfos = new List<ILucenePropertyInfo>();
            this.ClassType = classType;
        }

        public DocumentMapping CreateDocumentMapping()
        {
            DocumentMapping map = new DocumentMapping(this.ClassType);
            foreach (var p in this.PropertyInfos)
            {
                map.Fields.Add(p.CreateFieldMapping());
            }
            return map;
        }

    }
}
