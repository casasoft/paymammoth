﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.NHibernateClasses.FluentNH
{
    public static class FluentNHEnums
    {
        public enum MAP_TYPE
        {
            Set,
            Bag,
            List,
            Map
        }
        public enum FETCH_TYPE
        {
            Default,
            Join,
            Select,
            SubSelect
        }
        public enum CACHE_TYPE
        {
            Default,
            ReadOnly,
            ReadWrite,
            NonStrictReadWrite
        }
        public enum CASCADE_TYPE
        {
            Default,
            All,
            SaveUpdate,
            Delete,
            AllDeleteOrphan,
            DeleteOrphan,
            None

        }
    }
}
