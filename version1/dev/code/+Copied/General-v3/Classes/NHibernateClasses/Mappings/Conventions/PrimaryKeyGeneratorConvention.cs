﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using FluentNHibernate.Conventions.Inspections;
using FluentNHibernate.Conventions.Instances;

namespace CS.General_v3.Classes.NHibernateClasses.FluentNH.Conventions
{
    public class PrimaryKeyGeneratorConvention : FluentNHibernate.Conventions.IIdConvention
    {
        
        #region IConvention<IIdentityInspector,IIdentityInstance> Members

        public void Apply(IIdentityInstance instance)
        {
            instance.GeneratedBy.HiLo("4096");
            
            
        }

        #endregion
    }
}
