﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using NHibernate.Mapping.ByCode;
using System.Reflection;
using CS.General_v3.Classes.DbObjects;
using NHibernate.Type;

namespace CS.General_v3.Classes.NHibernateClasses.Mappings
{
    public class PropertyMapInfo<TData, TProperty> : IPropertyMapInfo
        where TData : BaseDbObject
    {
        public BaseClassMap<TData> ClassMap {get; private set;}
        public System.Linq.Expressions.Expression<Func<TData, TProperty>> Selector { get; private set; }
        public PropertyMapInfo(BaseClassMap<TData> classMap, System.Linq.Expressions.Expression<Func<TData, TProperty>> selector)
        {
            this.ClassMap = classMap;
            this.Selector = selector;
            this.MapEnumAsString = true;
            this.Property = CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(Selector);
            //this.CustomType = this.Property.PropertyType;
            initDefaults();
            this.LuceneInfo = new Lucene.LucenePropertyInfo(this.Property);
            
        }

        public Lucene.LucenePropertyInfo LuceneInfo { get; private set; }
        public PropertyInfo Property { get; private set; }
        private void initDefaults()
        {

            this.Length = 1000000; 
            string nameLower = this.Property.Name.ToLower();
         //   this.IsNullable = !this.Property.PropertyType.IsPrimitive;
            if (nameLower.Contains( "Identifier".ToLower()))
            {
                IsIndexed = true;
                this.Length = 200;
            }
            
        }
     //   public PropertyInfo Property { get; private set; }
        private int? _length;
        public int? Length
        {
            get 
            {
                if (IsIndexed && (!_length.HasValue || (_length.HasValue && _length.Value > 1000)))
                    return 200;
                else
                    return _length; 
            }
            set { _length = value; }
        }

        public bool? IsNullable { get; set; }
        public string CustomColumnName { get; set; }
        public string DefaultValue { get; set; }
        public bool IsIndexed { get; set; }
        private bool getNotNullableValue()
        {
            bool notNullable = false;
            if (IsNullable.HasValue)
                notNullable = !IsNullable.Value;
            else
            {
                Type t = typeof(TProperty);
                bool v = (t.IsPrimitive);
                notNullable = v;
                
            }
            if (notNullable)
            {
                int k = 5;
            }
            return notNullable;
        }

        
        private object getDefaultValue()
        {
            object defaultVal = null;
            
            Type t = typeof(TProperty);
            if (t.IsPrimitive)
            {
                if (t.IsAssignableFrom(typeof(double)) ||
                    t.IsAssignableFrom(typeof(float)) ||
                    t.IsAssignableFrom(typeof(int)) ||
                    t.IsAssignableFrom(typeof(long)) ||
                    t.IsAssignableFrom(typeof(byte)) ||
                    t.IsAssignableFrom(typeof(short)) ||
                    t.IsAssignableFrom(typeof(decimal)))
                {

                    defaultVal = 0;
                }
                else if (t.IsAssignableFrom(typeof(bool)))
                    defaultVal = false;
                else if (t.IsAssignableFrom(typeof(DateTime)))
                    defaultVal = new DateTime(1970, 1, 1);
                else
                    throw new InvalidOperationException("Primitive data type not yet catered for");
            }
            return defaultVal;
        }
        private void mapColumn(IColumnMapper map)
        {
            if (!string.IsNullOrWhiteSpace(this.CustomColumnName)) map.Name(CustomColumnName);
            {
                bool notNull = getNotNullableValue();
                if (this.Property.Name == "AccountBalance")
                {
                    int k = 5;
                    // notNull = false;
                }

                map.NotNullable(notNull);
            }
            {
                object defaultValue = getDefaultValue();
                map.Default(defaultValue);
            }
            
            if (Length.HasValue) map.Length(Length.Value);
           // if (IsIndexed) map.Index(Property.Name);
            
           
        }
        

        private void checkForEnum(IPropertyMapper map)
        {

            if (this.Property.PropertyType.IsEnumOrNullableEnum())
            {
                this.IsIndexed = true;
                if (CS.General_v3.Util.ReflectionUtil.CheckIfTypeIsEnum(this.Property.PropertyType) && this.MapEnumAsString)
                {
                    if (CS.General_v3.Util.ReflectionUtil.CheckIfTypeIsNullable(this.Property.PropertyType))
                    {
                        int k = 5;
                    }
                    //NHibernate.Type.EnumStringType<TProperty> t = new EnumStringType<TProperty>();
                    
                    
                    map.Type<Types.EnumNullableToStringType<TProperty>>(); //this makes sure enums are saved as strings, not integers
                }
            }
        }
        
        private void mapPropertyInfo(IPropertyMapper map)
        {




            if (this.CustomTypeMapping != null)
            {
                map.Type(this.CustomTypeMapping, null);
            }
            else
            {
                checkForEnum(map);
            }
            map.Column(mapColumn);
            //if (!string.IsNullOrWhiteSpace(this.DefaultValue)) map.d map.Default(DefaultValue);
           // if (IsIndexed) map.Index(Property.Name);
            
            map.Access(Accessor.Field);

            if (Length.HasValue) map.Length(Length.Value);
            {
                bool notNull = getNotNullableValue();
                if (this.Property.Name == "AccountBalance")
                {
                    int k = 5;
              //      notNull = false;
                }

                map.NotNullable(notNull);
            }
        }

        public void MapProperty<TProperty>() 
        {
          

            this.ClassMap.Property(this.Selector, mapPropertyInfo);
            
        }




      
        /// <summary>
        /// Use this to define a custom type mapping, e.g mapping a country enum to its equivalent 3-letter country code.
        /// </summary>
        public Type CustomTypeMapping { get; set; }


        public bool MapEnumAsString { get; set; }


        #region IPropertyMapInfo Members


        Lucene.ILucenePropertyInfo IPropertyMapInfo.LuceneInfo
        {
            get { return this.LuceneInfo; }
        }

        #endregion
    }
}
