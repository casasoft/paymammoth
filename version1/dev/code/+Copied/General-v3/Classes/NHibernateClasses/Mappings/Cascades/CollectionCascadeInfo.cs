﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.NHibernateClasses.Mappings.Cascades
{
    public class CollectionCascadeInfo
    {
        public bool Persist { get; set; }
        public bool All { get; set; }
        public bool DeleteOrphans { get; set; }
        public bool Detach { get; set; }
        public bool Merge { get; set; }
        public bool ReAttach { get; set; }
        public bool Refresh { get; set; }
        public bool Remove { get; set; }
        

        public void SetAll()
        {
            this.All = true;
        }
        public void Clear()
        {
            this.Persist = false;
            this.All = false;
            this.DeleteOrphans = false;
            this.Detach = false;
            this.Merge = false;
            this.ReAttach = false;
            this.Refresh = false;
            this.Remove = false;
            
        }

        public NHibernate.Mapping.ByCode.Cascade GetNhibernateCascade()
        {
            NHibernate.Mapping.ByCode.Cascade result = NHibernate.Mapping.ByCode.Cascade.None;
            if (All)
            {
                result = NHibernate.Mapping.ByCode.Cascade.All;
            }
            else
            {
                if (DeleteOrphans) result = result | NHibernate.Mapping.ByCode.Cascade.DeleteOrphans;
                if (Detach) result = result | NHibernate.Mapping.ByCode.Cascade.Detach;
                if (Merge) result = result | NHibernate.Mapping.ByCode.Cascade.Merge;
                if (Persist) result = result | NHibernate.Mapping.ByCode.Cascade.Persist;
                if (ReAttach) result = result | NHibernate.Mapping.ByCode.Cascade.ReAttach;
                if (Refresh) result = result | NHibernate.Mapping.ByCode.Cascade.Refresh;
                if (Remove) result = result | NHibernate.Mapping.ByCode.Cascade.Remove;
            }

            return result;
        }
    }
}
