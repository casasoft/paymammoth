﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using FluentNHibernate.Conventions.Inspections;
using FluentNHibernate.Conventions.Instances;

namespace CS.General_v3.Classes.NHibernateClasses.FluentNH.Conventions
{
    public class PropertyConventions : FluentNHibernate.Conventions.IPropertyConvention
    {
        #region IConvention<IPropertyInspector,IPropertyInstance> Members

        public void Apply(IPropertyInstance instance)
        {
            Type propertyType = instance.Type.GetUnderlyingSystemType();
            string nameLower = instance.Name.ToLower();
            bool nullable = (!propertyType.IsPrimitive);
            if (nameLower == "_Temporary_Flag".ToLower() || nameLower == "_Deleted".ToLower() || nameLower == "Identifier".ToLower())
            {
                instance.Index(instance.Name);
                //if (propertyType == typeof(string))
                  //  instance.Length(200);
                nullable = false;
            }
            if (nullable)
                instance.Nullable();
            

            if (propertyType == typeof(bool) || 
                propertyType == typeof(int) || 
                propertyType == typeof(double) || 
                propertyType == typeof(long) || 
                propertyType == typeof(float))
            {
                instance.Default("0");
            }
            else if (propertyType == typeof(DateTime))
            {
                instance.Default("'0001-01-01 00:00:00'");
            }

            

            //instance.Length(1000000);
            
        }

        #endregion
    }
}

