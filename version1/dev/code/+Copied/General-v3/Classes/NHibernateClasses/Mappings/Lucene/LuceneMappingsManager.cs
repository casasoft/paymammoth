﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.NHibernateClasses.Mappings.Lucene
{
    public class LuceneMappingsManager
    {
        public static LuceneMappingsManager Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<LuceneMappingsManager>(); } }

        private Dictionary<Type, LuceneMappingClassInfo> classInfos { get; set; }
        public LuceneMappingsManager()
        {
            this.classInfos = new Dictionary<Type, LuceneMappingClassInfo>();

        }

        private LuceneMappingClassInfo getClassInfo(Type t)
        {

            LuceneMappingClassInfo classInfo = null;
            if (!classInfos.TryGetValue(t, out classInfo))
            {
                classInfo = new LuceneMappingClassInfo(t);
                classInfos.Add(t, classInfo);
            }
            return classInfo;
        }

        public void CheckMappingForProperty(Type classType, IPropertyMapInfo pInfo)
        {

            var classInfo = getClassInfo(classType);

            var lucene = pInfo.LuceneInfo;
            if (lucene.MappedInLucene)
            {
                classInfo.PropertyInfos.Add(lucene);
            }

        }




    }
}
