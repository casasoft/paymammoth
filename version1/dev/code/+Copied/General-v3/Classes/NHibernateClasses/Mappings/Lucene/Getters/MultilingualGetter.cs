﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using NHibernate;
using NHibernate.Engine;
using NHibernate.Properties;

namespace CS.General_v3.Classes.NHibernateClasses.Mappings.Lucene.Getters
{
    [Serializable]
    public sealed class MultilingualGetter : IGetter, IOptimizableGetter
    {
        private readonly System.Type clazz;
        private readonly PropertyInfo property;
        private readonly string propertyName;

        /// <summary>
        /// Initializes a new instance of <see cref="TestGetter"/>.
        /// </summary>
        /// <param name="clazz">The <see cref="System.Type"/> that contains the Property <c>get</c>.</param>
        /// <param name="property">The <see cref="PropertyInfo"/> for reflection.</param>
        /// <param name="propertyName">The name of the Property.</param>
        public MultilingualGetter(System.Type clazz, PropertyInfo property, string propertyName)
        {
            this.clazz = clazz;
            this.property = property;
            this.propertyName = propertyName;
        }

        public PropertyInfo Property
        {
            get { return property; }
        }

        #region IGetter Members

        /// <summary>
        /// Gets the value of the Property from the object.
        /// </summary>
        /// <param name="target">The object to get the Property value from.</param>
        /// <returns>
        /// The value of the Property for the target.
        /// </returns>
        public object Get(object target)
        {
            //todo: reactivate this
            //if (target is IMultilingualContentInfo)
            //{
            //    try
            //    {
            //        IMultilingualContentInfo multiLingualTarget = (IMultilingualContentInfo)target;
            //        string s = (string)property.GetValue(target, new object[0]);
            //        if (!string.IsNullOrWhiteSpace(s))
            //        {
            //            StringBuilder sb = new StringBuilder();
            //            sb.Append("[" + multiLingualTarget.CultureInfo.GetCultureCode() + "]");
            //            sb.Append(" " + s);
            //            s = sb.ToString();
            //        }
            //        return s;
            //    }
            //    catch (Exception e)
            //    {
            //        throw new PropertyAccessException(e, "Exception occurred", false, clazz, propertyName);
            //    }
            //}
            //else
            //{
            //    throw new InvalidOperationException("Multilingual Getter is only available on IMultilingualContentInfo objects");
            //}
            return null;
        }

        /// <summary>
        /// Gets the <see cref="System.Type"/> that the Property returns.
        /// </summary>
        /// <value>The <see cref="System.Type"/> that the Property returns.</value>
        public System.Type ReturnType
        {
            get { return property.PropertyType; }
        }

        /// <summary>
        /// Gets the name of the Property.
        /// </summary>
        /// <value>The name of the Property.</value>
        public string PropertyName
        {
            get { return property.Name; }
        }

        /// <summary>
        /// Gets the <see cref="PropertyInfo"/> for the Property.
        /// </summary>
        /// <value>
        /// The <see cref="PropertyInfo"/> for the Property.
        /// </value>
        public MethodInfo Method
        {
            get { return property.GetGetMethod(true); }
        }

        public object GetForInsert(object owner, IDictionary mergeMap, ISessionImplementor session)
        {
            return Get(owner);
        }

        #endregion

        public void Emit(ILGenerator il)
        {
            MethodInfo method = Method;
            if (method == null)
            {
                throw new PropertyNotFoundException(clazz, property.Name, "getter");
            }
            il.EmitCall(OpCodes.Callvirt, method, null);
        }
    }
}
