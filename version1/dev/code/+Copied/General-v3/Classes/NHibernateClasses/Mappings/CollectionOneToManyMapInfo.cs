﻿using System; 
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.DbObjects;
using Iesi.Collections.Generic;
using NHibernate.Mapping.ByCode;

namespace CS.General_v3.Classes.NHibernateClasses.Mappings
{
    public class CollectionOneToManyMapInfo<TData, TElement> : BaseCollectionMapInfo<TData>
        where TData : BaseDbObject
    {
        public CollectionOneToManyMapInfo(BaseClassMap<TData> classMap, System.Linq.Expressions.Expression<Func<TData, IEnumerable<TElement>>> selector,

           Enums.NHIBERNATE_COLLECTION_RELATIONSHIP relationshipType)
            : base(classMap, relationshipType)
        {
            this.Selector = selector;
            setProperty(CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(this.Selector));

            initDefaults();

        }
        private void initDefaults()
        {
           
        }

        private string getOrderByAsSQLString()
        {
            HashedSet<string> list = new HashedSet<string>();
            if (this.OrderByValuesSql.Count > 0)
            {
                foreach (var v in this.OrderByValuesSql)
                {
                    list.Add(v.ToSql());
                }

            }
            return CS.General_v3.Util.Text.AppendStrings(", ", list);
        }





        protected void fillWhereClause(StringBuilder sb)
        {
            if (this.RelationshipType == Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany)
            {
                string deletedName = CS.General_v3.Util.ReflectionUtil<IBaseDbObject>.GetPropertyName(x => x.Deleted);
                string tempFlagName = CS.General_v3.Util.ReflectionUtil<IBaseDbObject>.GetPropertyName(x => x._Temporary_Flag);
                sb.Append("(");
                sb.Append("(`" + deletedName + "` = 0 OR `" + deletedName + "` IS Null) AND ");
                sb.Append("(`" + tempFlagName + "` = 0 OR `" + tempFlagName + "` IS Null)");
                sb.Append(")");
            }
            else
            {
                int k = 5;
            }
            //    return sb.ToString();

        }

        protected virtual void fillCollectionMappingInfo(ICollectionPropertiesMapper<TData,TElement> mappingInfo)
        {
            //if (this.CustomType != null)

            //  mappingInfo.Type(this.CustomType);
            checkForMultilingualClass();

            if (this.Property.Name == "ParentArticleLinks")
            {
                int k = 5;
            }
            mappingInfo.Cascade(this.CascadeType.GetNhibernateCascade());
            
            if (this.OrderByValuesSql.Count > 0) mappingInfo.OrderBy(getOrderByAsSQLString());


            mappingInfo.Access(Accessor.Field);

            

            if (this.LazyMode.HasValue)
                mappingInfo.Lazy(this.LazyMode.Value);

            {
                int batchSize = BatchSize.GetValueOrDefault(50);
                mappingInfo.BatchSize(batchSize);
            }
            if (string.IsNullOrWhiteSpace(this.ManyToManyTableName) && this.RelationshipType == Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.ManyToMany)
            {
                throw new InvalidOperationException("Many-to-many relationships must specify the table name");
            }
            if (!string.IsNullOrWhiteSpace(this.ManyToManyTableName))
            {
                mappingInfo.Table(ManyToManyTableName);
            }
            
            mapFetchType(mappingInfo);
            if (this.CacheType != Enums.NHIBERNATE_CACHE_TYPE.Default)
            {
                mappingInfo.Cache(mapCacheType);
            }
            {
                StringBuilder sbWhere = new StringBuilder();
                fillWhereClause(sbWhere);
                if (sbWhere.Length > 0)
                {
                    mappingInfo.Where(sbWhere.ToString());
                }
            }


            mappingInfo.Key(mapKeyInfo);


            mappingInfo.Inverse(this.Inverse.GetValueOrDefault());




        }

        public System.Linq.Expressions.Expression<Func<TData, IEnumerable<TElement>>> Selector { get; private set; }
        protected void fillBagSpecificMappingInfo(IBagPropertiesMapper<TData, TElement> mappingInfo)
        {
            fillCollectionMappingInfo(mappingInfo);
        }
        protected void fillSetSpecificMappingInfo(ISetPropertiesMapper<TData, TElement> mappingInfo)
        {
            fillCollectionMappingInfo(mappingInfo);
        }
        protected void fillListSpecificMappingInfo(IListPropertiesMapper<TData, TElement> mappingInfo)
        {
            fillCollectionMappingInfo(mappingInfo);
        }
        protected void mapFetchType(ICollectionPropertiesMapper<TData,TElement> mappingInfo)
        {
            if (this.FetchType != Enums.NHIBERNATE_COLLECTION_FETCHMODE.Default)
            {
                switch (this.FetchType)
                {
                    case Enums.NHIBERNATE_COLLECTION_FETCHMODE.Join:
                        mappingInfo.Fetch(CollectionFetchMode.Join);
                        break;
                    case Enums.NHIBERNATE_COLLECTION_FETCHMODE.Select:
                        mappingInfo.Fetch(CollectionFetchMode.Select);
                        break;
                    case Enums.NHIBERNATE_COLLECTION_FETCHMODE.SubSelect:
                        mappingInfo.Fetch(CollectionFetchMode.Subselect);
                        break;
                    default:
                        throw new InvalidOperationException("Invalid fetch type");
                }
            }
        }
        protected void fillCollectionElementRelationMapping(ICollectionElementRelation<TElement> relationMapping)
        {
            
            switch (this.RelationshipType)
            {
                case Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.ManyToMany:
                    {



                        relationMapping.ManyToMany(mapping =>
                        {
                            
                            mapping.Column(this.ChildKey);
                        });
                        break;
                    }
                case Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany:
                    {
                        relationMapping.OneToMany
                        (mapping =>
                        {
                            
                        });
                        break;
                    }
                default: throw new InvalidOperationException("Invalid relationship type");


            }
        }
         protected override void _mapCollection()
        {
            
            switch (this.CollectionType)
            {
                case Enums.NHIBERNATE_COLLECTION_TYPE.Bag:
                    {
                        
                        this.ClassMap.Bag<TElement>(this.Selector, fillBagSpecificMappingInfo, fillCollectionElementRelationMapping);
                        break;
                    }
                case Enums.NHIBERNATE_COLLECTION_TYPE.Set:
                    {

                        this.ClassMap.Set<TElement>(this.Selector, fillSetSpecificMappingInfo, fillCollectionElementRelationMapping);
                        break;
                    }
                case Enums.NHIBERNATE_COLLECTION_TYPE.List:
                    {

                        this.ClassMap.List<TElement>(this.Selector, fillListSpecificMappingInfo, fillCollectionElementRelationMapping);
                        break;
                    }
                default:
                    throw new InvalidOperationException("Invalid collection type");
            }

        }



    }
}
