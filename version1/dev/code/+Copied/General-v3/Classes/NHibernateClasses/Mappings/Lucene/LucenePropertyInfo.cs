﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Search.Bridge;
using Lucene.Net.Analysis;
using NHibernate.Properties;
using NHibernate.Search.Mapping;
using System.Reflection;

namespace CS.General_v3.Classes.NHibernateClasses.Mappings.Lucene
{
    public class LucenePropertyInfo : ILucenePropertyInfo
    {
        public LucenePropertyInfo(PropertyInfo propertyInfo)
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(propertyInfo, "Property Info is required");
            this.Property = propertyInfo;
        }
        public PropertyInfo Property { get; private set; }

        public NHibernate.Search.Attributes.Index Index { get; set; }
        public NHibernate.Search.Attributes.Store Store { get; set; }

        /// <summary>
        /// If no custom bridge defined, default is taken
        /// </summary>
        public IFieldBridge CustomBridge { get; set; }

        public bool UseStemming { get; set; }
        public bool IsMultilingual { get; set; }

        public Analyzer CustomAnalyzer { get; set; }
        public float? Boost { get; set; }
        public IGetter Getter { get; set; }



        #region ILucenePropertyInfo Members

        public bool MappedInLucene{get;set;}

        #endregion

        #region ILucenePropertyInfo Members


        private IFieldBridge getBridgeBasedOnDataType()
        {
            if (this.CustomBridge != null)
                return this.CustomBridge;
            else
            {
                if (this.Property.PropertyType == typeof (long))
                    return BridgeFactory.LONG;
                else if (this.Property.PropertyType == typeof (long))
                    return BridgeFactory.BOOLEAN;
                else if (this.Property.PropertyType == typeof (long))
                    return BridgeFactory.DOUBLE;
                else if (this.Property.PropertyType == typeof (long))
                    return BridgeFactory.DATE_SECOND;
                else if (this.Property.PropertyType == typeof (long))
                    return BridgeFactory.FLOAT;
                else if (this.Property.PropertyType == typeof (long))
                    return BridgeFactory.INTEGER;
                else if (this.Property.PropertyType == typeof (long))
                    return BridgeFactory.SHORT;
                else if (this.Property.PropertyType == typeof (long))
                    return BridgeFactory.STRING;
                else
                {
                    throw new InvalidOperationException("Default bridge does not exist for type '" + this.Property.PropertyType.FullName + "', please define in 'CustomBridge'. Property: " + this.Property.Name);

                }
            }

        }

        private IGetter getFieldGetter()
        {
            return null;
        }

        public NHibernate.Search.Mapping.FieldMapping CreateFieldMapping()
        {
            return null;
            
        }

        #endregion
    }
}
