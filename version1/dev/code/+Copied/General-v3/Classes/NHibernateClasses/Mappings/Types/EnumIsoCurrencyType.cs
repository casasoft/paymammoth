﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.NHibernateClasses.Mappings.Types
{
    [Serializable]
    public class EnumIsoCurrencyType : EnumNullableToStringType<CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217>

    {
        protected override string convertEnumValueToString(object value)
        {
            string s = null;
            if (value is Enum)
            {
                Enum enumVal = (Enum)value;
                s = CS.General_v3.Enums.ISO_ENUMS.Currency_ISO4217ToCode((Enums.ISO_ENUMS.CURRENCY_ISO4217)enumVal);
            }
            return s;
        }
        protected override Enum convertStringToEnumValue(string xml)
        {
            Enum val = null;
            if (!string.IsNullOrWhiteSpace(xml))
            {
                val = CS.General_v3.Enums.ISO_ENUMS.Currency_ISO4217FromCode(xml);
                if (val == null) val = base.convertStringToEnumValue(xml);
            }

            return val;

        }
    }
}
