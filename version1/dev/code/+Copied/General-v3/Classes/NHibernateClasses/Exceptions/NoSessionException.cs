﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.NHibernateClasses.Exceptions
{
    public class NoSessionException : Exception
    {
        public NoSessionException(string msg)
            : base(msg)
        {

        }
    }
}
