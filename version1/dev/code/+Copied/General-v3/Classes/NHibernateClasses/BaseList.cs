﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.MyList;

namespace CS.General_v3.Classes.NHibernateClasses
{
    public abstract class BaseDbList<TItem> : Iesi.Collections.Generic.ISet<TItem>
        where TItem : IBaseDbObject
        
    {

        public void RemoveTemporaryItemsFromList()
        {
            for (int i = 0; i < this.Count; i++)
            {
                var item = this[i];
                if (item._Temporary_Flag)
                {
                    this.RemoveAt(i);
                    i--;
                }
            }

        }

        public TItem FindByPKey(TItem itemPKey)
        {
            if (itemPKey != null)
                return FindByPKey(itemPKey.ID);
            else
                return default(TItem);

        }
        public void RemoveByPKey(BaseDbObject pKey)
        {
            this.RemoveByPKey(pKey.PrimaryKey);
        }
        
        public void RemoveByPKey(long pKey)
        {
            for (int i = 0; i < this.Count; i++)
            {
                if (this[i].ID == pKey)
                {
                    this.RemoveAt(i);
                    i--;
                    
                }
            }
            
        }
        public TItem FindByPKey(long pKey)
        {
            for (int i = 0; i < this.Count; i++)
            {
                if (this[i].PrimaryKey == pKey)
                    return this[i];
            }
            return default(TItem);
        }

        #region ISet<TItem> Members

        public abstract bool Add(TItem o);

        protected abstract bool _AddAll(ICollection<TItem> c);
        public bool AddAll(ICollection<TItem> c)
        {
            return _AddAll(c);
        }

        protected abstract bool _ContainsAll(ICollection<TItem> c);
        public bool ContainsAll(ICollection<TItem> c)
        {
            return _ContainsAll(c);
        }
        protected abstract Iesi.Collections.Generic.ISet<TItem> _ExclusiveOr(Iesi.Collections.Generic.ISet<TItem> a);
        public Iesi.Collections.Generic.ISet<TItem> ExclusiveOr(Iesi.Collections.Generic.ISet<TItem> a)
        {
            return _ExclusiveOr(a);
        }
        
        protected abstract Iesi.Collections.Generic.ISet<TItem> _Intersect(Iesi.Collections.Generic.ISet<TItem> a);
        public Iesi.Collections.Generic.ISet<TItem> Intersect(Iesi.Collections.Generic.ISet<TItem> a)
        {
            return _Intersect(a);

        }

        protected abstract Iesi.Collections.Generic.ISet<TItem> _Minus(Iesi.Collections.Generic.ISet<TItem> a);
        public Iesi.Collections.Generic.ISet<TItem> Minus(Iesi.Collections.Generic.ISet<TItem> a)
        {
            return _Minus(a);
        }

        public virtual bool IsEmpty
        {
            get { return false; }
        }


        protected abstract bool _RemoveAll(ICollection<TItem> c);
        public bool RemoveAll(ICollection<TItem> c)
        {
            return _RemoveAll(c);
        }

        protected abstract bool _RetainAll(ICollection<TItem> c);
        public bool RetainAll(ICollection<TItem> c)
        {
            return _RetainAll(c);
        }

        protected abstract Iesi.Collections.Generic.ISet<TItem> _Union(Iesi.Collections.Generic.ISet<TItem> a);
        public Iesi.Collections.Generic.ISet<TItem> Union(Iesi.Collections.Generic.ISet<TItem> a)
        {
            return _Union(a);
        }
        #endregion

        #region ICollection<TItem> Members

        void ICollection<TItem>.Add(TItem item)
        {
            this.Add(item);
        }

        public abstract void Clear();

        public abstract bool Contains(TItem item);
        public bool Contains(Predicate<TItem> condition)
        {
            foreach (var item in this)
            {
                if (condition(item))
                    return true;
            }
            return false;
        }

        public void CopyTo(TItem[] array, int arrayIndex)
        {
            List<int> l;
            
            int arrI = arrayIndex;
            for (int i = 0; i < this.Count; i++)
            {
                array[arrI] = this[i];
                arrI++;
            }
        }

        public abstract int Count
        {
            get;
        }

        public virtual bool IsReadOnly
        {
            get { return false; }
        }

        public abstract bool Remove(TItem item);

        #endregion

        #region IEnumerable<TItem> Members

        protected abstract IEnumerator<TItem> _GetEnumerator();
        public IEnumerator<TItem> GetEnumerator()
        {
            return _GetEnumerator();
        }


        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        #endregion

        #region ICloneable Members

        protected abstract object _Clone();
        public object Clone()
        {
            return _Clone();
        }

        #endregion

        public void RemoveDuplicatesFromList()
        {
            for (int i = 0; i < this.Count; i++)
            {
                var item1 = this[i];
                for (int j = i + 1; j < this.Count; j++)
                {
                    var item2 = this[j];
                    if (item2.PrimaryKey == item1.PrimaryKey)
                    {
                        this.RemoveAt(j);
                        j--;
                    }
                }
            }
        }

        public void Sort(Comparison<TItem> comparer)
        {

            IEnumerable<TItem> sortedList = CS.General_v3.Util.ListUtil.Sort<TItem>((IEnumerable<TItem>)this, comparer);
            this.Clear();
            this.AddRange( (IEnumerable<TItem> )sortedList);

        }

        public IList<TItem> GetAsIList()
        {
            

            MyList<TItem> list = new MyList<TItem>();
            list.AddRange(this);
            return list;
        }
        public bool ContainsItemWithPKey(TItem item)
        {
            if (item != null)
            {
                return ContainsItemWithPKey(item.PrimaryKey);
            }
            else
            {
                return false;
            }
        }
        public bool ContainsItemWithPKey(long pKey)
        {
            for (int i = 0; i < this.Count; i++)
            {
                var item = this[i];
                if (item != null)
                {
                    if (item.PrimaryKey == pKey)
                        return true;
                }
            }
            return false;
        }

        public TItem this[int index]
        {
            get
            {
                return this.ElementAt(index);
            }
        }
        public void AddRange(IEnumerable<TItem> list)
        {
            if (list != null)
            {
                foreach (var item in list)
                {
                    if (item != null)
                    {
                        this.Add(item);
                    }
                }
            }
        }


        public void RemoveAllFromDBAndList()
        {
            foreach (var item in this)
            {
                if (item != null)
                {
                    item.Delete();
                }
            }
            this.Clear();
            
        }

        public virtual List<TOutput> GetRandomItems<TOutput>(int itemsToGet) 
            where TOutput : TItem
        {
            if (itemsToGet > this.Count)
                itemsToGet = this.Count;
            List<TOutput> list = new List<TOutput>();
            var indexList = CS.General_v3.Util.Random.GetUniqueIntegerList(itemsToGet, 0, this.Count - 1);
            foreach (var index in indexList)
            {
                list.Add((TOutput)this[index]);
            }
            return list;
        }
        public virtual List<TItem> GetRandomItems(int itemsToGet)
            
        {
            return GetRandomItems<TItem>(itemsToGet);
        }
       

        public virtual TItem GetRandomItem(params TItem[] excludeItems)
        {
            bool ok = false;
            int index = -1;
            TItem item = default(TItem);
            
            if (this.Count > 0 &&  this.Count >  excludeItems.Length ) 
            {
                do
                {
                    index = CS.General_v3.Util.Random.GetInt(0, this.Count-1);
                    item = this[index];
                    ok = !excludeItems.Contains(item);
                } while (!ok);
            }

            
            return item;


        }

        /// <summary>
        /// This is just to make life easier.  You must MAKE SURE that the T fits
        /// </summary>
        /// <typeparam name="TOutput"></typeparam>
        /// <returns></returns>
        public IEnumerable<TOutput> ConvertAllByCasting<TOutput>()
        {
            return CS.General_v3.Util.ListUtil.ConvertAllByCasting<TOutput>(this);
        }
        public IList<TOutput> ConvertAll<TOutput>(Converter<TItem, TOutput> converter)
        {
            
            return CS.General_v3.Util.ListUtil.ConvertAll<TItem, TOutput>(this, converter);
        }
        public IList<long> GetAllPrimaryKeys()
        {
            List<long> list = new List<long>();
            foreach (var item in this)
            {
                if (item != null)
                {
                    long pKey = item.PrimaryKey;
                    list.Add(pKey);
                }
            }
            return list;
        }
        public int RemoveAll(Predicate<TItem> pCompare)
        {
            int result = 0;
             var afterRemoval = CS.General_v3.Util.ListUtil.RemoveAllFromEnumerable(this, pCompare, out result);
             this.Clear();
             this.AddRange(afterRemoval);
             return result;
        }
        public void RemoveAt(int index)
        {
            this.Remove(this[index]);
        }


        public void RemoveRange(int index, int count)
        {
            int cnt = count;
            while (cnt > 0 && index < this.Count)
            {
                this.RemoveAt(index);
                cnt--;
            }
        }
    }
}
