﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using NHibernate.Context;

namespace CS.General_v3.Classes.NHibernateClasses
{
    public class WebContextModule2 : IHttpModule
    {

        #region IHttpModule Members

        public void Dispose()
        {
            disposeSession(false);
        }

        private void createSession()
        {
            //SessionContextManager.Instance.CreateNewSessionForContext2();
        }
        private void disposeSession(bool throwError)
        {
           // SessionContextManager.Instance.DisposeCurrentSessionInContext2(throwErrorIfSessionDoesNotExistOrDisposed: throwError);
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += new EventHandler(context_BeginRequest);
            context.EndRequest += new EventHandler(context_EndRequest);
            


        }

        void context_EndRequest(object sender, EventArgs e)
        {
            disposeSession(true);
        }

        void context_BeginRequest(object sender, EventArgs e)
        {
            createSession();
            
        }
        

        #endregion
    }
}
