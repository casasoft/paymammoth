﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Context;
using NHibernate;
using System.Threading;
using System.Collections;
using NHibernate.Impl;
using System.Web;

namespace CS.General_v3.Classes.NHibernateClasses
{
  
    public class SessionContextManager 
    {
        private static readonly SessionContextManager _instance = new SessionContextManager();
        public static SessionContextManager Instance
        {
            get
            {
                return _instance;
            }
        }
        private SessionContextManager()
        {

        }

        [ThreadStatic]
        private static Stack<ISession> threadSessionStack;


        private Stack<ISession> httpContextSessionStack
        {
            get
            {
                return (Stack<ISession>)System.Web.HttpContext.Current.Items["___httpContextSessionStack"];
            }
            set
            {
                System.Web.HttpContext.Current.Items["___httpContextSessionStack"] = value;
            }
        }


        private Stack<ISession> getStackForCurrentContext(HttpContext context)
        {
            Stack<ISession> stack = null;
            if (context == null)
                context = System.Web.HttpContext.Current;
            if (context != null)
            {
                stack = httpContextSessionStack;
                if (stack == null)
                {
                    stack = new Stack<ISession>();
                    httpContextSessionStack = stack;
                }
            }
            else
            {
                stack = threadSessionStack;
                if (stack == null)
                {
                    stack = new Stack<ISession>();
                    threadSessionStack = stack;
                }
            }
            return stack;
        }


        public ISession GetCurrentSession(HttpContext context = null)
        {
            var stack = getStackForCurrentContext(context);
            return stack.Peek();

        }
        public ISession AttachSessionToCurrentContext(ISession session, HttpContext context = null)
        {
            var stack = getStackForCurrentContext(context);
            var newSession = session;
            stack.Push(newSession);
            return newSession;
        }
        public ISession CreateNewSessionForContext2(HttpContext context = null)
        {
            var newSession = NHManager.Instance.CreateNewSession();
            AttachSessionToCurrentContext(newSession, context);
            return newSession;
        }
        public ISession DisposeCurrentSessionInContext2(HttpContext context = null, bool throwErrorIfSessionDoesNotExistOrDisposed = true)
        {
            var stack = getStackForCurrentContext(context);
            if (stack.Count == 0 && throwErrorIfSessionDoesNotExistOrDisposed)
                throw new InvalidOperationException("No Session exists to be disposed!");
            var session = stack.Pop();
            if (!session.IsOpen && throwErrorIfSessionDoesNotExistOrDisposed)
                throw new InvalidOperationException("Session already closed / disposed!");

            NHManager.Instance.CloseSession(session);
            /*session.Flush();
            session.Close();
            session.Dispose();*/
            return session;
        }



    }
}
