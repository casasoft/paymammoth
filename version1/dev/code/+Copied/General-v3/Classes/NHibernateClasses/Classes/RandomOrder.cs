﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;

namespace CS.General_v3.Classes.NHibernateClasses.Classes
{
    public class RandomOrder : Order
    {
        public RandomOrder()
            : base("", true)
        {

        }
        public override NHibernate.SqlCommand.SqlString ToSqlString(NHibernate.ICriteria criteria, ICriteriaQuery criteriaQuery)
        {
            return new NHibernate.SqlCommand.SqlString("Rand()"); //mysql
            
        }
    }
}
