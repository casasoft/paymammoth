﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Impl;

namespace CS.General_v3.Classes.NHibernateClasses.Session
{
    public class MyNHSessionStateless : MyNHSessionBase, IStatelessSession
    {
        private NHibernate.Impl.StatelessSessionImpl s
        {
            get
            {
                return _session;
            }
        }
        private NHibernate.Impl.StatelessSessionImpl _nhSession
        {
            get
            {
                return _session;
            }
        }
        protected new NHibernate.Impl.StatelessSessionImpl _session
        {
            get { return (StatelessSessionImpl)base._session; }
        }
        

        public MyNHSessionStateless(NHibernate.Impl.StatelessSessionImpl nhSession)
            : base(nhSession)
        {
         
        }
        public override ISQLQuery CreateSqlQuery(string sql)
        {
            return this._nhSession.CreateSQLQuery(sql);

        }


        #region IStatelessSession Members

        protected internal override ITransaction _beginTransaction(System.Data.IsolationLevel isolationLevel)
        {
            return s.BeginTransaction(isolationLevel);

        }

        protected internal override ITransaction _beginTransaction()
        {
            return s.BeginTransaction();

        }

        public override void Close()
        {
            s.Close();

        }
        public override void Clear()
        {
            throw new NotImplementedException();
        }
        public override System.Data.IDbConnection Connection
        {
            get { return s.Connection; }
        }

        public  override ICriteria CreateCriteria(string entityName, string alias)
        {
            return s.CreateCriteria(entityName, alias);

        }

        public override  ICriteria CreateCriteria(string entityName)
        {
            return s.CreateCriteria(entityName);
        }

        public override  ICriteria CreateCriteria(Type entityType, string alias)
        {
            return s.CreateCriteria(entityType, alias);

        }

        public override  ICriteria CreateCriteria(Type entityType)
        {
            return s.CreateCriteria(entityType);

        }

        public override  ICriteria CreateCriteria<T>(string alias) 
        {
            return s.CreateCriteria<T>(alias);
        }

        public  override ICriteria CreateCriteria<T>() 
        {
            return s.CreateCriteria<T>();

        }

        public override IQuery CreateQuery(string queryString)
        {
            return s.CreateQuery(queryString);
        }

        public ISQLQuery CreateSQLQuery(string queryString)
        {
            return s.CreateSQLQuery(queryString);
        }

        public void Delete(string entityName, object entity)
        {
            s.Delete(entityName, entity);
        }

        public override void Delete(object entity)
        {
            s.Delete(entity);

        }

        public T Get<T>(object id, LockMode lockMode)
        {
            return s.Get<T>(id, lockMode);
        }

        public object Get(string entityName, object id, LockMode lockMode)
        {
            return s.Get(entityName, id, lockMode);
        }

        public override T Get<T>(object id)
        {
            
            return s.Get<T>(id);
        }
       
        
        public override object Get(string entityName, object id)
        {
            return s.Get(entityName, id);
        }

        public IQuery GetNamedQuery(string queryName)
        {
            return s.GetNamedQuery(queryName);
        }

        public NHibernate.Engine.ISessionImplementor GetSessionImplementation()
        {
            
            return s.GetSessionImplementation();
        }

        public object Insert(string entityName, object entity)
        {
            return s.Insert(entityName, entity);
        }

        public object Insert(object entity)
        {
            return s.Insert(entity);
        }

        public bool IsConnected
        {
            get { return s.IsConnected; }
        }

        public override bool IsOpen
        {
            get { return s.IsOpen; }
        }

        public IQueryOver<T, T> QueryOver<T>(System.Linq.Expressions.Expression<Func<T>> alias) where T : class
        {
            return s.QueryOver<T>(alias);
        }

        public void Refresh(string entityName, object entity, LockMode lockMode)
        {
            s.Refresh(entityName, entity, lockMode);
        }

        public void Refresh(object entity, LockMode lockMode)
        {
            s.Refresh(entity, lockMode);
        }

        public void Refresh(string entityName, object entity)
        {
            s.Refresh(entityName, entity);
        }

        public override void Refresh(object entity)
        {
            s.Refresh(entity);
        }

        public IStatelessSession SetBatchSize(int batchSize)
        {
            return s.SetBatchSize(batchSize);
        }

        public override ITransaction Transaction
        {
            get { return s.Transaction; }
        }

        public void Update(string entityName, object entity)
        {
            s.Update(entityName, entity);
        }

        public override void Update(object entity)
        {
            s.Update(entity);
        }

        #endregion

        #region IDisposable Members

        protected override void _dispose()
        {
            s.Dispose();
        }

        #endregion

        public override IQueryOver<T, T> QueryOver<T>()
        {
            return s.QueryOver<T>();

        }

        public override void Flush()
        {
            try
            {
                s.Flush();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public override ISession GetAsISession()
        {
            throw new InvalidOperationException("A stateless session can never be retrieved as an ISession");

        }

        public override IStatelessSession GetAsIStatelessSession()
        {
            return _session;

        }

        public override object Save(object o)
        {
            this.Insert(o);
            return o;
            
        }

        public override void Evict(object o)
        {

            throw new NotImplementedException("Not implemented in an IStatelessSession");
        }

        public override object SaveOrUpdateCopy(object o)
        {
            throw new NotImplementedException("Not implemented in an IStatelessSession");
        }

        public override object Merge(object o)
        {
            throw new NotImplementedException("Not implemented in an IStatelessSession");
        }

        public override void SaveOrUpdate(object o)
        {
            throw new NotImplementedException("Not implemented in an IStatelessSession");
        }

        public override void Lock(object obj, LockMode lockMode)
        {
            throw new NotImplementedException("Not implemented in an IStatelessSession");
        }

        public override void Lock(string entityName, object obj, LockMode lockMode)
        {
            throw new NotImplementedException("Not implemented in an IStatelessSession");
        }

        #region IStatelessSession Members

        ITransaction IStatelessSession.BeginTransaction(System.Data.IsolationLevel isolationLevel)
        {
            return _beginTransaction(isolationLevel);
            
        }

        ITransaction IStatelessSession.BeginTransaction()
        {
            return _beginTransaction();
            
        }

        #endregion

        public override IMultiCriteria CreateMultiCriteria()
        {
            throw new NotImplementedException("Not available in an IStatelessSession");
        }

        public override IMultiQuery CreateMultiQuery()
        {
            throw new NotImplementedException("Not available in an IStatelessSession");
        }

        public override IQuery CreateQuery(IQueryExpression queryExpression)
        {
            throw new NotImplementedException("Not available in an IStatelessSession");
        }
    }
}
