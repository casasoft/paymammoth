﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using System.Data;
using NHibernate.Impl;
using CS.General_v3.Classes.NHibernateClasses.Transactions;

namespace CS.General_v3.Classes.NHibernateClasses.Session
{
    public abstract class MyNHSessionBase : IDisposable
    {

        public abstract ISQLQuery CreateSqlQuery(string sql);

        public static MyNHSessionBase LoadFromSession(NHibernate.Impl.SessionImpl session)
        {
            return new MyNHSessionNormal(session);
        }
        public static MyNHSessionBase LoadFromSession(NHibernate.Impl.StatelessSessionImpl session)
        {
            return new MyNHSessionStateless(session);
        }



        //public bool AutoBeginTransaction { get; set; }
        public abstract IMultiCriteria CreateMultiCriteria();

        public abstract IMultiQuery CreateMultiQuery();
        public abstract IQuery CreateQuery(string queryString);
        public abstract IQuery CreateQuery(IQueryExpression queryExpression);
        protected AbstractSessionImpl _session { get; private set; }
        public MyNHSessionBase(AbstractSessionImpl session)
        {

            
            this._session = session;
            
            checkTransaction();


        }


        public abstract ITransaction Transaction { get; }

        protected void checkTransaction()
        {

            
        }

        public void RollbackCurrentTransaction()
        {
            if (Transaction != null)
            {
                Transaction.Rollback();

                checkTransaction();
                
            }

            
        }
        public void CommitCurrentTransaction(bool beginAnotherTransaction = true)
        {
            if (Transaction != null)
            {

                Transaction.Commit();
                if (beginAnotherTransaction)
                    checkTransaction();
            }


        }

        public abstract IDbConnection Connection { get; }

        public abstract IQueryOver<T, T> QueryOver<T>() where T : class;
        public abstract void Lock(object obj, LockMode lockMode);
        public abstract void Lock(string entityName, object obj, LockMode lockMode);
        public abstract bool IsOpen { get; }
        public abstract void Close();
        public abstract void Flush();
        public abstract void Clear();

        #region IDisposable Members

        protected abstract void _dispose();
        


        public void Dispose()
        {
            //if (this.AutoBeginTransaction && this.Transaction != null && this.Transaction.IsActive)
            //{
            //    CommitCurrentTransaction(beginAnotherTransaction: false);
            //}
            if (this.Transaction != null)
                this.Transaction.Dispose();
            
            _dispose();
        }


        public abstract ISession GetAsISession();
        public abstract IStatelessSession GetAsIStatelessSession();
        #endregion
        public abstract ICriteria CreateCriteria(string entityName, string alias);

        public abstract ICriteria CreateCriteria(string entityName);

        public abstract ICriteria CreateCriteria(Type persistentClass, string alias);
        public abstract ICriteria CreateCriteria(Type persistentClass);

        public abstract ICriteria CreateCriteria<T>(string alias)where T : class;

        

        public abstract ICriteria CreateCriteria<T>() where T : class;

        public abstract T Get<T>(object id);
        public abstract object Get(string entityName, object id);

        public abstract void Refresh(object o);

        public abstract void Delete(object o);

        public abstract object Save(object o);
        public abstract void Update(object o);
        public abstract void Evict(object o);

        [Obsolete("Use Merge(object) instead")]
        public abstract object SaveOrUpdateCopy(object o);
        public abstract object Merge(object o);


        public abstract void SaveOrUpdate(object o);


        public MyTransaction BeginTransaction()
        {
           
            MyTransaction result = new MyTransaction(this);
            result.Begin();
            return result;
        }
        public MyTransaction BeginTransaction(IsolationLevel isolationLevel)
        {
            //var nhTransaction = _beginTransaction(isolationLevel);
            MyTransaction result = new MyTransaction(this);
            result.Begin(isolationLevel);
            return result;
        }
        protected internal abstract ITransaction _beginTransaction();
        protected internal abstract ITransaction _beginTransaction(IsolationLevel isolationLevel);
    }
}
