﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;

namespace CS.General_v3.Classes.NHibernateClasses.Session
{
    public class LazySession : BaseLazySession, IDisposable
    {

        protected override MyNHSessionBase createSession()
        {
            return NHClasses.NhManager.CreateNewSession();
        }
        public new MyNHSessionNormal GetSession()
        {
            return (MyNHSessionNormal) base.GetSession();
        }
    }
}
