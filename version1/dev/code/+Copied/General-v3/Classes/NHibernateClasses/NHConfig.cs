﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Extensions;
using FluentNHibernate.Cfg;
using CS.General_v3.Classes.NHibernateClasses.FluentNH.Conventions;
using System.Reflection;
using FluentNHibernate.Mapping;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using log4net;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using MySql.Data.MySqlClient;
using FluentNHibernate.Cfg.Db;

namespace CS.General_v3.Classes.NHibernateClasses
{
    public class NHConfig
    {
        public static NHConfig CreateLastInstance()
        {
            return CS.General_v3.Util.ReflectionUtil.CreateObjectWithPrivateConstructorForLastType<NHConfig>();
        }

        /// <summary>
        /// Returns whether configuration exists already and is valid, or needs to be rebuilt (non existant or 'CheckMappings' is true)
        /// </summary>
        /// <returns></returns>
        public static bool CheckIfConfigurationExistsAndValid()
        {
            bool valid = false;
            string localPath = CS.General_v3.Util.PageUtil.MapPath(SERIALIZED_CFG_PATH);
            if (!CS.General_v3.Settings.Database.CheckMappingsAndOthers && File.Exists(localPath))
            {
                valid = true;
            }
            return valid;

        }

        private const string SERIALIZED_CFG_PATH = "/App_Data/bin/nh_config.bin";


        private static readonly ILog _log = log4net.LogManager.GetLogger(typeof(NHConfig));
        private Iesi.Collections.Generic.HashedSet<Assembly> _mappedAssemblies = null;
        private Iesi.Collections.Generic.HashedSet<Type> _mappedTypes = null;
        public NHConfig()
        {
            _mappedAssemblies = new Iesi.Collections.Generic.HashedSet<Assembly>();
            _mappedTypes = new Iesi.Collections.Generic.HashedSet<Type>();
        }

        public NHConfig(Iesi.Collections.Generic.HashedSet<Type> mappedTypes, Iesi.Collections.Generic.HashedSet<Assembly> mappedAssemblies)
        {
            _mappedAssemblies = mappedAssemblies;
            _mappedTypes = mappedTypes;
            
        }
        public void SetMappings(IEnumerable<Type> mappedTypes, IEnumerable<Assembly> mappedAssemblies)
        {
            if (mappedTypes != null)
            {
                _mappedTypes.AddAll(mappedTypes);
            }
            if (mappedAssemblies != null)
            {
                _mappedAssemblies.AddAll(mappedAssemblies);
            }
        }



        public Configuration GetConfiguration()
        {
            Configuration cfg = loadConfigurationFromFile();
            if (cfg == null)
            {
                cfg = initNHibernateViaFluent();
                _cfgToBeSaved = cfg;

                NotLoadedFromFile = true;
            }
            return cfg;

          

        }

        public bool NotLoadedFromFile { get; set; }

        protected virtual void initConfigProperties(Configuration config)
        {
            //config.Properties["current_session_context_class"] = "web";

            {
                var interceptor = CS.General_v3.Classes.Settings.SettingsMain.GetNhibernateInterceptor();
                if (interceptor != null)
                {
                    config.SetInterceptor(interceptor);
                }
            }


            config = config.SetProperty(NHibernate.Cfg.Environment.UseProxyValidator, "false"); //this removes the validation for proxies, such that they all require the virtual keyword

            config = config.SetProperty(NHibernate.Cfg.Environment.DefaultSchema, CS.General_v3.Settings.Database.DatabaseName);

            config = config.SetProperty(NHibernate.Cfg.Environment.UseSecondLevelCache, "true");

            config = config.SetProperty(NHibernate.Cfg.Environment.UseQueryCache, "true");

            config = config.SetProperty(NHibernate.Cfg.Environment.FormatSql, "true");
            config = config.SetProperty(NHibernate.Cfg.Environment.ShowSql, "false");
            
            config = config.SetProperty(NHibernate.Cfg.Environment.CacheProvider, typeof(NHibernate.Caches.SysCache.SysCacheProvider).AssemblyQualifiedName);
            {
                bool useReflectionOptimizer = true;
                string sUseReflectionOptimizer = (useReflectionOptimizer ? "true" : "false");
                NHibernate.Cfg.Environment.UseReflectionOptimizer = useReflectionOptimizer;
                config = config.SetProperty(NHibernate.Cfg.Environment.PropertyUseReflectionOptimizer, sUseReflectionOptimizer);
                config = config.SetProperty("hibernate.use_reflection_optimizer", sUseReflectionOptimizer);
            }
            config.Properties["relativeExpiration"] = "300";



        }
        
        private void addCustomConventions(ref FluentConfiguration cfg)
        {
            cfg = cfg.Mappings(m => m.FluentMappings.Conventions.Add(new EnumConvention(),
                new PrimaryKeyGeneratorConvention(), new PropertyConventions(), new CollectionConvention()));

        }


        protected virtual IPersistenceConfigurer getDatabasePersistenceConfigurer(int maxFetchDepth = 3)
        {
            string connectionString = "Server=" + CS.General_v3.Settings.Database.Host + ";" +
                  "Database=" + CS.General_v3.Settings.Database.DatabaseName + ";" +
                  "Uid=" + CS.General_v3.Settings.Database.User + ";" +
                  "Pwd=" + CS.General_v3.Settings.Database.Pass + ";" +
                  "Port=" + CS.General_v3.Settings.Database.Port + ";";

            var persistenceConfig =
                    FluentNHibernate.Cfg.Db.MySQLConfiguration.Standard
                    .ConnectionString(connectionString)

                    //.Cache(c => c.UseQueryCache()
                // .ProviderClass<NHibernate.Cache.HashtableCacheProvider>())
                //.MaxFetchDepth(3)

                    .AdoNetBatchSize(35);

            //  persistenceConfig.DoNot.UseReflectionOptimizer();
            //persistenceConfig.UseReflectionOptimizer();
            return persistenceConfig;
        }

        protected virtual void updateConfigurationWithCustomChanges(FluentConfiguration cfg)
        {

        }

        private Configuration initNHibernateViaFluent()
        {
            bool skipNormalMaps = false;
            //skipNormalMaps = true;


            var cfg = Fluently.Configure();

            addCustomConventions(ref cfg);
            
            IPersistenceConfigurer dbCfg = null;
            dbCfg = getDatabasePersistenceConfigurer();
            
            cfg = cfg.Database(dbCfg);
            var typesList = _mappedTypes.ToList();
            if (!skipNormalMaps)
            {
                for (int i = 0; i < typesList.Count; i++)
                {
                    var t = typesList[i];
                    cfg = cfg.Mappings(m => m.FluentMappings.Add(t));
                }
            }
            var customMappings = CS.General_v3.Classes.Settings.SettingsMain._getCustomNhibernateMappings();
            if (customMappings.IsNotNullOrEmpty())
            {
                foreach (var t in customMappings)
                {
                    cfg = cfg.Mappings(m => m.FluentMappings.Add(t));
                }
            }
            if (!skipNormalMaps)
            {
                foreach (var a in _mappedAssemblies)
                {
                    cfg = cfg.Mappings(m => m.FluentMappings.AddFromAssembly(a));
                }
            }
            updateConfigurationWithCustomChanges(cfg);
            Configuration nhConfig = null;
            try
            {
                cfg = cfg.ExposeConfiguration(initConfigProperties);
                nhConfig = cfg.BuildConfiguration();
            }
            catch (FluentConfigurationException ex)
            {
                int k = 5;
                throw;
            }
            //  initConfigProperties(nhConfig);
           
            return nhConfig;
        }

        public virtual void CreateSchema(Configuration config)
        {
            
            SchemaExport schema = new SchemaExport(config);
            schema.Execute(true,true,false);
            

        }
        protected internal virtual void checkSchema(Configuration config)
        {
            if (CS.General_v3.Settings.Database.CheckSchema)
            {
                UpdateSchema(config);
            }
        }

        public void UpdateSchema(Configuration config)
        {
            _log.Debug("Starting updating schema");
            var session = NHManager.Instance.CreateNewSessionForContext();
          //  session = NHManager.Instance.CreateNewSessionForContext();
            using (var transaction =  session.BeginTransaction())
            {
                SchemaUpdate upd = new SchemaUpdate(config);
                try
                {
                    
                    upd.Execute(false, true);
                }
                catch (MySqlException ex)
                {
                    int k = 5;
                }
                IList<Exception> exList = upd.Exceptions;
                if (_log.IsDebugEnabled)
                {

                    _log.Debug("Finished updating schema, Exception Count: " + upd.Exceptions.Count);
                    for (int i = 0; i < upd.Exceptions.Count; i++)
                    {
                        var ex = upd.Exceptions[i];
                        _log.Debug("Schema update / create - Exception (Warning) " + (i + 1) + ": " + ex.Message, ex);

                    }
                }
                transaction.Commit();
            }
            NHManager.Instance.DisposeCurrentSessionInContext();
        }

        private Configuration loadConfigurationFromFile()
        {
            Configuration cfg = null;
            string localPath = CS.General_v3.Util.PageUtil.MapPath(SERIALIZED_CFG_PATH);
            if (!CS.General_v3.Settings.Database.CheckMappingsAndOthers) //dont check mappings if possible
            {
                cfg = CS.General_v3.Util.ReflectionUtil.DeserializeObjectFromFile<Configuration>(localPath);
            }
            else
            {
                CS.General_v3.Util.IO.DeleteFile(localPath);
            }

            return cfg;

        }

        private Configuration _cfgToBeSaved = null;

        private void saveConfigurationToFile()
        {
            Configuration config = _cfgToBeSaved;
            string localPath = CS.General_v3.Util.PageUtil.MapPath(SERIALIZED_CFG_PATH);
            if (!CS.General_v3.Settings.Database.CheckMappingsAndOthers)
            {
                CS.General_v3.Util.ReflectionUtil.SerializeObjectToFile(config, localPath);
            }
            _cfgToBeSaved = null;
        }

        public void SaveConfigurationToFile(Configuration config)
        {
            if (NotLoadedFromFile)
            {
                _cfgToBeSaved = config;
                saveConfigurationToFile();
            }
        }
    }
}
