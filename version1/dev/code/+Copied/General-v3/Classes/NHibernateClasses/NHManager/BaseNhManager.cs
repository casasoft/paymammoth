﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.NHConfiguration;
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.NHibernateClasses.SessionContextManagers;
using log4net;
using MySql.Data.MySqlClient;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Exceptions;
using NHibernate.Tool.hbm2ddl;

namespace CS.General_v3.Classes.NHibernateClasses.NHManager
{
    public abstract class BaseNhManager: INhManager
    {
        private ILog _log = null;
        

        protected BaseNhManager()
        {
            _log = log4net.LogManager.GetLogger(this.GetType());
        }


        private static readonly object _padlock = new object();
        private NHibernate.Impl.SessionFactoryImpl _sessionFactory = null;

        public void Initialise()
        {
            lock (_padlock)
            {
                createSessionFactory();
                _initialised = true;
            }

        }
        /*
        public SessionImpl GetCurrentSessionFromContext()
        {
            var f = _sessionFactory;
            SessionImpl session = null;
            try
            {
                //session =  f.GetCurrentSession();
                session = ContextAndThreadSessionManager.GetCurrentSession();
            }
            catch (Exception ex)
            {
                int k = 5;
                throw;
            }
            return session;
            
        }
        */


        private Iesi.Collections.Generic.HashedSet<Assembly> _mappedAssemblies = new Iesi.Collections.Generic.HashedSet<Assembly>();
        private Iesi.Collections.Generic.HashedSet<Type> _mappedTypes = new Iesi.Collections.Generic.HashedSet<Type>();
        public void AddMappingFor(Assembly a)
        {
            checkMappingForInitialised();
            _mappedAssemblies.Add(a);
        }
        public void AddMappingFor(Type t)
        {
            checkMappingForInitialised();
            _mappedTypes.Add(t);
        }
        private void checkMappingForInitialised()
        {
            if (_initialised) throw new InvalidOperationException("Cannot add a mapping for a type after NH has already been initialised");
        }
        public void AddMappingFor<T>()
        {
            
            AddMappingFor(typeof(T));
            
        }
        public void AddMappingForAssemblyOf(Type t)
        {
            AddMappingFor(t.Assembly);
        }
        public void AddMappingForAssemblyOf<T>()
        {
            AddMappingFor(typeof(T).Assembly);
        }


       
   
        private bool _initialised = false;
        public ISessionFactory GetSessionFactory()
        {
            return _sessionFactory;
        }

        private INHConfig nhConfig
        {
            get { return NHClasses.NHConfig; }
        }


        public void RecreateSessionFactory()
        {
            createSessionFactory();
        }

        private void createSessionFactory()
        {
            
            nhConfig.SetMappings(_mappedTypes);
            _cfg = nhConfig.GetConfiguration();
            
            
            try
            {
                _log.Debug("Creating session factory - Started" );
          //  _sessionFactory = CS.General_v3.Util.ReflectionUtil.DeserializeObjectFromFile<NHibernate.Impl.SessionFactoryImpl>(@"D:\My Documents\test_factory.bin");

                _sessionFactory = (NHibernate.Impl.SessionFactoryImpl) _cfg.BuildSessionFactory();


              //  CS.General_v3.Util.ReflectionUtil.SerializeObjectToFile(_sessionFactory, @"D:\My Documents\test_factory.bin");
                _log.Debug("Creating session factory - Finished");

            }
            catch (Exception ex)
            {
                int k = 5;
                throw;
            }


            CheckSchema();
            
            nhConfig.SaveConfigurationToFile(_cfg);

        }

        public virtual void CheckSchema()
        {
            
            if (CS.General_v3.Settings.Database.GetWhetherToCheckSchema())
            {
                var session = CreateNewSessionForContextAndReturn();
            
                switch (CS.General_v3.Settings.Database.SchemaCheckType)
                {
                    case CS.General_v3.Settings.Database.SCHEMA_CHECK_TYPE.Update: UpdateSchema(); break;
                    case CS.General_v3.Settings.Database.SCHEMA_CHECK_TYPE.Create: CreateSchema(); break;
                    default:
                        throw new InvalidOperationException("Invalid SchemaCheckType");
                }
                DisposeCurrentSessionInContext();
                CS.General_v3.Settings.Database.SaveCheckSchemaFile();
            }
        }
        public void CreateSchema()
        {
            _log.Debug("Starting creating schema");

            var session = GetCurrentSessionFromContext();
            using (var transaction = session.BeginTransaction())
            {
                SchemaExport upd = new SchemaExport(_cfg);
                try
                {

                    upd.Create(true,true);
                    
                }
                catch (MySqlException ex)
                {
                    int k = 5;
                }
                
                transaction.Commit();
            }
            DisposeCurrentSessionInContext();
        }
        public void UpdateSchema2()
        {
            SchemaUpdate upd = new SchemaUpdate(_cfg);
            //upd.Execute(
        }

        public void UpdateSchema()
        {
           // UpdateSchema2();
          //  return;
            _log.Debug("Starting updating schema");
            var session =  GetCurrentSessionFromContext();
            CS.General_v3.Util.Stopwatch.ResetAndStartTimer(100);
            SchemaUpdate upd = null;
            for (int i = 0; i < 1; i++) //only once
            {
                
              //  using (var t = session.BeginTransaction())
                {
                    upd = new SchemaUpdate(_cfg);
                    try
                    {

                        upd.Execute(false, true);
                    }
                    catch (MySqlException ex)
                    {
                        int k = 5;
                    }
                   // t.Commit();
                }
            }
            CS.General_v3.Util.Stopwatch.EndTimer(100);
            double ms = CS.General_v3.Util.Stopwatch.ReadTimerMilleSec(100);
            IList<Exception> exList = upd.Exceptions;
            if (_log.IsDebugEnabled)
            {

                _log.Debug("Finished updating schema, Exception Count: " + upd.Exceptions.Count);
                for (int i = 0; i < upd.Exceptions.Count; i++)
                {
                    var ex = upd.Exceptions[i];
                    _log.Debug("Schema update / create - Exception (Warning) " + (i + 1) + ": " + ex.Message, ex);

                }
            }
               
            
           
        }

        private static Configuration _cfg = null;

        public MyNHSessionNormal CreateNewSession()
        {
            if (_sessionFactory == null)
                throw new InvalidOperationException("No session factory exists. Make sure you have the database settings specified in the App_Data/settings.user.xml file");

            var session = (NHibernate.Impl.SessionImpl)_sessionFactory.OpenSession();
            return new MyNHSessionNormal(session);

        }
        public MyNHSessionStateless CreateNewStatelessSession()
        {
            if (_sessionFactory == null)
                throw new InvalidOperationException("No session factory exists. Make sure you have the database settings specified in the App_Data/settings.user.xml file");

            var session = (NHibernate.Impl.StatelessSessionImpl)_sessionFactory.OpenStatelessSession();
            return new MyNHSessionStateless(session);


        }


        protected ISessionContextManager getSessionContextManager()
        {
            return NHClasses.GetSessionManager();
        }

        public bool CheckSessionExistsForContext()
        {
            var manager = getSessionContextManager();
            MyNHSessionBase session = manager.GetCurrentSession(throwErrorIfNoSessionexists: false);
            
            return session != null;
        }

        public MyNHSessionBase GetCurrentSessionFromContext()
        {

            return getSessionContextManager().GetCurrentSession();

        }
        public MyNHSessionNormal CreateNewSessionForContextAndReturn()
        {
            return (MyNHSessionNormal)getSessionContextManager().CreateNewSessionForContextAndReturn();
            
        }
        public void CreateNewSessionForContext()
        {
            getSessionContextManager().CreateNewSessionForContext();

        }
        public MyNHSessionBase DisposeCurrentSessionInContext(bool throwErrorIfSessionDoesNotExistOrDisposed = true)
        {
            return getSessionContextManager().DisposeCurrentSessionInContext(throwErrorIfSessionDoesNotExistOrDisposed: throwErrorIfSessionDoesNotExistOrDisposed);
        }
        public MyNHSessionStateless CreateNewStatelessSessionForContext()
        {
            return getSessionContextManager().CreateNewStatelessSessionForContext();

        }
        


        public void FlushCurrentSessionInContext()
        {
            try
            {
                var session = GetCurrentSessionFromContext();
                session.Flush();
            }
            catch (GenericADOException ex)
            {
                int k = 5;
                throw;
            }
            
        }

        #region INhManager Members


        public abstract void CloseSession(MyNHSessionBase session, bool commitOrRollbackExistingTransactions = true);

        #endregion

        #region INhManager Members


        public void DisposeCurrentSessionFactory()
        {
            this._sessionFactory.Close();
            this._sessionFactory.Dispose();
            this._sessionFactory = null;
            
        }

        #endregion
    }
}
