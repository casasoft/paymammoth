﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.Session;
using MySql.Data.MySqlClient;
using NHibernate;
using NHibernate.Cfg;
using System.Reflection;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Exceptions;
using NHibernate.Impl;
using log4net;
using CS.General_v3.Classes.NHibernateClasses.SessionContextManagers;
using CS.General_v3.Classes.NHibernateClasses.NHConfiguration;

namespace CS.General_v3.Classes.NHibernateClasses.NHManager
{
    public class TestNhManager : BaseNhManager, INhManager
    {

        public static TestNhManager Instance
        {
            get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<TestNhManager>(); }
        }

        public override void CheckSchema()
        {
            //do nothing
            
        }
        public override void CloseSession(MyNHSessionBase session, bool commitOrRollbackExistingTransactions = true)
        {
            session.Flush();
            //session.Clear();
        }

    }
}
