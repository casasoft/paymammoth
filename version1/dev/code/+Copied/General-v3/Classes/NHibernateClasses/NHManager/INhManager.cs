﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.NHConfiguration;
using CS.General_v3.Classes.NHibernateClasses.Session;
using NHibernate;

namespace CS.General_v3.Classes.NHibernateClasses.NHManager
{
    public interface INhManager
    {
        void Initialise();
        void AddMappingFor(Assembly a);
        void AddMappingFor(Type t);
        void AddMappingFor<T>();
        void AddMappingForAssemblyOf(Type t);
        void AddMappingForAssemblyOf<T>();
        ISessionFactory GetSessionFactory();
        void CheckSchema();
        void CreateSchema();
        void UpdateSchema();

        MyNHSessionNormal CreateNewSession();
        MyNHSessionStateless CreateNewStatelessSession();
        
        bool CheckSessionExistsForContext();
        MyNHSessionBase GetCurrentSessionFromContext();
        MyNHSessionNormal CreateNewSessionForContextAndReturn();
        void CreateNewSessionForContext();
        MyNHSessionBase DisposeCurrentSessionInContext(bool throwErrorIfSessionDoesNotExistOrDisposed = true);
        MyNHSessionStateless CreateNewStatelessSessionForContext();
        void CloseSession(MyNHSessionBase session, bool commitOrRollbackExistingTransactions = true);
        void FlushCurrentSessionInContext();
        void RecreateSessionFactory();

        void DisposeCurrentSessionFactory();
    }
}
