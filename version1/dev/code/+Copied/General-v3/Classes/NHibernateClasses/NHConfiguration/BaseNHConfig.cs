﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.DBConfig;


using log4net;
using NHibernate.Cfg;
using NHibernate.Mapping.ByCode;
using NHibernate.Search.Event;
using NHibernate.Tool.hbm2ddl;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Search.Store;

namespace CS.General_v3.Classes.NHibernateClasses.NHConfiguration
{
    public abstract class BaseNHConfig
    {
        protected ILog _log = null;
        public BaseNHConfig()
        {
           _mappedTypes = new Iesi.Collections.Generic.HashedSet<Type>();
            init();
        }
        private void init()
        {
            _log = log4net.LogManager.GetLogger(this.GetType());
            
        }

        public BaseNHConfig(Iesi.Collections.Generic.HashedSet<Type> mappedTypes, Iesi.Collections.Generic.HashedSet<Assembly> mappedAssemblies)
        {
            _mappedTypes = mappedTypes;
            init();
            
        }
        protected abstract string getSerializedConfigPath();
        
        private Iesi.Collections.Generic.HashedSet<Type> _mappedTypes = null;
        /// <summary>
        /// Returns whether configuration exists already and is valid, or needs to be rebuilt (non existant or 'CheckMappings' is true)
        /// </summary>
        /// <returns></returns>
        public  bool CheckIfConfigurationExistsAndValid()
        {
            bool valid = false;
            string localPath = CS.General_v3.Util.PageUtil.MapPath(getSerializedConfigPath());
            if (!CS.General_v3.Settings.Database.CheckMappingsAndOthers && File.Exists(localPath))
            {
                valid = true;
            }
            return valid;

        }

        public void SetMappings(IEnumerable<Type> mappedTypes)
        {
            if (mappedTypes != null)
            {
                _mappedTypes.AddAll(mappedTypes);
            }
           
        }


        private void initialiseLuceneNet(Configuration cfg)
        {
            
            cfg.SetListener(NHibernate.Event.ListenerType.PostUpdate, new FullTextIndexEventListener());
            cfg.SetListener(NHibernate.Event.ListenerType.PostInsert, new FullTextIndexEventListener());
            cfg.SetListener(NHibernate.Event.ListenerType.PostDelete, new FullTextIndexEventListener());

            string nhIndexPath = CS.General_v3.Util.PageUtil.MapPath("/App_Data/LuceneIndex");

            cfg.SetProperty("hibernate.search.default.directory_provider", typeof(FSDirectoryProvider).AssemblyQualifiedName);
            cfg.SetProperty("hibernate.search.default.indexBase", nhIndexPath);
            cfg.SetProperty("hibernate.search.default.indexBase.create", "true");

            var luceneMap = CS.General_v3.Util.ReflectionUtil.GetTypeFromExecutingAssemblies("OneOutOf100.Modules.ArticleModule.ArticleLuceneMap");
            if (luceneMap != null)
            {

                cfg.SetProperty("hibernate.search.mapping", luceneMap.AssemblyQualifiedName);
            }


        }

        public Configuration GetConfiguration()
        {
            Configuration cfg = loadConfigurationFromFile();
            
            if (cfg == null)
            {
                cfg = createNHibernateConfig();
                _cfgToBeSaved = cfg;
                
                NotLoadedFromFile = true;
            }
            return cfg;



        }

        public bool NotLoadedFromFile { get; set; }

        protected virtual void initConfigProperties(Configuration config)
        {
            //config.Properties["current_session_context_class"] = "web";

            {
                var interceptor = CS.General_v3.Classes.Application.AppInstance.Instance.GetNhibernateInterceptor();
                if (interceptor != null)
                {
                    config.SetInterceptor(interceptor);
                }
            }


            config = config.SetProperty(NHibernate.Cfg.Environment.UseProxyValidator, "false"); //this removes the validation for proxies, such that they all require the virtual keyword

          


            config = config.SetProperty(NHibernate.Cfg.Environment.UseSecondLevelCache, "true");

            config = config.SetProperty(NHibernate.Cfg.Environment.UseQueryCache, "true");

            config = config.SetProperty(NHibernate.Cfg.Environment.FormatSql, "true");
            config = config.SetProperty(NHibernate.Cfg.Environment.ShowSql, "false");

            config = config.SetProperty(NHibernate.Cfg.Environment.CacheProvider, typeof(NHibernate.Caches.SysCache.SysCacheProvider).AssemblyQualifiedName);
            {
                bool useReflectionOptimizer = true;
                string sUseReflectionOptimizer = (useReflectionOptimizer ? "true" : "false");
                NHibernate.Cfg.Environment.UseReflectionOptimizer = useReflectionOptimizer;
                config = config.SetProperty(NHibernate.Cfg.Environment.PropertyUseReflectionOptimizer, sUseReflectionOptimizer);
                config = config.SetProperty("hibernate.use_reflection_optimizer", sUseReflectionOptimizer);
            }
            config.Properties["relativeExpiration"] = "300";

           // _fluentDbConfig.InitConfigProperties(config);

        }

        



        protected INHibernateDbConfig getDbConfigBasedOnSettingsDatabaseType()
        {
            switch (CS.General_v3.Settings.Database.DatabaseType)
            {
                case CS.General_v3.Settings.Database.DATABASE_TYPE.MySQL: return new DBConfig.MySqlDbConfig();
                case CS.General_v3.Settings.Database.DATABASE_TYPE.SQLite: return new DBConfig.SQLiteDbConfig();
            }
            throw new InvalidOperationException("Invalid Database Type");
        }

        private void initMappings(Configuration cfg)
        {
            bool skipNormalMaps = false;
            var mapper = new ModelMapper();
            var typesList = _mappedTypes.ToList();
            if (!skipNormalMaps)
            {
                for (int i = 0; i < typesList.Count; i++)
                {
                    var t = typesList[i];
                    mapper.AddMapping(t);
                }
            }
            var customMappings = CS.General_v3.Classes.Application.AppInstance.Instance._getCustomNhibernateMappings();
            if (customMappings.IsNotNullOrEmpty())
            {
                foreach (var t in customMappings)
                {
                    mapper.AddMapping(t);
                    
                }
            }
            try
            {
                cfg.AddMapping(mapper.CompileMappingForAllExplicitlyAddedEntities());
            }
            catch (Exception ex)
            {
                int k = 5;
                throw;
            }
            
        }


        
        private Configuration createNHibernateConfig()
        {
            
            //skipNormalMaps = true;


            var cfg = new Configuration();

        

            //IPersistenceConfigurer dbCfg = null;
            var dbConfig = getDbConfigBasedOnSettingsDatabaseType();
            dbConfig.ConfigureDatabase(cfg);
            CS.General_v3.Classes.Application.AppInstance.Instance.onPreNHibernateConfigMapping(cfg);

            //dbCfg = _fluentDbConfig.GetDbConfiguration();

           // cfg = cfg.Database(dbCfg);
            initConfigProperties(cfg);
            initMappings(cfg);
            initialiseLuceneNet(cfg);

            

            return cfg;
        }

        public virtual void CreateSchema(Configuration config)
        {

            SchemaExport schema = new SchemaExport(config);
            schema.Execute(true, true, false);


        }


        private Configuration loadConfigurationFromFile()
        {
            Configuration cfg = null;
            string localPath = CS.General_v3.Util.PageUtil.MapPath(getSerializedConfigPath());
            if (!CS.General_v3.Settings.Database.CheckMappingsAndOthers) //dont check mappings if possible
            {
                cfg = CS.General_v3.Util.ReflectionUtil.DeserializeObjectFromFile<Configuration>(localPath);
            }
            else
            {
                CS.General_v3.Util.IO.DeleteFile(localPath);
            }

            return cfg;

        }

        private Configuration _cfgToBeSaved = null;

        private void saveConfigurationToFile()
        {
            Configuration config = _cfgToBeSaved;
            string localPath = CS.General_v3.Util.PageUtil.MapPath(getSerializedConfigPath());
            if (!CS.General_v3.Settings.Database.CheckMappingsAndOthers)
            {
                CS.General_v3.Util.ReflectionUtil.SerializeObjectToFile(config, localPath);
            }
            _cfgToBeSaved = null;
        }

        public void SaveConfigurationToFile(Configuration config)
        {
            if (NotLoadedFromFile)
            {
                _cfgToBeSaved = config;
                saveConfigurationToFile();
            }
        }

    }
}
