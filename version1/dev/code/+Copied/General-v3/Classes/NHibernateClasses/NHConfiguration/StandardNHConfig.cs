﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.NHConfiguration;
using CS.General_v3.Extensions;

using System.Reflection;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using log4net;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using MySql.Data.MySqlClient;

using CS.General_v3.Classes.NHibernateClasses.DBConfig;

namespace CS.General_v3.Classes.NHibernateClasses
{
    public class StandardNHConfig : BaseNHConfig, INHConfig
    {
        public static StandardNHConfig CreateLastInstance()
        {
            return CS.General_v3.Util.ReflectionUtil.CreateObjectWithPrivateConstructorForLastType<StandardNHConfig>();
        }

        private const string SERIALIZED_CFG_PATH = "/App_Data/nhSavedMappings/nh_config.bin";



        private StandardNHConfig()
        {
            
        }

        private StandardNHConfig(Iesi.Collections.Generic.HashedSet<Type> mappedTypes, Iesi.Collections.Generic.HashedSet<Assembly> mappedAssemblies)
            : base(mappedTypes, mappedAssemblies)
        {
            
            
        }


        protected override string getSerializedConfigPath()
        {
            return SERIALIZED_CFG_PATH;
            
        }


    }
}
