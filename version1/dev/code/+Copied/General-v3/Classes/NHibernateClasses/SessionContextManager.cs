﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Context;
using NHibernate;
using System.Threading;
using System.Collections;
using NHibernate.Impl;
using System.Web;

namespace CS.General_v3.Classes.NHibernateClasses
{
  
    public class SessionContextManager
    {
        private static readonly SessionContextManager _instance = new SessionContextManager();
        public static SessionContextManager Instance
        {
            get
            {
                return _instance;
            }
        }
        private SessionContextManager()
        {

        }

        [ThreadStatic]
        private static Stack<MyNHSessionBase> threadSessionStack;


        private Stack<MyNHSessionBase> httpContextSessionStack
        {
            get
            {
                return (Stack<MyNHSessionBase>)System.Web.HttpContext.Current.Items["___httpContextSessionStack_2"];
            }
            set
            {
                System.Web.HttpContext.Current.Items["___httpContextSessionStack_2"] = value;
            }
        }


        private Stack<MyNHSessionBase> getStackForCurrentContext(HttpContext context)
        {
            Stack<MyNHSessionBase> stack = null;
            if (context == null)
                context = System.Web.HttpContext.Current;
            if (context != null)
            {
                stack = httpContextSessionStack;
                if (stack == null)
                {
                    stack = new Stack<MyNHSessionBase>();
                    httpContextSessionStack = stack;
                }
            }
            else
            {
                stack = threadSessionStack;
                if (stack == null)
                {
                    stack = new Stack<MyNHSessionBase>();
                    threadSessionStack = stack;
                }
            }
            return stack;
        }


        public MyNHSessionBase GetCurrentSession(HttpContext context = null)
        {
            var stack = getStackForCurrentContext(context);
            if (stack.Count > 0)
                return stack.Peek();
            else
                throw new InvalidOperationException("Session does not exist at the current context");

        }
        public MyNHSessionBase AttachSessionToCurrentContext(MyNHSessionBase session, HttpContext context = null)
        {
            var stack = getStackForCurrentContext(context);
            var newSession = session;
            stack.Push(newSession);
            return newSession;
        }
        public MyNHSessionNormal CreateNewSessionForContext(HttpContext context = null)
        {


            var newSession = NHManager.Instance.CreateNewSession();
            AttachSessionToCurrentContext(newSession, context);
            return newSession;
        }
        public MyNHSessionStateless CreateNewStatelessSessionForContext(HttpContext context = null)
        {


            var newSession = NHManager.Instance.CreateNewStatelessSession();
            AttachSessionToCurrentContext(newSession, context);
            return newSession;
        }
        public MyNHSessionBase DisposeCurrentSessionInContext(HttpContext context = null, bool throwErrorIfSessionDoesNotExistOrDisposed = true)
        {
            var stack = getStackForCurrentContext(context);
            
            
            if (stack.Count == 0 && throwErrorIfSessionDoesNotExistOrDisposed)
                throw new InvalidOperationException("No Session exists to be disposed!");
            MyNHSessionBase session = null;
            if (stack.Count > 0)
            {

                session = stack.Pop();
                if (!session.IsOpen && throwErrorIfSessionDoesNotExistOrDisposed)
                    throw new InvalidOperationException("Session already closed / disposed!");

                NHManager.Instance.CloseSession(session);
            }
            /*session.Flush();
            session.Close();
            session.Dispose();*/
            return session;
        }



    }
}
