using System.Web;
using CS.General_v3.Classes.NHibernateClasses.Session;

namespace CS.General_v3.Classes.NHibernateClasses.SessionContextManagers
{
    public interface ISessionContextManager
    {
        MyNHSessionBase GetCurrentSession(bool throwErrorIfNoSessionexists = true);
        //MyNHSessionBase AttachSessionToCurrentContext(MyNHSessionBase session);
        void CreateNewSessionForContext();
        MyNHSessionNormal CreateNewSessionForContextAndReturn();

        MyNHSessionStateless CreateNewStatelessSessionForContext();
        MyNHSessionBase DisposeCurrentSessionInContext(bool throwErrorIfSessionDoesNotExistOrDisposed = true);
        
    }
}