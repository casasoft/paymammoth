﻿using System;
using System.Web;
using CS.General_v3.Classes.NHibernateClasses.NHManager;

namespace CS.General_v3.Classes.NHibernateClasses.SessionContextManagers
{
    public class WebContextModule : IHttpModule
    {
        #region IHttpModule Members
        public void Init(HttpApplication context)
        {

            context.BeginRequest += context_BeginRequest;

            context.EndRequest += context_EndRequest;

        }
        public void Dispose()
        {

        }


        private void context_BeginRequest(object sender, EventArgs e)
        {

            var application = (HttpApplication)sender;
            
            var context = application.Context;

            

           // BindSession(context);

        }


        private bool verifyExtensionOfCurrentUrlRequiresSession(HttpContext context)
        {
            string ext = "";
            string filename = "";
            if (context != null && context.Request.Url != null)
            {
                string s = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
                ext = CS.General_v3.Util.IO.GetExtension(s).ToLower();
                filename = CS.General_v3.Util.IO.GetFilenameAndExtension(s).ToLower();
            }
            bool ok = true;
            switch (ext)
            {
                case "jpg":
                case "jpeg":
                case "css":
                case "bmp":
                case "tif":
                case "js":
                case "html":
                case "htm":
                case "gif":
                case "png":
                    ok = false;
                    break;

            }
            if (ok)
            {
                switch (filename)
                {
                    case "renewsession.aspx":
                    case "combiner.ashx":
                        ok = false;
                        break;
                }
            }
            return ok;


        }

        

        private void context_EndRequest(object sender, EventArgs e)
        {

            var application = (HttpApplication)sender;

            var context = application.Context;



           // UnbindSession(context);

        }




        private void BindSession(HttpContext context)
        {

            //var sessionBuilder = SessionBuilderFactory.CurrentBuilder;

          //  NHClasses.NhManager.CreateNewSessionInCurrentContext();

          //  var session = NHClasses.NhManager.CreateNewSession();


            if (verifyExtensionOfCurrentUrlRequiresSession(context))
            {

                // Tell NH session context to use it
                // NHClasses.NhManager.BindSessionToCurrentContext(session);
                NHClasses.NhManager.CreateNewSessionForContext();
                //SessionContextManager.Instance.CreateNewSessionForContext2(context);


                //WebSessionContext.Bind(session);
            }

        }

        private void UnbindSession(HttpContext context)
        {
            NHClasses.NhManager.DisposeCurrentSessionInContext();
            //SessionContextManager.Instance.DisposeCurrentSessionInContext2(context);
            

            //NHClasses.NhManager.UnBindSessionFromCurrentContext(true);
            

        }
        #endregion
    }
}
