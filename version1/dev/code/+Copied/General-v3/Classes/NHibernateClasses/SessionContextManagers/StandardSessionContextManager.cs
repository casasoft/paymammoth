﻿using System;
using System.Collections.Generic;
using System.Web;
using CS.General_v3.Classes.NHibernateClasses.NHManager;
using CS.General_v3.Classes.NHibernateClasses.Session;

namespace CS.General_v3.Classes.NHibernateClasses.SessionContextManagers
{
  
    public class StandardSessionContextManager : ISessionContextManager
    {
        

        private static readonly StandardSessionContextManager _instance = new StandardSessionContextManager();
        public static StandardSessionContextManager Instance
        {
            get
            {
                return _instance;
            }
        }
        private StandardSessionContextManager()
        {

        }
        private static readonly object _padlock = new object();
        [ThreadStatic]
        private static Stack<MyNHSessionBase> __threadSessionStack;
        private static Stack<MyNHSessionBase> threadSessionStack
        {
            get 
            {
                if (__threadSessionStack == null)
                {
                    lock (_padlock)
                    {
                        if (__threadSessionStack == null)
                        {
                            __threadSessionStack = new Stack<MyNHSessionBase>();
                        }
                    }
                }
                return __threadSessionStack;
            }
        }



        private Stack<MyNHSessionBase> ___httpContextSessionStack_2
        {
            get
            {
                return (Stack<MyNHSessionBase>)System.Web.HttpContext.Current.Items["___httpContextSessionStack_2"];
            }
            set
            {
                System.Web.HttpContext.Current.Items["___httpContextSessionStack_2"] = value;
            }
        }
        private bool createSessionInContextOnFirstAccess 
        {
            get
            {
                return ((bool?)System.Web.HttpContext.Current.Items["createSessionInContextOnFirstAccess"]).GetValueOrDefault();
            }
            set
            {
                System.Web.HttpContext.Current.Items["createSessionInContextOnFirstAccess"] = value;
            }
        }
        private bool firstContextAccessCalled
        {
            get
            {
                return ((bool?)System.Web.HttpContext.Current.Items["firstContextAccessCalled"]).GetValueOrDefault();
            }
            set
            {
                System.Web.HttpContext.Current.Items["firstContextAccessCalled"] = value;
            }
        }
        private Stack<MyNHSessionBase> httpContextSessionStack
        {
            get
            {
                if (___httpContextSessionStack_2 == null)
                {
                    lock (_padlock)
                    {
                        if (___httpContextSessionStack_2 == null)
                        {
                            ___httpContextSessionStack_2 = new Stack<MyNHSessionBase>();
                        }
                    }
                }

                return ___httpContextSessionStack_2;
            }
           
        }


        private Stack<MyNHSessionBase> getStackForCurrentContext(HttpContext context)
        {
            Stack<MyNHSessionBase> stack = null;
            if (context == null)
                context = System.Web.HttpContext.Current;
            if (context != null)
            {
                stack = httpContextSessionStack;
                
            }
            else
            {
                stack = threadSessionStack;
                

            }
            return stack;
        }


        public MyNHSessionBase GetCurrentSession(HttpContext context = null, bool throwErrorIfNoSessionexists = true)
        {
            if (CS.General_v3.Util.PageUtil.CheckRequestExistsAtContext() && createSessionInContextOnFirstAccess)
            {
                lock (System.Web.HttpContext.Current)
                {
                    if (createSessionInContextOnFirstAccess)
                    {
                        _createNewSessionForContext(context);
                        createSessionInContextOnFirstAccess = false;
                    }
                }
            }
            var stack = getStackForCurrentContext(context);
            if (stack.Count > 0)
                return stack.Peek();
            else
            {
                if (throwErrorIfNoSessionexists)
                    throw new InvalidOperationException("Session does not exist at the current context");
            }
            return null;

        }
        public MyNHSessionBase AttachSessionToCurrentContext(MyNHSessionBase session, HttpContext context = null)
        {
            var stack = getStackForCurrentContext(context);
            var newSession = session;
            stack.Push(newSession);
            return newSession;
        }
        public MyNHSessionNormal CreateNewSessionForContextAndReturn(HttpContext context = null)
        {
            CreateNewSessionForContext(context);
            return (MyNHSessionNormal)GetCurrentSession(context);
        }

        private MyNHSessionNormal _createNewSessionForContext(HttpContext context = null)
        {
            var newSession = NHClasses.NhManager.CreateNewSession();
            AttachSessionToCurrentContext(newSession, context);
            return newSession;

        }

        public void CreateNewSessionForContext(HttpContext context = null)
        {
            if (CS.General_v3.Util.PageUtil.CheckRequestExistsAtContext() && !firstContextAccessCalled)
            {
                firstContextAccessCalled = true;
                createSessionInContextOnFirstAccess = true;
            }
            else
            {
                _createNewSessionForContext(context);
                
            }
        }
        public MyNHSessionStateless CreateNewStatelessSessionForContext(HttpContext context = null)
        {


            var newSession = NHClasses.NhManager.CreateNewStatelessSession();
            AttachSessionToCurrentContext(newSession, context);
            return newSession;
        }
        public MyNHSessionBase DisposeCurrentSessionInContext(HttpContext context = null, bool throwErrorIfSessionDoesNotExistOrDisposed = true)
        {


            var stack = getStackForCurrentContext(context);


            if (stack.Count == 0 && throwErrorIfSessionDoesNotExistOrDisposed)
            {
                if (CS.General_v3.Util.PageUtil.CheckRequestExistsAtContext() && createSessionInContextOnFirstAccess)
                {
                    //first session was not really created yet, so this is not an error
                    int k = 5;
                }
                else
                {
                    throw new InvalidOperationException("No Session exists to be disposed!");
                }
            }
            MyNHSessionBase session = null;
            if (stack.Count > 0)
            {

                session = stack.Pop();
                if (!session.IsOpen && throwErrorIfSessionDoesNotExistOrDisposed)
                    throw new InvalidOperationException("Session already closed / disposed!");

                NHClasses.NhManager.CloseSession(session);
            }
            /*session.Flush();
            session.Close();
            session.Dispose();*/
            return session;
        }



    }
}
