﻿using System;
using System.Collections.Generic;
using System.Web;
using CS.General_v3.Classes.NHibernateClasses.NHManager;
using CS.General_v3.Classes.NHibernateClasses.Session;

namespace CS.General_v3.Classes.NHibernateClasses.SessionContextManagers
{
   /// <summary>
   /// This session context manager should be used mainly for SQLite in-memory configuration
   /// </summary>
    public class TestSessionContextManager : ISessionContextManager
    {
        private static readonly TestSessionContextManager _instance = new TestSessionContextManager();
        public static TestSessionContextManager Instance
        {
            get
            {
                return _instance;
            }
        }
        private TestSessionContextManager()
        {

        }

        private static readonly object _sessionPadlock = new object();
        private static MyNHSessionBase _sessionValue;
        private static MyNHSessionBase session
        {
            get
            {
                lock (_sessionPadlock)
                {
                    return _sessionValue;
                }
            }
            set
            {
                lock (_sessionPadlock)
                {
                    _sessionValue = value;
                }
            }
        }



        public MyNHSessionBase GetCurrentSession(bool throwErrorIfNoSessionexists = true)
        {
            return session;

        }
        public MyNHSessionBase AttachSessionToCurrentContext(MyNHSessionBase sessionBase)
        {
            session = sessionBase;
            return session;
        }

        public MyNHSessionNormal CreateNewSessionForContextAndReturn()
        {
            CreateNewSessionForContext();
            return (MyNHSessionNormal)GetCurrentSession();

        }
        public void CreateNewSessionForContext()
        {

            if (session == null)
            {
                lock (_sessionPadlock)
                {
                    if (session == null)
                    {
                        session = NHClasses.NhManager.CreateNewSession();
                        NHClasses.NhManager.UpdateSchema();
                    }
                }
            }
            //return (MyNHSessionNormal)session;
        }
        public MyNHSessionStateless CreateNewStatelessSessionForContext()
        {


            var newSession = NHClasses.NhManager.CreateNewStatelessSession();
            AttachSessionToCurrentContext(newSession);
            return newSession;
        }
        public MyNHSessionBase DisposeCurrentSessionInContext(bool throwErrorIfSessionDoesNotExistOrDisposed = true)
        {
            session.Flush();
            // session.Clear();
            return session;
        }
        public void DisposeSessionOnEndOfTest()
        {
            if (session != null)
            {
                lock (_sessionPadlock)
                {
                    if (session != null)
                    {
                        session.Flush();
                        session.Close();
                        session.Dispose();
                        session = null;
                    }
                }
            }

        }




        #region ISessionContextManager Members



        #endregion
    }
}
