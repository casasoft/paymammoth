﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using FluentNHibernate.Cfg;
using NHibernate.Cfg;
using System.Reflection;
using NHibernate.Tool.hbm2ddl;
using CS.General_v3.Classes.NHibernateClasses.FluentNH.Conventions;
using FluentNHibernate.Conventions.Helpers;
using NHibernate.Exceptions;
using NHibernate.Impl;
using log4net;

namespace CS.General_v3.Classes.NHibernateClasses
{
    public class NHManager
    {
        private ILog _log = log4net.LogManager.GetLogger(typeof(NHManager));
        private static readonly NHManager _Instance = new NHManager();
        public static NHManager Instance { get { return _Instance; } }

        private NHibernate.Impl.SessionFactoryImpl _sessionFactory = null;

        public void Initialise()
        {
            lock (_Instance)
            {
                createSessionFactory();
                _initialised = true;
            }

        }
        /*
        public SessionImpl GetCurrentSessionFromContext()
        {
            var f = _sessionFactory;
            SessionImpl session = null;
            try
            {
                //session =  f.GetCurrentSession();
                session = ContextAndThreadSessionManager.GetCurrentSession();
            }
            catch (Exception ex)
            {
                int k = 5;
                throw;
            }
            return session;
            
        }
        */


        private Iesi.Collections.Generic.HashedSet<Assembly> _mappedAssemblies = new Iesi.Collections.Generic.HashedSet<Assembly>();
        private Iesi.Collections.Generic.HashedSet<Type> _mappedTypes = new Iesi.Collections.Generic.HashedSet<Type>();
        public void AddMappingFor(Assembly a)
        {
            checkMappingForInitialised();
            _mappedAssemblies.Add(a);
        }
        public void AddMappingFor(Type t)
        {
            checkMappingForInitialised();
            _mappedTypes.Add(t);
        }
        private void checkMappingForInitialised()
        {
            if (_initialised) throw new InvalidOperationException("Cannot add a mapping for a type after NH has already been initialised");
        }
        public void AddMappingFor<T>()
        {
            
            AddMappingFor(typeof(T));
            
        }
        public void AddMappingForAssemblyOf(Type t)
        {
            AddMappingFor(t.Assembly);
        }
        public void AddMappingForAssemblyOf<T>()
        {
            AddMappingFor(typeof(T).Assembly);
        }


       
   
        private bool _initialised = false;
        public ISessionFactory GetSessionFactory()
        {
            return _sessionFactory;
        }
        private void createSessionFactory()
        {
            NHConfig nhConfig = NHConfig.CreateLastInstance();
            nhConfig.SetMappings(_mappedTypes, _mappedAssemblies);
            var cfg = nhConfig.GetConfiguration();
            
            
            try
        {
            _log.Debug("Creating session factory - Started" );
          //  _sessionFactory = CS.General_v3.Util.ReflectionUtil.DeserializeObjectFromFile<NHibernate.Impl.SessionFactoryImpl>(@"D:\My Documents\test_factory.bin");

                _sessionFactory = (NHibernate.Impl.SessionFactoryImpl) cfg.BuildSessionFactory();


              //  CS.General_v3.Util.ReflectionUtil.SerializeObjectToFile(_sessionFactory, @"D:\My Documents\test_factory.bin");
                _log.Debug("Creating session factory - Finished");

            }
            catch (FluentConfigurationException ex)
            {
                int k = 5;
                throw;
            }

            
            nhConfig.checkSchema(cfg); //this will check the setting, not always!
            nhConfig.SaveConfigurationToFile(cfg);

        }

        public MyNHSessionNormal CreateNewSession()
        {
            if (_sessionFactory == null)
                throw new InvalidOperationException("No session factory exists. Make sure you have the database settings specified in the App_Data/settings.user.xml file");

            var session = (NHibernate.Impl.SessionImpl)_sessionFactory.OpenSession();
            return new MyNHSessionNormal(session);
            
        }
        public MyNHSessionStateless CreateNewStatelessSession()
        {
            if (_sessionFactory == null)
                throw new InvalidOperationException("No session factory exists. Make sure you have the database settings specified in the App_Data/settings.user.xml file");

            var session = (NHibernate.Impl.StatelessSessionImpl)_sessionFactory.OpenStatelessSession();
            return new MyNHSessionStateless(session);
            

        }


        public MyNHSessionBase GetCurrentSessionFromContext()
        {

            return SessionContextManager.Instance.GetCurrentSession();

        }
        public MyNHSessionNormal CreateNewSessionForContext()
        {
            return SessionContextManager.Instance.CreateNewSessionForContext();
            
        }
        public MyNHSessionBase DisposeCurrentSessionInContext(bool throwErrorIfSessionDoesNotExistOrDisposed = true)
        {
            return SessionContextManager.Instance.DisposeCurrentSessionInContext(throwErrorIfSessionDoesNotExistOrDisposed: throwErrorIfSessionDoesNotExistOrDisposed);
        }
        public MyNHSessionStateless CreateNewStatelessSessionForContext()
        {
            return SessionContextManager.Instance.CreateNewStatelessSessionForContext();

        }
        public void CloseSession(MyNHSessionBase session, bool commitOrRollbackExistingTransactions = true)
        {

            if (session != null)
            {
                //if (commitOrRollbackExistingTransactions && session.Transaction != null)
                //{
                //    if (session.Transaction.IsActive)
                //    {
                //        //if there is an active session, commit it
                //        session.Transaction.Commit();
                //    }
                //    else
                //    {
                //        //
                //        session.Transaction.Rollback();
                //    }
                //}
                try
                {
                    session.Flush();
                    session.Close();
                    session.Dispose();
                }
                catch (GenericADOException ex)
                {
                    int k = 5;
                    throw;
                }
            }
        }

        private NHManager()
        {

        }




        public void FlushCurrentSessionInContext()
        {
            try
            {
                var session = GetCurrentSessionFromContext();
                session.Flush();
            }
            catch (GenericADOException ex)
            {
                int k = 5;
                throw;
            }
            
        }
    }
}
