﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NHibernate.Cfg;


namespace CS.General_v3.Classes.NHibernateClasses.DBConfig
{
    public interface INHibernateDbConfig
    {
        void ConfigureDatabase(Configuration cfg);
        //IPersistenceConfigurer GetDbConfiguration();
        //void InitConfigProperties(Configuration config);
    }
}
