﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.ConnectionProviders;

using NHibernate.Cfg;
using NHibernate.Driver;
using NHibernate.Dialect;

namespace CS.General_v3.Classes.NHibernateClasses.DBConfig
{
    public class SQLiteDbConfig : INHibernateDbConfig
    {
        
        #region IFluentDbConfig Members


        public void ConfigureDatabase(Configuration cfg)
        {

            if (cfg.Properties.ContainsKey(NHibernate.Cfg.Environment.ConnectionStringName))
                cfg.Properties.Remove(NHibernate.Cfg.Environment.ConnectionStringName);

            cfg= cfg.DataBaseIntegration(x =>
            {
                x.Dialect<SQLiteDialect>();
                x.Driver<SQLite20Driver>();
              //  x.SchemaAction = SchemaAutoAction.Create;
                x.ConnectionString = "Data Source=" + CS.General_v3.Settings.Database.DatabasePath + ";Version=3;New=True;";
             
            });
            
              
                    


            
        }
      



        public void InitConfigProperties(Configuration config)
        {
            
            //config.Properties[NHibernate.Cfg.Environment.ReleaseConnections] = "on_close";
        }

        #endregion
    }
}
