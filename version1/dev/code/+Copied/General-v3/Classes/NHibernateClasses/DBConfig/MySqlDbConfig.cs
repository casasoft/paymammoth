﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.ConnectionProviders;
using MySql.Data.MySqlClient;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Driver;

namespace CS.General_v3.Classes.NHibernateClasses.DBConfig
{
    public class MySqlDbConfig: INHibernateDbConfig
    {
        
        #region IFluentDbConfig Members


        public MySqlDbConfig()
        {
            this.BatchSize = 35;

        }
        
            public int BatchSize { get; set; }

        public void ConfigureDatabase(Configuration cfg)
        {
            
            MySql.Data.MySqlClient.MySqlConnectionStringBuilder connStr = new MySqlConnectionStringBuilder();
            connStr.Server = CS.General_v3.Settings.Database.Host;
            connStr.UserID = CS.General_v3.Settings.Database.User;
            connStr.Password = CS.General_v3.Settings.Database.Pass;
            connStr.Database = CS.General_v3.Settings.Database.DatabaseName;
            connStr.Port = (uint)CS.General_v3.Settings.Database.Port;

            cfg= cfg.DataBaseIntegration(x =>
            {
                x.Dialect<MySQLDialect>();
                x.Driver<MySqlDataDriver>();
                x.ConnectionProvider<StandardConnectionProvider>();
                //  x.SchemaAction = SchemaAutoAction.Create;
                x.ConnectionString = (connStr).ToString();
                x.BatchSize = (short)this.BatchSize;
            });



            //var persistenceConfig =
            //        FluentNHibernate.Cfg.Db.MySQLConfiguration.Standard

            //        .ConnectionString(connectionString)
            //        .Provider<StandardConnectionProvider>()
            //    //.Cache(c => c.UseQueryCache()
            //    // .ProviderClass<NHibernate.Cache.HashtableCacheProvider>())
            //    //.MaxFetchDepth(3)

            //        .AdoNetBatchSize(35);

            ////  persistenceConfig.DoNot.UseReflectionOptimizer();
            ////persistenceConfig.UseReflectionOptimizer();
            //return persistenceConfig;
        }


        


        #endregion
    }
}
