﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Timers;

namespace CS.General_20090518.Classes.Timers
{
    public class SpecificDateTimer2 : IDisposable
    {
        private static int lastTimerID = 0;

        public delegate void CallDelegate(SpecificDateTimer2 timer);
        public DateTime StartDate { get; private set; }
        public DateTime CallDateTime { get; private set; }
        private Timer __tmr = null;
        public int TimerID { get; private set; }

        public Timer _tmr
        {
            get { return __tmr; }
            set 
            {
                __tmr = value;

                if (__tmr == null)
                {
                    int k = 5;
                }
            }
        }
        
        public event CallDelegate MethodToCall;
        public long getMilleSecondsLeft()
        {
            DateTime d = Util.Date.Now;
            TimeSpan span = CallDateTime.Subtract(d);
            return (long)Math.Ceiling(span.TotalSeconds) * (long)1000;
        }
        private int getCurrentTimerInterval()
        {
            int highResolutionCheckPeriod = 5000; // time to check with high-resolution (every 250ms)
            int maxInterval = Int32.MaxValue - 1;
            long millesecLeft = getMilleSecondsLeft();
            int tmrInterval = 0;
            if (millesecLeft > (long)maxInterval)
            { //if millesecs left is after max interval, set interval as the maximum (25days)
                tmrInterval = maxInterval;
            }
            else
            {
                if (millesecLeft > highResolutionCheckPeriod)
                {
                    //set interval to near the high resolution check period.  After that, start firing at 250ms
                    tmrInterval = ((int)millesecLeft - highResolutionCheckPeriod);
                }
                else
                {
                    //set interval to 1/4 second
                    tmrInterval = 250;
                }
            }
            return tmrInterval;
        }
        private int methodCalledNoOfTimes = 0;
        /// <summary>
        /// Checks the current timer, and returns whether it is still valid or not
        /// </summary>
        /// <returns></returns>
        private bool checkTimer()
        {
            bool ok = true;
            int currNewInterval = getCurrentTimerInterval();
            if (_tmr == null || (_tmr != null && currNewInterval != (int)_tmr.Interval))
            {
                if (_tmr != null)
                {
                    _tmr.Stop();
                    _tmr.Dispose();
                    _tmr = null;
                }
                ok = false;
                _tmr = new Timer();
                _tmr.Interval = currNewInterval;
                _tmr.Elapsed += new ElapsedEventHandler(_tmr_Elapsed);
                _tmr.Start();
            }
            return ok;
        }
        private bool checkDateTime()
        {

            DateTime d = CS.General_20090518.Util.Date.Now;
            bool ok = false;
            if (d >= this.CallDateTime)
            {
                methodCalledNoOfTimes++;
                if (methodCalledNoOfTimes > 1)
                {
                    int k = 5;
                }
                MethodToCall(this);
                ok = true;

            }
            return ok;


        }
        private bool inElapseMethod = false;
        void _tmr_Elapsed(object sender, ElapsedEventArgs e)
        {
            _tmr.Stop();
            _tmr.Dispose();
            _tmr = null;
            
            
            
            if (inElapseMethod)
                throw new InvalidOperationException("SHould not happen in SpecificDateTimer!");
            inElapseMethod = true;
            if (checkDateTime())
            { //method called, ready 
                Stop();
            }
            else
            {
                checkTimer();
            }
            inElapseMethod = false;
        }

        public SpecificDateTimer2(DateTime callDate)
        {

            this.TimerID = (lastTimerID + 1);
            lastTimerID++;
            this.StartDate = CS.General_20090518.Util.Date.Now;
            this.CallDateTime = callDate;


        }
        public void Start()
        {
            methodCalledNoOfTimes = 0;
            if (this.MethodToCall == null)
                throw new InvalidOperationException("MethodToCall event is not attached to");
            checkTimer();
        }
        public void Stop()
        {
            if (_tmr != null)
            {

                _tmr.Stop();
                _tmr.Dispose();
                _tmr = null;
            }
        }

        public void Dispose()
        {
            if (_tmr != null)
                _tmr.Dispose();

            
        }
    }
}
