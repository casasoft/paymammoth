﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Timers;

namespace CS.General_v3.Classes.Timers
{
    public class SpecificDateTimer : IDisposable
    {
        private static volatile int lastTimerID = 0;

        public delegate void CallDelegate(SpecificDateTimer timer);
        public DateTime StartDate { get; private set; }
        public DateTime CallDateTime { get; private set; }
        private Timer __tmr = null;
        public int TimerID { get; private set; }

        public Timer _tmr
        {
            get { return __tmr; }
            private set 
            {
                __tmr = value;

                
            }
        }
        
        public event CallDelegate MethodToCall;
        /*public long getMilleSecondsLeft()
        {
            DateTime d = Util.Date.Now;
            TimeSpan span = CallDateTime.Subtract(d);
            return (long)Math.Ceiling(span.TotalSeconds) * (long)1000;
        }*/
        /*
        private int getCurrentTimerInterval()
        {
            int highResolutionCheckPeriod = 5000; // time to check with high-resolution (every 250ms)
            int maxInterval = Int32.MaxValue - 1;
            long millesecLeft = getMilleSecondsLeft();
            int tmrInterval = 0;
            if (millesecLeft > (long)maxInterval)
            { //if millesecs left is after max interval, set interval as the maximum (25days)
                tmrInterval = maxInterval;
            }
            else
            {
                if (millesecLeft > highResolutionCheckPeriod)
                {
                    //set interval to near the high resolution check period.  After that, start firing at 250ms
                    tmrInterval = ((int)millesecLeft - highResolutionCheckPeriod);
                }
                else
                {
                    //set interval to 1/4 second
                    tmrInterval = 250;
                }
            }
            return tmrInterval;
        }*/
        private int methodCalledNoOfTimes = 0;
        /// <summary>
        /// Checks the current timer, and returns whether it is still valid or not
        /// </summary>
        /// <returns></returns>
        private void startTimer()
        {
            TimeSpan spanRemaining = CallDateTime.Subtract(CS.General_v3.Util.Date.Now);
            if (spanRemaining.TotalMilliseconds > 0)
            {
                if (_tmr != null)
                {
                    _tmr.Elapsed -= _tmr_Elapsed;
                    _tmr.Stop();
                    _tmr.Dispose();
                    _tmr = null;
                    
                }
                _tmr = new Timer();
                _tmr.Interval = spanRemaining.TotalMilliseconds;
                _tmr.Elapsed += _tmr_Elapsed;
                _tmr.Start();
            }
            else
            {
                _tmr_Elapsed(null,null);
            }

        }

        

        private bool inElapseMethod = false;
        private readonly object timerPadlock = new object();
        void _tmr_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (_tmr != null)
            {
                lock (timerPadlock)
                {
                    if (_tmr != null)
                    {
                        _tmr.Stop();
                        _tmr.Dispose();
                        _tmr = null;
                    }
                }
            }
            if (inElapseMethod)
            {
                //throw new InvalidOperationException("SHould not happen in SpecificDateTimer!");
            }
            lock (timerPadlock)
            {
                



                inElapseMethod = true;
                methodCalledNoOfTimes++;
                if (methodCalledNoOfTimes <= 1)
                {
                    MethodToCall(this);
                }
                else
                {
                    //throw new InvalidOperationException("SHould not happen in SpecificDateTimer!");
                }
                

                inElapseMethod = false;
            }
        }

        public SpecificDateTimer(DateTime callDate)
        {

            this.TimerID = (lastTimerID + 1);
            lastTimerID++;
            this.StartDate = CS.General_v3.Util.Date.Now;
            this.CallDateTime = callDate;


        }
        public void Start()
        {
            methodCalledNoOfTimes = 0;
            if (this.MethodToCall == null)
                throw new InvalidOperationException("MethodToCall event is not attached to");
            startTimer();
        }
        public void Stop()
        {
            if (_tmr != null)
            {

                _tmr.Stop();
                _tmr.Dispose();
                _tmr = null;
            }
        }

        public void Dispose()
        {
            if (_tmr != null)
                _tmr.Dispose();

            
        }
    }
}
