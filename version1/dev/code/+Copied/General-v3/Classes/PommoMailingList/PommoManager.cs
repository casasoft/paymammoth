﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;

namespace CS.General_v3.Classes.PommoMailingList
{
    public class PommoManager
    {
        public static string PROCESS_URL = "user/process.php";
        /// <summary>
        /// 
        /// </summary>
        /// <param name="subscriptionProcessURL">The mailing system base url</param>
        public PommoManager(string serverBaseURL)
        {
            this.ServerBaseURL = serverBaseURL;
        }
        public List<PommoField> Fields { get; set; }
        private string _ServerBaseURL = null;

        public string ServerBaseURL
        {
            get { return _ServerBaseURL; }
            set
            {
                _ServerBaseURL = value;
                if (_ServerBaseURL != null)
                {
                    if (!_ServerBaseURL.EndsWith("/")) _ServerBaseURL += "/";
                    if (!_ServerBaseURL.ToLower().StartsWith("http://")) _ServerBaseURL = "http://" + _ServerBaseURL;
                }

            }
        }

        private bool checkNV(NameValueCollection nv)
        {
            if (this.Fields != null)
            {
                for (int i = 0; i < this.Fields.Count; i++)
                {
                    var f = this.Fields[i];
                    if (f.Required && string.IsNullOrEmpty(nv[f.Identifier]))
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        private bool placeSubscription(NameValueCollection values)
        {
            string url = ServerBaseURL + PROCESS_URL;
            string s = CS.General_v3.Util.Web.HTTPPost(url, values);
            System.Web.HttpContext.Current.Response.Write(s);
            bool result = s.ToLower().Contains("Welcome to our mailing list".ToLower());
            return result;

        }
        private void fillDefaultValues(NameValueCollection nv)
        {
            nv["formSubmitted"] = "1";
            nv["pommo_signup"] = "true";
        }
        public bool Subscribe(NameValueCollection values, out string errorMsg)
        {
            fillDefaultValues(values);
            bool result = true;
            errorMsg = null;
            if (!checkNV(values))
            {
                result = false;
                errorMsg = "Not all required values are filled in";
            }
            if (result)
            {
                result = placeSubscription(values);
            }
            return result;
        }
    }
}
