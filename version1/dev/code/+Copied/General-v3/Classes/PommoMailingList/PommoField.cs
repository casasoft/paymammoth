﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.PommoMailingList
{
    public class PommoField
    {
        public string Title { get; set; }
        public string Identifier { get; set; }
        public bool Required { get; set; }
        
        public PommoField(string Title, string identifier, bool required)
        {
            this.Title = Title;
            this.Identifier = identifier;
            this.Required = required;
        }
    }
}
