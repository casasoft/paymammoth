﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace CS.General_v3.Classes
{
    public abstract class WebApplication : System.Web.HttpApplication
    {

        public static WebApplication Instance { get; private set; }

        public WebApplication()
        {
           
        }


        public override string GetVaryByCustomString(System.Web.HttpContext context, string custom)
        {
            if (string.Compare(custom, "url", true) == 0)
            {
                return context.Request.Url.AbsoluteUri;
            }
            else if (string.Compare(custom, "fullurl", true) == 0)
            {
                return context.Request.Url.AbsoluteUri + "?" + context.Request.Url.Query;
            }
            else
                return base.GetVaryByCustomString(context, custom);
        }

        protected bool verifyExtensionOfCurrentUrlRequiresSession(HttpContext context)
        {
            string ext = "";
            string filename = "";
            if (context != null && context.Request.Url != null)
            {
                string s = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
                ext = CS.General_v3.Util.IO.GetExtension(s).ToLower();
                filename = CS.General_v3.Util.IO.GetFilenameAndExtension(s).ToLower();
            }
            bool ok = true;
            switch (ext)
            {
                case "jpg":
                case "jpeg":
                case "css":
                case "bmp":
                case "tif":
                case "js":
                case "swf":
                case "flv":

                case "ico":
                case "html":
                case "htm":
                case "xml":
                case "gif":
                case "png":
                    ok = false;
                    break;

            }
            if (ok &&
                (CS.General_v3.Util.Image.CheckImageFileExtension(filename) || CS.General_v3.Util.VideoUtil.CheckVideoExtension(filename)))
            {
                ok = false;
            }

            if (ok)
            {
                switch (filename)
                {
                    case "renewsession.aspx":
                    case "combiner.ashx":
                        ok = false;
                        break;
                }
            }
            return ok;


        }

        

    }
}
