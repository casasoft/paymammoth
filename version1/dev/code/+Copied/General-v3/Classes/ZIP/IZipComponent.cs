﻿using System.Collections.Generic;
using System.IO;
using CS.General_v3.Classes.ZIP.Components;

namespace CS.General_v3.Classes.ZIP
{
    public interface IZipComponent
    {
        void ExtractZIP(string zipPath, string dirToExtractTo);
        
        void CreateZIPFile(string zipPath, IEnumerable<FileToZip> files,ZipCreateParams parameters);

    }
}
