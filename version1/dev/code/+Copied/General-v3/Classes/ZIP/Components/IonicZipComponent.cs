﻿using System;
using System.Collections.Generic;
using Ionic.Zip;

namespace CS.General_v3.Classes.ZIP.Components
{
    public class IonicZipComponent : IZipComponent
    {



        #region IZipComponent Members

        public void ExtractZIP(string zipPath, string dirToExtractTo)
        {
            using (ZipFile zip = ZipFile.Read(zipPath))
            {
                foreach (ZipEntry e in zip)
                {
                    e.Extract(dirToExtractTo, ExtractExistingFileAction.OverwriteSilently);
                }
            }
            
        }

        public void CreateZIPFile(string zipPath, IEnumerable<FileToZip> files, ZipCreateParams parameters)
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(parameters, "parameters");
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(files, "files");
            using (ZipFile zip = new ZipFile())
            {

                zip.CompressionLevel = (Ionic.Zlib.CompressionLevel)parameters.CompressionLevel;
                if (parameters.UseZip64WhenNecessary)
                    zip.UseZip64WhenSaving = Zip64Option.AsNecessary;
                    

                if (parameters.UseEncryption)
                {
                    if (parameters.MakeWinZipCompatibleFiles)
                        zip.Encryption = EncryptionAlgorithm.WinZipAes256;
                    //default encryption is ON
                }
                else
                {
                    zip.Encryption = EncryptionAlgorithm.None;
                }
                if (parameters.MakeWinZipCompatibleFiles)
                    zip.Encryption = EncryptionAlgorithm.None;
                foreach (var file in files)
                {
                    zip.AddFile(file.FilePath, file.DirectoryPathInArchive);
                }

                zip.Save(zipPath);
            }
            
        }

        #endregion
    }
}
