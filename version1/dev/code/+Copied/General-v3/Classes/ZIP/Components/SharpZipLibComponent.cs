﻿using System;
using System.Collections.Generic;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;

namespace CS.General_v3.Classes.ZIP.Components
{
    public class SharpZipLibComponent : IZipComponent
    {

        #region IZipManager Members

        public void ExtractZIP(string zipPath, string dirToExtractTo)
        {
            FastZip zip = new FastZip();
            //zipPath = @"d:\test_extract.zip";
            // dirToExtractTo = @"d:\testextract\";
            CS.General_v3.Util.IO.CreateDirectory(dirToExtractTo);

            zip.ExtractZip(zipPath, dirToExtractTo, FastZip.Overwrite.Always, null, null, null, false);
        }

        public void CreateZIPFileFromFolder(string zipPath, string folderToZip, bool recurse = true, string fileFilter = null, int compressionLevel = 5)
        {
            CS.General_v3.Util.IO.CreateDirectory(zipPath);
            FileStream fsOut = new FileStream(zipPath, FileMode.Create, FileAccess.Write);

            ZipOutputStream zipOutputStream = new ZipOutputStream(fsOut);


            folderToZip = CS.General_v3.Util.IO.EnsurePathEndsWithSlash(folderToZip);

            
            zipOutputStream.SetLevel(compressionLevel);
            DirectoryInfo dir = new DirectoryInfo(folderToZip);
            var allFiles = dir.GetFiles("*", SearchOption.AllDirectories);
            foreach (FileInfo f in allFiles)
            {
                string entryName = f.FullName.Substring(folderToZip.Length);

                ZipEntry entry = new ZipEntry(entryName);
                zipOutputStream.PutNextEntry(entry);
                byte[] byFiles = CS.General_v3.Util.IO.LoadFileAsByteArray(f.FullName);
                
                zipOutputStream.Write(byFiles, 0, byFiles.Length);
                //entry.CompressionMethod = compressionMethod;

            }
            zipOutputStream.Finish();
            zipOutputStream.Close();

            //CS.General_v3.Util.IO.GetFilesInDirByRegExp(folderToZip,"*", System.Text.RegularExpressions.RegexOptions.

            //ZipEntry z;


            //FastZip zip = new FastZip();
            //ICSharpCode.SharpZipLib.Zip.ZipFile.Create(
            //zip.CreateZip(zipPath, folderToZip, recurse: recurse, fileFilter: fileFilter);
        }

        public void CreateZIPFile(string zipPath, string content, string contentFileName)
        {
            if (string.IsNullOrEmpty(contentFileName))
                throw new InvalidOperationException("Content Filename must be specified");

            string tmpFolderPath = System.IO.Path.GetTempPath() + System.IO.Path.GetRandomFileName() + CS.General_v3.Util.Random.GetString(20, 20) + "\\";
            CS.General_v3.Util.IO.CreateDirectory(tmpFolderPath);



            CS.General_v3.Util.IO.SaveToFile(tmpFolderPath + contentFileName, content);
            CreateZIPFileFromFolder(zipPath, tmpFolderPath, recurse: false);
            //zip.CreateZip(zipPath, tmpFolderPath, false, null);
            CS.General_v3.Util.IO.EmptyDirectory(tmpFolderPath);
            System.IO.Directory.Delete(tmpFolderPath);
        }
        public void CreateZIPFile(string zipPath, IEnumerable<FileInfo> files)
        {
            List<FileToZip> list = new List<FileToZip>();
            foreach (var f in files)
            {
                FileToZip file = new FileToZip();
                file.SetFilePath(f.FullName);
                list.Add(file);
            }
            CreateZIPFile(zipPath, list);
        }
        public void CreateZIPFile(string zipPath, IEnumerable<FileToZip> files, int compressionLevel = 5)
        {
            string tempPath = CS.General_v3.Util.IO.CreateTempDir();
            foreach (var file in files)
            {
                string newPath = tempPath + CS.General_v3.Util.IO.GetFilenameAndExtension(file.FilePath);

                CS.General_v3.Util.IO.CopyFile(file.FilePath, newPath);

            }

            CreateZIPFileFromFolder(zipPath, tempPath, compressionLevel: compressionLevel);

            CS.General_v3.Util.IO.DeleteDirectory(tempPath);
        }

        #endregion

        #region IZipComponent Members


        public void CreateZIPFile(string zipPath, IEnumerable<FileToZip> files,ZipCreateParams parameters)
        {


            this.CreateZIPFile(zipPath, files, parameters.CompressionLevel);
            
        }

        #endregion
    }
}
