﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.ZIP
{
    public static class ZipUtil
    {
        public static IZipComponent GetComponentFromEnum(EnumsZip.ZIP_COMPONENT_TYPE componentType)
        {
            switch (componentType)
            {
                case EnumsZip.ZIP_COMPONENT_TYPE.IonicZip: return new ZIP.Components.IonicZipComponent();
                case EnumsZip.ZIP_COMPONENT_TYPE.SharpZipLib: return new ZIP.Components.SharpZipLibComponent();
                
            }
            throw new InvalidOperationException("Invalid component type");
        }
    }
}
