﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Iesi.Collections.Generic;
using System.IO;
using CS.General_v3.Classes.ZIP.Components;

namespace CS.General_v3.Classes.ZIP
{
    public class ZipCreator : IDisposable
    {
        private IZipComponent _zip;
        /// <summary>
        /// Creates a zip creator with the default (SharpZipLib) c
        /// </summary>
        public ZipCreator() : this( EnumsZip.ZIP_COMPONENT_TYPE.SharpZipLib)
        {

        }

        public ZipCreator(EnumsZip.ZIP_COMPONENT_TYPE componentType)
        {
            
            _zip = ZipUtil.GetComponentFromEnum(componentType);
            _files = new HashedSet<FileToZip>();
        }
        private HashedSet<FileToZip> _files = null;

        

        public void AddFile(FileToZip file)
        {
            _files.Add(file);
        }

        public void AddFile(string path, string relativeTo = null, string directoryToPlaceIn = null)
        {
            FileToZip zipFile = new FileToZip();
            zipFile.SetFilePath(path, relativeTo);
            if (directoryToPlaceIn != null) zipFile.DirectoryPathInArchive = directoryToPlaceIn;
            _files.Add(zipFile);
        }
        public void AddFile(FileInfo file, string relativeTo = null, string directoryToPlaceIn = null)
        {
            AddFile(file.FullName, relativeTo: relativeTo, directoryToPlaceIn: directoryToPlaceIn);
        }
        public void AddFiles(IEnumerable<FileInfo> files, string relativeTo = null, string directoryToPlaceIn = null)
        {
            foreach (var f in files)
            {
                AddFile(f, relativeTo: relativeTo, directoryToPlaceIn: directoryToPlaceIn);
            }
            
        }
        public void AddFiles(IEnumerable<string> filePaths, string relativeTo = null, string directoryToPlaceIn = null)
        {
            foreach (var f in filePaths)
            {
                AddFile(f, relativeTo: relativeTo, directoryToPlaceIn: directoryToPlaceIn);
            }
            
            
            
        }

        private readonly List<string> _tempFiles = new List<string>(); 

        public void AddStringContent(string data, string contentType, string contentFilename)
        {
            if (string.IsNullOrEmpty(contentFilename))
                throw new InvalidOperationException("Content Filename must be specified");

            string tmpFolderPath = System.IO.Path.GetTempPath() + System.IO.Path.GetRandomFileName() + CS.General_v3.Util.Random.GetString(20, 20) + "\\";
            CS.General_v3.Util.IO.CreateDirectory(tmpFolderPath);


            string filePath = tmpFolderPath + contentFilename;
            CS.General_v3.Util.IO.SaveToFile(filePath, contentType);

            AddFile(filePath);
            _tempFiles.Add(filePath);
        }

        public void AddFolder(string path, bool recursive = true)
        {
            var files = CS.General_v3.Util.IO.FindFilesInFolder(path, "*", true, recursive: recursive);
            foreach (var f in files)
            {
                AddFile(f.FullName, path);
            }
            
            
        }
        private void deleteTempFiles()
        {
            foreach (var f in _tempFiles)
            {
                CS.General_v3.Util.IO.DeleteFile(f);
                
            }
            _tempFiles.Clear();
        }

        public void CreateZipFile(string zipPath, ZipCreateParams zipParams)
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(zipParams, "zipParams");
            
            _zip.CreateZIPFile(zipPath,  _files,zipParams);
            
            deleteTempFiles();
        }






        #region IDisposable Members

        public void Dispose()
        {
            deleteTempFiles();
            
        }

        #endregion
    }
}
