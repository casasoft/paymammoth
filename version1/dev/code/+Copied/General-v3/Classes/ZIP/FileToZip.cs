﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.ZIP
{
    public class FileToZip
    {
        public FileToZip()
        {

        }
        
        public void SetFilePath(string filepath, string relativeTo = null)
        {
            this.FilePath = filepath;
            if (!string.IsNullOrWhiteSpace(relativeTo))
            {
                this.DirectoryPathInArchive = CS.General_v3.Util.IO.GetDirName(CS.General_v3.Util.IO.ConvertAbsolutePathToRelativePath(filepath, relativeTo));
            }
        }

        public string FilePath { get; set; }
        public string DirectoryPathInArchive { get; set; }
        public string Password { get; set; }
    }
}
