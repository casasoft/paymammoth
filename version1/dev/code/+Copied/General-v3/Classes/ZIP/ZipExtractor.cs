﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.ZIP
{
    public class ZipExtractor
    {
        private IZipComponent _zip;
        public ZipExtractor(EnumsZip.ZIP_COMPONENT_TYPE componentType)
        {
            _zip = ZipUtil.GetComponentFromEnum(componentType);
            
        }

        public void ExtractZIP(string zipPath, string dirToExtractTo)
        {
            _zip.ExtractZIP(zipPath, dirToExtractTo);
        }

    }
}
