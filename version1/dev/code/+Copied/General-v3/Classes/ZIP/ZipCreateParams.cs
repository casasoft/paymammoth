﻿namespace CS.General_v3.Classes.ZIP
{
    public class ZipCreateParams
    {
        
        
        public int CompressionLevel { get; set; }
        public string Password { get; set; }
        public bool UseZip64WhenNecessary { get; set; }
        public bool UseUnicode { get; set; }
        public bool MakeWinZipCompatibleFiles { get; set; }
        public bool UseEncryption { get; set; }
        public ZipCreateParams()
        {
            this.UseZip64WhenNecessary = true;
            this.CompressionLevel = 5;
            this.UseUnicode = true;
            this.MakeWinZipCompatibleFiles = true;
            this.UseEncryption = false;



        }


    }
}
