﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.RandomClasses
{
    public class RandomTextRepository
    {
        public int Length { get; private set; }
        public List<string> Words { get; private set; }
        public void LoadFromFile(string filePath, int length)
        {
            this.Length = length;
            this.Words = new List<string>();
            string s = CS.General_v3.Util.IO.LoadFile(filePath);
            string[] tokens = s.Split(new string[] { "\r", "\n", "\r\n" },   StringSplitOptions.None);
            for (int i = 0; i < tokens.Length; i++)
            {
                var t = tokens[i];
                t = t.Trim();
                if (!string.IsNullOrWhiteSpace(t))
                    this.Words.Add(t);
            }
        }
        public string GetRandomWord()
        {
            if (this.Words != null && this.Words.Count > 0)
            {
                int index = CS.General_v3.Util.Random.GetInt(0, this.Words.Count - 1);
                return this.Words[index];
            }
            return null;

        }
    }
}
