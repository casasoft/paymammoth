﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3.Classes.RandomClasses
{
    public class RandomTextGenerator
    {

        private Dictionary<int,RandomTextRepository> textRepositories = null;
        public RandomTextGenerator()
        {
            this.textRepositories = new Dictionary<int,RandomTextRepository>();
        }

        public void LoadFromFile(string filePath, int length = 0)
        {
            RandomTextRepository rep = new RandomTextRepository();
            rep.LoadFromFile(filePath, length);
            textRepositories[length] = rep;
        }
        public string GetRandomWord()
        {
            var keys = CS.General_v3.Util.ListUtil.GetListFromEnumerator(textRepositories.Keys);
            var length = keys[CS.General_v3.Util.Random.GetInt(0, keys.Count - 1)];
            return GetRandomWord(length);
        }
        public string GetRandomWord(int length)
        {
            return textRepositories[length].GetRandomWord();
        }

        public string GetRandomParagraph(int wordsFrom, int wordsTo)
        {
            int wordsRnd = CS.General_v3.Util.Random.GetInt(wordsFrom, wordsTo);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < wordsRnd; i++)
            {
                if (i > 0)
                    sb.Append(" ");
                sb.Append(GetRandomWord());
            }
            return sb.ToString();
        }
        public string GetRandomParagraph(int words)
        {
            return GetRandomParagraph(words, words);

        }
        public string GetRandomParagraphs(int paragraphsFrom, int paragraphsTo, int wordsFrom, int wordsTo, string paragraphDelimeter = "\r\n\r\n")
        {
            int totalParagraphcs = CS.General_v3.Util.Random.GetInt(paragraphsFrom, paragraphsTo);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < totalParagraphcs; i++)
            {
                if (i > 0)
                    sb.Append(paragraphDelimeter);
                sb.Append(GetRandomParagraph(wordsFrom,wordsTo));
            }
            return sb.ToString();
        }

    }
}
