using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Configuration;
using System.Web;
using System.IO;

namespace CS.General_20090518
{
    public static class Settings2
    {
        public static bool IsUsingTestingFramework { get; set; }
        
        public enum JSCRIPT_FRAMEWORK
        {
            Dojo,
            JQuery,
            NotSpecified
        }
        public static class POP
        {
            public static string Host { get { return GetSettingFromDBOrSettingsFile("Email/POP/Host"); } }
            public static string Username { get { return GetSettingFromDBOrSettingsFile("Email/POP/User"); } }
            public static string Password { get { return GetSettingFromDBOrSettingsFile("Email/POP/Pass"); } }
            public static int Port { get { return GetSettingInt("Email/POP/Port", 110); } }

        }

        public static class Emails
        {
            public static bool LoadEmailsFromDatabase
            {
                get
                {
                    return GetSettingBool("Email/LoadEmailsFromDatabase", false);
                }
                    
            }
            public static string ErrorEmail
            {
                get
                {
                    string s = GetSettingFromDBOrSettingsFile("Email/Error");
                    if (string.IsNullOrEmpty(s))
                    {
                        s = "errors@casasoft.com.mt";
                    }
                    return s;

                }
            }
            public static string NotificationEmail
            {
                get
                {
                    if ((CS.General_20090518.Util.Other.IsLocalTestingMachine))
                        return NotificationEmail_Localhost;
                    else
                        return NotificationEmail_Normal;
                    /*

                    if (CS.General_20090518.Classes.Settings.v1.SettingsMain.CheckIfInstanceIsFilled())
                        return CS.General_20090518.Classes.Settings.v1.SettingsMain.Instance.Other.GetNotificationEmailAddress();
                    else
                        return CS.General_20090518.Classes.Settings.v1.SettingsOther.getNotificationEmailAddress_Static();
*/
                    
                }
            }
            public static string NotificationEmail_Normal
            {
                get
                {
                    string s = GetSettingFromDBOrSettingsFile("Email/Notification");
                    if (string.IsNullOrEmpty(s)) s = GetSettingFromDBOrSettingsFile("NotificationEmail");
                    
                    if (string.IsNullOrEmpty(s))
                    {
                        s = Settings.AdminEmail;
                    }
                    return s;

                }
            }
            public static string NotificationEmail_Localhost
            {
                get
                {
                    string s = GetSettingFromDBOrSettingsFile("Email/Notification_Localhost");
                    if (string.IsNullOrEmpty(s))
                    {
                        s = NotificationEmail_Normal;
                    }
                    return s;

                }
            }
        }
        public static class Database
        {
            public class OVERRIDE_INFO
            {
                public string Host_Normal = null;
                public string Host_Localhost = null;
                public CS.General_20090518.DB.MSSQL.CONNECTION_TYPE? MicrosoftSQLServer_ConnectionType = null;
                public int? Port = null;
                public string DatabaseName = null;
                public string Username = null;
                public string Pass = null;
                public bool? EncryptedPass = null;
                public string PrimaryKeyValuesTable = null;
                public string CharacterSet = null;
                public bool? UsePrimaryKeyValuesTable = null;
                public void Reset()
                {
                    PrimaryKeyValuesTable = null;
                    DatabaseName = null;
                    Username = null;
                    Pass = null;
                    UsePrimaryKeyValuesTable = null;
                    Host_Normal = null;
                    Host_Localhost = null;
                    MicrosoftSQLServer_ConnectionType = null;
                    Port = null;
                    EncryptedPass = null;
                    CharacterSet = null;
                }

            }
            private static OVERRIDE_INFO _OverrideInfo = null;
            public static OVERRIDE_INFO OverrideInfo
            {
                get
                {
                    OVERRIDE_INFO info = null;
                    /*if (CS.General_20090518.Settings.Others.IsWebApplication && CS.General_20090518.Util.Other.IsWebApplication)
                    {
                        info = (OVERRIDE_INFO)CS.General_20090518.Util.Page.GetSessionObject("__Database_OverrideInfo");
                        if (info == null)
                        {
                            info = new OVERRIDE_INFO();
                            OverrideInfo = info;
                        }
                    }
                    else
                    {*/
                        if (_OverrideInfo == null)
                        {
                            _OverrideInfo = new OVERRIDE_INFO();
                        }
                        info = _OverrideInfo;

                    //}
                    return info;
                }
                set
                {
                    /*if (CS.General_20090518.Settings.Others.IsWebApplication)
                    {
                        CS.General_20090518.Util.Page.SetSessionObject("__Database_OverrideInfo", value);
                    }
                    else
                    {*/
                        _OverrideInfo = value;
                   // }
                }
            }

            public static CS.General_20090518.DB.MSSQL.CONNECTION_TYPE MicrosoftSQLServer_ConnectionType
            {
                get
                {
                    if (OverrideInfo.MicrosoftSQLServer_ConnectionType != null)
                        return OverrideInfo.MicrosoftSQLServer_ConnectionType.Value;
                    else
                    {
                        string s = getDatabaseSetting("Database/SQLServer_ConnectionType");
                        if (!string.IsNullOrEmpty(s))
                        {
                            return CS.General_20090518.DB.MSSQL.ConnectionTypeFromText(s);
                        }
                        else
                        {
                            return CS.General_20090518.DB.MSSQL.CONNECTION_TYPE.NamedPipes;
                        }
                    }
                }
            }
            public static string Host
            {
                get
                {
                    return (CS.General_20090518.Util.Other.IsLocalTestingMachine ? Host_Localhost : Host_Normal);
                }

            }
            public static string Host_Normal
            {
                get
                {
                    if (!string.IsNullOrEmpty(OverrideInfo.Host_Normal))
                        return OverrideInfo.Host_Normal;
                    else
                        return getDatabaseSetting("Database/Host");
                }
            }
            public static string Host_Localhost
            {
                get
                {
                    string s = null;
                    if (!string.IsNullOrEmpty(OverrideInfo.Host_Normal))
                    {
                        s=  OverrideInfo.Host_Localhost;
                    }
                    else
                    {
                        s = getDatabaseSetting("Database/Host_Localhost");
                        
                    }
                    if (string.IsNullOrEmpty(s))
                        s = Host_Normal;
                    return s;
                        
                }
            }
            public static int Port
            {
                get
                {
                    if (OverrideInfo.Port != null)
                        return OverrideInfo.Port.Value;
                    else
                        return Convert.ToInt32(getDatabaseSetting("Database/Port","3306"));
                }
            }
            public static string CharacterSet
            {
                get
                {
                    if (OverrideInfo.CharacterSet != null)
                        return OverrideInfo.CharacterSet;
                    else
                        return getDatabaseSetting("Database/CharacterSet");
                }
            }
            public static string User
            {
                get
                {
                    if (string.IsNullOrEmpty(OverrideInfo.Username))
                        return getDatabaseSetting("Database/User");
                    else
                        return OverrideInfo.Username;
                }
            }
            public static string Pass
            {
                get
                {
                    if (string.IsNullOrEmpty(OverrideInfo.Pass))
                    {
                        string pass = getDatabaseSetting("Database/Pass");
                        if (!DBEncryptedPass)
                            return pass;
                        else
                            return CS.General_20090518.Util.Encryption.Decrypt(pass);
                    }
                    else
                    {
                        return OverrideInfo.Pass;
                    }
                }
            }
            public static bool UsePrimaryKeyValuesTable
            {
                get
                {
                    bool b = true;
                    if (OverrideInfo.UsePrimaryKeyValuesTable != null)
                        b = OverrideInfo.UsePrimaryKeyValuesTable.Value;
                    else
                    {
                        string s = getDatabaseSetting("Database/UsePrimaryKeyValuesTable");
                        if (!string.IsNullOrEmpty(s))
                        {
                            b = convertStringToBool(getDatabaseSetting("Database/UsePrimaryKeyValuesTable"));
                        }
                    }
                    return b;
                }
            }
            /// <summary>
            /// Primary Keys Value Table.  Table must contain 2 columns: TableName (VarChar), LastValue (BigINT). Preferably tablename is indexed
            /// </summary>
            public static string PrimaryKeyValuesTable
            {
                get
                {
                    if (string.IsNullOrEmpty(OverrideInfo.PrimaryKeyValuesTable))
                        return getDatabaseSetting("Database/PrimaryKeyValuesTable");
                    else
                        return OverrideInfo.PrimaryKeyValuesTable;
                }
            }
            public static bool EncryptedPass
            {
                get
                {
                    if (OverrideInfo.EncryptedPass != null)
                        return OverrideInfo.EncryptedPass.Value;
                    else
                        return convertStringToBool(getDatabaseSetting("Database/EncryptedPass"));

                }
            }
            public static string DatabaseName
            {
                get
                {
                    if (string.IsNullOrEmpty(OverrideInfo.DatabaseName))
                    {
                        return getDatabaseSetting("Database/Name");
                    }
                    else
                    {
                        return OverrideInfo.DatabaseName;
                    }
                }
            }
        }
        public static class ImageInfo

        {
            public static int GetImageWidthFromSettings(string baseSetting)
            {
                int width, height;

                GetImageInfoFromSettings(baseSetting, out width, out height);
                return width;
            }
            public static int GetImageHeightFromSettings(string baseSetting)
            {
                int width, height;

                GetImageInfoFromSettings(baseSetting, out width, out height);
                return height;
            }
            public static void GetImageInfoFromSettings(string baseSetting,
               out int width, out int height)
            {
                string folder;

                GetImageInfoFromSettings(baseSetting, out width, out height, out folder);
            }
            public static void GetImageInfoFromSettings(string baseSetting,
               out int width, out int height, out string folder)
            {
                CS.General_20090518.Enums.CROP_IDENTIFIER cropIdentifier;
                GetImageInfoFromSettings(baseSetting, out width, out height, out folder,  out cropIdentifier);
            }

            public static void ParseImageSettingsIntoValues(string settingValue, out int width, out int height, out CS.General_20090518.Enums.CROP_IDENTIFIER cropIdentifier)
            {
                string[] tmp = settingValue.Split('-');
                string size = tmp[0].Trim();
                string cropInfo = null;
                if (tmp.Length > 1)
                    cropInfo = tmp[1].Trim();

                size = size.Replace(" ", "");
                size = size.ToLower();
                size = size.Replace("*", "x");
                string[] tokens = size.Split(new string[] { "x" }, StringSplitOptions.RemoveEmptyEntries);
                width = Convert.ToInt32(tokens[0]);
                height = Convert.ToInt32(tokens[1]);
                cropIdentifier = CS.General_20090518.Enums.CropIdentifierFromText(cropInfo);
                
            }

            public static void GetImageInfoFromSettings(string baseSetting,
                out int width, out int height, out string folder,  out CS.General_20090518.Enums.CROP_IDENTIFIER cropIdentifier)
            {
                if (!baseSetting.EndsWith("/"))
                    baseSetting += "/";

                //new way

                string sizeCropInfo = CS.General_20090518.Settings.GetSettingFromDBOrSettingsFile(baseSetting);
                string size = "";
                string cropInfo = "";
                if (!string.IsNullOrEmpty(sizeCropInfo))
                {
                    ParseImageSettingsIntoValues(sizeCropInfo, out width, out height, out cropIdentifier);
                }
                else
                {
                    size = CS.General_20090518.Settings.GetSettingFromDBOrSettingsFile(baseSetting + "Size");
                    cropInfo = CS.General_20090518.Settings.GetSettingFromDBOrSettingsFile(baseSetting + "CropIdentifier");
                    if (!string.IsNullOrEmpty(size))
                    {
                        size = size.Replace(" ", "");
                        size = size.ToLower();
                        size = size.Replace("*", "x");
                        string[] tokens = size.Split(new string[] { "x" }, StringSplitOptions.RemoveEmptyEntries);
                        width = Convert.ToInt32(tokens[0]);
                        height = Convert.ToInt32(tokens[1]);
                    }
                    else
                    {
                        width = CS.General_20090518.Settings.GetSettingInt(baseSetting + "W");
                        height = CS.General_20090518.Settings.GetSettingInt(baseSetting + "H");
                    }
                    cropIdentifier = CS.General_20090518.Enums.CropIdentifierFromText(cropInfo);
                }
                folder = CS.General_20090518.Settings.GetSettingFromDBOrSettingsFile(baseSetting + "Folder");
                if (CS.General_20090518.Settings.GetSettingFromDBOrSettingsFile(baseSetting + "FillsBox") != null)
                {
                    bool fillsBox = CS.General_20090518.Settings.GetSettingBool(baseSetting + "FillsBox", false);
                    if (fillsBox && cropIdentifier == CS.General_20090518.Enums.CROP_IDENTIFIER.None)
                        cropIdentifier = CS.General_20090518.Enums.CROP_IDENTIFIER.MiddleMiddle;
                }
                
                
                
            }
        }

        public static string GetSettingFromDBOrSettingsFile(string identifier, string defaultValue = null)
        {
            if (identifier.EndsWith("/")) identifier = identifier.Substring(0, identifier.Length - 1);
            string s = null;
            if (Classes.Settings.v1.SettingsMain.CheckIfInstanceIsFilled())
            {
                s = Classes.Settings.v1.SettingsMain.Instance.GetSetting(identifier,null);
            }
            if (string.IsNullOrWhiteSpace(s))
            {
                s = CS.General_20090518.Settings.getSettingFromFileSystem(identifier, defaultValue);
            }
            return s;
        }
        public static int GetSettingInt(string identifier, int defaultValue = 0)
        {
            string s = GetSettingFromDBOrSettingsFile(identifier, defaultValue.ToString());
            int num = 0;
            if (!Int32.TryParse(s, out num))
                num = defaultValue;
            return num;
        }
        public static double GetSettingFromDBOrSettingsFile_Double(string identifier, double defaultValue = 0)
        {

            

            string s = GetSettingFromDBOrSettingsFile(identifier, defaultValue.ToString("0.0000000000"));
            double num = 0;
            if (!double.TryParse(s, out num))
                num = defaultValue;
            return num;
            
        }
        public static bool GetSettingBool(string identifier, bool defaultValue)
        {
            string s = GetSettingFromDBOrSettingsFile(identifier, (defaultValue ? "1" : "0"));
            bool b = (CS.General_20090518.Util.Other.TextToBoolNullable(s)).GetValueOrDefault(defaultValue);
            return b;
        }
        public static class CompanyInfo
        {
            public static string Facebook { get { return CS.General_20090518.Settings.GetSettingFromDBOrSettingsFile("Company/Facebook"); } }
            public static string FacebookGroup { get { return CS.General_20090518.Settings.GetSettingFromDBOrSettingsFile("Company/FacebookGroup"); } }
            public static string Twitter { get { return CS.General_20090518.Settings.GetSettingFromDBOrSettingsFile("Company/Twitter"); } }
            public static string CompanyName { get { return CS.General_20090518.Settings.GetSettingFromDBOrSettingsFile("Company/CompanyName"); } }
            public static string Website { get { return CS.General_20090518.Settings.GetSettingFromDBOrSettingsFile("Company/Website"); } }
            public static string Tel { get { return CS.General_20090518.Settings.GetSettingFromDBOrSettingsFile("Company/Tel"); } }
            public static string Fax { get { return CS.General_20090518.Settings.GetSettingFromDBOrSettingsFile("Company/Fax"); } }
            public static string Skype { get { return CS.General_20090518.Settings.GetSettingFromDBOrSettingsFile("Company/Skype"); } }
            public static string DirectorName { get { return CS.General_20090518.Settings.GetSettingFromDBOrSettingsFile("Company/DirectorName"); } }
            public static string Mobile { get { return CS.General_20090518.Settings.GetSettingFromDBOrSettingsFile("Company/Mobile"); } }
            public static string Address1 { get { return CS.General_20090518.Settings.GetSettingFromDBOrSettingsFile("Company/Address1"); } }
            public static string Address2 { get { return CS.General_20090518.Settings.GetSettingFromDBOrSettingsFile("Company/Address2"); } }
            public static string Address3 { get { return CS.General_20090518.Settings.GetSettingFromDBOrSettingsFile("Company/Address3"); } }
            public static string PostCode { get { return CS.General_20090518.Settings.GetSettingFromDBOrSettingsFile("Company/PostCode"); } }
            public static string Country { get { return CS.General_20090518.Settings.GetSettingFromDBOrSettingsFile("Company/Country"); } }
            public static string Locality { get { return CS.General_20090518.Settings.GetSettingFromDBOrSettingsFile("Company/Locality"); } }
            public static string Email { get { return CS.General_20090518.Settings.GetSettingFromDBOrSettingsFile("Company/Email"); } }
            public static string VatNumber { get { return CS.General_20090518.Settings.GetSettingFromDBOrSettingsFile("Company/VATNumber"); } }
            public static string PaymentGatewayTransactionID { get { return CS.General_20090518.Settings.GetSettingFromDBOrSettingsFile("Company/PaymentGatewayTransactionID"); } }

            public static double? GoogleLat
            {
                get
                {
                    string str = CS.General_20090518.Settings.GetSettingFromDBOrSettingsFile("Company/GoogleLat");
                    if (!String.IsNullOrEmpty(str))
                    {
                        return Convert.ToDouble(str);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            public static double? GoogleLng
            {
                get
                {
                    string str = CS.General_20090518.Settings.GetSettingFromDBOrSettingsFile("Company/GoogleLng");
                    if (!String.IsNullOrEmpty(str))
                    {
                        return Convert.ToDouble(str);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            public static int? GoogleZoomLevel
            {
                get
                {
                    string str = CS.General_20090518.Settings.GetSettingFromDBOrSettingsFile("Company/GoogleZoomLevel");
                    if (!String.IsNullOrEmpty(str))
                    {
                        return Convert.ToInt32(str);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            public static string GetAddressAsOneLine(string delimeter)
            {
                string locAndPostCode = Locality;
                if (!String.IsNullOrEmpty(PostCode))
                {
                    locAndPostCode += ", " + PostCode;
                }
                string s = CS.General_20090518.Util.Text.AppendStrings(delimeter, Address1, Address2, Address3, locAndPostCode, Country);
                
                
                return s;

            }
            
        }
        private static bool checkIfSettingsMainXmlFileExists()
        {
            if (__xmlDocMain != null)
                return true;
            else
            {
                var path = getSettingsMainXmlFilePath();
                return System.IO.File.Exists(path);
            }
        }
        private static bool checkIfSettingsDatabaseXmlFileExists()
        {
            if (__xmlDocDatabase != null)
                return true;
            else
            {
                var path = getSettingsDatabaseXmlFilePath();
                return System.IO.File.Exists(path);
            }
        }

        private static string getSettingsDatabaseXmlFilePath()
        {
            string path = "";
            if (string.IsNullOrEmpty(CustomSettingsDatabaseFilePath))
            {
                List<string> list = new List<string>();
                list.Add(SettingsDatabasePath_1);
                list.Add(SettingsDatabasePath_2);
                for (int i = list.Count - 1; i >= 0; i--)
                {
                    path = CS.General_20090518.Util.Page.MapPath(list[i]);
                    if (File.Exists(path))
                        break;
                }
            }
            else
            {
                path = CustomSettingsDatabaseFilePath;
            }
            return path;
        }
        private static string getSettingsMainXmlFilePath()
        {
            string path = "";
            if (string.IsNullOrEmpty(CustomSettingsMainFilePath))
            {

                List<string> list = new List<string>();
                list.Add(SettingsMainPath_1);
                list.Add(SettingsMainPath_2);
                list.Add(SettingsMainPath_3);
                list.Add(SettingsMainPath_4);
                list.Add(SettingsMainPath_5);
                //read in reverse order
                for (int i = list.Count - 1; i >= 0; i--)
                {
                    path = CS.General_20090518.Util.Page.MapPath(list[i]);
                    if (File.Exists(path))
                        break;
                }
                


            }
            else
            {
                path = CustomSettingsMainFilePath;
            }
            return path;
        }
        private static readonly object __xmlDocLoadPadlock = new object();
        public static XmlDocument __xmlDocDatabase = null;
        public static XmlDocument _xmlDocDatabase
        {
            get
            {
                lock (__xmlDocLoadPadlock)
                {
                    if (__xmlDocDatabase == null)
                    {
                        string path = getSettingsDatabaseXmlFilePath();
                        if (System.IO.File.Exists(path))
                        {
                            XmlDocument doc = new XmlDocument();
                            doc.Load(path);
                            __xmlDocDatabase = doc;
                        }

                    }
                }
                return __xmlDocDatabase;
            }
        }
        
        private static void reloadSettingsXMLFile()
        {
            lock (__xmlDocLoadPadlock)
            {
                string path = getSettingsMainXmlFilePath();

                XmlDocument doc = new XmlDocument();
                doc.Load(path);
                __xmlDocMain = doc;
            }
        }
        public static XmlDocument __xmlDocMain = null;
        public static XmlDocument _xmlDocMain
        {
            get
            {
                lock (__xmlDocLoadPadlock)
                {
                    if (__xmlDocMain == null)
                    {
                        string path = getSettingsMainXmlFilePath();
                        reloadSettingsXMLFile();
                        CS.General_20090518.Util.IO.WatchFileForChanges(path, _onSettings_fileChange);
                    
                    }
                }
                return __xmlDocMain;
            }
        }
        public static void _onSettings_fileChange(object sender, FileSystemEventArgs e)
        {
            reloadSettingsXMLFile();

        }
        public static bool DoNotUseSettings = false;
        //IMPORTANT::  Make the settings.xml not publicly readable from IIS!!
        public enum PRICING_TYPE
        {
            EuroOnly, MTLOnly, MTLandEuro, Sterling, Dollar, None
        }
        public static class Others
        {
            public static string JavaScript_Com_Version_Override { get; set; }
            public static string JavaScript_Com_Version
            {
                get 
                {
                    if (JavaScript_Com_Version_Override == null)
                    {
                        string s = getSettingFromFileSystem("Others/JavaScript_Com_Version", "v2");
                        return s;
                    }
                    else
                    {
                        return JavaScript_Com_Version_Override;
                    }


                }


            

            }
            public static bool HTTPCombiner_UseCaching
            {
                get
                {
                    if (CS.General_20090518.Util.Other.IsLocalTestingMachine)
                        return HTTPCombiner_UseCaching_Localhost;
                    else
                        return HTTPCombiner_UseCaching_Normal;
                }
            }
            public static bool HTTPCombiner_UseCaching_Normal
            {
                get
                {
                    return Settings.GetSettingBool("HTTPCombiner/UseCaching", false);
                }
            }
            public static bool HTTPCombiner_UseCaching_Localhost
            {
                get
                {

                    bool? b = Settings.GetSettingBoolNullable("HTTPCombiner/UseCaching_Localhost");
                    if (b == null)
                        b = HTTPCombiner_UseCaching_Normal;
                    return b.GetValueOrDefault(true);
                    
                }
            }
            public static bool SendEmailsOnError
            {
                get
                {
                    return Settings.GetSettingBool("SendEmailsOnError", true);
                }
            }
            public static string CMS_Root
            {
                get
                {
                    string s = GetSettingFromDBOrSettingsFile("CMS_Root", "/cms/");


                    if (string.IsNullOrEmpty(s))
                    {
                        throw new InvalidOperationException("You must set the application setting 'CMS_Root' in the settings.config for the CMS to work");
                    }
                    else
                    {
                        if (!s.StartsWith("/")) s = "/" + s;
                        if (!s.EndsWith("/")) s = s + "/";
                    }
                    return s;
                }
            }
            public static string ApplicationPath
            {
                get
                {
                    return System.Windows.Forms.Application.ExecutablePath;
                }
            }
            public static double VATRate
            {
                get
                {
                    return GetSettingFromDBOrSettingsFile_Double("VATRate", 18);
                }
            }

            public static string CMSTitle
            {
                get
                {
                    return WebsiteName + " CMS";
                }
            }
            public static bool UseRewriteRules
            {
                get
                {
                    if (CS.General_20090518.Util.Other.IsLocalTestingMachine)
                        return UseRewriteRules_Localhost;
                    else
                        return UseRewriteRules_Normal;


                }
            }
            public static bool UseRewriteRules_Normal { get { return Settings.GetSettingBool("UseRewriteRules", false); } }
            public static bool UseRewriteRules_Localhost 
            { 
                get 
                { 
                    bool? b =Settings.GetSettingBoolNullable("UseRewriteRules_Localhost");
                    if (b == null)
                        b = UseRewriteRules_Normal;
                    return b.GetValueOrDefault(false);
                }
            }

            public static bool IsWebApplication
            {
                get
                {
                    if (System.Web.Hosting.HostingEnvironment.IsHosted)
                    {
                        return true;
                    }

                    else
                    {

                        string s = System.Web.Configuration.WebConfigurationManager.AppSettings["Website_IsWebsite"];
                        if (s != null && s.ToLower() == "true")
                        {
                            return true;
                        }
                        else
                            return false;
                    }

                }
            }
        }


        public static string SettingsDatabasePath_1 = "/Settings.database.xml";
        public static string SettingsDatabasePath_2 = "/App_Data/Settings.database.xml";
        public static string SettingsMainPath_1 = "/Settings.xml";
        public static string SettingsMainPath_2 = "/Settings.settings";
        public static string SettingsMainPath_3 = "/Settings.main.xml";
        public static string SettingsMainPath_4 = "/App_Data/Settings.xml";
        public static string SettingsMainPath_5 = "/App_Data/Settings.main.xml";
        public static string CustomSettingsMainFilePath { get; set; }
        public static string CustomSettingsDatabaseFilePath { get; set; }
        public static string GetSettingFromXML(string setting)
        {
            return getSettingFromXML(setting, _xmlDocMain);
        }
        private static string getSettingFromXML(string setting, XmlDocument doc)
        {
            setting = "Settings/" + setting;
            if (setting.EndsWith("/"))
                setting = setting.Substring(0,setting.Length-1);
            string s = "";
            try
            {
                if (doc != null && doc.SelectSingleNode(setting) != null)
                {
                    s = doc.SelectSingleNode(setting).InnerText;
                }
            }
            catch  (Exception ex)
            {
                int k = 5;
            }
            return s;
        }
        public static string PermanentRedirectionUrls
        {
            get
            {
                return GetSettingFromDBOrSettingsFile("PermanentRedirectionUrls", "");
            }
        }
        private static string getDatabaseSetting(string settingName, string defaultValue = null)
        {
            string s = getSettingFromFileSystem(settingName,defaultValue);
            if (string.IsNullOrEmpty(s))
            {
                s = getSettingFromXML(settingName, _xmlDocDatabase);
                
            }
            return s;
        }

        public static string GetSetting(string settingName)
        {
            return getSettingFromFileSystem(settingName, "");
        }
        private static readonly object tmpLock = new object();
        private static string getSettingFromFileSystem(string settingName, string defaultValue)
        {
            string s = null;
            
                string setting = settingName;
                if (setting.EndsWith("/"))
                    setting = setting.Substring(0, settingName.Length - 1);
                
                setting = setting.Replace('/', '_');
                bool ok = false;
                bool isWebApp = false;
                try
                {
                    isWebApp = Others.IsWebApplication;

                }
                catch
                {
                    isWebApp = false;
                }

                if (isWebApp && !checkIfSettingsMainXmlFileExists())
                {
                    s = System.Web.Configuration.WebConfigurationManager.AppSettings[setting];
                    if (s == null)
                        s = "";
                    ok = (s != "");
                }
                else
                {
                    s = GetSettingFromXML(settingName);
                }
                if (string.IsNullOrEmpty(s))
                    s = defaultValue;
            
            return s;
        }
        public static int GetSettingInt(string setting, int defaultValue)
        {
            string s = GetSetting(setting);
            if (s != null && s != "")
                return Convert.ToInt32(s);
            else
                return defaultValue;
        }
        public static int GetSettingInt(string setting)
        {
            return GetSettingInt(setting, Int32.MinValue);
        }
        /// <summary>
        /// Returns a list of settings (string).  Only in XML!
        /// </summary>
        /// <param name="settingName"></param>
        /// <returns></returns>
        public static List<string> GetSettingListString(string settingName)
        {
            if (settingName.StartsWith("/")) settingName = settingName.Remove(0, 1);
            XmlNode node = _xmlDocMain.SelectSingleNode("Settings/" + settingName);
            List<string> list = new List<string>();
            if (node != null)
            {
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.ChildNodes.Count == 1)
                    {
                        list.Add(n.InnerText);
                    }

                }
            }
            return list;
        }
        /// <summary>
        /// Returns a list of settings (int).  Only in XML!
        /// </summary>
        /// <param name="settingName"></param>
        /// <returns></returns>
        public static List<int> GetSettingListInt(string settingName)
        {
            if (settingName.StartsWith("/")) settingName = settingName.Remove(0, 1);
            XmlNode node = _xmlDocMain.SelectSingleNode("Settings/" + settingName);
            List<int> list = new List<int>();
            if (node != null)
            {
                foreach (XmlNode n in node.ChildNodes)
                {
                    if (n.ChildNodes.Count == 1)
                    {
                        int tmp;
                        if (Int32.TryParse(n.InnerText, out tmp))
                        {
                            list.Add(tmp);
                        }

                    }

                }
            }
            return list;
        }
        public static bool GetSettingBool(string setting)
        {
            return GetSettingBool(setting, false);
        }
        public static bool GetSettingBool(string setting, bool defValue)
        {
            bool? b = GetSettingBoolNullable(setting);

            if (b == null)
                b = defValue;
            return b.Value;
        }
        private static bool convertStringToBool(string s)
        {
            return (s == "true" || s == "1" || s == "yes" || s == "on" || s == "y");
        }
        public static bool? GetSettingBoolNullable(string setting)
        {
            string s = GetSetting(setting);
            if (s != null) s = s.Trim();
            bool? result = null;
            if (!string.IsNullOrEmpty(s))
            {
                s = s.ToLower();
                result = convertStringToBool(s);
            }
            
            return result;
        }
        public static double GetSettingDouble(string setting)
        {
            return GetSettingDouble(setting, double.MinValue);
            
        }
        public static double GetSettingDouble(string setting, double defaultValue)
        {
            string s = GetSetting(setting);
            if (s != null && s != "")
                return Convert.ToDouble(s);
            else
                return defaultValue;
        }
        ////////////////////////
        #region Settings
        public static string WebsiteName
        {
            get
            {
                string s =GetSettingFromDBOrSettingsFile("Website/Name");;
                if (string.IsNullOrEmpty(s)) s = GetSettingFromDBOrSettingsFile("Title");
                return s;
                
            }
        }
        public static string Title
        {
            get
            {
                return WebsiteName;
                
            }
        }
        public static bool IsOnProductionServer
        {
            get
            {
                return GetSettingBool("IsOnProductionServer", false);
            }
        }
        public static JSCRIPT_FRAMEWORK? JScriptFramework_Override { get; set; }
        public static JSCRIPT_FRAMEWORK JScriptFramework
        {
            get
            {
                if (JScriptFramework_Override == null)
                {
                    if (String.Compare(GetSetting("JScriptFramework"), "dojo", true) == 0)
                    {
                        return JSCRIPT_FRAMEWORK.Dojo;
                    }
                    else if (String.Compare(GetSetting("JScriptFramework"), "jquery", true) == 0)
                    {
                        return JSCRIPT_FRAMEWORK.JQuery;
                    }
                    else
                    {
                        //this is the default framework
                        return JSCRIPT_FRAMEWORK.Dojo;
                        //return JSCRIPT_FRAMEWORK.NotSpecified;
                    }
                }
                else
                {
                    return JScriptFramework_Override.Value;
                }
            }

        }
        public static bool IsInLocalTesting
        {
            get
            {
                return CS.General_20090518.Util.Other.IsLocalTestingMachine;
                /*
                CS.General_20090518.Util.Other.IsLocalTestingMachine
                bool b = GetSettingBool("IsInLocalTesting", false);
                if (IsOnProductionServer)
                    return false;
                else
                    return b;*/
            }
        }
        public static CS.General_20090518.DB.MSSQL.CONNECTION_TYPE DBSQLServer_ConnectionType
        {
            get
            {
                string s = GetSetting("Database/SQLServer_ConnectionType");
                if (!string.IsNullOrEmpty(s))
                {
                    return CS.General_20090518.DB.MSSQL.ConnectionTypeFromText(s);
                }
                else
                {
                    return CS.General_20090518.DB.MSSQL.CONNECTION_TYPE.NamedPipes;
                }
            }
        }
        public static string DBHost
        {
            get
            {
                return (CS.General_20090518.Util.Other.IsLocalTestingMachine ? DBHost_Localhost : DBHost_Normal);
            }
        }
        public static string DBHost_Normal
        {
            get
            {
                return Database.Host_Normal;
            }
        }
        public static string DBHost_Localhost
        {
            get
            {
                return Database.Host_Localhost;
            }
        }
        public static int DBPort
        {
            get
            {
                return Database.Port;
            }
        }
        /// <summary>
        /// Character set.  For MySQL, can be one of 'utf8', etc.
        /// </summary>
        public static string DBCharacterSet
        {
            get
            {
                return Database.CharacterSet;
            }
        }
        public static string DBUser
        {
            get
            {
                return Database.User;
            }
        }
        public static string DBPass
        {
            get
            {
                return Database.Pass;
            }
        }
        public static bool DBEncryptedPass
        {
            get
            {
                return Database.EncryptedPass;
            }
        }
        public static string DBName
        {
            get
            {
                return Database.DatabaseName;
            }
        }
        public static string MailDB_User
        {
            get
            {
                return GetSetting("Database/MailDB/User");

            }
        }
        public static string WebsiteBaseURL
        {
            get
            {
                return GetSetting("WebsiteBaseURL");
            }
        }
        public static bool MailDB_EncryptedPass
        {
            get
            {
                return GetSettingBool("Database/MailDB/EncryptedPass");
            }
        }
        public static string MailDB_Pass
        {
            get
            {
                string pass = GetSetting("Database/MailDB/Pass");
                if (!MailDB_EncryptedPass)
                    return pass;
                else
                    return CS.General_20090518.Util.Encryption.Decrypt(pass);
            }
        }
        private static string _LocalRootFolderCustom = "";
        private static string _BaseUrlCustom = "";

        /// <summary>
        /// The base URL to use when calling getBaseURL().  Please do not specify the http:// or https://, and without a trailing /
        /// </summary>
        public static string BaseUrlCustom
        {
            get { return Settings._BaseUrlCustom; }
            set
            {
                string s = value;
                if (s != null && s.Length >= "http://".Length && s.Substring(0, "http://".Length).ToLower() == "http://")
                {
                    s = s.Remove(0, "http://".Length);
                }
                else if (s != null && s.Length >= "https://".Length && s.Substring(0, "https://".Length).ToLower() == "https://")
                {
                    s = s.Remove(0, "https://".Length);
                }
                if (s != null && s.Length > "/".Length && s[s.Length - 1] == '/')
                    s = s.Remove(s.Length - 1, 1);
                Settings._BaseUrlCustom = s;

            }
        }
        /// <summary>
        /// When this is set, the return of 'LocalRootFolder' is this value
        /// </summary>
        public static string LocalRootFolderCustom
        {
            get { return _LocalRootFolderCustom; }
            set
            {
                _LocalRootFolderCustom = value;
                if (_LocalRootFolderCustom != null && _LocalRootFolderCustom.Length > 0 && _LocalRootFolderCustom[_LocalRootFolderCustom.Length - 1] != '\\')
                    _LocalRootFolderCustom += "\\";
            }
        }
        /// <summary>
        /// Gets the local root folder.  Returns either the local root folder, or LocalRootFolderCustom if it is set. Ends with a '/'
        /// </summary>
        public static string LocalRootFolder
        {
            get
            {
                if (!string.IsNullOrEmpty(LocalRootFolderCustom))
                {
                    return LocalRootFolderCustom;
                }
                else if (!string.IsNullOrEmpty(GetSetting("LocalRootFolderCustom")))
                {
                    string s = GetSetting("LocalRootFolderCustom");
                    s = s.Replace("/", "\\");
                    if (!s.EndsWith("\\"))
                        s += "\\";
                    return s;
                }
                else
                {
                    if (CS.General_20090518.Util.Other.IsWebApplication)
                    {
                        string s = CS.General_20090518.Util.Page.MapPath(CS.General_20090518.Util.Page.GetRoot());
                        if (s[s.Length - 1] != '\\') s += "\\";
                        return s;
                    }
                    else
                    {
                        return CS.General_20090518.Util.ApplicationUtil.ApplicationPath;
                    }
                }
            }
        }
        public static string MailDB_Name
        {
            get
            {
                return GetSetting("Database/MailDB/DBName");
            }
        }
        public static string MaxMind_GeoIP_DBLocation
        {
            get
            {
                return getSettingFromFileSystem("MaxMind/GeoIP_DBLocation","/_common/geoip/maxmind.dat");
            }
        }
        public static string MailDB_Identifier
        {
            get
            {
                return Settings.Title;
            }
        }
        public static string ErrorDB_User
        {
            get
            {
                return GetSetting("Database/ErrorDB/User");
            }
        }
        public static bool ErrorDB_EncryptedPass
        {
            get
            {
                return GetSettingBool("Database/ErrorDB/EncryptedPass");
            }
        }
        public static string ErrorDB_Pass
        {
            get
            {
                string pass = GetSetting("Database/ErrorDB/Pass");
                if (!ErrorDB_EncryptedPass)
                    return pass;
                else
                    return CS.General_20090518.Util.Encryption.Decrypt(pass);
            }
        }
        public static string ErrorDB_Email
        {
            get
            {
                return GetSetting("Database/ErrorDB/Email");

            }
        }

        public static string ErrorDB_Name
        {
            get
            {
                return GetSetting("Database/ErrorDB/DBName");

            }
        }
        public static string ErrorDB_Identifier
        {
            get
            {
                return Settings.Title;
            }
        }
        public static int ServerGMT
        {
            get
            {
                return GetSettingInt("Time/GMT");
            }
        }
        public static string AdminEmail
        {
            get
            {
                string s = null;
                if (IsInLocalTesting)
                {
                    s = AdminEmail_Localhost;
                }
                else
                {
                    s = AdminEmail_Normal;
                }
                return s;
                /**if (CS.General_20090518.Classes.Settings.v1.SettingsMain.CheckIfInstanceIsFilled())
                    return CS.General_20090518.Classes.Settings.v1.SettingsMain.Instance.Other.GetAdminEmailAddress();
                else
                    return CS.General_20090518.Classes.Settings.v1.SettingsOther.getAdminEmailAddress_Static();
                 //return (CS.General_20090518.Util.Other.IsLocalHost ? AdminEmail_Localhost : AdminEmail_Normal);*/
            }
        }
        public static string AdminEmail_Normal
        {
            get
            {
                string s = GetSettingFromDBOrSettingsFile("AdministrationEmail_From");
                if (string.IsNullOrEmpty(s)) s = GetSettingFromDBOrSettingsFile("Administration Email (From Email)");
                if (string.IsNullOrEmpty(s)) s = GetSettingFromDBOrSettingsFile("Email/Address");
                return s;
                /*
                if (string.IsNullOrEmpty(s))
                {
                    s = Settings.AdminEmail;
                }

                return GetSetting("Email/Address");*/
            }
        }
        public static string AdminEmail_Localhost
        {
            get
            {

                string s = GetSettingFromDBOrSettingsFile("Email/Address_Localhost");
                if (string.IsNullOrEmpty(s))
                {
                    s = AdminEmail_Normal;
                }
                //if (string.IsNullOrEmpty(s)) s = GetSettingFromDBOrSettingsFile("Administration Email (From Email)");
                //if (string.IsNullOrEmpty(s)) s = GetSettingFromDBOrSettingsFile("Email/Address");
                return s;
                /*string s =GetSetting("Email/Address_Localhost");
                if (string.IsNullOrEmpty(s))
                    s = AdminEmail_Normal;
                return s;*/
            }
        }


        public static bool PaymentTesting
        {
            get
            {
                return (CS.General_20090518.Util.Other.IsLocalTestingMachine ? PaymentTesting_Localhost : PaymentTesting_Normal);
            }
        }
        private static bool PaymentTesting_Normal
        {
            get
            {
                return GetSettingBool("PaymentTesting", false);
            }
        }
        private static bool PaymentTesting_Localhost
        {
            get
            {
                bool? b = GetSettingBoolNullable("PaymentTesting_Localhost");
                if (b == null)
                    b = PaymentTesting_Normal;
                return b.Value;
            }
        }
       
        public static bool PaymentUseSSL
        {
            get
            {
                return (CS.General_20090518.Util.Other.IsLocalTestingMachine ? PaymentUseSSL_Localhost : PaymentUseSSL_Normal);
            }
        }
        private static bool PaymentUseSSL_Normal
        {
            get
            {
                return GetSettingBool("PaymentUseSSL",true);
            }
        }
        private static bool PaymentUseSSL_Localhost
        {
            get
            {
                bool? b  = GetSettingBoolNullable("PaymentUseSSL_Localhost");
                if (b == null)
                    b = PaymentUseSSL_Normal;
                return b.Value;
            }
        }

        public static string AdminName
        {
            get
            {
                /*if (IsInLocalTesting)
                {
                    return AdminEmail_Localhost;
                }
                else
                {
                    return AdminEmail_Normal;
                }*/
                string s = CS.General_20090518.Settings.GetSettingFromDBOrSettingsFile("AdministrationEmail_Name");
                if (string.IsNullOrEmpty(s)) s = GetSettingFromDBOrSettingsFile("Administration Email (From Name)");
                if (string.IsNullOrEmpty(s)) s = CS.General_20090518.Settings.GetSettingFromDBOrSettingsFile("Email/Name");
                return s;
                
                /*if (CS.General_20090518.Classes.Settings.v1.SettingsMain.CheckIfInstanceIsFilled())
                    return CS.General_20090518.Classes.Settings.v1.SettingsMain.Instance.Other.GetAdminEmailName();
                else
                    return CS.General_20090518.Classes.Settings.v1.SettingsOther.getAdminEmailName_Static();
                */
                
            }
        }

        public static string SMTPHost
        {
            get
            {
                return (CS.General_20090518.Util.Other.IsLocalTestingMachine ? SMTPHost_Localhost : SMTPHost_Normal);
            }
        }
        public static string SMTPHost_Normal
        {
            get
            {
                return GetSettingFromDBOrSettingsFile("Email/SMTP/Host");
            }
        }
        public static string SMTPHost_Localhost
        {
            get
            {

                string s = GetSettingFromDBOrSettingsFile("Email/SMTP/Host_Localhost");
                if (string.IsNullOrEmpty(s))
                    s = SMTPHost_Normal;
                return s;
            }
        }
        public static double GeneralLogicVersion
        {
            get
            {
                return GetSettingDouble("GeneralLogicVersion", 1);
            }
        }
        public static string SMTPUser
        {
            get
            {
                return GetSettingFromDBOrSettingsFile("Email/SMTP/User");
            }
        }
        public static string SMTPPass
        {
            get
            {
                string s = GetSettingFromDBOrSettingsFile("Email/SMTP/Pass");
                if (!SMTPEncryptedPass)
                    return s;
                else
                    return CS.General_20090518.Util.Encryption.Decrypt(s);
            }
        }
        public static bool SMTPIsSecure
        {
            get
            {
                bool b = GetSettingBool("Email/SMTP/IsSecure", false);
                return b;
            }
        }
        public static bool SMTPEncryptedPass
        {
            get
            {
                return GetSettingBool("Email/SMTP/EncryptedPass", false);
            }
        }
        public static int SMTPPort
        {
            get
            {
                return GetSettingInt("Email/SMTP/Port", 25);
            }
        }
        public static string DBRoot_User
        {
            get
            {
                return GetSetting("Database/Root/User");
            }
        }
        public static bool CMS_AutoLoginFromDevMachines
        {
            get
            {
                return GetSettingBool("CMS/AutoLoginFromDevMachines",true);
            }
        }
        public static bool DBRoot_EncryptedPass
        {
            get
            {
                return GetSettingBool("Database/Root/EncryptedPass");
            }
        }
        public static string DBRoot_Pass
        {
            get
            {
                string pass = GetSetting("Database/Root/Pass");
                if (!DBRoot_EncryptedPass)
                    return pass;
                else
                    return CS.General_20090518.Util.Encryption.Decrypt(pass);
            }
        }
        public static string FFMpeg_Path
        {
            get
            {
                string ffmpegPath = GetSetting("FFMpeg/Path").Replace("/", "\\");
                string path = CS.General_20090518.Settings.LocalRootFolder + ffmpegPath + "ffmpeg.exe";
                return path;
            }
        }
        public static string ApplicationRoot
        {
            get
            {
                string root = GetSetting("Application/Root");
                if (root != null && root.Length > 0)
                {
                    if (root[root.Length - 1] == '/')
                        root = root.Remove(root.Length - 1, 1);
                }
                if (root != null && root.Length > 0)
                {
                    if (root[0] != '/')
                        root = "/" + root;
                }
                return root;
            }
        }
        public static string ApplicationFolder
        {
            get
            {
                string root = GetSetting("Application/Folder");
                if (root != null && root.Length > 0)
                {
                    if (root[root.Length - 1] != '\\')
                        root += "\\";
                }
                return root;
            }
        }
        public static string CurrencySymbol
        {
            get
            {
                return GetSettingFromDBOrSettingsFile("Application/CurrencySymbol", "�");
            }
        }
        public static double EuroToMTL
        {
            get
            {
                return GetSettingDouble("ExchangeRate/EuroToMTL");
            }
        }
        public static string EmailFolder
        {
            get
            {
                string s = getSettingFromFileSystem("Email/Folder","includes/emails");
                if (s[s.Length - 1] != '/')
                    s += "/";
                return s;
            }
        }
        /// <summary>
        /// Path from root to the signature file
        /// </summary>
        public static string EmailSignatureFile
        {
            get
            {
                return getSettingFromFileSystem("Email/SignatureFile","+signature.txt");
            }
        }
        public static string Email_HTMLTemplateFile
        {
            get
            {
                return getSettingFromFileSystem("Email/HTMLTemplateFile", "HTMLTemplate.html");
            }
        }
        /// <summary>
        /// signature tag
        /// </summary>
        public static string EmailSignatureTag
        {
            get
            {
                return GetSettingFromDBOrSettingsFile("Email/SignatureTag", "[SIGNATURE]");
            }
        }
        public static PRICING_TYPE PricingType
        {
            get
            {
                string s = GetSetting("Pricing/Type").ToLower();
                switch (s)
                {
                    case "eur": return PRICING_TYPE.EuroOnly;
                    case "mtl": return PRICING_TYPE.MTLOnly;
                    case "mtl+eur": return PRICING_TYPE.MTLandEuro;
                    case "gbp": return PRICING_TYPE.Sterling;
                    case "usd": return PRICING_TYPE.Dollar;
                }
                return PRICING_TYPE.None;


            }
        }
        #endregion



    }
}
