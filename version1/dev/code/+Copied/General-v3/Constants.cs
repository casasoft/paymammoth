﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CS.General_v3
{
    public class Constants
    {
        public const string QS_ID_PARAM = "id";
        public const string QS_CODE_PARAM = "code";
        public const string QS_IDENTIFIER_PARAM = "identifier";
        public const string QS_ADVERT_DISABLE_AD_NOTE_PARAM = "disableAdNote";
        public const string QS_GOOGLE_ANALYTICS_ID_PARAM = "analyticsID";
        public const string QS_GOOGLE_ANALYTICS_DOMAIN_PARAM = "domain";
        public const string SESSION_SOCIAL_LOGIN_REMEMBERME = "social_auth_remembermeval";
    }
}
