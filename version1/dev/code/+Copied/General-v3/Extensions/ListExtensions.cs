﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Util;

namespace CS.General_v3.Extensions
{
    public static class ListExtensions
    {


        public static void RandomizeListInSame<T>(this IList<T> list)
        {
            CS.General_v3.Util.ListUtil.RandomizeListInSame(list);
        }
        /// <summary>
        /// Sorts in the same list provided
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="comparers"></param>
        public static void SortInListByMultipleComparers<T>(this List<T> list, params Comparison<T>[] comparers)
        {
            Util.ListUtil.SortInListByMultipleComparers<T>(list, comparers);
        }
        public static void FilterDistinctItemsInList<T>(this List<T> list, Func<T,T, bool> equalityComparer)
        {
            for (int i = 0; i < list.Count; i++)
            {
                var itemI = list[i];
                for (int j = i + 1; j < list.Count; j++)
                {
                    var itemJ = list[j];
                    if (equalityComparer(itemI, itemJ))
                    {
                        list.RemoveAt(j);
                        j--;
                    }

                }
            }

        }
        public static string Join<T>(this IEnumerable<T> list, Func<T, string> toStringHandler, string delimiter, bool addIfNullOrEmpty = false, string lastDelimiter = null)
        {

            return ListUtil.JoinList(list, toStringHandler, delimiter, addIfNullOrEmpty, lastDelimiter);
        }
        public static string Join(this IEnumerable<string> list, string delimiter, bool addIfNullOrEmpty = false, string lastDelimiter = null)
        {

            return ListUtil.JoinList(list, delimiter, addIfNullOrEmpty, lastDelimiter);


        }
        public static string Join<T>(this IList<T> list)
        {
            return ListUtil.JoinList(list);
        }

    }
}
