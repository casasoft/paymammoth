﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using CS.General_v3.Util;

namespace CS.General_v3.Extensions
{
    public static class EnumerableExtensions
    {
        

        public static IEnumerable<T> RandomizeEnumerable<T>(this IEnumerable<T> list)
        {
            return CS.General_v3.Util.ListUtil.RandomizeEnumerable(list);


        }

        public static T GetRandomItem<T>(this IEnumerable<T> list)
        {
            return CS.General_v3.Util.ListUtil.GetRandomElement(list);
        }

        /// <summary>
        /// Sorts the list and returns a new one
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="comparers"></param>
        /// <returns></returns>
        public static IEnumerable<T> SortByMultipleComparers<T>(this IEnumerable<T> list, params Comparison<T>[] comparers)
        {
            
            
            return CS.General_v3.Util.ListUtil.SortByMultipleComparers(list, comparers);
            

        }
        public static bool IsNullOrEmpty(this IEnumerable enumerable)
        {
            return !IsNotNullOrEmpty(enumerable);
        }
        public static bool IsNotNullOrEmpty(this IEnumerable enumerable)
        {
            bool ok = false;
            if (enumerable != null)
            {
                
                var enumerator= enumerable.GetEnumerator();
                if (enumerator.MoveNext()) // it means there are zero items
                    ok = true;
            }
            return ok;
        }

        public static T Find<T>(this IEnumerable<T> list, Predicate<T> match)
        {
            return list.ToList().Find(match);
        }
        public static IEnumerable<T> FindAll<T>(this IEnumerable<T> list, Predicate<T> match)
        {
            return list.ToList().FindAll(match);
        }

        public static IEnumerable<TOutput> ConvertAll<TInput,TOutput>(this IEnumerable<TInput> enumerable, Converter<TInput, TOutput> converter)
        {
            return ListUtil.ConvertAll<TInput, TOutput>(enumerable, converter);
            
        }
        public static IEnumerable<T> FilterDistinctItems<T>(this IEnumerable<T> enumerable, Func<T, T, bool> equalityComparer)
        {
            var list = enumerable.ToList();
            list.FilterDistinctItemsInList(equalityComparer); ;
            return list;
        }

    
        public static IEnumerable<TOutput> ConvertAllByCasting<TOutput>(this IEnumerable enumerable)
        {
            List<TOutput> list = new List<TOutput>();
            foreach (var obj in enumerable)
            {
                list.Add((TOutput)obj);
            }
            return list;
        }

        public new static bool Contains<T>(this IEnumerable<T> enumerable, Func<T,bool> comparer)
        {   
            foreach (var obj in enumerable)
            {
                if (comparer(obj))
                {
                    return true;
                }
            }
            return false;
        }

    }
}
