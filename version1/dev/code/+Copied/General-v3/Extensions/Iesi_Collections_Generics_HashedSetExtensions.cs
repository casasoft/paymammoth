﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Iesi.Collections.Generic;

namespace CS.General_v3.Extensions
{
    public static  class Iesi_Collections_Generics_HashedSetExtensions
    {
        public static void AddAll<T>(this HashedSet<T> set, IEnumerable<T> list)
        {
            if (list.IsNotNullOrEmpty())
            {
                set.AddAll(list.ToList());
            }
        }
    }
}
