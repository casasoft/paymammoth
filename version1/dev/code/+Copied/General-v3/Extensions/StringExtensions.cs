﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace CS.General_v3.Extensions
{
    public static class StringExtensions
    {

        public static bool EqualsToCaseInsensitive(this string s1, string s2)
        {
            return (string.Compare(s1, s2, true) == 0);
                
        }

        /// <summary>
        /// Sorts the list and returns a new one
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="comparers"></param>
        /// <returns></returns>
        public static bool IsNotNullOrEmpty(this string s)
        {
            return !string.IsNullOrEmpty(s);
        }
        /// <summary>
        /// Sorts the list and returns a new one
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="comparers"></param>
        /// <returns></returns>
        public static bool IsNotNullOrWhitespace(this string s)
        {
            return !string.IsNullOrWhiteSpace(s);
        }
        public static bool IsNullOrEmpty(this string s)
        {
            return string.IsNullOrEmpty(s);
            
        }
        public static bool IsNullOrWhitespace(this string s)
        {
            return string.IsNullOrWhiteSpace(s);

        }
        public static bool ContainsCaseInsensitive(this string s, string subString)
        {
            string cmp = s ?? "";
            cmp = cmp.ToLower();
            subString = subString.ToLower();
            if (cmp.Contains(subString))
            {
                return true;
            }
            return false;
        }

    }
}
