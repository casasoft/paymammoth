﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Classes.Interfaces;
using PayMammoth.PaymentMethods.PayPal.v1;
using PayMammoth.PaymentMethods.Cheque;
using PayMammoth.Connector.Classes.Interfaces;

namespace PayMammoth.Util
{
    public static class PaymentUtil
    {
        public static IPaymentRedirector GetRedirectorBasedOnPaymentMethod(PayMammoth.Connector.Enums.PaymentMethodSpecific method)
        {
            switch (method)
            {
                case PayMammoth.Connector.Enums.PaymentMethodSpecific.PayPal_ExpressCheckout: return new PayPalRedirector();
                case PayMammoth.Connector.Enums.PaymentMethodSpecific.PaymentGateway_Transactium: return new PaymentMethods.PaymentGateways.Transactium.v1.TransactiumRedirector();
                case Connector.Enums.PaymentMethodSpecific.PaymentGateway_Realex: return new PaymentMethods.PaymentGateways.Realex.v1.RealexRedirector();
                case Connector.Enums.PaymentMethodSpecific.PaymentGateway_Apco: return new PaymentMethods.PaymentGateways.Apco.v1.ApcoPaymentRedirector();
                case PayMammoth.Connector.Enums.PaymentMethodSpecific.Cheque: return new ChequeRedirector();
                default:
                    throw new InvalidOperationException("GetRedirectorBasedOnPaymentMethod() - Payment Method not yet implemented");
            }
            return null;
        }


        public static bool CheckIfPaymentMethodRequiresManualPayment(PayMammoth.Connector.Enums.PaymentMethodSpecific method)
        {
            bool ok = false;
            switch (method)
            {
                case PayMammoth.Connector.Enums.PaymentMethodSpecific.Cheque:
                case Connector.Enums.PaymentMethodSpecific.BankTransfer:
                    ok = true;
                    break;
                    
            }
            return ok;
            
        }
    }
}
