﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using PayMammoth.Connector.Classes.Interfaces;
using PayMammoth.Modules.PaymentRequestModule;

namespace PayMammoth.Util
{
    public static class LogUtil
    {
        //public static void LogPaymentRequest(object sender, IPaymentRequest paymentDetails, CS.General_v3.Enums.LOG4NET_MSG_TYPE msgType, string msg, Exception ex = null)
        //{

        //    var logger = CS.General_v3.Util.Log4NetUtil.GetLoggerForObject(sender);

        //    LogPaymentRequest(logger,paymentDetails, msgType, msg, ex);

        //}
        
        
        public static string AddRequestIdentifierToLogMsg(IPaymentRequest paymentDetails, string msg)
        {
            string identifier = null;
            if (paymentDetails != null)
                identifier = paymentDetails.Identifier;
            else
                identifier = "N/A";
            return AddRequestIdentifierToLogMsg(identifier, msg);

        }
        public static string AddRequestIdentifierToLogMsg(string paymentRequestIdentifier, string msg)
        {
            msg = "[" + paymentRequestIdentifier + "] " + msg;
            return msg;
            
        }

       

        public static void Log2(this ILog log, CS.General_v3.Enums.LOG4NET_MSG_TYPE msgType, string msg, Exception ex = null)
        {
            CS.General_v3.Util.Log4NetUtil.LogMsg(log, msgType, msg, ex);
        }
    }
}
