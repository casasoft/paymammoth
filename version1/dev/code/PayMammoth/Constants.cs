﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth
{
    public static class Constants
    {
        


        

        public const string PARAM_SUCCESS = "success";









        /// <summary>
        /// Returns the list of interval try counts in seconds
        /// </summary>
        /// <returns></returns>
        public static List<int> GetPaymentNotificationsRetryCounts()
        {
            List<int> list = new List<int>();
            list.Add(5);
            list.Add(5);
            list.Add(5);
            list.Add(15);
            list.Add(30);
            list.Add(60);
            list.Add(120);
            list.Add(200);
            list.Add(300);
            list.Add(300);
            list.Add(300);
            list.Add(300);
            list.Add(600);
            list.Add(600);
            list.Add(600);
            list.Add(600);
            list.Add(600);
            list.Add(1200);
            list.Add(1200);
            list.Add(1200);
            list.Add(1200);
            list.Add(1200);
            list.Add(2400);
            list.Add(2400);
            list.Add(2400);
            list.Add(4800);
            list.Add(4800);
            list.Add(4800);
            list.Add(9600);
            list.Add(9600);
            list.Add(9600);
            list.Add(19200);
            list.Add(19200);
            list.Add(38400);
            list.Add(38400);
            return list;

        }

        /// <summary>
        /// Interval to wait between each notification retry, in millisec
        /// </summary>
        public static int PAYMENT_NOTIFICATION_WAIT_BETWEEN_RETRY { get { return 5000; } }





        
    }
}
