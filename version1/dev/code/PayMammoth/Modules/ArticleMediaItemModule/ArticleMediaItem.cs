using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using NHibernate;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;


namespace PayMammoth.Modules.ArticleMediaItemModule
{

//UserClass-File

    public abstract class ArticleMediaItem : PayMammoth.Modules._AutoGen.ArticleMediaItem_AutoGen, PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem
    {

		#region UserClass-AutoGenerated
		#endregion

      
      
        public ArticleMediaItem()
        {
            
        }    
    	protected override void initPropertiesForNewItem()
        {
        	//Fill any default values for NEW items.
        
            base.initPropertiesForNewItem();
        }
        public override string ToString()
        {
            return base.ToString();
        }
        
    }
}




