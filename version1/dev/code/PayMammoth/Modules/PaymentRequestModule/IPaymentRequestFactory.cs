using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules.PaymentRequestModule
{   

//IUserFactory-File

	public interface IPaymentRequestFactory : PayMammoth.Modules._AutoGen.IPaymentRequestFactoryAutoGen
    {

        IPaymentRequest GetRequestByIdentifier(string identifier);

      //  IPaymentRequest WaitUntilRequestIsMarkedAsPaid(IPaymentRequest request);
    }

}
