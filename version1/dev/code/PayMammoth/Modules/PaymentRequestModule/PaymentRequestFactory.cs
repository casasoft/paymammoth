using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using log4net;
using PayMammoth.Util;



namespace PayMammoth.Modules.PaymentRequestModule
{

//UserFactory-File

    public class PaymentRequestFactory : PayMammoth.Modules._AutoGen.PaymentRequestFactoryAutoGen, IPaymentRequestFactory
    {

        private ILog _log = LogManager.GetLogger(typeof(PaymentRequestFactory));
		
            
       private PaymentRequestFactory()
  	   {
    	   
   	   }

       public PaymentRequest GetRequestByIdentifier(string identifier)
       {
           var q = GetQuery();
           q = q.Where(x => x.Identifier == identifier);
           return FindItem(q);
       }




       //public PaymentRequest WaitUntilRequestIsMarkedAsPaid(PaymentRequest request)
       //{
       //    int timeoutInMilleSec = CS.General_v3.Settings.GetSettingFromDatabaseByGenericEnum<int>(PayMammoth.Enums.PAYMAMMOTH_SETTINGS.Other_Timeouts_PaymentSuccessNotificationAcknowledge);
       //    long pKey = request.ID;
           
       //  //  session.Flush();
       //    PaymentRequest curr = null;
       //    curr = GetByPrimaryKey(pKey);

       //    int totalWait = 0;
       //    int waitInterval = 1000;
       //    while (curr != null && curr.CheckIfPaid() == false && totalWait < timeoutInMilleSec) 
       //    {
       //        totalWait += waitInterval;
       //        System.Threading.Thread.Sleep(waitInterval);

       //        var session = BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();
       //        curr = GetByPrimaryKey(pKey);
       //        BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();
       //    }
           
       //    var currSession = BusinessLogic_v3.Util.nHibernateUtil.GetCurrentSessionFromContext();
       //    currSession.Merge(curr);

       //    if (curr != null && !curr.CheckIfPaid())
       //    {
       //        _log.Debug(LogUtil.AddRequestIdentifierToLogMsg( request, "WaitUntilRequestIsMarkedAsPaid [" + request.Identifier + "] - Not marked as paid"));
       //    }

       //    return curr;
           
       //}

       #region IPaymentRequestFactory Members

       IPaymentRequest IPaymentRequestFactory.GetRequestByIdentifier(string identifier)
       {
           return this.GetRequestByIdentifier(identifier);
           
       }


       #endregion

        /// <summary>
        /// Waits until the request notification is received.
        /// </summary>
        /// <param name="paymentRequest"></param>
        /// <returns></returns>
        public bool WaitUntilPaymentNotificationAcknowledgementReceived(long pKey)
        {
            bool received = false;
            ISession session = (ISession) BusinessLogic_v3.Util.nHibernateUtil.GetCurrentSessionFromContext();
            session.Flush();
            
            int timeoutInMilleSec = CS.General_v3.Settings.GetSettingFromDatabaseByGenericEnum<int>(PayMammoth.Enums.PAYMAMMOTH_SETTINGS.Other_Timeouts_PaymentSuccessNotificationAcknowledge);
            

            //  session.Flush();
            PaymentRequest curr = null;
            

            int totalWait = 0;
            int waitInterval = 1000;
            bool firstTime = true;
            do
            {
                if (!firstTime)
                {
                    totalWait += waitInterval;
                    System.Threading.Thread.Sleep(waitInterval);
                }

                var checkSession = BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();

                curr = GetByPrimaryKey(pKey);
                BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();
                
                
                firstTime = false;
            } while (curr != null && curr.CheckIfPaymentNotificationAcknowledged() == false && totalWait < timeoutInMilleSec);
            if (curr != null && curr.CheckIfPaymentNotificationAcknowledged())
            {
                received = true;
            }
            //var currSession = BusinessLogic_v3.Util.nHibernateUtil.GetCurrentSessionFromContext();
//            currSession.Merge(curr);
            if (curr != null && !curr.CheckIfPaymentNotificationAcknowledged())
            {
                _log.Debug(LogUtil.AddRequestIdentifierToLogMsg( curr, "WaitUntilRequestNotificationReceived [" + curr.Identifier + "] - Not marked as paid"));
            }

            return received;
        }

        public  IEnumerable<PaymentRequest> GetPaymentRequestsThatRequireNotificationsToBeSent(int pageNum, int pageSize = 100)
        {
            IEnumerable<PaymentRequest> result = null;
            int maxRetryCount = Constants.GetPaymentNotificationsRetryCounts().Count;
            using (var t = beginTransaction())
            {
                
                var q = GetQuery();
                
                
                q = q.Where(x => x.PaymentNotificationEnabled &&  //notification must be enabled
                    x.Paid &&
                    x.PaymentNotificationRetryCount < maxRetryCount &&  //retry count not over
                    !x.PaymentNotificationAcknowledged);//and not already acknowledged);
                BusinessLogic_v3.Util.nHibernateUtil.LimitQueryByPrimaryKeys(q, pageNum, pageSize);
                result = FindAll(q);

            }
            return result;
        }
    }
}
