using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules.PaymentMethodModule
{   

//IUserFactory-File

	public interface IPaymentMethodFactory : PayMammoth.Modules._AutoGen.IPaymentMethodFactoryAutoGen
    {

        IPaymentMethod GetPaymentMethodByIdentifier(PayMammoth.Connector.Enums.PaymentMethodSpecific method);

    }

}
