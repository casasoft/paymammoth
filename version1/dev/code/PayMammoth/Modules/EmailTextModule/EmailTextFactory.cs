using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;



namespace PayMammoth.Modules.EmailTextModule
{

//UserFactory-File

    public class EmailTextFactory : PayMammoth.Modules._AutoGen.EmailTextFactoryAutoGen, IEmailTextFactory
    {
        public IEmailText GetPayMammothEmail(Enums.EmailIdentifierPayMammoth identifier)
        {
            return (EmailText)base.getByIdentifier(identifier);
        }


        private EmailTextFactory()
  	   {
    	   
   	   }

    }
}
