using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using PayMammoth.Modules._AutoGen;


namespace PayMammoth.Modules.PaymentRequestItemDetailModule
{

//IUserClass-File
	
    public interface IPaymentRequestItemDetail : PayMammoth.Modules._AutoGen.IPaymentRequestItemDetailAutoGen, PayMammoth.Connector.Classes.Requests.IItemDetail
    {
    
    }
}
