using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;



namespace PayMammoth.Modules.WebsiteAccountModule
{

//UserFactory-File

    public class WebsiteAccountFactory : PayMammoth.Modules._AutoGen.WebsiteAccountFactoryAutoGen, IWebsiteAccountFactory
    {   
    
		
            
       private WebsiteAccountFactory()
  	   {
    	   
   	   }


       public WebsiteAccount GetAccountByCode(string code)
       {
           return getAccountsByCode(code).FirstOrDefault();
           
           
       }
       internal List<WebsiteAccount> getAccountsByCode(string code)
       {
           var q = GetQuery();
           q.Where(x => x.Code == code);
           return FindAll(q).ToList();

       }

    }
}
