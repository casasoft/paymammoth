using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.Category_CultureInfoModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class Category_CultureInfoMap_AutoGen : Category_CultureInfoMap_AutoGen_Z
    {
        public Category_CultureInfoMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]

			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.CultureInfo, CultureInfoMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.Category, CategoryMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.TitleSingular, TitleSingularMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.TitleSingular_Search, TitleSingular_SearchMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.TitlePlural, TitlePluralMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.TitlePlural_Search, TitlePlural_SearchMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.TitleSEO, TitleSEOMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Description, DescriptionMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.MetaKeywords, MetaKeywordsMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.MetaDescription, MetaDescriptionMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class Category_CultureInfoMap_AutoGen_Z : BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBaseMap<Category_CultureInfoImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
