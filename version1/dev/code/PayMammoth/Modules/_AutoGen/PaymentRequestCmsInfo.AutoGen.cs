using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Frontend.PaymentRequestModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class PaymentRequestCmsInfo_AutoGen : PayMammoth.Modules._AutoGen.PaymentRequestBaseCmsInfo
    {

		public PaymentRequestCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.PaymentRequestBase item)
            : base(item)
        {

        }
        

        private PaymentRequestFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = PaymentRequestFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new PaymentRequestFrontend FrontendItem
        {
            get { return (PaymentRequestFrontend) base.FrontendItem; }
        }
        
        
		public new PaymentRequest DbItem
        {
            get
            {
                return (PaymentRequest)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]

        public CmsCollectionInfo ItemDetails { get; protected set; }
        

        public CmsCollectionInfo Transactions { get; protected set; }
        



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.Identifier = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.Identifier),
        						isEditable: true, isVisible: true);

        this.DateTime = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.DateTime),
        						isEditable: true, isVisible: true);

        this.RequestIP = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.RequestIP),
        						isEditable: true, isVisible: true);

        this.ClientFirstName = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.ClientFirstName),
        						isEditable: true, isVisible: true);

        this.ClientMiddleName = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.ClientMiddleName),
        						isEditable: true, isVisible: true);

        this.ClientLastName = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.ClientLastName),
        						isEditable: true, isVisible: true);

        this.ClientAddress1 = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.ClientAddress1),
        						isEditable: true, isVisible: true);

        this.ClientAddress2 = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.ClientAddress2),
        						isEditable: true, isVisible: true);

        this.ClientAddress3 = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.ClientAddress3),
        						isEditable: true, isVisible: true);

        this.ClientLocality = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.ClientLocality),
        						isEditable: true, isVisible: true);

        this.ClientCountry = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.ClientCountry),
        						isEditable: true, isVisible: true);

        this.ClientPostCode = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.ClientPostCode),
        						isEditable: true, isVisible: true);

        this.Description = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.Description),
        						isEditable: true, isVisible: true);

        this.ClientEmail = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.ClientEmail),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.WebsiteAccount = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.WebsiteAccount),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaymentRequests)));

        
        this.TaxAmount = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.TaxAmount),
        						isEditable: true, isVisible: true);

        this.HandlingAmount = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.HandlingAmount),
        						isEditable: true, isVisible: true);

        this.ShippingAmount = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.ShippingAmount),
        						isEditable: true, isVisible: true);

        this.Reference = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.Reference),
        						isEditable: true, isVisible: true);

        this.Notes = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.Notes),
        						isEditable: true, isVisible: true);

        this.PriceExcTax = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.PriceExcTax),
        						isEditable: true, isVisible: true);

        this.CurrencyCode = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.CurrencyCode),
        						isEditable: true, isVisible: true);

        this.OrderLinkOnClientWebsite = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.OrderLinkOnClientWebsite),
        						isEditable: true, isVisible: true);

        this.Title = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.Title),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.CurrentTransaction = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.CurrentTransaction),
                null));

        
        this.Paid = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.Paid),
        						isEditable: true, isVisible: true);

        this.PaidOn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.PaidOn),
        						isEditable: true, isVisible: true);

        this.PaidByPaymentMethod = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.PaidByPaymentMethod),
        						isEditable: true, isVisible: true);

        this.ReturnUrlSuccess = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.ReturnUrlSuccess),
        						isEditable: true, isVisible: true);

        this.ReturnUrlFailure = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.ReturnUrlFailure),
        						isEditable: true, isVisible: true);

        this.LanguageCode = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.LanguageCode),
        						isEditable: true, isVisible: true);

        this.LanguageCountryCode = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.LanguageCountryCode),
        						isEditable: true, isVisible: true);

        this.LanguageSuffix = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.LanguageSuffix),
        						isEditable: true, isVisible: true);

        this.ClientTelephone = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.ClientTelephone),
        						isEditable: true, isVisible: true);

        this.ClientMobile = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.ClientMobile),
        						isEditable: true, isVisible: true);

        this.ClientTitle = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.ClientTitle),
        						isEditable: true, isVisible: true);

        this.ClientIp = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.ClientIp),
        						isEditable: true, isVisible: true);

        this.ClientReference = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.ClientReference),
        						isEditable: true, isVisible: true);

        this.PaymentNotificationAcknowledged = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.PaymentNotificationAcknowledged),
        						isEditable: true, isVisible: true);

        this.RequiresManualIntervention = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.RequiresManualIntervention),
        						isEditable: true, isVisible: true);

        this.PaymentNotificationAcknowledgedOn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.PaymentNotificationAcknowledgedOn),
        						isEditable: true, isVisible: true);

        this.PaymentNotificationRetryCount = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.PaymentNotificationRetryCount),
        						isEditable: true, isVisible: true);

        this.PaymentNotificationEnabled = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.PaymentNotificationEnabled),
        						isEditable: true, isVisible: true);

        this.PaymentNotificationNextRetryOn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.PaymentNotificationNextRetryOn),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]

		
		//InitCollectionUserOneToMany
		this.ItemDetails = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector(item => item.ItemDetails),
						CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetail>.GetPropertyBySelector(item => item.PaymentRequest)));
		
		//InitCollectionUserOneToMany
		this.Transactions = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector(item => item.Transactions),
						CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction>.GetPropertyBySelector(item => item.PaymentRequest)));


			base.initBasicFields();
		}
    
    }
}
