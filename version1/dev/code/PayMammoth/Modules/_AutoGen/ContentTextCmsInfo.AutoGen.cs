using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.ContentTextModule;
using CS.WebComponentsGeneralV3.Cms.ContentTextModule;
using PayMammoth.Frontend.ContentTextModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class ContentTextCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.ContentTextModule.ContentTextBaseCmsInfo
    {

		public ContentTextCmsInfo_AutoGen(BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase item)
            : base(item)
        {

        }
        

        private ContentTextFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = ContentTextFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new ContentTextFrontend FrontendItem
        {
            get { return (ContentTextFrontend) base.FrontendItem; }
        }
        
        
		public new ContentText DbItem
        {
            get
            {
                return (ContentText)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.Name = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContentTextModule.ContentText>.GetPropertyBySelector( item => item.Name),
        						isEditable: true, isVisible: true);

        this.Name_Search = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContentTextModule.ContentText>.GetPropertyBySelector( item => item.Name_Search),
        						isEditable: false, isVisible: false);

        this.Identifier = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContentTextModule.ContentText>.GetPropertyBySelector( item => item.Identifier),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.Parent = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContentTextModule.ContentText>.GetPropertyBySelector( item => item.Parent),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContentTextModule.ContentText>.GetPropertyBySelector( item => item.ChildContentTexts)));

        
        this.AllowAddSubItems_AccessRequired = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContentTextModule.ContentText>.GetPropertyBySelector( item => item.AllowAddSubItems_AccessRequired),
        						isEditable: true, isVisible: true);

        this.VisibleInCMS_AccessRequired = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContentTextModule.ContentText>.GetPropertyBySelector( item => item.VisibleInCMS_AccessRequired),
        						isEditable: true, isVisible: true);

        this.Content = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContentTextModule.ContentText>.GetPropertyBySelector( item => item.Content),
        						isEditable: true, isVisible: true);

        this.ContentType = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContentTextModule.ContentText>.GetPropertyBySelector( item => item.ContentType),
        						isEditable: true, isVisible: true);

        this.UsedInProject = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContentTextModule.ContentText>.GetPropertyBySelector( item => item.UsedInProject),
        						isEditable: true, isVisible: true);

        this.ImageAlternateText = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContentTextModule.ContentText>.GetPropertyBySelector( item => item.ImageAlternateText),
        						isEditable: true, isVisible: true);

        this.CustomContentTags = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContentTextModule.ContentText>.GetPropertyBySelector( item => item.CustomContentTags),
        						isEditable: true, isVisible: true);

        this.ProcessContentUsingNVelocity = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContentTextModule.ContentText>.GetPropertyBySelector( item => item.ProcessContentUsingNVelocity),
        						isEditable: true, isVisible: true);

        this._Computed_IsHtml = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ContentTextModule.ContentText>.GetPropertyBySelector( item => item._Computed_IsHtml),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
