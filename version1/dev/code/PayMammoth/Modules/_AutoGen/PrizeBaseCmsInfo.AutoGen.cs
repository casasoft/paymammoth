using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class PrizeBaseCmsInfo_AutoGen : CmsItemInfoDbBase<PayMammoth.Modules._AutoGen.PrizeBase>
    {
		public PrizeBaseCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.PrizeBase dbItem)
            : base(PayMammoth.Modules._AutoGen.PrizeBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new PrizeBase DbItem
        {
            get
            {
                return (PrizeBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; private set; }

        public CmsPropertyInfo Description { get; private set; }

        public CmsPropertyInfo RetailValue { get; private set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Title = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.PrizeBase>.GetPropertyBySelector( item => item.Title),
        			isEditable: true,
        			isVisible: true
        			);

        this.Description = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.PrizeBase>.GetPropertyBySelector( item => item.Description),
        			isEditable: true,
        			isVisible: true
        			);

        this.RetailValue = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.PrizeBase>.GetPropertyBySelector( item => item.RetailValue),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
