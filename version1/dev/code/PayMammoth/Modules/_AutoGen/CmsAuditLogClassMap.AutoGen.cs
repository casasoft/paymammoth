using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.CmsAuditLogModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class CmsAuditLogMap_AutoGen : CmsAuditLogMap_AutoGen_Z
    {
        public CmsAuditLogMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]

			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.CmsUser, CmsUserMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DateTime, DateTimeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Message, MessageMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.FurtherInformation, FurtherInformationMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.MsgType, MsgTypeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.IPAddress, IPAddressMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ObjectName, ObjectNameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Remarks, RemarksMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ItemID, ItemIDMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class CmsAuditLogMap_AutoGen_Z : BusinessLogic_v3.Modules.CmsAuditLogModule.CmsAuditLogBaseMap<CmsAuditLogImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
