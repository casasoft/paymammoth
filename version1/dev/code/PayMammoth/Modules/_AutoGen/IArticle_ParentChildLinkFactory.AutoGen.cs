using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.Article_ParentChildLinkModule;
using PayMammoth.Modules.Article_ParentChildLinkModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IArticle_ParentChildLinkFactoryAutoGen : BusinessLogic_v3.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLinkBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IArticle_ParentChildLink>
    
    {
	    new PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink CreateNewItem();
        new PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null);
        new IEnumerable<PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
