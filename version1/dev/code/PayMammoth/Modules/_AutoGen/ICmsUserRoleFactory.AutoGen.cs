using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.CmsUserRoleModule;
using PayMammoth.Modules.CmsUserRoleModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface ICmsUserRoleFactoryAutoGen : BusinessLogic_v3.Modules.CmsUserRoleModule.ICmsUserRoleBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<ICmsUserRole>
    
    {
	    new PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole CreateNewItem();
        new PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null);
        new IEnumerable<PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
