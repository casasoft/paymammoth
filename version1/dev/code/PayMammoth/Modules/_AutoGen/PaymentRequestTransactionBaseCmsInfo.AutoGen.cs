using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen.PaymentRequestTransactionModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaymentRequestTransactionBaseCmsInfo_AutoGen : CmsItemInfoDbBase<PayMammoth.Modules._AutoGen.PaymentRequestTransactionBase>
    {
		public PaymentRequestTransactionBaseCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.PaymentRequestTransactionBase dbItem)
            : base(PayMammoth.Modules._AutoGen.PaymentRequestTransactionBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new PaymentRequestTransactionBaseFrontend FrontendItem
        {
            get { return (PaymentRequestTransactionBaseFrontend)base.FrontendItem; }

        }
        public new PaymentRequestTransactionBase DbItem
        {
            get
            {
                return (PaymentRequestTransactionBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo PaymentRequest { get; protected set; }

        public CmsPropertyInfo Successful { get; protected set; }

        public CmsPropertyInfo PaymentMethod { get; protected set; }

        public CmsPropertyInfo Comments { get; protected set; }

        public CmsPropertyInfo IP { get; protected set; }

        public CmsPropertyInfo AuthCode { get; protected set; }

        public CmsPropertyInfo StartDate { get; protected set; }

        public CmsPropertyInfo LastUpdatedOn { get; protected set; }

        public CmsPropertyInfo EndDate { get; protected set; }

        public CmsPropertyInfo Reference { get; protected set; }

        public CmsPropertyInfo TransactionInfo { get; protected set; }

        public CmsPropertyInfo PaymentParameters { get; protected set; }

        public CmsPropertyInfo PaymentMethodVersion { get; protected set; }

        public CmsPropertyInfo FakePayment { get; protected set; }

        public CmsPropertyInfo RequiresManualIntervention { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
