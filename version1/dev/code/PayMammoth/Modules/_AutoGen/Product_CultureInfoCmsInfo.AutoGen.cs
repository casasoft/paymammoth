using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.Product_CultureInfoModule;
using CS.WebComponentsGeneralV3.Cms.Product_CultureInfoModule;
using PayMammoth.Frontend.Product_CultureInfoModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class Product_CultureInfoCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.Product_CultureInfoModule.Product_CultureInfoBaseCmsInfo
    {

		public Product_CultureInfoCmsInfo_AutoGen(BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBase item)
            : base(item)
        {

        }
        

        private Product_CultureInfoFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = Product_CultureInfoFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new Product_CultureInfoFrontend FrontendItem
        {
            get { return (Product_CultureInfoFrontend) base.FrontendItem; }
        }
        
        
		public new Product_CultureInfo DbItem
        {
            get
            {
                return (Product_CultureInfo)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

//InitFieldUser_LinkedProperty
			this.CultureInfo = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo>.GetPropertyBySelector( item => item.CultureInfo),
                null));

        
//InitFieldUser_LinkedProperty
			this.Product = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo>.GetPropertyBySelector( item => item.Product),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.Product_CultureInfos)));

        
        this.Title = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo>.GetPropertyBySelector( item => item.Title),
        						isEditable: true, isVisible: true);

        this.Description = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Product_CultureInfoModule.Product_CultureInfo>.GetPropertyBySelector( item => item.Description),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
