using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.CurrencyModule;
using BusinessLogic_v3.Modules.CurrencyModule;

//DbObjectBuilder v8

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{    

	
    public abstract class Currency_AutoGen : CurrencyBase, PayMammoth.Modules._AutoGen.ICurrencyAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]




// [userclass_collections_nhibernate]

        //UserClass_CollectionOneToManyLeftSide
        
		new internal class __CultureDetailsesCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.CurrencyModule.Currency, 
        	PayMammoth.Modules.CultureDetailsModule.CultureDetails,
        	PayMammoth.Modules.CultureDetailsModule.ICultureDetails>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.CurrencyModule.Currency, PayMammoth.Modules.CultureDetailsModule.CultureDetails>
        {
            private PayMammoth.Modules.CurrencyModule.Currency _item = null;
            public __CultureDetailsesCollectionInfo(PayMammoth.Modules.CurrencyModule.Currency item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.CultureDetailsModule.CultureDetails> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.CultureDetailsModule.CultureDetails>)_item.__collection__CultureDetailses; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.CurrencyModule.Currency, PayMammoth.Modules.CultureDetailsModule.CultureDetails>.SetLinkOnItem(PayMammoth.Modules.CultureDetailsModule.CultureDetails item, PayMammoth.Modules.CurrencyModule.Currency value)
            {
                item.DefaultCurrency = value;
            }
        }
        
        private __CultureDetailsesCollectionInfo _CultureDetailses = null;
        internal new __CultureDetailsesCollectionInfo CultureDetailses
        {
            get
            {
                if (_CultureDetailses == null)
                    _CultureDetailses = new __CultureDetailsesCollectionInfo((PayMammoth.Modules.CurrencyModule.Currency)this);
                return _CultureDetailses;
            }
        }               
           
		ICollectionManager<PayMammoth.Modules.CultureDetailsModule.ICultureDetails> ICurrencyAutoGen.CultureDetailses
        {
            get {  return this.CultureDetailses; }
        }
           

        
          
		public  static new CurrencyFactory Factory
        {
            get
            {
                return (CurrencyFactory)CurrencyBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<Currency, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
