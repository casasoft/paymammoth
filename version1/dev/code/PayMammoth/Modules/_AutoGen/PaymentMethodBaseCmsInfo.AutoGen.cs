using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen.PaymentMethodModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaymentMethodBaseCmsInfo_AutoGen : CmsItemInfoDbBase<PayMammoth.Modules._AutoGen.PaymentMethodBase>
    {
		public PaymentMethodBaseCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.PaymentMethodBase dbItem)
            : base(PayMammoth.Modules._AutoGen.PaymentMethodBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new PaymentMethodBaseFrontend FrontendItem
        {
            get { return (PaymentMethodBaseFrontend)base.FrontendItem; }

        }
        public new PaymentMethodBase DbItem
        {
            get
            {
                return (PaymentMethodBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo PaymentMethod { get; protected set; }

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }

        public CmsPropertyInfo ImageFilename { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
