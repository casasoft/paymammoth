using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule;
using PayMammoth.Modules.ProductCategoryFeatureValueModule;
using PayMammoth.Modules.CultureDetailsModule;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class ProductCategoryFeatureValueFactoryAutoGen : BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBaseFactory, 
    				PayMammoth.Modules._AutoGen.IProductCategoryFeatureValueFactoryAutoGen,
    				CS.General_v3.Classes.DbObjects.IBaseDbFactory<IProductCategoryFeatureValue>
    
    {
    
     	public new ProductCategoryFeatureValue ReloadItemFromDatabase(BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBase item)
        {
         	return (ProductCategoryFeatureValue)base.ReloadItemFromDatabase(item);
        }
     	
        public ProductCategoryFeatureValueFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(ProductCategoryFeatureValueImpl);
			PayMammoth.Cms.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueCmsFactory._initialiseStaticInstance();
        }
        public static new ProductCategoryFeatureValueFactory Instance
        {
            get
            {
                return (ProductCategoryFeatureValueFactory)ProductCategoryFeatureValueBaseFactory.Instance;
            }
      
      

        }
        
        /*
        public new IEnumerable<ProductCategoryFeatureValue> FindAll()
        {
            return base.FindAll().Cast<ProductCategoryFeatureValue>();
        }*/
    /*    public new IEnumerable<ProductCategoryFeatureValue> FindAll(IQuery query)
        {
            
            return base.FindAll(query).Cast<ProductCategoryFeatureValue>();

        }*/
        public new IEnumerable<ProductCategoryFeatureValue> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<ProductCategoryFeatureValue>();
            
        }
        public new IEnumerable<ProductCategoryFeatureValue> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<ProductCategoryFeatureValue>();
            
            
        }
     /*  public new IEnumerable<ProductCategoryFeatureValue> FindAll(ICriteria crit)
        {
            return base.FindAll(crit).Cast<ProductCategoryFeatureValue>();


        }*/
        public new IEnumerable<ProductCategoryFeatureValue> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<ProductCategoryFeatureValue>();
            
        }
        public new IEnumerable<ProductCategoryFeatureValue> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<ProductCategoryFeatureValue>();
        }
        
        public new ProductCategoryFeatureValue FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (ProductCategoryFeatureValue)base.FindItem(query);
        }
        public new ProductCategoryFeatureValue FindItem(IQueryOver query)
        {
            return (ProductCategoryFeatureValue)base.FindItem(query);
        }
        
        IProductCategoryFeatureValue IProductCategoryFeatureValueFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new ProductCategoryFeatureValue CreateNewItem()
        {
            return (ProductCategoryFeatureValue)base.CreateNewItem();
        }

        public new ProductCategoryFeatureValue GetByPrimaryKey(long pKey)
        {
            return (ProductCategoryFeatureValue)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<ProductCategoryFeatureValue, ProductCategoryFeatureValue> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();

        	var q = session.QueryOver<ProductCategoryFeatureValue>();
            qParams.FillQueryOver(q);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValue GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValue) base.GetByPrimaryKey(id, session);
        }
       public new IEnumerable<PayMammoth.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValue> FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValue>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValue> FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValue>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValue> FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValue>) base.FindAll(session);

       }
       public new PayMammoth.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValue FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValue) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValue FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValue) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValue FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValue) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValue> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValue>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue> Members
     
        PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue IBaseDbFactory<PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue IBaseDbFactory<PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue>.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue> IBaseDbFactory<PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue>.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue>.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue>.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue IBaseDbFactory<PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue>.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue>.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue>.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue> IBaseDbFactory<PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IProductCategoryFeatureValue> IProductCategoryFeatureValueFactoryAutoGen.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IProductCategoryFeatureValue> IProductCategoryFeatureValueFactoryAutoGen.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IProductCategoryFeatureValue> IProductCategoryFeatureValueFactoryAutoGen.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IProductCategoryFeatureValue IProductCategoryFeatureValueFactoryAutoGen.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IProductCategoryFeatureValue IProductCategoryFeatureValueFactoryAutoGen.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IProductCategoryFeatureValue IProductCategoryFeatureValueFactoryAutoGen.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IProductCategoryFeatureValue> IProductCategoryFeatureValueFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IProductCategoryFeatureValue IProductCategoryFeatureValueFactoryAutoGen.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
