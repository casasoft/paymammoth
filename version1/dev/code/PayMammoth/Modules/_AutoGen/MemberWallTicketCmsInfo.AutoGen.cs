using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using CS.General_v3.Extensions;
using PayMammoth.Modules.MemberWallTicketModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public class MemberWallTicketCmsInfo_AutoGen : PayMammoth.Modules._AutoGen.MemberWallTicketBaseCmsInfo
    {

		public MemberWallTicketCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.MemberWallTicketBase item)
            : base(item)
        {

        }
		public new MemberWallTicket DbItem
        {
            get
            {
                return (MemberWallTicket)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
