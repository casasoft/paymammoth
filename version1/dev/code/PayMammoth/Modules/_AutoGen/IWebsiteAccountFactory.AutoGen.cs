using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.WebsiteAccountModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IWebsiteAccountFactoryAutoGen : PayMammoth.Modules._AutoGen.IWebsiteAccountBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IWebsiteAccount>
    
    {
	    new PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount CreateNewItem();
        new PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null);
        new IEnumerable<PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
