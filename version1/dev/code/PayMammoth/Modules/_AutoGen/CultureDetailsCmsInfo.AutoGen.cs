using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.CultureDetailsModule;
using CS.WebComponentsGeneralV3.Cms.CultureDetailsModule;
using PayMammoth.Frontend.CultureDetailsModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class CultureDetailsCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.CultureDetailsModule.CultureDetailsBaseCmsInfo
    {

		public CultureDetailsCmsInfo_AutoGen(BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase item)
            : base(item)
        {

        }
        

        private CultureDetailsFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = CultureDetailsFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new CultureDetailsFrontend FrontendItem
        {
            get { return (CultureDetailsFrontend) base.FrontendItem; }
        }
        
        
		public new CultureDetails DbItem
        {
            get
            {
                return (CultureDetails)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.Title = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CultureDetailsModule.CultureDetails>.GetPropertyBySelector( item => item.Title),
        						isEditable: true, isVisible: true);

        this.LanguageISOCode = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CultureDetailsModule.CultureDetails>.GetPropertyBySelector( item => item.LanguageISOCode),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.DefaultCurrency = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CultureDetailsModule.CultureDetails>.GetPropertyBySelector( item => item.DefaultCurrency),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CurrencyModule.Currency>.GetPropertyBySelector( item => item.CultureDetailses)));

        
        this.SpecificCountryCode = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CultureDetailsModule.CultureDetails>.GetPropertyBySelector( item => item.SpecificCountryCode),
        						isEditable: true, isVisible: true);

        this.ScriptSuffix = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CultureDetailsModule.CultureDetails>.GetPropertyBySelector( item => item.ScriptSuffix),
        						isEditable: true, isVisible: true);

        this.IsDefaultCulture = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CultureDetailsModule.CultureDetails>.GetPropertyBySelector( item => item.IsDefaultCulture),
        						isEditable: true, isVisible: true);

        this.BaseUrlRegex = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CultureDetailsModule.CultureDetails>.GetPropertyBySelector( item => item.BaseUrlRegex),
        						isEditable: true, isVisible: true);

        this.BaseRedirectionUrl = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CultureDetailsModule.CultureDetails>.GetPropertyBySelector( item => item.BaseRedirectionUrl),
        						isEditable: true, isVisible: true);

        this.GoogleAnalyticsRootDomain = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CultureDetailsModule.CultureDetails>.GetPropertyBySelector( item => item.GoogleAnalyticsRootDomain),
        						isEditable: true, isVisible: true);

        this.GoogleAnalyticsID = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CultureDetailsModule.CultureDetails>.GetPropertyBySelector( item => item.GoogleAnalyticsID),
        						isEditable: true, isVisible: true);

        this.BaseRedirectionUrlLocalhost = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CultureDetailsModule.CultureDetails>.GetPropertyBySelector( item => item.BaseRedirectionUrlLocalhost),
        						isEditable: true, isVisible: true);

        this.BaseUrlRegexLocalhost = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CultureDetailsModule.CultureDetails>.GetPropertyBySelector( item => item.BaseUrlRegexLocalhost),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
