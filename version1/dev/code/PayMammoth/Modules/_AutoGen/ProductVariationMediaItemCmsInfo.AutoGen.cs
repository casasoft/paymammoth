using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.ProductVariationMediaItemModule;
using CS.WebComponentsGeneralV3.Cms.ProductVariationMediaItemModule;
using PayMammoth.Frontend.ProductVariationMediaItemModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class ProductVariationMediaItemCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.ProductVariationMediaItemModule.ProductVariationMediaItemBaseCmsInfo
    {

		public ProductVariationMediaItemCmsInfo_AutoGen(BusinessLogic_v3.Modules.ProductVariationMediaItemModule.ProductVariationMediaItemBase item)
            : base(item)
        {

        }
        

        private ProductVariationMediaItemFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = ProductVariationMediaItemFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new ProductVariationMediaItemFrontend FrontendItem
        {
            get { return (ProductVariationMediaItemFrontend) base.FrontendItem; }
        }
        
        
		public new ProductVariationMediaItem DbItem
        {
            get
            {
                return (ProductVariationMediaItem)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

//InitFieldUser_LinkedProperty
			this.ProductVariation = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem>.GetPropertyBySelector( item => item.ProductVariation),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductVariationModule.ProductVariation>.GetPropertyBySelector( item => item.MediaItems)));

        
        this.ImageFilename = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem>.GetPropertyBySelector( item => item.ImageFilename),
        						isEditable: false, isVisible: false);

        this.Caption = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem>.GetPropertyBySelector( item => item.Caption),
        						isEditable: true, isVisible: true);

        this.ExtraValueChoice = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem>.GetPropertyBySelector( item => item.ExtraValueChoice),
        						isEditable: true, isVisible: true);

        this.Title = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem>.GetPropertyBySelector( item => item.Title),
        						isEditable: true, isVisible: true);

        this.Reference = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem>.GetPropertyBySelector( item => item.Reference),
        						isEditable: true, isVisible: true);

        this.ImportReference = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem>.GetPropertyBySelector( item => item.ImportReference),
        						isEditable: true, isVisible: true);

        this.Size = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem>.GetPropertyBySelector( item => item.Size),
        						isEditable: true, isVisible: true);

        this.Colour = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem>.GetPropertyBySelector( item => item.Colour),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
