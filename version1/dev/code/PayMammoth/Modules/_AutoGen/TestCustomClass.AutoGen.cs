using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using CS.General_v3.Classes.DbObjects.Collections;
using CS.General_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.TestCustomClassModule;
using PayMammoth.Modules._AutoGen;

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
    public abstract class TestCustomClass_AutoGen : TestCustomClassBase, PayMammoth.Modules._AutoGen.ITestCustomClassAutoGen
	
      
      
    {
        


    
    
    
		
        



// [userclass_linkedproperties]




// [userclass_collections_nhibernate]


        
          
		public  static new TestCustomClassFactory Factory
        {
            get
            {
                return (TestCustomClassFactory)TestCustomClassBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<TestCustomClass, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        //UserClassOnly_CollectionOneToManyLeftSide
        
        
        public class __ArticlesCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.TestCustomClassModule.TestCustomClass, 
        	PayMammoth.Modules.ArticleModule.Article,
        	PayMammoth.Modules.ArticleModule.IArticle>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.TestCustomClassModule.TestCustomClass, PayMammoth.Modules.ArticleModule.Article>
        {
            private PayMammoth.Modules.TestCustomClassModule.TestCustomClass _item = null;
            public __ArticlesCollectionInfo(PayMammoth.Modules.TestCustomClassModule.TestCustomClass item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.ArticleModule.Article> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.ArticleModule.Article>)_item.__collection__Articles; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.TestCustomClassModule.TestCustomClass, PayMammoth.Modules.ArticleModule.Article>.SetLinkOnItem(PayMammoth.Modules.ArticleModule.Article item, PayMammoth.Modules.TestCustomClassModule.TestCustomClass value)
            {
                item.CustomProjOnlyLinkedObject = value;
            }
        }
        
        private __ArticlesCollectionInfo _Articles = null;
        public __ArticlesCollectionInfo Articles
        {
            get
            {
                if (_Articles == null)
                    _Articles = new __ArticlesCollectionInfo((PayMammoth.Modules.TestCustomClassModule.TestCustomClass)this);
                return _Articles;
            }
        }

   
		ICollectionManager<PayMammoth.Modules.ArticleModule.IArticle> ITestCustomClassAutoGen.Articles
        {
            get {  return this.Articles; }
        }
   
        protected virtual IEnumerable<BusinessLogic_v3.Modules.ArticleModule.ArticleBase> __collection__Articles
        {
            get { throw new InvalidOperationException("This must be defined in the final implementation"); }
        }
        
        
        
    }
    
}
