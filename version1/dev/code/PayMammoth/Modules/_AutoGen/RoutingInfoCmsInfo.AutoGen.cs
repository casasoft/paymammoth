using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.RoutingInfoModule;
using CS.WebComponentsGeneralV3.Cms.RoutingInfoModule;
using PayMammoth.Frontend.RoutingInfoModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class RoutingInfoCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.RoutingInfoModule.RoutingInfoBaseCmsInfo
    {

		public RoutingInfoCmsInfo_AutoGen(BusinessLogic_v3.Modules.RoutingInfoModule.RoutingInfoBase item)
            : base(item)
        {

        }
        

        private RoutingInfoFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = RoutingInfoFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new RoutingInfoFrontend FrontendItem
        {
            get { return (RoutingInfoFrontend) base.FrontendItem; }
        }
        
        
		public new RoutingInfo DbItem
        {
            get
            {
                return (RoutingInfo)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.RouteName = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RoutingInfoModule.RoutingInfo>.GetPropertyBySelector( item => item.RouteName),
        						isEditable: true, isVisible: true);

        this.PhysicalPath = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RoutingInfoModule.RoutingInfo>.GetPropertyBySelector( item => item.PhysicalPath),
        						isEditable: true, isVisible: true);

        this.VirtualPath = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RoutingInfoModule.RoutingInfo>.GetPropertyBySelector( item => item.VirtualPath),
        						isEditable: true, isVisible: true);

        this.Identifier = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.RoutingInfoModule.RoutingInfo>.GetPropertyBySelector( item => item.Identifier),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
