using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          

using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using BusinessLogic_v3.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Factories;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;

namespace PayMammoth.Modules._AutoGen
{
	public abstract class PaymentMethodBaseCmsFactory_AutoGen : CmsFactoryBase<PaymentMethodBaseCmsInfo, PaymentMethodBase>
    {
       
       public new static PaymentMethodBaseCmsFactory Instance
	    {
	         get
	         {
                 return (PaymentMethodBaseCmsFactory)CmsFactoryBase<PaymentMethodBaseCmsInfo, PaymentMethodBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(GetQueryParams qParams)
        {
        	//GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted);
        	//qParams.FillDefaultsForCms();
        	
            var q = PaymentMethodBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "PaymentMethod.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "PaymentMethod";

            this.QueryStringParamID = "PaymentMethodId";

            cmsInfo.TitlePlural = "Payment Methods";

            cmsInfo.TitleSingular =  "Payment Method";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public PaymentMethodBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystem.Instance.GetCmsRoot() + "PaymentMethod/";
			UsedInProject = PaymentMethodBaseFactoryAutoGen.UsedInProject;

        }
       
    }

}
