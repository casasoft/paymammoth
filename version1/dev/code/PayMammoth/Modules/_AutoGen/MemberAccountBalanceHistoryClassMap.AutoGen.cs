using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.MemberAccountBalanceHistoryModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class MemberAccountBalanceHistoryMap_AutoGen : MemberAccountBalanceHistoryMap_AutoGen_Z
    {
        public MemberAccountBalanceHistoryMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]

			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.Member, MemberMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.BalanceUpdate, BalanceUpdateMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Date, DateMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.OrderLink, OrderLinkMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.UpdateType, UpdateTypeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.BalanceTotal, BalanceTotalMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Comments, CommentsMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class MemberAccountBalanceHistoryMap_AutoGen_Z : BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBaseMap<MemberAccountBalanceHistoryImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
