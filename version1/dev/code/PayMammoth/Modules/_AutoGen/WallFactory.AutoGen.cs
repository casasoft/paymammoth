using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.DbObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.WallModule;
using PayMammoth.Modules.CultureDetailsModule;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class WallFactoryAutoGen : PayMammoth.Modules._AutoGen.WallBaseFactory, 
    				PayMammoth.Modules._AutoGen.IWallFactoryAutoGen,
    				CS.General_v3.Classes.DbObjects.IBaseDbFactory<IWall>
    
    {
    
     	
        public WallFactoryAutoGen ()
        {

			this.UsedInProject = true;
			_mainTypeCreated = typeof(WallImpl);
			PayMammoth.Cms.WallModule.WallCmsFactory._initialiseStaticInstance();
        }
        public static new WallFactory Instance
        {
            get
            {
                return (WallFactory)WallBaseFactory.Instance;
            }
      
      

        }
        
        
        public new IEnumerable<Wall> FindAll()
        {
            return base.FindAll().Cast<Wall>();
        }
        public new IEnumerable<Wall> FindAll(IQuery query)
        {
            
            return base.FindAll(query).Cast<Wall>();

        }
        public new IEnumerable<Wall> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<Wall>();
            
        }
        public new IEnumerable<Wall> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<Wall>();
            
            
        }
        public new IEnumerable<Wall> FindAll(ICriteria crit)
        {
            return base.FindAll(crit).Cast<Wall>();


        }
        public new IEnumerable<Wall> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<Wall>();
            
        }
        public new IEnumerable<Wall> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<Wall>();
        }
        
        public new Wall FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (Wall)base.FindItem(query);
        }
        public new Wall FindItem(IQueryOver query)
        {
            return (Wall)base.FindItem(query);
        }
        
        IWall IWallFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new Wall CreateNewItem()
        {
            return (Wall)base.CreateNewItem();
        }

        public new Wall GetByPrimaryKey(long pKey)
        {
            return (Wall)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<Wall, Wall> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();

        	var q = session.QueryOver<Wall>();
            qParams.FillQueryOver(q);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public PayMammoth.Modules.WallModule.Wall GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.WallModule.Wall) base.GetByPrimaryKey(id, session);
        }
       public IEnumerable<PayMammoth.Modules.WallModule.Wall> FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.WallModule.Wall>) base.FindAll(query, session);

       }
       public IEnumerable<PayMammoth.Modules.WallModule.Wall> FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.WallModule.Wall>) base.FindAll(query, session);

       }
       public IEnumerable<PayMammoth.Modules.WallModule.Wall> FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.WallModule.Wall>) base.FindAll(session);

       }
       public PayMammoth.Modules.WallModule.Wall FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.WallModule.Wall) base.FindItem(query, session);

       }
       public PayMammoth.Modules.WallModule.Wall FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.WallModule.Wall) base.FindItem(query, session);

       }

       public PayMammoth.Modules.WallModule.Wall FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.WallModule.Wall) base.FindItem(session);

       }
       public IEnumerable<PayMammoth.Modules.WallModule.Wall> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.WallModule.Wall>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.WallModule.IWall> Members
     
        PayMammoth.Modules.WallModule.IWall IBaseDbFactory<PayMammoth.Modules.WallModule.IWall>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.WallModule.IWall IBaseDbFactory<PayMammoth.Modules.WallModule.IWall>.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.WallModule.IWall> IBaseDbFactory<PayMammoth.Modules.WallModule.IWall>.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.WallModule.IWall> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.WallModule.IWall>.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.WallModule.IWall> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.WallModule.IWall>.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.WallModule.IWall IBaseDbFactory<PayMammoth.Modules.WallModule.IWall>.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.WallModule.IWall CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.WallModule.IWall>.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.WallModule.IWall CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.WallModule.IWall>.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.WallModule.IWall> IBaseDbFactory<PayMammoth.Modules.WallModule.IWall>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IWall> IWallFactoryAutoGen.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IWall> IWallFactoryAutoGen.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IWall> IWallFactoryAutoGen.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(session);
           
       }

       IWall IWallFactoryAutoGen.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(query, session);
           
       }

       IWall IWallFactoryAutoGen.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(query, session);
           
       }

       IWall IWallFactoryAutoGen.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IWall> IWallFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IWall IWallFactoryAutoGen.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
