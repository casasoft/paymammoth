using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.Category_CultureInfoModule;
using PayMammoth.Modules.Category_CultureInfoModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface ICategory_CultureInfoFactoryAutoGen : BusinessLogic_v3.Modules.Category_CultureInfoModule.ICategory_CultureInfoBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<ICategory_CultureInfo>
    
    {
	    new PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo CreateNewItem();
        new PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.Category_CultureInfoModule.ICategory_CultureInfo> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
