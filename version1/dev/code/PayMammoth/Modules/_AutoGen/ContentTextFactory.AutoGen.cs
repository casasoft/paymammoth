using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.ContentTextModule;
using PayMammoth.Modules.ContentTextModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class ContentTextFactoryAutoGen : BusinessLogic_v3.Modules.ContentTextModule.ContentTextBaseFactory, 
    				PayMammoth.Modules._AutoGen.IContentTextFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<IContentText>
    
    {
    
     	public new ContentText ReloadItemFromDatabase(BusinessLogic_v3.Modules.ContentTextModule.ContentTextBase item)
        {
         	return (ContentText)base.ReloadItemFromDatabase(item);
        }
     	static ContentTextFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public ContentTextFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(ContentTextImpl);
			PayMammoth.Cms.ContentTextModule.ContentTextCmsFactory._initialiseStaticInstance();
        }
        public static new ContentTextFactory Instance
        {
            get
            {
                return (ContentTextFactory)ContentTextBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<ContentText> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<ContentText>();
            
        }
        public new IEnumerable<ContentText> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<ContentText>();
            
            
        }
    
        public new IEnumerable<ContentText> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<ContentText>();
            
        }
        public new IEnumerable<ContentText> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<ContentText>();
        }
        
        public new ContentText FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (ContentText)base.FindItem(query);
        }
        public new ContentText FindItem(IQueryOver query)
        {
            return (ContentText)base.FindItem(query);
        }
        
        IContentText IContentTextFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new ContentText CreateNewItem()
        {
            return (ContentText)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<ContentText, ContentText> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<ContentText>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

            var itemList = ContentText.Factory.FindAll();

            var CultureInfos = CultureDetails.Factory.FindAll();

            foreach (var item in itemList)
            {
            	item.__CheckItemCultureInfos(autoSaveInDB: true, CultureInfos: CultureInfos);
            }

  

        }        


#endregion
        public new PayMammoth.Modules.ContentTextModule.ContentText GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.ContentTextModule.ContentText) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.ContentTextModule.ContentText> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ContentTextModule.ContentText>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ContentTextModule.ContentText> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ContentTextModule.ContentText>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ContentTextModule.ContentText> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ContentTextModule.ContentText>) base.FindAll(session);

       }
       public new PayMammoth.Modules.ContentTextModule.ContentText FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ContentTextModule.ContentText) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.ContentTextModule.ContentText FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ContentTextModule.ContentText) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.ContentTextModule.ContentText FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ContentTextModule.ContentText) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.ContentTextModule.ContentText> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ContentTextModule.ContentText>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.ContentTextModule.IContentText> Members
     
        PayMammoth.Modules.ContentTextModule.IContentText IBaseDbFactory<PayMammoth.Modules.ContentTextModule.IContentText>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.ContentTextModule.IContentText IBaseDbFactory<PayMammoth.Modules.ContentTextModule.IContentText>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.ContentTextModule.IContentText> IBaseDbFactory<PayMammoth.Modules.ContentTextModule.IContentText>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.ContentTextModule.IContentText> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.ContentTextModule.IContentText>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.ContentTextModule.IContentText> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.ContentTextModule.IContentText>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.ContentTextModule.IContentText IBaseDbFactory<PayMammoth.Modules.ContentTextModule.IContentText>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ContentTextModule.IContentText BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.ContentTextModule.IContentText>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ContentTextModule.IContentText BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.ContentTextModule.IContentText>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.ContentTextModule.IContentText> IBaseDbFactory<PayMammoth.Modules.ContentTextModule.IContentText>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IContentText> IContentTextFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IContentText> IContentTextFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IContentText> IContentTextFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IContentText IContentTextFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IContentText IContentTextFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IContentText IContentTextFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IContentText> IContentTextFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IContentText IContentTextFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
