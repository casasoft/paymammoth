using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using CS.General_v3.Classes.DbObjects.Collections;
using CS.General_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;

//BaseClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
	
    public interface IPaypalSettingsBaseAutoGen : BusinessLogic_v3.Classes.DB.IBaseDbObject
    {
    

// [interface_base_properties]

		//IProperty_LinkedToObject
        PayMammoth.Modules._AutoGen.IWebsiteAccountBase WebsiteAccount {get; set; }

		//Iproperty_normal
        bool Enabled { get; set; }
		//Iproperty_normal
        string MerchantEmail { get; set; }
   

// [interface_base_collections]

 


    	
    	
      
      

    }
}
