using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.ProductVariationMediaItemModule;
using PayMammoth.Modules.ProductVariationMediaItemModule;
using BusinessLogic_v3.Modules.ProductVariationMediaItemModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.ProductVariationMediaItemModule.ProductVariationMediaItemFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.ProductVariationMediaItemModule.ProductVariationMediaItemFrontend ToFrontend(this PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem item)
        {
        	return PayMammoth.Frontend.ProductVariationMediaItemModule.ProductVariationMediaItemFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class ProductVariationMediaItemFrontend_AutoGen : BusinessLogic_v3.Frontend.ProductVariationMediaItemModule.ProductVariationMediaItemBaseFrontend

    {
		
        
        protected ProductVariationMediaItemFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem Data
        {
            get { return (PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem)base.Data; }
            set { base.Data = value; }

        }

        public new static ProductVariationMediaItemFrontend CreateNewItem()
        {
            return (ProductVariationMediaItemFrontend)BusinessLogic_v3.Frontend.ProductVariationMediaItemModule.ProductVariationMediaItemBaseFrontend.CreateNewItem();
        }
        
        public new static ProductVariationMediaItemFrontend Get(BusinessLogic_v3.Modules.ProductVariationMediaItemModule.IProductVariationMediaItemBase data)
        {
            return (ProductVariationMediaItemFrontend)BusinessLogic_v3.Frontend.ProductVariationMediaItemModule.ProductVariationMediaItemBaseFrontend.Get(data);
        }
        public new static List<ProductVariationMediaItemFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.ProductVariationMediaItemModule.IProductVariationMediaItemBase> dataList)
        {
            return BusinessLogic_v3.Frontend.ProductVariationMediaItemModule.ProductVariationMediaItemBaseFrontend.GetList(dataList).Cast<ProductVariationMediaItemFrontend>().ToList();
        }


    }
}
