using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using CS.General_v3.Extensions;
using PayMammoth.Modules.TestCustomClassModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public class TestCustomClassCmsInfo_AutoGen : PayMammoth.Modules._AutoGen.TestCustomClassBaseCmsInfo
    {

		public TestCustomClassCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.TestCustomClassBase item)
            : base(item)
        {

        }
		public new TestCustomClass DbItem
        {
            get
            {
                return (TestCustomClass)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]

        public CmsCollectionInfo Articles { get; private set; }
        



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

	

// [usercmsinfo_initcollections]

		
		//InitCollectionUserOneToMany
		this.Articles = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.TestCustomClassModule.TestCustomClass>.GetPropertyBySelector(item => item.Articles),
						CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector(item => item.CustomProjOnlyLinkedObject)));


			base.initBasicFields();
		}
    
    }
}
