using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.Category_CultureInfoModule;
using PayMammoth.Cms.Category_CultureInfoModule;
using CS.WebComponentsGeneralV3.Cms.Category_CultureInfoModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class Category_CultureInfoCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.Category_CultureInfoModule.Category_CultureInfoBaseCmsFactory
    {
        public static new Category_CultureInfoBaseCmsFactory Instance 
        {
         	get { return (Category_CultureInfoBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.Category_CultureInfoModule.Category_CultureInfoBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override Category_CultureInfoBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = Category_CultureInfo.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            Category_CultureInfoCmsInfo cmsItem = new Category_CultureInfoCmsInfo(item);
            return cmsItem;
        }    
        
        public override Category_CultureInfoBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase item)
        {
            return new Category_CultureInfoCmsInfo((Category_CultureInfo)item);
            
        }

    }
}
