using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.ProductCategoryFeatureValueModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class ProductCategoryFeatureValueMap_AutoGen : ProductCategoryFeatureValueMap_AutoGen_Z
    {
        public ProductCategoryFeatureValueMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]

			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.Product, ProductMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class ProductCategoryFeatureValueMap_AutoGen_Z : BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBaseMap<ProductCategoryFeatureValueImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
