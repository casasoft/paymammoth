using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using CS.General_v3.Classes.DbObjects.Collections;
using CS.General_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.CmsAuditLogModule;
using BusinessLogic_v3.Modules.CmsAuditLogModule;


//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
    public abstract class CmsAuditLog_AutoGen : CmsAuditLogBase, PayMammoth.Modules._AutoGen.ICmsAuditLogAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.CmsUserModule.CmsUser CmsUser
        {
            get
            {
                return (PayMammoth.Modules.CmsUserModule.CmsUser)base.CmsUser;
            }
            set
            {
                base.CmsUser = value;
            }
        }

		PayMammoth.Modules.CmsUserModule.ICmsUser ICmsAuditLogAutoGen.CmsUser
        {
            get
            {
            	return this.CmsUser;
                
            }
            set
            {
                this.CmsUser = (PayMammoth.Modules.CmsUserModule.CmsUser)value;
            }
        }



// [userclass_collections_nhibernate]


        
          
		public  static new CmsAuditLogFactory Factory
        {
            get
            {
                return (CmsAuditLogFactory)CmsAuditLogBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<CmsAuditLog, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
