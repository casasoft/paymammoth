using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.CmsUserRoleModule;
using PayMammoth.Modules.CmsUserRoleModule;
using BusinessLogic_v3.Modules.CmsUserRoleModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.CmsUserRoleModule.CmsUserRoleFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.CmsUserRoleModule.CmsUserRoleFrontend ToFrontend(this PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole item)
        {
        	return PayMammoth.Frontend.CmsUserRoleModule.CmsUserRoleFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class CmsUserRoleFrontend_AutoGen : BusinessLogic_v3.Frontend.CmsUserRoleModule.CmsUserRoleBaseFrontend

    {
		
        
        protected CmsUserRoleFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole Data
        {
            get { return (PayMammoth.Modules.CmsUserRoleModule.ICmsUserRole)base.Data; }
            set { base.Data = value; }

        }

        public new static CmsUserRoleFrontend CreateNewItem()
        {
            return (CmsUserRoleFrontend)BusinessLogic_v3.Frontend.CmsUserRoleModule.CmsUserRoleBaseFrontend.CreateNewItem();
        }
        
        public new static CmsUserRoleFrontend Get(BusinessLogic_v3.Modules.CmsUserRoleModule.ICmsUserRoleBase data)
        {
            return (CmsUserRoleFrontend)BusinessLogic_v3.Frontend.CmsUserRoleModule.CmsUserRoleBaseFrontend.Get(data);
        }
        public new static List<CmsUserRoleFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.CmsUserRoleModule.ICmsUserRoleBase> dataList)
        {
            return BusinessLogic_v3.Frontend.CmsUserRoleModule.CmsUserRoleBaseFrontend.GetList(dataList).Cast<CmsUserRoleFrontend>().ToList();
        }


    }
}
