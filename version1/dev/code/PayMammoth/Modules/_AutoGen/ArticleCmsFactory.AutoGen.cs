using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.ArticleModule;
using PayMammoth.Cms.ArticleModule;
using CS.WebComponentsGeneralV3.Cms.ArticleModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class ArticleCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.ArticleModule.ArticleBaseCmsFactory
    {
        public static new ArticleBaseCmsFactory Instance 
        {
         	get { return (ArticleBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.ArticleModule.ArticleBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override ArticleBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	Article item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = Article.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        ArticleCmsInfo cmsItem = new ArticleCmsInfo(item);
            return cmsItem;
        }    
        
        public override ArticleBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.ArticleModule.ArticleBase item)
        {
            return new ArticleCmsInfo((Article)item);
            
        }

    }
}
