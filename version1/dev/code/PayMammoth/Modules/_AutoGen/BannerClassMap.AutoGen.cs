using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.BannerModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class BannerMap_AutoGen : BannerMap_AutoGen_Z
    {
        public BannerMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.Identifier, IdentifierMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Title, TitleMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.MediaItemFilename, MediaItemFilenameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Link, LinkMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DurationMS, DurationMSMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.HrefTarget, HrefTargetMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.SlideshowDelaySec, SlideshowDelaySecMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.HTMLDescription, HTMLDescriptionMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class BannerMap_AutoGen_Z : BusinessLogic_v3.Modules.BannerModule.BannerBaseMap<BannerImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
