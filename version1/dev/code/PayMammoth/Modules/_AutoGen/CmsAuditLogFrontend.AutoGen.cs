using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.CmsAuditLogModule;
using PayMammoth.Modules.CmsAuditLogModule;
using BusinessLogic_v3.Modules.CmsAuditLogModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.CmsAuditLogModule.CmsAuditLogFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.CmsAuditLogModule.CmsAuditLogFrontend ToFrontend(this PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog item)
        {
        	return PayMammoth.Frontend.CmsAuditLogModule.CmsAuditLogFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class CmsAuditLogFrontend_AutoGen : BusinessLogic_v3.Frontend.CmsAuditLogModule.CmsAuditLogBaseFrontend

    {
		
        
        protected CmsAuditLogFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog Data
        {
            get { return (PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog)base.Data; }
            set { base.Data = value; }

        }

        public new static CmsAuditLogFrontend CreateNewItem()
        {
            return (CmsAuditLogFrontend)BusinessLogic_v3.Frontend.CmsAuditLogModule.CmsAuditLogBaseFrontend.CreateNewItem();
        }
        
        public new static CmsAuditLogFrontend Get(BusinessLogic_v3.Modules.CmsAuditLogModule.ICmsAuditLogBase data)
        {
            return (CmsAuditLogFrontend)BusinessLogic_v3.Frontend.CmsAuditLogModule.CmsAuditLogBaseFrontend.Get(data);
        }
        public new static List<CmsAuditLogFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.CmsAuditLogModule.ICmsAuditLogBase> dataList)
        {
            return BusinessLogic_v3.Frontend.CmsAuditLogModule.CmsAuditLogBaseFrontend.GetList(dataList).Cast<CmsAuditLogFrontend>().ToList();
        }


    }
}
