using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PayMammoth.Modules._AutoGen;

using CS.General_v3.Classes.NHibernateClasses.Mappings;

namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class PaypalSettingsBaseMap_AutoGen<TData> : BusinessLogic_v3.Classes.NHibernateClasses.Mappings.BaseClassMap<TData>
    	where TData : PaypalSettingsBase
    {
        public PaypalSettingsBaseMap_AutoGen()
        {
            
        }
        
        


// [baseclassmap_properties]

//BaseClassMap_Property
             
        protected virtual void EnabledMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        
//BaseClassMap_Property
             
        protected virtual void MerchantEmailMappingInfo(IPropertyMapInfo mapInfo)
        {
        
            //do nothing
        }
        
        



// [baseclassmap_collections]

            
        
        protected override void initMappings()
        {
           
            
//[BaseClassMap_InitProperties]

//[BaseClassMap_InitCollections]                
            
            base.initMappings();
        }
        
        
        
        
        
    }
   
}
