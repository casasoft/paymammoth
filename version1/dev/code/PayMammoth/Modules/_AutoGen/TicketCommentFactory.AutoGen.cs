using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.TicketCommentModule;
using PayMammoth.Modules.TicketCommentModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class TicketCommentFactoryAutoGen : BusinessLogic_v3.Modules.TicketCommentModule.TicketCommentBaseFactory, 
    				PayMammoth.Modules._AutoGen.ITicketCommentFactoryAutoGen,
    				BusinessLogic_v3.Classes.DB.IBaseDbFactory<ITicketComment>
    
    {
    
     	public new TicketComment ReloadItemFromDatabase(BusinessLogic_v3.Modules.TicketCommentModule.TicketCommentBase item)
        {
         	return (TicketComment)base.ReloadItemFromDatabase(item);
        }
     	
        public TicketCommentFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(TicketCommentImpl);
			PayMammoth.Cms.TicketCommentModule.TicketCommentCmsFactory._initialiseStaticInstance();
        }
        public static new TicketCommentFactory Instance
        {
            get
            {
                return (TicketCommentFactory)TicketCommentBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<TicketComment> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<TicketComment>();
            
        }
        public new IEnumerable<TicketComment> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<TicketComment>();
            
            
        }
    
        public new IEnumerable<TicketComment> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<TicketComment>();
            
        }
        public new IEnumerable<TicketComment> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<TicketComment>();
        }
        
        public new TicketComment FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (TicketComment)base.FindItem(query);
        }
        public new TicketComment FindItem(IQueryOver query)
        {
            return (TicketComment)base.FindItem(query);
        }
        
        ITicketComment ITicketCommentFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new TicketComment CreateNewItem()
        {
            return (TicketComment)base.CreateNewItem();
        }

        public new TicketComment GetByPrimaryKey(long pKey)
        {
            return (TicketComment)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<TicketComment, TicketComment> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<TicketComment>();
            qParams.FillQueryOver(q);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.TicketCommentModule.TicketComment GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.TicketCommentModule.TicketComment) base.GetByPrimaryKey(id, session);
        }
       public new IEnumerable<PayMammoth.Modules.TicketCommentModule.TicketComment> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.TicketCommentModule.TicketComment>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.TicketCommentModule.TicketComment> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.TicketCommentModule.TicketComment>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.TicketCommentModule.TicketComment> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.TicketCommentModule.TicketComment>) base.FindAll(session);

       }
       public new PayMammoth.Modules.TicketCommentModule.TicketComment FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.TicketCommentModule.TicketComment) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.TicketCommentModule.TicketComment FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.TicketCommentModule.TicketComment) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.TicketCommentModule.TicketComment FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.TicketCommentModule.TicketComment) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.TicketCommentModule.TicketComment> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.TicketCommentModule.TicketComment>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.TicketCommentModule.ITicketComment> Members
     
        PayMammoth.Modules.TicketCommentModule.ITicketComment IBaseDbFactory<PayMammoth.Modules.TicketCommentModule.ITicketComment>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.TicketCommentModule.ITicketComment IBaseDbFactory<PayMammoth.Modules.TicketCommentModule.ITicketComment>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.TicketCommentModule.ITicketComment> IBaseDbFactory<PayMammoth.Modules.TicketCommentModule.ITicketComment>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.TicketCommentModule.ITicketComment> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.TicketCommentModule.ITicketComment>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.TicketCommentModule.ITicketComment> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.TicketCommentModule.ITicketComment>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.TicketCommentModule.ITicketComment IBaseDbFactory<PayMammoth.Modules.TicketCommentModule.ITicketComment>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.TicketCommentModule.ITicketComment BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.TicketCommentModule.ITicketComment>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.TicketCommentModule.ITicketComment BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.TicketCommentModule.ITicketComment>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.TicketCommentModule.ITicketComment> IBaseDbFactory<PayMammoth.Modules.TicketCommentModule.ITicketComment>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<ITicketComment> ITicketCommentFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<ITicketComment> ITicketCommentFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<ITicketComment> ITicketCommentFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       ITicketComment ITicketCommentFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       ITicketComment ITicketCommentFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       ITicketComment ITicketCommentFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<ITicketComment> ITicketCommentFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       ITicketComment ITicketCommentFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
