using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.AttachmentModule;
using PayMammoth.Cms.AttachmentModule;
using CS.WebComponentsGeneralV3.Cms.AttachmentModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class AttachmentCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.AttachmentModule.AttachmentBaseCmsFactory
    {
        public static new AttachmentBaseCmsFactory Instance 
        {
         	get { return (AttachmentBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.AttachmentModule.AttachmentBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override AttachmentBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = Attachment.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            AttachmentCmsInfo cmsItem = new AttachmentCmsInfo(item);
            return cmsItem;
        }    
        
        public override AttachmentBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.AttachmentModule.AttachmentBase item)
        {
            return new AttachmentCmsInfo((Attachment)item);
            
        }

    }
}
