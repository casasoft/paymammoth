using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.Category_CultureInfoModule;
using CS.WebComponentsGeneralV3.Cms.Category_CultureInfoModule;
using PayMammoth.Frontend.Category_CultureInfoModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class Category_CultureInfoCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.Category_CultureInfoModule.Category_CultureInfoBaseCmsInfo
    {

		public Category_CultureInfoCmsInfo_AutoGen(BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase item)
            : base(item)
        {

        }
        

        private Category_CultureInfoFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = Category_CultureInfoFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new Category_CultureInfoFrontend FrontendItem
        {
            get { return (Category_CultureInfoFrontend) base.FrontendItem; }
        }
        
        
		public new Category_CultureInfo DbItem
        {
            get
            {
                return (Category_CultureInfo)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

//InitFieldUser_LinkedProperty
			this.CultureInfo = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo>.GetPropertyBySelector( item => item.CultureInfo),
                null));

        
//InitFieldUser_LinkedProperty
			this.Category = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo>.GetPropertyBySelector( item => item.Category),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CategoryModule.Category>.GetPropertyBySelector( item => item.Category_CultureInfos)));

        
        this.TitleSingular = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo>.GetPropertyBySelector( item => item.TitleSingular),
        						isEditable: true, isVisible: true);

        this.TitleSingular_Search = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo>.GetPropertyBySelector( item => item.TitleSingular_Search),
        						isEditable: false, isVisible: false);

        this.TitlePlural = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo>.GetPropertyBySelector( item => item.TitlePlural),
        						isEditable: true, isVisible: true);

        this.TitlePlural_Search = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo>.GetPropertyBySelector( item => item.TitlePlural_Search),
        						isEditable: false, isVisible: false);

        this.TitleSEO = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo>.GetPropertyBySelector( item => item.TitleSEO),
        						isEditable: true, isVisible: true);

        this.Description = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo>.GetPropertyBySelector( item => item.Description),
        						isEditable: true, isVisible: true);

        this.MetaKeywords = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo>.GetPropertyBySelector( item => item.MetaKeywords),
        						isEditable: true, isVisible: true);

        this.MetaDescription = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Category_CultureInfoModule.Category_CultureInfo>.GetPropertyBySelector( item => item.MetaDescription),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
