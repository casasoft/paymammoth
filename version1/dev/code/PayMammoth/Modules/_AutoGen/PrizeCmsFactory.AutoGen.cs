using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using PayMammoth.Modules.PrizeModule;
using PayMammoth.Cms.PrizeModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class PrizeCmsFactory_AutoGen : PayMammoth.Modules._AutoGen.PrizeBaseCmsFactory
    {
        public static new PrizeBaseCmsFactory Instance 
        {
         	get { return (PrizeBaseCmsFactory)PayMammoth.Modules._AutoGen.PrizeBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override PrizeBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = Prize.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            PrizeCmsInfo cmsItem = new PrizeCmsInfo(item);
            return cmsItem;
        }    
        
        public override PrizeBaseCmsInfo CreateCmsItemInfo(PayMammoth.Modules._AutoGen.PrizeBase item)
        {
            return new PrizeCmsInfo((Prize)item);
            
        }

    }
}
