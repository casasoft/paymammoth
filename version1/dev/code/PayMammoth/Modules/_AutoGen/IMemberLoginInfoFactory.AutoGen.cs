using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.MemberLoginInfoModule;
using PayMammoth.Modules.MemberLoginInfoModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IMemberLoginInfoFactoryAutoGen : BusinessLogic_v3.Modules.MemberLoginInfoModule.IMemberLoginInfoBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IMemberLoginInfo>
    
    {
	    new PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo CreateNewItem();
        new PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null);
        new IEnumerable<PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
