using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.BrandModule;
using BusinessLogic_v3.Modules.BrandModule;


//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
    public abstract class Brand_AutoGen : BrandBase, PayMammoth.Modules._AutoGen.IBrandAutoGen
	                 
      , IMultilingualItem
      
      
      
    {
        
            

    
    
    
		

#region Multilingual
        public PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo GetCurrentCultureInfo()
        {
            return (PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo)__getCurrentCultureInfo();
        }             
          
    	public new PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo GetCultureInfoByLanguage(CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 lang)
        {
            return (PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo)base.GetCultureInfoByLanguage(lang);
        }

#endregion
        
        



// [userclass_linkedproperties]




// [userclass_collections_nhibernate]

        //UserClass_CollectionOneToManyLeftSide
        
		new internal class __Brand_CultureInfosCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.BrandModule.Brand, 
        	PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo,
        	PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.BrandModule.Brand, PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo>
        {
            private PayMammoth.Modules.BrandModule.Brand _item = null;
            public __Brand_CultureInfosCollectionInfo(PayMammoth.Modules.BrandModule.Brand item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo>)_item.__collection__Brand_CultureInfos; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.BrandModule.Brand, PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo>.SetLinkOnItem(PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo item, PayMammoth.Modules.BrandModule.Brand value)
            {
                item.Brand = value;
            }
        }
        
        private __Brand_CultureInfosCollectionInfo _Brand_CultureInfos = null;
        internal new __Brand_CultureInfosCollectionInfo Brand_CultureInfos
        {
            get
            {
                if (_Brand_CultureInfos == null)
                    _Brand_CultureInfos = new __Brand_CultureInfosCollectionInfo((PayMammoth.Modules.BrandModule.Brand)this);
                return _Brand_CultureInfos;
            }
        }               
           
		ICollectionManager<PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo> IBrandAutoGen.Brand_CultureInfos
        {
            get {  return this.Brand_CultureInfos; }
        }
           

        
          
		public  static new BrandFactory Factory
        {
            get
            {
                return (BrandFactory)BrandBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<Brand, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


        public override void __CheckItemCultureInfos(bool autoSaveInDB, IEnumerable<BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase> CultureInfos = null)
        {
        	BusinessLogic_v3.Classes.NHibernateClasses.Transactions.MyTransaction t = null;
            if (autoSaveInDB)
                t = beginTransaction();
            MultilingualChecker multilingualChecker = new MultilingualChecker();
            multilingualChecker.ItemToAdd+=new MultilingualChecker.ItemToAddHandler(multilingualChecker_ItemToAdd);
            multilingualChecker.ItemRemoved += new MultilingualChecker.ItemRemovedHandler(multilingualChecker_ItemRemoved);
            multilingualChecker.CheckItemCultureInfos(this, CultureInfos);
            if (autoSaveInDB)
            {
                this.Save();
                t.Commit();
                t.Dispose();
            }
        }


        private void multilingualChecker_ItemToAdd(BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase cultureDetails)
        {
            var itemCultureInfo = PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo.Factory.CreateNewItem();

            itemCultureInfo.Brand = (Brand)this;  //item link
            itemCultureInfo.CultureInfo = (PayMammoth.Modules.CultureDetailsModule.CultureDetails)cultureDetails; //culture


// [userfactory_checkcultureinfos_multilingualfields]

                        itemCultureInfo.Description = this.__Description_cultureBase;



			itemCultureInfo.MarkAsNotTemporary();
			this.Brand_CultureInfos.Add((PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo) itemCultureInfo);
            
            

        }

		private void multilingualChecker_ItemRemoved(IMultilingualContentInfo item)
        {   
        	this.Brand_CultureInfos.Remove((PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo)item);
            
        }

        
        
         #region IMultilingualItem Members

        IEnumerable<IMultilingualContentInfo> IMultilingualItem.GetMultilingualContentInfos()
        {
            return this.Brand_CultureInfos;
            
        }

        protected override IEnumerable<IMultilingualContentInfo> __getMultilingualContentInfos()
        {
            return this.Brand_CultureInfos;
        }
        
		#endregion    

     
        
        


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
