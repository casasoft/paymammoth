using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.ProductVariation_CultureInfoModule;
using CS.WebComponentsGeneralV3.Cms.ProductVariation_CultureInfoModule;
using PayMammoth.Frontend.ProductVariation_CultureInfoModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class ProductVariation_CultureInfoCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBaseCmsInfo
    {

		public ProductVariation_CultureInfoCmsInfo_AutoGen(BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBase item)
            : base(item)
        {

        }
        

        private ProductVariation_CultureInfoFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = ProductVariation_CultureInfoFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new ProductVariation_CultureInfoFrontend FrontendItem
        {
            get { return (ProductVariation_CultureInfoFrontend) base.FrontendItem; }
        }
        
        
		public new ProductVariation_CultureInfo DbItem
        {
            get
            {
                return (ProductVariation_CultureInfo)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

//InitFieldUser_LinkedProperty
			this.CultureInfo = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo>.GetPropertyBySelector( item => item.CultureInfo),
                null));

        
//InitFieldUser_LinkedProperty
			this.ProductVariation = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo>.GetPropertyBySelector( item => item.ProductVariation),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductVariationModule.ProductVariation>.GetPropertyBySelector( item => item.ProductVariation_CultureInfos)));

        
	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
