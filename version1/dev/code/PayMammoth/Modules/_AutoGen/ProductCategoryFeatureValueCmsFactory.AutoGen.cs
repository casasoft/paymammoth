using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.ProductCategoryFeatureValueModule;
using PayMammoth.Cms.ProductCategoryFeatureValueModule;
using CS.WebComponentsGeneralV3.Cms.ProductCategoryFeatureValueModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class ProductCategoryFeatureValueCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBaseCmsFactory
    {
        public static new ProductCategoryFeatureValueBaseCmsFactory Instance 
        {
         	get { return (ProductCategoryFeatureValueBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override ProductCategoryFeatureValueBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = ProductCategoryFeatureValue.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            ProductCategoryFeatureValueCmsInfo cmsItem = new ProductCategoryFeatureValueCmsInfo(item);
            return cmsItem;
        }    
        
        public override ProductCategoryFeatureValueBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBase item)
        {
            return new ProductCategoryFeatureValueCmsInfo((ProductCategoryFeatureValue)item);
            
        }

    }
}
