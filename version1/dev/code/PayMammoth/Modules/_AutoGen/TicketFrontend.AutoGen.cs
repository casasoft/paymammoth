using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.TicketModule;
using PayMammoth.Modules.TicketModule;
using BusinessLogic_v3.Modules.TicketModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.TicketModule.TicketFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.TicketModule.ITicket> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.TicketModule.TicketFrontend ToFrontend(this PayMammoth.Modules.TicketModule.ITicket item)
        {
        	return PayMammoth.Frontend.TicketModule.TicketFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class TicketFrontend_AutoGen : BusinessLogic_v3.Frontend.TicketModule.TicketBaseFrontend

    {
		
        
        protected TicketFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.TicketModule.ITicket Data
        {
            get { return (PayMammoth.Modules.TicketModule.ITicket)base.Data; }
            set { base.Data = value; }

        }

        public new static TicketFrontend CreateNewItem()
        {
            return (TicketFrontend)BusinessLogic_v3.Frontend.TicketModule.TicketBaseFrontend.CreateNewItem();
        }
        
        public new static TicketFrontend Get(BusinessLogic_v3.Modules.TicketModule.ITicketBase data)
        {
            return (TicketFrontend)BusinessLogic_v3.Frontend.TicketModule.TicketBaseFrontend.Get(data);
        }
        public new static List<TicketFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.TicketModule.ITicketBase> dataList)
        {
            return BusinessLogic_v3.Frontend.TicketModule.TicketBaseFrontend.GetList(dataList).Cast<TicketFrontend>().ToList();
        }


    }
}
