using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.NHibernateClasses;

using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule;
using PayMammoth.Modules.MemberAccountBalanceHistoryModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IMemberAccountBalanceHistoryFactoryAutoGen : BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistoryBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IMemberAccountBalanceHistory>
    
    {
	    new PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory CreateNewItem();
        new PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
