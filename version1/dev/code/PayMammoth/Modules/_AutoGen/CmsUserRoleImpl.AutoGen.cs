using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using NHibernate;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.CmsUserRoleModule;

namespace PayMammoth.Modules._AutoGen
{

//UserClassImpl-File

    public class CmsUserRoleImpl : PayMammoth.Modules.CmsUserRoleModule.CmsUserRole
    {
/*
 		public new CmsUserImpl LastEditedBy
        {
            get { return (CmsUserImpl) base.LastEditedBy; }
            set { base.LastEditedBy = value; }
        }
        public new CmsUserImpl DeletedBy
        {
            get { return (CmsUserImpl)base.DeletedBy; }
            set { base.DeletedBy = value; }
        }
*/
		#region UserClass-AutoGenerated
		#endregion

      
      
        private CmsUserRoleImpl()
        {
            
        }    
    	

// [userclassimpl_collections]

        //UserClassImpl_CollectionManyToManyLeftSide
        
        protected override IEnumerable<BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase> __collection__CmsUsers
        {
            get
            {
                return this._CmsUsers;
            }
        }

        private Iesi.Collections.Generic.ISet<CmsUserImpl> _CmsUsers = new Iesi.Collections.Generic.HashedSet<CmsUserImpl>();
        public new virtual Iesi.Collections.Generic.ISet<CmsUserImpl> CmsUsers
        {
            get { return _CmsUsers; }

        }
    	


    	
        
    }
}




