using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using PayMammoth.Modules._AutoGen;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace PayMammoth.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "PaymentRequestTransaction")]
    public abstract class PaymentRequestTransactionBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IPaymentRequestTransactionBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static PaymentRequestTransactionBaseFactory Factory
        {
            get
            {
                return PaymentRequestTransactionBaseFactory.Instance; 
            }
        }    
        /*
        public static PaymentRequestTransactionBase CreateNewItem()
        {
            return (PaymentRequestTransactionBase)Factory.CreateNewItem();
        }

		public static PaymentRequestTransactionBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<PaymentRequestTransactionBase, PaymentRequestTransactionBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static PaymentRequestTransactionBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static PaymentRequestTransactionBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<PaymentRequestTransactionBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<PaymentRequestTransactionBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<PaymentRequestTransactionBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? PaymentRequestID
		{
		 	get 
		 	{
		 		return (this.PaymentRequest != null ? (long?)this.PaymentRequest.ID : null);
		 	}
		}*/
		
        protected PayMammoth.Modules._AutoGen.PaymentRequestBase _paymentrequest;
        public virtual PayMammoth.Modules._AutoGen.PaymentRequestBase PaymentRequest 
        {
        	get
        	{
        		return _paymentrequest;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentrequest,value);
        		_paymentrequest = value;
        	}
        }
            
        PayMammoth.Modules._AutoGen.IPaymentRequestBase PayMammoth.Modules._AutoGen.IPaymentRequestTransactionBaseAutoGen.PaymentRequest 
        {
            get
            {
            	return this.PaymentRequest;
            }
            set
            {
            	this.PaymentRequest = (PayMammoth.Modules._AutoGen.PaymentRequestBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private bool _successful;
        
        public virtual bool Successful 
        {
        	get
        	{
        		return _successful;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_successful,value);
        		_successful = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private PayMammoth.Connector.Enums.PaymentMethodSpecific _paymentmethod;
        
        public virtual PayMammoth.Connector.Enums.PaymentMethodSpecific PaymentMethod 
        {
        	get
        	{
        		return _paymentmethod;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentmethod,value);
        		_paymentmethod = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _comments;
        
        public virtual string Comments 
        {
        	get
        	{
        		return _comments;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_comments,value);
        		_comments = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _ip;
        
        public virtual string IP 
        {
        	get
        	{
        		return _ip;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_ip,value);
        		_ip = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _authcode;
        
        public virtual string AuthCode 
        {
        	get
        	{
        		return _authcode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_authcode,value);
        		_authcode = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _startdate;
        
        public virtual DateTime StartDate 
        {
        	get
        	{
        		return _startdate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_startdate,value);
        		_startdate = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _lastupdatedon;
        
        public virtual DateTime LastUpdatedOn 
        {
        	get
        	{
        		return _lastupdatedon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_lastupdatedon,value);
        		_lastupdatedon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _enddate;
        
        public virtual DateTime EndDate 
        {
        	get
        	{
        		return _enddate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_enddate,value);
        		_enddate = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _reference;
        
        public virtual string Reference 
        {
        	get
        	{
        		return _reference;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_reference,value);
        		_reference = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _transactioninfo;
        
        public virtual string TransactionInfo 
        {
        	get
        	{
        		return _transactioninfo;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_transactioninfo,value);
        		_transactioninfo = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paymentparameters;
        
        public virtual string PaymentParameters 
        {
        	get
        	{
        		return _paymentparameters;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentparameters,value);
        		_paymentparameters = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paymentmethodversion;
        
        public virtual string PaymentMethodVersion 
        {
        	get
        	{
        		return _paymentmethodversion;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentmethodversion,value);
        		_paymentmethodversion = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _fakepayment;
        
        public virtual bool FakePayment 
        {
        	get
        	{
        		return _fakepayment;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_fakepayment,value);
        		_fakepayment = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _requiresmanualintervention;
        
        public virtual bool RequiresManualIntervention 
        {
        	get
        	{
        		return _requiresmanualintervention;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_requiresmanualintervention,value);
        		_requiresmanualintervention = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
