using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.ArticleModule;
using PayMammoth.Modules.ArticleModule;
using BusinessLogic_v3.Modules.ArticleModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.ArticleModule.ArticleFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.ArticleModule.IArticle> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.ArticleModule.ArticleFrontend ToFrontend(this PayMammoth.Modules.ArticleModule.IArticle item)
        {
        	return PayMammoth.Frontend.ArticleModule.ArticleFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class ArticleFrontend_AutoGen : BusinessLogic_v3.Frontend.ArticleModule.ArticleBaseFrontend

    {
		
        
        protected ArticleFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.ArticleModule.IArticle Data
        {
            get { return (PayMammoth.Modules.ArticleModule.IArticle)base.Data; }
            set { base.Data = value; }

        }

        public new static ArticleFrontend CreateNewItem()
        {
            return (ArticleFrontend)BusinessLogic_v3.Frontend.ArticleModule.ArticleBaseFrontend.CreateNewItem();
        }
        
        public new static ArticleFrontend Get(BusinessLogic_v3.Modules.ArticleModule.IArticleBase data)
        {
            return (ArticleFrontend)BusinessLogic_v3.Frontend.ArticleModule.ArticleBaseFrontend.Get(data);
        }
        public new static List<ArticleFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.ArticleModule.IArticleBase> dataList)
        {
            return BusinessLogic_v3.Frontend.ArticleModule.ArticleBaseFrontend.GetList(dataList).Cast<ArticleFrontend>().ToList();
        }


    }
}
