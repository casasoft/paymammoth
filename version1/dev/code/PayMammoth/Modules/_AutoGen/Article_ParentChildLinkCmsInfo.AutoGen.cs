using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.Article_ParentChildLinkModule;
using CS.WebComponentsGeneralV3.Cms.Article_ParentChildLinkModule;
using PayMammoth.Frontend.Article_ParentChildLinkModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class Article_ParentChildLinkCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.Article_ParentChildLinkModule.Article_ParentChildLinkBaseCmsInfo
    {

		public Article_ParentChildLinkCmsInfo_AutoGen(BusinessLogic_v3.Modules.Article_ParentChildLinkModule.Article_ParentChildLinkBase item)
            : base(item)
        {

        }
        

        private Article_ParentChildLinkFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = Article_ParentChildLinkFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new Article_ParentChildLinkFrontend FrontendItem
        {
            get { return (Article_ParentChildLinkFrontend) base.FrontendItem; }
        }
        
        
		public new Article_ParentChildLink DbItem
        {
            get
            {
                return (Article_ParentChildLink)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

//InitFieldUser_LinkedProperty
			this.Parent = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink>.GetPropertyBySelector( item => item.Parent),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.ChildArticleLinks)));

        
//InitFieldUser_LinkedProperty
			this.Child = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink>.GetPropertyBySelector( item => item.Child),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.ParentArticleLinks)));

        
	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
