using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.ProductCategorySpecificationValueModule;
using CS.WebComponentsGeneralV3.Cms.ProductCategorySpecificationValueModule;
using PayMammoth.Frontend.ProductCategorySpecificationValueModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class ProductCategorySpecificationValueCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueBaseCmsInfo
    {

		public ProductCategorySpecificationValueCmsInfo_AutoGen(BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueBase item)
            : base(item)
        {

        }
        

        private ProductCategorySpecificationValueFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = ProductCategorySpecificationValueFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new ProductCategorySpecificationValueFrontend FrontendItem
        {
            get { return (ProductCategorySpecificationValueFrontend) base.FrontendItem; }
        }
        
        
		public new ProductCategorySpecificationValue DbItem
        {
            get
            {
                return (ProductCategorySpecificationValue)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.Value = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValue>.GetPropertyBySelector( item => item.Value),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.Product = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValue>.GetPropertyBySelector( item => item.Product),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.ProductCategorySpecificationValues)));

        
        this.Title = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValue>.GetPropertyBySelector( item => item.Title),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
