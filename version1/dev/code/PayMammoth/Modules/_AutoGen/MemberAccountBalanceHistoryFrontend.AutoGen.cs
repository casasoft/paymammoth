using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.MemberAccountBalanceHistoryModule;
using PayMammoth.Modules.MemberAccountBalanceHistoryModule;
using BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryFrontend ToFrontend(this PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory item)
        {
        	return PayMammoth.Frontend.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class MemberAccountBalanceHistoryFrontend_AutoGen : BusinessLogic_v3.Frontend.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBaseFrontend

    {
		
        
        protected MemberAccountBalanceHistoryFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory Data
        {
            get { return (PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory)base.Data; }
            set { base.Data = value; }

        }

        public new static MemberAccountBalanceHistoryFrontend CreateNewItem()
        {
            return (MemberAccountBalanceHistoryFrontend)BusinessLogic_v3.Frontend.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBaseFrontend.CreateNewItem();
        }
        
        public new static MemberAccountBalanceHistoryFrontend Get(BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistoryBase data)
        {
            return (MemberAccountBalanceHistoryFrontend)BusinessLogic_v3.Frontend.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBaseFrontend.Get(data);
        }
        public new static List<MemberAccountBalanceHistoryFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistoryBase> dataList)
        {
            return BusinessLogic_v3.Frontend.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBaseFrontend.GetList(dataList).Cast<MemberAccountBalanceHistoryFrontend>().ToList();
        }


    }
}
