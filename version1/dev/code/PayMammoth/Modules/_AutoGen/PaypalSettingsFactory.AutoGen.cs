using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.DbObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.PaypalSettingsModule;
using PayMammoth.Modules.CultureDetailsModule;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaypalSettingsFactoryAutoGen : PayMammoth.Modules._AutoGen.PaypalSettingsBaseFactory, 
    				PayMammoth.Modules._AutoGen.IPaypalSettingsFactoryAutoGen,
    				CS.General_v3.Classes.DbObjects.IBaseDbFactory<IPaypalSettings>
    
    {
    
     	
        public PaypalSettingsFactoryAutoGen ()
        {

			this.UsedInProject = true;
			_mainTypeCreated = typeof(PaypalSettingsImpl);
			PayMammoth.Cms.PaypalSettingsModule.PaypalSettingsCmsFactory._initialiseStaticInstance();
        }
        public static new PaypalSettingsFactory Instance
        {
            get
            {
                return (PaypalSettingsFactory)PaypalSettingsBaseFactory.Instance;
            }
      
      

        }
        
        
        public new IEnumerable<PaypalSettings> FindAll()
        {
            return base.FindAll().Cast<PaypalSettings>();
        }
        public new IEnumerable<PaypalSettings> FindAll(IQuery query)
        {
            
            return base.FindAll(query).Cast<PaypalSettings>();

        }
        public new IEnumerable<PaypalSettings> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<PaypalSettings>();
            
        }
        public new IEnumerable<PaypalSettings> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<PaypalSettings>();
            
            
        }
        public new IEnumerable<PaypalSettings> FindAll(ICriteria crit)
        {
            return base.FindAll(crit).Cast<PaypalSettings>();


        }
        public new IEnumerable<PaypalSettings> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<PaypalSettings>();
            
        }
        public new IEnumerable<PaypalSettings> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<PaypalSettings>();
        }
        
        public new PaypalSettings FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (PaypalSettings)base.FindItem(query);
        }
        public new PaypalSettings FindItem(IQueryOver query)
        {
            return (PaypalSettings)base.FindItem(query);
        }
        
        IPaypalSettings IPaypalSettingsFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new PaypalSettings CreateNewItem()
        {
            return (PaypalSettings)base.CreateNewItem();
        }

        public new PaypalSettings GetByPrimaryKey(long pKey)
        {
            return (PaypalSettings)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<PaypalSettings, PaypalSettings> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();

        	var q = session.QueryOver<PaypalSettings>();
            qParams.FillQueryOver(q);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public PayMammoth.Modules.PaypalSettingsModule.PaypalSettings GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.PaypalSettingsModule.PaypalSettings) base.GetByPrimaryKey(id, session);
        }
       public IEnumerable<PayMammoth.Modules.PaypalSettingsModule.PaypalSettings> FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PaypalSettingsModule.PaypalSettings>) base.FindAll(query, session);

       }
       public IEnumerable<PayMammoth.Modules.PaypalSettingsModule.PaypalSettings> FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PaypalSettingsModule.PaypalSettings>) base.FindAll(query, session);

       }
       public IEnumerable<PayMammoth.Modules.PaypalSettingsModule.PaypalSettings> FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PaypalSettingsModule.PaypalSettings>) base.FindAll(session);

       }
       public PayMammoth.Modules.PaypalSettingsModule.PaypalSettings FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.PaypalSettingsModule.PaypalSettings) base.FindItem(query, session);

       }
       public PayMammoth.Modules.PaypalSettingsModule.PaypalSettings FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.PaypalSettingsModule.PaypalSettings) base.FindItem(query, session);

       }

       public PayMammoth.Modules.PaypalSettingsModule.PaypalSettings FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.PaypalSettingsModule.PaypalSettings) base.FindItem(session);

       }
       public IEnumerable<PayMammoth.Modules.PaypalSettingsModule.PaypalSettings> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PaypalSettingsModule.PaypalSettings>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.PaypalSettingsModule.IPaypalSettings> Members
     
        PayMammoth.Modules.PaypalSettingsModule.IPaypalSettings IBaseDbFactory<PayMammoth.Modules.PaypalSettingsModule.IPaypalSettings>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.PaypalSettingsModule.IPaypalSettings IBaseDbFactory<PayMammoth.Modules.PaypalSettingsModule.IPaypalSettings>.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.PaypalSettingsModule.IPaypalSettings> IBaseDbFactory<PayMammoth.Modules.PaypalSettingsModule.IPaypalSettings>.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.PaypalSettingsModule.IPaypalSettings> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.PaypalSettingsModule.IPaypalSettings>.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.PaypalSettingsModule.IPaypalSettings> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.PaypalSettingsModule.IPaypalSettings>.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.PaypalSettingsModule.IPaypalSettings IBaseDbFactory<PayMammoth.Modules.PaypalSettingsModule.IPaypalSettings>.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.PaypalSettingsModule.IPaypalSettings CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.PaypalSettingsModule.IPaypalSettings>.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.PaypalSettingsModule.IPaypalSettings CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.PaypalSettingsModule.IPaypalSettings>.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.PaypalSettingsModule.IPaypalSettings> IBaseDbFactory<PayMammoth.Modules.PaypalSettingsModule.IPaypalSettings>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IPaypalSettings> IPaypalSettingsFactoryAutoGen.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IPaypalSettings> IPaypalSettingsFactoryAutoGen.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IPaypalSettings> IPaypalSettingsFactoryAutoGen.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(session);
           
       }

       IPaypalSettings IPaypalSettingsFactoryAutoGen.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(query, session);
           
       }

       IPaypalSettings IPaypalSettingsFactoryAutoGen.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(query, session);
           
       }

       IPaypalSettings IPaypalSettingsFactoryAutoGen.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IPaypalSettings> IPaypalSettingsFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IPaypalSettings IPaypalSettingsFactoryAutoGen.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
