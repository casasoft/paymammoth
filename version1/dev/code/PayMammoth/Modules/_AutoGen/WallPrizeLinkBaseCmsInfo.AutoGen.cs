using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class WallPrizeLinkBaseCmsInfo_AutoGen : CmsItemInfoDbBase<PayMammoth.Modules._AutoGen.WallPrizeLinkBase>
    {
		public WallPrizeLinkBaseCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.WallPrizeLinkBase dbItem)
            : base(PayMammoth.Modules._AutoGen.WallPrizeLinkBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new WallPrizeLinkBase DbItem
        {
            get
            {
                return (WallPrizeLinkBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Wall { get; private set; }

        public CmsPropertyInfo Prize { get; private set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Wall = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.WallPrizeLinkBase>.GetPropertyBySelector( item => item.Wall),
        			isEditable: true,
        			isVisible: true
        			);

        this.Prize = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.WallPrizeLinkBase>.GetPropertyBySelector( item => item.Prize),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
