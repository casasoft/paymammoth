using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.Article_CultureInfoModule;
using CS.WebComponentsGeneralV3.Cms.Article_CultureInfoModule;
using PayMammoth.Frontend.Article_CultureInfoModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class Article_CultureInfoCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.Article_CultureInfoModule.Article_CultureInfoBaseCmsInfo
    {

		public Article_CultureInfoCmsInfo_AutoGen(BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase item)
            : base(item)
        {

        }
        

        private Article_CultureInfoFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = Article_CultureInfoFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new Article_CultureInfoFrontend FrontendItem
        {
            get { return (Article_CultureInfoFrontend) base.FrontendItem; }
        }
        
        
		public new Article_CultureInfo DbItem
        {
            get
            {
                return (Article_CultureInfo)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

//InitFieldUser_LinkedProperty
			this.CultureInfo = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo>.GetPropertyBySelector( item => item.CultureInfo),
                null));

        
//InitFieldUser_LinkedProperty
			this.Article = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo>.GetPropertyBySelector( item => item.Article),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.Article_CultureInfos)));

        
        this.HtmlText = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo>.GetPropertyBySelector( item => item.HtmlText),
        						isEditable: true, isVisible: true);

        this.HtmlText_Search = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo>.GetPropertyBySelector( item => item.HtmlText_Search),
        						isEditable: false, isVisible: false);

        this.MetaKeywords = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo>.GetPropertyBySelector( item => item.MetaKeywords),
        						isEditable: true, isVisible: true);

        this.MetaKeywords_Search = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo>.GetPropertyBySelector( item => item.MetaKeywords_Search),
        						isEditable: false, isVisible: false);

        this.MetaDescription = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo>.GetPropertyBySelector( item => item.MetaDescription),
        						isEditable: true, isVisible: true);

        this.MetaDescription_Search = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo>.GetPropertyBySelector( item => item.MetaDescription_Search),
        						isEditable: false, isVisible: false);

        this.PageTitle = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo>.GetPropertyBySelector( item => item.PageTitle),
        						isEditable: true, isVisible: true);

        this.PageTitle_Search = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo>.GetPropertyBySelector( item => item.PageTitle_Search),
        						isEditable: false, isVisible: false);

        this.DialogWidth = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo>.GetPropertyBySelector( item => item.DialogWidth),
        						isEditable: true, isVisible: true);

        this.DialogHeight = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo>.GetPropertyBySelector( item => item.DialogHeight),
        						isEditable: true, isVisible: true);

        this.SubTitle = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo>.GetPropertyBySelector( item => item.SubTitle),
        						isEditable: true, isVisible: true);

        this._ComputedURL = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo>.GetPropertyBySelector( item => item._ComputedURL),
        						isEditable: true, isVisible: true);

        this._Computed_ParentArticleNames = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo>.GetPropertyBySelector( item => item._Computed_ParentArticleNames),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
