using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace PayMammoth.Modules._AutoGen
{
	public abstract class WallPrizeLinkBaseCmsFactory_AutoGen : CmsFactoryBase<WallPrizeLinkBaseCmsInfo, WallPrizeLinkBase>
    {
       
       public new static WallPrizeLinkBaseCmsFactory Instance
	    {
	         get
	         {
                 return (WallPrizeLinkBaseCmsFactory)CmsFactoryBase<WallPrizeLinkBaseCmsInfo, WallPrizeLinkBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = WallPrizeLinkBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "WallPrizeLink.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "WallPrizeLink";

            this.QueryStringParamID = "WallPrizeLinkId";

            cmsInfo.TitlePlural = "Wall Prize Links";

            cmsInfo.TitleSingular =  "Wall Prize Link";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public WallPrizeLinkBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "WallPrizeLink/";


        }
       
    }

}
