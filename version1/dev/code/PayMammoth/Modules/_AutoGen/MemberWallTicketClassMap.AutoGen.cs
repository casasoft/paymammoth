using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.MemberWallTicketModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class MemberWallTicketMap_AutoGen : MemberWallTicketMap_AutoGen_Z
    {
        public MemberWallTicketMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]

			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.Wall, WallMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.TicketNumber, TicketNumberMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PurchasedOn, PurchasedOnMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.Member, MemberMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.WonPrize, WonPrizeMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class MemberWallTicketMap_AutoGen_Z : PayMammoth.Modules._AutoGen.MemberWallTicketBaseMap<MemberWallTicketImpl>
    {


// [userclassmap_properties]

//ClassMap_PropertyLinkedObject

        protected virtual void WallMappingInfo(ICollectionMapInfo mapInfo)
        {
        
            //mapInfo.CollectionType = CS.General_v3.Enums.NHIBERNATE_COLLECTION_TYPE.Set;
            
            mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.None;
            
            mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
            mapInfo.Inverse = false;
            mapInfo.ColumnName = "WallId";   
            
            mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
           
        }
        
        

//ClassMap_PropertyLinkedObject

        protected virtual void WonPrizeMappingInfo(ICollectionMapInfo mapInfo)
        {
        
            //mapInfo.CollectionType = CS.General_v3.Enums.NHIBERNATE_COLLECTION_TYPE.Set;
            
            mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.None;
            
            mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
            mapInfo.Inverse = false;
            mapInfo.ColumnName = "WonPrizeId";   
            
            mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
           
        }
        
        




// [userclassmap_collections]

            
        
        
    }
}
