using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.Category_ParentChildLinkModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class Category_ParentChildLinkMap_AutoGen : Category_ParentChildLinkMap_AutoGen_Z
    {
        public Category_ParentChildLinkMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]

			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.ParentCategory, ParentCategoryMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.ChildCategory, ChildCategoryMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class Category_ParentChildLinkMap_AutoGen_Z : BusinessLogic_v3.Modules.Category_ParentChildLinkModule.Category_ParentChildLinkBaseMap<Category_ParentChildLinkImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
