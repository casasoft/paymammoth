using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.WebsiteAccountModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class WebsiteAccountFactoryAutoGen : PayMammoth.Modules._AutoGen.WebsiteAccountBaseFactory, 
    				PayMammoth.Modules._AutoGen.IWebsiteAccountFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<IWebsiteAccount>
    
    {
    
     	public new WebsiteAccount ReloadItemFromDatabase(PayMammoth.Modules._AutoGen.WebsiteAccountBase item)
        {
         	return (WebsiteAccount)base.ReloadItemFromDatabase(item);
        }
     	static WebsiteAccountFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public WebsiteAccountFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(WebsiteAccountImpl);
			PayMammoth.Cms.WebsiteAccountModule.WebsiteAccountCmsFactory._initialiseStaticInstance();
        }
        public static new WebsiteAccountFactory Instance
        {
            get
            {
                return (WebsiteAccountFactory)WebsiteAccountBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<WebsiteAccount> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<WebsiteAccount>();
            
        }
        public new IEnumerable<WebsiteAccount> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<WebsiteAccount>();
            
            
        }
    
        public new IEnumerable<WebsiteAccount> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<WebsiteAccount>();
            
        }
        public new IEnumerable<WebsiteAccount> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<WebsiteAccount>();
        }
        
        public new WebsiteAccount FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (WebsiteAccount)base.FindItem(query);
        }
        public new WebsiteAccount FindItem(IQueryOver query)
        {
            return (WebsiteAccount)base.FindItem(query);
        }
        
        IWebsiteAccount IWebsiteAccountFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new WebsiteAccount CreateNewItem()
        {
            return (WebsiteAccount)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<WebsiteAccount, WebsiteAccount> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<WebsiteAccount>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

            var itemList = WebsiteAccount.Factory.FindAll();

            var CultureInfos = CultureDetails.Factory.FindAll();

            foreach (var item in itemList)
            {
            	item.__CheckItemCultureInfos(autoSaveInDB: true, CultureInfos: CultureInfos);
            }

  

        }        


#endregion
        public new PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>) base.FindAll(session);

       }
       public new PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount> Members
     
        PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount IBaseDbFactory<PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount IBaseDbFactory<PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount> IBaseDbFactory<PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount IBaseDbFactory<PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount> IBaseDbFactory<PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IWebsiteAccount> IWebsiteAccountFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IWebsiteAccount> IWebsiteAccountFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IWebsiteAccount> IWebsiteAccountFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IWebsiteAccount IWebsiteAccountFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IWebsiteAccount IWebsiteAccountFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IWebsiteAccount IWebsiteAccountFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IWebsiteAccount> IWebsiteAccountFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IWebsiteAccount IWebsiteAccountFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
