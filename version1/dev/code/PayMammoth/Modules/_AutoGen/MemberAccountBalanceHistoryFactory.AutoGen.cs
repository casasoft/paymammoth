using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule;
using PayMammoth.Modules.MemberAccountBalanceHistoryModule;
using PayMammoth.Modules.CultureDetailsModule;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class MemberAccountBalanceHistoryFactoryAutoGen : BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBaseFactory, 
    				PayMammoth.Modules._AutoGen.IMemberAccountBalanceHistoryFactoryAutoGen,
    				CS.General_v3.Classes.DbObjects.IBaseDbFactory<IMemberAccountBalanceHistory>
    
    {
    
     	public new MemberAccountBalanceHistory ReloadItemFromDatabase(BusinessLogic_v3.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryBase item)
        {
         	return (MemberAccountBalanceHistory)base.ReloadItemFromDatabase(item);
        }
     	
        public MemberAccountBalanceHistoryFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(MemberAccountBalanceHistoryImpl);
			PayMammoth.Cms.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistoryCmsFactory._initialiseStaticInstance();
        }
        public static new MemberAccountBalanceHistoryFactory Instance
        {
            get
            {
                return (MemberAccountBalanceHistoryFactory)MemberAccountBalanceHistoryBaseFactory.Instance;
            }
      
      

        }
        
        /*
        public new IEnumerable<MemberAccountBalanceHistory> FindAll()
        {
            return base.FindAll().Cast<MemberAccountBalanceHistory>();
        }*/
    /*    public new IEnumerable<MemberAccountBalanceHistory> FindAll(IQuery query)
        {
            
            return base.FindAll(query).Cast<MemberAccountBalanceHistory>();

        }*/
        public new IEnumerable<MemberAccountBalanceHistory> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<MemberAccountBalanceHistory>();
            
        }
        public new IEnumerable<MemberAccountBalanceHistory> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<MemberAccountBalanceHistory>();
            
            
        }
     /*  public new IEnumerable<MemberAccountBalanceHistory> FindAll(ICriteria crit)
        {
            return base.FindAll(crit).Cast<MemberAccountBalanceHistory>();


        }*/
        public new IEnumerable<MemberAccountBalanceHistory> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<MemberAccountBalanceHistory>();
            
        }
        public new IEnumerable<MemberAccountBalanceHistory> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<MemberAccountBalanceHistory>();
        }
        
        public new MemberAccountBalanceHistory FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (MemberAccountBalanceHistory)base.FindItem(query);
        }
        public new MemberAccountBalanceHistory FindItem(IQueryOver query)
        {
            return (MemberAccountBalanceHistory)base.FindItem(query);
        }
        
        IMemberAccountBalanceHistory IMemberAccountBalanceHistoryFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new MemberAccountBalanceHistory CreateNewItem()
        {
            return (MemberAccountBalanceHistory)base.CreateNewItem();
        }

        public new MemberAccountBalanceHistory GetByPrimaryKey(long pKey)
        {
            return (MemberAccountBalanceHistory)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<MemberAccountBalanceHistory, MemberAccountBalanceHistory> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();

        	var q = session.QueryOver<MemberAccountBalanceHistory>();
            qParams.FillQueryOver(q);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistory GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistory) base.GetByPrimaryKey(id, session);
        }
       public new IEnumerable<PayMammoth.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistory> FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistory>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistory> FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistory>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistory> FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistory>) base.FindAll(session);

       }
       public new PayMammoth.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistory FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistory) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistory FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistory) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistory FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistory) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistory> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.MemberAccountBalanceHistoryModule.MemberAccountBalanceHistory>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory> Members
     
        PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory IBaseDbFactory<PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory IBaseDbFactory<PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory>.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory> IBaseDbFactory<PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory>.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory>.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory>.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory IBaseDbFactory<PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory>.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory>.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory>.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory> IBaseDbFactory<PayMammoth.Modules.MemberAccountBalanceHistoryModule.IMemberAccountBalanceHistory>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IMemberAccountBalanceHistory> IMemberAccountBalanceHistoryFactoryAutoGen.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IMemberAccountBalanceHistory> IMemberAccountBalanceHistoryFactoryAutoGen.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IMemberAccountBalanceHistory> IMemberAccountBalanceHistoryFactoryAutoGen.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IMemberAccountBalanceHistory IMemberAccountBalanceHistoryFactoryAutoGen.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IMemberAccountBalanceHistory IMemberAccountBalanceHistoryFactoryAutoGen.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IMemberAccountBalanceHistory IMemberAccountBalanceHistoryFactoryAutoGen.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IMemberAccountBalanceHistory> IMemberAccountBalanceHistoryFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IMemberAccountBalanceHistory IMemberAccountBalanceHistoryFactoryAutoGen.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
