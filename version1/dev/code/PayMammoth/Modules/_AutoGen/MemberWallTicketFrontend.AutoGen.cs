using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.MemberWallTicketModule;
using PayMammoth.Modules.MemberWallTicketModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class MemberWallTicketFrontend_AutoGen : PayMammoth.Modules._AutoGen.MemberWallTicketModule.MemberWallTicketBaseFrontend

    {
		
        
        protected MemberWallTicketFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket Data
        {
            get { return (PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket)base.Data; }
            set { base.Data = value; }

        }

        public new static MemberWallTicketFrontend Get(PayMammoth.Modules._AutoGen.IMemberWallTicketBase data)
        {
            return (MemberWallTicketFrontend)PayMammoth.Modules._AutoGen.MemberWallTicketModule.MemberWallTicketBaseFrontend.Get(data);
        }
        public new static List<MemberWallTicketFrontend> GetList(IEnumerable<PayMammoth.Modules._AutoGen.IMemberWallTicketBase> dataList)
        {
            return PayMammoth.Modules._AutoGen.MemberWallTicketModule.MemberWallTicketBaseFrontend.GetList(dataList).Cast<MemberWallTicketFrontend>().ToList();
        }


    }
}
