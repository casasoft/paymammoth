using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.CmsAuditLogModule;
using CS.WebComponentsGeneralV3.Cms.CmsAuditLogModule;
using PayMammoth.Frontend.CmsAuditLogModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class CmsAuditLogCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.CmsAuditLogModule.CmsAuditLogBaseCmsInfo
    {

		public CmsAuditLogCmsInfo_AutoGen(BusinessLogic_v3.Modules.CmsAuditLogModule.CmsAuditLogBase item)
            : base(item)
        {

        }
        

        private CmsAuditLogFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = CmsAuditLogFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new CmsAuditLogFrontend FrontendItem
        {
            get { return (CmsAuditLogFrontend) base.FrontendItem; }
        }
        
        
		public new CmsAuditLog DbItem
        {
            get
            {
                return (CmsAuditLog)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

//InitFieldUser_LinkedProperty
			this.CmsUser = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsAuditLogModule.CmsAuditLog>.GetPropertyBySelector( item => item.CmsUser),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsUserModule.CmsUser>.GetPropertyBySelector( item => item.CmsAuditLogs)));

        
        this.DateTime = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsAuditLogModule.CmsAuditLog>.GetPropertyBySelector( item => item.DateTime),
        						isEditable: true, isVisible: true);

        this.Message = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsAuditLogModule.CmsAuditLog>.GetPropertyBySelector( item => item.Message),
        						isEditable: true, isVisible: true);

        this.FurtherInformation = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsAuditLogModule.CmsAuditLog>.GetPropertyBySelector( item => item.FurtherInformation),
        						isEditable: true, isVisible: true);

        this.MsgType = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsAuditLogModule.CmsAuditLog>.GetPropertyBySelector( item => item.MsgType),
        						isEditable: true, isVisible: true);

        this.IPAddress = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsAuditLogModule.CmsAuditLog>.GetPropertyBySelector( item => item.IPAddress),
        						isEditable: true, isVisible: true);

        this.ObjectName = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsAuditLogModule.CmsAuditLog>.GetPropertyBySelector( item => item.ObjectName),
        						isEditable: true, isVisible: true);

        this.Remarks = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsAuditLogModule.CmsAuditLog>.GetPropertyBySelector( item => item.Remarks),
        						isEditable: true, isVisible: true);

        this.ItemID = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsAuditLogModule.CmsAuditLog>.GetPropertyBySelector( item => item.ItemID),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
