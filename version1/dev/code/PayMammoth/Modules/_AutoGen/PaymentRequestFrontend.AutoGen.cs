using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.PaymentRequestModule;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.Modules._AutoGen;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.PaymentRequestModule.PaymentRequestFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.PaymentRequestModule.IPaymentRequest> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.PaymentRequestModule.PaymentRequestFrontend ToFrontend(this PayMammoth.Modules.PaymentRequestModule.IPaymentRequest item)
        {
        	return PayMammoth.Frontend.PaymentRequestModule.PaymentRequestFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaymentRequestFrontend_AutoGen : PayMammoth.Modules._AutoGen.PaymentRequestModule.PaymentRequestBaseFrontend

    {
		
        
        protected PaymentRequestFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.PaymentRequestModule.IPaymentRequest Data
        {
            get { return (PayMammoth.Modules.PaymentRequestModule.IPaymentRequest)base.Data; }
            set { base.Data = value; }

        }

        public new static PaymentRequestFrontend CreateNewItem()
        {
            return (PaymentRequestFrontend)PayMammoth.Modules._AutoGen.PaymentRequestModule.PaymentRequestBaseFrontend.CreateNewItem();
        }
        
        public new static PaymentRequestFrontend Get(PayMammoth.Modules._AutoGen.IPaymentRequestBase data)
        {
            return (PaymentRequestFrontend)PayMammoth.Modules._AutoGen.PaymentRequestModule.PaymentRequestBaseFrontend.Get(data);
        }
        public new static List<PaymentRequestFrontend> GetList(IEnumerable<PayMammoth.Modules._AutoGen.IPaymentRequestBase> dataList)
        {
            return PayMammoth.Modules._AutoGen.PaymentRequestModule.PaymentRequestBaseFrontend.GetList(dataList).Cast<PaymentRequestFrontend>().ToList();
        }


    }
}
