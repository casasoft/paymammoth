using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.TicketModule;
using PayMammoth.Modules.TicketModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface ITicketFactoryAutoGen : BusinessLogic_v3.Modules.TicketModule.ITicketBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<ITicket>
    
    {
	    new PayMammoth.Modules.TicketModule.ITicket CreateNewItem();
        new PayMammoth.Modules.TicketModule.ITicket GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.TicketModule.ITicket> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.TicketModule.ITicket> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.TicketModule.ITicket> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.TicketModule.ITicket FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.TicketModule.ITicket FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.TicketModule.ITicket FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.TicketModule.ITicket> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
