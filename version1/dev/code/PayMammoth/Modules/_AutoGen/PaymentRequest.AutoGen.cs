using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.Modules._AutoGen;

//DbObjectBuilder v8

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{    

	
    public abstract class PaymentRequest_AutoGen : PaymentRequestBase, PayMammoth.Modules._AutoGen.IPaymentRequestAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount WebsiteAccount
        {
            get
            {
                return (PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount)base.WebsiteAccount;
            }
            set
            {
                base.WebsiteAccount = value;
            }
        }

		PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount IPaymentRequestAutoGen.WebsiteAccount
        {
            get
            {
            	return this.WebsiteAccount;
                
            }
            set
            {
                this.WebsiteAccount = (PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount)value;
            }
        }
		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction CurrentTransaction
        {
            get
            {
                return (PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction)base.CurrentTransaction;
            }
            set
            {
                base.CurrentTransaction = value;
            }
        }

		PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction IPaymentRequestAutoGen.CurrentTransaction
        {
            get
            {
            	return this.CurrentTransaction;
                
            }
            set
            {
                this.CurrentTransaction = (PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction)value;
            }
        }



// [userclass_collections_nhibernate]


        
          
		public  static new PaymentRequestFactory Factory
        {
            get
            {
                return (PaymentRequestFactory)PaymentRequestBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<PaymentRequest, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        //UserClassOnly_CollectionOneToManyLeftSide
        
        
        public class __ItemDetailsCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.PaymentRequestModule.PaymentRequest, 
        	PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetail,
        	PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.PaymentRequestModule.PaymentRequest, PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetail>
        {
            private PayMammoth.Modules.PaymentRequestModule.PaymentRequest _item = null;
            public __ItemDetailsCollectionInfo(PayMammoth.Modules.PaymentRequestModule.PaymentRequest item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetail> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetail>)_item.__collection__ItemDetails; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.PaymentRequestModule.PaymentRequest, PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetail>.SetLinkOnItem(PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetail item, PayMammoth.Modules.PaymentRequestModule.PaymentRequest value)
            {
                item.PaymentRequest = value;
            }
        }
        
        private __ItemDetailsCollectionInfo _ItemDetails = null;
        public __ItemDetailsCollectionInfo ItemDetails
        {
            get
            {
                if (_ItemDetails == null)
                    _ItemDetails = new __ItemDetailsCollectionInfo((PayMammoth.Modules.PaymentRequestModule.PaymentRequest)this);
                return _ItemDetails;
            }
        }

   
		ICollectionManager<PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetail> IPaymentRequestAutoGen.ItemDetails
        {
            get {  return this.ItemDetails; }
        }
   
        protected virtual IEnumerable<PayMammoth.Modules._AutoGen.PaymentRequestItemDetailBase> __collection__ItemDetails
        {
            get { throw new InvalidOperationException("This must be defined in the final implementation"); }
        }
        //UserClassOnly_CollectionOneToManyLeftSide
        
        
        public class __TransactionsCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.PaymentRequestModule.PaymentRequest, 
        	PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction,
        	PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.PaymentRequestModule.PaymentRequest, PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction>
        {
            private PayMammoth.Modules.PaymentRequestModule.PaymentRequest _item = null;
            public __TransactionsCollectionInfo(PayMammoth.Modules.PaymentRequestModule.PaymentRequest item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction>)_item.__collection__Transactions; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.PaymentRequestModule.PaymentRequest, PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction>.SetLinkOnItem(PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction item, PayMammoth.Modules.PaymentRequestModule.PaymentRequest value)
            {
                item.PaymentRequest = value;
            }
        }
        
        private __TransactionsCollectionInfo _Transactions = null;
        public __TransactionsCollectionInfo Transactions
        {
            get
            {
                if (_Transactions == null)
                    _Transactions = new __TransactionsCollectionInfo((PayMammoth.Modules.PaymentRequestModule.PaymentRequest)this);
                return _Transactions;
            }
        }

   
		ICollectionManager<PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction> IPaymentRequestAutoGen.Transactions
        {
            get {  return this.Transactions; }
        }
   
        protected virtual IEnumerable<PayMammoth.Modules._AutoGen.PaymentRequestTransactionBase> __collection__Transactions
        {
            get { throw new InvalidOperationException("This must be defined in the final implementation"); }
        }
        
        
        
    }
    
}
