using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.Article_RelatedLinkModule;
using PayMammoth.Modules.Article_RelatedLinkModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class Article_RelatedLinkFactoryAutoGen : BusinessLogic_v3.Modules.Article_RelatedLinkModule.Article_RelatedLinkBaseFactory, 
    				PayMammoth.Modules._AutoGen.IArticle_RelatedLinkFactoryAutoGen,
    				BusinessLogic_v3.Classes.DB.IBaseDbFactory<IArticle_RelatedLink>
    
    {
    
     	public new Article_RelatedLink ReloadItemFromDatabase(BusinessLogic_v3.Modules.Article_RelatedLinkModule.Article_RelatedLinkBase item)
        {
         	return (Article_RelatedLink)base.ReloadItemFromDatabase(item);
        }
     	
        public Article_RelatedLinkFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(Article_RelatedLinkImpl);
			PayMammoth.Cms.Article_RelatedLinkModule.Article_RelatedLinkCmsFactory._initialiseStaticInstance();
        }
        public static new Article_RelatedLinkFactory Instance
        {
            get
            {
                return (Article_RelatedLinkFactory)Article_RelatedLinkBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<Article_RelatedLink> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<Article_RelatedLink>();
            
        }
        public new IEnumerable<Article_RelatedLink> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<Article_RelatedLink>();
            
            
        }
    
        public new IEnumerable<Article_RelatedLink> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<Article_RelatedLink>();
            
        }
        public new IEnumerable<Article_RelatedLink> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<Article_RelatedLink>();
        }
        
        public new Article_RelatedLink FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (Article_RelatedLink)base.FindItem(query);
        }
        public new Article_RelatedLink FindItem(IQueryOver query)
        {
            return (Article_RelatedLink)base.FindItem(query);
        }
        
        IArticle_RelatedLink IArticle_RelatedLinkFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new Article_RelatedLink CreateNewItem()
        {
            return (Article_RelatedLink)base.CreateNewItem();
        }

        public new Article_RelatedLink GetByPrimaryKey(long pKey)
        {
            return (Article_RelatedLink)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<Article_RelatedLink, Article_RelatedLink> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<Article_RelatedLink>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.Article_RelatedLinkModule.Article_RelatedLink GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.Article_RelatedLinkModule.Article_RelatedLink) base.GetByPrimaryKey(id, session);
        }
       public new IEnumerable<PayMammoth.Modules.Article_RelatedLinkModule.Article_RelatedLink> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.Article_RelatedLinkModule.Article_RelatedLink>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.Article_RelatedLinkModule.Article_RelatedLink> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.Article_RelatedLinkModule.Article_RelatedLink>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.Article_RelatedLinkModule.Article_RelatedLink> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.Article_RelatedLinkModule.Article_RelatedLink>) base.FindAll(session);

       }
       public new PayMammoth.Modules.Article_RelatedLinkModule.Article_RelatedLink FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.Article_RelatedLinkModule.Article_RelatedLink) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.Article_RelatedLinkModule.Article_RelatedLink FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.Article_RelatedLinkModule.Article_RelatedLink) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.Article_RelatedLinkModule.Article_RelatedLink FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.Article_RelatedLinkModule.Article_RelatedLink) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.Article_RelatedLinkModule.Article_RelatedLink> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.Article_RelatedLinkModule.Article_RelatedLink>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink> Members
     
        PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink IBaseDbFactory<PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink IBaseDbFactory<PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink> IBaseDbFactory<PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink IBaseDbFactory<PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink> IBaseDbFactory<PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IArticle_RelatedLink> IArticle_RelatedLinkFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IArticle_RelatedLink> IArticle_RelatedLinkFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IArticle_RelatedLink> IArticle_RelatedLinkFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IArticle_RelatedLink IArticle_RelatedLinkFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IArticle_RelatedLink IArticle_RelatedLinkFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IArticle_RelatedLink IArticle_RelatedLinkFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IArticle_RelatedLink> IArticle_RelatedLinkFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IArticle_RelatedLink IArticle_RelatedLinkFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
