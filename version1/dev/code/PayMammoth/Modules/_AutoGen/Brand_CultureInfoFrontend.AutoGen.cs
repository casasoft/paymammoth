using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.Brand_CultureInfoModule;
using PayMammoth.Modules.Brand_CultureInfoModule;
using BusinessLogic_v3.Modules.Brand_CultureInfoModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.Brand_CultureInfoModule.Brand_CultureInfoFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.Brand_CultureInfoModule.Brand_CultureInfoFrontend ToFrontend(this PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo item)
        {
        	return PayMammoth.Frontend.Brand_CultureInfoModule.Brand_CultureInfoFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class Brand_CultureInfoFrontend_AutoGen : BusinessLogic_v3.Frontend.Brand_CultureInfoModule.Brand_CultureInfoBaseFrontend

    {
		
        
        protected Brand_CultureInfoFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo Data
        {
            get { return (PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo)base.Data; }
            set { base.Data = value; }

        }

        public new static Brand_CultureInfoFrontend CreateNewItem()
        {
            return (Brand_CultureInfoFrontend)BusinessLogic_v3.Frontend.Brand_CultureInfoModule.Brand_CultureInfoBaseFrontend.CreateNewItem();
        }
        
        public new static Brand_CultureInfoFrontend Get(BusinessLogic_v3.Modules.Brand_CultureInfoModule.IBrand_CultureInfoBase data)
        {
            return (Brand_CultureInfoFrontend)BusinessLogic_v3.Frontend.Brand_CultureInfoModule.Brand_CultureInfoBaseFrontend.Get(data);
        }
        public new static List<Brand_CultureInfoFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.Brand_CultureInfoModule.IBrand_CultureInfoBase> dataList)
        {
            return BusinessLogic_v3.Frontend.Brand_CultureInfoModule.Brand_CultureInfoBaseFrontend.GetList(dataList).Cast<Brand_CultureInfoFrontend>().ToList();
        }


    }
}
