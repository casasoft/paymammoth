using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ContentTextModule;
using PayMammoth.Modules.ContentTextModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IContentTextFactoryAutoGen : BusinessLogic_v3.Modules.ContentTextModule.IContentTextBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IContentText>
    
    {
	    new PayMammoth.Modules.ContentTextModule.IContentText CreateNewItem();
        new PayMammoth.Modules.ContentTextModule.IContentText GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null);
        new IEnumerable<PayMammoth.Modules.ContentTextModule.IContentText> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ContentTextModule.IContentText> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ContentTextModule.IContentText> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.ContentTextModule.IContentText FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ContentTextModule.IContentText FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ContentTextModule.IContentText FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ContentTextModule.IContentText> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
