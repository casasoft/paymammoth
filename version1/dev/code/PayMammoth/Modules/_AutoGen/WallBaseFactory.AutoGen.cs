using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
   
    public abstract class WallBaseFactoryAutoGen  : BusinessLogic_v3.Classes.DB.BaseDbFactory<WallBase, IWallBase>, IWallBaseFactoryAutoGen
    
    {
    
        public WallBaseFactoryAutoGen()
        {
        	
            
        }
        

        
        
        public new static WallBaseFactory Instance
        {
            get
            {
                return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<WallBaseFactory>(null);
            }
        }
        
		public IQueryOver<WallBase, WallBase> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
        	
        	var q = session.QueryOver<WallBase>();
            qParams.FillQueryOver(q);
        	postGetQueryOver(q, qParams);
            return q;
            
        } 
        
        
    }
}
