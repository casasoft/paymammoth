using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.Category_ParentChildLinkModule;
using PayMammoth.Modules.Category_ParentChildLinkModule;
using BusinessLogic_v3.Modules.Category_ParentChildLinkModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.Category_ParentChildLinkModule.Category_ParentChildLinkFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.Category_ParentChildLinkModule.Category_ParentChildLinkFrontend ToFrontend(this PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink item)
        {
        	return PayMammoth.Frontend.Category_ParentChildLinkModule.Category_ParentChildLinkFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class Category_ParentChildLinkFrontend_AutoGen : BusinessLogic_v3.Frontend.Category_ParentChildLinkModule.Category_ParentChildLinkBaseFrontend

    {
		
        
        protected Category_ParentChildLinkFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink Data
        {
            get { return (PayMammoth.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLink)base.Data; }
            set { base.Data = value; }

        }

        public new static Category_ParentChildLinkFrontend CreateNewItem()
        {
            return (Category_ParentChildLinkFrontend)BusinessLogic_v3.Frontend.Category_ParentChildLinkModule.Category_ParentChildLinkBaseFrontend.CreateNewItem();
        }
        
        public new static Category_ParentChildLinkFrontend Get(BusinessLogic_v3.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLinkBase data)
        {
            return (Category_ParentChildLinkFrontend)BusinessLogic_v3.Frontend.Category_ParentChildLinkModule.Category_ParentChildLinkBaseFrontend.Get(data);
        }
        public new static List<Category_ParentChildLinkFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.Category_ParentChildLinkModule.ICategory_ParentChildLinkBase> dataList)
        {
            return BusinessLogic_v3.Frontend.Category_ParentChildLinkModule.Category_ParentChildLinkBaseFrontend.GetList(dataList).Cast<Category_ParentChildLinkFrontend>().ToList();
        }


    }
}
