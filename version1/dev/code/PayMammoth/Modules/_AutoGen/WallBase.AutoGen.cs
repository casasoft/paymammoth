using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using CS.General_v3.Classes.DbObjects.Collections;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using CS.General_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using PayMammoth.Modules._AutoGen;                

//BaseClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
	
    public abstract class WallBaseAutoGen 
    : BusinessLogic_v3.Classes.DB.BaseDbObject, IWallBaseAutoGen
    
      
      
      
    
    
    		
    {
    
   
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	
        public static PayMammoth.Modules._AutoGen.WallBaseCmsFactory CmsFactory
        {
            get { return PayMammoth.Modules._AutoGen.WallBaseCmsFactory.Instance; }
        }
        
     	public static WallBaseFactory Factory
        {
            get
            {
                return WallBaseFactory.Instance; 
            }
        }    
        /*
        public static WallBase CreateNewItem()
        {
            return (WallBase)Factory.CreateNewItem();
        }

		public static WallBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<WallBase, WallBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static WallBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static WallBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<WallBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<WallBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<WallBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _title;
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _description;
        public virtual string Description 
        {
        	get
        	{
        		return _description;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_description,value);
        		_description = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _startdate;
        public virtual DateTime StartDate 
        {
        	get
        	{
        		return _startdate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_startdate,value);
        		_startdate = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _closedon;
        public virtual DateTime? ClosedOn 
        {
        	get
        	{
        		return _closedon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_closedon,value);
        		_closedon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _lotterydrawdate;
        public virtual DateTime? LotteryDrawDate 
        {
        	get
        	{
        		return _lotterydrawdate;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_lotterydrawdate,value);
        		_lotterydrawdate = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _starsrequiredforticket;
        public virtual double StarsRequiredForTicket 
        {
        	get
        	{
        		return _starsrequiredforticket;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_starsrequiredforticket,value);
        		_starsrequiredforticket = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _isfinalised;
        public virtual bool IsFinalised 
        {
        	get
        	{
        		return _isfinalised;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_isfinalised,value);
        		_isfinalised = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _finalisedon;
        public virtual DateTime? FinalisedOn 
        {
        	get
        	{
        		return _finalisedon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_finalisedon,value);
        		_finalisedon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _lotteryhasbeendrawn;
        public virtual bool LotteryHasBeenDrawn 
        {
        	get
        	{
        		return _lotteryhasbeendrawn;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_lotteryhasbeendrawn,value);
        		_lotteryhasbeendrawn = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    
#region Multilingual
          


#endregion    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
