using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ArticleMediaItemModule;
using PayMammoth.Modules.ArticleMediaItemModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IArticleMediaItemFactoryAutoGen : BusinessLogic_v3.Modules.ArticleMediaItemModule.IArticleMediaItemBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IArticleMediaItem>
    
    {
	    new PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem CreateNewItem();
        new PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
