using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;


namespace PayMammoth.Modules._AutoGen
{
   
    public interface IMemberWallTicketBaseFactoryAutoGen : CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules._AutoGen.IMemberWallTicketBase>
    {
    
    }
}
