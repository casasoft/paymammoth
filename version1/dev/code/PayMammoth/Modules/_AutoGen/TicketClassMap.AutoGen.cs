using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.TicketModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class TicketMap_AutoGen : TicketMap_AutoGen_Z
    {
        public TicketMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.IPAddress, IPAddressMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CreatedOn, CreatedOnMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.AssignedTo, AssignedToMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ClosedOn, ClosedOnMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.ClosedBy, ClosedByMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ClosedByIP, ClosedByIPMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ReOpenedOn, ReOpenedOnMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.ReOpenedByCmsUser, ReOpenedByCmsUserMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ReOpenedByIP, ReOpenedByIPMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Title, TitleMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.TicketPriority, TicketPriorityMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.TicketState, TicketStateMappingInfo);



// [userclassmap_initcollections]

//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.TicketComments, TicketCommentsMappingInfo, BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class TicketMap_AutoGen_Z : BusinessLogic_v3.Modules.TicketModule.TicketBaseMap<TicketImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
