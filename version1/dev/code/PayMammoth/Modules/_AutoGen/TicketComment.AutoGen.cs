using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.TicketCommentModule;
using BusinessLogic_v3.Modules.TicketCommentModule;


//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
    public abstract class TicketComment_AutoGen : TicketCommentBase, PayMammoth.Modules._AutoGen.ITicketCommentAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.TicketModule.Ticket Ticket
        {
            get
            {
                return (PayMammoth.Modules.TicketModule.Ticket)base.Ticket;
            }
            set
            {
                base.Ticket = value;
            }
        }

		PayMammoth.Modules.TicketModule.ITicket ITicketCommentAutoGen.Ticket
        {
            get
            {
            	return this.Ticket;
                
            }
            set
            {
                this.Ticket = (PayMammoth.Modules.TicketModule.Ticket)value;
            }
        }
		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.CmsUserModule.CmsUser AssignedStaffUser
        {
            get
            {
                return (PayMammoth.Modules.CmsUserModule.CmsUser)base.AssignedStaffUser;
            }
            set
            {
                base.AssignedStaffUser = value;
            }
        }

		PayMammoth.Modules.CmsUserModule.ICmsUser ITicketCommentAutoGen.AssignedStaffUser
        {
            get
            {
            	return this.AssignedStaffUser;
                
            }
            set
            {
                this.AssignedStaffUser = (PayMammoth.Modules.CmsUserModule.CmsUser)value;
            }
        }



// [userclass_collections_nhibernate]

        //UserClass_CollectionOneToManyLeftSide
        
		new internal class __AttachmentsCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.TicketCommentModule.TicketComment, 
        	PayMammoth.Modules.AttachmentModule.Attachment,
        	PayMammoth.Modules.AttachmentModule.IAttachment>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.TicketCommentModule.TicketComment, PayMammoth.Modules.AttachmentModule.Attachment>
        {
            private PayMammoth.Modules.TicketCommentModule.TicketComment _item = null;
            public __AttachmentsCollectionInfo(PayMammoth.Modules.TicketCommentModule.TicketComment item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.AttachmentModule.Attachment> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.AttachmentModule.Attachment>)_item.__collection__Attachments; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.TicketCommentModule.TicketComment, PayMammoth.Modules.AttachmentModule.Attachment>.SetLinkOnItem(PayMammoth.Modules.AttachmentModule.Attachment item, PayMammoth.Modules.TicketCommentModule.TicketComment value)
            {
                item.TicketComment = value;
            }
        }
        
        private __AttachmentsCollectionInfo _Attachments = null;
        internal new __AttachmentsCollectionInfo Attachments
        {
            get
            {
                if (_Attachments == null)
                    _Attachments = new __AttachmentsCollectionInfo((PayMammoth.Modules.TicketCommentModule.TicketComment)this);
                return _Attachments;
            }
        }               
           
		ICollectionManager<PayMammoth.Modules.AttachmentModule.IAttachment> ITicketCommentAutoGen.Attachments
        {
            get {  return this.Attachments; }
        }
           

        
          
		public  static new TicketCommentFactory Factory
        {
            get
            {
                return (TicketCommentFactory)TicketCommentBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<TicketComment, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
