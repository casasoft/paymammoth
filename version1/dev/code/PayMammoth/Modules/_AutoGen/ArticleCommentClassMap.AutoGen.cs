using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.ArticleCommentModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class ArticleCommentMap_AutoGen : ArticleCommentMap_AutoGen_Z
    {
        public ArticleCommentMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.Email, EmailMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Comment, CommentMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.IPAddress, IPAddressMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PostedOn, PostedOnMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.Article, ArticleMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Author, AuthorMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Date, DateMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Company, CompanyMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.IsApproved, IsApprovedMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class ArticleCommentMap_AutoGen_Z : BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBaseMap<ArticleCommentImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
