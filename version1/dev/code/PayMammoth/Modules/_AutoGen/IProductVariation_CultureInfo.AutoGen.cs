using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule;
using PayMammoth.Modules.ProductVariation_CultureInfoModule;

//IUserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
	
    public interface IProductVariation_CultureInfoAutoGen : BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfoBase
    {




// [interface_userclass_properties]

		//IProperty_LinkedToObject
        new PayMammoth.Modules.CultureDetailsModule.ICultureDetails CultureInfo {get; set; }

		//IProperty_LinkedToObject
        new PayMammoth.Modules.ProductVariationModule.IProductVariation ProductVariation {get; set; }

   
    

// [interface_userclassonly_properties]

   

                                     

// [interface_userclass_collections_nhibernate]




// [interface_userclassonly_collections]

 


    	
    	
      
      

    }
}
