using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;

//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
   
    public abstract class WebsiteAccount_CultureInfoBaseFactoryAutoGen  : BusinessLogic_v3.Classes.DbObjects.Factories.BaseDbFactory<WebsiteAccount_CultureInfoBase, IWebsiteAccount_CultureInfoBase>, IWebsiteAccount_CultureInfoBaseFactoryAutoGen
    
    {
    
        public WebsiteAccount_CultureInfoBaseFactoryAutoGen()
        {
        	
            
        }
        
		static WebsiteAccount_CultureInfoBaseFactoryAutoGen()
        {
            
        }
        
        
        public new static WebsiteAccount_CultureInfoBaseFactory Instance
        {
            get
            {
                return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<WebsiteAccount_CultureInfoBaseFactory>(null);
            }
        }
        
		public IQueryOver<WebsiteAccount_CultureInfoBase, WebsiteAccount_CultureInfoBase> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
        	ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<WebsiteAccount_CultureInfoBase>();
            qParams.FillQueryOver(q, this);
        	postGetQueryOver(q, qParams);
            return q;
            
        } 
        
        
    }
}
