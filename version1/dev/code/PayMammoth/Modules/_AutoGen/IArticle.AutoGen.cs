using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ArticleModule;
using PayMammoth.Modules.ArticleModule;

//IUserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
	
    public interface IArticleAutoGen : BusinessLogic_v3.Modules.ArticleModule.IArticleBase
    {




// [interface_userclass_properties]

		//IProperty_LinkedToObject
        new PayMammoth.Modules.RoutingInfoModule.IRoutingInfo LinkedRoute {get; set; }

   
    

// [interface_userclassonly_properties]

		//Iproperty_normal
        long CustomProjOnlyField { get; set; }
		//Iproperty_normal
        long CustomProjOnlyLinkedObject { get; set; }
   

                                     

// [interface_userclass_collections_nhibernate]

        //UserClass_CollectionOneToManyLeftSide
        
		new ICollectionManager<PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo> Article_CultureInfos { get; }

        //UserClass_CollectionOneToManyLeftSide
        
		new ICollectionManager<PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink> ChildArticleLinks { get; }

        //UserClass_CollectionOneToManyLeftSide
        
		new ICollectionManager<PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink> ParentArticleLinks { get; }




// [interface_userclassonly_collections]

 


    	
    	
      
      

    }
}
