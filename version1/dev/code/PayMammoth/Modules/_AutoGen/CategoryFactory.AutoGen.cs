using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.CategoryModule;
using PayMammoth.Modules.CategoryModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class CategoryFactoryAutoGen : BusinessLogic_v3.Modules.CategoryModule.CategoryBaseFactory, 
    				PayMammoth.Modules._AutoGen.ICategoryFactoryAutoGen,
    				BusinessLogic_v3.Classes.DB.IBaseDbFactory<ICategory>
    
    {
    
     	public new Category ReloadItemFromDatabase(BusinessLogic_v3.Modules.CategoryModule.CategoryBase item)
        {
         	return (Category)base.ReloadItemFromDatabase(item);
        }
     	
        public CategoryFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(CategoryImpl);
			PayMammoth.Cms.CategoryModule.CategoryCmsFactory._initialiseStaticInstance();
        }
        public static new CategoryFactory Instance
        {
            get
            {
                return (CategoryFactory)CategoryBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<Category> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<Category>();
            
        }
        public new IEnumerable<Category> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<Category>();
            
            
        }
    
        public new IEnumerable<Category> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<Category>();
            
        }
        public new IEnumerable<Category> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<Category>();
        }
        
        public new Category FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (Category)base.FindItem(query);
        }
        public new Category FindItem(IQueryOver query)
        {
            return (Category)base.FindItem(query);
        }
        
        ICategory ICategoryFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new Category CreateNewItem()
        {
            return (Category)base.CreateNewItem();
        }

        public new Category GetByPrimaryKey(long pKey)
        {
            return (Category)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<Category, Category> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<Category>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

            var itemList = Category.Factory.FindAll();

            var CultureInfos = CultureDetails.Factory.FindAll();

            foreach (var item in itemList)
            {
            	item.__CheckItemCultureInfos(autoSaveInDB: true, CultureInfos: CultureInfos);
            }

  

        }        


#endregion
        public new PayMammoth.Modules.CategoryModule.Category GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.CategoryModule.Category) base.GetByPrimaryKey(id, session);
        }
       public new IEnumerable<PayMammoth.Modules.CategoryModule.Category> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.CategoryModule.Category>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.CategoryModule.Category> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.CategoryModule.Category>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.CategoryModule.Category> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.CategoryModule.Category>) base.FindAll(session);

       }
       public new PayMammoth.Modules.CategoryModule.Category FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.CategoryModule.Category) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.CategoryModule.Category FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.CategoryModule.Category) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.CategoryModule.Category FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.CategoryModule.Category) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.CategoryModule.Category> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.CategoryModule.Category>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.CategoryModule.ICategory> Members
     
        PayMammoth.Modules.CategoryModule.ICategory IBaseDbFactory<PayMammoth.Modules.CategoryModule.ICategory>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.CategoryModule.ICategory IBaseDbFactory<PayMammoth.Modules.CategoryModule.ICategory>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.CategoryModule.ICategory> IBaseDbFactory<PayMammoth.Modules.CategoryModule.ICategory>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.CategoryModule.ICategory> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.CategoryModule.ICategory>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.CategoryModule.ICategory> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.CategoryModule.ICategory>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.CategoryModule.ICategory IBaseDbFactory<PayMammoth.Modules.CategoryModule.ICategory>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.CategoryModule.ICategory BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.CategoryModule.ICategory>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.CategoryModule.ICategory BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.CategoryModule.ICategory>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.CategoryModule.ICategory> IBaseDbFactory<PayMammoth.Modules.CategoryModule.ICategory>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<ICategory> ICategoryFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<ICategory> ICategoryFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<ICategory> ICategoryFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       ICategory ICategoryFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       ICategory ICategoryFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       ICategory ICategoryFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<ICategory> ICategoryFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       ICategory ICategoryFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
