using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.NHibernateClasses;

using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ProductVariationMediaItemModule;
using PayMammoth.Modules.ProductVariationMediaItemModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IProductVariationMediaItemFactoryAutoGen : BusinessLogic_v3.Modules.ProductVariationMediaItemModule.IProductVariationMediaItemBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IProductVariationMediaItem>
    
    {
	    new PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem CreateNewItem();
        new PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
