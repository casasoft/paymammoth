using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen.PaymentRequestItemDetailModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaymentRequestItemDetailBaseCmsInfo_AutoGen : CmsItemInfoDbBase<PayMammoth.Modules._AutoGen.PaymentRequestItemDetailBase>
    {
		public PaymentRequestItemDetailBaseCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.PaymentRequestItemDetailBase dbItem)
            : base(PayMammoth.Modules._AutoGen.PaymentRequestItemDetailBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new PaymentRequestItemDetailBaseFrontend FrontendItem
        {
            get { return (PaymentRequestItemDetailBaseFrontend)base.FrontendItem; }

        }
        public new PaymentRequestItemDetailBase DbItem
        {
            get
            {
                return (PaymentRequestItemDetailBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }

        public CmsPropertyInfo PaymentRequest { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
