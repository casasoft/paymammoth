using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using CS.General_v3.Classes.DbObjects.Collections;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using CS.General_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using PayMammoth.Modules._AutoGen;                

//BaseClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
	
    public abstract class PaymentRequestItemBaseAutoGen 
    : BusinessLogic_v3.Classes.DB.BaseDbObject, IPaymentRequestItemBaseAutoGen
    
      
      
      
    
    
    		
    {
    
   
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	
        public static PayMammoth.Modules._AutoGen.PaymentRequestItemBaseCmsFactory CmsFactory
        {
            get { return PayMammoth.Modules._AutoGen.PaymentRequestItemBaseCmsFactory.Instance; }
        }
        
     	public static PaymentRequestItemBaseFactory Factory
        {
            get
            {
                return PaymentRequestItemBaseFactory.Instance; 
            }
        }    
        /*
        public static PaymentRequestItemBase CreateNewItem()
        {
            return (PaymentRequestItemBase)Factory.CreateNewItem();
        }

		public static PaymentRequestItemBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<PaymentRequestItemBase, PaymentRequestItemBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static PaymentRequestItemBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static PaymentRequestItemBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<PaymentRequestItemBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<PaymentRequestItemBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<PaymentRequestItemBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private int _quantity;
        public virtual int Quantity 
        {
        	get
        	{
        		return _quantity;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_quantity,value);
        		_quantity = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _priceexctax;
        public virtual double PriceExcTax 
        {
        	get
        	{
        		return _priceexctax;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_priceexctax,value);
        		_priceexctax = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _taxamount;
        public virtual double TaxAmount 
        {
        	get
        	{
        		return _taxamount;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_taxamount,value);
        		_taxamount = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _shippingamount;
        public virtual double ShippingAmount 
        {
        	get
        	{
        		return _shippingamount;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_shippingamount,value);
        		_shippingamount = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _handlingamount;
        public virtual double HandlingAmount 
        {
        	get
        	{
        		return _handlingamount;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_handlingamount,value);
        		_handlingamount = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _discount;
        public virtual double Discount 
        {
        	get
        	{
        		return _discount;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_discount,value);
        		_discount = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _title;
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _description;
        public virtual string Description 
        {
        	get
        	{
        		return _description;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_description,value);
        		_description = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _widthinmm;
        public virtual double WidthInMm 
        {
        	get
        	{
        		return _widthinmm;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_widthinmm,value);
        		_widthinmm = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _heightinmm;
        public virtual double HeightInMm 
        {
        	get
        	{
        		return _heightinmm;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_heightinmm,value);
        		_heightinmm = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _lengthinmm;
        public virtual double LengthInMm 
        {
        	get
        	{
        		return _lengthinmm;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_lengthinmm,value);
        		_lengthinmm = value;
        		
        	}
        }
        
/*
		public virtual long? PaymentRequestID
		{
		 	get 
		 	{
		 		return (this.PaymentRequest != null ? (long?)this.PaymentRequest.ID : null);
		 	}
		}*/
		
        protected PayMammoth.Modules._AutoGen.PaymentRequestBase _paymentrequest;
        public virtual PayMammoth.Modules._AutoGen.PaymentRequestBase PaymentRequest 
        {
        	get
        	{
        		return _paymentrequest;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentrequest,value);
        		_paymentrequest = value;
        	}
        }
            
        PayMammoth.Modules._AutoGen.IPaymentRequestBase PayMammoth.Modules._AutoGen.IPaymentRequestItemBaseAutoGen.PaymentRequest 
        {
            get
            {
            	return this.PaymentRequest;
            }
            set
            {
            	this.PaymentRequest = (PayMammoth.Modules._AutoGen.PaymentRequestBase) value;
            }
        }
            
 //check Property_Pkey, etc


// [base_collections]


    
#region Multilingual
          


#endregion    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
