using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.Article_ParentChildLinkModule;
using PayMammoth.Cms.Article_ParentChildLinkModule;
using CS.WebComponentsGeneralV3.Cms.Article_ParentChildLinkModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class Article_ParentChildLinkCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.Article_ParentChildLinkModule.Article_ParentChildLinkBaseCmsFactory
    {
        public static new Article_ParentChildLinkBaseCmsFactory Instance 
        {
         	get { return (Article_ParentChildLinkBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.Article_ParentChildLinkModule.Article_ParentChildLinkBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override Article_ParentChildLinkBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	Article_ParentChildLink item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = Article_ParentChildLink.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        Article_ParentChildLinkCmsInfo cmsItem = new Article_ParentChildLinkCmsInfo(item);
            return cmsItem;
        }    
        
        public override Article_ParentChildLinkBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.Article_ParentChildLinkModule.Article_ParentChildLinkBase item)
        {
            return new Article_ParentChildLinkCmsInfo((Article_ParentChildLink)item);
            
        }

    }
}
