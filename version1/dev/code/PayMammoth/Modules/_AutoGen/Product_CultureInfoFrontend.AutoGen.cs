using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.Product_CultureInfoModule;
using PayMammoth.Modules.Product_CultureInfoModule;
using BusinessLogic_v3.Modules.Product_CultureInfoModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.Product_CultureInfoModule.Product_CultureInfoFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.Product_CultureInfoModule.IProduct_CultureInfo> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.Product_CultureInfoModule.Product_CultureInfoFrontend ToFrontend(this PayMammoth.Modules.Product_CultureInfoModule.IProduct_CultureInfo item)
        {
        	return PayMammoth.Frontend.Product_CultureInfoModule.Product_CultureInfoFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class Product_CultureInfoFrontend_AutoGen : BusinessLogic_v3.Frontend.Product_CultureInfoModule.Product_CultureInfoBaseFrontend

    {
		
        
        protected Product_CultureInfoFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.Product_CultureInfoModule.IProduct_CultureInfo Data
        {
            get { return (PayMammoth.Modules.Product_CultureInfoModule.IProduct_CultureInfo)base.Data; }
            set { base.Data = value; }

        }

        public new static Product_CultureInfoFrontend CreateNewItem()
        {
            return (Product_CultureInfoFrontend)BusinessLogic_v3.Frontend.Product_CultureInfoModule.Product_CultureInfoBaseFrontend.CreateNewItem();
        }
        
        public new static Product_CultureInfoFrontend Get(BusinessLogic_v3.Modules.Product_CultureInfoModule.IProduct_CultureInfoBase data)
        {
            return (Product_CultureInfoFrontend)BusinessLogic_v3.Frontend.Product_CultureInfoModule.Product_CultureInfoBaseFrontend.Get(data);
        }
        public new static List<Product_CultureInfoFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.Product_CultureInfoModule.IProduct_CultureInfoBase> dataList)
        {
            return BusinessLogic_v3.Frontend.Product_CultureInfoModule.Product_CultureInfoBaseFrontend.GetList(dataList).Cast<Product_CultureInfoFrontend>().ToList();
        }


    }
}
