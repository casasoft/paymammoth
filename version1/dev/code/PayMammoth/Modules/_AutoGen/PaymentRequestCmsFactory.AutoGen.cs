using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.Cms.PaymentRequestModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaymentRequestCmsFactory_AutoGen : PayMammoth.Modules._AutoGen.PaymentRequestBaseCmsFactory
    {
        public static new PaymentRequestBaseCmsFactory Instance 
        {
         	get { return (PaymentRequestBaseCmsFactory)PayMammoth.Modules._AutoGen.PaymentRequestBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override PaymentRequestBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	PaymentRequest item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = PaymentRequest.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        PaymentRequestCmsInfo cmsItem = new PaymentRequestCmsInfo(item);
            return cmsItem;
        }    
        
        public override PaymentRequestBaseCmsInfo CreateCmsItemInfo(PayMammoth.Modules._AutoGen.PaymentRequestBase item)
        {
            return new PaymentRequestCmsInfo((PaymentRequest)item);
            
        }

    }
}
