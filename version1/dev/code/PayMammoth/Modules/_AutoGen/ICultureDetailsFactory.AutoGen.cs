using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using PayMammoth.Modules.CultureDetailsModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface ICultureDetailsFactoryAutoGen : BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<ICultureDetails>
    
    {
	    new PayMammoth.Modules.CultureDetailsModule.ICultureDetails CreateNewItem();
        new PayMammoth.Modules.CultureDetailsModule.ICultureDetails GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null);
        new IEnumerable<PayMammoth.Modules.CultureDetailsModule.ICultureDetails> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.CultureDetailsModule.ICultureDetails> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.CultureDetailsModule.ICultureDetails> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.CultureDetailsModule.ICultureDetails FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.CultureDetailsModule.ICultureDetails FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.CultureDetailsModule.ICultureDetails FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.CultureDetailsModule.ICultureDetails> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
