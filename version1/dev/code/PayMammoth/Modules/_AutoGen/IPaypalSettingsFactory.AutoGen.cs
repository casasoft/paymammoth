using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.PaypalSettingsModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IPaypalSettingsFactoryAutoGen : PayMammoth.Modules._AutoGen.IPaypalSettingsBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IPaypalSettings>
    
    {
	    new PayMammoth.Modules.PaypalSettingsModule.IPaypalSettings CreateNewItem();
        new PayMammoth.Modules.PaypalSettingsModule.IPaypalSettings GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.PaypalSettingsModule.IPaypalSettings> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.PaypalSettingsModule.IPaypalSettings> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.PaypalSettingsModule.IPaypalSettings> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.PaypalSettingsModule.IPaypalSettings FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.PaypalSettingsModule.IPaypalSettings FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.PaypalSettingsModule.IPaypalSettings FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.PaypalSettingsModule.IPaypalSettings> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
