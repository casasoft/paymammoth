using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.SettingModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class SettingMap_AutoGen : SettingMap_AutoGen_Z
    {
        public SettingMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.Name, NameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Value, ValueMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.VisibleInCMS_AccessRequired, VisibleInCMS_AccessRequiredMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DataType, DataTypeMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.Parent, ParentMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Identifier, IdentifierMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Description, DescriptionMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.UsedInProject, UsedInProjectMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.LocalhostValue, LocalhostValueMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.EnumType, EnumTypeMappingInfo);



// [userclassmap_initcollections]

//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.ChildSettings, ChildSettingsMappingInfo, BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class SettingMap_AutoGen_Z : BusinessLogic_v3.Modules.SettingModule.SettingBaseMap<SettingImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
