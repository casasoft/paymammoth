using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.CategoryModule;
using PayMammoth.Modules.CategoryModule;
using BusinessLogic_v3.Modules.CategoryModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.CategoryModule.CategoryFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.CategoryModule.ICategory> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.CategoryModule.CategoryFrontend ToFrontend(this PayMammoth.Modules.CategoryModule.ICategory item)
        {
        	return PayMammoth.Frontend.CategoryModule.CategoryFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class CategoryFrontend_AutoGen : BusinessLogic_v3.Frontend.CategoryModule.CategoryBaseFrontend

    {
		
        
        protected CategoryFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.CategoryModule.ICategory Data
        {
            get { return (PayMammoth.Modules.CategoryModule.ICategory)base.Data; }
            set { base.Data = value; }

        }

        public new static CategoryFrontend CreateNewItem()
        {
            return (CategoryFrontend)BusinessLogic_v3.Frontend.CategoryModule.CategoryBaseFrontend.CreateNewItem();
        }
        
        public new static CategoryFrontend Get(BusinessLogic_v3.Modules.CategoryModule.ICategoryBase data)
        {
            return (CategoryFrontend)BusinessLogic_v3.Frontend.CategoryModule.CategoryBaseFrontend.Get(data);
        }
        public new static List<CategoryFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.CategoryModule.ICategoryBase> dataList)
        {
            return BusinessLogic_v3.Frontend.CategoryModule.CategoryBaseFrontend.GetList(dataList).Cast<CategoryFrontend>().ToList();
        }


    }
}
