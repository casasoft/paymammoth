using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.Article_ParentChildLinkModule;
using BusinessLogic_v3.Modules.Article_ParentChildLinkModule;

//DbObjectBuilder v8

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{    

	
    public abstract class Article_ParentChildLink_AutoGen : Article_ParentChildLinkBase, PayMammoth.Modules._AutoGen.IArticle_ParentChildLinkAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.ArticleModule.Article Parent
        {
            get
            {
                return (PayMammoth.Modules.ArticleModule.Article)base.Parent;
            }
            set
            {
                base.Parent = value;
            }
        }

		PayMammoth.Modules.ArticleModule.IArticle IArticle_ParentChildLinkAutoGen.Parent
        {
            get
            {
            	return this.Parent;
                
            }
            set
            {
                this.Parent = (PayMammoth.Modules.ArticleModule.Article)value;
            }
        }
		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.ArticleModule.Article Child
        {
            get
            {
                return (PayMammoth.Modules.ArticleModule.Article)base.Child;
            }
            set
            {
                base.Child = value;
            }
        }

		PayMammoth.Modules.ArticleModule.IArticle IArticle_ParentChildLinkAutoGen.Child
        {
            get
            {
            	return this.Child;
                
            }
            set
            {
                this.Child = (PayMammoth.Modules.ArticleModule.Article)value;
            }
        }



// [userclass_collections_nhibernate]


        
          
		public  static new Article_ParentChildLinkFactory Factory
        {
            get
            {
                return (Article_ParentChildLinkFactory)Article_ParentChildLinkBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<Article_ParentChildLink, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
