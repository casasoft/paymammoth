using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Modules._AutoGen.PaymentRequestTransactionModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Modules._AutoGen.PaymentRequestTransactionModule.PaymentRequestTransactionBaseFrontend> ToFrontendBaseList(this IEnumerable<PayMammoth.Modules._AutoGen.IPaymentRequestTransactionBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static PayMammoth.Modules._AutoGen.PaymentRequestTransactionModule.PaymentRequestTransactionBaseFrontend ToFrontendBase(this PayMammoth.Modules._AutoGen.IPaymentRequestTransactionBase item)
        {
        	return PayMammoth.Modules._AutoGen.PaymentRequestTransactionModule.PaymentRequestTransactionBaseFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaymentRequestTransactionBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<PaymentRequestTransactionBaseFrontend, PayMammoth.Modules._AutoGen.IPaymentRequestTransactionBase>

    {
		
        
        protected PaymentRequestTransactionBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
