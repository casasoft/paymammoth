using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.OrderModule;
using CS.WebComponentsGeneralV3.Cms.OrderModule;
using PayMammoth.Frontend.OrderModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class OrderCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.OrderModule.OrderBaseCmsInfo
    {

		public OrderCmsInfo_AutoGen(BusinessLogic_v3.Modules.OrderModule.OrderBase item)
            : base(item)
        {

        }
        

        private OrderFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = OrderFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new OrderFrontend FrontendItem
        {
            get { return (OrderFrontend) base.FrontendItem; }
        }
        
        
		public new Order DbItem
        {
            get
            {
                return (Order)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.DateCreated = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.DateCreated),
        						isEditable: true, isVisible: true);

        this.Reference = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.Reference),
        						isEditable: true, isVisible: true);

        this.CustomerFirstName = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.CustomerFirstName),
        						isEditable: true, isVisible: true);

        this.CustomerEmail = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.CustomerEmail),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.Member = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.Member),
                null));

        
        this.CustomerAddress1 = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.CustomerAddress1),
        						isEditable: true, isVisible: true);

        this.CustomerAddress2 = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.CustomerAddress2),
        						isEditable: true, isVisible: true);

        this.CustomerAddress3 = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.CustomerAddress3),
        						isEditable: true, isVisible: true);

        this.AuthCode = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.AuthCode),
        						isEditable: true, isVisible: true);

        this.CustomerCountry = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.CustomerCountry),
        						isEditable: true, isVisible: true);

        this.CustomerPostCode = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.CustomerPostCode),
        						isEditable: true, isVisible: true);

        this.CustomerLocality = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.CustomerLocality),
        						isEditable: true, isVisible: true);

        this.CustomerState = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.CustomerState),
        						isEditable: true, isVisible: true);

        this.Paid = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.Paid),
        						isEditable: true, isVisible: true);

        this.PaidOn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.PaidOn),
        						isEditable: true, isVisible: true);

        this.Cancelled = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.Cancelled),
        						isEditable: true, isVisible: true);

        this.CancelledOn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.CancelledOn),
        						isEditable: true, isVisible: true);

        this.Remarks = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.Remarks),
        						isEditable: true, isVisible: true);

        this.CreatedByAdmin = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.CreatedByAdmin),
        						isEditable: true, isVisible: true);

        this.GUID = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.GUID),
        						isEditable: true, isVisible: true);

        this.ItemID = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.ItemID),
        						isEditable: true, isVisible: true);

        this.ItemType = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.ItemType),
        						isEditable: true, isVisible: true);

        this.IsShoppingCart = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.IsShoppingCart),
        						isEditable: true, isVisible: true);

        this.CustomerLastName = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.CustomerLastName),
        						isEditable: true, isVisible: true);

        this.ShippedOn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.ShippedOn),
        						isEditable: true, isVisible: true);

        this.ShipmentTrackingCode = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.ShipmentTrackingCode),
        						isEditable: true, isVisible: true);

        this.ShipmentType = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.ShipmentType),
        						isEditable: true, isVisible: true);

        this.ShipmentRemarks = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.ShipmentRemarks),
        						isEditable: true, isVisible: true);

        this.IsConfirmed = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.IsConfirmed),
        						isEditable: true, isVisible: true);

        this.ShipmentTrackingUrl = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.ShipmentTrackingUrl),
        						isEditable: true, isVisible: true);

        this.FixedDiscount = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.FixedDiscount),
        						isEditable: true, isVisible: true);

        this.OrderStatus = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.OrderStatus),
        						isEditable: true, isVisible: true);

        this.TotalShippingCost = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.TotalShippingCost),
        						isEditable: true, isVisible: true);

        this.LastUpdatedOn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.LastUpdatedOn),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.OrderCurrency = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.OrderCurrency),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CurrencyModule.Currency>.GetPropertyBySelector( item => item.Orders)));

        
//InitFieldUser_LinkedProperty
			this.ShippingMethod = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.ShippingMethod),
                null));

        
//InitFieldUser_LinkedProperty
			this.LinkedAffiliate = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.LinkedAffiliate),
                null));

        
        this.AffiliateCommissionRate = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.AffiliateCommissionRate),
        						isEditable: true, isVisible: true);

        this.CustomerMiddleName = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.CustomerMiddleName),
        						isEditable: true, isVisible: true);

        this.PayMammothIdentifier = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.PayMammothIdentifier),
        						isEditable: true, isVisible: true);

        this.PendingManualPayment = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.PendingManualPayment),
        						isEditable: true, isVisible: true);

        this.PaymentMethod = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.PaymentMethod),
        						isEditable: true, isVisible: true);

        this.PayPippaProcessCompletedOn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.PayPippaProcessCompletedOn),
        						isEditable: true, isVisible: true);

        this.AccountBalanceDifference = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.OrderModule.Order>.GetPropertyBySelector( item => item.AccountBalanceDifference),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
