using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.DbObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.WallPrizeLinkModule;
using PayMammoth.Modules.CultureDetailsModule;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class WallPrizeLinkFactoryAutoGen : PayMammoth.Modules._AutoGen.WallPrizeLinkBaseFactory, 
    				PayMammoth.Modules._AutoGen.IWallPrizeLinkFactoryAutoGen,
    				CS.General_v3.Classes.DbObjects.IBaseDbFactory<IWallPrizeLink>
    
    {
    
     	
        public WallPrizeLinkFactoryAutoGen ()
        {

			this.UsedInProject = true;
			_mainTypeCreated = typeof(WallPrizeLinkImpl);
			PayMammoth.Cms.WallPrizeLinkModule.WallPrizeLinkCmsFactory._initialiseStaticInstance();
        }
        public static new WallPrizeLinkFactory Instance
        {
            get
            {
                return (WallPrizeLinkFactory)WallPrizeLinkBaseFactory.Instance;
            }
      
      

        }
        
        
        public new IEnumerable<WallPrizeLink> FindAll()
        {
            return base.FindAll().Cast<WallPrizeLink>();
        }
        public new IEnumerable<WallPrizeLink> FindAll(IQuery query)
        {
            
            return base.FindAll(query).Cast<WallPrizeLink>();

        }
        public new IEnumerable<WallPrizeLink> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<WallPrizeLink>();
            
        }
        public new IEnumerable<WallPrizeLink> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<WallPrizeLink>();
            
            
        }
        public new IEnumerable<WallPrizeLink> FindAll(ICriteria crit)
        {
            return base.FindAll(crit).Cast<WallPrizeLink>();


        }
        public new IEnumerable<WallPrizeLink> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<WallPrizeLink>();
            
        }
        public new IEnumerable<WallPrizeLink> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<WallPrizeLink>();
        }
        
        public new WallPrizeLink FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (WallPrizeLink)base.FindItem(query);
        }
        public new WallPrizeLink FindItem(IQueryOver query)
        {
            return (WallPrizeLink)base.FindItem(query);
        }
        
        IWallPrizeLink IWallPrizeLinkFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new WallPrizeLink CreateNewItem()
        {
            return (WallPrizeLink)base.CreateNewItem();
        }

        public new WallPrizeLink GetByPrimaryKey(long pKey)
        {
            return (WallPrizeLink)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<WallPrizeLink, WallPrizeLink> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();

        	var q = session.QueryOver<WallPrizeLink>();
            qParams.FillQueryOver(q);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public PayMammoth.Modules.WallPrizeLinkModule.WallPrizeLink GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.WallPrizeLinkModule.WallPrizeLink) base.GetByPrimaryKey(id, session);
        }
       public IEnumerable<PayMammoth.Modules.WallPrizeLinkModule.WallPrizeLink> FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.WallPrizeLinkModule.WallPrizeLink>) base.FindAll(query, session);

       }
       public IEnumerable<PayMammoth.Modules.WallPrizeLinkModule.WallPrizeLink> FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.WallPrizeLinkModule.WallPrizeLink>) base.FindAll(query, session);

       }
       public IEnumerable<PayMammoth.Modules.WallPrizeLinkModule.WallPrizeLink> FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.WallPrizeLinkModule.WallPrizeLink>) base.FindAll(session);

       }
       public PayMammoth.Modules.WallPrizeLinkModule.WallPrizeLink FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.WallPrizeLinkModule.WallPrizeLink) base.FindItem(query, session);

       }
       public PayMammoth.Modules.WallPrizeLinkModule.WallPrizeLink FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.WallPrizeLinkModule.WallPrizeLink) base.FindItem(query, session);

       }

       public PayMammoth.Modules.WallPrizeLinkModule.WallPrizeLink FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.WallPrizeLinkModule.WallPrizeLink) base.FindItem(session);

       }
       public IEnumerable<PayMammoth.Modules.WallPrizeLinkModule.WallPrizeLink> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.WallPrizeLinkModule.WallPrizeLink>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink> Members
     
        PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink IBaseDbFactory<PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink IBaseDbFactory<PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink>.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink> IBaseDbFactory<PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink>.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink>.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink>.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink IBaseDbFactory<PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink>.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink>.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink>.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink> IBaseDbFactory<PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IWallPrizeLink> IWallPrizeLinkFactoryAutoGen.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IWallPrizeLink> IWallPrizeLinkFactoryAutoGen.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IWallPrizeLink> IWallPrizeLinkFactoryAutoGen.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(session);
           
       }

       IWallPrizeLink IWallPrizeLinkFactoryAutoGen.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(query, session);
           
       }

       IWallPrizeLink IWallPrizeLinkFactoryAutoGen.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(query, session);
           
       }

       IWallPrizeLink IWallPrizeLinkFactoryAutoGen.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IWallPrizeLink> IWallPrizeLinkFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IWallPrizeLink IWallPrizeLinkFactoryAutoGen.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
