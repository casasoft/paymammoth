using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.ArticleRatingModule;
using CS.WebComponentsGeneralV3.Cms.ArticleRatingModule;
using PayMammoth.Frontend.ArticleRatingModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class ArticleRatingCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.ArticleRatingModule.ArticleRatingBaseCmsInfo
    {

		public ArticleRatingCmsInfo_AutoGen(BusinessLogic_v3.Modules.ArticleRatingModule.ArticleRatingBase item)
            : base(item)
        {

        }
        

        private ArticleRatingFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = ArticleRatingFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new ArticleRatingFrontend FrontendItem
        {
            get { return (ArticleRatingFrontend) base.FrontendItem; }
        }
        
        
		public new ArticleRating DbItem
        {
            get
            {
                return (ArticleRating)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.IPAddress = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleRatingModule.ArticleRating>.GetPropertyBySelector( item => item.IPAddress),
        						isEditable: true, isVisible: true);

        this.Date = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleRatingModule.ArticleRating>.GetPropertyBySelector( item => item.Date),
        						isEditable: true, isVisible: true);

        this.Rating = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleRatingModule.ArticleRating>.GetPropertyBySelector( item => item.Rating),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.Article = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleRatingModule.ArticleRating>.GetPropertyBySelector( item => item.Article),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ArticleModule.Article>.GetPropertyBySelector( item => item.ArticleRatings)));

        
	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
