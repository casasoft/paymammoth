using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class MemberWallTicketBaseCmsInfo_AutoGen : CmsItemInfoDbBase<PayMammoth.Modules._AutoGen.MemberWallTicketBase>
    {
		public MemberWallTicketBaseCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.MemberWallTicketBase dbItem)
            : base(PayMammoth.Modules._AutoGen.MemberWallTicketBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new MemberWallTicketBase DbItem
        {
            get
            {
                return (MemberWallTicketBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Wall { get; private set; }

        public CmsPropertyInfo TicketNumber { get; private set; }

        public CmsPropertyInfo PurchasedOn { get; private set; }

        public CmsPropertyInfo Member { get; private set; }

        public CmsPropertyInfo WonPrize { get; private set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]

        this.Wall = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.MemberWallTicketBase>.GetPropertyBySelector( item => item.Wall),
        			isEditable: true,
        			isVisible: true
        			);

        this.TicketNumber = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.MemberWallTicketBase>.GetPropertyBySelector( item => item.TicketNumber),
        			isEditable: true,
        			isVisible: true
        			);

        this.PurchasedOn = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.MemberWallTicketBase>.GetPropertyBySelector( item => item.PurchasedOn),
        			isEditable: true,
        			isVisible: true
        			);

        this.Member = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.MemberWallTicketBase>.GetPropertyBySelector( item => item.Member),
        			isEditable: true,
        			isVisible: true
        			);

        this.WonPrize = base.AddProperty(
        			 
        			CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules._AutoGen.MemberWallTicketBase>.GetPropertyBySelector( item => item.WonPrize),
        			isEditable: true,
        			isVisible: true
        			);



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
