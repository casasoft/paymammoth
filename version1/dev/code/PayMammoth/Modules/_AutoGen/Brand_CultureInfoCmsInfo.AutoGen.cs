using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.Brand_CultureInfoModule;
using CS.WebComponentsGeneralV3.Cms.Brand_CultureInfoModule;
using PayMammoth.Frontend.Brand_CultureInfoModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class Brand_CultureInfoCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.Brand_CultureInfoModule.Brand_CultureInfoBaseCmsInfo
    {

		public Brand_CultureInfoCmsInfo_AutoGen(BusinessLogic_v3.Modules.Brand_CultureInfoModule.Brand_CultureInfoBase item)
            : base(item)
        {

        }
        

        private Brand_CultureInfoFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = Brand_CultureInfoFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new Brand_CultureInfoFrontend FrontendItem
        {
            get { return (Brand_CultureInfoFrontend) base.FrontendItem; }
        }
        
        
		public new Brand_CultureInfo DbItem
        {
            get
            {
                return (Brand_CultureInfo)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

//InitFieldUser_LinkedProperty
			this.CultureInfo = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo>.GetPropertyBySelector( item => item.CultureInfo),
                null));

        
//InitFieldUser_LinkedProperty
			this.Brand = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo>.GetPropertyBySelector( item => item.Brand),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.BrandModule.Brand>.GetPropertyBySelector( item => item.Brand_CultureInfos)));

        
        this.Description = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Brand_CultureInfoModule.Brand_CultureInfo>.GetPropertyBySelector( item => item.Description),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
