using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using PayMammoth.Modules.PaymentRequestItemModule;
using PayMammoth.Cms.PaymentRequestItemModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaymentRequestItemCmsFactory_AutoGen : PayMammoth.Modules._AutoGen.PaymentRequestItemBaseCmsFactory
    {
        public static new PaymentRequestItemBaseCmsFactory Instance 
        {
         	get { return (PaymentRequestItemBaseCmsFactory)PayMammoth.Modules._AutoGen.PaymentRequestItemBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override PaymentRequestItemBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = PaymentRequestItem.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            PaymentRequestItemCmsInfo cmsItem = new PaymentRequestItemCmsInfo(item);
            return cmsItem;
        }    
        
        public override PaymentRequestItemBaseCmsInfo CreateCmsItemInfo(PayMammoth.Modules._AutoGen.PaymentRequestItemBase item)
        {
            return new PaymentRequestItemCmsInfo((PaymentRequestItem)item);
            
        }

    }
}
