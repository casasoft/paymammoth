using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.TicketModule;
using PayMammoth.Modules.TicketModule;

//IUserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
	
    public interface ITicketAutoGen : BusinessLogic_v3.Modules.TicketModule.ITicketBase
    {




// [interface_userclass_properties]

		//IProperty_LinkedToObject
        new PayMammoth.Modules.CmsUserModule.ICmsUser AssignedTo {get; set; }

		//IProperty_LinkedToObject
        new PayMammoth.Modules.CmsUserModule.ICmsUser ClosedBy {get; set; }

		//IProperty_LinkedToObject
        new PayMammoth.Modules.CmsUserModule.ICmsUser ReOpenedByCmsUser {get; set; }

   
    

// [interface_userclassonly_properties]

   

                                     

// [interface_userclass_collections_nhibernate]

        //UserClass_CollectionOneToManyLeftSide
        
		new ICollectionManager<PayMammoth.Modules.TicketCommentModule.ITicketComment> TicketComments { get; }




// [interface_userclassonly_collections]

 


    	
    	
      
      

    }
}
