using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.EmailTextModule;
using PayMammoth.Modules.EmailTextModule;
using BusinessLogic_v3.Modules.EmailTextModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.EmailTextModule.EmailTextFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.EmailTextModule.IEmailText> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.EmailTextModule.EmailTextFrontend ToFrontend(this PayMammoth.Modules.EmailTextModule.IEmailText item)
        {
        	return PayMammoth.Frontend.EmailTextModule.EmailTextFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class EmailTextFrontend_AutoGen : BusinessLogic_v3.Frontend.EmailTextModule.EmailTextBaseFrontend

    {
		
        
        protected EmailTextFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.EmailTextModule.IEmailText Data
        {
            get { return (PayMammoth.Modules.EmailTextModule.IEmailText)base.Data; }
            set { base.Data = value; }

        }

        public new static EmailTextFrontend CreateNewItem()
        {
            return (EmailTextFrontend)BusinessLogic_v3.Frontend.EmailTextModule.EmailTextBaseFrontend.CreateNewItem();
        }
        
        public new static EmailTextFrontend Get(BusinessLogic_v3.Modules.EmailTextModule.IEmailTextBase data)
        {
            return (EmailTextFrontend)BusinessLogic_v3.Frontend.EmailTextModule.EmailTextBaseFrontend.Get(data);
        }
        public new static List<EmailTextFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.EmailTextModule.IEmailTextBase> dataList)
        {
            return BusinessLogic_v3.Frontend.EmailTextModule.EmailTextBaseFrontend.GetList(dataList).Cast<EmailTextFrontend>().ToList();
        }


    }
}
