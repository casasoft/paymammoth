using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.CmsUserModule;
using CS.WebComponentsGeneralV3.Cms.CmsUserModule;
using PayMammoth.Frontend.CmsUserModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class CmsUserCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.CmsUserModule.CmsUserBaseCmsInfo
    {

		public CmsUserCmsInfo_AutoGen(BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase item)
            : base(item)
        {

        }
        

        private CmsUserFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = CmsUserFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new CmsUserFrontend FrontendItem
        {
            get { return (CmsUserFrontend) base.FrontendItem; }
        }
        
        
		public new CmsUser DbItem
        {
            get
            {
                return (CmsUser)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.Name = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsUserModule.CmsUser>.GetPropertyBySelector( item => item.Name),
        						isEditable: true, isVisible: true);

        this.Surname = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsUserModule.CmsUser>.GetPropertyBySelector( item => item.Surname),
        						isEditable: true, isVisible: true);

        this.Username = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsUserModule.CmsUser>.GetPropertyBySelector( item => item.Username),
        						isEditable: true, isVisible: true);

        this.Password = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsUserModule.CmsUser>.GetPropertyBySelector( item => item.Password),
        						isEditable: true, isVisible: true);

        this.LastLoggedIn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsUserModule.CmsUser>.GetPropertyBySelector( item => item.LastLoggedIn),
        						isEditable: true, isVisible: true);

        this._LastLoggedIn_New = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsUserModule.CmsUser>.GetPropertyBySelector( item => item._LastLoggedIn_New),
        						isEditable: true, isVisible: true);

        this.AccessType = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsUserModule.CmsUser>.GetPropertyBySelector( item => item.AccessType),
        						isEditable: true, isVisible: true);

        this.SessionGUID = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsUserModule.CmsUser>.GetPropertyBySelector( item => item.SessionGUID),
        						isEditable: true, isVisible: true);

        this.PasswordSalt = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsUserModule.CmsUser>.GetPropertyBySelector( item => item.PasswordSalt),
        						isEditable: true, isVisible: true);

        this.PasswordEncryptionType = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsUserModule.CmsUser>.GetPropertyBySelector( item => item.PasswordEncryptionType),
        						isEditable: true, isVisible: true);

        this.PasswordIterations = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsUserModule.CmsUser>.GetPropertyBySelector( item => item.PasswordIterations),
        						isEditable: true, isVisible: true);

        this.HiddenFromNonCasaSoft = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsUserModule.CmsUser>.GetPropertyBySelector( item => item.HiddenFromNonCasaSoft),
        						isEditable: true, isVisible: true);

        this.CmsCustomLogoFilename = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsUserModule.CmsUser>.GetPropertyBySelector( item => item.CmsCustomLogoFilename),
        						isEditable: false, isVisible: false);

        this.CmsCustomLogoLinkUrl = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsUserModule.CmsUser>.GetPropertyBySelector( item => item.CmsCustomLogoLinkUrl),
        						isEditable: true, isVisible: true);

        this.Disabled = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsUserModule.CmsUser>.GetPropertyBySelector( item => item.Disabled),
        						isEditable: true, isVisible: true);

        this.TicketingSystemSupportUser = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsUserModule.CmsUser>.GetPropertyBySelector( item => item.TicketingSystemSupportUser),
        						isEditable: true, isVisible: true);

        this.Email = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsUserModule.CmsUser>.GetPropertyBySelector( item => item.Email),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
