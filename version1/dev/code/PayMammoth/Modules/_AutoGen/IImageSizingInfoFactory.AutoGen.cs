using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ImageSizingInfoModule;
using PayMammoth.Modules.ImageSizingInfoModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IImageSizingInfoFactoryAutoGen : BusinessLogic_v3.Modules.ImageSizingInfoModule.IImageSizingInfoBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IImageSizingInfo>
    
    {
	    new PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo CreateNewItem();
        new PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null);
        new IEnumerable<PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
