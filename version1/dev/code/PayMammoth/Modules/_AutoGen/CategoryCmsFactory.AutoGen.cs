using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.CategoryModule;
using PayMammoth.Cms.CategoryModule;
using CS.WebComponentsGeneralV3.Cms.CategoryModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class CategoryCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.CategoryModule.CategoryBaseCmsFactory
    {
        public static new CategoryBaseCmsFactory Instance 
        {
         	get { return (CategoryBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.CategoryModule.CategoryBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override CategoryBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = Category.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            CategoryCmsInfo cmsItem = new CategoryCmsInfo(item);
            return cmsItem;
        }    
        
        public override CategoryBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.CategoryModule.CategoryBase item)
        {
            return new CategoryCmsInfo((Category)item);
            
        }

    }
}
