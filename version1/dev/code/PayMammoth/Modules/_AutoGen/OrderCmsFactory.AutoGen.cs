using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.OrderModule;
using PayMammoth.Cms.OrderModule;
using CS.WebComponentsGeneralV3.Cms.OrderModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class OrderCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.OrderModule.OrderBaseCmsFactory
    {
        public static new OrderBaseCmsFactory Instance 
        {
         	get { return (OrderBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.OrderModule.OrderBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override OrderBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = Order.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            OrderCmsInfo cmsItem = new OrderCmsInfo(item);
            return cmsItem;
        }    
        
        public override OrderBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.OrderModule.OrderBase item)
        {
            return new OrderCmsInfo((Order)item);
            
        }

    }
}
