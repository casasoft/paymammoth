using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen.PaymentRequestModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaymentRequestBaseCmsInfo_AutoGen : CmsItemInfoDbBase<PayMammoth.Modules._AutoGen.PaymentRequestBase>
    {
		public PaymentRequestBaseCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.PaymentRequestBase dbItem)
            : base(PayMammoth.Modules._AutoGen.PaymentRequestBaseCmsFactory.Instance, dbItem)
        {

            
        }
    
    
        public new PaymentRequestBaseFrontend FrontendItem
        {
            get { return (PaymentRequestBaseFrontend)base.FrontendItem; }

        }
        public new PaymentRequestBase DbItem
        {
            get
            {
                return (PaymentRequestBase)base.DbItem;
            }
        }                       
        

// [basecmsinfo_fieldsdeclarations]

        public CmsPropertyInfo Identifier { get; protected set; }

        public CmsPropertyInfo DateTime { get; protected set; }

        public CmsPropertyInfo RequestIP { get; protected set; }

        public CmsPropertyInfo ClientFirstName { get; protected set; }

        public CmsPropertyInfo ClientMiddleName { get; protected set; }

        public CmsPropertyInfo ClientLastName { get; protected set; }

        public CmsPropertyInfo ClientAddress1 { get; protected set; }

        public CmsPropertyInfo ClientAddress2 { get; protected set; }

        public CmsPropertyInfo ClientAddress3 { get; protected set; }

        public CmsPropertyInfo ClientLocality { get; protected set; }

        public CmsPropertyInfo ClientCountry { get; protected set; }

        public CmsPropertyInfo ClientPostCode { get; protected set; }

        public CmsPropertyInfo Description { get; protected set; }

        public CmsPropertyInfo ClientEmail { get; protected set; }

        public CmsPropertyInfo WebsiteAccount { get; protected set; }

        public CmsPropertyInfo TaxAmount { get; protected set; }

        public CmsPropertyInfo HandlingAmount { get; protected set; }

        public CmsPropertyInfo ShippingAmount { get; protected set; }

        public CmsPropertyInfo Reference { get; protected set; }

        public CmsPropertyInfo Notes { get; protected set; }

        public CmsPropertyInfo PriceExcTax { get; protected set; }

        public CmsPropertyInfo CurrencyCode { get; protected set; }

        public CmsPropertyInfo OrderLinkOnClientWebsite { get; protected set; }

        public CmsPropertyInfo Title { get; protected set; }

        public CmsPropertyInfo CurrentTransaction { get; protected set; }

        public CmsPropertyInfo Paid { get; protected set; }

        public CmsPropertyInfo PaidOn { get; protected set; }

        public CmsPropertyInfo PaidByPaymentMethod { get; protected set; }

        public CmsPropertyInfo ReturnUrlSuccess { get; protected set; }

        public CmsPropertyInfo ReturnUrlFailure { get; protected set; }

        public CmsPropertyInfo LanguageCode { get; protected set; }

        public CmsPropertyInfo LanguageCountryCode { get; protected set; }

        public CmsPropertyInfo LanguageSuffix { get; protected set; }

        public CmsPropertyInfo ClientTelephone { get; protected set; }

        public CmsPropertyInfo ClientMobile { get; protected set; }

        public CmsPropertyInfo ClientTitle { get; protected set; }

        public CmsPropertyInfo ClientIp { get; protected set; }

        public CmsPropertyInfo ClientReference { get; protected set; }

        public CmsPropertyInfo PaymentNotificationAcknowledged { get; protected set; }

        public CmsPropertyInfo RequiresManualIntervention { get; protected set; }

        public CmsPropertyInfo PaymentNotificationAcknowledgedOn { get; protected set; }

        public CmsPropertyInfo PaymentNotificationRetryCount { get; protected set; }

        public CmsPropertyInfo PaymentNotificationEnabled { get; protected set; }

        public CmsPropertyInfo PaymentNotificationNextRetryOn { get; protected set; }



// [basecmsinfo_collectiondeclarations]




        protected override void initBasicFields()
        {


// [basecmsinfo_initbasicfields]



// [basecmsinfo_initcollections]



			base.initBasicFields();
          
        }

    }
}
