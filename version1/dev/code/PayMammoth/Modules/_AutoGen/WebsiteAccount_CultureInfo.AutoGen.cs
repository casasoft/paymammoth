using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.WebsiteAccount_CultureInfoModule;
using PayMammoth.Modules._AutoGen;

//DbObjectBuilder v8

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{    

	
    public abstract class WebsiteAccount_CultureInfo_AutoGen : WebsiteAccount_CultureInfoBase, PayMammoth.Modules._AutoGen.IWebsiteAccount_CultureInfoAutoGen
	
      
      
      , IMultilingualContentInfo
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.CultureDetailsModule.CultureDetails CultureInfo
        {
            get
            {
                return (PayMammoth.Modules.CultureDetailsModule.CultureDetails)base.CultureInfo;
            }
            set
            {
                base.CultureInfo = value;
            }
        }

		PayMammoth.Modules.CultureDetailsModule.ICultureDetails IWebsiteAccount_CultureInfoAutoGen.CultureInfo
        {
            get
            {
            	return this.CultureInfo;
                
            }
            set
            {
                this.CultureInfo = (PayMammoth.Modules.CultureDetailsModule.CultureDetails)value;
            }
        }
		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount WebsiteAccount
        {
            get
            {
                return (PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount)base.WebsiteAccount;
            }
            set
            {
                base.WebsiteAccount = value;
            }
        }

		PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccount IWebsiteAccount_CultureInfoAutoGen.WebsiteAccount
        {
            get
            {
            	return this.WebsiteAccount;
                
            }
            set
            {
                this.WebsiteAccount = (PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount)value;
            }
        }



// [userclass_collections_nhibernate]


        
          
		public  static new WebsiteAccount_CultureInfoFactory Factory
        {
            get
            {
                return (WebsiteAccount_CultureInfoFactory)WebsiteAccount_CultureInfoBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<WebsiteAccount_CultureInfo, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
