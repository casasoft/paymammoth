using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.TicketCommentModule;
using PayMammoth.Modules.TicketCommentModule;
using BusinessLogic_v3.Modules.TicketCommentModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.TicketCommentModule.TicketCommentFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.TicketCommentModule.ITicketComment> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.TicketCommentModule.TicketCommentFrontend ToFrontend(this PayMammoth.Modules.TicketCommentModule.ITicketComment item)
        {
        	return PayMammoth.Frontend.TicketCommentModule.TicketCommentFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class TicketCommentFrontend_AutoGen : BusinessLogic_v3.Frontend.TicketCommentModule.TicketCommentBaseFrontend

    {
		
        
        protected TicketCommentFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.TicketCommentModule.ITicketComment Data
        {
            get { return (PayMammoth.Modules.TicketCommentModule.ITicketComment)base.Data; }
            set { base.Data = value; }

        }

        public new static TicketCommentFrontend CreateNewItem()
        {
            return (TicketCommentFrontend)BusinessLogic_v3.Frontend.TicketCommentModule.TicketCommentBaseFrontend.CreateNewItem();
        }
        
        public new static TicketCommentFrontend Get(BusinessLogic_v3.Modules.TicketCommentModule.ITicketCommentBase data)
        {
            return (TicketCommentFrontend)BusinessLogic_v3.Frontend.TicketCommentModule.TicketCommentBaseFrontend.Get(data);
        }
        public new static List<TicketCommentFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.TicketCommentModule.ITicketCommentBase> dataList)
        {
            return BusinessLogic_v3.Frontend.TicketCommentModule.TicketCommentBaseFrontend.GetList(dataList).Cast<TicketCommentFrontend>().ToList();
        }


    }
}
