using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.EmailLogModule;
using PayMammoth.Modules.EmailLogModule;
using BusinessLogic_v3.Modules.EmailLogModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.EmailLogModule.EmailLogFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.EmailLogModule.IEmailLog> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.EmailLogModule.EmailLogFrontend ToFrontend(this PayMammoth.Modules.EmailLogModule.IEmailLog item)
        {
        	return PayMammoth.Frontend.EmailLogModule.EmailLogFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class EmailLogFrontend_AutoGen : BusinessLogic_v3.Frontend.EmailLogModule.EmailLogBaseFrontend

    {
		
        
        protected EmailLogFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.EmailLogModule.IEmailLog Data
        {
            get { return (PayMammoth.Modules.EmailLogModule.IEmailLog)base.Data; }
            set { base.Data = value; }

        }

        public new static EmailLogFrontend CreateNewItem()
        {
            return (EmailLogFrontend)BusinessLogic_v3.Frontend.EmailLogModule.EmailLogBaseFrontend.CreateNewItem();
        }
        
        public new static EmailLogFrontend Get(BusinessLogic_v3.Modules.EmailLogModule.IEmailLogBase data)
        {
            return (EmailLogFrontend)BusinessLogic_v3.Frontend.EmailLogModule.EmailLogBaseFrontend.Get(data);
        }
        public new static List<EmailLogFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.EmailLogModule.IEmailLogBase> dataList)
        {
            return BusinessLogic_v3.Frontend.EmailLogModule.EmailLogBaseFrontend.GetList(dataList).Cast<EmailLogFrontend>().ToList();
        }


    }
}
