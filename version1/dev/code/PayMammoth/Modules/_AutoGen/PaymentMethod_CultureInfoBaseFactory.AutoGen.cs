using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;

//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
   
    public abstract class PaymentMethod_CultureInfoBaseFactoryAutoGen  : BusinessLogic_v3.Classes.DbObjects.Factories.BaseDbFactory<PaymentMethod_CultureInfoBase, IPaymentMethod_CultureInfoBase>, IPaymentMethod_CultureInfoBaseFactoryAutoGen
    
    {
    
        public PaymentMethod_CultureInfoBaseFactoryAutoGen()
        {
        	
            
        }
        
		static PaymentMethod_CultureInfoBaseFactoryAutoGen()
        {
            
        }
        
        
        public new static PaymentMethod_CultureInfoBaseFactory Instance
        {
            get
            {
                return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<PaymentMethod_CultureInfoBaseFactory>(null);
            }
        }
        
		public IQueryOver<PaymentMethod_CultureInfoBase, PaymentMethod_CultureInfoBase> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
        	ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<PaymentMethod_CultureInfoBase>();
            qParams.FillQueryOver(q, this);
        	postGetQueryOver(q, qParams);
            return q;
            
        } 
        
        
    }
}
