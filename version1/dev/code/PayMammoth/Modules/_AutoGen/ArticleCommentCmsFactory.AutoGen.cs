using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.ArticleCommentModule;
using PayMammoth.Cms.ArticleCommentModule;
using CS.WebComponentsGeneralV3.Cms.ArticleCommentModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class ArticleCommentCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.ArticleCommentModule.ArticleCommentBaseCmsFactory
    {
        public static new ArticleCommentBaseCmsFactory Instance 
        {
         	get { return (ArticleCommentBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.ArticleCommentModule.ArticleCommentBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override ArticleCommentBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = ArticleComment.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            ArticleCommentCmsInfo cmsItem = new ArticleCommentCmsInfo(item);
            return cmsItem;
        }    
        
        public override ArticleCommentBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase item)
        {
            return new ArticleCommentCmsInfo((ArticleComment)item);
            
        }

    }
}
