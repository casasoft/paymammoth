using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;

//BaseClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
	
    public interface IWebsiteAccount_CultureInfoBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
    {
    

// [interface_base_properties]

		//IProperty_LinkedToObject
        BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase CultureInfo {get; set; }

		//IProperty_LinkedToObject
        PayMammoth.Modules._AutoGen.IWebsiteAccountBase WebsiteAccount {get; set; }

		//Iproperty_normal
        string PaymentGatewayRealexDescription { get; set; }
		//Iproperty_normal
        string PaymentGatewayApcoDescription { get; set; }
		//Iproperty_normal
        string PaymentGatewayTransactiumDescription { get; set; }
   

// [interface_base_collections]

 
		

    	
    	
      
      

    }
}
