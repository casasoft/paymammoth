using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ContentText_CultureInfoModule;
using PayMammoth.Modules.ContentText_CultureInfoModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IContentText_CultureInfoFactoryAutoGen : BusinessLogic_v3.Modules.ContentText_CultureInfoModule.IContentText_CultureInfoBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IContentText_CultureInfo>
    
    {
	    new PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo CreateNewItem();
        new PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null);
        new IEnumerable<PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
