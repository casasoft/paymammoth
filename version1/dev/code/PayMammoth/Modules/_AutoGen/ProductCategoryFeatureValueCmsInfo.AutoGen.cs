using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.ProductCategoryFeatureValueModule;
using CS.WebComponentsGeneralV3.Cms.ProductCategoryFeatureValueModule;
using PayMammoth.Frontend.ProductCategoryFeatureValueModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class ProductCategoryFeatureValueCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBaseCmsInfo
    {

		public ProductCategoryFeatureValueCmsInfo_AutoGen(BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBase item)
            : base(item)
        {

        }
        

        private ProductCategoryFeatureValueFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = ProductCategoryFeatureValueFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new ProductCategoryFeatureValueFrontend FrontendItem
        {
            get { return (ProductCategoryFeatureValueFrontend) base.FrontendItem; }
        }
        
        
		public new ProductCategoryFeatureValue DbItem
        {
            get
            {
                return (ProductCategoryFeatureValue)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

//InitFieldUser_LinkedProperty
			this.Product = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValue>.GetPropertyBySelector( item => item.Product),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.ProductCategoryFeatureValues)));

        
//InitFieldUser_LinkedProperty
			this.CategoryFeature = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductCategoryFeatureValueModule.ProductCategoryFeatureValue>.GetPropertyBySelector( item => item.CategoryFeature),
                null));

        
	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
