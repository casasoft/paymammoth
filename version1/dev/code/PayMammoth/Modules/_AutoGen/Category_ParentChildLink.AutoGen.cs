using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.Category_ParentChildLinkModule;
using BusinessLogic_v3.Modules.Category_ParentChildLinkModule;


//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
    public abstract class Category_ParentChildLink_AutoGen : Category_ParentChildLinkBase, PayMammoth.Modules._AutoGen.ICategory_ParentChildLinkAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.CategoryModule.Category ParentCategory
        {
            get
            {
                return (PayMammoth.Modules.CategoryModule.Category)base.ParentCategory;
            }
            set
            {
                base.ParentCategory = value;
            }
        }

		PayMammoth.Modules.CategoryModule.ICategory ICategory_ParentChildLinkAutoGen.ParentCategory
        {
            get
            {
            	return this.ParentCategory;
                
            }
            set
            {
                this.ParentCategory = (PayMammoth.Modules.CategoryModule.Category)value;
            }
        }
		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.CategoryModule.Category ChildCategory
        {
            get
            {
                return (PayMammoth.Modules.CategoryModule.Category)base.ChildCategory;
            }
            set
            {
                base.ChildCategory = value;
            }
        }

		PayMammoth.Modules.CategoryModule.ICategory ICategory_ParentChildLinkAutoGen.ChildCategory
        {
            get
            {
            	return this.ChildCategory;
                
            }
            set
            {
                this.ChildCategory = (PayMammoth.Modules.CategoryModule.Category)value;
            }
        }



// [userclass_collections_nhibernate]


        
          
		public  static new Category_ParentChildLinkFactory Factory
        {
            get
            {
                return (Category_ParentChildLinkFactory)Category_ParentChildLinkBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<Category_ParentChildLink, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
