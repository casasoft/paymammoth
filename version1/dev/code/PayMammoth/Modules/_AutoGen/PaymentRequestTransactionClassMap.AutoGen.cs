using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.PaymentRequestTransactionModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class PaymentRequestTransactionMap_AutoGen : PaymentRequestTransactionMap_AutoGen_Z
    {
        public PaymentRequestTransactionMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]

			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.PaymentRequest, PaymentRequestMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Successful, SuccessfulMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentMethod, PaymentMethodMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Comments, CommentsMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.IP, IPMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.AuthCode, AuthCodeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.StartDate, StartDateMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.LastUpdatedOn, LastUpdatedOnMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.EndDate, EndDateMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Reference, ReferenceMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.TransactionInfo, TransactionInfoMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentParameters, PaymentParametersMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentMethodVersion, PaymentMethodVersionMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.FakePayment, FakePaymentMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.RequiresManualIntervention, RequiresManualInterventionMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class PaymentRequestTransactionMap_AutoGen_Z : PayMammoth.Modules._AutoGen.PaymentRequestTransactionBaseMap<PaymentRequestTransactionImpl>
    {


// [userclassmap_properties]

//ClassMap_PropertyLinkedObject

        protected virtual void PaymentRequestMappingInfo(ICollectionMapInfo mapInfo)
        {
        
            //mapInfo.CollectionType = CS.General_v3.Enums.NHIBERNATE_COLLECTION_TYPE.Set;
            
            mapInfo.CascadeType.Clear();
            
            mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
            mapInfo.Inverse = false;
            mapInfo.ColumnName = "PaymentRequestId";   
            
            mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
           
        }
        
        




// [userclassmap_collections]

            
        
        
    }
}
