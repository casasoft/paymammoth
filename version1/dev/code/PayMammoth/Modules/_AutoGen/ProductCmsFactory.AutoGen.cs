using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.ProductModule;
using PayMammoth.Cms.ProductModule;
using CS.WebComponentsGeneralV3.Cms.ProductModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class ProductCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.ProductModule.ProductBaseCmsFactory
    {
        public static new ProductBaseCmsFactory Instance 
        {
         	get { return (ProductBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.ProductModule.ProductBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override ProductBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = Product.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            ProductCmsInfo cmsItem = new ProductCmsInfo(item);
            return cmsItem;
        }    
        
        public override ProductBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.ProductModule.ProductBase item)
        {
            return new ProductCmsInfo((Product)item);
            
        }

    }
}
