using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.TicketCommentModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class TicketCommentMap_AutoGen : TicketCommentMap_AutoGen_Z
    {
        public TicketCommentMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]

			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.Ticket, TicketMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Comment, CommentMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.IPAddress, IPAddressMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PostedOn, PostedOnMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.AssignedStaffUser, AssignedStaffUserMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CommentType, CommentTypeMappingInfo);



// [userclassmap_initcollections]

//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.Attachments, AttachmentsMappingInfo, BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class TicketCommentMap_AutoGen_Z : BusinessLogic_v3.Modules.TicketCommentModule.TicketCommentBaseMap<TicketCommentImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
