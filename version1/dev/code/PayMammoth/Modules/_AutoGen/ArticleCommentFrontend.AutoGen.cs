using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.ArticleCommentModule;
using PayMammoth.Modules.ArticleCommentModule;
using BusinessLogic_v3.Modules.ArticleCommentModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.ArticleCommentModule.ArticleCommentFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.ArticleCommentModule.IArticleComment> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.ArticleCommentModule.ArticleCommentFrontend ToFrontend(this PayMammoth.Modules.ArticleCommentModule.IArticleComment item)
        {
        	return PayMammoth.Frontend.ArticleCommentModule.ArticleCommentFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class ArticleCommentFrontend_AutoGen : BusinessLogic_v3.Frontend.ArticleCommentModule.ArticleCommentBaseFrontend

    {
		
        
        protected ArticleCommentFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.ArticleCommentModule.IArticleComment Data
        {
            get { return (PayMammoth.Modules.ArticleCommentModule.IArticleComment)base.Data; }
            set { base.Data = value; }

        }

        public new static ArticleCommentFrontend CreateNewItem()
        {
            return (ArticleCommentFrontend)BusinessLogic_v3.Frontend.ArticleCommentModule.ArticleCommentBaseFrontend.CreateNewItem();
        }
        
        public new static ArticleCommentFrontend Get(BusinessLogic_v3.Modules.ArticleCommentModule.IArticleCommentBase data)
        {
            return (ArticleCommentFrontend)BusinessLogic_v3.Frontend.ArticleCommentModule.ArticleCommentBaseFrontend.Get(data);
        }
        public new static List<ArticleCommentFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.ArticleCommentModule.IArticleCommentBase> dataList)
        {
            return BusinessLogic_v3.Frontend.ArticleCommentModule.ArticleCommentBaseFrontend.GetList(dataList).Cast<ArticleCommentFrontend>().ToList();
        }


    }
}
