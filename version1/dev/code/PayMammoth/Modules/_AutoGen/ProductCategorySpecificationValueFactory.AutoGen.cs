using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule;
using PayMammoth.Modules.ProductCategorySpecificationValueModule;
using PayMammoth.Modules.CultureDetailsModule;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class ProductCategorySpecificationValueFactoryAutoGen : BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueBaseFactory, 
    				PayMammoth.Modules._AutoGen.IProductCategorySpecificationValueFactoryAutoGen,
    				CS.General_v3.Classes.DbObjects.IBaseDbFactory<IProductCategorySpecificationValue>
    
    {
    
     	public new ProductCategorySpecificationValue ReloadItemFromDatabase(BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueBase item)
        {
         	return (ProductCategorySpecificationValue)base.ReloadItemFromDatabase(item);
        }
     	
        public ProductCategorySpecificationValueFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(ProductCategorySpecificationValueImpl);
			PayMammoth.Cms.ProductCategorySpecificationValueModule.ProductCategorySpecificationValueCmsFactory._initialiseStaticInstance();
        }
        public static new ProductCategorySpecificationValueFactory Instance
        {
            get
            {
                return (ProductCategorySpecificationValueFactory)ProductCategorySpecificationValueBaseFactory.Instance;
            }
      
      

        }
        
        /*
        public new IEnumerable<ProductCategorySpecificationValue> FindAll()
        {
            return base.FindAll().Cast<ProductCategorySpecificationValue>();
        }*/
    /*    public new IEnumerable<ProductCategorySpecificationValue> FindAll(IQuery query)
        {
            
            return base.FindAll(query).Cast<ProductCategorySpecificationValue>();

        }*/
        public new IEnumerable<ProductCategorySpecificationValue> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<ProductCategorySpecificationValue>();
            
        }
        public new IEnumerable<ProductCategorySpecificationValue> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<ProductCategorySpecificationValue>();
            
            
        }
     /*  public new IEnumerable<ProductCategorySpecificationValue> FindAll(ICriteria crit)
        {
            return base.FindAll(crit).Cast<ProductCategorySpecificationValue>();


        }*/
        public new IEnumerable<ProductCategorySpecificationValue> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<ProductCategorySpecificationValue>();
            
        }
        public new IEnumerable<ProductCategorySpecificationValue> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<ProductCategorySpecificationValue>();
        }
        
        public new ProductCategorySpecificationValue FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (ProductCategorySpecificationValue)base.FindItem(query);
        }
        public new ProductCategorySpecificationValue FindItem(IQueryOver query)
        {
            return (ProductCategorySpecificationValue)base.FindItem(query);
        }
        
        IProductCategorySpecificationValue IProductCategorySpecificationValueFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new ProductCategorySpecificationValue CreateNewItem()
        {
            return (ProductCategorySpecificationValue)base.CreateNewItem();
        }

        public new ProductCategorySpecificationValue GetByPrimaryKey(long pKey)
        {
            return (ProductCategorySpecificationValue)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<ProductCategorySpecificationValue, ProductCategorySpecificationValue> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();

        	var q = session.QueryOver<ProductCategorySpecificationValue>();
            qParams.FillQueryOver(q);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValue GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValue) base.GetByPrimaryKey(id, session);
        }
       public new IEnumerable<PayMammoth.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValue> FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValue>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValue> FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValue>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValue> FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValue>) base.FindAll(session);

       }
       public new PayMammoth.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValue FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValue) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValue FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValue) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValue FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValue) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValue> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ProductCategorySpecificationValueModule.ProductCategorySpecificationValue>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue> Members
     
        PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue IBaseDbFactory<PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue IBaseDbFactory<PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue>.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue> IBaseDbFactory<PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue>.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue>.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue>.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue IBaseDbFactory<PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue>.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue>.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue>.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue> IBaseDbFactory<PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IProductCategorySpecificationValue> IProductCategorySpecificationValueFactoryAutoGen.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IProductCategorySpecificationValue> IProductCategorySpecificationValueFactoryAutoGen.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IProductCategorySpecificationValue> IProductCategorySpecificationValueFactoryAutoGen.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IProductCategorySpecificationValue IProductCategorySpecificationValueFactoryAutoGen.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IProductCategorySpecificationValue IProductCategorySpecificationValueFactoryAutoGen.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IProductCategorySpecificationValue IProductCategorySpecificationValueFactoryAutoGen.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IProductCategorySpecificationValue> IProductCategorySpecificationValueFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IProductCategorySpecificationValue IProductCategorySpecificationValueFactoryAutoGen.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
