using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.Brand_CultureInfoModule;
using PayMammoth.Cms.Brand_CultureInfoModule;
using CS.WebComponentsGeneralV3.Cms.Brand_CultureInfoModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class Brand_CultureInfoCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.Brand_CultureInfoModule.Brand_CultureInfoBaseCmsFactory
    {
        public static new Brand_CultureInfoBaseCmsFactory Instance 
        {
         	get { return (Brand_CultureInfoBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.Brand_CultureInfoModule.Brand_CultureInfoBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override Brand_CultureInfoBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = Brand_CultureInfo.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            Brand_CultureInfoCmsInfo cmsItem = new Brand_CultureInfoCmsInfo(item);
            return cmsItem;
        }    
        
        public override Brand_CultureInfoBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.Brand_CultureInfoModule.Brand_CultureInfoBase item)
        {
            return new Brand_CultureInfoCmsInfo((Brand_CultureInfo)item);
            
        }

    }
}
