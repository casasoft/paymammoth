using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.ContentText_CultureInfoModule;
using PayMammoth.Modules.ContentText_CultureInfoModule;
using BusinessLogic_v3.Modules.ContentText_CultureInfoModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.ContentText_CultureInfoModule.ContentText_CultureInfoFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.ContentText_CultureInfoModule.ContentText_CultureInfoFrontend ToFrontend(this PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo item)
        {
        	return PayMammoth.Frontend.ContentText_CultureInfoModule.ContentText_CultureInfoFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class ContentText_CultureInfoFrontend_AutoGen : BusinessLogic_v3.Frontend.ContentText_CultureInfoModule.ContentText_CultureInfoBaseFrontend

    {
		
        
        protected ContentText_CultureInfoFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo Data
        {
            get { return (PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo)base.Data; }
            set { base.Data = value; }

        }

        public new static ContentText_CultureInfoFrontend CreateNewItem()
        {
            return (ContentText_CultureInfoFrontend)BusinessLogic_v3.Frontend.ContentText_CultureInfoModule.ContentText_CultureInfoBaseFrontend.CreateNewItem();
        }
        
        public new static ContentText_CultureInfoFrontend Get(BusinessLogic_v3.Modules.ContentText_CultureInfoModule.IContentText_CultureInfoBase data)
        {
            return (ContentText_CultureInfoFrontend)BusinessLogic_v3.Frontend.ContentText_CultureInfoModule.ContentText_CultureInfoBaseFrontend.Get(data);
        }
        public new static List<ContentText_CultureInfoFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.ContentText_CultureInfoModule.IContentText_CultureInfoBase> dataList)
        {
            return BusinessLogic_v3.Frontend.ContentText_CultureInfoModule.ContentText_CultureInfoBaseFrontend.GetList(dataList).Cast<ContentText_CultureInfoFrontend>().ToList();
        }


    }
}
