using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.BannerModule;
using PayMammoth.Modules.BannerModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class BannerFactoryAutoGen : BusinessLogic_v3.Modules.BannerModule.BannerBaseFactory, 
    				PayMammoth.Modules._AutoGen.IBannerFactoryAutoGen,
    				BusinessLogic_v3.Classes.DB.IBaseDbFactory<IBanner>
    
    {
    
     	public new Banner ReloadItemFromDatabase(BusinessLogic_v3.Modules.BannerModule.BannerBase item)
        {
         	return (Banner)base.ReloadItemFromDatabase(item);
        }
     	
        public BannerFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(BannerImpl);
			PayMammoth.Cms.BannerModule.BannerCmsFactory._initialiseStaticInstance();
        }
        public static new BannerFactory Instance
        {
            get
            {
                return (BannerFactory)BannerBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<Banner> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<Banner>();
            
        }
        public new IEnumerable<Banner> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<Banner>();
            
            
        }
    
        public new IEnumerable<Banner> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<Banner>();
            
        }
        public new IEnumerable<Banner> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<Banner>();
        }
        
        public new Banner FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (Banner)base.FindItem(query);
        }
        public new Banner FindItem(IQueryOver query)
        {
            return (Banner)base.FindItem(query);
        }
        
        IBanner IBannerFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new Banner CreateNewItem()
        {
            return (Banner)base.CreateNewItem();
        }

        public new Banner GetByPrimaryKey(long pKey)
        {
            return (Banner)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<Banner, Banner> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<Banner>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.BannerModule.Banner GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.BannerModule.Banner) base.GetByPrimaryKey(id, session);
        }
       public new IEnumerable<PayMammoth.Modules.BannerModule.Banner> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.BannerModule.Banner>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.BannerModule.Banner> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.BannerModule.Banner>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.BannerModule.Banner> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.BannerModule.Banner>) base.FindAll(session);

       }
       public new PayMammoth.Modules.BannerModule.Banner FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.BannerModule.Banner) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.BannerModule.Banner FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.BannerModule.Banner) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.BannerModule.Banner FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.BannerModule.Banner) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.BannerModule.Banner> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.BannerModule.Banner>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.BannerModule.IBanner> Members
     
        PayMammoth.Modules.BannerModule.IBanner IBaseDbFactory<PayMammoth.Modules.BannerModule.IBanner>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.BannerModule.IBanner IBaseDbFactory<PayMammoth.Modules.BannerModule.IBanner>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.BannerModule.IBanner> IBaseDbFactory<PayMammoth.Modules.BannerModule.IBanner>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.BannerModule.IBanner> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.BannerModule.IBanner>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.BannerModule.IBanner> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.BannerModule.IBanner>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.BannerModule.IBanner IBaseDbFactory<PayMammoth.Modules.BannerModule.IBanner>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.BannerModule.IBanner BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.BannerModule.IBanner>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.BannerModule.IBanner BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.BannerModule.IBanner>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.BannerModule.IBanner> IBaseDbFactory<PayMammoth.Modules.BannerModule.IBanner>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IBanner> IBannerFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IBanner> IBannerFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IBanner> IBannerFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IBanner IBannerFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IBanner IBannerFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IBanner IBannerFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IBanner> IBannerFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IBanner IBannerFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
