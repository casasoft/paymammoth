using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.PaymentMethodModule;
using PayMammoth.Modules.PaymentMethodModule;
using PayMammoth.Modules._AutoGen;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.PaymentMethodModule.PaymentMethodFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.PaymentMethodModule.IPaymentMethod> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.PaymentMethodModule.PaymentMethodFrontend ToFrontend(this PayMammoth.Modules.PaymentMethodModule.IPaymentMethod item)
        {
        	return PayMammoth.Frontend.PaymentMethodModule.PaymentMethodFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaymentMethodFrontend_AutoGen : PayMammoth.Modules._AutoGen.PaymentMethodModule.PaymentMethodBaseFrontend

    {
		
        
        protected PaymentMethodFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.PaymentMethodModule.IPaymentMethod Data
        {
            get { return (PayMammoth.Modules.PaymentMethodModule.IPaymentMethod)base.Data; }
            set { base.Data = value; }

        }

        public new static PaymentMethodFrontend CreateNewItem()
        {
            return (PaymentMethodFrontend)PayMammoth.Modules._AutoGen.PaymentMethodModule.PaymentMethodBaseFrontend.CreateNewItem();
        }
        
        public new static PaymentMethodFrontend Get(PayMammoth.Modules._AutoGen.IPaymentMethodBase data)
        {
            return (PaymentMethodFrontend)PayMammoth.Modules._AutoGen.PaymentMethodModule.PaymentMethodBaseFrontend.Get(data);
        }
        public new static List<PaymentMethodFrontend> GetList(IEnumerable<PayMammoth.Modules._AutoGen.IPaymentMethodBase> dataList)
        {
            return PayMammoth.Modules._AutoGen.PaymentMethodModule.PaymentMethodBaseFrontend.GetList(dataList).Cast<PaymentMethodFrontend>().ToList();
        }


    }
}
