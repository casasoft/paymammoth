using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.CmsUserModule;
using PayMammoth.Modules.CmsUserModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class CmsUserFactoryAutoGen : BusinessLogic_v3.Modules.CmsUserModule.CmsUserBaseFactory, 
    				PayMammoth.Modules._AutoGen.ICmsUserFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<ICmsUser>
    
    {
    
     	public new CmsUser ReloadItemFromDatabase(BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase item)
        {
         	return (CmsUser)base.ReloadItemFromDatabase(item);
        }
     	static CmsUserFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public CmsUserFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(CmsUserImpl);
			PayMammoth.Cms.CmsUserModule.CmsUserCmsFactory._initialiseStaticInstance();
        }
        public static new CmsUserFactory Instance
        {
            get
            {
                return (CmsUserFactory)CmsUserBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<CmsUser> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<CmsUser>();
            
        }
        public new IEnumerable<CmsUser> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<CmsUser>();
            
            
        }
    
        public new IEnumerable<CmsUser> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<CmsUser>();
            
        }
        public new IEnumerable<CmsUser> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<CmsUser>();
        }
        
        public new CmsUser FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (CmsUser)base.FindItem(query);
        }
        public new CmsUser FindItem(IQueryOver query)
        {
            return (CmsUser)base.FindItem(query);
        }
        
        ICmsUser ICmsUserFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new CmsUser CreateNewItem()
        {
            return (CmsUser)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<CmsUser, CmsUser> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<CmsUser>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.CmsUserModule.CmsUser GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.CmsUserModule.CmsUser) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.CmsUserModule.CmsUser> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.CmsUserModule.CmsUser>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.CmsUserModule.CmsUser> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.CmsUserModule.CmsUser>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.CmsUserModule.CmsUser> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.CmsUserModule.CmsUser>) base.FindAll(session);

       }
       public new PayMammoth.Modules.CmsUserModule.CmsUser FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.CmsUserModule.CmsUser) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.CmsUserModule.CmsUser FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.CmsUserModule.CmsUser) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.CmsUserModule.CmsUser FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.CmsUserModule.CmsUser) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.CmsUserModule.CmsUser> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.CmsUserModule.CmsUser>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.CmsUserModule.ICmsUser> Members
     
        PayMammoth.Modules.CmsUserModule.ICmsUser IBaseDbFactory<PayMammoth.Modules.CmsUserModule.ICmsUser>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.CmsUserModule.ICmsUser IBaseDbFactory<PayMammoth.Modules.CmsUserModule.ICmsUser>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.CmsUserModule.ICmsUser> IBaseDbFactory<PayMammoth.Modules.CmsUserModule.ICmsUser>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.CmsUserModule.ICmsUser> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.CmsUserModule.ICmsUser>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.CmsUserModule.ICmsUser> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.CmsUserModule.ICmsUser>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.CmsUserModule.ICmsUser IBaseDbFactory<PayMammoth.Modules.CmsUserModule.ICmsUser>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.CmsUserModule.ICmsUser BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.CmsUserModule.ICmsUser>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.CmsUserModule.ICmsUser BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.CmsUserModule.ICmsUser>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.CmsUserModule.ICmsUser> IBaseDbFactory<PayMammoth.Modules.CmsUserModule.ICmsUser>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<ICmsUser> ICmsUserFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<ICmsUser> ICmsUserFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<ICmsUser> ICmsUserFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       ICmsUser ICmsUserFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       ICmsUser ICmsUserFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       ICmsUser ICmsUserFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<ICmsUser> ICmsUserFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       ICmsUser ICmsUserFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
