using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.PaymentRequestTransactionModule;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Frontend.PaymentRequestTransactionModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class PaymentRequestTransactionCmsInfo_AutoGen : PayMammoth.Modules._AutoGen.PaymentRequestTransactionBaseCmsInfo
    {

		public PaymentRequestTransactionCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.PaymentRequestTransactionBase item)
            : base(item)
        {

        }
        

        private PaymentRequestTransactionFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = PaymentRequestTransactionFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new PaymentRequestTransactionFrontend FrontendItem
        {
            get { return (PaymentRequestTransactionFrontend) base.FrontendItem; }
        }
        
        
		public new PaymentRequestTransaction DbItem
        {
            get
            {
                return (PaymentRequestTransaction)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

//InitFieldUser_LinkedProperty
			this.PaymentRequest = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction>.GetPropertyBySelector( item => item.PaymentRequest),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.Transactions)));

        
        this.Successful = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction>.GetPropertyBySelector( item => item.Successful),
        						isEditable: true, isVisible: true);

        this.PaymentMethod = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction>.GetPropertyBySelector( item => item.PaymentMethod),
        						isEditable: true, isVisible: true);

        this.Comments = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction>.GetPropertyBySelector( item => item.Comments),
        						isEditable: true, isVisible: true);

        this.IP = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction>.GetPropertyBySelector( item => item.IP),
        						isEditable: true, isVisible: true);

        this.AuthCode = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction>.GetPropertyBySelector( item => item.AuthCode),
        						isEditable: true, isVisible: true);

        this.StartDate = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction>.GetPropertyBySelector( item => item.StartDate),
        						isEditable: true, isVisible: true);

        this.LastUpdatedOn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction>.GetPropertyBySelector( item => item.LastUpdatedOn),
        						isEditable: true, isVisible: true);

        this.EndDate = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction>.GetPropertyBySelector( item => item.EndDate),
        						isEditable: true, isVisible: true);

        this.Reference = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction>.GetPropertyBySelector( item => item.Reference),
        						isEditable: true, isVisible: true);

        this.TransactionInfo = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction>.GetPropertyBySelector( item => item.TransactionInfo),
        						isEditable: true, isVisible: true);

        this.PaymentParameters = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction>.GetPropertyBySelector( item => item.PaymentParameters),
        						isEditable: true, isVisible: true);

        this.PaymentMethodVersion = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction>.GetPropertyBySelector( item => item.PaymentMethodVersion),
        						isEditable: true, isVisible: true);

        this.FakePayment = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction>.GetPropertyBySelector( item => item.FakePayment),
        						isEditable: true, isVisible: true);

        this.RequiresManualIntervention = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction>.GetPropertyBySelector( item => item.RequiresManualIntervention),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
