using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.EmailLogModule;
using PayMammoth.Modules.EmailLogModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class EmailLogFactoryAutoGen : BusinessLogic_v3.Modules.EmailLogModule.EmailLogBaseFactory, 
    				PayMammoth.Modules._AutoGen.IEmailLogFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<IEmailLog>
    
    {
    
     	public new EmailLog ReloadItemFromDatabase(BusinessLogic_v3.Modules.EmailLogModule.EmailLogBase item)
        {
         	return (EmailLog)base.ReloadItemFromDatabase(item);
        }
     	static EmailLogFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public EmailLogFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(EmailLogImpl);
			PayMammoth.Cms.EmailLogModule.EmailLogCmsFactory._initialiseStaticInstance();
        }
        public static new EmailLogFactory Instance
        {
            get
            {
                return (EmailLogFactory)EmailLogBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<EmailLog> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<EmailLog>();
            
        }
        public new IEnumerable<EmailLog> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<EmailLog>();
            
            
        }
    
        public new IEnumerable<EmailLog> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<EmailLog>();
            
        }
        public new IEnumerable<EmailLog> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<EmailLog>();
        }
        
        public new EmailLog FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (EmailLog)base.FindItem(query);
        }
        public new EmailLog FindItem(IQueryOver query)
        {
            return (EmailLog)base.FindItem(query);
        }
        
        IEmailLog IEmailLogFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new EmailLog CreateNewItem()
        {
            return (EmailLog)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<EmailLog, EmailLog> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<EmailLog>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.EmailLogModule.EmailLog GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.EmailLogModule.EmailLog) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.EmailLogModule.EmailLog> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.EmailLogModule.EmailLog>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.EmailLogModule.EmailLog> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.EmailLogModule.EmailLog>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.EmailLogModule.EmailLog> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.EmailLogModule.EmailLog>) base.FindAll(session);

       }
       public new PayMammoth.Modules.EmailLogModule.EmailLog FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.EmailLogModule.EmailLog) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.EmailLogModule.EmailLog FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.EmailLogModule.EmailLog) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.EmailLogModule.EmailLog FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.EmailLogModule.EmailLog) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.EmailLogModule.EmailLog> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.EmailLogModule.EmailLog>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.EmailLogModule.IEmailLog> Members
     
        PayMammoth.Modules.EmailLogModule.IEmailLog IBaseDbFactory<PayMammoth.Modules.EmailLogModule.IEmailLog>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.EmailLogModule.IEmailLog IBaseDbFactory<PayMammoth.Modules.EmailLogModule.IEmailLog>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.EmailLogModule.IEmailLog> IBaseDbFactory<PayMammoth.Modules.EmailLogModule.IEmailLog>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.EmailLogModule.IEmailLog> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.EmailLogModule.IEmailLog>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.EmailLogModule.IEmailLog> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.EmailLogModule.IEmailLog>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.EmailLogModule.IEmailLog IBaseDbFactory<PayMammoth.Modules.EmailLogModule.IEmailLog>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.EmailLogModule.IEmailLog BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.EmailLogModule.IEmailLog>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.EmailLogModule.IEmailLog BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.EmailLogModule.IEmailLog>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.EmailLogModule.IEmailLog> IBaseDbFactory<PayMammoth.Modules.EmailLogModule.IEmailLog>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IEmailLog> IEmailLogFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IEmailLog> IEmailLogFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IEmailLog> IEmailLogFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IEmailLog IEmailLogFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IEmailLog IEmailLogFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IEmailLog IEmailLogFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IEmailLog> IEmailLogFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IEmailLog IEmailLogFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
