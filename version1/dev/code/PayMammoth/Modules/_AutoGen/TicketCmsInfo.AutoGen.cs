using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.TicketModule;
using CS.WebComponentsGeneralV3.Cms.TicketModule;
using PayMammoth.Frontend.TicketModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class TicketCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.TicketModule.TicketBaseCmsInfo
    {

		public TicketCmsInfo_AutoGen(BusinessLogic_v3.Modules.TicketModule.TicketBase item)
            : base(item)
        {

        }
        

        private TicketFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = TicketFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new TicketFrontend FrontendItem
        {
            get { return (TicketFrontend) base.FrontendItem; }
        }
        
        
		public new Ticket DbItem
        {
            get
            {
                return (Ticket)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.IPAddress = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.TicketModule.Ticket>.GetPropertyBySelector( item => item.IPAddress),
        						isEditable: true, isVisible: true);

        this.CreatedOn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.TicketModule.Ticket>.GetPropertyBySelector( item => item.CreatedOn),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.AssignedTo = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.TicketModule.Ticket>.GetPropertyBySelector( item => item.AssignedTo),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsUserModule.CmsUser>.GetPropertyBySelector( item => item.AssignedTickets)));

        
//InitFieldUser_LinkedProperty
			this.Member = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.TicketModule.Ticket>.GetPropertyBySelector( item => item.Member),
                null));

        
        this.ClosedOn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.TicketModule.Ticket>.GetPropertyBySelector( item => item.ClosedOn),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.ClosedBy = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.TicketModule.Ticket>.GetPropertyBySelector( item => item.ClosedBy),
                null));

        
        this.ClosedByIP = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.TicketModule.Ticket>.GetPropertyBySelector( item => item.ClosedByIP),
        						isEditable: true, isVisible: true);

        this.ReOpenedOn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.TicketModule.Ticket>.GetPropertyBySelector( item => item.ReOpenedOn),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.ReOpenedByCmsUser = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.TicketModule.Ticket>.GetPropertyBySelector( item => item.ReOpenedByCmsUser),
                null));

        
        this.ReOpenedByIP = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.TicketModule.Ticket>.GetPropertyBySelector( item => item.ReOpenedByIP),
        						isEditable: true, isVisible: true);

        this.Title = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.TicketModule.Ticket>.GetPropertyBySelector( item => item.Title),
        						isEditable: true, isVisible: true);

        this.TicketPriority = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.TicketModule.Ticket>.GetPropertyBySelector( item => item.TicketPriority),
        						isEditable: true, isVisible: true);

        this.TicketState = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.TicketModule.Ticket>.GetPropertyBySelector( item => item.TicketState),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.ReOpenedByMember = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.TicketModule.Ticket>.GetPropertyBySelector( item => item.ReOpenedByMember),
                null));

        
	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
