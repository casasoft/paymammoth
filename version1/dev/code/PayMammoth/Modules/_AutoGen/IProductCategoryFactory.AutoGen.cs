using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.NHibernateClasses;

using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ProductCategoryModule;
using PayMammoth.Modules.ProductCategoryModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IProductCategoryFactoryAutoGen : BusinessLogic_v3.Modules.ProductCategoryModule.IProductCategoryBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IProductCategory>
    
    {
	    new PayMammoth.Modules.ProductCategoryModule.IProductCategory CreateNewItem();
        new PayMammoth.Modules.ProductCategoryModule.IProductCategory GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ProductCategoryModule.IProductCategory> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ProductCategoryModule.IProductCategory> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ProductCategoryModule.IProductCategory> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.ProductCategoryModule.IProductCategory FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ProductCategoryModule.IProductCategory FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ProductCategoryModule.IProductCategory FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ProductCategoryModule.IProductCategory> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
