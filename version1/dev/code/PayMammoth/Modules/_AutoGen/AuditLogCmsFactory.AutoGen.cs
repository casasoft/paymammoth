using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.AuditLogModule;
using PayMammoth.Cms.AuditLogModule;
using CS.WebComponentsGeneralV3.Cms.AuditLogModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class AuditLogCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.AuditLogModule.AuditLogBaseCmsFactory
    {
        public static new AuditLogBaseCmsFactory Instance 
        {
         	get { return (AuditLogBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.AuditLogModule.AuditLogBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override AuditLogBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	AuditLog item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = AuditLog.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        AuditLogCmsInfo cmsItem = new AuditLogCmsInfo(item);
            return cmsItem;
        }    
        
        public override AuditLogBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.AuditLogModule.AuditLogBase item)
        {
            return new AuditLogCmsInfo((AuditLog)item);
            
        }

    }
}
