using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.TicketModule;
using BusinessLogic_v3.Modules.TicketModule;


//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
    public abstract class Ticket_AutoGen : TicketBase, PayMammoth.Modules._AutoGen.ITicketAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.CmsUserModule.CmsUser AssignedTo
        {
            get
            {
                return (PayMammoth.Modules.CmsUserModule.CmsUser)base.AssignedTo;
            }
            set
            {
                base.AssignedTo = value;
            }
        }

		PayMammoth.Modules.CmsUserModule.ICmsUser ITicketAutoGen.AssignedTo
        {
            get
            {
            	return this.AssignedTo;
                
            }
            set
            {
                this.AssignedTo = (PayMammoth.Modules.CmsUserModule.CmsUser)value;
            }
        }
		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.CmsUserModule.CmsUser ClosedBy
        {
            get
            {
                return (PayMammoth.Modules.CmsUserModule.CmsUser)base.ClosedBy;
            }
            set
            {
                base.ClosedBy = value;
            }
        }

		PayMammoth.Modules.CmsUserModule.ICmsUser ITicketAutoGen.ClosedBy
        {
            get
            {
            	return this.ClosedBy;
                
            }
            set
            {
                this.ClosedBy = (PayMammoth.Modules.CmsUserModule.CmsUser)value;
            }
        }
		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.CmsUserModule.CmsUser ReOpenedByCmsUser
        {
            get
            {
                return (PayMammoth.Modules.CmsUserModule.CmsUser)base.ReOpenedByCmsUser;
            }
            set
            {
                base.ReOpenedByCmsUser = value;
            }
        }

		PayMammoth.Modules.CmsUserModule.ICmsUser ITicketAutoGen.ReOpenedByCmsUser
        {
            get
            {
            	return this.ReOpenedByCmsUser;
                
            }
            set
            {
                this.ReOpenedByCmsUser = (PayMammoth.Modules.CmsUserModule.CmsUser)value;
            }
        }



// [userclass_collections_nhibernate]

        //UserClass_CollectionOneToManyLeftSide
        
		new internal class __TicketCommentsCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.TicketModule.Ticket, 
        	PayMammoth.Modules.TicketCommentModule.TicketComment,
        	PayMammoth.Modules.TicketCommentModule.ITicketComment>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.TicketModule.Ticket, PayMammoth.Modules.TicketCommentModule.TicketComment>
        {
            private PayMammoth.Modules.TicketModule.Ticket _item = null;
            public __TicketCommentsCollectionInfo(PayMammoth.Modules.TicketModule.Ticket item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.TicketCommentModule.TicketComment> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.TicketCommentModule.TicketComment>)_item.__collection__TicketComments; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.TicketModule.Ticket, PayMammoth.Modules.TicketCommentModule.TicketComment>.SetLinkOnItem(PayMammoth.Modules.TicketCommentModule.TicketComment item, PayMammoth.Modules.TicketModule.Ticket value)
            {
                item.Ticket = value;
            }
        }
        
        private __TicketCommentsCollectionInfo _TicketComments = null;
        internal new __TicketCommentsCollectionInfo TicketComments
        {
            get
            {
                if (_TicketComments == null)
                    _TicketComments = new __TicketCommentsCollectionInfo((PayMammoth.Modules.TicketModule.Ticket)this);
                return _TicketComments;
            }
        }               
           
		ICollectionManager<PayMammoth.Modules.TicketCommentModule.ITicketComment> ITicketAutoGen.TicketComments
        {
            get {  return this.TicketComments; }
        }
           

        
          
		public  static new TicketFactory Factory
        {
            get
            {
                return (TicketFactory)TicketBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<Ticket, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
