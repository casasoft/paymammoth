using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.TicketCommentModule;
using PayMammoth.Modules.TicketCommentModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface ITicketCommentFactoryAutoGen : BusinessLogic_v3.Modules.TicketCommentModule.ITicketCommentBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<ITicketComment>
    
    {
	    new PayMammoth.Modules.TicketCommentModule.ITicketComment CreateNewItem();
        new PayMammoth.Modules.TicketCommentModule.ITicketComment GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.TicketCommentModule.ITicketComment> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.TicketCommentModule.ITicketComment> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.TicketCommentModule.ITicketComment> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.TicketCommentModule.ITicketComment FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.TicketCommentModule.ITicketComment FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.TicketCommentModule.ITicketComment FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.TicketCommentModule.ITicketComment> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
