using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.ProductVariationMediaItemModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class ProductVariationMediaItemMap_AutoGen : ProductVariationMediaItemMap_AutoGen_Z
    {
        public ProductVariationMediaItemMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]

			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.ProductVariation, ProductVariationMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ImageFilename, ImageFilenameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Caption, CaptionMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ExtraValueChoice, ExtraValueChoiceMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Title, TitleMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Reference, ReferenceMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ImportReference, ImportReferenceMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Size, SizeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Colour, ColourMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class ProductVariationMediaItemMap_AutoGen_Z : BusinessLogic_v3.Modules.ProductVariationMediaItemModule.ProductVariationMediaItemBaseMap<ProductVariationMediaItemImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
