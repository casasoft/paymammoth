using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.NHibernateClasses;

using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule;
using PayMammoth.Modules.ProductCategorySpecificationValueModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IProductCategorySpecificationValueFactoryAutoGen : BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValueBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IProductCategorySpecificationValue>
    
    {
	    new PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue CreateNewItem();
        new PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
