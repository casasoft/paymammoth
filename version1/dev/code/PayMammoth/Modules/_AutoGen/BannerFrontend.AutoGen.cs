using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.BannerModule;
using PayMammoth.Modules.BannerModule;
using BusinessLogic_v3.Modules.BannerModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.BannerModule.BannerFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.BannerModule.IBanner> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.BannerModule.BannerFrontend ToFrontend(this PayMammoth.Modules.BannerModule.IBanner item)
        {
        	return PayMammoth.Frontend.BannerModule.BannerFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class BannerFrontend_AutoGen : BusinessLogic_v3.Frontend.BannerModule.BannerBaseFrontend

    {
		
        
        protected BannerFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.BannerModule.IBanner Data
        {
            get { return (PayMammoth.Modules.BannerModule.IBanner)base.Data; }
            set { base.Data = value; }

        }

        public new static BannerFrontend CreateNewItem()
        {
            return (BannerFrontend)BusinessLogic_v3.Frontend.BannerModule.BannerBaseFrontend.CreateNewItem();
        }
        
        public new static BannerFrontend Get(BusinessLogic_v3.Modules.BannerModule.IBannerBase data)
        {
            return (BannerFrontend)BusinessLogic_v3.Frontend.BannerModule.BannerBaseFrontend.Get(data);
        }
        public new static List<BannerFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.BannerModule.IBannerBase> dataList)
        {
            return BusinessLogic_v3.Frontend.BannerModule.BannerBaseFrontend.GetList(dataList).Cast<BannerFrontend>().ToList();
        }


    }
}
