using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using PayMammoth.Modules._AutoGen;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace PayMammoth.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "PaymentRequest")]
    public abstract class PaymentRequestBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IPaymentRequestBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static PaymentRequestBaseFactory Factory
        {
            get
            {
                return PaymentRequestBaseFactory.Instance; 
            }
        }    
        /*
        public static PaymentRequestBase CreateNewItem()
        {
            return (PaymentRequestBase)Factory.CreateNewItem();
        }

		public static PaymentRequestBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<PaymentRequestBase, PaymentRequestBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static PaymentRequestBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static PaymentRequestBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<PaymentRequestBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<PaymentRequestBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<PaymentRequestBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _identifier;
        
        public virtual string Identifier 
        {
        	get
        	{
        		return _identifier;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_identifier,value);
        		_identifier = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime _datetime;
        
        public virtual DateTime DateTime 
        {
        	get
        	{
        		return _datetime;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_datetime,value);
        		_datetime = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _requestip;
        
        public virtual string RequestIP 
        {
        	get
        	{
        		return _requestip;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_requestip,value);
        		_requestip = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _clientfirstname;
        
        public virtual string ClientFirstName 
        {
        	get
        	{
        		return _clientfirstname;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_clientfirstname,value);
        		_clientfirstname = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _clientmiddlename;
        
        public virtual string ClientMiddleName 
        {
        	get
        	{
        		return _clientmiddlename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_clientmiddlename,value);
        		_clientmiddlename = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _clientlastname;
        
        public virtual string ClientLastName 
        {
        	get
        	{
        		return _clientlastname;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_clientlastname,value);
        		_clientlastname = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _clientaddress1;
        
        public virtual string ClientAddress1 
        {
        	get
        	{
        		return _clientaddress1;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_clientaddress1,value);
        		_clientaddress1 = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _clientaddress2;
        
        public virtual string ClientAddress2 
        {
        	get
        	{
        		return _clientaddress2;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_clientaddress2,value);
        		_clientaddress2 = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _clientaddress3;
        
        public virtual string ClientAddress3 
        {
        	get
        	{
        		return _clientaddress3;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_clientaddress3,value);
        		_clientaddress3 = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _clientlocality;
        
        public virtual string ClientLocality 
        {
        	get
        	{
        		return _clientlocality;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_clientlocality,value);
        		_clientlocality = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? _clientcountry;
        
        public virtual CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? ClientCountry 
        {
        	get
        	{
        		return _clientcountry;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_clientcountry,value);
        		_clientcountry = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _clientpostcode;
        
        public virtual string ClientPostCode 
        {
        	get
        	{
        		return _clientpostcode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_clientpostcode,value);
        		_clientpostcode = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _description;
        
        public virtual string Description 
        {
        	get
        	{
        		return _description;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_description,value);
        		_description = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _clientemail;
        
        public virtual string ClientEmail 
        {
        	get
        	{
        		return _clientemail;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_clientemail,value);
        		_clientemail = value;
        		
        	}
        }
        
/*
		public virtual long? WebsiteAccountID
		{
		 	get 
		 	{
		 		return (this.WebsiteAccount != null ? (long?)this.WebsiteAccount.ID : null);
		 	}
		}*/
		
        protected PayMammoth.Modules._AutoGen.WebsiteAccountBase _websiteaccount;
        public virtual PayMammoth.Modules._AutoGen.WebsiteAccountBase WebsiteAccount 
        {
        	get
        	{
        		return _websiteaccount;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_websiteaccount,value);
        		_websiteaccount = value;
        	}
        }
            
        PayMammoth.Modules._AutoGen.IWebsiteAccountBase PayMammoth.Modules._AutoGen.IPaymentRequestBaseAutoGen.WebsiteAccount 
        {
            get
            {
            	return this.WebsiteAccount;
            }
            set
            {
            	this.WebsiteAccount = (PayMammoth.Modules._AutoGen.WebsiteAccountBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private double _taxamount;
        
        public virtual double TaxAmount 
        {
        	get
        	{
        		return _taxamount;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_taxamount,value);
        		_taxamount = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _handlingamount;
        
        public virtual double HandlingAmount 
        {
        	get
        	{
        		return _handlingamount;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_handlingamount,value);
        		_handlingamount = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _shippingamount;
        
        public virtual double ShippingAmount 
        {
        	get
        	{
        		return _shippingamount;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_shippingamount,value);
        		_shippingamount = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _reference;
        
        public virtual string Reference 
        {
        	get
        	{
        		return _reference;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_reference,value);
        		_reference = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _notes;
        
        public virtual string Notes 
        {
        	get
        	{
        		return _notes;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_notes,value);
        		_notes = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _priceexctax;
        
        public virtual double PriceExcTax 
        {
        	get
        	{
        		return _priceexctax;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_priceexctax,value);
        		_priceexctax = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 _currencycode;
        
        public virtual CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 CurrencyCode 
        {
        	get
        	{
        		return _currencycode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_currencycode,value);
        		_currencycode = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _orderlinkonclientwebsite;
        
        public virtual string OrderLinkOnClientWebsite 
        {
        	get
        	{
        		return _orderlinkonclientwebsite;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_orderlinkonclientwebsite,value);
        		_orderlinkonclientwebsite = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
/*
		public virtual long? CurrentTransactionID
		{
		 	get 
		 	{
		 		return (this.CurrentTransaction != null ? (long?)this.CurrentTransaction.ID : null);
		 	}
		}*/
		
        protected PayMammoth.Modules._AutoGen.PaymentRequestTransactionBase _currenttransaction;
        public virtual PayMammoth.Modules._AutoGen.PaymentRequestTransactionBase CurrentTransaction 
        {
        	get
        	{
        		return _currenttransaction;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_currenttransaction,value);
        		_currenttransaction = value;
        	}
        }
            
        PayMammoth.Modules._AutoGen.IPaymentRequestTransactionBase PayMammoth.Modules._AutoGen.IPaymentRequestBaseAutoGen.CurrentTransaction 
        {
            get
            {
            	return this.CurrentTransaction;
            }
            set
            {
            	this.CurrentTransaction = (PayMammoth.Modules._AutoGen.PaymentRequestTransactionBase) value;
            }
        }
            
		//BaseClass/Fields/Property_Normal
        private bool _paid;
        
        public virtual bool Paid 
        {
        	get
        	{
        		return _paid;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paid,value);
        		_paid = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _paidon;
        
        public virtual DateTime? PaidOn 
        {
        	get
        	{
        		return _paidon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paidon,value);
        		_paidon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private PayMammoth.Connector.Enums.PaymentMethodSpecific? _paidbypaymentmethod;
        
        public virtual PayMammoth.Connector.Enums.PaymentMethodSpecific? PaidByPaymentMethod 
        {
        	get
        	{
        		return _paidbypaymentmethod;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paidbypaymentmethod,value);
        		_paidbypaymentmethod = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _returnurlsuccess;
        
        public virtual string ReturnUrlSuccess 
        {
        	get
        	{
        		return _returnurlsuccess;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_returnurlsuccess,value);
        		_returnurlsuccess = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _returnurlfailure;
        
        public virtual string ReturnUrlFailure 
        {
        	get
        	{
        		return _returnurlfailure;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_returnurlfailure,value);
        		_returnurlfailure = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639? _languagecode;
        
        public virtual CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639? LanguageCode 
        {
        	get
        	{
        		return _languagecode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_languagecode,value);
        		_languagecode = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? _languagecountrycode;
        
        public virtual CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? LanguageCountryCode 
        {
        	get
        	{
        		return _languagecountrycode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_languagecountrycode,value);
        		_languagecountrycode = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _languagesuffix;
        
        public virtual string LanguageSuffix 
        {
        	get
        	{
        		return _languagesuffix;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_languagesuffix,value);
        		_languagesuffix = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _clienttelephone;
        
        public virtual string ClientTelephone 
        {
        	get
        	{
        		return _clienttelephone;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_clienttelephone,value);
        		_clienttelephone = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _clientmobile;
        
        public virtual string ClientMobile 
        {
        	get
        	{
        		return _clientmobile;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_clientmobile,value);
        		_clientmobile = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _clienttitle;
        
        public virtual string ClientTitle 
        {
        	get
        	{
        		return _clienttitle;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_clienttitle,value);
        		_clienttitle = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _clientip;
        
        public virtual string ClientIp 
        {
        	get
        	{
        		return _clientip;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_clientip,value);
        		_clientip = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _clientreference;
        
        public virtual string ClientReference 
        {
        	get
        	{
        		return _clientreference;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_clientreference,value);
        		_clientreference = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _paymentnotificationacknowledged;
        
        public virtual bool PaymentNotificationAcknowledged 
        {
        	get
        	{
        		return _paymentnotificationacknowledged;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentnotificationacknowledged,value);
        		_paymentnotificationacknowledged = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _requiresmanualintervention;
        
        public virtual bool RequiresManualIntervention 
        {
        	get
        	{
        		return _requiresmanualintervention;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_requiresmanualintervention,value);
        		_requiresmanualintervention = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _paymentnotificationacknowledgedon;
        
        public virtual DateTime? PaymentNotificationAcknowledgedOn 
        {
        	get
        	{
        		return _paymentnotificationacknowledgedon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentnotificationacknowledgedon,value);
        		_paymentnotificationacknowledgedon = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _paymentnotificationretrycount;
        
        public virtual int PaymentNotificationRetryCount 
        {
        	get
        	{
        		return _paymentnotificationretrycount;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentnotificationretrycount,value);
        		_paymentnotificationretrycount = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _paymentnotificationenabled;
        
        public virtual bool PaymentNotificationEnabled 
        {
        	get
        	{
        		return _paymentnotificationenabled;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentnotificationenabled,value);
        		_paymentnotificationenabled = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private DateTime? _paymentnotificationnextretryon;
        
        public virtual DateTime? PaymentNotificationNextRetryOn 
        {
        	get
        	{
        		return _paymentnotificationnextretryon;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentnotificationnextretryon,value);
        		_paymentnotificationnextretryon = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
