using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.PaymentMethod_CultureInfoModule;
using PayMammoth.Modules._AutoGen;

//DbObjectBuilder v8

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{    

	
    public abstract class PaymentMethod_CultureInfo_AutoGen : PaymentMethod_CultureInfoBase, PayMammoth.Modules._AutoGen.IPaymentMethod_CultureInfoAutoGen
	
      
      
      , IMultilingualContentInfo
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.CultureDetailsModule.CultureDetails CultureInfo
        {
            get
            {
                return (PayMammoth.Modules.CultureDetailsModule.CultureDetails)base.CultureInfo;
            }
            set
            {
                base.CultureInfo = value;
            }
        }

		PayMammoth.Modules.CultureDetailsModule.ICultureDetails IPaymentMethod_CultureInfoAutoGen.CultureInfo
        {
            get
            {
            	return this.CultureInfo;
                
            }
            set
            {
                this.CultureInfo = (PayMammoth.Modules.CultureDetailsModule.CultureDetails)value;
            }
        }
		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.PaymentMethodModule.PaymentMethod PaymentMethod
        {
            get
            {
                return (PayMammoth.Modules.PaymentMethodModule.PaymentMethod)base.PaymentMethod;
            }
            set
            {
                base.PaymentMethod = value;
            }
        }

		PayMammoth.Modules.PaymentMethodModule.IPaymentMethod IPaymentMethod_CultureInfoAutoGen.PaymentMethod
        {
            get
            {
            	return this.PaymentMethod;
                
            }
            set
            {
                this.PaymentMethod = (PayMammoth.Modules.PaymentMethodModule.PaymentMethod)value;
            }
        }



// [userclass_collections_nhibernate]


        
          
		public  static new PaymentMethod_CultureInfoFactory Factory
        {
            get
            {
                return (PaymentMethod_CultureInfoFactory)PaymentMethod_CultureInfoBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<PaymentMethod_CultureInfo, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
