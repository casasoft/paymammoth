using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.ContactFormModule;
using PayMammoth.Cms.ContactFormModule;
using CS.WebComponentsGeneralV3.Cms.ContactFormModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class ContactFormCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.ContactFormModule.ContactFormBaseCmsFactory
    {
        public static new ContactFormBaseCmsFactory Instance 
        {
         	get { return (ContactFormBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.ContactFormModule.ContactFormBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override ContactFormBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	ContactForm item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = ContactForm.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        ContactFormCmsInfo cmsItem = new ContactFormCmsInfo(item);
            return cmsItem;
        }    
        
        public override ContactFormBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase item)
        {
            return new ContactFormCmsInfo((ContactForm)item);
            
        }

    }
}
