using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.ContentText_CultureInfoModule;
using PayMammoth.Modules.ContentText_CultureInfoModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class ContentText_CultureInfoFactoryAutoGen : BusinessLogic_v3.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoBaseFactory, 
    				PayMammoth.Modules._AutoGen.IContentText_CultureInfoFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<IContentText_CultureInfo>
    
    {
    
     	public new ContentText_CultureInfo ReloadItemFromDatabase(BusinessLogic_v3.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoBase item)
        {
         	return (ContentText_CultureInfo)base.ReloadItemFromDatabase(item);
        }
     	static ContentText_CultureInfoFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public ContentText_CultureInfoFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(ContentText_CultureInfoImpl);
			PayMammoth.Cms.ContentText_CultureInfoModule.ContentText_CultureInfoCmsFactory._initialiseStaticInstance();
        }
        public static new ContentText_CultureInfoFactory Instance
        {
            get
            {
                return (ContentText_CultureInfoFactory)ContentText_CultureInfoBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<ContentText_CultureInfo> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<ContentText_CultureInfo>();
            
        }
        public new IEnumerable<ContentText_CultureInfo> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<ContentText_CultureInfo>();
            
            
        }
    
        public new IEnumerable<ContentText_CultureInfo> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<ContentText_CultureInfo>();
            
        }
        public new IEnumerable<ContentText_CultureInfo> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<ContentText_CultureInfo>();
        }
        
        public new ContentText_CultureInfo FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (ContentText_CultureInfo)base.FindItem(query);
        }
        public new ContentText_CultureInfo FindItem(IQueryOver query)
        {
            return (ContentText_CultureInfo)base.FindItem(query);
        }
        
        IContentText_CultureInfo IContentText_CultureInfoFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new ContentText_CultureInfo CreateNewItem()
        {
            return (ContentText_CultureInfo)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<ContentText_CultureInfo, ContentText_CultureInfo> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<ContentText_CultureInfo>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo>) base.FindAll(session);

       }
       public new PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo> Members
     
        PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo IBaseDbFactory<PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo IBaseDbFactory<PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo> IBaseDbFactory<PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo IBaseDbFactory<PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo> IBaseDbFactory<PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IContentText_CultureInfo> IContentText_CultureInfoFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IContentText_CultureInfo> IContentText_CultureInfoFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IContentText_CultureInfo> IContentText_CultureInfoFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IContentText_CultureInfo IContentText_CultureInfoFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IContentText_CultureInfo IContentText_CultureInfoFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IContentText_CultureInfo IContentText_CultureInfoFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IContentText_CultureInfo> IContentText_CultureInfoFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IContentText_CultureInfo IContentText_CultureInfoFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
