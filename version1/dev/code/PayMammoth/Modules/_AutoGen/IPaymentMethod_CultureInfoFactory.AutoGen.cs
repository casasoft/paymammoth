using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.PaymentMethod_CultureInfoModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IPaymentMethod_CultureInfoFactoryAutoGen : PayMammoth.Modules._AutoGen.IPaymentMethod_CultureInfoBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IPaymentMethod_CultureInfo>
    
    {
	    new PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo CreateNewItem();
        new PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null);
        new IEnumerable<PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
