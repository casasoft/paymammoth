using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using CS.General_v3.Classes.DbObjects.Collections;
using CS.General_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.ProductCategorySpecificationValueModule;
using BusinessLogic_v3.Modules.ProductCategorySpecificationValueModule;


//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
    public abstract class ProductCategorySpecificationValue_AutoGen : ProductCategorySpecificationValueBase, PayMammoth.Modules._AutoGen.IProductCategorySpecificationValueAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.ProductModule.Product Product
        {
            get
            {
                return (PayMammoth.Modules.ProductModule.Product)base.Product;
            }
            set
            {
                base.Product = value;
            }
        }

		PayMammoth.Modules.ProductModule.IProduct IProductCategorySpecificationValueAutoGen.Product
        {
            get
            {
            	return this.Product;
                
            }
            set
            {
                this.Product = (PayMammoth.Modules.ProductModule.Product)value;
            }
        }



// [userclass_collections_nhibernate]


        
          
		public  static new ProductCategorySpecificationValueFactory Factory
        {
            get
            {
                return (ProductCategorySpecificationValueFactory)ProductCategorySpecificationValueBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<ProductCategorySpecificationValue, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
