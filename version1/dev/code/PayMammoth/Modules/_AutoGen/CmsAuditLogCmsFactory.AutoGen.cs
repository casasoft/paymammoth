using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.CmsAuditLogModule;
using PayMammoth.Cms.CmsAuditLogModule;
using CS.WebComponentsGeneralV3.Cms.CmsAuditLogModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class CmsAuditLogCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.CmsAuditLogModule.CmsAuditLogBaseCmsFactory
    {
        public static new CmsAuditLogBaseCmsFactory Instance 
        {
         	get { return (CmsAuditLogBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.CmsAuditLogModule.CmsAuditLogBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override CmsAuditLogBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = CmsAuditLog.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            CmsAuditLogCmsInfo cmsItem = new CmsAuditLogCmsInfo(item);
            return cmsItem;
        }    
        
        public override CmsAuditLogBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.CmsAuditLogModule.CmsAuditLogBase item)
        {
            return new CmsAuditLogCmsInfo((CmsAuditLog)item);
            
        }

    }
}
