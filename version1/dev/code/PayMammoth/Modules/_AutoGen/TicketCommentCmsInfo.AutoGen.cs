using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.TicketCommentModule;
using CS.WebComponentsGeneralV3.Cms.TicketCommentModule;
using PayMammoth.Frontend.TicketCommentModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class TicketCommentCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.TicketCommentModule.TicketCommentBaseCmsInfo
    {

		public TicketCommentCmsInfo_AutoGen(BusinessLogic_v3.Modules.TicketCommentModule.TicketCommentBase item)
            : base(item)
        {

        }
        

        private TicketCommentFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = TicketCommentFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new TicketCommentFrontend FrontendItem
        {
            get { return (TicketCommentFrontend) base.FrontendItem; }
        }
        
        
		public new TicketComment DbItem
        {
            get
            {
                return (TicketComment)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

//InitFieldUser_LinkedProperty
			this.Ticket = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.TicketCommentModule.TicketComment>.GetPropertyBySelector( item => item.Ticket),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.TicketModule.Ticket>.GetPropertyBySelector( item => item.TicketComments)));

        
        this.Comment = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.TicketCommentModule.TicketComment>.GetPropertyBySelector( item => item.Comment),
        						isEditable: true, isVisible: true);

        this.IPAddress = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.TicketCommentModule.TicketComment>.GetPropertyBySelector( item => item.IPAddress),
        						isEditable: true, isVisible: true);

        this.PostedOn = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.TicketCommentModule.TicketComment>.GetPropertyBySelector( item => item.PostedOn),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.AssignedStaffUser = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.TicketCommentModule.TicketComment>.GetPropertyBySelector( item => item.AssignedStaffUser),
                null));

        
//InitFieldUser_LinkedProperty
			this.Member = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.TicketCommentModule.TicketComment>.GetPropertyBySelector( item => item.Member),
                null));

        
        this.CommentType = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.TicketCommentModule.TicketComment>.GetPropertyBySelector( item => item.CommentType),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
