using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.ContactFormModule;
using PayMammoth.Modules.ContactFormModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class ContactFormFactoryAutoGen : BusinessLogic_v3.Modules.ContactFormModule.ContactFormBaseFactory, 
    				PayMammoth.Modules._AutoGen.IContactFormFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<IContactForm>
    
    {
    
     	public new ContactForm ReloadItemFromDatabase(BusinessLogic_v3.Modules.ContactFormModule.ContactFormBase item)
        {
         	return (ContactForm)base.ReloadItemFromDatabase(item);
        }
     	static ContactFormFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public ContactFormFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(ContactFormImpl);
			PayMammoth.Cms.ContactFormModule.ContactFormCmsFactory._initialiseStaticInstance();
        }
        public static new ContactFormFactory Instance
        {
            get
            {
                return (ContactFormFactory)ContactFormBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<ContactForm> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<ContactForm>();
            
        }
        public new IEnumerable<ContactForm> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<ContactForm>();
            
            
        }
    
        public new IEnumerable<ContactForm> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<ContactForm>();
            
        }
        public new IEnumerable<ContactForm> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<ContactForm>();
        }
        
        public new ContactForm FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (ContactForm)base.FindItem(query);
        }
        public new ContactForm FindItem(IQueryOver query)
        {
            return (ContactForm)base.FindItem(query);
        }
        
        IContactForm IContactFormFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new ContactForm CreateNewItem()
        {
            return (ContactForm)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<ContactForm, ContactForm> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<ContactForm>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.ContactFormModule.ContactForm GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.ContactFormModule.ContactForm) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.ContactFormModule.ContactForm> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ContactFormModule.ContactForm>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ContactFormModule.ContactForm> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ContactFormModule.ContactForm>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ContactFormModule.ContactForm> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ContactFormModule.ContactForm>) base.FindAll(session);

       }
       public new PayMammoth.Modules.ContactFormModule.ContactForm FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ContactFormModule.ContactForm) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.ContactFormModule.ContactForm FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ContactFormModule.ContactForm) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.ContactFormModule.ContactForm FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ContactFormModule.ContactForm) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.ContactFormModule.ContactForm> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ContactFormModule.ContactForm>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.ContactFormModule.IContactForm> Members
     
        PayMammoth.Modules.ContactFormModule.IContactForm IBaseDbFactory<PayMammoth.Modules.ContactFormModule.IContactForm>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.ContactFormModule.IContactForm IBaseDbFactory<PayMammoth.Modules.ContactFormModule.IContactForm>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.ContactFormModule.IContactForm> IBaseDbFactory<PayMammoth.Modules.ContactFormModule.IContactForm>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.ContactFormModule.IContactForm> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.ContactFormModule.IContactForm>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.ContactFormModule.IContactForm> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.ContactFormModule.IContactForm>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.ContactFormModule.IContactForm IBaseDbFactory<PayMammoth.Modules.ContactFormModule.IContactForm>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ContactFormModule.IContactForm BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.ContactFormModule.IContactForm>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ContactFormModule.IContactForm BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.ContactFormModule.IContactForm>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.ContactFormModule.IContactForm> IBaseDbFactory<PayMammoth.Modules.ContactFormModule.IContactForm>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IContactForm> IContactFormFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IContactForm> IContactFormFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IContactForm> IContactFormFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IContactForm IContactFormFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IContactForm IContactFormFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IContactForm IContactFormFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IContactForm> IContactFormFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IContactForm IContactFormFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
