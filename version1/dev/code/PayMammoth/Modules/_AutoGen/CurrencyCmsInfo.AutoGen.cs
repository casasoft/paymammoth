using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.CurrencyModule;
using CS.WebComponentsGeneralV3.Cms.CurrencyModule;
using PayMammoth.Frontend.CurrencyModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class CurrencyCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.CurrencyModule.CurrencyBaseCmsInfo
    {

		public CurrencyCmsInfo_AutoGen(BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase item)
            : base(item)
        {

        }
        

        private CurrencyFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = CurrencyFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new CurrencyFrontend FrontendItem
        {
            get { return (CurrencyFrontend) base.FrontendItem; }
        }
        
        
		public new Currency DbItem
        {
            get
            {
                return (Currency)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.Title = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CurrencyModule.Currency>.GetPropertyBySelector( item => item.Title),
        						isEditable: true, isVisible: true);

        this.CurrencyISOCode = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CurrencyModule.Currency>.GetPropertyBySelector( item => item.CurrencyISOCode),
        						isEditable: true, isVisible: true);

        this.Symbol = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CurrencyModule.Currency>.GetPropertyBySelector( item => item.Symbol),
        						isEditable: true, isVisible: true);

        this.ExchangeRateMultiplier = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CurrencyModule.Currency>.GetPropertyBySelector( item => item.ExchangeRateMultiplier),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
