using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.ImageSpecificSizeInfoModule;
using BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule;

//DbObjectBuilder v8

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{    

	
    public abstract class ImageSpecificSizeInfo_AutoGen : ImageSpecificSizeInfoBase, PayMammoth.Modules._AutoGen.IImageSpecificSizeInfoAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo ImageSizingInfo
        {
            get
            {
                return (PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo)base.ImageSizingInfo;
            }
            set
            {
                base.ImageSizingInfo = value;
            }
        }

		PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo IImageSpecificSizeInfoAutoGen.ImageSizingInfo
        {
            get
            {
            	return this.ImageSizingInfo;
                
            }
            set
            {
                this.ImageSizingInfo = (PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo)value;
            }
        }



// [userclass_collections_nhibernate]


        
          
		public  static new ImageSpecificSizeInfoFactory Factory
        {
            get
            {
                return (ImageSpecificSizeInfoFactory)ImageSpecificSizeInfoBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<ImageSpecificSizeInfo, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
