using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.ImageSpecificSizeInfoModule;
using PayMammoth.Cms.ImageSpecificSizeInfoModule;
using CS.WebComponentsGeneralV3.Cms.ImageSpecificSizeInfoModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class ImageSpecificSizeInfoCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoBaseCmsFactory
    {
        public static new ImageSpecificSizeInfoBaseCmsFactory Instance 
        {
         	get { return (ImageSpecificSizeInfoBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override ImageSpecificSizeInfoBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	ImageSpecificSizeInfo item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = ImageSpecificSizeInfo.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        ImageSpecificSizeInfoCmsInfo cmsItem = new ImageSpecificSizeInfoCmsInfo(item);
            return cmsItem;
        }    
        
        public override ImageSpecificSizeInfoBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoBase item)
        {
            return new ImageSpecificSizeInfoCmsInfo((ImageSpecificSizeInfo)item);
            
        }

    }
}
