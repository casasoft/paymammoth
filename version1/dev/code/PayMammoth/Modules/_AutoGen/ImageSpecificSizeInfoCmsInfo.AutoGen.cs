using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.ImageSpecificSizeInfoModule;
using CS.WebComponentsGeneralV3.Cms.ImageSpecificSizeInfoModule;
using PayMammoth.Frontend.ImageSpecificSizeInfoModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class ImageSpecificSizeInfoCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoBaseCmsInfo
    {

		public ImageSpecificSizeInfoCmsInfo_AutoGen(BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoBase item)
            : base(item)
        {

        }
        

        private ImageSpecificSizeInfoFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = ImageSpecificSizeInfoFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new ImageSpecificSizeInfoFrontend FrontendItem
        {
            get { return (ImageSpecificSizeInfoFrontend) base.FrontendItem; }
        }
        
        
		public new ImageSpecificSizeInfo DbItem
        {
            get
            {
                return (ImageSpecificSizeInfo)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

//InitFieldUser_LinkedProperty
			this.ImageSizingInfo = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo>.GetPropertyBySelector( item => item.ImageSizingInfo),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfo>.GetPropertyBySelector( item => item.SizingInfos)));

        
        this.Width = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo>.GetPropertyBySelector( item => item.Width),
        						isEditable: true, isVisible: true);

        this.Height = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo>.GetPropertyBySelector( item => item.Height),
        						isEditable: true, isVisible: true);

        this.Title = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo>.GetPropertyBySelector( item => item.Title),
        						isEditable: true, isVisible: true);

        this.Identifier = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo>.GetPropertyBySelector( item => item.Identifier),
        						isEditable: true, isVisible: true);

        this.Description = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo>.GetPropertyBySelector( item => item.Description),
        						isEditable: true, isVisible: true);

        this.CropIdentifier = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo>.GetPropertyBySelector( item => item.CropIdentifier),
        						isEditable: true, isVisible: true);

        this.ImageFormat = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfo>.GetPropertyBySelector( item => item.ImageFormat),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
