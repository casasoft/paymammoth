using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.WallPrizeLinkModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IWallPrizeLinkFactoryAutoGen : PayMammoth.Modules._AutoGen.IWallPrizeLinkBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IWallPrizeLink>
    
    {
	    new PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink CreateNewItem();
        new PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.WallPrizeLinkModule.IWallPrizeLink> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
