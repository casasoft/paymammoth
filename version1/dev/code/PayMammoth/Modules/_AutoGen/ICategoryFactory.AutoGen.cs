using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.CategoryModule;
using PayMammoth.Modules.CategoryModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface ICategoryFactoryAutoGen : BusinessLogic_v3.Modules.CategoryModule.ICategoryBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<ICategory>
    
    {
	    new PayMammoth.Modules.CategoryModule.ICategory CreateNewItem();
        new PayMammoth.Modules.CategoryModule.ICategory GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.CategoryModule.ICategory> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.CategoryModule.ICategory> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.CategoryModule.ICategory> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.CategoryModule.ICategory FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.CategoryModule.ICategory FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.CategoryModule.ICategory FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.CategoryModule.ICategory> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
