using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.ProductVariationMediaItemModule;
using PayMammoth.Modules.ProductVariationMediaItemModule;
using PayMammoth.Modules.CultureDetailsModule;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class ProductVariationMediaItemFactoryAutoGen : BusinessLogic_v3.Modules.ProductVariationMediaItemModule.ProductVariationMediaItemBaseFactory, 
    				PayMammoth.Modules._AutoGen.IProductVariationMediaItemFactoryAutoGen,
    				CS.General_v3.Classes.DbObjects.IBaseDbFactory<IProductVariationMediaItem>
    
    {
    
     	public new ProductVariationMediaItem ReloadItemFromDatabase(BusinessLogic_v3.Modules.ProductVariationMediaItemModule.ProductVariationMediaItemBase item)
        {
         	return (ProductVariationMediaItem)base.ReloadItemFromDatabase(item);
        }
     	
        public ProductVariationMediaItemFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(ProductVariationMediaItemImpl);
			PayMammoth.Cms.ProductVariationMediaItemModule.ProductVariationMediaItemCmsFactory._initialiseStaticInstance();
        }
        public static new ProductVariationMediaItemFactory Instance
        {
            get
            {
                return (ProductVariationMediaItemFactory)ProductVariationMediaItemBaseFactory.Instance;
            }
      
      

        }
        
        /*
        public new IEnumerable<ProductVariationMediaItem> FindAll()
        {
            return base.FindAll().Cast<ProductVariationMediaItem>();
        }*/
    /*    public new IEnumerable<ProductVariationMediaItem> FindAll(IQuery query)
        {
            
            return base.FindAll(query).Cast<ProductVariationMediaItem>();

        }*/
        public new IEnumerable<ProductVariationMediaItem> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<ProductVariationMediaItem>();
            
        }
        public new IEnumerable<ProductVariationMediaItem> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<ProductVariationMediaItem>();
            
            
        }
     /*  public new IEnumerable<ProductVariationMediaItem> FindAll(ICriteria crit)
        {
            return base.FindAll(crit).Cast<ProductVariationMediaItem>();


        }*/
        public new IEnumerable<ProductVariationMediaItem> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<ProductVariationMediaItem>();
            
        }
        public new IEnumerable<ProductVariationMediaItem> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<ProductVariationMediaItem>();
        }
        
        public new ProductVariationMediaItem FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (ProductVariationMediaItem)base.FindItem(query);
        }
        public new ProductVariationMediaItem FindItem(IQueryOver query)
        {
            return (ProductVariationMediaItem)base.FindItem(query);
        }
        
        IProductVariationMediaItem IProductVariationMediaItemFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new ProductVariationMediaItem CreateNewItem()
        {
            return (ProductVariationMediaItem)base.CreateNewItem();
        }

        public new ProductVariationMediaItem GetByPrimaryKey(long pKey)
        {
            return (ProductVariationMediaItem)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<ProductVariationMediaItem, ProductVariationMediaItem> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();

        	var q = session.QueryOver<ProductVariationMediaItem>();
            qParams.FillQueryOver(q);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem) base.GetByPrimaryKey(id, session);
        }
       public new IEnumerable<PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem> FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem> FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem> FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem>) base.FindAll(session);

       }
       public new PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem> Members
     
        PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem IBaseDbFactory<PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem IBaseDbFactory<PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem>.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem> IBaseDbFactory<PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem>.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem>.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem>.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem IBaseDbFactory<PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem>.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem>.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem>.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem> IBaseDbFactory<PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IProductVariationMediaItem> IProductVariationMediaItemFactoryAutoGen.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IProductVariationMediaItem> IProductVariationMediaItemFactoryAutoGen.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IProductVariationMediaItem> IProductVariationMediaItemFactoryAutoGen.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IProductVariationMediaItem IProductVariationMediaItemFactoryAutoGen.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IProductVariationMediaItem IProductVariationMediaItemFactoryAutoGen.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IProductVariationMediaItem IProductVariationMediaItemFactoryAutoGen.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IProductVariationMediaItem> IProductVariationMediaItemFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IProductVariationMediaItem IProductVariationMediaItemFactoryAutoGen.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
