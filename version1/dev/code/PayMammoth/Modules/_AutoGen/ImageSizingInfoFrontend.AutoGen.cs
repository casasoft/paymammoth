using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.ImageSizingInfoModule;
using PayMammoth.Modules.ImageSizingInfoModule;
using BusinessLogic_v3.Modules.ImageSizingInfoModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.ImageSizingInfoModule.ImageSizingInfoFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.ImageSizingInfoModule.ImageSizingInfoFrontend ToFrontend(this PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo item)
        {
        	return PayMammoth.Frontend.ImageSizingInfoModule.ImageSizingInfoFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class ImageSizingInfoFrontend_AutoGen : BusinessLogic_v3.Frontend.ImageSizingInfoModule.ImageSizingInfoBaseFrontend

    {
		
        
        protected ImageSizingInfoFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo Data
        {
            get { return (PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfo)base.Data; }
            set { base.Data = value; }

        }

        public new static ImageSizingInfoFrontend CreateNewItem()
        {
            return (ImageSizingInfoFrontend)BusinessLogic_v3.Frontend.ImageSizingInfoModule.ImageSizingInfoBaseFrontend.CreateNewItem();
        }
        
        public new static ImageSizingInfoFrontend Get(BusinessLogic_v3.Modules.ImageSizingInfoModule.IImageSizingInfoBase data)
        {
            return (ImageSizingInfoFrontend)BusinessLogic_v3.Frontend.ImageSizingInfoModule.ImageSizingInfoBaseFrontend.Get(data);
        }
        public new static List<ImageSizingInfoFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.ImageSizingInfoModule.IImageSizingInfoBase> dataList)
        {
            return BusinessLogic_v3.Frontend.ImageSizingInfoModule.ImageSizingInfoBaseFrontend.GetList(dataList).Cast<ImageSizingInfoFrontend>().ToList();
        }


    }
}
