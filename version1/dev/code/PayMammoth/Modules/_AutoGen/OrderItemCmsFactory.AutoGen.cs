using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.OrderItemModule;
using PayMammoth.Cms.OrderItemModule;
using CS.WebComponentsGeneralV3.Cms.OrderItemModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class OrderItemCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.OrderItemModule.OrderItemBaseCmsFactory
    {
        public static new OrderItemBaseCmsFactory Instance 
        {
         	get { return (OrderItemBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.OrderItemModule.OrderItemBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override OrderItemBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = OrderItem.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            OrderItemCmsInfo cmsItem = new OrderItemCmsInfo(item);
            return cmsItem;
        }    
        
        public override OrderItemBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.OrderItemModule.OrderItemBase item)
        {
            return new OrderItemCmsInfo((OrderItem)item);
            
        }

    }
}
