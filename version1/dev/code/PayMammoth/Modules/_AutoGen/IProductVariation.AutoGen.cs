using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ProductVariationModule;
using PayMammoth.Modules.ProductVariationModule;

//IUserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
	
    public interface IProductVariationAutoGen : BusinessLogic_v3.Modules.ProductVariationModule.IProductVariationBase
    {




// [interface_userclass_properties]

		//IProperty_LinkedToObject
        new PayMammoth.Modules.ProductModule.IProduct Product {get; set; }

   
    

// [interface_userclassonly_properties]

   

                                     

// [interface_userclass_collections_nhibernate]

        //UserClass_CollectionOneToManyLeftSide
        
		new ICollectionManager<PayMammoth.Modules.OrderItemModule.IOrderItem> OrderItems { get; }

        //UserClass_CollectionOneToManyLeftSide
        
		new ICollectionManager<PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo> ProductVariation_CultureInfos { get; }

        //UserClass_CollectionOneToManyLeftSide
        
		new ICollectionManager<PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem> MediaItems { get; }




// [interface_userclassonly_collections]

 


    	
    	
      
      

    }
}
