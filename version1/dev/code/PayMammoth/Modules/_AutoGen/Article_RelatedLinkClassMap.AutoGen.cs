using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.Article_RelatedLinkModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class Article_RelatedLinkMap_AutoGen : Article_RelatedLinkMap_AutoGen_Z
    {
        public Article_RelatedLinkMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]

			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.ParentPage, ParentPageMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.RelatedPage, RelatedPageMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class Article_RelatedLinkMap_AutoGen_Z : BusinessLogic_v3.Modules.Article_RelatedLinkModule.Article_RelatedLinkBaseMap<Article_RelatedLinkImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
