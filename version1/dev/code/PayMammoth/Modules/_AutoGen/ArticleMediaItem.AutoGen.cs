using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.ArticleMediaItemModule;
using BusinessLogic_v3.Modules.ArticleMediaItemModule;


//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
    public abstract class ArticleMediaItem_AutoGen : ArticleMediaItemBase, PayMammoth.Modules._AutoGen.IArticleMediaItemAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.ArticleModule.Article ContentPage
        {
            get
            {
                return (PayMammoth.Modules.ArticleModule.Article)base.ContentPage;
            }
            set
            {
                base.ContentPage = value;
            }
        }

		PayMammoth.Modules.ArticleModule.IArticle IArticleMediaItemAutoGen.ContentPage
        {
            get
            {
            	return this.ContentPage;
                
            }
            set
            {
                this.ContentPage = (PayMammoth.Modules.ArticleModule.Article)value;
            }
        }



// [userclass_collections_nhibernate]


        
          
		public  static new ArticleMediaItemFactory Factory
        {
            get
            {
                return (ArticleMediaItemFactory)ArticleMediaItemBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<ArticleMediaItem, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
