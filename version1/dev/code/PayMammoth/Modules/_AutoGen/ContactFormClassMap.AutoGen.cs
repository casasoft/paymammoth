using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.ContactFormModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class ContactFormMap_AutoGen : ContactFormMap_AutoGen_Z
    {
        public ContactFormMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.DateSent, DateSentMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Name, NameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Company, CompanyMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Subject, SubjectMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Email, EmailMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Telephone, TelephoneMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Mobile, MobileMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Address1, Address1MappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Address2, Address2MappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Address3, Address3MappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ZIPCode, ZIPCodeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.State, StateMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CountryStr, CountryStrMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.City, CityMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Fax, FaxMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Website, WebsiteMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.IP, IPMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Enquiry, EnquiryMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Remark, RemarkMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Surname, SurnameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Username, UsernameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.FileFilename, FileFilenameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.HowDidYouFindUs, HowDidYouFindUsMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class ContactFormMap_AutoGen_Z : BusinessLogic_v3.Modules.ContactFormModule.ContactFormBaseMap<ContactFormImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
