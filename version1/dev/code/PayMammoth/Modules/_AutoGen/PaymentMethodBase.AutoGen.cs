using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using PayMammoth.Modules._AutoGen;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace PayMammoth.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "PaymentMethod")]
    public abstract class PaymentMethodBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IPaymentMethodBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
        

			var cultureInfo = __getCurrentCultureInfo(); //since this is a new item, it is imperative that the culture is preloaded
            
            
            base.initPropertiesForNewItem();
        }
    	
  
    	public PayMammoth.Modules._AutoGen.PaymentMethod_CultureInfoBase GetCultureInfoByLanguage(CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 lang)
        {
            return (PayMammoth.Modules._AutoGen.PaymentMethod_CultureInfoBase) __getCultureInfoByLanguage(lang);
        }

     	public static PaymentMethodBaseFactory Factory
        {
            get
            {
                return PaymentMethodBaseFactory.Instance; 
            }
        }    
        /*
        public static PaymentMethodBase CreateNewItem()
        {
            return (PaymentMethodBase)Factory.CreateNewItem();
        }

		public static PaymentMethodBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<PaymentMethodBase, PaymentMethodBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static PaymentMethodBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static PaymentMethodBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<PaymentMethodBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<PaymentMethodBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<PaymentMethodBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private PayMammoth.Connector.Enums.PaymentMethodSpecific _paymentmethod;
        
        public virtual PayMammoth.Connector.Enums.PaymentMethodSpecific PaymentMethod 
        {
        	get
        	{
        		return _paymentmethod;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentmethod,value);
        		_paymentmethod = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string Title
        {
            get
            {
                var cultureInfo = (PayMammoth.Modules._AutoGen.PaymentMethod_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.Title;
                
            }
            set
            {
            
                
                var cultureInfo = (PayMammoth.Modules._AutoGen.PaymentMethod_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__Title_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.Title,value);
                cultureInfo.Title = value;
            }
        }
        
        private string _title;
        public virtual string __Title_cultureBase
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		
        		_title = value;
        	}
        }
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string Description
        {
            get
            {
                var cultureInfo = (PayMammoth.Modules._AutoGen.PaymentMethod_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.Description;
                
            }
            set
            {
            
                
                var cultureInfo = (PayMammoth.Modules._AutoGen.PaymentMethod_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__Description_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.Description,value);
                cultureInfo.Description = value;
            }
        }
        
        private string _description;
        public virtual string __Description_cultureBase
        {
        	get
        	{
        		return _description;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_description,value);
        		
        		_description = value;
        	}
        }
		//BaseClass/Fields/Property_Normal
        private string _imagefilename;
        
        public virtual string ImageFilename 
        {
        	get
        	{
        		return _imagefilename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_imagefilename,value);
        		_imagefilename = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
