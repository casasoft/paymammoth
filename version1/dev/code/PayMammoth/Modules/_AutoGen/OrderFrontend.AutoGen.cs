using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.OrderModule;
using PayMammoth.Modules.OrderModule;
using BusinessLogic_v3.Modules.OrderModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.OrderModule.OrderFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.OrderModule.IOrder> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.OrderModule.OrderFrontend ToFrontend(this PayMammoth.Modules.OrderModule.IOrder item)
        {
        	return PayMammoth.Frontend.OrderModule.OrderFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class OrderFrontend_AutoGen : BusinessLogic_v3.Frontend.OrderModule.OrderBaseFrontend

    {
		
        
        protected OrderFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.OrderModule.IOrder Data
        {
            get { return (PayMammoth.Modules.OrderModule.IOrder)base.Data; }
            set { base.Data = value; }

        }

        public new static OrderFrontend CreateNewItem()
        {
            return (OrderFrontend)BusinessLogic_v3.Frontend.OrderModule.OrderBaseFrontend.CreateNewItem();
        }
        
        public new static OrderFrontend Get(BusinessLogic_v3.Modules.OrderModule.IOrderBase data)
        {
            return (OrderFrontend)BusinessLogic_v3.Frontend.OrderModule.OrderBaseFrontend.Get(data);
        }
        public new static List<OrderFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.OrderModule.IOrderBase> dataList)
        {
            return BusinessLogic_v3.Frontend.OrderModule.OrderBaseFrontend.GetList(dataList).Cast<OrderFrontend>().ToList();
        }


    }
}
