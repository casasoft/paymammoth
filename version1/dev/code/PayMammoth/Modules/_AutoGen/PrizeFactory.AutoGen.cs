using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.DbObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.PrizeModule;
using PayMammoth.Modules.CultureDetailsModule;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class PrizeFactoryAutoGen : PayMammoth.Modules._AutoGen.PrizeBaseFactory, 
    				PayMammoth.Modules._AutoGen.IPrizeFactoryAutoGen,
    				CS.General_v3.Classes.DbObjects.IBaseDbFactory<IPrize>
    
    {
    
     	
        public PrizeFactoryAutoGen ()
        {

			this.UsedInProject = true;
			_mainTypeCreated = typeof(PrizeImpl);
			PayMammoth.Cms.PrizeModule.PrizeCmsFactory._initialiseStaticInstance();
        }
        public static new PrizeFactory Instance
        {
            get
            {
                return (PrizeFactory)PrizeBaseFactory.Instance;
            }
      
      

        }
        
        
        public new IEnumerable<Prize> FindAll()
        {
            return base.FindAll().Cast<Prize>();
        }
        public new IEnumerable<Prize> FindAll(IQuery query)
        {
            
            return base.FindAll(query).Cast<Prize>();

        }
        public new IEnumerable<Prize> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<Prize>();
            
        }
        public new IEnumerable<Prize> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<Prize>();
            
            
        }
        public new IEnumerable<Prize> FindAll(ICriteria crit)
        {
            return base.FindAll(crit).Cast<Prize>();


        }
        public new IEnumerable<Prize> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<Prize>();
            
        }
        public new IEnumerable<Prize> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<Prize>();
        }
        
        public new Prize FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (Prize)base.FindItem(query);
        }
        public new Prize FindItem(IQueryOver query)
        {
            return (Prize)base.FindItem(query);
        }
        
        IPrize IPrizeFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new Prize CreateNewItem()
        {
            return (Prize)base.CreateNewItem();
        }

        public new Prize GetByPrimaryKey(long pKey)
        {
            return (Prize)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<Prize, Prize> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();

        	var q = session.QueryOver<Prize>();
            qParams.FillQueryOver(q);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public PayMammoth.Modules.PrizeModule.Prize GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.PrizeModule.Prize) base.GetByPrimaryKey(id, session);
        }
       public IEnumerable<PayMammoth.Modules.PrizeModule.Prize> FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PrizeModule.Prize>) base.FindAll(query, session);

       }
       public IEnumerable<PayMammoth.Modules.PrizeModule.Prize> FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PrizeModule.Prize>) base.FindAll(query, session);

       }
       public IEnumerable<PayMammoth.Modules.PrizeModule.Prize> FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PrizeModule.Prize>) base.FindAll(session);

       }
       public PayMammoth.Modules.PrizeModule.Prize FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.PrizeModule.Prize) base.FindItem(query, session);

       }
       public PayMammoth.Modules.PrizeModule.Prize FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.PrizeModule.Prize) base.FindItem(query, session);

       }

       public PayMammoth.Modules.PrizeModule.Prize FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.PrizeModule.Prize) base.FindItem(session);

       }
       public IEnumerable<PayMammoth.Modules.PrizeModule.Prize> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PrizeModule.Prize>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.PrizeModule.IPrize> Members
     
        PayMammoth.Modules.PrizeModule.IPrize IBaseDbFactory<PayMammoth.Modules.PrizeModule.IPrize>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.PrizeModule.IPrize IBaseDbFactory<PayMammoth.Modules.PrizeModule.IPrize>.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.PrizeModule.IPrize> IBaseDbFactory<PayMammoth.Modules.PrizeModule.IPrize>.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.PrizeModule.IPrize> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.PrizeModule.IPrize>.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.PrizeModule.IPrize> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.PrizeModule.IPrize>.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.PrizeModule.IPrize IBaseDbFactory<PayMammoth.Modules.PrizeModule.IPrize>.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.PrizeModule.IPrize CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.PrizeModule.IPrize>.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.PrizeModule.IPrize CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.PrizeModule.IPrize>.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.PrizeModule.IPrize> IBaseDbFactory<PayMammoth.Modules.PrizeModule.IPrize>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IPrize> IPrizeFactoryAutoGen.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IPrize> IPrizeFactoryAutoGen.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IPrize> IPrizeFactoryAutoGen.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(session);
           
       }

       IPrize IPrizeFactoryAutoGen.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(query, session);
           
       }

       IPrize IPrizeFactoryAutoGen.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(query, session);
           
       }

       IPrize IPrizeFactoryAutoGen.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IPrize> IPrizeFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IPrize IPrizeFactoryAutoGen.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
