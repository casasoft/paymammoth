using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.MemberResponsibleLimitsModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class MemberResponsibleLimitsMap_AutoGen : MemberResponsibleLimitsMap_AutoGen_Z
    {
        public MemberResponsibleLimitsMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]

			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.Member, MemberMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.StartDate, StartDateMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Value, ValueMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.FrequencyType, FrequencyTypeMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class MemberResponsibleLimitsMap_AutoGen_Z : BusinessLogic_v3.Modules.MemberResponsibleLimitsModule.MemberResponsibleLimitsBaseMap<MemberResponsibleLimitsImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
