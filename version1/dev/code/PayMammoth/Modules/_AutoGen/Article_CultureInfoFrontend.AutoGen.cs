using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.Article_CultureInfoModule;
using PayMammoth.Modules.Article_CultureInfoModule;
using BusinessLogic_v3.Modules.Article_CultureInfoModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.Article_CultureInfoModule.Article_CultureInfoFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.Article_CultureInfoModule.Article_CultureInfoFrontend ToFrontend(this PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo item)
        {
        	return PayMammoth.Frontend.Article_CultureInfoModule.Article_CultureInfoFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class Article_CultureInfoFrontend_AutoGen : BusinessLogic_v3.Frontend.Article_CultureInfoModule.Article_CultureInfoBaseFrontend

    {
		
        
        protected Article_CultureInfoFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo Data
        {
            get { return (PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo)base.Data; }
            set { base.Data = value; }

        }

        public new static Article_CultureInfoFrontend CreateNewItem()
        {
            return (Article_CultureInfoFrontend)BusinessLogic_v3.Frontend.Article_CultureInfoModule.Article_CultureInfoBaseFrontend.CreateNewItem();
        }
        
        public new static Article_CultureInfoFrontend Get(BusinessLogic_v3.Modules.Article_CultureInfoModule.IArticle_CultureInfoBase data)
        {
            return (Article_CultureInfoFrontend)BusinessLogic_v3.Frontend.Article_CultureInfoModule.Article_CultureInfoBaseFrontend.Get(data);
        }
        public new static List<Article_CultureInfoFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.Article_CultureInfoModule.IArticle_CultureInfoBase> dataList)
        {
            return BusinessLogic_v3.Frontend.Article_CultureInfoModule.Article_CultureInfoBaseFrontend.GetList(dataList).Cast<Article_CultureInfoFrontend>().ToList();
        }


    }
}
