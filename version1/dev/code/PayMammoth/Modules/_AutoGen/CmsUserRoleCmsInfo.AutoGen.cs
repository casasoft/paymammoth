using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.CmsUserRoleModule;
using CS.WebComponentsGeneralV3.Cms.CmsUserRoleModule;
using PayMammoth.Frontend.CmsUserRoleModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class CmsUserRoleCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.CmsUserRoleModule.CmsUserRoleBaseCmsInfo
    {

		public CmsUserRoleCmsInfo_AutoGen(BusinessLogic_v3.Modules.CmsUserRoleModule.CmsUserRoleBase item)
            : base(item)
        {

        }
        

        private CmsUserRoleFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = CmsUserRoleFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new CmsUserRoleFrontend FrontendItem
        {
            get { return (CmsUserRoleFrontend) base.FrontendItem; }
        }
        
        
		public new CmsUserRole DbItem
        {
            get
            {
                return (CmsUserRole)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.Title = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsUserRoleModule.CmsUserRole>.GetPropertyBySelector( item => item.Title),
        						isEditable: true, isVisible: true);

        this.Identifier = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CmsUserRoleModule.CmsUserRole>.GetPropertyBySelector( item => item.Identifier),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
