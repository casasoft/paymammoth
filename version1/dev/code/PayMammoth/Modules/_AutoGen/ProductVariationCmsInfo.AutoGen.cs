using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.ProductVariationModule;
using CS.WebComponentsGeneralV3.Cms.ProductVariationModule;
using PayMammoth.Frontend.ProductVariationModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class ProductVariationCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.ProductVariationModule.ProductVariationBaseCmsInfo
    {

		public ProductVariationCmsInfo_AutoGen(BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase item)
            : base(item)
        {

        }
        

        private ProductVariationFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = ProductVariationFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new ProductVariationFrontend FrontendItem
        {
            get { return (ProductVariationFrontend) base.FrontendItem; }
        }
        
        
		public new ProductVariation DbItem
        {
            get
            {
                return (ProductVariation)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.ReferenceCode = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductVariationModule.ProductVariation>.GetPropertyBySelector( item => item.ReferenceCode),
        						isEditable: true, isVisible: true);

        this.SupplierRefCode = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductVariationModule.ProductVariation>.GetPropertyBySelector( item => item.SupplierRefCode),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.Product = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductVariationModule.ProductVariation>.GetPropertyBySelector( item => item.Product),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.ProductVariations)));

        
        this.Barcode = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductVariationModule.ProductVariation>.GetPropertyBySelector( item => item.Barcode),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.Colour = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductVariationModule.ProductVariation>.GetPropertyBySelector( item => item.Colour),
                null));

        
//InitFieldUser_LinkedProperty
			this.Size = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductVariationModule.ProductVariation>.GetPropertyBySelector( item => item.Size),
                null));

        
        this.Quantity = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductVariationModule.ProductVariation>.GetPropertyBySelector( item => item.Quantity),
        						isEditable: true, isVisible: true);

        this.ReorderAmount = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductVariationModule.ProductVariation>.GetPropertyBySelector( item => item.ReorderAmount),
        						isEditable: true, isVisible: true);

        this.ImportReference = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductVariationModule.ProductVariation>.GetPropertyBySelector( item => item.ImportReference),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
