using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.AttachmentModule;
using CS.WebComponentsGeneralV3.Cms.AttachmentModule;
using PayMammoth.Frontend.AttachmentModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class AttachmentCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.AttachmentModule.AttachmentBaseCmsInfo
    {

		public AttachmentCmsInfo_AutoGen(BusinessLogic_v3.Modules.AttachmentModule.AttachmentBase item)
            : base(item)
        {

        }
        

        private AttachmentFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = AttachmentFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new AttachmentFrontend FrontendItem
        {
            get { return (AttachmentFrontend) base.FrontendItem; }
        }
        
        
		public new Attachment DbItem
        {
            get
            {
                return (Attachment)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.FileName = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.AttachmentModule.Attachment>.GetPropertyBySelector( item => item.FileName),
        						isEditable: false, isVisible: false);

//InitFieldUser_LinkedProperty
			this.TicketComment = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.AttachmentModule.Attachment>.GetPropertyBySelector( item => item.TicketComment),
                null));

        
        this.ContentType = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.AttachmentModule.Attachment>.GetPropertyBySelector( item => item.ContentType),
        						isEditable: true, isVisible: true);

        this.DateSubmitted = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.AttachmentModule.Attachment>.GetPropertyBySelector( item => item.DateSubmitted),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
