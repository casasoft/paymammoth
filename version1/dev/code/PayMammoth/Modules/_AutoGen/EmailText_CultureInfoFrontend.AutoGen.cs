using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.EmailText_CultureInfoModule;
using PayMammoth.Modules.EmailText_CultureInfoModule;
using BusinessLogic_v3.Modules.EmailText_CultureInfoModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.EmailText_CultureInfoModule.EmailText_CultureInfoFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.EmailText_CultureInfoModule.EmailText_CultureInfoFrontend ToFrontend(this PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo item)
        {
        	return PayMammoth.Frontend.EmailText_CultureInfoModule.EmailText_CultureInfoFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class EmailText_CultureInfoFrontend_AutoGen : BusinessLogic_v3.Frontend.EmailText_CultureInfoModule.EmailText_CultureInfoBaseFrontend

    {
		
        
        protected EmailText_CultureInfoFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo Data
        {
            get { return (PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo)base.Data; }
            set { base.Data = value; }

        }

        public new static EmailText_CultureInfoFrontend CreateNewItem()
        {
            return (EmailText_CultureInfoFrontend)BusinessLogic_v3.Frontend.EmailText_CultureInfoModule.EmailText_CultureInfoBaseFrontend.CreateNewItem();
        }
        
        public new static EmailText_CultureInfoFrontend Get(BusinessLogic_v3.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfoBase data)
        {
            return (EmailText_CultureInfoFrontend)BusinessLogic_v3.Frontend.EmailText_CultureInfoModule.EmailText_CultureInfoBaseFrontend.Get(data);
        }
        public new static List<EmailText_CultureInfoFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfoBase> dataList)
        {
            return BusinessLogic_v3.Frontend.EmailText_CultureInfoModule.EmailText_CultureInfoBaseFrontend.GetList(dataList).Cast<EmailText_CultureInfoFrontend>().ToList();
        }


    }
}
