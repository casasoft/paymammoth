using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.MemberModule;
using PayMammoth.Cms.MemberModule;
using CS.WebComponentsGeneralV3.Cms.MemberModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class MemberCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.MemberModule.MemberBaseCmsFactory
    {
        public static new MemberBaseCmsFactory Instance 
        {
         	get { return (MemberBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.MemberModule.MemberBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override MemberBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = Member.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            MemberCmsInfo cmsItem = new MemberCmsInfo(item);
            return cmsItem;
        }    
        
        public override MemberBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.MemberModule.MemberBase item)
        {
            return new MemberCmsInfo((Member)item);
            
        }

    }
}
