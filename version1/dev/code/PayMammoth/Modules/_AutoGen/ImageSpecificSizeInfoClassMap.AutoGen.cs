using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.ImageSpecificSizeInfoModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class ImageSpecificSizeInfoMap_AutoGen : ImageSpecificSizeInfoMap_AutoGen_Z
    {
        public ImageSpecificSizeInfoMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]

			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.ImageSizingInfo, ImageSizingInfoMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Width, WidthMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Height, HeightMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Title, TitleMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Identifier, IdentifierMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Description, DescriptionMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CropIdentifier, CropIdentifierMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ImageFormat, ImageFormatMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class ImageSpecificSizeInfoMap_AutoGen_Z : BusinessLogic_v3.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoBaseMap<ImageSpecificSizeInfoImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
