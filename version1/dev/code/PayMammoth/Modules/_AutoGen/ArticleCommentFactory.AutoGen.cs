using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.ArticleCommentModule;
using PayMammoth.Modules.ArticleCommentModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class ArticleCommentFactoryAutoGen : BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBaseFactory, 
    				PayMammoth.Modules._AutoGen.IArticleCommentFactoryAutoGen,
    				BusinessLogic_v3.Classes.DB.IBaseDbFactory<IArticleComment>
    
    {
    
     	public new ArticleComment ReloadItemFromDatabase(BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase item)
        {
         	return (ArticleComment)base.ReloadItemFromDatabase(item);
        }
     	
        public ArticleCommentFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(ArticleCommentImpl);
			PayMammoth.Cms.ArticleCommentModule.ArticleCommentCmsFactory._initialiseStaticInstance();
        }
        public static new ArticleCommentFactory Instance
        {
            get
            {
                return (ArticleCommentFactory)ArticleCommentBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<ArticleComment> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<ArticleComment>();
            
        }
        public new IEnumerable<ArticleComment> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<ArticleComment>();
            
            
        }
    
        public new IEnumerable<ArticleComment> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<ArticleComment>();
            
        }
        public new IEnumerable<ArticleComment> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<ArticleComment>();
        }
        
        public new ArticleComment FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (ArticleComment)base.FindItem(query);
        }
        public new ArticleComment FindItem(IQueryOver query)
        {
            return (ArticleComment)base.FindItem(query);
        }
        
        IArticleComment IArticleCommentFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new ArticleComment CreateNewItem()
        {
            return (ArticleComment)base.CreateNewItem();
        }

        public new ArticleComment GetByPrimaryKey(long pKey)
        {
            return (ArticleComment)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<ArticleComment, ArticleComment> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<ArticleComment>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.ArticleCommentModule.ArticleComment GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.ArticleCommentModule.ArticleComment) base.GetByPrimaryKey(id, session);
        }
       public new IEnumerable<PayMammoth.Modules.ArticleCommentModule.ArticleComment> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ArticleCommentModule.ArticleComment>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ArticleCommentModule.ArticleComment> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ArticleCommentModule.ArticleComment>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ArticleCommentModule.ArticleComment> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ArticleCommentModule.ArticleComment>) base.FindAll(session);

       }
       public new PayMammoth.Modules.ArticleCommentModule.ArticleComment FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ArticleCommentModule.ArticleComment) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.ArticleCommentModule.ArticleComment FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ArticleCommentModule.ArticleComment) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.ArticleCommentModule.ArticleComment FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ArticleCommentModule.ArticleComment) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.ArticleCommentModule.ArticleComment> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ArticleCommentModule.ArticleComment>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.ArticleCommentModule.IArticleComment> Members
     
        PayMammoth.Modules.ArticleCommentModule.IArticleComment IBaseDbFactory<PayMammoth.Modules.ArticleCommentModule.IArticleComment>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.ArticleCommentModule.IArticleComment IBaseDbFactory<PayMammoth.Modules.ArticleCommentModule.IArticleComment>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.ArticleCommentModule.IArticleComment> IBaseDbFactory<PayMammoth.Modules.ArticleCommentModule.IArticleComment>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.ArticleCommentModule.IArticleComment> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.ArticleCommentModule.IArticleComment>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.ArticleCommentModule.IArticleComment> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.ArticleCommentModule.IArticleComment>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.ArticleCommentModule.IArticleComment IBaseDbFactory<PayMammoth.Modules.ArticleCommentModule.IArticleComment>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ArticleCommentModule.IArticleComment BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.ArticleCommentModule.IArticleComment>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ArticleCommentModule.IArticleComment BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.ArticleCommentModule.IArticleComment>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.ArticleCommentModule.IArticleComment> IBaseDbFactory<PayMammoth.Modules.ArticleCommentModule.IArticleComment>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IArticleComment> IArticleCommentFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IArticleComment> IArticleCommentFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IArticleComment> IArticleCommentFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IArticleComment IArticleCommentFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IArticleComment IArticleCommentFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IArticleComment IArticleCommentFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IArticleComment> IArticleCommentFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IArticleComment IArticleCommentFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
