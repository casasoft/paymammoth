using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.Category_ParentChildLinkModule;
using CS.WebComponentsGeneralV3.Cms.Category_ParentChildLinkModule;
using PayMammoth.Frontend.Category_ParentChildLinkModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class Category_ParentChildLinkCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.Category_ParentChildLinkModule.Category_ParentChildLinkBaseCmsInfo
    {

		public Category_ParentChildLinkCmsInfo_AutoGen(BusinessLogic_v3.Modules.Category_ParentChildLinkModule.Category_ParentChildLinkBase item)
            : base(item)
        {

        }
        

        private Category_ParentChildLinkFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = Category_ParentChildLinkFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new Category_ParentChildLinkFrontend FrontendItem
        {
            get { return (Category_ParentChildLinkFrontend) base.FrontendItem; }
        }
        
        
		public new Category_ParentChildLink DbItem
        {
            get
            {
                return (Category_ParentChildLink)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

//InitFieldUser_LinkedProperty
			this.ParentCategory = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink>.GetPropertyBySelector( item => item.ParentCategory),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CategoryModule.Category>.GetPropertyBySelector( item => item.ChildCategoryLinks)));

        
//InitFieldUser_LinkedProperty
			this.ChildCategory = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.Category_ParentChildLinkModule.Category_ParentChildLink>.GetPropertyBySelector( item => item.ChildCategory),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CategoryModule.Category>.GetPropertyBySelector( item => item.ParentCategoryLinks)));

        
	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
