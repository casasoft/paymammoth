using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using CS.General_v3.Util;
using CS.General_v3.Extensions;

namespace PayMammoth.Modules._AutoGen
{

//IBaseClass-File
	
    public interface IPaymentRequestTransactionBase : PayMammoth.Modules._AutoGen.IPaymentRequestTransactionBaseAutoGen
    {
    
    }
}
