using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.PaymentRequestItemDetailModule;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Frontend.PaymentRequestItemDetailModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class PaymentRequestItemDetailCmsInfo_AutoGen : PayMammoth.Modules._AutoGen.PaymentRequestItemDetailBaseCmsInfo
    {

		public PaymentRequestItemDetailCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.PaymentRequestItemDetailBase item)
            : base(item)
        {

        }
        

        private PaymentRequestItemDetailFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = PaymentRequestItemDetailFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new PaymentRequestItemDetailFrontend FrontendItem
        {
            get { return (PaymentRequestItemDetailFrontend) base.FrontendItem; }
        }
        
        
		public new PaymentRequestItemDetail DbItem
        {
            get
            {
                return (PaymentRequestItemDetail)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.Title = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetail>.GetPropertyBySelector( item => item.Title),
        						isEditable: true, isVisible: true);

        this.Description = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetail>.GetPropertyBySelector( item => item.Description),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.PaymentRequest = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetail>.GetPropertyBySelector( item => item.PaymentRequest),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector( item => item.ItemDetails)));

        
	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
