using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.Product_CultureInfoModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class Product_CultureInfoMap_AutoGen : Product_CultureInfoMap_AutoGen_Z
    {
        public Product_CultureInfoMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]

			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.CultureInfo, CultureInfoMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.Product, ProductMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Title, TitleMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Description, DescriptionMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class Product_CultureInfoMap_AutoGen_Z : BusinessLogic_v3.Modules.Product_CultureInfoModule.Product_CultureInfoBaseMap<Product_CultureInfoImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
