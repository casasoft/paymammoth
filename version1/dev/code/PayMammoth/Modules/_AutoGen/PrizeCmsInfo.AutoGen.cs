using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using CS.General_v3.Extensions;
using PayMammoth.Modules.PrizeModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public class PrizeCmsInfo_AutoGen : PayMammoth.Modules._AutoGen.PrizeBaseCmsInfo
    {

		public PrizeCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.PrizeBase item)
            : base(item)
        {

        }
		public new Prize DbItem
        {
            get
            {
                return (Prize)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
