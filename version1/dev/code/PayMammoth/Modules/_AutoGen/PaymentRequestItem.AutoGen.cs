using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using CS.General_v3.Classes.DbObjects.Collections;
using CS.General_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.PaymentRequestItemModule;
using PayMammoth.Modules._AutoGen;

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaymentRequestItem_AutoGen : PaymentRequestItemBase, PayMammoth.Modules._AutoGen.IPaymentRequestItemAutoGen
	
      
      
    {
        


    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.PaymentRequestModule.PaymentRequest PaymentRequest
        {
            get
            {
                return (PayMammoth.Modules.PaymentRequestModule.PaymentRequest)base.PaymentRequest;
            }
            set
            {
                base.PaymentRequest = value;
            }
        }




// [userclass_collections_nhibernate]


        
          
		public  static new PaymentRequestItemFactory Factory
        {
            get
            {
                return (PaymentRequestItemFactory)PaymentRequestItemBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<PaymentRequestItem, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
