using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using PayMammoth.Modules._AutoGen;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace PayMammoth.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "PaymentRequestItemDetail")]
    public abstract class PaymentRequestItemDetailBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IPaymentRequestItemDetailBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	

     	public static PaymentRequestItemDetailBaseFactory Factory
        {
            get
            {
                return PaymentRequestItemDetailBaseFactory.Instance; 
            }
        }    
        /*
        public static PaymentRequestItemDetailBase CreateNewItem()
        {
            return (PaymentRequestItemDetailBase)Factory.CreateNewItem();
        }

		public static PaymentRequestItemDetailBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<PaymentRequestItemDetailBase, PaymentRequestItemDetailBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static PaymentRequestItemDetailBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static PaymentRequestItemDetailBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<PaymentRequestItemDetailBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<PaymentRequestItemDetailBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<PaymentRequestItemDetailBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _title;
        
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _description;
        
        public virtual string Description 
        {
        	get
        	{
        		return _description;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_description,value);
        		_description = value;
        		
        	}
        }
        
/*
		public virtual long? PaymentRequestID
		{
		 	get 
		 	{
		 		return (this.PaymentRequest != null ? (long?)this.PaymentRequest.ID : null);
		 	}
		}*/
		
        protected PayMammoth.Modules._AutoGen.PaymentRequestBase _paymentrequest;
        public virtual PayMammoth.Modules._AutoGen.PaymentRequestBase PaymentRequest 
        {
        	get
        	{
        		return _paymentrequest;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentrequest,value);
        		_paymentrequest = value;
        	}
        }
            
        PayMammoth.Modules._AutoGen.IPaymentRequestBase PayMammoth.Modules._AutoGen.IPaymentRequestItemDetailBaseAutoGen.PaymentRequest 
        {
            get
            {
            	return this.PaymentRequest;
            }
            set
            {
            	this.PaymentRequest = (PayMammoth.Modules._AutoGen.PaymentRequestBase) value;
            }
        }
            
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
