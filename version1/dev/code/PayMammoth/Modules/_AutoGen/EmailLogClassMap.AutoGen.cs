using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.EmailLogModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class EmailLogMap_AutoGen : EmailLogMap_AutoGen_Z
    {
        public EmailLogMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.DateTime, DateTimeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.FromEmail, FromEmailMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.FromName, FromNameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ToEmail, ToEmailMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ToName, ToNameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ReplyToEmail, ReplyToEmailMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ReplyToName, ReplyToNameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Success, SuccessMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Subject, SubjectMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PlainText, PlainTextMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.HtmlText, HtmlTextMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Attachements, AttachementsMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Resources, ResourcesMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Host, HostMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Port, PortMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.User, UserMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Pass, PassMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.UsesSecureConnection, UsesSecureConnectionMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ResultDetails, ResultDetailsMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class EmailLogMap_AutoGen_Z : BusinessLogic_v3.Modules.EmailLogModule.EmailLogBaseMap<EmailLogImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
