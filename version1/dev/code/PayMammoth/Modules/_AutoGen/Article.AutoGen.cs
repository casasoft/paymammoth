using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.ArticleModule;
using BusinessLogic_v3.Modules.ArticleModule;

//DbObjectBuilder v8

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{    

	
    public abstract class Article_AutoGen : ArticleBase, PayMammoth.Modules._AutoGen.IArticleAutoGen
	                 
      , IMultilingualItem
      
      
      
    {
        
            


		protected override bool hasLastEditedFields()
        {
            return true;
        }
        

    
    
    
		

#region Multilingual
        public PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo GetCurrentCultureInfo()
        {
            return (PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo)__getCurrentCultureInfo();
        }             
          
    	public new PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo GetCultureInfoByLanguage(CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 lang)
        {
            return (PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo)base.GetCultureInfoByLanguage(lang);
        }

#endregion
        
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.RoutingInfoModule.RoutingInfo LinkedRoute
        {
            get
            {
                return (PayMammoth.Modules.RoutingInfoModule.RoutingInfo)base.LinkedRoute;
            }
            set
            {
                base.LinkedRoute = value;
            }
        }

		PayMammoth.Modules.RoutingInfoModule.IRoutingInfo IArticleAutoGen.LinkedRoute
        {
            get
            {
            	return this.LinkedRoute;
                
            }
            set
            {
                this.LinkedRoute = (PayMammoth.Modules.RoutingInfoModule.RoutingInfo)value;
            }
        }



// [userclass_collections_nhibernate]

        //UserClass_CollectionOneToManyLeftSide
        
		new internal class __Article_CultureInfosCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.ArticleModule.Article, 
        	PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo,
        	PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.ArticleModule.Article, PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo>
        {
            private PayMammoth.Modules.ArticleModule.Article _item = null;
            public __Article_CultureInfosCollectionInfo(PayMammoth.Modules.ArticleModule.Article item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo>)_item.__collection__Article_CultureInfos; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.ArticleModule.Article, PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo>.SetLinkOnItem(PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo item, PayMammoth.Modules.ArticleModule.Article value)
            {
                item.Article = value;
            }
        }
        
        private __Article_CultureInfosCollectionInfo _Article_CultureInfos = null;
        internal new __Article_CultureInfosCollectionInfo Article_CultureInfos
        {
            get
            {
                if (_Article_CultureInfos == null)
                    _Article_CultureInfos = new __Article_CultureInfosCollectionInfo((PayMammoth.Modules.ArticleModule.Article)this);
                return _Article_CultureInfos;
            }
        }               
           
		ICollectionManager<PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo> IArticleAutoGen.Article_CultureInfos
        {
            get {  return this.Article_CultureInfos; }
        }
           
        //UserClass_CollectionOneToManyLeftSide
        
		new internal class __ChildArticleLinksCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.ArticleModule.Article, 
        	PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink,
        	PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.ArticleModule.Article, PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink>
        {
            private PayMammoth.Modules.ArticleModule.Article _item = null;
            public __ChildArticleLinksCollectionInfo(PayMammoth.Modules.ArticleModule.Article item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink>)_item.__collection__ChildArticleLinks; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.ArticleModule.Article, PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink>.SetLinkOnItem(PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink item, PayMammoth.Modules.ArticleModule.Article value)
            {
                item.Parent = value;
            }
        }
        
        private __ChildArticleLinksCollectionInfo _ChildArticleLinks = null;
        internal new __ChildArticleLinksCollectionInfo ChildArticleLinks
        {
            get
            {
                if (_ChildArticleLinks == null)
                    _ChildArticleLinks = new __ChildArticleLinksCollectionInfo((PayMammoth.Modules.ArticleModule.Article)this);
                return _ChildArticleLinks;
            }
        }               
           
		ICollectionManager<PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink> IArticleAutoGen.ChildArticleLinks
        {
            get {  return this.ChildArticleLinks; }
        }
           
        //UserClass_CollectionOneToManyLeftSide
        
		new internal class __ParentArticleLinksCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.ArticleModule.Article, 
        	PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink,
        	PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.ArticleModule.Article, PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink>
        {
            private PayMammoth.Modules.ArticleModule.Article _item = null;
            public __ParentArticleLinksCollectionInfo(PayMammoth.Modules.ArticleModule.Article item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink>)_item.__collection__ParentArticleLinks; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.ArticleModule.Article, PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink>.SetLinkOnItem(PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLink item, PayMammoth.Modules.ArticleModule.Article value)
            {
                item.Child = value;
            }
        }
        
        private __ParentArticleLinksCollectionInfo _ParentArticleLinks = null;
        internal new __ParentArticleLinksCollectionInfo ParentArticleLinks
        {
            get
            {
                if (_ParentArticleLinks == null)
                    _ParentArticleLinks = new __ParentArticleLinksCollectionInfo((PayMammoth.Modules.ArticleModule.Article)this);
                return _ParentArticleLinks;
            }
        }               
           
		ICollectionManager<PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLink> IArticleAutoGen.ParentArticleLinks
        {
            get {  return this.ParentArticleLinks; }
        }
           

        
          
		public  static new ArticleFactory Factory
        {
            get
            {
                return (ArticleFactory)ArticleBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<Article, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


        public override void __CheckItemCultureInfos(bool autoSaveInDB, IEnumerable<BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase> CultureInfos = null)
        {
        	BusinessLogic_v3.Classes.NHibernateClasses.Transactions.MyTransaction t = null;
            if (autoSaveInDB)
                t = beginTransaction();
            MultilingualChecker multilingualChecker = new MultilingualChecker();
            multilingualChecker.ItemToAdd+=new MultilingualChecker.ItemToAddHandler(multilingualChecker_ItemToAdd);
            multilingualChecker.ItemRemoved += new MultilingualChecker.ItemRemovedHandler(multilingualChecker_ItemRemoved);
            multilingualChecker.CheckItemCultureInfos(this, CultureInfos);
            if (autoSaveInDB)
            {
                this.Save();
                t.Commit();
                t.Dispose();
            }
        }

 		protected override IMultilingualContentInfo __createCultureInfoForCulture(long cultureID)
        {

            var cultureDetails = BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetByPrimaryKey(cultureID);
            IMultilingualContentInfo result = null;
            if (cultureDetails != null)
            {
                var itemCultureInfo = this.Article_CultureInfos.CreateNewItem();
                result = itemCultureInfo;

//                itemCultureInfo.Article = (Article)this;  //item link
            	itemCultureInfo.CultureInfo = (PayMammoth.Modules.CultureDetailsModule.CultureDetails)cultureDetails; //culture
	

// [userfactory_checkcultureinfos_multilingualfields]

                        itemCultureInfo.HtmlText = this.__HtmlText_cultureBase;

                        itemCultureInfo.HtmlText_Search = this.__HtmlText_Search_cultureBase;

                        itemCultureInfo.MetaKeywords = this.__MetaKeywords_cultureBase;

                        itemCultureInfo.MetaKeywords_Search = this.__MetaKeywords_Search_cultureBase;

                        itemCultureInfo.MetaDescription = this.__MetaDescription_cultureBase;

                        itemCultureInfo.MetaDescription_Search = this.__MetaDescription_Search_cultureBase;

                        itemCultureInfo.PageTitle = this.__PageTitle_cultureBase;

                        itemCultureInfo.PageTitle_Search = this.__PageTitle_Search_cultureBase;

                        itemCultureInfo.DialogWidth = this.__DialogWidth_cultureBase;

                        itemCultureInfo.DialogHeight = this.__DialogHeight_cultureBase;

                        itemCultureInfo.SubTitle = this.__SubTitle_cultureBase;

                        itemCultureInfo._ComputedURL = this.___ComputedURL_cultureBase;

                        itemCultureInfo._Computed_ParentArticleNames = this.___Computed_ParentArticleNames_cultureBase;


	
				itemCultureInfo.MarkAsNotTemporary();
//				this.Article_CultureInfos.Add((PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo) itemCultureInfo);


            }
            return result;
        }


        private void multilingualChecker_ItemToAdd(BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase cultureDetails)
        {
            var itemCultureInfo = PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo.Factory.CreateNewItem();

            itemCultureInfo.Article = (Article)this;  //item link
            itemCultureInfo.CultureInfo = (PayMammoth.Modules.CultureDetailsModule.CultureDetails)cultureDetails; //culture


// [userfactory_checkcultureinfos_multilingualfields]

                        itemCultureInfo.HtmlText = this.__HtmlText_cultureBase;

                        itemCultureInfo.HtmlText_Search = this.__HtmlText_Search_cultureBase;

                        itemCultureInfo.MetaKeywords = this.__MetaKeywords_cultureBase;

                        itemCultureInfo.MetaKeywords_Search = this.__MetaKeywords_Search_cultureBase;

                        itemCultureInfo.MetaDescription = this.__MetaDescription_cultureBase;

                        itemCultureInfo.MetaDescription_Search = this.__MetaDescription_Search_cultureBase;

                        itemCultureInfo.PageTitle = this.__PageTitle_cultureBase;

                        itemCultureInfo.PageTitle_Search = this.__PageTitle_Search_cultureBase;

                        itemCultureInfo.DialogWidth = this.__DialogWidth_cultureBase;

                        itemCultureInfo.DialogHeight = this.__DialogHeight_cultureBase;

                        itemCultureInfo.SubTitle = this.__SubTitle_cultureBase;

                        itemCultureInfo._ComputedURL = this.___ComputedURL_cultureBase;

                        itemCultureInfo._Computed_ParentArticleNames = this.___Computed_ParentArticleNames_cultureBase;



			itemCultureInfo.MarkAsNotTemporary();
			this.Article_CultureInfos.Add((PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo) itemCultureInfo);
            
            

        }

		private void multilingualChecker_ItemRemoved(IMultilingualContentInfo item)
        {   
        	this.Article_CultureInfos.Remove((PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo)item);
            
        }

        
        
         #region IMultilingualItem Members

        IEnumerable<IMultilingualContentInfo> IMultilingualItem.GetMultilingualContentInfos()
        {
            return this.Article_CultureInfos;
            
        }

        protected override IEnumerable<IMultilingualContentInfo> __getMultilingualContentInfos()
        {
            return this.Article_CultureInfos;
        }
        
		#endregion    

     
        
        


#endregion    
    

        
        

// [userclassonly_properties]

		//UserClass/Fields/Property_Normal
        private long _customprojonlyfield;
        
        public virtual long CustomProjOnlyField 
        {
        	get
        	{
        		return _customprojonlyfield;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_customprojonlyfield,value);
        		_customprojonlyfield = value;
        		
        	}
        }
        long PayMammoth.Modules._AutoGen.IArticleAutoGen.CustomProjOnlyField 
        {
            get
            {
                return this.CustomProjOnlyField ;
                
            }
            set
            {
                this.CustomProjOnlyField = (long)value;
            }
        }
        
		//UserClass/Fields/Property_Normal
        private long _customprojonlylinkedobject;
        
        public virtual long CustomProjOnlyLinkedObject 
        {
        	get
        	{
        		return _customprojonlylinkedobject;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_customprojonlylinkedobject,value);
        		_customprojonlylinkedobject = value;
        		
        	}
        }
        long PayMammoth.Modules._AutoGen.IArticleAutoGen.CustomProjOnlyLinkedObject 
        {
            get
            {
                return this.CustomProjOnlyLinkedObject ;
                
            }
            set
            {
                this.CustomProjOnlyLinkedObject = (long)value;
            }
        }
        



// [userclassonly_collections]

        
        
        
    }
    
}
