using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.EmailLogModule;
using PayMammoth.Modules.EmailLogModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IEmailLogFactoryAutoGen : BusinessLogic_v3.Modules.EmailLogModule.IEmailLogBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IEmailLog>
    
    {
	    new PayMammoth.Modules.EmailLogModule.IEmailLog CreateNewItem();
        new PayMammoth.Modules.EmailLogModule.IEmailLog GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null);
        new IEnumerable<PayMammoth.Modules.EmailLogModule.IEmailLog> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.EmailLogModule.IEmailLog> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.EmailLogModule.IEmailLog> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.EmailLogModule.IEmailLog FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.EmailLogModule.IEmailLog FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.EmailLogModule.IEmailLog FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.EmailLogModule.IEmailLog> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
