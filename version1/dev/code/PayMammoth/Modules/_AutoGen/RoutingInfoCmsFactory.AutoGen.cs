using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.RoutingInfoModule;
using PayMammoth.Cms.RoutingInfoModule;
using CS.WebComponentsGeneralV3.Cms.RoutingInfoModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class RoutingInfoCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.RoutingInfoModule.RoutingInfoBaseCmsFactory
    {
        public static new RoutingInfoBaseCmsFactory Instance 
        {
         	get { return (RoutingInfoBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.RoutingInfoModule.RoutingInfoBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override RoutingInfoBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	RoutingInfo item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = RoutingInfo.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        RoutingInfoCmsInfo cmsItem = new RoutingInfoCmsInfo(item);
            return cmsItem;
        }    
        
        public override RoutingInfoBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.RoutingInfoModule.RoutingInfoBase item)
        {
            return new RoutingInfoCmsInfo((RoutingInfo)item);
            
        }

    }
}
