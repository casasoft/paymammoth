using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace PayMammoth.Modules._AutoGen
{
	public abstract class MemberWallTicketBaseCmsFactory_AutoGen : CmsFactoryBase<MemberWallTicketBaseCmsInfo, MemberWallTicketBase>
    {
       
       public new static MemberWallTicketBaseCmsFactory Instance
	    {
	         get
	         {
                 return (MemberWallTicketBaseCmsFactory)CmsFactoryBase<MemberWallTicketBaseCmsInfo, MemberWallTicketBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = MemberWallTicketBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "MemberWallTicket.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "MemberWallTicket";

            this.QueryStringParamID = "MemberWallTicketId";

            cmsInfo.TitlePlural = "Member Wall Tickets";

            cmsInfo.TitleSingular =  "Member Wall Ticket";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public MemberWallTicketBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "MemberWallTicket/";


        }
       
    }

}
