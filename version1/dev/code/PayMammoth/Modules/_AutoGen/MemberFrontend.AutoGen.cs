using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.MemberModule;
using PayMammoth.Modules.MemberModule;
using BusinessLogic_v3.Modules.MemberModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.MemberModule.MemberFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.MemberModule.IMember> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.MemberModule.MemberFrontend ToFrontend(this PayMammoth.Modules.MemberModule.IMember item)
        {
        	return PayMammoth.Frontend.MemberModule.MemberFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class MemberFrontend_AutoGen : BusinessLogic_v3.Frontend.MemberModule.MemberBaseFrontend

    {
		
        
        protected MemberFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.MemberModule.IMember Data
        {
            get { return (PayMammoth.Modules.MemberModule.IMember)base.Data; }
            set { base.Data = value; }

        }

        public new static MemberFrontend CreateNewItem()
        {
            return (MemberFrontend)BusinessLogic_v3.Frontend.MemberModule.MemberBaseFrontend.CreateNewItem();
        }
        
        public new static MemberFrontend Get(BusinessLogic_v3.Modules.MemberModule.IMemberBase data)
        {
            return (MemberFrontend)BusinessLogic_v3.Frontend.MemberModule.MemberBaseFrontend.Get(data);
        }
        public new static List<MemberFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.MemberModule.IMemberBase> dataList)
        {
            return BusinessLogic_v3.Frontend.MemberModule.MemberBaseFrontend.GetList(dataList).Cast<MemberFrontend>().ToList();
        }


    }
}
