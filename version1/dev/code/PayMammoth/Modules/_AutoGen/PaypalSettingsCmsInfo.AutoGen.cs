using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using CS.General_v3.Extensions;
using PayMammoth.Modules.PaypalSettingsModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public class PaypalSettingsCmsInfo_AutoGen : PayMammoth.Modules._AutoGen.PaypalSettingsBaseCmsInfo
    {

		public PaypalSettingsCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.PaypalSettingsBase item)
            : base(item)
        {

        }
		public new PaypalSettings DbItem
        {
            get
            {
                return (PaypalSettings)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
