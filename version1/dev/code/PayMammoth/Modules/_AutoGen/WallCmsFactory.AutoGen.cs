using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using PayMammoth.Modules.WallModule;
using PayMammoth.Cms.WallModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class WallCmsFactory_AutoGen : PayMammoth.Modules._AutoGen.WallBaseCmsFactory
    {
        public static new WallBaseCmsFactory Instance 
        {
         	get { return (WallBaseCmsFactory)PayMammoth.Modules._AutoGen.WallBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override WallBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = Wall.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            WallCmsInfo cmsItem = new WallCmsInfo(item);
            return cmsItem;
        }    
        
        public override WallBaseCmsInfo CreateCmsItemInfo(PayMammoth.Modules._AutoGen.WallBase item)
        {
            return new WallCmsInfo((Wall)item);
            
        }

    }
}
