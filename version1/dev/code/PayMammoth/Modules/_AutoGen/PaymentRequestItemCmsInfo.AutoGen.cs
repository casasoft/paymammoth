using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using CS.General_v3.Extensions;
using PayMammoth.Modules.PaymentRequestItemModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public class PaymentRequestItemCmsInfo_AutoGen : PayMammoth.Modules._AutoGen.PaymentRequestItemBaseCmsInfo
    {

		public PaymentRequestItemCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.PaymentRequestItemBase item)
            : base(item)
        {

        }
		public new PaymentRequestItem DbItem
        {
            get
            {
                return (PaymentRequestItem)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
