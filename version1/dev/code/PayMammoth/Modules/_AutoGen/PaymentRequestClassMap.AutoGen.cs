using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.PaymentRequestModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class PaymentRequestMap_AutoGen : PaymentRequestMap_AutoGen_Z
    {
        public PaymentRequestMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.Identifier, IdentifierMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DateTime, DateTimeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.RequestIP, RequestIPMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ClientFirstName, ClientFirstNameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ClientMiddleName, ClientMiddleNameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ClientLastName, ClientLastNameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ClientAddress1, ClientAddress1MappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ClientAddress2, ClientAddress2MappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ClientAddress3, ClientAddress3MappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ClientLocality, ClientLocalityMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ClientCountry, ClientCountryMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ClientPostCode, ClientPostCodeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Description, DescriptionMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ClientEmail, ClientEmailMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.WebsiteAccount, WebsiteAccountMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.TaxAmount, TaxAmountMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.HandlingAmount, HandlingAmountMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ShippingAmount, ShippingAmountMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Reference, ReferenceMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Notes, NotesMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PriceExcTax, PriceExcTaxMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CurrencyCode, CurrencyCodeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.OrderLinkOnClientWebsite, OrderLinkOnClientWebsiteMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Title, TitleMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.CurrentTransaction, CurrentTransactionMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Paid, PaidMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaidOn, PaidOnMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaidByPaymentMethod, PaidByPaymentMethodMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ReturnUrlSuccess, ReturnUrlSuccessMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ReturnUrlFailure, ReturnUrlFailureMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.LanguageCode, LanguageCodeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.LanguageCountryCode, LanguageCountryCodeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.LanguageSuffix, LanguageSuffixMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ClientTelephone, ClientTelephoneMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ClientMobile, ClientMobileMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ClientTitle, ClientTitleMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ClientIp, ClientIpMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ClientReference, ClientReferenceMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentNotificationAcknowledged, PaymentNotificationAcknowledgedMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.RequiresManualIntervention, RequiresManualInterventionMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentNotificationAcknowledgedOn, PaymentNotificationAcknowledgedOnMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentNotificationRetryCount, PaymentNotificationRetryCountMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentNotificationEnabled, PaymentNotificationEnabledMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentNotificationNextRetryOn, PaymentNotificationNextRetryOnMappingInfo);



// [userclassmap_initcollections]

//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.ItemDetails, ItemDetailsMappingInfo, BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.Transactions, TransactionsMappingInfo, BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class PaymentRequestMap_AutoGen_Z : PayMammoth.Modules._AutoGen.PaymentRequestBaseMap<PaymentRequestImpl>
    {


// [userclassmap_properties]

//ClassMap_PropertyLinkedObject

        protected virtual void WebsiteAccountMappingInfo(ICollectionMapInfo mapInfo)
        {
        
            //mapInfo.CollectionType = CS.General_v3.Enums.NHIBERNATE_COLLECTION_TYPE.Set;
            
            mapInfo.CascadeType.Clear();
            
            mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
            mapInfo.Inverse = false;
            mapInfo.ColumnName = "WebsiteAccountId";   
            
            mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
           
        }
        
        

//ClassMap_PropertyLinkedObject

        protected virtual void CurrentTransactionMappingInfo(ICollectionMapInfo mapInfo)
        {
        
            //mapInfo.CollectionType = CS.General_v3.Enums.NHIBERNATE_COLLECTION_TYPE.Set;
            
            mapInfo.CascadeType.Clear();
            
            mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
            mapInfo.Inverse = false;
            mapInfo.ColumnName = "CurrentTransactionId";   
            
            mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
           
        }
        
        




// [userclassmap_collections]

//BaseClassMap_Collection_OneToMany

        protected virtual void ItemDetailsMappingInfo(ICollectionMapInfo mapInfo)
        {
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.SaveUpdate;
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.None;
                
           mapInfo.CascadeType.SetAll();     
           if (mapInfo.LazyMode == null)
           {
               if (true)
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
               else
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.NoLazy;
           }                
           if (!mapInfo.Inverse.HasValue) mapInfo.Inverse = true;
           if (string.IsNullOrEmpty(mapInfo.ColumnName)) mapInfo.ColumnName = "PaymentRequestID";
           mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
            
        }
        
        
//BaseClassMap_Collection_OneToMany

        protected virtual void TransactionsMappingInfo(ICollectionMapInfo mapInfo)
        {
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.SaveUpdate;
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.None;
                
           mapInfo.CascadeType.SetAll();     
           if (mapInfo.LazyMode == null)
           {
               if (true)
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
               else
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.NoLazy;
           }                
           if (!mapInfo.Inverse.HasValue) mapInfo.Inverse = true;
           if (string.IsNullOrEmpty(mapInfo.ColumnName)) mapInfo.ColumnName = "PaymentRequestID";
           mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
            
        }
        
        
            
        
        
    }
}
