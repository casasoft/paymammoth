using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.OrderModule;
using PayMammoth.Modules.OrderModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IOrderFactoryAutoGen : BusinessLogic_v3.Modules.OrderModule.IOrderBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IOrder>
    
    {
	    new PayMammoth.Modules.OrderModule.IOrder CreateNewItem();
        new PayMammoth.Modules.OrderModule.IOrder GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.OrderModule.IOrder> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.OrderModule.IOrder> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.OrderModule.IOrder> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.OrderModule.IOrder FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.OrderModule.IOrder FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.OrderModule.IOrder FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.OrderModule.IOrder> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
