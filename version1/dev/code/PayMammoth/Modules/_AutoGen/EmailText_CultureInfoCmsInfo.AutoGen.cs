using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.EmailText_CultureInfoModule;
using CS.WebComponentsGeneralV3.Cms.EmailText_CultureInfoModule;
using PayMammoth.Frontend.EmailText_CultureInfoModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class EmailText_CultureInfoCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.EmailText_CultureInfoModule.EmailText_CultureInfoBaseCmsInfo
    {

		public EmailText_CultureInfoCmsInfo_AutoGen(BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase item)
            : base(item)
        {

        }
        

        private EmailText_CultureInfoFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = EmailText_CultureInfoFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new EmailText_CultureInfoFrontend FrontendItem
        {
            get { return (EmailText_CultureInfoFrontend) base.FrontendItem; }
        }
        
        
		public new EmailText_CultureInfo DbItem
        {
            get
            {
                return (EmailText_CultureInfo)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

//InitFieldUser_LinkedProperty
			this.CultureInfo = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo>.GetPropertyBySelector( item => item.CultureInfo),
                null));

        
//InitFieldUser_LinkedProperty
			this.EmailText = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo>.GetPropertyBySelector( item => item.EmailText),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailTextModule.EmailText>.GetPropertyBySelector( item => item.EmailText_CultureInfos)));

        
        this.Subject = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo>.GetPropertyBySelector( item => item.Subject),
        						isEditable: true, isVisible: true);

        this.Subject_Search = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo>.GetPropertyBySelector( item => item.Subject_Search),
        						isEditable: false, isVisible: false);

        this.Body = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo>.GetPropertyBySelector( item => item.Body),
        						isEditable: true, isVisible: true);

        this.Body_Search = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo>.GetPropertyBySelector( item => item.Body_Search),
        						isEditable: false, isVisible: false);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
