using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.Article_CultureInfoModule;
using PayMammoth.Modules.Article_CultureInfoModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class Article_CultureInfoFactoryAutoGen : BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBaseFactory, 
    				PayMammoth.Modules._AutoGen.IArticle_CultureInfoFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<IArticle_CultureInfo>
    
    {
    
     	public new Article_CultureInfo ReloadItemFromDatabase(BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase item)
        {
         	return (Article_CultureInfo)base.ReloadItemFromDatabase(item);
        }
     	static Article_CultureInfoFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public Article_CultureInfoFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(Article_CultureInfoImpl);
			PayMammoth.Cms.Article_CultureInfoModule.Article_CultureInfoCmsFactory._initialiseStaticInstance();
        }
        public static new Article_CultureInfoFactory Instance
        {
            get
            {
                return (Article_CultureInfoFactory)Article_CultureInfoBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<Article_CultureInfo> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<Article_CultureInfo>();
            
        }
        public new IEnumerable<Article_CultureInfo> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<Article_CultureInfo>();
            
            
        }
    
        public new IEnumerable<Article_CultureInfo> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<Article_CultureInfo>();
            
        }
        public new IEnumerable<Article_CultureInfo> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<Article_CultureInfo>();
        }
        
        public new Article_CultureInfo FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (Article_CultureInfo)base.FindItem(query);
        }
        public new Article_CultureInfo FindItem(IQueryOver query)
        {
            return (Article_CultureInfo)base.FindItem(query);
        }
        
        IArticle_CultureInfo IArticle_CultureInfoFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new Article_CultureInfo CreateNewItem()
        {
            return (Article_CultureInfo)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<Article_CultureInfo, Article_CultureInfo> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<Article_CultureInfo>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo>) base.FindAll(session);

       }
       public new PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfo>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo> Members
     
        PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo IBaseDbFactory<PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo IBaseDbFactory<PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo> IBaseDbFactory<PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo IBaseDbFactory<PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo> IBaseDbFactory<PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IArticle_CultureInfo> IArticle_CultureInfoFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IArticle_CultureInfo> IArticle_CultureInfoFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IArticle_CultureInfo> IArticle_CultureInfoFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IArticle_CultureInfo IArticle_CultureInfoFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IArticle_CultureInfo IArticle_CultureInfoFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IArticle_CultureInfo IArticle_CultureInfoFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IArticle_CultureInfo> IArticle_CultureInfoFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IArticle_CultureInfo IArticle_CultureInfoFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
