using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.PaymentMethod_CultureInfoModule;
using PayMammoth.Modules.PaymentMethod_CultureInfoModule;
using PayMammoth.Modules._AutoGen;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfoFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfoFrontend ToFrontend(this PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo item)
        {
        	return PayMammoth.Frontend.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfoFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaymentMethod_CultureInfoFrontend_AutoGen : PayMammoth.Modules._AutoGen.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfoBaseFrontend

    {
		
        
        protected PaymentMethod_CultureInfoFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo Data
        {
            get { return (PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfo)base.Data; }
            set { base.Data = value; }

        }

        public new static PaymentMethod_CultureInfoFrontend CreateNewItem()
        {
            return (PaymentMethod_CultureInfoFrontend)PayMammoth.Modules._AutoGen.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfoBaseFrontend.CreateNewItem();
        }
        
        public new static PaymentMethod_CultureInfoFrontend Get(PayMammoth.Modules._AutoGen.IPaymentMethod_CultureInfoBase data)
        {
            return (PaymentMethod_CultureInfoFrontend)PayMammoth.Modules._AutoGen.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfoBaseFrontend.Get(data);
        }
        public new static List<PaymentMethod_CultureInfoFrontend> GetList(IEnumerable<PayMammoth.Modules._AutoGen.IPaymentMethod_CultureInfoBase> dataList)
        {
            return PayMammoth.Modules._AutoGen.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfoBaseFrontend.GetList(dataList).Cast<PaymentMethod_CultureInfoFrontend>().ToList();
        }


    }
}
