using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.EmailTextModule;
using BusinessLogic_v3.Modules.EmailTextModule;

//DbObjectBuilder v8

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{    

	
    public abstract class EmailText_AutoGen : EmailTextBase, PayMammoth.Modules._AutoGen.IEmailTextAutoGen
	                 
      , IMultilingualItem
      
      
      
    {
        
            

    
    
    
		

#region Multilingual
        public PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo GetCurrentCultureInfo()
        {
            return (PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo)__getCurrentCultureInfo();
        }             
          
    	public new PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo GetCultureInfoByLanguage(CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 lang)
        {
            return (PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo)base.GetCultureInfoByLanguage(lang);
        }

#endregion
        
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.EmailTextModule.EmailText Parent
        {
            get
            {
                return (PayMammoth.Modules.EmailTextModule.EmailText)base.Parent;
            }
            set
            {
                base.Parent = value;
            }
        }

		PayMammoth.Modules.EmailTextModule.IEmailText IEmailTextAutoGen.Parent
        {
            get
            {
            	return this.Parent;
                
            }
            set
            {
                this.Parent = (PayMammoth.Modules.EmailTextModule.EmailText)value;
            }
        }



// [userclass_collections_nhibernate]

        //UserClass_CollectionOneToManyLeftSide
        
		new internal class __ChildEmailsCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.EmailTextModule.EmailText, 
        	PayMammoth.Modules.EmailTextModule.EmailText,
        	PayMammoth.Modules.EmailTextModule.IEmailText>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.EmailTextModule.EmailText, PayMammoth.Modules.EmailTextModule.EmailText>
        {
            private PayMammoth.Modules.EmailTextModule.EmailText _item = null;
            public __ChildEmailsCollectionInfo(PayMammoth.Modules.EmailTextModule.EmailText item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.EmailTextModule.EmailText> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.EmailTextModule.EmailText>)_item.__collection__ChildEmails; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.EmailTextModule.EmailText, PayMammoth.Modules.EmailTextModule.EmailText>.SetLinkOnItem(PayMammoth.Modules.EmailTextModule.EmailText item, PayMammoth.Modules.EmailTextModule.EmailText value)
            {
                item.Parent = value;
            }
        }
        
        private __ChildEmailsCollectionInfo _ChildEmails = null;
        internal new __ChildEmailsCollectionInfo ChildEmails
        {
            get
            {
                if (_ChildEmails == null)
                    _ChildEmails = new __ChildEmailsCollectionInfo((PayMammoth.Modules.EmailTextModule.EmailText)this);
                return _ChildEmails;
            }
        }               
           
		ICollectionManager<PayMammoth.Modules.EmailTextModule.IEmailText> IEmailTextAutoGen.ChildEmails
        {
            get {  return this.ChildEmails; }
        }
           
        //UserClass_CollectionOneToManyLeftSide
        
		new internal class __EmailText_CultureInfosCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.EmailTextModule.EmailText, 
        	PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo,
        	PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.EmailTextModule.EmailText, PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo>
        {
            private PayMammoth.Modules.EmailTextModule.EmailText _item = null;
            public __EmailText_CultureInfosCollectionInfo(PayMammoth.Modules.EmailTextModule.EmailText item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo>)_item.__collection__EmailText_CultureInfos; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.EmailTextModule.EmailText, PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo>.SetLinkOnItem(PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo item, PayMammoth.Modules.EmailTextModule.EmailText value)
            {
                item.EmailText = value;
            }
        }
        
        private __EmailText_CultureInfosCollectionInfo _EmailText_CultureInfos = null;
        internal new __EmailText_CultureInfosCollectionInfo EmailText_CultureInfos
        {
            get
            {
                if (_EmailText_CultureInfos == null)
                    _EmailText_CultureInfos = new __EmailText_CultureInfosCollectionInfo((PayMammoth.Modules.EmailTextModule.EmailText)this);
                return _EmailText_CultureInfos;
            }
        }               
           
		ICollectionManager<PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfo> IEmailTextAutoGen.EmailText_CultureInfos
        {
            get {  return this.EmailText_CultureInfos; }
        }
           

        
          
		public  static new EmailTextFactory Factory
        {
            get
            {
                return (EmailTextFactory)EmailTextBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<EmailText, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


        public override void __CheckItemCultureInfos(bool autoSaveInDB, IEnumerable<BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase> CultureInfos = null)
        {
        	BusinessLogic_v3.Classes.NHibernateClasses.Transactions.MyTransaction t = null;
            if (autoSaveInDB)
                t = beginTransaction();
            MultilingualChecker multilingualChecker = new MultilingualChecker();
            multilingualChecker.ItemToAdd+=new MultilingualChecker.ItemToAddHandler(multilingualChecker_ItemToAdd);
            multilingualChecker.ItemRemoved += new MultilingualChecker.ItemRemovedHandler(multilingualChecker_ItemRemoved);
            multilingualChecker.CheckItemCultureInfos(this, CultureInfos);
            if (autoSaveInDB)
            {
                this.Save();
                t.Commit();
                t.Dispose();
            }
        }

 		protected override IMultilingualContentInfo __createCultureInfoForCulture(long cultureID)
        {

            var cultureDetails = BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetByPrimaryKey(cultureID);
            IMultilingualContentInfo result = null;
            if (cultureDetails != null)
            {
                var itemCultureInfo = this.EmailText_CultureInfos.CreateNewItem();
                result = itemCultureInfo;

//                itemCultureInfo.EmailText = (EmailText)this;  //item link
            	itemCultureInfo.CultureInfo = (PayMammoth.Modules.CultureDetailsModule.CultureDetails)cultureDetails; //culture
	

// [userfactory_checkcultureinfos_multilingualfields]

                        itemCultureInfo.Subject = this.__Subject_cultureBase;

                        itemCultureInfo.Subject_Search = this.__Subject_Search_cultureBase;

                        itemCultureInfo.Body = this.__Body_cultureBase;

                        itemCultureInfo.Body_Search = this.__Body_Search_cultureBase;


	
				itemCultureInfo.MarkAsNotTemporary();
//				this.EmailText_CultureInfos.Add((PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo) itemCultureInfo);


            }
            return result;
        }


        private void multilingualChecker_ItemToAdd(BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase cultureDetails)
        {
            var itemCultureInfo = PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo.Factory.CreateNewItem();

            itemCultureInfo.EmailText = (EmailText)this;  //item link
            itemCultureInfo.CultureInfo = (PayMammoth.Modules.CultureDetailsModule.CultureDetails)cultureDetails; //culture


// [userfactory_checkcultureinfos_multilingualfields]

                        itemCultureInfo.Subject = this.__Subject_cultureBase;

                        itemCultureInfo.Subject_Search = this.__Subject_Search_cultureBase;

                        itemCultureInfo.Body = this.__Body_cultureBase;

                        itemCultureInfo.Body_Search = this.__Body_Search_cultureBase;



			itemCultureInfo.MarkAsNotTemporary();
			this.EmailText_CultureInfos.Add((PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo) itemCultureInfo);
            
            

        }

		private void multilingualChecker_ItemRemoved(IMultilingualContentInfo item)
        {   
        	this.EmailText_CultureInfos.Remove((PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfo)item);
            
        }

        
        
         #region IMultilingualItem Members

        IEnumerable<IMultilingualContentInfo> IMultilingualItem.GetMultilingualContentInfos()
        {
            return this.EmailText_CultureInfos;
            
        }

        protected override IEnumerable<IMultilingualContentInfo> __getMultilingualContentInfos()
        {
            return this.EmailText_CultureInfos;
        }
        
		#endregion    

     
        
        


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
