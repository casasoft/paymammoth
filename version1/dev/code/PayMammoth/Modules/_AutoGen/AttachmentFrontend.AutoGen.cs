using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.AttachmentModule;
using PayMammoth.Modules.AttachmentModule;
using BusinessLogic_v3.Modules.AttachmentModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.AttachmentModule.AttachmentFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.AttachmentModule.IAttachment> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.AttachmentModule.AttachmentFrontend ToFrontend(this PayMammoth.Modules.AttachmentModule.IAttachment item)
        {
        	return PayMammoth.Frontend.AttachmentModule.AttachmentFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class AttachmentFrontend_AutoGen : BusinessLogic_v3.Frontend.AttachmentModule.AttachmentBaseFrontend

    {
		
        
        protected AttachmentFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.AttachmentModule.IAttachment Data
        {
            get { return (PayMammoth.Modules.AttachmentModule.IAttachment)base.Data; }
            set { base.Data = value; }

        }

        public new static AttachmentFrontend CreateNewItem()
        {
            return (AttachmentFrontend)BusinessLogic_v3.Frontend.AttachmentModule.AttachmentBaseFrontend.CreateNewItem();
        }
        
        public new static AttachmentFrontend Get(BusinessLogic_v3.Modules.AttachmentModule.IAttachmentBase data)
        {
            return (AttachmentFrontend)BusinessLogic_v3.Frontend.AttachmentModule.AttachmentBaseFrontend.Get(data);
        }
        public new static List<AttachmentFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.AttachmentModule.IAttachmentBase> dataList)
        {
            return BusinessLogic_v3.Frontend.AttachmentModule.AttachmentBaseFrontend.GetList(dataList).Cast<AttachmentFrontend>().ToList();
        }


    }
}
