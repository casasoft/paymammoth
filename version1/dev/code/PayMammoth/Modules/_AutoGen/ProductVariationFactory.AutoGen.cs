using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.ProductVariationModule;
using PayMammoth.Modules.ProductVariationModule;
using PayMammoth.Modules.CultureDetailsModule;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class ProductVariationFactoryAutoGen : BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBaseFactory, 
    				PayMammoth.Modules._AutoGen.IProductVariationFactoryAutoGen,
    				CS.General_v3.Classes.DbObjects.IBaseDbFactory<IProductVariation>
    
    {
    
     	public new ProductVariation ReloadItemFromDatabase(BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase item)
        {
         	return (ProductVariation)base.ReloadItemFromDatabase(item);
        }
     	
        public ProductVariationFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(ProductVariationImpl);
			PayMammoth.Cms.ProductVariationModule.ProductVariationCmsFactory._initialiseStaticInstance();
        }
        public static new ProductVariationFactory Instance
        {
            get
            {
                return (ProductVariationFactory)ProductVariationBaseFactory.Instance;
            }
      
      

        }
        
        /*
        public new IEnumerable<ProductVariation> FindAll()
        {
            return base.FindAll().Cast<ProductVariation>();
        }*/
    /*    public new IEnumerable<ProductVariation> FindAll(IQuery query)
        {
            
            return base.FindAll(query).Cast<ProductVariation>();

        }*/
        public new IEnumerable<ProductVariation> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<ProductVariation>();
            
        }
        public new IEnumerable<ProductVariation> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<ProductVariation>();
            
            
        }
     /*  public new IEnumerable<ProductVariation> FindAll(ICriteria crit)
        {
            return base.FindAll(crit).Cast<ProductVariation>();


        }*/
        public new IEnumerable<ProductVariation> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<ProductVariation>();
            
        }
        public new IEnumerable<ProductVariation> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<ProductVariation>();
        }
        
        public new ProductVariation FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (ProductVariation)base.FindItem(query);
        }
        public new ProductVariation FindItem(IQueryOver query)
        {
            return (ProductVariation)base.FindItem(query);
        }
        
        IProductVariation IProductVariationFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new ProductVariation CreateNewItem()
        {
            return (ProductVariation)base.CreateNewItem();
        }

        public new ProductVariation GetByPrimaryKey(long pKey)
        {
            return (ProductVariation)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<ProductVariation, ProductVariation> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();

        	var q = session.QueryOver<ProductVariation>();
            qParams.FillQueryOver(q);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

            var itemList = ProductVariation.Factory.FindAll();

            var CultureInfos = CultureDetails.Factory.FindAll();

            foreach (var item in itemList)
            {
            	item.CheckItemCultureInfos(autoSaveInDB: true, CultureInfos: CultureInfos);
            }

  

        }        


#endregion
        public new PayMammoth.Modules.ProductVariationModule.ProductVariation GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.ProductVariationModule.ProductVariation) base.GetByPrimaryKey(id, session);
        }
       public new IEnumerable<PayMammoth.Modules.ProductVariationModule.ProductVariation> FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ProductVariationModule.ProductVariation>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ProductVariationModule.ProductVariation> FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ProductVariationModule.ProductVariation>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.ProductVariationModule.ProductVariation> FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ProductVariationModule.ProductVariation>) base.FindAll(session);

       }
       public new PayMammoth.Modules.ProductVariationModule.ProductVariation FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ProductVariationModule.ProductVariation) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.ProductVariationModule.ProductVariation FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ProductVariationModule.ProductVariation) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.ProductVariationModule.ProductVariation FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.ProductVariationModule.ProductVariation) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.ProductVariationModule.ProductVariation> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.ProductVariationModule.ProductVariation>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.ProductVariationModule.IProductVariation> Members
     
        PayMammoth.Modules.ProductVariationModule.IProductVariation IBaseDbFactory<PayMammoth.Modules.ProductVariationModule.IProductVariation>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.ProductVariationModule.IProductVariation IBaseDbFactory<PayMammoth.Modules.ProductVariationModule.IProductVariation>.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.ProductVariationModule.IProductVariation> IBaseDbFactory<PayMammoth.Modules.ProductVariationModule.IProductVariation>.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.ProductVariationModule.IProductVariation> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.ProductVariationModule.IProductVariation>.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.ProductVariationModule.IProductVariation> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.ProductVariationModule.IProductVariation>.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.ProductVariationModule.IProductVariation IBaseDbFactory<PayMammoth.Modules.ProductVariationModule.IProductVariation>.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ProductVariationModule.IProductVariation CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.ProductVariationModule.IProductVariation>.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.ProductVariationModule.IProductVariation CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.ProductVariationModule.IProductVariation>.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.ProductVariationModule.IProductVariation> IBaseDbFactory<PayMammoth.Modules.ProductVariationModule.IProductVariation>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IProductVariation> IProductVariationFactoryAutoGen.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IProductVariation> IProductVariationFactoryAutoGen.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IProductVariation> IProductVariationFactoryAutoGen.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IProductVariation IProductVariationFactoryAutoGen.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IProductVariation IProductVariationFactoryAutoGen.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IProductVariation IProductVariationFactoryAutoGen.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IProductVariation> IProductVariationFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IProductVariation IProductVariationFactoryAutoGen.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
