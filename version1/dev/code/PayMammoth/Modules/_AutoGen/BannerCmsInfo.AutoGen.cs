using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.BannerModule;
using CS.WebComponentsGeneralV3.Cms.BannerModule;
using PayMammoth.Frontend.BannerModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class BannerCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.BannerModule.BannerBaseCmsInfo
    {

		public BannerCmsInfo_AutoGen(BusinessLogic_v3.Modules.BannerModule.BannerBase item)
            : base(item)
        {

        }
        

        private BannerFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = BannerFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new BannerFrontend FrontendItem
        {
            get { return (BannerFrontend) base.FrontendItem; }
        }
        
        
		public new Banner DbItem
        {
            get
            {
                return (Banner)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.Identifier = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.BannerModule.Banner>.GetPropertyBySelector( item => item.Identifier),
        						isEditable: true, isVisible: true);

        this.Title = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.BannerModule.Banner>.GetPropertyBySelector( item => item.Title),
        						isEditable: true, isVisible: true);

        this.MediaItemFilename = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.BannerModule.Banner>.GetPropertyBySelector( item => item.MediaItemFilename),
        						isEditable: false, isVisible: false);

        this.Link = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.BannerModule.Banner>.GetPropertyBySelector( item => item.Link),
        						isEditable: true, isVisible: true);

        this.DurationMS = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.BannerModule.Banner>.GetPropertyBySelector( item => item.DurationMS),
        						isEditable: true, isVisible: true);

        this.HrefTarget = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.BannerModule.Banner>.GetPropertyBySelector( item => item.HrefTarget),
        						isEditable: true, isVisible: true);

        this.SlideshowDelaySec = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.BannerModule.Banner>.GetPropertyBySelector( item => item.SlideshowDelaySec),
        						isEditable: true, isVisible: true);

        this.HTMLDescription = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.BannerModule.Banner>.GetPropertyBySelector( item => item.HTMLDescription),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
