using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.BrandModule;
using PayMammoth.Modules.BrandModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class BrandFactoryAutoGen : BusinessLogic_v3.Modules.BrandModule.BrandBaseFactory, 
    				PayMammoth.Modules._AutoGen.IBrandFactoryAutoGen,
    				BusinessLogic_v3.Classes.DB.IBaseDbFactory<IBrand>
    
    {
    
     	public new Brand ReloadItemFromDatabase(BusinessLogic_v3.Modules.BrandModule.BrandBase item)
        {
         	return (Brand)base.ReloadItemFromDatabase(item);
        }
     	
        public BrandFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(BrandImpl);
			PayMammoth.Cms.BrandModule.BrandCmsFactory._initialiseStaticInstance();
        }
        public static new BrandFactory Instance
        {
            get
            {
                return (BrandFactory)BrandBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<Brand> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<Brand>();
            
        }
        public new IEnumerable<Brand> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<Brand>();
            
            
        }
    
        public new IEnumerable<Brand> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<Brand>();
            
        }
        public new IEnumerable<Brand> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<Brand>();
        }
        
        public new Brand FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (Brand)base.FindItem(query);
        }
        public new Brand FindItem(IQueryOver query)
        {
            return (Brand)base.FindItem(query);
        }
        
        IBrand IBrandFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new Brand CreateNewItem()
        {
            return (Brand)base.CreateNewItem();
        }

        public new Brand GetByPrimaryKey(long pKey)
        {
            return (Brand)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<Brand, Brand> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<Brand>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

            var itemList = Brand.Factory.FindAll();

            var CultureInfos = CultureDetails.Factory.FindAll();

            foreach (var item in itemList)
            {
            	item.__CheckItemCultureInfos(autoSaveInDB: true, CultureInfos: CultureInfos);
            }

  

        }        


#endregion
        public new PayMammoth.Modules.BrandModule.Brand GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.BrandModule.Brand) base.GetByPrimaryKey(id, session);
        }
       public new IEnumerable<PayMammoth.Modules.BrandModule.Brand> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.BrandModule.Brand>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.BrandModule.Brand> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.BrandModule.Brand>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.BrandModule.Brand> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.BrandModule.Brand>) base.FindAll(session);

       }
       public new PayMammoth.Modules.BrandModule.Brand FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.BrandModule.Brand) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.BrandModule.Brand FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.BrandModule.Brand) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.BrandModule.Brand FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.BrandModule.Brand) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.BrandModule.Brand> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.BrandModule.Brand>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.BrandModule.IBrand> Members
     
        PayMammoth.Modules.BrandModule.IBrand IBaseDbFactory<PayMammoth.Modules.BrandModule.IBrand>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.BrandModule.IBrand IBaseDbFactory<PayMammoth.Modules.BrandModule.IBrand>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.BrandModule.IBrand> IBaseDbFactory<PayMammoth.Modules.BrandModule.IBrand>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.BrandModule.IBrand> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.BrandModule.IBrand>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.BrandModule.IBrand> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.BrandModule.IBrand>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.BrandModule.IBrand IBaseDbFactory<PayMammoth.Modules.BrandModule.IBrand>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.BrandModule.IBrand BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.BrandModule.IBrand>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.BrandModule.IBrand BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.BrandModule.IBrand>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.BrandModule.IBrand> IBaseDbFactory<PayMammoth.Modules.BrandModule.IBrand>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IBrand> IBrandFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IBrand> IBrandFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IBrand> IBrandFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IBrand IBrandFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IBrand IBrandFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IBrand IBrandFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IBrand> IBrandFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IBrand IBrandFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
