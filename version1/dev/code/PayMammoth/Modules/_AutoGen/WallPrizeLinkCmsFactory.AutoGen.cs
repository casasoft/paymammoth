using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using PayMammoth.Modules.WallPrizeLinkModule;
using PayMammoth.Cms.WallPrizeLinkModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class WallPrizeLinkCmsFactory_AutoGen : PayMammoth.Modules._AutoGen.WallPrizeLinkBaseCmsFactory
    {
        public static new WallPrizeLinkBaseCmsFactory Instance 
        {
         	get { return (WallPrizeLinkBaseCmsFactory)PayMammoth.Modules._AutoGen.WallPrizeLinkBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override WallPrizeLinkBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = WallPrizeLink.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            WallPrizeLinkCmsInfo cmsItem = new WallPrizeLinkCmsInfo(item);
            return cmsItem;
        }    
        
        public override WallPrizeLinkBaseCmsInfo CreateCmsItemInfo(PayMammoth.Modules._AutoGen.WallPrizeLinkBase item)
        {
            return new WallPrizeLinkCmsInfo((WallPrizeLink)item);
            
        }

    }
}
