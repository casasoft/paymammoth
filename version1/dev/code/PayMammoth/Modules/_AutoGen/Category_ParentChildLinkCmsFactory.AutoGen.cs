using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.Category_ParentChildLinkModule;
using PayMammoth.Cms.Category_ParentChildLinkModule;
using CS.WebComponentsGeneralV3.Cms.Category_ParentChildLinkModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class Category_ParentChildLinkCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.Category_ParentChildLinkModule.Category_ParentChildLinkBaseCmsFactory
    {
        public static new Category_ParentChildLinkBaseCmsFactory Instance 
        {
         	get { return (Category_ParentChildLinkBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.Category_ParentChildLinkModule.Category_ParentChildLinkBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override Category_ParentChildLinkBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = Category_ParentChildLink.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            Category_ParentChildLinkCmsInfo cmsItem = new Category_ParentChildLinkCmsInfo(item);
            return cmsItem;
        }    
        
        public override Category_ParentChildLinkBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.Category_ParentChildLinkModule.Category_ParentChildLinkBase item)
        {
            return new Category_ParentChildLinkCmsInfo((Category_ParentChildLink)item);
            
        }

    }
}
