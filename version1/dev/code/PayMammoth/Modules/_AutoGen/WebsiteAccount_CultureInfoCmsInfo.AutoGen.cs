using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.WebsiteAccount_CultureInfoModule;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Frontend.WebsiteAccount_CultureInfoModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class WebsiteAccount_CultureInfoCmsInfo_AutoGen : PayMammoth.Modules._AutoGen.WebsiteAccount_CultureInfoBaseCmsInfo
    {

		public WebsiteAccount_CultureInfoCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.WebsiteAccount_CultureInfoBase item)
            : base(item)
        {

        }
        

        private WebsiteAccount_CultureInfoFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = WebsiteAccount_CultureInfoFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new WebsiteAccount_CultureInfoFrontend FrontendItem
        {
            get { return (WebsiteAccount_CultureInfoFrontend) base.FrontendItem; }
        }
        
        
		public new WebsiteAccount_CultureInfo DbItem
        {
            get
            {
                return (WebsiteAccount_CultureInfo)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

//InitFieldUser_LinkedProperty
			this.CultureInfo = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo>.GetPropertyBySelector( item => item.CultureInfo),
                null));

        
//InitFieldUser_LinkedProperty
			this.WebsiteAccount = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo>.GetPropertyBySelector( item => item.WebsiteAccount),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.WebsiteAccount_CultureInfos)));

        
        this.PaymentGatewayRealexDescription = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo>.GetPropertyBySelector( item => item.PaymentGatewayRealexDescription),
        						isEditable: true, isVisible: true);

        this.PaymentGatewayApcoDescription = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo>.GetPropertyBySelector( item => item.PaymentGatewayApcoDescription),
        						isEditable: true, isVisible: true);

        this.PaymentGatewayTransactiumDescription = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo>.GetPropertyBySelector( item => item.PaymentGatewayTransactiumDescription),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
