using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.ArticleMediaItemModule;
using PayMammoth.Modules.ArticleMediaItemModule;
using BusinessLogic_v3.Modules.ArticleMediaItemModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.ArticleMediaItemModule.ArticleMediaItemFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.ArticleMediaItemModule.ArticleMediaItemFrontend ToFrontend(this PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem item)
        {
        	return PayMammoth.Frontend.ArticleMediaItemModule.ArticleMediaItemFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class ArticleMediaItemFrontend_AutoGen : BusinessLogic_v3.Frontend.ArticleMediaItemModule.ArticleMediaItemBaseFrontend

    {
		
        
        protected ArticleMediaItemFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem Data
        {
            get { return (PayMammoth.Modules.ArticleMediaItemModule.IArticleMediaItem)base.Data; }
            set { base.Data = value; }

        }

        public new static ArticleMediaItemFrontend CreateNewItem()
        {
            return (ArticleMediaItemFrontend)BusinessLogic_v3.Frontend.ArticleMediaItemModule.ArticleMediaItemBaseFrontend.CreateNewItem();
        }
        
        public new static ArticleMediaItemFrontend Get(BusinessLogic_v3.Modules.ArticleMediaItemModule.IArticleMediaItemBase data)
        {
            return (ArticleMediaItemFrontend)BusinessLogic_v3.Frontend.ArticleMediaItemModule.ArticleMediaItemBaseFrontend.Get(data);
        }
        public new static List<ArticleMediaItemFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.ArticleMediaItemModule.IArticleMediaItemBase> dataList)
        {
            return BusinessLogic_v3.Frontend.ArticleMediaItemModule.ArticleMediaItemBaseFrontend.GetList(dataList).Cast<ArticleMediaItemFrontend>().ToList();
        }


    }
}
