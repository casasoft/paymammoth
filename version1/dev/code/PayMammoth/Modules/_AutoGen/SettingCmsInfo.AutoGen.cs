using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.SettingModule;
using CS.WebComponentsGeneralV3.Cms.SettingModule;
using PayMammoth.Frontend.SettingModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class SettingCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.SettingModule.SettingBaseCmsInfo
    {

		public SettingCmsInfo_AutoGen(BusinessLogic_v3.Modules.SettingModule.SettingBase item)
            : base(item)
        {

        }
        

        private SettingFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = SettingFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new SettingFrontend FrontendItem
        {
            get { return (SettingFrontend) base.FrontendItem; }
        }
        
        
		public new Setting DbItem
        {
            get
            {
                return (Setting)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.Name = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.SettingModule.Setting>.GetPropertyBySelector( item => item.Name),
        						isEditable: true, isVisible: true);

        this.Value = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.SettingModule.Setting>.GetPropertyBySelector( item => item.Value),
        						isEditable: true, isVisible: true);

        this.VisibleInCMS_AccessRequired = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.SettingModule.Setting>.GetPropertyBySelector( item => item.VisibleInCMS_AccessRequired),
        						isEditable: true, isVisible: true);

        this.DataType = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.SettingModule.Setting>.GetPropertyBySelector( item => item.DataType),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.Parent = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.SettingModule.Setting>.GetPropertyBySelector( item => item.Parent),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.SettingModule.Setting>.GetPropertyBySelector( item => item.ChildSettings)));

        
        this.Identifier = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.SettingModule.Setting>.GetPropertyBySelector( item => item.Identifier),
        						isEditable: true, isVisible: true);

        this.Description = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.SettingModule.Setting>.GetPropertyBySelector( item => item.Description),
        						isEditable: true, isVisible: true);

        this.UsedInProject = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.SettingModule.Setting>.GetPropertyBySelector( item => item.UsedInProject),
        						isEditable: true, isVisible: true);

        this.LocalhostValue = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.SettingModule.Setting>.GetPropertyBySelector( item => item.LocalhostValue),
        						isEditable: true, isVisible: true);

        this.EnumType = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.SettingModule.Setting>.GetPropertyBySelector( item => item.EnumType),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
