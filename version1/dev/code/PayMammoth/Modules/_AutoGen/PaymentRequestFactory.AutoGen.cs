using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaymentRequestFactoryAutoGen : PayMammoth.Modules._AutoGen.PaymentRequestBaseFactory, 
    				PayMammoth.Modules._AutoGen.IPaymentRequestFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<IPaymentRequest>
    
    {
    
     	public new PaymentRequest ReloadItemFromDatabase(PayMammoth.Modules._AutoGen.PaymentRequestBase item)
        {
         	return (PaymentRequest)base.ReloadItemFromDatabase(item);
        }
     	static PaymentRequestFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public PaymentRequestFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(PaymentRequestImpl);
			PayMammoth.Cms.PaymentRequestModule.PaymentRequestCmsFactory._initialiseStaticInstance();
        }
        public static new PaymentRequestFactory Instance
        {
            get
            {
                return (PaymentRequestFactory)PaymentRequestBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<PaymentRequest> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<PaymentRequest>();
            
        }
        public new IEnumerable<PaymentRequest> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<PaymentRequest>();
            
            
        }
    
        public new IEnumerable<PaymentRequest> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<PaymentRequest>();
            
        }
        public new IEnumerable<PaymentRequest> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<PaymentRequest>();
        }
        
        public new PaymentRequest FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (PaymentRequest)base.FindItem(query);
        }
        public new PaymentRequest FindItem(IQueryOver query)
        {
            return (PaymentRequest)base.FindItem(query);
        }
        
        IPaymentRequest IPaymentRequestFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new PaymentRequest CreateNewItem()
        {
            return (PaymentRequest)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<PaymentRequest, PaymentRequest> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<PaymentRequest>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.PaymentRequestModule.PaymentRequest GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.PaymentRequestModule.PaymentRequest) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.PaymentRequestModule.PaymentRequest> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.PaymentRequestModule.PaymentRequest> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.PaymentRequestModule.PaymentRequest> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>) base.FindAll(session);

       }
       public new PayMammoth.Modules.PaymentRequestModule.PaymentRequest FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.PaymentRequestModule.PaymentRequest) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.PaymentRequestModule.PaymentRequest FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.PaymentRequestModule.PaymentRequest) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.PaymentRequestModule.PaymentRequest FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.PaymentRequestModule.PaymentRequest) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.PaymentRequestModule.PaymentRequest> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.PaymentRequestModule.IPaymentRequest> Members
     
        PayMammoth.Modules.PaymentRequestModule.IPaymentRequest IBaseDbFactory<PayMammoth.Modules.PaymentRequestModule.IPaymentRequest>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.PaymentRequestModule.IPaymentRequest IBaseDbFactory<PayMammoth.Modules.PaymentRequestModule.IPaymentRequest>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.PaymentRequestModule.IPaymentRequest> IBaseDbFactory<PayMammoth.Modules.PaymentRequestModule.IPaymentRequest>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.PaymentRequestModule.IPaymentRequest> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.PaymentRequestModule.IPaymentRequest>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.PaymentRequestModule.IPaymentRequest> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.PaymentRequestModule.IPaymentRequest>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.PaymentRequestModule.IPaymentRequest IBaseDbFactory<PayMammoth.Modules.PaymentRequestModule.IPaymentRequest>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.PaymentRequestModule.IPaymentRequest BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.PaymentRequestModule.IPaymentRequest>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.PaymentRequestModule.IPaymentRequest BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.PaymentRequestModule.IPaymentRequest>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.PaymentRequestModule.IPaymentRequest> IBaseDbFactory<PayMammoth.Modules.PaymentRequestModule.IPaymentRequest>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IPaymentRequest> IPaymentRequestFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IPaymentRequest> IPaymentRequestFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IPaymentRequest> IPaymentRequestFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IPaymentRequest IPaymentRequestFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IPaymentRequest IPaymentRequestFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IPaymentRequest IPaymentRequestFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IPaymentRequest> IPaymentRequestFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IPaymentRequest IPaymentRequestFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
