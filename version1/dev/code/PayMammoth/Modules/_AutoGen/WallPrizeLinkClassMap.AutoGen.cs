using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.WallPrizeLinkModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class WallPrizeLinkMap_AutoGen : WallPrizeLinkMap_AutoGen_Z
    {
        public WallPrizeLinkMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]

			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.Wall, WallMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.Prize, PrizeMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class WallPrizeLinkMap_AutoGen_Z : PayMammoth.Modules._AutoGen.WallPrizeLinkBaseMap<WallPrizeLinkImpl>
    {


// [userclassmap_properties]

//ClassMap_PropertyLinkedObject

        protected virtual void WallMappingInfo(ICollectionMapInfo mapInfo)
        {
        
            //mapInfo.CollectionType = CS.General_v3.Enums.NHIBERNATE_COLLECTION_TYPE.Set;
            
            mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.None;
            
            mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
            mapInfo.Inverse = false;
            mapInfo.ColumnName = "WallId";   
            
            mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
           
        }
        
        

//ClassMap_PropertyLinkedObject

        protected virtual void PrizeMappingInfo(ICollectionMapInfo mapInfo)
        {
        
            //mapInfo.CollectionType = CS.General_v3.Enums.NHIBERNATE_COLLECTION_TYPE.Set;
            
            mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.None;
            
            mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
            mapInfo.Inverse = false;
            mapInfo.ColumnName = "PrizeId";   
            
            mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
           
        }
        
        




// [userclassmap_collections]

            
        
        
    }
}
