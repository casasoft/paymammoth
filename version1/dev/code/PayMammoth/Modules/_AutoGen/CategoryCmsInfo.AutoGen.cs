using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.CategoryModule;
using CS.WebComponentsGeneralV3.Cms.CategoryModule;
using PayMammoth.Frontend.CategoryModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class CategoryCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.CategoryModule.CategoryBaseCmsInfo
    {

		public CategoryCmsInfo_AutoGen(BusinessLogic_v3.Modules.CategoryModule.CategoryBase item)
            : base(item)
        {

        }
        

        private CategoryFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = CategoryFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new CategoryFrontend FrontendItem
        {
            get { return (CategoryFrontend) base.FrontendItem; }
        }
        
        
		public new Category DbItem
        {
            get
            {
                return (Category)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.TitleSingular = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CategoryModule.Category>.GetPropertyBySelector( item => item.TitleSingular),
        						isEditable: true, isVisible: true);

        this.TitleSingular_Search = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CategoryModule.Category>.GetPropertyBySelector( item => item.TitleSingular_Search),
        						isEditable: false, isVisible: false);

        this.TitlePlural = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CategoryModule.Category>.GetPropertyBySelector( item => item.TitlePlural),
        						isEditable: true, isVisible: true);

        this.TitlePlural_Search = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CategoryModule.Category>.GetPropertyBySelector( item => item.TitlePlural_Search),
        						isEditable: false, isVisible: false);

        this.TitleSEO = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CategoryModule.Category>.GetPropertyBySelector( item => item.TitleSEO),
        						isEditable: true, isVisible: true);

        this.Description = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CategoryModule.Category>.GetPropertyBySelector( item => item.Description),
        						isEditable: true, isVisible: true);

        this.MetaKeywords = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CategoryModule.Category>.GetPropertyBySelector( item => item.MetaKeywords),
        						isEditable: true, isVisible: true);

        this.MetaDescription = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CategoryModule.Category>.GetPropertyBySelector( item => item.MetaDescription),
        						isEditable: true, isVisible: true);

        this.CanDelete = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CategoryModule.Category>.GetPropertyBySelector( item => item.CanDelete),
        						isEditable: true, isVisible: true);

        this.Identifier = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CategoryModule.Category>.GetPropertyBySelector( item => item.Identifier),
        						isEditable: true, isVisible: true);

        this.ComingSoon = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CategoryModule.Category>.GetPropertyBySelector( item => item.ComingSoon),
        						isEditable: true, isVisible: true);

        this.PriorityAll = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CategoryModule.Category>.GetPropertyBySelector( item => item.PriorityAll),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.Brand = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CategoryModule.Category>.GetPropertyBySelector( item => item.Brand),
                null));

        
        this.ImportReference = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CategoryModule.Category>.GetPropertyBySelector( item => item.ImportReference),
        						isEditable: true, isVisible: true);

        this.DontShowInNavigation = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.CategoryModule.Category>.GetPropertyBySelector( item => item.DontShowInNavigation),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
