using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.ContentText_CultureInfoModule;
using PayMammoth.Cms.ContentText_CultureInfoModule;
using CS.WebComponentsGeneralV3.Cms.ContentText_CultureInfoModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class ContentText_CultureInfoCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.ContentText_CultureInfoModule.ContentText_CultureInfoBaseCmsFactory
    {
        public static new ContentText_CultureInfoBaseCmsFactory Instance 
        {
         	get { return (ContentText_CultureInfoBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.ContentText_CultureInfoModule.ContentText_CultureInfoBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override ContentText_CultureInfoBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	ContentText_CultureInfo item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = ContentText_CultureInfo.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        ContentText_CultureInfoCmsInfo cmsItem = new ContentText_CultureInfoCmsInfo(item);
            return cmsItem;
        }    
        
        public override ContentText_CultureInfoBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoBase item)
        {
            return new ContentText_CultureInfoCmsInfo((ContentText_CultureInfo)item);
            
        }

    }
}
