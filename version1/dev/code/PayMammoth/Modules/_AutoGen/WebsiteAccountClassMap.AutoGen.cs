using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.WebsiteAccountModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class WebsiteAccountMap_AutoGen : WebsiteAccountMap_AutoGen_Z
    {
        public WebsiteAccountMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.WebsiteName, WebsiteNameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Code, CodeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.AllowedIPs, AllowedIPsMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ResponseUrl, ResponseUrlMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.SecretWord, SecretWordMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.IterationsToDeriveHash, IterationsToDeriveHashMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaypalSandboxUsername, PaypalSandboxUsernameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaypalSandboxPassword, PaypalSandboxPasswordMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaypalLiveUsername, PaypalLiveUsernameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaypalLivePassword, PaypalLivePasswordMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaypalEnabled, PaypalEnabledMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaypalUseLiveEnvironment, PaypalUseLiveEnvironmentMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaypalLiveSignature, PaypalLiveSignatureMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaypalSandboxSignature, PaypalSandboxSignatureMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaypalLiveMerchantId, PaypalLiveMerchantIdMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaypalSandboxMerchantId, PaypalSandboxMerchantIdMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaypalLiveMerchantEmail, PaypalLiveMerchantEmailMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaypalSandboxMerchantEmail, PaypalSandboxMerchantEmailMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ChequeEnabled, ChequeEnabledMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.MoneybookersEnabled, MoneybookersEnabledMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.NetellerEnabled, NetellerEnabledMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.EnableFakePayments, EnableFakePaymentsMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ChequeAddress1, ChequeAddress1MappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ChequeAddress2, ChequeAddress2MappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ChequeAddressCountry, ChequeAddressCountryMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ChequeAddressPostCode, ChequeAddressPostCodeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ChequeAddressLocality, ChequeAddressLocalityMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ChequePayableTo, ChequePayableToMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGatewayTransactiumLiveUsername, PaymentGatewayTransactiumLiveUsernameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGatewayTransactiumLivePassword, PaymentGatewayTransactiumLivePasswordMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGatewayTransactiumStagingUsername, PaymentGatewayTransactiumStagingUsernameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGatewayTransactiumStagingPassword, PaymentGatewayTransactiumStagingPasswordMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGatewayTransactiumUseLiveEnvironment, PaymentGatewayTransactiumUseLiveEnvironmentMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGatewayTransactiumProfileTag, PaymentGatewayTransactiumProfileTagMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGatewayTransactiumBankStatementText, PaymentGatewayTransactiumBankStatementTextMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGatewayTransactiumEnabled, PaymentGatewayTransactiumEnabledMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGatewayApcoEnabled, PaymentGatewayApcoEnabledMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGatewayApcoLiveProfileId, PaymentGatewayApcoLiveProfileIdMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGatewayApcoLiveSecretWord, PaymentGatewayApcoLiveSecretWordMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGatewayApcoBankStatementText, PaymentGatewayApcoBankStatementTextMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGatewayApcoStagingSecretWord, PaymentGatewayApcoStagingSecretWordMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGatewayApcoStagingProfileId, PaymentGatewayApcoStagingProfileIdMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGatewayApcoUseLiveEnvironment, PaymentGatewayApcoUseLiveEnvironmentMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CssFilename, CssFilenameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.LogoFilename, LogoFilenameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.NotifyClientsByEmailAboutPayment, NotifyClientsByEmailAboutPaymentMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.NotificationEmail, NotificationEmailMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ContactEmail, ContactEmailMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ContactName, ContactNameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGatewayRealexEnabled, PaymentGatewayRealexEnabledMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGatewayRealexUseLiveEnvironment, PaymentGatewayRealexUseLiveEnvironmentMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGatewayRealexMerchantId, PaymentGatewayRealexMerchantIdMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGatewayRealexLiveAccountName, PaymentGatewayRealexLiveAccountNameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGatewayRealexStagingAccountName, PaymentGatewayRealexStagingAccountNameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGatewayRealexSecretWord, PaymentGatewayRealexSecretWordMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DisableResponseUrlNotifications, DisableResponseUrlNotificationsMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.WaitUntilPaymentNotificationAcknowledgedBeforeRedirectToSuccess, WaitUntilPaymentNotificationAcknowledgedBeforeRedirectToSuccessMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGatewayTransactiumDescription, PaymentGatewayTransactiumDescriptionMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGatewayApcoDescription, PaymentGatewayApcoDescriptionMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGatewayRealexDescription, PaymentGatewayRealexDescriptionMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PaymentGatewayRealexStatementText, PaymentGatewayRealexStatementTextMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.FakePaymentKey, FakePaymentKeyMappingInfo);



// [userclassmap_initcollections]

//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.PaymentRequests, PaymentRequestsMappingInfo, BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.WebsiteAccount_CultureInfos, WebsiteAccount_CultureInfosMappingInfo, BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class WebsiteAccountMap_AutoGen_Z : PayMammoth.Modules._AutoGen.WebsiteAccountBaseMap<WebsiteAccountImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

//BaseClassMap_Collection_OneToMany

        protected virtual void PaymentRequestsMappingInfo(ICollectionMapInfo mapInfo)
        {
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.SaveUpdate;
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.None;
                
           mapInfo.CascadeType.SetAll();     
           if (mapInfo.LazyMode == null)
           {
               if (true)
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
               else
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.NoLazy;
           }                
           if (!mapInfo.Inverse.HasValue) mapInfo.Inverse = true;
           if (string.IsNullOrEmpty(mapInfo.ColumnName)) mapInfo.ColumnName = "WebsiteAccountID";
           mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
            
        }
        
        
//BaseClassMap_Collection_OneToMany

        protected virtual void WebsiteAccount_CultureInfosMappingInfo(ICollectionMapInfo mapInfo)
        {
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.SaveUpdate;
           //mapInfo.CascadeType = NHibernate.Mapping.ByCode.Cascade.None;
                
           mapInfo.CascadeType.SetAll();     
           if (mapInfo.LazyMode == null)
           {
               if (false)
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.Lazy;
               else
                   mapInfo.LazyMode = NHibernate.Mapping.ByCode.CollectionLazy.NoLazy;
           }                
           if (!mapInfo.Inverse.HasValue) mapInfo.Inverse = true;
           if (string.IsNullOrEmpty(mapInfo.ColumnName)) mapInfo.ColumnName = "WebsiteAccountID";
           mapInfo.OrderByValuesSql.Add(new OrderByValue("Priority", CS.General_v3.Enums.SORT_TYPE.Ascending));
            
        }
        
        
            
        
        
    }
}
