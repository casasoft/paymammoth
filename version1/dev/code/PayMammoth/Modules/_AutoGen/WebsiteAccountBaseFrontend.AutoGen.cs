using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Modules._AutoGen.WebsiteAccountModule;



namespace BusinessLogic_v3.Extensions
{
    public static partial class _AutoGen_BaseFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Modules._AutoGen.WebsiteAccountModule.WebsiteAccountBaseFrontend> ToFrontendBaseList(this IEnumerable<PayMammoth.Modules._AutoGen.IWebsiteAccountBase> list)
        {
            return list.ToList().ConvertAll(ToFrontendBase);
        }
        
        public static PayMammoth.Modules._AutoGen.WebsiteAccountModule.WebsiteAccountBaseFrontend ToFrontendBase(this PayMammoth.Modules._AutoGen.IWebsiteAccountBase item)
        {
        	return PayMammoth.Modules._AutoGen.WebsiteAccountModule.WebsiteAccountBaseFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class WebsiteAccountBaseFrontend_AutoGen : BusinessLogic_v3.Frontend._Base.BaseFrontend<WebsiteAccountBaseFrontend, PayMammoth.Modules._AutoGen.IWebsiteAccountBase>

    {
		
        
        protected WebsiteAccountBaseFrontend_AutoGen ()
            : base()
        {

        }



    }
}
