using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using PayMammoth.Modules._AutoGen;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace PayMammoth.Modules._AutoGen
{

//BaseCmsInfo-Class

    public abstract class PaymentMethod_CultureInfoBaseCmsInfo : PayMammoth.Modules._AutoGen.PaymentMethod_CultureInfoBaseCmsInfo_AutoGen
    {
    	public PaymentMethod_CultureInfoBaseCmsInfo(PaymentMethod_CultureInfoBase item)
            : base(item)
        {

        }
    
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
	}
}
