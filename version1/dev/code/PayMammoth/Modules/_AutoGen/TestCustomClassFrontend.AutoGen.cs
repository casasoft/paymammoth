using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.TestCustomClassModule;
using PayMammoth.Modules.TestCustomClassModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class TestCustomClassFrontend_AutoGen : PayMammoth.Modules._AutoGen.TestCustomClassModule.TestCustomClassBaseFrontend

    {
		
        
        protected TestCustomClassFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.TestCustomClassModule.ITestCustomClass Data
        {
            get { return (PayMammoth.Modules.TestCustomClassModule.ITestCustomClass)base.Data; }
            set { base.Data = value; }

        }

        public new static TestCustomClassFrontend Get(PayMammoth.Modules._AutoGen.ITestCustomClassBase data)
        {
            return (TestCustomClassFrontend)PayMammoth.Modules._AutoGen.TestCustomClassModule.TestCustomClassBaseFrontend.Get(data);
        }
        public new static List<TestCustomClassFrontend> GetList(IEnumerable<PayMammoth.Modules._AutoGen.ITestCustomClassBase> dataList)
        {
            return PayMammoth.Modules._AutoGen.TestCustomClassModule.TestCustomClassBaseFrontend.GetList(dataList).Cast<TestCustomClassFrontend>().ToList();
        }


    }
}
