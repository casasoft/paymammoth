using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;

//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
   
    public abstract class PaymentRequestItemDetailBaseFactoryAutoGen  : BusinessLogic_v3.Classes.DbObjects.Factories.BaseDbFactory<PaymentRequestItemDetailBase, IPaymentRequestItemDetailBase>, IPaymentRequestItemDetailBaseFactoryAutoGen
    
    {
    
        public PaymentRequestItemDetailBaseFactoryAutoGen()
        {
        	
            
        }
        
		static PaymentRequestItemDetailBaseFactoryAutoGen()
        {
            
        }
        
        
        public new static PaymentRequestItemDetailBaseFactory Instance
        {
            get
            {
                return CS.General_v3.Classes.Singletons.Singleton.GetInstanceForLastTypeInInheritanceChain<PaymentRequestItemDetailBaseFactory>(null);
            }
        }
        
		public IQueryOver<PaymentRequestItemDetailBase, PaymentRequestItemDetailBase> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
        	ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<PaymentRequestItemDetailBase>();
            qParams.FillQueryOver(q, this);
        	postGetQueryOver(q, qParams);
            return q;
            
        } 
        
        
    }
}
