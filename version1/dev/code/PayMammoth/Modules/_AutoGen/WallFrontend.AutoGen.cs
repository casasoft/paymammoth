using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.WallModule;
using PayMammoth.Modules.WallModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class WallFrontend_AutoGen : PayMammoth.Modules._AutoGen.WallModule.WallBaseFrontend

    {
		
        
        protected WallFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.WallModule.IWall Data
        {
            get { return (PayMammoth.Modules.WallModule.IWall)base.Data; }
            set { base.Data = value; }

        }

        public new static WallFrontend Get(PayMammoth.Modules._AutoGen.IWallBase data)
        {
            return (WallFrontend)PayMammoth.Modules._AutoGen.WallModule.WallBaseFrontend.Get(data);
        }
        public new static List<WallFrontend> GetList(IEnumerable<PayMammoth.Modules._AutoGen.IWallBase> dataList)
        {
            return PayMammoth.Modules._AutoGen.WallModule.WallBaseFrontend.GetList(dataList).Cast<WallFrontend>().ToList();
        }


    }
}
