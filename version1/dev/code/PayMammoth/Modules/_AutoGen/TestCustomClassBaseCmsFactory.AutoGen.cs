using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;          
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Factories;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules._AutoGen;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Parameters;


namespace PayMammoth.Modules._AutoGen
{
	public abstract class TestCustomClassBaseCmsFactory_AutoGen : CmsFactoryBase<TestCustomClassBaseCmsInfo, TestCustomClassBase>
    {
       
       public new static TestCustomClassBaseCmsFactory Instance
	    {
	         get
	         {
                 return (TestCustomClassBaseCmsFactory)CmsFactoryBase<TestCustomClassBaseCmsInfo, TestCustomClassBase>.Instance;
	         }

	    }

		protected override NHibernate.IQueryOver _getQueryForSearchResults(bool showDeleted)
        {
        	GetQueryParams qParams = new GetQueryParams(loadDelItems: showDeleted, orderByPriority: false);
            var q = TestCustomClassBaseFactory.Instance.GetQuery(qParams );
            return q;
        }
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
            
         //   this.editPageName = "TestCustomClass.aspx";

           // this.listingPageName = "default.aspx";

            cmsInfo.Name = "TestCustomClass";

            this.QueryStringParamID = "TestCustomClassId";

            cmsInfo.TitlePlural = "Test Custom Class";

            cmsInfo.TitleSingular =  "Test Custom Class";

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        public TestCustomClassBaseCmsFactory_AutoGen() : base()
        {
            this.baseFolder = CmsSystemBase.Instance.GetCmsRoot() + "TestCustomClass/";


        }
       
    }

}
