using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.MemberLoginInfoModule;
using PayMammoth.Modules.MemberLoginInfoModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class MemberLoginInfoFactoryAutoGen : BusinessLogic_v3.Modules.MemberLoginInfoModule.MemberLoginInfoBaseFactory, 
    				PayMammoth.Modules._AutoGen.IMemberLoginInfoFactoryAutoGen,
    				BusinessLogic_v3.Classes.DB.IBaseDbFactory<IMemberLoginInfo>
    
    {
    
     	public new MemberLoginInfo ReloadItemFromDatabase(BusinessLogic_v3.Modules.MemberLoginInfoModule.MemberLoginInfoBase item)
        {
         	return (MemberLoginInfo)base.ReloadItemFromDatabase(item);
        }
     	static MemberLoginInfoFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public MemberLoginInfoFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(MemberLoginInfoImpl);
			PayMammoth.Cms.MemberLoginInfoModule.MemberLoginInfoCmsFactory._initialiseStaticInstance();
        }
        public static new MemberLoginInfoFactory Instance
        {
            get
            {
                return (MemberLoginInfoFactory)MemberLoginInfoBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<MemberLoginInfo> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<MemberLoginInfo>();
            
        }
        public new IEnumerable<MemberLoginInfo> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<MemberLoginInfo>();
            
            
        }
    
        public new IEnumerable<MemberLoginInfo> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<MemberLoginInfo>();
            
        }
        public new IEnumerable<MemberLoginInfo> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<MemberLoginInfo>();
        }
        
        public new MemberLoginInfo FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (MemberLoginInfo)base.FindItem(query);
        }
        public new MemberLoginInfo FindItem(IQueryOver query)
        {
            return (MemberLoginInfo)base.FindItem(query);
        }
        
        IMemberLoginInfo IMemberLoginInfoFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new MemberLoginInfo CreateNewItem()
        {
            return (MemberLoginInfo)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<MemberLoginInfo, MemberLoginInfo> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<MemberLoginInfo>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.MemberLoginInfoModule.MemberLoginInfo GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.MemberLoginInfoModule.MemberLoginInfo) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.MemberLoginInfoModule.MemberLoginInfo> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.MemberLoginInfoModule.MemberLoginInfo>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.MemberLoginInfoModule.MemberLoginInfo> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.MemberLoginInfoModule.MemberLoginInfo>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.MemberLoginInfoModule.MemberLoginInfo> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.MemberLoginInfoModule.MemberLoginInfo>) base.FindAll(session);

       }
       public new PayMammoth.Modules.MemberLoginInfoModule.MemberLoginInfo FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.MemberLoginInfoModule.MemberLoginInfo) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.MemberLoginInfoModule.MemberLoginInfo FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.MemberLoginInfoModule.MemberLoginInfo) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.MemberLoginInfoModule.MemberLoginInfo FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.MemberLoginInfoModule.MemberLoginInfo) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.MemberLoginInfoModule.MemberLoginInfo> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.MemberLoginInfoModule.MemberLoginInfo>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo> Members
     
        PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo IBaseDbFactory<PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo IBaseDbFactory<PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo> IBaseDbFactory<PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo> BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo IBaseDbFactory<PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo BusinessLogic_v3.Classes.DB.IBaseDbFactory<PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo> IBaseDbFactory<PayMammoth.Modules.MemberLoginInfoModule.IMemberLoginInfo>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IMemberLoginInfo> IMemberLoginInfoFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IMemberLoginInfo> IMemberLoginInfoFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IMemberLoginInfo> IMemberLoginInfoFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IMemberLoginInfo IMemberLoginInfoFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IMemberLoginInfo IMemberLoginInfoFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IMemberLoginInfo IMemberLoginInfoFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IMemberLoginInfo> IMemberLoginInfoFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IMemberLoginInfo IMemberLoginInfoFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
