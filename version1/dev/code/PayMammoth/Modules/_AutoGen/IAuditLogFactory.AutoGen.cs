using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.AuditLogModule;
using PayMammoth.Modules.AuditLogModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IAuditLogFactoryAutoGen : BusinessLogic_v3.Modules.AuditLogModule.IAuditLogBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IAuditLog>
    
    {
	    new PayMammoth.Modules.AuditLogModule.IAuditLog CreateNewItem();
        new PayMammoth.Modules.AuditLogModule.IAuditLog GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null);
        new IEnumerable<PayMammoth.Modules.AuditLogModule.IAuditLog> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.AuditLogModule.IAuditLog> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.AuditLogModule.IAuditLog> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.AuditLogModule.IAuditLog FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.AuditLogModule.IAuditLog FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.AuditLogModule.IAuditLog FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.AuditLogModule.IAuditLog> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
