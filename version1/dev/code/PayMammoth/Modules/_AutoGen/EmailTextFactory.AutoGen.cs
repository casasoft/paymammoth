using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.EmailTextModule;
using PayMammoth.Modules.EmailTextModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class EmailTextFactoryAutoGen : BusinessLogic_v3.Modules.EmailTextModule.EmailTextBaseFactory, 
    				PayMammoth.Modules._AutoGen.IEmailTextFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<IEmailText>
    
    {
    
     	public new EmailText ReloadItemFromDatabase(BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase item)
        {
         	return (EmailText)base.ReloadItemFromDatabase(item);
        }
     	static EmailTextFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public EmailTextFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(EmailTextImpl);
			PayMammoth.Cms.EmailTextModule.EmailTextCmsFactory._initialiseStaticInstance();
        }
        public static new EmailTextFactory Instance
        {
            get
            {
                return (EmailTextFactory)EmailTextBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<EmailText> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<EmailText>();
            
        }
        public new IEnumerable<EmailText> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<EmailText>();
            
            
        }
    
        public new IEnumerable<EmailText> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<EmailText>();
            
        }
        public new IEnumerable<EmailText> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<EmailText>();
        }
        
        public new EmailText FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (EmailText)base.FindItem(query);
        }
        public new EmailText FindItem(IQueryOver query)
        {
            return (EmailText)base.FindItem(query);
        }
        
        IEmailText IEmailTextFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new EmailText CreateNewItem()
        {
            return (EmailText)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<EmailText, EmailText> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<EmailText>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

            var itemList = EmailText.Factory.FindAll();

            var CultureInfos = CultureDetails.Factory.FindAll();

            foreach (var item in itemList)
            {
            	item.__CheckItemCultureInfos(autoSaveInDB: true, CultureInfos: CultureInfos);
            }

  

        }        


#endregion
        public new PayMammoth.Modules.EmailTextModule.EmailText GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.EmailTextModule.EmailText) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.EmailTextModule.EmailText> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.EmailTextModule.EmailText>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.EmailTextModule.EmailText> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.EmailTextModule.EmailText>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.EmailTextModule.EmailText> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.EmailTextModule.EmailText>) base.FindAll(session);

       }
       public new PayMammoth.Modules.EmailTextModule.EmailText FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.EmailTextModule.EmailText) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.EmailTextModule.EmailText FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.EmailTextModule.EmailText) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.EmailTextModule.EmailText FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.EmailTextModule.EmailText) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.EmailTextModule.EmailText> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.EmailTextModule.EmailText>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.EmailTextModule.IEmailText> Members
     
        PayMammoth.Modules.EmailTextModule.IEmailText IBaseDbFactory<PayMammoth.Modules.EmailTextModule.IEmailText>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.EmailTextModule.IEmailText IBaseDbFactory<PayMammoth.Modules.EmailTextModule.IEmailText>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.EmailTextModule.IEmailText> IBaseDbFactory<PayMammoth.Modules.EmailTextModule.IEmailText>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.EmailTextModule.IEmailText> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.EmailTextModule.IEmailText>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.EmailTextModule.IEmailText> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.EmailTextModule.IEmailText>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.EmailTextModule.IEmailText IBaseDbFactory<PayMammoth.Modules.EmailTextModule.IEmailText>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.EmailTextModule.IEmailText BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.EmailTextModule.IEmailText>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.EmailTextModule.IEmailText BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.EmailTextModule.IEmailText>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.EmailTextModule.IEmailText> IBaseDbFactory<PayMammoth.Modules.EmailTextModule.IEmailText>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IEmailText> IEmailTextFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IEmailText> IEmailTextFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IEmailText> IEmailTextFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IEmailText IEmailTextFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IEmailText IEmailTextFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IEmailText IEmailTextFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IEmailText> IEmailTextFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IEmailText IEmailTextFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
