using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using PayMammoth.Classes.Application;
using BusinessLogic_v3.Classes.NHibernateClasses;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class AppInstance_AutoGen : BusinessLogic_v3.Classes.Application.AppInstance
    {

        public new static AppInstance Instance
        {
            get
            {
                if (BusinessLogic_v3.Classes.Application.AppInstance.Instance == null)
                {
                    BusinessLogic_v3.Classes.Application.AppInstance.Instance = CS.General_v3.Classes.Singletons.Singleton.GetInstance<Classes.Application.AppInstance>();
                }
                return (AppInstance)BusinessLogic_v3.Classes.Application.AppInstance.Instance;
            }
        }
        public AppInstance_AutoGen()
        {
            this.AddProjectAssembly(Assembly.GetAssembly(typeof(AppInstance_AutoGen)));
            this.AddProjectAssembly(Assembly.GetAssembly(typeof(CS.WebComponentsGeneralV3.Code.Classes.Contact.ContactDetailsData)));
            
        }
        protected override void onPostApplicationStart()
        {
            
            base.onPostApplicationStart();
            CmsSystem.Instance.OnApplicationStart();
            CmsSystem.Instance.FinishedInitialisingFactories();
        }
       protected override void initFactories()
        {
            

// [appinstance_initfactories]

                PayMammoth.Modules.ArticleModule.ArticleFactory._initialiseStaticInstance();

                PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfoFactory._initialiseStaticInstance();

                PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLinkFactory._initialiseStaticInstance();

                PayMammoth.Modules.AuditLogModule.AuditLogFactory._initialiseStaticInstance();

                PayMammoth.Modules.CmsUserModule.CmsUserFactory._initialiseStaticInstance();

                PayMammoth.Modules.CmsUserRoleModule.CmsUserRoleFactory._initialiseStaticInstance();

                PayMammoth.Modules.ContactFormModule.ContactFormFactory._initialiseStaticInstance();

                PayMammoth.Modules.ContentTextModule.ContentTextFactory._initialiseStaticInstance();

                PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoFactory._initialiseStaticInstance();

                PayMammoth.Modules.CultureDetailsModule.CultureDetailsFactory._initialiseStaticInstance();

                PayMammoth.Modules.CurrencyModule.CurrencyFactory._initialiseStaticInstance();

                PayMammoth.Modules.EmailLogModule.EmailLogFactory._initialiseStaticInstance();

                PayMammoth.Modules.EmailTextModule.EmailTextFactory._initialiseStaticInstance();

                PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoFactory._initialiseStaticInstance();

                PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfoFactory._initialiseStaticInstance();

                PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoFactory._initialiseStaticInstance();

                PayMammoth.Modules.PaymentMethodModule.PaymentMethodFactory._initialiseStaticInstance();

                PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfoFactory._initialiseStaticInstance();

                PayMammoth.Modules.PaymentRequestModule.PaymentRequestFactory._initialiseStaticInstance();

                PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetailFactory._initialiseStaticInstance();

                PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransactionFactory._initialiseStaticInstance();

                PayMammoth.Modules.RoutingInfoModule.RoutingInfoFactory._initialiseStaticInstance();

                PayMammoth.Modules.SettingModule.SettingFactory._initialiseStaticInstance();

                PayMammoth.Modules.WebsiteAccountModule.WebsiteAccountFactory._initialiseStaticInstance();

                PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfoFactory._initialiseStaticInstance();

        
            base.initFactories();
        }
        
        protected override void initNHibernateMappings()
        { 
        	

// [appinstance_initmappings]

                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.ArticleModule.ArticleMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.Article_CultureInfoModule.Article_CultureInfoMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.Article_ParentChildLinkModule.Article_ParentChildLinkMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.AuditLogModule.AuditLogMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.CmsUserModule.CmsUserMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.CmsUserRoleModule.CmsUserRoleMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.ContactFormModule.ContactFormMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.ContentTextModule.ContentTextMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.CultureDetailsModule.CultureDetailsMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.CurrencyModule.CurrencyMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.EmailLogModule.EmailLogMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.EmailTextModule.EmailTextMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.ImageSizingInfoModule.ImageSizingInfoMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.ImageSpecificSizeInfoModule.ImageSpecificSizeInfoMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.PaymentMethodModule.PaymentMethodMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfoMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.PaymentRequestModule.PaymentRequestMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetailMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransactionMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.RoutingInfoModule.RoutingInfoMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.SettingModule.SettingMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccountMap>();
                NHClasses.NhManager.AddMappingFor<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfoMap>();
        
				
			base.initNHibernateMappings();
        }

    }
}
