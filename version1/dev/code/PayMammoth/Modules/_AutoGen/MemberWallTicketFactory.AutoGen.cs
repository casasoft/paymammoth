using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.DbObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.MemberWallTicketModule;
using PayMammoth.Modules.CultureDetailsModule;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class MemberWallTicketFactoryAutoGen : PayMammoth.Modules._AutoGen.MemberWallTicketBaseFactory, 
    				PayMammoth.Modules._AutoGen.IMemberWallTicketFactoryAutoGen,
    				CS.General_v3.Classes.DbObjects.IBaseDbFactory<IMemberWallTicket>
    
    {
    
     	
        public MemberWallTicketFactoryAutoGen ()
        {

			this.UsedInProject = true;
			_mainTypeCreated = typeof(MemberWallTicketImpl);
			PayMammoth.Cms.MemberWallTicketModule.MemberWallTicketCmsFactory._initialiseStaticInstance();
        }
        public static new MemberWallTicketFactory Instance
        {
            get
            {
                return (MemberWallTicketFactory)MemberWallTicketBaseFactory.Instance;
            }
      
      

        }
        
        
        public new IEnumerable<MemberWallTicket> FindAll()
        {
            return base.FindAll().Cast<MemberWallTicket>();
        }
        public new IEnumerable<MemberWallTicket> FindAll(IQuery query)
        {
            
            return base.FindAll(query).Cast<MemberWallTicket>();

        }
        public new IEnumerable<MemberWallTicket> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<MemberWallTicket>();
            
        }
        public new IEnumerable<MemberWallTicket> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<MemberWallTicket>();
            
            
        }
        public new IEnumerable<MemberWallTicket> FindAll(ICriteria crit)
        {
            return base.FindAll(crit).Cast<MemberWallTicket>();


        }
        public new IEnumerable<MemberWallTicket> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<MemberWallTicket>();
            
        }
        public new IEnumerable<MemberWallTicket> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<MemberWallTicket>();
        }
        
        public new MemberWallTicket FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (MemberWallTicket)base.FindItem(query);
        }
        public new MemberWallTicket FindItem(IQueryOver query)
        {
            return (MemberWallTicket)base.FindItem(query);
        }
        
        IMemberWallTicket IMemberWallTicketFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new MemberWallTicket CreateNewItem()
        {
            return (MemberWallTicket)base.CreateNewItem();
        }

        public new MemberWallTicket GetByPrimaryKey(long pKey)
        {
            return (MemberWallTicket)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<MemberWallTicket, MemberWallTicket> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();

        	var q = session.QueryOver<MemberWallTicket>();
            qParams.FillQueryOver(q);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public PayMammoth.Modules.MemberWallTicketModule.MemberWallTicket GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.MemberWallTicketModule.MemberWallTicket) base.GetByPrimaryKey(id, session);
        }
       public IEnumerable<PayMammoth.Modules.MemberWallTicketModule.MemberWallTicket> FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.MemberWallTicketModule.MemberWallTicket>) base.FindAll(query, session);

       }
       public IEnumerable<PayMammoth.Modules.MemberWallTicketModule.MemberWallTicket> FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.MemberWallTicketModule.MemberWallTicket>) base.FindAll(query, session);

       }
       public IEnumerable<PayMammoth.Modules.MemberWallTicketModule.MemberWallTicket> FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.MemberWallTicketModule.MemberWallTicket>) base.FindAll(session);

       }
       public PayMammoth.Modules.MemberWallTicketModule.MemberWallTicket FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.MemberWallTicketModule.MemberWallTicket) base.FindItem(query, session);

       }
       public PayMammoth.Modules.MemberWallTicketModule.MemberWallTicket FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.MemberWallTicketModule.MemberWallTicket) base.FindItem(query, session);

       }

       public PayMammoth.Modules.MemberWallTicketModule.MemberWallTicket FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.MemberWallTicketModule.MemberWallTicket) base.FindItem(session);

       }
       public IEnumerable<PayMammoth.Modules.MemberWallTicketModule.MemberWallTicket> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.MemberWallTicketModule.MemberWallTicket>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket> Members
     
        PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket IBaseDbFactory<PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket IBaseDbFactory<PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket>.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket> IBaseDbFactory<PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket>.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket>.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket>.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket IBaseDbFactory<PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket>.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket>.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket>.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket> IBaseDbFactory<PayMammoth.Modules.MemberWallTicketModule.IMemberWallTicket>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IMemberWallTicket> IMemberWallTicketFactoryAutoGen.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IMemberWallTicket> IMemberWallTicketFactoryAutoGen.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IMemberWallTicket> IMemberWallTicketFactoryAutoGen.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindAll(session);
           
       }

       IMemberWallTicket IMemberWallTicketFactoryAutoGen.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(query, session);
           
       }

       IMemberWallTicket IMemberWallTicketFactoryAutoGen.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(query, session);
           
       }

       IMemberWallTicket IMemberWallTicketFactoryAutoGen.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IMemberWallTicket> IMemberWallTicketFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IMemberWallTicket IMemberWallTicketFactoryAutoGen.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
