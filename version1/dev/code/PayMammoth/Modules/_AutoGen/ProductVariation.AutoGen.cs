using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using CS.General_v3.Classes.DbObjects.Collections;
using CS.General_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.ProductVariationModule;
using BusinessLogic_v3.Modules.ProductVariationModule;


//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
    public abstract class ProductVariation_AutoGen : ProductVariationBase, PayMammoth.Modules._AutoGen.IProductVariationAutoGen
	                 
      , IMultilingualItem
      
      
      
    {
        
            

    
    
    
		

#region Multilingual
        public new PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo getCurrentCulture()
        {
            return (PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo)__getCurrentCulture();
        }             
#endregion
        
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.ProductModule.Product Product
        {
            get
            {
                return (PayMammoth.Modules.ProductModule.Product)base.Product;
            }
            set
            {
                base.Product = value;
            }
        }

		PayMammoth.Modules.ProductModule.IProduct IProductVariationAutoGen.Product
        {
            get
            {
            	return this.Product;
                
            }
            set
            {
                this.Product = (PayMammoth.Modules.ProductModule.Product)value;
            }
        }



// [userclass_collections_nhibernate]

        //UserClass_CollectionOneToManyLeftSide
        
		new internal class __OrderItemsCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.ProductVariationModule.ProductVariation, 
        	PayMammoth.Modules.OrderItemModule.OrderItem,
        	PayMammoth.Modules.OrderItemModule.IOrderItem>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.ProductVariationModule.ProductVariation, PayMammoth.Modules.OrderItemModule.OrderItem>
        {
            private PayMammoth.Modules.ProductVariationModule.ProductVariation _item = null;
            public __OrderItemsCollectionInfo(PayMammoth.Modules.ProductVariationModule.ProductVariation item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.OrderItemModule.OrderItem> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.OrderItemModule.OrderItem>)_item.__collection__OrderItems; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.ProductVariationModule.ProductVariation, PayMammoth.Modules.OrderItemModule.OrderItem>.SetLinkOnItem(PayMammoth.Modules.OrderItemModule.OrderItem item, PayMammoth.Modules.ProductVariationModule.ProductVariation value)
            {
                item.LinkedProductVariation = value;
            }
        }
        
        private __OrderItemsCollectionInfo _OrderItems = null;
        internal new __OrderItemsCollectionInfo OrderItems
        {
            get
            {
                if (_OrderItems == null)
                    _OrderItems = new __OrderItemsCollectionInfo((PayMammoth.Modules.ProductVariationModule.ProductVariation)this);
                return _OrderItems;
            }
        }               
           
		ICollectionManager<PayMammoth.Modules.OrderItemModule.IOrderItem> IProductVariationAutoGen.OrderItems
        {
            get {  return this.OrderItems; }
        }
           
        //UserClass_CollectionOneToManyLeftSide
        
		new internal class __ProductVariation_CultureInfosCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.ProductVariationModule.ProductVariation, 
        	PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo,
        	PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.ProductVariationModule.ProductVariation, PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo>
        {
            private PayMammoth.Modules.ProductVariationModule.ProductVariation _item = null;
            public __ProductVariation_CultureInfosCollectionInfo(PayMammoth.Modules.ProductVariationModule.ProductVariation item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo>)_item.__collection__ProductVariation_CultureInfos; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.ProductVariationModule.ProductVariation, PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo>.SetLinkOnItem(PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo item, PayMammoth.Modules.ProductVariationModule.ProductVariation value)
            {
                item.ProductVariation = value;
            }
        }
        
        private __ProductVariation_CultureInfosCollectionInfo _ProductVariation_CultureInfos = null;
        internal new __ProductVariation_CultureInfosCollectionInfo ProductVariation_CultureInfos
        {
            get
            {
                if (_ProductVariation_CultureInfos == null)
                    _ProductVariation_CultureInfos = new __ProductVariation_CultureInfosCollectionInfo((PayMammoth.Modules.ProductVariationModule.ProductVariation)this);
                return _ProductVariation_CultureInfos;
            }
        }               
           
		ICollectionManager<PayMammoth.Modules.ProductVariation_CultureInfoModule.IProductVariation_CultureInfo> IProductVariationAutoGen.ProductVariation_CultureInfos
        {
            get {  return this.ProductVariation_CultureInfos; }
        }
           
        //UserClass_CollectionOneToManyLeftSide
        
		new internal class __MediaItemsCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.ProductVariationModule.ProductVariation, 
        	PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem,
        	PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.ProductVariationModule.ProductVariation, PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem>
        {
            private PayMammoth.Modules.ProductVariationModule.ProductVariation _item = null;
            public __MediaItemsCollectionInfo(PayMammoth.Modules.ProductVariationModule.ProductVariation item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem>)_item.__collection__MediaItems; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.ProductVariationModule.ProductVariation, PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem>.SetLinkOnItem(PayMammoth.Modules.ProductVariationMediaItemModule.ProductVariationMediaItem item, PayMammoth.Modules.ProductVariationModule.ProductVariation value)
            {
                item.ProductVariation = value;
            }
        }
        
        private __MediaItemsCollectionInfo _MediaItems = null;
        internal new __MediaItemsCollectionInfo MediaItems
        {
            get
            {
                if (_MediaItems == null)
                    _MediaItems = new __MediaItemsCollectionInfo((PayMammoth.Modules.ProductVariationModule.ProductVariation)this);
                return _MediaItems;
            }
        }               
           
		ICollectionManager<PayMammoth.Modules.ProductVariationMediaItemModule.IProductVariationMediaItem> IProductVariationAutoGen.MediaItems
        {
            get {  return this.MediaItems; }
        }
           

        
          
		public  static new ProductVariationFactory Factory
        {
            get
            {
                return (ProductVariationFactory)ProductVariationBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<ProductVariation, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


        public void CheckItemCultureInfos(bool autoSaveInDB, IEnumerable<PayMammoth.Modules.CultureDetailsModule.CultureDetails> CultureInfos = null)
        {
            MultilingualChecker multilingualChecker = new MultilingualChecker();
            multilingualChecker.ItemToAdd+=new MultilingualChecker.ItemToAddHandler(multilingualChecker_ItemToAdd);
            multilingualChecker.ItemRemoved += new MultilingualChecker.ItemRemovedHandler(multilingualChecker_ItemRemoved);
            multilingualChecker.CheckItemCultureInfos(this, CultureInfos);
            if (autoSaveInDB)
                this.Save();
        }


        private void multilingualChecker_ItemToAdd(BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase cultureDetails)
        {
            var itemCultureInfo = PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo.Factory.CreateNewItem();

            itemCultureInfo.ProductVariation = (ProductVariation)this;  //item link
            itemCultureInfo.CultureInfo = (PayMammoth.Modules.CultureDetailsModule.CultureDetails)cultureDetails; //culture


// [userfactory_checkcultureinfos_multilingualfields]



			itemCultureInfo.MarkAsNotTemporary();
			this.ProductVariation_CultureInfos.Add((PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo) itemCultureInfo);
            
            

        }

		private void multilingualChecker_ItemRemoved(IMultilingualContentInfo item)
        {   
        	this.ProductVariation_CultureInfos.Remove((PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo)item);
            
        }

        
        protected override BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBase __getCultureByCultureDBId(long cultureDBId)
        {
        	bool sessionExists = NHClasses.NhManager.CheckSessionExistsForContext();
            MyNHSessionBase session = null;
            if (!sessionExists)
            {

                CS.General_v3.Error.Reporter.ReportError(CS.General_v3.Enums.LOG4NET_MSG_TYPE.Debug, "Session did not exist when retrieving the multilingual collection");

                 session = NHClasses.NhManager.CreateNewSessionForContextAndReturn();
                
                 this.Merge(session);
            }
        	foreach (var cultureInfo in this.ProductVariation_CultureInfos)
            {
                if (cultureInfo.CultureInfo.ID == cultureDBId)
                {
                    _currentCulture = cultureInfo;
                    break;
                }
            }
            if (!sessionExists)
            {
                NHClasses.NhManager.DisposeCurrentSessionInContext();
            }
            return _currentCulture;
        }
        private PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo _currentCulture = null;

        protected override BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBase __getCurrentCulture()
        {

            var currentCultureID = GetCurrentCultureDBId();
            
            if (_currentCulture == null || _currentCulture.CultureInfo.ID != currentCultureID)
            {   //requires re-loading
            	_currentCulture = null;
                _currentCulture = (PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo)__getCultureByCultureDBId(currentCultureID);
            }
            
            if (_currentCulture == null)
            {
                this.CheckItemCultureInfos(autoSaveInDB:false);
                
            }
            
            if (_currentCulture == null)
            {
                _currentCulture = (PayMammoth.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfo)__getCultureByCultureDBId(currentCultureID);
            }
            return _currentCulture;
        }
        

        
         #region IMultilingualItem Members

        IEnumerable<IMultilingualContentInfo> IMultilingualItem.GetMultilingualContentInfos()
        {
            return this.ProductVariation_CultureInfos;
            
        }


#endregion    

     
        
        


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
