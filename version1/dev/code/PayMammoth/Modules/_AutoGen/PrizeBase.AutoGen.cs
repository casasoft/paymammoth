using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using CS.General_v3.Classes.DbObjects.Collections;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using CS.General_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using PayMammoth.Modules._AutoGen;                

//BaseClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
	
    public abstract class PrizeBaseAutoGen 
    : BusinessLogic_v3.Classes.DB.BaseDbObject, IPrizeBaseAutoGen
    
      
      
      
    
    
    		
    {
    
   
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	
        public static PayMammoth.Modules._AutoGen.PrizeBaseCmsFactory CmsFactory
        {
            get { return PayMammoth.Modules._AutoGen.PrizeBaseCmsFactory.Instance; }
        }
        
     	public static PrizeBaseFactory Factory
        {
            get
            {
                return PrizeBaseFactory.Instance; 
            }
        }    
        /*
        public static PrizeBase CreateNewItem()
        {
            return (PrizeBase)Factory.CreateNewItem();
        }

		public static PrizeBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<PrizeBase, PrizeBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static PrizeBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static PrizeBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<PrizeBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<PrizeBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<PrizeBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _title;
        public virtual string Title 
        {
        	get
        	{
        		return _title;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_title,value);
        		_title = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _description;
        public virtual string Description 
        {
        	get
        	{
        		return _description;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_description,value);
        		_description = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private double _retailvalue;
        public virtual double RetailValue 
        {
        	get
        	{
        		return _retailvalue;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_retailvalue,value);
        		_retailvalue = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    
#region Multilingual
          


#endregion    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
