using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.ProductModule;
using CS.WebComponentsGeneralV3.Cms.ProductModule;
using PayMammoth.Frontend.ProductModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class ProductCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.ProductModule.ProductBaseCmsInfo
    {

		public ProductCmsInfo_AutoGen(BusinessLogic_v3.Modules.ProductModule.ProductBase item)
            : base(item)
        {

        }
        

        private ProductFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = ProductFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new ProductFrontend FrontendItem
        {
            get { return (ProductFrontend) base.FrontendItem; }
        }
        
        
		public new Product DbItem
        {
            get
            {
                return (Product)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.Title = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.Title),
        						isEditable: true, isVisible: true);

        this.ReferenceCode = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.ReferenceCode),
        						isEditable: true, isVisible: true);

        this.SupplierReferenceCode = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.SupplierReferenceCode),
        						isEditable: true, isVisible: true);

        this.Description = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.Description),
        						isEditable: true, isVisible: true);

        this.PriceRetailIncTaxPerUnit = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.PriceRetailIncTaxPerUnit),
        						isEditable: true, isVisible: true);

        this.PriceWholesale = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.PriceWholesale),
        						isEditable: true, isVisible: true);

//InitFieldUser_LinkedProperty
			this.Brand = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.Brand),
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.BrandModule.Brand>.GetPropertyBySelector( item => item.Products)));

        
        this.ExtraInfo = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.ExtraInfo),
        						isEditable: true, isVisible: true);

        this.PriceRetailBefore = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.PriceRetailBefore),
        						isEditable: true, isVisible: true);

        this.PDFFilename = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.PDFFilename),
        						isEditable: false, isVisible: false);

        this.IsSpecialOffer = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.IsSpecialOffer),
        						isEditable: true, isVisible: true);

        this.ShowPrice = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.ShowPrice),
        						isEditable: true, isVisible: true);

        this.WeightInKg = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.WeightInKg),
        						isEditable: true, isVisible: true);

        this.WarrantyInMonths = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.WarrantyInMonths),
        						isEditable: true, isVisible: true);

        this.DateAdded = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.DateAdded),
        						isEditable: true, isVisible: true);

        this.IsFeatured = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.IsFeatured),
        						isEditable: true, isVisible: true);

        this.ImportReference = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.ImportReference),
        						isEditable: true, isVisible: true);

        this.WarrantyText = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.WarrantyText),
        						isEditable: true, isVisible: true);

        this.MetaKeywords = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.MetaKeywords),
        						isEditable: true, isVisible: true);

        this.CategoryFeatureValues_ForSearch = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.CategoryFeatureValues_ForSearch),
        						isEditable: true, isVisible: true);

        this.PriceRetailTaxPerUnit = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.PriceRetailTaxPerUnit),
        						isEditable: true, isVisible: true);

        this.TaxRatePercentage = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.TaxRatePercentage),
        						isEditable: true, isVisible: true);

        this.ProductSpecificationsHtml = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.ProductSpecificationsHtml),
        						isEditable: true, isVisible: true);

        this.DimensionsLengthInCm = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.DimensionsLengthInCm),
        						isEditable: true, isVisible: true);

        this.DimensionsWidthInCm = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.DimensionsWidthInCm),
        						isEditable: true, isVisible: true);

        this.DimensionsHeightInCm = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.DimensionsHeightInCm),
        						isEditable: true, isVisible: true);

        this.DimensionsVolumeInCc = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.ProductModule.Product>.GetPropertyBySelector( item => item.DimensionsVolumeInCc),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
