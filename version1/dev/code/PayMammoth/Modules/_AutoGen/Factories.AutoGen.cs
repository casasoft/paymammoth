using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.Modules._AutoGen
{
    public class FactoriesAutoGen 
    {


// [userproject_factories_interfaces]

		//UserProject_Factory_InterfaceDeclaration
		
        /*public static PayMammoth.Modules.ArticleModule.IArticleFactory ArticleFactory
        {
            get { return (PayMammoth.Modules.ArticleModule.IArticleFactory)BusinessLogic_v3.Modules.Factories.ArticleFactory; }
            set { BusinessLogic_v3.Modules.Factories.ArticleFactory = value; }
        }*/
        public static PayMammoth.Modules.ArticleModule.IArticleFactory ArticleFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<PayMammoth.Modules.ArticleModule.IArticleFactory>(); }
        } 

		//UserProject_Factory_InterfaceDeclaration
		
        /*public static PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfoFactory Article_CultureInfoFactory
        {
            get { return (PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfoFactory)BusinessLogic_v3.Modules.Factories.Article_CultureInfoFactory; }
            set { BusinessLogic_v3.Modules.Factories.Article_CultureInfoFactory = value; }
        }*/
        public static PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfoFactory Article_CultureInfoFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfoFactory>(); }
        } 

		//UserProject_Factory_InterfaceDeclaration
		
        /*public static PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLinkFactory Article_ParentChildLinkFactory
        {
            get { return (PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLinkFactory)BusinessLogic_v3.Modules.Factories.Article_ParentChildLinkFactory; }
            set { BusinessLogic_v3.Modules.Factories.Article_ParentChildLinkFactory = value; }
        }*/
        public static PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLinkFactory Article_ParentChildLinkFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<PayMammoth.Modules.Article_ParentChildLinkModule.IArticle_ParentChildLinkFactory>(); }
        } 

		//UserProject_Factory_InterfaceDeclaration
		
        /*public static PayMammoth.Modules.AuditLogModule.IAuditLogFactory AuditLogFactory
        {
            get { return (PayMammoth.Modules.AuditLogModule.IAuditLogFactory)BusinessLogic_v3.Modules.Factories.AuditLogFactory; }
            set { BusinessLogic_v3.Modules.Factories.AuditLogFactory = value; }
        }*/
        public static PayMammoth.Modules.AuditLogModule.IAuditLogFactory AuditLogFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<PayMammoth.Modules.AuditLogModule.IAuditLogFactory>(); }
        } 

		//UserProject_Factory_InterfaceDeclaration
		
        /*public static PayMammoth.Modules.CmsUserModule.ICmsUserFactory CmsUserFactory
        {
            get { return (PayMammoth.Modules.CmsUserModule.ICmsUserFactory)BusinessLogic_v3.Modules.Factories.CmsUserFactory; }
            set { BusinessLogic_v3.Modules.Factories.CmsUserFactory = value; }
        }*/
        public static PayMammoth.Modules.CmsUserModule.ICmsUserFactory CmsUserFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<PayMammoth.Modules.CmsUserModule.ICmsUserFactory>(); }
        } 

		//UserProject_Factory_InterfaceDeclaration
		
        /*public static PayMammoth.Modules.CmsUserRoleModule.ICmsUserRoleFactory CmsUserRoleFactory
        {
            get { return (PayMammoth.Modules.CmsUserRoleModule.ICmsUserRoleFactory)BusinessLogic_v3.Modules.Factories.CmsUserRoleFactory; }
            set { BusinessLogic_v3.Modules.Factories.CmsUserRoleFactory = value; }
        }*/
        public static PayMammoth.Modules.CmsUserRoleModule.ICmsUserRoleFactory CmsUserRoleFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<PayMammoth.Modules.CmsUserRoleModule.ICmsUserRoleFactory>(); }
        } 

		//UserProject_Factory_InterfaceDeclaration
		
        /*public static PayMammoth.Modules.ContactFormModule.IContactFormFactory ContactFormFactory
        {
            get { return (PayMammoth.Modules.ContactFormModule.IContactFormFactory)BusinessLogic_v3.Modules.Factories.ContactFormFactory; }
            set { BusinessLogic_v3.Modules.Factories.ContactFormFactory = value; }
        }*/
        public static PayMammoth.Modules.ContactFormModule.IContactFormFactory ContactFormFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<PayMammoth.Modules.ContactFormModule.IContactFormFactory>(); }
        } 

		//UserProject_Factory_InterfaceDeclaration
		
        /*public static PayMammoth.Modules.ContentTextModule.IContentTextFactory ContentTextFactory
        {
            get { return (PayMammoth.Modules.ContentTextModule.IContentTextFactory)BusinessLogic_v3.Modules.Factories.ContentTextFactory; }
            set { BusinessLogic_v3.Modules.Factories.ContentTextFactory = value; }
        }*/
        public static PayMammoth.Modules.ContentTextModule.IContentTextFactory ContentTextFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<PayMammoth.Modules.ContentTextModule.IContentTextFactory>(); }
        } 

		//UserProject_Factory_InterfaceDeclaration
		
        /*public static PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfoFactory ContentText_CultureInfoFactory
        {
            get { return (PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfoFactory)BusinessLogic_v3.Modules.Factories.ContentText_CultureInfoFactory; }
            set { BusinessLogic_v3.Modules.Factories.ContentText_CultureInfoFactory = value; }
        }*/
        public static PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfoFactory ContentText_CultureInfoFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfoFactory>(); }
        } 

		//UserProject_Factory_InterfaceDeclaration
		
        /*public static PayMammoth.Modules.CultureDetailsModule.ICultureDetailsFactory CultureDetailsFactory
        {
            get { return (PayMammoth.Modules.CultureDetailsModule.ICultureDetailsFactory)BusinessLogic_v3.Modules.Factories.CultureDetailsFactory; }
            set { BusinessLogic_v3.Modules.Factories.CultureDetailsFactory = value; }
        }*/
        public static PayMammoth.Modules.CultureDetailsModule.ICultureDetailsFactory CultureDetailsFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<PayMammoth.Modules.CultureDetailsModule.ICultureDetailsFactory>(); }
        } 

		//UserProject_Factory_InterfaceDeclaration
		
        /*public static PayMammoth.Modules.CurrencyModule.ICurrencyFactory CurrencyFactory
        {
            get { return (PayMammoth.Modules.CurrencyModule.ICurrencyFactory)BusinessLogic_v3.Modules.Factories.CurrencyFactory; }
            set { BusinessLogic_v3.Modules.Factories.CurrencyFactory = value; }
        }*/
        public static PayMammoth.Modules.CurrencyModule.ICurrencyFactory CurrencyFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<PayMammoth.Modules.CurrencyModule.ICurrencyFactory>(); }
        } 

		//UserProject_Factory_InterfaceDeclaration
		
        /*public static PayMammoth.Modules.EmailLogModule.IEmailLogFactory EmailLogFactory
        {
            get { return (PayMammoth.Modules.EmailLogModule.IEmailLogFactory)BusinessLogic_v3.Modules.Factories.EmailLogFactory; }
            set { BusinessLogic_v3.Modules.Factories.EmailLogFactory = value; }
        }*/
        public static PayMammoth.Modules.EmailLogModule.IEmailLogFactory EmailLogFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<PayMammoth.Modules.EmailLogModule.IEmailLogFactory>(); }
        } 

		//UserProject_Factory_InterfaceDeclaration
		
        /*public static PayMammoth.Modules.EmailTextModule.IEmailTextFactory EmailTextFactory
        {
            get { return (PayMammoth.Modules.EmailTextModule.IEmailTextFactory)BusinessLogic_v3.Modules.Factories.EmailTextFactory; }
            set { BusinessLogic_v3.Modules.Factories.EmailTextFactory = value; }
        }*/
        public static PayMammoth.Modules.EmailTextModule.IEmailTextFactory EmailTextFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<PayMammoth.Modules.EmailTextModule.IEmailTextFactory>(); }
        } 

		//UserProject_Factory_InterfaceDeclaration
		
        /*public static PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfoFactory EmailText_CultureInfoFactory
        {
            get { return (PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfoFactory)BusinessLogic_v3.Modules.Factories.EmailText_CultureInfoFactory; }
            set { BusinessLogic_v3.Modules.Factories.EmailText_CultureInfoFactory = value; }
        }*/
        public static PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfoFactory EmailText_CultureInfoFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<PayMammoth.Modules.EmailText_CultureInfoModule.IEmailText_CultureInfoFactory>(); }
        } 

		//UserProject_Factory_InterfaceDeclaration
		
        /*public static PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfoFactory ImageSizingInfoFactory
        {
            get { return (PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfoFactory)BusinessLogic_v3.Modules.Factories.ImageSizingInfoFactory; }
            set { BusinessLogic_v3.Modules.Factories.ImageSizingInfoFactory = value; }
        }*/
        public static PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfoFactory ImageSizingInfoFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<PayMammoth.Modules.ImageSizingInfoModule.IImageSizingInfoFactory>(); }
        } 

		//UserProject_Factory_InterfaceDeclaration
		
        /*public static PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfoFactory ImageSpecificSizeInfoFactory
        {
            get { return (PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfoFactory)BusinessLogic_v3.Modules.Factories.ImageSpecificSizeInfoFactory; }
            set { BusinessLogic_v3.Modules.Factories.ImageSpecificSizeInfoFactory = value; }
        }*/
        public static PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfoFactory ImageSpecificSizeInfoFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<PayMammoth.Modules.ImageSpecificSizeInfoModule.IImageSpecificSizeInfoFactory>(); }
        } 

		//UserProject_Factory_InterfaceDeclaration
		
        /*private static PayMammoth.Modules.PaymentMethodModule.IPaymentMethodFactory _PaymentMethodFactory = PayMammoth.Modules.PaymentMethodModule.PaymentMethodFactory.Instance;
        public static PayMammoth.Modules.PaymentMethodModule.IPaymentMethodFactory PaymentMethodFactory
        {
            get { return _PaymentMethodFactory; }
            set { _PaymentMethodFactory = value; }
        }*/
        public static PayMammoth.Modules.PaymentMethodModule.IPaymentMethodFactory PaymentMethodFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<PayMammoth.Modules.PaymentMethodModule.IPaymentMethodFactory>(); }
        } 

		//UserProject_Factory_InterfaceDeclaration
		
        /*private static PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfoFactory _PaymentMethod_CultureInfoFactory = PayMammoth.Modules.PaymentMethod_CultureInfoModule.PaymentMethod_CultureInfoFactory.Instance;
        public static PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfoFactory PaymentMethod_CultureInfoFactory
        {
            get { return _PaymentMethod_CultureInfoFactory; }
            set { _PaymentMethod_CultureInfoFactory = value; }
        }*/
        public static PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfoFactory PaymentMethod_CultureInfoFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<PayMammoth.Modules.PaymentMethod_CultureInfoModule.IPaymentMethod_CultureInfoFactory>(); }
        } 

		//UserProject_Factory_InterfaceDeclaration
		
        /*private static PayMammoth.Modules.PaymentRequestModule.IPaymentRequestFactory _PaymentRequestFactory = PayMammoth.Modules.PaymentRequestModule.PaymentRequestFactory.Instance;
        public static PayMammoth.Modules.PaymentRequestModule.IPaymentRequestFactory PaymentRequestFactory
        {
            get { return _PaymentRequestFactory; }
            set { _PaymentRequestFactory = value; }
        }*/
        public static PayMammoth.Modules.PaymentRequestModule.IPaymentRequestFactory PaymentRequestFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<PayMammoth.Modules.PaymentRequestModule.IPaymentRequestFactory>(); }
        } 

		//UserProject_Factory_InterfaceDeclaration
		
        /*private static PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetailFactory _PaymentRequestItemDetailFactory = PayMammoth.Modules.PaymentRequestItemDetailModule.PaymentRequestItemDetailFactory.Instance;
        public static PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetailFactory PaymentRequestItemDetailFactory
        {
            get { return _PaymentRequestItemDetailFactory; }
            set { _PaymentRequestItemDetailFactory = value; }
        }*/
        public static PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetailFactory PaymentRequestItemDetailFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<PayMammoth.Modules.PaymentRequestItemDetailModule.IPaymentRequestItemDetailFactory>(); }
        } 

		//UserProject_Factory_InterfaceDeclaration
		
        /*private static PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransactionFactory _PaymentRequestTransactionFactory = PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransactionFactory.Instance;
        public static PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransactionFactory PaymentRequestTransactionFactory
        {
            get { return _PaymentRequestTransactionFactory; }
            set { _PaymentRequestTransactionFactory = value; }
        }*/
        public static PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransactionFactory PaymentRequestTransactionFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransactionFactory>(); }
        } 

		//UserProject_Factory_InterfaceDeclaration
		
        /*public static PayMammoth.Modules.RoutingInfoModule.IRoutingInfoFactory RoutingInfoFactory
        {
            get { return (PayMammoth.Modules.RoutingInfoModule.IRoutingInfoFactory)BusinessLogic_v3.Modules.Factories.RoutingInfoFactory; }
            set { BusinessLogic_v3.Modules.Factories.RoutingInfoFactory = value; }
        }*/
        public static PayMammoth.Modules.RoutingInfoModule.IRoutingInfoFactory RoutingInfoFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<PayMammoth.Modules.RoutingInfoModule.IRoutingInfoFactory>(); }
        } 

		//UserProject_Factory_InterfaceDeclaration
		
        /*public static PayMammoth.Modules.SettingModule.ISettingFactory SettingFactory
        {
            get { return (PayMammoth.Modules.SettingModule.ISettingFactory)BusinessLogic_v3.Modules.Factories.SettingFactory; }
            set { BusinessLogic_v3.Modules.Factories.SettingFactory = value; }
        }*/
        public static PayMammoth.Modules.SettingModule.ISettingFactory SettingFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<PayMammoth.Modules.SettingModule.ISettingFactory>(); }
        } 

		//UserProject_Factory_InterfaceDeclaration
		
        /*private static PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccountFactory _WebsiteAccountFactory = PayMammoth.Modules.WebsiteAccountModule.WebsiteAccountFactory.Instance;
        public static PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccountFactory WebsiteAccountFactory
        {
            get { return _WebsiteAccountFactory; }
            set { _WebsiteAccountFactory = value; }
        }*/
        public static PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccountFactory WebsiteAccountFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<PayMammoth.Modules.WebsiteAccountModule.IWebsiteAccountFactory>(); }
        } 

		//UserProject_Factory_InterfaceDeclaration
		
        /*private static PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfoFactory _WebsiteAccount_CultureInfoFactory = PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfoFactory.Instance;
        public static PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfoFactory WebsiteAccount_CultureInfoFactory
        {
            get { return _WebsiteAccount_CultureInfoFactory; }
            set { _WebsiteAccount_CultureInfoFactory = value; }
        }*/
        public static PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfoFactory WebsiteAccount_CultureInfoFactory
        {
            get { return BusinessLogic_v3.Classes.DbObjects.DataFactoriesController.Instance.GetFactoryWhichImplements<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfoFactory>(); }
        } 



    }
}
