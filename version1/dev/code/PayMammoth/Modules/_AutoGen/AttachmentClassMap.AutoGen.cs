using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.AttachmentModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class AttachmentMap_AutoGen : AttachmentMap_AutoGen_Z
    {
        public AttachmentMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.FileName, FileNameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ContentType, ContentTypeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DateSubmitted, DateSubmittedMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class AttachmentMap_AutoGen_Z : BusinessLogic_v3.Modules.AttachmentModule.AttachmentBaseMap<AttachmentImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
