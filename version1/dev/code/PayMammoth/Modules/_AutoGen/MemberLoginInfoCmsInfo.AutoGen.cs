using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.MemberLoginInfoModule;
using CS.WebComponentsGeneralV3.Cms.MemberLoginInfoModule;
using PayMammoth.Frontend.MemberLoginInfoModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class MemberLoginInfoCmsInfo_AutoGen : CS.WebComponentsGeneralV3.Cms.MemberLoginInfoModule.MemberLoginInfoBaseCmsInfo
    {

		public MemberLoginInfoCmsInfo_AutoGen(BusinessLogic_v3.Modules.MemberLoginInfoModule.MemberLoginInfoBase item)
            : base(item)
        {

        }
        

        private MemberLoginInfoFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = MemberLoginInfoFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new MemberLoginInfoFrontend FrontendItem
        {
            get { return (MemberLoginInfoFrontend) base.FrontendItem; }
        }
        
        
		public new MemberLoginInfo DbItem
        {
            get
            {
                return (MemberLoginInfo)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

//InitFieldUser_LinkedProperty
			this.Member = base.AddProperty(
            new CmsLinkedPropertyInfo(this,
                CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberLoginInfoModule.MemberLoginInfo>.GetPropertyBySelector( item => item.Member),
                null));

        
        this.DateTime = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberLoginInfoModule.MemberLoginInfo>.GetPropertyBySelector( item => item.DateTime),
        						isEditable: true, isVisible: true);

        this.IP = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberLoginInfoModule.MemberLoginInfo>.GetPropertyBySelector( item => item.IP),
        						isEditable: true, isVisible: true);

        this.LoginInfoType = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberLoginInfoModule.MemberLoginInfo>.GetPropertyBySelector( item => item.LoginInfoType),
        						isEditable: true, isVisible: true);

        this.AdditionalMsg = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.MemberLoginInfoModule.MemberLoginInfo>.GetPropertyBySelector( item => item.AdditionalMsg),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]



			base.initBasicFields();
		}
    
    }
}
