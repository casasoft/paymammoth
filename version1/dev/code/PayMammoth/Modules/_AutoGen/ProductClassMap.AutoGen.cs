using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.ProductModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class ProductMap_AutoGen : ProductMap_AutoGen_Z
    {
        public ProductMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.Title, TitleMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ReferenceCode, ReferenceCodeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.SupplierReferenceCode, SupplierReferenceCodeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Description, DescriptionMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PriceRetailIncTaxPerUnit, PriceRetailIncTaxPerUnitMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PriceWholesale, PriceWholesaleMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.Brand, BrandMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ExtraInfo, ExtraInfoMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PriceRetailBefore, PriceRetailBeforeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PDFFilename, PDFFilenameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.IsSpecialOffer, IsSpecialOfferMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ShowPrice, ShowPriceMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.WeightInKg, WeightInKgMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.WarrantyInMonths, WarrantyInMonthsMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DateAdded, DateAddedMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.IsFeatured, IsFeaturedMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ImportReference, ImportReferenceMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.WarrantyText, WarrantyTextMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.MetaKeywords, MetaKeywordsMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CategoryFeatureValues_ForSearch, CategoryFeatureValues_ForSearchMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.PriceRetailTaxPerUnit, PriceRetailTaxPerUnitMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.TaxRatePercentage, TaxRatePercentageMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ProductSpecificationsHtml, ProductSpecificationsHtmlMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DimensionsLengthInCm, DimensionsLengthInCmMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DimensionsWidthInCm, DimensionsWidthInCmMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DimensionsHeightInCm, DimensionsHeightInCmMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.DimensionsVolumeInCc, DimensionsVolumeInCcMappingInfo);



// [userclassmap_initcollections]

//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.Product_CultureInfos, Product_CultureInfosMappingInfo, CS.General_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.CategoryLinks, CategoryLinksMappingInfo, CS.General_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.ProductCategoryFeatureValues, ProductCategoryFeatureValuesMappingInfo, CS.General_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.ProductCategorySpecificationValues, ProductCategorySpecificationValuesMappingInfo, CS.General_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.ProductVariations, ProductVariationsMappingInfo, CS.General_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class ProductMap_AutoGen_Z : BusinessLogic_v3.Modules.ProductModule.ProductBaseMap<ProductImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
