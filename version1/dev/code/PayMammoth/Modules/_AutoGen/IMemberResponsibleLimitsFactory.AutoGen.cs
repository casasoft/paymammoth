using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.NHibernateClasses;

using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.MemberResponsibleLimitsModule;
using PayMammoth.Modules.MemberResponsibleLimitsModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IMemberResponsibleLimitsFactoryAutoGen : BusinessLogic_v3.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimitsBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IMemberResponsibleLimits>
    
    {
	    new PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits CreateNewItem();
        new PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.MemberResponsibleLimitsModule.IMemberResponsibleLimits> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
