using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.CultureDetailsModule;
using PayMammoth.Modules.CultureDetailsModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class CultureDetailsFactoryAutoGen : BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory, 
    				PayMammoth.Modules._AutoGen.ICultureDetailsFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<ICultureDetails>
    
    {
    
     	public new CultureDetails ReloadItemFromDatabase(BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase item)
        {
         	return (CultureDetails)base.ReloadItemFromDatabase(item);
        }
     	static CultureDetailsFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public CultureDetailsFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(CultureDetailsImpl);
			PayMammoth.Cms.CultureDetailsModule.CultureDetailsCmsFactory._initialiseStaticInstance();
        }
        public static new CultureDetailsFactory Instance
        {
            get
            {
                return (CultureDetailsFactory)CultureDetailsBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<CultureDetails> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<CultureDetails>();
            
        }
        public new IEnumerable<CultureDetails> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<CultureDetails>();
            
            
        }
    
        public new IEnumerable<CultureDetails> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<CultureDetails>();
            
        }
        public new IEnumerable<CultureDetails> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<CultureDetails>();
        }
        
        public new CultureDetails FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (CultureDetails)base.FindItem(query);
        }
        public new CultureDetails FindItem(IQueryOver query)
        {
            return (CultureDetails)base.FindItem(query);
        }
        
        ICultureDetails ICultureDetailsFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new CultureDetails CreateNewItem()
        {
            return (CultureDetails)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<CultureDetails, CultureDetails> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<CultureDetails>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.CultureDetailsModule.CultureDetails GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.CultureDetailsModule.CultureDetails) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.CultureDetailsModule.CultureDetails> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.CultureDetailsModule.CultureDetails>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.CultureDetailsModule.CultureDetails> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.CultureDetailsModule.CultureDetails>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.CultureDetailsModule.CultureDetails> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.CultureDetailsModule.CultureDetails>) base.FindAll(session);

       }
       public new PayMammoth.Modules.CultureDetailsModule.CultureDetails FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.CultureDetailsModule.CultureDetails) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.CultureDetailsModule.CultureDetails FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.CultureDetailsModule.CultureDetails) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.CultureDetailsModule.CultureDetails FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.CultureDetailsModule.CultureDetails) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.CultureDetailsModule.CultureDetails> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.CultureDetailsModule.CultureDetails>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.CultureDetailsModule.ICultureDetails> Members
     
        PayMammoth.Modules.CultureDetailsModule.ICultureDetails IBaseDbFactory<PayMammoth.Modules.CultureDetailsModule.ICultureDetails>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.CultureDetailsModule.ICultureDetails IBaseDbFactory<PayMammoth.Modules.CultureDetailsModule.ICultureDetails>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.CultureDetailsModule.ICultureDetails> IBaseDbFactory<PayMammoth.Modules.CultureDetailsModule.ICultureDetails>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.CultureDetailsModule.ICultureDetails> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.CultureDetailsModule.ICultureDetails>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.CultureDetailsModule.ICultureDetails> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.CultureDetailsModule.ICultureDetails>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.CultureDetailsModule.ICultureDetails IBaseDbFactory<PayMammoth.Modules.CultureDetailsModule.ICultureDetails>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.CultureDetailsModule.ICultureDetails BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.CultureDetailsModule.ICultureDetails>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.CultureDetailsModule.ICultureDetails BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.CultureDetailsModule.ICultureDetails>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.CultureDetailsModule.ICultureDetails> IBaseDbFactory<PayMammoth.Modules.CultureDetailsModule.ICultureDetails>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<ICultureDetails> ICultureDetailsFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<ICultureDetails> ICultureDetailsFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<ICultureDetails> ICultureDetailsFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       ICultureDetails ICultureDetailsFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       ICultureDetails ICultureDetailsFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       ICultureDetails ICultureDetailsFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<ICultureDetails> ICultureDetailsFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       ICultureDetails ICultureDetailsFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
