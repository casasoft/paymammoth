using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using CS.General_v3.Classes.DbObjects.Collections;
using CS.General_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.ProductVariation_CultureInfoModule;
using BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule;


//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
    public abstract class ProductVariation_CultureInfo_AutoGen : ProductVariation_CultureInfoBase, PayMammoth.Modules._AutoGen.IProductVariation_CultureInfoAutoGen
	
      
      
      , IMultilingualContentInfo
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.CultureDetailsModule.CultureDetails CultureInfo
        {
            get
            {
                return (PayMammoth.Modules.CultureDetailsModule.CultureDetails)base.CultureInfo;
            }
            set
            {
                base.CultureInfo = value;
            }
        }

		PayMammoth.Modules.CultureDetailsModule.ICultureDetails IProductVariation_CultureInfoAutoGen.CultureInfo
        {
            get
            {
            	return this.CultureInfo;
                
            }
            set
            {
                this.CultureInfo = (PayMammoth.Modules.CultureDetailsModule.CultureDetails)value;
            }
        }
		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.ProductVariationModule.ProductVariation ProductVariation
        {
            get
            {
                return (PayMammoth.Modules.ProductVariationModule.ProductVariation)base.ProductVariation;
            }
            set
            {
                base.ProductVariation = value;
            }
        }

		PayMammoth.Modules.ProductVariationModule.IProductVariation IProductVariation_CultureInfoAutoGen.ProductVariation
        {
            get
            {
            	return this.ProductVariation;
                
            }
            set
            {
                this.ProductVariation = (PayMammoth.Modules.ProductVariationModule.ProductVariation)value;
            }
        }



// [userclass_collections_nhibernate]


        
          
		public  static new ProductVariation_CultureInfoFactory Factory
        {
            get
            {
                return (ProductVariation_CultureInfoFactory)ProductVariation_CultureInfoBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<ProductVariation_CultureInfo, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
