using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using CS.General_v3.Classes.DbObjects.Collections;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using CS.General_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using PayMammoth.Modules._AutoGen;                

//BaseClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
	
    public abstract class WallPrizeLinkBaseAutoGen 
    : BusinessLogic_v3.Classes.DB.BaseDbObject, IWallPrizeLinkBaseAutoGen
    
      
      
      
    
    
    		
    {
    
   
    	protected override void initPropertiesForNewItem()
        {          
            
            base.initPropertiesForNewItem();
        }
    	
        public static PayMammoth.Modules._AutoGen.WallPrizeLinkBaseCmsFactory CmsFactory
        {
            get { return PayMammoth.Modules._AutoGen.WallPrizeLinkBaseCmsFactory.Instance; }
        }
        
     	public static WallPrizeLinkBaseFactory Factory
        {
            get
            {
                return WallPrizeLinkBaseFactory.Instance; 
            }
        }    
        /*
        public static WallPrizeLinkBase CreateNewItem()
        {
            return (WallPrizeLinkBase)Factory.CreateNewItem();
        }

		public static WallPrizeLinkBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<WallPrizeLinkBase, WallPrizeLinkBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static WallPrizeLinkBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static WallPrizeLinkBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<WallPrizeLinkBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<WallPrizeLinkBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<WallPrizeLinkBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

/*
		public virtual long? WallID
		{
		 	get 
		 	{
		 		return (this.Wall != null ? (long?)this.Wall.ID : null);
		 	}
		}*/
		
        protected PayMammoth.Modules._AutoGen.WallBase _wall;
        public virtual PayMammoth.Modules._AutoGen.WallBase Wall 
        {
        	get
        	{
        		return _wall;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_wall,value);
        		_wall = value;
        	}
        }
            
        PayMammoth.Modules._AutoGen.IWallBase PayMammoth.Modules._AutoGen.IWallPrizeLinkBaseAutoGen.Wall 
        {
            get
            {
            	return this.Wall;
            }
            set
            {
            	this.Wall = (PayMammoth.Modules._AutoGen.WallBase) value;
            }
        }
            
/*
		public virtual long? PrizeID
		{
		 	get 
		 	{
		 		return (this.Prize != null ? (long?)this.Prize.ID : null);
		 	}
		}*/
		
        protected PayMammoth.Modules._AutoGen.PrizeBase _prize;
        public virtual PayMammoth.Modules._AutoGen.PrizeBase Prize 
        {
        	get
        	{
        		return _prize;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_prize,value);
        		_prize = value;
        	}
        }
            
        PayMammoth.Modules._AutoGen.IPrizeBase PayMammoth.Modules._AutoGen.IWallPrizeLinkBaseAutoGen.Prize 
        {
            get
            {
            	return this.Prize;
            }
            set
            {
            	this.Prize = (PayMammoth.Modules._AutoGen.PrizeBase) value;
            }
        }
            
 //check Property_Pkey, etc


// [base_collections]


    
#region Multilingual
          


#endregion    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
