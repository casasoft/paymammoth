using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.CultureDetailsModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Modules.CultureDetailsModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.CultureDetailsModule.CultureDetailsFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.CultureDetailsModule.ICultureDetails> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.CultureDetailsModule.CultureDetailsFrontend ToFrontend(this PayMammoth.Modules.CultureDetailsModule.ICultureDetails item)
        {
        	return PayMammoth.Frontend.CultureDetailsModule.CultureDetailsFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class CultureDetailsFrontend_AutoGen : BusinessLogic_v3.Frontend.CultureDetailsModule.CultureDetailsBaseFrontend

    {
		
        
        protected CultureDetailsFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.CultureDetailsModule.ICultureDetails Data
        {
            get { return (PayMammoth.Modules.CultureDetailsModule.ICultureDetails)base.Data; }
            set { base.Data = value; }

        }

        public new static CultureDetailsFrontend CreateNewItem()
        {
            return (CultureDetailsFrontend)BusinessLogic_v3.Frontend.CultureDetailsModule.CultureDetailsBaseFrontend.CreateNewItem();
        }
        
        public new static CultureDetailsFrontend Get(BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase data)
        {
            return (CultureDetailsFrontend)BusinessLogic_v3.Frontend.CultureDetailsModule.CultureDetailsBaseFrontend.Get(data);
        }
        public new static List<CultureDetailsFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase> dataList)
        {
            return BusinessLogic_v3.Frontend.CultureDetailsModule.CultureDetailsBaseFrontend.GetList(dataList).Cast<CultureDetailsFrontend>().ToList();
        }


    }
}
