using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.AuditLogModule;
using PayMammoth.Modules.AuditLogModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class AuditLogFactoryAutoGen : BusinessLogic_v3.Modules.AuditLogModule.AuditLogBaseFactory, 
    				PayMammoth.Modules._AutoGen.IAuditLogFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<IAuditLog>
    
    {
    
     	public new AuditLog ReloadItemFromDatabase(BusinessLogic_v3.Modules.AuditLogModule.AuditLogBase item)
        {
         	return (AuditLog)base.ReloadItemFromDatabase(item);
        }
     	static AuditLogFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public AuditLogFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(AuditLogImpl);
			PayMammoth.Cms.AuditLogModule.AuditLogCmsFactory._initialiseStaticInstance();
        }
        public static new AuditLogFactory Instance
        {
            get
            {
                return (AuditLogFactory)AuditLogBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<AuditLog> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<AuditLog>();
            
        }
        public new IEnumerable<AuditLog> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<AuditLog>();
            
            
        }
    
        public new IEnumerable<AuditLog> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<AuditLog>();
            
        }
        public new IEnumerable<AuditLog> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<AuditLog>();
        }
        
        public new AuditLog FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (AuditLog)base.FindItem(query);
        }
        public new AuditLog FindItem(IQueryOver query)
        {
            return (AuditLog)base.FindItem(query);
        }
        
        IAuditLog IAuditLogFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new AuditLog CreateNewItem()
        {
            return (AuditLog)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<AuditLog, AuditLog> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<AuditLog>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.AuditLogModule.AuditLog GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.AuditLogModule.AuditLog) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.AuditLogModule.AuditLog> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.AuditLogModule.AuditLog>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.AuditLogModule.AuditLog> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.AuditLogModule.AuditLog>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.AuditLogModule.AuditLog> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.AuditLogModule.AuditLog>) base.FindAll(session);

       }
       public new PayMammoth.Modules.AuditLogModule.AuditLog FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.AuditLogModule.AuditLog) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.AuditLogModule.AuditLog FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.AuditLogModule.AuditLog) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.AuditLogModule.AuditLog FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.AuditLogModule.AuditLog) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.AuditLogModule.AuditLog> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.AuditLogModule.AuditLog>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.AuditLogModule.IAuditLog> Members
     
        PayMammoth.Modules.AuditLogModule.IAuditLog IBaseDbFactory<PayMammoth.Modules.AuditLogModule.IAuditLog>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.AuditLogModule.IAuditLog IBaseDbFactory<PayMammoth.Modules.AuditLogModule.IAuditLog>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.AuditLogModule.IAuditLog> IBaseDbFactory<PayMammoth.Modules.AuditLogModule.IAuditLog>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.AuditLogModule.IAuditLog> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.AuditLogModule.IAuditLog>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.AuditLogModule.IAuditLog> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.AuditLogModule.IAuditLog>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.AuditLogModule.IAuditLog IBaseDbFactory<PayMammoth.Modules.AuditLogModule.IAuditLog>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.AuditLogModule.IAuditLog BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.AuditLogModule.IAuditLog>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.AuditLogModule.IAuditLog BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.AuditLogModule.IAuditLog>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.AuditLogModule.IAuditLog> IBaseDbFactory<PayMammoth.Modules.AuditLogModule.IAuditLog>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IAuditLog> IAuditLogFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IAuditLog> IAuditLogFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IAuditLog> IAuditLogFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IAuditLog IAuditLogFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IAuditLog IAuditLogFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IAuditLog IAuditLogFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IAuditLog> IAuditLogFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IAuditLog IAuditLogFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
