using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Modules.PaymentRequestTransactionModule;
using PayMammoth.Modules.CultureDetailsModule;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DbObjects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.NHibernateClasses.NHManager;


//DbObjectBuilder v8

namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaymentRequestTransactionFactoryAutoGen : PayMammoth.Modules._AutoGen.PaymentRequestTransactionBaseFactory, 
    				PayMammoth.Modules._AutoGen.IPaymentRequestTransactionFactoryAutoGen,
    				BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<IPaymentRequestTransaction>
    
    {
    
     	public new PaymentRequestTransaction ReloadItemFromDatabase(PayMammoth.Modules._AutoGen.PaymentRequestTransactionBase item)
        {
         	return (PaymentRequestTransaction)base.ReloadItemFromDatabase(item);
        }
     	static PaymentRequestTransactionFactoryAutoGen()
        {
        	UsedInProject = true;
            
        }
        public PaymentRequestTransactionFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(PaymentRequestTransactionImpl);
			PayMammoth.Cms.PaymentRequestTransactionModule.PaymentRequestTransactionCmsFactory._initialiseStaticInstance();
        }
        public static new PaymentRequestTransactionFactory Instance
        {
            get
            {
                return (PaymentRequestTransactionFactory)PaymentRequestTransactionBaseFactory.Instance;
            }
      
      

        }
        
    
        public new IEnumerable<PaymentRequestTransaction> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<PaymentRequestTransaction>();
            
        }
        public new IEnumerable<PaymentRequestTransaction> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<PaymentRequestTransaction>();
            
            
        }
    
        public new IEnumerable<PaymentRequestTransaction> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<PaymentRequestTransaction>();
            
        }
        public new IEnumerable<PaymentRequestTransaction> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<PaymentRequestTransaction>();
        }
        
        public new PaymentRequestTransaction FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (PaymentRequestTransaction)base.FindItem(query);
        }
        public new PaymentRequestTransaction FindItem(IQueryOver query)
        {
            return (PaymentRequestTransaction)base.FindItem(query);
        }
        
        IPaymentRequestTransaction IPaymentRequestTransactionFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new PaymentRequestTransaction CreateNewItem()
        {
            return (PaymentRequestTransaction)base.CreateNewItem();
        }


        
    
        
		public new IQueryOver<PaymentRequestTransaction, PaymentRequestTransaction> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();
			ifSessionIsNullThrowError(session);
        	var q = session.QueryOver<PaymentRequestTransaction>();
            qParams.FillQueryOver(q, this);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null, LockMode lockMode = null)
        {
            return (PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction) base.GetByPrimaryKey(id, session, lockMode);
        }
       public new IEnumerable<PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction> FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction> FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction> FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction>) base.FindAll(session);

       }
       public new PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.PaymentRequestTransactionModule.PaymentRequestTransaction>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction> Members
     
        PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction IBaseDbFactory<PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction IBaseDbFactory<PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction>.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
           
       }
       IEnumerable<PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction> IBaseDbFactory<PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction>.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction>.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction> BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction>.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction IBaseDbFactory<PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction>.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction>.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction BusinessLogic_v3.Classes.DbObjects.Factories.IBaseDbFactory<PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction>.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction> IBaseDbFactory<PayMammoth.Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IPaymentRequestTransaction> IPaymentRequestTransactionFactoryAutoGen.FindAll(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IPaymentRequestTransaction> IPaymentRequestTransactionFactoryAutoGen.FindAll(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IPaymentRequestTransaction> IPaymentRequestTransactionFactoryAutoGen.FindAll(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IPaymentRequestTransaction IPaymentRequestTransactionFactoryAutoGen.FindItem(IQueryOver query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IPaymentRequestTransaction IPaymentRequestTransactionFactoryAutoGen.FindItem(ICriteria query, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IPaymentRequestTransaction IPaymentRequestTransactionFactoryAutoGen.FindItem(BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IPaymentRequestTransaction> IPaymentRequestTransactionFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IPaymentRequestTransaction IPaymentRequestTransactionFactoryAutoGen.GetByPrimaryKey(long id, BusinessLogic_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session, LockMode lockMode = null)
       {
           return this.GetByPrimaryKey(id, session, lockMode);
       }

	}
	
}
