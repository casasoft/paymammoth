using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;

using BusinessLogic_v3.Classes.DbObjects.Collections;
using NHibernate;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using System.Collections;         
using PayMammoth.Modules._AutoGen;                

//BaseClass.AutoGen
//DbObjectBuilder v8
namespace PayMammoth.Modules._AutoGen
{
	[BusinessLogic_v3.Classes.Attributes.DbObject(ClassTitle = "WebsiteAccount")]
    public abstract class WebsiteAccountBaseAutoGen : BusinessLogic_v3.Classes.DbObjects.Objects.BaseDbObject, 
       											  IWebsiteAccountBaseAutoGen,
											      BusinessLogic_v3.Classes.DbObjects.Objects.IBaseDbObject
											      
    
      
    {
    
    	protected override void initPropertiesForNewItem()
        {          
        

			var cultureInfo = __getCurrentCultureInfo(); //since this is a new item, it is imperative that the culture is preloaded
            
            
            base.initPropertiesForNewItem();
        }
    	
  
    	public PayMammoth.Modules._AutoGen.WebsiteAccount_CultureInfoBase GetCultureInfoByLanguage(CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 lang)
        {
            return (PayMammoth.Modules._AutoGen.WebsiteAccount_CultureInfoBase) __getCultureInfoByLanguage(lang);
        }

     	public static WebsiteAccountBaseFactory Factory
        {
            get
            {
                return WebsiteAccountBaseFactory.Instance; 
            }
        }    
        /*
        public static WebsiteAccountBase CreateNewItem()
        {
            return (WebsiteAccountBase)Factory.CreateNewItem();
        }

		public static WebsiteAccountBase GetByPrimaryKey(long pKey)
        {
            return Factory.GetByPrimaryKey(pKey);
            
        }
        
        public static IQueryOver<WebsiteAccountBase, WebsiteAccountBase> GetQuery(GetQueryParams qParams = null)
        {
        	return Factory.GetQuery(qParams);
        }
        public static WebsiteAccountBase FindItem(QueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static WebsiteAccountBase FindItem(IQueryOver query)
        {
            return Factory.FindItem(query);
        }
        public static IEnumerable<WebsiteAccountBase> FindAll()
        {
            return Factory.FindAll();

        }
        public static IEnumerable<WebsiteAccountBase> FindAll(QueryOver query)
        {
            return Factory.FindAll(query);
        }
         public static IEnumerable<WebsiteAccountBase> FindAll(IQueryOver query)
        {
            return Factory.FindAll(query);
        }
      */
        

// [base_properties]

		//BaseClass/Fields/Property_Normal
        private string _websitename;
        
        public virtual string WebsiteName 
        {
        	get
        	{
        		return _websitename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_websitename,value);
        		_websitename = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _code;
        
        public virtual string Code 
        {
        	get
        	{
        		return _code;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_code,value);
        		_code = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _allowedips;
        
        public virtual string AllowedIPs 
        {
        	get
        	{
        		return _allowedips;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_allowedips,value);
        		_allowedips = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _responseurl;
        
        public virtual string ResponseUrl 
        {
        	get
        	{
        		return _responseurl;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_responseurl,value);
        		_responseurl = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _secretword;
        
        public virtual string SecretWord 
        {
        	get
        	{
        		return _secretword;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_secretword,value);
        		_secretword = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private int _iterationstoderivehash;
        
        public virtual int IterationsToDeriveHash 
        {
        	get
        	{
        		return _iterationstoderivehash;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_iterationstoderivehash,value);
        		_iterationstoderivehash = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paypalsandboxusername;
        
        public virtual string PaypalSandboxUsername 
        {
        	get
        	{
        		return _paypalsandboxusername;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paypalsandboxusername,value);
        		_paypalsandboxusername = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paypalsandboxpassword;
        
        public virtual string PaypalSandboxPassword 
        {
        	get
        	{
        		return _paypalsandboxpassword;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paypalsandboxpassword,value);
        		_paypalsandboxpassword = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paypalliveusername;
        
        public virtual string PaypalLiveUsername 
        {
        	get
        	{
        		return _paypalliveusername;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paypalliveusername,value);
        		_paypalliveusername = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paypallivepassword;
        
        public virtual string PaypalLivePassword 
        {
        	get
        	{
        		return _paypallivepassword;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paypallivepassword,value);
        		_paypallivepassword = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _paypalenabled;
        
        public virtual bool PaypalEnabled 
        {
        	get
        	{
        		return _paypalenabled;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paypalenabled,value);
        		_paypalenabled = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _paypaluseliveenvironment;
        
        public virtual bool PaypalUseLiveEnvironment 
        {
        	get
        	{
        		return _paypaluseliveenvironment;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paypaluseliveenvironment,value);
        		_paypaluseliveenvironment = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paypallivesignature;
        
        public virtual string PaypalLiveSignature 
        {
        	get
        	{
        		return _paypallivesignature;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paypallivesignature,value);
        		_paypallivesignature = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paypalsandboxsignature;
        
        public virtual string PaypalSandboxSignature 
        {
        	get
        	{
        		return _paypalsandboxsignature;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paypalsandboxsignature,value);
        		_paypalsandboxsignature = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paypallivemerchantid;
        
        public virtual string PaypalLiveMerchantId 
        {
        	get
        	{
        		return _paypallivemerchantid;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paypallivemerchantid,value);
        		_paypallivemerchantid = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paypalsandboxmerchantid;
        
        public virtual string PaypalSandboxMerchantId 
        {
        	get
        	{
        		return _paypalsandboxmerchantid;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paypalsandboxmerchantid,value);
        		_paypalsandboxmerchantid = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paypallivemerchantemail;
        
        public virtual string PaypalLiveMerchantEmail 
        {
        	get
        	{
        		return _paypallivemerchantemail;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paypallivemerchantemail,value);
        		_paypallivemerchantemail = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paypalsandboxmerchantemail;
        
        public virtual string PaypalSandboxMerchantEmail 
        {
        	get
        	{
        		return _paypalsandboxmerchantemail;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paypalsandboxmerchantemail,value);
        		_paypalsandboxmerchantemail = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _chequeenabled;
        
        public virtual bool ChequeEnabled 
        {
        	get
        	{
        		return _chequeenabled;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_chequeenabled,value);
        		_chequeenabled = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _moneybookersenabled;
        
        public virtual bool MoneybookersEnabled 
        {
        	get
        	{
        		return _moneybookersenabled;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_moneybookersenabled,value);
        		_moneybookersenabled = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _netellerenabled;
        
        public virtual bool NetellerEnabled 
        {
        	get
        	{
        		return _netellerenabled;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_netellerenabled,value);
        		_netellerenabled = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _enablefakepayments;
        
        public virtual bool EnableFakePayments 
        {
        	get
        	{
        		return _enablefakepayments;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_enablefakepayments,value);
        		_enablefakepayments = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _chequeaddress1;
        
        public virtual string ChequeAddress1 
        {
        	get
        	{
        		return _chequeaddress1;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_chequeaddress1,value);
        		_chequeaddress1 = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _chequeaddress2;
        
        public virtual string ChequeAddress2 
        {
        	get
        	{
        		return _chequeaddress2;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_chequeaddress2,value);
        		_chequeaddress2 = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _chequeaddresscountry;
        
        public virtual string ChequeAddressCountry 
        {
        	get
        	{
        		return _chequeaddresscountry;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_chequeaddresscountry,value);
        		_chequeaddresscountry = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _chequeaddresspostcode;
        
        public virtual string ChequeAddressPostCode 
        {
        	get
        	{
        		return _chequeaddresspostcode;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_chequeaddresspostcode,value);
        		_chequeaddresspostcode = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _chequeaddresslocality;
        
        public virtual string ChequeAddressLocality 
        {
        	get
        	{
        		return _chequeaddresslocality;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_chequeaddresslocality,value);
        		_chequeaddresslocality = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _chequepayableto;
        
        public virtual string ChequePayableTo 
        {
        	get
        	{
        		return _chequepayableto;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_chequepayableto,value);
        		_chequepayableto = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paymentgatewaytransactiumliveusername;
        
        public virtual string PaymentGatewayTransactiumLiveUsername 
        {
        	get
        	{
        		return _paymentgatewaytransactiumliveusername;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgatewaytransactiumliveusername,value);
        		_paymentgatewaytransactiumliveusername = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paymentgatewaytransactiumlivepassword;
        
        public virtual string PaymentGatewayTransactiumLivePassword 
        {
        	get
        	{
        		return _paymentgatewaytransactiumlivepassword;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgatewaytransactiumlivepassword,value);
        		_paymentgatewaytransactiumlivepassword = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paymentgatewaytransactiumstagingusername;
        
        public virtual string PaymentGatewayTransactiumStagingUsername 
        {
        	get
        	{
        		return _paymentgatewaytransactiumstagingusername;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgatewaytransactiumstagingusername,value);
        		_paymentgatewaytransactiumstagingusername = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paymentgatewaytransactiumstagingpassword;
        
        public virtual string PaymentGatewayTransactiumStagingPassword 
        {
        	get
        	{
        		return _paymentgatewaytransactiumstagingpassword;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgatewaytransactiumstagingpassword,value);
        		_paymentgatewaytransactiumstagingpassword = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _paymentgatewaytransactiumuseliveenvironment;
        
        public virtual bool PaymentGatewayTransactiumUseLiveEnvironment 
        {
        	get
        	{
        		return _paymentgatewaytransactiumuseliveenvironment;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgatewaytransactiumuseliveenvironment,value);
        		_paymentgatewaytransactiumuseliveenvironment = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paymentgatewaytransactiumprofiletag;
        
        public virtual string PaymentGatewayTransactiumProfileTag 
        {
        	get
        	{
        		return _paymentgatewaytransactiumprofiletag;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgatewaytransactiumprofiletag,value);
        		_paymentgatewaytransactiumprofiletag = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paymentgatewaytransactiumbankstatementtext;
        
        public virtual string PaymentGatewayTransactiumBankStatementText 
        {
        	get
        	{
        		return _paymentgatewaytransactiumbankstatementtext;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgatewaytransactiumbankstatementtext,value);
        		_paymentgatewaytransactiumbankstatementtext = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _paymentgatewaytransactiumenabled;
        
        public virtual bool PaymentGatewayTransactiumEnabled 
        {
        	get
        	{
        		return _paymentgatewaytransactiumenabled;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgatewaytransactiumenabled,value);
        		_paymentgatewaytransactiumenabled = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _paymentgatewayapcoenabled;
        
        public virtual bool PaymentGatewayApcoEnabled 
        {
        	get
        	{
        		return _paymentgatewayapcoenabled;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgatewayapcoenabled,value);
        		_paymentgatewayapcoenabled = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paymentgatewayapcoliveprofileid;
        
        public virtual string PaymentGatewayApcoLiveProfileId 
        {
        	get
        	{
        		return _paymentgatewayapcoliveprofileid;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgatewayapcoliveprofileid,value);
        		_paymentgatewayapcoliveprofileid = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paymentgatewayapcolivesecretword;
        
        public virtual string PaymentGatewayApcoLiveSecretWord 
        {
        	get
        	{
        		return _paymentgatewayapcolivesecretword;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgatewayapcolivesecretword,value);
        		_paymentgatewayapcolivesecretword = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paymentgatewayapcobankstatementtext;
        
        public virtual string PaymentGatewayApcoBankStatementText 
        {
        	get
        	{
        		return _paymentgatewayapcobankstatementtext;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgatewayapcobankstatementtext,value);
        		_paymentgatewayapcobankstatementtext = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paymentgatewayapcostagingsecretword;
        
        public virtual string PaymentGatewayApcoStagingSecretWord 
        {
        	get
        	{
        		return _paymentgatewayapcostagingsecretword;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgatewayapcostagingsecretword,value);
        		_paymentgatewayapcostagingsecretword = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paymentgatewayapcostagingprofileid;
        
        public virtual string PaymentGatewayApcoStagingProfileId 
        {
        	get
        	{
        		return _paymentgatewayapcostagingprofileid;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgatewayapcostagingprofileid,value);
        		_paymentgatewayapcostagingprofileid = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _paymentgatewayapcouseliveenvironment;
        
        public virtual bool PaymentGatewayApcoUseLiveEnvironment 
        {
        	get
        	{
        		return _paymentgatewayapcouseliveenvironment;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgatewayapcouseliveenvironment,value);
        		_paymentgatewayapcouseliveenvironment = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _cssfilename;
        
        public virtual string CssFilename 
        {
        	get
        	{
        		return _cssfilename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_cssfilename,value);
        		_cssfilename = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _logofilename;
        
        public virtual string LogoFilename 
        {
        	get
        	{
        		return _logofilename;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_logofilename,value);
        		_logofilename = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _notifyclientsbyemailaboutpayment;
        
        public virtual bool NotifyClientsByEmailAboutPayment 
        {
        	get
        	{
        		return _notifyclientsbyemailaboutpayment;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_notifyclientsbyemailaboutpayment,value);
        		_notifyclientsbyemailaboutpayment = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _notificationemail;
        
        public virtual string NotificationEmail 
        {
        	get
        	{
        		return _notificationemail;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_notificationemail,value);
        		_notificationemail = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _contactemail;
        
        public virtual string ContactEmail 
        {
        	get
        	{
        		return _contactemail;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_contactemail,value);
        		_contactemail = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _contactname;
        
        public virtual string ContactName 
        {
        	get
        	{
        		return _contactname;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_contactname,value);
        		_contactname = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _paymentgatewayrealexenabled;
        
        public virtual bool PaymentGatewayRealexEnabled 
        {
        	get
        	{
        		return _paymentgatewayrealexenabled;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgatewayrealexenabled,value);
        		_paymentgatewayrealexenabled = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _paymentgatewayrealexuseliveenvironment;
        
        public virtual bool PaymentGatewayRealexUseLiveEnvironment 
        {
        	get
        	{
        		return _paymentgatewayrealexuseliveenvironment;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgatewayrealexuseliveenvironment,value);
        		_paymentgatewayrealexuseliveenvironment = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paymentgatewayrealexmerchantid;
        
        public virtual string PaymentGatewayRealexMerchantId 
        {
        	get
        	{
        		return _paymentgatewayrealexmerchantid;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgatewayrealexmerchantid,value);
        		_paymentgatewayrealexmerchantid = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paymentgatewayrealexliveaccountname;
        
        public virtual string PaymentGatewayRealexLiveAccountName 
        {
        	get
        	{
        		return _paymentgatewayrealexliveaccountname;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgatewayrealexliveaccountname,value);
        		_paymentgatewayrealexliveaccountname = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paymentgatewayrealexstagingaccountname;
        
        public virtual string PaymentGatewayRealexStagingAccountName 
        {
        	get
        	{
        		return _paymentgatewayrealexstagingaccountname;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgatewayrealexstagingaccountname,value);
        		_paymentgatewayrealexstagingaccountname = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _paymentgatewayrealexsecretword;
        
        public virtual string PaymentGatewayRealexSecretWord 
        {
        	get
        	{
        		return _paymentgatewayrealexsecretword;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgatewayrealexsecretword,value);
        		_paymentgatewayrealexsecretword = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _disableresponseurlnotifications;
        
        public virtual bool DisableResponseUrlNotifications 
        {
        	get
        	{
        		return _disableresponseurlnotifications;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_disableresponseurlnotifications,value);
        		_disableresponseurlnotifications = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private bool _waituntilpaymentnotificationacknowledgedbeforeredirecttosuccess;
        
        public virtual bool WaitUntilPaymentNotificationAcknowledgedBeforeRedirectToSuccess 
        {
        	get
        	{
        		return _waituntilpaymentnotificationacknowledgedbeforeredirecttosuccess;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_waituntilpaymentnotificationacknowledgedbeforeredirecttosuccess,value);
        		_waituntilpaymentnotificationacknowledgedbeforeredirecttosuccess = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string PaymentGatewayTransactiumDescription
        {
            get
            {
                var cultureInfo = (PayMammoth.Modules._AutoGen.WebsiteAccount_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.PaymentGatewayTransactiumDescription;
                
            }
            set
            {
            
                
                var cultureInfo = (PayMammoth.Modules._AutoGen.WebsiteAccount_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__PaymentGatewayTransactiumDescription_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.PaymentGatewayTransactiumDescription,value);
                cultureInfo.PaymentGatewayTransactiumDescription = value;
            }
        }
        
        private string _paymentgatewaytransactiumdescription;
        public virtual string __PaymentGatewayTransactiumDescription_cultureBase
        {
        	get
        	{
        		return _paymentgatewaytransactiumdescription;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgatewaytransactiumdescription,value);
        		
        		_paymentgatewaytransactiumdescription = value;
        	}
        }
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string PaymentGatewayApcoDescription
        {
            get
            {
                var cultureInfo = (PayMammoth.Modules._AutoGen.WebsiteAccount_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.PaymentGatewayApcoDescription;
                
            }
            set
            {
            
                
                var cultureInfo = (PayMammoth.Modules._AutoGen.WebsiteAccount_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__PaymentGatewayApcoDescription_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.PaymentGatewayApcoDescription,value);
                cultureInfo.PaymentGatewayApcoDescription = value;
            }
        }
        
        private string _paymentgatewayapcodescription;
        public virtual string __PaymentGatewayApcoDescription_cultureBase
        {
        	get
        	{
        		return _paymentgatewayapcodescription;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgatewayapcodescription,value);
        		
        		_paymentgatewayapcodescription = value;
        	}
        }
		//BaseClass/Fields/Property_Multilingual
		[BusinessLogic_v3.Classes.Attributes.Multilingual]
	    public virtual string PaymentGatewayRealexDescription
        {
            get
            {
                var cultureInfo = (PayMammoth.Modules._AutoGen.WebsiteAccount_CultureInfoBase)__getCurrentCultureInfo();
                return cultureInfo.PaymentGatewayRealexDescription;
                
            }
            set
            {
            
                
                var cultureInfo = (PayMammoth.Modules._AutoGen.WebsiteAccount_CultureInfoBase)__getCurrentCultureInfo();
                if (cultureInfo.CultureInfo.ID == BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetDefaultCulture().ID)
                {
                	__PaymentGatewayRealexDescription_cultureBase = value;
                }
                
                checkPropertyUpdateAndUpdateDirtyFlag(cultureInfo.PaymentGatewayRealexDescription,value);
                cultureInfo.PaymentGatewayRealexDescription = value;
            }
        }
        
        private string _paymentgatewayrealexdescription;
        public virtual string __PaymentGatewayRealexDescription_cultureBase
        {
        	get
        	{
        		return _paymentgatewayrealexdescription;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgatewayrealexdescription,value);
        		
        		_paymentgatewayrealexdescription = value;
        	}
        }
		//BaseClass/Fields/Property_Normal
        private string _paymentgatewayrealexstatementtext;
        
        public virtual string PaymentGatewayRealexStatementText 
        {
        	get
        	{
        		return _paymentgatewayrealexstatementtext;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_paymentgatewayrealexstatementtext,value);
        		_paymentgatewayrealexstatementtext = value;
        		
        	}
        }
        
		//BaseClass/Fields/Property_Normal
        private string _fakepaymentkey;
        
        public virtual string FakePaymentKey 
        {
        	get
        	{
        		return _fakepaymentkey;
        	}
        	set
        	{
        		checkPropertyUpdateAndUpdateDirtyFlag(_fakepaymentkey,value);
        		_fakepaymentkey = value;
        		
        	}
        }
        
 //check Property_Pkey, etc


// [base_collections]


    

#region Multilingual (ContentInfo)



#endregion       

		

    }
}
