using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.ContentTextModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class ContentTextMap_AutoGen : ContentTextMap_AutoGen_Z
    {
        public ContentTextMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.Name, NameMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Name_Search, Name_SearchMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Identifier, IdentifierMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.Parent, ParentMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.AllowAddSubItems_AccessRequired, AllowAddSubItems_AccessRequiredMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.VisibleInCMS_AccessRequired, VisibleInCMS_AccessRequiredMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.Content, ContentMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ContentType, ContentTypeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.UsedInProject, UsedInProjectMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ImageAlternateText, ImageAlternateTextMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.CustomContentTags, CustomContentTagsMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ProcessContentUsingNVelocity, ProcessContentUsingNVelocityMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item._Computed_IsHtml, _Computed_IsHtmlMappingInfo);



// [userclassmap_initcollections]

//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.ChildContentTexts, ChildContentTextsMappingInfo, BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
//ClassMap_Collection_OneToManyInit              
              addCollection(x => x.ContentText_CultureInfos, ContentText_CultureInfosMappingInfo, BusinessLogic_v3.Enums.NHIBERNATE_COLLECTION_RELATIONSHIP.OneToMany);
            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class ContentTextMap_AutoGen_Z : BusinessLogic_v3.Modules.ContentTextModule.ContentTextBaseMap<ContentTextImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
