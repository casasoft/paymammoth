using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.PaymentMethodModule;
using PayMammoth.Cms.PaymentMethodModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaymentMethodCmsFactory_AutoGen : PayMammoth.Modules._AutoGen.PaymentMethodBaseCmsFactory
    {
        public static new PaymentMethodBaseCmsFactory Instance 
        {
         	get { return (PaymentMethodBaseCmsFactory)PayMammoth.Modules._AutoGen.PaymentMethodBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override PaymentMethodBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	PaymentMethod item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = PaymentMethod.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        PaymentMethodCmsInfo cmsItem = new PaymentMethodCmsInfo(item);
            return cmsItem;
        }    
        
        public override PaymentMethodBaseCmsInfo CreateCmsItemInfo(PayMammoth.Modules._AutoGen.PaymentMethodBase item)
        {
            return new PaymentMethodCmsInfo((PaymentMethod)item);
            
        }

    }
}
