using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.ContentTextModule;
using BusinessLogic_v3.Modules.ContentTextModule;

//DbObjectBuilder v8

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{    

	
    public abstract class ContentText_AutoGen : ContentTextBase, PayMammoth.Modules._AutoGen.IContentTextAutoGen
	                 
      , IMultilingualItem
      
      
      
    {
        
            

    
    
    
		

#region Multilingual
        public PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo GetCurrentCultureInfo()
        {
            return (PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo)__getCurrentCultureInfo();
        }             
          
    	public new PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo GetCultureInfoByLanguage(CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 lang)
        {
            return (PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo)base.GetCultureInfoByLanguage(lang);
        }

#endregion
        
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.ContentTextModule.ContentText Parent
        {
            get
            {
                return (PayMammoth.Modules.ContentTextModule.ContentText)base.Parent;
            }
            set
            {
                base.Parent = value;
            }
        }

		PayMammoth.Modules.ContentTextModule.IContentText IContentTextAutoGen.Parent
        {
            get
            {
            	return this.Parent;
                
            }
            set
            {
                this.Parent = (PayMammoth.Modules.ContentTextModule.ContentText)value;
            }
        }



// [userclass_collections_nhibernate]

        //UserClass_CollectionOneToManyLeftSide
        
		new internal class __ChildContentTextsCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.ContentTextModule.ContentText, 
        	PayMammoth.Modules.ContentTextModule.ContentText,
        	PayMammoth.Modules.ContentTextModule.IContentText>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.ContentTextModule.ContentText, PayMammoth.Modules.ContentTextModule.ContentText>
        {
            private PayMammoth.Modules.ContentTextModule.ContentText _item = null;
            public __ChildContentTextsCollectionInfo(PayMammoth.Modules.ContentTextModule.ContentText item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.ContentTextModule.ContentText> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.ContentTextModule.ContentText>)_item.__collection__ChildContentTexts; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.ContentTextModule.ContentText, PayMammoth.Modules.ContentTextModule.ContentText>.SetLinkOnItem(PayMammoth.Modules.ContentTextModule.ContentText item, PayMammoth.Modules.ContentTextModule.ContentText value)
            {
                item.Parent = value;
            }
        }
        
        private __ChildContentTextsCollectionInfo _ChildContentTexts = null;
        internal new __ChildContentTextsCollectionInfo ChildContentTexts
        {
            get
            {
                if (_ChildContentTexts == null)
                    _ChildContentTexts = new __ChildContentTextsCollectionInfo((PayMammoth.Modules.ContentTextModule.ContentText)this);
                return _ChildContentTexts;
            }
        }               
           
		ICollectionManager<PayMammoth.Modules.ContentTextModule.IContentText> IContentTextAutoGen.ChildContentTexts
        {
            get {  return this.ChildContentTexts; }
        }
           
        //UserClass_CollectionOneToManyLeftSide
        
		new internal class __ContentText_CultureInfosCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.ContentTextModule.ContentText, 
        	PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo,
        	PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.ContentTextModule.ContentText, PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo>
        {
            private PayMammoth.Modules.ContentTextModule.ContentText _item = null;
            public __ContentText_CultureInfosCollectionInfo(PayMammoth.Modules.ContentTextModule.ContentText item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo>)_item.__collection__ContentText_CultureInfos; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.ContentTextModule.ContentText, PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo>.SetLinkOnItem(PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo item, PayMammoth.Modules.ContentTextModule.ContentText value)
            {
                item.ContentText = value;
            }
        }
        
        private __ContentText_CultureInfosCollectionInfo _ContentText_CultureInfos = null;
        internal new __ContentText_CultureInfosCollectionInfo ContentText_CultureInfos
        {
            get
            {
                if (_ContentText_CultureInfos == null)
                    _ContentText_CultureInfos = new __ContentText_CultureInfosCollectionInfo((PayMammoth.Modules.ContentTextModule.ContentText)this);
                return _ContentText_CultureInfos;
            }
        }               
           
		ICollectionManager<PayMammoth.Modules.ContentText_CultureInfoModule.IContentText_CultureInfo> IContentTextAutoGen.ContentText_CultureInfos
        {
            get {  return this.ContentText_CultureInfos; }
        }
           

        
          
		public  static new ContentTextFactory Factory
        {
            get
            {
                return (ContentTextFactory)ContentTextBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<ContentText, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


        public override void __CheckItemCultureInfos(bool autoSaveInDB, IEnumerable<BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase> CultureInfos = null)
        {
        	BusinessLogic_v3.Classes.NHibernateClasses.Transactions.MyTransaction t = null;
            if (autoSaveInDB)
                t = beginTransaction();
            MultilingualChecker multilingualChecker = new MultilingualChecker();
            multilingualChecker.ItemToAdd+=new MultilingualChecker.ItemToAddHandler(multilingualChecker_ItemToAdd);
            multilingualChecker.ItemRemoved += new MultilingualChecker.ItemRemovedHandler(multilingualChecker_ItemRemoved);
            multilingualChecker.CheckItemCultureInfos(this, CultureInfos);
            if (autoSaveInDB)
            {
                this.Save();
                t.Commit();
                t.Dispose();
            }
        }

 		protected override IMultilingualContentInfo __createCultureInfoForCulture(long cultureID)
        {

            var cultureDetails = BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetByPrimaryKey(cultureID);
            IMultilingualContentInfo result = null;
            if (cultureDetails != null)
            {
                var itemCultureInfo = this.ContentText_CultureInfos.CreateNewItem();
                result = itemCultureInfo;

//                itemCultureInfo.ContentText = (ContentText)this;  //item link
            	itemCultureInfo.CultureInfo = (PayMammoth.Modules.CultureDetailsModule.CultureDetails)cultureDetails; //culture
	

// [userfactory_checkcultureinfos_multilingualfields]

                        itemCultureInfo.Content = this.__Content_cultureBase;

                        itemCultureInfo.ImageAlternateText = this.__ImageAlternateText_cultureBase;


	
				itemCultureInfo.MarkAsNotTemporary();
//				this.ContentText_CultureInfos.Add((PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo) itemCultureInfo);


            }
            return result;
        }


        private void multilingualChecker_ItemToAdd(BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase cultureDetails)
        {
            var itemCultureInfo = PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo.Factory.CreateNewItem();

            itemCultureInfo.ContentText = (ContentText)this;  //item link
            itemCultureInfo.CultureInfo = (PayMammoth.Modules.CultureDetailsModule.CultureDetails)cultureDetails; //culture


// [userfactory_checkcultureinfos_multilingualfields]

                        itemCultureInfo.Content = this.__Content_cultureBase;

                        itemCultureInfo.ImageAlternateText = this.__ImageAlternateText_cultureBase;



			itemCultureInfo.MarkAsNotTemporary();
			this.ContentText_CultureInfos.Add((PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo) itemCultureInfo);
            
            

        }

		private void multilingualChecker_ItemRemoved(IMultilingualContentInfo item)
        {   
        	this.ContentText_CultureInfos.Remove((PayMammoth.Modules.ContentText_CultureInfoModule.ContentText_CultureInfo)item);
            
        }

        
        
         #region IMultilingualItem Members

        IEnumerable<IMultilingualContentInfo> IMultilingualItem.GetMultilingualContentInfos()
        {
            return this.ContentText_CultureInfos;
            
        }

        protected override IEnumerable<IMultilingualContentInfo> __getMultilingualContentInfos()
        {
            return this.ContentText_CultureInfos;
        }
        
		#endregion    

     
        
        


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
