using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using CS.General_v3.Classes.DbObjects.Collections;
using CS.General_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules._AutoGen;

//BaseClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
	
    public interface IPaymentRequestItemBaseAutoGen : BusinessLogic_v3.Classes.DB.IBaseDbObject
    {
    

// [interface_base_properties]

		//Iproperty_normal
        int Quantity { get; set; }
		//Iproperty_normal
        double PriceExcTax { get; set; }
		//Iproperty_normal
        double TaxAmount { get; set; }
		//Iproperty_normal
        double ShippingAmount { get; set; }
		//Iproperty_normal
        double HandlingAmount { get; set; }
		//Iproperty_normal
        double Discount { get; set; }
		//Iproperty_normal
        string Title { get; set; }
		//Iproperty_normal
        string Description { get; set; }
		//Iproperty_normal
        double WidthInMm { get; set; }
		//Iproperty_normal
        double HeightInMm { get; set; }
		//Iproperty_normal
        double LengthInMm { get; set; }
		//IProperty_LinkedToObject
        PayMammoth.Modules._AutoGen.IPaymentRequestBase PaymentRequest {get; set; }

   

// [interface_base_collections]

 


    	
    	
      
      

    }
}
