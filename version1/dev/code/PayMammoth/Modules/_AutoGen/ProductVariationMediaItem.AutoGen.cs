using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using CS.General_v3.Classes.DbObjects.Collections;
using CS.General_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.ProductVariationMediaItemModule;
using BusinessLogic_v3.Modules.ProductVariationMediaItemModule;


//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
    public abstract class ProductVariationMediaItem_AutoGen : ProductVariationMediaItemBase, PayMammoth.Modules._AutoGen.IProductVariationMediaItemAutoGen
	
      
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.ProductVariationModule.ProductVariation ProductVariation
        {
            get
            {
                return (PayMammoth.Modules.ProductVariationModule.ProductVariation)base.ProductVariation;
            }
            set
            {
                base.ProductVariation = value;
            }
        }

		PayMammoth.Modules.ProductVariationModule.IProductVariation IProductVariationMediaItemAutoGen.ProductVariation
        {
            get
            {
            	return this.ProductVariation;
                
            }
            set
            {
                this.ProductVariation = (PayMammoth.Modules.ProductVariationModule.ProductVariation)value;
            }
        }



// [userclass_collections_nhibernate]


        
          
		public  static new ProductVariationMediaItemFactory Factory
        {
            get
            {
                return (ProductVariationMediaItemFactory)ProductVariationMediaItemBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<ProductVariationMediaItem, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
