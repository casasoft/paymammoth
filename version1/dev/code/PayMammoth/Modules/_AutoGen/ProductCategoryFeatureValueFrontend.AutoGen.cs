using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.ProductCategoryFeatureValueModule;
using PayMammoth.Modules.ProductCategoryFeatureValueModule;
using BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueFrontend ToFrontend(this PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue item)
        {
        	return PayMammoth.Frontend.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class ProductCategoryFeatureValueFrontend_AutoGen : BusinessLogic_v3.Frontend.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBaseFrontend

    {
		
        
        protected ProductCategoryFeatureValueFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue Data
        {
            get { return (PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue)base.Data; }
            set { base.Data = value; }

        }

        public new static ProductCategoryFeatureValueFrontend CreateNewItem()
        {
            return (ProductCategoryFeatureValueFrontend)BusinessLogic_v3.Frontend.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBaseFrontend.CreateNewItem();
        }
        
        public new static ProductCategoryFeatureValueFrontend Get(BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValueBase data)
        {
            return (ProductCategoryFeatureValueFrontend)BusinessLogic_v3.Frontend.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBaseFrontend.Get(data);
        }
        public new static List<ProductCategoryFeatureValueFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValueBase> dataList)
        {
            return BusinessLogic_v3.Frontend.ProductCategoryFeatureValueModule.ProductCategoryFeatureValueBaseFrontend.GetList(dataList).Cast<ProductCategoryFeatureValueFrontend>().ToList();
        }


    }
}
