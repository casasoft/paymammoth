using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using CS.General_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.ProductModule;
using PayMammoth.Modules.ProductModule;

//IUserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
	
    public interface IProductAutoGen : BusinessLogic_v3.Modules.ProductModule.IProductBase
    {




// [interface_userclass_properties]

		//IProperty_LinkedToObject
        new PayMammoth.Modules.BrandModule.IBrand Brand {get; set; }

   
    

// [interface_userclassonly_properties]

   

                                     

// [interface_userclass_collections_nhibernate]

        //UserClass_CollectionOneToManyLeftSide
        
		new ICollectionManager<PayMammoth.Modules.Product_CultureInfoModule.IProduct_CultureInfo> Product_CultureInfos { get; }

        //UserClass_CollectionOneToManyLeftSide
        
		new ICollectionManager<PayMammoth.Modules.ProductCategoryModule.IProductCategory> CategoryLinks { get; }

        //UserClass_CollectionOneToManyLeftSide
        
		new ICollectionManager<PayMammoth.Modules.ProductCategoryFeatureValueModule.IProductCategoryFeatureValue> ProductCategoryFeatureValues { get; }

        //UserClass_CollectionOneToManyLeftSide
        
		new ICollectionManager<PayMammoth.Modules.ProductCategorySpecificationValueModule.IProductCategorySpecificationValue> ProductCategorySpecificationValues { get; }

        //UserClass_CollectionOneToManyLeftSide
        
		new ICollectionManager<PayMammoth.Modules.ProductVariationModule.IProductVariation> ProductVariations { get; }




// [interface_userclassonly_collections]

 


    	
    	
      
      

    }
}
