using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.ProductVariationModule;
using PayMammoth.Modules.ProductVariationModule;
using BusinessLogic_v3.Modules.ProductVariationModule;
                          
namespace PayMammoth.Extensions
{
    public static partial class _AutoGen_UserFrontendExtensions
    {
        public static IEnumerable<PayMammoth.Frontend.ProductVariationModule.ProductVariationFrontend> ToFrontendList(this IEnumerable<PayMammoth.Modules.ProductVariationModule.IProductVariation> list)
        {
            return list.ToList().ConvertAll(ToFrontend);
        }

        public static PayMammoth.Frontend.ProductVariationModule.ProductVariationFrontend ToFrontend(this PayMammoth.Modules.ProductVariationModule.IProductVariation item)
        {
        	return PayMammoth.Frontend.ProductVariationModule.ProductVariationFrontend.Get(item);
        }
    }
}


namespace PayMammoth.Modules._AutoGen
{
    public abstract class ProductVariationFrontend_AutoGen : BusinessLogic_v3.Frontend.ProductVariationModule.ProductVariationBaseFrontend

    {
		
        
        protected ProductVariationFrontend_AutoGen ()
            : base()
        {

        }

        public new PayMammoth.Modules.ProductVariationModule.IProductVariation Data
        {
            get { return (PayMammoth.Modules.ProductVariationModule.IProductVariation)base.Data; }
            set { base.Data = value; }

        }

        public new static ProductVariationFrontend CreateNewItem()
        {
            return (ProductVariationFrontend)BusinessLogic_v3.Frontend.ProductVariationModule.ProductVariationBaseFrontend.CreateNewItem();
        }
        
        public new static ProductVariationFrontend Get(BusinessLogic_v3.Modules.ProductVariationModule.IProductVariationBase data)
        {
            return (ProductVariationFrontend)BusinessLogic_v3.Frontend.ProductVariationModule.ProductVariationBaseFrontend.Get(data);
        }
        public new static List<ProductVariationFrontend> GetList(IEnumerable<BusinessLogic_v3.Modules.ProductVariationModule.IProductVariationBase> dataList)
        {
            return BusinessLogic_v3.Frontend.ProductVariationModule.ProductVariationBaseFrontend.GetList(dataList).Cast<ProductVariationFrontend>().ToList();
        }


    }
}
