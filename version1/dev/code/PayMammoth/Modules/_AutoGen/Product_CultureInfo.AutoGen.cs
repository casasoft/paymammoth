using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using CS.General_v3.Classes.DbObjects.Collections;
using CS.General_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.Product_CultureInfoModule;
using BusinessLogic_v3.Modules.Product_CultureInfoModule;


//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{
    public abstract class Product_CultureInfo_AutoGen : Product_CultureInfoBase, PayMammoth.Modules._AutoGen.IProduct_CultureInfoAutoGen
	
      
      
      , IMultilingualContentInfo
      
    {
        
            

    
    
    
		
        



// [userclass_linkedproperties]

		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.CultureDetailsModule.CultureDetails CultureInfo
        {
            get
            {
                return (PayMammoth.Modules.CultureDetailsModule.CultureDetails)base.CultureInfo;
            }
            set
            {
                base.CultureInfo = value;
            }
        }

		PayMammoth.Modules.CultureDetailsModule.ICultureDetails IProduct_CultureInfoAutoGen.CultureInfo
        {
            get
            {
            	return this.CultureInfo;
                
            }
            set
            {
                this.CultureInfo = (PayMammoth.Modules.CultureDetailsModule.CultureDetails)value;
            }
        }
		//UserClass/Fields/Property_LinkedObject
        public new virtual PayMammoth.Modules.ProductModule.Product Product
        {
            get
            {
                return (PayMammoth.Modules.ProductModule.Product)base.Product;
            }
            set
            {
                base.Product = value;
            }
        }

		PayMammoth.Modules.ProductModule.IProduct IProduct_CultureInfoAutoGen.Product
        {
            get
            {
            	return this.Product;
                
            }
            set
            {
                this.Product = (PayMammoth.Modules.ProductModule.Product)value;
            }
        }



// [userclass_collections_nhibernate]


        
          
		public  static new Product_CultureInfoFactory Factory
        {
            get
            {
                return (Product_CultureInfoFactory)Product_CultureInfoBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<Product_CultureInfo, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        
        
        
    }
    
}
