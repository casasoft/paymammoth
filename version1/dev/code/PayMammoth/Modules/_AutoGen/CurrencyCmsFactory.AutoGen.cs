using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.CurrencyModule;
using PayMammoth.Cms.CurrencyModule;
using CS.WebComponentsGeneralV3.Cms.CurrencyModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class CurrencyCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.CurrencyModule.CurrencyBaseCmsFactory
    {
        public static new CurrencyBaseCmsFactory Instance 
        {
         	get { return (CurrencyBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.CurrencyModule.CurrencyBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override CurrencyBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	Currency item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = Currency.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        CurrencyCmsInfo cmsItem = new CurrencyCmsInfo(item);
            return cmsItem;
        }    
        
        public override CurrencyBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase item)
        {
            return new CurrencyCmsInfo((Currency)item);
            
        }

    }
}
