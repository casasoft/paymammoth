using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using Iesi.Collections.Generic;
using BusinessLogic_v3.Classes.DbObjects.Collections;
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using PayMammoth.Modules.WebsiteAccountModule;
using PayMammoth.Modules._AutoGen;

//DbObjectBuilder v8

//UserClass.AutoGen

namespace PayMammoth.Modules._AutoGen
{    

	
    public abstract class WebsiteAccount_AutoGen : WebsiteAccountBase, PayMammoth.Modules._AutoGen.IWebsiteAccountAutoGen
	                 
      , IMultilingualItem
      
      
      
    {
        
            

    
    
    
		

#region Multilingual
        public PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo GetCurrentCultureInfo()
        {
            return (PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo)__getCurrentCultureInfo();
        }             
          
    	public new PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo GetCultureInfoByLanguage(CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639 lang)
        {
            return (PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo)base.GetCultureInfoByLanguage(lang);
        }

#endregion
        
        



// [userclass_linkedproperties]




// [userclass_collections_nhibernate]


        
          
		public  static new WebsiteAccountFactory Factory
        {
            get
            {
                return (WebsiteAccountFactory)WebsiteAccountBase.Factory;
            }
        }  
		
        public  virtual object getPreviousStateValue<TValue>(System.Linq.Expressions.Expression<Func<WebsiteAccount, TValue>> val )
        {                                            
        	return CS.General_v3.Util.ReflectionUtil.GetPropertyBySelector(val);
            
        }
      

#region Multilingual
          


        public override void __CheckItemCultureInfos(bool autoSaveInDB, IEnumerable<BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBase> CultureInfos = null)
        {
        	BusinessLogic_v3.Classes.NHibernateClasses.Transactions.MyTransaction t = null;
            if (autoSaveInDB)
                t = beginTransaction();
            MultilingualChecker multilingualChecker = new MultilingualChecker();
            multilingualChecker.ItemToAdd+=new MultilingualChecker.ItemToAddHandler(multilingualChecker_ItemToAdd);
            multilingualChecker.ItemRemoved += new MultilingualChecker.ItemRemovedHandler(multilingualChecker_ItemRemoved);
            multilingualChecker.CheckItemCultureInfos(this, CultureInfos);
            if (autoSaveInDB)
            {
                this.Save();
                t.Commit();
                t.Dispose();
            }
        }

 		protected override IMultilingualContentInfo __createCultureInfoForCulture(long cultureID)
        {

            var cultureDetails = BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseFactory.Instance.GetByPrimaryKey(cultureID);
            IMultilingualContentInfo result = null;
            if (cultureDetails != null)
            {
                var itemCultureInfo = this.WebsiteAccount_CultureInfos.CreateNewItem();
                result = itemCultureInfo;

//                itemCultureInfo.WebsiteAccount = (WebsiteAccount)this;  //item link
            	itemCultureInfo.CultureInfo = (PayMammoth.Modules.CultureDetailsModule.CultureDetails)cultureDetails; //culture
	

// [userfactory_checkcultureinfos_multilingualfields]

                        itemCultureInfo.PaymentGatewayTransactiumDescription = this.__PaymentGatewayTransactiumDescription_cultureBase;

                        itemCultureInfo.PaymentGatewayApcoDescription = this.__PaymentGatewayApcoDescription_cultureBase;

                        itemCultureInfo.PaymentGatewayRealexDescription = this.__PaymentGatewayRealexDescription_cultureBase;


	
				itemCultureInfo.MarkAsNotTemporary();
//				this.WebsiteAccount_CultureInfos.Add((PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo) itemCultureInfo);


            }
            return result;
        }


        private void multilingualChecker_ItemToAdd(BusinessLogic_v3.Modules.CultureDetailsModule.ICultureDetailsBase cultureDetails)
        {
            var itemCultureInfo = PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo.Factory.CreateNewItem();

            itemCultureInfo.WebsiteAccount = (WebsiteAccount)this;  //item link
            itemCultureInfo.CultureInfo = (PayMammoth.Modules.CultureDetailsModule.CultureDetails)cultureDetails; //culture


// [userfactory_checkcultureinfos_multilingualfields]

                        itemCultureInfo.PaymentGatewayTransactiumDescription = this.__PaymentGatewayTransactiumDescription_cultureBase;

                        itemCultureInfo.PaymentGatewayApcoDescription = this.__PaymentGatewayApcoDescription_cultureBase;

                        itemCultureInfo.PaymentGatewayRealexDescription = this.__PaymentGatewayRealexDescription_cultureBase;



			itemCultureInfo.MarkAsNotTemporary();
			this.WebsiteAccount_CultureInfos.Add((PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo) itemCultureInfo);
            
            

        }

		private void multilingualChecker_ItemRemoved(IMultilingualContentInfo item)
        {   
        	this.WebsiteAccount_CultureInfos.Remove((PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo)item);
            
        }

        
        
         #region IMultilingualItem Members

        IEnumerable<IMultilingualContentInfo> IMultilingualItem.GetMultilingualContentInfos()
        {
            return this.WebsiteAccount_CultureInfos;
            
        }

        protected override IEnumerable<IMultilingualContentInfo> __getMultilingualContentInfos()
        {
            return this.WebsiteAccount_CultureInfos;
        }
        
		#endregion    

     
        
        


#endregion    
    

        
        

// [userclassonly_properties]




// [userclassonly_collections]

        //UserClassOnly_CollectionOneToManyLeftSide
        
        
        public class __PaymentRequestsCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount, 
        	PayMammoth.Modules.PaymentRequestModule.PaymentRequest,
        	PayMammoth.Modules.PaymentRequestModule.IPaymentRequest>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount, PayMammoth.Modules.PaymentRequestModule.PaymentRequest>
        {
            private PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount _item = null;
            public __PaymentRequestsCollectionInfo(PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.PaymentRequestModule.PaymentRequest> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>)_item.__collection__PaymentRequests; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount, PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.SetLinkOnItem(PayMammoth.Modules.PaymentRequestModule.PaymentRequest item, PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount value)
            {
                item.WebsiteAccount = value;
            }
        }
        
        private __PaymentRequestsCollectionInfo _PaymentRequests = null;
        public __PaymentRequestsCollectionInfo PaymentRequests
        {
            get
            {
                if (_PaymentRequests == null)
                    _PaymentRequests = new __PaymentRequestsCollectionInfo((PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount)this);
                return _PaymentRequests;
            }
        }

   
		ICollectionManager<PayMammoth.Modules.PaymentRequestModule.IPaymentRequest> IWebsiteAccountAutoGen.PaymentRequests
        {
            get {  return this.PaymentRequests; }
        }
   
        protected virtual IEnumerable<PayMammoth.Modules._AutoGen.PaymentRequestBase> __collection__PaymentRequests
        {
            get { throw new InvalidOperationException("This must be defined in the final implementation"); }
        }
        //UserClassOnly_CollectionOneToManyLeftSide
        
        
        public class __WebsiteAccount_CultureInfosCollectionInfo : DbCollectionOneToManyManager<
        	PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount, 
        	PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo,
        	PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfo>,
            IDbCollectionOneToManyInfo<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount, PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo>
        {
            private PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount _item = null;
            public __WebsiteAccount_CultureInfosCollectionInfo(PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount item) : base(item)
            {
                this._item = item;
                this.setCollectionInfo(this);
            }

            
            public IEnumerable<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo> Collection
            {
                get { return (IEnumerable<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo>)_item.__collection__WebsiteAccount_CultureInfos; }
            }

            void IDbCollectionOneToManyInfo<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount, PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo>.SetLinkOnItem(PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo item, PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount value)
            {
                item.WebsiteAccount = value;
            }
        }
        
        private __WebsiteAccount_CultureInfosCollectionInfo _WebsiteAccount_CultureInfos = null;
        public __WebsiteAccount_CultureInfosCollectionInfo WebsiteAccount_CultureInfos
        {
            get
            {
                if (_WebsiteAccount_CultureInfos == null)
                    _WebsiteAccount_CultureInfos = new __WebsiteAccount_CultureInfosCollectionInfo((PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount)this);
                return _WebsiteAccount_CultureInfos;
            }
        }

   
		ICollectionManager<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.IWebsiteAccount_CultureInfo> IWebsiteAccountAutoGen.WebsiteAccount_CultureInfos
        {
            get {  return this.WebsiteAccount_CultureInfos; }
        }
   
        protected virtual IEnumerable<PayMammoth.Modules._AutoGen.WebsiteAccount_CultureInfoBase> __collection__WebsiteAccount_CultureInfos
        {
            get { throw new InvalidOperationException("This must be defined in the final implementation"); }
        }
        
        
        
    }
    
}
