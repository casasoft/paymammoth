using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using CS.General_v3.Classes.NHibernateClasses.Session;
using CS.General_v3.Classes.NHibernateClasses;

using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.CmsAuditLogModule;
using PayMammoth.Modules.CmsAuditLogModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface ICmsAuditLogFactoryAutoGen : BusinessLogic_v3.Modules.CmsAuditLogModule.ICmsAuditLogBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<ICmsAuditLog>
    
    {
	    new PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog CreateNewItem();
        new PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.CmsAuditLogModule.ICmsAuditLog> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
