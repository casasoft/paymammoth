using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.NHibernateClasses.Mappings;
using PayMammoth.Modules.CultureDetailsModule;


namespace PayMammoth.Modules._AutoGen
{
    
    public abstract class CultureDetailsMap_AutoGen : CultureDetailsMap_AutoGen_Z
    {
        public CultureDetailsMap_AutoGen()
        {
           
        }
        
        protected override void initMappings()
        {
            
            

// [userclassmap_initproperties]


			//ClassMap_PropertyInit
             addProperty(item=>item.Title, TitleMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.LanguageISOCode, LanguageISOCodeMappingInfo);
			 //UserClassMap/ClassMap_PropertyInit_LinkedObject
             addManyToOneCollection(x => x.DefaultCurrency, DefaultCurrencyMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.SpecificCountryCode, SpecificCountryCodeMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.ScriptSuffix, ScriptSuffixMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.IsDefaultCulture, IsDefaultCultureMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.BaseUrlRegex, BaseUrlRegexMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.BaseRedirectionUrl, BaseRedirectionUrlMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.GoogleAnalyticsRootDomain, GoogleAnalyticsRootDomainMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.GoogleAnalyticsID, GoogleAnalyticsIDMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.BaseRedirectionUrlLocalhost, BaseRedirectionUrlLocalhostMappingInfo);

			//ClassMap_PropertyInit
             addProperty(item=>item.BaseUrlRegexLocalhost, BaseUrlRegexLocalhostMappingInfo);



// [userclassmap_initcollections]

            
            
          
            
            
            base.initMappings();
            

		 

        }
        
        
        
        
        
    }
    
    public abstract class CultureDetailsMap_AutoGen_Z : BusinessLogic_v3.Modules.CultureDetailsModule.CultureDetailsBaseMap<CultureDetailsImpl>
    {


// [userclassmap_properties]




// [userclassmap_collections]

            
        
        
    }
}
