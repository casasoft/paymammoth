using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.Article_CultureInfoModule;
using PayMammoth.Cms.Article_CultureInfoModule;
using CS.WebComponentsGeneralV3.Cms.Article_CultureInfoModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class Article_CultureInfoCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.Article_CultureInfoModule.Article_CultureInfoBaseCmsFactory
    {
        public static new Article_CultureInfoBaseCmsFactory Instance 
        {
         	get { return (Article_CultureInfoBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.Article_CultureInfoModule.Article_CultureInfoBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override Article_CultureInfoBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	Article_CultureInfo item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = Article_CultureInfo.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        Article_CultureInfoCmsInfo cmsItem = new Article_CultureInfoCmsInfo(item);
            return cmsItem;
        }    
        
        public override Article_CultureInfoBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase item)
        {
            return new Article_CultureInfoCmsInfo((Article_CultureInfo)item);
            
        }

    }
}
