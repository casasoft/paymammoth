using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.DbObjects;
using BusinessLogic_v3.Modules.MemberModule;
using PayMammoth.Modules.MemberModule;
using PayMammoth.Modules.CultureDetailsModule;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.DB;
using BusinessLogic_v3.Classes.Multilingual;
using NHibernate;
using CS.General_v3.Classes.NHibernateClasses;
using CS.General_v3.Classes.NHibernateClasses.NHManager;




namespace PayMammoth.Modules._AutoGen
{
    public abstract class MemberFactoryAutoGen : BusinessLogic_v3.Modules.MemberModule.MemberBaseFactory, 
    				PayMammoth.Modules._AutoGen.IMemberFactoryAutoGen,
    				CS.General_v3.Classes.DbObjects.IBaseDbFactory<IMember>
    
    {
    
     	public new Member ReloadItemFromDatabase(BusinessLogic_v3.Modules.MemberModule.MemberBase item)
        {
         	return (Member)base.ReloadItemFromDatabase(item);
        }
     	
        public MemberFactoryAutoGen ()
        {

			_mainTypeCreated = typeof(MemberImpl);
			PayMammoth.Cms.MemberModule.MemberCmsFactory._initialiseStaticInstance();
        }
        public static new MemberFactory Instance
        {
            get
            {
                return (MemberFactory)MemberBaseFactory.Instance;
            }
      
      

        }
        
        /*
        public new IEnumerable<Member> FindAll()
        {
            return base.FindAll().Cast<Member>();
        }*/
    /*    public new IEnumerable<Member> FindAll(IQuery query)
        {
            
            return base.FindAll(query).Cast<Member>();

        }*/
        public new IEnumerable<Member> FindAll(IQueryOver query)
        {
            return base.FindAll(query).Cast<Member>();
            
        }
        public new IEnumerable<Member> FindAll(IDetachedQuery query)
        {
            return base.FindAll(query).Cast<Member>();
            
            
        }
     /*  public new IEnumerable<Member> FindAll(ICriteria crit)
        {
            return base.FindAll(crit).Cast<Member>();


        }*/
        public new IEnumerable<Member> FindAll(NHibernate.Criterion.QueryOver query)
        {
            return base.FindAll(query).Cast<Member>();
            
        }
        public new IEnumerable<Member> FindAll(NHibernate.Criterion.DetachedCriteria crit)
        {
            return base.FindAll(crit).Cast<Member>();
        }
        
        public new Member FindItem(NHibernate.Criterion.QueryOver query)
        {
            return (Member)base.FindItem(query);
        }
        public new Member FindItem(IQueryOver query)
        {
            return (Member)base.FindItem(query);
        }
        
        IMember IMemberFactoryAutoGen.CreateNewItem()
        {
            return CreateNewItem();
        }
        public new Member CreateNewItem()
        {
            return (Member)base.CreateNewItem();
        }

        public new Member GetByPrimaryKey(long pKey)
        {
            return (Member)base.GetByPrimaryKey(pKey);
        }
        
        
    
        
		public new IQueryOver<Member, Member> GetQuery(GetQueryParams qParams = null)
        {
        	if (qParams == null) qParams = new GetQueryParams();
            var session = qParams.GetSessionOrDefault();

        	var q = session.QueryOver<Member>();
            qParams.FillQueryOver(q);
            postGetQueryOver(q, qParams);
                
            return q;
        
        
        }
        
        
        
      
      
        
        

        
#region Multilingual



        /// <summary>
        /// Checks for any language infos that do not exist or extra ones not existing any more
        /// </summary>
        public override void CheckCultureInfos()
        {

  

        }        


#endregion
        public new PayMammoth.Modules.MemberModule.Member GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
        {
            return (PayMammoth.Modules.MemberModule.Member) base.GetByPrimaryKey(id, session);
        }
       public new IEnumerable<PayMammoth.Modules.MemberModule.Member> FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.MemberModule.Member>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.MemberModule.Member> FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.MemberModule.Member>) base.FindAll(query, session);

       }
       public new IEnumerable<PayMammoth.Modules.MemberModule.Member> FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.MemberModule.Member>) base.FindAll(session);

       }
       public new PayMammoth.Modules.MemberModule.Member FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.MemberModule.Member) base.FindItem(query, session);

       }
       public new PayMammoth.Modules.MemberModule.Member FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.MemberModule.Member) base.FindItem(query, session);

       }

       public new PayMammoth.Modules.MemberModule.Member FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (PayMammoth.Modules.MemberModule.Member) base.FindItem(session);

       }
       public new IEnumerable<PayMammoth.Modules.MemberModule.Member> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session = null)
       {
           return (IEnumerable<PayMammoth.Modules.MemberModule.Member>) base.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);

       }

 		#region IBaseDbFactory<PayMammoth.Modules.MemberModule.IMember> Members
     
        PayMammoth.Modules.MemberModule.IMember IBaseDbFactory<PayMammoth.Modules.MemberModule.IMember>.CreateNewItem()
       {
           return this.CreateNewItem();
           
       }

       PayMammoth.Modules.MemberModule.IMember IBaseDbFactory<PayMammoth.Modules.MemberModule.IMember>.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
           
       }
       IEnumerable<PayMammoth.Modules.MemberModule.IMember> IBaseDbFactory<PayMammoth.Modules.MemberModule.IMember>.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }


       IEnumerable<PayMammoth.Modules.MemberModule.IMember> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.MemberModule.IMember>.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<PayMammoth.Modules.MemberModule.IMember> CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.MemberModule.IMember>.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }
       PayMammoth.Modules.MemberModule.IMember IBaseDbFactory<PayMammoth.Modules.MemberModule.IMember>.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.MemberModule.IMember CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.MemberModule.IMember>.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }
       PayMammoth.Modules.MemberModule.IMember CS.General_v3.Classes.DbObjects.IBaseDbFactory<PayMammoth.Modules.MemberModule.IMember>.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }
       IEnumerable<PayMammoth.Modules.MemberModule.IMember> IBaseDbFactory<PayMammoth.Modules.MemberModule.IMember>.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }

       #endregion
       
       IEnumerable<IMember> IMemberFactoryAutoGen.FindAll(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IMember> IMemberFactoryAutoGen.FindAll(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(query, session);
           
       }

       IEnumerable<IMember> IMemberFactoryAutoGen.FindAll(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindAll(session);
           
       }

       IMember IMemberFactoryAutoGen.FindItem(IQueryOver query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IMember IMemberFactoryAutoGen.FindItem(ICriteria query, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(query, session);
           
       }

       IMember IMemberFactoryAutoGen.FindItem(CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.FindItem(session);
           
       }

       IEnumerable<IMember> IMemberFactoryAutoGen.GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKeys(ids, loadAsSeperateQueries, session);
           
       }
       
       IMember IMemberFactoryAutoGen.GetByPrimaryKey(long id, CS.General_v3.Classes.NHibernateClasses.Session.MyNHSessionBase session)
       {
           return this.GetByPrimaryKey(id, session);
       }

	}
	
}
