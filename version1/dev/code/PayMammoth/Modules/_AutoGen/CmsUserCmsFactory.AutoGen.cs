using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.CmsUserModule;
using PayMammoth.Cms.CmsUserModule;
using CS.WebComponentsGeneralV3.Cms.CmsUserModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class CmsUserCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.CmsUserModule.CmsUserBaseCmsFactory
    {
        public static new CmsUserBaseCmsFactory Instance 
        {
         	get { return (CmsUserBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.CmsUserModule.CmsUserBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override CmsUserBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	CmsUser item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = CmsUser.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        CmsUserCmsInfo cmsItem = new CmsUserCmsInfo(item);
            return cmsItem;
        }    
        
        public override CmsUserBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.CmsUserModule.CmsUserBase item)
        {
            return new CmsUserCmsInfo((CmsUser)item);
            
        }

    }
}
