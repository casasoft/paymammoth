using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using PayMammoth.Modules.PaypalSettingsModule;
using PayMammoth.Cms.PaypalSettingsModule;
using PayMammoth.Modules._AutoGen;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class PaypalSettingsCmsFactory_AutoGen : PayMammoth.Modules._AutoGen.PaypalSettingsBaseCmsFactory
    {
        public static new PaypalSettingsBaseCmsFactory Instance 
        {
         	get { return (PaypalSettingsBaseCmsFactory)PayMammoth.Modules._AutoGen.PaypalSettingsBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override PaypalSettingsBaseCmsInfo _createNewItem(bool isTemporary)
        {
            var item = PaypalSettings.Factory.CreateNewItem();
            if (item.HasTemporaryFields())
            {
            	if (isTemporary)
                {
                    item.MarkAsTemporary();
                }
                item.Create(new SaveParams(savePermanently: false));
            }
            PaypalSettingsCmsInfo cmsItem = new PaypalSettingsCmsInfo(item);
            return cmsItem;
        }    
        
        public override PaypalSettingsBaseCmsInfo CreateCmsItemInfo(PayMammoth.Modules._AutoGen.PaypalSettingsBase item)
        {
            return new PaypalSettingsCmsInfo((PaypalSettings)item);
            
        }

    }
}
