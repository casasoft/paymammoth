using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Extensions;
using PayMammoth.Modules.WebsiteAccountModule;
using PayMammoth.Modules._AutoGen;
using PayMammoth.Frontend.WebsiteAccountModule;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Collections;
namespace PayMammoth.Modules._AutoGen
{
    public class WebsiteAccountCmsInfo_AutoGen : PayMammoth.Modules._AutoGen.WebsiteAccountBaseCmsInfo
    {

		public WebsiteAccountCmsInfo_AutoGen(PayMammoth.Modules._AutoGen.WebsiteAccountBase item)
            : base(item)
        {

        }
        

        private WebsiteAccountFrontend _m_frontendItem = null;
        protected override BusinessLogic_v3.Frontend._Base.IBaseFrontend _frontendItem
        {
            get
            {
                if (_m_frontendItem == null)
                    _m_frontendItem = WebsiteAccountFrontend.Get(this.DbItem);
                return _m_frontendItem;
            }
            
        }

        public new WebsiteAccountFrontend FrontendItem
        {
            get { return (WebsiteAccountFrontend) base.FrontendItem; }
        }
        
        
		public new WebsiteAccount DbItem
        {
            get
            {
                return (WebsiteAccount)base.DbItem;
            }
        }




// [usercmsinfo_fieldsdeclarations]



// [usercmsinfo_collectiondeclarations]

        public CmsCollectionInfo PaymentRequests { get; protected set; }
        

        public CmsCollectionInfo WebsiteAccount_CultureInfos { get; protected set; }
        



		protected override void initBasicFields()
		{
		

// [usercmsinfo_initbasicfields]

        this.WebsiteName = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.WebsiteName),
        						isEditable: true, isVisible: true);

        this.Code = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.Code),
        						isEditable: true, isVisible: true);

        this.AllowedIPs = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.AllowedIPs),
        						isEditable: true, isVisible: true);

        this.ResponseUrl = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.ResponseUrl),
        						isEditable: true, isVisible: true);

        this.SecretWord = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.SecretWord),
        						isEditable: true, isVisible: true);

        this.IterationsToDeriveHash = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.IterationsToDeriveHash),
        						isEditable: true, isVisible: true);

        this.PaypalSandboxUsername = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaypalSandboxUsername),
        						isEditable: true, isVisible: true);

        this.PaypalSandboxPassword = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaypalSandboxPassword),
        						isEditable: true, isVisible: true);

        this.PaypalLiveUsername = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaypalLiveUsername),
        						isEditable: true, isVisible: true);

        this.PaypalLivePassword = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaypalLivePassword),
        						isEditable: true, isVisible: true);

        this.PaypalEnabled = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaypalEnabled),
        						isEditable: true, isVisible: true);

        this.PaypalUseLiveEnvironment = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaypalUseLiveEnvironment),
        						isEditable: true, isVisible: true);

        this.PaypalLiveSignature = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaypalLiveSignature),
        						isEditable: true, isVisible: true);

        this.PaypalSandboxSignature = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaypalSandboxSignature),
        						isEditable: true, isVisible: true);

        this.PaypalLiveMerchantId = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaypalLiveMerchantId),
        						isEditable: true, isVisible: true);

        this.PaypalSandboxMerchantId = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaypalSandboxMerchantId),
        						isEditable: true, isVisible: true);

        this.PaypalLiveMerchantEmail = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaypalLiveMerchantEmail),
        						isEditable: true, isVisible: true);

        this.PaypalSandboxMerchantEmail = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaypalSandboxMerchantEmail),
        						isEditable: true, isVisible: true);

        this.ChequeEnabled = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.ChequeEnabled),
        						isEditable: true, isVisible: true);

        this.MoneybookersEnabled = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.MoneybookersEnabled),
        						isEditable: true, isVisible: true);

        this.NetellerEnabled = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.NetellerEnabled),
        						isEditable: true, isVisible: true);

        this.EnableFakePayments = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.EnableFakePayments),
        						isEditable: true, isVisible: true);

        this.ChequeAddress1 = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.ChequeAddress1),
        						isEditable: true, isVisible: true);

        this.ChequeAddress2 = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.ChequeAddress2),
        						isEditable: true, isVisible: true);

        this.ChequeAddressCountry = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.ChequeAddressCountry),
        						isEditable: true, isVisible: true);

        this.ChequeAddressPostCode = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.ChequeAddressPostCode),
        						isEditable: true, isVisible: true);

        this.ChequeAddressLocality = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.ChequeAddressLocality),
        						isEditable: true, isVisible: true);

        this.ChequePayableTo = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.ChequePayableTo),
        						isEditable: true, isVisible: true);

        this.PaymentGatewayTransactiumLiveUsername = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaymentGatewayTransactiumLiveUsername),
        						isEditable: true, isVisible: true);

        this.PaymentGatewayTransactiumLivePassword = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaymentGatewayTransactiumLivePassword),
        						isEditable: true, isVisible: true);

        this.PaymentGatewayTransactiumStagingUsername = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaymentGatewayTransactiumStagingUsername),
        						isEditable: true, isVisible: true);

        this.PaymentGatewayTransactiumStagingPassword = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaymentGatewayTransactiumStagingPassword),
        						isEditable: true, isVisible: true);

        this.PaymentGatewayTransactiumUseLiveEnvironment = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaymentGatewayTransactiumUseLiveEnvironment),
        						isEditable: true, isVisible: true);

        this.PaymentGatewayTransactiumProfileTag = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaymentGatewayTransactiumProfileTag),
        						isEditable: true, isVisible: true);

        this.PaymentGatewayTransactiumBankStatementText = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaymentGatewayTransactiumBankStatementText),
        						isEditable: true, isVisible: true);

        this.PaymentGatewayTransactiumEnabled = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaymentGatewayTransactiumEnabled),
        						isEditable: true, isVisible: true);

        this.PaymentGatewayApcoEnabled = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaymentGatewayApcoEnabled),
        						isEditable: true, isVisible: true);

        this.PaymentGatewayApcoLiveProfileId = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaymentGatewayApcoLiveProfileId),
        						isEditable: true, isVisible: true);

        this.PaymentGatewayApcoLiveSecretWord = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaymentGatewayApcoLiveSecretWord),
        						isEditable: true, isVisible: true);

        this.PaymentGatewayApcoBankStatementText = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaymentGatewayApcoBankStatementText),
        						isEditable: true, isVisible: true);

        this.PaymentGatewayApcoStagingSecretWord = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaymentGatewayApcoStagingSecretWord),
        						isEditable: true, isVisible: true);

        this.PaymentGatewayApcoStagingProfileId = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaymentGatewayApcoStagingProfileId),
        						isEditable: true, isVisible: true);

        this.PaymentGatewayApcoUseLiveEnvironment = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaymentGatewayApcoUseLiveEnvironment),
        						isEditable: true, isVisible: true);

        this.CssFilename = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.CssFilename),
        						isEditable: false, isVisible: false);

        this.LogoFilename = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.LogoFilename),
        						isEditable: false, isVisible: false);

        this.NotifyClientsByEmailAboutPayment = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.NotifyClientsByEmailAboutPayment),
        						isEditable: true, isVisible: true);

        this.NotificationEmail = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.NotificationEmail),
        						isEditable: true, isVisible: true);

        this.ContactEmail = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.ContactEmail),
        						isEditable: true, isVisible: true);

        this.ContactName = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.ContactName),
        						isEditable: true, isVisible: true);

        this.PaymentGatewayRealexEnabled = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaymentGatewayRealexEnabled),
        						isEditable: true, isVisible: true);

        this.PaymentGatewayRealexUseLiveEnvironment = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaymentGatewayRealexUseLiveEnvironment),
        						isEditable: true, isVisible: true);

        this.PaymentGatewayRealexMerchantId = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaymentGatewayRealexMerchantId),
        						isEditable: true, isVisible: true);

        this.PaymentGatewayRealexLiveAccountName = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaymentGatewayRealexLiveAccountName),
        						isEditable: true, isVisible: true);

        this.PaymentGatewayRealexStagingAccountName = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaymentGatewayRealexStagingAccountName),
        						isEditable: true, isVisible: true);

        this.PaymentGatewayRealexSecretWord = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaymentGatewayRealexSecretWord),
        						isEditable: true, isVisible: true);

        this.DisableResponseUrlNotifications = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.DisableResponseUrlNotifications),
        						isEditable: true, isVisible: true);

        this.WaitUntilPaymentNotificationAcknowledgedBeforeRedirectToSuccess = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.WaitUntilPaymentNotificationAcknowledgedBeforeRedirectToSuccess),
        						isEditable: true, isVisible: true);

        this.PaymentGatewayTransactiumDescription = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaymentGatewayTransactiumDescription),
        						isEditable: true, isVisible: true);

        this.PaymentGatewayApcoDescription = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaymentGatewayApcoDescription),
        						isEditable: true, isVisible: true);

        this.PaymentGatewayRealexDescription = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaymentGatewayRealexDescription),
        						isEditable: true, isVisible: true);

        this.PaymentGatewayRealexStatementText = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.PaymentGatewayRealexStatementText),
        						isEditable: true, isVisible: true);

        this.FakePaymentKey = base.AddProperty( 
        CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector( item => item.FakePaymentKey),
        						isEditable: true, isVisible: true);

	

// [usercmsinfo_initcollections]

		
		//InitCollectionUserOneToMany
		this.PaymentRequests = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector(item => item.PaymentRequests),
						CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.PaymentRequestModule.PaymentRequest>.GetPropertyBySelector(item => item.WebsiteAccount)));
		
		//InitCollectionUserOneToMany
		this.WebsiteAccount_CultureInfos = this.AddProperty(new CmsCollectionInfo(this,
						CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccountModule.WebsiteAccount>.GetPropertyBySelector(item => item.WebsiteAccount_CultureInfos),
						CS.General_v3.Util.ReflectionUtil<PayMammoth.Modules.WebsiteAccount_CultureInfoModule.WebsiteAccount_CultureInfo>.GetPropertyBySelector(item => item.WebsiteAccount)));


			base.initBasicFields();
		}
    
    }
}
