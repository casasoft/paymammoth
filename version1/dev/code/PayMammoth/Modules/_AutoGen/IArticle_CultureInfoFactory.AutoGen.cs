using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using BusinessLogic_v3.Classes.DbObjects.Factories;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.Article_CultureInfoModule;
using PayMammoth.Modules.Article_CultureInfoModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IArticle_CultureInfoFactoryAutoGen : BusinessLogic_v3.Modules.Article_CultureInfoModule.IArticle_CultureInfoBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IArticle_CultureInfo>
    
    {
	    new PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo CreateNewItem();
        new PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo GetByPrimaryKey(long id, MyNHSessionBase session = null, LockMode lockMode = null);
        new IEnumerable<PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.Article_CultureInfoModule.IArticle_CultureInfo> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
