using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

using PayMammoth.Modules.EmailText_CultureInfoModule;
using PayMammoth.Cms.EmailText_CultureInfoModule;
using CS.WebComponentsGeneralV3.Cms.EmailText_CultureInfoModule;

namespace PayMammoth.Modules._AutoGen
{
    public abstract class EmailText_CultureInfoCmsFactory_AutoGen : CS.WebComponentsGeneralV3.Cms.EmailText_CultureInfoModule.EmailText_CultureInfoBaseCmsFactory
    {
        public static new EmailText_CultureInfoBaseCmsFactory Instance 
        {
         	get { return (EmailText_CultureInfoBaseCmsFactory)CS.WebComponentsGeneralV3.Cms.EmailText_CultureInfoModule.EmailText_CultureInfoBaseCmsFactory.Instance; }
        }                                                                                           
        
        
        
        protected override EmailText_CultureInfoBaseCmsInfo _createNewItem(bool isTemporary)
        {
        	EmailText_CultureInfo item = null;
            using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
            	item = EmailText_CultureInfo.Factory.CreateNewItem();
	            if (item.HasTemporaryFields())
	            {
	            	if (isTemporary)
	                {
	                    item.MarkAsTemporary();
	                }
	                item.Create(new SaveParams(savePermanently: false));
	            }
	            t.Commit();
            }
	        EmailText_CultureInfoCmsInfo cmsItem = new EmailText_CultureInfoCmsInfo(item);
            return cmsItem;
        }    
        
        public override EmailText_CultureInfoBaseCmsInfo CreateCmsItemInfo(BusinessLogic_v3.Modules.EmailText_CultureInfoModule.EmailText_CultureInfoBase item)
        {
            return new EmailText_CultureInfoCmsInfo((EmailText_CultureInfo)item);
            
        }

    }
}
