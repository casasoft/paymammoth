using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using PayMammoth.Modules._AutoGen;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace PayMammoth.Modules._AutoGen
{

//BaseCmsFactory-Class

    public abstract class PaymentRequestTransactionBaseCmsFactory : PayMammoth.Modules._AutoGen.PaymentRequestTransactionBaseCmsFactory_AutoGen
    {
       

        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
        	//any user-specific information, add them to 'cmsInfo'.

            base.getUserSpecificGeneralInfo(cmsInfo);
        }

    }
}
