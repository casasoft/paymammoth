using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.Brand_CultureInfoModule;
using PayMammoth.Modules.Brand_CultureInfoModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IBrand_CultureInfoFactoryAutoGen : BusinessLogic_v3.Modules.Brand_CultureInfoModule.IBrand_CultureInfoBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IBrand_CultureInfo>
    
    {
	    new PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo CreateNewItem();
        new PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.Brand_CultureInfoModule.IBrand_CultureInfo> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
