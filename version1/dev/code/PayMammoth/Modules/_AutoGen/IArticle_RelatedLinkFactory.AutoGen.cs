using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Criterion;
using BusinessLogic_v3.Classes.DB;
using NHibernate;                             
using BusinessLogic_v3.Classes.NHibernateClasses.Session;
using BusinessLogic_v3.Classes.NHibernateClasses;

using BusinessLogic_v3.Classes.DbObjects.Parameters;
using BusinessLogic_v3.Modules.Article_RelatedLinkModule;
using PayMammoth.Modules.Article_RelatedLinkModule;


namespace PayMammoth.Modules._AutoGen
{
//IUserFactory.AutoGen-File
    public interface IArticle_RelatedLinkFactoryAutoGen : BusinessLogic_v3.Modules.Article_RelatedLinkModule.IArticle_RelatedLinkBaseFactory
    
    //, CS.General_v3.Classes.DbObjects.IBaseDbFactory<IArticle_RelatedLink>
    
    {
	    new PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink CreateNewItem();
        new PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink GetByPrimaryKey(long id, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink> FindAll(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink> FindAll(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink> FindAll(MyNHSessionBase session = null);
        new PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink FindItem(NHibernate.IQueryOver query, MyNHSessionBase session = null);
        new PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink FindItem(NHibernate.ICriteria query, MyNHSessionBase session = null);
        new PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink FindItem(MyNHSessionBase session = null);
        new IEnumerable<PayMammoth.Modules.Article_RelatedLinkModule.IArticle_RelatedLink> GetByPrimaryKeys(IEnumerable<long> ids, bool loadAsSeperateQueries, MyNHSessionBase session = null);
    }
}
