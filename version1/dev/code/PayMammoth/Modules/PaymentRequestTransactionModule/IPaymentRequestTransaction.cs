using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Exceptions;
using CS.General_v3.Classes.HelperClasses;
using BusinessLogic_v3.Classes.NHibernateClasses;
using CS.General_v3.URL;
using CS.General_v3.Util;
using CS.General_v3.Extensions;
using PayMammoth.Modules._AutoGen;


namespace PayMammoth.Modules.PaymentRequestTransactionModule
{

//IUserClass-File
	
    public interface IPaymentRequestTransaction : PayMammoth.Modules._AutoGen.IPaymentRequestTransactionAutoGen
    {
        void MarkAsFakePayment();
        void MarkAsSuccessful(bool requiresManualPayment, string authCode = null);
        void SetPaymentParametersFromObject<T>(T item, string prependKey, params System.Linq.Expressions.Expression<Func<T, object>>[] propertySelectors);
        void SetPaymentParameters(QueryString qs);
        void SetPaymentParameters(params string[] nameValuePairs);

        QueryString GetParametersAsQueryString();

        void AddMessage(string msg, CS.General_v3.Enums.STATUS_MSG_TYPE msgType, bool autoSave = true);
    }
}
