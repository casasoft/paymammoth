using CS.General_v3.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using CS.General_v3.Extensions;
using CS.General_v3.Util;
using PayMammoth.Connector.Classes.Interfaces;
using log4net;
using PayMammoth.Util;


namespace PayMammoth.Frontend.PaymentRequestModule
{
    public class PaymentRequestFrontend : PayMammoth.Modules._AutoGen.PaymentRequestFrontend_AutoGen
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(PaymentRequestFrontend));
        protected PaymentRequestFrontend()
            : base()
        {

        }


        public void RedirectToChoicePage()
        {
            CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass(Urls.Url_Payment_Choice);
            url[PayMammoth.Connector.Constants.PARAM_IDENTIFIER] = this.Data.Identifier;
            url.RedirectTo();

        }
        public string GetSuccessUrl(bool waitUntilRequestNotificationReceivedIfNecessary = true)
        {
            if (waitUntilRequestNotificationReceivedIfNecessary)
            {
                waitUntilAcknowledgedIfNecessary();
            }
            else
            {
                if (_log.IsDebugEnabled) _log.Debug(LogUtil.AddRequestIdentifierToLogMsg( this.Data, "No wait necessary for request notification acknowledgement"));
            }

            CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass(((IPaymentRequestDetails)this.Data).ReturnUrlSuccess);
            url[PayMammoth.Connector.Constants.PARAM_IDENTIFIER] = this.Data.Identifier;
            
            return url.ToString();
        }

        /// <summary>
        /// This will wait until the 'Notification' sent on the 'ResponseUrl' of the account is acknowledged, or until the timeout is received.
        /// </summary>
        /// <returns>Whether it was acknowledged successfully or not</returns>
        private bool waitUntilAcknowledgedIfNecessary()
        {
            if (_log.IsDebugEnabled) _log.Debug(LogUtil.AddRequestIdentifierToLogMsg(this.Data,"Check if waiting for notification acknowledgement is required"));
            
            bool ok = true;
            if (this.Data.WebsiteAccount.WaitUntilPaymentNotificationAcknowledgedBeforeRedirectToSuccess)
            {
                ok = false;
                //this means that the account requires to wait until response acknoweledged, before sending to success page
                if (_log.IsDebugEnabled) _log.Debug(LogUtil.AddRequestIdentifierToLogMsg(this.Data,"Waiting until payment notification is acknowledged"));
                
                ok = this.Data.WaitUntilPaymentNotificationAcknowledgementReceived();

                if (ok)
                {
                    this.ReloadDataFromDatabase();
                    if (_log.IsDebugEnabled) _log.Debug(LogUtil.AddRequestIdentifierToLogMsg(this.Data, "Payment notification is acknowledged"));
                }
                else
                {
                    if (_log.IsWarnEnabled) _log.Warn(LogUtil.AddRequestIdentifierToLogMsg(this.Data,"Payment notification could not be acknowledged!"));
                }

            }
            else
            {
                if (_log.IsDebugEnabled) _log.Debug(LogUtil.AddRequestIdentifierToLogMsg( this.Data, "Payment notification acknowledgement NOT required."));
            }
            return ok;
        }

        public bool RedirectToSuccessUrl()
        {
            
            if (_log.IsDebugEnabled) _log.Debug(LogUtil.AddRequestIdentifierToLogMsg( this.Data, "Redirecting to success url"));
            bool ok = waitUntilAcknowledgedIfNecessary();
            
            
            string url = GetSuccessUrl();
            CS.General_v3.Util.PageUtil.RedirectPage(url);
            
            return ok;
        }
        public bool RedirectToSuccessUrlUsingJavaScriptInParent(System.Web.UI.Page pg)
        {
            bool ok = waitUntilAcknowledgedIfNecessary();
            
            {
                string url = GetSuccessUrl();
                CS.General_v3.Util.PageUtil.RedirectParentUsingJavaScript(pg, url);
            }
            return ok;

        }
        public bool RedirectToSuccessUrlUsingJavaScriptInPage(System.Web.UI.Page pg)
        {
            bool ok = waitUntilAcknowledgedIfNecessary();
            
            {
                string url = GetSuccessUrl();
                CS.General_v3.Util.PageUtil.RedirectPageUsingJavaScript(pg, url);
            }
            return ok;

        }
        public void RedirectToFailureUrl()
        {
            CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass(((IPaymentRequestDetails)this.Data).ReturnUrlFailure);
            url[PayMammoth.Connector.Constants.PARAM_IDENTIFIER] = this.Data.Identifier;

            url.RedirectTo();
        }
    }
}
