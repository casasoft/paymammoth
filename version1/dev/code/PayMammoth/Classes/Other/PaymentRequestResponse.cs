﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Modules.PaymentRequestModule;
using CS.General_v3.Classes.HelperClasses;

namespace PayMammoth.Classes.Other
{
    public class PaymentRequestResponse
    {
        public PaymentRequest Request { get; set; }
        public Enums.GenerateResponseStatus Status { get; set; }
        
        public PaymentRequestResponse()
        {
            this.Status = Enums.GenerateResponseStatus.Ok;
        }
    }
}
