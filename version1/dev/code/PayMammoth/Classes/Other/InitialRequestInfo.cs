﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using CS.General_v3.Classes.Serialization;
using PayMammoth.Classes.Interfaces;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.Modules.WebsiteAccountModule;
using PayMammoth.Connector.Classes.Interfaces;
using log4net;
using CS.General_v3.URL;
using PayMammoth.Util;
using PayMammoth.Connector.Classes.Requests;
using PayMammoth.Connector.Classes.Serialization;

namespace PayMammoth.Classes.Other
{
    public class InitialRequestInfo : BaseSerializable<IInitialRequestDetails>, IInitialRequestDetails
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(InitialRequestInfo));
        public InitialRequestInfo()
        {
            this.CurrencyCode = CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.EuroMemberCountriesEuro;
        }

        public void LoadFromQuerystring()
        {
            base.loadFromQuerystring<IInitialRequestDetails>();
        }
        public bool LoadFromNameValueCollection(NameValueCollection nv)
        {
            base.loadFromNameValueCollection<IInitialRequestDetails>(nv);
            bool ok = VerifyRequirements(throwError: false);
            return ok;
        }

        public string WebsiteAccountCode { get; set; }
        public double PriceExcTax { get; set; }
        public double TaxAmount{ get; set; }
        


        public double HandlingAmount { get; set; }
        public double ShippingAmount { get; set; }
        public string ClientFirstName { get; set; }
        public string ClientMiddleName { get; set; }
        public string ClientLastName { get; set; }
        public string ClientAddress1{ get; set; }
        public string ClientAddress2 { get; set; }
        public string ClientAddress3 { get; set; }
        public string ClientLocality { get; set; }
        public CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? ClientCountry { get; set; }
        public string ClientPostCode{ get; set; }
        public string ClientEmail { get; set; }
        public string Reference { get; set; }
        public string Description { get; set; }
        public string Notes { get; set; }
       
        public CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 CurrencyCode { get; set; }
        public string OrderLinkOnClientWebsite { get; set; }


        public bool VerifyRequirements(bool throwError = true)
        {
            bool ok = true;
            StringBuilder sb = new StringBuilder();
            if (PayMammoth.Connector.Extensions.PaymentRequestDetailsExtensions.GetTotalPrice(this) <= 0) sb.AppendLine("Please make sure total price is greater than 0");
            if (string.IsNullOrWhiteSpace(this.ReturnUrlSuccess)) sb.AppendLine("ReturnUrlSuccess is required");
            if (!string.IsNullOrWhiteSpace(this.ReturnUrlSuccess) && this.ReturnUrlSuccess.StartsWith("/")) sb.AppendLine("ReturnUrlSuccess must be a fully qualified url starting with e.g http:// or https://");
            if (string.IsNullOrWhiteSpace(this.ReturnUrlFailure)) sb.AppendLine("ReturnUrlFailure is required");
            if (!string.IsNullOrWhiteSpace(this.ReturnUrlFailure) && this.ReturnUrlSuccess.StartsWith("/")) sb.AppendLine("ReturnUrlFailure must be a fully qualified url starting with e.g http:// or https://");
            

            if (string.IsNullOrWhiteSpace(this.WebsiteAccountCode)) sb.AppendLine("Website account code must be filled in");
            if (sb.Length > 0)
            {
                if (throwError)
                    throw new InvalidOperationException(sb.ToString());
                ok = false;
            }
            return ok;

        }

        public PaymentRequestResponse GeneratePaymentRequest()
        {
            PaymentRequestResponse response = new PaymentRequestResponse();
            PaymentRequest request = null;
            if (VerifyRequirements())
            {
                WebsiteAccount account = PayMammoth.Modules.WebsiteAccountModule.WebsiteAccountFactory.Instance.GetAccountByCode(this.WebsiteAccountCode);
                if (account != null)
                {
                    if (account.VerifyRequest(this))
                    {
                        request = account.GenerateNewRequest(this);
                    }
                    else
                    {
                        response.Status = Enums.GenerateResponseStatus.HashVerificationFailed;
                        
                    }
                }
                else
                {

                    response.Status = Enums.GenerateResponseStatus.AccountDoesNotExist;
                    
                }
            }
            else
            {
                response.Status = Enums.GenerateResponseStatus.InvalidDetails;
                

            }
            response.Request = request;

            if (response.Status == Enums.GenerateResponseStatus.Ok)
            {
                if (_log.IsDebugEnabled) _log.Debug(LogUtil.AddRequestIdentifierToLogMsg( response.Request,  "Generated new request - " + response.Request.Identifier));
            }
            else
            {
                QueryString qs = new QueryString();
                this.SerializeToNameValueCollection(qs);

                if (_log.IsWarnEnabled) _log.Warn(LogUtil.AddRequestIdentifierToLogMsg( response.Request,  "Error generating new request - data: " + qs.ToString() + ", IP: " + CS.General_v3.Util.PageUtil.GetUserIP()));
            }
            
                
            return response;
        }





        public string Title { get; set; }

        #region IPaymentRequestDetails Members


        
        #endregion

        #region IPaymentRequestDetails Members


        public string ReturnUrlSuccess { get; set; }

        public string ReturnUrlFailure { get; set; }

        #endregion

        #region IInitialRequestDetails Members

        public string HashValue { get; set; }

        #endregion

        #region IPaymentRequestDetails Members



        #endregion

        #region IClientDetails Members


        public string GetAddressAsOneLine(bool includeCountry)
        {
            
            return PayMammoth.Connector.Extensions.ClientDetailsExtensions.GetAddressAsOneLine(this, includeCountry);
            
        }


        #endregion

        #region IClientDetails Members


        public string ClientTelephone { get; set; }

        public string ClientMobile { get; set; }

        public CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639? LanguageCode { get; set; }

        public CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? LanguageCountryCode { get; set; }

        public string LanguageSuffix { get; set; }

        #endregion

        #region IPaymentRequestDetails Members


        #endregion

        #region IClientDetails Members



        #endregion

        #region IPaymentRequestDetails Members


        public long GetTotalPriceInCents()
        {
            return PayMammoth.Connector.Extensions.PaymentRequestDetailsExtensions.GetTotalPriceInCents(this);
            
        }

        #endregion

        public string GetClientFullName()
        {
            return PayMammoth.Connector.Extensions.ClientDetailsExtensions.GetClientFullName(this);

        }

        #region IPaymentRequestDetails Members


        public string ClientIp { get; set; }
        #endregion

        #region IClientDetails Members


        public string ClientReference { get; set; }

        #endregion

        #region IPaymentRequestDetails Members


        public string ItemDetails { get; set; }

        #endregion




        #region IPaymentRequestDetails Members

        public IEnumerable<IItemDetail> GetItemDetails()
        {
            return ItemDetailsSerializer.DeserializeStringToItemDetails(this.ItemDetailsStr, () => new ItemDetailImpl() );
            
        }

        public string ItemDetailsStr { get; set; }

        #endregion
    }
}
