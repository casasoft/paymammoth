﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Collections;
using System.Collections.Specialized;
using System.Text.RegularExpressions;

namespace PayMammoth.Classes.Attributes
{
    public class PaymentInfoFieldAttribute : Attribute
    {
        //public delegate object ValueFromStringConverterDelegate(string s);

        public bool IsRequired { get; set; }
        public string FieldName { get; set; }
        /// <summary>
        /// Allows only A-Z 0-9 , . - / |
        /// </summary>
        public bool AllowOnlyAlphaNumericAndDigits { get; set; }
        public bool ThrowErrorIfValueContainsNonAllowedCharacters {get;set;}
        public string ExtraCharactersToAllow { get; set; }
        public int MaxLength { get; set; }
        public bool ThrowErrorIfExceedsLengths { get; set; }
        public int MinLength { get; set; }
       // public ValueFromStringConverterDelegate ValueFromStringConverter { get; set; }

        public double MaxAmount { get; set; }


        public PaymentInfoFieldAttribute()
        {
            MaxLength = int.MaxValue;
            MaxAmount = double.MaxValue;
            this.ThrowErrorIfExceedsLengths = true;
        }
        
        private string getKey(PropertyInfo pInfo, string keyPrepend, string keyAppend)
        {
            string key = null;
            if (string.IsNullOrEmpty(this.FieldName))
            {
                this.FieldName = pInfo.Name;
            }
            
            key = keyPrepend + this.FieldName + keyAppend;
            return key;
        }

        public void AddToNameValueCollection(PropertyInfo pInfo, object value, string keyPrepend, string keyAppend, NameValueCollection nv)
        {

            if (value is IEnumerable && !(value is string))
            {
                IEnumerable enumerable = (IEnumerable)value;
                int index = 0;
                foreach (var item in enumerable)
                {
                    string key = getKey(pInfo, keyPrepend, keyAppend);
                    key = key.Replace("{n}", index.ToString());
                    string s = getStringForPaymentServerFromValue(item, true);
                    if (!string.IsNullOrEmpty(s))
                        nv[key] = s;
                    index++;
                }
            }
            else
            {

                string s = getStringForPaymentServerFromValue(value, true);
                string key = getKey(pInfo, keyPrepend, keyAppend);
                if (!string.IsNullOrEmpty(s))
                    nv[key] = s;

            }
        }
        protected virtual object getValueBasedOnDataTypeFromString(string s, Type t)
        {
            object val = null;
            if (!string.IsNullOrEmpty(s))
            {

                if (t.IsAssignableFrom(typeof(double)) || t.IsAssignableFrom(typeof(int)))
                {
                    s = s.Trim();
                    double d = Convert.ToDouble(s);
                    val = d;
                    if (t.IsAssignableFrom(typeof(int)))
                        val = (int)d;
                }

                else if (t.IsAssignableFrom(typeof(bool)))
                {
                    s = s.Trim();
                    bool b = CS.General_v3.Util.Other.TextToBool(s);
                    val = b;
                }
                else if (t.IsAssignableFrom(typeof(string)))
                {
                    val = s;

                }


                else
                    throw new InvalidOperationException("Invalid type!");
            }
            return val;
        }
        public object GetValueFromNameValueCollection(PropertyInfo pInfo, string keyPrepend, string keyAppend, NameValueCollection nv)
        {

            object o = null;
            if (pInfo.PropertyType.IsAssignableFrom(typeof(IEnumerable)))
            {
                List<string> list = new List<string>();
                o = list;
                for (int i = 0; i < 99; i++)
                {
                    string key = getKey(pInfo, keyPrepend, keyAppend);
                    key = key.Replace("{n}", i.ToString());
                    string s = nv[key];
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        list.Add(s);
                    }
                    else
                        break;
                }

            }
            else
            {

                string key = getKey(pInfo, keyPrepend, keyAppend);
                string value = nv[key];
                object valueAsDataType = getValueBasedOnDataTypeFromString(value, pInfo.PropertyType);
                return valueAsDataType;
                
            }
            return o;
        }


        protected virtual string getStringFromIntegerValue(object value)
        {
            long val = Convert.ToInt64(value);
            return val.ToString();
        }
        protected virtual string getStringFromDoubleValue(object value)
        {
            double dbl = Convert.ToDouble(value);
            return dbl.ToString("0.00");
        }
        protected virtual string getStringFromDateTime(DateTime value)
        {
            DateTime d = (DateTime) value;
            return d.ToString("yyyyMMddHHmmss");
        }
        protected virtual string getStringFromBoolean(bool b)
        {
            string s = (b ? "1" : "0");
            return s;

        }

        protected virtual string getStringForPaymentServerFromValue(object value, bool validateValueBefore)
        {
            string s = null;
            bool ok = true;
            if (validateValueBefore)
                ok = this.ValidateValue(value);
            if (ok)
            {

                if (value is int || value is Int32 || value is long || value is byte || value is short || value is Int64)
                {
                    s = getStringFromIntegerValue(value);
                }
                else if (value is double || value is float || value is decimal)
                {
                    s = getStringFromDoubleValue(value);
                }
                else if (value is DateTime)
                {
                    s = getStringFromDateTime((DateTime) value);
                }
                else if (value is bool || value is Boolean)
                {
                    s = getStringFromBoolean((bool) value);
                }
                else
                {
                    if (value != null)
                        s = value.ToString();
                }
                s = checkValue(s, this.AllowOnlyAlphaNumericAndDigits, this.ExtraCharactersToAllow, this.ThrowErrorIfValueContainsNonAllowedCharacters);
            }
            return s;
        }

         private string checkValue(string s, bool allowOnlyAlphaNumeric, string extraCharsToAllow, bool throwErrorIfValueContainsNonAllowedCharacters)
        {
            if (!string.IsNullOrEmpty(s))
            {
                string regex = "";
                if (allowOnlyAlphaNumeric)
                    regex += "A-Za-z0-9";
                regex += CS.General_v3.Util.Text.ForRegExp(extraCharsToAllow);
                if (!string.IsNullOrEmpty(regex))
                {
                    regex = "[^" + regex + "]";
                    bool containsNonAllowedCharacters = Regex.IsMatch(s, regex);
                    if (containsNonAllowedCharacters)
                    {
                        if (throwErrorIfValueContainsNonAllowedCharacters)
                            throw new InvalidOperationException("Non allowed characters found in string <" + s + ">.  Allowed characters are " + regex);
                        s = Regex.Replace(s, regex, "");

                    }


                }

                if (s.Length > MaxLength && MaxLength > 0)
                {
                    s = s.Substring(0, MaxLength);
                }

            }
             return s;
        }

         private void checkValueTest()
         {
             string s = "3481-5680 Londa";
             string result = checkValue(s, true, "-", false);
             s = "3481-+5680 Londa";
             result = checkValue(s, true, "- ", false);
             result = checkValue(s, true, "-", true);
             

         }

        public virtual bool ValidateValue(object value)
        {
            if ((value is int || value is Int32 || value is long || value is byte || value is short || value is Int64) || (value is double || value is float || value is decimal))
            {
                double d = Convert.ToDouble(value);
                if (d > MaxAmount )
                {
                    throw new InvalidOperationException(this.FieldName + " - Property value must contain a number smaller or equal than " + MaxAmount);
                }
            }
            

            string s = getStringForPaymentServerFromValue(value, false);
            //s = checkValue(s, this.AllowOnlyAlphaNumericAndDigits, this.ExtraCharactersToAllow, this.ThrowErrorIfValueContainsNonAllowedCharacters);
            int len = (s != null ? s.Length : 0);

            if (ThrowErrorIfExceedsLengths && len > MaxLength)
            {
                throw new InvalidOperationException(this.FieldName + " - Value must not be longer than " + MaxLength + " characters <" + s + ">");
            }
            if (ThrowErrorIfExceedsLengths && len < MinLength)
            {
                throw new InvalidOperationException(this.FieldName + " - Value must be at least than " + MinLength + " characters <" + s + ">");
            }
            if (string.IsNullOrEmpty(s) && IsRequired)
                throw new InvalidOperationException(this.FieldName + " is required");
            return true;
        }

    }
}
