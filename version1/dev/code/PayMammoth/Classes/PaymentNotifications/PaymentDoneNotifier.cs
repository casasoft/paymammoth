﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.DbObjects.Objects;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.Classes.PaymentNotifications;
using log4net;
using System.Threading;
using BusinessLogic_v3.Modules.SettingModule;
using System.Collections;

namespace PayMammoth.Classes.PaymentNotifications
{
    public class PaymentDoneNotifier
    {
        private readonly object _lock = new object();
        private const string JOB_NAME = "PaymentDoneNotifier-SendPayments";
        public static PaymentDoneNotifier Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<PaymentDoneNotifier>(); } }
        private static readonly ILog _log = LogManager.GetLogger(typeof(PaymentDoneNotifier));


        /// <summary>
        /// Keeps a list of the currently sending items
        /// </summary>
        private ConcurrentDictionary<string, bool> _currentlySending = null;
        
        public PaymentDoneNotifier()
        {

            _currentlySending = new ConcurrentDictionary<string, bool>();
            BusinessLogic_v3.Modules.SettingModule.SettingBaseFactory.Instance.OnItemUpdate += new BusinessLogic_v3.Classes.DbObjects.General.OnItemUpdateHandler(SettingBaseFactoryInstance_OnItemUpdate);
        }

        void SettingBaseFactoryInstance_OnItemUpdate(IBaseDbObject item, BusinessLogic_v3.Enums.UPDATE_TYPE updateType)
        {
            SettingBase setting = item as SettingBase;
            if (this.Started && setting != null && string.Compare(setting.Identifier, CS.General_v3.Util.EnumUtils.StringValueOf(PayMammoth.Enums.PAYMAMMOTH_SETTINGS.Payment_Notifier_SendEveryXSeconds),true) == 0)
            {
                Stop();
                Start();
            }

        }

        public NotifyMsg NotifyResponseUrlAboutPayment(PaymentRequest r)
        {
            

            NotifyMsg msg = null;
           

            if (r != null)
            {
                
                bool isCurrentlySending = _currentlySending.ContainsKey(r.Identifier);
                
                if (!isCurrentlySending)
                {
                    if (r.WebsiteAccount != null)
                    {
                        if (!string.IsNullOrEmpty(r.WebsiteAccount.ResponseUrl))
                        {
                            _currentlySending[r.Identifier] = true;
                            msg = new NotifyMsg();
                            msg.Identifier = r.Identifier;
                            msg.RetryCount = r.PaymentNotificationRetryCount;
                            msg.Url = r.WebsiteAccount.ResponseUrl;
                            msg.OnReady += new Action<NotifyMsg>(msg_OnReady);

                            msg.Start();
                        }
                        else
                        {
                            
                            _log.Debug("NotifyResponseUrlAboutPayment called for request <" + r.Identifier + "> but it does not have any response url. Disabling notifications");
                            r.DisablePaymentNotifications();
                        }
                    }
                    else
                    {
                        _log.Debug("NotifyResponseUrlAboutPayment called for request <" + r.Identifier + "> but it does not have a linked website account");
                    }
                }
            }

        

            return msg;
        }

        void msg_OnReady(NotifyMsg obj)
        {
            bool tmp ;
            bool removedOk = _currentlySending.TryRemove(obj.Identifier, out tmp);

            

        }

       

        private void waitUntilAllAreSent(IEnumerable<NotifyMsg> list)
        {

            int waitInterval = 500;
            int maxTimeout = 120 * 1000; //1 minute
            int currTimeout = 0;
            bool allSent = false;
            while (currTimeout < maxTimeout && !allSent)
            {
                allSent = true;
                foreach (var item in list)
                {
                    if (!item.Finished)
                    {
                        allSent = false;
                        break;
                    }
                }
                System.Threading.Thread.Sleep(waitInterval);
                currTimeout += waitInterval;
            }

            if (currTimeout >= maxTimeout)
            {
                _log.Warn("Not all notification were sent in the timeout of <" + maxTimeout + " ms>");
            }
        }

        public void SendPendingNotifications()
        {
            lock (_lock) //only one at a time
            {
                int pgSize = CS.General_v3.Settings.GetSettingFromDatabaseByGenericEnum<int>(PayMammoth.Enums.PAYMAMMOTH_SETTINGS.Payment_Notifier_SendPerBatch);

                if (CS.General_v3.Util.Other.IsLocalTestingMachine) pgSize = 1;//temp, should be removed

                int pgNum = 1;
                bool retry = false;
                do
                {
                    BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();
                    IEnumerable<PaymentRequest> list = PaymentRequestFactory.Instance.GetPaymentRequestsThatRequireNotificationsToBeSent(pgNum, pgSize);
                    List<NotifyMsg> currentBatch = new List<NotifyMsg>();
                    foreach (var p in list)
                    {
                        var msg = NotifyResponseUrlAboutPayment(p);
                        if (msg != null)
                            currentBatch.Add(msg);


                    }

                    waitUntilAllAreSent(currentBatch);

                    BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();
                    retry = list.Count() > 0;
                    pgNum++;

                } while (retry);
            }
        }

        public void Start()
        {

            lock (_lock)
            {
                if (this.Started) throw new InvalidOperationException("Cannot start the payment notifier when it has already been started");

                int checkEverySeconds = CS.General_v3.Settings.GetSettingFromDatabaseByGenericEnum<int>(PayMammoth.Enums.PAYMAMMOTH_SETTINGS.Payment_Notifier_SendEveryXSeconds);

                BusinessLogic_v3.Util.SchedulingUtil.ScheduleRepeatMethodCall(SendPendingNotifications,
                                                                              JOB_NAME, null, checkEverySeconds, null);

                this.Started = true;
            }
            _log.Debug("PaymentDoneNotifier - Started");
        }

        public bool Started { get; set; }

        public void Stop()
        {
            lock (_lock)
            {
                if (!this.Started) throw new InvalidOperationException("Cannot stop the payment notifier when it has already been stopped ");

                BusinessLogic_v3.Util.SchedulingUtil.StopJob(JOB_NAME);
                this.Started = false;
            }
            _log.Debug("PaymentDoneNotifier - Stopped");
        }


    }
}
