﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using PayMammoth.Modules.PaymentRequestModule;
using CS.General_v3.Util;
using PayMammoth.Util;
namespace PayMammoth.Classes.PaymentNotifications
{
    public class NotifyMsg : IDisposable
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(NotifyMsg));
        public string Identifier { get; set; }
        public string Url { get; set; }
        public bool Finished { get; set; }
        public bool Success { get; set; }
       
        public int RetryCount { get; set; }
        public event Action<NotifyMsg> OnReady;

        
        private void sendMsg()
        {

        }
        /// <summary>
        /// Sends the actual notification
        /// </summary>
        /// <returns>Whether it was acknowledged or not</returns>
        private bool _notify()
        {
            
                
            bool ok = false;
            CS.General_v3.Classes.URL.URLClass urlClass = new CS.General_v3.Classes.URL.URLClass(this.Url);
            urlClass[PayMammoth.Connector.Constants.PARAM_IDENTIFIER] = this.Identifier;

            string sUrl = urlClass.ToString();
            if (_log.IsDebugEnabled) _log.Debug(LogUtil.AddRequestIdentifierToLogMsg(this.Identifier, "Notification #" + this.RetryCount + " for payment <" + this.Identifier + "> (Try #" + this.RetryCount + ") - Sending to url: " + sUrl));
            string response = null;
            try
            {

                response = CS.General_v3.Util.Web.HTTPGet(sUrl);
            }
            catch (Exception ex)
            {
                _log.Warn(LogUtil.AddRequestIdentifierToLogMsg(this.Identifier, "Error occured while trying to contact [" + sUrl + "] with notification of payment"), ex);
                
                response = null;
            }
            if (_log.IsDebugEnabled) _log.Debug(LogUtil.AddRequestIdentifierToLogMsg(this.Identifier, "Notification #" + this.RetryCount + " for payment <" + this.Identifier + "> - Response received: '" + response + "'"));

            bool concurrencyProb = false;
            do
            {
                concurrencyProb = false;


                var session =  BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();
                try
                {
                    
                    var request = PayMammoth.Modules.Factories.PaymentRequestFactory.GetRequestByIdentifier(this.Identifier);
                    
                    if (request != null)
                    {
                        lock (request.ItemLock)
                        {
                            if (!request.IsPaymentNotificationAcknowledge())
                            {

                                if (string.Compare(response, PayMammoth.Connector.Constants.RESPONSE_OK, true) == 0)
                                {
                                    //ok, response received
                                    request.MarkAsNotificationResponseAcknowledged();
                                    if (_log.IsDebugEnabled) _log.Debug(LogUtil.AddRequestIdentifierToLogMsg(this.Identifier, "Notification #" + this.RetryCount + " for payment <" + this.Identifier + "> - Notification marked as acknowledged"));
                                    ok = true;
                                    this.Success = true;
                                }
                                else
                                {
                                    request.IncrementPaymentNotificationRetryCount();
                                    if (_log.IsWarnEnabled) _log.Warn(LogUtil.AddRequestIdentifierToLogMsg(this.Identifier, "Notification #" + this.RetryCount + " for payment <" + this.Identifier + "> - Notification NOT marked acknowledged"));
                                }
                            }
                        }
                    }
                
                    
                }
                catch (NHibernate.StaleStateException ex)
                {
                    concurrencyProb = true;
                    session.RollbackCurrentTransaction();
//                    session.Clear(); //dont save any changes,
                    //this happens if another thread updated this notification msg
                }

                BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();
            } while (concurrencyProb);

            return ok;
        }

        /// <summary>
        /// This ensures that the request is saved and confirmed as paid first, before trying to send a notification
        /// </summary>
        /// <returns></returns>
        private bool waitUntilRequestConfirmedAsPaid()
        {
            bool ok = false;
            IPaymentRequest request = null;
            BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();
            request = PayMammoth.Modules.Factories.PaymentRequestFactory.GetRequestByIdentifier(this.Identifier);
            ok = request.WaitUntilRequestIsMarkedAsPaid();
            BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();

            if (_log.IsInfoEnabled)
            {
                if (ok)
                    _log.Info(LogUtil.AddRequestIdentifierToLogMsg(this.Identifier, "Request <" + this.Identifier + "> - Paid OK"));
                else
                    _log.Info(LogUtil.AddRequestIdentifierToLogMsg(this.Identifier, "Request <" + this.Identifier + "> - Paid NOT ok"));
                    
            }
            return ok;
                
        }
        /// <summary>
        /// This checks whether the request still requires the notification to be sent.
        /// </summary>
        /// <returns></returns>
        private bool checkIfRequestStillRequiresNotificationToBeSent(out int retryCount)
        {
            retryCount = 0;
            bool toSend = false;

            BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();
            var request = PayMammoth.Modules.Factories.PaymentRequestFactory.GetRequestByIdentifier(this.Identifier);
            toSend = request.CheckIfPaymentNotificationStillNeedsToBeSent();
            if (toSend)
                retryCount = request.PaymentNotificationRetryCount;
            BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();
            return toSend;


        }
        ///// <summary>
        ///// This ensures that the request is saved and confirmed as paid first, before trying to send a notification
        ///// </summary>
        ///// <returns></returns>
        //private bool waitUntilRequestConfirmedAsPaid()
        //{
        //    bool ok = false;
        //    IPaymentRequest request = null;
        //    int waitInterval = 0;
        //    int maxWait = 30; //seconds
        //    do
        //    {
        //        BusinessLogic_v3.Util.nHibernateUtil.CreateNewSessionForCurrentContext();
        //        request = PayMammoth.Modules.Factories.PaymentRequestFactory.GetRequestByIdentifier(this.Identifier);
        //        request.WaitUntilRequestIsMarkedAsPaid
        //        BusinessLogic_v3.Util.nHibernateUtil.DisposeCurrentSessionInContext();
        //        if (request != null && request.CheckIfPaid())
        //        {
        //            ok = true;
        //            break;
        //        }
        //        else
        //        {
        //            System.Threading.Thread.Sleep(1000);
        //            waitInterval++;
        //        }

        //    } while (waitInterval < maxWait);
        //    if (_log.IsInfoEnabled)
        //    {
        //        if (ok)
        //            _log.Info(LogUtil.AddRequestIdentifierToLogMsg(this.Identifier, "Request <" + this.Identifier + "> - Paid OK"));
        //        else
        //            _log.Info(LogUtil.AddRequestIdentifierToLogMsg(this.Identifier, "Request <" + this.Identifier + "> - Paid NOT ok"));
                    
        //    }
        //    return ok;
                
        //}

        /// <summary>
        /// Starts the request - Sends an amount of notifictions, with increasing intervals, until one is confirmed.
        /// </summary>
        private void _start()
        {
           // int waitInterval = 3000;

            if (_log.IsInfoEnabled) _log.Info(LogUtil.AddRequestIdentifierToLogMsg(this.Identifier, "Notification #" +this.RetryCount + " for payment <" + this.Identifier + "> - Started")); 
            
           // System.Threading.Thread.Sleep(waitInterval); // this is to give a buffer to the other things to get ready.
            bool markedAsPaid = waitUntilRequestConfirmedAsPaid();
            bool notified = false;
            if (_log.IsInfoEnabled) _log.Info(LogUtil.AddRequestIdentifierToLogMsg(this.Identifier, "Notification #" + this.RetryCount + "for payment <" + this.Identifier + "> - OK, starting now"));
            int currRetryCount = 0;
            bool notificationStillNeedsToBeSent = checkIfRequestStillRequiresNotificationToBeSent(out currRetryCount);
            if (notificationStillNeedsToBeSent)
            {
                if (!string.IsNullOrWhiteSpace(this.Url) && markedAsPaid)
                {
                    if (_log.IsInfoEnabled) _log.Info(LogUtil.AddRequestIdentifierToLogMsg(this.Identifier, "Notification #" + this.RetryCount + "for payment <" + this.Identifier + "> - Starting notification requests"));
                    var retryCounts = Constants.GetPaymentNotificationsRetryCounts(); //gets the list of notification retry intervals
                    notified = false;
                    
                    notified = _notify();
                    
                    if (!notified)
                    {
                        _log.Warn(LogUtil.AddRequestIdentifierToLogMsg(this.Identifier, "Notification #" + this.RetryCount + "for payment <" + this.Identifier + "> - Failed to notify about payment"));
                    }

                }
                else
                {
                    _log.Warn(LogUtil.AddRequestIdentifierToLogMsg(this.Identifier, "Notification #" + this.RetryCount + "for payment <" + this.Identifier + "> not sent as it was not even marked as paid"));
                }
            }
            else
            {
                _log.Info(LogUtil.AddRequestIdentifierToLogMsg(this.Identifier, "Notification #" + this.RetryCount + "for payment <" + this.Identifier + "> not sent as it was already sent"));
                
            }
            this.Dispose();
            


        }

        public void Start()
        {
            if (Url != null)
            {
                CS.General_v3.Util.ThreadUtil.CallMethodOnSeperateThread(_start, "NotifyMsg-" + this.Identifier);
            }
        }

        #region IDisposable Members

        private void readyProcessing()
        {
            this.Finished = true;
            if (OnReady != null)
                OnReady(this);
        }

        public void Dispose()
        {
            readyProcessing();
         
        }

        #endregion
    }
}
