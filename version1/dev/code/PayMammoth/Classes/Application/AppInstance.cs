using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace PayMammoth.Classes.Application
{
    public class AppInstance : PayMammoth.Modules._AutoGen.AppInstance_AutoGen
    {
		public AppInstance()
        {
            this.AddProjectAssembly(typeof(AppInstance));
        }
        protected override void onPostApplicationStart()
        {

            PayMammoth.Connector.Constants.SecretWord = "PAYMAMMOTH";
            
            base.onPostApplicationStart();
        }
        protected override void launchBackgroundTasks()
        {
            PaymentNotifications.PaymentDoneNotifier.Instance.Start();

            base.launchBackgroundTasks();
        }
    }
}
