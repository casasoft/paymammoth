﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Modules.PaymentMethodModule;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.Modules.WebsiteAccountModule;

namespace PayMammoth.Classes.Interfaces
{
    public interface IPaymentMethodInfo
    {
        PayMammoth.Connector.Enums.PaymentMethodSpecific PaymentMethod { get; set; }
        string Title { get; set; }
        string Description { get; set; }
        string CustomImageUrl { get; set; }
        int Priority { get; set; }
        void FillFromPaymentMethod(IPaymentRequest request);
        void FillFromPaymentMethod(IPaymentMethod method, IWebsiteAccount account);

    }
}
