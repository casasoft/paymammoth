﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.Classes.Interfaces
{
    public interface IPaymentMethod
    {
        string Title { get; }
        string Description { get; }
        PayMammoth.Connector.Enums.PaymentMethodSpecific PaymentMethodType { get; }
    }
}
