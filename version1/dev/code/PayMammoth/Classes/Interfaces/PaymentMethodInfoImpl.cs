﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Modules.PaymentMethodModule;
using PayMammoth.Modules.WebsiteAccountModule;
using PayMammoth.Modules.PaymentRequestModule;

namespace PayMammoth.Classes.Interfaces
{
    public class PaymentMethodInfoImpl: IPaymentMethodInfo
    {
        #region IPaymentMethodInfo Members

        public Connector.Enums.PaymentMethodSpecific PaymentMethod { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string CustomImageUrl { get; set; }

        #endregion
        public void FillFromPaymentMethod(IPaymentRequest request)
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(request, "Request is required");
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(request.CurrentTransaction, "Current Transaction is required");
            var method = request.CurrentTransaction.PaymentMethod;
            var paymentMethod = Modules.Factories.PaymentMethodFactory.GetPaymentMethodByIdentifier(method);
            FillFromPaymentMethod(paymentMethod, request.WebsiteAccount);

        }

        public void FillFromPaymentMethod(IPaymentMethod method, IWebsiteAccount account)
        {
            this.PaymentMethod = method.PaymentMethod;
            this.Title = method.Title;
            this.Description = method.Description;
            if (!method.Icon.IsEmpty())
            {
                this.CustomImageUrl = method.Icon.GetSpecificSizeUrl(Modules.PaymentMethodModule.PaymentMethod.IconSizingEnum.Normal);
            }
            switch (this.PaymentMethod)
            {
                case Connector.Enums.PaymentMethodSpecific.PaymentGateway_Transactium:
                    if (!string.IsNullOrWhiteSpace(account.PaymentGatewayTransactiumDescription)) this.Description = account.PaymentGatewayTransactiumDescription; break;

                case Connector.Enums.PaymentMethodSpecific.PaymentGateway_Realex:
                    if (!string.IsNullOrWhiteSpace(account.PaymentGatewayRealexDescription)) this.Description = account.PaymentGatewayRealexDescription; break;

                case Connector.Enums.PaymentMethodSpecific.PaymentGateway_Apco:
                    if (!string.IsNullOrWhiteSpace(account.PaymentGatewayApcoDescription)) this.Description = account.PaymentGatewayApcoDescription; break;
            }
            this.Priority = method.Priority;
            
        }

        #region IPaymentMethodInfo Members


        public int Priority { get; set; }
        #endregion
    }
}
