﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Extensions;
using PayMammoth.Classes.Interfaces;
using PayMammoth.Frontend.PaymentRequestModule;

namespace PayMammoth.Classes.Pages.Frontend
{
    public class BasePageUsingPagesMasterPage : BasePage
    {
        public new BasePagesMasterPage Master
        {
            get { return (BasePagesMasterPage) base.Master; }
        }

        public string PageTitle { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            init();
            base.OnLoad(e);
        }

        private void init()
        {
            
        }

        private void setTitle()
        {
            if (!String.IsNullOrEmpty(this.PageTitle) && this.Master != null )
            {
                this.Master.PageTitle = this.PageTitle;
            }


        }

        protected override void OnPreRender(EventArgs e)
        {
            setTitle();
            base.OnPreRender(e);
        }
    }
}
