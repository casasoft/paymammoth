﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.PaymentMethods.PayPal.v1;

namespace PayMammoth.Classes.Pages.Frontend.PayPal
{
    public abstract class BasePayPalPage : BasePaymentPage
    {
        protected void confirmThatPaymentIsStillPaypal()
        {
            if (!PayPalManager.Instance.ConfirmRequestIsStillPaypal(this.CurrentPaymentRequest.Data))
            {
                //here it means that for some reason, a new transaction was strated
                this.SetStatusMsg("Current transaction is no longer PayPal", CS.General_v3.Enums.STATUS_MSG_TYPE.Error);
                
                

            }
        }
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);
            confirmThatPaymentIsStillPaypal();
        }
    }
}
