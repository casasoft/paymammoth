﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Classes.Pages;
using BusinessLogic_v3.Frontend.ContentTextModule;

namespace PayMammoth.Classes.Pages.Frontend
{
    public abstract class BasePage : CS.WebComponentsGeneralV3.Code.Classes.Pages.BasePage<BaseMasterPageBase>
    {
        public override System.Web.UI.HtmlControls.HtmlGenericControl HtmlTag
        {
            get { return null; }
        }

        public void SetStatusMsg(ContentTextBaseFrontend ct, CS.General_v3.Enums.STATUS_MSG_TYPE msgType)
        {
            string msg = ct.GetContent();
            SetStatusMsg(msg, msgType);
            
        }
        public void SetStatusMsg(string msg, CS.General_v3.Enums.STATUS_MSG_TYPE msgType)
        {
            
            SessionData.SetStatusMsg(msg, msgType);
        }
    }
}
