﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;

namespace PayMammoth.Classes.Pages.Frontend
{
    public abstract class BasePagesMasterPage : BaseMasterPage
    {
        public new BaseMainMasterPage Master { get { return (BaseMainMasterPage)base.Master; } }

        public override System.Web.UI.HtmlControls.HtmlGenericControl HtmlTag
        {
            get { return this.Master.HtmlTag; }
        }

        public string PageTitle { get; set; }

        protected abstract HtmlGenericControl _divMessage { get; }

        protected override void OnPreRender(EventArgs e)
        {
            showStatusMessage();
            base.OnPreRender(e);
        }

        private void showStatusMessage()
        {
            _divMessage.Visible = false;
            if (!string.IsNullOrWhiteSpace(PayMammoth.SessionData.LastMessage))
            {
                string messageType =
                    CS.General_v3.Util.EnumUtils.StringValueOf(PayMammoth.SessionData.LastMessageType.Value);
                _divMessage.Attributes["class"] += " " + messageType.ToLower() + "-message";
                _divMessage.Visible = true;
                _divMessage.InnerHtml = messageType + ": " + CS.General_v3.Util.Text.HtmlEncode(PayMammoth.SessionData.LastMessage);
            }
            PayMammoth.SessionData.ResetStatusMsg();
        }
    }
}
