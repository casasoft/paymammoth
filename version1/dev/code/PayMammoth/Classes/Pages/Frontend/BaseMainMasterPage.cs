﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Pages;
using System.Web.UI.HtmlControls;

namespace PayMammoth.Classes.Pages.Frontend
{
    using PayMammoth.Frontend.PaymentRequestModule;

    public abstract class BaseMainMasterPage : CS.WebComponentsGeneralV3.Code.Classes.Pages.BaseMainMasterPage
    {

        public override System.Web.UI.HtmlControls.HtmlGenericControl HtmlTag
        {
            get { return Master.HtmlTag; }
        }

        //protected abstract HtmlGenericControl _divMessage { get; }

        protected void showStatusMsg()
        {
            //_divMessage.Visible = false;
            //if (!string.IsNullOrWhiteSpace(PayMammoth.SessionData.LastMessage))
            //{
            //    string messageType =
            //        CS.General_v3.Util.EnumUtils.StringValueOf(PayMammoth.SessionData.LastMessageType.Value);
            //    _divMessage.Attributes["class"] += " " + messageType.ToLower() + "-message";
            //    _divMessage.Visible = true;
            //    _divMessage.InnerHtml = messageType + ": " + CS.General_v3.Util.Text.HtmlEncode(PayMammoth.SessionData.LastMessage);
            //}
            //PayMammoth.SessionData.ResetStatusMsg();
        }

        protected override void OnPreRender(EventArgs e)
        {
            showStatusMsg();
            base.OnPreRender(e);
        }
        /*
        private bool _currentPaymentLoaded = false;
        private PaymentRequestFrontend _currentPayment = null;

        public PaymentRequestFrontend CurrentPaymentRequest
        {
            get
            {
                if (!_currentPaymentLoaded)
                {
                    _currentPaymentLoaded = true;
                    var data = QuerystringData.GetPaymentRequestFromQuerystring();
                    _currentPayment = PaymentRequestFrontend.Get(data);
                }
                return _currentPayment;
            }
        }
        */
    }
}
