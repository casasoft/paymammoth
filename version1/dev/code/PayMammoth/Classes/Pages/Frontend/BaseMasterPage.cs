﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;
using CS.General_v3.Controls.WebControls.Common;
using CS.WebComponentsGeneralV3.Code.Classes.Pages;

namespace PayMammoth.Classes.Pages.Frontend
{
    public abstract class BaseMasterPage : CS.WebComponentsGeneralV3.Code.Classes.Pages.BaseMasterPage<BaseMasterPageBase>
    {

        //protected abstract HtmlGenericControl _divMessage { get; }

        //protected void showStatusMsg()
        //{
        //    _divMessage.Visible = false;
        //    if (!string.IsNullOrWhiteSpace(PayMammoth.SessionData.LastMessage))
        //    {
        //        _divMessage.Visible = true;
        //        _divMessage.InnerHtml = CS.General_v3.Util.EnumUtils.StringValueOf(PayMammoth.SessionData.LastMessageType.Value) + ": " + CS.General_v3.Util.Text.HtmlEncode(PayMammoth.SessionData.LastMessage);
        //    }
        //    PayMammoth.SessionData.ResetStatusMsg();
        //}

        protected override void OnPreRender(EventArgs e)
        {
            //showStatusMsg();
            base.OnPreRender(e);
        }
    }
}
