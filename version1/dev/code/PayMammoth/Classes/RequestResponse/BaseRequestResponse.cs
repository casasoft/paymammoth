﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;

using System.Reflection;
using PayMammoth.Classes.Attributes;

namespace PayMammoth.Classes.RequestResponse
{
    public class BaseRequestResponse
    {

        protected string realexServerUrl
        {
            get { return CS.General_v3.Settings.GetSettingFromDatabaseByGenericEnum(Enums.PAYMAMMOTH_SETTINGS.Realex_v1_ServerUrl); }
        }


        protected virtual NameValueCollection createNameValueCollectionWithPaymentServerInfo()
        {
            NameValueCollection nv = new NameValueCollection();
            FillNameValueCollection(nv, null, null);
            return nv;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nv"></param>
        /// <param name="keyPrepend"></param>
        /// <param name="keyAppend"></param>
        public virtual void ParseFromNameValueCollection(NameValueCollection nv, string keyPrepend, string keyAppend)
        {

            var properties = CS.General_v3.Util.ReflectionUtil.GetAllPropertiesWithAttributes(this.GetType(), typeof(PaymentInfoFieldAttribute));
            foreach (var pInfo in properties)
            {
                PaymentInfoFieldAttribute fieldInfo = pInfo.Attributes.Cast<PaymentInfoFieldAttribute>().FirstOrDefault();
                object val = fieldInfo.GetValueFromNameValueCollection(pInfo.Property, keyPrepend, keyAppend, nv);
                if (pInfo.Property.CanWrite)
                {
                    pInfo.Property.SetValue(this, val, null);
                }
                else
                {
                    int k = 5;
                }

            }
        }
        public virtual void FillNameValueCollection(NameValueCollection nv, string keyPrepend, string keyAppend)
        {
            var properties = CS.General_v3.Util.ReflectionUtil.GetAllPropertiesWithAttributes(this.GetType(), typeof(PaymentInfoFieldAttribute));
            foreach (var pInfo in properties)
            {
                PaymentInfoFieldAttribute fieldInfo = pInfo.Attributes.Cast<PaymentInfoFieldAttribute>().FirstOrDefault();
                fieldInfo.AddToNameValueCollection(pInfo.Property, pInfo.Property.GetValue(this, null), keyPrepend, keyAppend, nv);
            }
            
        }
    }
}
