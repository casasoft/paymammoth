﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Classes.Interfaces;

namespace PayMammoth.Classes.Impl
{
    public class PaymentMethodImpl : IPaymentMethod
    {

        #region IPaymentMethod Members

        public string Title { get; set; }

        public string Description { get; set; }



        public PayMammoth.Connector.Enums.PaymentMethodSpecific PaymentMethodType { get; set; }

        #endregion
        public PaymentMethodImpl()
        {

        }
        public PaymentMethodImpl(PayMammoth.Connector.Enums.PaymentMethodSpecific paymentMethod, string title = null, string desc = null)
        {
            this.Title = title;
            this.Description = desc;
            this.PaymentMethodType = paymentMethod;
            if (string.IsNullOrEmpty(this.Title))
                this.Title = CS.General_v3.Util.EnumUtils.StringValueOf(paymentMethod);
        }

    }
}
