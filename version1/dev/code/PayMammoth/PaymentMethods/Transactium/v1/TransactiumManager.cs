﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Classes.Interfaces;
using PayMammoth.PaymentMethods.PayPal.v1.ExpressCheckout.NVP;
using PayMammoth.Modules.WebsiteAccountModule;

using PayMammoth.PaymentMethods.PayPal.v1.HelperClasses;
using PayMammoth.Classes.Other;
using PayMammoth.Modules.PaymentRequestModule;
using CS.General_v3.Classes.URL;
using PayMammoth.Modules.PaymentRequestTransactionModule;
using PayMammoth.PaymentMethods.PayPal.v1.ExpressCheckout.InstantPaymentNotifications;
using CS.General_v3.Classes.HelperClasses;
using PayMammoth.Connector.Classes.Interfaces;
using PayMammoth.PaymentMethods.Transactium.v1.HelperClasses;
using PayMammoth.PaymentMethods.Transactium.v1.Wrappers;
using log4net;

namespace PayMammoth.PaymentMethods.Transactium.v1
{
    public class TransactiumManager

    {
        protected static readonly ILog _log = LogManager.GetLogger(typeof(TransactiumManager));

        public static TransactiumManager Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<TransactiumManager>(); } }
        private TransactiumManager()
        {

        }
        public GatewayClient GetGatewayClient(IWebsiteAccount account)
        {
            GatewayClient client = new GatewayClient(!account.PaymentGatewayTransactiumUseLiveEnvironment,
                account.PaymentGatewayTransactiumLiveUsername,
                account.PaymentGatewayTransactiumLivePassword,
                account.PaymentGatewayTransactiumStagingUsername,
                account.PaymentGatewayTransactiumStagingPassword);
            return client;
        }

        public CreateRequestResponse CreateRequest(IPaymentRequest request)
        {
            CreateRequestResponse response = new CreateRequestResponse();
            
            

            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(request, "Request is required");

            PaymentRequestTransaction transaction = request.StartNewTransaction(PayMammoth.Connector.Enums.PaymentMethodSpecific.PaymentGateway_Transactium);






            long amtInCents = request.GetTotalPriceInCents();
            var client = TransactiumManager.Instance.GetGatewayClient(request.WebsiteAccount);



            string successUrl = TransactiumUrls.GetSuccessUrl(request.Identifier);
            string failureUrl = TransactiumUrls.GetFailureUrl(request.Identifier);
            string paymentUrl = TransactiumUrls.GetPaymentStatusUrl(request.Identifier);

            try
            {
                response.PaymentResponse = client.CreatePaymentRequest(transaction);
                //    amtInCents,
                //    EnumsTransactium.GetCurrencyCodeFromISO(((IPaymentRequestDetails)request).CurrencyCode),
                //    successUrl,
                //    failureUrl,
                //    EnumsTransactium.GetCountryCodeFromISO(((IPaymentRequestDetails)request).ClientCountry),
                //    ((IPaymentRequestDetails)request).ClientEmail,
                //    transaction.ID.ToString(),
                //    null,
                //    request.WebsiteAccount.PaymentGatewayTransactiumProfileTag);

            }
            catch (Exception ex)
            {
                response.PaymentResponse = null;
                response.Result.AddException(ex);
                _log.Warn("Error occured while trying to create Payment Request", ex);
            }
            if (response.Result.IsSuccessful)
            {
                transaction.Reference = response.PaymentResponse.HPSID;
            }

            return response;
            
            


        }
        public CheckPaymentResponse CheckPayment(IPaymentRequest request)
        {
            CheckPaymentResponse response = new CheckPaymentResponse();
            OperationResult result = response.Result;
            response.Status = EnumsTransactium.CHECK_PAYMENT_RESPONSE.Fail;
            var transaction = request.CurrentTransaction;
            if (request != null && transaction != null && transaction.PaymentMethod == PayMammoth.Connector.Enums.PaymentMethodSpecific.PaymentGateway_Transactium)
            {
                if (!request.CheckIfPaid())
                {
                    var gateway = GetGatewayClient(request.WebsiteAccount);
                    MyHPSPayment payment = gateway.CheckPaymentStatus();
                    if (payment != null)
                    {
                        //transaction.AddDetailsFromTransactiumPayment(payment, true);
                        //transaction.MarkAsNotTemporary(false);
                        payment.AddDetailsToTransactionParameters(transaction);


                        if (payment.Status == EnumsTransactium.HPStatus.Completed && payment.ProceedWithPurchase)
                        {
                            //sucess
                            string authCode = payment.GetAuthCode();
                            response.Status = EnumsTransactium.CHECK_PAYMENT_RESPONSE.Success;
                            transaction.MarkAsSuccessful(authCode);


                        }
                        else
                        {
                            if (payment.Status == EnumsTransactium.HPStatus.Cancelled)
                            {
                                response.Status = EnumsTransactium.CHECK_PAYMENT_RESPONSE.Cancelled;
                                result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Information, "Payment cancelled");
                            }
                            else
                            {
                                response.Status = EnumsTransactium.CHECK_PAYMENT_RESPONSE.Fail;
                                //check that HPS10 is catered well for it.
                                result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, "Error occured [" + payment.GetErrorCode() + "]");
                            }

                        }




                    }
                    else
                    {
                        result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, "Could not find payment");
                    }
                }
                else
                {
                    _log.Info("Request [" + request.Identifier + "] already paid");
                    result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Warning, "Request already paid");
                }
            }
            else
            {
                result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, "Request not found");
            }
            return response;

        }

    }
}
