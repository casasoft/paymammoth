﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.PaymentMethods.Transactium.v1.Wrappers;
using CS.General_v3.Classes.HelperClasses;

namespace PayMammoth.PaymentMethods.Transactium.v1.HelperClasses
{
    public class CreateRequestResponse
    {
        public MyHPSPayment PaymentResponse {get;set;}
        public OperationResult Result { get; set; }
        public CreateRequestResponse()
        {
            this.Result = new OperationResult();
        }

    }
}
