﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.PaymentMethods.PaymentGateways.Apco.v1
{
    public static class ApcoConstants
    {
        public const string PARAM_PARAMS = "params";
        public const string RESPONSE_OK = "OK";
        public static PayMammoth.Connector.Enums.PaymentMethodSpecific PaymentMethodApco = Connector.Enums.PaymentMethodSpecific.PaymentGateway_Apco;
        public static string DEFAULT_GATEWAY_URL
        {
            get
            {
                //            return "https://www.apsp.biz/pay/FP2/checkout.aspx";
                //    return "https://www.apsp.biz/pay/FP3/checkout.aspx";
                return "https://www.apsp.biz/pay/FP4/checkout.aspx";
            }
        }
        
        
    }
}
