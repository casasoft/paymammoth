﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using log4net;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.Util;

namespace PayMammoth.PaymentMethods.PaymentGateways.Apco.v1
{
    public abstract class ApcoStatusHandler : CS.WebComponentsGeneralV3.Code.Classes.HTTPHandlers.BaseHandler
    {
        protected virtual void TransactionSuccessful(IPaymentRequest request, ApcoResponse response)
        {

            request.CurrentTransaction.MarkAsSuccessful(requiresManualPayment: false, authCode: response.AuthorisationCode);
            
        }

        protected virtual void TransactionUnSuccessful(IPaymentRequest request, ApcoResponse response)
        {
            int k = 5;

        }

        private static readonly ILog _log = LogManager.GetLogger(typeof(ApcoStatusHandler));
       

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }

        protected override void processRequest(HttpContext ctx)
        {
            
            string identifier = ctx.Request.QueryString[PayMammoth.Connector.Constants.PARAM_IDENTIFIER];
            
            if (_log.IsInfoEnabled) _log.Info(LogUtil.AddRequestIdentifierToLogMsg( identifier, "processRequest: QS: " + ctx.Request.QueryString.ToString() + ", Form: " + ctx.Request.Form.ToString()));
            
            var paymentRequest=  PayMammoth.Modules.Factories.PaymentRequestFactory.GetRequestByIdentifier(identifier);
            bool ok = false;
            if (paymentRequest != null)
            {

                ApcoResponse response = new ApcoResponse();
                response.LoadFromContext(ctx);
                var result = response.VerifyResponse(paymentRequest);

                if (result != ApcoEnums.RESPONSE_RESULT.InvalidHash)
                {
                    if (result == ApcoEnums.RESPONSE_RESULT.Success)
                    {
                        TransactionSuccessful(paymentRequest, response);
                    }
                    else if (result == ApcoEnums.RESPONSE_RESULT.Failed)
                    {
                        TransactionUnSuccessful(paymentRequest, response);
                    }
                    response.WriteConfirmation(ctx.Response);
                    ok = true;
                }
                
            }
            if (!ok)
            {
                _log.Info(LogUtil.AddRequestIdentifierToLogMsg(paymentRequest, "processRequest: Hash verification failed"));
            }


        }
    }
}
