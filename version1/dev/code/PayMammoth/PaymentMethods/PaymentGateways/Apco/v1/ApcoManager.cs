﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.HelperClasses;
using log4net;
using PayMammoth.Modules.PaymentRequestTransactionModule;
using PayMammoth.Modules.WebsiteAccountModule;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.PaymentMethods.PaymentGateways.Apco.v1.WebForm;
using System.Web;

namespace PayMammoth.PaymentMethods.PaymentGateways.Apco.v1
{
    public class ApcoManager
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(ApcoManager));
         public static ApcoManager Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<ApcoManager>(); } }
         private ApcoManager()
        {

        }

         public ApcoClient GetGatewayClient(IPaymentRequestTransaction request)
         {
             ApcoClient client = new ApcoClient();
             client.Parameters.FillFromTransaction(request);
             
             
             return client;
         }

        /// <summary>
        /// This will create an initial request with Apco, and writes the form to the page
        /// </summary>
        /// <param name="pg"></param>
        /// <param name="request"></param>
        /// <returns></returns>
         public void CreateInitialRequest(System.Web.UI.Page pg, IPaymentRequest request)
         {
             if (!request.WebsiteAccount.IsApcoEnabled())
             {
                 throw new InvalidOperationException("Apco Payment gateway must be switched on in order to use this");
             }
             
             
             var transaction = request.StartNewTransaction(Connector.Enums.PaymentMethodSpecific.PaymentGateway_Apco);


             ApcoPaymentFormWriter writer = new ApcoPaymentFormWriter(pg);
             
             ApcoCredentials apcoCredentials = request.WebsiteAccount.GetApcoCredentials();
             writer.Parameters.FillFromTransaction(transaction);
             
             writer.WriteApcoForm();


         }


        /// <summary>
        /// Verifies that the transaction is still an apco one
        /// </summary>
        /// <param name="iPaymentRequestTransaction"></param>
        /// <returns></returns>
         public bool VerifyTransaction(IPaymentRequestTransaction iPaymentRequestTransaction)
         {
             return iPaymentRequestTransaction.PaymentMethod == ApcoConstants.PaymentMethodApco;
         }

         public void RedirectToFailurePageInParent(System.Web.UI.Page pg, IPaymentRequest request)
         {

             string failureUrl = ApcoUrls.GetPaymentUrl(request.Identifier);
             PayMammoth.SessionData.SetStatusMsg("Payment unsuccessful, please try again", CS.General_v3.Enums.STATUS_MSG_TYPE.Error);
             CS.General_v3.Util.PageUtil.RedirectParentUsingJavaScript(pg, failureUrl);
         }
    }
}
