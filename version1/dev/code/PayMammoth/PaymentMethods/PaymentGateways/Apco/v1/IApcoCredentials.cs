namespace PayMammoth.PaymentMethods.PaymentGateways.Apco.v1
{
    public interface IApcoCredentials
    {
        string ProfileId { get; set; }
        string SecretWord { get; set; }
    }
}