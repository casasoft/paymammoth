﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using CS.General_v3.Classes.Attributes;

namespace PayMammoth.PaymentMethods.PaymentGateways.Realex.v1
{
    public static class RealexEnums
    {
        
        

        public enum CURRENCY_CODE
        {
            [Description("EUR")]
            Euro,
            [Description("GBP")]
            PoundSterling,
            [Description("USD")]
            USDollar,
            [Description("WEK")]
            SwedishKrona,
            [Description("CHF")]
            SwissFranc,
            [Description("HKD")]
            HongKongDollar,
            [Description("JPY")]
            JapaneseYen
        }
        public static CURRENCY_CODE GetRealexCurrencyCodeFromISOEnum(CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 isoCurrency)
        {
            switch (isoCurrency)
            {
                case CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.EuroMemberCountriesEuro: return CURRENCY_CODE.Euro;
                case CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.UnitedKingdomPounds: return CURRENCY_CODE.PoundSterling;
                case CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.UnitedStatesOfAmericaDollars: return CURRENCY_CODE.USDollar;
                case CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.SwedenKronor: return CURRENCY_CODE.SwedishKrona;
                case CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.SwitzerlandFrancs: return CURRENCY_CODE.SwissFranc;
                case CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.HongKongDollars: return CURRENCY_CODE.HongKongDollar;
                case CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.JapanYen: return CURRENCY_CODE.JapaneseYen;
            }
            throw new InvalidOperationException("Currency <" + isoCurrency + "> not supported in Realex");
        }
        public static CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 GetISOCurrencyFromRealexCurrencyCode(CURRENCY_CODE code)
        {
            string currCode = CS.General_v3.Util.EnumUtils.StringValueOf(code);
            return CS.General_v3.Enums.ISO_ENUMS.Currency_ISO4217FromCode(currCode).GetValueOrDefault( CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.EuroMemberCountriesEuro);
        }


        public enum CVN_RESULT
        {
            [Description("M")]
            CVV_Matched,
            [Description("N")]
            CVV_NotMatched,
            [Description("I")]
            CVV_NotCheckedDueToCircumstances,
            [Description("U")]
            CVV_NotChecked_IssuerNotCertified,
            [Description("P")]
            CVV_NotProcessed
        }
        public static CVN_RESULT GetCVNResultFromString(string s)
        {
            return (CVN_RESULT)CS.General_v3.Util.EnumUtils.EnumValueOf(typeof(CVN_RESULT), s);
        }

        public enum RESPONSE_CODE
        {
            Successful,
            DeclinedByBank,
            ReferrralByBank,
            CardReportedLostOrStolen,
            ErrorWithBankSystems,
            ErrorWithRealexPaymentsSystem,
            IncorrectXMLMessageFormationOrContent,
            ClientDeactivated,
            InvalidResponseCode
        }
        public static Enums.PaymentStatus GetPayMammothStatusFromResponseCode(RESPONSE_CODE code)
        {
            switch (code)
            {
                case RESPONSE_CODE.Successful: return Enums.PaymentStatus.Success;

                case RESPONSE_CODE.ReferrralByBank:
                case RESPONSE_CODE.CardReportedLostOrStolen:
                case RESPONSE_CODE.DeclinedByBank: return Enums.PaymentStatus.InvalidCardDetails;

                case RESPONSE_CODE.ErrorWithRealexPaymentsSystem:
                case RESPONSE_CODE.ErrorWithBankSystems: return Enums.PaymentStatus.CommunicationError;

                case RESPONSE_CODE.IncorrectXMLMessageFormationOrContent:
                case RESPONSE_CODE.ClientDeactivated:
                case RESPONSE_CODE.InvalidResponseCode: return Enums.PaymentStatus.GenericError;
                default: throw new InvalidOperationException("Response code not yet mapped");

            }
        }

        public static RESPONSE_CODE GetResponseCodeFromString(string s)
        {
            switch (s)
            {
                case "00": return RESPONSE_CODE.Successful;
                case "101": return RESPONSE_CODE.DeclinedByBank;
                case "102": return RESPONSE_CODE.ReferrralByBank;
                case "103": return RESPONSE_CODE.CardReportedLostOrStolen;
                case "666": return RESPONSE_CODE.ClientDeactivated;
                default:
                    {
                        if (!string.IsNullOrEmpty(s))
                        {
                            switch (s[0])
                            {
                                case '2': return RESPONSE_CODE.ErrorWithBankSystems;
                                case '3': return RESPONSE_CODE.ErrorWithRealexPaymentsSystem;
                                case '5': return RESPONSE_CODE.IncorrectXMLMessageFormationOrContent;
                            }
                        }
                    }
                    break;


            }
            return RESPONSE_CODE.InvalidResponseCode;
        }
        
    }
}
