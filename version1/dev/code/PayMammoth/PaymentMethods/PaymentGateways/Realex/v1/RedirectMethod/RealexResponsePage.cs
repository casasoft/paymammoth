﻿using System;
using System.Web;
using log4net;
using System.Text;
using PayMammoth.PaymentMethods.PaymentGateways.Realex.v1.Classes;
using PayMammoth.Util;

namespace PayMammoth.PaymentMethods.PaymentGateways.Realex.v1.RedirectMethod
{
    public abstract class RealexResponsePage : CS.WebComponentsGeneralV3.Code.Classes.Pages.BasePageMostBasic
    {
        private static ILog _log = log4net.LogManager.GetLogger(typeof(RealexResponsePage));

        public RealexResponsePage()
        {
            
            _log.Debug( "RealexResponsePage");
        }
        private void checkResponse()
        {
            ResponseChecker checker = new ResponseChecker();
            var response = checker.CheckResponse(this.Request.Form);
            if (response.Success)
            {
                checker_OnTransactionSuccessful(response);
            }
            else
            {
                checker_OnTransactionFailed(response);
            }

        }

        protected virtual void checker_OnTransactionFailed(RealexResponseCheckResponse response)
        {
            var realexResponse = response.RealexResponse;
            string orderID = "N/A";
            if (realexResponse != null)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("RealexResponsePage.checker_OnTransactionFailed - OrderID: " + realexResponse.OrderID + ", ProductID: " + realexResponse.ProductID);
                sb.AppendLine("Form: " + this.Request.Form);
                sb.AppendLine("Querystring: " + this.Request.QueryString);
                _log.Debug( sb.ToString());
            }

            
         
        }

        protected virtual void checker_OnTransactionSuccessful(RealexResponseCheckResponse response)
        {
            var realexResponse = response.RealexResponse;
            if (realexResponse != null)
            {
                _log.Debug( "RealexResponsePage.checker_OnTransactionSuccessful - OrderID: " + realexResponse.OrderID + ", ProductID: " + realexResponse.ProductID);
            }

        }
        
        protected override void OnInit(EventArgs e)
        {
            _log.Debug( "RealexResponsePage.onInit()");
            checkResponse();
            base.OnInit(e);
        }



        public override void UpdateTitle(string pageTitle, bool useOpenGraph = true)
        {
            
        }

        public override void UpdateTitleFromContentPage(BusinessLogic_v3.Frontend.ArticleModule.ArticleBaseFrontend page, bool useOpenGraph = true)
        {
           
        }

        public override void UpdateTitleAndMetaTags(string title, string metaKeywords, string metaDesc, bool useOpenGraph = true)
        {
           
        }

        public override void UpdateOpenGraphPageType(CS.General_v3.Enums.OPEN_GRAPH_TYPE type, bool overwrite = true)
        {
        
        }

        public override void UpdateOpenGraphTag(CS.General_v3.Enums.OPEN_GRAPH tag, string value, bool overwrite = true)
        {
        
        }
    }
}
