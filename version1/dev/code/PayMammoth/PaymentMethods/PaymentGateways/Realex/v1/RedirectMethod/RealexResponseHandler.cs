﻿using System;
using System.Web;
using log4net;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.Util;

namespace PayMammoth.PaymentMethods.PaymentGateways.Realex.v1.RedirectMethod
{
    public abstract class RealexResponseHandler : CS.WebComponentsGeneralV3.Code.Classes.HTTPHandlers.BaseHandler, IHttpHandler
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(RealexResponseHandler));
        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return true; }
        }

        private IPaymentRequest _request = null;
        public IPaymentRequest GetRequest()
        {
            if (_request == null)
            {
                var request = QuerystringData.GetPaymentRequestFromQuerystring(currentContext.Request.QueryString);
                _request = request;

            }
            return _request;
        }

        private void checkResponse()
        {
            var request = GetRequest();
            if (request != null)
            {
                ResponseChecker checker = new ResponseChecker();

                if (_log.IsDebugEnabled) _log.Debug(LogUtil.AddRequestIdentifierToLogMsg( request, "RealexResponseHandler.CheckResponse()"));
                checker.CheckResponse(currentContext.Request.Form);
            }
            else
            {
                _log.Info( "No request found - QS: " + currentContext.Request.QueryString.ToString() + " , Form: " + currentContext.Request.Form.ToString());
            }

        }


        private HttpContext currentContext = null;
        public RealexResponseHandler()
        {
            _log.Debug( "RealexResponseHandler()");
        }
        protected override void processRequest(HttpContext ctx)
        {

            currentContext = ctx;
            checkResponse();



            currentContext = null;
         
        }




        #endregion
    }
}
