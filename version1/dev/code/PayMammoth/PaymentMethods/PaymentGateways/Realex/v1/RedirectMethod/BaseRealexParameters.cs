﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Classes.Attributes;
using PayMammoth.Classes.RequestResponse;

using System.IO;
using PayMammoth.PaymentMethods.PaymentGateways.Realex.v1.Classes;

namespace PayMammoth.PaymentMethods.PaymentGateways.Realex.v1.RedirectMethod
{
    public abstract class BaseRealexParameters : BaseRequestResponse
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(RealexRequest));
        protected IRealexCredentials realexCredentials { get; private set; }
        public BaseRealexParameters(IRealexCredentials credentials)
        {
            this.realexCredentials = credentials;
            this.timeStampStr = CS.General_v3.Util.Date.Now.ToString("yyyyMMddHHmmss");

        }

        /// <summary>
        /// Supplied by Realex – note this is not the merchant number supplied by your bank.
        /// </summary>
        [PaymentInfoField(FieldName = "MERCHANT_ID", IsRequired = true, MaxLength = 50)]
        protected string realexMerchantID
        {
            get { return realexCredentials.MerchantId; }
        }
        


        /// <summary>
        /// Optional. The sub‐account to use for this transaction. If not present, the default sub‐account, ‘internet’, will be used. For testing, use 'internettest'
        /// </summary>
        [PaymentInfoField(FieldName = "ACCOUNT", MaxLength = 30)]
        public string RealexAccountName
        {
            get { return realexCredentials.AccountName; }
        }

        public void SetTestingAccountName()
        {
            
        }

        /// <summary>
        /// A unique alphanumeric id that’s used to identify the transaction. This can be up to 40 characters long. No spaces are allowed
        /// </summary>
        [PaymentInfoField(FieldName = "ORDER_ID", AllowOnlyAlphaNumericAndDigits = true, ExtraCharactersToAllow = "_-", MaxLength = 40, ThrowErrorIfExceedsLengths = false)]
        public string OrderID { get; set; }

        /// <summary>
        /// Total amount to authorise in the lowest unit of the currency – i.e. 100 euro would be entered as 10000. If there is no decimal in the currency then (e.g. JPY Yen) then contact Realex Payments. No decimal points are allowed.
        /// </summary>
        [PaymentInfoField(FieldName = "AMOUNT", IsRequired = true)]
        protected int? amountInCents { get; set; }

        public int AmountInCents
        {
            get { return this.amountInCents.GetValueOrDefault(); }
        }
        
        public double Amount
        {
            get { return (double)amountInCents.GetValueOrDefault() / 100; }
            set
            {
               
                amountInCents = (int)Math.Round(value * 100);
            }
        }

        [PaymentInfoField(FieldName = "CURRENCY", AllowOnlyAlphaNumericAndDigits=true, MinLength=3, MaxLength=3, IsRequired = true)]
        protected string currencyCode { get; set; }






        protected RealexEnums.CURRENCY_CODE _Currency = RealexEnums.CURRENCY_CODE.Euro;
        public RealexEnums.CURRENCY_CODE Currency
        {
            get
            {
                return _Currency;
            }
            set
            {
                _Currency = value;
                currencyCode = CS.General_v3.Util.EnumUtils.StringValueOf(value);

            }

        }


        [PaymentInfoField(FieldName = "TIMESTAMP", MinLength = 14, MaxLength = 14, IsRequired = true)]
        protected string timeStampStr {get;set;}
        /// <summary>
        /// A digital signature generated using the MD5 algorithm. You have the option of using the MD5 hash or the SHA‐1 hash (below). Realex prefer you use the SHA‐1 hash as it is more secure. See section
        /// </summary>
        [PaymentInfoField(FieldName = "SHA1HASH", MaxLength =40 , IsRequired = false)]
        protected virtual string SHA1Hash {get;set;}
       

        /// <summary>
        /// Used to signify whether or not you wish the transaction to be captured in the next batch or not. If set to “1” and assuming the transaction is authorised then it will automatically be settled in the next batch. If set to “0” then the merchant must use the realcontrol application to manually settle the transaction. This option can be used if a merchant wishes to delay the payment until after the goods have been shipped. Transactions can be settled for up to 115% of the original amount.
        /// </summary>
        [PaymentInfoField(FieldName = "AUTO_SETTLE_FLAG", MaxLength = 1, IsRequired = true)]
        public bool AutoSettleTransaction { get; set; }

        [PaymentInfoField(FieldName = "COMMENT1", MaxLength = 255, ThrowErrorIfExceedsLengths = false)]
        public string Comment1 { get; set; }

        [PaymentInfoField(FieldName = "COMMENT2", MaxLength = 255, ThrowErrorIfExceedsLengths = false)]
        public string Comment2 { get; set; }

        private string _shippingPostCode;

        [PaymentInfoField(FieldName = "SHIPPING_CODE", AllowOnlyAlphaNumericAndDigits = true, ExtraCharactersToAllow = ",.-/|", MaxLength = 30, ThrowErrorIfExceedsLengths = false)]
        public string ShippingPostCode
        {
            get { return _shippingPostCode; }
            set { _shippingPostCode = value; }
        }

        [PaymentInfoField(FieldName = "SHIPPING_CO", AllowOnlyAlphaNumericAndDigits = true, ExtraCharactersToAllow = ",.-", MaxLength = 30, ThrowErrorIfExceedsLengths = false)]
        public string ShippingCountry { get; set; }

        [PaymentInfoField(FieldName = "BILLING_CODE", AllowOnlyAlphaNumericAndDigits = true, ExtraCharactersToAllow = ",.-/|", MaxLength = 30, ThrowErrorIfExceedsLengths=false)]

        public string BillingPostCode { get; set; }

        [PaymentInfoField(FieldName = "BILLING_CO", AllowOnlyAlphaNumericAndDigits = true, ExtraCharactersToAllow = ",.-", MaxLength = 30, ThrowErrorIfExceedsLengths = false)]
        public string BillingCountry { get; set; }

        /// <summary>
        /// The customer number of the customer.
        /// </summary>
        [PaymentInfoField(FieldName = "CUST_NUM", AllowOnlyAlphaNumericAndDigits = true, ExtraCharactersToAllow = "+@_,.-", MaxLength = 50, ThrowErrorIfExceedsLengths = false)]
        public string CustomerNumber { get; set; }
        [PaymentInfoField(FieldName = "VAR_REF", AllowOnlyAlphaNumericAndDigits = true, ExtraCharactersToAllow = "+@_,.-", MaxLength = 50, ThrowErrorIfExceedsLengths = false)]
        public string ExtraReference { get; set; }
        [PaymentInfoField(FieldName = "PROD_ID", AllowOnlyAlphaNumericAndDigits = true, ExtraCharactersToAllow = "+@_,.-", MaxLength = 50, ThrowErrorIfExceedsLengths = false)]
        public string ProductID { get; set; }

    }
}
