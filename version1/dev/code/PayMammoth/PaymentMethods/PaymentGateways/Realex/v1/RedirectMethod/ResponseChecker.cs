﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.PaymentMethods.PaymentGateways.Realex.v1.Classes;
using log4net;
using PayMammoth.Modules.PaymentRequestTransactionModule;
using PayMammoth.Modules;
using PayMammoth.Util;

namespace PayMammoth.PaymentMethods.PaymentGateways.Realex.v1.RedirectMethod
{
    public class ResponseChecker
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(ResponseChecker));
        public delegate void RealexResponseHandler(RealexResponse response);


        private IPaymentRequestTransaction _paymentRequestTransaction = null;
        private IPaymentRequest paymentRequest
        {
            get 
            {
                if (_paymentRequestTransaction != null)
                    return _paymentRequestTransaction.PaymentRequest;
                else
                    return null;
            }
        }
        private bool loadTransaction()
        {
            string sOrderId = _form[RealexConstants.PARAM_FORM_ORDERID];
            long orderId;
            if (long.TryParse(sOrderId, out orderId))
            {
                _paymentRequestTransaction = Factories.PaymentRequestTransactionFactory.GetByPrimaryKey(orderId);
            }

            return (_paymentRequestTransaction != null);
            

        }

        private NameValueCollection _form = null;
        public RealexResponseCheckResponse CheckResponse(NameValueCollection form)
        {
            RealexResponseCheckResponse result = new RealexResponseCheckResponse();
            _form = form;

            if (_log.IsInfoEnabled) _log.Info(LogUtil.AddRequestIdentifierToLogMsg(paymentRequest, "Checking response - form: " + form.ToString()));
            if (loadTransaction())
            {
                //transaction found

                IRealexCredentials realexCredentials = paymentRequest.WebsiteAccount.GetRealexCredentials();
                paymentRequest.VerifyCurrentTransactionIsStill(PayMammoth.PaymentMethods.PaymentGateways.Realex.v1.RealexConstants.PaymentMethod);
                RealexResponse response = new RealexResponse(form, realexCredentials);
                result.RealexResponse = response;
                var t = _paymentRequestTransaction;
                
                t.SetPaymentParametersFromObject(response, null,
                                                     x => x.AutoSettleTransaction,
                                                     x => x.Amount,
                                                     x => x.BatchID,
                                                     x => x.ResponseCode,
                                                     x => x.RealexAccountName,
                                                     x => x.OrderID,
                                                     x => x.CardVerificationResult,
                                                     x => x.RealexReference,
                                                     x => x.TransactionAuthCode,
                                                     x => x.TransactionSuitabilityScore);
                t.Update();
                if (response.VerifySuccessfulTransaction())
                {
                    if ((long)response.AmountInCents == t.PaymentRequest.GetTotalPriceInCents())
                    {
                        result.Status = Enums.PaymentStatus.Success;
                        t.Reference = response.RealexReference;
                        t.MarkAsSuccessful(requiresManualPayment: false, authCode: response.TransactionAuthCode);
                        
                    }
                    else
                    {
                        result.Status = Enums.PaymentStatus.GenericError;
                        _log.Warn(LogUtil.AddRequestIdentifierToLogMsg(t.PaymentRequest,  "Realex hash verified, altough amounts did not match! Realex amount: " + response.Amount.ToString("0.00") + ", Transaction: " + t.PaymentRequest.GetTotalPriceInCents()));
                    }


                }
                else
                {
                    result.Status = RealexEnums.GetPayMammothStatusFromResponseCode(response.ResponseCode);
                    _log.Info(LogUtil.AddRequestIdentifierToLogMsg(paymentRequest, "Realex response failed: " + form.ToString()));
                }
                //
            }
            result.Transaction = this._paymentRequestTransaction;
            return result;
            
        }

    }
}
