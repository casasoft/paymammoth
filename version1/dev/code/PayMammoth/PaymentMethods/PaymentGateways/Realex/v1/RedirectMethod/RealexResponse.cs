﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Classes.Attributes;

using System.Collections.Specialized;
using PayMammoth.PaymentMethods.PaymentGateways.Realex.v1.Classes;
using PayMammoth.Util;

namespace PayMammoth.PaymentMethods.PaymentGateways.Realex.v1.RedirectMethod
{
    public class RealexResponse : BaseRealexParameters
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(RealexRequest));

        [PaymentInfoField(FieldName="PASREF")]
        public string RealexReference { get; private set; }

        [PaymentInfoField(FieldName = "AUTHCODE")]
        public string TransactionAuthCode { get; private set; }

        private string _responseCode = null;

        [PaymentInfoField(FieldName = "RESULT")]
        protected string responseCodeStr
        {
            get { return _responseCode; }
            set
            {
                _responseCode = value;
                this.ResponseCode = RealexEnums.GetResponseCodeFromString(_responseCode);
            }
        }

        public RealexEnums.RESPONSE_CODE ResponseCode { get; private set; }


        [PaymentInfoField(FieldName = "MESSAGE")]
        public string Message { get; private set; }


        private string _cardVerificationResultCode = null;

        [PaymentInfoField(FieldName = "CVNRESULT")]
        protected string cardVerificationResultCode
        {
            get { return _cardVerificationResultCode; }
            set 
            {
                _cardVerificationResultCode = value;
                if (!string.IsNullOrEmpty(_cardVerificationResultCode))
                {
                    this.CardVerificationResult = RealexEnums.GetCVNResultFromString(_cardVerificationResultCode);
                }
            }
        }
        
        public RealexEnums.CVN_RESULT? CardVerificationResult { get; private set; }

        [PaymentInfoField(FieldName = "BATCHID")]
        public string BatchID { get; private set; }

        [PaymentInfoField(FieldName = "TSS")]
        public string TransactionSuitabilityScore { get; private set; }

        public RealexResponse(NameValueCollection formData, IRealexCredentials realexCredentials) : base(realexCredentials)
        {
            
            this.ParseFromNameValueCollection(formData, null, null);
            if (logger.IsDebugEnabled)
            {
                logger.Debug( "Realex response - Form Data: " + CS.General_v3.Util.ListUtil.ConvertNameValueCollectionToString(formData));
                logger.Debug( "Response code: " + this.responseCodeStr + " | Msg: " + this.Message);
            }

        }

        public string GenerateSHA1Hash()
        {


            {
                var Request = System.Web.HttpContext.Current.Request;
                // variables to hold the values posted by Realex.
                string timestamp = Request.Form["TIMESTAMP"];
                string result = Request.Form["RESULT"];
                string orderID = Request.Form["ORDER_ID"];
                string message = Request.Form["MESSAGE"];
                string authcode = Request.Form["AUTHCODE"];
                string pasref = Request.Form["PASREF"];
                string realexHash = Request.Form["SHA1HASH"];

                // get the Realex settings from the web config
                string merchantID = this.realexMerchantID;
                string sharedSecret = this.realexCredentials.SecretWord;

                // generate the hash according to Realex's rules
                string temp1 = timestamp + "." + merchantID + "." + orderID + "." + result + "." + message + "." + pasref + "." + authcode;
                string temp2 = Dovetail.Realex.Realauth.RealexProcessor.ComputeSha1Hash(temp1);
                string temp2b = CS.General_v3.Util.Cryptography.GetSHA1Hash(temp1);
                string temp3 = temp2 + "." + sharedSecret;
                string hash2 = Dovetail.Realex.Realauth.RealexProcessor.ComputeSha1Hash(temp3);
            }


            string tempa = timeStampStr + "." + realexMerchantID + "." + OrderID + "." + responseCodeStr + "." + Message + "." + RealexReference + "." + TransactionAuthCode;
            string tempb = CS.General_v3.Util.Cryptography.GetSHA1Hash(tempa).ToLower();
            string tempc = tempb + "." + this.realexCredentials.SecretWord;
            string hash = CS.General_v3.Util.Cryptography.GetSHA1Hash(tempc).ToLower();

            return hash;

        }
        public bool VerifyHash()
        {
            string realexHash = this.SHA1Hash;

            string generateHash = this.GenerateSHA1Hash();
            bool match =(realexHash == generateHash);;
            
            
            logger.Debug( "Verify response hash - Realex Hash <" + realexHash + ">, Generated Hash <" + generateHash + ">, Match: " + (match ? "Yes" : "No"));
            return match;
            

        }
        public bool VerifySuccessfulTransaction()
        {
            return VerifyHash() && this.ResponseCode == RealexEnums.RESPONSE_CODE.Successful;
        }

    }
}
