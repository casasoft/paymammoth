﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.PaymentMethods.PaymentGateways.Realex.v1.Classes
{
    public class RealexCredentialsImpl : IRealexCredentials
    {
        #region IRealexCredentials Members

        public string MerchantId { get; set; }

        public string SecretWord { get; set; }

        public string AccountName { get; set; }

        #endregion
    }
}
