﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.URL;

namespace PayMammoth.PaymentMethods.PaymentGateways.Realex.v1
{
    public static class RealexUrls
    {


        public static string GetPrePaymentUrl(string identifier)
        {
            URLClass url = new URLClass(CS.General_v3.Util.PageUtil.GetApplicationBaseUrl() + "payment/gateways/realex/");
            url[PayMammoth.Connector.Constants.PARAM_IDENTIFIER] = identifier;
            return url.ToString();
        }

    }
}
