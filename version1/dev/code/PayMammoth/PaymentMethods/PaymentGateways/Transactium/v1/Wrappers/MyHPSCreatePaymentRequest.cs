﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Connector.Classes.Interfaces;

namespace PayMammoth.PaymentMethods.PaymentGateways.Transactium.v1.Wrappers
{
    public class MyHPSCreatePaymentRequest
    {
        private object _HPSCreatePaymentRequest = null;

        public long Amount { get; set; }
        public EnumsTransactium.CountryCodes ClientBillingCountry { get; set; }
        public string ClientEmail { get; set; }
        public string ClientIPRestriction { get; set; }
        public string ClientReference { get; set; }
        public EnumsTransactium.CurrencyCode Currency { get; set; }
        public string FailURL { get; set; }
        public string HPSProfileTag { get; set; }
        public string LanguageCode { get; set; }
        public int MaxTimeLimit { get; set; }
        public string OrderReference { get; set; }
        public EnumsTransactium.HPSPaymentType PaymentType { get; set; }
        public string SuccessURL { get; set; }
        public MyHPSCreatePaymentRequest()
        {

        }

        public void VerifyDetails()
        {
            {
                StringBuilder errMsg = new StringBuilder();

                //        if (transactiumProfileTag == null)
                ///          errMsg.AppendLine("Transactium Profile Tag cannot be null");
                //if (clientReference == null)
                //    errMsg.AppendLine("Client Ref. cannot be null");
                if (this.ClientReference != null && this.ClientReference.Length > 25)
                    errMsg.AppendLine("client Ref. <" + this.ClientReference + "> cannot be longer than 25 characters");
                if (this.OrderReference == null)
                    errMsg.AppendLine("Order Ref. cannot be null");
                if (this.OrderReference != null && this.OrderReference.Length > 25)
                    errMsg.AppendLine("Order Ref. <" + this.OrderReference + "> cannot be longer than 25 characters");
                if (this.ClientEmail != null && this.ClientEmail.Length > 250)
                    this.ClientEmail = this.ClientEmail.Substring(0, 250);
                //errMsg.AppendLine("client Email <" + clientEmail + "> cannot be longer than 250 characters");
                if (errMsg.Length > 0)
                {
                    throw new InvalidOperationException("Invalid Payment Information: \r\n" + errMsg.ToString());
                }
            }
        }
        public MyHPSCreatePaymentRequest(object HPSCreatePaymentRequest)
        {

            if (HPSCreatePaymentRequest is TransactiumWebService.v1.Live.HPSCreatePaymentRequest)
                FillFromLive((TransactiumWebService.v1.Live.HPSCreatePaymentRequest)HPSCreatePaymentRequest);
            else if (HPSCreatePaymentRequest is TransactiumWebService.v1.Staging.HPSCreatePaymentRequest)
                FillFromStaging((TransactiumWebService.v1.Staging.HPSCreatePaymentRequest)HPSCreatePaymentRequest);
            else
                throw new InvalidOperationException("Object must be either of type Live/Staging HPSCreatePaymentRequest");
            
        }


        public TransactiumWebService.v1.Live.HPSCreatePaymentRequest ConvertToLive()
        {
            if (!(_HPSCreatePaymentRequest is TransactiumWebService.v1.Live.HPSCreatePaymentRequest))
            {
                TransactiumWebService.v1.Live.HPSCreatePaymentRequest payment = new TransactiumWebService.v1.Live.HPSCreatePaymentRequest();
                payment.Amount = this.Amount;
                payment.ClientBillingCountry = EnumsTransactium.ConvertFromGeneralTransactiumEnumToSpecific<TransactiumWebService.v1.Live.CountryCodes>(this.ClientBillingCountry);
                payment.ClientEmail = this.ClientEmail;
                payment.ClientIPRestriction = this.ClientIPRestriction;
                payment.ClientReference = this.ClientReference;
                payment.Currency = EnumsTransactium.ConvertFromGeneralTransactiumEnumToSpecific<TransactiumWebService.v1.Live.CurrencyCode>(this.Currency);
                payment.FailURL = this.FailURL;
                payment.HPSProfileTag = this.HPSProfileTag;
                payment.LanguageCode = this.LanguageCode;
                payment.MaxTimeLimit = this.MaxTimeLimit;
                payment.OrderReference = this.OrderReference;
                payment.PaymentType = EnumsTransactium.ConvertFromGeneralTransactiumEnumToSpecific<TransactiumWebService.v1.Live.HPSPaymentType>(this.PaymentType);
                payment.SuccessURL = this.SuccessURL;
                return payment;
            }
            else
            {
                return (TransactiumWebService.v1.Live.HPSCreatePaymentRequest)_HPSCreatePaymentRequest;
            }
        }
        public TransactiumWebService.v1.Staging.HPSCreatePaymentRequest ConvertToStaging()
        {
            if (!(_HPSCreatePaymentRequest is TransactiumWebService.v1.Staging.HPSCreatePaymentRequest))
            {
                TransactiumWebService.v1.Staging.HPSCreatePaymentRequest payment = new TransactiumWebService.v1.Staging.HPSCreatePaymentRequest();
                payment.Amount = this.Amount;
                payment.ClientBillingCountry = EnumsTransactium.ConvertFromGeneralTransactiumEnumToSpecific<TransactiumWebService.v1.Staging.CountryCodes>(this.ClientBillingCountry);
                payment.ClientEmail = this.ClientEmail;
                payment.ClientIPRestriction = this.ClientIPRestriction;
                payment.ClientReference = this.ClientReference;
                payment.Currency = EnumsTransactium.ConvertFromGeneralTransactiumEnumToSpecific<TransactiumWebService.v1.Staging.CurrencyCode>(this.Currency);
                
                payment.FailURL = this.FailURL;
                payment.HPSProfileTag = this.HPSProfileTag;
                payment.LanguageCode = this.LanguageCode;
                payment.MaxTimeLimit = this.MaxTimeLimit;
                payment.OrderReference = this.OrderReference;
                payment.PaymentType = EnumsTransactium.ConvertFromGeneralTransactiumEnumToSpecific<TransactiumWebService.v1.Staging.HPSPaymentType>(this.PaymentType); 
                
                payment.SuccessURL = this.SuccessURL;
                return payment;
            }
            else
            {
                return (TransactiumWebService.v1.Staging.HPSCreatePaymentRequest)_HPSCreatePaymentRequest;
            }
        }


        public void FillFromLive(TransactiumWebService.v1.Live.HPSCreatePaymentRequest payment)
        {
            _HPSCreatePaymentRequest = payment;
            this.Amount = payment.Amount;
            this.ClientBillingCountry = EnumsTransactium.ConvertFromSpecificTransactiumEnumToGeneral<EnumsTransactium.CountryCodes>(payment.ClientBillingCountry); ;
            
            this.ClientEmail = payment.ClientEmail;
            this.ClientIPRestriction = payment.ClientIPRestriction;
            this.ClientReference = payment.ClientReference;
            this.Currency = EnumsTransactium.ConvertFromSpecificTransactiumEnumToGeneral<EnumsTransactium.CurrencyCode>(payment.Currency);
            this.FailURL = payment.FailURL;
            this.HPSProfileTag = payment.HPSProfileTag;
            this.LanguageCode = payment.LanguageCode;
            this.MaxTimeLimit = payment.MaxTimeLimit;
            this.OrderReference = payment.OrderReference;
            this.PaymentType = EnumsTransactium.ConvertFromSpecificTransactiumEnumToGeneral<EnumsTransactium.HPSPaymentType>(payment.PaymentType); 
            this.SuccessURL = payment.SuccessURL;
        }
        public void FillFromStaging(TransactiumWebService.v1.Staging.HPSCreatePaymentRequest payment)
        {
            _HPSCreatePaymentRequest = payment;
            this.Amount = payment.Amount;
            this.ClientBillingCountry = EnumsTransactium.ConvertFromSpecificTransactiumEnumToGeneral<EnumsTransactium.CountryCodes>(payment.ClientBillingCountry); ;
            this.ClientEmail = payment.ClientEmail;
            this.ClientIPRestriction = payment.ClientIPRestriction;
            this.ClientReference = payment.ClientReference;
            this.Currency = EnumsTransactium.ConvertFromSpecificTransactiumEnumToGeneral<EnumsTransactium.CurrencyCode>(payment.Currency);
            this.FailURL = payment.FailURL;
            this.HPSProfileTag = payment.HPSProfileTag;
            this.LanguageCode = payment.LanguageCode;
            this.MaxTimeLimit = payment.MaxTimeLimit;
            this.OrderReference = payment.OrderReference;
            this.PaymentType = EnumsTransactium.ConvertFromSpecificTransactiumEnumToGeneral<EnumsTransactium.HPSPaymentType>(payment.PaymentType); 
            this.SuccessURL = payment.SuccessURL;
        }

        public void FillDetailsFromTransaction(Modules.PaymentRequestTransactionModule.IPaymentRequestTransaction transaction)
        {
            var paymentDetails = this;
            paymentDetails.Amount = transaction.PaymentRequest.GetTotalPriceInCents(); //payment amount
            IPaymentRequestDetails req = transaction.PaymentRequest;
            paymentDetails.Currency = EnumsTransactium.GetCurrencyCodeFromISO( req.CurrencyCode); //payment currency
            paymentDetails.PaymentType = EnumsTransactium.HPSPaymentType.Sale;
            paymentDetails.HPSProfileTag = transaction.PaymentRequest.WebsiteAccount.PaymentGatewayTransactiumProfileTag; //payment profile
            paymentDetails.MaxTimeLimit = 0; //max time limit
            paymentDetails.OrderReference = transaction.ID.ToString(); //order reference
            paymentDetails.ClientReference = null; //client reference
            //TODO: KARL switch this back on when ip restriction is handled well. check with ::1
            paymentDetails.ClientIPRestriction = CS.General_v3.Util.PageUtil.GetUserIP(); //customer IP

            paymentDetails.ClientBillingCountry = EnumsTransactium.GetCountryCodeFromISO(req.ClientCountry).GetValueOrDefault( EnumsTransactium.CountryCodes.NotSet); //customer billing country code
            paymentDetails.ClientEmail = req.ClientEmail; //customer email address

            string successUrl = TransactiumUrls.GetSuccessUrl(transaction.PaymentRequest.Identifier);
            string failureUrl = TransactiumUrls.GetFailureUrl(transaction.PaymentRequest.Identifier);
            string paymentUrl = TransactiumUrls.GetPaymentStatusUrl(transaction.PaymentRequest.Identifier);

            paymentDetails.SuccessURL =  successUrl; //URL to redirect in case of success
            paymentDetails.FailURL = failureUrl; //URL to redirect in case of error

            

            paymentDetails.LanguageCode = ConstantsTransactium.GetLanguageCode(req.LanguageCode, req.LanguageCountryCode, req.LanguageSuffix);
            
            
        }
    }
}
