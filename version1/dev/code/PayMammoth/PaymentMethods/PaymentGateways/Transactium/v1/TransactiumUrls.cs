﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.URL;

namespace PayMammoth.PaymentMethods.PaymentGateways.Transactium.v1
{
    public class TransactiumUrls
    {



        public static string PaymentStatusUrl { get { return CS.General_v3.Util.PageUtil.GetApplicationBaseUrl() + "payment/transactium/v1/gatewayResult.aspx"; } }
        public static string PaymentPage { get { return CS.General_v3.Util.PageUtil.GetApplicationBaseUrl() + "payment/transactium/v1/"; } }
        public static string RedirectIFrameUrl { get { return CS.General_v3.Util.PageUtil.GetApplicationBaseUrl() + "payment/transactium/v1/redirectIFrame.aspx"; } }

        public static string GetSuccessUrl(string identifier)
        {
            URLClass url = new URLClass(RedirectIFrameUrl);
            url[PayMammoth.Connector.Constants.PARAM_IDENTIFIER] = identifier;
            url[Constants.PARAM_SUCCESS] = "Y";
            return url.ToString();
        }
        public static string GetFailureUrl(string identifier)
        {
            URLClass url = new URLClass(RedirectIFrameUrl);
            url[PayMammoth.Connector.Constants.PARAM_IDENTIFIER] = identifier;
            url[Constants.PARAM_SUCCESS] = "N";
            return url.ToString();
        }

        public static string GetPaymentStatusUrl(string identifier)
        {
            URLClass url = new URLClass(PaymentStatusUrl);
            url[PayMammoth.Connector.Constants.PARAM_IDENTIFIER] = identifier;
            return url.ToString();
        }

        public static string GetPaymentPageUrl(string identifier)
        {
            URLClass url = new URLClass(PaymentPage);
            url[PayMammoth.Connector.Constants.PARAM_IDENTIFIER] = identifier;
            return url.ToString();
            
        }


    }
}
