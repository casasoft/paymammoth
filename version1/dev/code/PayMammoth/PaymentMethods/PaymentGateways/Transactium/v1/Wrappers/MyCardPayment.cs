﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.PaymentMethods.PaymentGateways.Transactium.v1.Wrappers
{
    public class MyCardPayment
    {
        private object _CardPayment = null;
        public MyCardPayment()
        {
            

        }
        public MyCardPayment(object CardPayment)
        {


            if (CardPayment is TransactiumWebService.v1.Live.CardPayment)
                FillFromLive((TransactiumWebService.v1.Live.CardPayment)CardPayment);
            else if (CardPayment is TransactiumWebService.v1.Staging.CardPayment)
                FillFromStaging((TransactiumWebService.v1.Staging.CardPayment)CardPayment);
            else
                throw new InvalidOperationException("Object must be either of type Live/Staging CardPayment");

        }
        #region Properties
        public string AccountNumber { get; set; }
        public string BatchNumber { get; set; }
        public EnumsTransactium.CountryCodes BinCountry { get; set; }
        public string CardIdentifier { get; set; }
        public string CardNumber { get; set; }
        public EnumsTransactium.CVV2CheckResponseType CVV2CheckResponse { get; set; }
        public bool CVV2Supplied { get; set; }
        public EnumsTransactium.FSBlockReasonType FSBlockReason { get; set; }
        public string HostAuthorisationCode { get; set; }
        public string HostCode { get; set; }
        public string HostMessage { get; set; }
        public EnumsTransactium.CountryCodes IPCountry { get; set; }
        public DateTime TransactionDateTime { get; set; }
        public string TransactionID { get; set; }
        public EnumsTransactium.TransactionStatusType TransactionResultStatus { get; set; }
        #endregion
        public TransactiumWebService.v1.Live.CardPayment ConvertToLive()
        {
            if (!(_CardPayment is TransactiumWebService.v1.Live.CardPayment))
            {
                TransactiumWebService.v1.Live.CardPayment payment = new TransactiumWebService.v1.Live.CardPayment();
                
                payment.AccountNumber = this.AccountNumber;
                payment.BatchNumber = this.BatchNumber;
                payment.BinCountry = (TransactiumWebService.v1.Live.CountryCodes)(int)this.BinCountry;
                payment.CardIdentifier= this.CardIdentifier;
                payment.CardNumber= this.CardNumber;
                payment.CVV2CheckResponse = (TransactiumWebService.v1.Live.CVV2CheckResponseType)(int)this.CVV2CheckResponse;
                payment.CVV2Supplied = this.CVV2Supplied;
                payment.FSBlockReason = (TransactiumWebService.v1.Live.FSBlockReasonType)(int)this.FSBlockReason;
                payment.HostAuthorisationCode = this.HostAuthorisationCode;
                payment.HostCode = this.HostCode;
                payment.HostMessage = this.HostMessage;
                payment.IPCountry = (TransactiumWebService.v1.Live.CountryCodes)(int)this.IPCountry;
                payment.TransactionDateTime = this.TransactionDateTime;
                payment.TransactionID = this.TransactionID;
                payment.TransactionResultStatus = (TransactiumWebService.v1.Live.TransactionStatusType)(int)this.TransactionResultStatus;
                
                

                return payment;
            }
            else
            {
                return (TransactiumWebService.v1.Live.CardPayment)_CardPayment;
            }
        }

        public TransactiumWebService.v1.Staging.CardPayment ConvertToStaging()
        {
            if (!(_CardPayment is TransactiumWebService.v1.Staging.CardPayment))
            {
                TransactiumWebService.v1.Staging.CardPayment payment = new TransactiumWebService.v1.Staging.CardPayment();

                payment.AccountNumber = this.AccountNumber;
                payment.BatchNumber = this.BatchNumber;
                payment.BinCountry = (TransactiumWebService.v1.Staging.CountryCodes)(int)this.BinCountry;
                payment.CardIdentifier = this.CardIdentifier;
                payment.CardNumber = this.CardNumber;
                payment.CVV2CheckResponse = (TransactiumWebService.v1.Staging.CVV2CheckResponseType)(int)this.CVV2CheckResponse;
                payment.CVV2Supplied = this.CVV2Supplied;
                payment.FSBlockReason = (TransactiumWebService.v1.Staging.FSBlockReasonType)(int)this.FSBlockReason;
                payment.HostAuthorisationCode = this.HostAuthorisationCode;
                payment.HostCode = this.HostCode;
                payment.HostMessage = this.HostMessage;
                payment.IPCountry = (TransactiumWebService.v1.Staging.CountryCodes)(int)this.IPCountry;
                payment.TransactionDateTime = this.TransactionDateTime;
                payment.TransactionID = this.TransactionID;
                payment.TransactionResultStatus = (TransactiumWebService.v1.Staging.TransactionStatusType)(int)this.TransactionResultStatus;

                return payment;
            }
            else
            {
                return (TransactiumWebService.v1.Staging.CardPayment)_CardPayment;
            }
        }

        public void FillFromStaging(TransactiumWebService.v1.Staging.CardPayment payment)
        {
           _CardPayment = payment;

           this.AccountNumber = payment.AccountNumber;
           this.BatchNumber = payment.BatchNumber;
           this.BinCountry = (EnumsTransactium.CountryCodes)(int)payment.BinCountry;
           this.CardIdentifier = payment.CardIdentifier;
           this.CardNumber = payment.CardNumber;
           this.CVV2CheckResponse =  (EnumsTransactium.CVV2CheckResponseType)(int)payment.CVV2CheckResponse;
           this.CVV2Supplied = payment.CVV2Supplied;
           this.FSBlockReason = (EnumsTransactium.FSBlockReasonType)(int)payment.FSBlockReason;
           this.HostAuthorisationCode = payment.HostAuthorisationCode;
           this.HostCode = payment.HostCode;
           this.HostMessage = payment.HostMessage;
           this.IPCountry = (EnumsTransactium.CountryCodes)(int)payment.IPCountry;
           this.TransactionDateTime = payment.TransactionDateTime;
           this.TransactionID = payment.TransactionID;
           this.TransactionResultStatus = (EnumsTransactium.TransactionStatusType)(int)payment.TransactionResultStatus;

        }
        public void FillFromLive(TransactiumWebService.v1.Live.CardPayment payment)
        {
            _CardPayment = payment;

            this.AccountNumber = payment.AccountNumber;
            this.BatchNumber = payment.BatchNumber;
            this.BinCountry = (EnumsTransactium.CountryCodes)(int)payment.BinCountry;
            this.CardIdentifier = payment.CardIdentifier;
            this.CardNumber = payment.CardNumber;
            this.CVV2CheckResponse = (EnumsTransactium.CVV2CheckResponseType)(int) payment.CVV2CheckResponse;
            this.CVV2Supplied = payment.CVV2Supplied;
            this.FSBlockReason = (EnumsTransactium.FSBlockReasonType)(int)payment.FSBlockReason;
            this.HostAuthorisationCode = payment.HostAuthorisationCode;
            this.HostCode = payment.HostCode;
            this.HostMessage = payment.HostMessage;
            this.IPCountry = (EnumsTransactium.CountryCodes)(int)payment.IPCountry;
            this.TransactionDateTime = payment.TransactionDateTime;
            this.TransactionID = payment.TransactionID;
            this.TransactionResultStatus = (EnumsTransactium.TransactionStatusType)(int)payment.TransactionResultStatus;

        }
    }
}
