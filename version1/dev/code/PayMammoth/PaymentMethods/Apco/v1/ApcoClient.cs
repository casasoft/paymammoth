﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;

namespace PayMammoth.PaymentMethods.Apco.v1
{
    public partial class ApcoClient
    {
       
        
       
       
        
        public ApcoParameters Parameters {get;set;}
        /// <summary>
        /// Initialises Apco with the default secret word
        /// </summary>
        public ApcoClient()
        {
            this.Parameters = new ApcoParameters();
        }

        

        
        public string GatewayURL { get; set; }
        public void AddParametersToForm(System.Web.UI.Page pg)
        {
            AddParametersToForm(pg.Form);
            writeJavascriptToPage(pg);
        }

        private void writeJavascriptToPage(System.Web.UI.Page pg)
        {


        }
        private void addParamToForm(HtmlForm form, string id, string value)
        {

            HiddenField field = new HiddenField();
            field.ID = id;
            field.Value = value;
            form.Controls.Add(field);

        }

        public string GetParamsAsString()
        {
            XmlDocument doc = Parameters.ToXML();
            return doc.OuterXml;
        }

        private void AddParametersToForm(HtmlForm form)
        {
            if (string.IsNullOrEmpty(GatewayURL))
                GatewayURL = ApcoConstants.DEFAULT_GATEWAY_URL;
         /*   WebClient client = new WebClient();
            

            client.Headers.Add("Cache-Control", "no-cache");
            client.Headers.Add("Accept-Language", "en-us");
            client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
            */
            //XmlDocument doc = Parameters.ToXML();
            //_xmlContent = doc.OuterXml;
            //doc.Save(BASE_PATH + "test_xml.xml");
            
            addParamToForm(form, ApcoConstants.PARAM_PARAMS, GetParamsAsString());
            
        }
        public void FillFromApcoCredentials(ApcoCredentials cred)
        {
            this.Parameters.FillFromApcoCredentials(cred);
            

        }

    }

}
