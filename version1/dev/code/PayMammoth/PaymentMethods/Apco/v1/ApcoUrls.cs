﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.PaymentMethods.Apco.v1
{
    public static class ApcoUrls
    {
        private static string _baseFolder = "payment/apco/v1/";
        public static string GetStatusUrl(string identifier)
        {
            CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass(CS.General_v3.Util.PageUtil.GetBaseURL() + _baseFolder + "status.ashx");
            url[PayMammoth.Connector.Constants.PARAM_IDENTIFIER] = identifier;
            return url.ToString();
            
        }
        public static string GetReturnUrl(string identifier)
        {
            CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass(CS.General_v3.Util.PageUtil.GetBaseURL() + _baseFolder + "payment-success.aspx");
            url[PayMammoth.Connector.Constants.PARAM_IDENTIFIER] = identifier;
            url[PayMammoth.Constants.PARAM_SUCCESS] = true;
            
            string s = url.ToString();
            s += "&t=t";
            return s;
        }
        public static string GetPaymentIframeUrl(string identifier)
        {
            CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass(CS.General_v3.Util.PageUtil.GetBaseURL() + _baseFolder + "payment.aspx");
            url[PayMammoth.Connector.Constants.PARAM_IDENTIFIER] = identifier;
            return url.ToString();
        }
        public static string GetFailureUrl(string identifier)
        {
            CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass(CS.General_v3.Util.PageUtil.GetBaseURL() + _baseFolder + "payment-failure.aspx");
            url[PayMammoth.Connector.Constants.PARAM_IDENTIFIER] = identifier;
            url[PayMammoth.Constants.PARAM_SUCCESS] = false;
            string s = url.ToString();
            s += "&t=t";
            return s;
        }

        public static string GetPaymentUrl(string identifier)
        {
            CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass(CS.General_v3.Util.PageUtil.GetBaseURL() + _baseFolder);
            url[PayMammoth.Connector.Constants.PARAM_IDENTIFIER] = identifier;
            
            return url.ToString();
        }
    }
}
