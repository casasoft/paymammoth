﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web;
using PayMammoth.PaymentMethods.PayPal.v1.HelperClasses;
using PayMammoth.PaymentMethods.PayPal.v1.ExpressCheckout.NVP;
using log4net;

namespace PayMammoth.PaymentMethods.PayPal.v1.ExpressCheckout.InstantPaymentNotifications
{
    public class IPNMessage : BasePayPalObject
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(IPNMessage));
        [PayPalFieldInfo(FieldName="mc_gross")]
        public double GrossTotal { get; set; }

        [PayPalFieldInfo(FieldName = "invoice")]
        public string Reference { get; set; }

        [PayPalFieldInfo(FieldName = "protection_eligibility")]
        public string ProtectionEligibility { get; set; }

        [PayPalFieldInfo(FieldName = "address_status")]
        public string AddressStatus { get; set; }

        [PayPalFieldInfo(FieldName = "payer_id")]
        public string PayerID { get; set; }

        [PayPalFieldInfo(FieldName = "tax")]
        public double TaxTotal { get; set; }

        [PayPalFieldInfo(FieldName = "address_street")]
        public string AddressStreet1 { get; set; }

        [PayPalFieldInfo(FieldName = "payment_date")]
        public string PaymentDate { get; set; }

        [PayPalFieldInfo(FieldName = "payment_status")]
        protected string _PaymentStatus { get; set; }
        public PayPalEnums.PAYMENT_STATUS PaymentStatus
        {
            get
            {
                string s = _PaymentStatus;
                PayPalEnums.PAYMENT_STATUS status = PayPalEnums.PAYMENT_STATUS.None;
                try
                {
                    if (!string.IsNullOrEmpty(s))
                    {
                        status = (PayPalEnums.PAYMENT_STATUS) CS.General_v3.Util.EnumUtils.EnumValueOf(typeof (PayPalEnums.PAYMENT_STATUS), s);
                    }
                }
                catch (Exception ex)
                {
                    _log.Fatal("Could not parse <" + s + "> into a valid PayPal PAYMENT_STATUS enum value", ex);
                    
                }
                return status;
            }
        }

        [PayPalFieldInfo(FieldName = "address_zip")]
        public string AddressZIPCode { get; set; }

        [PayPalFieldInfo(FieldName = "mc_shipping")]
        public double ShippingTotal { get; set; }

        [PayPalFieldInfo(FieldName = "mc_handling")]
        public double HandlingTotal { get; set; }

        [PayPalFieldInfo(FieldName = "first_name")]
        public string ClientFirstName { get; set; }

        [PayPalFieldInfo(FieldName = "address_country_code")]
        public string AddressCountryCode { get; set; }

        [PayPalFieldInfo(FieldName = "address_name")]
        public string AddressPersonName { get; set; }

        [PayPalFieldInfo(FieldName = "notify_version")]
        public string NotifyVersion { get; set; }

        [PayPalFieldInfo(FieldName = "custom")]
        public string CustomField { get; set; }

        [PayPalFieldInfo(FieldName = "payer_status")]
        public string PayerStatus { get; set; }

        [PayPalFieldInfo(FieldName = "address_country")]
        public string AddressCountry { get; set; }

        [PayPalFieldInfo(FieldName = "num_cart_items")]
        public int TotalItemsInCart { get; set; }

        [PayPalFieldInfo(FieldName = "address_city")]
        public string AddressCity { get; set; }

        [PayPalFieldInfo(FieldName = "verify_sign")]
        public string VerifySignature { get; set; }

        [PayPalFieldInfo(FieldName = "payer_email")]
        public string PayerEmail { get; set; }

        [PayPalFieldInfo(FieldName = "txn_id")]
        public string PayPalTransactionID { get; set; }

        [PayPalFieldInfo(FieldName = "payment_type")]
        public string PaymentType { get; set; }

        [PayPalFieldInfo(FieldName = "last_name")]
        public string ClientLastName { get; set; }

        [PayPalFieldInfo(FieldName = "address_state")]
        public string AddressState { get; set; }

        [PayPalFieldInfo(FieldName = "receiver_email")]
        public string MerchantEmail { get; set; }

        [PayPalFieldInfo(FieldName = "shipping_discount")]
        public double ShippingDiscountTotal { get; set; }

        [PayPalFieldInfo(FieldName = "insurance_amount")]
        public double InsuranceTotalAmount { get; set; }

        [PayPalFieldInfo(FieldName = "receiver_id")]
        public string MerchantPaypalID { get; set; }

        [PayPalFieldInfo(FieldName = "pending_reason")]
        public string PendingReason { get; set; }

        [PayPalFieldInfo(FieldName = "txn_type")]
        protected string _transactionType { get; set; }
        public PayPalEnums.TRANSACTION_TYPE TransactionType
        {
            get
            {
                string s = _transactionType;
                PayPalEnums.TRANSACTION_TYPE status = PayPalEnums.TRANSACTION_TYPE.CouldNotParse;
                try
                {
                    if (!string.IsNullOrEmpty(s))
                    {
                        status = (PayPalEnums.TRANSACTION_TYPE)CS.General_v3.Util.EnumUtils.EnumValueOf(typeof(PayPalEnums.TRANSACTION_TYPE), s);
                    }
                }
                catch (Exception ex)
                {
                    _log.Fatal("Could not parse <" + s + "> into a valid PayPal TRANSACTION_TYPE enum value", ex);

                }
                return status;
            }
        }
        

        [PayPalFieldInfo(FieldName = "discount")]
        public double TotalDiscount { get; set; }

        [PayPalFieldInfo(FieldName = "mc_currency")]
        public string CurrencyCode { get; set; }

        [PayPalFieldInfo(FieldName = "residence_country")]
        public string ResidenceCountry { get; set; }

        [PayPalFieldInfo(FieldName = "shipping_method")]
        public string ShippingMethod { get; set; }

        [PayPalFieldInfo(FieldName = "transaction_subject")]
        public string TransactionSubject { get; set; }

        [PayPalFieldInfo(FieldName = "payment_gross")]
        public string PaymentGross { get; set; }


        public bool IsSuccess()
        {
            return PayPalEnums.CheckIfPaymentStatusIsSuccess(this.PaymentStatus);
        }
        public bool CheckIfRequiresManualIntervention()
        {
            return PayPalEnums.CheckIfPaymentStatusRequiresManualIntervention(this.PaymentStatus);
        }

    }
}
