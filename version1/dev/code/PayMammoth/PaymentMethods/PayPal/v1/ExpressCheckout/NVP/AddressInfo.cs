﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using PayMammoth.PaymentMethods.PayPal.v1.Util;
using PayMammoth.PaymentMethods.PayPal.v1.HelperClasses;
using PayMammoth.Classes.Interfaces;
using PayMammoth.Connector.Classes.Interfaces;

namespace PayMammoth.PaymentMethods.PayPal.v1.ExpressCheckout.NVP
{
    public class AddressInfo : BasePayPalObject, IClientDetails
    {
        [PayPalFieldInfo(FieldName = "ShipToName", MaxLength = 32)]
        public string CustomerName
        {
            get { return CS.General_v3.Util.Text.AppendStrings(" ", this._clientFirstName, _clientMiddleName, _clientLastName); }
            set 
            {
                _clientFirstName = value;
                _clientLastName = null;
                _clientMiddleName = null;
            }
        }
        [PayPalFieldInfo(FieldName = "ShipToStreet", MaxLength = 100)]
        public string Street1 { get; set; }
        [PayPalFieldInfo(FieldName = "ShipToStreet2", MaxLength = 100)]
        public string Street2
        {
            get { return CS.General_v3.Util.Text.AppendStrings(", ", _street2, _street3); }
            set
            {
                _street2 = value;
                _street3 = null;
            }

        }
        [PayPalFieldInfo(FieldName = "ShipToCity", MaxLength = 40)]
        public string City { get; set; }
        [PayPalFieldInfo(FieldName = "ShipToState", MaxLength = 40)]
        public string State { get; set; }
        [PayPalFieldInfo(FieldName = "ShipToZIP", MaxLength = 20)]
        public string ZIPCode { get; set; }

        [PayPalFieldInfo(FieldName = "ShipToCountryCode", MaxLength = 2)]
        private string _CountryCode_Str { get; set; }
        public CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166 CountryCode
        {
            get
            {
                return (CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166)CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_FromCode(_CountryCode_Str);
            }
            set
            {
                _CountryCode_Str = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_To2LetterCode(value);

            }
        }


        [PayPalFieldInfo(FieldName = "ShipToPhoneNum", MaxLength = 32)]
        public string ClientPhoneNumber { get; set; }


        private string _clientFirstName;
        private string _clientMiddleName;
        private string _clientLastName;

        private string _street2;
        private string _street3;

        #region IClientDetails Members

        string IClientDetails.ClientFirstName
        {
            get
            {
                return _clientFirstName;
                
            }
            set
            {
                _clientFirstName = value;
                
                
            }
        }

        string IClientDetails.ClientMiddleName
        {
            get
            {
                return _clientMiddleName;
                
            }
            set
            {
                _clientMiddleName = value;
                
            }
        }

        string IClientDetails.ClientLastName
        {
            get
            {
                return _clientLastName;
                
            }
            set
            {
                _clientLastName = value;
                
            }
        }

        string IClientDetails.ClientAddress1
        {
            get
            {
                return this.Street1;
                
            }
            set
            {
                this.Street1 = value;
                
            }
        }

        string IClientDetails.ClientAddress2
        {
            get
            {
                return _street2;
            }
            set
            {
                _street2 = value;
                
            }
        }

        string IClientDetails.ClientAddress3
        {
            get
            {
                return _street3;
                
            }
            set
            {
                _street3 = value;
                
            }
        }

        string IClientDetails.ClientLocality
        {
            get
            {
                return this.City;
                
            }
            set
            {
                this.City = value;
                
            }
        }

        CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? IClientDetails.ClientCountry
        {
            get
            {
                return this.CountryCode;
                
            }
            set
            {
                this.CountryCode = value.GetValueOrDefault(CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166.UnitedKingdom);
                
                
            }
        }

        string IClientDetails.ClientPostCode
        {
            get
            {
                return this.ZIPCode;
                
            }
            set
            {
                this.ZIPCode = value;
                
            }
        }

        string IClientDetails.ClientEmail
        {
            get
            {

                return null;
            }
            set
            {
                
            }
        }

        #endregion

        #region IClientDetails Members


        public string GetAddressAsOneLine(bool includeCountry)
        {
            return PayMammoth.Connector.Extensions.ClientDetailsExtensions.GetAddressAsOneLine(this, includeCountry);
            
        }


        #endregion

        #region IClientDetails Members

        string IClientDetails.ClientTelephone
        {
            get
            {
                return null;
            }
            set
            {
                
            }
        }

        string IClientDetails.ClientMobile
        {
            get
            {

                return null;
            }
            set
            {
                
            }
        }


        #endregion

        #region IClientDetails Members


        public string GetClientFullName()
        {
            return PayMammoth.Connector.Extensions.ClientDetailsExtensions.GetClientFullName(this);
            
        }

        #endregion

        #region IClientDetails Members


        public string ClientReference { get; set; }

        #endregion
    }
        
}
