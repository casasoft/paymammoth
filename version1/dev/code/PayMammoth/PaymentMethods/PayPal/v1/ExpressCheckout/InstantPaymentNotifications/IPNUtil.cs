﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using CS.General_v3.Util;
using log4net;
using NHibernate.Linq.Functions;

namespace PayMammoth.PaymentMethods.PayPal.v1.ExpressCheckout.InstantPaymentNotifications
{
    public static class IPNUtil
    {

        private static readonly ILog _log = LogManager.GetLogger(typeof(IPNUtil));

        public static bool ValidateIPNMessage(string msgBlock, string apiServerUrl, out string paypalResponse)
        {
            int retryCount = 0;
            int maxRetry = 3;
            int waitBetweenRetries = 15;//seconds

            _log.DebugFormat("ValidateIpnMessage - ApiServer: {0} | MsgBlock: {1}", apiServerUrl, msgBlock);
            bool ok = false;
            bool retry = false;
            paypalResponse = null;
            do
            {
                
            
                retryCount++;
                _log.DebugFormat("Retry Count #{0}", retryCount);
                if (retryCount > 1)
                {//wait a bit
                    System.Threading.Thread.Sleep(waitBetweenRetries * 1000);
                }
                retry = false;
                
                try
                {
                    string ipnMsg = PayPalConstants.PARAM_CMD + "=" + PayPalConstants.COMMAND_NOTIFY_VALIDATE + "&" + msgBlock;
                    var httpResponse = CS.General_v3.Util.Web.HTTPRequest(apiServerUrl, CS.General_v3.Enums.HTTP_METHOD.POST, null, ipnMsg, null);
                    string response = null;


                    Stream responseStream = httpResponse.GetResponseStream();
                    if (responseStream != null)
                    {
                        StreamReader sr = new StreamReader(responseStream);
                        response = sr.ReadToEnd();
                        sr.Close();
                    }




                    if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        if (response == PayPalConstants.IPN_VERIFIED)
                        {
                            ok = true;
                            if (_log.IsDebugEnabled) _log.Debug("ValidateIPNMessage() - IPN_Verified");
                        }
                        else
                        {
                            _log.Warn("ValidateIPNMessage() - IPN Not verified");
                        }
                    }
                    else
                    {
                        if (_log.IsWarnEnabled) _log.Warn("ValidateIPNMessage() - Status code shows error - [" + httpResponse.StatusCode + "]");

                    }
                    paypalResponse = response;
                }
                catch (Exception ex)
                {
                    if (ex is WebException)
                    {
                        retry = true;
                    }
                    _log.Warn("Error occured while trying to get response stream", ex);
                
                }

            } while (retry && retryCount < maxRetry);
            return ok;
        }
    }
}
