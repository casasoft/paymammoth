﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using PayMammoth.Modules.PaymentRequestModule;
using log4net;
using PayMammoth.Modules.PaymentRequestTransactionModule;
using PayMammoth.Util;
using CS.General_v3.URL;

namespace PayMammoth.PaymentMethods.PayPal.v1.ExpressCheckout.InstantPaymentNotifications
{
    public abstract class IPNHandler : CS.WebComponentsGeneralV3.Code.Classes.HTTPHandlers.BaseHandler, IHttpHandler
    {
        private ILog _log = LogManager.GetLogger(typeof(IPNHandler));
        private IPaymentRequest _request = null;
        protected virtual void TransactionSuccessful(IpnResponse response)
        {
            var t = _request.CurrentTransaction;
            var msg = response.IpnMessage;
            if (!t.Successful)
            {
                
                var qs = t.GetParametersAsQueryString();
                bool requiresManualPayment = PayPalEnums.CheckIfPaymentStatusRequiresManualIntervention(msg.PaymentStatus);
                t.MarkAsSuccessful(requiresManualPayment);
                using (var nhTransaction = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
                {
                    addIpnResponseToTransaction(qs, response);
                    
                    
                    qs.AddByProperty(msg, x => x.PayPalTransactionID);
                    qs.AddByProperty(msg, x => x.PaymentStatus);
                    qs.AddByProperty(msg, x => x.VerifySignature);
                    qs.AddByProperty(msg, x => x.PendingReason);
                    qs.AddByProperty(msg, x => x.NotifyVersion);
                    qs.AddByProperty(msg, x => x.TransactionSubject);
                    qs.AddByProperty(msg, x => x.TransactionType);
                    
                    t.SetPaymentParameters(qs);
                    nhTransaction.Commit();

                }
            }

        }
        private void addIpnResponseToTransaction(QueryString qs, IpnResponse response)
        {
            
            qs.Add("Ipn PayPal Response", response.PaypalResponse);



        }

        protected virtual void TransactionInvalid(IpnResponse response)
        {
            IPaymentRequestTransaction t = _request.CurrentTransaction;
            
            t.AddMessage("Invalid Transaction", CS.General_v3.Enums.STATUS_MSG_TYPE.Error);
            using (var nhTransaction = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
            {
                var qs = t.GetParametersAsQueryString();
                    
                addIpnResponseToTransaction(qs, response);
                t.SetPaymentParameters(qs);
                nhTransaction.Commit();

            }
        }
        protected override void processRequest(HttpContext context)
        {
            if (CS.General_v3.Util.Other.IsLocalTestingMachine)
            {
                //System.Threading.Thread.Sleep(5000);
            }
            _request = QuerystringData.GetPaymentRequestFromQuerystring();

            Encoding encoding = Encoding.UTF8;

            string qsContents = context.Request.QueryString.ToString();
            byte[] formContentBy = context.Request.BinaryRead(context.Request.ContentLength);
            string formContents = encoding.GetString(formContentBy);
            //string formContents = context.Request.Form.ToString();
            //formContents = CS.General_v3.Util.ListUtil.ConvertNameValueCollectionToString(context.Request.Form);

            if (_log.IsDebugEnabled) _log.Debug(LogUtil.AddRequestIdentifierToLogMsg( _request,  "ProcessRequest() - Started. Querystring: " + qsContents+ ", Form: " + formContents));
            
            var ipnClient = PayPalManager.Instance.GetIpnClient(_request.WebsiteAccount);
            IpnResponse response = ipnClient.VerifyTransaction(formContents);
            if (response.Success)
            {
                if (_log.IsDebugEnabled) _log.Debug(LogUtil.AddRequestIdentifierToLogMsg( _request, "ProcessRequest() - IPN_OK, Calling 'TransactionSuccessful'"));
                
                TransactionSuccessful(response);
                context.Response.Write(PayPalConstants.IPN_OK);
                if (_log.IsDebugEnabled) _log.Debug(LogUtil.AddRequestIdentifierToLogMsg( _request, "ProcessRequest() - IPN_OK, Ready'"));
            }
            else
            {

                if (_log.IsWarnEnabled) _log.Warn(LogUtil.AddRequestIdentifierToLogMsg( _request, "ProcessRequest() - IPN_FAIL, Calling 'TransactionInvalid'"));
                TransactionInvalid(response);
                context.Response.Write(PayPalConstants.IPN_FAIL);
                if (_log.IsWarnEnabled) _log.Warn(LogUtil.AddRequestIdentifierToLogMsg(_request, "ProcessRequest() - IPN_FAIL, Ready"));
            }
            if (_log.IsDebugEnabled) _log.Debug(LogUtil.AddRequestIdentifierToLogMsg( _request, "ProcessRequest() - Finished"));


        }

        public virtual bool IsReusable
        {
            get
            {
                return true;
            }
        }
    }
}
