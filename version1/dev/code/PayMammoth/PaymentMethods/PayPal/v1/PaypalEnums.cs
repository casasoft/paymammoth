﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace PayMammoth.PaymentMethods.PayPal.v1
{
    public static class PayPalEnums
    {
        public enum CONFIRM_RESULT
        {
            OK,
            Error

        }

        public enum ERROR_CODE
        {

        }
        public enum CHECKOUT_STATUS
        {
            PaymentActionNotInitiated,
            PaymentActionFailed,
            PaymentActionInProgress,
            PaymentCompleted
        }

        public enum PAYMENT_STATUS
        {
            None,
            [Description("Canceled-Reversal")]
            Cancelled_Reversal,
            Completed,
            Denied,
            Expired,
            Failed,
            [Description("In-Progress")]
            In_Progress,
            [Description("Partially-Refunded")]
            Partially_Refunded,
            Pending,
            Refunded,
            Reversed,
            Processed,
            Voided
        }
        public static bool CheckIfPaymentStatusIsSuccess(PAYMENT_STATUS status)
        {
            if (status == PAYMENT_STATUS.Completed || status == PAYMENT_STATUS.Pending)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool CheckIfPaymentStatusRequiresManualIntervention(PAYMENT_STATUS status)
        {
            if (status == PAYMENT_STATUS.Pending)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public enum PENDING_REASON
        {
            None,
            Address,
            Authorization,
            ECheck,
            [Description("intl")]
            International,
            /// <summary>
            /// You do not have a balance in the currency sent, and you do not have your Payment Receiving Preferences set to automatically convert and accept this payment. You must manually accept or deny this payment.
            /// </summary>
            [Description("multi-currency")]
            MultiCurrency,
            Order,
            PaymentReview,
            Unilateral,
            Verify,
            Other
        }
        public enum ACKNOWLEDGEMENT
        {
            [DescriptionAttribute("success")]
            Success,
            [DescriptionAttribute("successwithwarning")]
            SuccessWithWarning,
            [DescriptionAttribute("failure")]
            Failure,
            [DescriptionAttribute("failurewithwarning")]
            FailureWithWarning

        }
        /*



        public static ACKNOWLEDGEMENT AcknowledgementFromPaypalCode(string code)
        {
            if (code == null) code = "";
            code = code.ToLower();
            switch (code)
            {
                case "success": return ACKNOWLEDGEMENT.Success;
                case "successwithwarning": return ACKNOWLEDGEMENT.SuccessWithWarning;
                case "failure": return ACKNOWLEDGEMENT.Failure;
                case "failurewithwarning": return ACKNOWLEDGEMENT.FailureWithWarning;
            }
            return ACKNOWLEDGEMENT.Failure;
        }*/
        public enum ADDRESS_STATUS
        {
            [DescriptionAttribute("Confirmed")]
            Confirmed,
            [DescriptionAttribute("Unconfirmed")]
            UnConfirmed,
            [DescriptionAttribute("None")]
            None
        }
        
        public enum PAYER_STATUS
        {
            [DescriptionAttribute("verified")]
            Verified,
            [DescriptionAttribute("unverified")]
            UnVerified
        }

        public enum SOLUTION_TYPE
        {
            [DescriptionAttribute("Sole")]
            PaypalAccountOptional,
            [DescriptionAttribute("Mark")]
            PaypalAccountRequired
        }

        public enum CHANNEL_TYPE
        {
            [DescriptionAttribute("Merchant")]
            Merchant,
            [DescriptionAttribute("eBayItem")]

            EBayItem
        }
       

        public enum LANDING_PAGE_TYPE
        {
            [DescriptionAttribute("Billing")]
            Billing,
            [DescriptionAttribute("Login")]
            Login
        }

        public enum LANGUAGE
        {
            [DescriptionAttribute("AU")]
            Australian,
            [DescriptionAttribute("AT")]
            Austrian,
            [DescriptionAttribute("BE")]
            Belgium,
            [DescriptionAttribute("BR")]
            Brazilian,
            [DescriptionAttribute("CA")]
            Canadian,
            [DescriptionAttribute("CH")]
            Swiss,
            [DescriptionAttribute("CN")]
            Chinese,
            [DescriptionAttribute("DE")]
            German,
            [DescriptionAttribute("ES")]
            Spanish,
            [DescriptionAttribute("GB")]
            EnglishUK,
            [DescriptionAttribute("FR")]
            French,
            [DescriptionAttribute("IT")]
            Italian,
            [DescriptionAttribute("NL")]
            Dutch,
            [DescriptionAttribute("PL")]
            Polish,
            [DescriptionAttribute("PT")]
            Portuguese,
            [DescriptionAttribute("RU")]
            Russian,

            [DescriptionAttribute("US")]
            EnglishUS,
            [DescriptionAttribute("da_DK")]
            DanishDenmark,

            [DescriptionAttribute("he_IL")]
            Hebrew,

            [DescriptionAttribute("id_ID")]
            Indonesian,

            [DescriptionAttribute("jp_JP")]
            Japanese,

            [DescriptionAttribute("no_NO")]
            Norwegian,

            [DescriptionAttribute("pt_BR")]
            BrazilianPortuguese,

            [DescriptionAttribute("ru_RU")]
            RussianLithuaniaLatviaUkraine,

            [DescriptionAttribute("sv_SE")]
            Swedish,

            [DescriptionAttribute("th_TH")]
            Thai,

            [DescriptionAttribute("tr_TR")]
            Turkish,

            [DescriptionAttribute("zh_CN")]
            SimplifiedChineseChina,

            [DescriptionAttribute("zh_HK")]
            TraditionalChineseHongKong,
            [DescriptionAttribute("zh_TW")]
            TraditionalChineseTaiWan,
            

        }

        public enum PAYMENT_ACTION
        {
            [DescriptionAttribute("Sale")]
            Sale,
            [DescriptionAttribute("Authorization")]
            Authorization,
            [DescriptionAttribute("Order")]
            Order
        }

        public static LANGUAGE LanguageFromISO(CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639? language, CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? countryLanguage)
        {
            LANGUAGE? result = null;
            if (countryLanguage.HasValue)
            {
                switch(countryLanguage.Value)
                {
                    case  CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166.Belgium: result = LANGUAGE.Belgium;break;
                    case CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166.Australia: result = LANGUAGE.Australian; break;
                    case CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166.Canada: result = LANGUAGE.Canadian; break;
                    case CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166.Brazil: result = LANGUAGE.Brazilian; break;
                    case CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166.Switzerland: result = LANGUAGE.Swiss;break;
                    case CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166.China: result = LANGUAGE.Chinese;break;
                    
                    case CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166.Germany: result = LANGUAGE.German;break;
                    case CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166.Spain: result = LANGUAGE.Spanish;break;
                    case CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166.UnitedKingdom: result = LANGUAGE.EnglishUK; break;
                    case CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166.France: result = LANGUAGE.French; break;
                    case CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166.Italy: result = LANGUAGE.Italian; break;
                    case CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166.Netherlands: result = LANGUAGE.Dutch; break;
                    case CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166.Poland: result = LANGUAGE.Polish; break;
                    case CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166.Portugal: result = LANGUAGE.Portuguese; break;
                    case CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166.RussianFederation: result = LANGUAGE.Russian; break;
                    case CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166.UnitedStates: result = LANGUAGE.EnglishUS; break;

                    case CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166.HongKong: result = LANGUAGE.TraditionalChineseHongKong; break;
                    case CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166.Taiwan: result = LANGUAGE.TraditionalChineseTaiWan; break;

                    case CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166.Latvia:
                    case CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166.Lithuania:
                    case CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166.Ukraine: result = LANGUAGE.RussianLithuaniaLatviaUkraine; break;

                }
            }
            if (!result.HasValue)
            {
                result = LANGUAGE.EnglishUK; //default language
                if (language.HasValue)
                {
                    switch (language.Value)
                    {
                        case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.English: result = LANGUAGE.EnglishUK; break;
                        case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Chinese: result = LANGUAGE.EnglishUK; break;
                        case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.German: result = LANGUAGE.EnglishUK; break;
                        case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Spanish: result = LANGUAGE.EnglishUK; break;
                        case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.French: result = LANGUAGE.EnglishUK; break;
                        case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Italian: result = LANGUAGE.EnglishUK; break;
                        case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Dutch: result = LANGUAGE.EnglishUK; break;
                        case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Polish: result = LANGUAGE.EnglishUK; break;
                        case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Portuguese: result = LANGUAGE.EnglishUK; break;
                        case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Russian: result = LANGUAGE.EnglishUK; break;

                        case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Danish: result = LANGUAGE.DanishDenmark; break;
                        case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Hebrew: result = LANGUAGE.Hebrew; break;
                        case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Indonesian: result = LANGUAGE.Indonesian; break;
                        case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Japanese: result = LANGUAGE.Japanese; break;
                        case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Norwegian:
                        case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.NorwegianBokmål: 
                        case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.NorwegianNynorsk: result = LANGUAGE.Norwegian; break;
                        case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Lithuanian:
                        case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Latvian: 
                        case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Ukrainian: result = LANGUAGE.RussianLithuaniaLatviaUkraine; break;
                        case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Swedish: result = LANGUAGE.Swedish; break;
                        case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Thai: result = LANGUAGE.Thai; break;
                        case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.Turkish: result = LANGUAGE.Turkish; break;
                        case CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639.ChineseSimplified: result = LANGUAGE.SimplifiedChineseChina; break;
                        
                        


                    }
                }
            }
            return result.Value;
            
        }

        public enum TRANSACTION_TYPE
        {
            [DescriptionAttribute("adjustment")]
            Adjustment,

            [DescriptionAttribute("cart")]
            Cart,
            [DescriptionAttribute("express-checkout")]
            ExpressCheckout,
            [DescriptionAttribute("masspay")]
            MassPay,
            [DescriptionAttribute("mp_signup")]
            CreatedABillingAgreement,
            [DescriptionAttribute("merch_pmt")]
            MonthlySubscriptionPaidForWebsitePaymentsPro,
            [DescriptionAttribute("new_case")]
            NewDisputeFiled,
            [DescriptionAttribute("recurring_payment")]
            RecurringPaymentReceived,
            [DescriptionAttribute("recurring_payment_expired")]
            RecurringPaymentExpired,
            [DescriptionAttribute("recurring_payment_profile_created")]
            RecurringPaymentProfileCreated,
            [DescriptionAttribute("recurring_payment_skipped")]
            RecurringPaymentSkipped,
            [DescriptionAttribute("send_money")]
            SendMoney,
            [DescriptionAttribute("subscr_cancel")]
            SubscriptionCancelled,
            [DescriptionAttribute("subscr_eot")]
            SubscriptionExpired,
            [DescriptionAttribute("subscr_failed")]
            SubscriptionSignupFailed,
            [DescriptionAttribute("subscr_modify")]
            SubscriptionModified,
            [DescriptionAttribute("subscr_payment")]
            SubscriptionPaymentReceived,
            [DescriptionAttribute("subscr_signup")]
            SubscriptionStarted,
            [DescriptionAttribute("virtual_terminal")]
            PaymentReceivedFromVirtualTerminal,
            [DescriptionAttribute("web_accept")]
            PaymentReceivedFromBuyNow_Donation_AuctionSmartLogos,
            CouldNotParse

        }
        /*
        public static TRANSACTION_TYPE TransactionTypeFromPayPalCode(string code)
        {
            if (code == null) code = "";
            code = code.ToLower();
            switch (code)
            {
                case "cart":
                    return TRANSACTION_TYPE.Cart;
                case "express-checkout": return TRANSACTION_TYPE.ExpressCheckout;
            }
            return TRANSACTION_TYPE.ExpressCheckout;
        }*/
        public enum PAYMENT_TYPE
        {
            [DescriptionAttribute("none")]
            None,
            [DescriptionAttribute("echeck")]
            eCheck,
            [DescriptionAttribute("instant")]
            Instant

        }
        /*
        public static PAYMENT_TYPE PaymentTypeFromPayPalCode(string code)
        {
            if (code == null) code = "";
            code = code.ToLower();
            switch (code)
            {
                case "none": return PAYMENT_TYPE.None;
                case "echeck": return PAYMENT_TYPE.eCheck;
                case "instant": return PAYMENT_TYPE.Instant;
            }
            return PAYMENT_TYPE.None;
        }*/
    }
}
