﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using PayMammoth.PaymentMethods.PayPal.v1.HelperClasses;
using System.Reflection;
using System.Collections;
namespace PayMammoth.PaymentMethods.PayPal.v1.Util
{
    public static class GeneralUtil
    {
        public static string ConvertObjectToPayPal(object o)
        {
            string s = null;
            if (o != null)
            {
                if (o is double)
                {
                    double d = (double)o;
                    s = d.ToString("0.00").Replace(",", ".");
                }
                else if (o is bool)
                {
                    bool b = (bool)o;
                    if (b)
                        s = "1";
                    else
                        s = "0";
                }
                else
                {
                    s = o.ToString();
                }
            }
            return s;
        }
        public static void AddItemToNVForPaypal(NameValueCollection nv, string title, object o)
        {
            string s = ConvertObjectToPayPal(o);
            
            if (!string.IsNullOrEmpty(s))
            {
                nv[title] = s;
            }
        }
        public static void AddPaypalFieldsToNameValueColl(NameValueCollection nv, object obj, string keyPrepend, string keyAppend)
        {

            var properties = CS.General_v3.Util.ReflectionUtil.GetAllPropertiesWithAttributes(obj.GetType(), typeof(PayPalFieldInfoAttribute));
            foreach (var pInfo in properties)
            {
                PayPalFieldInfoAttribute fieldInfo = pInfo.Attributes.Cast<PayPalFieldInfoAttribute>().FirstOrDefault();
                fieldInfo.AddToNameValueCollection(pInfo.Property, pInfo.Property.GetValue(obj, null), keyPrepend,keyAppend,  nv);
            }
        }
        public static void FillObjectFromPaypalNameValueColl(NameValueCollection nv, object obj, string keyPrepend, string keyAppend)
        {
            var properties = CS.General_v3.Util.ReflectionUtil.GetAllPropertiesWithAttributes(obj.GetType(), typeof(PayPalFieldInfoAttribute));
            foreach (var pInfo in properties)
            {
                PayPalFieldInfoAttribute fieldInfo = pInfo.Attributes.Cast<PayPalFieldInfoAttribute>().FirstOrDefault();
                object val = fieldInfo.GetValueFromNameValueCollection(pInfo.Property, keyPrepend, keyAppend, nv);
                if (pInfo.Property.CanWrite)
                {
                    pInfo.Property.SetValue(obj, val, null);
                }
                
            }
        }
        public static object ConvertPaypalStringToDataType(string s, Type t)
        {
            object val = null;
            if (!string.IsNullOrEmpty(s))
            {
                
                if (t.IsAssignableFrom(typeof(double)) || t.IsAssignableFrom(typeof(int)))
                {
                    s = s.Trim();
                    double d = Convert.ToDouble(s);
                    val = d;
                    if (t.IsAssignableFrom(typeof(int)))
                        val = (int)d;
                }
                
                else if (t.IsAssignableFrom(typeof(bool)))
                {
                    s = s.Trim();
                    bool b = CS.General_v3.Util.Other.TextToBool(s);
                    val = b;
                }
                else if (t.IsAssignableFrom(typeof(string)))
                {
                    val = s;

                }
                

                else
                    throw new InvalidOperationException("Invalid type!");
            }
            return val;
        }

    }
}
