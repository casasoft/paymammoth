﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Classes.Interfaces;
using PayMammoth.PaymentMethods.PayPal.v1.ExpressCheckout.NVP;
using PayMammoth.Modules.WebsiteAccountModule;

using PayMammoth.PaymentMethods.PayPal.v1.HelperClasses;
using PayMammoth.Classes.Other;
using PayMammoth.Modules.PaymentRequestModule;
using CS.General_v3.Classes.URL;
using PayMammoth.Modules.PaymentRequestTransactionModule;
using PayMammoth.PaymentMethods.PayPal.v1.ExpressCheckout.InstantPaymentNotifications;
using CS.General_v3.Classes.HelperClasses;
using PayMammoth.Connector.Classes.Interfaces;
using PayMammoth.Modules;
using log4net;

namespace PayMammoth.PaymentMethods.PayPal.v1
{
    public class PayPalManager

    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(PayPalManager));
            public static PayPalManager Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<PayPalManager>(); } }
        private PayPalManager()
        {
            
        }
        public PayPalClient GetPayPalClient(IWebsiteAccount account)
        {
            IPayPalSettings settings = account.GetPaypalSettings();
            return new PayPalClient(settings);
        }
        public IPNClient GetIpnClient(IWebsiteAccount account)
        {
            IPayPalSettings settings = account.GetPaypalSettings();
            return new IPNClient(settings);
        }
        /// <summary>
        /// Starts an express checkout response.  If successful, redirects to paypal page
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public SetExpressCheckoutResponse StartExpressCheckout(IPaymentRequestTransaction transaction, bool autoRedirect = true)
        {
            IPaymentRequest request = transaction.PaymentRequest;
            var client = GetPayPalClient(request.WebsiteAccount);
            string returnUrl = null;
            {
                URLClass redirectUrlClass = new URLClass(PayPalUrls.ConfirmUrl);
                redirectUrlClass[PayMammoth.Connector.Constants.PARAM_IDENTIFIER] = request.Identifier;
                returnUrl = redirectUrlClass.ToString();
                     
            }

            string paymentChoicePage = Urls.GetPaymentChoiceUrl(request.Identifier);

            SetExpressCheckoutRequest expCheckoutRequest = new SetExpressCheckoutRequest(client, returnUrl, paymentChoicePage);
            PaymentDetailsInfo paymentInfo = expCheckoutRequest.AddNewPaymentDetails();
            expCheckoutRequest.Language = PayPalEnums.LanguageFromISO(((IPaymentRequestDetails)request).LanguageCode, ((IPaymentRequestDetails)request).LanguageCountryCode);
            string notifyUrl = null;
            {
                URLClass urlClass = new URLClass(PayPalUrls.IpnHandlerUrl);
                urlClass[PayMammoth.Connector.Constants.PARAM_IDENTIFIER] = request.Identifier;
                notifyUrl = urlClass.ToString();
            }

            paymentInfo.NotifyUrl = notifyUrl;
            CS.General_v3.Util.ReflectionUtil.CopyProperties<IClientDetails>(request, paymentInfo.AddressInfo);
            paymentInfo.CurrencyCode = ((IPaymentRequestDetails)request).CurrencyCode;
            IPaymentRequestDetails requestDetails = request;

            paymentInfo.Description = requestDetails.Description;
            paymentInfo.HandlingAmount = requestDetails.HandlingAmount;
            paymentInfo.ReferenceNum = request.CurrentTransaction.ID.ToString();
            paymentInfo.ShippingAmount = requestDetails.ShippingAmount;

            
            var paymentItemInfo = paymentInfo.AddNewPaymentItem();
            paymentItemInfo.Description = requestDetails.Description;
            paymentItemInfo.PriceExcTaxPerUnit = requestDetails.PriceExcTax;
            paymentItemInfo.TaxAmountPerUnit = requestDetails.TaxAmount;
            paymentItemInfo.Name = requestDetails.Title;
            paymentItemInfo.Quantity = 1;

            //paymentInfo.DisplaySettings.CartBorderColor = "FF0000";
            //paymentInfo.DisplaySettings.HeaderImageUrl = CS.General_v3.Util.PageUtil.GetBaseURL() + "images/paypal/logo1.jpg";
            //paymentInfo.DisplaySettings.ImageUrl = "https://www.ibm.com/developerworks/collaboration/uploads/idsteam/HTTPS1.JPG";
            //paymentInfo.DisplaySettings.LanguageCode = "fr";
            //paymentInfo.DisplaySettings.LogoImageUrl = CS.General_v3.Util.PageUtil.GetBaseURL() + "images/paypal/logo3.jpg";

            SetExpressCheckoutResponse paypalResponse = expCheckoutRequest.GetResponse();

            if (paypalResponse.Success)
            {
                using (var t = BusinessLogic_v3.Util.nHibernateUtil.BeginTransactionFromSessionInCurrentContext())
                {
                    transaction.Reference = paypalResponse.Token;
                    paypalResponse.UpdateTransactionWithInfo(transaction);
                    
                    transaction.Save();
                    t.CommitIfActiveElseFlush();
                }
                if (autoRedirect)
                {
                    paypalResponse.RedirectBrowserToPaypalCheckout();
                }
            }

            return paypalResponse;


        }
        public bool ConfirmRequestIsStillPaypal(IPaymentRequest request)
        {
            if (request.CurrentTransaction != null && request.CurrentTransaction.PaymentMethod == PayMammoth.Connector.Enums.PaymentMethodSpecific.PayPal_ExpressCheckout)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public PayPalEnums.CONFIRM_RESULT ConfirmPayment(IPaymentRequest request)
        {
            PayPalEnums.CONFIRM_RESULT result = PayPalEnums.CONFIRM_RESULT.Error;
            if (_log.IsDebugEnabled) _log.Debug("Confirm Payment - Start");
            //OperationResult result = new OperationResult();
            PayPalDoExpressCheckoutConfirmer paypalRequest = new PayPalDoExpressCheckoutConfirmer(GetPayPalClient(request.WebsiteAccount));
            bool ok = paypalRequest.ConfirmPayment();
            if (ok)
            {

                ok = request.WaitUntilRequestIsMarkedAsPaid();
                //request =  Factories.PaymentRequestFactory.WaitUntilRequestIsMarkedAsPaid(request);
                ok= request.CheckIfPaid();
                result = PayPalEnums.CONFIRM_RESULT.OK;
            }
            else
            {
                
                
                //problem
            }
            if (!ok)
            {
                    
                if (_log.IsDebugEnabled) _log.Debug("Confirm Payment - Error occurred [Code: " + paypalRequest.ResponseStatus.ErrorCode + ", Short Msg: " + paypalRequest.ResponseStatus.ShortMessage
                    + ", Long Msg: " + paypalRequest.ResponseStatus.LongMessage);
                

            }
            else
            {
                if (_log.IsDebugEnabled) _log.Debug("Payment confirmed");
            }

            if (_log.IsDebugEnabled) _log.Debug("Confirm Payment - Finish");
            return result;

        }

    }
}
