﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.PaymentMethods.PayPal.v1
{
    public static class PayPalConstants
    {
        public const string IPN_OK = "OK";
        public const string IPN_FAIL = "FAIL";
        public const string IPN_VERIFIED = "VERIFIED";
        public const string PARAM_CMD = "cmd";
        public const string PARAM_TOKEN = "token";
        public const string PARAM_USER = "user";
        public const string PARAM_PASSWORD = "pwd";
        public const string PARAM_SIGNATURE = "signature";
        public const string PARAM_VERSION = "version";
        //public const string APIVersion = "65.0";
        public const string APIVersion = "84.0";
        
        public const string COMMAND_NOTIFY_VALIDATE = "_notify-validate";
        public const string COMMAND_EXPRESS_CHECKOUT = "_express-checkout";


    }
}
