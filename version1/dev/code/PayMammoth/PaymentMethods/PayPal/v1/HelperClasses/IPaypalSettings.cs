﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.PaymentMethods.PayPal.v1.HelperClasses
{
    public interface IPayPalSettings
    {
        IPayPalCredentials SandboxCredentials { get;  }
        IPayPalCredentials LiveCredentials { get; }
        bool UseLiveEnvironment { get; }
        
        
    }
}
