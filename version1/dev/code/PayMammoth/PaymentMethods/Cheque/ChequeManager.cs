﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Frontend.PaymentRequestModule;
using PayMammoth.Modules._AutoGen.PaymentRequestModule;
using PayMammoth.Modules.PaymentRequestModule;
using BusinessLogic_v3.Extensions;
using PayMammoth.Extensions;

namespace PayMammoth.PaymentMethods.Cheque
{
    public class ChequeManager
    {
        public static ChequeManager Instance { get { return CS.General_v3.Classes.Singletons.Singleton.GetInstance<ChequeManager>(); } }

        public void ConfirmByCheque(IPaymentRequest request)
        {
            CS.General_v3.Util.ContractsUtil.RequiresNotNullable(request, "Request is required");

            

            var transaction = request.StartNewTransaction(PayMammoth.Connector.Enums.PaymentMethodSpecific.Cheque);
            transaction.MarkAsSuccessful(requiresManualIntervention: true);

            PaymentRequestFrontend requestFrontend = request.ToFrontend();
                
            requestFrontend.RedirectToSuccessUrl();

        }

    }
}
