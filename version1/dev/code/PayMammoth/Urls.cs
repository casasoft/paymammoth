﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth
{
    public static class Urls
    {

        public static string Url_Payment_Choice { get { return CS.General_v3.Util.PageUtil.GetApplicationBaseUrl() + "payment/"; } }
        public static string GetPaymentChoiceUrl(string identifier)
        {
            CS.General_v3.Classes.URL.URLClass urlClass = new CS.General_v3.Classes.URL.URLClass(Url_Payment_Choice);
            urlClass[PayMammoth.Connector.Constants.PARAM_IDENTIFIER] = identifier;
            return urlClass.ToString();
        }

        public static string GetSuccessUrl()
        {
            CS.General_v3.Classes.URL.URLClass urlClass = new CS.General_v3.Classes.URL.URLClass(Url_Payment_Choice);
            urlClass[Constants.PARAM_SUCCESS] = "yes";
            return urlClass.ToString();

        }
        public static string GetFailureUrl()
        {
            CS.General_v3.Classes.URL.URLClass urlClass = new CS.General_v3.Classes.URL.URLClass(PayMammoth.PaymentMethods.PaymentGateways.Transactium.v1.TransactiumUrls.PaymentPage);
            urlClass[Constants.PARAM_SUCCESS] = "no";
            return urlClass.ToString();

        }
        public static string Url_Test_PaymentSuccess { get { return CS.General_v3.Util.PageUtil.GetApplicationBaseUrl() + "testing/testpayment-success.aspx"; } }
        public static string Url_Test_PaymentFailure { get { return CS.General_v3.Util.PageUtil.GetApplicationBaseUrl() + "testing/testpayment-failure.aspx"; } }
        public static string Url_Test_OrderLinkOnClient { get { return CS.General_v3.Util.PageUtil.GetApplicationBaseUrl() + "testing/test-orderlinkonclient.aspx"; } }
       // public static string Url_Payment_Cancelled { get { return CS.General_v3.Util.PageUtil.GetBaseURL() + "payment/?cancelled=true"; } }
        public static string Url_Payment_Cheque_Info { get { return CS.General_v3.Util.PageUtil.GetApplicationBaseUrl() + "payment/cheque/"; } }
    }
}
