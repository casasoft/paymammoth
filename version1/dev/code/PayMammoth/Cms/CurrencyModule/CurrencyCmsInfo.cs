using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.CurrencyModule;

namespace PayMammoth.Cms.CurrencyModule
{

//UserCmsInfo-Class

    public class CurrencyCmsInfo : PayMammoth.Modules._AutoGen.CurrencyCmsInfo_AutoGen
    {
    	
        public CurrencyCmsInfo(BusinessLogic_v3.Modules.CurrencyModule.CurrencyBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
    }
}
