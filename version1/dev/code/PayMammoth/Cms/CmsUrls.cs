﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Modules.WebsiteAccountModule;

namespace PayMammoth.Cms
{
    public static class CmsUrls
    {
        public static string GetWebsiteAccount_MakeTestPaymentUrl(IWebsiteAccount account)
        {
            string s = PayMammoth.Cms.WebsiteAccountModule.WebsiteAccountCmsFactory.Instance.GetCmsFolderUrl() + "MakeTestPayment.aspx";
            CS.General_v3.Classes.URL.URLClass url = new CS.General_v3.Classes.URL.URLClass(s);
            url[BusinessLogic_v3.Constants.ParameterNames.ID] = account.ID.ToString();
            return url.ToString();


        }
    }
}
