using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.Article_CultureInfoModule;

namespace PayMammoth.Cms.Article_CultureInfoModule
{

//UserCmsInfo-Class

    public class Article_CultureInfoCmsInfo : PayMammoth.Modules._AutoGen.Article_CultureInfoCmsInfo_AutoGen
    {
    	
        public Article_CultureInfoCmsInfo(BusinessLogic_v3.Modules.Article_CultureInfoModule.Article_CultureInfoBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
    }
}
