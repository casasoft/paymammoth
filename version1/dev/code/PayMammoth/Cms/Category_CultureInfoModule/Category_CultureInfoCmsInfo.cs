using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.Category_CultureInfoModule;

namespace PayMammoth.Cms.Category_CultureInfoModule
{

//UserCmsInfo-Class

    public class Category_CultureInfoCmsInfo : PayMammoth.Modules._AutoGen.Category_CultureInfoCmsInfo_AutoGen
    {
    	
        public Category_CultureInfoCmsInfo(BusinessLogic_v3.Modules.Category_CultureInfoModule.Category_CultureInfoBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
    }
}
