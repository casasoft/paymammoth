using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.General_v3.Classes.DbObjects.Parameters;

namespace PayMammoth.Cms.ProductCategoryFeatureValueModule
{                          

//CmsFactory-Class

    public class ProductCategoryFeatureValueCmsFactory : PayMammoth.Modules._AutoGen.ProductCategoryFeatureValueCmsFactory_AutoGen
    {
        
        
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
        	//any user-specific information, add them to 'cmsInfo'.

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        protected ProductCategoryFeatureValueCmsFactory()
        {

        }
    }
}
