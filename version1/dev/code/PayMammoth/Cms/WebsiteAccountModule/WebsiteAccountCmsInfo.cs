using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.WebsiteAccountModule;
using CS.WebComponentsGeneralV3.Code.Cms.Util;

namespace PayMammoth.Cms.WebsiteAccountModule
{

//UserCmsInfo-Class

    public class WebsiteAccountCmsInfo : PayMammoth.Modules._AutoGen.WebsiteAccountCmsInfo_AutoGen
    {
    	
        public WebsiteAccountCmsInfo(PayMammoth.Modules._AutoGen.WebsiteAccountBase item)
            : base(item)
        {
            
        }


        

        protected override void customInitFields()
        {
            addCustomOperations();
            addMediaItems();
            //custom init field logic here
            this.WebsiteName.ShowInListing = true;
            this.Code.ShowInListing = true;

            this.PaymentRequests.ShowLinkInCms = true;
            this.CssFilename.ShowInEdit = false;
            this.LogoFilename.ShowInEdit = false;
            this.ListingOrderPriorities.AddColumnsToStart(this.WebsiteName, this.Code);

            this.SecretWord.HelpMessage = "This should be a random string, ideally more than 12 characters long, which secure communications to/from PayMammoth";

            this.Css.HelpMessage = "Upload a Css file here to be used to style the PayMammoth website";
            this.Logo.HelpMessage = "Upload a custom logo to be used to style the PayMammoth website";
            this.FakePaymentKey.HelpMessage = "This key is used to temporarily enable fake payments, by specifying the key as a querystring variable in the choice page, named 'EnableFakePayments'";

            this.PaymentGatewayApcoDescription.StringDataType = this.PaymentGatewayRealexDescription.StringDataType = this.PaymentGatewayTransactiumDescription.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.Html;

            this.PaymentGatewayApcoDescription.HelpMessage = this.PaymentGatewayRealexDescription.HelpMessage = this.PaymentGatewayTransactiumDescription.HelpMessage =
                "If this is filled, this is used to show the description of this payment method underneath.  If not, default is taken";
            

            this.EditOrderPriorities.AddColumnsToStart(
                this.WebsiteName, this.Code, this.EnableFakePayments, this.AllowedIPs,
                this.ContactEmail,this.ContactName, this.NotificationEmail,
                this.IterationsToDeriveHash, this.ResponseUrl, this.DisableResponseUrlNotifications, this.WaitUntilPaymentNotificationAcknowledgedBeforeRedirectToSuccess,
                this.SecretWord, this.FakePaymentKey,
                this.Css, this.Logo, 
                this.NotifyClientsByEmailAboutPayment,

                //cheque
                this.ChequeEnabled, this.ChequePayableTo, this.ChequeAddress1, this.ChequeAddress2, this.ChequeAddressLocality, this.ChequeAddressPostCode,
                this.ChequeAddressCountry,

                //paypal
                this.PaypalEnabled, this.PaypalUseLiveEnvironment,
                this.PaypalLiveMerchantEmail, this.PaypalLiveMerchantId, this.PaypalLiveUsername, this.PaypalLivePassword, this.PaypalLiveSignature,
                this.PaypalSandboxMerchantEmail, this.PaypalSandboxMerchantId, this.PaypalSandboxUsername, this.PaypalSandboxPassword, this.PaypalSandboxSignature,
                //payment gateway - transactium

                this.PaymentGatewayTransactiumEnabled,  this.PaymentGatewayTransactiumBankStatementText, 
                this.PaymentGatewayTransactiumProfileTag, this.PaymentGatewayTransactiumUseLiveEnvironment,

                this.PaymentGatewayTransactiumLiveUsername, this.PaymentGatewayTransactiumLivePassword,
                this.PaymentGatewayTransactiumStagingUsername, this.PaymentGatewayTransactiumStagingPassword, this.PaymentGatewayTransactiumDescription,

                //payment gateway - apco

                this.PaymentGatewayApcoEnabled,  this.PaymentGatewayApcoBankStatementText,
                this.PaymentGatewayApcoUseLiveEnvironment, this.PaymentGatewayApcoLiveProfileId, this.PaymentGatewayApcoLiveSecretWord,
                this.PaymentGatewayApcoStagingProfileId, this.PaymentGatewayApcoStagingSecretWord, this.PaymentGatewayApcoDescription,

                //payment gateway - realex

                this.PaymentGatewayRealexEnabled,  this.PaymentGatewayRealexStatementText,
                this.PaymentGatewayRealexUseLiveEnvironment, this.PaymentGatewayRealexMerchantId, this.PaymentGatewayRealexSecretWord,
                this.PaymentGatewayRealexLiveAccountName, this.PaymentGatewayRealexStagingAccountName, this.PaymentGatewayRealexDescription,

                this.MoneybookersEnabled,

                this.NetellerEnabled);

            this.SetFormGroupForFields("Cheque", this.ChequeEnabled, this.ChequePayableTo, this.ChequeAddress1, this.ChequeAddress2, this.ChequeAddressLocality, this.ChequeAddressPostCode,
                this.ChequeAddressCountry);

            this.SetFormGroupForFields("PayPal", this.PaypalEnabled, this.PaypalUseLiveEnvironment,
                this.PaypalLiveMerchantEmail, this.PaypalLiveMerchantId, this.PaypalLiveUsername, this.PaypalLivePassword, this.PaypalLiveSignature,
                this.PaypalSandboxMerchantEmail, this.PaypalSandboxMerchantId, this.PaypalSandboxUsername, this.PaypalSandboxPassword, this.PaypalSandboxSignature);

            this.SetFormGroupForFields("Payment Gateway - Transactium", this.PaymentGatewayTransactiumEnabled, this.PaymentGatewayTransactiumDescription, this.PaymentGatewayTransactiumBankStatementText,
                this.PaymentGatewayTransactiumProfileTag, this.PaymentGatewayTransactiumUseLiveEnvironment,
                this.PaymentGatewayTransactiumLiveUsername, this.PaymentGatewayTransactiumLivePassword,
                this.PaymentGatewayTransactiumStagingUsername, this.PaymentGatewayTransactiumStagingPassword);

            this.SetFormGroupForFields("Payment Gateway - Apco", this.PaymentGatewayApcoEnabled, this.PaymentGatewayApcoDescription, this.PaymentGatewayApcoBankStatementText,
                this.PaymentGatewayApcoUseLiveEnvironment, this.PaymentGatewayApcoLiveProfileId, this.PaymentGatewayApcoLiveSecretWord,
                this.PaymentGatewayApcoStagingProfileId, this.PaymentGatewayApcoStagingSecretWord);


            this.SetFormGroupForFields("Payment Gateway - Realex", this.PaymentGatewayRealexEnabled, this.PaymentGatewayRealexDescription, this.PaymentGatewayRealexStatementText,
                this.PaymentGatewayRealexUseLiveEnvironment, this.PaymentGatewayRealexMerchantId, this.PaymentGatewayRealexSecretWord,
                this.PaymentGatewayRealexLiveAccountName, this.PaymentGatewayRealexStagingAccountName);


            this.NotifyClientsByEmailAboutPayment.HelpMessage = "This will enable the system to send notifications as well to clients about their payment, if the email is filled in in the initial payment request";

            this.DisableResponseUrlNotifications.HelpMessage = "This will disable the system sending notifications on the 'ResponseUrl'. Not recommended!";
            this.WaitUntilPaymentNotificationAcknowledgedBeforeRedirectToSuccess.HelpMessage = @"If this is ticked, the user is redirected to the 'success url' as defined in the request
only when the acknowledgement from the 'ResponseUrl' is received. Not recommended - Ideally the integrating system should cater for this";
                this.NotificationEmail.HelpMessage = "An email (multiple can be provided, comma seperated) that is used to send notifications of payments to administration";

                this.ResponseUrl.HelpMessage = @"This 'ResponseUrl' is used by the system to send notifications of payment on the url provided.  
This serves as the 'back-office' communication from PayMammoth to the integrating website. Default is: /_ComponentsGeneric/Frontend/Pages/Common/Purchase/successHandler.ashx";

                this.AllowedIPs.HelpMessage = "A RegEx expression which can be used to filter the allowed IPs which can initiate a PaymentRequest";
                this.AllowedIPs.Label = "Allowed IPs";

                this.PaymentGatewayApcoBankStatementText.HelpMessage = this.PaymentGatewayRealexStatementText.HelpMessage = this.PaymentGatewayTransactiumBankStatementText.HelpMessage = "This is the text that will appear on the client's bank statement";



            base.customInitFields();
        }

        public void SetFormGroupForFields(string formGroup, params ICmsFieldBase[] fields)
        {
            foreach (var f in fields)
            {
                if (f != null)
                {
                    f.FormGroup = formGroup;
                }
            }
        
        }
        private void addCustomOperations()
        {
            addCustomOperation_CloneAccount();
            addCustomOperation_MakeTestPayment();
        }
        private void addCustomOperation_MakeTestPayment()
        {
            if (this.DbItem != null)
            {
                string url = PayMammoth.Cms.CmsUrls.GetWebsiteAccount_MakeTestPaymentUrl(this.DbItem);
                this.CustomCmsOperations.Add(new CmsItemSpecificOperation(this,
                    "Make test payment", CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.EuroSign, url));
            }

        }

        private void addCustomOperation_CloneAccount()
        {
            if (InEditMode)
            {
                CmsItemSpecificOperation op = new CmsItemSpecificOperation(this,
                    "Clone Account", CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Add,
                    customOperation_CloneAccount);
                op.ConfirmMessage = "Are you sure you want to clone this account?";
                this.CustomCmsOperations.Add(op);
            }
        }

        private void customOperation_CloneAccount()
        {
            var newItem = this.DbItem.CloneAccount();
            CmsUtil.ShowStatusMessageInCMS("Website account [" + this.DbItem.Code + "] cloned successfully", CS.General_v3.Enums.STATUS_MSG_TYPE.Success);


            var url = WebsiteAccountCmsFactory.Instance.GetEditUrlForItemWithID(newItem.ID);
            url.RedirectTo();
        }


        private void addMediaItems()
        {
            this.Logo = this.AddPropertyForMediaItem<WebsiteAccount>(item => item.Logo);
            this.Css= this.AddPropertyForMediaItem<WebsiteAccount>(item => item.Css);
        }
        public CmsPropertyMediaItem<WebsiteAccount> Logo { get; set; }
        public CmsPropertyMediaItem<WebsiteAccount> Css { get; set; }

    }
}

