using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using CS.General_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.ProductVariation_CultureInfoModule;

namespace PayMammoth.Cms.ProductVariation_CultureInfoModule
{

//UserCmsInfo-Class

    public class ProductVariation_CultureInfoCmsInfo : PayMammoth.Modules._AutoGen.ProductVariation_CultureInfoCmsInfo_AutoGen
    {
    	
        public ProductVariation_CultureInfoCmsInfo(BusinessLogic_v3.Modules.ProductVariation_CultureInfoModule.ProductVariation_CultureInfoBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
    }
}
