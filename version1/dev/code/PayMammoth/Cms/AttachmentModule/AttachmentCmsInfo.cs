using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.AttachmentModule;
using CS.WebComponentsGeneralV3.Code.Cms.Util;

namespace PayMammoth.Cms.AttachmentModule
{

//UserCmsInfo-Class

    public class AttachmentCmsInfo : PayMammoth.Modules._AutoGen.AttachmentCmsInfo_AutoGen
    {
    	
        public AttachmentCmsInfo(BusinessLogic_v3.Modules.AttachmentModule.AttachmentBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here
            
            base.customInitFields();
        }
    }
}
