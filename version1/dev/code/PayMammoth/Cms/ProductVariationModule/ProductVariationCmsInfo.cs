using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using CS.General_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.ProductVariationModule;

namespace PayMammoth.Cms.ProductVariationModule
{

//UserCmsInfo-Class

    public class ProductVariationCmsInfo : PayMammoth.Modules._AutoGen.ProductVariationCmsInfo_AutoGen
    {
    	
        public ProductVariationCmsInfo(BusinessLogic_v3.Modules.ProductVariationModule.ProductVariationBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
    }
}
