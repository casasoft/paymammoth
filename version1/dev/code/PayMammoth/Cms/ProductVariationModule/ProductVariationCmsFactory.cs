using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.General_v3.Classes.DbObjects.Parameters;

namespace PayMammoth.Cms.ProductVariationModule
{                          

//CmsFactory-Class

    public class ProductVariationCmsFactory : PayMammoth.Modules._AutoGen.ProductVariationCmsFactory_AutoGen
    {
        
        
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
        	//any user-specific information, add them to 'cmsInfo'.

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        protected ProductVariationCmsFactory()
        {

        }
    }
}
