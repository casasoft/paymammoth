using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace PayMammoth.Cms.WebsiteAccount_CultureInfoModule
{                          

//CmsFactory-Class

    public class WebsiteAccount_CultureInfoCmsFactory : PayMammoth.Modules._AutoGen.WebsiteAccount_CultureInfoCmsFactory_AutoGen
    {
        
        
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
        	//any user-specific information, add them to 'cmsInfo'.

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        protected WebsiteAccount_CultureInfoCmsFactory()
        {

        }
    }
}
