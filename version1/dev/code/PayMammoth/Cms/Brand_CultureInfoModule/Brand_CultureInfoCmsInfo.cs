using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.Brand_CultureInfoModule;

namespace PayMammoth.Cms.Brand_CultureInfoModule
{

//UserCmsInfo-Class

    public class Brand_CultureInfoCmsInfo : PayMammoth.Modules._AutoGen.Brand_CultureInfoCmsInfo_AutoGen
    {
    	
        public Brand_CultureInfoCmsInfo(BusinessLogic_v3.Modules.Brand_CultureInfoModule.Brand_CultureInfoBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
    }
}
