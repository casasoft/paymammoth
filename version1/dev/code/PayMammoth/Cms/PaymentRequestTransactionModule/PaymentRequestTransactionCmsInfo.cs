using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.PaymentRequestTransactionModule;

namespace PayMammoth.Cms.PaymentRequestTransactionModule
{

//UserCmsInfo-Class

    public class PaymentRequestTransactionCmsInfo : PayMammoth.Modules._AutoGen.PaymentRequestTransactionCmsInfo_AutoGen
    {
    	
        public PaymentRequestTransactionCmsInfo(PayMammoth.Modules._AutoGen.PaymentRequestTransactionBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here
            this.AuthCode.ShowInListing = true;
            this.StartDate.ShowInListing = true;
            this.EndDate.ShowInListing = true;
            this.LastUpdatedOn.ShowInListing = true;
            this.IP.ShowInListing = true;
            this.PaymentMethod.ShowInListing = true;
            this.Reference.ShowInListing = true;
            this.Successful.ShowInListing = true;
            this.ListingOrderPriorities.AddColumnsToStart(this.LastUpdatedOn, this.Reference, this.StartDate, this.EndDate, this.IP, this.PaymentMethod, this.Successful);
            this.SetDefaultSortField(this.LastUpdatedOn, CS.General_v3.Enums.SORT_TYPE.Descending);
            this.TransactionInfo.AccessTypeRequired_Edit.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            this.TransactionInfo.StringDataType = CS.General_v3.Enums.STRING_DATA_TYPE.MultiLine;
            this.AccessTypeRequired_ToEdit.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            //this.SetAccessRequiredToEditForProperties(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne, CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne, null,null,null,
            //    this.Successful,this.RequiresManualIntervention,this.

            base.customInitFields();
        }
    }
}
