using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.ArticleCommentModule;

namespace PayMammoth.Cms.ArticleCommentModule
{

//UserCmsInfo-Class

    public class ArticleCommentCmsInfo : PayMammoth.Modules._AutoGen.ArticleCommentCmsInfo_AutoGen
    {
    	
        public ArticleCommentCmsInfo(BusinessLogic_v3.Modules.ArticleCommentModule.ArticleCommentBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
    }
}
