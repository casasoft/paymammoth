using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.EmailTextModule;

namespace PayMammoth.Cms.EmailTextModule
{

//UserCmsInfo-Class

    public class EmailTextCmsInfo : PayMammoth.Modules._AutoGen.EmailTextCmsInfo_AutoGen
    {
    	
        public EmailTextCmsInfo(BusinessLogic_v3.Modules.EmailTextModule.EmailTextBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
    }
}
