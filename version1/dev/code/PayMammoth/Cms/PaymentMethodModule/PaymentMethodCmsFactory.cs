using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.CmsItems;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using BusinessLogic_v3.Classes.DbObjects.Parameters;

namespace PayMammoth.Cms.PaymentMethodModule
{                          

//CmsFactory-Class

    public class PaymentMethodCmsFactory : PayMammoth.Modules._AutoGen.PaymentMethodCmsFactory_AutoGen
    {
        
        
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
        	//any user-specific information, add them to 'cmsInfo'.
            cmsInfo.ShowInCmsMainMenu = true;
            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        protected PaymentMethodCmsFactory()
        {
            
        }
    }
}
