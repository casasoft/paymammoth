using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.ContentText_CultureInfoModule;

namespace PayMammoth.Cms.ContentText_CultureInfoModule
{

//UserCmsInfo-Class

    public class ContentText_CultureInfoCmsInfo : PayMammoth.Modules._AutoGen.ContentText_CultureInfoCmsInfo_AutoGen
    {
    	
        public ContentText_CultureInfoCmsInfo(BusinessLogic_v3.Modules.ContentText_CultureInfoModule.ContentText_CultureInfoBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
    }
}
