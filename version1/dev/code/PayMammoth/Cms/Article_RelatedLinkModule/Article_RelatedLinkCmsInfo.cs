using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.Article_RelatedLinkModule;

namespace PayMammoth.Cms.Article_RelatedLinkModule
{

//UserCmsInfo-Class

    public class Article_RelatedLinkCmsInfo : PayMammoth.Modules._AutoGen.Article_RelatedLinkCmsInfo_AutoGen
    {
    	
        public Article_RelatedLinkCmsInfo(BusinessLogic_v3.Modules.Article_RelatedLinkModule.Article_RelatedLinkBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
    }
}
