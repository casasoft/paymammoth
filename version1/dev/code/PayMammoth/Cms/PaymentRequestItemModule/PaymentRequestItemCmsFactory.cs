using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects.CmsItems;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using CS.General_v3.Classes.DbObjects.Parameters;

namespace PayMammoth.Cms.PaymentRequestItemModule
{                          

//CmsFactory-Class

    public class PaymentRequestItemCmsFactory : PayMammoth.Modules._AutoGen.PaymentRequestItemCmsFactory_AutoGen
    {
        
        
        protected override void getUserSpecificGeneralInfo(CmsItemGeneralInfo cmsInfo)
        {
        	//any user-specific information, add them to 'cmsInfo'.

            base.getUserSpecificGeneralInfo(cmsInfo);
        }
        
        protected PaymentRequestItemCmsFactory()
        {

        }
    }
}
