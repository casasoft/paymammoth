using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Cms.CmsObjects;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties;
using BusinessLogic_v3.Classes.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using CS.General_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.PaymentRequestItemModule;

namespace PayMammoth.Cms.PaymentRequestItemModule
{

//UserCmsInfo-Class

    public class PaymentRequestItemCmsInfo : PayMammoth.Modules._AutoGen.PaymentRequestItemCmsInfo_AutoGen
    {
    	
        public PaymentRequestItemCmsInfo(PayMammoth.Modules._AutoGen.PaymentRequestItemBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
    }
}
