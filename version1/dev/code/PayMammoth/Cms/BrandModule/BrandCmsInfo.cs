using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.BrandModule;

namespace PayMammoth.Cms.BrandModule
{

//UserCmsInfo-Class

    public class BrandCmsInfo : PayMammoth.Modules._AutoGen.BrandCmsInfo_AutoGen
    {
    	
        public BrandCmsInfo(BusinessLogic_v3.Modules.BrandModule.BrandBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
    }
}
