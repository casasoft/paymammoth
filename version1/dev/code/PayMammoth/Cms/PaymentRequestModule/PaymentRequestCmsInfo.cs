using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.PaymentRequestModule;
using CS.WebComponentsGeneralV3.Code.Cms.Util;
using System.Web.UI.WebControls;

namespace PayMammoth.Cms.PaymentRequestModule
{

//UserCmsInfo-Class

    public class PaymentRequestCmsInfo : PayMammoth.Modules._AutoGen.PaymentRequestCmsInfo_AutoGen
    {
    	
        public PaymentRequestCmsInfo(PayMammoth.Modules._AutoGen.PaymentRequestBase item)
            : base(item)
        {

        }
        private void addCustomOperations()
        {
            addCustomOperation_ResendNotificationOfPayment();
        }
        private void addCustomOperation_ResendNotificationOfPayment()
        {
            if (this.DbItem != null && this.DbItem.CheckIfPaid() &&  
                (this.DbItem.WebsiteAccount != null && !this.DbItem.WebsiteAccount.DisableResponseUrlNotifications &&
                !string.IsNullOrWhiteSpace(this.DbItem.WebsiteAccount.ResponseUrl)))
            {
                //show this buttononly if - PAID,  website account is not null,
                //WebsiteAccount account is not set to disable response url notifications, and website account response url is not empty

                this.CustomCmsOperations.Add(
                    new CmsItemSpecificOperation(this, "Re-Post Payment Notification",
                         CS.WebComponentsGeneralV3.Code.Cms.EnumsCms.IMAGE_ICON.Update,
                          operation_ResendNotificationOfPayment));
            }
        }

        private void operation_ResendNotificationOfPayment()
        {
             this.DbItem.PostNoticeOnResponseUrl();
             CmsUtil.ShowStatusMessageInCMSAndRefresh("Payment notification re-instantiated", CS.General_v3.Enums.STATUS_MSG_TYPE.Success);
        }

        protected override void customInitFields()
        {
            createTotalField();
            addCustomOperations();
            //custom init field logic here
            this.DateTime.ShowInListing = true;
            this.SetShowInListingAndOrder(this.DateTime, this.WebsiteAccount, this.ClientFirstName, this.ClientLastName,
                this.ClientCountry, this.Total, this.Reference, this.Paid, this.CurrencyCode, this.PaidByPaymentMethod, this.PaidOn, this.PaymentNotificationAcknowledged);
            this.EditOrderPriorities.AddColumnsToStart(this.DateTime, this.Identifier,  this.Title, this.Description, this.WebsiteAccount, this.Reference, this.ClientTitle, this.ClientFirstName, this.ClientMiddleName, this.ClientLastName,
                this.ClientEmail,
                this.ClientAddress1, this.ClientAddress2, this.ClientAddress3, this.ClientLocality, this.ClientPostCode, this.ClientCountry, this.ClientTelephone, this.ClientMobile,
                this.ClientReference,this.ClientIp,
                this.CurrencyCode, this.Total, this.PriceExcTax, this.TaxAmount, this.ShippingAmount, this.HandlingAmount, this.Paid, this.PaidByPaymentMethod, this.PaidOn,
                this.PaymentNotificationEnabled, this.PaymentNotificationAcknowledged, this.PaymentNotificationAcknowledgedOn, this.PaymentNotificationRetryCount, this.PaymentNotificationNextRetryOn);

            this.ClientReference.HelpMessage = "This is the client reference on the website, for example the MemberID";

            this.ClientAddress1.FormGroup = this.ClientAddress2.FormGroup = this.ClientAddress3.FormGroup = this.ClientCountry.FormGroup = this.ClientEmail.FormGroup = this.ClientFirstName.FormGroup = this.ClientIp.FormGroup =
                this.ClientLastName.FormGroup = this.ClientLocality.FormGroup = this.ClientMiddleName.FormGroup = this.ClientMobile.FormGroup = this.ClientPostCode.FormGroup = this.ClientReference.FormGroup =
                this.ClientTelephone.FormGroup = this.ClientTitle.FormGroup = "Client Details";

            this.Total.FormGroup= this.PriceExcTax.FormGroup = this.TaxAmount.FormGroup = this.ShippingAmount.FormGroup = this.HandlingAmount.FormGroup = "Pricing Details";

            this.Identifier.FormGroup = this.Title.FormGroup = this.Description.FormGroup = this.WebsiteAccount.FormGroup = this.Reference.FormGroup = this.DateTime.FormGroup = "Request General Information";

            this.Reference.HelpMessage = "This is the order reference on the website";
            this.DateTime.DateShowTime = true;
            
            this.Transactions.ShowLinkInCms = true;
            this.SetDefaultSortField(this.DateTime, CS.General_v3.Enums.SORT_TYPE.Descending);
            this.WebsiteAccount.ShowInEdit = true;
            this.WebsiteAccount.ShowInListing = true;
            this.PaymentNotificationAcknowledged.AccessTypeRequired_Edit.SetAccess(CS.General_v3.Enums.CMS_ACCESS_TYPE.NoOne);
            
            this.ItemDetails.ShowLinkInCms = true;
            base.customInitFields();
            
        }

        private void createTotalField()
        {
            this.Total = AddProperty(new CmsPropertyLiteral<PaymentRequest>(this, "Total", x =>
            {
                Literal lit = new Literal();
                lit.Text = x.GetTotalPrice().ToString("0.00");
                return lit;
            }));
        }

        public CmsPropertyLiteral<PaymentRequest> Total { get; set; }


    }
}
