using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties;
using CS.WebComponentsGeneralV3.Code.Cms.CmsObjects.Properties.CustomPropertyTypes; 
using CS.General_v3.Extensions;
using BusinessLogic_v3.Classes.DbObjects.Parameters;
using PayMammoth.Modules.EmailLogModule;

namespace PayMammoth.Cms.EmailLogModule
{

//UserCmsInfo-Class

    public class EmailLogCmsInfo : PayMammoth.Modules._AutoGen.EmailLogCmsInfo_AutoGen
    {
    	
        public EmailLogCmsInfo(BusinessLogic_v3.Modules.EmailLogModule.EmailLogBase item)
            : base(item)
        {

        }
        
		protected override void customInitFields()
        {
            //custom init field logic here

            base.customInitFields();
        }
    }
}
