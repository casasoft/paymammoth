﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace PayMammothTest
{
    [SetUpFixture]
    public class Setup : CS.TestGeneral_v3.BaseSetupFixture
    {

        protected override void onPostSetup()
        {
            //bool checkSchema = true;

            CS.General_v3.Util.Other.SetIsInUnitTestingEnvironment();
            
            CS.General_v3.Settings.Database.CheckSchemaOverride = false;
            CS.General_v3.Settings.Database.CheckMappingsAndOthersOverride = false;
            CS.General_v3.Settings.Database.CheckDefaultsInRealDatabaseOverride = false;
            // 

            PayMammoth.Classes.Application.AppInstance.Instance.OnApplicationStart();
            PayMammoth.Classes.Application.AppInstance.Instance.OnSessionStart();
            //NHibernateProfiler.Initialize();
        }

        protected override void onPreSetup()
        {
            this.ProjectFolderRelativePath = "../../../../../deploy";
            CS.General_v3.Settings.Database.CustomDatabaseType = CS.General_v3.Settings.Database.DATABASE_TYPE.MySQL;
        }

        protected override void onTearDown()
        {

        }
    }
}
