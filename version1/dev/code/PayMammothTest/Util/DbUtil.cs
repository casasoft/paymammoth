﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Classes.Other;
using PayMammoth.Modules.PaymentRequestModule;
using PayMammoth.Modules.WebsiteAccountModule;

namespace PayMammothTest.Util
{
    public static class DbUtil
    {
        public static WebsiteAccount CreateWebsiteAccount(string websiteName = "Test Website", 
            string allowedIPs = null, string code = "-TEST-", int iterationsToDeriveHash = 4096, string responseUrl = "http://www.casasoft.com.mt/payment/test",
            string secretWord = "TestTestTest", bool allowFakePayments = false, bool autoSave = true)
        {
            WebsiteAccount item = WebsiteAccount.Factory.CreateNewItem();
            item.AllowedIPs = allowedIPs;
            item.EnableFakePayments = allowFakePayments;
            item.Code = code;
            item.IterationsToDeriveHash = iterationsToDeriveHash;
            item.ResponseUrl = responseUrl;
            item.SecretWord = secretWord;
            item.WebsiteName = websiteName;
            if (autoSave)
                item.CreateAndCommit();
            return item;
        }

        public static PaymentRequest CreateNewRequest(WebsiteAccount account, string address1 = "Test Address 1", string address2 = "Test address 3",
      string address3 = null, CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166 country = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166.Malta,
          string email = "email.test@casasoft.com.mt", string firstName = "Test First Name", string middleName = null,
          string lastName = "Test Last Name", string locality = "Test Zurrieq", string postcode = "TST11234",
          CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 currency = CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.EuroMemberCountriesEuro,
          double handlingAmount = 5, double priceExcTax = 10,
          string reference = null, double shippingAmount = 2, double taxAmount = 1, string title = "Testing",
            string clientIp = "192.168.1.1"
          )
        {
            InitialRequestInfo info = new InitialRequestInfo();
            info.ClientIp = clientIp;
            info.ClientAddress1 = address1;
            info.ClientAddress2 = address2;
            info.ClientAddress3 = address3;
            info.ClientCountry = country;
            info.ClientEmail = email;
            info.ClientFirstName = firstName;
            info.ClientMiddleName = middleName;
            info.ClientLastName = lastName;
            info.ClientLocality = locality;
            info.ClientPostCode = postcode;
            info.CurrencyCode = currency;
            info.HandlingAmount = 5;
            info.PriceExcTax = 10;
            if (reference == null) reference = CS.General_v3.Util.Date.Now.ToString("yyyyMMddHHmmss");
            info.Reference = reference;
            info.ShippingAmount = shippingAmount;
            info.TaxAmount = taxAmount;
            info.Title = title;
            info.WebsiteAccountCode = account.Code;

            info.HashValue = PayMammoth.Connector.Util.HashUtil.GenerateInitialRequestHash(info, account.SecretWord);
            
            return account.GenerateNewRequest(info);
        }
    }
}
