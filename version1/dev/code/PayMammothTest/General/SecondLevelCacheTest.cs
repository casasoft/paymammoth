﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

using PayMammoth.Modules.ArticleModule;

namespace PayMammothTest.General
{
    [TestFixture]
    public class SecondLevelCacheTest : CS.TestGeneral_v3.BaseTestFixtureMySQL
    {

        private long _itemID = 0;

        private Article article1 = null;
        private Article article1_1 = null;
        private Article article1_2 = null;
        private Article article1_2_1 = null;
        private Article article1_2_2 = null;

        private Article createArticle(int index)
        {
            Article a = null;
            using (var t = beginTransaction())
            {
                a = Article.Factory.CreateNewItem();
                a.PageTitle = "Test" + index;
                a.Save();
                t.Commit();
                
            }
            return a;
        }

        [Test]
        public void test1()
        {
            article1 = createArticle(1);
            article1_1 = createArticle(11);
            article1_2 = createArticle(12);
            article1_2_1 = createArticle(121);
            article1_2_2 = createArticle(122);

            article1.AddChildArticle(article1_1);
            article1.AddChildArticle(article1_2);
            article1_2_1.AddParentArticle(article1_2);

            disposeCurrentAndCreateNewSession();



            Article test = Article.Factory.GetByPrimaryKey(article1_2.ID);
            Assert.That(test.GetChildArticles().Count() == 1);
            Assert.That(test.GetChildArticles().Contains(article1_2_1));
            Assert.That(!test.GetChildArticles().Contains(article1_2_2));
            article1_2.Merge();
            test.AddChildArticle(article1_2_2);

            disposeCurrentAndCreateNewSession();
            test = Article.Factory.GetByPrimaryKey(article1_2.ID);
            Assert.That(test.GetChildArticles().Count() == 2);
            Assert.That(test.GetChildArticles().Contains(article1_2_1));
            Assert.That(test.GetChildArticles().Contains(article1_2_2));


        }

    }
}
