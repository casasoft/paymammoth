﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules;
using BusinessLogic_v3.Modules.OrderModule;
using PayMammothConnectorBL.v3.Requests;
//using CS.WebComponentsGeneralV3.Code.Util;
using BusinessLogic_v3;
using BusinessLogic_v3.Classes.Routing;
using BusinessLogic_v3.Extensions;
using CS.General_v3.Util;
namespace PayMammothConnectorBL.v3.Util
{
    public static class FrontendGeneral
    {
        public static void GeneratePayMammothRequestFromOrderAndRedirect(IOrderBase order)
        {
            string secretWord =Factories.SettingFactory.GetSettingValue<string>(CS.General_v3.Enums.SETTINGS_ENUM.PayMammoth_SecretWord);

            string websiteAccountCode =Factories.SettingFactory.GetSettingValue<string>(CS.General_v3.Enums.SETTINGS_ENUM.PayMammoth_WebsiteAccountCode);

            var successHandlerUrl = Factories.SettingFactory.GetSettingValue<string>(Enums.BusinessLogicSettingsEnum.ModuleSettings_Payment_Succes_HandlerUrl);
            var siteUrl = PageUtil.GetApplicationBaseUrl().ToString();
            if (siteUrl.EndsWith("/") && successHandlerUrl.StartsWith("/"))
            {
                successHandlerUrl = successHandlerUrl.Substring(1);
            }
            string successUrl = siteUrl + successHandlerUrl;// PurchaseSuccessRoute.GetURL();
            
            //string successUrl =
            //    Factories.ArticleFactory.GetArticleByIdentifier(Enums.CONTENT_PAGE_IDENTIFIER.PayPippa_SuccessPage).GetUrl();
            string cancelUrl = Factories.ArticleFactory.GetArticleByIdentifier(Enums.CONTENT_PAGE_IDENTIFIER.PayPippa_CancelPage).GetUrl();

            InitialRequest request = new InitialRequest(websiteAccountCode, secretWord) {ReturnUrlSuccess = successUrl, ReturnUrlFailure = cancelUrl};
            request.FillFromOrder(order);
            var response = request.CreateRequestWithPayMammoth();
            if (response.Result.Status == CS.General_v3.Enums.STATUS_MSG_TYPE.Success)
            {
                string payMammothIdentifier = response.PayMammothIdentifier;

                CS.General_v3.Util.PageUtil.SetSessionObject(BusinessLogic_v3.Constants.SessionIdentifiers.PAY_MAMMOTH_SESSION_IDENTIFIER, payMammothIdentifier);

                response.RedirectToPayMammoth();
            }
            else
            {
                throw new InvalidOperationException("An error occured whilst creating PayMammoth response. [" + response.StatusCode + "] - " + response.Result.GetMessageAsString() + 
                    " | Url: " + PayMammoth.Connector.Constants.GetPayMammothBaseUrl() );
            }
        }
    }
}
