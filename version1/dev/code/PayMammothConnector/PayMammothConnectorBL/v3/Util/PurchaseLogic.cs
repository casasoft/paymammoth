﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Classes.Routing;
using BusinessLogic_v3.Modules.MemberModule;
using CS.General_v3.Util;
using PayMammoth.Connector.Classes.Responses;
using BusinessLogic_v3.Modules.OrderModule;
using BusinessLogic_v3.Modules;
using BusinessLogic_v3;

namespace PayMammothConnectorBL.v3.Util
{
    public static class PurchaseLogic
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier">If identifier is left null, it is retrieved from querystring / session</param>
        /// <param name="order"></param>
        /// <returns></returns>
        public static bool VerifyPurchaseAndConfirmAfterPayPippa(string identifier, out IOrderBase order)
        {
            bool verified = false;
            order = null;
            
            if (identifier == null)
            {
                identifier = CS.General_v3.Util.PageUtil.GetVariableFromQuerystring(PayMammoth.Connector.Constants.PARAM_IDENTIFIER);
            }
            if (identifier == null)
            {
                identifier = (string)PageUtil.GetSessionObjectAndRemove(BusinessLogic_v3.Constants.SessionIdentifiers.PAY_MAMMOTH_SESSION_IDENTIFIER,false);
            }
            string payPippaIdentifier = identifier;
            if (!string.IsNullOrWhiteSpace(payPippaIdentifier))
            {
                order = PayMammothConnectorBL.v3.Util.PurchaseLogic.confirmPurchaseAfterPayPippa(payPippaIdentifier);
                if (order != null)
                {
                    verified = true;
                    
                }
                else
                {
                    throw new InvalidOperationException("'order' can never be null! - This can be due to the payment not being confirmed with PayMammoth");
                }
            }
            else
            {
                throw new InvalidOperationException("Identifier cannot be null");
                //PageUtil.RedirectPage(ErrorsRoute.GetGenericErrorURL());
            }
            return verified;
        }

        private static IOrderBase confirmPurchaseAfterPayPippa(string payPippaIdentifier)
        {
            PayMammoth.Connector.Classes.Requests.ConfirmRequest confirmReq =
                new PayMammoth.Connector.Classes.Requests.ConfirmRequest(payPippaIdentifier);

            ConfirmResponse result = confirmReq.Confirm();
            if (result.Success)
            {
                long orderId = 0;
                bool referenceParseSuccess = false;
                referenceParseSuccess = long.TryParse(result.Reference, out orderId);
                if (referenceParseSuccess)
                {
                    IOrderBase order = Factories.OrderFactory.GetByPrimaryKey(orderId);
                    if (order != null)
                    {
                        if(!result.PaymentMethod.HasValue)
                        {
                            throw new InvalidOperationException("result.PaymentMethod can never be null at this stage!");
                        }

                        order.PaymentProcessCompleted(result.AuthCode, result.RequiresManualPayment,
                                                      result.PaymentMethod.Value);
                        //CS.General_v3.Util.PageUtil.RedirectPage(
                        //    Factories.ArticleFactory.GetArticleByIdentifier(Enums.CONTENT_PAGE_IDENTIFIER.PayPippa_FinalSuccessPage).
                        //        GetUrl());
                        return order;
                    }
                    else
                    {
                        throw new InvalidOperationException("Reference ID is null or does not exist!");
                    }
                }
                else
                {
                    throw new InvalidOperationException("Reference ID should match order ID (at this stage only!) - it will later become a real ref ID!");
                }
            }
            else
            {
                throw new InvalidOperationException(
                    "An error occured whilst validation the PayPippa Confirmation (check PayPippa not specific).");
            }
        }
    }
}
