﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.OrderModule;

namespace PayMammothConnectorBL.v3.Requests
{
    public class ConfirmRequest :PayMammoth.Connector.Classes.Requests.ConfirmRequest
    {
        public ConfirmRequest(IOrderBase order)
            : base(order.PayMammothIdentifier)
        {
            
        }
    }
}
