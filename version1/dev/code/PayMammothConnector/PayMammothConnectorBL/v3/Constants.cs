﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammothConnectorBL.v3
{
    public class Constants
    {
        
        public const string HANDLER_PAYMENT_NOTIFICATION_URL = "__paymentNotification.ashx";
        public const string SESSION_ORDER_DETAILS = "sessionOrderDetails";
    }
}
