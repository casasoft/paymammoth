﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v2.DB;
using BusinessLogic_v2.Modules.v2.Order;
using BusinessLogic_v2.Modules.v2.OrderItem;

namespace PayMammothConnectorBL.v2.Requests
{
    public class InitialRequest : PayMammoth.Connector.Classes.Requests.InitialRequestDetails
    {
        public InitialRequest(string websiteAccountCode, string secretWord)
            : base(websiteAccountCode, secretWord)
        {

        }

        public void FillFromOrder(OrderBaseFrontend order)
        {
            
            this.ClientAddress1 = order.Data.Address1;
            this.ClientAddress2 = order.Data.Address2;
            this.ClientAddress3 = order.Data.Address3;
            this.ClientCountry = CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166_FromCode(order.Data.AddressCountry);
            this.ClientEmail = order.Data.CustomerEmail;
            this.ClientFirstName = order.Data.CustomerName;
            this.ClientLastName = order.Data.CustomerSurname;
            this.ClientLocality = order.Data.AddressLocality;
            this.ClientPostCode = order.Data.AddressPostCode;
            
            {
                var curr = order.GetCurrency();
                //this.cur
                //this.CurrencyCode = CS.General_v3.Enums.ISO_ENUMS.Currency_ISO4217FromCode(curr.Data.CurrencyISOCode);//.GetValueOrDefault(CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217.EuroMemberCountriesEuro);
                
            }
            OrderItemBaseFrontend firstItem = order.GetOrderItemsInOrder().FirstOrDefault();


            if (firstItem != null)
            {
                if (string.IsNullOrEmpty(this.Title))
                {
                    this.Title = "(Order: " + order.Data.ReferenceCode + ") - " + firstItem.Data.Title;
                }
            }

           
            this.OrderLinkOnClientWebsite = null;
            this.PriceExcTax = order.GetTotalPrice(includeTax: false, includeShipping: false, reduceDiscount: true);
            this.Reference = order.Data.ReferenceCode;
            this.ReturnUrlFailure = null;
            this.ReturnUrlSuccess = null;
            this.ShippingAmount = order.GetTotalShippingCost().GetValueOrDefault();
            this.TaxAmount = order.GetTotalTaxAmount();




        }

    }
}
