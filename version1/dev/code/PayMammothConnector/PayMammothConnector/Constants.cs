﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Connector.Classes.Interfaces;
using PayMammoth.Connector.Classes.Responses;

namespace PayMammoth.Connector
{
    public class Constants
    {

        static Constants()
        {
            
        }

        /* These five constants must be filled in before it can be used */

        private static string _secretWord;
        public static string SecretWord
        {
            get { return _secretWord; }
            set { _secretWord = value; }
        }

        private static string _websiteAccountCode;
        public static string WebsiteAccountCode
        {
            get { return _websiteAccountCode; }
            set { _websiteAccountCode = value; }
        }

        /*----------------------------------------------------------------------*/


        public static string PAYMAMMOTH_BASE_URL = @"http://www.paymammoth.com/";
        

        public static string GetPayMammothBaseUrl()
        {
            string s = PAYMAMMOTH_BASE_URL; //live

            if (CS.General_v3.Util.Other.IsLocalTestingMachine && (false || _loadFromOfficeCasaSoftServer)) // in order to turn on testing for locally
            {
                

                s = "http://office.casasoft.com.mt/";
                int k = 5;
                //s = "http://localhost:8090/";
            }
            return s;
            
        }

        //public static string PayMammothBaseUrl = "http://localhost:30419/";
           

        public static string PayMammothInitPaymentHandlerUrl 
        {
            get { return GetPayMammothBaseUrl() + "payment/initPayment.ashx"; }
        }
        public static string PayMammothConfirmHandlerUrl
        {
            get { return GetPayMammothBaseUrl() + "payment/confirm.ashx"; }
        }
        public static string PayMammothPaymentChoiceUrl
        {
            get { return GetPayMammothBaseUrl() + "payment/"; }
        }
       


        
        public const string PARAM_IDENTIFIER = "__pm_id";
        public const string PARAM_ERRORCODE = "__pm_error";
        public const string PARAM_HASH = "__pm_hash";
        
        

        public const string RESPONSE_OK = "OK";
        public const string RESPONSE_NO = "NO";
        public const string RESPONSE_YES = "YES";


        public static bool _loadFromOfficeCasaSoftServer { get; set; }

    }
}
