﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.URL;
using PayMammoth.Connector.Classes.Interfaces;
using PayMammoth.Connector.Classes.Requests;
using PayMammoth.Connector.Classes.Responses;
using log4net;

namespace PayMammoth.Connector.Handlers
{
    public abstract class PaymentConfirmationHandler : CS.General_v3.HTTPHandlers.BaseHandler
    {
        private ILog _log = LogManager.GetLogger(typeof(PaymentConfirmationHandler));
        public PaymentConfirmationHandler()
        {
            _log.Debug("PaymentConfirmationHandler - Constructor");
        }

        protected abstract void OnPaidSuccessfully(ConfirmResponse response);

        
        protected override void processRequest(System.Web.HttpContext ctx)
        {

            bool success = false;
            QueryString qs = new QueryString(ctx.Request.QueryString);
            if (_log.IsDebugEnabled) _log.Debug("PaymentConfirmationHandler - This Request's Url: " + ctx.Request.Url.ToString());
            string identifier = qs[Constants.PARAM_IDENTIFIER];
            if (!string.IsNullOrWhiteSpace(identifier))
            {
                ConfirmRequest req = new ConfirmRequest(identifier);
                req.SecretWord = Constants.SecretWord;
                
                var responseResult = req.Confirm();
                if (responseResult.Success)
                {
                    if (_log.IsDebugEnabled) _log.Debug("PaymentConfirmationHandler - Success, calling out 'OnPaidSuccessfully' method");
                    OnPaidSuccessfully(responseResult);
                    success = true;
                    if (_log.IsDebugEnabled) _log.Debug("PaymentConfirmationHandler - Success - All OK");
                    
                }
                else
                {

                    if (_log.IsDebugEnabled) _log.Debug("PaymentConfirmationHandler - Failed - " + responseResult.Result.GetMessageAsString());
                }
            }
            ctx.Response.Write(PayMammoth.Connector.Constants.RESPONSE_OK);
        }
    }
}
