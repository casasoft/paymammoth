﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.Connector.Util
{
    public static class HashUtil
    {
        public static string GenerateInitialRequestHash(string accountCode, string title, long totalAmountInCents, string secretWord)
        {
            string s = accountCode.ToUpper() + title.ToUpper() + totalAmountInCents + secretWord;

            return CS.General_v3.Util.Cryptography.GetSHA512Hash(s);
            
        }
        public static string GenerateInitialResponseHash(string identifier, string secretWord)
        {
            string s = identifier + secretWord;
            
            return CS.General_v3.Util.Cryptography.GetSHA512Hash(s);

        }
        public static string GenerateConfirmHash(string identifier, string secretWord)
        {
            string s = identifier + secretWord;

            return CS.General_v3.Util.Cryptography.GetSHA512Hash(s);

        }

        public static string GenerateInitialRequestHash(PayMammoth.Connector.Classes.Interfaces.IInitialRequestDetails requestDetails, string secretWord)
        {
            return GenerateInitialRequestHash(requestDetails.WebsiteAccountCode, requestDetails.Title, requestDetails.GetTotalPriceInCents(), secretWord);
            
        }
    }
}
