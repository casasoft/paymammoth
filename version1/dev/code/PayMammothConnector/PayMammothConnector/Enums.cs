﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace PayMammoth.Connector
{
    public class Enums
    {
        public enum PaymentMethod
        {
            [Description("PayPal")]
            PayPal = 1000,
            [Description("Credit Card")]
            PaymentGateway = 2000,
            Moneybookers = 3000,
            Cheque = 4000,
            BankTransfer = 5000,
            Neteller = 6000

        }
        //When you add a PaymentMethodSpecific, update the 'GetPaymentMethodFromSpecific' method below.
        public enum PaymentMethodSpecific
        {
            [Description("PayPal")]
            PayPal_ExpressCheckout = 1000,
            [Description("Online Payment")]
            PaymentGateway_Transactium = 2000,
            [Description("Online Payment")]
            PaymentGateway_Apco = 2200,
            [Description("Online Payment")]
            PaymentGateway_Realex = 2400,
            [Description("Money Bookers")]
            Moneybookers = 3000,
            [Description("Cheque")]
            Cheque = 4000,
            [Description("Bank Transfer")]
            BankTransfer = 5000,
            [Description("Neteller")]
            Neteller = 6000,
            [Description("Testing")]
            NUnitTest = 10000
        }

        public static bool GetWhetherPaymentMethodRequiresManualIntervention(PaymentMethodSpecific method)
        {
            switch (method)
            {
                case PaymentMethodSpecific.Cheque:
                    return true;
            }
            return false;
        }

        public enum InitialRequestResponseStatus
        {
            Success,
            HashNotVerified,
            GenericError,
            ErrorConnectingToServer
        }
        

        public static PaymentMethod GetPaymentMethodFromSpecific(PaymentMethodSpecific value)
        {
            switch (value)
            {
                case PaymentMethodSpecific.PayPal_ExpressCheckout:
                    return PaymentMethod.PayPal;

                case PaymentMethodSpecific.PaymentGateway_Transactium:
                case PaymentMethodSpecific.PaymentGateway_Apco:
                case PaymentMethodSpecific.PaymentGateway_Realex:
                    return PaymentMethod.PaymentGateway;

                case PaymentMethodSpecific.Moneybookers:
                    return PaymentMethod.Moneybookers;

                case PaymentMethodSpecific.Cheque:
                    return PaymentMethod.Cheque;
                    
                case PaymentMethodSpecific.BankTransfer:
                    return PaymentMethod.BankTransfer;

                case PaymentMethodSpecific.Neteller:
                    return PaymentMethod.Neteller;

                default:
                    throw new InvalidOperationException("Specific value <" + value + "> not mapped to a payment method");
            }
        }
    }
}
