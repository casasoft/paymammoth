﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Connector.Classes.Requests;

namespace PayMammoth.Connector.Classes.Serialization
{
    public static class ItemDetailsSerializer
    {

        public const string INITIAL_ITEM_DETAILS_ITEM_DELIMETER = "#|#";
        public const string INITIAL_ITEM_DETAILS_TOKEN_DELIMETER = "$|$";

        public static string SerializeItemDetailToString(IItemDetail itemDetail)
        {
            List<string> tokens = new List<string>();
            tokens.Add(itemDetail.Title);
            tokens.Add(itemDetail.Description);
            return CS.General_v3.Util.Text.AppendStrings(INITIAL_ITEM_DETAILS_TOKEN_DELIMETER, tokens);

        }
        public static string SerializeItemDetailsToString(IEnumerable<IItemDetail> itemDetails)
        {
            StringBuilder sb = new StringBuilder();
            List<string> list = new List<string>();
            foreach (var item in itemDetails)
            {
                list.Add(SerializeItemDetailToString(item));
            }
            return CS.General_v3.Util.Text.AppendStrings(INITIAL_ITEM_DETAILS_ITEM_DELIMETER, list);

        }
        public static void DeserializeStringToItemDetail(string s, IItemDetail itemDetail)
        {
            var tokens = CS.General_v3.Util.Text.Split(s, INITIAL_ITEM_DETAILS_TOKEN_DELIMETER);
            int tokenIndex = 0;
            if (tokens.Count > tokenIndex) itemDetail.Title = tokens[tokenIndex++];
            if (tokens.Count > tokenIndex) itemDetail.Description = tokens[tokenIndex++];

        }
        /// <summary>
        /// Deserializes a string to a list of item details
        /// </summary>
        /// <param name="s"></param>
        /// <param name="itemDetails">The collection to fill</param>
        /// <param name="itemDetailCreator">A function that creates an IItemDetail</param>
        public static void DeserializeStringToItemDetails(string s, ICollection<IItemDetail> itemDetails, Func<IItemDetail> itemDetailCreator)
        {
            
            var itemTokens = CS.General_v3.Util.Text.Split(s, INITIAL_ITEM_DETAILS_ITEM_DELIMETER);
            
            foreach (var item in itemTokens)
            {
                if (!string.IsNullOrWhiteSpace(item))
                {
                    IItemDetail itemDetail = itemDetailCreator();
                    DeserializeStringToItemDetail(item, itemDetail);
                    itemDetails.Add(itemDetail);

                }
            }
            
            
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="itemDetailCreator">A function that creates an IItemDetail</param>
        /// <returns></returns>
        public static List<IItemDetail> DeserializeStringToItemDetails(string s, Func<IItemDetail> itemDetailCreator)
        {
            List<IItemDetail> list = new List<IItemDetail>();
            DeserializeStringToItemDetails(s, list, itemDetailCreator);
            return list;


        }
    }
}
