﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Connector.Classes.Interfaces;
using CS.General_v3.Classes.Serialization;
using System.Collections.Specialized;
using CS.General_v3.Classes.HelperClasses;

namespace PayMammoth.Connector.Classes.Responses
{
    public class ConfirmResponse : BaseSerializable<IConfirmResponseDetails>, IConfirmResponseDetails
    {
        #region IConfirmResponseDetails Members
        public string AuthCode { get; set; }
        public string RequestIdentifier { get; set; }
        public bool Success { get; set; }
        public bool RequiresManualPayment { get; set; }
        public bool IsFakePayment { get; set; }
        public Enums.PaymentMethodSpecific? PaymentMethod { get; set; }
        public string Hash { get; set; }
        
        #endregion
        public bool VerifyHash(string secretWord)
        {
            string computedHash = Util.HashUtil.GenerateConfirmHash(this.RequestIdentifier, secretWord);
            return (computedHash == this.Hash);
        }

        public OperationResult Result { get; private set; }
        public ConfirmResponse()
        {
            this.Result = new OperationResult();
        }






        public string Reference { get; set; }

        public override string ToString()
        {

            return "Success: " + this.Success + " - Identifier: " + this.RequestIdentifier + ", Ref: " + this.Reference;
        }

    }
}
