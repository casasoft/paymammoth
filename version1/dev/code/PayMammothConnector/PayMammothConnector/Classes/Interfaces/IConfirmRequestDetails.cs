﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.Connector.Classes.Interfaces
{
    public interface IConfirmRequestDetails
    {
        string RequestIdentifier { get; set; }
        string HashValue { get; set; }
    }
}
