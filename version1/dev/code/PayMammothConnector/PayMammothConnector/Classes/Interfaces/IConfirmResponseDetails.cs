﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.Connector.Classes.Interfaces
{
    public interface IConfirmResponseDetails
    {
        string RequestIdentifier { get; set; }
        string AuthCode { get; set; }
        string Reference { get; set; }
        bool Success { get; set; }
        bool RequiresManualPayment { get; set; }
        Enums.PaymentMethodSpecific? PaymentMethod { get; set; }
        bool IsFakePayment { get; set; }
        string Hash { get; set; }
    }
}
