﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.Connector.Classes.Interfaces
{
    public interface IClientDetails
    {
        string ClientFirstName { get; set; }
         string ClientMiddleName { get; set; }
         string ClientLastName { get; set; }
         string ClientAddress1 { get; set; }
         string ClientAddress2 { get; set; }
         string ClientAddress3 { get; set; }
         string ClientLocality { get; set; }
         string ClientTelephone { get; set; }
         string ClientMobile { get; set; }
         string ClientReference { get; set; }
         string GetClientFullName();
         
         string GetAddressAsOneLine(bool includeCountry);
         CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? ClientCountry { get; set; }
         
          
         string ClientPostCode { get; set; }
         string ClientEmail { get; set; }
    }
}
