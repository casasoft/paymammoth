﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Connector.Classes.Requests;

namespace PayMammoth.Connector.Classes.Interfaces
{
    public interface IPaymentRequestDetails : IClientDetails
    {
        IEnumerable<IItemDetail> GetItemDetails();
        double PriceExcTax { get; set; }
        
        double TaxAmount { get; set; }
        double HandlingAmount { get; set; }
        double ShippingAmount { get; set; }
        long GetTotalPriceInCents();
        CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639? LanguageCode { get; set; }
        CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? LanguageCountryCode { get; set; }
        string LanguageSuffix { get; set; }

        string ClientIp { get; set; }

        string Reference { get; set; }
        string Description { get; set; }
        /// <summary>
        /// This is only used for serialization / deserialization.  To process this, call 'GetItemDetails()'
        /// </summary>
        string ItemDetailsStr { get; set; }

        string Notes { get; set; }

        CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 CurrencyCode { get; set; }
        string ReturnUrlSuccess { get; set; }
        string ReturnUrlFailure { get; set; }
        string OrderLinkOnClientWebsite { get; set; }
        string Title { get; set; }

            /*
             * Continue implementing the payment request details interface, and the payment request item details
             * These should then be implemented both by QuerystringRequestDetails and QuerystringRequestItemDetails, as well as by PaymentRequest and PaymentRequestItem
             */

        
    }
}
