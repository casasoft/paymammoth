﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.Connector.Classes.Requests
{
    public interface IItemDetail
    {
        string Title { get; set; }
        string Description { get; set; }
    }
}
