﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.HelperClasses;
using CS.General_v3.URL;
using PayMammoth.Connector.Classes.Interfaces;
using CS.General_v3.Classes.Serialization;
using log4net;
using PayMammoth.Connector.Classes.Responses;

namespace PayMammoth.Connector.Classes.Requests
{
    public class ConfirmRequest : BaseSerializable, IConfirmRequestDetails
    {
       
        

        private static readonly ILog _log = LogManager.GetLogger(typeof(ConfirmRequest));
        #region IConfirmRequestDetails Members
        public string SecretWord { get; set; }
        public string RequestIdentifier { get; set; }

        string IConfirmRequestDetails.HashValue
        {
            get
            {
                return this.ComputeHash();
                
            }
            set { throw new InvalidOperationException("This can never be setted"); }

        }
        public string ComputeHash()
        {
            return Util.HashUtil.GenerateConfirmHash(this.RequestIdentifier, this.SecretWord);
        }

        public ConfirmRequest(string requestID)
        {
            this.RequestIdentifier = requestID;
            this.SecretWord = Constants.SecretWord;
        }

        private void verifyRequirements()
        {
            StringBuilder sb = new StringBuilder();
            if (string.IsNullOrWhiteSpace(this.SecretWord))
                sb.AppendLine("Please fill in 'SecretWord'");
            if (sb.Length > 0)
                throw new InvalidOperationException(sb.ToString());
        }

        public ConfirmResponse Confirm()
        {
            if (_log.IsDebugEnabled) _log.Debug("Starting confirm response");
            verifyRequirements();
            bool ok = false;
            CS.General_v3.Classes.URL.URLClass urlClass = new CS.General_v3.Classes.URL.URLClass(Constants.PayMammothConfirmHandlerUrl);
            base.serializeToNameValueCollection<IConfirmRequestDetails>(urlClass.QS);
            string getUrl = urlClass.ToString();

            string response = null;
            try
            {
                if (_log.IsDebugEnabled) _log.Debug("Sending confirm response");
                response =CS.General_v3.Util.Web.HTTPGet(getUrl);
                if (_log.IsDebugEnabled) _log.Debug("Sent confirm response");
            }
            catch (Exception ex)
            {
                _log.Warn("Error occurred during trying to confirm request", ex);
                response = null;
            }
            ConfirmResponse confirmResp = new ConfirmResponse();
            
            QueryString qsResponse = new QueryString(response);
            confirmResp.LoadFromNameValueCollection(qsResponse);
            var result = confirmResp.Result;
            if (!confirmResp.VerifyHash(PayMammoth.Connector.Constants.SecretWord))
            {
                if (_log.IsWarnEnabled) _log.Warn("Confirm response - hash invalid");
                result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, "Hash invalid");
            }
            else if (!confirmResp.Success)
            {
                if (_log.IsWarnEnabled) _log.Warn("Confirm response - failed");
                result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, "Failed");
            }
            else if (confirmResp.Success)
            {
                if (_log.IsDebugEnabled) _log.Debug("Confirm response - OK");
            }
            return confirmResp;
            



        }
        #endregion
    }
}
