﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Connector.Classes.Interfaces;

namespace PayMammoth.Connector.Extensions
{
    public static class PaymentRequestDetailsExtensions
    {
        public static double GetTotalPrice(this IPaymentRequestDetails details, bool incTaxes = true, bool incShipping = true, bool incHandling = true)
        {
            double d = details.PriceExcTax;
            if (incTaxes) d += details.TaxAmount;
            if (incShipping) d += details.ShippingAmount;
            if (incHandling) d += details.HandlingAmount;
            return d;


        }

        

        public static int GetTotalPriceInCents(this IInitialRequestDetails initRequest)
        {
            return (int)Math.Round(GetTotalPrice(initRequest) * 100);

        }
    }
}
