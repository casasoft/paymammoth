﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Connector.Classes.Requests;

namespace PayMammoth.Connector.Extensions
{
    public static class ItemDetailExtensions
    {
        public static bool CheckIfItemDetailsAreEmpty(this IEnumerable<IItemDetail> itemDetails)
        {
            bool isEmpty = true;
            if (itemDetails != null)
            {
                foreach (var item in itemDetails)
                {
                    if (item != null && !string.IsNullOrWhiteSpace(item.Title))
                    {
                        isEmpty = false;
                        break;
                    }
                    
                }
            }
            return isEmpty;
        }

    }
}
