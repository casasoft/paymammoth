﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Connector.Classes.Interfaces;
using CS.General_v3.Classes.Serialization;
using System.Collections.Specialized;
using CS.General_v3.Classes.HelperClasses;

namespace PayMammoth.Connector.Classes.Responses
{
    public class ImmediatePaymentConfirmationResponse : BaseConfirmResponse , IConfirmationResponse
    {
        
        public string AuthCode { get; set; }
        /// <summary>
        /// This is the PayMammoth Identifier
        /// </summary>
        public string PayMammothRequestIdentifier { get; set; }
        
        public bool RequiresManualPayment { get; set; }
        public bool IsFakePayment { get; set; }
        public Enums.PaymentMethodSpecific? PaymentMethod { get; set; }
        public string Hash { get; set; }
        
        
       



        /// <summary>
        /// This is the external reference, e.g Order reference
        /// </summary>
        public string ClientReference { get; set; }
       
        public bool RecurringPaymentEnabled
        {
            get;
            set;
        }

        public string RecurringPaymentID
        {
            get;
            set;
        }


        public Enums.RECURRING_CREATEPROFILE_STATUS_MESSAGE RecurringPaymentCreateStatus
        {
            get;
            set;
        }

       
        public override bool VerifyHash(string secretWord)
        {
            string computedHash = Util.HashUtil.GenerateConfirmHash(this.PayMammothRequestIdentifier, secretWord);
            return (computedHash == this.Hash);
        }

        
        public ImmediatePaymentConfirmationResponse()
        {
            
            this.ResponseType = Enums.NOTIFICATION_RESPONSE_TYPE.ImmediatePayment;
                
        }



        public override string ToString()
        {

            return "Success: " + this.ConfirmationSuccess + " - Identifier: " + this.PayMammothRequestIdentifier + ", Ref: " + this.ClientReference;
        }

        public override void SerializeToNameValueCollection(NameValueCollection nv)
        {
            CS.General_v3.Util.ReflectionUtil.SerializeObjectToNameValueCollection(nv, typeof(ImmediatePaymentConfirmationResponse), this);
        }
        public override void DeserializeFromNameValueCollection(NameValueCollection nv)
        {
            CS.General_v3.Util.ReflectionUtil.DeserializeObjectFromNameValueCollection(nv, typeof(ImmediatePaymentConfirmationResponse), this);
            
        }
       
    }
}
