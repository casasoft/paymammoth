﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace PayMammoth.Connector.Classes.Responses
{
    public abstract class BaseConfirmResponse : IConfirmationResponse
    {
        public static BaseConfirmResponse CreateFromResponse(NameValueCollection nv)
        {

            
			
            string responseTypeKey = CS.General_v3.Util.ReflectionUtil<BaseConfirmResponse>.GetPropertyName(x => x.ResponseType);
            string responseTypeStr = nv[responseTypeKey];
            Enums.NOTIFICATION_RESPONSE_TYPE? responseType = CS.General_v3.Util.EnumUtils.GetEnumByStringValueNullable<Enums.NOTIFICATION_RESPONSE_TYPE>(responseTypeStr);
            BaseConfirmResponse result = null;
            if (responseType.HasValue)
            {
                switch (responseType.Value)
                {
                    case Enums.NOTIFICATION_RESPONSE_TYPE.ImmediatePayment:
                        {
                            result = new ImmediatePaymentConfirmationResponse();
                            break;
                        }
                    case Enums.NOTIFICATION_RESPONSE_TYPE.RecurringPaymentFailure:
                    case Enums.NOTIFICATION_RESPONSE_TYPE.RecurringPaymentSuccess:
                        {
                            result = new RecurringPaymentConfirmationResponse(responseType.Value);
                            break;
                        }
                    case Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileActivated:
                    case Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileCancelled:
                    case Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileCouldNotCreate:
                    case Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileExpired:
                    case Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileSuspended:
                        {
                            result = new RecurringProfileUpdateConfirmationResponse(responseType.Value);
                            break;
                        }
                    default:
                        {
                            throw new InvalidOperationException(string.Format("Not implemented for type <{0}>", responseType.Value));
                            break;
                        }
                }
                result.DeserializeFromNameValueCollection(nv);
            }
            return result;

        }

        public string Message { get; set; }
        /// <summary>
        /// Whether the confirmation response was a success
        /// </summary>
        public bool ConfirmationSuccess { get; set; }
        public Enums.NOTIFICATION_RESPONSE_TYPE? ResponseType { get; set; }

        public abstract bool VerifyHash(string secretWord);

        
        public abstract void SerializeToNameValueCollection(System.Collections.Specialized.NameValueCollection nv);
        public abstract void DeserializeFromNameValueCollection(System.Collections.Specialized.NameValueCollection nv);

        
    }
}
