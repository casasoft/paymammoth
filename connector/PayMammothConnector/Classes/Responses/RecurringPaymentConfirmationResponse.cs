﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Connector.Classes.Interfaces;
using CS.General_v3.Classes.Serialization;
using System.Collections.Specialized;
using CS.General_v3.Classes.HelperClasses;

namespace PayMammoth.Connector.Classes.Responses
{
    public class RecurringPaymentConfirmationResponse : BaseConfirmResponse , IConfirmationResponse
    {
        
        
        /// <summary>
        /// This is the PayMammoth Identifier
        /// </summary>
        public string RequestIdentifier { get; set; }
        
        
        public bool IsFakePayment { get; set; }
        public Enums.PaymentMethodSpecific? PaymentMethod { get; set; }
        public string Hash { get; set; }
        
        
        public string ExternalGatewayReference { get; set; }
       
       
        public override bool VerifyHash(string secretWord)
        {
            string computedHash = Util.HashUtil.GenerateConfirmHash(this.RequestIdentifier, secretWord);
            return (computedHash == this.Hash);
        }


        public RecurringPaymentConfirmationResponse(Enums.NOTIFICATION_RESPONSE_TYPE responseType)
        {

            this.ResponseType = responseType;
            
                
        }

        public DateTime? TransactionTimestamp { get; set; }
        public double? TotalAmount { get; set; }
        public string ProfileReference { get; set; }

        public Enums.RecurringProfilePaymentStatus? Status { get; set; }

        public override string ToString()
        {

            return "Success: " + this.ConfirmationSuccess + " - Identifier: " + this.RequestIdentifier + ", Ref: " + this.ExternalGatewayReference;
        }

        public override void SerializeToNameValueCollection(NameValueCollection nv)
        {
            CS.General_v3.Util.ReflectionUtil.SerializeObjectToNameValueCollection(nv, typeof(RecurringPaymentConfirmationResponse), this);
        }
        public override void DeserializeFromNameValueCollection(NameValueCollection nv)
        {
            CS.General_v3.Util.ReflectionUtil.DeserializeObjectFromNameValueCollection(nv, typeof(RecurringPaymentConfirmationResponse), this);
            
        }


        
    }
}
