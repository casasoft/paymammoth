﻿using System.Collections.Specialized;

namespace PayMammoth.Connector.Classes.Responses
{
    public interface IConfirmationResponse
    {
        void SerializeToNameValueCollection(NameValueCollection nv);
        Enums.NOTIFICATION_RESPONSE_TYPE? ResponseType { get; set; }
        string Message { get; set; }
        /// <summary>
        /// This signifies that the confirmation was OK, and not that the actual item was successful.
        /// </summary>
        bool ConfirmationSuccess { get; set; }
    }
}
