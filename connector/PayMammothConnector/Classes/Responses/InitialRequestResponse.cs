﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.Classes.HelperClasses;

namespace PayMammoth.Connector.Classes.Responses
{
    public class InitialRequestResponse
    {
        public InitialRequestResponse()
        {
            this.Result = new OperationResult();
        }

        public OperationResult Result { get; set; }
        public PayMammoth.Connector.Enums.InitialRequestResponseStatus StatusCode { get; set; } 
        public string PayMammothIdentifier { get; set; }

        public void RedirectToPayMammoth()
        {
            if (this.Result.Status == CS.General_v3.Enums.STATUS_MSG_TYPE.Success)
            {
                var response = this;
                if (response == null || (response != null && response.Result.Status != CS.General_v3.Enums.STATUS_MSG_TYPE.Success))
                    throw new InvalidOperationException("You must first call 'CreateRequestWithPayMammoth' successfully, before you can redirect");
                CS.General_v3.Util.ContractsUtil.RequiresNotNullable(response, null);
                CS.General_v3.Util.ContractsUtil.RequiresNotNullable(response.PayMammothIdentifier, null);
                CS.General_v3.Classes.URL.URLClass urlClass = new CS.General_v3.Classes.URL.URLClass(Constants.PayMammothPaymentChoiceUrl);
                urlClass[Constants.PARAM_IDENTIFIER] = response.PayMammothIdentifier;
                urlClass.RedirectTo();
            }
            else
            {
                throw new InvalidOperationException("You cannot call redirect to payMammoth if the operation was not successful, Error: <" + this.StatusCode + ">");
            }
        }

    }
}
