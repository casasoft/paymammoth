﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Connector.Classes.Interfaces;
using CS.General_v3.Classes.Serialization;
using System.Collections.Specialized;
using CS.General_v3.Classes.HelperClasses;

namespace PayMammoth.Connector.Classes.Responses
{
    public class RecurringProfileUpdateConfirmationResponse : BaseConfirmResponse , IConfirmationResponse
    {
        
        
        /// <summary>
        /// This is the PayMammoth Identifier
        /// </summary>
        public string ProfileIdentifier { get; set; }
        
        
        
        public string Hash { get; set; }
        
        public string ExternalGatewayReference { get; set; }
       
       
        public override bool VerifyHash(string secretWord)
        {
            string computedHash = Util.HashUtil.GenerateConfirmHash(this.ProfileIdentifier, secretWord);
            return (computedHash == this.Hash);
        }


        public RecurringProfileUpdateConfirmationResponse(Enums.NOTIFICATION_RESPONSE_TYPE responseType)
        {

            this.ResponseType = responseType;
            
                
        }

        


        public override string ToString()
        {

            return "Success: " + this.ConfirmationSuccess + " - Identifier: " + this.ProfileIdentifier;
        }

        public override void SerializeToNameValueCollection(NameValueCollection nv)
        {
            CS.General_v3.Util.ReflectionUtil.SerializeObjectToNameValueCollection(nv, typeof(RecurringProfileUpdateConfirmationResponse), this);
        }
        public override void DeserializeFromNameValueCollection(NameValueCollection nv)
        {
            CS.General_v3.Util.ReflectionUtil.DeserializeObjectFromNameValueCollection(nv, typeof(RecurringProfileUpdateConfirmationResponse), this);
            
        }
       
    }
}
