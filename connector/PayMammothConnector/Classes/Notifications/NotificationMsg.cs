﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace PayMammoth.Connector.Classes.Notifications
{
    public interface INotificationMsg
    {
         string Identifier { get; set; }
         string UniqueId { get; set; }
         string Message { get; set; }
         string WebsiteAccountCode { get; set; }
         Connector.Enums.NOTIFICATION_STATUS_CODE? StatusCode { get; set; }
         Connector.Enums.NOTIFICATION_RESPONSE_TYPE? NotificationType { get; set; }
         string ConfirmationHash { get; set; }
    }

    public class NotificationMsg : INotificationMsg
    {
        public string Identifier { get; set; }
        public string UniqueId { get; set; }
        public string Message { get; set; }
        public string WebsiteAccountCode { get; set; }
        public Connector.Enums.NOTIFICATION_STATUS_CODE? StatusCode { get; set; }
        public Connector.Enums.NOTIFICATION_RESPONSE_TYPE? NotificationType { get; set; }
        public string ConfirmationHash { get; set; }

        public void DeserializerFromNameValueCollection(NameValueCollection nv)
        {
            CS.General_v3.Util.ReflectionUtil.DeserializeObjectFromNameValueCollection<INotificationMsg>(nv, this);
            
            
        }
        public void SerializeToNameValueCollection(NameValueCollection nv)
        {
            CS.General_v3.Util.ReflectionUtil.SerializeObjectToNameValueCollection<INotificationMsg>(nv, this);


        }

        



    }
}
