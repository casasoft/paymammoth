﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Connector.Classes.Responses;
using log4net;

namespace PayMammoth.Connector.Classes.Notifications
{


    //NotificationConfirmationParserService - This is the component name, e.g ProfileActivationNotificationSenderService				

    public interface INotificationConfirmationParserService
    {
        void ParseConfirmationResponse(INotificationMsg msg, IConfirmationResponse responseGeneric);
    }

   
    public class NotificationConfirmationParserService : INotificationConfirmationParserService
    {

        private static readonly ILog _log = LogManager.GetLogger(typeof(NotificationConfirmationParserService));
		
			
        private static readonly NotificationConfirmationParserService _instance = new NotificationConfirmationParserService();

        public static NotificationConfirmationParserService Instance
        {
            get { return _instance; }
        }

        public  void ParseConfirmationResponse(INotificationMsg msg, IConfirmationResponse responseGeneric)
        {
            //do something

            
            if (responseGeneric.ResponseType.HasValue)
            {
                switch (responseGeneric.ResponseType.Value)
                {
                    case Enums.NOTIFICATION_RESPONSE_TYPE.ImmediatePayment:
                        {
                            ImmediatePaymentConfirmationResponse response = (ImmediatePaymentConfirmationResponse)responseGeneric;
                            Events.invokeOnPaidSuccessfully(msg, response);
                            break;
                        }
                    case Enums.NOTIFICATION_RESPONSE_TYPE.RecurringPaymentFailure:
                    case Enums.NOTIFICATION_RESPONSE_TYPE.RecurringPaymentSuccess:
                        {
                            RecurringPaymentConfirmationResponse response = (RecurringPaymentConfirmationResponse)responseGeneric;

                            Events.invokeOnRecurringPayment(msg, response);
                            break;
                        }
                    case Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileCancelled:
                    case Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileCouldNotCreate:
                    case Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileExpired:
                    case Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileSuspended:
                    case Enums.NOTIFICATION_RESPONSE_TYPE.RecurringProfileActivated:
                        {
                            RecurringProfileUpdateConfirmationResponse response = (RecurringProfileUpdateConfirmationResponse)responseGeneric;

                            Events.invokeOnRecurringProfileStatusChange(msg, response);
                            break;
                        }
                    default:
                        {
                            _log.WarnFormat("Not implemented for type <{0}>", responseGeneric.ResponseType);
                            break;
                        }

                }
            }

        }

			
    }
			
}
