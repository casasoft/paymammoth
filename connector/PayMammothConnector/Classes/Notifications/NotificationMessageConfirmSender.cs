﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Connector.Classes.Requests;
using PayMammoth.Connector.Classes.Responses;

namespace PayMammoth.Connector.Classes.Notifications
{
    
    public class NotificationMessageConfirmSender
    {
        public IConfirmationResponse ConfirmNotification(NotificationMsg msg)
        {
            
            ConfirmRequest request = new ConfirmRequest(msg);
            return request.Confirm();
        }
    }
}
