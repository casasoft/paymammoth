﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace PayMammoth.Connector.Classes.Interfaces
{
    public interface IInitialRequestDetails : IPaymentRequestDetails
    {

        string HashValue { get; set;  }

        string WebsiteAccountCode { get; set; }

        
    }
}
