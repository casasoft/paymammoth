﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Connector.Classes.Requests;

namespace PayMammoth.Connector.Classes.Interfaces
{
    public interface IPaymentRequestDetails : IClientDetails
    {
        IEnumerable<IItemDetail> GetItemDetails();
        double PriceExcTax { get; set; }
        
        double TaxAmount { get; set; }
        double HandlingAmount { get; set; }
        double ShippingAmount { get; set; }
        long GetTotalPriceInCents();
        CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639? LanguageCode { get; set; }
        CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? LanguageCountryCode { get; set; }
        string LanguageSuffix { get; set; }

        string ClientIp { get; set; }

        string Reference { get; set; }
        string Description { get; set; }
        /// <summary>
        /// This is only used for serialization / deserialization.  To process this, call 'GetItemDetails()'
        /// </summary>
        string ItemDetailsStr { get; set; }

        string Notes { get; set; }
        /// <summary>
        /// This is the url for sending backend notification messages.  If this is empty or null, the website account's is taken
        /// </summary>
        string NotificationUrl { get; set; }
        CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 CurrencyCode { get; set; }
        string ReturnUrlSuccess { get; set; }
        string ReturnUrlFailure { get; set; }
        string OrderLinkOnClientWebsite { get; set; }
        string Title { get; set; }

        /// <summary>
        /// If true, the system will create a RecurringProfile based on this transaction - make sure to fill in the RecurringProfile_ fields!
        /// </summary>
        bool RecurringProfileRequired { get; set; }

        /// <summary>
        /// (Required) Unit for billing during this subscription period.
        /// For SemiMonth, billing is done on the 1st and 15th of each month.
        /// NOTE:The combination of BillingPeriod and BillingFrequency cannot exceed one year.
        /// </summary>
        Enums.RECURRING_BILLINGPERIOD RecurringProfile_BillingPeriod { get; set; }

        /// <summary>
        /// (Optional) Number of billing cycles for payment period.
        /// For the regular payment period, if no value is specified or the value is 0, the regular payment period continues 
        /// until the profile is canceled or deactivated.
        /// For the regular payment period, if the value is greater than 0, the regular payment period will expire after the 
        /// trial period is finished and continue at the billing frequency for TotalBillingCycles cycles.
        /// </summary>
        int? RecurringProfile_TotalBillingCycles { get; set; }
        /// <summary>
        /// (Optional) Number of billing cycles for payment period.
        /// For the regular payment period, if no value is specified or the value is 0, the regular payment period continues 
        /// until the profile is canceled or deactivated.
        /// For the regular payment period, if the value is greater than 0, the regular payment period will expire after the 
        /// trial period is finished and continue at the billing frequency for TotalBillingCycles cycles.
        /// </summary>
        int RecurringProfile_MaxFailedAttempts { get; set; }

        /// <summary>
        /// (Required) Number of billing periods that make up one billing cycle.
        ///The combination of billing frequency and billing period must be less than or equal to one year. 
        /// For example, if the billing cycle is Month, the maximum value for billing frequency is 12. Similarly, 
        /// if the billing cycle is Week, the maximum value for billing frequency is 52.
        /// NOTE: If the billing period is SemiMonth., the billing frequency must be 1.
        /// </summary>
        int RecurringProfile_BillingFrequency { get; set; }

            /*
             * Continue implementing the payment request details interface, and the payment request item details
             * These should then be implemented both by QuerystringRequestDetails and QuerystringRequestItemDetails, as well as by PaymentRequest and PaymentRequestItem
             */

        
    }
}
