﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.Connector.Classes.Requests
{
    public class ItemDetailImpl : IItemDetail
    {
        public ItemDetailImpl()
        {

        }
        public ItemDetailImpl(string title, string description = null)
        {
            this.Title = title;
            this.Description = description;
        }

        #region IItemDetail Members

        public string Title { get; set; }

        public string Description { get; set; }

        #endregion
    }
}
