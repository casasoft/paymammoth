﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CS.General_v3.URL;
using PayMammoth.Connector.Classes.Interfaces;
using CS.General_v3.Classes.Serialization;
using PayMammoth.Connector.Classes.Notifications;
using log4net;
using PayMammoth.Connector.Classes.Responses;
using System.Collections.Specialized;

namespace PayMammoth.Connector.Classes.Requests
{
    public class ConfirmRequest : BaseSerializable, IConfirmRequestDetails
    {
       
        

        private static readonly ILog _log = LogManager.GetLogger(typeof(ConfirmRequest));
        #region IConfirmRequestDetails Members
        public string SecretWord { get; set; }
        public NotificationMsg NotificationMsg { get; set; }

        public string RequestIdentifier { get; set; }

        

        string IConfirmRequestDetails.HashValue
        {
            get
            {
                return this.ComputeHash();
                
            }
            set { throw new InvalidOperationException("This can never be setted"); }

        }
        public string ComputeHash()
        {
            return Util.HashUtil.GenerateConfirmHash(this.RequestIdentifier, this.SecretWord);
        }

        public ConfirmRequest(NotificationMsg msg)
        {
            this.NotificationMsg = msg;
            this.SecretWord = Constants.SecretWord;
        }

        private void verifyRequirements()
        {
            StringBuilder sb = new StringBuilder();
            if (string.IsNullOrWhiteSpace(this.SecretWord))
                sb.AppendLine("Please fill in 'SecretWord'");
            if (sb.Length > 0)
                throw new InvalidOperationException(sb.ToString());
        }

        public IConfirmationResponse Confirm()
        {
            
            if (_log.IsDebugEnabled) _log.Debug("Starting confirm response");
            verifyRequirements();
            bool ok = false;
            this.NotificationMsg.ConfirmationHash = Util.HashUtil.GenerateConfirmHash(this.NotificationMsg.Identifier, this.SecretWord);
            NameValueCollection nv = new NameValueCollection();
            CS.General_v3.Classes.URL.URLClass urlClass = new CS.General_v3.Classes.URL.URLClass(Constants.PayMammothConfirmHandlerUrl);
            CS.General_v3.Util.ReflectionUtil.SerializeObjectToNameValueCollection(nv, typeof(NotificationMsg), this.NotificationMsg);
            
            string getUrl = urlClass.ToString();

            string response = null;
            try
            {
                if (_log.IsDebugEnabled) _log.Debug("Sending confirm response");
                response =CS.General_v3.Util.Web.HTTPPost(getUrl, nv);
                if (_log.IsDebugEnabled) _log.Debug("Sent confirm response");
            }
            catch (Exception ex)
            {
                _log.Warn("Error occurred during trying to confirm request", ex);
                response = null;
            }
            QueryString qsResponse = new QueryString(response);
            BaseConfirmResponse confirmResp = BaseConfirmResponse.CreateFromResponse(qsResponse);

            if (confirmResp != null)
            {
                if (!confirmResp.VerifyHash(PayMammoth.Connector.Constants.SecretWord))
                {
                    confirmResp.Message = "Confirm response - hash invalid";
                    if (_log.IsWarnEnabled) _log.Warn("Confirm response - hash invalid");

                }
                else
                {
                    if (_log.IsDebugEnabled) _log.Debug("Confirm response - OK");
                }
            }
            return confirmResp;
            



        }
        #endregion


    }
}
