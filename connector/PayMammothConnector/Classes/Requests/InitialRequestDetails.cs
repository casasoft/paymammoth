﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using PayMammoth.Connector.Classes.Interfaces;
using CS.General_v3.Classes.Serialization;
using CS.General_v3.URL;
using CS.General_v3.Classes.HelperClasses;
using PayMammoth.Connector.Classes.Responses;
using System.Collections.Specialized;
using PayMammoth.Connector.Classes.Serialization;

namespace PayMammoth.Connector.Classes.Requests
{
    public class InitialRequestDetails : BaseSerializable, IInitialRequestDetails
    {
        protected static readonly ILog _log = LogManager.GetLogger(typeof(InitialRequestDetails));
        #region IPaymentRequestDetails Members

        public double PriceExcTax{get;set;}

        public double TaxAmount { get; set; }

        public double HandlingAmount { get; set; }

        public double ShippingAmount { get; set; }

        public string Reference { get; set; }
        public string Description { get; set; }
        public string Notes { get; set; }

        public CS.General_v3.Enums.ISO_ENUMS.CURRENCY_ISO4217 CurrencyCode { get; set; }
        

        public string ReturnUrlSuccess { get; set; }

        public string ReturnUrlFailure { get; set; }

        public string OrderLinkOnClientWebsite { get; set; }

        public string Title { get; set; }

        public string NotificationUrl { get; set; }

        #endregion

        #region IClientDetails Members

        public string ClientFirstName { get; set; }

        public string ClientMiddleName { get; set; }
        public string ClientLastName { get; set; }
        public string ClientAddress1 { get; set; }
        public string ClientAddress2 { get; set; }
        public string ClientAddress3 { get; set; }

        public string ClientLocality { get; set; }

        //public string PayMammothIdentifier { get; set; }

        /// <summary>
        /// This should be the 3-letter version of the ISO enum
        /// </summary>
        public CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? ClientCountry { get; set; }

        public string ClientPostCode { get; set; }
        public string ClientEmail { get; set; }

        #endregion

        public InitialRequestDetails(string websiteAccountCode, string secretWord)
        {
            this.SecretWord = secretWord;
            this.WebsiteAccountCode = websiteAccountCode;

            Constants.SecretWord = this.SecretWord;
            Constants.WebsiteAccountCode = this.WebsiteAccountCode;

            this.ClientIp = CS.General_v3.Util.PageUtil.GetUserIP();
            //this.ReturnUrlFailure = Constants.PaymentUrl_Failure;
            //this.ReturnUrlSuccess = Constants.PaymentUrl_Success;
            this.ItemDetails = new List<IItemDetail>();
            
        }

        public string SecretWord { get; set; }

        public string ComputeHash()
        {
            return Util.HashUtil.GenerateInitialRequestHash(this.WebsiteAccountCode, this.Title, Extensions.PaymentRequestDetailsExtensions.GetTotalPriceInCents(this), this.SecretWord);
        }

        public List<IItemDetail> ItemDetails { get; private set; }

        #region IInitialRequestDetails Members

        //string IInitialRequestDetails.Hash
        //{
        //    get { return this.ComputeHash(); }
        //}

        #endregion

        #region IInitialRequestDetails Members


        public string WebsiteAccountCode { get; set; }



        #endregion

        public void VerifyRequirements()
        {
            StringBuilder sb = new StringBuilder();
            if (this.PriceExcTax <= 0) sb.AppendLine("Please fill in 'PriceExcTax' with a value greater than 0");
            if (string.IsNullOrWhiteSpace(this.SecretWord)) sb.AppendLine("Please fill in 'Secret Word'");
            if (string.IsNullOrWhiteSpace(this.WebsiteAccountCode)) sb.AppendLine("Please fill in 'Website Account Code'");
            if (string.IsNullOrWhiteSpace(this.Title)) sb.AppendLine("Please fill in 'Title'");
            if (string.IsNullOrWhiteSpace(this.Reference)) sb.AppendLine("Please fill in 'Reference'");
            if (string.IsNullOrWhiteSpace(this.ReturnUrlFailure)) sb.AppendLine("Please fill in 'Return Url Failure'");
            if (string.IsNullOrWhiteSpace(this.ReturnUrlSuccess)) sb.AppendLine("Please fill in 'Return Url Success'");
            if (sb.Length > 0)
                throw new InvalidOperationException(sb.ToString());
        }


        public InitialRequestResponse CreateRequestWithPayMammoth()
        {

            VerifyRequirements();
            InitialRequestResponse requestResponse = new InitialRequestResponse();
            requestResponse.StatusCode = Enums.InitialRequestResponseStatus.GenericError;
            
            string payMammothUrl = Constants.PayMammothInitPaymentHandlerUrl ;
            NameValueCollection formData = new NameValueCollection();
            base.serializeToNameValueCollection < IInitialRequestDetails>(formData);
            if (_log.IsInfoEnabled) _log.Info("Posting payment initial request to: " + payMammothUrl + ", Data: " + CS.General_v3.Util.ListUtil.ConvertNameValueCollectionToString(formData));
            

            string response = null;

            int retryCount = 0;
            int maxRetries = 5;
            bool responseOk = false;
            while (responseOk == false && retryCount < maxRetries)
            {


                try
                {
                    response = CS.General_v3.Util.Web.HTTPPost(payMammothUrl, formData);
                    responseOk = true;
                }
                catch (Exception ex)
                {
                    responseOk = false;
                    
                    requestResponse.StatusCode = Enums.InitialRequestResponseStatus.ErrorConnectingToServer;
                    _log.Warn("Error while trying to send initial request", ex);
                    requestResponse.Result.AddException(ex);
                    response = null;
                    if (retryCount >= maxRetries)
                    {
                        _log.Error("Could not connect to PayMammoth to send initial request");
                    }
                }
                retryCount++;
            }

            QueryString qs = new QueryString(response);
            if (responseOk)
            {//only proceed if the respones was OK
                try
                {
                    string errorCode = qs[Constants.PARAM_ERRORCODE];
                    if (_log.IsInfoEnabled) _log.Info("Received result: " + qs.ToString());
                    if (string.IsNullOrWhiteSpace(errorCode))
                    {
                        string identifier = qs[Constants.PARAM_IDENTIFIER];
                        string hash = qs[Constants.PARAM_HASH];
                        string computedHash = Util.HashUtil.GenerateInitialResponseHash(identifier, this.SecretWord);
                        if (string.Compare(computedHash, hash, true) == 0)
                        {
                            requestResponse.PayMammothIdentifier = identifier;
                            //all OK it means
                            if (_log.IsInfoEnabled) _log.Info("Initial request OK");
                            requestResponse.StatusCode = Enums.InitialRequestResponseStatus.Success;
                        }
                        else
                        {
                            requestResponse.StatusCode = Enums.InitialRequestResponseStatus.HashNotVerified;
                            if (_log.IsWarnEnabled) _log.Warn("Hash mismatch - Response: " + qs.ToString() + ", Hash received: " + hash + ", Computed Hash: " + computedHash);
                            //if (_log.IsInfoEnabled) _log.Info("Hash mismatch - Response: " + qs.ToString() + ", Hash received: " + hash + ", Computed Hash: " + computedHash);
                            requestResponse.Result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, "Hash not correct - Hash Received: " + hash);
                        }
                    }
                    else
                    {
                        if (_log.IsWarnEnabled) _log.Warn("Error in response: " + qs.ToString() + ", Error Code: " + errorCode);

                        requestResponse.Result.AddStatusMsg(CS.General_v3.Enums.STATUS_MSG_TYPE.Error, qs[Constants.PARAM_ERRORCODE]);
                    }
                }
                catch (Exception ex)
                {
                    _log.Error(string.Format("Error occurred while trying to parse initial request/response details. Response: {0}", response), ex);
                    throw;
                }
            }
            return requestResponse;
            


        }

        
        #region IInitialRequestDetails Members


        
        #endregion

        #region IInitialRequestDetails Members

        string IInitialRequestDetails.HashValue
        {
            get { return this.ComputeHash(); }
            set { throw new InvalidOperationException("This can never be set"); }
        }

        #endregion

        #region IPaymentRequestDetails Members


        #endregion

        #region IClientDetails Members


        #endregion

        #region IPaymentRequestDetails Members



        #endregion

        #region IClientDetails Members


        public string GetAddressAsOneLine(bool includeCountry)
        {
            return Extensions.ClientDetailsExtensions.GetAddressAsOneLine(this, includeCountry);
            
        }


        #endregion



        public string LanguageSuffix { get; set; }


        #region IClientDetails Members


        public string ClientTelephone { get; set; }

        public string ClientMobile { get; set; }

        public CS.General_v3.Enums.ISO_ENUMS.LANGUAGE_ISO639? LanguageCode { get; set; }

        
        #endregion

        #region IPaymentRequestDetails Members


        public CS.General_v3.Enums.ISO_ENUMS.Country_ISO3166? LanguageCountryCode { get; set; }

        #endregion

        #region IPaymentRequestDetails Members


        public long GetTotalPriceInCents()
        {
            return Extensions.PaymentRequestDetailsExtensions.GetTotalPriceInCents(this);
            
        }

        #endregion

        #region IClientDetails Members


        public string GetClientFullName()
        {
            return Extensions.ClientDetailsExtensions.GetClientFullName(this);
            
        }

        #endregion

        #region IPaymentRequestDetails Members


        public string ClientIp { get; set; }

        #endregion

        #region IClientDetails Members


        public string ClientReference { get; set; }

        #endregion

       



        #region IPaymentRequestDetails Members

        IEnumerable<IItemDetail> IPaymentRequestDetails.GetItemDetails()
        {

            return this.ItemDetails;
            
        }

        string IPaymentRequestDetails.ItemDetailsStr
        {
            get
            {
                return ItemDetailsSerializer.SerializeItemDetailsToString(this.ItemDetails);
                
            }
            set
            {
                this.ItemDetails = ItemDetailsSerializer.DeserializeStringToItemDetails(value, () => new ItemDetailImpl());
            }
        }

        #endregion

        
        public void AddItemDetail(string title, string description = null)
        {
            this.ItemDetails.Add(new ItemDetailImpl(title, description));

        }


        public bool RecurringProfileRequired
        {
            get;
            set;
        }

        public Enums.RECURRING_BILLINGPERIOD RecurringProfile_BillingPeriod
        {
            get;
            set;
        }

        public int? RecurringProfile_TotalBillingCycles
        {
            get;
            set;
        }

        public int RecurringProfile_BillingFrequency
        {
            get;
            set;
        }

        #region IPaymentRequestDetails Members


        public int RecurringProfile_MaxFailedAttempts { get; set; }

        #endregion
    }
}
