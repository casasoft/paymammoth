﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using log4net.Repository.Hierarchy;

namespace PayMammoth.Connector.Util
{
    public static class HashUtil
    {
        public static readonly ILog _log = LogManager.GetLogger(typeof(HashUtil));

        public static string GenerateInitialRequestHash(string accountCode, string title, long totalAmountInCents, string secretWord)
        {
            string s = accountCode.ToUpper() + title.ToUpper() + totalAmountInCents + secretWord;
            _log.InfoFormat("GenerateInitialRequestHash: Computing hash of [ {0} ]", s);
            return CS.General_v3.Util.Cryptography.GetSHA512Hash(s);

        }
        public static string GenerateInitialResponseHash(string identifier, string secretWord)
        {
            string s = identifier + secretWord;
            _log.InfoFormat("GenerateInitialResponseHash [identifier/password]: Computing hash of [ {0} ]", s);
            return CS.General_v3.Util.Cryptography.GetSHA512Hash(s);

        }
        public static string GenerateConfirmHash(string identifier, string secretWord)
        {
            string s = identifier + secretWord;
            _log.InfoFormat("GenerateConfirmHash: Computing hash of [ {0} ]", s);
            return CS.General_v3.Util.Cryptography.GetSHA512Hash(s);

        }

        public static string GenerateInitialRequestHash(PayMammoth.Connector.Classes.Interfaces.IInitialRequestDetails requestDetails, string secretWord)
        {
            return GenerateInitialRequestHash(requestDetails.WebsiteAccountCode, requestDetails.Title, requestDetails.GetTotalPriceInCents(), secretWord);

        }
    }

    public static class HashUtil_old
    {
        public static readonly ILog _log = LogManager.GetLogger(typeof(HashUtil));
        public static string GenerateInitialRequestHash(string accountCode, string title, long totalAmountInCents, string secretWord)
        {
            string s = accountCode.ToUpper() + title.ToUpper() + totalAmountInCents + secretWord;
            _log.DebugFormat("GenerateInitialRequestHash: Computing hash of [ {0} ]", s);
            return CS.General_v3.Util.Cryptography.GetSHA512Hash(s);
            
        }
        public static string GenerateInitialResponseHash(string identifier, string secretWord)
        {
            
            return GenerateConfirmHash(new List<string>() { identifier }, secretWord);
            

        }
        public static string GenerateConfirmHash(string identifier, string secretWord)
        {
            string s = identifier + secretWord;

            return GenerateConfirmHash(new List<string>() { identifier }, secretWord);

        }
        public static string GenerateConfirmHash(List<string> keys, string secretWord)
        {
            StringBuilder sb = new StringBuilder();
            if (keys != null)
            {
                foreach (var key in keys)
                {
                    sb.Append(key);
                }
            }
            sb.Append(secretWord);
            string s = sb.ToString();
            _log.DebugFormat("GenerateConfirmHash: Computing hash of [ {0} ]", s);
            return CS.General_v3.Util.Cryptography.GetSHA512Hash(s);

        }
        public static string GenerateInitialRequestHash(PayMammoth.Connector.Classes.Interfaces.IInitialRequestDetails requestDetails, string secretWord)
        {
            return GenerateInitialRequestHash(requestDetails.WebsiteAccountCode, requestDetails.Title, requestDetails.GetTotalPriceInCents(), secretWord);
            
        }
    }
}
