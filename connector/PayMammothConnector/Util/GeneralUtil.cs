﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PayMammoth.Connector.Util
{
    public static class GeneralUtil
    {
        /// <summary>
        /// This should return the date time, after adding a period to a specific date.  E.g adding 2 months to 3rd Jan would be 3rdMarch.
        /// </summary>
        /// <param name="relativeDate"></param>
        /// <param name="period"></param>
        /// <param name="interval"></param>
        /// <returns></returns>
        public static DateTime GetDateTimeAfterIncrementingBillingPeriod(DateTime relativeDate, Enums.RECURRING_BILLINGPERIOD period, int interval)
        {

            //TODO: [For: Karl | 30Nov12] create unit test (WrittenBy: Karl)        			
			
            DateTime newDate = relativeDate;
            switch (period)
            {
                case Enums.RECURRING_BILLINGPERIOD.Day:
                    {
                        newDate = newDate.AddDays(interval);
                        break;
                    }
                case Enums.RECURRING_BILLINGPERIOD.Month:
                    {
                        newDate = newDate.AddMonths(interval);
                        break;
                    }
                case Enums.RECURRING_BILLINGPERIOD.Year:
                    {
                        newDate = newDate.AddYears(interval);
                        break;
                    }
            }
            return newDate;

        }

    }
}
