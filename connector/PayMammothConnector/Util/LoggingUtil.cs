﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Connector.Classes.Notifications;

namespace PayMammoth.Connector.Util
{
    public static class LoggingUtil
    {
        public static string GetNotificationDetailsForLog(NotificationMsg msg)
        {
            return string.Format("[Identifier: {0} | NotificationType: {1} | Msg: {2} | UniqueID: {3}]", msg.UniqueId, msg.NotificationType, msg.Message, msg.UniqueId);
        }
    }
}
