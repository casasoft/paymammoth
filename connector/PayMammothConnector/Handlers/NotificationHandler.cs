﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using CS.General_v3.URL;
using PayMammoth.Connector.Classes.Interfaces;
using PayMammoth.Connector.Classes.Requests;
using PayMammoth.Connector.Classes.Responses;
using log4net;
using PayMammoth.Connector.Classes.Notifications;


namespace PayMammoth.Connector.Handlers
{
    public abstract class NotificationHandler : CS.General_v3.HTTPHandlers.BaseHandler
    {
        private ILog _log = LogManager.GetLogger(typeof(NotificationHandler));

        protected NotificationHandler()
        {
            if (_log.IsDebugEnabled) _log.Debug("NotificationHandler - Constructor");
        }

      

        //private class onPaidSuccessfullyCaller
        //{
        //    private PaymentConfirmationHandler _handler = null;
        //    public onPaidSuccessfullyCaller(PaymentConfirmationHandler handler)
        //    {
        //        _handler = handler;
        //    }

        //    public ConfirmResponse Response { get; set; }
        //    public void Start()
        //    {
        //        _handler.OnPaidSuccessfully(Response);
        //    }

        //}

       
        protected override void processRequest(System.Web.HttpContext ctx)
        {
            if (ctx.Request.Form.AllKeys.Any())
            {//form is filled in
                NotificationMsg msg = new NotificationMsg();
                msg.DeserializerFromNameValueCollection(ctx.Request.Form);
                if (msg.NotificationType.HasValue)
                {
                    ConfirmRequest confirmRequest = new ConfirmRequest(msg);
                    var response = confirmRequest.Confirm();
                    if (response != null && response.ConfirmationSuccess)
                    {
                        NotificationConfirmationParserService.Instance.ParseConfirmationResponse(msg, response);
                        
                    }
                }

                ctx.Response.Write(PayMammoth.Connector.Constants.RESPONSE_OK);

            }

            
            
        }
    }
}
