﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using CS.General_v3.URL;
using PayMammoth.Connector.Classes.Interfaces;
using PayMammoth.Connector.Classes.Requests;
using PayMammoth.Connector.Classes.Responses;
using log4net;

namespace PayMammoth.Connector.Handlers
{
    public abstract class PaymentConfirmationHandler : CS.General_v3.HTTPHandlers.BaseHandler
    {
        private ILog _log = LogManager.GetLogger(typeof(PaymentConfirmationHandler));
        public PaymentConfirmationHandler()
        {
            _log.Debug("PaymentConfirmationHandler - Constructor");
        }

        protected abstract void OnPaidSuccessfully(ImmediatePaymentConfirmationResponse response);

        //private class onPaidSuccessfullyCaller
        //{
        //    private PaymentConfirmationHandler _handler = null;
        //    public onPaidSuccessfullyCaller(PaymentConfirmationHandler handler)
        //    {
        //        _handler = handler;
        //    }

        //    public ConfirmResponse Response { get; set; }
        //    public void Start()
        //    {
        //        _handler.OnPaidSuccessfully(Response);
        //    }

        //}



        protected override void processRequest(System.Web.HttpContext ctx)
        {

            //bool success = false;
            //QueryString qs = new QueryString(ctx.Request.QueryString);
            //if (_log.IsDebugEnabled) _log.Debug("PaymentConfirmationHandler - This Request's Url: " + ctx.Request.Url.ToString());
            //string identifier = qs[Constants.PARAM_IDENTIFIER];
            //if (!string.IsNullOrWhiteSpace(identifier))
            //{
            //    ConfirmRequest req = new ConfirmRequest(identifier);
            //    req.SecretWord = Constants.SecretWord;
            //    if (_log.IsDebugEnabled) _log.Debug(string.Format("[{0}] PaymentConfirmationHandler - Identifier: " + identifier + " - Confirming response", identifier));
            //    var responseResult = req.Confirm();
            //    if (responseResult.Success)
            //    {
            //        if (_log.IsDebugEnabled) _log.Debug(string.Format("[{0}] PaymentConfirmationHandler - Success, calling out 'OnPaidSuccessfully'", identifier));
                    
            //        //onPaidSuccessfullyCaller successfulCaller = new onPaidSuccessfullyCaller(this);
            //        //successfulCaller.Response = responseResult;
            //        //CS.General_v3.Util.ThreadUtil.CallMethodOnSeperateThread(successfulCaller.Start, "OnPaidSuccessful-" + identifier);
            //        OnPaidSuccessfully(responseResult);
            //        success = true;
            //        if (_log.IsDebugEnabled) _log.Debug(string.Format("[{0}] PaymentConfirmationHandler - Success - All OK", identifier));
                    
            //    }
            //    else
            //    {

            //        if (_log.IsDebugEnabled) _log.Debug(string.Format("[{0}] PaymentConfirmationHandler - Failed - " + responseResult.Result.GetMessageAsString(), identifier));
            //    }
            //}
            
            
            //ctx.Response.Write(PayMammoth.Connector.Constants.RESPONSE_OK);
        }
    }
}
