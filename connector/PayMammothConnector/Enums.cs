﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace PayMammoth.Connector
{
    public class Enums
    {
        public enum PaymentMethod
        {
            [Description("PayPal")]
            PayPal = 1000,
            [Description("Credit Card")]
            PaymentGateway = 2000,
            Moneybookers = 3000,
            Cheque = 4000,
            BankTransfer = 5000,
            Neteller = 6000

        }
        //When you add a PaymentMethodSpecific, update the 'GetPaymentMethodFromSpecific' method below.
        public enum PaymentMethodSpecific
        {
            [Description("PayPal")]
            PayPal_ExpressCheckout = 1000,
            [Description("Online Payment")]
            PaymentGateway_Transactium = 2000,
            [Description("Online Payment")]
            PaymentGateway_Apco = 2200,
            [Description("Online Payment")]
            PaymentGateway_Realex = 2400,
            [Description("Money Bookers")]
            Moneybookers = 3000,
            [Description("Cheque")]
            Cheque = 4000,
            [Description("Bank Transfer")]
            BankTransfer = 5000,
            [Description("Neteller")]
            Neteller = 6000,
            [Description("Testing")]
            NUnitTest = 10000
        }

        public static bool GetWhetherPaymentMethodRequiresManualIntervention(PaymentMethodSpecific method)
        {
            switch (method)
            {
                case PaymentMethodSpecific.Cheque:
                    return true;
            }
            return false;
        }

        public enum InitialRequestResponseStatus
        {
            Success,
            HashNotVerified,
            GenericError,
            ErrorConnectingToServer
        }
        

        public static PaymentMethod GetPaymentMethodFromSpecific(PaymentMethodSpecific value)
        {
            switch (value)
            {
                case PaymentMethodSpecific.PayPal_ExpressCheckout:
                    return PaymentMethod.PayPal;

                case PaymentMethodSpecific.PaymentGateway_Transactium:
                case PaymentMethodSpecific.PaymentGateway_Apco:
                case PaymentMethodSpecific.PaymentGateway_Realex:
                    return PaymentMethod.PaymentGateway;

                case PaymentMethodSpecific.Moneybookers:
                    return PaymentMethod.Moneybookers;

                case PaymentMethodSpecific.Cheque:
                    return PaymentMethod.Cheque;
                    
                case PaymentMethodSpecific.BankTransfer:
                    return PaymentMethod.BankTransfer;

                case PaymentMethodSpecific.Neteller:
                    return PaymentMethod.Neteller;

                default:
                    throw new InvalidOperationException("Specific value <" + value + "> not mapped to a payment method");
            }
        }

        /// <summary>
        /// Status messages used when created a recurring profile on a payment gateway.
        /// </summary>
        public enum RECURRING_CREATEPROFILE_STATUS_MESSAGE
        {
            /// <summary>
            /// The Recurring profile was created successfully.
            /// </summary>
            Success,
            /// <summary>
            /// The user account does not allow recurring profile from being created for a reason or another.
            /// </summary>
            UserProfileError,
            /// <summary>
            /// An error occured from the gateway side - this could be caused also by incorrect information being supplied to the gateway, see logs.
            /// </summary>
            GatewayError,
            /// <summary>
            /// A buyer's country of residence (Payment Account Holder) does not match the shipping country of the transaction.
            /// </summary>
            BuyerCountryDoesNotMatchShippingCountry,
            /// <summary>
            /// An error in code logic within PayPippa has occured - see the logs.
            /// </summary>
            InternalError
        }

        public enum RECURRING_PAYMENT_NOTIFICATION_STATUS
        {
            Completed,
            Failed
        }
        public enum RecurringProfilePaymentStatus
        {
            Pending,
            Failed,
            Success
        }
        public enum NOTIFICATION_RESPONSE_TYPE
        {
            ImmediatePayment,
            RecurringPaymentFailure,
            /// <summary>
            /// This occurs when a payment is skipped. A skipped payment means when a payment is not done for some reason.  This is done in line to mimic PayPal functionality, where it will try 3 times, before
            /// marking a payment as failed.
            /// </summary>
            RecurringPaymentSkipped,
            RecurringPaymentSuccess,

            /// <summary>
            /// Cancelled means that the user willfully stopped it
            /// </summary>
            RecurringProfileCancelled,
            /// <summary>
            /// Suspended means that payment could not be taken, hence it was suspended
            /// </summary>
            RecurringProfileSuspended,
            /// <summary>
            /// Expired means that the profile has reached its billing limit / time period, e.g profile was for a total of 1 year.
            /// </summary>
            RecurringProfileExpired,
            RecurringProfileCouldNotCreate,

            
            RecurringProfileActivated,

            CancelRecurringProfile,
            

            

        }

        public enum RECURRING_BILLINGPERIOD
        {
            Day,
            Month,
            Year
        }

        public enum NOTIFICATION_STATUS_CODE
        {
            Success,
            Failure
        }

    }
}
