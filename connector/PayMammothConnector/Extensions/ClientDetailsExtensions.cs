﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Connector.Classes.Interfaces;

namespace PayMammoth.Connector.Extensions
{
    public static class ClientDetailsExtensions
    {

        public static string GetAddressAsOneLine(this IClientDetails details, bool includeCountry)
        {
            List<string> list = new List<string>();

            string s = null;

            list.Add(details.ClientAddress1);
            list.Add(details.ClientAddress2);
            list.Add(details.ClientAddress3);
            list.Add(details.ClientLocality);
            s = CS.General_v3.Util.Text.AppendStrings(", ", list);

            if (!string.IsNullOrEmpty(details.ClientPostCode))
                s += ". " + details.ClientPostCode;
            {
                var country = details.ClientCountry;

                if (country.HasValue)
                    s += ", " + CS.General_v3.Util.EnumUtils.StringValueOf(country.Value);
            }
            return s;
        }




        public static string GetClientFullName(this IClientDetails initialRequestDetails)
        {
            return CS.General_v3.Util.Text.AppendStrings(" ", initialRequestDetails.ClientFirstName, initialRequestDetails.ClientMiddleName, initialRequestDetails.ClientLastName);
            
        }
    }
}
