﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PayMammoth.Connector.Classes.Notifications;
using PayMammoth.Connector.Classes.Responses;

namespace PayMammoth.Connector
{
    public static class Events
    {

        public delegate void OnPaidSuccessfullyDelegate(INotificationMsg msg, ImmediatePaymentConfirmationResponse response);
        public delegate void OnRecurringPaymentDelegate(INotificationMsg msg, RecurringPaymentConfirmationResponse response);
        public delegate void OnRecurringProfileStatusChangeDelegate(INotificationMsg msg, RecurringProfileUpdateConfirmationResponse response);

        public static event OnPaidSuccessfullyDelegate OnPaidSuccessfully;
        
        internal static void invokeOnPaidSuccessfully(INotificationMsg msg, ImmediatePaymentConfirmationResponse response)
        {

            if (OnPaidSuccessfully != null)
            {
                OnPaidSuccessfully(msg, response);
            }
        }

        /// <summary>
        /// This is called for when an event related to recurring payment is done (success or fail)
        /// </summary>
        public static event OnRecurringPaymentDelegate OnRecurringPayment;

        internal static void invokeOnRecurringPayment(INotificationMsg msg, RecurringPaymentConfirmationResponse response)
        {

            if (OnRecurringPayment != null)
            {
                OnRecurringPayment(msg, response);
            }
        }


        /// <summary>
        /// This is called for when an event related to a recurring profile happens
        /// </summary>
        public static event OnRecurringProfileStatusChangeDelegate OnRecurringProfileStatusChange;

        internal static void invokeOnRecurringProfileStatusChange(INotificationMsg msg, RecurringProfileUpdateConfirmationResponse response)
        {

            if (OnRecurringProfileStatusChange != null)
            {
                OnRecurringProfileStatusChange(msg, response);
            }
        }
    }
}
