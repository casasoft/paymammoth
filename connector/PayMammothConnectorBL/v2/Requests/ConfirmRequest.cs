﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v2.Modules.v2.Order;

namespace PayMammothConnectorBL.v2.Requests
{
    public class ConfirmRequest :PayMammoth.Connector.Classes.Requests.ConfirmRequest
    {
        public ConfirmRequest(OrderBaseFrontend order)
            : base(order.Data.PayMammothIdentifier)
        {
            
        }
    }
}
