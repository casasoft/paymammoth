﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Extensions;
using BusinessLogic_v3.Modules.OrderModule;
using BusinessLogic_v3.Modules.OrderItemModule;

namespace PayMammothConnectorBL.v3.Requests
{
    public class InitialRequest : PayMammoth.Connector.Classes.Requests.InitialRequestDetails
    {
        public InitialRequest(string websiteAccountCode, string secretWord)
            : base(websiteAccountCode, secretWord)
        {

        }

        public void FillFromOrder(IOrderBase order)
        {

            this.ClientAddress1 = order.CustomerAddress1;
            this.ClientAddress2 = order.CustomerAddress2;
            this.ClientAddress3 = order.CustomerAddress3;
            this.ClientCountry = order.CustomerCountry;
            this.ClientEmail = order.CustomerEmail;
            this.ClientFirstName = order.CustomerFirstName;
            this.ClientLastName = order.CustomerLastName;
            this.ClientLocality = order.CustomerLocality;
            this.ClientPostCode = order.CustomerPostCode;

            {
                var curr = order.OrderCurrency;
                //this.cur
                this.CurrencyCode = curr.GetCurrencyISOAsEnumValue();

            }

            this.Title = "(Order: " + order.Reference + ")";

            foreach(IOrderItemBase orderItem in order.OrderItems)
            {
                this.AddItemDetail(orderItem.GetTitle(), orderItem.Description);
            }

            
            this.NotificationUrl = CS.General_v3.Util.PageUtil.GetApplicationBaseUrl() + Constants.StatusUrl;
            this.OrderLinkOnClientWebsite = order.ToFrontendBase().GetURL();
            this.PriceExcTax = order.GetTotalPrice(includeTax: false, includeShipping: false, reduceDiscount: true);
            this.Reference = order.ID.ToString();
            this.ClientReference = order.ID.ToString();
            //this.ReturnUrlFailure = null;
            //this.ReturnUrlSuccess = null;
            this.ShippingAmount = order.GetTotalShippingCost().GetValueOrDefault();
            this.TaxAmount = order.GetTotalTaxAmount();
        }

    }
}
