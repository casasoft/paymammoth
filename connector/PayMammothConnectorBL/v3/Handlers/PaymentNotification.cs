﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic_v3.Modules.OrderModule;
using log4net;

namespace PayMammothConnectorBL.v3.Handlers
{
    public class PaymentNotification : PayMammoth.Connector.Handlers.PaymentConfirmationHandler
    {
        private ILog _log = LogManager.GetLogger(typeof(PaymentNotification));
        protected override void OnPaidSuccessfully(PayMammoth.Connector.Classes.Responses.ConfirmResponse response)
        {
            if (_log.IsDebugEnabled)_log.Debug("OnPaidSuccessfully - Start()");
            string sOrderId = response.Reference;
            long orderId = 0;
            bool ok = false;
            if (long.TryParse(sOrderId, out orderId))
            {
                IOrderBase order = BusinessLogic_v3.Modules.Factories.OrderFactory.GetByPrimaryKey(orderId);
                if (order != null)
                {
                    //string sPaymentMethod = response.PaymentMethod.HasValue ? CS.General_v3.Util.EnumUtils.StringValueOf(response.PaymentMethod.Value) : "";
                    if (!response.PaymentMethod.HasValue)
                        throw new InvalidOperationException("Response.PaymentMethod should never be null - Ref: " + response.Reference);

                    order.PaymentProcessCompleted(response.AuthCode, response.RequiresManualPayment, response.PaymentMethod.Value);
                    ok = true;
                }

            }
            if (!ok)
            {
                if (_log.IsDebugEnabled)_log.Debug("OnPaidSuccessfully - Fail, Ref: " + sOrderId );
            }
            else
            {
                if (_log.IsDebugEnabled)_log.Debug("OnPaidSuccessfully - Ref: " + sOrderId + " - SUCCESS");
            }

        }
    }
}
